# LeAn®
[LeAn®](https://www.le-an.de) is the tool for municipalities to design vital downtowns and centers. A digital platform for forward-looking vacancy and settlement management that brings all user groups together around a "digital" table under the leadership of the municipality. The data protection-compliant web application makes it easier for municipalities to manage existing space, provides an up-to-date overview of property occupancy and (impending) vacancies, contains a dashboard with extensive relevant data on the environment and usability of the property, and records settlement applications for city-specific settlement management. As an ecosystem for downtown stakeholders, LeAn® enables municipalities to react quickly and match properties and reuse concepts precisely. of real estate and after-use concepts for sustainable neighborhood upgrading. Providers benefit from an attractive environment and a simplified location search, while property owners and the brokerage industry receive professional property exposés and the opportunity to actively shape the inner city together with the municipality on the basis of data. Developed as part of the project "[Stadtlabore für Deutschland: Leerstand und Ansiedlung](https://www.stadtlabore-deutschland)" (08/2021-12/2022) funded by the [Bundesministerium für Wirtschaft und Klimaschutz](https://www.bmwk.de), LeAn® provides the basis for a dialog at eye level between all inner city stakeholders and the foundation for standardized processes that enable forward-looking design.

## Video course
Further information on LeAn® and the project "Stadtlabore für Deutschland: Leerstand und Ansiedlung" is summarized in the [video course](https://www.stadtlabore-deutschland.de/videokurs/).

# License
LeAn® is Open Source Software published under the [MIT License](https://gitlab.opencode.de/bmwk/lean/lean/-/blob/main/LICENSE.txt) and developed by [immovativ GmbH](https://www.immovativ.de) as part of the "Stadtlabore für Deutschland: Leerstand und Ansiedlung" project (08/2021-12/2022) funded by the Bundesministerium für Wirtschaft und Klimaschutz.

# Technology Stack
LeAn® was developed on the basis of open programming languages and frameworks. Essentially, the platform is developed on the basis of [PHP](https://www.php.net/) with the [Symfony framework](https://symfony.com/) as well as [Typescript](https://www.typescriptlang.org/) and [React](https://reactjs.org/).

# System requirements
The following software modules are basic requirements for the operation of LeAn®. A correspondingly configured web server environment (e. g. nginx) based on Linux (e.g. alpine Linux from version 3.17) with a publicly accessible host name incl. SSL certificate is required.

# Packages:
- PHP (from version: 8.2.1)
- composer (from version:: 2.5.1)
- supervisor (from version:: 4.2.4)
- mariaDB (from version:: 10.5.12)
- dpkg-dev (from version:: 1.21.9)
- dpkg (from version:n: 1.21.9)
- libzip-dev (from version:: 1.9.2)
- libpng-dev (from version:: 1.6.38)
- imagemagick (from version:: 7.1.0.52)
- imagemagick-dev (from version:: 7.1.0.52)
- icu-dev (from version: 72.1)
- libssh2-dev (from version:: 1.10.0)
- autoconf (from version:: 2.71)
- build-base (from version:: 0.5)
- nginx (from version:: 1.22.1)
- fcgiwrap (from version:: 1.1.0)
- fcgiwrap-openrc (from version:: 1.1.0)
- openrc (from version:: 0.45.2)
- mapserver (from version:: 8.0.0)
- nodejs (from version:: 18.12)
- npm (from version:: 9.1.2)
- chromium (from version:: 108.0.5359.125)
- msttcorefonts-installer (from version:: 3.8)
- fontconfig (from version:: 2.14.1)
- ProFTPD (from version:: 1.3.7a)

### PHP extensions
- ctype
- fileinfo
- ftp
- gd
- iconv
- imagick
- intl
- pdo
- pdo_mysql
- simplexml
- ssh2
- xmlreader
- zip
- openssl

# Getting Started
The setup is highly dependent on the underlying server infrastructure. Knowledge of web servers or cloud infrastructures, databases and modern web applications based on PHP/Symfony is essential. Host names for the application and the ProFTP server to be integrated including SSL certificates is a basic requirement. The essential steps for setting up LeAn® are explained below.

1. set up web server environment (LeAn® is optimized for nginx)
2. set up a mariaDB database for LeAn
3. set up a mariaDB database for the ProFTP-Server to manage ftp users for OpenImmo import
4. optionally set up a mariaDB database for the EarlyWarningSystem ([Frühwarnsystem](https://gitlab.opencode.de/bmwk/lean/lean-fruehwarnsystem))
5. clone repository with the last stable version from git
6. configure the ENV-variables
7. run composer install
8. run npm install
9. create the databases with Symfonys migration tool
10. import location data by executing the symfony console with app:location:import
11. import industry and nac classifications executing the symfony console with
    1. app:classification:import:industry-classification
    2. app:classification:import:nace
    3. app:classification:link
12. create a master-account for LeAn® by executing the symfony console with app:account:create; you are asked to enter
    1. the AccountName (city or institution operating LeAn)
    2. the Email-Adress for the account
    3. the App-Hostname (externally accessible hostname where LeAn-Instance is running)
    4. the PlaceId (ID of the city for which the LeAn instance is set up – must be taken from the place entity)
13. optionaly implement the necessary scripts located in /src/Command as cronjobs

# Information about the ENV variables
To setup the LeAn® infrastructure you have to configure several environment variables for your instance:

## DATABASE_URL
LeAn® needs this configuration to write the database model to MariaDB and store all data of the application.

## DATABASE_PROFTPD_URL
LeAn® needs this database to store the FTP accounts for the OpenImmo interface for real estate agents.

## DATABASE_EWS_URL
If you want to use the [early warning system](https://gitlab.opencode.de/bmwk/lean/lean-fruehwarnsystem), it needs its own database.

## MAILER_DSN
For LeAn® to be able to send e-mails, SMTP access must be entered.
[More information about Symfonys Mailer component.](https://symfony.com/doc/current/mailer.html)

## DEFAULT_URI_FOR_PDF_GENERATOR
The domain pointing to the PDF generator. Must be set, for example, if LeAn® runs behind a proxy. If it is left empty the DEFAULT_API_URL will be used.

## Hosting provider data
The hoster of the LeAn® instance, which is displayed in the footer of the platform.

## NO_REPLY_EMAIL_ADDRESS
The email address where system e-mails are sent. It should correspond with MAILER_DNS.

## Customer Care data
It can be defined whether a ticket system (OsTicket) is to be connected in LeAn® or whether support is to be offered only via email.

## GOOGLE_MAPS_API_KEY
If you want to use Google Maps for reverse geocoding, then a valid GoogleMaps API key must be entered.

## FTP User data
Configuration for ProFTP.

## OpenImmo Importer data
Defined Hostname and upload path for the ProFTP-Host to handle OpenImmo-Files.

## OpenImmo Exporter data
TempPath where XML-Files are generated and ziped befor send to an external property portal. 

## Anonymize/Delete data
Enable or disable Backup for anonymized data and set time till deletion
