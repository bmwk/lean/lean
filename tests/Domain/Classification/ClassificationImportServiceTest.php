<?php

declare(strict_types=1);

namespace App\Tests\Domain\Classification;

use App\Domain\Classification\ClassificationImportService;
use App\Domain\Classification\ClassificationService;
use App\Domain\Entity\Classification\ClassificationExample;
use App\Domain\Entity\Classification\IndustryClassification;
use App\Domain\Entity\Classification\NaceClassification;
use App\Domain\Entity\Classification\NaceClassificationLevel;
use App\Repository\Classification\ClassificationExampleRepository;
use App\Repository\Classification\IndustryClassificationRepository;
use App\Repository\Classification\NaceClassificationRepository;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;

class ClassificationImportServiceTest extends TestCase
{
    public function testImportNaceClassifications()
    {
        $testData = [
            0 => [
                'Schlüssel WZ 2008'         => 'A',
                'Ebene'                     => '1',
                'Titel'                     => 'Land- und Forstwirtschaft, Fischerei',
                'Erläuterungstitel'         => '',
                'Kurztitel'                 => 'Land-u. Forstwirtschaft, Fischerei',
                'Mitteltitel'               => 'Land-u. Forstwirtschaft,Fischerei',
                'Langtitel'                 => 'Land- und Forstwirtschaft, Fischerei',
                'Allgemeine Bemerkungen'    => '',
                'Einschlüsse'               => 'Dieser Abschnitt umfasst die Nutzung der...',
                'Umfasst ferner'            => '',
                'Ausschlüsse'               => '',
                'Maßeinheit'                => '',
            ],
            1 => [
                'Schlüssel WZ 2008'      => '01',
                'Ebene'                  => '2',
                'Titel'                  => 'Landwirtschaft, Jagd und damit verbundene Tätigkeiten',
                'Erläuterungstitel'      => '',
                'Kurztitel'              => 'Landwirtschaft,Jagd u. verbundene Tätigkeiten',
                'Mitteltitel'            => 'Landwirtschaft,Jagd u.verbundene Tätigkeiten',
                'Langtitel'              => 'Landwirtschaft, Jagd und damit verbundene Tätigkeiten',
                'Allgemeine Bemerkungen' => '',
                'Einschlüsse'            => 'Diese Abteilung umfasst die beiden Tätigkeitsbereiche...',
                'Umfasst ferner'         => '',
                'Ausschlüsse'            => 'Diese Abteilung umfasst nicht: ...',
                'Maßeinheit'             => '',
            ],

            2 => [
                'Schlüssel WZ 2008'      => '01.1',
                'Ebene'                  => '3',
                'Titel'                  => 'Something',
            ],
            3 => [
                'Schlüssel WZ 2008'      => '01.11',
                'Ebene'                  => '4',
                'Titel'                  => 'Something 11',
            ],
            4 => [
                'Schlüssel WZ 2008'      => '01.11.0',
                'Ebene'                  => '5',
                'Titel'                  => 'Something 11.0',
            ],
            5 => [
                'Schlüssel WZ 2008'      => '01.2',
                'Ebene'                  => '3',
                'Titel'                  => 'Something 2',
            ],
            6 => [
                'Schlüssel WZ 2008'      => '01.21',
                'Ebene'                  => '4',
                'Titel'                  => 'Something 21',
            ],
            7 => [
                'Schlüssel WZ 2008'      => '01.21.0',
                'Ebene'                  => '5',
                'Titel'                  => 'Something 21.0',
            ],
            8 => [
                'Schlüssel WZ 2008'      => 'B',
                'Ebene'                  => '1',
                'Titel'                  => 'B-Thing',
            ],
        ];

        $entityManager = $this->getMockBuilder(EntityManagerInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $naceClassificationRepository = $this->getMockBuilder(NaceClassificationRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $industryClassificationRepository = $this->getMockBuilder(IndustryClassificationRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $classificationExampleRepository = $this->getMockBuilder(ClassificationExampleRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $classificationService = new ClassificationService(
            $naceClassificationRepository,
            $industryClassificationRepository,
            $classificationExampleRepository
        );

        $classificationImportService = new ClassificationImportService($entityManager, $classificationService);
        $naceClassifications = $classificationImportService->importNaceClassificationsFromCsvToDatabase($testData);

        $expectedNaceClassification = new NaceClassification();
        $expectedNaceClassification
            ->setName('Land- und Forstwirtschaft, Fischerei')
            ->setCode('A')
            ->setLevel(NaceClassificationLevel::LEVEL_ONE);

        $secondNaceClasification = new NaceClassification();
        $secondNaceClasification
            ->setName($testData[1]['Titel'])
            ->setCode('01')
            ->setParent($expectedNaceClassification)
            ->setLevel(NaceClassificationLevel::LEVEL_TWO);

        $this->assertEquals(9, count($naceClassifications));
        $this->assertEquals($expectedNaceClassification, $naceClassifications[0]);

        $returnedFirstNaceClassification = $naceClassifications[0];
        $this->assertEquals($returnedFirstNaceClassification->getName(), $testData[0]['Titel']);
        $this->assertEquals($returnedFirstNaceClassification->getCode(), $testData[0]['Schlüssel WZ 2008']);

        $returnedSecondNaceClassification = $naceClassifications[1];
        $this->assertEquals($secondNaceClasification, $returnedSecondNaceClassification);

        $this->assertSame($naceClassifications[5]->getParent(), $naceClassifications[1]);
        $this->assertNull($naceClassifications[8]->getParent());
    }

    public function testImportIndustryClassifications(): void
    {
        $testData = [
            [
                'Nutzungsart-ID' => '1',
                'Nutzungsart' => 'Einzelhandel',
                'Branchenbereich-ID' => null,
                'Branchenbereich' => null,
                'Branchendetail-ID' => null,
                'Branchendetail' => null,
                'WZ-Codes' => null,
                'Beispiele-Teilbranchen' => null,
                'Beispiele-Marken' => null
            ],
            [
                'Nutzungsart-ID' => '1',
                'Nutzungsart' => 'Einzelhandel',
                'Branchenbereich-ID' => '1',
                'Branchenbereich' => 'Lebensmittel & Getränke',
                'Branchendetail-ID' => null,
                'Branchendetail' => null,
                'WZ-Codes' => null,
                'Beispiele-Teilbranchen' => null,
                'Beispiele-Marken' => null
            ],
            [
                'Nutzungsart-ID' => '1',
                'Nutzungsart' => 'Einzelhandel',
                'Branchenbereich-ID' => '1',
                'Branchenbereich' => 'Lebensmittel & Getränke',
                'Branchendetail-ID' => '1',
                'Branchendetail' => 'Geschäfte mit umfassendem Lebensmittelsortiment (Supermarkt, Discounter, Verbrauchermarkt, SB-Warenhaus, Bio-Supermärkte, Lebenmittelminimarkt u. Ä.)',
                'WZ-Codes' => '47.11',
                'Beispiele-Teilbranchen' => 'Insbesondere überregionale, aber auch lokale/regionale Anbieter',
                'Beispiele-Marken' => 'Edeka, Netto, Rewe, Rewe to-go, Penny, Aldi, Lidl, Kaufland, Globus, Bartels-Langness, Citi, Norma, Bünting, Dohle, Dennree, Tegut, teo, Alnatura, Basic, Spar Express, Denn\'s Biomarkt, Marktkauf, Hit, Famila, Feneberg, Wasgau, Bio Company, Konsum-Markt, Super Bio Markt'
            ],
            [
                'Nutzungsart-ID' => '1',
                'Nutzungsart' => 'Einzelhandel',
                'Branchenbereich-ID' => '2',
                'Branchenbereich' => 'Kiosk, Büdchen, Trinkhalle o.Ä.',
                'Branchendetail-ID' => null,
                'Branchendetail' => null,
                'WZ-Codes' => null,
                'Beispiele-Teilbranchen' => null,
                'Beispiele-Marken' => null
            ],
            [
                'Nutzungsart-ID' => '1',
                'Nutzungsart' => 'Einzelhandel',
                'Branchenbereich-ID' => '7',
                'Branchenbereich' => 'Sport, Spiel, Hobby',
                'Branchendetail-ID' => null,
                'Branchendetail' => null,
                'WZ-Codes' => null,
                'Beispiele-Teilbranchen' => null,
                'Beispiele-Marken' => null
            ],
            [
                'Nutzungsart-ID' => '1',
                'Nutzungsart' => 'Einzelhandel',
                'Branchenbereich-ID' => '7',
                'Branchenbereich' => 'Sport, Spiel, Hobby',
                'Branchendetail-ID' => '1',
                'Branchendetail' => 'Spielwaren, Bastelbedarf, Modellbau, Karnevalsartikel, Kinderfahrzeuge, Puppen u. Ä.',
                'WZ-Codes' => '47.65',
                'Beispiele-Teilbranchen' => 'Insbesondere überregionale, aber auch lokale/regionale Anbieter',
                'Beispiele-Marken' => 'Edeka, Netto, Rewe, Rewe to-go, Penny, Aldi, Lidl, Kaufland, Globus, Bartels-Langness, Citi, Norma, Bünting, Dohle, Dennree, Tegut, teo, Alnatura, Basic, Spar Express, Denn\'s Biomarkt, Marktkauf, Hit, Famila, Feneberg, Wasgau, Bio Company, Konsum-Markt, Super Bio Markt'
            ],
            [
                'Nutzungsart-ID' => '2',
                'Nutzungsart' => 'Something else',
                'Branchenbereich-ID' => null,
                'Branchenbereich' => null,
                'Branchendetail-ID' => null,
                'Branchendetail' => null,
                'WZ-Codes' => null,
                'Beispiele-Teilbranchen' => null,
                'Beispiele-Marken' => null
            ],
        ];
        $entityManager = $this->getMockBuilder(EntityManagerInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $naceClassificationRepository = $this->getMockBuilder(NaceClassificationRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $industryClassificationRepository = $this->getMockBuilder(IndustryClassificationRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $classificationExampleRepository = $this->getMockBuilder(ClassificationExampleRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $classificationService = new ClassificationService(
            $naceClassificationRepository,
            $industryClassificationRepository,
            $classificationExampleRepository
        );

        $classificationImportService = new ClassificationImportService(
            entityManager: $entityManager,
            classificationService: $classificationService
        );

        $industryClassifications = $classificationImportService->importIndustryClassificationsFromCsvIntoDatabase($testData);

        $this->assertNull($industryClassifications[0]->getParent());
        $this->assertEquals(
            expected: 'Einzelhandel',
            actual: $industryClassifications[0]->getName()
        );
        $this->assertSame(
            expected: $industryClassifications[0],
            actual: $industryClassifications[1]->getParent()
        );
        $this->assertEquals(
            expected: 'Lebensmittel & Getränke',
            actual: $industryClassifications[1]->getName()
        );
        $this->assertSame(
            expected: $industryClassifications[1],
            actual: $industryClassifications[2]->getParent()
        );
        $this->assertEquals(
            expected: 'Geschäfte mit umfassendem Lebensmittelsortiment (Supermarkt, Discounter, Verbrauchermarkt, SB-Warenhaus, Bio-Supermärkte, Lebenmittelminimarkt u. Ä.)',
            actual: $industryClassifications[2]->getName()
        );
        $this->assertSame(
            expected: $industryClassifications[0],
            actual: $industryClassifications[3]->getParent()
        );
        $this->assertSame(
            expected: $industryClassifications[0],
            actual: $industryClassifications[4]->getParent()
        );
        $this->assertSame(
            expected: $industryClassifications[4],
            actual: $industryClassifications[5]->getParent()
        );
        $this->assertNull($industryClassifications[6]->getParent());
    }

    public function testNormalizeIndustryClassificationCsvLinesAfterBrandName(): void
    {
        $testData = [
            [
                'Nutzungsart-ID' => 1,
                'Nutzungsart' => 'Einzelhandel',
                'Branchenbereich-ID' => 1,
                'Branchenbereich' => 'Lebensmittel & Getränke',
                'Branchendetail-ID' => 1,
                'Branchendetail' => 'Geschäfte mit umfassendem Lebensmittelsortiment (Supermarkt, Discounter, Verbrauchermarkt, SB-Warenhaus, Bio-Supermärkte, Lebenmittelminimarkt u. Ä.)',
                'WZ-Codes' => '47.11',
                'Beispiele-Teilbranchen' => 'Insbesondere überregionale, aber auch lokale/regionale Anbieter',
                'Beispiele-Marken' => 'Edeka, Netto'
            ]
        ];

        $expectedResult = [
            [
                'Nutzungsart-ID' => 1,
                'Nutzungsart' => 'Einzelhandel',
                'Branchenbereich-ID' => 1,
                'Branchenbereich' => 'Lebensmittel & Getränke',
                'Branchendetail-ID' => 1,
                'Branchendetail' => 'Geschäfte mit umfassendem Lebensmittelsortiment (Supermarkt, Discounter, Verbrauchermarkt, SB-Warenhaus, Bio-Supermärkte, Lebenmittelminimarkt u. Ä.)',
                'WZ-Codes' => '47.11',
                'Beispiele-Teilbranchen' => 'Insbesondere überregionale, aber auch lokale/regionale Anbieter',
                'Beispiele-Marken' => 'Edeka'
            ], [
                'Nutzungsart-ID' => 1,
                'Nutzungsart' => 'Einzelhandel',
                'Branchenbereich-ID' => 1,
                'Branchenbereich' => 'Lebensmittel & Getränke',
                'Branchendetail-ID' => 1,
                'Branchendetail' => 'Geschäfte mit umfassendem Lebensmittelsortiment (Supermarkt, Discounter, Verbrauchermarkt, SB-Warenhaus, Bio-Supermärkte, Lebenmittelminimarkt u. Ä.)',
                'WZ-Codes' => '47.11',
                'Beispiele-Teilbranchen' => 'Insbesondere überregionale, aber auch lokale/regionale Anbieter',
                'Beispiele-Marken' => 'Netto'
            ]
        ];

        $entityManager = $this->getMockBuilder(EntityManagerInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $naceClassificationRepository = $this->getMockBuilder(NaceClassificationRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $industryClassificationRepository = $this->getMockBuilder(IndustryClassificationRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $classificationExampleRepository = $this->getMockBuilder(ClassificationExampleRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $classificationService = new ClassificationService(
            $naceClassificationRepository,
            $industryClassificationRepository,
            $classificationExampleRepository
        );

        $classificationImportService = new ClassificationImportService(
            entityManager: $entityManager,
            classificationService: $classificationService
        );

        $this->assertEquals(
            $expectedResult,
            $classificationImportService->normalizeIndustryClassificationCsvLinesAfterBrandName($testData)
        );

        $testDataWithoutBrands = [
            [
                'Nutzungsart-ID' => 1,
                'Nutzungsart' => 'Einzelhandel',
                'Branchenbereich-ID' => 1,
                'Branchenbereich' => 'Lebensmittel & Getränke',
                'Branchendetail-ID' => 1,
                'Branchendetail' => 'Geschäfte mit umfassendem Lebensmittelsortiment (Supermarkt, Discounter, Verbrauchermarkt, SB-Warenhaus, Bio-Supermärkte, Lebenmittelminimarkt u. Ä.)',
                'WZ-Codes' => '47.11',
                'Beispiele-Teilbranchen' => 'Insbesondere überregionale, aber auch lokale/regionale Anbieter',
                'Beispiele-Marken' => ''
            ]
        ];

        $this->assertEquals(
            [],
            $classificationImportService->normalizeIndustryClassificationCsvLinesAfterBrandName($testDataWithoutBrands)
        );
    }

    public function testImportClassificationExamplesFromCsvIntoDatabase(): void
    {
        $testData = [
            [
                'Nutzungsart-ID' => 1,
                'Nutzungsart' => 'Einzelhandel',
                'Branchenbereich-ID' => 1,
                'Branchenbereich' => 'Lebensmittel & Getränke',
                'Branchendetail-ID' => 1,
                'Branchendetail' => 'Geschäfte mit umfassendem Lebensmittelsortiment (Supermarkt, Discounter, Verbrauchermarkt, SB-Warenhaus, Bio-Supermärkte, Lebenmittelminimarkt u. Ä.)',
                'WZ-Codes' => '47.11',
                'Beispiele-Teilbranchen' => 'Insbesondere überregionale, aber auch lokale/regionale Anbieter',
                'Beispiele-Marken' => 'Edeka'
            ]
        ];

        $naceCode = new NaceClassification();
        $naceCode
            ->setId(1)
            ->setName('Something 47.11')
            ->setCode('47.11');

        $industryClassification = new IndustryClassification();
        $industryClassification
            ->setId(1)
            ->setName('Geschäfte mit umfassendem Lebensmittelsortiment...')
            ->setLevelOne(1)
            ->setLevelTwo(1)
            ->setLevelThree(1);

        $expectedClassificationExample = new ClassificationExample();
        $expectedClassificationExample
            ->setBrandName('Edeka')
            ->getNaceClassifications()->add($naceCode);

        $expectedClassificationExample
            ->getIndustryClassifications()
            ->add($industryClassification);

        $entityManager = $this->getMockBuilder(EntityManagerInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $naceClassificationRepository = $this->getMockBuilder(NaceClassificationRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $industryClassificationRepository = $this->getMockBuilder(IndustryClassificationRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $classificationExampleRepository = $this->getMockBuilder(ClassificationExampleRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $classificationService = new ClassificationService(
            $naceClassificationRepository,
            $industryClassificationRepository,
            $classificationExampleRepository
        );

        $classificationImportService = new ClassificationImportService(
            entityManager: $entityManager,
            classificationService: $classificationService
        );

        $naceClassificationRepository
            ->expects($this->once())
            ->method('findBy')
            ->with(
                $this->equalTo(['code' => ['47.11']])
            )
            ->willReturn([$naceCode]);

        $industryClassificationRepository
            ->expects($this->once())
            ->method('findOneBy')
            ->with(
                [
                    'levelOne'   => 1,
                    'levelTwo'   => 1,
                    'levelThree' => 1
                ]
            )
            ->willReturn($industryClassification);

        $this->assertEquals(
            [$expectedClassificationExample],
            $classificationImportService->importClassificationExamplesFromCsvIntoDatabase($testData)
        );
    }

    public function testImportClassificationExamplesFromCsvIntoDatabaseMultiple(): void
    {
        $testData = [
            [
                'Nutzungsart-ID' => 1,
                'Nutzungsart' => 'Einzelhandel',
                'Branchenbereich-ID' => 1,
                'Branchenbereich' => 'Lebensmittel & Getränke',
                'Branchendetail-ID' => 1,
                'Branchendetail' => 'Geschäfte mit umfassendem Lebensmittelsortiment (Supermarkt, Discounter, Verbrauchermarkt, SB-Warenhaus, Bio-Supermärkte, Lebenmittelminimarkt u. Ä.)',
                'WZ-Codes' => '47.11',
                'Beispiele-Teilbranchen' => 'Insbesondere überregionale, aber auch lokale/regionale Anbieter',
                'Beispiele-Marken' => 'Edeka'
            ], [
                'Nutzungsart-ID' => 1,
                'Nutzungsart' => 'Einzelhandel',
                'Branchenbereich-ID' => 1,
                'Branchenbereich' => 'Lebensmittel & Getränke',
                'Branchendetail-ID' => 3,
                'Branchendetail' => 'Something...',
                'WZ-Codes' => '47.12',
                'Beispiele-Teilbranchen' => 'Insbesondere überregionale, aber auch lokale/regionale Anbieter',
                'Beispiele-Marken' => 'Edeka'
            ]
        ];

        $naceCode = new NaceClassification();
        $naceCode
            ->setId(1)
            ->setName('Something 47.11')
            ->setCode('47.11');

        $secondNaceCode = new NaceClassification();
        $secondNaceCode
            ->setId(1)
            ->setName('Something 47.12')
            ->setCode('47.12');

        $industryClassification = new IndustryClassification();
        $industryClassification
            ->setId(1)
            ->setName('Geschäfte mit umfassendem Lebensmittelsortiment...')
            ->setLevelOne(1)
            ->setLevelTwo(1)
            ->setLevelThree(1);

        $secondIndustryClassification = new IndustryClassification();
        $secondIndustryClassification
            ->setId(1)
            ->setName('Something...')
            ->setLevelOne(1)
            ->setLevelTwo(1)
            ->setLevelThree(3);

        $expectedClassificationExample = new ClassificationExample();
        $expectedClassificationExample
            ->setBrandName('Edeka')
            ->getNaceClassifications()->add($naceCode);

        $expectedClassificationExample
            ->getNaceClassifications()->add($secondNaceCode);

        $expectedClassificationExample
            ->getIndustryClassifications()
            ->add($industryClassification);

        $expectedClassificationExample
            ->getIndustryClassifications()
            ->add($secondIndustryClassification);

        $entityManager = $this->getMockBuilder(EntityManagerInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $naceClassificationRepository = $this->getMockBuilder(NaceClassificationRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $industryClassificationRepository = $this->getMockBuilder(IndustryClassificationRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $classificationExampleRepository = $this->getMockBuilder(ClassificationExampleRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $classificationService = new ClassificationService(
            $naceClassificationRepository,
            $industryClassificationRepository,
            $classificationExampleRepository
        );

        $classificationImportService = new ClassificationImportService(
            entityManager: $entityManager,
            classificationService: $classificationService
        );

        $naceClassificationRepository
            ->expects($this->exactly(2))
            ->method('findBy')
            ->withConsecutive(
                [$this->equalTo(['code' => ['47.11']])],
                [$this->equalTo(['code' => ['47.12']])]
            )
            ->willReturnOnConsecutiveCalls([$naceCode], [$secondNaceCode]);

        $industryClassificationRepository
            ->expects($this->exactly(2))
            ->method('findOneBy')
            ->withConsecutive(
                [
                    $this->equalTo(
                        [
                            'levelOne'   => 1,
                            'levelTwo'   => 1,
                            'levelThree' => 1
                        ]
                    )
                ],
                [
                    $this->equalTo(
                        [
                            'levelOne'   => 1,
                            'levelTwo'   => 1,
                            'levelThree' => 3
                        ]
                    )
                ]
            )
            ->willReturnOnConsecutiveCalls($industryClassification, $secondIndustryClassification);

        $this->assertEquals(
            [$expectedClassificationExample],
            $classificationImportService->importClassificationExamplesFromCsvIntoDatabase($testData)
        );
    }

    public function testGetTopMostParentFromIndustryClassification(): void
    {
        $naceClassificationRepository = $this->getMockBuilder(NaceClassificationRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $industryClassificationRepository = $this->getMockBuilder(IndustryClassificationRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $classificationExampleRepository = $this->getMockBuilder(ClassificationExampleRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $classificationService = new ClassificationService(
            $naceClassificationRepository,
            $industryClassificationRepository,
            $classificationExampleRepository
        );

        $industryClassification = new IndustryClassification();
        $industryClassification
            ->setLevelOne(1)
            ->setName('Test Level One');

        $this->assertSame(
            $industryClassification,
            $classificationService->getTopMostParentFromIndustryClassification($industryClassification)
        );

        $levelTwoClassification = new IndustryClassification();
        $levelTwoClassification
            ->setLevelOne(1)
            ->setLevelTwo(1)
            ->setParent($industryClassification);

        $this->assertSame(
            $industryClassification,
            $classificationService->getTopMostParentFromIndustryClassification($industryClassification)
        );
    }
}
