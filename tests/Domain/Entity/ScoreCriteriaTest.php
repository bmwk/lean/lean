<?php

declare(strict_types=1);

namespace App\Tests\Domain\Entity;

use App\Domain\Entity\LookingForPropertyRequest\ScoreCriteria;
use App\Domain\Entity\MinMaxScoreCriterion;
use App\Domain\Entity\ScoreCriterion;
use PHPUnit\Framework\TestCase;

class ScoreCriteriaTest extends TestCase
{
    public function testCalculatePoints(): void
    {
        $scoreCriteria = new ScoreCriteria();

        $minimumSpace = new ScoreCriterion();
        $minimumSpace
            ->setSet(true)
            ->setEvaluationResult(true)
            ->setName('minimumSpace')
            ->setPoints(8);

        $maximumSpace = new ScoreCriterion();
        $maximumSpace
            ->setSet(true)
            ->setEvaluationResult(true)
            ->setName('maximumSpace')
            ->setPoints(8);

        $space = new MinMaxScoreCriterion();
        $space
            ->setMinScoreCriterion($minimumSpace)
            ->setMaxScoreCriterion($maximumSpace)
            ->setName('space');

        $roomCount = new ScoreCriterion();
        $roomCount
            ->setSet(true)
            ->setEvaluationResult(true)
            ->setName('roomCount')
            ->setPoints(6);

        $numberOfParkingLots = new ScoreCriterion();
        $numberOfParkingLots
            ->setSet(true)
            ->setEvaluationResult(true)
            ->setName('numberOfParkingLots')
            ->setPoints(6);

        $availableFrom = new ScoreCriterion();
        $availableFrom
            ->setSet(true)
            ->setEvaluationResult(true)
            ->setName('availableFrom')
            ->setPoints(6);

        $minUsableSpace = new ScoreCriterion();
        $minUsableSpace
            ->setSet(true)
            ->setEvaluationResult(true)
            ->setName('minUsableSpace')
            ->setPoints(3);

        $maxUsableSpace = new ScoreCriterion();
        $maxUsableSpace
            ->setSet(true)
            ->setEvaluationResult(true)
            ->setName('maxStorageSpace')
            ->setPoints(3);

        $usableSpace = new MinMaxScoreCriterion();
        $usableSpace
            ->setName('storageSpace')
            ->setMinScoreCriterion($minUsableSpace)
            ->setMaxScoreCriterion($maxUsableSpace);

        $minRetailSpace = new ScoreCriterion();
        $minRetailSpace
            ->setName('minRetailSpace')
            ->setSet(true)
            ->setEvaluationResult(true)
            ->setPoints(8);

        $maxRetailSpace = new ScoreCriterion();
        $maxRetailSpace
            ->setName('maxRetailSpace')
            ->setSet(true)
            ->setEvaluationResult(true)
            ->setPoints(8);

        $retailSpace = new MinMaxScoreCriterion();
        $retailSpace
            ->setName('retailSpace')
            ->setMinScoreCriterion($minRetailSpace)
            ->setMaxScoreCriterion($maxRetailSpace);

        $minOutdoorSpace = new ScoreCriterion();
        $minOutdoorSpace
            ->setSet(true)
            ->setEvaluationResult(true)
            ->setName('minOutdoorSpace')
            ->setPoints(3);

        $maxOutdoorSpace = new ScoreCriterion();
        $maxOutdoorSpace
            ->setSet(true)
            ->setEvaluationResult(true)
            ->setName('maxOutdoorSpace')
            ->setPoints(3);

        $outdoorSpace = new MinMaxScoreCriterion();
        $outdoorSpace
            ->setName('outdoorSpace')
            ->setMinScoreCriterion($minOutdoorSpace)
            ->setMaxScoreCriterion($maxOutdoorSpace);

        $minStorageSpace = new ScoreCriterion();
        $minStorageSpace
            ->setSet(true)
            ->setEvaluationResult(true)
            ->setName('minStorageSpace')
            ->setPoints(3);

        $maxStorageSpace = new ScoreCriterion();
        $maxStorageSpace
            ->setSet(true)
            ->setEvaluationResult(true)
            ->setName('maxStorageSpace')
            ->setPoints(3);

        $storageSpace = new MinMaxScoreCriterion();
        $storageSpace
            ->setName('storageSpace')
            ->setMinScoreCriterion($minStorageSpace)
            ->setMaxScoreCriterion($maxStorageSpace);

        $minShopWindowLength = new ScoreCriterion();
        $minShopWindowLength
            ->setSet(true)
            ->setEvaluationResult(true)
            ->setName('minShopWindowLength')
            ->setPoints(8);

        $maxShopWindowLength = new ScoreCriterion();
        $maxShopWindowLength
            ->setSet(true)
            ->setEvaluationResult(true)
            ->setName('maxShopWindowLength')
            ->setPoints(8);

        $shopWindowLength = new MinMaxScoreCriterion();
        $shopWindowLength
            ->setName('shopWindowLength')
            ->setMinScoreCriterion($minShopWindowLength)
            ->setMaxScoreCriterion($maxShopWindowLength);

        $minShopWidth = new ScoreCriterion();
        $minShopWidth
            ->setSet(true)
            ->setEvaluationResult(true)
            ->setName('minShopWidth')
            ->setPoints(8);

        $maxShopWidth = new ScoreCriterion();
        $maxShopWidth
            ->setSet(true)
            ->setEvaluationResult(true)
            ->setName('maxShopWidth')
            ->setPoints(8);

        $shopWidth = new MinMaxScoreCriterion();
        $shopWidth
            ->setName('shopWidth')
            ->setMinScoreCriterion($minShopWidth)
            ->setMaxScoreCriterion($maxShopWidth);

        $locationCategory = new ScoreCriterion();
        $locationCategory
            ->setSet(true)
            ->setEvaluationResult(true)
            ->setName('locationCategory')
            ->setPoints(6);

        $maxPrice = new ScoreCriterion();
        $maxPrice
            ->setSet(true)
            ->setEvaluationResult(true)
            ->setName('maxPrice')
            ->setPoints(10);

        $minPrice = new ScoreCriterion();
        $minPrice
            ->setSet(true)
            ->setEvaluationResult(true)
            ->setName('minPrice')
            ->setPoints(10);

        $branchDensity = new ScoreCriterion();
        $branchDensity
            ->setSet(true)
            ->setEvaluationResult(true)
            ->setName('branchDensity')
            ->setPoints(10);

        $scoreCriteria
            ->setSpace($space)
            ->setRoomCount($roomCount)
            ->setNumberOfParkingLots($numberOfParkingLots)
            ->setAvailableFrom($availableFrom)
            ->setSubsidiarySpace($usableSpace)
            ->setRetailSpace($retailSpace)
            ->setOutdoorSpace($outdoorSpace)
            ->setStorageSpace($storageSpace)
            ->setShopWindowLength($shopWindowLength)
            ->setShopWidth($shopWidth)
            ->setLocationCategory($locationCategory)
            ->setMaximumPrice($maxPrice)
            ->setMinimumPrice($minPrice)
            ->setIndustryDensity($branchDensity);

        $this->assertEquals(136, $scoreCriteria->calculateMaxPoints());

        $branchDensity->setSet(false);
        $this->assertEquals(126, $scoreCriteria->calculatePoints());

        $minimumSpace->setEvaluationResult(false);
        $this->assertEquals(110, $scoreCriteria->calculatePoints());

        $minimumSpace->setSet(false);
        $this->assertEquals(118, $scoreCriteria->calculatePoints());

        $maximumSpace->setEvaluationResult(false);
        $this->assertEquals(110, $scoreCriteria->calculatePoints());

        $maximumSpace->setSet(false);
        $this->assertEquals(110, $scoreCriteria->calculatePoints());

        $minPrice->setEvaluationResult(false);
        $this->assertEquals(100, $scoreCriteria->calculatePoints());

        $minPrice->setSet(false);
        $this->assertEquals(100, $scoreCriteria->calculatePoints());

        $maxPrice->setEvaluationResult(false);
        $this->assertEquals(90, $scoreCriteria->calculatePoints());

        $maxPrice->setSet(false);
        $this->assertEquals(90, $scoreCriteria->calculatePoints());

        $minRetailSpace->setEvaluationResult(false);
        $this->assertEquals(74, $scoreCriteria->calculatePoints());

        $minRetailSpace->setSet(false);
        $this->assertEquals(82, $scoreCriteria->calculatePoints());

        $maxRetailSpace->setEvaluationResult(false);
        $this->assertEquals(74, $scoreCriteria->calculatePoints());

        $maxRetailSpace->setSet(false);
        $this->assertEquals(74, $scoreCriteria->calculatePoints());

        $minShopWidth->setEvaluationResult(false);
        $this->assertEquals(58, $scoreCriteria->calculatePoints());

        $minShopWidth->setSet(false);
        $this->assertEquals(66, $scoreCriteria->calculatePoints());

        $maxShopWidth->setEvaluationResult(false);
        $this->assertEquals(58, $scoreCriteria->calculatePoints());

        $maxShopWidth->setSet(false);
        $this->assertEquals(58, $scoreCriteria->calculatePoints());

        $roomCount->setEvaluationResult(false);
        $this->assertEquals(52, $scoreCriteria->calculatePoints());

        $numberOfParkingLots->setEvaluationResult(false);
        $this->assertEquals(46, $scoreCriteria->calculatePoints());

        $availableFrom->setSet(false);
        $this->assertEquals(40, $scoreCriteria->calculatePoints());

        $minUsableSpace->setEvaluationResult(false);
        $this->assertEquals(34, $scoreCriteria->calculatePoints());

        $minUsableSpace->setSet(false);
        $this->assertEquals(37, $scoreCriteria->calculatePoints());

        $maxUsableSpace->setEvaluationResult(false);
        $this->assertEquals(34, $scoreCriteria->calculatePoints());

        $maxUsableSpace->setSet(false);
        $this->assertEquals(34, $scoreCriteria->calculatePoints());

        $minOutdoorSpace->setEvaluationResult(false);
        $this->assertEquals(28, $scoreCriteria->calculatePoints());

        $minOutdoorSpace->setSet(false);
        $this->assertEquals(31, $scoreCriteria->calculatePoints());

        $maxOutdoorSpace->setEvaluationResult(false);
        $this->assertEquals(28, $scoreCriteria->calculatePoints());

        $maxOutdoorSpace->setSet(false);
        $this->assertEquals(28, $scoreCriteria->calculatePoints());

        $minStorageSpace->setEvaluationResult(false);
        $this->assertEquals(22, $scoreCriteria->calculatePoints());

        $minStorageSpace->setSet(false);
        $this->assertEquals(25, $scoreCriteria->calculatePoints());

        $maxStorageSpace->setEvaluationResult(false);
        $this->assertEquals(22, $scoreCriteria->calculatePoints());

        $maxStorageSpace->setSet(false);
        $this->assertEquals(22, $scoreCriteria->calculatePoints());

        $minShopWindowLength->setEvaluationResult(false);
        $this->assertEquals(6, $scoreCriteria->calculatePoints());

        $minShopWindowLength->setSet(false);
        $this->assertEquals(14, $scoreCriteria->calculatePoints());

        $maxShopWindowLength->setEvaluationResult(false);
        $this->assertEquals(6, $scoreCriteria->calculatePoints());

        $maxShopWindowLength->setSet(false);
        $this->assertEquals(6, $scoreCriteria->calculatePoints());

        $locationCategory->setEvaluationResult(false);
        $this->assertEquals(0, $scoreCriteria->calculatePoints());
    }

    public function testCalculateMaxPoints(): void
    {
        $scoreCriteria = new ScoreCriteria();

        $minimumSpace = new ScoreCriterion();
        $minimumSpace
            ->setSet(true)
            ->setName('minimumSpace')
            ->setPoints(8);

        $maximumSpace = new ScoreCriterion();
        $maximumSpace
            ->setSet(true)
            ->setName('maximumSpace')
            ->setPoints(8);

        $space = new MinMaxScoreCriterion();
        $space
            ->setMinScoreCriterion($minimumSpace)
            ->setMaxScoreCriterion($maximumSpace)
            ->setName('space');

        $roomCount = new ScoreCriterion();
        $roomCount
            ->setSet(true)
            ->setName('roomCount')
            ->setPoints(6);

        $numberOfParkingLots = new ScoreCriterion();
        $numberOfParkingLots
            ->setSet(true)
            ->setName('numberOfParkingLots')
            ->setPoints(6);

        $availableFrom = new ScoreCriterion();
        $availableFrom
            ->setSet(true)
            ->setName('availableFrom')
            ->setPoints(6);

        $minUsableSpace = new ScoreCriterion();
        $minUsableSpace
            ->setSet(true)
            ->setName('minUsableSpace')
            ->setPoints(3);

        $maxUsableSpace = new ScoreCriterion();
        $maxUsableSpace
            ->setSet(true)
            ->setName('maxStorageSpace')
            ->setPoints(3);

        $usableSpace = new MinMaxScoreCriterion();
        $usableSpace
            ->setName('storageSpace')
            ->setMinScoreCriterion($minUsableSpace)
            ->setMaxScoreCriterion($maxUsableSpace);

        $minRetailSpace = new ScoreCriterion();
        $minRetailSpace
            ->setName('minRetailSpace')
            ->setSet(true)
            ->setPoints(8);

        $maxRetailSpace = new ScoreCriterion();
        $maxRetailSpace
            ->setName('maxRetailSpace')
            ->setSet(true)
            ->setPoints(8);

        $retailSpace = new MinMaxScoreCriterion();
        $retailSpace
            ->setName('retailSpace')
            ->setMinScoreCriterion($minRetailSpace)
            ->setMaxScoreCriterion($maxRetailSpace);

        $minOutdoorSpace = new ScoreCriterion();
        $minOutdoorSpace
            ->setSet(true)
            ->setName('minOutdoorSpace')
            ->setPoints(3);

        $maxOutdoorSpace = new ScoreCriterion();
        $maxOutdoorSpace
            ->setSet(true)
            ->setName('maxOutdoorSpace')
            ->setPoints(3);

        $outdoorSpace = new MinMaxScoreCriterion();
        $outdoorSpace
            ->setName('outdoorSpace')
            ->setMinScoreCriterion($minOutdoorSpace)
            ->setMaxScoreCriterion($maxOutdoorSpace);

        $minStorageSpace = new ScoreCriterion();
        $minStorageSpace
            ->setSet(true)
            ->setName('minStorageSpace')
            ->setPoints(3);

        $maxStorageSpace = new ScoreCriterion();
        $maxStorageSpace
            ->setSet(true)
            ->setName('maxStorageSpace')
            ->setPoints(3);

        $storageSpace = new MinMaxScoreCriterion();
        $storageSpace
            ->setName('storageSpace')
            ->setMinScoreCriterion($minStorageSpace)
            ->setMaxScoreCriterion($maxStorageSpace);

        $minShopWindowLength = new ScoreCriterion();
        $minShopWindowLength
            ->setSet(true)
            ->setName('minShopWindowLength')
            ->setPoints(8);

        $maxShopWindowLength = new ScoreCriterion();
        $maxShopWindowLength
            ->setSet(true)
            ->setName('maxShopWindowLength')
            ->setPoints(8);

        $shopWindowLength = new MinMaxScoreCriterion();
        $shopWindowLength
            ->setName('shopWindowLength')
            ->setMinScoreCriterion($minShopWindowLength)
            ->setMaxScoreCriterion($maxShopWindowLength);

        $minShopWidth = new ScoreCriterion();
        $minShopWidth
            ->setSet(true)
            ->setName('minShopWidth')
            ->setPoints(8);

        $maxShopWidth = new ScoreCriterion();
        $maxShopWidth
            ->setSet(true)
            ->setName('maxShopWidth')
            ->setPoints(8);

        $shopWidth = new MinMaxScoreCriterion();
        $shopWidth
            ->setName('shopWidth')
            ->setMinScoreCriterion($minShopWidth)
            ->setMaxScoreCriterion($maxShopWidth);

        $locationCategory = new ScoreCriterion();
        $locationCategory
            ->setSet(true)
            ->setName('locationCategory')
            ->setPoints(6);

        $maxPrice = new ScoreCriterion();
        $maxPrice
            ->setSet(true)
            ->setName('maxPrice')
            ->setPoints(10);

        $minPrice = new ScoreCriterion();
        $minPrice
            ->setSet(true)
            ->setName('minPrice')
            ->setPoints(10);

        $branchDensity = new ScoreCriterion();
        $branchDensity
            ->setSet(true)
            ->setName('branchDensity')
            ->setPoints(10);

        $scoreCriteria
            ->setSpace($space)
            ->setRoomCount($roomCount)
            ->setNumberOfParkingLots($numberOfParkingLots)
            ->setAvailableFrom($availableFrom)
            ->setSubsidiarySpace($usableSpace)
            ->setRetailSpace($retailSpace)
            ->setOutdoorSpace($outdoorSpace)
            ->setStorageSpace($storageSpace)
            ->setShopWindowLength($shopWindowLength)
            ->setShopWidth($shopWidth)
            ->setLocationCategory($locationCategory)
            ->setMaximumPrice($maxPrice)
            ->setMinimumPrice($minPrice)
            ->setIndustryDensity($branchDensity);

        $this->assertEquals(136, $scoreCriteria->calculateMaxPoints());

        $branchDensity->setSet(false);
        $this->assertEquals(126, $scoreCriteria->calculateMaxPoints());

        $minimumSpace->setSet(false);
        $this->assertEquals(118, $scoreCriteria->calculateMaxPoints());

        $maximumSpace->setSet(false);
        $this->assertEquals(110, $scoreCriteria->calculateMaxPoints());

        $minPrice->setSet(false);
        $this->assertEquals(100, $scoreCriteria->calculateMaxPoints());

        $maxPrice->setSet(false);
        $this->assertEquals(90, $scoreCriteria->calculateMaxPoints());

        $minRetailSpace->setSet(false);
        $this->assertEquals(82, $scoreCriteria->calculateMaxPoints());

        $maxRetailSpace->setSet(false);
        $this->assertEquals(74, $scoreCriteria->calculateMaxPoints());

        $minShopWidth->setSet(false);
        $this->assertEquals(66, $scoreCriteria->calculateMaxPoints());

        $maxShopWidth->setSet(false);
        $this->assertEquals(58, $scoreCriteria->calculateMaxPoints());

        $roomCount->setSet(false);
        $this->assertEquals(52, $scoreCriteria->calculateMaxPoints());

        $numberOfParkingLots->setSet(false);
        $this->assertEquals(46, $scoreCriteria->calculateMaxPoints());

        $availableFrom->setSet(false);
        $this->assertEquals(40, $scoreCriteria->calculateMaxPoints());

        $minUsableSpace->setSet(false);
        $this->assertEquals(37, $scoreCriteria->calculateMaxPoints());

        $maxUsableSpace->setSet(false);
        $this->assertEquals(34, $scoreCriteria->calculateMaxPoints());

        $minOutdoorSpace->setSet(false);
        $this->assertEquals(31, $scoreCriteria->calculateMaxPoints());

        $maxOutdoorSpace->setSet(false);
        $this->assertEquals(28, $scoreCriteria->calculateMaxPoints());

        $minStorageSpace->setSet(false);
        $this->assertEquals(25, $scoreCriteria->calculateMaxPoints());

        $maxStorageSpace->setSet(false);
        $this->assertEquals(22, $scoreCriteria->calculateMaxPoints());

        $minShopWindowLength->setSet(false);
        $this->assertEquals(14, $scoreCriteria->calculateMaxPoints());

        $maxShopWindowLength->setSet(false);
        $this->assertEquals(6, $scoreCriteria->calculateMaxPoints());

        $locationCategory->setSet(false);
        $this->assertEquals(0, $scoreCriteria->calculateMaxPoints());
    }
}
