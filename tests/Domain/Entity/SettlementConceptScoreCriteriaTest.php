<?php

declare(strict_types=1);

namespace App\Tests\Domain\Entity;

use App\Domain\Entity\ScoreCriterion;
use App\Domain\Entity\SettlementConcept\SettlementConceptScoreCriteria;
use PHPUnit\Framework\TestCase;

class SettlementConceptScoreCriteriaTest extends TestCase
{
    public function testCalculateMaxPoints(): void
    {
        $secondarySpace = new ScoreCriterion();
        $secondarySpace->setPoints(1);
        $outdoorSalesArea = new ScoreCriterion();
        $outdoorSalesArea->setPoints(2);
        $storeWidth = new ScoreCriterion();
        $storeWidth->setPoints(3);
        $shopWindowLength = new ScoreCriterion();
        $shopWindowLength->setPoints(5);
        $locationFactors = new ScoreCriterion();
        $locationFactors->setPoints(7);

        $settlementConceptScoreCriteria = new SettlementConceptScoreCriteria();
        $settlementConceptScoreCriteria
            ->setSecondarySpace($secondarySpace)
            ->setOutdoorSalesArea($outdoorSalesArea)
            ->setStoreWidth($storeWidth)
            ->setShopWindowLength($shopWindowLength)
            ->setLocationFactors($locationFactors);

        $this->assertEquals(expected: 0, actual: $settlementConceptScoreCriteria->calculatePoints());

        $secondarySpace->setSet(true)->setEvaluationResult(true);
        $this->assertEquals(expected: 1, actual: $settlementConceptScoreCriteria->calculatePoints());

        $outdoorSalesArea
            ->setSet(true)
            ->setEvaluationResult(true);
        $this->assertEquals(expected: 3, actual: $settlementConceptScoreCriteria->calculatePoints());

        $storeWidth
            ->setSet(true)
            ->setEvaluationResult(true);
        $this->assertEquals(expected:6, actual: $settlementConceptScoreCriteria->calculatePoints());

        $shopWindowLength
            ->setSet(true)
            ->setEvaluationResult(true);
        $this->assertEquals(expected: 11, actual: $settlementConceptScoreCriteria->calculatePoints());

        $locationFactors
            ->setSet(true)
            ->setEvaluationResult(true);
        $this->assertEquals(expected: 18, actual: $settlementConceptScoreCriteria->calculatePoints());
    }

    public function testCalculatePoints(): void
    {
        $secondarySpace = new ScoreCriterion();
        $secondarySpace->setPoints(1);
        $outdoorSalesArea = new ScoreCriterion();
        $outdoorSalesArea->setPoints(2);
        $storeWidth = new ScoreCriterion();
        $storeWidth->setPoints(3);
        $shopWindowLength = new ScoreCriterion();
        $shopWindowLength->setPoints(5);
        $locationFactors = new ScoreCriterion();
        $locationFactors->setPoints(7);

        $settlementConceptScoreCriteria = new SettlementConceptScoreCriteria();
        $settlementConceptScoreCriteria
            ->setSecondarySpace($secondarySpace)
            ->setOutdoorSalesArea($outdoorSalesArea)
            ->setStoreWidth($storeWidth)
            ->setShopWindowLength($shopWindowLength)
            ->setLocationFactors($locationFactors);

        $this->assertEquals(expected: 0, actual: $settlementConceptScoreCriteria->calculateMaxPoints());

        $secondarySpace->setSet(true);
        $this->assertEquals(expected: 1, actual: $settlementConceptScoreCriteria->calculateMaxPoints());

        $outdoorSalesArea
            ->setSet(true);
        $this->assertEquals(expected: 3, actual: $settlementConceptScoreCriteria->calculateMaxPoints());

        $storeWidth
            ->setSet(true);
        $this->assertEquals(expected:6, actual: $settlementConceptScoreCriteria->calculateMaxPoints());

        $shopWindowLength
            ->setSet(true);
        $this->assertEquals(expected: 11, actual: $settlementConceptScoreCriteria->calculateMaxPoints());

        $locationFactors
            ->setSet(true);
        $this->assertEquals(expected: 18, actual: $settlementConceptScoreCriteria->calculateMaxPoints());
    }
}
