<?php

declare(strict_types=1);

namespace App\Tests\Domain\Entity;

use App\Domain\Entity\MinMaxScoreCriterion;
use PHPUnit\Framework\TestCase;

class MinMaxScoreCriterionTest extends TestCase
{
    public function testIsEvaluationResult()
    {
        $testMinMaxScoreCriterion = MinMaxScoreCriterion::createNewMinMaxScoreCriterion('DUT');
        $testMinScoreCriterion = $testMinMaxScoreCriterion->getMinScoreCriterion();
        $testMaxScoreCriterion = $testMinMaxScoreCriterion->getMaxScoreCriterion();

        $this->assertFalse($testMinMaxScoreCriterion->isEvaluationResult());

        $testMinScoreCriterion->setSet(true);
        $this->assertFalse($testMinMaxScoreCriterion->isEvaluationResult());

        $testMaxScoreCriterion->setSet(true);
        $this->assertFalse($testMinMaxScoreCriterion->isEvaluationResult());

        $testMinScoreCriterion->setEvaluationResult(true);
        $this->assertFalse($testMinMaxScoreCriterion->isEvaluationResult());

        $testMaxScoreCriterion->setSet(false);
        $this->assertTrue($testMinMaxScoreCriterion->isEvaluationResult());

        $testMinScoreCriterion->setSet(false)->setEvaluationResult(false);
        $testMaxScoreCriterion->setSet(true)->setEvaluationResult(true);
        $this->assertTrue($testMinMaxScoreCriterion->isEvaluationResult());

        $testMaxScoreCriterion->setSet(true)->setEvaluationResult(true);
        $testMinScoreCriterion->setSet(true)->setEvaluationResult(false);
        $this->assertFalse($testMinMaxScoreCriterion->isEvaluationResult());

        $testMinScoreCriterion->setEvaluationResult(true);
        $testMaxScoreCriterion->setEvaluationResult(false);
        $this->assertFalse($testMinMaxScoreCriterion->isEvaluationResult());
    }

    public function testIsSet()
    {
        $testMinMaxScoreCriterion = MinMaxScoreCriterion::createNewMinMaxScoreCriterion('DUT');
        $testMinScoreCriterion = $testMinMaxScoreCriterion->getMinScoreCriterion();
        $testMaxScoreCriterion = $testMinMaxScoreCriterion->getMaxScoreCriterion();

        $this->assertFalse($testMinMaxScoreCriterion->isSet());

        $testMinScoreCriterion->setSet(true);
        $this->assertTrue($testMinMaxScoreCriterion->isSet());

        $testMaxScoreCriterion->setSet(true);
        $this->assertTrue($testMinMaxScoreCriterion->isSet());
    }
}
