<?php

declare(strict_types=1);

namespace App\Tests\Domain\Entity;

use App\Domain\Entity\EliminationCriterion;
use App\Domain\Entity\SettlementConcept\SettlementConceptEliminationCriteria;
use PHPUnit\Framework\TestCase;

class SettlementConceptEliminationCriteriaTest extends TestCase
{
    public function testIsEliminated(): void
    {
        $settlementConceptEliminationCriteria = new SettlementConceptEliminationCriteria();

        $propertyOfferTypes = new EliminationCriterion();
        $propertyOfferTypes->setName('propertyOfferTypes');

        $minSpace = new EliminationCriterion();
        $minSpace->setName('minSpace');

        $maxSpace = new EliminationCriterion();
        $maxSpace->setName('maxSpace');

        $outdoorSpaceRequired = new EliminationCriterion();
        $outdoorSpaceRequired->setName('outdoorSpaceRequired');

        $groundLevelSalesAreaRequired = new EliminationCriterion();
        $groundLevelSalesAreaRequired->setName('groundLevelSalesAreaRequired');

        $barrierFreeAccessRequired = new EliminationCriterion();
        $barrierFreeAccessRequired->setName('barrierFreeAccessRequired');

        $locationCategories = new EliminationCriterion();
        $locationCategories->setName('locationCategories');

        $parkingLotsRequired = new EliminationCriterion();
        $parkingLotsRequired->setName('parkingLotsRequired');

        $ageStructures = new EliminationCriterion();
        $ageStructures->setName('ageStructures');

        $industryClassification = new EliminationCriterion();
        $industryClassification->setName('industryClassification');

        $settlementConceptEliminationCriteria
            ->setPropertyOfferTypes($propertyOfferTypes)
            ->setMinSpace($minSpace)
            ->setMaxSpace($maxSpace)
            ->setOutdoorSpaceRequired($outdoorSpaceRequired)
            ->setGroundLevelSalesAreaRequired($groundLevelSalesAreaRequired)
            ->setBarrierFreeAccessRequired($barrierFreeAccessRequired)
            ->setLocationCategories($locationCategories)
            ->setParkingLotsRequired($parkingLotsRequired)
            ->setAgeStructures($ageStructures)
            ->setIndustryClassification($industryClassification);

        $this->assertFalse($settlementConceptEliminationCriteria->isEliminated());

        $propertyOfferTypes->setSet(true);
        $minSpace->setSet(true);
        $maxSpace->setSet(true);
        $outdoorSpaceRequired->setSet(true);
        $groundLevelSalesAreaRequired->setSet(true);
        $barrierFreeAccessRequired->setSet(true);
        $locationCategories->setSet(true);
        $parkingLotsRequired->setSet(true);
        $ageStructures->setSet(true);
        $industryClassification->setSet(true);

        $this->assertTrue($settlementConceptEliminationCriteria->isEliminated());

        $propertyOfferTypes->setEvaluationResult(true);
        $minSpace->setEvaluationResult(true);
        $maxSpace->setEvaluationResult(true);
        $outdoorSpaceRequired->setEvaluationResult(true);
        $groundLevelSalesAreaRequired->setEvaluationResult(true);
        $barrierFreeAccessRequired->setEvaluationResult(true);
        $locationCategories->setEvaluationResult(true);
        $parkingLotsRequired->setEvaluationResult(true);
        $ageStructures->setEvaluationResult(true);
        $industryClassification->setEvaluationResult(true);

        $this->assertFalse($settlementConceptEliminationCriteria->isEliminated());
    }
}
