<?php

declare(strict_types=1);

namespace App\Tests\Domain\Entity;

use App\Domain\Entity\EliminationCriteria;
use App\Domain\Entity\EliminationCriterion;
use PHPUnit\Framework\TestCase;

class EliminationCriteriaTest extends TestCase
{
    public function testIsEliminated(): void
    {
        $commissionable = new EliminationCriterion();
        $commissionable
            ->setName('commissionable')
            ->setEvaluationResult(true)
            ->setSet(true);

        $inPlace = new EliminationCriterion();
        $inPlace
            ->setName('inPlace')
            ->setEvaluationResult(true)
            ->setSet(true);

        $inQuarter = new EliminationCriterion();
        $inQuarter
            ->setName('inQuarter')
            ->setEvaluationResult(true)
            ->setSet(true);

        $maximumPrice = new EliminationCriterion();
        $maximumPrice
            ->setName('maximumPrice')
            ->setEvaluationResult(true)
            ->setSet(true);

        $minimumPrice = new EliminationCriterion();
        $minimumPrice
            ->setName('minimumPrice')
            ->setEvaluationResult(true)
            ->setSet(true);

        $priceType = new EliminationCriterion();
        $priceType
            ->setName('priceType')
            ->setEvaluationResult(true)
            ->setSet(true);

        $roofing = new EliminationCriterion();
        $roofing
            ->setName('roofing')
            ->setEvaluationResult(true)
            ->setSet(true);

        $outdoorSpace = new EliminationCriterion();
        $outdoorSpace
            ->setName('outdoorSpace')
            ->setEvaluationResult(true)
            ->setSet(true);

        $showroom = new EliminationCriterion();
        $showroom
            ->setName('showRoom')
            ->setEvaluationResult(true)
            ->setSet(true);

        $shopWindow = new EliminationCriterion();
        $shopWindow
            ->setName('shopWindow')
            ->setEvaluationResult(true)
            ->setSet(true);

        $groundLevelSalesArea = new EliminationCriterion();
        $groundLevelSalesArea
            ->setName('groundLevelSalesArea')
            ->setEvaluationResult(true)
            ->setSet(true);

        $barrierFreeAccess = new EliminationCriterion();
        $barrierFreeAccess
            ->setName('barrierFreeAccess')
            ->setEvaluationResult(true)
            ->setSet(true);

        $minimumSpace = new EliminationCriterion();
        $minimumSpace
            ->setName('minimumSpace')
            ->setEvaluationResult(true)
            ->setSet(true);

        $maximumSpace = new EliminationCriterion();
        $maximumSpace
            ->setName('maximumSpace')
            ->setEvaluationResult(true)
            ->setSet(true);


        $eliminationCriteria = new EliminationCriteria();
        $eliminationCriteria
            ->setCommissionable($commissionable)
            ->setInPlace($inPlace)
            ->setInQuarter($inQuarter)
            ->setMaximumPrice($maximumPrice)
            ->setMinimumPrice($minimumPrice)
            ->setPropertyOfferTypes($priceType)
            ->setRoofing($roofing)
            ->setOutdoorSpace($outdoorSpace)
            ->setShowroom($showroom)
            ->setShopWindow($shopWindow)
            ->setGroundLevelSalesArea($groundLevelSalesArea)
            ->setBarrierFreeAccess($barrierFreeAccess)
            ->setMinimumSpace($minimumSpace)
            ->setMaximumSpace($maximumSpace);

        $this->assertFalse($eliminationCriteria->isEliminated());

        $inPlace->setEvaluationResult(false);
        $this->assertTrue($eliminationCriteria->isEliminated());

        $inPlace->setSet(false);
        $this->assertFalse($eliminationCriteria->isEliminated());

        $inQuarter->setEvaluationResult(false);
        $this->assertTrue($eliminationCriteria->isEliminated());

        $inQuarter->setSet(false);
        $this->assertFalse($eliminationCriteria->isEliminated());

        $maximumPrice->setEvaluationResult(false);
        $this->assertTrue($eliminationCriteria->isEliminated());

        $maximumPrice->setSet(false);
        $this->assertFalse($eliminationCriteria->isEliminated());

        $minimumPrice->setEvaluationResult(false);
        $this->assertTrue($eliminationCriteria->isEliminated());

        $minimumPrice->setSet(false);
        $this->assertFalse($eliminationCriteria->isEliminated());

        $priceType->setEvaluationResult(false);
        $this->assertTrue($eliminationCriteria->isEliminated());

        $priceType->setSet(false);
        $this->assertFalse($eliminationCriteria->isEliminated());

        $roofing->setEvaluationResult(false);
        $this->assertTrue($eliminationCriteria->isEliminated());

        $roofing->setSet(false);
        $this->assertFalse($eliminationCriteria->isEliminated());

        $outdoorSpace->setEvaluationResult(false);
        $this->assertTrue($eliminationCriteria->isEliminated());

        $outdoorSpace->setSet(false);
        $this->assertFalse($eliminationCriteria->isEliminated());

        $showroom->setEvaluationResult(false);
        $this->assertTrue($eliminationCriteria->isEliminated());

        $showroom->setSet(false);
        $this->assertFalse($eliminationCriteria->isEliminated());

        $shopWindow->setEvaluationResult(false);
        $this->assertTrue($eliminationCriteria->isEliminated());

        $shopWindow->setSet(false);
        $this->assertFalse($eliminationCriteria->isEliminated());

        $groundLevelSalesArea->setEvaluationResult(false);
        $this->assertTrue($eliminationCriteria->isEliminated());

        $groundLevelSalesArea->setSet(false);
        $this->assertFalse($eliminationCriteria->isEliminated());

        $barrierFreeAccess->setEvaluationResult(false);
        $this->assertTrue($eliminationCriteria->isEliminated());

        $barrierFreeAccess->setSet(false);
        $this->assertFalse($eliminationCriteria->isEliminated());

        $minimumSpace->setEvaluationResult(false);
        $this->assertTrue($eliminationCriteria->isEliminated());

        $minimumSpace->setSet(false);
        $this->assertFalse($eliminationCriteria->isEliminated());

        $maximumSpace->setEvaluationResult(false);
        $this->assertTrue($eliminationCriteria->isEliminated());

        $maximumSpace->setSet(false);
        $this->assertFalse($eliminationCriteria->isEliminated());
    }
}
