<?php

declare(strict_types=1);

namespace App\Tests\Domain\SettlementConcept\Matching;

use App\Domain\Classification\ClassificationService;
use App\Domain\Entity\AgeStructure;
use App\Domain\Entity\Classification\IndustryClassification;
use App\Domain\Entity\Classification\NaceClassification;
use App\Domain\Entity\Classification\NaceClassificationLevel;
use App\Domain\Entity\LocationCategory;
use App\Domain\Entity\LocationFactor;
use App\Domain\Entity\ParkingLotRequirementType;
use App\Domain\Entity\Property\BarrierFreeAccess;
use App\Domain\Entity\Property\Building;
use App\Domain\Entity\Property\BuildingUnit;
use App\Domain\Entity\Property\PropertyMarketingInformation;
use App\Domain\Entity\Property\PropertySpacesData;
use App\Domain\Entity\PropertyOfferType;
use App\Domain\Entity\SettlementConcept\SettlementConcept;
use App\Domain\Entity\SettlementConcept\SettlementConceptPropertyRequirement;
use App\Domain\Entity\ValueRequirement;
use App\Domain\SettlementConcept\Matching\SettlementConceptPointMatchingStrategy;
use PHPUnit\Framework\TestCase;

class SettlementConceptPointMatchingStrategyTest extends TestCase
{
    public function testDoMatching(): void
    {
        $buildingUnit = $this->createBuildingUnit();
        $settlementConcept = $this->createSettlementConcept();
        $classificationServiceMock = $this->getMockBuilder(ClassificationService::class)
            ->disableOriginalConstructor()
            ->getMock();

        $childIndustryClassification = new IndustryClassification();
        $childIndustryClassification
            ->setName('Child')
            ->setLevelOne(1)
            ->setLevelTwo(2)
            ->setLevelThree(3);
        $parentIndustryClassification = new IndustryClassification();
        $parentIndustryClassification
            ->setName('parent')
            ->setLevelOne(1);

        $settlementConcept->setIndustryClassification($parentIndustryClassification);

        $valueMap = [
            [$parentIndustryClassification, $parentIndustryClassification],
            [$childIndustryClassification, $parentIndustryClassification]
        ];

        $classificationServiceMock
            ->method('getTopMostParentFromIndustryClassification')
            ->willReturnMap($valueMap);

        $strategy = new SettlementConceptPointMatchingStrategy($classificationServiceMock);

        $results = $strategy->doMatching([$settlementConcept], $buildingUnit);
        $this->assertTrue($results[0]->getSettlementConceptEliminationCriteria()->getPropertyOfferTypes()->isSet());
        $this->assertFalse($results[0]->getSettlementConceptEliminationCriteria()->getPropertyOfferTypes()->isEvaluationResult());

        $propertyMarketingInformation = $buildingUnit->getPropertyMarketingInformation();
        $results = $strategy->doMatching([$settlementConcept], $buildingUnit);
        $this->assertTrue($results[0]->getSettlementConceptEliminationCriteria()->getPropertyOfferTypes()->isSet());
        $this->assertFalse($results[0]->getSettlementConceptEliminationCriteria()->getPropertyOfferTypes()->isEvaluationResult());

        $buildingUnit->setPropertyMarketingInformation($propertyMarketingInformation);
        $buildingUnit->getPropertyMarketingInformation()->setPropertyOfferType(PropertyOfferType::RENT);
        $results = $strategy->doMatching([$settlementConcept], $buildingUnit);
        $this->assertTrue($results[0]->getSettlementConceptEliminationCriteria()->getPropertyOfferTypes()->isSet());
        $this->assertFalse($results[0]->getSettlementConceptEliminationCriteria()->getPropertyOfferTypes()->isEvaluationResult());

        $settlementConcept->setPropertyOfferTypes([PropertyOfferType::RENT, PropertyOfferType::PURCHASE]);
        $results = $strategy->doMatching([$settlementConcept], $buildingUnit);
        $this->assertTrue($results[0]->getSettlementConceptEliminationCriteria()->getPropertyOfferTypes()->isSet());
        $this->assertTrue($results[0]->getSettlementConceptEliminationCriteria()->getPropertyOfferTypes()->isEvaluationResult());

        $settlementConcept->getSettlementConceptPropertyRequirement()->getSpace()->setMinimumValue(100);
        $results = $strategy->doMatching([$settlementConcept], $buildingUnit);
        $this->assertTrue($results[0]->getSettlementConceptEliminationCriteria()->getMinSpace()->isSet());
        $this->assertFalse($results[0]->getSettlementConceptEliminationCriteria()->getMinSpace()->isEvaluationResult());

        $buildingUnit->setAreaSize(70);
        $results = $strategy->doMatching([$settlementConcept], $buildingUnit);
        $this->assertTrue($results[0]->getSettlementConceptEliminationCriteria()->getMinSpace()->isSet());
        $this->assertFalse($results[0]->getSettlementConceptEliminationCriteria()->getMinSpace()->isEvaluationResult());

        $buildingUnit->setAreaSize(110);
        $results = $strategy->doMatching([$settlementConcept], $buildingUnit);
        $this->assertTrue($results[0]->getSettlementConceptEliminationCriteria()->getMinSpace()->isSet());
        $this->assertTrue($results[0]->getSettlementConceptEliminationCriteria()->getMinSpace()->isEvaluationResult());

        $settlementConcept->getSettlementConceptPropertyRequirement()->getSpace()->setMaximumValue(300);
        $buildingUnit->setAreaSize(361);
        $results = $strategy->doMatching([$settlementConcept], $buildingUnit);
        $this->assertTrue($results[0]->getSettlementConceptEliminationCriteria()->getMaxSpace()->isSet());
        $this->assertFalse($results[0]->getSettlementConceptEliminationCriteria()->getMaxSpace()->isEvaluationResult());

        $buildingUnit->setAreaSize(150);
        $results = $strategy->doMatching([$settlementConcept], $buildingUnit);
        $this->assertTrue($results[0]->getSettlementConceptEliminationCriteria()->getMaxSpace()->isSet());
        $this->assertTrue($results[0]->getSettlementConceptEliminationCriteria()->getMaxSpace()->isEvaluationResult());

        $results = $strategy->doMatching([$settlementConcept], $buildingUnit);
        $this->assertFalse($results[0]->getSettlementConceptEliminationCriteria()->getOutdoorSpaceRequired()->isSet());
        $this->assertFalse($results[0]->getSettlementConceptEliminationCriteria()->getOutdoorSpaceRequired()->isEvaluationResult());

        $settlementConcept->getSettlementConceptPropertyRequirement()->setOutdoorSalesAreaRequired(true);
        $results = $strategy->doMatching([$settlementConcept], $buildingUnit);
        $this->assertTrue($results[0]->getSettlementConceptEliminationCriteria()->getOutdoorSpaceRequired()->isSet());
        $this->assertFalse($results[0]->getSettlementConceptEliminationCriteria()->getOutdoorSpaceRequired()->isEvaluationResult());

        $results = $strategy->doMatching([$settlementConcept], $buildingUnit);
        $this->assertTrue($results[0]->getSettlementConceptEliminationCriteria()->getOutdoorSpaceRequired()->isSet());
        $this->assertFalse($results[0]->getSettlementConceptEliminationCriteria()->getOutdoorSpaceRequired()->isEvaluationResult());

        $buildingUnit->setPropertyMarketingInformation($propertyMarketingInformation);
        $buildingUnit->getPropertySpacesData()->setOutdoorSalesArea(12);
        $results = $strategy->doMatching([$settlementConcept], $buildingUnit);
        $this->assertTrue($results[0]->getSettlementConceptEliminationCriteria()->getOutdoorSpaceRequired()->isSet());
        $this->assertTrue($results[0]->getSettlementConceptEliminationCriteria()->getOutdoorSpaceRequired()->isEvaluationResult());

        $this->assertFalse($results[0]->getSettlementConceptEliminationCriteria()->getGroundLevelSalesAreaRequired()->isSet());
        $this->assertFalse($results[0]->getSettlementConceptEliminationCriteria()->getGroundLevelSalesAreaRequired()->isEvaluationResult());

        $settlementConcept->getSettlementConceptPropertyRequirement()->setGroundLevelSalesAreaRequired(true);
        $results = $strategy->doMatching([$settlementConcept], $buildingUnit);
        $this->assertTrue($results[0]->getSettlementConceptEliminationCriteria()->getGroundLevelSalesAreaRequired()->isSet());
        $this->assertFalse($results[0]->getSettlementConceptEliminationCriteria()->getGroundLevelSalesAreaRequired()->isEvaluationResult());

        $buildingUnit->setGroundLevelSalesArea(true);
        $results = $strategy->doMatching([$settlementConcept], $buildingUnit);
        $this->assertTrue($results[0]->getSettlementConceptEliminationCriteria()->getGroundLevelSalesAreaRequired()->isSet());
        $this->assertTrue($results[0]->getSettlementConceptEliminationCriteria()->getGroundLevelSalesAreaRequired()->isEvaluationResult());

        $this->assertFalse($results[0]->getSettlementConceptEliminationCriteria()->getBarrierFreeAccessRequired()->isSet());
        $this->assertFalse($results[0]->getSettlementConceptEliminationCriteria()->getBarrierFreeAccessRequired()->isEvaluationResult());

        $settlementConcept->getSettlementConceptPropertyRequirement()->setBarrierFreeAccessRequired(true);
        $results = $strategy->doMatching([$settlementConcept], $buildingUnit);
        $this->assertTrue($results[0]->getSettlementConceptEliminationCriteria()->getBarrierFreeAccessRequired()->isSet());
        $this->assertFalse($results[0]->getSettlementConceptEliminationCriteria()->getBarrierFreeAccessRequired()->isEvaluationResult());

        $buildingUnit->setBarrierFreeAccess(BarrierFreeAccess::IS_NOT_POSSIBLE);
        $results = $strategy->doMatching([$settlementConcept], $buildingUnit);
        $this->assertTrue($results[0]->getSettlementConceptEliminationCriteria()->getBarrierFreeAccessRequired()->isSet());
        $this->assertFalse($results[0]->getSettlementConceptEliminationCriteria()->getBarrierFreeAccessRequired()->isEvaluationResult());

        $buildingUnit->setBarrierFreeAccess(BarrierFreeAccess::CAN_BE_GUARANTEED);
        $results = $strategy->doMatching([$settlementConcept], $buildingUnit);
        $this->assertTrue($results[0]->getSettlementConceptEliminationCriteria()->getBarrierFreeAccessRequired()->isSet());
        $this->assertTrue($results[0]->getSettlementConceptEliminationCriteria()->getBarrierFreeAccessRequired()->isEvaluationResult());

        $buildingUnit->setBarrierFreeAccess(BarrierFreeAccess::IS_AVAILABLE);
        $results = $strategy->doMatching([$settlementConcept], $buildingUnit);
        $this->assertTrue($results[0]->getSettlementConceptEliminationCriteria()->getBarrierFreeAccessRequired()->isSet());
        $this->assertTrue($results[0]->getSettlementConceptEliminationCriteria()->getBarrierFreeAccessRequired()->isEvaluationResult());

        $settlementConcept->getSettlementConceptPropertyRequirement()->setLocationCategories([]);

        $results = $strategy->doMatching([$settlementConcept], $buildingUnit);
        $this->assertFalse($results[0]->getSettlementConceptEliminationCriteria()->getLocationCategories()->isSet());
        $this->assertFalse($results[0]->getSettlementConceptEliminationCriteria()->getLocationCategories()->isEvaluationResult());

        $settlementConcept->getSettlementConceptPropertyRequirement()->setLocationCategories([LocationCategory::ONE_A_LOCATION]);
        $results = $strategy->doMatching([$settlementConcept], $buildingUnit);
        $this->assertTrue($results[0]->getSettlementConceptEliminationCriteria()->getLocationCategories()->isSet());
        $this->assertFalse($results[0]->getSettlementConceptEliminationCriteria()->getLocationCategories()->isEvaluationResult());

        $buildingUnit->getBuilding()->setLocationCategory(LocationCategory::ONE_A_LOCATION);
        $results = $strategy->doMatching([$settlementConcept], $buildingUnit);
        $this->assertTrue($results[0]->getSettlementConceptEliminationCriteria()->getLocationCategories()->isSet());
        $this->assertTrue($results[0]->getSettlementConceptEliminationCriteria()->getLocationCategories()->isEvaluationResult());

        $this->assertFalse($results[0]->getSettlementConceptEliminationCriteria()->getParkingLotsRequired()->isSet());
        $this->assertFalse($results[0]->getSettlementConceptEliminationCriteria()->getParkingLotsRequired()->isEvaluationResult());

        $settlementConcept->getSettlementConceptPropertyRequirement()->setParkingLotsRequired(ParkingLotRequirementType::MANDATORY);
        $results = $strategy->doMatching([$settlementConcept], $buildingUnit);
        $this->assertTrue($results[0]->getSettlementConceptEliminationCriteria()->getParkingLotsRequired()->isSet());
        $this->assertFalse($results[0]->getSettlementConceptEliminationCriteria()->getParkingLotsRequired()->isEvaluationResult());

        $buildingUnit->setNumberOfParkingLots(20);
        $results = $strategy->doMatching([$settlementConcept], $buildingUnit);
        $this->assertTrue($results[0]->getSettlementConceptEliminationCriteria()->getParkingLotsRequired()->isSet());
        $this->assertTrue($results[0]->getSettlementConceptEliminationCriteria()->getParkingLotsRequired()->isEvaluationResult());

        $this->assertTrue($results[0]->getSettlementConceptEliminationCriteria()->getIndustryClassification()->isSet());
        $this->assertFalse($results[0]->getSettlementConceptEliminationCriteria()->getIndustryClassification()->isEvaluationResult());

        $results = $strategy->doMatching([$settlementConcept], $buildingUnit);
        $this->assertTrue($results[0]->getSettlementConceptEliminationCriteria()->getIndustryClassification()->isSet());
        $this->assertFalse($results[0]->getSettlementConceptEliminationCriteria()->getIndustryClassification()->isEvaluationResult());

        $buildingUnit->setPropertyMarketingInformation($propertyMarketingInformation);
        $buildingUnit->getPropertyMarketingInformation()->getPossibleUsageIndustryClassifications()->add($parentIndustryClassification);
        $results = $strategy->doMatching([$settlementConcept], $buildingUnit);
        $this->assertTrue($results[0]->getSettlementConceptEliminationCriteria()->getIndustryClassification()->isSet());
        $this->assertTrue($results[0]->getSettlementConceptEliminationCriteria()->getIndustryClassification()->isEvaluationResult());

        $settlementConcept->setIndustryClassification($childIndustryClassification);
        $results = $strategy->doMatching([$settlementConcept], $buildingUnit);
        $this->assertTrue($results[0]->getSettlementConceptEliminationCriteria()->getIndustryClassification()->isSet());
        $this->assertTrue($results[0]->getSettlementConceptEliminationCriteria()->getIndustryClassification()->isEvaluationResult());

        $this->assertFalse($results[0]->getSettlementConceptEliminationCriteria()->isEliminated());

        $this->assertFalse($results[0]->getSettlementConceptScoreCriteria()->getOutdoorSalesArea()->isSet());
        $this->assertFalse($results[0]->getSettlementConceptScoreCriteria()->getOutdoorSalesArea()->isEvaluationResult());

        $settlementConcept->getSettlementConceptPropertyRequirement()->getOutdoorSalesArea()
            ->setMinimumValue(35)
            ->setMaximumValue(70);
        $results = $strategy->doMatching([$settlementConcept], $buildingUnit);
        $this->assertTrue($results[0]->getSettlementConceptScoreCriteria()->getOutdoorSalesArea()->isSet());
        $this->assertFalse($results[0]->getSettlementConceptScoreCriteria()->getOutdoorSalesArea()->isEvaluationResult());

        $buildingUnit->getPropertySpacesData()->setOutdoorSalesArea(30.0);
        $results = $strategy->doMatching([$settlementConcept], $buildingUnit);
        $this->assertTrue($results[0]->getSettlementConceptScoreCriteria()->getOutdoorSalesArea()->isSet());
        $this->assertFalse($results[0]->getSettlementConceptScoreCriteria()->getOutdoorSalesArea()->isEvaluationResult());

        $buildingUnit->getPropertySpacesData()->setOutdoorSalesArea(71.00);
        $results = $strategy->doMatching([$settlementConcept], $buildingUnit);
        $this->assertTrue($results[0]->getSettlementConceptScoreCriteria()->getOutdoorSalesArea()->isSet());
        $this->assertFalse($results[0]->getSettlementConceptScoreCriteria()->getOutdoorSalesArea()->isEvaluationResult());

        $buildingUnit->getPropertySpacesData()->setOutdoorSalesArea(40.00);
        $results = $strategy->doMatching([$settlementConcept], $buildingUnit);
        $this->assertTrue($results[0]->getSettlementConceptScoreCriteria()->getOutdoorSalesArea()->isSet());
        $this->assertTrue($results[0]->getSettlementConceptScoreCriteria()->getOutdoorSalesArea()->isEvaluationResult());

        $this->assertFalse($results[0]->getSettlementConceptScoreCriteria()->getStoreWidth()->isSet());
        $this->assertFalse($results[0]->getSettlementConceptScoreCriteria()->getStoreWidth()->isEvaluationResult());

        $settlementConcept->getSettlementConceptPropertyRequirement()->getStoreWidth()
            ->setMinimumValue(10)
            ->setMaximumValue(20);
        $results = $strategy->doMatching([$settlementConcept], $buildingUnit);

        $this->assertTrue($results[0]->getSettlementConceptScoreCriteria()->getStoreWidth()->isSet());
        $this->assertFalse($results[0]->getSettlementConceptScoreCriteria()->getStoreWidth()->isEvaluationResult());

        $buildingUnit->setShopWidth(9);
        $results = $strategy->doMatching([$settlementConcept], $buildingUnit);

        $this->assertTrue($results[0]->getSettlementConceptScoreCriteria()->getStoreWidth()->isSet());
        $this->assertFalse($results[0]->getSettlementConceptScoreCriteria()->getStoreWidth()->isEvaluationResult());

        $buildingUnit->setShopWidth(21);
        $results = $strategy->doMatching([$settlementConcept], $buildingUnit);

        $this->assertTrue($results[0]->getSettlementConceptScoreCriteria()->getStoreWidth()->isSet());
        $this->assertFalse($results[0]->getSettlementConceptScoreCriteria()->getStoreWidth()->isEvaluationResult());

        $buildingUnit->setShopWidth(15);
        $results = $strategy->doMatching([$settlementConcept], $buildingUnit);

        $this->assertTrue($results[0]->getSettlementConceptScoreCriteria()->getStoreWidth()->isSet());
        $this->assertTrue($results[0]->getSettlementConceptScoreCriteria()->getStoreWidth()->isEvaluationResult());

        $this->assertFalse($results[0]->getSettlementConceptScoreCriteria()->getShopWindowLength()->isSet());
        $this->assertFalse($results[0]->getSettlementConceptScoreCriteria()->getShopWindowLength()->isEvaluationResult());

        $settlementConcept->getSettlementConceptPropertyRequirement()->getShopWindowLength()
            ->setMinimumValue(10)
            ->setMaximumValue(20);
        $results = $strategy->doMatching([$settlementConcept], $buildingUnit);
        $this->assertTrue($results[0]->getSettlementConceptScoreCriteria()->getShopWindowLength()->isSet());
        $this->assertFalse($results[0]->getSettlementConceptScoreCriteria()->getShopWindowLength()->isEvaluationResult());

        $buildingUnit->setShopWindowFrontWidth(9);
        $results = $strategy->doMatching([$settlementConcept], $buildingUnit);
        $this->assertTrue($results[0]->getSettlementConceptScoreCriteria()->getShopWindowLength()->isSet());
        $this->assertFalse($results[0]->getSettlementConceptScoreCriteria()->getShopWindowLength()->isEvaluationResult());

        $buildingUnit->setShopWindowFrontWidth(21);
        $results = $strategy->doMatching([$settlementConcept], $buildingUnit);
        $this->assertTrue($results[0]->getSettlementConceptScoreCriteria()->getShopWindowLength()->isSet());
        $this->assertFalse($results[0]->getSettlementConceptScoreCriteria()->getShopWindowLength()->isEvaluationResult());

        $buildingUnit->setShopWindowFrontWidth(15);
        $results = $strategy->doMatching([$settlementConcept], $buildingUnit);
        $this->assertTrue($results[0]->getSettlementConceptScoreCriteria()->getShopWindowLength()->isSet());
        $this->assertTrue($results[0]->getSettlementConceptScoreCriteria()->getShopWindowLength()->isEvaluationResult());

        $this->assertFalse($results[0]->getSettlementConceptScoreCriteria()->getSecondarySpace()->isSet());
        $this->assertFalse($results[0]->getSettlementConceptScoreCriteria()->getSecondarySpace()->isEvaluationResult());

        $settlementConcept->getSettlementConceptPropertyRequirement()->getSecondarySpace()
            ->setMinimumValue(50)
            ->setMaximumValue(100);
        $results = $strategy->doMatching([$settlementConcept], $buildingUnit);
        $this->assertTrue($results[0]->getSettlementConceptScoreCriteria()->getSecondarySpace()->isSet());
        $this->assertFalse($results[0]->getSettlementConceptScoreCriteria()->getSecondarySpace()->isEvaluationResult());

        $buildingUnit->getPropertySpacesData()->setRetailSpace(90);
        $results = $strategy->doMatching([$settlementConcept], $buildingUnit);
        $this->assertTrue($results[0]->getSettlementConceptScoreCriteria()->getRetailSpace()->isSet());
        $this->assertFalse($results[0]->getSettlementConceptScoreCriteria()->getRetailSpace()->isEvaluationResult());

        $buildingUnit->getPropertySpacesData()->setRetailSpace(150);
        $results = $strategy->doMatching([$settlementConcept], $buildingUnit);
        $this->assertTrue($results[0]->getSettlementConceptScoreCriteria()->getRetailSpace()->isSet());
        $this->assertTrue($results[0]->getSettlementConceptScoreCriteria()->getRetailSpace()->isEvaluationResult());

        $this->assertFalse($results[0]->getSettlementConceptScoreCriteria()->getLocationFactors()->isSet());
        $this->assertFalse($results[0]->getSettlementConceptScoreCriteria()->getLocationFactors()->isEvaluationResult());

        $settlementConcept->getSettlementConceptPropertyRequirement()->setLocationFactors([LocationFactor::AIR_PORT]);
        $results = $strategy->doMatching([$settlementConcept], $buildingUnit);
        $this->assertTrue($results[0]->getSettlementConceptScoreCriteria()->getLocationFactors()->isSet());
        $this->assertFalse($results[0]->getSettlementConceptScoreCriteria()->getLocationFactors()->isEvaluationResult());

        $buildingUnit->getBuilding()->setLocationFactors([
            LocationFactor::AIR_PORT,
            LocationFactor::MAIN_STREET,
            LocationFactor::PEDESTRIAN_ZONE
        ]);
        $results = $strategy->doMatching([$settlementConcept], $buildingUnit);
        $this->assertTrue($results[0]->getSettlementConceptScoreCriteria()->getLocationFactors()->isSet());
        $this->assertTrue($results[0]->getSettlementConceptScoreCriteria()->getLocationFactors()->isEvaluationResult());
    }

    private function createSettlementConcept(): SettlementConcept
    {
        $settlementConcept = new SettlementConcept();

        $propertyOfferTypes = [PropertyOfferType::PURCHASE];
        $ageStructures = [AgeStructure::YOUNG_PEOPLE, AgeStructure::OLDER_PEOPLE];
        $naceClassification = new NaceClassification();

        $naceClassification
            ->setName('Test')
            ->setLevel(NaceClassificationLevel::LEVEL_FIVE)
            ->setCode('01.01.0');

        $settlementConcept
            ->setPropertyOfferTypes($propertyOfferTypes)
            ->setAgeStructures($ageStructures)
            ->setNaceClassification($naceClassification)
            ->setSettlementConceptPropertyRequirement($this->createSettlementConceptPropertyRequirement());

        return $settlementConcept;
    }

    private function createSettlementConceptPropertyRequirement(): SettlementConceptPropertyRequirement
    {
        $settlementConceptPropertyRequirement = new SettlementConceptPropertyRequirement();
        $space = new ValueRequirement();
        $secondarySpace = new ValueRequirement();
        $outdoorSalesArea = new ValueRequirement();
        $storeWidth = new ValueRequirement();
        $shopWindowLength = new ValueRequirement();

        $settlementConceptPropertyRequirement
            ->setSpace($space)
            ->setSecondarySpace($secondarySpace)
            ->setOutdoorSalesArea($outdoorSalesArea)
            ->setStoreWidth($storeWidth)
            ->setShopWindowLength($shopWindowLength);

        return $settlementConceptPropertyRequirement;
    }

    private function createBuildingUnit(): BuildingUnit
    {
        $buildingUnit = new BuildingUnit();
        $propertyMarketingInformation = new PropertyMarketingInformation();
        $propertySpacesData = new PropertySpacesData();
        $building = new Building();

        $buildingUnit
            ->setPropertyMarketingInformation($propertyMarketingInformation)
            ->setPropertySpacesData($propertySpacesData)
            ->setBuilding($building);

        return $buildingUnit;
    }
}
