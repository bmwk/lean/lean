<?php

declare(strict_types=1);

namespace App\Tests\Domain\SettlementConcept;

use App\Domain\Entity\SettlementConcept\SettlementConceptAccountUser;
use App\Domain\SettlementConcept\SettlementConceptAccountUserService;
use App\Repository\SettlementConcept\SettlementConceptAccountUserRepository;
use App\Repository\SettlementConcept\SettlementConceptAuthenticationTokenGenerationRequestRepository;
use App\Security\JsonWebTokenService;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Uid\UuidV6;

class SettlementConceptAccountUserServiceTest extends TestCase
{
    public function testGenerateJsonWebToken(): void
    {
        $settlementConceptAccountUser = new SettlementConceptAccountUser();
        $settlementConceptAccountUser->setIdentifier(UuidV6::fromString('1ed292cb-58c3-6c38-9f4d-a3c6769fb094'));

        $mailerMock = $this->getMockBuilder(MailerInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $settlementConceptAccountUserRepositoryMock = $this->getMockBuilder(SettlementConceptAccountUserRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $settlementConceptAuthenticationTokenGenerationRequestRepositoryMock = $this
            ->getMockBuilder(SettlementConceptAuthenticationTokenGenerationRequestRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $entityManagerMock = $this->getMockBuilder(EntityManagerInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $jsonWebTokenService = new JsonWebTokenService('cafsasdfasdf');

        $settlementConceptAccountUserService = new SettlementConceptAccountUserService(
            settlementConceptAuthenticationTokenGenerationRequestRepository: $settlementConceptAuthenticationTokenGenerationRequestRepositoryMock,
            settlementConceptAccountUserRepository: $settlementConceptAccountUserRepositoryMock,
            jsonWebTokenService: $jsonWebTokenService,
            entityManager: $entityManagerMock,
            mailer: $mailerMock
        );

        $expectedToken='eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJpZGVudGlmaWVyIjoiMWVkMjkyY2ItNThjMy02YzM4LTlmNGQtYTNjNjc2OWZiMDk0In0.1a8a609000947e3a15a008bd1e98eae7d2e81ca3197dce0824a7dcb5fd808870153b4d62f7eacf4a6eb2dfdba40436e9fdaa38d507c002b45458875404a53827';
        $token = $settlementConceptAccountUserService->generateJsonWebToken($settlementConceptAccountUser);

        $this->assertEquals(expected: $expectedToken, actual: $token);
    }
}
