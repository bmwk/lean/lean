<?php

declare(strict_types=1);

namespace App\Tests\Domain\LookingForPropertyRequest\Matching;

use App\Domain\Classification\ClassificationService;
use App\Domain\Entity\Account;
use App\Domain\Entity\Classification\IndustryClassification;
use App\Domain\Entity\GeolocationPoint;
use App\Domain\Entity\LocationCategory;
use App\Domain\Entity\LookingForPropertyRequest\LookingForPropertyRequest;
use App\Domain\Entity\LookingForPropertyRequest\PropertyRequirement;
use App\Domain\Entity\MinMaxScoreCriterion;
use App\Domain\Entity\Place;
use App\Domain\Entity\PriceRequirement;
use App\Domain\Entity\Property\Address;
use App\Domain\Entity\Property\BarrierFreeAccess;
use App\Domain\Entity\Property\Building;
use App\Domain\Entity\Property\BuildingUnit;
use App\Domain\Entity\Property\PropertyMarketingInformation;
use App\Domain\Entity\Property\PropertyPricingInformation;
use App\Domain\Entity\Property\PropertySpacesData;
use App\Domain\Entity\Property\PropertyUser;
use App\Domain\Entity\PropertyMandatoryRequirement;
use App\Domain\Entity\PropertyOfferType;
use App\Domain\Entity\PropertyValueRangeRequirement;
use App\Domain\Entity\ValueRequirement;
use App\Domain\LookingForPropertyRequest\Matching\PointMatchingStrategy;
use App\Domain\Property\BuildingUnitService;
use CrEOF\Spatial\PHP\Types\Geometry\Point;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Uid\UuidV6;

class PointMatchingStrategyTest extends TestCase
{
    public function testDoMatching(): void
    {
        $buildingUnitFetchReturn = new ArrayCollection();
        $buildingUnitFetchReturn->add(new BuildingUnit());
        $buildingUnitFetchReturn->add(new BuildingUnit());
        $buildingUnitFetchReturn->add(new BuildingUnit());
        $buildingUnitFetchReturn->add(new BuildingUnit());
        $buildingUnitFetchReturn->add(new BuildingUnit());
        $buildingUnitFetchReturn->add(new BuildingUnit());

        $buildingUnitService = $this->getMockBuilder(BuildingUnitService::class)
            ->disableOriginalConstructor()
            ->getMock();

        $buildingUnitService
            ->method('fetchBuildingUnitsWithCurrentUsageInBoundingBox')
            ->will(
                $this->returnCallback(
                    function () use (&$buildingUnitFetchReturn) {
                        return $buildingUnitFetchReturn->toArray();
                    }
                )
            );

        $lookingForPropertyRequest = $this->createLookingForPropertyRequest();

        $industryClassification = new IndustryClassification();
        $industryClassification
            ->setLevelOne(1)
            ->setName('Test Level One');
        $classificationService = $this->getMockBuilder(ClassificationService::class)
            ->disableOriginalConstructor()
            ->getMock();
        $classificationService
            ->method('getTopMostParentFromIndustryClassification')
            ->will(
                $this->returnCallback(
                    function () use (&$industryClassification) {
                        return $industryClassification;
                    }
                )
            );

        $pointMatchingStrategy =  new PointMatchingStrategy(
            buildingUnitService: $buildingUnitService,
            classificationService: $classificationService
        );
        $propertyMarketingInformation = new PropertyMarketingInformation();
        $propertySpacesData = new PropertySpacesData();

        $propertyRequirement = $lookingForPropertyRequest->getPropertyRequirement();
        $priceRequirement = $lookingForPropertyRequest->getPriceRequirement();
        $propertyValueRangeRequirement = $propertyRequirement->getPropertyValueRangeRequirement();
        $propertyMandatoryRequirements = $propertyRequirement->getPropertyMandatoryRequirement();
        $point = new Point(1, 2);
        $geolocationPoint = new GeolocationPoint();
        $geolocationPoint
            ->setPoint($point);

        $place = new Place();
        $place
            ->setId(0)
            ->setUuid(new UuidV6())
            ->setPlaceName('Hans Hausen');

        $address = new Address();
        $address
            ->setId(0)
            ->setPlace($place);

        $building = new Building();

        $account = new Account();

        $buildingUnit = new BuildingUnit();
        $buildingUnit
            ->setAddress($address)
            ->setPropertyMarketingInformation($propertyMarketingInformation)
            ->setPropertySpacesData($propertySpacesData)
            ->setBuilding($building)
            ->setGeolocationPoint($geolocationPoint)
            ->setAccount($account);

        $propertyPricingInformation = new PropertyPricingInformation();
        $propertyMarketingInformation->setPropertyPricingInformation($propertyPricingInformation);

        $priceRequirement->setPropertyOfferTypes([PropertyOfferType::RENT]);

        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);

        $this->assertFalse($results[0]->getEliminationCriteria()->getInQuarter()->isSet());
        $this->assertFalse($results[0]->getEliminationCriteria()->getInQuarter()->isEvaluationResult());
        $this->assertFalse($results[0]->getEliminationCriteria()->getInPlace()->isSet());
        $this->assertFalse($results[0]->getEliminationCriteria()->getInPlace()->isEvaluationResult());
        $this->assertFalse($results[0]->getEliminationCriteria()->getCommissionable()->isSet());
        $this->assertFalse($results[0]->getEliminationCriteria()->getCommissionable()->isEvaluationResult());
        $this->assertTrue($results[0]->getEliminationCriteria()->getPropertyOfferTypes()->isSet());
        $this->assertFalse($results[0]->getEliminationCriteria()->getPropertyOfferTypes()->isEvaluationResult());
        $this->assertFalse($results[0]->getEliminationCriteria()->getMaximumPrice()->isSet());
        $this->assertFalse($results[0]->getEliminationCriteria()->getMaximumPrice()->isEvaluationResult());
        $this->assertFalse($results[0]->getEliminationCriteria()->getMinimumPrice()->isSet());
        $this->assertFalse($results[0]->getEliminationCriteria()->getMinimumPrice()->isEvaluationResult());
        $this->assertFalse($results[0]->getEliminationCriteria()->getRoofing()->isSet());
        $this->assertFalse($results[0]->getEliminationCriteria()->getRoofing()->isEvaluationResult());
        $this->assertFalse($results[0]->getEliminationCriteria()->getOutdoorSpace()->isSet());
        $this->assertFalse($results[0]->getEliminationCriteria()->getOutdoorSpace()->isEvaluationResult());
        $this->assertFalse($results[0]->getEliminationCriteria()->getShopWindow()->isSet());
        $this->assertFalse($results[0]->getEliminationCriteria()->getShopWindow()->isEvaluationResult());
        $this->assertFalse($results[0]->getEliminationCriteria()->getShowroom()->isSet());
        $this->assertFalse($results[0]->getEliminationCriteria()->getShowroom()->isEvaluationResult());
        $this->assertFalse($results[0]->getEliminationCriteria()->getGroundLevelSalesArea()->isSet());
        $this->assertFalse($results[0]->getEliminationCriteria()->getGroundLevelSalesArea()->isEvaluationResult());
        $this->assertFalse($results[0]->getEliminationCriteria()->getBarrierFreeAccess()->isSet());
        $this->assertFalse($results[0]->getEliminationCriteria()->getBarrierFreeAccess()->isEvaluationResult());
        $this->assertFalse($results[0]->getEliminationCriteria()->getMinimumSpace()->isSet());
        $this->assertFalse($results[0]->getEliminationCriteria()->getMinimumSpace()->isEvaluationResult());
        $this->assertFalse($results[0]->getEliminationCriteria()->getMaximumSpace()->isSet());
        $this->assertFalse($results[0]->getEliminationCriteria()->getMaximumSpace()->isEvaluationResult());
        $this->assertTrue($results[0]->getEliminationCriteria()->getIndustryClassification()->isSet());
        $this->assertFalse($results[0]->getEliminationCriteria()->getIndustryClassification()->isEvaluationResult());

        $quarter = new Place();

        $quarter
            ->setId(1)
            ->setUuid(new UuidV6())
            ->setPlaceName('Test Quarter');

        $address->setQuarterPlace($quarter);

        $wrongQuarter = new Place();

        $wrongQuarter
            ->setId(1)
            ->setUuid(new UuidV6())
            ->setPlaceName('Bad Quarter');

        $propertyRequirement->getInQuarters()->add($wrongQuarter);

        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getEliminationCriteria()->getInQuarter()->isSet());
        $this->assertFalse($results[0]->getEliminationCriteria()->getInQuarter()->isEvaluationResult());

        $propertyRequirement->getInQuarters()->add($quarter);

        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getEliminationCriteria()->getInQuarter()->isSet());
        $this->assertTrue($results[0]->getEliminationCriteria()->getInQuarter()->isEvaluationResult());

        $propertyRequirement->setInQuarters(new ArrayCollection());

        $wrongPlace = new Place();

        $wrongPlace
            ->setId(0)
            ->setUuid(new UuidV6())
            ->setPlaceName('Hans Hausen');

        $propertyRequirement->getInPlaces()->add($wrongPlace);

        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getEliminationCriteria()->getInPlace()->isSet());
        $this->assertFalse($results[0]->getEliminationCriteria()->getInPlace()->isEvaluationResult());

        $propertyRequirement->getInPlaces()->add($place);

        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getEliminationCriteria()->getInPlace()->isSet());
        $this->assertTrue($results[0]->getEliminationCriteria()->getInPlace()->isEvaluationResult());

        $priceRequirement->setNonCommissionable(null);
        $propertyPricingInformation->setCommissionable(true);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getEliminationCriteria()->getCommissionable()->isSet());
        $this->assertFalse($results[0]->getEliminationCriteria()->getCommissionable()->isEvaluationResult());

        $priceRequirement->setNonCommissionable(false);
        $propertyPricingInformation->setCommissionable(true);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getEliminationCriteria()->getCommissionable()->isSet());
        $this->assertFalse($results[0]->getEliminationCriteria()->getCommissionable()->isEvaluationResult());

        $priceRequirement->setNonCommissionable(true);
        $propertyPricingInformation->setCommissionable(true);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getEliminationCriteria()->getCommissionable()->isSet());
        $this->assertTrue($results[0]->getEliminationCriteria()->getCommissionable()->isEvaluationResult());

        $propertyPricingInformation->setCommissionable(false);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getEliminationCriteria()->getCommissionable()->isSet());
        $this->assertTrue($results[0]->getEliminationCriteria()->getCommissionable()->isEvaluationResult());

        $propertyMarketingInformation->setPropertyOfferType(PropertyOfferType::PURCHASE);
        $propertyPricingInformation->setPurchasePriceNet(13);
        $priceRequirement->setPropertyOfferTypes([PropertyOfferType::PURCHASE]);
        $priceRequirement->getPurchasePriceNet()->setMaximumValue(10);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getEliminationCriteria()->getMaximumPrice()->isSet());
        $this->assertFalse($results[0]->getEliminationCriteria()->getMaximumPrice()->isEvaluationResult());

        $propertyPricingInformation->setPurchasePriceNet(9);

        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getEliminationCriteria()->getMaximumPrice()->isSet());
        $this->assertTrue($results[0]->getEliminationCriteria()->getMaximumPrice()->isEvaluationResult());

        $priceRequirement->getPurchasePriceNet()->setMaximumValue(null);
        $priceRequirement->getPurchasePriceGross()->setMaximumValue(10);
        $propertyPricingInformation->setPurchasePriceGross(13);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getEliminationCriteria()->getMaximumPrice()->isSet());
        $this->assertFalse($results[0]->getEliminationCriteria()->getMaximumPrice()->isEvaluationResult());

        $propertyPricingInformation->setPurchasePriceGross(10);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getEliminationCriteria()->getMaximumPrice()->isSet());
        $this->assertTrue($results[0]->getEliminationCriteria()->getMaximumPrice()->isEvaluationResult());

        $priceRequirement->getPurchasePriceGross()->setMaximumValue(null);
        $priceRequirement->getPurchasePricePerSquareMeter()->setMaximumValue(10);
        $propertyPricingInformation->setPurchasePricePerSquareMeter(13);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getEliminationCriteria()->getMaximumPrice()->isSet());
        $this->assertFalse($results[0]->getEliminationCriteria()->getMaximumPrice()->isEvaluationResult());

        $propertyPricingInformation->setPurchasePricePerSquareMeter(10);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getEliminationCriteria()->getMaximumPrice()->isSet());
        $this->assertTrue($results[0]->getEliminationCriteria()->getMaximumPrice()->isEvaluationResult());

        $propertyMarketingInformation->setPropertyOfferType(PropertyOfferType::RENT);
        $priceRequirement->setPropertyOfferTypes([PropertyOfferType::RENT]);
        $priceRequirement->getPurchasePricePerSquareMeter()->setMaximumValue(null);
        $priceRequirement->getNetColdRent()->setMaximumValue(10);
        $propertyPricingInformation->setNetColdRent(13);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getEliminationCriteria()->getMaximumPrice()->isSet());
        $this->assertFalse($results[0]->getEliminationCriteria()->getMaximumPrice()->isEvaluationResult());

        $propertyPricingInformation->setNetColdRent(10);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getEliminationCriteria()->getMaximumPrice()->isSet());
        $this->assertTrue($results[0]->getEliminationCriteria()->getMaximumPrice()->isEvaluationResult());

        $priceRequirement->getNetColdRent()->setMaximumValue(null);
        $priceRequirement->getColdRent()->setMaximumValue(10);
        $propertyPricingInformation->setColdRent(13);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getEliminationCriteria()->getMaximumPrice()->isSet());
        $this->assertFalse($results[0]->getEliminationCriteria()->getMaximumPrice()->isEvaluationResult());

        $propertyPricingInformation->setColdRent(10);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getEliminationCriteria()->getMaximumPrice()->isSet());
        $this->assertTrue($results[0]->getEliminationCriteria()->getMaximumPrice()->isEvaluationResult());

        $priceRequirement->getColdRent()->setMaximumValue(null);
        $priceRequirement->getRentalPricePerSquareMeter()->setMaximumValue(10);
        $propertyPricingInformation->setRentalPricePerSquareMeter(13);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getEliminationCriteria()->getMaximumPrice()->isSet());
        $this->assertFalse($results[0]->getEliminationCriteria()->getMaximumPrice()->isEvaluationResult());

        $propertyPricingInformation->setRentalPricePerSquareMeter(10);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getEliminationCriteria()->getMaximumPrice()->isSet());
        $this->assertTrue($results[0]->getEliminationCriteria()->getMaximumPrice()->isEvaluationResult());

        $propertyMarketingInformation->setPropertyOfferType(PropertyOfferType::PURCHASE);
        $priceRequirement->setPropertyOfferTypes([PropertyOfferType::PURCHASE]);
        $propertyPricingInformation->setPurchasePriceNet(7);
        $priceRequirement->getPurchasePriceNet()->setMinimumValue(10);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getEliminationCriteria()->getMinimumPrice()->isSet());
        $this->assertFalse($results[0]->getEliminationCriteria()->getMinimumPrice()->isEvaluationResult());

        $propertyPricingInformation->setPurchasePriceNet(11);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getEliminationCriteria()->getMinimumPrice()->isSet());
        $this->assertTrue($results[0]->getEliminationCriteria()->getMinimumPrice()->isEvaluationResult());

        $priceRequirement->getPurchasePriceNet()->setMinimumValue(null);
        $priceRequirement->getPurchasePriceGross()->setMinimumValue(10);
        $propertyPricingInformation->setPurchasePriceGross(7);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getEliminationCriteria()->getMinimumPrice()->isSet());
        $this->assertFalse($results[0]->getEliminationCriteria()->getMinimumPrice()->isEvaluationResult());

        $propertyPricingInformation->setPurchasePriceGross(11);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getEliminationCriteria()->getMinimumPrice()->isSet());
        $this->assertTrue($results[0]->getEliminationCriteria()->getMinimumPrice()->isEvaluationResult());

        $priceRequirement->getPurchasePriceGross()->setMinimumValue(null);
        $propertyPricingInformation->setPurchasePricePerSquareMeter(7);
        $priceRequirement->getPurchasePricePerSquareMeter()->setMinimumValue(10);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getEliminationCriteria()->getMinimumPrice()->isSet());
        $this->assertFalse($results[0]->getEliminationCriteria()->getMinimumPrice()->isEvaluationResult());

        $propertyPricingInformation->setPurchasePricePerSquareMeter(11);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getEliminationCriteria()->getMinimumPrice()->isSet());
        $this->assertTrue($results[0]->getEliminationCriteria()->getMinimumPrice()->isEvaluationResult());

        $priceRequirement->getPurchasePricePerSquareMeter()->setMinimumValue(null);
        $propertyMarketingInformation->setPropertyOfferType(PropertyOfferType::RENT);

        $priceRequirement->setPropertyOfferTypes([PropertyOfferType::RENT]);
        $priceRequirement->getNetColdRent()->setMinimumValue(10);
        $propertyPricingInformation->setNetColdRent(7);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getEliminationCriteria()->getMinimumPrice()->isSet());
        $this->assertFalse($results[0]->getEliminationCriteria()->getMinimumPrice()->isEvaluationResult());

        $propertyPricingInformation->setNetColdRent(11);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getEliminationCriteria()->getMinimumPrice()->isSet());
        $this->assertTrue($results[0]->getEliminationCriteria()->getMinimumPrice()->isEvaluationResult());

        $priceRequirement->getNetColdRent()->setMinimumValue(null);
        $propertyPricingInformation->setColdRent(7);
        $priceRequirement->getColdRent()->setMinimumValue(10);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getEliminationCriteria()->getMinimumPrice()->isSet());
        $this->assertFalse($results[0]->getEliminationCriteria()->getMinimumPrice()->isEvaluationResult());

        $propertyPricingInformation->setColdRent(11);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getEliminationCriteria()->getMinimumPrice()->isSet());
        $this->assertTrue($results[0]->getEliminationCriteria()->getMinimumPrice()->isEvaluationResult());

        $priceRequirement->getColdRent()->setMinimumValue(null);
        $propertyPricingInformation->setRentalPricePerSquareMeter(7);
        $priceRequirement->getRentalPricePerSquareMeter()->setMinimumValue(10);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getEliminationCriteria()->getMinimumPrice()->isSet());
        $this->assertFalse($results[0]->getEliminationCriteria()->getMinimumPrice()->isEvaluationResult());

        $priceRequirement->getRentalPricePerSquareMeter()->setMaximumValue(null);
        $propertyPricingInformation->setRentalPricePerSquareMeter(11);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getEliminationCriteria()->getMinimumPrice()->isSet());
        $this->assertTrue($results[0]->getEliminationCriteria()->getMinimumPrice()->isEvaluationResult());

        $propertyMandatoryRequirements->setRoofingRequired(true);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getEliminationCriteria()->getRoofing()->isSet());
        $this->assertFalse($results[0]->getEliminationCriteria()->getRoofing()->isEvaluationResult());

        $buildingUnit->setRoofingPresent(true);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getEliminationCriteria()->getRoofing()->isSet());
        $this->assertTrue($results[0]->getEliminationCriteria()->getRoofing()->isEvaluationResult());

        $propertyMandatoryRequirements->setOutdoorSpaceRequired(true);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getEliminationCriteria()->getOutdoorSpace()->isSet());
        $this->assertFalse($results[0]->getEliminationCriteria()->getOutdoorSpace()->isEvaluationResult());

        $buildingUnit->getPropertySpacesData()->setTotalOutdoorArea(100);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getEliminationCriteria()->getOutdoorSpace()->isSet());
        $this->assertTrue($results[0]->getEliminationCriteria()->getOutdoorSpace()->isEvaluationResult());

        $propertyMandatoryRequirements->setShopWindowRequired(true);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getEliminationCriteria()->getShopWindow()->isSet());
        $this->assertFalse($results[0]->getEliminationCriteria()->getShopWindow()->isEvaluationResult());

        $buildingUnit->setShopWindowFrontWidth(10.0);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getEliminationCriteria()->getShopWindow()->isSet());
        $this->assertTrue($results[0]->getEliminationCriteria()->getShopWindow()->isEvaluationResult());

        $propertyMandatoryRequirements->setShowroomRequired(true);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getEliminationCriteria()->getShowroom()->isSet());
        $this->assertFalse($results[0]->getEliminationCriteria()->getShowroom()->isEvaluationResult());

        $buildingUnit->setShowroomAvailable(true);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getEliminationCriteria()->getShowroom()->isSet());
        $this->assertTrue($results[0]->getEliminationCriteria()->getShowroom()->isEvaluationResult());

        $propertyMandatoryRequirements->setGroundLevelSalesAreaRequired(true);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getEliminationCriteria()->getGroundLevelSalesArea()->isSet());
        $this->assertFalse($results[0]->getEliminationCriteria()->getGroundLevelSalesArea()->isEvaluationResult());

        $buildingUnit->setGroundLevelSalesArea(true);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getEliminationCriteria()->getGroundLevelSalesArea()->isSet());
        $this->assertTrue($results[0]->getEliminationCriteria()->getGroundLevelSalesArea()->isEvaluationResult());

        $propertyMandatoryRequirements->setBarrierFreeAccessRequired(true);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getEliminationCriteria()->getBarrierFreeAccess()->isSet());
        $this->assertFalse($results[0]->getEliminationCriteria()->getBarrierFreeAccess()->isEvaluationResult());

        $buildingUnit->setBarrierFreeAccess(BarrierFreeAccess::IS_CURRENTLY_NOT_AVAILABLE);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getEliminationCriteria()->getBarrierFreeAccess()->isSet());
        $this->assertFalse($results[0]->getEliminationCriteria()->getBarrierFreeAccess()->isEvaluationResult());

        $buildingUnit->setBarrierFreeAccess(BarrierFreeAccess::IS_NOT_POSSIBLE);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getEliminationCriteria()->getBarrierFreeAccess()->isSet());
        $this->assertFalse($results[0]->getEliminationCriteria()->getBarrierFreeAccess()->isEvaluationResult());

        $buildingUnit->setBarrierFreeAccess(BarrierFreeAccess::IS_AVAILABLE);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getEliminationCriteria()->getBarrierFreeAccess()->isSet());
        $this->assertTrue($results[0]->getEliminationCriteria()->getBarrierFreeAccess()->isEvaluationResult());

        $buildingUnit->setBarrierFreeAccess(BarrierFreeAccess::CAN_BE_GUARANTEED);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getEliminationCriteria()->getBarrierFreeAccess()->isSet());
        $this->assertTrue($results[0]->getEliminationCriteria()->getBarrierFreeAccess()->isEvaluationResult());

        $propertyValueRangeRequirement->getTotalSpace()->setMinimumValue(100.00);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getEliminationCriteria()->getMinimumSpace()->isSet());
        $this->assertFalse($results[0]->getEliminationCriteria()->getMinimumSpace()->isEvaluationResult());

        $buildingUnit->setAreaSize(81);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getEliminationCriteria()->getMinimumSpace()->isSet());
        $this->assertTrue($results[0]->getEliminationCriteria()->getMinimumSpace()->isEvaluationResult());
        $propertyValueRangeRequirement->getTotalSpace()->setMinimumValue(null);
        $buildingUnit->setAreaSize(null);

        $propertyValueRangeRequirement->getTotalSpace()->setMaximumValue(100.00);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getEliminationCriteria()->getMaximumSpace()->isSet());
        $this->assertFalse($results[0]->getEliminationCriteria()->getMaximumSpace()->isEvaluationResult());

        $buildingUnit->setAreaSize(120.00);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getEliminationCriteria()->getMaximumSpace()->isSet());
        $this->assertTrue($results[0]->getEliminationCriteria()->getMaximumSpace()->isEvaluationResult());

        $propertyMarketingInformation->setPropertyOfferType(PropertyOfferType::PURCHASE);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertFalse($results[0]->getEliminationCriteria()->getPropertyOfferTypes()->isEvaluationResult());

        $propertyMarketingInformation->setPropertyOfferType(PropertyOfferType::RENT);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getEliminationCriteria()->getPropertyOfferTypes()->isEvaluationResult());

        $propertyRequirement
            ->setindustryClassification($industryClassification);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getEliminationCriteria()->getIndustryClassification()->isSet());
        $this->assertFalse($results[0]->getEliminationCriteria()->getIndustryClassification()->isEvaluationResult());
        $this->assertTrue($results[0]->getEliminationCriteria()->isEliminated());

        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getEliminationCriteria()->getIndustryClassification()->isSet());
        $this->assertFalse($results[0]->getEliminationCriteria()->getIndustryClassification()->isEvaluationResult());
        $buildingUnit->setPropertyMarketingInformation($propertyMarketingInformation);

        $propertyUser = new PropertyUser();
        $propertyUser->setIndustryClassification($industryClassification);
        $buildingUnit->getPropertyUsers()->add($propertyUser);

        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getEliminationCriteria()->getIndustryClassification()->isSet());
        $this->assertTrue($results[0]->getEliminationCriteria()->getIndustryClassification()->isEvaluationResult());

        /** @var MinMaxScoreCriterion $spaceCriterion */
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $spaceCriterion = $results[0]->getScoreCriteria()->getSpace();
        $this->assertFalse($spaceCriterion->getMinScoreCriterion()->isSet());
        $this->assertFalse($spaceCriterion->getMinScoreCriterion()->isEvaluationResult());

        $propertyValueRangeRequirement->getTotalSpace()
            ->setMinimumValue(20)
            ->setMaximumValue(null);

        $buildingUnit->setAreaSize(20);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $spaceCriterion = $results[0]->getScoreCriteria()->getSpace();
        $this->assertTrue($spaceCriterion->getMinScoreCriterion()->isSet());
        $this->assertTrue($spaceCriterion->getMinScoreCriterion()->isEvaluationResult());

        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        /** @var MinMaxScoreCriterion $spaceCriterion */
        $spaceCriterion = $results[0]->getScoreCriteria()->getSpace();
        $propertyValueRangeRequirement->getTotalSpace()->setMaximumValue(null);
        $this->assertFalse($spaceCriterion->getMaxScoreCriterion()->isSet());
        $this->assertFalse($spaceCriterion->getMaxScoreCriterion()->isEvaluationResult());

        $propertyValueRangeRequirement->getTotalSpace()->setMaximumValue(25);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $spaceCriterion = $results[0]->getScoreCriteria()->getSpace();
        $this->assertTrue($spaceCriterion->getMaxScoreCriterion()->isSet());
        $this->assertTrue($spaceCriterion->getMaxScoreCriterion()->isEvaluationResult());

        $propertyValueRangeRequirement->getTotalSpace()->setMaximumValue(35);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $spaceCriterion = $results[0]->getScoreCriteria()->getSpace();
        $this->assertTrue($spaceCriterion->getMaxScoreCriterion()->isSet());
        $this->assertTrue($spaceCriterion->getMaxScoreCriterion()->isEvaluationResult());

        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertFalse($results[0]->getScoreCriteria()->getRoomCount()->isSet());
        $this->assertFalse($results[0]->getScoreCriteria()->getRoomCount()->isEvaluationResult());

        $propertyRequirement->setRoomCount(5);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getRoomCount()->isSet());
        $this->assertFalse($results[0]->getScoreCriteria()->getRoomCount()->isEvaluationResult());

        $propertySpacesData->setRoomCount(4);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getRoomCount()->isSet());
        $this->assertTrue($results[0]->getScoreCriteria()->getRoomCount()->isEvaluationResult());

        $propertySpacesData->setRoomCount(5);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getRoomCount()->isSet());
        $this->assertTrue($results[0]->getScoreCriteria()->getRoomCount()->isEvaluationResult());

        $this->assertFalse($results[0]->getScoreCriteria()->getNumberOfParkingLots()->isSet());
        $this->assertFalse($results[0]->getScoreCriteria()->getNumberOfParkingLots()->isEvaluationResult());

        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertFalse($results[0]->getScoreCriteria()->getAvailableFrom()->isSet());
        $this->assertFalse($results[0]->getScoreCriteria()->getAvailableFrom()->isEvaluationResult());

        $propertyRequirement->setNumberOfParkingLots(5);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getNumberOfParkingLots()->isSet());
        $this->assertFalse($results[0]->getScoreCriteria()->getNumberOfParkingLots()->isEvaluationResult());

        $buildingUnit->setNumberOfParkingLots(4);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getNumberOfParkingLots()->isSet());
        $this->assertFalse($results[0]->getScoreCriteria()->getNumberOfParkingLots()->isEvaluationResult());

        $buildingUnit->setNumberOfParkingLots(5);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getNumberOfParkingLots()->isSet());
        $this->assertTrue($results[0]->getScoreCriteria()->getNumberOfParkingLots()->isEvaluationResult());

        $propertyRequirement->setEarliestAvailableFrom(new \DateTime('2022-05-05'));
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getAvailableFrom()->isSet());
        $this->assertFalse($results[0]->getScoreCriteria()->getAvailableFrom()->isEvaluationResult());

        $propertyMarketingInformation->setAvailableFrom(new \DateTime('2022-05-04'));
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getAvailableFrom()->isSet());
        $this->assertFalse($results[0]->getScoreCriteria()->getAvailableFrom()->isEvaluationResult());

        $propertyMarketingInformation->setAvailableFrom(new \DateTime('2022-05-05'));
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getAvailableFrom()->isSet());
        $this->assertTrue($results[0]->getScoreCriteria()->getAvailableFrom()->isEvaluationResult());

        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertFalse($results[0]->getScoreCriteria()->getSubsidiarySpace()->isSet());
        $this->assertFalse($results[0]->getScoreCriteria()->getSubsidiarySpace()->isEvaluationResult());

        $propertyValueRangeRequirement->getSubsidiarySpace()->setMinimumValue(12.00);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getSubsidiarySpace()->isSet());
        $this->assertFalse($results[0]->getScoreCriteria()->getSubsidiarySpace()->isEvaluationResult());

        $propertySpacesData->setSubsidiarySpace(10);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getSubsidiarySpace()->isSet());
        $this->assertFalse($results[0]->getScoreCriteria()->getSubsidiarySpace()->isEvaluationResult());

        $propertySpacesData->setSubsidiarySpace(15);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getSubsidiarySpace()->isSet());
        $this->assertTrue($results[0]->getScoreCriteria()->getSubsidiarySpace()->isEvaluationResult());

        $propertyValueRangeRequirement->getSubsidiarySpace()->setMaximumValue(13.00);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getSubsidiarySpace()->isSet());
        $this->assertFalse($results[0]->getScoreCriteria()->getSubsidiarySpace()->isEvaluationResult());

        $propertyValueRangeRequirement->getSubsidiarySpace()->setMaximumValue(20.00);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getSubsidiarySpace()->isSet());
        $this->assertTrue($results[0]->getScoreCriteria()->getSubsidiarySpace()->isEvaluationResult());

        $this->assertFalse($results[0]->getScoreCriteria()->getRetailSpace()->isSet());
        $this->assertFalse($results[0]->getScoreCriteria()->getRetailSpace()->isEvaluationResult());

        $propertyValueRangeRequirement->getRetailSpace()->setMinimumValue(12.00);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getRetailSpace()->isSet());
        $this->assertFalse($results[0]->getScoreCriteria()->getRetailSpace()->isEvaluationResult());

        $propertySpacesData->setRetailSpace(10);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getRetailSpace()->isSet());
        $this->assertFalse($results[0]->getScoreCriteria()->getRetailSpace()->isEvaluationResult());

        $propertySpacesData->setRetailSpace(15);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getRetailSpace()->isSet());
        $this->assertTrue($results[0]->getScoreCriteria()->getRetailSpace()->isEvaluationResult());

        $propertyValueRangeRequirement->getRetailSpace()->setMaximumValue(13.00);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getRetailSpace()->isSet());
        $this->assertFalse($results[0]->getScoreCriteria()->getRetailSpace()->isEvaluationResult());

        $propertyValueRangeRequirement->getRetailSpace()->setMaximumValue(20.00);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getRetailSpace()->isSet());
        $this->assertTrue($results[0]->getScoreCriteria()->getRetailSpace()->isEvaluationResult());

        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertFalse($results[0]->getScoreCriteria()->getOutdoorSpace()->isSet());
        $this->assertFalse($results[0]->getScoreCriteria()->getOutdoorSpace()->isEvaluationResult());

        $propertyValueRangeRequirement->getOutdoorSpace()
            ->setMinimumValue(12)
            ->setMaximumValue(30);

        $buildingUnit->getPropertySpacesData()->setTotalOutdoorArea(8);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getOutdoorSpace()->isSet());
        $this->assertFalse($results[0]->getScoreCriteria()->getOutdoorSpace()->isEvaluationResult());

        $buildingUnit->getPropertySpacesData()->setTotalOutdoorArea(12);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getOutdoorSpace()->isEvaluationResult());

        $buildingUnit->getPropertySpacesData()->setTotalOutdoorArea(31);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertFalse($results[0]->getScoreCriteria()->getOutdoorSpace()->isEvaluationResult());

        $buildingUnit->getPropertySpacesData()->setTotalOutdoorArea(30);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getOutdoorSpace()->isEvaluationResult());

        $buildingUnit->getPropertySpacesData()->setTotalOutdoorArea(null);
        $lookingForPropertyRequest
            ->getPropertyRequirement()
            ->getPropertyMandatoryRequirement()
            ->setOutdoorSpaceRequired(false);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertFalse($results[0]->getScoreCriteria()->getOutdoorSpace()->isEvaluationResult());

        $buildingUnit->getPropertySpacesData()->setTotalOutdoorArea(30);
        $lookingForPropertyRequest
            ->getPropertyRequirement()
            ->getPropertyMandatoryRequirement()
            ->setOutdoorSpaceRequired(true);

        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertFalse($results[0]->getScoreCriteria()->getStorageSpace()->isSet());
        $this->assertFalse($results[0]->getScoreCriteria()->getStorageSpace()->isEvaluationResult());

        $propertyValueRangeRequirement->getStorageSpace()->setMaximumValue(120.00);
        $propertySpacesData->setStorageSpace(null);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getStorageSpace()->isSet());
        $this->assertFalse($results[0]->getScoreCriteria()->getStorageSpace()->isEvaluationResult());

        $propertyValueRangeRequirement->getStorageSpace()->setMinimumValue(10);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getStorageSpace()->isSet());
        $this->assertFalse($results[0]->getScoreCriteria()->getStorageSpace()->isEvaluationResult());

        $propertySpacesData->setStorageSpace(1);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getStorageSpace()->isSet());
        $this->assertFalse($results[0]->getScoreCriteria()->getStorageSpace()->isEvaluationResult());

        $propertySpacesData->setStorageSpace(121.00);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getStorageSpace()->isSet());
        $this->assertFalse($results[0]->getScoreCriteria()->getStorageSpace()->isEvaluationResult());

        $propertySpacesData->setStorageSpace(10.00);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getStorageSpace()->isSet());
        $this->assertTrue($results[0]->getScoreCriteria()->getStorageSpace()->isEvaluationResult());

        $propertySpacesData->setStorageSpace(120.00);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getStorageSpace()->isSet());
        $this->assertTrue($results[0]->getScoreCriteria()->getStorageSpace()->isEvaluationResult());

        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertFalse($results[0]->getScoreCriteria()->getShopWindowLength()->isSet());
        $this->assertFalse($results[0]->getScoreCriteria()->getShopWindowLength()->isEvaluationResult());

        $propertyValueRangeRequirement->getShopWindowLength()->setMinimumValue(2)->setMaximumValue(10);
        $buildingUnit->setShopWindowFrontWidth(null);
        $propertyMandatoryRequirements->setShopWindowRequired(false);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getShopWindowLength()->isSet());
        $this->assertFalse($results[0]->getScoreCriteria()->getShopWindowLength()->isEvaluationResult());

        $buildingUnit->setShopWindowFrontWidth(1);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getShopWindowLength()->isSet());
        $this->assertFalse($results[0]->getScoreCriteria()->getShopWindowLength()->isEvaluationResult());

        $buildingUnit->setShopWindowFrontWidth(11);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getShopWindowLength()->isSet());
        $this->assertFalse($results[0]->getScoreCriteria()->getShopWindowLength()->isEvaluationResult());

        $buildingUnit->setShopWindowFrontWidth(2);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getShopWindowLength()->isSet());
        $this->assertTrue($results[0]->getScoreCriteria()->getShopWindowLength()->isEvaluationResult());

        $buildingUnit->setShopWindowFrontWidth(10);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getShopWindowLength()->isSet());
        $this->assertTrue($results[0]->getScoreCriteria()->getShopWindowLength()->isEvaluationResult());

        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertFalse($results[0]->getScoreCriteria()->getShopWidth()->isSet());
        $this->assertFalse($results[0]->getScoreCriteria()->getShopWidth()->isEvaluationResult());

        $propertyValueRangeRequirement->getShopWidth()->setMinimumValue(2)->setMaximumValue(10);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getShopWidth()->isSet());
        $this->assertFalse($results[0]->getScoreCriteria()->getShopWidth()->isEvaluationResult());

        $buildingUnit->setShopWidth(1);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getShopWidth()->isSet());
        $this->assertFalse($results[0]->getScoreCriteria()->getShopWidth()->isEvaluationResult());

        $buildingUnit->setShopWidth(11);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getShopWidth()->isSet());
        $this->assertFalse($results[0]->getScoreCriteria()->getShopWidth()->isEvaluationResult());

        $buildingUnit->setShopWidth(2);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getShopWidth()->isSet());
        $this->assertTrue($results[0]->getScoreCriteria()->getShopWidth()->isEvaluationResult());

        $buildingUnit->setShopWidth(10);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getShopWidth()->isSet());
        $this->assertTrue($results[0]->getScoreCriteria()->getShopWidth()->isEvaluationResult());

        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertFalse($results[0]->getScoreCriteria()->getLocationCategory()->isSet());
        $this->assertFalse($results[0]->getScoreCriteria()->getLocationCategory()->isEvaluationResult());

        $propertyRequirement->setLocationCategories([LocationCategory::ONE_A_LOCATION]);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getLocationCategory()->isSet());
        $this->assertFalse($results[0]->getScoreCriteria()->getLocationCategory()->isEvaluationResult());

        $building->setLocationCategory(LocationCategory::ONE_B_LOCATION);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getLocationCategory()->isSet());
        $this->assertFalse($results[0]->getScoreCriteria()->getLocationCategory()->isEvaluationResult());

        $building->setLocationCategory(LocationCategory::ONE_A_LOCATION);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getLocationCategory()->isSet());
        $this->assertTrue($results[0]->getScoreCriteria()->getLocationCategory()->isEvaluationResult());

        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertFalse($results[0]->getScoreCriteria()->getMaximumPrice()->isSet());
        $this->assertFalse($results[0]->getScoreCriteria()->getMaximumPrice()->isEvaluationResult());

        $propertyMarketingInformation->setPropertyOfferType(PropertyOfferType::PURCHASE);
        $priceRequirement->setPropertyOfferTypes([PropertyOfferType::PURCHASE]);
        $priceRequirement->getPurchasePriceNet()->setMaximumValue(10);
        $propertyPricingInformation->setPurchasePriceNet(12);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getMaximumPrice()->isSet());
        $this->assertFalse($results[0]->getScoreCriteria()->getMaximumPrice()->isEvaluationResult());

        $propertyPricingInformation->setPurchasePriceNet(9);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getMaximumPrice()->isSet());
        $this->assertTrue($results[0]->getScoreCriteria()->getMaximumPrice()->isEvaluationResult());

        $priceRequirement->getPurchasePriceNet()->setMaximumValue(null);
        $priceRequirement->getPurchasePriceGross()->setMaximumValue(10);
        $propertyPricingInformation->setPurchasePriceGross(12);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getMaximumPrice()->isSet());
        $this->assertFalse($results[0]->getScoreCriteria()->getMaximumPrice()->isEvaluationResult());

        $propertyPricingInformation->setPurchasePriceGross(9);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getMaximumPrice()->isSet());
        $this->assertTrue($results[0]->getScoreCriteria()->getMaximumPrice()->isEvaluationResult());

        $priceRequirement->getPurchasePriceGross()->setMaximumValue(null);
        $priceRequirement->getPurchasePricePerSquareMeter()->setMaximumValue(10);
        $propertyPricingInformation->setPurchasePricePerSquareMeter(12);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getMaximumPrice()->isSet());
        $this->assertFalse($results[0]->getScoreCriteria()->getMaximumPrice()->isEvaluationResult());

        $propertyPricingInformation->setPurchasePricePerSquareMeter(9);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getMaximumPrice()->isSet());
        $this->assertTrue($results[0]->getScoreCriteria()->getMaximumPrice()->isEvaluationResult());

        $propertyMarketingInformation->setPropertyOfferType(PropertyOfferType::RENT);
        $priceRequirement->setPropertyOfferTypes([PropertyOfferType::RENT]);
        $priceRequirement->getPurchasePricePerSquareMeter()->setMaximumValue(null);
        $priceRequirement->getNetColdRent()->setMaximumValue(10);
        $propertyPricingInformation->setNetColdRent(12);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getMaximumPrice()->isSet());
        $this->assertFalse($results[0]->getScoreCriteria()->getMaximumPrice()->isEvaluationResult());

        $propertyPricingInformation->setNetColdRent(9);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getMaximumPrice()->isSet());
        $this->assertTrue($results[0]->getScoreCriteria()->getMaximumPrice()->isEvaluationResult());

        $priceRequirement->getNetColdRent()->setMaximumValue(null);
        $priceRequirement->getColdRent()->setMaximumValue(10);
        $propertyPricingInformation->setColdRent(12);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getMaximumPrice()->isSet());
        $this->assertFalse($results[0]->getScoreCriteria()->getMaximumPrice()->isEvaluationResult());

        $propertyPricingInformation->setColdRent(9);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getMaximumPrice()->isSet());
        $this->assertTrue($results[0]->getScoreCriteria()->getMaximumPrice()->isEvaluationResult());

        $priceRequirement->getColdRent()->setMaximumValue(null);
        $priceRequirement->getRentalPricePerSquareMeter()->setMaximumValue(10);
        $propertyPricingInformation->setRentalPricePerSquareMeter(12);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getMaximumPrice()->isSet());
        $this->assertFalse($results[0]->getScoreCriteria()->getMaximumPrice()->isEvaluationResult());

        $propertyPricingInformation->setRentalPricePerSquareMeter(9);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getMaximumPrice()->isSet());
        $this->assertTrue($results[0]->getScoreCriteria()->getMaximumPrice()->isEvaluationResult());

        $propertyPricingInformation->setRentalPricePerSquareMeter(null);
        $priceRequirement->getRentalPricePerSquareMeter()->setMinimumValue(null);
        $priceRequirement->getRentalPricePerSquareMeter()->setMaximumValue(1000);
        $propertyMarketingInformation->setPropertyOfferType(PropertyOfferType::PURCHASE);
        $priceRequirement->setPropertyOfferTypes([PropertyOfferType::PURCHASE]);
        $priceRequirement->getPurchasePriceNet()->setMinimumValue(10);
        $propertyPricingInformation->setPurchasePriceNet(9);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getMinimumPrice()->isSet());
        $this->assertFalse($results[0]->getScoreCriteria()->getMinimumPrice()->isEvaluationResult());

        $propertyPricingInformation->setPurchasePriceNet(11);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getMinimumPrice()->isSet());
        $this->assertTrue($results[0]->getScoreCriteria()->getMinimumPrice()->isEvaluationResult());

        $priceRequirement->getPurchasePriceNet()->setMinimumValue(null);
        $priceRequirement->getPurchasePriceGross()->setMinimumValue(10);
        $propertyPricingInformation->setPurchasePriceGross(9);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getMinimumPrice()->isSet());
        $this->assertFalse($results[0]->getScoreCriteria()->getMinimumPrice()->isEvaluationResult());

        $propertyPricingInformation->setPurchasePriceGross(11);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getMinimumPrice()->isSet());
        $this->assertTrue($results[0]->getScoreCriteria()->getMinimumPrice()->isEvaluationResult());

        $priceRequirement->getPurchasePriceGross()->setMinimumValue(null);
        $priceRequirement->getPurchasePricePerSquareMeter()->setMinimumValue(10);
        $propertyPricingInformation->setPurchasePricePerSquareMeter(9);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getMinimumPrice()->isSet());
        $this->assertFalse($results[0]->getScoreCriteria()->getMinimumPrice()->isEvaluationResult());

        $propertyPricingInformation->setPurchasePricePerSquareMeter(11);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getMinimumPrice()->isSet());
        $this->assertTrue($results[0]->getScoreCriteria()->getMinimumPrice()->isEvaluationResult());

        $propertyMarketingInformation->setPropertyOfferType(PropertyOfferType::RENT);
        $priceRequirement->setPropertyOfferTypes([PropertyOfferType::RENT]);
        $priceRequirement->getNetColdRent()->setMinimumValue(10);
        $propertyPricingInformation->setNetColdRent(9);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getMinimumPrice()->isSet());
        $this->assertFalse($results[0]->getScoreCriteria()->getMinimumPrice()->isEvaluationResult());

        $propertyPricingInformation->setNetColdRent(11);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getMinimumPrice()->isSet());
        $this->assertTrue($results[0]->getScoreCriteria()->getMinimumPrice()->isEvaluationResult());

        $priceRequirement->getNetColdRent()->setMinimumValue(null);
        $priceRequirement->getColdRent()->setMinimumValue(10);
        $propertyPricingInformation->setColdRent(9);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getMinimumPrice()->isSet());
        $this->assertFalse($results[0]->getScoreCriteria()->getMinimumPrice()->isEvaluationResult());

        $propertyPricingInformation->setColdRent(11);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getMinimumPrice()->isSet());
        $this->assertTrue($results[0]->getScoreCriteria()->getMinimumPrice()->isEvaluationResult());

        $priceRequirement->getColdRent()->setMinimumValue(null);
        $priceRequirement->getRentalPricePerSquareMeter()->setMinimumValue(10);
        $propertyPricingInformation->setRentalPricePerSquareMeter(9);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getMinimumPrice()->isSet());
        $this->assertFalse($results[0]->getScoreCriteria()->getMinimumPrice()->isEvaluationResult());

        $propertyPricingInformation->setRentalPricePerSquareMeter(11);
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getMinimumPrice()->isSet());
        $this->assertTrue($results[0]->getScoreCriteria()->getMinimumPrice()->isEvaluationResult());

        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getIndustryDensity()->isSet());
        $this->assertFalse($results[0]->getScoreCriteria()->getIndustryDensity()->isEvaluationResult());

        $buildingUnitFetchReturn->clear();
        $results = $pointMatchingStrategy->doMatching([$lookingForPropertyRequest], $buildingUnit);
        $this->assertTrue($results[0]->getScoreCriteria()->getIndustryDensity()->isSet());
        $this->assertTrue($results[0]->getScoreCriteria()->getIndustryDensity()->isEvaluationResult());

        $this->assertEquals(98, $results[0]->getScore());

        $lookingForPropertyRequest2 = $this->createLookingForPropertyRequest();

        $lookingForPropertyRequest2->getPriceRequirement()
            ->setPropertyOfferTypes([PropertyOfferType::RENT]);

        $results = $pointMatchingStrategy->doMatching(
            [
                $lookingForPropertyRequest,
                $lookingForPropertyRequest2,
            ],
            $buildingUnit
        );

        $this->assertTrue($results[0]->getScore() >= $results[1]->getScore());

        $lookingForPropertyRequest3 = $this->createLookingForPropertyRequest();

        $lookingForPropertyRequest3->getPriceRequirement()
            ->setNonCommissionable(true)
            ->setPropertyOfferTypes([PropertyOfferType::RENT]);

        $lookingForPropertyRequest3->getPropertyRequirement()
            ->setNumberOfParkingLots(5);

        $lookingForPropertyRequest3->getPropertyRequirement()->getInQuarters()->add($quarter);
        $lookingForPropertyRequest3->getPropertyRequirement()->getInPlaces()->add($place);

        $lookingForPropertyRequest3->getPropertyRequirement()->getPropertyValueRangeRequirement()->getTotalSpace()
            ->setMaximumValue(35);

        $results = $pointMatchingStrategy->doMatching(
            [
                $lookingForPropertyRequest,
                $lookingForPropertyRequest2,
                $lookingForPropertyRequest3,
            ],
            $buildingUnit
        );

        $this->assertEquals(98, $results[0]->getScore());
        $this->assertEquals(24, $results[1]->getScore());
        $this->assertEquals(10, $results[2]->getScore());
    }

    private function createValueRequirement(string $name): ValueRequirement
    {
        $valueRequirement = new ValueRequirement();
        $valueRequirement->setName($name);

        return $valueRequirement;
    }

    private function createLookingForPropertyRequest(): LookingForPropertyRequest
    {
        $totalSpace = $this->createValueRequirement('totalSpace');
        $retailSpace = $this->createValueRequirement('retailSpace');
        $outDoorSpace = $this->createValueRequirement('outDoorSpace');
        $subsidarySpace = $this->createValueRequirement('subsidarySpace');
        $storageSpace = $this->createValueRequirement('storageSpace');
        $shopWindowLength = $this->createValueRequirement('shopWindowLength');
        $shopWidth = $this->createValueRequirement('shopWidth');

        $purchasePriceNet  = $this->createValueRequirement('purchasePriceNet');
        $purchasePriceGross = $this->createValueRequirement('purchasePriceGross');
        $purchasePricePerSquareMeter = $this->createValueRequirement('purchasePricePerSquareMeter');
        $netColdRent = $this->createValueRequirement('netColdRent');
        $coldRent = $this->createValueRequirement('coldRent');
        $rentalPricePerSquareMeter = $this->createValueRequirement('rentalPricePerSquareMeter');
        $lease = $this->createValueRequirement('lease');

        $lookingForPropertyRequest = new LookingForPropertyRequest();
        $industryClassification = new IndustryClassification();
        $industryClassification
            ->setName('Test Classification')
            ->setLevelOne(1);
        $propertyRequirement = new PropertyRequirement();
        $propertyRequirement
            ->setIndustryClassification($industryClassification);
        $priceRequirement = new PriceRequirement();
        $priceRequirement
            ->setPurchasePriceNet($purchasePriceNet)
            ->setPurchasePriceGross($purchasePriceGross)
            ->setPurchasePricePerSquareMeter($purchasePricePerSquareMeter)
            ->setNetColdRent($netColdRent)
            ->setColdRent($coldRent)
            ->setRentalPricePerSquareMeter($rentalPricePerSquareMeter)
            ->setLease($lease);

        $propertyMandatoryRequirement = new PropertyMandatoryRequirement();
        $propertyValueRangeRequirement = new PropertyValueRangeRequirement();

        $propertyValueRangeRequirement
            ->setTotalSpace($totalSpace)
            ->setRetailSpace($retailSpace)
            ->setOutdoorSpace($outDoorSpace)
            ->setSubsidiarySpace($subsidarySpace)
            ->setStorageSpace($storageSpace)
            ->setShopWindowLength($shopWindowLength)
            ->setShopWidth($shopWidth);

        $lookingForPropertyRequest
            ->setPropertyRequirement($propertyRequirement)
            ->setPriceRequirement($priceRequirement);

        $propertyRequirement
            ->setPropertyMandatoryRequirement($propertyMandatoryRequirement)
            ->setPropertyValueRangeRequirement($propertyValueRangeRequirement);

        return $lookingForPropertyRequest;
    }
}
