interface PropertyRequirementFormData {
    totalSpaceMin: string;
    totalSpaceMax: string;
    retailSpaceMin: string;
    retailSpaceMax: string;
    outdoorSpaceMin: string;
    outdoorSpaceMax: string;
    subsidarySpaceMin: string;
    subsidarySpaceMax: string;
    usableSpaceMin: string;
    usableSpaceMax: string;
    storageSpaceMin: string;
    storageSpaceMax: string;
    numberOfParkingLots: string;
    roomCount: string;
    shopWindowLengthMin: string;
    shopWindowLengthMax: string;
    shopWidthMin: string;
    shopWidthMax: string;
    purchasePriceNetMin: string;
    purchasePriceNetMax: string;
    purchasePriceGrossMin: string;
    purchasePriceGrossMax: string;
    purchasePricePerSquareMeterMin: string;
    purchasePricePerSquareMeterMax: string;
    netColdRentMin: string;
    netColdRentMax: string;
    coldRentMin: string;
    coldRentMax: string;
    rentalPricePerSquareMeterMin: string;
    rentalPricePerSquareMeterMax: string;
    leaseMin: string;
    leaseMax: string;
    earliestAvailableFrom: string;
    latestAvailableFrom: string;
    onlyNonCommissionable: boolean;
    propertyOfferTypes: {buy: boolean, rent: boolean, lease: boolean};
    outdoorSpaceRequired: boolean;
    roofingRequired: boolean;
    showroomRequired: boolean;
    shopWindowRequired: boolean;
    barrierFreeAccessRequired: boolean;
    groundLevelSalesAreaRequired: boolean;
}

enum PropertyRequirementField {
    TotalSpaceMax,
    RetailSpaceMax,
    OutdoorSpaceMax,
    SubsidarySpaceMax,
    UsableSpaceMax,
    StorageSpaceMax,
    NumberOfParkingLots,
    RoomCount ,
    ShopWindowLengthMax,
    ShopWidthMax,
    PurchasePriceNetMax,
    PurchasePriceGrossMax,
    PurchasePricePerSquareMeterMax,
    NetColdRentMax,
    ColdRentMax,
    RentalPricePerSquareMeterMax ,
    LeaseMax,
    EarliestAvailableFrom,
    LatestAvailableFrom,
    OnlyNonCommissionable,
    PropertyOfferTypes,
    OutdoorSpaceRequired,
    RoofingRequired ,
    ShowroomRequired ,
    ShopWindowRequired ,
    BarrierFreeAccessRequired ,
    GroundLevelSalesAreaRequired,
    TotalSpaceMin,
    RetailSpaceMin,
    OutdoorSpaceMin,
    SubsidarySpaceMin,
    UsableSpaceMin,
    StorageSpaceMin,
    ShopWindowLengthMin,
    ShopWidthMin,
    PurchasePriceNetMin,
    PurchasePriceGrossMin,
    PurchasePricePerSquareMeterMin,
    NetColdRentMin,
    ColdRentMin,
    RentalPricePerSquareMeterMin,
    LeaseMin,
}

enum PropertyRequirementFieldName {
    TotalSpaceMax = 'totalSpaceMax',
    RetailSpaceMax = 'retailSpaceMax',
    OutdoorSpaceMax = 'outdoorSpaceMax',
    SubsidarySpaceMax = 'subsidarySpaceMax',
    UsableSpaceMax = 'usableSpaceMax',
    StorageSpaceMax = 'storageSpaceMax',
    NumberOfParkingLots = 'numberOfParkingLots',
    RoomCount = 'roomCount',
    ShopWindowLengthMax = 'shopWindowLengthMax',
    ShopWidthMax = 'shopWidthMax',
    PurchasePriceNetMax = 'purchasePriceNetMax',
    PurchasePriceGrossMax = 'purchasePriceGrossMax',
    PurchasePricePerSquareMeterMax = 'purchasePricePerSquareMeterMax',
    NetColdRentMax = 'netColdRentMax',
    ColdRentMax = 'coldRentMax',
    RentalPricePerSquareMeterMax = 'rentalPricePerSquareMeterMax',
    LeaseMax = 'leaseMax',
    EarliestAvailableFrom = 'earliestAvailableFrom',
    LatestAvailableFrom = 'latestAvailableFrom',
    OnlyNonCommissionable = 'onlyNonCommissionable',
    PropertyOfferTypes = 'propertyOfferTypes',
    OutdoorSpaceRequired = 'outdoorSpaceRequired',
    RoofingRequired = 'roofingRequired',
    ShowroomRequired = 'showroomRequired',
    ShopWindowRequired = 'shopWindowRequired',
    BarrierFreeAccessRequired = 'barrierFreeAccessRequired',
    GroundLevelSalesAreaRequired = 'groundLevelSalesAreaRequired',
    TotalSpaceMin = 'totalSpaceMin',
    RetailSpaceMin = 'retailSpaceMin',
    OutdoorSpaceMin = 'outdoorSpaceMin',
    SubsidarySpaceMin = 'subsidarySpaceMin',
    UsableSpaceMin = 'usableSpaceMin',
    StorageSpaceMin = 'storageSpaceMin',
    ShopWindowLengthMin = 'shopWindowLengthMin',
    ShopWidthMin = 'shopWidthMin',
    PurchasePriceNetMin = 'purchasePriceNetMin',
    PurchasePriceGrossMin = 'purchasePriceGrossMin',
    PurchasePricePerSquareMeterMin = 'purchasePricePerSquareMeterMin',
    NetColdRentMin = 'netColdRentMin',
    ColdRentMin = 'coldRentMin',
    RentalPricePerSquareMeterMin = 'rentalPricePerSquareMeterMin',
    LeaseMin = 'leaseMin',
}

enum PropertyRequirementFieldNameKey {
    'totalSpaceMax',
    'retailSpaceMax',
    'outdoorSpaceMax',
    'subsidarySpaceMax',
    'usableSpaceMax',
    'storageSpaceMax',
    'numberOfParkingLots',
    'roomCount',
    'shopWindowLengthMax',
    'shopWidthMax',
    'purchasePriceNetMax',
    'purchasePriceGrossMax',
    'purchasePricePerSquareMeterMax',
    'netColdRentMax',
    'coldRentMax',
    'rentalPricePerSquareMeterMax',
    'leaseMax',
    'earliestAvailableFrom',
    'latestAvailableFrom',
    'onlyNonCommissionable',
    'propertyOfferTypesBuy',
    'outdoorSpaceRequired' ,
    'roofingRequired' ,
    'showroomRequired' ,
    'shopWindowRequired' ,
    'barrierFreeAccessRequired' ,
    'groundLevelSalesAreaRequired',
    'totalSpaceMin',
    'retailSpaceMin',
    'outdoorSpaceMin',
    'subsidarySpaceMin',
    'usableSpaceMin',
    'storageSpaceMin',
    'shopWindowLengthMin' ,
    'shopWidthMin',
    'purchasePriceNetMin',
    'purchasePriceGrossMin',
    'purchasePricePerSquareMeterMin',
    'netColdRentMin',
    'coldRentMin',
    'rentalPricePerSquareMeterMin',
    'leaseMin',
}

export { PropertyRequirementFormData, PropertyRequirementField, PropertyRequirementFieldName, PropertyRequirementFieldNameKey };
