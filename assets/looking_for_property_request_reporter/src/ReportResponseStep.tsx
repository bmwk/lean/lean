import apiStyle from './Api.module.css';
import bootstrapStyle from './Bootstrap.module.scss';
import {Button} from '@mui/material';
import React from 'react';

interface ReportResponseStepProps {
    isResponseSuccessful: boolean;
    newReport: Function;
}

export default function ReportResponseStep({isResponseSuccessful, newReport}: ReportResponseStepProps) {

    const newReportClick = (): void => {
        newReport();
    }

    return (
        <div className={bootstrapStyle['container']}>
            {isResponseSuccessful === true ?
                <>
                    <div className={[ bootstrapStyle['row'], bootstrapStyle['mb-4'] ].join(' ')}>
                        <p className={apiStyle['headline-2']}>Ihre Meldung war <strong>erfolgreich</strong></p>
                        <p style={{textAlign: "justify"}}><strong>Vielen Dank</strong> für die Meldung Ihres Ansiedlungsgesuchs. <br/>
                            Sie erhalten in Kürze eine <strong>E-Mail mit
                            einem Bestätigungs-Link</strong> von uns.
                            Bitte klicken Sie auf diesen Link, <strong>um Ihre Meldung abzuschließen</strong>.
                            Ihre Angaben werden im Anschluss an die zuständigen Sachbearbeiter:innen weitergeleitet und diese werden sich
                            ggf. für Rückfragen mit Ihnen in Verbindung setzen.
                        </p>
                    </div>
                    <div className={[ bootstrapStyle['row'], bootstrapStyle['justify-content-end'] ].join(' ')}>
                        <Button className={[bootstrapStyle['col-6'], bootstrapStyle['col-md-4'], bootstrapStyle['col-lg-3'], bootstrapStyle['mx-md-3']].join(' ')}
                                variant="contained" color="secondary" size="medium" onClick={newReportClick}>
                            Weitere Gesuchsmeldung aufgeben
                        </Button>
                    </div>
                </>
                :
                <>
                    <div className={[ bootstrapStyle['row'], bootstrapStyle['mb-4'] ].join(' ')}>
                        <p className={apiStyle['headline-2']}>Das Absenden der Meldung ist leider <strong>fehlgeschlagen</strong></p>
                        <div style={{textAlign: "justify"}}>
                            Leider konnte der Gesuch aufgrund eines technischen Problems nicht gemeldet werden.<br/>
                            Bitte versuchen Sie, die Meldung erneut einzureichen.
                            Sollte der Fehler weiterhin bestehen, so schreiben Sie uns bitte eine E-Mail. <br/>
                            Vielen Dank für Ihr Verständnis.
                        </div>
                    </div>
                    <div className={[ bootstrapStyle['row'], bootstrapStyle['justify-content-end'] ].join(' ')}>
                        <Button className={[bootstrapStyle['col-6'], bootstrapStyle['col-md-4'], bootstrapStyle['col-lg-3'], bootstrapStyle['mx-md-3']].join(' ')}
                                variant="contained" color="secondary" size="medium" onClick={newReportClick}>
                            Erneut versuchen
                        </Button>
                    </div>
                </>
            }
        </div>
    );
}
