interface LocationRequirementFormData {
    quarterPlaceUuids: string[];
    locationCategories: string[];
    placeUuids: string[];
    locationFactors: string[];
    otherRequirementsForTheLocation: string;
}

enum LocationRequirementField {
    QuarterPlaceUuids,
    LocationCategories,
    PlaceUuids,
    LocationFactors ,
    OtherRequirementsForTheLocation
}

enum LocationRequirementFieldName {
    QuarterPlaceUuids = 'quarterPlaceUuids',
    LocationCategories = 'locationCategories',
    PlaceUuids = 'placeUuids',
    LocationFactors = 'locationFactors',
    OtherRequirementsForTheLocation = 'otherRequirementsForTheLocation'
}

enum LocationRequirementFieldNameKey {
    'quarterPlaceUuids',
    'locationCategories',
    'placeUuids',
    'locationFactors' ,
    'otherRequirementsForTheLocation'
}

export { LocationRequirementFormData, LocationRequirementField, LocationRequirementFieldName, LocationRequirementFieldNameKey };
