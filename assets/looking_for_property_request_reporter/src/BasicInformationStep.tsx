import Axios from 'axios';
import React, { useEffect, useRef, useState } from 'react';
import CheckboxField from '../../components/src/Form/CheckboxField';
import DateField from '../../components/src/Form/DateField';
import InputField from '../../components/src/Form/InputField';
import SelectField from '../../components/src/Form/SelectField';
import TextareaField from '../../components/src/Form/TextareaField';
import { BasicInformationField, BasicInformationFieldName, BasicInformationFormData } from './BasicInformationField';
import bootstrapStyle from './Bootstrap.module.scss';
import { IndustryClassification, Option } from './InterfaceLibrary';

interface BasicInformationStep {
    basicInformationFormData: BasicInformationFormData;
    setBasicInformationFormData: Function;
    requiredBasicInformationFormFields: BasicInformationField[];
    industryClassifications: IndustryClassification[];
    setHasIndustrySectorClassificationOptions: Function;
    setHasIndustryDetailsClassificationOptions: Function;
    apiUrl: string;
    errors: {[key: string]: string};
}

export default function BasicInformationStep({basicInformationFormData, setBasicInformationFormData, requiredBasicInformationFormFields, industryClassifications, setHasIndustrySectorClassificationOptions, setHasIndustryDetailsClassificationOptions, apiUrl, errors }: BasicInformationStep) {
    const [industryClassificationId, setIndustryClassificationId] = useState<string>(basicInformationFormData.industryClassificationId.toString());
    const [industrySectorId, setIndustrySectorId] = useState<string>(basicInformationFormData.industrySectorIndustryClassificationId.toString());
    const [industrySectorOptions, setIndustrySectorOptions] = useState<Option[]>([]);
    const [industryDetailsOptions, setIndustryDetailsOptions] = useState<Option[]>([]);
    const [showIndustrySectorOptions, setShowIndustrySectorOptions] = useState<boolean>(false);
    const [showIndustryDetailOptions, setShowIndustryDetailOptions] = useState<boolean>(false);
    const [industryClassificationIdDisabled, setIndustryClassificationIdDisabled] = useState<boolean>(false);
    const [industrySectorDisabled, setIndustrySectorDisabled] = useState<boolean>(false);
    const mountedRef = useRef(true)

    useEffect(() => {
        mountedRef.current = true
        if (basicInformationFormData.industryClassificationId != '') {
            initSectorAndDetailsOptions();
        }

        if (basicInformationFormData.requestAvailableTill != null ||  checkRequired(BasicInformationField.RequestAvailableTill)) {
            setBasicInformationFormData(BasicInformationFieldName.AutomaticEnding, true);
        }

        return () => {
            mountedRef.current = false
        }
    }, [])

    useEffect(() => {
        if (industryClassificationId != '') {
            fetchAndSetIndustrySectorOptionsByIndustryClassificationId();
        }
    },[industryClassificationId]);

    useEffect(() => {
        if (industrySectorId != '') {
            fetchAndSetIndustryDetailsOptionsByIndustrySectorId();
        }
    },[industrySectorId]);

    const initSectorAndDetailsOptions = async () => {
        await fetchAndSetIndustrySectorOptionsByIndustryClassificationId();

        if (basicInformationFormData.industrySectorIndustryClassificationId.toString() != '') {
            await fetchAndSetIndustryDetailsOptionsByIndustrySectorId();
        }
    }

    const checkRequired = (basicInformationField: BasicInformationField): boolean => {
        return requiredBasicInformationFormFields.includes(basicInformationField);
    }

    const resetClassification = (classification: string): void => {
        if (classification === BasicInformationFieldName.IndustrySectorIndustryClassificationId) {
            setIndustrySectorOptions([]);
            setIndustryDetailsOptions([]);
            setHasIndustrySectorClassificationOptions(true);
            setHasIndustryDetailsClassificationOptions(true)
            setBasicInformationFormData(BasicInformationFieldName.IndustrySectorIndustryClassificationId, '');
            setBasicInformationFormData(BasicInformationFieldName.IndustryDetailsIndustryClassificationId, '');

        } else if (classification === BasicInformationFieldName.IndustryDetailsIndustryClassificationId) {
            setIndustryDetailsOptions([]);
            setHasIndustryDetailsClassificationOptions(true)
            setBasicInformationFormData(BasicInformationFieldName.IndustryDetailsIndustryClassificationId, '');
        }
    }

    const handleInputChange = async (name: string, value: string | boolean) => {
        setBasicInformationFormData(name, value);

        if (name === BasicInformationFieldName.IndustryClassificationId) {
            setIndustryClassificationId(value.toString());
            setIndustryClassificationIdDisabled(true);
            setShowIndustrySectorOptions(true);
            setShowIndustryDetailOptions(false);
            resetClassification(BasicInformationFieldName.IndustrySectorIndustryClassificationId);
        }

        if (name === BasicInformationFieldName.IndustrySectorIndustryClassificationId) {
            setIndustrySectorId(value.toString());
            setIndustryClassificationIdDisabled(true);
            setIndustrySectorDisabled(true);
            setShowIndustryDetailOptions(true);
            resetClassification(BasicInformationFieldName.IndustryDetailsIndustryClassificationId);
        }
    };

    const fetchAndSetIndustrySectorOptionsByIndustryClassificationId = async () => {
        const options: Option[] = [];

        const response = await Axios.get(apiUrl + '/api/classifications/industry-classifications/' + basicInformationFormData.industryClassificationId + '/industry-classifications');

        response.data.forEach((industrySectorOption: {id: number, name: string}) => {
            options.push({label: industrySectorOption.name, value: industrySectorOption.id.toString()});
        })

        if (mountedRef.current) {
            setIndustrySectorOptions(options);
            setIndustryClassificationIdDisabled(false);
            setShowIndustrySectorOptions(options.length > 0)
            setHasIndustrySectorClassificationOptions(options.length > 0);
            setHasIndustryDetailsClassificationOptions(options.length > 0);
        }
    };

    const fetchAndSetIndustryDetailsOptionsByIndustrySectorId = async () => {
        const options: Option[] = [];

        const response = await Axios.get(apiUrl + '/api/classifications/industry-classifications/' + basicInformationFormData.industrySectorIndustryClassificationId + '/industry-classifications');

        response.data.forEach((industryDetailsOption: {id: number, name: string}) => {
            options.push({label: industryDetailsOption.name, value: industryDetailsOption.id.toString()});
        })

        setHasIndustryDetailsClassificationOptions(options.length > 0);

        if (mountedRef.current) {
            setIndustryDetailsOptions(options);
            setIndustryClassificationIdDisabled(false);
            setIndustrySectorDisabled(false);
            setShowIndustryDetailOptions(options.length > 0);
            setHasIndustryDetailsClassificationOptions(options.length > 0);
        }
    };

    const requestReasonOptions: Option[] = [
        {label: 'Erweiterung', value: 0},
        {label: 'Zweigstelle', value: 1},
        {label: 'Zuzug', value: 2},
        {label: 'Umzug', value: 3},
        {label: 'Existenzgründung', value: 4},
        {label: 'Sonstiges', value: 5},
    ];

    const getPropertyUsageToolTipText = (): JSX.Element => {
        const industryClassificationLevelOnes = industryClassifications.map(industryClassification => industryClassification.levelOne);

        return (
            <span>
                {industryClassificationLevelOnes.includes(1) && <span><strong>Einzelhandel:</strong><br/>Klassische Einzelhandelsgeschäft, wie Buchläden, Lebensmitteleinzelhandel, Textilwarengeschäfte<br/></span>}
                {industryClassificationLevelOnes.includes(2) && <span><strong>Dienstleistung:</strong><br/>Ein Klassischer Dienstleistungsbetrieb, wie ein Friseur, ein Architekt aber auch Banken, Reinigungen oder Anwaltsbüros<br/></span>}
                {industryClassificationLevelOnes.includes(3) && <span><strong>Gastronomie/Beherbergung:</strong><br/>Beherbergungsbetriebe wie Hotels und Gästehäuser sowie Restaurants, Bars oder Cafés<br/></span>}
                {industryClassificationLevelOnes.includes(4) && <span><strong>Kunst, Kultur, Unterhaltung, Sport, Spiel, Freizeit:</strong><br/>Theater, Museen, Kinos, Kirchen, Sporthallen<br/></span>}
                {industryClassificationLevelOnes.includes(5) && <span><strong>Parken, Personenbeförderung & Transport:</strong><br/>z. B. Parkhäuser, Tiefgaragen<br/></span>}
                {industryClassificationLevelOnes.includes(6) && <span><strong>Bildung & Lernen:</strong><br/>z. B. Schulen, Kindergärten/-tagesstätten, Fort- und Weiterbildungsangebote, Erwachsenenbildung<br/></span>}
                {industryClassificationLevelOnes.includes(7) && <span><strong>Gesundheitswesen:</strong><br/>z. B. Krankenhäuser/Kliniken, Tageskliniken, Arzt- und Zahnarztpraxen, Orthopädie<br/></span>}
                {industryClassificationLevelOnes.includes(8) && <span><strong>Sozialwesen/Gemeinwohl:</strong><br/>z. B. Jugendzentrum, Bürgerbüro/-haus, Obdachlosen-, Jugend-, Gemeinschafts- und Nachbarschaftshilfe<br/></span>}
                {industryClassificationLevelOnes.includes(9) && <span><strong>Einrichtungen der Verwaltung:</strong><br/>z. B. Rathaus, Finanzamt, Arbeitsamt und andere Verwaltungseinrichtungen<br/></span>}
                {industryClassificationLevelOnes.includes(10) && <span><strong>Interessensvertretungen, Vereine, kirchliche Einrichtungen:</strong><br/>z. B. Gewerkschaften, Parteien, Verbände, Jugendorganisationen, Kammern<br/></span>}
                {industryClassificationLevelOnes.includes(11) && <span><strong>Handwerk, Produktion, Manufaktur:</strong><br/>Produktions- und Handwerksbetriebe, beispielsweise für Goldschmiedearbeiten, Sanitär- und Elektroinstallationen<br/></span>}
                {industryClassificationLevelOnes.includes(12) && <span><strong>Kreativwirtschaft:</strong><br/>z. B. Verlagswesen, Film, Hörfunk und TV, Tonstudios<br/></span>}
                {industryClassificationLevelOnes.includes(13) && <span><strong>Großhandel & Handelsvermittlung:</strong><br/>Unternehmen, die Waren verschiedener Hersteller beschaffen und an gewerbliche Kunden und Großabnehmer weiterverkaufen<br/></span>}
                {industryClassificationLevelOnes.includes(14) && <span><strong>Andere gewerbliche Nutzung\t:</strong><br/>z. B. als Büro für andere als die aufgelisteten Zwecke<br/></span>}
                {industryClassificationLevelOnes.includes(15) && <span><strong>Wohnen:</strong><br/>z. B. Wohnungen<br/></span>}
                {industryClassificationLevelOnes.includes(16) && <span><strong>Andere nicht-gewerbliche Nutzung:</strong><br/>Sonstiges<br/></span>}
            </span>
        );
    };

    const getCurrentDate = (separator: string = '-'): string => {
        let newDate = new Date();
        let date = newDate.getDate();
        let month = newDate.getMonth() + 1;
        let year = newDate.getFullYear();

        return `${year}${separator}${month < 10 ? `0${month}` : `${month}`}${separator}${date < 10 ? `0${date}` : `${date}`}`;
    };

    return (
        <>
            <div className={bootstrapStyle['row']}>
                <div className={[bootstrapStyle['col-12'], bootstrapStyle['mb-4']].join(' ')}>
                    <InputField
                        type={'text'}
                        value={basicInformationFormData.title}
                        name={BasicInformationFieldName.Title}
                        isRequired={checkRequired(BasicInformationField.Title)}
                        label='Titel Ihres Gesuchs'
                        handleInputChange={handleInputChange}
                        error={errors[BasicInformationFieldName.Title]}
                    />
                </div>
            </div>

            <div className={bootstrapStyle['row']}>
                <div className={[bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['mb-4']].join(' ')}>
                    <SelectField
                        label='Gesuchsgrund'
                        value={basicInformationFormData.requestReason}
                        name={BasicInformationFieldName.RequestReason}
                        isRequired={checkRequired(BasicInformationField.RequestReason)}
                        handleInputChange={handleInputChange}
                        options={requestReasonOptions}
                        error={errors[BasicInformationFieldName.RequestReason]}
                    />
                </div>
                <div className={[bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['mb-4']].join(' ')}>
                    <SelectField
                        label='Vorgesehene Nutzung'
                        value={basicInformationFormData.industryClassificationId}
                        name={BasicInformationFieldName.IndustryClassificationId}
                        isRequired={checkRequired(BasicInformationField.IndustryClassificationId)}
                        handleInputChange={handleInputChange}
                        error={errors[BasicInformationFieldName.IndustryClassificationId]}
                        disabled={industryClassificationIdDisabled}
                        popoverText={getPropertyUsageToolTipText()}
                        options={industryClassifications.map((industryClassification) => ({label: industryClassification.name, value: industryClassification.id}))}
                    />
                </div>
            </div>

            <div className={bootstrapStyle['row']}>
                {showIndustrySectorOptions === true &&
                <>
                    {industrySectorOptions.length > 0 ?
                    <div className={[bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['mb-4']].join(' ')}>
                        <SelectField
                            label='Branchenbereich'
                            value={basicInformationFormData.industrySectorIndustryClassificationId}
                            name={BasicInformationFieldName.IndustrySectorIndustryClassificationId}
                            isRequired={checkRequired(BasicInformationField.IndustrySectorIndustryClassificationId)}
                            handleInputChange={handleInputChange}
                            options={industrySectorOptions}
                            error={errors[BasicInformationFieldName.IndustrySectorIndustryClassificationId]}
                            disabled={industrySectorDisabled}
                        />
                    </div>
                    :
                    <div className={[bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['d-flex'], bootstrapStyle['mb-4'], bootstrapStyle['text-center'], bootstrapStyle['align-self-center']].join(' ')}>
                        <div className={[bootstrapStyle['mx-3'], bootstrapStyle['fs-6']].join(' ')} style={{fontStyle: 'italic'}}>Suche nach weiteren Optionen</div>
                        <div className={[bootstrapStyle['spinner-border'], bootstrapStyle['spinner-border-sm'], bootstrapStyle['align-self-center']].join(' ')} role="status"><span className="sr-only"/></div>
                    </div>
                    }
                </>
                }
                {showIndustryDetailOptions === true &&
                <>
                    {industryDetailsOptions.length > 0 ?
                    <div className={[bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['mb-4']].join(' ')}>
                        <SelectField
                            label='Branchendetails'
                            value={basicInformationFormData.industryDetailsIndustryClassificationId}
                            name={BasicInformationFieldName.IndustryDetailsIndustryClassificationId}
                            isRequired={checkRequired(BasicInformationField.IndustryDetailsIndustryClassificationId)}
                            handleInputChange={handleInputChange}
                            options={industryDetailsOptions}
                            error={errors[BasicInformationFieldName.IndustryDetailsIndustryClassificationId]}
                        />
                    </div>
                    :
                    <div className={[bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['d-flex'], bootstrapStyle['mb-4'], bootstrapStyle['text-center'], bootstrapStyle['align-self-center']].join(' ')}>
                        <div className={[bootstrapStyle['mx-3'], bootstrapStyle['fs-6']].join(' ')} style={{fontStyle: 'italic'}}>Suche nach weiteren Optionen</div>
                        <div className={[bootstrapStyle['spinner-border'], bootstrapStyle['spinner-border-sm'], bootstrapStyle['align-self-center']].join(' ')} role="status"><span className="sr-only"/></div>
                    </div>
                    }
                </>
                }
            </div>

            <div className={bootstrapStyle['row']}>
                <div className={[bootstrapStyle['col-12'], bootstrapStyle['mb-4']].join(' ')}>
                    <TextareaField
                        label={'Beschreibung des Konzepts'}
                        value={basicInformationFormData.conceptDescription}
                        name={BasicInformationFieldName.ConceptDescription}
                        isRequired={checkRequired(BasicInformationField.ConceptDescription)}
                        handleInputChange={handleInputChange}
                        error={errors[BasicInformationFieldName.ConceptDescription]}
                    />
                </div>
            </div>

            <div className={[bootstrapStyle['row'], bootstrapStyle['mb-4']].join(' ')}>
                <div className={[bootstrapStyle['col-12'], bootstrapStyle['col-md-6']].join(' ')}>
                    <CheckboxField
                        name={BasicInformationFieldName.AutomaticEnding}
                        label={'Gesuch soll automatisch enden'}
                        checked={basicInformationFormData.automaticEnding}
                        isRequired={false}
                        handleInputChange={handleInputChange}
                    />
                </div>
                <div className={[bootstrapStyle['col-12'], bootstrapStyle['col-md-6']].join(' ')}>
                    {basicInformationFormData.automaticEnding === true &&
                        <DateField
                            label={'Gesuch endet'}
                            name={BasicInformationFieldName.RequestAvailableTill}
                            value={basicInformationFormData.requestAvailableTill}
                            handleInputChange={handleInputChange}
                            minDate={getCurrentDate()}
                            required={checkRequired(BasicInformationField.RequestAvailableTill)}
                            error={errors[BasicInformationFieldName.RequestAvailableTill]}
                        />
                    }
                </div>
            </div>
        </>
    );
}
