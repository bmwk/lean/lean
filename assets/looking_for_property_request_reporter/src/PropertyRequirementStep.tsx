import bootstrapStyle from './Bootstrap.module.scss';
import InputField from '../../components/src/Form/InputField';
import CheckboxField from '../../components/src/Form/CheckboxField';
import DateField from '../../components/src/Form/DateField';
import { PropertyRequirementFieldName, PropertyRequirementFormData, PropertyRequirementField } from './PropertyRequirementField';
import React, { useEffect } from 'react';

interface PropertyRequirementStep {
    propertyRequirementFormData: PropertyRequirementFormData;
    setPropertyRequirementFormData: Function;
    requiredPropertyInformationFormFields: PropertyRequirementField[];
    errors: {[key: string]: string};
    showFieldDependentOnSelectedIndustryClassificationId: Function;
}

export default function PropertyRequirementStep({propertyRequirementFormData, setPropertyRequirementFormData, requiredPropertyInformationFormFields, showFieldDependentOnSelectedIndustryClassificationId, errors}: PropertyRequirementStep) {
    const showRetailSpaceField: boolean = showFieldDependentOnSelectedIndustryClassificationId(PropertyRequirementFieldName.RetailSpaceMax);
    const showUsableSpaceField: boolean = showFieldDependentOnSelectedIndustryClassificationId(PropertyRequirementFieldName.UsableSpaceMax);
    const showSubsidarySpaceField: boolean = showFieldDependentOnSelectedIndustryClassificationId(PropertyRequirementFieldName.SubsidarySpaceMax);
    const showStorageSpaceField: boolean = showFieldDependentOnSelectedIndustryClassificationId(PropertyRequirementFieldName.StorageSpaceMax);
    const showShopWidthField: boolean = showFieldDependentOnSelectedIndustryClassificationId(PropertyRequirementFieldName.ShopWidthMax);

    useEffect(() => {
        if (propertyRequirementFormData.purchasePriceGrossMin != ''
            || propertyRequirementFormData.purchasePriceGrossMax != ''
            || propertyRequirementFormData.purchasePriceNetMin != ''
            || propertyRequirementFormData.purchasePriceNetMax != ''
            || propertyRequirementFormData.purchasePricePerSquareMeterMin != ''
            || propertyRequirementFormData.purchasePricePerSquareMeterMax != ''
        ) {
            propertyRequirementFormData.propertyOfferTypes.buy = true;
        }

        if (propertyRequirementFormData.netColdRentMin != ''
            || propertyRequirementFormData.netColdRentMax != ''
            || propertyRequirementFormData.coldRentMin != ''
            || propertyRequirementFormData.coldRentMax != ''
            || propertyRequirementFormData.rentalPricePerSquareMeterMin != ''
            || propertyRequirementFormData.rentalPricePerSquareMeterMax != ''
        ) {
            propertyRequirementFormData.propertyOfferTypes.rent = true;
        }

        if (propertyRequirementFormData.leaseMin != '' || propertyRequirementFormData.leaseMax != '') {
            propertyRequirementFormData.propertyOfferTypes.lease = true;
        }

        setPropertyRequirementFormData(PropertyRequirementFieldName.PropertyOfferTypes, propertyRequirementFormData.propertyOfferTypes);

        if (propertyRequirementFormData.outdoorSpaceMin != '' || propertyRequirementFormData.outdoorSpaceMax != '') {
            setPropertyRequirementFormData(PropertyRequirementFieldName.OutdoorSpaceRequired, true);
        }

        if (propertyRequirementFormData.shopWindowLengthMin != '' || propertyRequirementFormData.shopWindowLengthMax != '') {
            setPropertyRequirementFormData(PropertyRequirementFieldName.ShopWindowRequired, true);
        }
    }, [])

    const checkRequired = (propertyRequirementField: PropertyRequirementField): boolean => {
        return requiredPropertyInformationFormFields.includes(propertyRequirementField);
    }

    const handleInputChange = (name: string, value: string | boolean, group: string = null): void => {
        if (group === PropertyRequirementFieldName.PropertyOfferTypes
            && typeof value === "boolean"
            && (name === 'buy' || name === 'rent' || name === 'lease')
        ) {
            setPropertyRequirementFormData(PropertyRequirementFieldName.PropertyOfferTypes, {...propertyRequirementFormData.propertyOfferTypes, [name]: value});

        } else {
            setPropertyRequirementFormData(name, value);
        }
    };

    const getCurrentDate = (separator: string = '-'): string => {
        let newDate = new Date();
        let date = newDate.getDate();
        let month = newDate.getMonth() + 1;
        let year = newDate.getFullYear();
        return `${year}${separator}${month < 10 ? `0${month}` : `${month}`}${separator}${date < 10 ? `0${date}` : `${date}`}`;
    };

    return (
        <>
            <div className={ bootstrapStyle['row'] }>
                <div style={errors[PropertyRequirementFieldName.PropertyOfferTypes] && {color:'#d32f2f'}} className={[bootstrapStyle['col-6'], bootstrapStyle['fw-bold']].join(' ')}>
                    Angebotstypen *
                    {errors[PropertyRequirementFieldName.PropertyOfferTypes] &&
                    <p style={{color:'#d32f2f', fontSize: '0.75rem', fontWeight:'400'}}>{errors[PropertyRequirementFieldName.PropertyOfferTypes]}</p>}
                </div>
                <div className={bootstrapStyle['col-6']}>
                    <CheckboxField
                        name={PropertyRequirementFieldName.OnlyNonCommissionable}
                        label={'Nur provisionsfreie Angebote'}
                        checked={propertyRequirementFormData.onlyNonCommissionable}
                        isRequired={false}
                        handleInputChange={handleInputChange}
                        error={errors[PropertyRequirementFieldName.OnlyNonCommissionable]}
                    />
                </div>
            </div>

            <div className={ bootstrapStyle['row'] }>
                <div className={[bootstrapStyle['col-12'], bootstrapStyle['mb-2']].join(' ')}>
                    <CheckboxField
                        name={'buy'}
                        label={'zum Kauf'}
                        checked={propertyRequirementFormData.propertyOfferTypes.buy}
                        handleInputChange={handleInputChange}
                        isRequired={false}
                        group={PropertyRequirementFieldName.PropertyOfferTypes}
                    />
                </div>
                {propertyRequirementFormData.propertyOfferTypes.buy === true &&
                     <>
                        <div className={[ bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['col-lg-3'], bootstrapStyle['mb-4'] ].join(' ')}>
                            <InputField
                                type={'number'}
                                label='Nettokaufpreis min'
                                adornment={'€'}
                                value={propertyRequirementFormData.purchasePriceNetMin}
                                name={PropertyRequirementFieldName.PurchasePriceNetMin}
                                isRequired={checkRequired(PropertyRequirementField.PurchasePriceNetMin)}
                                handleInputChange={handleInputChange}
                                error={errors[PropertyRequirementFieldName.PurchasePriceNetMin]}
                            />
                        </div>
                        <div className={[ bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['col-lg-3'], bootstrapStyle['mb-4'] ].join(' ')}>
                            <InputField
                                type={'number'}
                                label='Nettokaufpreis max'
                                adornment={'€'}
                                value={propertyRequirementFormData.purchasePriceNetMax}
                                name={PropertyRequirementFieldName.PurchasePriceNetMax}
                                isRequired={checkRequired(PropertyRequirementField.PurchasePriceNetMax)}
                                handleInputChange={handleInputChange}
                                error={errors[PropertyRequirementFieldName.PurchasePriceNetMax]}
                            />
                        </div>
                        <div className={[ bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['col-lg-3'], bootstrapStyle['mb-4'] ].join(' ')}>
                            <InputField
                                 type={'number'}
                                 label='Bruttokaufpreis min'
                                 adornment={'€'}
                                 value={propertyRequirementFormData.purchasePriceGrossMin}
                                 name={PropertyRequirementFieldName.PurchasePriceGrossMin}
                                 isRequired={checkRequired(PropertyRequirementField.PurchasePriceGrossMin)}
                                 handleInputChange={handleInputChange}
                                 error={errors[PropertyRequirementFieldName.PurchasePriceGrossMin]}
                            />
                        </div>
                        <div className={[ bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['col-lg-3'], bootstrapStyle['mb-4'] ].join(' ')}>
                            <InputField
                                 type={'number'}
                                 label='Bruttokaufpreis max'
                                 adornment={'€'}
                                 value={propertyRequirementFormData.purchasePriceGrossMax}
                                 name={PropertyRequirementFieldName.PurchasePriceGrossMax}
                                 isRequired={checkRequired(PropertyRequirementField.PurchasePriceGrossMax)}
                                 handleInputChange={handleInputChange}
                                 error={errors[PropertyRequirementFieldName.PurchasePriceGrossMax]}
                            />
                        </div>
                        <div className={[ bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['col-lg-3'], bootstrapStyle['mb-4'] ].join(' ')}>
                            <InputField
                                 type={'number'}
                                 label='Kaufpreis pro m² min'
                                 adornment={'€'}
                                 value={propertyRequirementFormData.purchasePricePerSquareMeterMin}
                                 name={PropertyRequirementFieldName.PurchasePricePerSquareMeterMin}
                                 isRequired={checkRequired(PropertyRequirementField.PurchasePricePerSquareMeterMin)}
                                 handleInputChange={handleInputChange}
                                 error={errors[PropertyRequirementFieldName.PurchasePricePerSquareMeterMin]}
                            />
                        </div>
                        <div className={[ bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['col-lg-3'], bootstrapStyle['mb-4'] ].join(' ')}>
                            <InputField
                                 type={'number'}
                                 label='Kaufpreis pro m² max'
                                 adornment={'€'}
                                 value={propertyRequirementFormData.purchasePricePerSquareMeterMax}
                                 name={PropertyRequirementFieldName.PurchasePricePerSquareMeterMax}
                                 isRequired={checkRequired(PropertyRequirementField.PurchasePricePerSquareMeterMax)}
                                 handleInputChange={handleInputChange}
                                 error={errors[PropertyRequirementFieldName.PurchasePricePerSquareMeterMax]}
                            />
                        </div>
                     </>
                }
            </div>

            <div className={ bootstrapStyle['row'] }>
                <div className={ [bootstrapStyle['col-12'], bootstrapStyle['mb-2'] ].join(' ')}>
                    <CheckboxField
                        name={'rent'}
                        label={'zur Miete'}
                        checked={propertyRequirementFormData.propertyOfferTypes.rent}
                        handleInputChange={handleInputChange}
                        isRequired={false}
                        group={PropertyRequirementFieldName.PropertyOfferTypes}
                    />
                </div>
                {propertyRequirementFormData.propertyOfferTypes.rent === true &&
                    <>
                        <div className={[ bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['col-lg-3'], bootstrapStyle['mb-4'] ].join(' ')}>
                            <InputField
                                type={'number'}
                                label='Nettokaltmiete min'
                                adornment={'€'}
                                value={propertyRequirementFormData.netColdRentMin}
                                name={PropertyRequirementFieldName.NetColdRentMin}
                                isRequired={checkRequired(PropertyRequirementField.NetColdRentMin)}
                                handleInputChange={handleInputChange}
                                error={errors[PropertyRequirementFieldName.NetColdRentMin]}
                            />
                        </div>
                        <div className={[ bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['col-lg-3'], bootstrapStyle['mb-4'] ].join(' ')}>
                            <InputField
                                type={'number'}
                                label='Nettokaltmiete max'
                                adornment={'€'}
                                value={propertyRequirementFormData.netColdRentMax}
                                name={PropertyRequirementFieldName.NetColdRentMax}
                                isRequired={checkRequired(PropertyRequirementField.NetColdRentMax)}
                                handleInputChange={handleInputChange}
                                error={errors[PropertyRequirementFieldName.NetColdRentMax]}
                            />
                        </div>
                        <div className={[ bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['col-lg-3'], bootstrapStyle['mb-4'] ].join(' ')}>
                            <InputField
                                type={'number'}
                                label='Kaltmiete min'
                                adornment={'€'}
                                value={propertyRequirementFormData.coldRentMin}
                                name={PropertyRequirementFieldName.ColdRentMin}
                                isRequired={checkRequired(PropertyRequirementField.ColdRentMin)}
                                handleInputChange={handleInputChange}
                                error={errors[PropertyRequirementFieldName.ColdRentMin]}
                            />
                        </div>
                        <div className={[ bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['col-lg-3'], bootstrapStyle['mb-4'] ].join(' ')}>
                            <InputField
                                type={'number'}
                                label='Kaltmiete max'
                                adornment={'€'}
                                value={propertyRequirementFormData.coldRentMax}
                                name={PropertyRequirementFieldName.ColdRentMax}
                                isRequired={checkRequired(PropertyRequirementField.ColdRentMax)}
                                handleInputChange={handleInputChange}
                                error={errors[PropertyRequirementFieldName.ColdRentMax]}
                            />
                        </div>
                        <div className={[ bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['col-lg-3'], bootstrapStyle['mb-4'] ].join(' ')}>
                            <InputField
                                type={'number'}
                                label='Mietpreis pro m² min'
                                adornment={'€'}
                                value={propertyRequirementFormData.rentalPricePerSquareMeterMin}
                                name={PropertyRequirementFieldName.RentalPricePerSquareMeterMin}
                                isRequired={checkRequired(PropertyRequirementField.RentalPricePerSquareMeterMin)}
                                handleInputChange={handleInputChange}
                                error={errors[PropertyRequirementFieldName.RentalPricePerSquareMeterMin]}
                            />
                        </div>
                        <div className={[ bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['col-lg-3'], bootstrapStyle['mb-4'] ].join(' ')}>
                            <InputField
                                type={'number'}
                                label='Mietpreis pro m² max'
                                adornment={'€'}
                                value={propertyRequirementFormData.rentalPricePerSquareMeterMax}
                                name={PropertyRequirementFieldName.RentalPricePerSquareMeterMax}
                                isRequired={checkRequired(PropertyRequirementField.RentalPricePerSquareMeterMax)}
                                handleInputChange={handleInputChange}
                                error={errors[PropertyRequirementFieldName.RentalPricePerSquareMeterMax]}
                            />
                        </div>
                    </>
                }
            </div>

            <div className={[ bootstrapStyle['row'], bootstrapStyle['mb-4'] ].join(' ')}>
                <div className={ [bootstrapStyle['col-12'], bootstrapStyle['mb-2'] ].join(' ')}>
                    <CheckboxField
                        name={'lease'}
                        label={'zur Pacht'}
                        checked={propertyRequirementFormData.propertyOfferTypes.lease}
                        handleInputChange={handleInputChange}
                        isRequired={false}
                        group={PropertyRequirementFieldName.PropertyOfferTypes}
                    />
                </div>
                {propertyRequirementFormData.propertyOfferTypes.lease === true &&
                    <>
                        <div className={[ bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['col-lg-3'], bootstrapStyle['mb-4'] ].join(' ')}>
                            <InputField
                                type={'number'}
                                label='Pachtpreis min'
                                adornment={'€'}
                                value={propertyRequirementFormData.leaseMin}
                                name={PropertyRequirementFieldName.LeaseMin}
                                isRequired={checkRequired(PropertyRequirementField.LeaseMin)}
                                handleInputChange={handleInputChange}
                                error={errors[PropertyRequirementFieldName.LeaseMin]}
                            />
                        </div>
                        <div className={[ bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['col-lg-3'], bootstrapStyle['mb-4'] ].join(' ')}>
                            <InputField
                                type={'number'}
                                label='Pachtpreis max'
                                adornment={'€'}
                                value={propertyRequirementFormData.leaseMax}
                                name={PropertyRequirementFieldName.LeaseMax}
                                isRequired={checkRequired(PropertyRequirementField.LeaseMax)}
                                handleInputChange={handleInputChange}
                                error={errors[PropertyRequirementFieldName.LeaseMax]}
                            />
                        </div>
                    </>
                }
            </div>

            <div className={[ bootstrapStyle['row'], bootstrapStyle['mb-4'] ].join(' ')}>
                <div className={[ bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['col-xl-3'], bootstrapStyle['mb-4'] ].join(' ')}>
                    <InputField
                        type={'number'}
                        label='Gesamtflächenbedarf min'
                        adornment={'m²'}
                        value={propertyRequirementFormData.totalSpaceMin}
                        name={PropertyRequirementFieldName.TotalSpaceMin}
                        isRequired={checkRequired(PropertyRequirementField.TotalSpaceMin)}
                        handleInputChange={handleInputChange}
                        error={errors[PropertyRequirementFieldName.TotalSpaceMin]}
                    />
                </div>
                <div className={[ bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['col-xl-3'], bootstrapStyle['mb-4'] ].join(' ')}>
                    <InputField
                        type={'number'}
                        label='Gesamtflächenbedarf max'
                        adornment={'m²'}
                        value={propertyRequirementFormData.totalSpaceMax}
                        name={PropertyRequirementFieldName.TotalSpaceMax}
                        isRequired={checkRequired(PropertyRequirementField.TotalSpaceMax)}
                        handleInputChange={handleInputChange}
                        error={errors[PropertyRequirementFieldName.TotalSpaceMax]}
                    />
                </div>
                {showRetailSpaceField === true &&
                <>
                    <div className={[ bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['col-xl-3'], bootstrapStyle['mb-4'] ].join(' ')}>
                        <InputField
                            type={'number'}
                            label='Verkaufsflächenbedarf min'
                            adornment={'m²'}
                            value={propertyRequirementFormData.retailSpaceMin}
                            name={PropertyRequirementFieldName.RetailSpaceMin}
                            isRequired={checkRequired(PropertyRequirementField.RetailSpaceMin)}
                            handleInputChange={handleInputChange}
                            error={errors[PropertyRequirementFieldName.RetailSpaceMin]}
                        />
                    </div>
                    <div className={[ bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['col-xl-3'], bootstrapStyle['mb-4'] ].join(' ')}>
                        <InputField
                            type={'number'}
                            label='Verkaufsflächenbedarf max'
                            adornment={'m²'}
                            value={propertyRequirementFormData.retailSpaceMax}
                            name={PropertyRequirementFieldName.RetailSpaceMax}
                            isRequired={checkRequired(PropertyRequirementField.RetailSpaceMax)}
                            handleInputChange={handleInputChange}
                            error={errors[PropertyRequirementFieldName.RetailSpaceMax]}
                        />
                    </div>
                </>}
                {showSubsidarySpaceField === true &&
                <>
                    <div className={[ bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['col-xl-3'], bootstrapStyle['mb-4'] ].join(' ')}>
                        <InputField
                            type={'number'}
                            label='Nebenflächenbedarf min'
                            adornment={'m²'}
                            value={propertyRequirementFormData.subsidarySpaceMin}
                            name={PropertyRequirementFieldName.SubsidarySpaceMin}
                            isRequired={checkRequired(PropertyRequirementField.SubsidarySpaceMin)}
                            handleInputChange={handleInputChange}
                            error={errors[PropertyRequirementFieldName.SubsidarySpaceMin]}
                        />
                    </div>
                    <div className={[ bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['col-xl-3'], bootstrapStyle['mb-4'] ].join(' ')}>
                        <InputField
                            type={'number'}
                            label='Nebenflächenbedarf max'
                            adornment={'m²'}
                            value={propertyRequirementFormData.subsidarySpaceMax}
                            name={PropertyRequirementFieldName.SubsidarySpaceMax}
                            isRequired={checkRequired(PropertyRequirementField.SubsidarySpaceMax)}
                            handleInputChange={handleInputChange}
                            error={errors[PropertyRequirementFieldName.SubsidarySpaceMax]}
                        />
                    </div>
                </>}
                {showStorageSpaceField === true &&
                <>
                    <div className={[ bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['col-xl-3'], bootstrapStyle['mb-4'] ].join(' ')}>
                        <InputField
                            type={'number'}
                            label='Lagerflächenbedarf min'
                            adornment={'m²'}
                            value={propertyRequirementFormData.storageSpaceMin}
                            name={PropertyRequirementFieldName.StorageSpaceMin}
                            isRequired={checkRequired(PropertyRequirementField.StorageSpaceMin)}
                            handleInputChange={handleInputChange}
                            error={errors[PropertyRequirementFieldName.StorageSpaceMin]}
                        />
                    </div>
                    <div className={[ bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['col-xl-3'], bootstrapStyle['mb-4'] ].join(' ')}>
                        <InputField
                            type={'number'}
                            label='Lagerflächenbedarf max'
                            adornment={'m²'}
                            value={propertyRequirementFormData.storageSpaceMax}
                            name={PropertyRequirementFieldName.StorageSpaceMax}
                            isRequired={checkRequired(PropertyRequirementField.StorageSpaceMax)}
                            handleInputChange={handleInputChange}
                            error={errors[PropertyRequirementFieldName.StorageSpaceMax]}
                        />
                    </div>
                </>}
                {showUsableSpaceField === true &&
                <>
                    <div className={[ bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['col-xl-3'], bootstrapStyle['mb-4'] ].join(' ')}>
                        <InputField
                            type={'number'}
                            label='Nutzflächenbedarf min'
                            adornment={'m²'}
                            value={propertyRequirementFormData.usableSpaceMin}
                            name={PropertyRequirementFieldName.UsableSpaceMin}
                            isRequired={checkRequired(PropertyRequirementField.UsableSpaceMin)}
                            handleInputChange={handleInputChange}
                            error={errors[PropertyRequirementFieldName.UsableSpaceMin]}
                        />
                    </div>
                    <div className={[ bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['col-xl-3'], bootstrapStyle['mb-4'] ].join(' ')}>
                        <InputField
                            type={'number'}
                            label='Nutzflächenbedarf max'
                            adornment={'m²'}
                            value={propertyRequirementFormData.usableSpaceMax}
                            name={PropertyRequirementFieldName.UsableSpaceMax}
                            isRequired={checkRequired(PropertyRequirementField.UsableSpaceMax)}
                            handleInputChange={handleInputChange}
                            error={errors[PropertyRequirementFieldName.UsableSpaceMax]}
                        />
                    </div>
                </>}

            </div>

            <div className={ bootstrapStyle['row'] }>
                <div className={[ bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['col-xl-3'], bootstrapStyle['mb-4'] ].join(' ')}>
                    <CheckboxField
                        name={PropertyRequirementFieldName.OutdoorSpaceRequired}
                        label={'Außenfläche benötigt'}
                        checked={propertyRequirementFormData.outdoorSpaceRequired}
                        handleInputChange={handleInputChange}
                        isRequired={false}
                        error={errors[PropertyRequirementFieldName.OutdoorSpaceRequired]}
                    />
                </div>
                {propertyRequirementFormData.outdoorSpaceRequired === true &&
                    <>
                        <div className={[ bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['col-xl-3'], bootstrapStyle['mb-4'] ].join(' ')}>
                            <InputField
                                type={'number'}
                                label='Außenflächenbedarf min'
                                adornment={'m²'}
                                value={propertyRequirementFormData.outdoorSpaceMin}
                                name={PropertyRequirementFieldName.OutdoorSpaceMin}
                                isRequired={checkRequired(PropertyRequirementField.OutdoorSpaceMin)}
                                handleInputChange={handleInputChange}
                                error={errors[PropertyRequirementFieldName.OutdoorSpaceMin]}
                            />
                        </div>
                        <div className={[ bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['col-xl-3'], bootstrapStyle['mb-4'] ].join(' ')}>
                            <InputField
                                type={'number'}
                                label='Außenflächenbedarf max'
                                adornment={'m²'}
                                value={propertyRequirementFormData.outdoorSpaceMax}
                                name={PropertyRequirementFieldName.OutdoorSpaceMax}
                                isRequired={checkRequired(PropertyRequirementField.OutdoorSpaceMax)}
                                handleInputChange={handleInputChange}
                                error={errors[PropertyRequirementFieldName.OutdoorSpaceMax]}
                            />
                        </div>
                        <div className={[ bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['col-xl-3'], bootstrapStyle['mb-4'] ].join(' ')}>
                            <CheckboxField
                                name={PropertyRequirementFieldName.RoofingRequired}
                                label={'Überdachung benötigt'}
                                checked={propertyRequirementFormData.roofingRequired}
                                isRequired={false}
                                handleInputChange={handleInputChange}
                                error={errors[PropertyRequirementFieldName.RoofingRequired]}
                            />
                        </div>
                    </>
                }
            </div>

            <div className={ bootstrapStyle['row'] }>
                <div className={[ bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['col-xl-3'], bootstrapStyle['mb-4'] ].join(' ')}>
                    <CheckboxField
                        name={PropertyRequirementFieldName.ShowroomRequired}
                        label={'Showroom benötigt'}
                        checked={propertyRequirementFormData.showroomRequired}
                        isRequired={false}
                        handleInputChange={handleInputChange}
                        error={errors[PropertyRequirementFieldName.ShowroomRequired]}
                    />
                </div>
                <div className={[ bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['col-xl-3'], bootstrapStyle['mb-4'] ].join(' ')}>
                    <CheckboxField
                        name={PropertyRequirementFieldName.ShopWindowRequired}
                        label={'Schaufenster benötigt'}
                        checked={propertyRequirementFormData.shopWindowRequired}
                        handleInputChange={handleInputChange}
                        isRequired={false}
                        error={errors[PropertyRequirementFieldName.ShopWindowRequired]}
                    />
                </div>
                {propertyRequirementFormData.shopWindowRequired === true &&
                    <>
                        <div className={[ bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['col-xl-3'], bootstrapStyle['mb-4'] ].join(' ')}>
                            <InputField
                                type={'number'}
                                label='Schaufensterbreite min'
                                adornment={'m'}
                                value={propertyRequirementFormData.shopWindowLengthMin}
                                name={PropertyRequirementFieldName.ShopWindowLengthMin}
                                isRequired={checkRequired(PropertyRequirementField.ShopWindowLengthMin)}
                                handleInputChange={handleInputChange}
                                error={errors[PropertyRequirementFieldName.ShopWindowLengthMin]}
                            />
                        </div>
                        <div className={[ bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['col-xl-3'], bootstrapStyle['mb-4'] ].join(' ')}>
                            <InputField
                                type={'number'}
                                label='Schaufensterbreite max'
                                adornment={'m'}
                                value={propertyRequirementFormData.shopWindowLengthMax}
                                name={PropertyRequirementFieldName.ShopWindowLengthMax}
                                isRequired={checkRequired(PropertyRequirementField.ShopWindowLengthMax)}
                                handleInputChange={handleInputChange}
                                error={errors[PropertyRequirementFieldName.ShopWindowLengthMax]}
                            />
                        </div>
                    </>
                }
            </div>

            <div className={ bootstrapStyle['row'] }>
                <div className={[ bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], showShopWidthField && bootstrapStyle['col-lg-3'], bootstrapStyle['mb-4'] ].join(' ')}>
                    <InputField
                        type={'number'}
                        label='Anzahl benötigter Parkplätze'
                        value={propertyRequirementFormData.numberOfParkingLots}
                        name={PropertyRequirementFieldName.NumberOfParkingLots}
                        isRequired={checkRequired(PropertyRequirementField.NumberOfParkingLots)}
                        handleInputChange={handleInputChange}
                        error={errors[PropertyRequirementFieldName.NumberOfParkingLots]}
                    />
                </div>
                <div className={[ bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], showShopWidthField && bootstrapStyle['col-lg-3'], bootstrapStyle['mb-4'] ].join(' ')}>
                    <InputField
                        type={'number'}
                        label='Anzahl Räume'
                        value={propertyRequirementFormData.roomCount}
                        name={PropertyRequirementFieldName.RoomCount}
                        isRequired={checkRequired(PropertyRequirementField.RoomCount)}
                        handleInputChange={handleInputChange}
                        error={errors[PropertyRequirementFieldName.RoomCount]}
                    />
                </div>
                {showShopWidthField === true &&
                <>
                    <div className={[ bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['col-lg-3'], bootstrapStyle['mb-4'] ].join(' ')}>
                        <InputField
                            type={'number'}
                            label='Ladenbreite min'
                            adornment={'m'}
                            value={propertyRequirementFormData.shopWidthMin}
                            name={PropertyRequirementFieldName.ShopWidthMin}
                            isRequired={checkRequired(PropertyRequirementField.ShopWidthMin)}
                            handleInputChange={handleInputChange}
                            error={errors[PropertyRequirementFieldName.ShopWidthMin]}
                        />
                    </div>

                    <div className={[ bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['col-lg-3'], bootstrapStyle['mb-4'] ].join(' ')}>
                        <InputField
                            type={'number'}
                            label='Ladenbreite max'
                            adornment={'m'}
                            value={propertyRequirementFormData.shopWidthMax}
                            name={PropertyRequirementFieldName.ShopWidthMax}
                            isRequired={checkRequired(PropertyRequirementField.ShopWidthMax)}
                            handleInputChange={handleInputChange}
                            error={errors[PropertyRequirementFieldName.ShopWidthMax]}
                        />
                    </div>
                </>
                }
            </div>

            <div className={ bootstrapStyle['row'] }>
                <div className={[ bootstrapStyle['col-6'], bootstrapStyle['col-lg-3'], bootstrapStyle['mb-4'] ].join(' ')}>
                    <CheckboxField
                        name={PropertyRequirementFieldName.BarrierFreeAccessRequired}
                        label={'Barrierefreier Zugang benötigt'}
                        checked={propertyRequirementFormData.barrierFreeAccessRequired}
                        isRequired={false}
                        handleInputChange={handleInputChange}
                        error={errors[PropertyRequirementFieldName.BarrierFreeAccessRequired]}
                    />
                </div>
                <div className={[ bootstrapStyle['col-6'], bootstrapStyle['col-lg-3'], bootstrapStyle['mb-4'] ].join(' ')}>
                    <CheckboxField
                        name={PropertyRequirementFieldName.GroundLevelSalesAreaRequired}
                        label={'Ebenerdige Verkaufsfläche benötigt'}
                        checked={propertyRequirementFormData.groundLevelSalesAreaRequired}
                        isRequired={false}
                        handleInputChange={handleInputChange}
                        error={errors[PropertyRequirementFieldName.GroundLevelSalesAreaRequired]}
                    />
                </div>
                <div className={[ bootstrapStyle['col-12'], bootstrapStyle['col-6'], bootstrapStyle['col-lg-3'], bootstrapStyle['mb-4'] ].join(' ')}>
                    <DateField
                        label={'frühestes Bezugsdatum'}
                        name={PropertyRequirementFieldName.EarliestAvailableFrom}
                        value={propertyRequirementFormData.earliestAvailableFrom}
                        handleInputChange={handleInputChange}
                        required={checkRequired(PropertyRequirementField.EarliestAvailableFrom)}
                        minDate={getCurrentDate()}
                        error={errors[PropertyRequirementFieldName.EarliestAvailableFrom]}
                    />
                </div>
                <div className={[ bootstrapStyle['col-12'], bootstrapStyle['col-6'], bootstrapStyle['col-lg-3'], bootstrapStyle['mb-4'] ].join(' ')}>
                    <DateField
                        label={'spätestes Bezugsdatum'}
                        name={PropertyRequirementFieldName.LatestAvailableFrom}
                        value={propertyRequirementFormData.latestAvailableFrom}
                        handleInputChange={handleInputChange}
                        required={checkRequired(PropertyRequirementField.LatestAvailableFrom)}
                        minDate={getCurrentDate()}
                        error={errors[PropertyRequirementFieldName.LatestAvailableFrom]}
                    />
                </div>
            </div>

        </>
    );
}
