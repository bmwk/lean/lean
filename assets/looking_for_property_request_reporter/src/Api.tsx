import { Button } from '@mui/material';
import { createTheme, Theme, ThemeProvider } from '@mui/material/styles';
import Axios, { AxiosResponse } from 'axios';
import dayjs from 'dayjs';
import React, { useEffect, useState } from 'react';
import LoadingImage from '../../lean/images/lean-loading.gif';
import apiStyle from './Api.module.css';
import { BasicInformationFieldName, BasicInformationFieldNameKey, BasicInformationFormData } from './BasicInformationField';
import BasicInformationStep from './BasicInformationStep';
import bootstrapStyle from './Bootstrap.module.scss';
import { IndustryClassification, Place } from './InterfaceLibrary';
import { LocationRequirementFieldName, LocationRequirementFieldNameKey, LocationRequirementFormData } from './LocationRequirementField';
import LocationRequirementStep from './LocationRequirementStep';
import { PropertyRequirementFieldName, PropertyRequirementFieldNameKey, PropertyRequirementFormData } from './PropertyRequirementField';
import PropertyRequirementStep from './PropertyRequirementStep';
import { ReporterFieldName, ReporterFieldNameKey, ReporterFormData } from './ReporterField';
import ReporterStep from './ReporterStep';
import ReportResponseStep from './ReportResponseStep';

interface ApiProps {
    accountKey: string;
    apiUrl: string;
}

export default function Api({accountKey, apiUrl}: ApiProps) {
    const apiContainer = React.useRef<HTMLDivElement>();
    const progressBar = React.useRef<HTMLDivElement>();
    const [theme, setTheme] = useState<Theme>(createTheme());
    const [currentStepNumber, setCurrentStepNumber] = useState<number>(0);
    const [config, setConfig] = useState<any>();
    const [onReportSend, setOnReportSend] = useState<boolean>(false);
    const [isResponseSuccessful, setIsResponseSuccessful] = useState<boolean>(false);
    const [errors, setErrors] = useState<{ [key: string]: string }>({});
    const [propertyUsageList, setPropertyUsageList] = useState<IndustryClassification[]>([]);
    const [quarterOptions, setQuarterOptions] = useState<Place[]>();
    const [placeUuids, setPlaceUuids] = useState<string[]>();
    const [hasIndustrySectorClassificationOptions, setHasIndustrySectorClassificationOptions] = useState<boolean>(true);
    const [hasIndustryDetailsClassificationOptions, setHasIndustryDetailsClassificationOptions] = useState<boolean>(true);

    const initialBasicInformationFormData: BasicInformationFormData = {
        title: '',
        requestReason: '',
        industryClassificationId: '',
        industrySectorIndustryClassificationId: '',
        industryDetailsIndustryClassificationId: '',
        conceptDescription: '',
        automaticEnding: false,
        requestAvailableTill: null
    };
    const initialLocationRequirementFormData: LocationRequirementFormData = {
        placeUuids: [],
        quarterPlaceUuids: [],
        locationCategories: [],
        locationFactors: [],
        otherRequirementsForTheLocation: ''
    };
    const initialPropertyRequirementFormData: PropertyRequirementFormData = {
        onlyNonCommissionable: false,
        propertyOfferTypes: {buy: true, rent: true, lease: true},
        purchasePriceNetMin: '',
        purchasePriceNetMax: '',
        purchasePriceGrossMin: '',
        purchasePriceGrossMax: '',
        purchasePricePerSquareMeterMin: '',
        purchasePricePerSquareMeterMax: '',
        netColdRentMin: '',
        netColdRentMax: '',
        coldRentMin: '',
        coldRentMax: '',
        rentalPricePerSquareMeterMin: '',
        rentalPricePerSquareMeterMax: '',
        leaseMin: '',
        leaseMax: '',
        totalSpaceMin: '',
        totalSpaceMax: '',
        retailSpaceMin: '',
        retailSpaceMax: '',
        subsidarySpaceMin: '',
        subsidarySpaceMax: '',
        usableSpaceMin: '',
        usableSpaceMax: '',
        storageSpaceMin: '',
        storageSpaceMax: '',
        outdoorSpaceRequired: false,
        outdoorSpaceMin: '',
        outdoorSpaceMax: '',
        roofingRequired: false,
        showroomRequired: false,
        shopWindowRequired: false,
        shopWindowLengthMin: '',
        shopWindowLengthMax: '',
        numberOfParkingLots: '',
        roomCount: '',
        barrierFreeAccessRequired: false,
        groundLevelSalesAreaRequired: false,
        shopWidthMax: '',
        shopWidthMin: '',
        earliestAvailableFrom: null,
        latestAvailableFrom: null
    };
    const initialReporterFormData: ReporterFormData = {
        companyName: '',
        salutation: '',
        name: '',
        firstName: '',
        email: '',
        phoneNumber: '',
        privacyPolicyAccept: false,
    };

    const [basicInformationFormData, setBasicInformationFormData] = useState<BasicInformationFormData>(initialBasicInformationFormData);
    const [locationRequirementFormData, setLocationRequirementFormData] = useState<LocationRequirementFormData>(initialLocationRequirementFormData);
    const [propertyRequirementFormData, setPropertyRequirementFormData] = useState<PropertyRequirementFormData>(initialPropertyRequirementFormData);
    const [reporterFormData, setReporterFormData] = useState<ReporterFormData>(initialReporterFormData);

    useEffect(() => {
        fetchConfiguration();
    }, []);

    useEffect(() => {
        if (config != undefined) {
            setCurrentStepNumber(1);
        }
    }, [config]);

    useEffect(() => {
        updateProgressBar();

        if (currentStepNumber > 1 && currentStepNumber < 5) {
            progressBar.current.parentElement.parentElement.scrollIntoView({behavior: 'smooth', block: 'start'});
        }
    }, [currentStepNumber]);

    const fetchConfiguration = async (): Promise<void> => {
        const response = await Axios.get(apiUrl + '/api/looking-for-property-request-reporter/configurations/' + accountKey);
        initializeApi(response);
    };

    const initializeApi = (response: AxiosResponse): void => {
        initializeStyles(response);
        initializeStep(response);
    };

    const initializeStyles = (response: AxiosResponse): void => {
        apiContainer.current.style.setProperty('--main-bg-color', response.data.configuration.firstColor);
        apiContainer.current.style.setProperty('--main-font-color', response.data.configuration.firstFontColor);
        apiContainer.current.style.setProperty('--second-bg-color', response.data.configuration.secondaryColor);
        apiContainer.current.style.setProperty('--second-font-color', response.data.configuration.secondaryFontColor);
        setTheme(createTheme({
            palette: {
                primary: {
                    main: response.data.configuration.firstColor,
                },
                secondary: {
                    main: response.data.configuration.secondaryColor,
                },
            }
        }));
    };

    const initializeStep = async (response: AxiosResponse) => {
        await fetchQuarterList(response.data.places[0].uuid);
        setPropertyUsageList(response.data.configuration.industryClassifications);
        setPlaceUuids([response.data.places[0].uuid]);
        setConfig(response.data.configuration);

    };

    const fetchQuarterList = async (placeUuid: string): Promise<void> => {
        const response = await Axios.get(apiUrl + '/api/location/places/' + placeUuid + '/city-quarters');
        setQuarterOptions(response.data);
    };

    const gotoNextStep = (): void => {
        if (validateFormDataStep() === true) {
            if (currentStepNumber < 5) {
                setCurrentStepNumber(currentStepNumber + 1);
            }
        }
    };

    const gotoPreviousStep = (): void => {
        if (currentStepNumber > 1) {
            setCurrentStepNumber(currentStepNumber - 1);
        }
    };

    const showStep = (): JSX.Element => {
        switch (currentStepNumber) {
            case 0:
                return <div className={[bootstrapStyle['row'], bootstrapStyle['justify-content-center'], bootstrapStyle['text-center'],
                                        bootstrapStyle['m-5']].join(' ')}>
                    <div className={[bootstrapStyle['col-6'], bootstrapStyle['col-md-3'], bootstrapStyle['justify-content-center'], bootstrapStyle['text-center'],
                        bootstrapStyle['m-5']].join(' ')}>
                        <img className={bootstrapStyle['col-12']} src={LoadingImage} alt="Loading..."/>
                    </div>
                    <p className={bootstrapStyle['mt-3']}>Gesuchsmelder wird vorbereitet... </p>
                </div>;
            case 1:
                return <BasicInformationStep
                    basicInformationFormData={basicInformationFormData}
                    setBasicInformationFormData={(name: string, value: string | boolean | []) => {
                        setBasicInformationFormData(prevState => {
                            return {...prevState, [name]: value};
                        });
                    }}
                    requiredBasicInformationFormFields={config.requiredBasicInformationFormFields}
                    industryClassifications={propertyUsageList}
                    setHasIndustrySectorClassificationOptions={setHasIndustrySectorClassificationOptions}
                    setHasIndustryDetailsClassificationOptions={setHasIndustryDetailsClassificationOptions}
                    apiUrl={apiUrl}
                    errors={errors}
                />;
            case 2:
                return <LocationRequirementStep
                    locationRequirementFormData={locationRequirementFormData}
                    setLocationRequirementFormData={(name: string, value: string | boolean | []) => {
                        setLocationRequirementFormData(prevState => {
                            return {...prevState, [name]: value};
                        });
                    }}
                    requiredLocationFormFields={config.requiredLocationFormFields}
                    quarterOptions={quarterOptions}
                    errors={errors}
                    showFieldDependentOnSelectedIndustryClassificationId={showFieldDependentOnSelectedIndustryClassificationId}
                />;
            case 3:
                return <PropertyRequirementStep
                    propertyRequirementFormData={propertyRequirementFormData}
                    setPropertyRequirementFormData={(name: string, value: string | boolean | []) => {
                        setPropertyRequirementFormData(prevState => {
                            return {...prevState, [name]: value};
                        });
                    }}
                    requiredPropertyInformationFormFields={config.requiredPropertyInformationFormFields}
                    errors={errors}
                    showFieldDependentOnSelectedIndustryClassificationId={showFieldDependentOnSelectedIndustryClassificationId}
                />;
            case 4:
                return <ReporterStep
                    reporterFormData={reporterFormData}
                    setReporterFormData={(name: string, value: string | boolean | []) => {
                        setReporterFormData(prevState => {
                            return {...prevState, [name]: value};
                        });
                    }}
                    requiredReporterFormFields={config.requiredReporterFormFields}
                    privacyPolicy={config.privacyPolicy}
                    errors={errors}
                />;
            case 5:
                return <ReportResponseStep newReport={() => setCurrentStepNumber(1)} isResponseSuccessful={isResponseSuccessful}/>;
        }
    };

    const showFieldDependentOnSelectedIndustryClassificationId = (fieldName: string): boolean => {
        const industryClassificationId: number = parseInt(basicInformationFormData.industryClassificationId);

        const retailIndustry: IndustryClassification = propertyUsageList.find((element) => {
          return element.levelOne == 1;
        })
        const gastronomyIndustry: IndustryClassification = propertyUsageList.find((element) => {
            return element.levelOne == 3;
        })
        const educationIndustry: IndustryClassification = propertyUsageList.find((element) => {
            return element.levelOne == 6;
        })
        const socialIndustry: IndustryClassification = propertyUsageList.find((element) => {
            return element.levelOne == 8;
        })
        const churchIndustry: IndustryClassification = propertyUsageList.find((element) => {
            return element.levelOne == 10;
        })
        const productionIndustry: IndustryClassification = propertyUsageList.find((element) => {
            return element.levelOne == 11;
        })
        const creativeIndustry: IndustryClassification = propertyUsageList.find((element) => {
            return element.levelOne == 12;
        })
        const wholesaleIndustry: IndustryClassification = propertyUsageList.find((element) => {
            return element.levelOne == 13;
        })

        if (fieldName === PropertyRequirementFieldName.RetailSpaceMax || fieldName === PropertyRequirementFieldName.ShopWidthMax) {
            const industryFields: number[] = [];

            if (retailIndustry !== undefined) {
                industryFields.push(retailIndustry.id);
            }

            if (wholesaleIndustry !== undefined) {
                industryFields.push(wholesaleIndustry.id);
            }
            return industryFields.includes(industryClassificationId);

        } else if (fieldName === PropertyRequirementFieldName.SubsidarySpaceMax || fieldName === PropertyRequirementFieldName.UsableSpaceMax) {
            const industryFields: number[] = [];

            if (retailIndustry !== undefined) {
                industryFields.push(retailIndustry.id);
            }

            if (gastronomyIndustry !== undefined) {
                industryFields.push(gastronomyIndustry.id);
            }

            if (educationIndustry !== undefined) {
                industryFields.push(educationIndustry.id);
            }

            if (socialIndustry !== undefined) {
                industryFields.push(socialIndustry.id);
            }

            if (productionIndustry !== undefined) {
                industryFields.push(productionIndustry.id);
            }

            if (creativeIndustry !== undefined) {
                industryFields.push(creativeIndustry.id);
            }

            if (wholesaleIndustry !== undefined) {
                industryFields.push(wholesaleIndustry.id);
            }
            return industryFields.includes(industryClassificationId);

        } else if (fieldName === PropertyRequirementFieldName.StorageSpaceMax) {
            const industryFields: number[] = [];

            if (retailIndustry !== undefined) {
                industryFields.push(retailIndustry.id);
            }

            if (gastronomyIndustry !== undefined) {
                industryFields.push(gastronomyIndustry.id);
            }
            if (churchIndustry !== undefined) {
                industryFields.push(churchIndustry.id);
            }

            if (creativeIndustry !== undefined) {
                industryFields.push(creativeIndustry.id);
            }

            if (wholesaleIndustry !== undefined) {
                industryFields.push(wholesaleIndustry.id);
            }
            return industryFields.includes(industryClassificationId);

        } else if (fieldName === LocationRequirementFieldName.LocationCategories) {
            return true;
        }
        return true;
    };

    const validateEmail = (mail: string): boolean => {
        return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,})+$/.test(mail);
    };

    const validatePhoneNumber = (phone: string): boolean => {
        return /(?:\(\+?\d+\)|\+?\d+)(?:\s*[\-\/]*\s*\d+)+/.test(phone);
    };

    const checkRequired = (fieldName: string): boolean => {
        let fieldId: number;
        switch (currentStepNumber) {
            case 1:
                fieldId = BasicInformationFieldNameKey[fieldName as keyof typeof BasicInformationFieldNameKey];
                return config.requiredBasicInformationFormFields.includes(fieldId);
            case 2:
                fieldId = LocationRequirementFieldNameKey[fieldName as keyof typeof LocationRequirementFieldNameKey];
                return config.requiredLocationFormFields.includes(fieldId);
            case 3:
                fieldId = PropertyRequirementFieldNameKey[fieldName as keyof typeof PropertyRequirementFieldNameKey];
                return config.requiredPropertyInformationFormFields.includes(fieldId);
            case 4:
                fieldId = ReporterFieldNameKey[fieldName as keyof typeof ReporterFieldNameKey];
                return config.requiredReporterFormFields.includes(fieldId);
        }
    };

    const checkRequiredDependedOnShownField = (fieldName: string): boolean => {

        if (fieldName === BasicInformationFieldName.IndustrySectorIndustryClassificationId && hasIndustrySectorClassificationOptions === false
            || fieldName === BasicInformationFieldName.IndustryDetailsIndustryClassificationId && hasIndustryDetailsClassificationOptions === false) {
            return false;
        }

        if ((fieldName === PropertyRequirementFieldName.PurchasePriceGrossMax
            || fieldName === PropertyRequirementFieldName.PurchasePriceNetMax
            || fieldName === PropertyRequirementFieldName.PurchasePricePerSquareMeterMax
        ) && propertyRequirementFormData.propertyOfferTypes.buy === false) {
            return false;
        }

        if ((fieldName === PropertyRequirementFieldName.NetColdRentMax
            || fieldName === PropertyRequirementFieldName.ColdRentMax
            || fieldName === PropertyRequirementFieldName.RentalPricePerSquareMeterMax
        ) && propertyRequirementFormData.propertyOfferTypes.rent === false) {
            return false;
        }

        if (fieldName === PropertyRequirementFieldName.LeaseMax && propertyRequirementFormData.propertyOfferTypes.lease === false) {
            return false;
        }

        if (fieldName === PropertyRequirementFieldName.OutdoorSpaceMax && propertyRequirementFormData.outdoorSpaceRequired === false) {
            return false;
        }

        if (fieldName === PropertyRequirementFieldName.ShopWindowLengthMax && propertyRequirementFormData.shopWindowRequired === false) {
            return false;
        }

        return true;
    }

    const validateFrom = (formData: any): boolean => {
        let validationErrors: { [key: string]: string } = {};

        const validationResults = Object.keys(formData).map(fieldName => {
            const value = formData[fieldName];
            let required = checkRequired(fieldName);

            if (required
                && (parseFloat(value) < 0 || value === '' || value === null || value.length <= 0)
                && checkRequiredDependedOnShownField(fieldName)
                && showFieldDependentOnSelectedIndustryClassificationId(fieldName)
            ) {
                validationErrors[fieldName] = 'Dieses Feld ist ein Pflichtfeld';
                return false;
            }

            if (fieldName === ReporterFieldName.Email) {
                if (validateEmail(value.toString()) === false) {
                    validationErrors[fieldName] = 'E-Mail hat nicht das richtige Format';
                    return false;
                }
            }

            if (required && fieldName === ReporterFieldName.PhoneNumber) {
                if (validatePhoneNumber(value.toString()) === false || value.length > 30) {
                    validationErrors[fieldName] = 'Telefonnummer hat nicht das richtige Format';
                    return false;
                }
            }

            if (required
                && (fieldName === BasicInformationFieldName.RequestAvailableTill
                    || fieldName === PropertyRequirementFieldName.LatestAvailableFrom
                    || fieldName === PropertyRequirementFieldName.EarliestAvailableFrom)
            ) {
                if (dayjs(value).isValid() === false) {
                    validationErrors[fieldName] = 'Datum hat nicht das richtige Format';
                    return false;
                }
            }

            if (fieldName === ReporterFieldName.PrivacyPolicyAccept && value === false) {
                validationErrors[fieldName] = 'Bitte stimmen Sie den Datenschutzbestimmungen zu';
                return false;
            }

            if (fieldName === PropertyRequirementFieldName.PropertyOfferTypes) {
                if (value.buy === false && value.rent === false && value.lease === false) {
                    validationErrors[fieldName] = 'Es muss mindestens ein Angebotstyp ausgewählt werden';
                    return false;
                }
            }

            return true;
        });

        setErrors(validationErrors);
        return !validationResults.includes(false);
    };

    const validateFormDataStep = (): boolean => {
        let isValid: boolean = true;
        switch (currentStepNumber) {
            case 1:
                isValid = validateFrom(basicInformationFormData);
                break;
            case 2:
                isValid = validateFrom(locationRequirementFormData);
                break;
            case 3:
                isValid = validateFrom(propertyRequirementFormData);
                break;
            case 4:
                isValid = validateFrom(reporterFormData);
                break;
        }
        return isValid;
    };

    const prepareFormDataToSend = (): { [key: string]: any } => {
        let form: { [key: string]: any } = {};
        form['accountKey'] = accountKey;

        Object.assign(form['basicInformation'] = {}, basicInformationFormData);
        Object.keys(form['basicInformation']).forEach(key => {

            if (form['basicInformation'][key] === '' || (Array.isArray(form['basicInformation'][key]) && form['basicInformation'][key].length === 0)) {
                form['basicInformation'][key] = null;

            } else if (isNaN(parseInt(form['basicInformation'][key])) === false
                && key !== BasicInformationFieldName.RequestAvailableTill
                && key != BasicInformationFieldName.Title
                && key != BasicInformationFieldName.ConceptDescription
            ) {
                if (['industryClassificationId', 'industrySectorIndustryClassificationId', 'industryDetailsIndustryClassificationId'].includes(key)) {
                    let industryClassificationId = basicInformationFormData.industryDetailsIndustryClassificationId ?
                                                   basicInformationFormData.industryDetailsIndustryClassificationId :
                                                   basicInformationFormData.industrySectorIndustryClassificationId ?
                                                   basicInformationFormData.industrySectorIndustryClassificationId :
                                                   basicInformationFormData.industryClassificationId;
                    form['basicInformation']['industryClassificationId'] = parseInt(industryClassificationId);
                } else {
                    form['basicInformation'][key] = parseInt(form['basicInformation'][key]);
                }
            }

        });

        Object.assign(form['locationRequirement'] = {}, locationRequirementFormData);
        Object.keys(form['locationRequirement']).forEach(key => {

            if (form['locationRequirement'][key] === '' || (Array.isArray(form['locationRequirement'][key]) && form['locationRequirement'][key].length === 0)) {
                form['locationRequirement'][key] = null;
            }

        });
        form['locationRequirement']['placeUuids'] = placeUuids;

        Object.assign(form['propertyRequirement'] = {}, propertyRequirementFormData);
        let propertyOfferTypes: number[] = [];
        Object.keys(form['propertyRequirement']).forEach(key => {

            if (form['propertyRequirement'][key] === '' || (Array.isArray(form['propertyRequirement'][key]) && form['propertyRequirement'][key].length === 0)) {
                form['propertyRequirement'][key] = null;

            } else if (isNaN(parseFloat(form['propertyRequirement'][key])) === false
                && (key !== PropertyRequirementFieldName.EarliestAvailableFrom && key !== PropertyRequirementFieldName.LatestAvailableFrom)
            ) {
                form['propertyRequirement'][key] = parseFloat(form['propertyRequirement'][key]);
            }

            if (key === PropertyRequirementFieldName.PropertyOfferTypes) {

                if (form['propertyRequirement'][key].buy === true) {
                    propertyOfferTypes.push(0);
                }

                if (form['propertyRequirement'][key].rent === true) {
                    propertyOfferTypes.push(1);
                }

                if (form['propertyRequirement'][key].lease === true) {
                    propertyOfferTypes.push(2);
                }

                form['propertyRequirement'][PropertyRequirementFieldName.PropertyOfferTypes] = propertyOfferTypes;
            }
        });

        Object.assign(form['reporter'] = {}, reporterFormData);
        Object.keys(form['reporter']).forEach(key => {

            if (form['reporter'][key] === '' || (Array.isArray(form['reporter'][key]) && form['reporter'][key].length === 0)) {
                form['reporter'][key] = null;

            } else if (key === ReporterFieldName.Salutation) {
                form['reporter'][key] = parseInt(form['reporter'][key]);
            }

        });

        return form;
    };

    const sendReport = async () => {
        if (validateFormDataStep() === true) {
            let formData = prepareFormDataToSend();
            setOnReportSend(true);

            try {
                const response = await Axios.post(apiUrl + '/api/looking-for-property-request-reporter/reports', formData);

                if (response.status === 200) {
                    setIsResponseSuccessful(true);
                    gotoNextStep();
                    resetReportValues();
                }

            } catch (error) {
                setIsResponseSuccessful(false);
                gotoNextStep();
                resetReportValues();
            }
        }
    };

    const resetReportValues = (): void => {
        setOnReportSend(false);
        setBasicInformationFormData(initialBasicInformationFormData);
        setLocationRequirementFormData(initialLocationRequirementFormData);
        setPropertyRequirementFormData(initialPropertyRequirementFormData);
        setReporterFormData(initialReporterFormData);
    };

    const updateProgressBar = (): void => {
        let progress = progressBar.current;

        if (progress != null) {
            let value = ((currentStepNumber / 4) * 100);
            progress.style.width = value.toString() + '%';
        }
    };

    const getHeadlineText = (): string => {
        switch (currentStepNumber) {
            case 1:
                return 'Grundinformationen';
            case 2:
                return 'Anforderungen an die Lage';
            case 3:
                return 'Anforderungen an das Objekt/die Fläche';
            case 4:
                return 'Persönliche Angaben';
            default:
                return '';
        }
    };

    let headlineText: string = getHeadlineText();

    return (
        <ThemeProvider theme={theme}>
            <div ref={apiContainer} className={[apiStyle['main-container'], bootstrapStyle['container']].join(' ')}>
                <div className={bootstrapStyle['mb-4']}>
                    {currentStepNumber < 2 && currentStepNumber > 0 &&
                    <>
                        <p className={apiStyle.headline}>{config.title}</p>
                        <p className={apiStyle.content}>{config.text}</p>
                    </>
                    }
                </div>

                {headlineText != '' &&
                <div className={[bootstrapStyle['text-center'], bootstrapStyle['text-md-start'], bootstrapStyle['px-2'], bootstrapStyle['mb-4']].join(' ')}>
                    <div className={[apiStyle['headline-2'], bootstrapStyle['d-md-flex']].join(' ')}>
                        <span className={bootstrapStyle['mx-3']}>{currentStepNumber} <span style={{fontSize: '14px'}}>/ 4</span></span>
                        <span>{headlineText}</span>
                    </div>
                    <div className={[apiStyle['progress'], bootstrapStyle['progress']].join(' ')}>
                        <div ref={progressBar} className={[bootstrapStyle['progress-bar'], bootstrapStyle['progress-bar-striped']].join(' ')}
                             role="progressbar">
                        </div>
                    </div>
                </div>
                }
                <div>
                    {showStep()}
                </div>
                <div className={[bootstrapStyle['container'], bootstrapStyle['p-2'], bootstrapStyle['mt-4']].join(' ')}>
                    <div className={bootstrapStyle['row']}>
                        <div className={[bootstrapStyle['col-md-6'], bootstrapStyle['mb-3'], bootstrapStyle['text-start']].join(' ')}>
                            {currentStepNumber > 0 &&
                            <small>
                                Die mit * gekennzeichneten Felder sind Pflichtangaben</small>
                            }
                        </div>
                        <div className={[bootstrapStyle['col-md-6'], bootstrapStyle['text-end'], bootstrapStyle['justify-content-end']].join(' ')}>
                            {currentStepNumber > 1 &&
                            currentStepNumber < 5 &&
                            onReportSend === false &&
                            <Button
                                className={[bootstrapStyle['col-6'], bootstrapStyle['col-md-4'], bootstrapStyle['col-lg-3'], bootstrapStyle['mx-md-3']].join(' ')}
                                variant="outlined" color="secondary" size="medium" onClick={gotoPreviousStep}>Zurück</Button>

                            }
                            {currentStepNumber > 0 &&
                            currentStepNumber < 4 &&
                            <Button
                                className={[bootstrapStyle['col-6'], bootstrapStyle['col-md-4'], bootstrapStyle['col-lg-3'], bootstrapStyle['mx-md-3']].join(' ')}
                                variant="contained" color="secondary" size="medium" onClick={gotoNextStep}>Weiter</Button>
                            }
                            {currentStepNumber === 4 &&
                            <>
                                {onReportSend === false ?
                                 <Button className={[bootstrapStyle['col-6'], bootstrapStyle['col-md-4'], bootstrapStyle['col-lg-3'],
                                                     bootstrapStyle['mx-md-3']].join(' ')} variant="contained" color="secondary" size="medium"
                                         onClick={sendReport}>Absenden</Button>
                                :
                                 <Button className={[bootstrapStyle['col-6'], bootstrapStyle['col-md-4'], bootstrapStyle['col-lg-3'],
                                                     bootstrapStyle['mx-md-3']].join(' ')} variant="contained" color="secondary" size="medium" onClick={sendReport}>
                                     <div className={bootstrapStyle['spinner-border']} role="status"><span className="sr-only"/></div>
                                 </Button>
                                }
                            </>
                            }
                        </div>
                    </div>
                </div>
            </div>
        </ThemeProvider>
    );
}
