interface FormData {
    name: string;
    value: string | boolean | number | string[];
    isValid: boolean;
}

interface IndustryClassification {
    id: number;
    name: string;
    levelOne: number;
}

interface Option {
    label: string;
    value: string | number;
}

interface Place {
    uuid: string;
    placeName: string;
    placeShortName?: string;
    placeType: number;
}

export { FormData, IndustryClassification, Option, Place };