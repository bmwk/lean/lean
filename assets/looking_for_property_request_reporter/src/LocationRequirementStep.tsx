import bootstrapStyle from './Bootstrap.module.scss';
import AutocompleteMultipleSelectField from '../../components/src/Form/AutocompleteMultipleSelectField';
import TextareaField from '../../components/src/Form/TextareaField';
import { LocationRequirementField, LocationRequirementFieldName, LocationRequirementFormData } from './LocationRequirementField';
import { Place, Option } from './InterfaceLibrary';
import React from 'react';

interface LocationRequirementStep {
    locationRequirementFormData: LocationRequirementFormData;
    setLocationRequirementFormData: Function;
    requiredLocationFormFields: LocationRequirementField[];
    quarterOptions: Place[];
    errors: {[key: string]: string};
    showFieldDependentOnSelectedIndustryClassificationId: Function;
}

export default function LocationRequirementStep({locationRequirementFormData, setLocationRequirementFormData, requiredLocationFormFields, quarterOptions, errors, showFieldDependentOnSelectedIndustryClassificationId}: LocationRequirementStep) {
    const showLocationCategoriesField: boolean = showFieldDependentOnSelectedIndustryClassificationId(LocationRequirementFieldName.LocationCategories);

    const checkRequired = (locationRequirementField: LocationRequirementField): boolean => {
        return requiredLocationFormFields.includes(locationRequirementField);
    }

    const handleInputChange = (name: string, value: string | boolean): void => {
        setLocationRequirementFormData(name, value);
    }

    const locationCategoryOptions: Option[] = [
        {label: '1A-Lage', value: 0},
        {label: '1B-Lage', value: 1},
        {label: '2A-Lage', value: 2},
        {label: '2B-Lage', value: 3},
        {label: '1C-Lage', value: 5},
        {label: '2C-Lage', value: 6},
        {label: 'Sonstige', value: 4}
    ];

    const locationfactorOptions: Option[] = [
        {label: 'Ärztehaus', value: 0},
        {label: 'Autobahnnähe', value: 1},
        {label: 'Bahnhof', value: 2},
        {label: 'Baumärkte', value: 3},
        {label: 'Ecklage', value: 4},
        {label: 'Ein-/Ausfallstraßen; Ringstraßen', value: 5},
        {label: 'Einkaufsstraßen', value: 6},
        {label: 'Einkaufszentrum/Mall', value: 7},
        {label: 'Fachmarktzentrum', value: 8},
        {label: 'Flughafen', value: 9},
        {label: 'Fußgängerzone', value: 10},
        {label: 'Gewerbegebiet', value: 11},
        {label: 'Großstadtlage', value: 12},
        {label: 'Haltestellen', value: 13},
        {label: 'Hauptstraße', value: 14},
        {label: 'Hochfrequentierte Verkehrslage', value: 15},
        {label: 'Innenstadt', value: 16},
        {label: 'LKW-Anliefermöglichkeit', value: 17},
        {label: 'Marktplatz', value: 18},
        {label: 'Nahversorgungszentrum', value: 19},
        {label: 'Nebenstraße', value: 20},
        {label: 'ÖPNV-Anschluss', value: 21},
        {label: 'SB-Warenhäuser', value: 22},
        {label: 'Schulnähe', value: 23},
        {label: 'Stadtteillage', value: 24},
        {label: 'Touristische Plätze', value: 25},
        {label: 'Vorkassenzonen', value: 26},
        {label: 'Wohngebietsnähe', value: 27},
    ];

    return (
        <>
            <div className={bootstrapStyle['row']}>
                <div className={[bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['mb-4']].join(' ')}>
                    <AutocompleteMultipleSelectField
                        label={'Stadtteile'}
                        name={LocationRequirementFieldName.QuarterPlaceUuids}
                        isRequired={checkRequired(LocationRequirementField.QuarterPlaceUuids)}
                        options={quarterOptions.map((quarter: Place) => ({label: quarter.placeName, value: quarter.uuid}))}
                        selectedOptionIds={locationRequirementFormData.quarterPlaceUuids}
                        handleInputChange={handleInputChange}
                        error={errors[LocationRequirementFieldName.QuarterPlaceUuids]}
                    />
                </div>
                {showLocationCategoriesField === true &&
                    <div className={[bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['mb-4']].join(' ')}>
                        <AutocompleteMultipleSelectField
                            label={'Lagekategorie'}
                            name={LocationRequirementFieldName.LocationCategories}
                            isRequired={checkRequired(LocationRequirementField.LocationCategories)}
                            options={locationCategoryOptions}
                            selectedOptionIds={locationRequirementFormData.locationCategories}
                            handleInputChange={handleInputChange}
                            error={errors[LocationRequirementFieldName.LocationCategories]}
                        />
                    </div>
                }
                <div className={[bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['mb-4']].join(' ')}>
                    <AutocompleteMultipleSelectField
                        label={'Standortfaktoren'}
                        name={LocationRequirementFieldName.LocationFactors}
                        isRequired={checkRequired(LocationRequirementField.LocationFactors)}
                        options={locationfactorOptions}
                        selectedOptionIds={locationRequirementFormData.locationFactors}
                        handleInputChange={handleInputChange}
                        error={errors[LocationRequirementFieldName.LocationFactors]}
                    />
                </div>

                <div className={[bootstrapStyle['col-12'], showLocationCategoriesField === true && bootstrapStyle['col-md-6'], bootstrapStyle['mb-4']].join(' ')}>
                    <TextareaField
                        name={LocationRequirementFieldName.OtherRequirementsForTheLocation}
                        label={'Weitere Anforderungen an Standort'}
                        handleInputChange={handleInputChange}
                        isRequired={checkRequired(LocationRequirementField.OtherRequirementsForTheLocation)}
                        value={''}
                        error={errors[LocationRequirementFieldName.OtherRequirementsForTheLocation]}
                    />
                </div>
            </div>
        </>
    );
}
