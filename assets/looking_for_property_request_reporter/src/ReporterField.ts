interface ReporterFormData {
    salutation: string;
    name: string;
    firstName: string;
    email: string;
    phoneNumber: string;
    privacyPolicyAccept: boolean;
    companyName: string;
}

enum ReporterField {
    Salutation ,
    Name ,
    FirstName ,
    Email ,
    PhoneNumber ,
    PrivacyPolicyAccept ,
    CompanyName ,
}

enum ReporterFieldName {
    Salutation = 'salutation',
    Name = 'name',
    FirstName = 'firstName',
    Email = 'email',
    PhoneNumber = 'phoneNumber',
    PrivacyPolicyAccept = 'privacyPolicyAccept',
    CompanyName = 'companyName',
}

enum ReporterFieldNameKey {
    'salutation',
    'name' ,
    'firstName' ,
    'email' ,
    'phoneNumber' ,
    'privacyPolicyAccept',
    'companyName' ,
}

export { ReporterFormData, ReporterField, ReporterFieldName, ReporterFieldNameKey };
