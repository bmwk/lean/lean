interface BasicInformationFormData {
    title: string;
    requestReason: string;
    industryClassificationId: string;
    industrySectorIndustryClassificationId: string;
    industryDetailsIndustryClassificationId: string;
    conceptDescription: string;
    requestAvailableTill: string;
    automaticEnding: boolean;
}

enum BasicInformationField {
    Title ,
    RequestReason ,
    IndustryClassificationId ,
    IndustrySectorIndustryClassificationId ,
    IndustryDetailsIndustryClassificationId ,
    ConceptDescription ,
    RequestAvailableTill,
    AutomaticEnding,
}

enum BasicInformationFieldName {
    Title = 'title',
    RequestReason = 'requestReason',
    IndustryClassificationId = 'industryClassificationId',
    IndustrySectorIndustryClassificationId = 'industrySectorIndustryClassificationId',
    IndustryDetailsIndustryClassificationId = 'industryDetailsIndustryClassificationId',
    ConceptDescription = 'conceptDescription',
    RequestAvailableTill = 'requestAvailableTill',
    AutomaticEnding = 'automaticEnding',
}

enum BasicInformationFieldNameKey {
    'title' ,
    'requestReason' ,
    'industryClassificationId',
    'industrySectorIndustryClassificationId',
    'industryDetailsIndustryClassificationId' ,
    'conceptDescription' ,
    'requestAvailableTill',
    'automaticEnding',
}

export { BasicInformationFormData, BasicInformationField, BasicInformationFieldName, BasicInformationFieldNameKey };
