import bootstrapStyle from './Bootstrap.module.scss';
import SelectField from '../../components/src/Form/SelectField';
import InputField from '../../components/src/Form/InputField';
import CheckboxField from '../../components/src/Form/CheckboxField';
import ModalField from '../../components/src/Form/ModalField';
import { ReporterField, ReporterFieldName, ReporterFormData } from './ReporterField';
import React from 'react';

interface ReporterStepProps {
    reporterFormData: ReporterFormData;
    setReporterFormData: Function;
    requiredReporterFormFields: ReporterField[];
    privacyPolicy: string;
    errors: {[key: string]: string};
}

export default function ReporterStep({ reporterFormData, setReporterFormData, requiredReporterFormFields, privacyPolicy, errors }: ReporterStepProps) {

    const checkRequired = (reporterField: ReporterField): boolean => {
        return requiredReporterFormFields.includes(reporterField);
    }

    const handleInputChange = (name: string, value: string | boolean): void => {
        setReporterFormData(name, value);
    }

    const setDsgvoConfirm = async () => {
        setReporterFormData('privacyPolicyAccept', true);
    }

    return (
        <>
            <div className={bootstrapStyle['row']}>
                <div className={[bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['mb-4']].join(' ')}>
                    <InputField
                        label={'Unternehmen'}
                        type={'text'}
                        name={ReporterFieldName.CompanyName}
                        value={reporterFormData.companyName}
                        isRequired={checkRequired(ReporterField.CompanyName)}
                        handleInputChange={handleInputChange}
                        error={errors[ReporterFieldName.CompanyName]}
                    />
                </div>

                <div className={[bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['mb-4']].join(' ')}>
                    <SelectField
                        label={'Anrede'}
                        name={ReporterFieldName.Salutation}
                        value={reporterFormData.salutation}
                        isRequired={checkRequired(ReporterField.Salutation)}
                        handleInputChange={handleInputChange}
                        options={[
                            {label: 'Herr', value: 0,},
                            {label: 'Frau', value: 1},
                            {label: 'Divers', value: 2}
                        ]}
                        error={errors[ReporterFieldName.Salutation]}
                    />
                </div>
            </div>

            <div className={bootstrapStyle['row']}>
                <div className={[bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['mb-4']].join(' ')}>
                    <InputField
                        label={'Vorname'}
                        type={'text'}
                        name={ReporterFieldName.FirstName}
                        value={reporterFormData.firstName}
                        isRequired={checkRequired(ReporterField.FirstName)}
                        handleInputChange={handleInputChange}
                        error={errors[ReporterFieldName.FirstName]}
                    />
                </div>

                <div className={[bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['mb-4']].join(' ')}>
                    <InputField
                        label={'Nachname'}
                        type={'text'}
                        name={ReporterFieldName.Name}
                        value={reporterFormData.name}
                        isRequired={checkRequired(ReporterField.Name)}
                        handleInputChange={handleInputChange}
                        error={errors[ReporterFieldName.Name]}
                    />
                </div>
            </div>

            <div className={bootstrapStyle['row']}>
                <div className={[bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['mb-4']].join(' ')}>
                    <InputField
                        label={'Telefon'}
                        type={'text'}
                        name={ReporterFieldName.PhoneNumber}
                        value={reporterFormData.phoneNumber}
                        isRequired={checkRequired(ReporterField.PhoneNumber)}
                        handleInputChange={handleInputChange}
                        error={errors[ReporterFieldName.PhoneNumber]}
                    />
                </div>

                <div className={[bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['mb-4']].join(' ')}>
                    <InputField
                        label={'E-Mail-Adresse'}
                        type={'email'}
                        name={ReporterFieldName.Email}
                        value={reporterFormData.email}
                        isRequired={checkRequired(ReporterField.Email)}
                        handleInputChange={handleInputChange}
                        error={errors[ReporterFieldName.Email]}
                    />
                </div>
            </div>

            <div className={bootstrapStyle['row']}>
                <div className={[ bootstrapStyle['col-12'] ].join(' ')}>
                    <CheckboxField
                        name={ReporterFieldName.PrivacyPolicyAccept}
                        checked={reporterFormData.privacyPolicyAccept}
                        error={errors[ReporterFieldName.PrivacyPolicyAccept]}
                        label={'* Mit Absenden des Kontaktformulars werden Ihre Angaben zum Objekt und Ihre personenbezogenen Daten auf elektronischem Weg an den zuständigen Sachbearbeiter übermittelt. Gleichzeitig stimmen Sie zu, dass diese gemäß unserer Datenschutzbestimmungen zur Bearbeitung Ihrer Meldung erhoben und verarbeitet werden dürfen.'}
                        isRequired={true}
                        handleInputChange={handleInputChange}
                        isFontSmall={true}
                    />

                    <div className={[ bootstrapStyle['mt-2'], bootstrapStyle['mt-md-0'], bootstrapStyle['px-0'], bootstrapStyle['px-md-4'], bootstrapStyle['text-md-end'], bootstrapStyle['text-center'] ].join(' ')}>
                        <ModalField name={'dsgvoModal'} buttonLabel={'Informationen zum Datenschutz'} title={'Nutzungsbeschreibung und Datenschutzerklärung'} text={privacyPolicy} submitModal={setDsgvoConfirm} submitModalButtonText={'Akzeptieren'}/>
                    </div>
                </div>
            </div>
        </>
    );
}

