import 'swagger-ui-dist/swagger-ui.css';
import SwaggerUIBundle from 'swagger-ui-dist/swagger-ui-bundle.js';

SwaggerUIBundle({
    url: '/dokumentation/api/settlement-concept.yaml',
    dom_id: '#swagger-ui',
});
