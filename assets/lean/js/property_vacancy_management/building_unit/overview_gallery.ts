import '../../../styles/property/building_unit_overview_gallery.scss';
import '../../../styles/property/building_unit_overview_filter_form.scss';
import { OverviewGalleryPage } from '../../src/PropertyVacancyManagement/Page/BuildingUnit/OverviewGalleryPage';

const overviewGalleryPage: OverviewGalleryPage = new OverviewGalleryPage();
