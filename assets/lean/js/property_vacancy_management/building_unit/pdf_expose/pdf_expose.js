const puppeteer = require('puppeteer');
const fs = require('fs');
const path = require('path');

(async () => {
    const browser = await puppeteer.launch({
        headless: true,
        executablePath: '/usr/bin/chromium-browser',
        args: ['--no-sandbox', '--disable-setuid-sandbox','--font-render-hinting=none']
    });

    const [, , schemeAndHostname, pdfHeaderUrlPath, pdfContentUrlPath] = process.argv;

    const headerTemplate = await browser.newPage();

    await headerTemplate.goto(schemeAndHostname + pdfHeaderUrlPath,  {
        waitUntil: 'networkidle0',
        timeout: 120000
    });

    await headerTemplate.waitForSelector('#header_template', {
        visible: true,
        timeout: 120000
    });

    await headerTemplate.emulateMediaType('screen');

    let headerContent = await headerTemplate.evaluate(() => document.querySelector('*').innerHTML);

    const page = await browser.newPage();

    await page.goto(schemeAndHostname + pdfContentUrlPath, {
        waitUntil: 'networkidle0',
        timeout: 120000
    });

    let divSelectorToRemove = '.map-data';

    await page.evaluate((sel) => {
        let elements = document.querySelectorAll(sel);

        elements.forEach(element => {
            element.parentNode.removeChild(element);
        });
    }, divSelectorToRemove);

    await page.waitForSelector('#page-loaded', {
        timeout: 120000
    });

    let date = new Date()
    let month = (date.getMonth() + 1).toString().padStart(2, '0');
    let day = date.getDate().toString().padStart(2, '0');
    let currentDate = day + '.' + month + '.' + date.getFullYear();

    let footerTemplate = fs.readFileSync(path.resolve(__dirname, './footer.html'), 'utf8').toString();

    footerTemplate = footerTemplate.replace('current_date', currentDate);

    await page.evaluateHandle('document.fonts.ready');

    const pdf = await page.pdf({
        preferCSSPageSize: true,
        displayHeaderFooter: true,
        format: 'A4',
        headerTemplate: headerContent,
        footerTemplate: footerTemplate
    });

    await browser.close();

    process.stdout.write(pdf);
})();
