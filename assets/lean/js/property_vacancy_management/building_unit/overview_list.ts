import '../../../styles/property/building_unit_overview_list.scss';
import '../../../styles/property/building_unit_overview_filter_form.scss';
import { OverviewListPage } from '../../src/PropertyVacancyManagement/Page/BuildingUnit/OverviewListPage';

const overviewListPage: OverviewListPage = new OverviewListPage();
