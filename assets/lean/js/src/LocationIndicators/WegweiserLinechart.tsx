import * as React from 'react';
import { CartesianGrid, Legend, ResponsiveContainer } from 'recharts';
import { LineChart } from 'recharts';
import { XAxis } from 'recharts';
import { YAxis } from 'recharts';
import { Tooltip } from 'recharts';
import { Line } from 'recharts';
import {Stack, Typography, Tooltip as MuiTooltip} from '@mui/material';
import { Props} from 'recharts/types/component/DefaultLegendContent';
import {ReactNode} from 'react';
import {InfoOutlined} from '@mui/icons-material';
import { IndicatorsAndTopic, WegweiserResult } from './LocationIndicatorsDetail';
import { ColorMapContext } from '../ColorGen';

interface IndicatorDef {
    id: string
    yearPoints: number[]
    def: IndicatorsAndTopic
}

const customLegend : (props: Props, indicatorDefs: IndicatorDef[]) => ReactNode = (props: Props, indicatorDefs: IndicatorDef[]) => {
  return <Stack direction="row" spacing={1} justifyContent="center">
    {props.payload?.map((entry: any, idx: number) => {
      const def = indicatorDefs.find((i) => i.def.friendlyUrl === entry.dataKey)?.def;

      return <Stack key={idx} direction="row">
        <Stack direction="row" columnGap={0.25}>
          <Typography variant="body2" color={entry.color}>
            {def?.title}
          </Typography>

          <MuiTooltip title={def?.explanation || ''}>
            <InfoOutlined sx={{color: entry.color}} fontSize="small" />
          </MuiTooltip>
        </Stack>
      </Stack>
    })}
  </Stack>
}

export function WegweiserLinechart(
    props: {
        title?: string,
        indicators: readonly string[]
        queryData: WegweiserResult
    },
) {

    const colorMap = React.useContext(ColorMapContext)
    const getColor = React.useCallback(colorMap.get, [JSON.stringify(colorMap.map)])

    const indicatorDefs: IndicatorDef[] = props.indicators.map((indicator) => {
    const def = props.queryData.indicatorsAndTopics.find((i) => i.friendlyUrl === indicator)
    const data = props.queryData.data.indicators.find((i) => i.friendlyUrl === indicator)
    const points = data?.regionYearValues[0]
    if (def !== undefined && points !== undefined && data !== undefined) {
      return {id: indicator, yearPoints: points, def}
    } else {
      return undefined
    }
  }).filter((i): i is IndicatorDef => i !== undefined)

  const chartData = props.queryData.years.map((year, yearIdx) => {
    return indicatorDefs.reduce((acc, i) => {
      acc[i.id] = i.yearPoints[yearIdx]
      return acc
    }, { year: year } as any,
    )
  })

  return <Stack direction="column" spacing={1}>
    {props.title && <Typography align="center">
      {props.title}
    </Typography>}

      <ResponsiveContainer height={300}>
        <LineChart data={chartData}>
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="year" />
          <YAxis />
          <Tooltip />
          <Legend content={(props) => customLegend(props, indicatorDefs)} />
          {indicatorDefs.map((i) => <Line
            connectNulls
            dot={{r: 4, strokeWidth: 4}}
            activeDot={{r: 4}}
            isAnimationActive={false}
            name={`${i.def.title.replace(' - Ist-Daten', '')} (${i.def.unit})`}
            key={`line-${i.id}`}
            dataKey={i.id}
            stroke={getColor(i.def.title)} />)}
        </LineChart>
      </ResponsiveContainer>
  </Stack>
}
