import { WegweiserIndicatorVisualization } from './LocationIndicatorsDetail';
import MdcContextMenuReactComponent from '../Component/MdcContextMenuReactComponent';
import LocationIndicatorSection from './LocationIndicatorSection';
import React from 'react';

type LocationIndicatorsConfig = {[section in string]: readonly WegweiserIndicatorVisualization[]}

const LocationIndicatorsWidget = () => {

    const locationIndicatorsConfig: LocationIndicatorsConfig = {
        'demography': [
            {
                visualization: 'lastyear-text',
                indicators: ['bevoelkerung', 'bevoelkerungsentwicklung-ueber-die-letzten-5-jahre', 'durchschnittsalter', 'einwohner-innendichte'],
                showIndicatorsAsTrend: ['bevoelkerungsentwicklung-ueber-die-letzten-5-jahre'],
            },
        ]
    }

    const locationIndicatorsWidget: HTMLDivElement = document.querySelector('#location_indicators_widget');
    return (
        <div>
            <div className="card-header d-flex mb-4">
                <div className="icon-thumbnail me-4 ms-0 align-text-top">
                    <span>
                        <i className="material-icons">
                            where_to_vote
                        </i>
                    </span>
                </div>
                <div className="card-title align-self-center">
                    <h2 className="mb-1 fs-4">Standortindikatoren</h2>
                    <p className="mb-0">Überblick zu den wichtigsten Standortindikatoren unserer Kommune</p>
                </div>
                <div className="ms-auto align-text-top">
                    <MdcContextMenuReactComponent idPrefix={'location_indicators_widget_'} listItems={[{
                        label: 'zu den Details',
                        icon: 'arrow_outward',
                        url: '/reports-und-berichte/standortindikatoren'
                    }]}/>
                </div>
            </div>

            <LocationIndicatorSection
                id="demography"
                indicatorVisualizations={locationIndicatorsConfig['demography']}
                region={locationIndicatorsWidget.dataset.region}
            />
        </div>
    );
};

export default LocationIndicatorsWidget;
