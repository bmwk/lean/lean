import React, { useState } from 'react';
import { Box, Tab, Tabs, ThemeProvider } from '@mui/material';
import { TabPanel } from '../CityGoal/TabPanel';
import LocationIndicatorSection from './LocationIndicatorSection';
import { createTheme } from '@mui/material/styles';

export interface RegionsAndRegionFilter {
    friendlyUrl: string;
    name: string;
    title: string;
    id: number;
    gkz: string;
    ars: string;
    type: string;
    demographicType: number;
    parent: string;
}

export interface Indicator {
    id: number;
    friendlyUrl: string;
    name: string;
    unit: string;
    minValue: number;
    maxValue: number;
    regionYearValues: number[][];
}

export interface IndicatorsAndTopic {
    id: number;
    name: string;
    title: string;
    type: string;
    explanation: string;
    hint: string;
    calculation: string;
    source: string;
    unit: string | undefined;
    decimalPlaces: number;
    friendlyUrl: string;
    years: number[];
    minimumRegionType: string;
    maximumRegionType: string;
    topics: string[];
    colorSchema: string;
}

export interface Region {
    friendlyUrl: string;
    name: string;
    title: string;
    id: number;
    gkz: string;
    ars: string;
    type: string;
    demographicType: number;
    parent: string;
}

export interface Data {
    regions: Region[];
    indicators: Indicator[];
    source: string;
}

export interface WegweiserResult {
    regionsAndRegionFilters: RegionsAndRegionFilter[];
    indicatorsAndTopics: IndicatorsAndTopic[];
    renderer: string;
    mapOutline?: any;
    years: number[];
    includeComparisonYear: boolean;
    data: Data;
}

let theme = createTheme({
    palette: {
        primary: {
            main: '#014A5D',
        },
        secondary: {
            main: '#1d81a1',
        },
    },
});


export type WegweiserRenderVisualization = 'lastyear-text' | 'historical-chart' | 'lastyear-table'
export type WegweiserSectionId = 'demography'| 'finance'| 'integration'| 'employment'| 'commute'| 'social'| 'longevity'

export interface WegweiserIndicatorVisualization {
    indicators: readonly string[];
    visualization: WegweiserRenderVisualization;
    showIndicatorsAsTrend?: readonly string[];
    subheader?: string;
    customRows?: readonly {
        name: string
        indicator1: string
        indicator2: string
        indicator3: string
    }[]
    customHeaders?: readonly string[];
}

type Config = {[section in WegweiserSectionId]: readonly WegweiserIndicatorVisualization[]}
const config: Config = {
    'demography': [
        {
            visualization: 'lastyear-text',
            indicators: ['bevoelkerung', 'bevoelkerungsentwicklung-ueber-die-letzten-5-jahre', 'durchschnittsalter', 'einwohner-innendichte'],
            showIndicatorsAsTrend: ['bevoelkerungsentwicklung-ueber-die-letzten-5-jahre'],
        },
        {
            visualization: 'lastyear-table',
            indicators: [
                'anteil-0-bis-2-jaehrige-1',
                'anteil-3-bis-5-jaehrige-1',
                'anteil-6-bis-9-jaehrige-1',
                'anteil-10-bis-15-jaehrige-1',
                'anteil-16-bis-18-jaehrige-1',
                'anteil-19-bis-24-jaehrige-1',
                'anteil-25-bis-44-jaehrige-1',
                'anteil-45-bis-64-jaehrige-1',
                'anteil-65-bis-79-jaehrige',
                'anteil-ab-80-jaehrige-3',

                'entwicklung-0-bis-2-jaehrige-seit-2011',
                'entwicklung-3-bis-5-jaehrige-seit-2011',
                'entwicklung-6-bis-9-jaehrige-seit-2011',
                'entwicklung-10-bis-15-jaehrige-seit-2011',
                'entwicklung-16-bis-18-jaehrige-seit-2011',
                'entwicklung-19-bis-24-jaehrige-seit-2011',
                'entwicklung-25-bis-44-jaehrige-seit-2011',
                'entwicklung-45-bis-64-jaehrige-seit-2011',
                'entwicklung-65-bis-79-jaehrige-seit-2011',
                'entwicklung-ab-80-jaehrige-seit-2011',

                'bevoelkerung-0-bis-2-jaehrige-1',
                'bevoelkerung-3-bis-5-jaehrige-1',
                'bevoelkerung-6-bis-9-jaehrige-1',
                'bevoelkerung-10-bis-15-jaehrige-1',
                'bevoelkerung-16-bis-18-jaehrige-1',
                'bevoelkerung-19-bis-24-jaehrige-1',
                'bevoelkerung-25-bis-44-jaehrige-1',
                'bevoelkerung-45-bis-64-jaehrige-1',
                'bevoelkerung-65-bis-79-jaehrige-1',
                'bevoelkerung-ab-80-jaehrige-1',
            ],
            showIndicatorsAsTrend: [
                'entwicklung-0-bis-2-jaehrige-seit-2011',
                'entwicklung-3-bis-5-jaehrige-seit-2011',
                'entwicklung-6-bis-9-jaehrige-seit-2011',
                'entwicklung-10-bis-15-jaehrige-seit-2011',
                'entwicklung-16-bis-18-jaehrige-seit-2011',
                'entwicklung-19-bis-24-jaehrige-seit-2011',
                'entwicklung-25-bis-44-jaehrige-seit-2011',
                'entwicklung-45-bis-64-jaehrige-seit-2011',
                'entwicklung-65-bis-79-jaehrige-seit-2011',
                'entwicklung-ab-80-jaehrige-seit-2011',
            ],
            customHeaders: [
                'Bevölkerungsstruktur nach Alter',
                'Bevölkerung',
                'Anteil (%)',
                'Entwicklung seit 2011 (%)',
            ],
            customRows: [
                {
                    name: '0 bis 2-Jährige',
                    indicator2: 'anteil-0-bis-2-jaehrige-1',
                    indicator3: 'entwicklung-0-bis-2-jaehrige-seit-2011',
                    indicator1: 'bevoelkerung-0-bis-2-jaehrige-1',
                },
                {
                    name: '3 bis 5-Jährige',
                    indicator2: 'anteil-3-bis-5-jaehrige-1',
                    indicator3: 'entwicklung-3-bis-5-jaehrige-seit-2011',
                    indicator1: 'bevoelkerung-3-bis-5-jaehrige-1',
                },
                {
                    name: '6 bis 9-Jährige',
                    indicator2: 'anteil-6-bis-9-jaehrige-1',
                    indicator3: 'entwicklung-6-bis-9-jaehrige-seit-2011',
                    indicator1: 'bevoelkerung-6-bis-9-jaehrige-1',
                },
                {
                    name: '10 bis 15-Jährige',
                    indicator2: 'anteil-10-bis-15-jaehrige-1',
                    indicator3: 'entwicklung-10-bis-15-jaehrige-seit-2011',
                    indicator1: 'bevoelkerung-10-bis-15-jaehrige-1',
                },
                {
                    name: '16 bis 18-Jährige',
                    indicator2: 'anteil-16-bis-18-jaehrige-1',
                    indicator3: 'entwicklung-16-bis-18-jaehrige-seit-2011',
                    indicator1: 'bevoelkerung-16-bis-18-jaehrige-1',
                },
                {
                    name: '19 bis 24-Jährige',
                    indicator2: 'anteil-19-bis-24-jaehrige-1',
                    indicator3: 'entwicklung-19-bis-24-jaehrige-seit-2011',
                    indicator1: 'bevoelkerung-19-bis-24-jaehrige-1',
                },
                {
                    name: '25 bis 44-Jährige',
                    indicator2: 'anteil-25-bis-44-jaehrige-1',
                    indicator3: 'entwicklung-25-bis-44-jaehrige-seit-2011',
                    indicator1: 'bevoelkerung-25-bis-44-jaehrige-1',
                },
                {
                    name: '45 bis 64-Jährige',
                    indicator2: 'anteil-45-bis-64-jaehrige-1',
                    indicator3: 'entwicklung-45-bis-64-jaehrige-seit-2011',
                    indicator1: 'bevoelkerung-45-bis-64-jaehrige-1',
                },
                {
                    name: '65 bis 79-Jährige',
                    indicator2: 'anteil-65-bis-79-jaehrige',
                    indicator3: 'entwicklung-65-bis-79-jaehrige-seit-2011',
                    indicator1: 'bevoelkerung-65-bis-79-jaehrige-1',
                },
                {
                    name: 'Ab 80-Jährige',
                    indicator2: 'anteil-ab-80-jaehrige-3',
                    indicator3: 'entwicklung-ab-80-jaehrige-seit-2011',
                    indicator1: 'bevoelkerung-ab-80-jaehrige-1',
                },
            ],
        },
        {visualization: 'lastyear-table', indicators: [
                'jugendquotient', 'altenquotient',
            ]},
        {
            visualization: 'historical-chart',
            subheader: 'Wanderungssaldo',
            indicators: ['wanderungssaldo-2', 'familienwanderung', 'alterswanderung'],
        },
    ],
    'finance': [
        {
            visualization: 'historical-chart',
            subheader: 'Hebesatz',
            indicators: ['hebesatz-gewerbesteuer'],
        },
        {
            visualization: 'historical-chart',
            subheader: 'Steuer',
            indicators: ['gewerbesteuer-netto', 'steuereinnahmen-pro-einwohner-in'],
        },
    ],
    'integration': [{visualization: 'lastyear-text',
        indicators: ['anteil-auslaender-innen', 'arbeitslose-an-der-gesamtbevoelkerung']}],
    'employment': [
        {
            visualization: 'historical-chart',
            subheader: 'Beschäftigung',
            indicators: ['beschaeftigungsquote', 'beschaeftigungsanteil-im-dienstleistungssektor'],
        },
        {
            visualization: 'historical-chart',
            subheader: 'Existenzgründungen',
            indicators: ['existenzgruendungen'],
        },
    ],
    'commute': [
        {
            visualization: 'historical-chart',
            indicators: ['einpendler-innen-an-der-bevoelkerung-gesamt', 'auspendler-innen-an-der-bevoelkerung-gesamt', 'pendlersaldo-an-der-bevoelkerung-gesamt'],
        },
    ],
    'social': [{visualization: 'lastyear-text',
        indicators: ['haushalte-mit-niedrigem-einkommen', 'haushalte-mit-mittlerem-einkommen', 'haushalte-mit-hohem-einkommen',
                     'sgb-ii-quote', 'arbeitslose-an-den-svb', 'einpersonen-haushalte', 'haushalte-mit-kindern', 'wohnflaeche-pro-person']}],
    'longevity': [{visualization: 'lastyear-text',
        indicators: ['pkw-dichte', 'flaecheninanspruchnahme', 'naherholungsflaechen']}],
}

const LocationIndicatorsDetail = () => {

    const [tabIndex, setTabIndex] = useState(0)
    const region = (document.querySelector('#location-indicators-app') as HTMLElement).dataset.region

    return (
        <div className="mdc-card">
            <ThemeProvider theme={theme}>
                <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                    <Tabs variant="fullWidth" value={tabIndex} onChange={(e, value) => setTabIndex(value)}>
                        <Tab label="Demografische Entwicklung" />
                        <Tab label="Finanzen"  />
                        <Tab label="Integration"  />
                        <Tab label="Arbeitsmarkt" />
                        <Tab label="Soziale Lage" />
                        <Tab label="Sonstiges" />
                    </Tabs>
                </Box>
                <TabPanel value={tabIndex} index={0}>
                    <LocationIndicatorSection showTitle={true} id="demography" indicatorVisualizations={config['demography']} region={region}/>
                </TabPanel>
                <TabPanel value={tabIndex} index={1}>
                    <LocationIndicatorSection showTitle={true} id="finance" indicatorVisualizations={config['finance']} region={region}/>
                </TabPanel>
                <TabPanel value={tabIndex} index={2}>
                    <LocationIndicatorSection showTitle={true} id="integration" indicatorVisualizations={config['integration']} region={region}/>
                </TabPanel>
                <TabPanel value={tabIndex} index={3}>
                    <LocationIndicatorSection showTitle={true} id="employment" indicatorVisualizations={config['employment']} region={region}/>
                </TabPanel>
                <TabPanel value={tabIndex} index={4}>
                    <LocationIndicatorSection showTitle={true} id="social" indicatorVisualizations={config['social']} region={region}/>
                </TabPanel>
                <TabPanel value={tabIndex} index={5}>
                    <LocationIndicatorSection showTitle={true} id="longevity" indicatorVisualizations={config['longevity']} region={region}/>
                </TabPanel>
            </ThemeProvider>
        </div>
    );
};

export default LocationIndicatorsDetail;
