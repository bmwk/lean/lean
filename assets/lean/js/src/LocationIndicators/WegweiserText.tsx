import { InfoOutlined, TrendingDown, TrendingUp } from '@mui/icons-material';
import { Grid, Stack, Tooltip, Typography } from '@mui/material';
import * as React from 'react';
import { WegweiserResult } from './LocationIndicatorsDetail';

export const lastNonNullIndex = (arr: readonly number[]) => {
  const res = arr.length - 1 - arr.slice(0).reverse().findIndex((e) => e !== null)

  return res === arr.length ? -1 : res;
}

function arrowIcon(value: number) : React.ReactElement | undefined {
  if (value > 0) {
    return <TrendingUp fontSize="small" sx={{color: 'green'}}/>
  } else if (value < 0) {
    return <TrendingDown fontSize="small" sx={{color: 'red'}}/>
  }
  return undefined
}

export function WegweiserText(
    props: {
        indicators: readonly string[]
        queryData: WegweiserResult
        showIndicatorsAsTrend?: readonly string[]
    },
) {
  return (
    <Grid container rowGap={2}>
      {props.indicators.map((indicator) => {
        const def = props.queryData.indicatorsAndTopics.find((i) => i.friendlyUrl === indicator)
        const data = props.queryData.data.indicators.find((i) => i.friendlyUrl === indicator)
        const pointIndex = data ? lastNonNullIndex(data.regionYearValues[0]) : undefined
        const point = pointIndex ? data?.regionYearValues[0][pointIndex] : undefined
        const year = pointIndex ? props.queryData.years[pointIndex] : undefined
        if (!def || !data || point === undefined || year === undefined) {
          return null
        } else {
          const isTrend = props.showIndicatorsAsTrend?.includes(indicator)

          return <Grid item
              md={6} xl={3}
            key={`wegweiser-text-${indicator}`}
            sx={{display: 'flex', flexDirection: 'column', alignItems: 'center', justifyItems: 'center', justifyContent: 'center', textAlign: 'center'}}>
            <Typography variant="caption">
              {def.name}
            </Typography>

            <Stack direction="row" spacing={0.25} alignItems="center">
              <Typography variant="body2">
                {isTrend && point > 0 ? '+' : ''}{isTrend && point < 0 ? '-' : ''}{point.toLocaleString('de-DE')} {def.unit}
              </Typography>
              {props.showIndicatorsAsTrend?.includes(indicator) ? arrowIcon(point) : undefined}
            </Stack>

            <Stack direction="row" columnGap={0.25}>
              <Typography variant="body2" color='grey.400'>
                Stand: {year}
              </Typography>
              <Tooltip title={def.explanation}>
                <InfoOutlined sx={{color: 'grey.400'}} fontSize="small" />
              </Tooltip>
            </Stack>
          </Grid>
        }
      })}

    </Grid>
  )
}
