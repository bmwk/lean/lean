import { Data, IndicatorsAndTopic, WegweiserResult } from './LocationIndicatorsDetail';
import { Alert, Stack, Table, Tooltip } from '@mui/material';
import TableHead from '@mui/material/TableHead';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableRow from '@mui/material/TableRow';
import { InfoOutlined } from '@mui/icons-material';
import React from 'react';

function renderLastYear(indicator: IndicatorsAndTopic, data: Data, lastYearIndex: number): string {
  const indicatorData = data.indicators.find((i) => i.friendlyUrl === indicator.friendlyUrl)
  const unit = indicatorData?.unit
  const regionValues = indicatorData?.regionYearValues?.slice(-1)[0]
  let lastYearValue = regionValues?.[lastYearIndex]

  // No value found, choose last non-null value from the array
  if (lastYearValue === undefined) {
    lastYearValue = regionValues?.filter((value: any) => value !== null)?.slice(-1)[0]
  }

  return lastYearValue ? `${lastYearValue.toLocaleString('de-DE')} ${unit}` : 'Keine Daten vorhanden'
}

export function LastYearTable(props: { indicatorHeader: string, indicators: readonly string[], queryData: WegweiserResult }) {
  const indicatorsToShow = props.queryData.indicatorsAndTopics.filter((i) => props.indicators.includes(i.friendlyUrl));
  const lastYear = props.queryData.years.slice(-1)[0];

  if (lastYear === undefined) {
    return <Alert severity="warning">Die geladenen Daten können nicht angezeigt werden.</Alert>
  }

  return (
    <Table size="small">
      <TableHead>
        <TableRow>
          <TableCell>
            {props.indicatorHeader}
          </TableCell>
          <TableCell>
            {lastYear}
          </TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {indicatorsToShow.map((indicator) => {
          return (
            <TableRow key={indicator.id}>
              <TableCell>
                <Stack direction="row" columnGap={0.25}>
                  {indicator.name}
                  <Tooltip title={indicator.explanation}>
                    <InfoOutlined fontSize="small" sx={{color: 'grey.400'}}/>
                  </Tooltip>
                </Stack>
              </TableCell>
              <TableCell>
                {renderLastYear(indicator, props.queryData.data, props.queryData.years.length - 1)}
              </TableCell>
            </TableRow>
          )
        })}
      </TableBody>
    </Table>
  )
}
