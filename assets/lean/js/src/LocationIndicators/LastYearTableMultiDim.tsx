import React from 'react';
import { Stack, Table, Tooltip, Typography } from '@mui/material';
import TableHead from '@mui/material/TableHead';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableRow from '@mui/material/TableRow';
import { lastNonNullIndex } from './WegweiserText';
import { InfoOutlined, TrendingDown, TrendingUp } from '@mui/icons-material';
import { WegweiserResult } from './LocationIndicatorsDetail';

export function LastYearTableMultiDim(props: {
    title?: string,
    indicators: readonly string[],
    queryData: WegweiserResult,
    customRows: readonly {
        name: string
        indicator1: string
        indicator2: string
        indicator3: string
    }[]
    customHeaders: readonly string[]
    showIndicatorsAsTrend?: readonly string[]
}) {
    const maybeTrend: (point: number | undefined, indicator: string) => string | undefined = (point, indicator) => {
        if (point === undefined) return undefined;
        if (props.showIndicatorsAsTrend?.includes(indicator)) {
            return point > 0 ? '+' : point < 0 ? '-' : '';
        }
    };

    function maybeArrow(point: number | undefined, indicator: string): React.ReactElement | undefined {
        if (point === undefined) return undefined;
        if (props.showIndicatorsAsTrend?.includes(indicator)) {
            if (point > 0) {
                return <TrendingUp fontSize="small" sx={{color: 'green'}}/>;
            } else if (point < 0) {
                return <TrendingDown fontSize="small" sx={{color: 'red'}}/>;
            }
        }
        return undefined;
    }

    return (
        <Stack direction="column" spacing={1}>
            {props.title && <Typography align="center">
                {props.title}
            </Typography>}

            <Table size="small">
                <TableHead>
                    <TableRow>
                        {props.customHeaders.map((h, idx) => <TableCell key={`header-dim-${idx}`}>
                            <Stack direction="row">
                                <Typography variant="body2">{h}</Typography>
                                <Typography variant="body2" className="ms-1"
                                            color="grey.400">(Stand: {props.queryData.years[lastNonNullIndex(props.queryData.years)]})</Typography>
                            </Stack>
                        </TableCell>)}
                    </TableRow>
                </TableHead>
                <TableBody>
                    {props.customRows.map((r, idx) => {
                        const data1 = props.queryData.data.indicators.find((i) => i.friendlyUrl === r.indicator1);
                        const explanation1 = props.queryData.indicatorsAndTopics.find((i) => i.friendlyUrl === r.indicator1)?.explanation;
                        const pointIndex1 = data1 ? lastNonNullIndex(data1.regionYearValues[0]) : undefined;
                        const point1 = pointIndex1 ? data1?.regionYearValues[0][pointIndex1] : undefined;

                        const data2 = props.queryData.data.indicators.find((i) => i.friendlyUrl === r.indicator2);
                        const explanation2 = props.queryData.indicatorsAndTopics.find((i) => i.friendlyUrl === r.indicator2)?.explanation;
                        const pointIndex2 = data2 ? lastNonNullIndex(data2.regionYearValues[0]) : undefined;
                        const point2 = pointIndex2 ? data2?.regionYearValues[0][pointIndex2] : undefined;

                        const data3 = props.queryData.data.indicators.find((i) => i.friendlyUrl === r.indicator3);
                        const explanation3 = props.queryData.indicatorsAndTopics.find((i) => i.friendlyUrl === r.indicator3)?.explanation;
                        const pointIndex3 = data3 ? lastNonNullIndex(data3.regionYearValues[0]) : undefined;
                        const point3 = pointIndex3 ? data3?.regionYearValues[0][pointIndex3] : undefined;

                        return (
                            <TableRow key={`row-${idx}`}>
                                <TableCell>
                                    {r.name}
                                </TableCell>
                                <TableCell align="right">
                                    {maybeTrend(point1, r.indicator1)}{point1 !== undefined ? point1.toLocaleString('de-DE') : 'k.A.'} {maybeArrow(point1, r.indicator1)}

                                    <Tooltip title={explanation1 || ''}>
                                        <InfoOutlined fontSize="small" sx={{color: 'grey.400'}}/>
                                    </Tooltip>
                                </TableCell>
                                <TableCell align="right">
                                    {maybeTrend(point2, r.indicator2)}{point2 !== undefined ? point2.toLocaleString('de-DE') : 'k.A.'} {maybeArrow(point2, r.indicator2)}

                                    <Tooltip title={explanation2 || ''}>
                                        <InfoOutlined fontSize="small" sx={{color: 'grey.400'}}/>
                                    </Tooltip>
                                </TableCell>
                                <TableCell align="right">
                                    {maybeTrend(point3, r.indicator3)}{point3 !== undefined ? point3.toLocaleString('de-DE') : 'k.A.'} {maybeArrow(point3, r.indicator3)}

                                    <Tooltip title={explanation3 || ''}>
                                        <InfoOutlined fontSize="small" sx={{color: 'grey.400'}}/>
                                    </Tooltip>
                                </TableCell>
                            </TableRow>
                        );
                    })}
                </TableBody>
            </Table>
        </Stack>
    );
}
