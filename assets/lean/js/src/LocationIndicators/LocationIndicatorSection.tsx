import React, { useEffect, useState } from 'react';
import { WegweiserIndicatorVisualization, WegweiserResult } from './LocationIndicatorsDetail';
import { LastYearTableMultiDim } from './LastYearTableMultiDim';
import { LastYearTable } from './LastYearTable';
import { WegweiserLinechart } from './WegweiserLinechart';
import { WegweiserText } from './WegweiserText';
import { Box, CardContent, CardHeader, IconButton, Stack, Tooltip, Typography } from '@mui/material';
import { OpenInBrowser } from '@mui/icons-material';
import { ColorMapContext, colorStore } from '../ColorGen';

function renderVisualization(iv: WegweiserIndicatorVisualization,
                             idx: number, queryData: WegweiserResult): React.ReactNode {
    switch (iv.visualization) {
        case 'lastyear-table':
            if (iv.customHeaders !== undefined && iv.customRows !== undefined) {
                return <LastYearTableMultiDim
                    indicators={iv.indicators}
                    queryData={queryData}
                    key={`visualization-${idx}`}
                    customHeaders={iv.customHeaders}
                    showIndicatorsAsTrend={iv.showIndicatorsAsTrend}
                    customRows={iv.customRows}
                />
            } else {
                return <LastYearTable
                    indicatorHeader="Jugend- & Altenquotient"
                    indicators={iv.indicators}
                    queryData={queryData}
                    key={`visualization-${idx}`}
                />
            }
        case 'historical-chart': return <WegweiserLinechart title={iv.subheader} key={`visualization-${idx}`} indicators={iv.indicators} queryData={queryData} />
        case 'lastyear-text': return <WegweiserText key={`visualization-${idx}`} indicators={iv.indicators}
                                                    queryData={queryData} showIndicatorsAsTrend={iv.showIndicatorsAsTrend}/>
    }
}

function indicatorLink(indicators: readonly string[], region: string) {
    return encodeURIComponent(`${indicators.join('+')}+${region}+tabelle`)
}

function wegweiserIdToSectionTitle(id: string): string {
    switch (id) {
        case 'demography': return 'Demografische Entwicklung'
        case 'finance': return 'Finanzen'
        case 'integration': return 'Anteil Ausländer:innen und Arbeitslose'
        case 'employment': return 'Beschäftigung / Arbeitsmarkt'
        case 'commute': return 'Pendler:innen'
        case 'social': return 'Soziale Lage'
        case 'longevity': return 'Sonstiges'
    }
}



const LocationIndicatorSection = (
    props: {
        id: string,
        indicatorVisualizations: readonly WegweiserIndicatorVisualization[]
        region: string
        showTitle?: boolean
    },

) => {

    const [indicatorData, setIndicatorData] = useState(null)

    const title = wegweiserIdToSectionTitle(props.id)
    const allIndicators = props.indicatorVisualizations.map((iv) => iv.indicators).flat()


    useEffect(()=>{
        const getData = async () => {
            const indicators = props.indicatorVisualizations.flatMap((c) => c.indicators).join('+');
            const response = await fetch(`https://www.wegweiser-kommune.de/data-api/rest/statistics/data/${indicators}+${props.region}`)
            const data = await response.json();
            setIndicatorData(data)
        }
        getData();
    }, [])

    return (
        <div>
            {indicatorData &&
            <>
                {props.showTitle && <CardHeader
                    disableTypography
                    title={
                        <Box sx={{display: 'flex', alignItems: 'baseline'}}>
                            <Typography variant="h5" sx={{mr: 1}}>{title}
                                <Tooltip
                                    title='Öffnet die Indikatoren in einem neuen Tab'>
                                    <IconButton target='_blank'
                                                href={`https://www.wegweiser-kommune.de/daten/${indicatorLink(allIndicators, props.region)}`}>
                                        <OpenInBrowser />
                                    </IconButton>
                                </Tooltip>
                            </Typography>
                        </Box>
                    }
                />}

                <CardContent>
                    <ColorMapContext.Provider value={colorStore()}>
                        <Stack direction="column" spacing={4}>
                            {props.indicatorVisualizations.map((iv, idx) => renderVisualization(iv, idx, indicatorData)) }
                            <Typography color='grey.400' sx={{ fontStyle: 'italic', pt: 4, textAlign: 'center' }} variant="caption">
                                Quelle: Bertelsmann Stiftung/www.wegweiser-kommune.de
                            </Typography>
                        </Stack>
                    </ColorMapContext.Provider>
                </CardContent>
            </>
            }
        </div>
    );
};

export default LocationIndicatorSection;
