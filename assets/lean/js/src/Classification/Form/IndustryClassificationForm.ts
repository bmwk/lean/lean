import { SelectComponent } from '../../Component/SelectComponent';
import Axios, { AxiosResponse } from 'axios';

interface IndustryClassification {
    id: number;
    levelOne: number|null;
    levelThree: number|null;
    levelTwo: number|null;
    name: string;
    parent: IndustryClassification|null;
}

interface onInitialize {
    (industryClassificationLevelOne: number): void
}

interface onChange {
    (industryClassificationLevelOne: number): void
}

class IndustryClassificationForm {

    private readonly industryUsageSelect: SelectComponent;
    private readonly industryAreaSelect: SelectComponent;
    private readonly industryDetailsSelect: SelectComponent;
    private readonly industryAreaWrapperDiv: HTMLDivElement;
    private readonly industryDetailsWrapperDiv: HTMLDivElement;
    private readonly industryClassificationIdInputField: HTMLInputElement;
    private readonly onInitialize?: onInitialize;
    private readonly onChange?: onChange;
    private isSettingSelectWithFetching: boolean;
    private isResetting: boolean;

    constructor(idPrefix: string, onInitialize?: onInitialize, onChange?: onChange) {
        this.industryUsageSelect = new SelectComponent(document.querySelector('#' + idPrefix + 'industryUsage_mdc_select'), {clearButton: true});
        this.industryAreaSelect = new SelectComponent(document.querySelector('#' + idPrefix + 'industryArea_mdc_select'), {clearButton: true});
        this.industryDetailsSelect = new SelectComponent(document.querySelector('#' + idPrefix + 'industryDetails_mdc_select'), {clearButton: true});
        this.industryAreaWrapperDiv = document.querySelector('#' + idPrefix + 'industry_area_wrapper');
        this.industryDetailsWrapperDiv = document.querySelector('#' + idPrefix + 'industry_details_wrapper');
        this.industryClassificationIdInputField = document.querySelector('#' + idPrefix + 'industryClassificationId');

        this.onInitialize = onInitialize;
        this.onChange = onChange;

        this.isSettingSelectWithFetching = true;
        this.isResetting = false;

        this.industryUsageSelect.mdcSelect.required = true;

        this.onInitialize(parseInt(this.industryUsageSelect.mdcSelect.value));

        this.initializeIndustryClassificationIfExist();

        this.industryUsageSelect.mdcSelect.listen('MDCSelect:change', () => this.handleIndustrySelectChange(this.industryUsageSelect, 1));
        this.industryAreaSelect.mdcSelect.listen('MDCSelect:change', () => this.handleIndustrySelectChange(this.industryAreaSelect, 2));
        this.industryDetailsSelect.mdcSelect.listen('MDCSelect:change', () => this.handleIndustrySelectChange(this.industryDetailsSelect, 3));
    }

    public setRequired(required: boolean) {
        this.industryUsageSelect.mdcSelect.required = required;
    }

    public isFormValid(): boolean {
        let hasErrors: boolean = false;

        if (this.industryUsageSelect.mdcSelect.value.length === 0) {
            this.industryUsageSelect.mdcSelect.valid = false;
            hasErrors = true;
        }

        return (hasErrors === false);
    }

    private async initializeIndustryClassificationIfExist() {
        this.disableAllSelectFields();

        if (this.industryClassificationIdInputField.value != '') {
            await this.fetchAndSetIndustryClassificationById(this.industryClassificationIdInputField.value);
        }

        this.enableAllSelectFields();
    }

    private async fetchAndSetIndustryClassificationById(id: string) {
        try {
            const industryClassificationResponse: AxiosResponse = await Axios.get('/api/classifications/industry-classifications/' + id);
            await this.fillAndSetAllIndustryClassificationLevelsByIndustryElement(industryClassificationResponse.data)
        } catch (error) {
        }
    }

    private async fillAndSetAllIndustryClassificationLevelsByIndustryElement(industryClassification: IndustryClassification) {
        this.isSettingSelectWithFetching = false;

        const level: number = this.getIndustryClassificationLevel(industryClassification);

        if (level === 3) {
            this.industryUsageSelect.mdcSelect.value = industryClassification.parent.parent.id.toString();
            await this.fetchIndustryClassification(industryClassification.parent.parent.id.toString(), 1);
            this.industryAreaSelect.mdcSelect.value = industryClassification.parent.id.toString();
            await this.fetchIndustryClassification(industryClassification.parent.id.toString(), 2);
            this.industryDetailsSelect.mdcSelect.value = industryClassification.id.toString();
            this.industryClassificationIdInputField.value = this.industryDetailsSelect.mdcSelect.value;
        } else if (level === 2) {
            this.industryUsageSelect.mdcSelect.value = industryClassification.parent.id.toString();
            await this.fetchIndustryClassification(industryClassification.parent.id.toString(), 1);
            this.industryAreaSelect.mdcSelect.value = industryClassification.id.toString();
            await this.fetchIndustryClassification(industryClassification.id.toString(), 2);
        } else if (level === 1) {
            this.industryUsageSelect.mdcSelect.value = industryClassification.id.toString();
            await this.fetchIndustryClassification(industryClassification.id.toString(), 1);
        }

        this.isSettingSelectWithFetching = true;
    }

    private getIndustryClassificationLevel(industryClassification: IndustryClassification): number {
        if (industryClassification.levelOne != null && industryClassification.levelTwo === null && industryClassification.levelThree === null) {
            return 1;
        } else if (industryClassification.levelOne != null && industryClassification.levelTwo != null && industryClassification.levelThree === null) {
            return 2;
        } else {
            return 3;
        }
    }

    private async handleIndustrySelectChange(selectComponent: SelectComponent, level: number) {
        if (level === 1) {
            this.onChange(parseInt(selectComponent.mdcSelect.value));
        }

        if (this.isSettingSelectWithFetching === false || this.isResetting === true) {
            return;
        }

        this.disableAllSelectFields();

        if (level === 1) {
            this.showOrHideIndustryClassificationByLevel(level);
        }

        await this.fetchIndustryClassification(selectComponent.mdcSelect.value, level);

        this.enableAllSelectFields();
    }

    private disableAllSelectFields(): void {
        this.industryAreaSelect.mdcSelect.disabled = true;
        this.industryDetailsSelect.mdcSelect.disabled = true;
    }

    private enableAllSelectFields(): void {
        this.industryAreaSelect.mdcSelect.disabled = false;
        this.industryDetailsSelect.mdcSelect.disabled = false;
    }

    private fillIndustryClassificationSelectOptionsByLevel(industryClassifications: IndustryClassification[], level: number): void {
        switch (level) {
            case 1:
                this.industryAreaSelect.mdcSelect.root.querySelector('.mdc-list').innerHTML = '';

                industryClassifications.forEach((industryClassification: IndustryClassification): void => {
                    this.industryAreaSelect.mdcSelect.root.querySelector('.mdc-list').innerHTML += IndustryClassificationForm.createSelectListItem(industryClassification.id, industryClassification.name);
                });

                this.industryAreaSelect.mdcSelect.layoutOptions();
                break;
            case 2:
                this.industryDetailsSelect.mdcSelect.root.querySelector('.mdc-list').innerHTML = '';

                industryClassifications.forEach((industryClassification: IndustryClassification): void => {
                    this.industryDetailsSelect.mdcSelect.root.querySelector('.mdc-list').innerHTML += IndustryClassificationForm.createSelectListItem(industryClassification.id, industryClassification.name);
                });

                this.industryDetailsSelect.mdcSelect.layoutOptions();
                break;
        }
    }

    private async fetchIndustryClassification(id: string, level: number) {
        try {
            this.showOrHideIndustryClassificationByLevel(level);
            this.resetIndustryClassificationToLevel(level);
            this.industryClassificationIdInputField.value = id;

            const industryClassificationResponseList: AxiosResponse = await Axios.get('/api/classifications/industry-classifications/' + id + '/industry-classifications');

            if (industryClassificationResponseList.data.length === 0) {
                this.showOrHideIndustryClassificationByLevel(level - 1);
            } else {
                this.fillIndustryClassificationSelectOptionsByLevel(industryClassificationResponseList.data, level);
            }
        } catch (error) {
        }

        return true;
    }

    private resetIndustryClassificationToLevel(level: number): void {
        this.isResetting = true;

        switch (level) {
            case 1:
                this.industryAreaSelect.mdcSelect.setValue('');
                this.industryDetailsSelect.mdcSelect.setValue('');
                break;
            case 2:
                this.industryDetailsSelect.mdcSelect.setValue('');
                break;
            case 3:
                break;
            default:
                this.industryAreaSelect.mdcSelect.setValue('');
                this.industryDetailsSelect.mdcSelect.setValue('');
        }

        this.isResetting = false;
    }

    private showOrHideIndustryClassificationByLevel(level: number): void {
        switch (level) {
            case 1:
                this.industryAreaWrapperDiv.classList.remove('d-none');
                this.industryDetailsWrapperDiv.classList.add('d-none');
                break;
            case 2:
                this.industryAreaWrapperDiv.classList.remove('d-none');
                this.industryDetailsWrapperDiv.classList.remove('d-none');
                break;
            case 3:
                break;
            default:
                this.industryAreaWrapperDiv.classList.add('d-none');
                this.industryDetailsWrapperDiv.classList.add('d-none');
        }
    }

    private static createSelectListItem(id: number, titel: string): string {
        return '<li class="mdc-list-item" aria-selected="false" data-value="' + id + '" role="option" tabindex="-1">' + "\n" +
            '   <span class="mdc-list-item__ripple"></span>' + "\n" +
            '   <span class="mdc-list-item__text">' + titel + '</span>' + "\n" +
            '</li>' + "\n";
    }

}

export { IndustryClassificationForm };
