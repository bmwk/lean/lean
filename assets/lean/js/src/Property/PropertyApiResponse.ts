interface PropertyApiResponse {
    address: Address;
    geolocationMultiPolygon: null;
    geolocationPoint: GeolocationPoint;
    geolocationPolygon: GeolocationPolygon;
    id: number;
    internalNumber: string;
    name: string;
    currentUsageIndustryClassification: NaceClassification;
    pastUsageIndustryClassification: NaceClassification;
    futureUsageIndustryClassification: NaceClassification;
    objectBecomesEmpty: boolean;
    objectIsEmpty: boolean;
    reuseAgreed: boolean;
    objectBecomesEmptyFrom: string;
    objectIsEmptySince: string;
    reuseFrom: string;
    keyProperty: boolean;
    inFloors: string;
    propertyMarketingInformation: MarketingInformation;
    areaSize: string;
    barrierFreeAccess: number;
    shopWindowFrontWidth: string;
    assignedToAccountUser: { id: number, fullName: string };
    updatedAt: string;
    createdAt: string;
}

interface NaceClassification {
    id: number;
    name: string;
    levelOne: number;
    levelTwo: number;
    levelThree: number;
}

interface Address {
    place: Place;
    quarterPlace: Place;
    streetName: string;
    houseNumber: string;
    postalCode: number;
}

interface Place {
    placeName: string;
}

interface GeolocationPoint {
    point: Point;
}

interface GeolocationPolygon {
    id: number;
    polygon: Polygon;
}

interface Point {
    latitude: number;
    longitude: number;
}

interface Polygon {
    rings: Ring[];
}

interface Ring {
    points: Point[];
}

interface MarketingInformation {
    possibleUsageIndustryClassifications: NaceClassification[];
    propertyOfferType: number;
}

export { PropertyApiResponse, Address, MarketingInformation, NaceClassification };
