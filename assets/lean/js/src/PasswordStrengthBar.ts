import '../../styles/password_strength_bar.scss';
import { MDCTextField } from '@material/textfield';

class PasswordStrengthBar {

    private readonly mdcTextField: MDCTextField;
    private readonly passwordStrengthBarElement: HTMLDivElement;

    constructor(mdcTextField: MDCTextField) {
        this.mdcTextField = mdcTextField;
        this.passwordStrengthBarElement = document.createElement('div');

        this.passwordStrengthBarElement.classList.add('password_strength_bar');
        this.mdcTextField.root.insertBefore(this.passwordStrengthBarElement, null);

        this.mdcTextField.root.querySelector('input').addEventListener('keyup', (): void => {
            this.check(this.mdcTextField.value);
        });
    }

    private check(plainPassword: string) {
        if (new RegExp('(?=.{6,}).*', 'g').test(plainPassword) == false) {
            this.passwordStrengthBarElement.classList.remove('medium');
            this.passwordStrengthBarElement.classList.remove('strong');
            this.passwordStrengthBarElement.classList.add('weak');
        } else if (new RegExp('^(?=.{8,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*\\W).*$', 'g').test(plainPassword)) {
            this.passwordStrengthBarElement.classList.remove('medium');
            this.passwordStrengthBarElement.classList.remove('weak');
            this.passwordStrengthBarElement.classList.add('strong');
        } else if (new RegExp('^(?=.{7,})(((?=.*[A-Z])(?=.*[a-z]))|((?=.*[A-Z])(?=.*[0-9]))|((?=.*[a-z])(?=.*[0-9]))).*$', 'g').test(plainPassword)) {
            this.passwordStrengthBarElement.classList.remove('weak');
            this.passwordStrengthBarElement.classList.remove('strong');
            this.passwordStrengthBarElement.classList.add('medium');
        } else {
            this.passwordStrengthBarElement.classList.remove('weak');
            this.passwordStrengthBarElement.classList.remove('strong');
            this.passwordStrengthBarElement.classList.add('medium');
        }
    }

}

export { PasswordStrengthBar };
