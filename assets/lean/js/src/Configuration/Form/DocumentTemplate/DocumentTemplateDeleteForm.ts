import { DeleteForm } from '../../../Form/DeleteForm';

class DocumentTemplateDeleteForm extends DeleteForm {

    constructor(idPrefix: string) {
        super(idPrefix);
    }

}

export { DocumentTemplateDeleteForm };
