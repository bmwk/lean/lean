import { TextFieldComponent } from '../../../Component/TextFieldComponent';

class DocumentTemplateForm {

    private readonly titleTextField: TextFieldComponent;

    constructor(idPrefix: string) {
        this.titleTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'name_mdc_text_field'));

        this.titleTextField.mdcTextField.required = true;
    }

}

export { DocumentTemplateForm };
