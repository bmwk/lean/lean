import { DeleteForm } from '../../../Form/DeleteForm';

class OpenImmoFtpCredentialDeleteForm extends DeleteForm {

    constructor(idPrefix: string) {
        super(idPrefix);
    }

}

export { OpenImmoFtpCredentialDeleteForm };
