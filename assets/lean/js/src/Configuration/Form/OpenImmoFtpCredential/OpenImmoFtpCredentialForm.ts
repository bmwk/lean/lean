import { TextFieldComponent } from '../../../Component/TextFieldComponent';
import { SelectComponent } from '../../../Component/SelectComponent';

class OpenImmoFtpCredentialForm {

    private readonly portalNameTextField: TextFieldComponent;
    private readonly portalProviderNumberTextField: TextFieldComponent;
    private readonly hostnameTextField: TextFieldComponent;
    private readonly portTextField: TextFieldComponent;
    private readonly ftpProtocolSelect: SelectComponent;
    private readonly usernameTextField: TextFieldComponent;
    private readonly plainPasswordTextField: TextFieldComponent;
    private readonly passwordContainer: HTMLDivElement;
    private readonly changePasswordButtonContainer: HTMLDivElement;
    private readonly changePasswordButton: HTMLButtonElement;

    constructor(idPrefix: string) {
        this.portalNameTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'portalName_mdc_text_field'));
        this.portalProviderNumberTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'portalProviderNumber_mdc_text_field'));
        this.hostnameTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'hostname_mdc_text_field'));
        this.portTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'port_mdc_text_field'));
        this.ftpProtocolSelect = new SelectComponent(document.querySelector('#' + idPrefix + 'ftpProtocol_mdc_select'));
        this.usernameTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'username_mdc_text_field'));
        this.plainPasswordTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'plainPassword_mdc_text_field'));

        if (document.querySelector('#' + idPrefix + 'change_password_button_container') !== null) {
            this.changePasswordButtonContainer = document.querySelector('#' + idPrefix + 'change_password_button_container');
            this.changePasswordButton = document.querySelector('#' + idPrefix + 'change_password_button');
            this.passwordContainer = document.querySelector('#' + idPrefix + 'password_container');
            this.plainPasswordTextField.mdcTextField.disabled = true;

            this.changePasswordButton.addEventListener('click', (event): void => {
                event.preventDefault();

                this.changePasswordButtonContainer.classList.add('d-none');
                this.passwordContainer.classList.remove('d-none');
                this.plainPasswordTextField.mdcTextField.disabled = false;
                this.plainPasswordTextField.mdcTextField.required = true;
            });
        }

        this.portalNameTextField.mdcTextField.required = true;
        this.hostnameTextField.mdcTextField.required = true;
        this.ftpProtocolSelect.mdcSelect.required = true;
        this.usernameTextField.mdcTextField.required = true;

        if (document.querySelector('#' + idPrefix + 'change_password_button_container') === null) {
            this.plainPasswordTextField.mdcTextField.required = true;
        }
    }

    public isFormValid(): boolean {
        let hasErrors: boolean = false;

        if (this.portalNameTextField.mdcTextField.valid === false) {
            this.portalNameTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (this.hostnameTextField.mdcTextField.valid === false) {
            this.hostnameTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (this.usernameTextField.mdcTextField.valid === false) {
            this.usernameTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (this.plainPasswordTextField.mdcTextField.valid === false && this.plainPasswordTextField.mdcTextField.required === true) {
            this.plainPasswordTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        return (hasErrors === false);
    }

}

export { OpenImmoFtpCredentialForm };
