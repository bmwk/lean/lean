import { DeleteForm } from '../../../Form/DeleteForm';

class SurveyResultDeleteForm extends DeleteForm {

    constructor(idPrefix: string) {
        super(idPrefix);
    }

}

export { SurveyResultDeleteForm };
