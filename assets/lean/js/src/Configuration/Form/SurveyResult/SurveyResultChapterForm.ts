import { TextFieldComponent } from '../../../Component/TextFieldComponent';

class SurveyResultChapterForm {

    private readonly titleTextField: TextFieldComponent;
    private readonly descriptionTextField: TextFieldComponent;

    constructor(idPrefix: string) {
        this.titleTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'title_mdc_text_field'));
        this.descriptionTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'description_mdc_text_field'), {autoFitTextarea: true});

        this.titleTextField.mdcTextField.required = true;
    }

    public isFormValid(): boolean {
        let hasErrors: boolean = false;

        if (this.titleTextField.mdcTextField.valid === false) {
            this.titleTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        return (hasErrors === false);
    }

}

export { SurveyResultChapterForm };
