import { DeleteForm } from '../../../Form/DeleteForm';

class SurveyResultChapterDeleteForm extends DeleteForm {

    constructor(idPrefix: string) {
        super(idPrefix);
    }

}

export { SurveyResultChapterDeleteForm };
