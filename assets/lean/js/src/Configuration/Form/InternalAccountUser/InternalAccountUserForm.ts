import { TextFieldComponent } from '../../../Component/TextFieldComponent';
import { CheckboxFieldComponent } from '../../../Component/CheckboxFieldComponent';
import { FormFieldValidator } from '../../../FormFieldValidator/FormFieldValidator';

class InternalAccountUserForm {

    private readonly fullNameTextField: TextFieldComponent;
    private readonly nameAbbreviationTextField: TextFieldComponent;
    private readonly phoneNumberTextField: TextFieldComponent;
    private readonly emailTextField: TextFieldComponent;
    private readonly usernameTextField: TextFieldComponent;
    private readonly roleCheckboxFields: CheckboxFieldComponent[] = [];

    constructor(idPrefix: string) {
        this.fullNameTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'fullName_mdc_text_field'));
        this.nameAbbreviationTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'nameAbbreviation_mdc_text_field'));
        this.phoneNumberTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'phoneNumber_mdc_text_field'));
        this.emailTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'email_mdc_text_field'));
        this.usernameTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'username_mdc_text_field'));

        document.querySelectorAll('div.mdc-form-field[id^=' + idPrefix + 'roles_]').forEach((element: HTMLDivElement) => {
            this.roleCheckboxFields.push(new CheckboxFieldComponent(element));
        });

        this.fullNameTextField.mdcTextField.required = true;
        this.emailTextField.mdcTextField.required = true;
        this.usernameTextField.mdcTextField.required = true;
    }

    public isFormValid(): boolean {
        let hasErrors: boolean = false;

        if (this.fullNameTextField.mdcTextField.valid === false) {
            this.fullNameTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (FormFieldValidator.validateEmail(this.emailTextField.mdcTextField.value) === false) {
            this.emailTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (this.usernameTextField.mdcTextField.valid === false) {
            this.usernameTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        return (hasErrors === false);
    }

    public isRoleSelected(): boolean {
        let hasAtLeastOneCheckedField: boolean = false;

        this.roleCheckboxFields.forEach((checkboxField: CheckboxFieldComponent): void => {
            if (checkboxField.mdcCheckbox.checked === true) {
                hasAtLeastOneCheckedField = true;
            }
        });

        return hasAtLeastOneCheckedField;
    }

}

export { InternalAccountUserForm };
