import { SearchFieldComponent } from '../../../Component/SearchFieldComponent';

class InternalAccountUserSearchForm {

    private readonly searchFieldComponent: SearchFieldComponent;

    constructor(idPrefix: string) {
        this.searchFieldComponent = new SearchFieldComponent(idPrefix, 'text');
    }

}

export { InternalAccountUserSearchForm };
