import { TextFieldComponent } from '../../../Component/TextFieldComponent';

class LayoutForm {

    private readonly primaryColorTextField: TextFieldComponent;
    private readonly secondaryColorTextField: TextFieldComponent;

    constructor(idPrefix: string) {
        this.primaryColorTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'primaryColor_mdc_text_field'));
        this.secondaryColorTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'secondaryColor_mdc_text_field'));
    }

}

export { LayoutForm };
