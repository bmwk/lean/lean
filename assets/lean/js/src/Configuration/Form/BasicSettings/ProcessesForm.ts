import { MDCSwitch } from '@material/switch';

class ProcessesForm {

    private readonly allowProviderRegistrationMdcSwitch: MDCSwitch;
    private readonly allowProviderRegistrationInputField: HTMLInputElement;
    private readonly allowSeekerRegistrationMdcSwitch: MDCSwitch;
    private readonly allowSeekerRegistrationInputField: HTMLInputElement;


    constructor(idPrefix: string) {
        this.allowProviderRegistrationMdcSwitch = new MDCSwitch(document.querySelector('#' + idPrefix + 'allow_provider_registration_mdc_switch'));
        this.allowSeekerRegistrationMdcSwitch = new MDCSwitch(document.querySelector('#' + idPrefix + 'allow_seeker_registration_mdc_switch'));
        this.allowProviderRegistrationInputField = document.querySelector('#' + idPrefix + 'allowProviderRegistration');
        this.allowSeekerRegistrationInputField = document.querySelector('#' + idPrefix + 'allowSeekerRegistration');

        this.setAllowProviderRegistrationState();
        this.setAllowSeekerRegistrationState();

        this.allowProviderRegistrationMdcSwitch.listen('click', (): void => {
            if (this.allowProviderRegistrationMdcSwitch.selected === true) {
                this.allowProviderRegistrationInputField.value = '1';
            } else {
                this.allowProviderRegistrationInputField.value = '0';
            }
        });

        this.allowSeekerRegistrationMdcSwitch.listen('click', (): void => {
            if (this.allowSeekerRegistrationMdcSwitch.selected === true) {
                this.allowSeekerRegistrationInputField.value = '1';
            } else {
                this.allowSeekerRegistrationInputField.value = '0';
            }
        });

    }

    private setAllowProviderRegistrationState(): void {
        this.allowProviderRegistrationMdcSwitch.selected = this.allowProviderRegistrationInputField.value === '1';
    }

    private setAllowSeekerRegistrationState(): void {
        this.allowSeekerRegistrationMdcSwitch.selected = this.allowSeekerRegistrationInputField.value === '1';
    }

}

export { ProcessesForm };
