import { DeleteForm } from '../../../../Form/DeleteForm';

class CityExposeDeleteForm extends DeleteForm {

    constructor(idPrefix: string) {
        super(idPrefix);
    }

}

export { CityExposeDeleteForm };
