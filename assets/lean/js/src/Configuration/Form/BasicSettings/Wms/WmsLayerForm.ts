import { TextFieldComponent } from '../../../../Component/TextFieldComponent';

class WmsLayerForm {

    private readonly titleTextField: TextFieldComponent;

    constructor(idPrefix: string) {
        this.titleTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'title_mdc_text_field'));

        this.titleTextField.mdcTextField.required = true;
    }

    public isFormValid(): boolean {
        let hasErrors: boolean = false;

        if (this.titleTextField.mdcTextField.valid === false) {
            this.titleTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        return (hasErrors === false);
    }

}

export { WmsLayerForm };
