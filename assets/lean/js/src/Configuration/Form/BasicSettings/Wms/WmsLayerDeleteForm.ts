import { DeleteForm } from '../../../../Form/DeleteForm';

class WmsLayerDeleteForm extends DeleteForm {

    constructor(idPrefix: string) {
        super(idPrefix);
    }

}

export { WmsLayerDeleteForm };
