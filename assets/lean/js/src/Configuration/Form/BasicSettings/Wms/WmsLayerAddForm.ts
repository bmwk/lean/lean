import { TextFieldComponent } from '../../../../Component/TextFieldComponent';

class WmsLayerAddForm {

    private readonly capabilitiesUrlTextField: TextFieldComponent;

    constructor(idPrefix: string) {
        this.capabilitiesUrlTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'capabilitiesUrl_mdc_text_field'));

        this.capabilitiesUrlTextField.mdcTextField.required = true;
    }

    public isFormValid(): boolean {
        let hasErrors: boolean = false;

        if (this.capabilitiesUrlTextField.mdcTextField.valid === false) {
            this.capabilitiesUrlTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        return (hasErrors === false);
    }

}

export { WmsLayerAddForm };
