import { TextFieldComponent } from '../../../Component/TextFieldComponent';

class HallOfInspirationConfigurationForm {

    private readonly accessTokenTextField: TextFieldComponent;

    constructor(idPrefix: string) {
        this.accessTokenTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'accessToken_mdc_text_field'));
    }

}

export { HallOfInspirationConfigurationForm };
