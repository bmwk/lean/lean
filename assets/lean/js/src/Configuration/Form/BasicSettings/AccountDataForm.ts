import { TextFieldComponent } from '../../../Component/TextFieldComponent';
import { FormFieldValidator } from '../../../FormFieldValidator/FormFieldValidator';

class AccountDataForm {

    private readonly nameTextField: TextFieldComponent;
    private readonly streetNameTextField: TextFieldComponent;
    private readonly houseNumberTextField: TextFieldComponent;
    private readonly postalCodeTextField: TextFieldComponent;
    private readonly placeNameTextField: TextFieldComponent;
    private readonly phoneNumberTextField: TextFieldComponent;
    private readonly emailTextField: TextFieldComponent;
    private readonly websiteTextField: TextFieldComponent;

    constructor(idPrefix: string) {
        this.nameTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'name_mdc_text_field'));
        this.streetNameTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'streetName_mdc_text_field'));
        this.houseNumberTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'houseNumber_mdc_text_field'));
        this.postalCodeTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'postalCode_mdc_text_field'));
        this.placeNameTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'placeName_mdc_text_field'));
        this.phoneNumberTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'phoneNumber_mdc_text_field'));
        this.emailTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'email_mdc_text_field'));
        this.websiteTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'website_mdc_text_field'));

        this.nameTextField.mdcTextField.required = true;
        this.emailTextField.mdcTextField.required = true;
    }

    public isFormValid(): boolean {
        let hasErrors: boolean = false;

        if (this.nameTextField.mdcTextField.valid === false) {
            this.nameTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (FormFieldValidator.validateEmail(this.emailTextField.mdcTextField.value) === false) {
            this.emailTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        return (hasErrors === false);
    }

}

export { AccountDataForm };
