import { TextFieldComponent } from '../../../Component/TextFieldComponent';

class ZielbildcheckConfigurationForm {

    private readonly accessTokenTextField: TextFieldComponent;

    constructor(idPrefix: string) {
        this.accessTokenTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'accessToken_mdc_text_field'));
    }

}

export { ZielbildcheckConfigurationForm };
