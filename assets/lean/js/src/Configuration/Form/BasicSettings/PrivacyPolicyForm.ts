import { RichTextEditorComponent } from "../../../Component/RichTextEditorComponent";
import { TextFieldComponent } from "../../../Component/TextFieldComponent";

class PrivacyPolicyForm {

    private readonly privacyPolicyProviderRichTextEditorComponent: RichTextEditorComponent;
    private readonly privacyPolicySeekerRichTextEditorComponent: RichTextEditorComponent;
    private readonly pdfExposeDisclaimerTextField: TextFieldComponent;

    constructor(idPrefix: string) {

        const privacyPolicyProviderTextAreaElement: HTMLTextAreaElement = document.querySelector('#' + idPrefix + 'privacyPolicyProvider_text_area');
        this.privacyPolicyProviderRichTextEditorComponent = new RichTextEditorComponent(privacyPolicyProviderTextAreaElement);
        const privacyPolicySeekerTextAreaElement: HTMLTextAreaElement = document.querySelector('#' + idPrefix + 'privacyPolicySeeker_text_area');
        this.privacyPolicySeekerRichTextEditorComponent = new RichTextEditorComponent(privacyPolicySeekerTextAreaElement);
        this.pdfExposeDisclaimerTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'pdfExposeDisclaimer_mdc_text_field'), {'autoFitTextarea': true});
    }

    public doBeforeSubmit(): void {
        this.privacyPolicyProviderRichTextEditorComponent.doBeforeSubmit();
        this.privacyPolicySeekerRichTextEditorComponent.doBeforeSubmit();
    }

    public isFormValid(): boolean {
        let hasErrors: boolean = false;

        if (this.privacyPolicyProviderRichTextEditorComponent.isInputEmpty() === true && this.privacyPolicySeekerRichTextEditorComponent.isInputEmpty() === true && this.pdfExposeDisclaimerTextField.mdcTextField.value == '') {
            hasErrors = true;
        }

        return (hasErrors === false);
    }

}

export { PrivacyPolicyForm };
