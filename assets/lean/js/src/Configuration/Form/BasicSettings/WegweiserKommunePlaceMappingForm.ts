import { TextFieldComponent } from '../../../Component/TextFieldComponent';

class WegweiserKommunePlaceMappingForm {

    private readonly wegweiserKommuneRegionIdTextField: TextFieldComponent;
    private readonly wegweiserKommuneRegionFriendlyUrlTextField: TextFieldComponent;

    constructor(idPrefix: string) {
        this.wegweiserKommuneRegionIdTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'wegweiserKommuneRegionId_mdc_text_field'));
        this.wegweiserKommuneRegionFriendlyUrlTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'wegweiserKommuneRegionFriendlyUrl_mdc_text_field'));
    }

}

export { WegweiserKommunePlaceMappingForm };
