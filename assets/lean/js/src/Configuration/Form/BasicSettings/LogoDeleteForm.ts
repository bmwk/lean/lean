import { DeleteForm } from '../../../Form/DeleteForm';

class LogoDeleteForm extends DeleteForm {

    constructor(idPrefix: string) {
        super(idPrefix);
    }

}

export { LogoDeleteForm };
