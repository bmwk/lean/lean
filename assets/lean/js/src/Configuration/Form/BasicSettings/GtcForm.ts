import { RichTextEditorComponent } from "../../../Component/RichTextEditorComponent";

class GtcForm {

    private readonly gtcProviderRichTextEditorComponent: RichTextEditorComponent;
    private readonly gtcSeekerRichTextEditorComponent: RichTextEditorComponent;

    constructor(idPrefix: string) {

        const gtcProviderTextAreaElement: HTMLTextAreaElement = document.querySelector('#' + idPrefix + 'generalTermsAndConditionsProvider_text_area');
        this.gtcProviderRichTextEditorComponent = new RichTextEditorComponent(gtcProviderTextAreaElement);
        const gtcSeekerTextAreaElement: HTMLTextAreaElement = document.querySelector('#' + idPrefix + 'generalTermsAndConditionsSeeker_text_area');
        this.gtcSeekerRichTextEditorComponent = new RichTextEditorComponent(gtcSeekerTextAreaElement);

    }

    public doBeforeSubmit(): void {
        this.gtcProviderRichTextEditorComponent.doBeforeSubmit();
        this.gtcSeekerRichTextEditorComponent.doBeforeSubmit();
    }

    public isFormValid(): boolean {
        let hasErrors: boolean = false;

        if (this.gtcProviderRichTextEditorComponent.isInputEmpty() === true && this.gtcSeekerRichTextEditorComponent.isInputEmpty() === true) {
            hasErrors = true;
        }

        return (hasErrors === false);
    }

}

export { GtcForm };
