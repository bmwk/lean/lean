import { RichTextEditorComponent } from "../../../Component/RichTextEditorComponent";

class PrivacyPolicyForm {

    private readonly richTextEditorComponent: RichTextEditorComponent;

    constructor(idPrefix: string) {

        const privacyPolicyTextAreaElement: HTMLTextAreaElement = document.querySelector('#' + idPrefix + 'privacyPolicy_text_area');
        this.richTextEditorComponent = new RichTextEditorComponent(privacyPolicyTextAreaElement);

    }

    public doBeforeSubmit(): void {
        this.richTextEditorComponent.doBeforeSubmit();
    }

    public isFormValid(): boolean {
        let hasErrors: boolean = false;

        if (this.richTextEditorComponent.isInputEmpty() === true) {
            hasErrors = true;
        }

        return (hasErrors === false);
    }

}

export { PrivacyPolicyForm };
