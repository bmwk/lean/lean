import { TextFieldComponent } from '../../../Component/TextFieldComponent';

class LayoutForm {

    private readonly firstColorTextField: TextFieldComponent;
    private readonly secondaryColorTextField: TextFieldComponent;

    constructor(idPrefix: string) {
        this.firstColorTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'firstColor_mdc_text_field'));
        this.secondaryColorTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'secondaryColor_mdc_text_field'));
    }

}

export { LayoutForm };
