import { MDCFormField } from '@material/form-field';

class MandatoryFieldsForm {

    private readonly buildingConditionMdcFormField: MDCFormField;
    private readonly plotSizeMdcFormField: MDCFormField;
    private readonly areaSizeMdcFormField: MDCFormField;
    private readonly retailSpaceMdcFormField: MDCFormField;
    private readonly livingSpaceMdcFormField: MDCFormField;
    private readonly gastronomySpaceMdcFormField: MDCFormField;
    private readonly propertyDescriptionMdcFormField: MDCFormField;
    private readonly quarterPlaceMdcFormField: MDCFormField;
    private readonly placeDescriptionMdcFormField: MDCFormField;
    private readonly salutationMdcFormField: MDCFormField;
    private readonly firstnameMdcFormField: MDCFormField;
    private readonly phoneNumberMdcFormField: MDCFormField;

    constructor(idPrefix: string) {
        this.buildingConditionMdcFormField = new MDCFormField(document.querySelector('#' + idPrefix + 'requiredPropertyInformationFormFields_0_mdc_form_field'));
        this.plotSizeMdcFormField = new MDCFormField(document.querySelector('#' + idPrefix + 'requiredPropertyInformationFormFields_1_mdc_form_field'));
        this.areaSizeMdcFormField = new MDCFormField(document.querySelector('#' + idPrefix + 'requiredPropertyInformationFormFields_2_mdc_form_field'));
        this.retailSpaceMdcFormField = new MDCFormField(document.querySelector('#' + idPrefix + 'requiredPropertyInformationFormFields_3_mdc_form_field'));
        this.livingSpaceMdcFormField = new MDCFormField(document.querySelector('#' + idPrefix + 'requiredPropertyInformationFormFields_4_mdc_form_field'));
        this.gastronomySpaceMdcFormField = new MDCFormField(document.querySelector('#' + idPrefix + 'requiredPropertyInformationFormFields_5_mdc_form_field'));
        this.propertyDescriptionMdcFormField = new MDCFormField(document.querySelector('#' + idPrefix + 'requiredPropertyInformationFormFields_6_mdc_form_field'));
        this.quarterPlaceMdcFormField = new MDCFormField(document.querySelector('#' + idPrefix + 'requiredPropertyLocationFormFields_0_mdc_form_field'));
        this.placeDescriptionMdcFormField = new MDCFormField(document.querySelector('#' + idPrefix + 'requiredPropertyLocationFormFields_1_mdc_form_field'));
        this.salutationMdcFormField = new MDCFormField(document.querySelector('#' + idPrefix + 'requiredReporterFormFields_0_mdc_form_field'));
        this.firstnameMdcFormField = new MDCFormField(document.querySelector('#' + idPrefix + 'requiredReporterFormFields_1_mdc_form_field'));
        this.phoneNumberMdcFormField = new MDCFormField(document.querySelector('#' + idPrefix + 'requiredReporterFormFields_2_mdc_form_field'));
    }
}

export { MandatoryFieldsForm };
