import { MDCFormField } from '@material/form-field';

class IndustryClassificationForm {

    private readonly industryClassificationFormElement: HTMLFormElement;
    private readonly industryClassificationFormFields: MDCFormField[];

    constructor(idPrefix: string) {
        this.industryClassificationFormElement = document.querySelector('#' + idPrefix + 'form');
        this.industryClassificationFormFields = [];

        this.industryClassificationFormElement.querySelectorAll('.mdc-form-field').forEach((element: HTMLElement): void => {
            this.industryClassificationFormFields.push(new MDCFormField(element));
        });
    }
}

export { IndustryClassificationForm };
