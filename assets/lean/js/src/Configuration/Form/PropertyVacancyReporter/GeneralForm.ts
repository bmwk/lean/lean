import { TextFieldComponent } from '../../../Component/TextFieldComponent';

class GeneralForm {

    private readonly reportNotificationEmailAddressTextField: TextFieldComponent;
    private readonly titleTextField: TextFieldComponent;
    private readonly textTextField: TextFieldComponent;

    constructor(idPrefix: string) {
        this.reportNotificationEmailAddressTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'reportNotificationEmail_mdc_text_field'));
        this.titleTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'title_mdc_text_field'));
        this.textTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'text_mdc_text_field'), {autoFitTextarea: true});
    }

}

export { GeneralForm };
