import { ListComponent } from '../../../Component/ListComponent';

class WmsLayerListComponent extends ListComponent {

    constructor(idPrefix: string) {
        super(idPrefix);
    }

}

export { WmsLayerListComponent };
