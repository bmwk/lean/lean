import { ListComponent } from '../../../Component/ListComponent';

class InternalAccountUserListComponent extends ListComponent {

    constructor(idPrefix: string) {
        super(idPrefix);
    }

}

export { InternalAccountUserListComponent };
