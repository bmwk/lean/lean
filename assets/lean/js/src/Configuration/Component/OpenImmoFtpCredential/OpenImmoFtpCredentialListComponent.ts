import { ListComponent } from '../../../Component/ListComponent';

class OpenImmoFtpCredentialListComponent extends ListComponent {

    constructor(idPrefix: string) {
        super(idPrefix);
    }

}

export { OpenImmoFtpCredentialListComponent };
