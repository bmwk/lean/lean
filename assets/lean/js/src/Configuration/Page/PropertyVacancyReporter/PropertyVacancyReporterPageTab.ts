enum PropertyVacancyReporterPageTab {
    General = 'property_vacancy_reporter_configuration_general_tab',
    PropertyUsage = 'property_vacancy_reporter_configuration_property_usage_tab',
    MandatoryFields = 'property_vacancy_reporter_configuration_mandatory_fields_tab',
    PrivacyPolicy = 'property_vacancy_reporter_configuration_privacy_policy_tab',
    Layout = 'property_vacancy_reporter_configuration_layout_tab',
    Preview = 'property_vacancy_reporter_configuration_preview_tab',
    EmbedCode = 'property_vacancy_reporter_configuration_code_generator_tab',
    Faq = 'property_vacancy_reporter_configuration_faq_tab',
}

export { PropertyVacancyReporterPageTab };
