import { PropertyVacancyReporterPage } from './PropertyVacancyReporterPage';
import { PropertyVacancyReporterPageTab } from './PropertyVacancyReporterPageTab';
import { MandatoryFieldsForm } from '../../Form/PropertyVacancyReporter/MandatoryFieldsForm';

class MandatoryFieldsPage extends PropertyVacancyReporterPage {

    private readonly mandatoryFieldsForm: MandatoryFieldsForm;
    private readonly mandatoryFieldsFormElement: HTMLFormElement;
    private readonly mandatoryFieldsFormSubmitButton: HTMLButtonElement;

    constructor() {
        super(PropertyVacancyReporterPageTab.MandatoryFields);

        const idPrefix: string = 'mandatory_fields_';

        this.mandatoryFieldsForm = new MandatoryFieldsForm('mandatory_fields_');
        this.mandatoryFieldsFormElement = document.querySelector('#' + idPrefix + 'form');
        this.mandatoryFieldsFormSubmitButton = document.querySelector('#' + idPrefix + 'save_submit_button');

        this.mandatoryFieldsFormSubmitButton.addEventListener('click', (): void => {
            this.mandatoryFieldsFormElement.submit();
        });
    }

}

export { MandatoryFieldsPage };
