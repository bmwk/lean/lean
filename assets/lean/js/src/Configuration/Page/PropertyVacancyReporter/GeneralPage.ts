import { PropertyVacancyReporterPage } from './PropertyVacancyReporterPage';
import { PropertyVacancyReporterPageTab } from './PropertyVacancyReporterPageTab';
import { GeneralForm } from '../../Form/PropertyVacancyReporter/GeneralForm';

class GeneralPage extends PropertyVacancyReporterPage {

    private readonly generalForm: GeneralForm;
    private readonly generalFormElement: HTMLFormElement;
    private readonly generalFormSubmitButton: HTMLButtonElement;

    constructor() {
        super(PropertyVacancyReporterPageTab.General);

        const idPrefix: string = 'general_';

        this.generalForm = new GeneralForm(idPrefix);
        this.generalFormElement = document.querySelector('#' + idPrefix + 'form');
        this.generalFormSubmitButton = document.querySelector('#' + idPrefix + 'save_submit_button');

        this.generalFormSubmitButton.addEventListener('click', (): void => {
            this.generalFormElement.submit();
        });
    }

}

export { GeneralPage };
