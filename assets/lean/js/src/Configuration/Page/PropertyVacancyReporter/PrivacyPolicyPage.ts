import { PropertyVacancyReporterPage } from './PropertyVacancyReporterPage';
import { PropertyVacancyReporterPageTab } from './PropertyVacancyReporterPageTab';
import { PrivacyPolicyForm } from '../../Form/PropertyVacancyReporter/PrivacyPolicyForm';
import { MessageBoxComponent } from '../../../Component/MessageBoxComponent/MessageBoxComponent';
import { MessageBoxAlertType } from '../../../Component/MessageBoxComponent/MessageBoxAlertType';

class PrivacyPolicyPage extends PropertyVacancyReporterPage {

    private readonly privacyPolicyForm: PrivacyPolicyForm;
    private readonly privacyPolicyFormElement: HTMLFormElement;
    private readonly privacyPolicyFormSubmitButton: HTMLButtonElement;
    private readonly messageBoxComponent: MessageBoxComponent;

    constructor() {
        super(PropertyVacancyReporterPageTab.PrivacyPolicy);

        const idPrefix: string = 'privacy_policy_';

        this.privacyPolicyForm = new PrivacyPolicyForm(idPrefix);
        this.privacyPolicyFormElement = document.querySelector('#' + idPrefix + 'form');
        this.privacyPolicyFormSubmitButton = document.querySelector('#' + idPrefix + 'save_submit_button');
        this.messageBoxComponent = new MessageBoxComponent();

        this.privacyPolicyFormSubmitButton.addEventListener('click', (): void => {
            if (this.privacyPolicyForm.isFormValid() === false) {
                this.messageBoxComponent.openMessageBox('Die Daten konnten nicht gespeichert werden. Der Datenschutztext darf nicht leer sein.\'', MessageBoxAlertType.DANGER);
                return;
            }

            this.privacyPolicyForm.doBeforeSubmit();
            this.privacyPolicyFormElement.submit();
        });
    }

}

export { PrivacyPolicyPage };
