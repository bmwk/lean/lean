import { PropertyVacancyReporterPageTab } from './PropertyVacancyReporterPageTab';
import { MdcTabBarPage } from '../../../MdcTabBarPage';

abstract class PropertyVacancyReporterPage extends MdcTabBarPage {

    protected constructor(propertyVacancyReporterPageTab: PropertyVacancyReporterPageTab) {
        super(document.querySelector('#property_vacancy_reporter_configuration_tab_bar'));

        this.activateTab(propertyVacancyReporterPageTab);
    }

}

export { PropertyVacancyReporterPage };
