import { PropertyVacancyReporterPage } from './PropertyVacancyReporterPage';
import { PropertyVacancyReporterPageTab } from './PropertyVacancyReporterPageTab';

class PreviewPage extends PropertyVacancyReporterPage {

    constructor() {
        super(PropertyVacancyReporterPageTab.Preview);
    }

}

export { PreviewPage };
