import { PropertyVacancyReporterPage } from './PropertyVacancyReporterPage';
import { PropertyVacancyReporterPageTab } from './PropertyVacancyReporterPageTab';
import { LayoutForm } from '../../Form/PropertyVacancyReporter/LayoutForm';

class LayoutPage extends PropertyVacancyReporterPage {

    private readonly layoutForm: LayoutForm;
    private readonly layoutFormElement: HTMLFormElement;
    private readonly layoutFormSubmitButton: HTMLButtonElement;

    constructor() {
        super(PropertyVacancyReporterPageTab.Layout);

        const idPrefix: string = 'layout_';

        this.layoutForm = new LayoutForm(idPrefix);
        this.layoutFormElement = document.querySelector('#' + idPrefix + 'form');
        this.layoutFormSubmitButton = document.querySelector('#' + idPrefix + 'save_submit_button');

        this.layoutFormSubmitButton.addEventListener('click', (): void => {
            this.layoutFormElement.submit();
        });
    }

}

export { LayoutPage };
