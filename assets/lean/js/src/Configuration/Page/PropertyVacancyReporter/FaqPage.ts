import { PropertyVacancyReporterPage } from './PropertyVacancyReporterPage';
import { PropertyVacancyReporterPageTab } from './PropertyVacancyReporterPageTab';

class FaqPage extends PropertyVacancyReporterPage {

    constructor() {
        super(PropertyVacancyReporterPageTab.Faq);
    }

}

export { FaqPage };
