import { PropertyVacancyReporterPage } from './PropertyVacancyReporterPage';
import { PropertyVacancyReporterPageTab } from './PropertyVacancyReporterPageTab';
import { IndustryClassificationForm } from '../../Form/PropertyVacancyReporter/IndustryClassificationForm';

class PropertyUsagePage extends PropertyVacancyReporterPage {

    private readonly industryClassificationForm: IndustryClassificationForm;
    private readonly industryClassificationFormElement: HTMLFormElement;
    private readonly industryClassificationFormSubmitButton: HTMLButtonElement;

    constructor() {
        super(PropertyVacancyReporterPageTab.PropertyUsage);

        const idPrefix: string = 'industry_classification_';

        this.industryClassificationForm = new IndustryClassificationForm(idPrefix);
        this.industryClassificationFormElement = document.querySelector('#' + idPrefix + 'form');
        this.industryClassificationFormSubmitButton = document.querySelector('#' + idPrefix + 'save_submit_button');

        this.industryClassificationFormSubmitButton.addEventListener('click', (): void => {
            this.industryClassificationFormElement.submit();
        });
    }

}

export { PropertyUsagePage };
