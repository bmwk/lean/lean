import { InternalAccountUserForm } from '../../Form/InternalAccountUser/InternalAccountUserForm';
import { AccountUserDeleteForm } from '../../Form/InternalAccountUser/AccountUserDeleteForm';

class DeletePage {

    private readonly accountUserDeleteForm: AccountUserDeleteForm;
    private readonly accountUserDeleteFormElement: HTMLFormElement;
    private readonly accountUserDeleteFormSubmitButton: HTMLButtonElement;
    private readonly internalAccountUserForm: InternalAccountUserForm

    constructor() {
        const idPrefix: string = 'account_user_delete_';

        this.accountUserDeleteForm = new AccountUserDeleteForm(idPrefix);
        this.accountUserDeleteFormElement = document.querySelector('#' + idPrefix + 'form');
        this.accountUserDeleteFormSubmitButton = document.querySelector('#' + idPrefix + 'delete_submit_button');
        this.internalAccountUserForm = new InternalAccountUserForm('internal_account_user_');

        this.accountUserDeleteFormSubmitButton.addEventListener('click', (): void => {
            this.accountUserDeleteFormElement.submit();
        });
    }

}

export { DeletePage };
