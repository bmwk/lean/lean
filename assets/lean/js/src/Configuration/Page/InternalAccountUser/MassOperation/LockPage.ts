import { MassOperationPage } from '../MassOperationPage';

class LockPage extends MassOperationPage {

    constructor() {
        const idPrefix: string = 'account_user_mass_operation_lock_';

        super(
            document.querySelector('#' + idPrefix + 'form'),
            document.querySelector('#' + idPrefix + 'lock_submit_button')
        );
    }

    protected doOnConfirmationFormSubmit(): void {
        this.confirmationFormElement.submit();
    }

}

export { LockPage };
