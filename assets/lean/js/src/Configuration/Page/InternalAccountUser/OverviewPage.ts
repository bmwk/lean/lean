import { InternalAccountUserFilterForm } from '../../Form/InternalAccountUser/InternalAccountUserFilterForm';
import { InternalAccountUserSearchForm } from '../../Form/InternalAccountUser/InternalAccountUserSearchForm';
import { InternalAccountUserListComponent } from '../../Component/InternalAccountUser/InternalAccountUserListComponent';

class OverviewPage {

    private readonly internalAccountUserFilterForm: InternalAccountUserFilterForm;
    private readonly internalAccountUserSearchForm: InternalAccountUserSearchForm;
    private readonly internalAccountUserListComponent: InternalAccountUserListComponent;

    constructor() {
        this.internalAccountUserFilterForm = new InternalAccountUserFilterForm('internal_account_user_filter_');
        this.internalAccountUserSearchForm = new InternalAccountUserSearchForm('internal_account_user_search_');

        if (InternalAccountUserListComponent.checkContainerExists('internal_account_user_overview_') === true) {
            this.internalAccountUserListComponent = new InternalAccountUserListComponent('internal_account_user_overview_');
        }
    }

}

export { OverviewPage };
