import { InternalAccountUserForm } from '../../Form/InternalAccountUser/InternalAccountUserForm';
import { MdcContextMenuComponent } from '../../../Component/MdcContextMenuComponent';
import { MessageBoxComponent } from '../../../Component/MessageBoxComponent/MessageBoxComponent';
import { MessageBoxAlertType } from '../../../Component/MessageBoxComponent/MessageBoxAlertType';

class CreatePage {

    private readonly internalAccountUserForm: InternalAccountUserForm;
    private readonly internalAccountUserFormElement: HTMLFormElement;
    private readonly internalAccountUserFormSubmitButton: HTMLButtonElement;
    private readonly userRolesContextMenu: MdcContextMenuComponent;
    private readonly messageBoxComponent: MessageBoxComponent;

    constructor() {
        const idPrefix: string = 'internal_account_user_';

        this.internalAccountUserForm = new InternalAccountUserForm(idPrefix);
        this.internalAccountUserFormElement = document.querySelector('#' + idPrefix + 'form');
        this.internalAccountUserFormSubmitButton = document.querySelector('#' + idPrefix + 'save_submit_button');
        this.userRolesContextMenu = new MdcContextMenuComponent('user_roles_overview_document_');
        this.messageBoxComponent = new MessageBoxComponent();

        this.internalAccountUserFormSubmitButton.addEventListener('click', (): void => {
            if (this.internalAccountUserForm.isFormValid() === false) {
                this.messageBoxComponent.openMessageBox('Die Daten konnten nicht gespeichert werden. Bitte kontrollieren Sie die rot markierten Felder.', MessageBoxAlertType.DANGER);

                return;
            }

            if (this.internalAccountUserForm.isRoleSelected() === false) {
                this.messageBoxComponent.openMessageBox('Mindestens eine Benutzerrolle muss ausgewählt sein.', MessageBoxAlertType.DANGER);

                return;
            }

            this.internalAccountUserFormElement.submit();
        });
    }

}

export { CreatePage };
