import { TextFieldComponent } from '../../../Component/TextFieldComponent';
import { MessageBoxComponent } from '../../../Component/MessageBoxComponent/MessageBoxComponent';
import { MessageBoxAlertType } from '../../../Component/MessageBoxComponent/MessageBoxAlertType';

class FtpUserPage {

    private readonly ftpHostnameTextField: TextFieldComponent;
    private readonly ftpUsernameTextField: TextFieldComponent;
    private readonly ftpPasswordTextField: TextFieldComponent;
    private readonly ftpHostnameCopyButton: HTMLAnchorElement
    private readonly ftpUsernameCopyButton: HTMLAnchorElement;
    private readonly ftpPasswordCopyButton: HTMLAnchorElement;
    private readonly messageBoxComponent: MessageBoxComponent;

    constructor() {
        const idPrefix: string = 'ftp_user_data_';

        const ftpHostnameTextFieldElement = document.querySelector('#' + idPrefix + 'ftpHostname_mdc_text_field');
        const ftpUsernameTextFieldElement = document.querySelector('#' + idPrefix + 'ftpUsername_mdc_text_field');
        const ftpPasswordTextFieldElement = document.querySelector('#' + idPrefix + 'ftpPassword_mdc_text_field');

        this.messageBoxComponent = new MessageBoxComponent();

        if (ftpHostnameTextFieldElement !== null) {
            this.ftpHostnameTextField = new TextFieldComponent(ftpHostnameTextFieldElement);
        }

        if (ftpUsernameTextFieldElement !== null) {
            this.ftpUsernameTextField = new TextFieldComponent(ftpUsernameTextFieldElement);
        }

        if (ftpPasswordTextFieldElement !== null) {
            this.ftpPasswordTextField = new TextFieldComponent(ftpPasswordTextFieldElement);
        }

        if (document.querySelector('#copy_' + idPrefix + 'ftpHostname') !== null) {
            this.ftpHostnameCopyButton = document.querySelector('#copy_' + idPrefix + 'ftpHostname');

            this.ftpHostnameCopyButton.addEventListener('click', (): void => {
                this.copyDataToClipboard(this.ftpHostnameTextField, 'Der Hostname');
            });
        }

        if (document.querySelector('#copy_' + idPrefix + 'ftpUsername') !== null) {
            this.ftpUsernameCopyButton = document.querySelector('#copy_' + idPrefix + 'ftpUsername');

            this.ftpUsernameCopyButton.addEventListener('click', (): void => {
                this.copyDataToClipboard(this.ftpUsernameTextField, 'Der Benutzername');
            });
        }

        if (document.querySelector('#copy_' + idPrefix + 'ftpPassword') !== null) {
            this.ftpPasswordCopyButton = document.querySelector('#copy_' + idPrefix + 'ftpPassword');

            this.ftpPasswordCopyButton.addEventListener('click', (): void => {
                this.copyDataToClipboard(this.ftpPasswordTextField, 'Das Passwort');
            });
        }
    }

    private copyDataToClipboard(textField: TextFieldComponent, title: string): void {
        const textToCopy = textField.mdcTextField.value;

        if (!navigator.clipboard) {
            const textArea: HTMLTextAreaElement = document.createElement('textarea');

            textArea.value = textToCopy;
            textArea.style.position = 'fixed';
            textArea.className = 'd-none';

            document.body.appendChild(textArea);

            textArea.focus();
            textArea.select();

            try {
                document.execCommand('copy');

                this.messageBoxComponent.openMessageBox(title + ' wurde in die Zwischenablage kopiert.', MessageBoxAlertType.SUCCESS);
            } catch (err) {
                this.messageBoxComponent.openMessageBox(title + ' konnte leider nicht in die Zwischenablage kopiert werden.', MessageBoxAlertType.DANGER);
            }

            document.body.removeChild(textArea)
            document.execCommand('copy');

            this.messageBoxComponent.openMessageBox(title + ' wurde in die Zwischenablage kopiert.', MessageBoxAlertType.SUCCESS);
        } else {
            navigator.clipboard.writeText(textToCopy).then((): void => {
                this.messageBoxComponent.openMessageBox(title + ' wurde in die Zwischenablage kopiert.', MessageBoxAlertType.SUCCESS);
            }, (): void => {
                this.messageBoxComponent.openMessageBox(title + ' konnte leider nicht in die Zwischenablage kopiert werden.', MessageBoxAlertType.DANGER);
            });
        }
    }

}

export { FtpUserPage };
