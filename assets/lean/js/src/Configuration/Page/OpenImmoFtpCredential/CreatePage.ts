import { OpenImmoFtpCredentialForm } from '../../Form/OpenImmoFtpCredential/OpenImmoFtpCredentialForm';
import { MessageBoxComponent } from '../../../Component/MessageBoxComponent/MessageBoxComponent';
import { MessageBoxAlertType } from '../../../Component/MessageBoxComponent/MessageBoxAlertType';

class CreatePage {

    private readonly openImmoFtpCredentialForm: OpenImmoFtpCredentialForm;
    private readonly openImmoFtpCredentialFormElement: HTMLFormElement;
    private readonly openImmoFtpCredentialFormSubmitButton: HTMLButtonElement;
    private readonly messageBoxComponent: MessageBoxComponent;

    constructor() {
        const idPrefix: string = 'open_immo_ftp_credential_';

        this.openImmoFtpCredentialForm = new OpenImmoFtpCredentialForm(idPrefix);
        this.openImmoFtpCredentialFormElement = document.querySelector('#' + idPrefix + 'form');
        this.openImmoFtpCredentialFormSubmitButton = document.querySelector('#' + idPrefix + 'save_submit_button');
        this.messageBoxComponent = new MessageBoxComponent();

        this.openImmoFtpCredentialFormSubmitButton.addEventListener('click', (): void => {
            if (this.openImmoFtpCredentialForm.isFormValid() === false) {
                this.messageBoxComponent.openMessageBox('Die Daten konnten nicht gespeichert werden. Bitte kontrollieren Sie die rot markierten Felder.', MessageBoxAlertType.DANGER);

                return;
            }

            this.openImmoFtpCredentialFormElement.submit();
        });
    }

}

export { CreatePage };
