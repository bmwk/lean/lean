import { OpenImmoFtpCredentialDeleteForm } from '../../Form/OpenImmoFtpCredential/OpenImmoFtpCredentialDeleteForm';

class DeletePage{

    private readonly openImmoFtpCredentialDeleteForm: OpenImmoFtpCredentialDeleteForm;
    private readonly openImmoFtpCredentialDeleteFormElement: HTMLFormElement;
    private readonly openImmoFtpCredentialDeleteFormSubmitButton: HTMLButtonElement;

    constructor() {
        const idPrefix: string = 'open_immo_ftp_credential_delete_';

        this.openImmoFtpCredentialDeleteForm = new OpenImmoFtpCredentialDeleteForm(idPrefix);
        this.openImmoFtpCredentialDeleteFormElement = document.querySelector('#' + idPrefix + 'form');
        this.openImmoFtpCredentialDeleteFormSubmitButton = document.querySelector('#' + idPrefix + 'delete_submit_button');

        this.openImmoFtpCredentialDeleteFormSubmitButton.addEventListener('click', (): void => {
            this.openImmoFtpCredentialDeleteFormElement.submit();
        });
    }

}

export { DeletePage };
