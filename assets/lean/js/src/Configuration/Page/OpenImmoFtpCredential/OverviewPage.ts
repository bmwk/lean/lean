import { OpenImmoFtpCredentialListComponent } from '../../Component/OpenImmoFtpCredential/OpenImmoFtpCredentialListComponent';

class OverviewPage {

    private readonly openImmoFtpCredentialListComponent: OpenImmoFtpCredentialListComponent;

    constructor() {
        this.openImmoFtpCredentialListComponent = new OpenImmoFtpCredentialListComponent('open_immo_ftp_credential_');
    }

}

export { OverviewPage };
