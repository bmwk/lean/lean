import { LookingForPropertyRequestReporterPage } from './LookingForPropertyRequestReporterPage';
import { LookingForPropertyRequestReporterPageTab } from './LookingForPropertyRequestReporterPageTab';
import { GeneralForm } from '../../Form/LookingForPropertyRequestReporter/GeneralForm';
import { MessageBoxComponent } from '../../../Component/MessageBoxComponent/MessageBoxComponent';
import { MessageBoxAlertType } from '../../../Component/MessageBoxComponent/MessageBoxAlertType';

class GeneralPage extends LookingForPropertyRequestReporterPage {

    private readonly generalForm: GeneralForm;
    private readonly generalFormElement: HTMLFormElement;
    private readonly generalFormSubmitButton: HTMLButtonElement;
    private readonly messageBoxComponent: MessageBoxComponent;

    constructor() {
        super(LookingForPropertyRequestReporterPageTab.General);

        const idPrefix: string = 'general_';

        this.generalForm = new GeneralForm(idPrefix);
        this.generalFormElement = document.querySelector('#' + idPrefix + 'form');
        this.generalFormSubmitButton = document.querySelector('#' + idPrefix + 'save_submit_button');
        this.messageBoxComponent = new MessageBoxComponent();

        this.generalFormSubmitButton.addEventListener('click', (): void => {
            if (this.generalForm.isFormValid() === false) {
                this.messageBoxComponent.openMessageBox('Die Daten konnten nicht gespeichert werden. Bitte kontrollieren Sie die rot markierten Felder.', MessageBoxAlertType.DANGER);

                return;
            }

            this.generalFormElement.submit();
        });
    }

}

export { GeneralPage };
