import { LookingForPropertyRequestReporterPage } from './LookingForPropertyRequestReporterPage';
import { LookingForPropertyRequestReporterPageTab } from './LookingForPropertyRequestReporterPageTab';

class PreviewPage extends LookingForPropertyRequestReporterPage {

    constructor() {
        super(LookingForPropertyRequestReporterPageTab.Preview);
    }

}

export { PreviewPage };
