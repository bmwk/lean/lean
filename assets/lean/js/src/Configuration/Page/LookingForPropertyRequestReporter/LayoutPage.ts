import { LookingForPropertyRequestReporterPage } from './LookingForPropertyRequestReporterPage';
import { LookingForPropertyRequestReporterPageTab } from './LookingForPropertyRequestReporterPageTab';
import { LayoutForm } from '../../Form/LookingForPropertyRequestReporter/LayoutForm';

class LayoutPage extends LookingForPropertyRequestReporterPage {

    private readonly layoutForm: LayoutForm;
    private readonly layoutFormElement: HTMLFormElement;
    private readonly layoutFormSubmitButton: HTMLButtonElement;

    constructor() {
        super(LookingForPropertyRequestReporterPageTab.Layout);

        const idPrefix: string = 'layout_';

        this.layoutForm = new LayoutForm(idPrefix);
        this.layoutFormElement = document.querySelector('#' + idPrefix + 'form');
        this.layoutFormSubmitButton = document.querySelector('#' + idPrefix + 'save_submit_button');

        this.layoutFormSubmitButton.addEventListener('click', (): void => {
            this.layoutFormElement.submit();
        });
    }

}

export { LayoutPage };
