import { LookingForPropertyRequestReporterPage } from './LookingForPropertyRequestReporterPage';
import { LookingForPropertyRequestReporterPageTab } from './LookingForPropertyRequestReporterPageTab';

class EmbedCodePage extends LookingForPropertyRequestReporterPage {

    private readonly codeTextarea: HTMLTextAreaElement;
    private readonly codeCopyButton: HTMLButtonElement;

    constructor() {
        super(LookingForPropertyRequestReporterPageTab.EmbedCode);

        this.codeTextarea = document.querySelector('textarea[name="htmlCodeText"]');
        this.codeCopyButton = document.querySelector('button[name="htmlCodeCopy"]');

        this.activateCodeCopyButton();
    }

    private activateCodeCopyButton(): void {
        this.codeCopyButton.addEventListener('click', (): void => {
            if (!navigator.clipboard) {
                this.codeTextarea.select();
                document.execCommand('copy');
                this.codeCopiedEvent(true);
            } else {
                navigator.clipboard.writeText(this.codeTextarea.value).then((): void => {
                    this.codeCopiedEvent(true);
                }, (): void => {
                    this.codeCopiedEvent(false);
                });
            }
        });
    }

    private codeCopiedEvent(success: boolean): void {
        if (success) {
            this.codeCopyButton.textContent = 'Kopiert';
        } else {
            this.codeCopyButton.textContent = 'Fehler: Nicht kopiert';
        }

        setTimeout((): void => {
            this.codeCopyButton.textContent = 'Code kopieren';
        }, 1500);
    }

}

export { EmbedCodePage };
