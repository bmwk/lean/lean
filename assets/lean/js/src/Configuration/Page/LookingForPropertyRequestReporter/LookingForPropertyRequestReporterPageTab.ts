enum LookingForPropertyRequestReporterPageTab {
    General = 'looking_for_property_request_reporter_configuration_general_tab',
    PropertyUsage = 'looking_for_property_request_reporter_configuration_property_usage_tab',
    MandatoryFields = 'looking_for_property_request_reporter_configuration_mandatory_fields_tab',
    PrivacyPolicy = 'looking_for_property_request_reporter_configuration_privacy_policy_tab',
    Layout = 'looking_for_property_request_reporter_configuration_layout_tab',
    Preview = 'looking_for_property_request_reporter_configuration_preview_tab',
    EmbedCode = 'looking_for_property_request_reporter_configuration_code_generator_tab',
    Faq = 'looking_for_property_request_reporter_configuration_faq_tab',
}

export { LookingForPropertyRequestReporterPageTab };
