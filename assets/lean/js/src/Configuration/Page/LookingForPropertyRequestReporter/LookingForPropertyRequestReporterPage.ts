import { LookingForPropertyRequestReporterPageTab } from './LookingForPropertyRequestReporterPageTab';
import { MdcTabBarPage } from '../../../MdcTabBarPage';

abstract class LookingForPropertyRequestReporterPage extends MdcTabBarPage {

    protected constructor(lookingForPropertyRequestReporterPageTab: LookingForPropertyRequestReporterPageTab) {
        super(document.querySelector('#looking_for_property_request_reporter_configuration_tab_bar'));

        this.activateTab(lookingForPropertyRequestReporterPageTab);
    }

}

export { LookingForPropertyRequestReporterPage };
