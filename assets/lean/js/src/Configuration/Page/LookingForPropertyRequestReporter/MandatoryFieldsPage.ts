import { LookingForPropertyRequestReporterPage } from './LookingForPropertyRequestReporterPage';
import { LookingForPropertyRequestReporterPageTab } from './LookingForPropertyRequestReporterPageTab';
import { MandatoryFieldsForm } from '../../Form/LookingForPropertyRequestReporter/MandatoryFieldsForm';

class MandatoryFieldsPage extends LookingForPropertyRequestReporterPage {

    private readonly mandatoryFieldsForm: MandatoryFieldsForm;
    private readonly mandatoryFieldsFormElement: HTMLFormElement;
    private readonly mandatoryFieldsFormSubmitButton: HTMLButtonElement;

    constructor() {
        super(LookingForPropertyRequestReporterPageTab.MandatoryFields);

        const idPrefix: string = 'mandatory_fields_';

        this.mandatoryFieldsForm = new MandatoryFieldsForm('mandatory_fields_');
        this.mandatoryFieldsFormElement = document.querySelector('#' + idPrefix + 'form');
        this.mandatoryFieldsFormSubmitButton = document.querySelector('#' + idPrefix + 'save_submit_button');

        this.mandatoryFieldsFormSubmitButton.addEventListener('click', (): void => {
            this.mandatoryFieldsFormElement.submit();
        });
    }

}

export { MandatoryFieldsPage };
