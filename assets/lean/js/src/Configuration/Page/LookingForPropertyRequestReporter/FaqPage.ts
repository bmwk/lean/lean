import { LookingForPropertyRequestReporterPage } from './LookingForPropertyRequestReporterPage';
import { LookingForPropertyRequestReporterPageTab } from './LookingForPropertyRequestReporterPageTab';

class FaqPage extends LookingForPropertyRequestReporterPage {

    constructor() {
        super(LookingForPropertyRequestReporterPageTab.Faq);
    }

}

export { FaqPage };
