import { LookingForPropertyRequestReporterPage } from './LookingForPropertyRequestReporterPage';
import { LookingForPropertyRequestReporterPageTab } from './LookingForPropertyRequestReporterPageTab';
import { IndustryClassificationForm } from '../../Form/LookingForPropertyRequestReporter/IndustryClassificationForm';

class PropertyUsagePage extends LookingForPropertyRequestReporterPage {

    private readonly industryClassificationForm: IndustryClassificationForm;
    private readonly industryClassificationFormElement: HTMLFormElement;
    private readonly industryClassificationFormSubmitButton: HTMLButtonElement;

    constructor() {
        super(LookingForPropertyRequestReporterPageTab.PropertyUsage);

        const idPrefix: string = 'industry_classification_';

        this.industryClassificationForm = new IndustryClassificationForm(idPrefix);
        this.industryClassificationFormElement = document.querySelector('#' + idPrefix + 'form');
        this.industryClassificationFormSubmitButton = document.querySelector('#' + idPrefix + 'save_submit_button');

        this.industryClassificationFormSubmitButton.addEventListener('click', (): void => {
            this.industryClassificationFormElement.submit();
        });
    }

}

export { PropertyUsagePage };
