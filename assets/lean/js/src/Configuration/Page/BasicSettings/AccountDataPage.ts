import { BasicSettingsPage } from './BasicSettingsPage';
import { BasicSettingsPageTab } from './BasicSettingsPageTab';
import { AccountDataForm } from '../../Form/BasicSettings/AccountDataForm';
import { MessageBoxComponent } from '../../../Component/MessageBoxComponent/MessageBoxComponent';
import { MessageBoxAlertType } from '../../../Component/MessageBoxComponent/MessageBoxAlertType';

class AccountDataPage extends BasicSettingsPage {

    private readonly accountDataForm: AccountDataForm;
    private readonly accountDataFormElement: HTMLFormElement;
    private readonly accountDataFormSubmitButton: HTMLButtonElement;
    private readonly messageBoxComponent: MessageBoxComponent;

    constructor() {
        super(BasicSettingsPageTab.AccountData);

        const idPrefix: string = 'account_data_';

        this.accountDataForm = new AccountDataForm(idPrefix);
        this.accountDataFormElement = document.querySelector('#' + idPrefix + 'form');
        this.accountDataFormSubmitButton = document.querySelector('#' + idPrefix + 'save_submit_button');
        this.messageBoxComponent = new MessageBoxComponent();

        this.accountDataFormSubmitButton.addEventListener('click', (): void => {
            if (this.accountDataForm.isFormValid() === false) {
                this.messageBoxComponent.openMessageBox('Die Daten konnten nicht gespeichert werden. Bitte kontrollieren Sie die rot markierten Felder.', MessageBoxAlertType.DANGER);

                return;
            }

            this.accountDataFormElement.submit();
        });
    }

}

export { AccountDataPage };
