import { BasicSettingsPage } from './BasicSettingsPage';
import { BasicSettingsPageTab } from './BasicSettingsPageTab';
import { ImprintForm } from '../../Form/BasicSettings/ImprintForm';
import { MessageBoxComponent } from '../../../Component/MessageBoxComponent/MessageBoxComponent';
import { MessageBoxAlertType } from '../../../Component/MessageBoxComponent/MessageBoxAlertType';

class ImprintPage extends BasicSettingsPage {

    private readonly imprintForm: ImprintForm;
    private readonly imprintFormElement: HTMLFormElement;
    private readonly imprintFormSubmitButton: HTMLButtonElement;
    private readonly messageBoxComponent: MessageBoxComponent;

    constructor() {
        super(BasicSettingsPageTab.Imprint);

        const idPrefix: string = 'imprint_';

        this.imprintForm = new ImprintForm(idPrefix);
        this.imprintFormElement = document.querySelector('#' + idPrefix + 'form');
        this.imprintFormSubmitButton = document.querySelector('#' + idPrefix + 'save_submit_button');
        this.messageBoxComponent = new MessageBoxComponent();

        this.imprintFormSubmitButton.addEventListener('click', (): void => {
            this.imprintForm.doBeforeSubmit();
            if (this.imprintForm.isFormValid() === false) {
                this.messageBoxComponent.openMessageBox('Die Daten konnten nicht gespeichert werden. Bitte geben Sie eine Erklärung zum Datenschutz ein.', MessageBoxAlertType.DANGER);

                return;
            }

            this.imprintFormElement.submit();
        });
    }

}

export { ImprintPage };
