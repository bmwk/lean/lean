import { BasicSettingsPage } from './BasicSettingsPage';
import { BasicSettingsPageTab } from './BasicSettingsPageTab';
import { PrivacyPolicyForm } from '../../Form/BasicSettings/PrivacyPolicyForm';
import { MessageBoxComponent } from '../../../Component/MessageBoxComponent/MessageBoxComponent';
import { MessageBoxAlertType } from '../../../Component/MessageBoxComponent/MessageBoxAlertType';

class PrivacyPolicyPage extends BasicSettingsPage {

    private readonly privacyPolicyForm: PrivacyPolicyForm;
    private readonly privacyPolicyFormElement: HTMLFormElement;
    private readonly privacyPolicyFormSubmitButton: HTMLButtonElement;
    private readonly messageBoxComponent: MessageBoxComponent;

    constructor() {
        super(BasicSettingsPageTab.PrivacyPolicy);

        const idPrefix: string = 'privacy_policy_';

        this.privacyPolicyForm = new PrivacyPolicyForm(idPrefix);
        this.privacyPolicyFormElement = document.querySelector('#' + idPrefix + 'form');
        this.privacyPolicyFormSubmitButton = document.querySelector('#' + idPrefix + 'save_submit_button');
        this.messageBoxComponent = new MessageBoxComponent();

        this.privacyPolicyFormSubmitButton.addEventListener('click', (): void => {
            this.privacyPolicyForm.doBeforeSubmit();

            if (this.privacyPolicyForm.isFormValid() === false) {
                this.messageBoxComponent.openMessageBox('Die Daten konnten nicht gespeichert werden. Bitte geben Sie eine Erklärung zum Datenschutz ein.', MessageBoxAlertType.DANGER);

                return;
            }

            this.privacyPolicyFormElement.submit();
        });
    }

}

export { PrivacyPolicyPage };
