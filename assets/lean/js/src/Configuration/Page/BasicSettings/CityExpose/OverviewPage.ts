import { BasicSettingsPage } from '../BasicSettingsPage';
import { BasicSettingsPageTab } from '../BasicSettingsPageTab';
import { DropzoneUploadComponent } from '../../../../Component/DropzoneUploadComponent';
import { MdcContextMenuComponent } from '../../../../Component/MdcContextMenuComponent';

class OverviewPage extends BasicSettingsPage {

    private readonly dropzoneUploadComponent: DropzoneUploadComponent;
    private readonly contextMenu: MdcContextMenuComponent;

    constructor() {
        super(BasicSettingsPageTab.CityExpose);

        const idPrefix: string = 'city_expose_';

        const acceptedFiles: string[] = [
            'application/pdf',
        ];

        this.dropzoneUploadComponent = new DropzoneUploadComponent(
            document.querySelector('#city_expose_pdf_upload_dropzone_container'),
            document.querySelector('#city_expose_pdf_upload_submit_button'),
            'cityExposePdfFile',
            acceptedFiles
        );

        if (MdcContextMenuComponent.checkContainerExists(idPrefix) === true) {
            this.contextMenu = new MdcContextMenuComponent(idPrefix);
        }
    }

}

export { OverviewPage };
