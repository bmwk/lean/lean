import { CityExposeDeleteForm } from '../../../Form/BasicSettings/CityExpose/CityExposeDeleteForm'

class DeletePage {

    private readonly cityExposeDeleteForm: CityExposeDeleteForm;
    private readonly cityExposeDeleteFormElement: HTMLFormElement;
    private readonly cityExposeDeleteFormSubmitButton: HTMLButtonElement;

    constructor() {
        const idPrefix: string = 'city_expose_delete_';

        this.cityExposeDeleteForm = new CityExposeDeleteForm(idPrefix);
        this.cityExposeDeleteFormElement = document.querySelector('#' + idPrefix + 'form');
        this.cityExposeDeleteFormSubmitButton = document.querySelector('#' + idPrefix + 'delete_submit_button');

        this.cityExposeDeleteFormSubmitButton.addEventListener('click', (): void => {
            this.cityExposeDeleteFormElement.submit();
        });
    }

}

export { DeletePage };
