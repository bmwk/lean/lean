enum BasicSettingsPageTab {
    AccountData = 'basic_settings_configuration_account_data_tab',
    Imprint = 'basic_settings_configuration_imprint_tab',
    PrivacyPolicy = 'basic_settings_configuration_privacy_policy_tab',
    Gtc = 'basic_settings_configuration_gtc_tab',
    Layout = 'basic_settings_configuration_layout_tab',
    CityExpose = 'basic_settings_configuration_city_expose_tab',
    Processes = 'basic_settings_configuration__processes_tab',
    Interfaces = 'basic_settings_configuration_interfaces_tab',
    Wms = 'basic_settings_configuration_wms_tab',
}

export { BasicSettingsPageTab };
