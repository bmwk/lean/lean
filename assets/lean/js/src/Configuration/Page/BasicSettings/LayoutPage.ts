import { BasicSettingsPage } from './BasicSettingsPage';
import { BasicSettingsPageTab } from './BasicSettingsPageTab';
import { LayoutForm } from '../../Form/BasicSettings/LayoutForm';
import { DropzoneUploadComponent } from '../../../Component/DropzoneUploadComponent';
import { MdcContextMenuComponent } from '../../../Component/MdcContextMenuComponent';

class LayoutPage extends BasicSettingsPage {

    private readonly layoutForm: LayoutForm;
    private readonly layoutFormElement: HTMLFormElement;
    private readonly layoutFormSubmitButton: HTMLButtonElement;
    private readonly dropzoneUploadComponent: DropzoneUploadComponent;
    private readonly contextMenu: MdcContextMenuComponent;

    constructor() {
        super(BasicSettingsPageTab.Layout);

        const idPrefix: string = 'layout_';

        this.layoutForm = new LayoutForm(idPrefix);
        this.layoutFormElement = document.querySelector('#' + idPrefix + 'form');
        this.layoutFormSubmitButton = document.querySelector('#' + idPrefix + 'save_submit_button');

        const acceptedFiles: string[] = [
            'image/jpeg',
            'image/png',
            'image/svg+xml',
        ];

        this.dropzoneUploadComponent = new DropzoneUploadComponent(
            document.querySelector('#layout_logo_image_upload_dropzone_container'),
            document.querySelector('#layout_logo_image_upload_submit_button'),
            'logoImageFile',
            acceptedFiles
        );

        if (MdcContextMenuComponent.checkContainerExists(idPrefix) === true) {
            this.contextMenu = new MdcContextMenuComponent(idPrefix);
        }

        this.layoutFormSubmitButton.addEventListener('click', (): void => {
            this.layoutFormElement.submit();
        });
    }

}

export { LayoutPage };
