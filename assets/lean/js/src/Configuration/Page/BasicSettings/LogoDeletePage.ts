import { LogoDeleteForm } from '../../Form/BasicSettings/LogoDeleteForm';

class LogoDeletePage {

    private readonly logoDeleteForm: LogoDeleteForm;
    private readonly logoDeleteFormElement: HTMLFormElement;
    private readonly logoDeleteFormSubmitButton: HTMLButtonElement;

    constructor() {
        const idPrefix: string = 'logo_delete_';

        this.logoDeleteForm = new LogoDeleteForm(idPrefix);
        this.logoDeleteFormElement = document.querySelector('#' + idPrefix + 'form');
        this.logoDeleteFormSubmitButton = document.querySelector('#' + idPrefix + 'delete_submit_button');

        this.logoDeleteFormSubmitButton.addEventListener('click', (): void => {
            this.logoDeleteFormElement.submit();
        });
    }

}

export { LogoDeletePage };
