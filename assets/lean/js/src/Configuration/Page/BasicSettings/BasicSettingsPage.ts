import { BasicSettingsPageTab } from './BasicSettingsPageTab';
import { MdcTabBarPage } from '../../../MdcTabBarPage';

abstract class BasicSettingsPage extends MdcTabBarPage{

    protected constructor(basicSettingsPageTab: BasicSettingsPageTab) {
        super(document.querySelector('#basic_settings_configuration_tab_bar'));

        this.activateTab(basicSettingsPageTab);
    }

}

export { BasicSettingsPage };
