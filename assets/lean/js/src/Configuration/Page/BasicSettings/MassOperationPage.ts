import { MessageBoxComponent } from '../../../Component/MessageBoxComponent/MessageBoxComponent';
import { BasicSettingsPage } from "./BasicSettingsPage";
import {BasicSettingsPageTab} from "./BasicSettingsPageTab";

abstract class MassOperationPage extends BasicSettingsPage {

    protected readonly confirmationFormElement: HTMLFormElement;
    protected readonly confirmationFormSubmitButton: HTMLButtonElement;
    protected readonly messageBoxComponent: MessageBoxComponent;

    protected constructor(confirmationFormElement: HTMLFormElement, confirmationFormButton: HTMLButtonElement) {
        super(BasicSettingsPageTab.Wms);

        this.confirmationFormElement = confirmationFormElement;
        this.confirmationFormSubmitButton = confirmationFormButton;
        this.messageBoxComponent = new MessageBoxComponent();

        this.confirmationFormSubmitButton.addEventListener('click', (event: MouseEvent): void => {
            event.preventDefault();
            this.doOnConfirmationFormSubmit();
        });
    }

    protected abstract doOnConfirmationFormSubmit(): void;

}

export { MassOperationPage };
