import { BasicSettingsPage } from './BasicSettingsPage';
import { BasicSettingsPageTab } from './BasicSettingsPageTab';
import { ProcessesForm } from '../../Form/BasicSettings/ProcessesForm';

class ProcessesPage extends BasicSettingsPage {

    private readonly processesForm: ProcessesForm;
    private readonly processesFormElement: HTMLFormElement;
    private readonly processesFormSubmitButton: HTMLButtonElement;

    constructor() {
        super(BasicSettingsPageTab.Processes);

        const idPrefix: string = 'processes_';

        this.processesForm = new ProcessesForm(idPrefix);
        this.processesFormElement = document.querySelector('#' + idPrefix + 'form');
        this.processesFormSubmitButton = document.querySelector('#' + idPrefix + 'save_submit_button');

        this.processesFormSubmitButton.addEventListener('click', (): void => {
            this.processesFormElement.submit();
        });
    }

}

export { ProcessesPage };
