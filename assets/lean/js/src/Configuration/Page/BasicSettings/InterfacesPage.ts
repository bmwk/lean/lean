import { BasicSettingsPage } from './BasicSettingsPage';
import { BasicSettingsPageTab } from './BasicSettingsPageTab';
import { HystreetConfigurationForm } from '../../Form/BasicSettings/HystreetConfigurationForm';
import { ZielbildcheckConfigurationForm } from '../../Form/BasicSettings/ZielbildcheckConfigurationForm';
import { WegweiserKommunePlaceMappingForm } from '../../Form/BasicSettings/WegweiserKommunePlaceMappingForm';
import { HallOfInspirationConfigurationForm } from "../../Form/BasicSettings/HallOfInspirationConfigurationForm";

class InterfacesPage extends BasicSettingsPage {

    private readonly hallOfInspirationConfigurationForm: HallOfInspirationConfigurationForm;
    private readonly hystreetConfigurationForm: HystreetConfigurationForm;
    private readonly zielbildcheckConfigurationForm: ZielbildcheckConfigurationForm;
    private readonly wegweiserKommunePlaceMappingForm: WegweiserKommunePlaceMappingForm;

    constructor() {
        super(BasicSettingsPageTab.Interfaces);

        this.hallOfInspirationConfigurationForm = new HallOfInspirationConfigurationForm('hall_of_inspiration_configuration_');
        this.hystreetConfigurationForm = new HystreetConfigurationForm('hystreet_configuration_');
        this.zielbildcheckConfigurationForm = new ZielbildcheckConfigurationForm('zielbildcheck_configuration_');
        this.wegweiserKommunePlaceMappingForm = new WegweiserKommunePlaceMappingForm('wegweiser_kommune_place_mapping_');
    }

}

export { InterfacesPage };
