import { BasicSettingsPage } from './BasicSettingsPage';
import { BasicSettingsPageTab } from './BasicSettingsPageTab';
import { GtcForm } from '../../Form/BasicSettings/GtcForm';
import { MessageBoxComponent } from '../../../Component/MessageBoxComponent/MessageBoxComponent';
import { MessageBoxAlertType } from '../../../Component/MessageBoxComponent/MessageBoxAlertType';

class GtcPage extends BasicSettingsPage {

    private readonly gtcForm: GtcForm;
    private readonly gtcFormElement: HTMLFormElement;
    private readonly gtcFormSubmitButton: HTMLButtonElement;
    private readonly messageBoxComponent: MessageBoxComponent;

    constructor() {
        super(BasicSettingsPageTab.Gtc);

        const idPrefix: string = 'gtc_';

        this.gtcForm = new GtcForm(idPrefix);
        this.gtcFormElement = document.querySelector('#' + idPrefix + 'form');
        this.gtcFormSubmitButton = document.querySelector('#' + idPrefix + 'save_submit_button');
        this.messageBoxComponent = new MessageBoxComponent();

        this.gtcFormSubmitButton.addEventListener('click', (): void => {
            this.gtcForm.doBeforeSubmit();

            if (this.gtcForm.isFormValid() === false) {
                this.messageBoxComponent.openMessageBox('Die Daten konnten nicht gespeichert werden. Bitte geben Sie die Nutzungsbedingungen ein.', MessageBoxAlertType.DANGER);

                return;
            }

            this.gtcFormElement.submit();
        });

    }

}

export { GtcPage };
