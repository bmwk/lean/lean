import { BasicSettingsPage } from '../BasicSettingsPage';
import { BasicSettingsPageTab } from '../BasicSettingsPageTab';
import { ChooseLayersForm } from '../../../Form/BasicSettings/Wms/ChooseLayersForm';
import { MessageBoxComponent } from '../../../../Component/MessageBoxComponent/MessageBoxComponent';
import { MessageBoxAlertType } from '../../../../Component/MessageBoxComponent/MessageBoxAlertType';

class ChooseLayersPage extends BasicSettingsPage {

    private readonly chooseLayersForm: ChooseLayersForm;
    private readonly chooseLayersFormElement: HTMLFormElement;
    private readonly chooseLayersFormSubmitButton: HTMLButtonElement;
    private readonly messageBoxComponent: MessageBoxComponent;

    constructor() {
        super(BasicSettingsPageTab.Wms);

        const idPrefix: string = 'choose_layers_';

        this.chooseLayersForm = new ChooseLayersForm(idPrefix);
        this.chooseLayersFormElement = document.querySelector('#' + idPrefix + 'form');
        this.chooseLayersFormSubmitButton = document.querySelector('#' + idPrefix + 'save_submit_button');
        this.messageBoxComponent = new MessageBoxComponent();

        this.chooseLayersFormSubmitButton.addEventListener('click', (): void => {
            if (this.chooseLayersForm.isFormValid() === false) {
                this.messageBoxComponent.openMessageBox('Sie haben keine Bezeichnung angegeben.', MessageBoxAlertType.DANGER);

                return;
            }

            this.chooseLayersFormElement.submit();
        });
    }

}

export { ChooseLayersPage };
