import { BasicSettingsPage } from '../BasicSettingsPage';
import { BasicSettingsPageTab } from '../BasicSettingsPageTab';
import { WmsLayerAddForm } from '../../../Form/BasicSettings/Wms/WmsLayerAddForm';
import { TextFieldComponent } from '../../../../Component/TextFieldComponent';
import { WmsLayerListComponent } from '../../../Component/BasicSettings/WmsLayerListComponent';
import { MessageBoxComponent } from '../../../../Component/MessageBoxComponent/MessageBoxComponent';
import { MessageBoxAlertType } from '../../../../Component/MessageBoxComponent/MessageBoxAlertType';

class OverviewPage extends BasicSettingsPage {

    private readonly capabilitiesUrlVersionOneDotOneTextField: TextFieldComponent;
    private readonly capabilitiesUrlVersionOneDotThreeTextField: TextFieldComponent;
    private readonly capabilitiesUrlVersionOneDotOneCopyButton: HTMLAnchorElement
    private readonly capabilitiesUrlVersionOneDotThreeCopyButton: HTMLAnchorElement;
    private readonly wmsLayerAddForm: WmsLayerAddForm;
    private readonly wmsLayerAddFormElement: HTMLFormElement;
    private readonly wmsLayerAddFormSubmitButton: HTMLButtonElement;
    private readonly wmsLayerListComponent: WmsLayerListComponent;
    private readonly messageBoxComponent: MessageBoxComponent;

    constructor() {
        super(BasicSettingsPageTab.Wms);

        const idPrefix: string = 'wms_layer_add_';

        const capabilitiesUrlVersionOneDotOneTextFieldElement = document.querySelector('#wms_capabilities_url_capabilitiesUrlVersionOneDotOne_mdc_text_field');
        const capabilitiesUrlVersionOneDotThreeTextFieldElement = document.querySelector('#wms_capabilities_url_capabilitiesUrlVersionOneDotThree_mdc_text_field');

        if (capabilitiesUrlVersionOneDotOneTextFieldElement !== null) {
            this.capabilitiesUrlVersionOneDotOneTextField = new TextFieldComponent(capabilitiesUrlVersionOneDotOneTextFieldElement);
            this.capabilitiesUrlVersionOneDotOneCopyButton = document.querySelector('#copy_wms_capabilities_url_capabilitiesUrlVersionOneDotOne');

            this.capabilitiesUrlVersionOneDotOneCopyButton.addEventListener('click', (): void => {
                this.copyDataToClipboard(this.capabilitiesUrlVersionOneDotOneTextField);
            });
        }

        if (capabilitiesUrlVersionOneDotThreeTextFieldElement !== null) {
            this.capabilitiesUrlVersionOneDotThreeTextField = new TextFieldComponent(capabilitiesUrlVersionOneDotThreeTextFieldElement);
            this.capabilitiesUrlVersionOneDotThreeCopyButton = document.querySelector('#copy_wms_capabilities_url_capabilitiesUrlVersionOneDotThree');

            this.capabilitiesUrlVersionOneDotThreeCopyButton.addEventListener('click', (): void => {
                this.copyDataToClipboard(this.capabilitiesUrlVersionOneDotThreeTextField);
            });
        }

        this.wmsLayerAddForm = new WmsLayerAddForm(idPrefix);
        this.wmsLayerAddFormElement = document.querySelector('#' + idPrefix + 'form');
        this.wmsLayerAddFormSubmitButton = document.querySelector('#' + idPrefix + 'add_submit_button');

        if (WmsLayerListComponent.checkContainerExists('wms_layer_') === true) {
            this.wmsLayerListComponent = new WmsLayerListComponent('wms_layer_');
        }

        this.messageBoxComponent = new MessageBoxComponent();

        this.wmsLayerAddFormSubmitButton.addEventListener('click', (): void => {
            if (this.wmsLayerAddForm.isFormValid() === false) {
                this.messageBoxComponent.openMessageBox('Sie haben keine Capabiliteis-URL angegeben.', MessageBoxAlertType.DANGER);

                return;
            }

            this.wmsLayerAddFormElement.submit();
        });
    }

    private copyDataToClipboard(textField: TextFieldComponent): void {
        const textToCopy = textField.mdcTextField.value;

        if (!navigator.clipboard) {
            const textArea: HTMLTextAreaElement = document.createElement('textarea');

            textArea.value = textToCopy;
            textArea.style.position = 'fixed';
            textArea.className = 'd-none';

            document.body.appendChild(textArea);

            textArea.focus();
            textArea.select();

            try {
                document.execCommand('copy');

                this.messageBoxComponent.openMessageBox('Die Capabilities-URL wurde in die Zwischenablage kopiert.', MessageBoxAlertType.SUCCESS);
            } catch (err) {
                this.messageBoxComponent.openMessageBox('Die Capabilities-URL konnte leider nicht in die Zwischenablage kopiert werden.', MessageBoxAlertType.DANGER);
            }

            document.body.removeChild(textArea)
            document.execCommand('copy');

            this.messageBoxComponent.openMessageBox('Die Capabilities-URL wurde in die Zwischenablage kopiert.', MessageBoxAlertType.SUCCESS);
        } else {
            navigator.clipboard.writeText(textToCopy).then((): void => {
                this.messageBoxComponent.openMessageBox('Die Capabilities-URL wurde in die Zwischenablage kopiert.', MessageBoxAlertType.SUCCESS);
            }, (): void => {
                this.messageBoxComponent.openMessageBox('Die Capabilities-URL konnte leider nicht in die Zwischenablage kopiert werden.', MessageBoxAlertType.DANGER);
            });
        }
    }

}

export { OverviewPage };
