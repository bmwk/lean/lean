import { BasicSettingsPage } from '../BasicSettingsPage';
import { BasicSettingsPageTab } from '../BasicSettingsPageTab';
import { WmsLayerDeleteForm } from '../../../Form/BasicSettings/Wms/WmsLayerDeleteForm';

class DeletePage extends BasicSettingsPage {

    private readonly wmsLayerDeleteForm: WmsLayerDeleteForm;
    private readonly wmsLayerDeleteFormElement: HTMLFormElement;
    private readonly wmsLayerDeleteFormSubmitButton: HTMLButtonElement;

    constructor() {
        super(BasicSettingsPageTab.Wms);

        const idPrefix: string = 'wms_layer_delete_';

        this.wmsLayerDeleteForm = new WmsLayerDeleteForm(idPrefix);
        this.wmsLayerDeleteFormElement = document.querySelector('#' + idPrefix + 'form');
        this.wmsLayerDeleteFormSubmitButton = document.querySelector('#' + idPrefix + 'delete_submit_button');

        this.wmsLayerDeleteFormSubmitButton.addEventListener('click', (): void => {
            this.wmsLayerDeleteFormElement.submit();
        });
    }

}

export { DeletePage };
