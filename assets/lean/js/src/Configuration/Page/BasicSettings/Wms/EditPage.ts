import { BasicSettingsPage } from '../BasicSettingsPage';
import { BasicSettingsPageTab } from '../BasicSettingsPageTab';
import { WmsLayerForm } from '../../../Form/BasicSettings/Wms/WmsLayerForm';
import { MdcContextMenuComponent } from '../../../../Component/MdcContextMenuComponent';
import { MessageBoxComponent } from '../../../../Component/MessageBoxComponent/MessageBoxComponent';
import { MessageBoxAlertType } from '../../../../Component/MessageBoxComponent/MessageBoxAlertType';

class EditPage extends BasicSettingsPage {

    private readonly wmsLayerForm: WmsLayerForm;
    private readonly wmsLayerFormElement: HTMLFormElement;
    private readonly wmsLayerFormSubmitButton: HTMLButtonElement;
    private readonly contextMenu: MdcContextMenuComponent;
    private readonly messageBoxComponent: MessageBoxComponent;

    constructor() {
        super(BasicSettingsPageTab.Wms);

        const idPrefix: string = 'wms_layer_';

        this.wmsLayerForm = new WmsLayerForm(idPrefix);
        this.wmsLayerFormElement = document.querySelector('#' + idPrefix + 'form');
        this.wmsLayerFormSubmitButton = document.querySelector('#' + idPrefix + 'save_submit_button');
        this.contextMenu = new MdcContextMenuComponent('wms_layer_')
        this.messageBoxComponent = new MessageBoxComponent();

        this.wmsLayerFormSubmitButton.addEventListener('click', (): void => {
            if (this.wmsLayerForm.isFormValid() === false) {
                this.messageBoxComponent.openMessageBox('Sie haben keine Bezeichnung angegeben.', MessageBoxAlertType.DANGER);

                return;
            }

            this.wmsLayerFormElement.submit();
        });
    }

}

export { EditPage };
