import { DocumentTemplateForm } from '../../Form/DocumentTemplate/DocumentTemplateForm';
import { DocumentTemplateDeleteForm } from '../../Form/DocumentTemplate/DocumentTemplateDeleteForm';

class DeletePage {

    private readonly documentTemplateForm: DocumentTemplateForm;
    private readonly documentTemplateDeleteForm: DocumentTemplateDeleteForm;
    private readonly documentTemplateDeleteFormElement: HTMLFormElement;
    private readonly documentTemplateDeleteFormSubmitButton: HTMLButtonElement;

    constructor() {
        const idPrefix: string = 'document_template_delete_';

        this.documentTemplateForm = new DocumentTemplateForm('document_template_');
        this.documentTemplateDeleteForm = new DocumentTemplateDeleteForm(idPrefix);
        this.documentTemplateDeleteFormElement = document.querySelector('#' + idPrefix + 'form');
        this.documentTemplateDeleteFormSubmitButton = document.querySelector('#' + idPrefix + 'delete_submit_button');

        this.documentTemplateDeleteFormSubmitButton.addEventListener('click', (): void => {
            this.documentTemplateDeleteFormElement.submit();
        });
    }

}

export { DeletePage };
