import { DocumentTemplateForm } from '../../Form/DocumentTemplate/DocumentTemplateForm';
import { MdcContextMenuComponent } from '../../../Component/MdcContextMenuComponent';

class EditPage {

    private readonly documentTemplateForm: DocumentTemplateForm;
    private readonly documentTemplateFormElement: HTMLFormElement;
    private readonly documentTemplateFormSubmitButton: HTMLButtonElement;
    private readonly contextMenu: MdcContextMenuComponent;

    constructor() {
        const idPrefix: string = 'document_template_';

        this.documentTemplateForm = new DocumentTemplateForm(idPrefix);
        this.documentTemplateFormElement = document.querySelector('#' + idPrefix + 'form');
        this.documentTemplateFormSubmitButton = document.querySelector('#' + idPrefix + 'save_submit_button');
        this.contextMenu = new MdcContextMenuComponent(idPrefix);

        this.documentTemplateFormSubmitButton.addEventListener('click', (): void => {
            this.documentTemplateFormElement.submit();
        });
    }

}

export { EditPage };
