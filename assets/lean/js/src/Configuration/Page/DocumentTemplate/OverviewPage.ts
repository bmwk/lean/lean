import { DropzoneUploadComponent } from '../../../Component/DropzoneUploadComponent';

class OverviewPage {

    private readonly dropzoneUploadComponent: DropzoneUploadComponent;

    constructor() {
        const acceptedFiles: string[] = [
            'application/pdf',
            'application/msword',
            'application/msexcel',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        ];

        this.dropzoneUploadComponent = new DropzoneUploadComponent(
            document.querySelector('#document_template_upload_dropzone_container'),
            document.querySelector('#document_template_upload_submit_button'),
            'documentTemplateFile',
            acceptedFiles
        );
    }

}

export { OverviewPage };
