import { LazyImageLoader } from '../../../LazyImageLoader';

class OverviewPage {

    private readonly lazyImageLoader: LazyImageLoader;

    constructor() {
        this.lazyImageLoader = new LazyImageLoader();
    }

}

export { OverviewPage };
