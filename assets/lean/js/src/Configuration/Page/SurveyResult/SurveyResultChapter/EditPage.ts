import { SurveyResultChapterForm } from '../../../Form/SurveyResult/SurveyResultChapterForm';
import { MdcContextMenuComponent } from '../../../../Component/MdcContextMenuComponent';
import { DropzoneUploadComponent } from '../../../../Component/DropzoneUploadComponent';
import { MessageBoxComponent } from '../../../../Component/MessageBoxComponent/MessageBoxComponent';
import { MessageBoxAlertType } from '../../../../Component/MessageBoxComponent/MessageBoxAlertType';

class EditPage {

    private readonly surveyResultChapterForm: SurveyResultChapterForm;
    private readonly surveyResultChapterFormElement: HTMLFormElement;
    private readonly surveyResultChapterFormSubmitButton: HTMLButtonElement;
    private readonly surveyResultContextMenu: MdcContextMenuComponent;
    private readonly dropzoneImageUploadComponent: DropzoneUploadComponent;
    private readonly dropzoneDocumentUploadComponent: DropzoneUploadComponent;
    private readonly messageBoxComponent: MessageBoxComponent;

    constructor() {
        const idPrefix: string = 'survey_result_chapter_';

        this.surveyResultChapterForm = new SurveyResultChapterForm(idPrefix);
        this.surveyResultChapterFormElement = document.querySelector('#' + idPrefix + 'form');
        this.surveyResultChapterFormSubmitButton = document.querySelector('#' + idPrefix + 'save_submit_button');
        this.surveyResultContextMenu = new MdcContextMenuComponent(idPrefix);

        const acceptedImageFiles: string[] = [
            'image/jpeg',
            'image/png',
        ];

        this.dropzoneImageUploadComponent = new DropzoneUploadComponent(
            document.querySelector('#survey_result_chapter_image_upload_dropzone_container'),
            document.querySelector('#survey_result_chapter_image_upload_submit_button'),
            'chapterImageFile',
            acceptedImageFiles
        );

        const acceptedDocumentFiles: string[] = [
            'application/pdf',
        ];

        this.dropzoneDocumentUploadComponent = new DropzoneUploadComponent(
            document.querySelector('#survey_result_chapter_document_upload_dropzone_container'),
            document.querySelector('#survey_result_chapter_document_upload_submit_button'),
            'chapterDocumentFile',
            acceptedDocumentFiles
        );

        this.messageBoxComponent = new MessageBoxComponent();

        this.surveyResultChapterFormSubmitButton.addEventListener('click', (): void => {
            if (this.surveyResultChapterForm.isFormValid() === false) {
                this.messageBoxComponent.openMessageBox('Die Daten konnten nicht gespeichert werden. Bitte kontrollieren Sie die rot markierten Felder.', MessageBoxAlertType.DANGER);
                return;
            }

            this.surveyResultChapterFormElement.submit();
        });
    }

}

export { EditPage };
