import { SurveyResultChapterForm } from '../../../Form/SurveyResult/SurveyResultChapterForm';
import { MessageBoxComponent } from '../../../../Component/MessageBoxComponent/MessageBoxComponent';
import { MessageBoxAlertType } from '../../../../Component/MessageBoxComponent/MessageBoxAlertType';

class CreatePage {

    private readonly surveyResultChapterForm: SurveyResultChapterForm;
    private readonly surveyResultChapterFormElement: HTMLFormElement;
    private readonly surveyResultChapterFormSubmitButton: HTMLButtonElement;
    private readonly messageBoxComponent: MessageBoxComponent;

    constructor() {
        const idPrefix: string = 'survey_result_chapter_';

        this.surveyResultChapterForm = new SurveyResultChapterForm(idPrefix);
        this.surveyResultChapterFormElement = document.querySelector('#' + idPrefix + 'form');
        this.surveyResultChapterFormSubmitButton = document.querySelector('#' + idPrefix + 'save_submit_button');
        this.messageBoxComponent = new MessageBoxComponent();

        this.surveyResultChapterFormSubmitButton.addEventListener('click', (): void => {
            if (this.surveyResultChapterForm.isFormValid() === false) {
                this.messageBoxComponent.openMessageBox('Die Daten konnten nicht gespeichert werden. Bitte kontrollieren Sie die rot markierten Felder.', MessageBoxAlertType.DANGER);
                return;
            }

            this.surveyResultChapterFormElement.submit();
        });
    }

}

export { CreatePage };
