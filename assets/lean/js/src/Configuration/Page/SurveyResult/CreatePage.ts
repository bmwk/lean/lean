import { SurveyResultForm } from '../../Form/SurveyResult/SurveyResultForm';
import { MessageBoxComponent } from '../../../Component/MessageBoxComponent/MessageBoxComponent';
import {MessageBoxAlertType} from "../../../Component/MessageBoxComponent/MessageBoxAlertType";

class CreatePage {

    private readonly surveyResultForm: SurveyResultForm;
    private readonly surveyResultFormElement: HTMLFormElement;
    private readonly surveyResultFormSubmitButton: HTMLButtonElement;
    private readonly messageBoxComponent: MessageBoxComponent;

    constructor() {
        const idPrefix: string = 'survey_result_';

        this.surveyResultForm = new SurveyResultForm(idPrefix);
        this.surveyResultFormElement = document.querySelector('#' + idPrefix + 'form');
        this.surveyResultFormSubmitButton = document.querySelector('#' + idPrefix + 'save_submit_button');
        this.messageBoxComponent = new MessageBoxComponent();

        this.surveyResultFormSubmitButton.addEventListener('click', (): void => {
            if (this.surveyResultForm.isFormValid() === false) {
                this.messageBoxComponent.openMessageBox('Die Daten konnten nicht gespeichert werden. Bitte kontrollieren Sie die rot markierten Felder.', MessageBoxAlertType.DANGER);
                return;
            }

            this.surveyResultFormElement.submit();
        });
    }
}

export { CreatePage };
