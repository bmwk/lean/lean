import { SurveyResultDeleteForm } from '../../Form/SurveyResult/SurveyResultDeleteForm';
import { MessageBoxAlertType } from "../../../Component/MessageBoxComponent/MessageBoxAlertType";
import { MessageBoxComponent } from "../../../Component/MessageBoxComponent/MessageBoxComponent";

class DeletePage {

    private readonly deleteForm: SurveyResultDeleteForm;
    private readonly deleteFormElement: HTMLFormElement;
    private readonly deleteFormSubmitButton: HTMLButtonElement;
    private readonly messageBoxComponent: MessageBoxComponent;

    constructor() {
        const idPrefix: string = 'survey_result_delete_';

        this.deleteForm = new SurveyResultDeleteForm(idPrefix);
        this.deleteFormElement = document.querySelector('#' + idPrefix + 'form');
        this.deleteFormSubmitButton = document.querySelector('#' + idPrefix + 'delete_submit_button');
        this.messageBoxComponent = new MessageBoxComponent();

        this.deleteFormSubmitButton.addEventListener('click', (): void => {
            if (this.deleteForm.isFormValid() === false) {
                this.messageBoxComponent.openMessageBox('Das Kapitel konnte nicht gelöscht werden, es wurde kein Bestätigungscode eingegeben.', MessageBoxAlertType.DANGER);

                return;
            }

            this.deleteFormElement.submit();
        });
    }

}

export { DeletePage };
