import { SurveyResultForm } from '../../Form/SurveyResult/SurveyResultForm';
import { MdcContextMenuComponent } from '../../../Component/MdcContextMenuComponent';
import { MessageBoxComponent } from '../../../Component/MessageBoxComponent/MessageBoxComponent';
import { MessageBoxAlertType } from "../../../Component/MessageBoxComponent/MessageBoxAlertType";
import { DropzoneUploadComponent } from '../../../Component/DropzoneUploadComponent';
import { LazyImageLoader } from '../../../LazyImageLoader';

class EditPage {

    private readonly surveyResultForm: SurveyResultForm;
    private readonly surveyResultFormElement: HTMLFormElement;
    private readonly surveyResultFormSubmitButton: HTMLButtonElement;
    private readonly surveyResultContextMenu: MdcContextMenuComponent;
    private readonly messageBoxComponent: MessageBoxComponent;
    private readonly dropzoneImageUploadComponent: DropzoneUploadComponent;
    private readonly dropzoneDocumentUploadComponent: DropzoneUploadComponent;
    private readonly lazyImageLoader: LazyImageLoader;

    constructor() {
        const idPrefix: string = 'survey_result_';

        this.surveyResultForm = new SurveyResultForm(idPrefix);
        this.surveyResultFormElement = document.querySelector('#' + idPrefix + 'form');
        this.surveyResultFormSubmitButton = document.querySelector('#' + idPrefix + 'save_submit_button');
        this.surveyResultContextMenu = new MdcContextMenuComponent(idPrefix);
        this.messageBoxComponent = new MessageBoxComponent();
        this.lazyImageLoader = new LazyImageLoader();

        const acceptedImageFiles: string[] = [
            'image/jpeg',
            'image/png',
        ];

        this.dropzoneImageUploadComponent = new DropzoneUploadComponent(
            document.querySelector('#survey_result_image_upload_dropzone_container'),
            document.querySelector('#survey_result_image_upload_submit_button'),
            'imageFile',
            acceptedImageFiles
        );

        const acceptedDocumentFiles: string[] = [
            'application/pdf',
        ];

        this.dropzoneDocumentUploadComponent = new DropzoneUploadComponent(
            document.querySelector('#survey_result_document_upload_dropzone_container'),
            document.querySelector('#survey_result_document_upload_submit_button'),
            'documentFile',
            acceptedDocumentFiles
        );

        this.surveyResultFormSubmitButton.addEventListener('click', (): void => {
            if (this.surveyResultForm.isFormValid() === false) {
                this.messageBoxComponent.openMessageBox('Die Daten konnten nicht gespeichert werden. Bitte kontrollieren Sie die rot markierten Felder.', MessageBoxAlertType.DANGER);
                return;
            }

            this.surveyResultFormElement.submit();
        });
    }
}

export { EditPage };
