import React from 'react';
import { createRoot, Root } from 'react-dom/client';
import BusinessLocationAreaMap from '../../../Map/BusinessLocationAreaMap';

class MapPage {

    private readonly mapContainer: HTMLDivElement;
    private readonly businessLocationAreaMapRoot: Root;

    constructor() {
        this.mapContainer = document.querySelector('#business_location_area_map');
        this.businessLocationAreaMapRoot = createRoot(this.mapContainer!);

        const centerPoint = {
            longitude: 0,
            latitude: 0
        };

        if (this.mapContainer.dataset.longitude !== undefined && this.mapContainer.dataset.latitude !== undefined) {
            centerPoint.longitude = parseFloat(this.mapContainer.dataset.longitude);
            centerPoint.latitude = parseFloat(this.mapContainer.dataset.latitude);
        }

        this.businessLocationAreaMapRoot.render(
            <React.StrictMode>
                <BusinessLocationAreaMap
                    permissions={{canView: true, canEdit: true}}
                    centerPoint={centerPoint}
                    archived={false}
                />
            </React.StrictMode>
        );
    }

}

export { MapPage };
