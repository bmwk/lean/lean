enum PersonType {
    NaturalPerson,
    Company,
    Commune,
}

export { PersonType };
