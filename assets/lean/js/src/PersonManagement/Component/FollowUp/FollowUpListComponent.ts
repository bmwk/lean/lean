import { ListComponent } from '../../../Component/ListComponent';

class FollowUpListComponent extends ListComponent {

    constructor(idPrefix: string) {
        super(idPrefix);
    }

}

export { FollowUpListComponent };
