import { ListComponent } from '../../../Component/ListComponent';

class PersonListComponent extends ListComponent {

    constructor(idPrefix: string) {
        super(idPrefix);
    }

}

export { PersonListComponent };
