import { ListComponent } from '../../../Component/ListComponent';

class OccurrenceListComponent extends ListComponent {

    constructor(idPrefix: string) {
        super(idPrefix);
    }

}

export { OccurrenceListComponent };
