import { DropzoneUploadComponent } from '../../../Component/DropzoneUploadComponent';

class OccurrenceAttachmentComponent {

    private readonly dropzoneContainer: HTMLDivElement;
    private readonly submitButton: HTMLButtonElement;
    private readonly dropzoneUploadComponent: DropzoneUploadComponent = null;

    constructor(idPrefix: string) {
        this.dropzoneContainer = document.querySelector('#' + idPrefix + 'occurrence_attachment_upload_dropzone_container');
        this.submitButton = document.querySelector('#' + idPrefix + 'occurrence_attachment_upload_submit_button');

        if (this.dropzoneContainer !== null &&  this.submitButton !== null) {
            const acceptedFiles: string[] = [
                'application/pdf',
                'application/msword',
                'application/msexcel',
                'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            ];

            this.dropzoneUploadComponent = new DropzoneUploadComponent(
                this.dropzoneContainer,
                this.submitButton,
                'occurrenceAttachmentFile',
                acceptedFiles
            );
        }

    }

    public static checkContainerExists(idPrefix: string): boolean {
        return document.querySelector('#' + idPrefix + 'occurrence_attachment_upload_dropzone_container') !== null;
    }

}

export { OccurrenceAttachmentComponent };
