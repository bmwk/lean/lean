import { ListComponent } from '../../../Component/ListComponent';

class ContactListComponent extends ListComponent {

    constructor(idPrefix: string) {
        super(idPrefix);
    }

}

export { ContactListComponent };
