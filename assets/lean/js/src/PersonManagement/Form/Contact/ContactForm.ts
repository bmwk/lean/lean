import { SelectComponent } from '../../../Component/SelectComponent';
import { TextFieldComponent } from '../../../Component/TextFieldComponent';
import { FormFieldValidator } from '../../../FormFieldValidator/FormFieldValidator';

class ContactForm {

    private salutationSelect: SelectComponent;
    private personTitleSelect: SelectComponent;
    private firstNameTextField: TextFieldComponent;
    private nameTextField: TextFieldComponent;
    private phoneNumberTextField: TextFieldComponent;
    private mobilePhoneNumberTextField: TextFieldComponent;
    private emailTextField: TextFieldComponent;
    private faxNumberTextField: TextFieldComponent;
    private positionTextField: TextFieldComponent;

    constructor(idPrefix: string) {
        this.salutationSelect = new SelectComponent(document.querySelector('#' + idPrefix + 'salutation_mdc_select'), {'clearButton': true});
        this.personTitleSelect = new SelectComponent(document.querySelector('#' + idPrefix + 'personTitle_mdc_select'), {'clearButton': true});
        this.firstNameTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'firstName_mdc_text_field'));
        this.nameTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'name_mdc_text_field'));
        this.phoneNumberTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'phoneNumber_mdc_text_field'));
        this.mobilePhoneNumberTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'mobilePhoneNumber_mdc_text_field'));
        this.emailTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'email_mdc_text_field'));
        this.faxNumberTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'faxNumber_mdc_text_field'));
        this.positionTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'position_mdc_text_field'));

        this.nameTextField.mdcTextField.required = true;
    }

    public isFormValid(): boolean {
        let hasErrors: boolean = false;

        if (this.nameTextField.mdcTextField.valid === false) {
            this.nameTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (this.emailTextField.mdcTextField.value !== '' && FormFieldValidator.validateEmail(this.emailTextField.mdcTextField.value) === false) {
            this.emailTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        return (hasErrors === false);
    }

}

export { ContactForm };
