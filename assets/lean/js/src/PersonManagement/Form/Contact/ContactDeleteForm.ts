import { DeleteForm } from '../../../Form/DeleteForm';

class ContactDeleteForm extends DeleteForm {

    constructor(idPrefix: string) {
        super(idPrefix);
    }

}

export { ContactDeleteForm };
