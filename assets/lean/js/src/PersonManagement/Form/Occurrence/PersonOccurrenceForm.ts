import { OccurrenceForm } from './OccurrenceForm';
import { SelectComponent } from '../../../Component/SelectComponent';

class PersonOccurrenceForm extends OccurrenceForm {

    private readonly personSelect: SelectComponent;

    constructor(idPrefix: string) {
        super(idPrefix);

        this.personSelect = new SelectComponent(document.querySelector('#' + idPrefix + 'person_mdc_select'));

        this.personSelect.mdcSelect.required = true;
    }

    public isFormValid(): boolean {
        let hasErrors: boolean = (super.isFormValid() === false);

        if (this.personSelect.mdcSelect.value.length === 0) {
            this.personSelect.mdcSelect.valid = false;
            hasErrors = true;
        }

        return (hasErrors === false);
    }

}

export { PersonOccurrenceForm };
