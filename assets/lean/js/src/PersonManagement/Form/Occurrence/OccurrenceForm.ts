import { SelectComponent } from '../../../Component/SelectComponent';
import { TextFieldComponent } from '../../../Component/TextFieldComponent';
import { CheckboxFieldComponent } from '../../../Component/CheckboxFieldComponent';

abstract class OccurrenceForm {

    private readonly occurrenceTypeSelect: SelectComponent;
    private readonly contactSelect: SelectComponent;
    private readonly noteTextField: TextFieldComponent;
    private readonly occurrenceAtTextField: TextFieldComponent;
    private readonly clerkTextField: TextFieldComponent;
    private readonly followUpCheckboxField: CheckboxFieldComponent;
    private readonly followUpDoneCheckboxField: CheckboxFieldComponent;
    private readonly followUpAtTextField: TextFieldComponent;
    private readonly followUpAtWrapper: HTMLDivElement;
    private readonly followUpDoneWrapper: HTMLDivElement;

    protected constructor(idPrefix: string) {
        this.occurrenceTypeSelect = new SelectComponent(document.querySelector('#' + idPrefix + 'occurrenceType_mdc_select'));
        this.noteTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'note_mdc_text_field'), {autoFitTextarea: true});
        this.occurrenceAtTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'occurrenceAt_mdc_text_field'), {datePicker: true});
        this.clerkTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'clerk_mdc_text_field'));
        this.followUpCheckboxField = new CheckboxFieldComponent(document.querySelector('#' + idPrefix + 'followUp_mdc_form_field'));
        this.followUpDoneCheckboxField = new CheckboxFieldComponent(document.querySelector('#' + idPrefix + 'followUpDone_mdc_form_field'));
        this.followUpAtTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'followUpAt_mdc_text_field'), {datePicker: true});
        this.followUpAtWrapper = document.querySelector('#followUpAtWrapper');
        this.followUpDoneWrapper = document.querySelector('#followUpDoneWrapper');

        this.occurrenceAtTextField.setMaxDate(new Date());
        this.followUpAtTextField.setMinDate(new Date());

        if (this.followUpCheckboxField.isChecked() === true) {
            this.followUpAtWrapper.classList.remove('d-none');
            this.followUpDoneWrapper.classList.remove('d-none');
        }

        this.followUpCheckboxField.mdcFormField.listen('change', (event: Event): void => {
            if (this.followUpCheckboxField.isChecked() === true) {
                this.followUpAtWrapper.classList.remove('d-none');
                this.followUpDoneWrapper.classList.remove('d-none');
            } else {
                this.followUpAtWrapper.classList.add('d-none');
                this.followUpDoneWrapper.classList.add('d-none');
            }
        });

        this.occurrenceTypeSelect.mdcSelect.required = true;
        this.noteTextField.mdcTextField.required = true;
        this.occurrenceAtTextField.mdcTextField.required = true;
    }

    public isFormValid(): boolean {
        let hasErrors: boolean = false;

        if (this.occurrenceTypeSelect.mdcSelect.value.length === 0) {
            this.occurrenceTypeSelect.mdcSelect.valid = false;
            hasErrors = true;
        }

        if (this.noteTextField.mdcTextField.valid === false) {
            this.noteTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (this.occurrenceAtTextField.mdcTextField.valid === false) {
            this.occurrenceAtTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        return (hasErrors === false);
    }

    public disableFields(): void {
        this.occurrenceTypeSelect.mdcSelect.disabled = true;
        this.noteTextField.mdcTextField.disabled = true;
        this.occurrenceAtTextField.mdcTextField.disabled = true;
        this.clerkTextField.mdcTextField.disabled = true;
        this.followUpCheckboxField.mdcCheckbox.disabled = true;
        this.followUpDoneCheckboxField.mdcCheckbox.disabled = true;
        this.followUpAtTextField.mdcTextField.disabled = true;
    }

}

export { OccurrenceForm };
