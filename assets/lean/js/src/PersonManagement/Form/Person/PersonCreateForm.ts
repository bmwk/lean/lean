import { NaturalPersonForm } from './NaturalPersonForm';
import { CompanyForm } from './CompanyForm';
import { CommuneForm } from './CommuneForm';
import { PersonType } from '../../PersonType';
import { MDCTabBar } from '@material/tab-bar';

class PersonCreateForm {

    private readonly selectedPersonTypeInputField: HTMLInputElement;
    private readonly naturalPersonForm: NaturalPersonForm;
    private readonly companyForm: CompanyForm;
    private readonly communeForm: CommuneForm;
    private readonly personTypeMdcTabBar: MDCTabBar;
    private readonly naturalPersonMdcTab: HTMLButtonElement;
    private readonly companyMdcTab: HTMLButtonElement;
    private readonly communeMdcTab: HTMLButtonElement;
    private readonly naturalPersonFormContainer: HTMLDivElement;
    private readonly companyFormContainer: HTMLDivElement;
    private readonly communeFormContainer: HTMLDivElement;
    private selectedPersonType: PersonType;

    constructor(idPrefix: string) {
        this.selectedPersonTypeInputField = document.querySelector('#' + idPrefix + 'selectedPersonType');
        this.naturalPersonForm = new NaturalPersonForm(idPrefix + 'naturalPerson_');
        this.companyForm = new CompanyForm(idPrefix + 'company_');
        this.communeForm = new CommuneForm(idPrefix + 'commune_');

        this.personTypeMdcTabBar = new MDCTabBar(document.querySelector('#' + idPrefix + 'personType_mdc_tab_bar'));
        this.naturalPersonMdcTab = document.querySelector('#' + idPrefix + 'naturalPerson_mdc_tab');
        this.companyMdcTab = document.querySelector('#' + idPrefix + 'company_mdc_tab');
        this.communeMdcTab = document.querySelector('#' + idPrefix + 'commune_mdc_tab');

        this.naturalPersonFormContainer = document.querySelector('#' + idPrefix + 'naturalPerson_form_container');
        this.companyFormContainer = document.querySelector('#' + idPrefix + 'company_form_container');
        this.communeFormContainer = document.querySelector('#' + idPrefix + 'commune_form_container');

        this.naturalPersonMdcTab.addEventListener('click', (mouseEvent: MouseEvent): void => {
            mouseEvent.preventDefault();
            this.selectNaturalPerson();
        });

        this.companyMdcTab.addEventListener('click', (mouseEvent: MouseEvent): void => {
            mouseEvent.preventDefault();
            this.selectCompany();
        });

        this.communeMdcTab.addEventListener('click', (mouseEvent: MouseEvent): void => {
            mouseEvent.preventDefault();
            this.selectCommune();
        });

        switch (Number(this.selectedPersonTypeInputField.value)) {
            case PersonType.NaturalPerson:
                this.selectNaturalPerson();
                break;
            case PersonType.Company:
                this.selectCompany();
                break;
            case PersonType.Commune:
                this.selectCommune();
                break;
            default:
                this.selectNaturalPerson();
        }
    }

    public isFormValid(): boolean {
        if (this.selectedPersonType === PersonType.NaturalPerson) {
            return this.naturalPersonForm.isFormValid();
        } else if (this.selectedPersonType === PersonType.Company) {
            return this.companyForm.isFormValid();
        } else if (this.selectedPersonType === PersonType.Commune) {
            return this.communeForm.isFormValid();
        }

        return false;
    }

    public selectNaturalPerson(): void {
        this.selectedPersonType = PersonType.NaturalPerson;
        this.personTypeMdcTabBar.activateTab(PersonType.NaturalPerson);
        this.selectedPersonTypeInputField.value = this.selectedPersonType.toString();
        this.showNaturalPersonForm();
        this.hideCompanyForm();
        this.hideCommuneForm();
    }

    public selectCompany(): void {
        this.selectedPersonType = PersonType.Company;
        this.personTypeMdcTabBar.activateTab(PersonType.Company);
        this.selectedPersonTypeInputField.value = this.selectedPersonType.toString();
        this.hideNaturalPersonForm();
        this.showCompanyForm();
        this.hideCommuneForm();
    }

    public selectCommune(): void {
        this.selectedPersonType = PersonType.Commune;
        this.personTypeMdcTabBar.activateTab(PersonType.Commune);
        this.selectedPersonTypeInputField.value = this.selectedPersonType.toString();
        this.hideNaturalPersonForm();
        this.hideCompanyForm();
        this.showCommuneForm();
    }

    private showNaturalPersonForm(): void {
        PersonCreateForm.showDivElement(this.naturalPersonFormContainer);
    }

    private hideNaturalPersonForm(): void {
        PersonCreateForm.hideDivElement(this.naturalPersonFormContainer);
    }

    private showCompanyForm(): void {
        PersonCreateForm.showDivElement(this.companyFormContainer);
    }

    private hideCompanyForm(): void {
        PersonCreateForm.hideDivElement(this.companyFormContainer);
    }

    private showCommuneForm(): void {
        PersonCreateForm.showDivElement(this.communeFormContainer);
    }

    private hideCommuneForm(): void {
        PersonCreateForm.hideDivElement(this.communeFormContainer);
    }

    private static showDivElement(divElement: HTMLDivElement) : void {
        divElement.classList.remove('d-none');
    }

    private static hideDivElement(divElement: HTMLDivElement) : void {
        divElement.classList.add('d-none');
    }

}

export { PersonCreateForm };
