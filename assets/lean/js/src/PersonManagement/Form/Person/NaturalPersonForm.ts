import { PersonForm } from './PersonForm';
import { TextFieldComponent } from '../../../Component/TextFieldComponent';
import { SelectComponent } from '../../../Component/SelectComponent';

class NaturalPersonForm extends PersonForm {

    private readonly salutationSelect: SelectComponent;
    private readonly personTitleSelect: SelectComponent;
    private readonly firstNameTextField: TextFieldComponent;
    private readonly birthNameTextField: TextFieldComponent;

    constructor(idPrefix: string) {
        super(idPrefix);

        this.salutationSelect = new SelectComponent(document.querySelector('#' + idPrefix + 'salutation_mdc_select'), {clearButton: true});
        this.personTitleSelect = new SelectComponent(document.querySelector('#' + idPrefix + 'personTitle_mdc_select'), {clearButton: true});
        this.firstNameTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'firstName_mdc_text_field'));
        this.birthNameTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'birthName_mdc_text_field'));
    }

}

export { NaturalPersonForm };
