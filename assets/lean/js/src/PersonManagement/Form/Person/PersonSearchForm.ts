import { SearchFieldComponent } from '../../../Component/SearchFieldComponent';

class PersonSearchForm {

    private readonly searchFieldComponent: SearchFieldComponent;

    constructor(idPrefix: string) {
        this.searchFieldComponent = new SearchFieldComponent(idPrefix, 'text');
    }

}

export { PersonSearchForm };
