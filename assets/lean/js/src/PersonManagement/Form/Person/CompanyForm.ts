import { PersonForm } from './PersonForm';
import { SelectComponent } from '../../../Component/SelectComponent';

class CompanyForm extends PersonForm {

    private readonly legalEntityTypeSelect: SelectComponent;

    constructor(idPrefix: string) {
        super(idPrefix);

        this.legalEntityTypeSelect = new SelectComponent(document.querySelector('#' + idPrefix + 'legalEntityType_mdc_select'), {clearButton: true});
    }

}

export { CompanyForm };
