import { FormFieldValidator } from '../../../FormFieldValidator/FormFieldValidator';
import { TextFieldComponent } from '../../../Component/TextFieldComponent';

abstract class PersonForm {

    protected readonly nameTextField: TextFieldComponent;
    protected readonly streetNameTextField: TextFieldComponent;
    protected readonly houseNumberTextField: TextFieldComponent;
    protected readonly postalCodeTextField: TextFieldComponent;
    protected readonly placeNameTextField: TextFieldComponent;
    protected readonly additionalAddressInformationTextField: TextFieldComponent;
    protected readonly phoneNumberTextField: TextFieldComponent;
    protected readonly mobilePhoneNumberTextField: TextFieldComponent;
    protected readonly emailTextField: TextFieldComponent;
    protected readonly faxNumberTextField: TextFieldComponent;
    protected readonly websiteTextField: TextFieldComponent;

    protected constructor(idPrefix: string) {
        this.nameTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'name_mdc_text_field'));
        this.streetNameTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'streetName_mdc_text_field'));
        this.houseNumberTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'houseNumber_mdc_text_field'));
        this.postalCodeTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'postalCode_mdc_text_field'));
        this.placeNameTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'placeName_mdc_text_field'));
        this.additionalAddressInformationTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'additionalAddressInformation_mdc_text_field'));
        this.phoneNumberTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'phoneNumber_mdc_text_field'));
        this.mobilePhoneNumberTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'mobilePhoneNumber_mdc_text_field'));
        this.emailTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'email_mdc_text_field'));
        this.faxNumberTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'faxNumber_mdc_text_field'));
        this.websiteTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'website_mdc_text_field'));

        this.nameTextField.mdcTextField.required = true;
    }

    public isFormValid(): boolean {
        let hasErrors: boolean = false;

        if (this.nameTextField.mdcTextField.valid === false) {
            this.nameTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (this.emailTextField.mdcTextField.value !== '' && FormFieldValidator.validateEmail(this.emailTextField.mdcTextField.value) === false) {
            this.emailTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        return (hasErrors === false);
    }

}

export { PersonForm };
