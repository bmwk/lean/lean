import { DeleteForm } from '../../../Form/DeleteForm';

class PersonDeleteForm extends DeleteForm {

    constructor(idPrefix: string) {
        super(idPrefix);
    }

}

export { PersonDeleteForm };
