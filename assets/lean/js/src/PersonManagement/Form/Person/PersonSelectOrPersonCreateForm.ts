import { PersonSelectForm } from './PersonSelectForm';
import { PersonCreateForm } from './PersonCreateForm';

class PersonSelectOrPersonCreateForm {

    private readonly selectOrCreateInputField: HTMLInputElement;
    private readonly personSelectForm: PersonSelectForm;
    private readonly personSelectContainer: HTMLDivElement;
    private readonly personCreateContainer: HTMLDivElement;
    private readonly personCreateButton: HTMLButtonElement;
    private readonly personCreateAbortButton: HTMLButtonElement;
    private readonly personCreateForm: PersonCreateForm;

    constructor(idPrefix: string) {
        this.selectOrCreateInputField = document.querySelector('#' + idPrefix + 'selectOrCreate');
        this.personSelectForm = new PersonSelectForm(idPrefix + 'personSelect_');
        this.personSelectContainer = document.querySelector('#' + idPrefix + 'select_container');
        this.personCreateContainer = document.querySelector('#' + idPrefix + 'create_container');
        this.personCreateButton = document.querySelector('#' + idPrefix + 'personCreateButton');
        this.personCreateAbortButton = document.querySelector('#' + idPrefix + 'person_create_abort_button');

        if (this.personCreateContainer !== null) {
            this.personCreateForm = new PersonCreateForm(idPrefix + 'personCreate_');
        }

        if (this.personCreateButton !== null) {
            this.personCreateButton.addEventListener('click', (mouseEvent: MouseEvent): void => {
                mouseEvent.preventDefault();
                this.setPersonCreate();
            });
        }

        if (this.personCreateAbortButton !== null) {
            this.personCreateAbortButton.addEventListener('click', (mouseEvent: MouseEvent): void => {
                mouseEvent.preventDefault();
                this.setPersonSelect();
            });
        }

        if (this.personCreateContainer !== null) {
            if (this.selectOrCreateInputField.value === 'create') {
                this.setPersonCreate();
            } else {
                this.setPersonSelect();
            }
        } else {
            this.showPersonSelectContainer();
        }
    }

    public isFormValid(): boolean {
        if (this.selectOrCreateInputField.value === 'create') {
            return this.personCreateForm.isFormValid();
        } else {
            return this.personSelectForm.isFormValid();
        }
    }

    public setPersonCreate(): void {
        this.hidePersonSelectContainer();
        this.showPersonCreateContainer();

        this.selectOrCreateInputField.value = 'create';
    }

    public setPersonSelect(): void {
        this.hidePersonCreateContainer();
        this.showPersonSelectContainer();

        this.selectOrCreateInputField.value = 'select';
    }

    public showPersonSelectContainer(): void {
        this.personSelectContainer.classList.remove('d-none');
    }

    public hidePersonSelectContainer(): void {
        this.personSelectContainer.classList.add('d-none');
    }

    public showPersonCreateContainer(): void {
        this.personCreateContainer.classList.remove('d-none');
    }

    public hidePersonCreateContainer(): void {
        this.personCreateContainer.classList.add('d-none');
    }

    public disableFields(): void {
        this.personSelectForm.disableFields();
        if (this.personCreateButton !== null) {
            this.personCreateButton.disabled = true;
        }
    }

    public static checkFormExists(idPrefix: string): boolean {
        return document.querySelector('#' + idPrefix + 'selectOrCreate') !== null;
    }

}

export { PersonSelectOrPersonCreateForm };
