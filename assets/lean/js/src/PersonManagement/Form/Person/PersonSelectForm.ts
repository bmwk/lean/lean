import '../../../../../styles/person/form/person_select_form.scss';
import { TextFieldComponent } from '../../../Component/TextFieldComponent';

interface Person {
    id: number;
    personType: number;
    displayName: string;
}

class PersonSelectForm {

    private readonly personSearchTextField: TextFieldComponent;
    private readonly personIdInputField: HTMLInputElement;
    private initialPersonSearchValue: string;

    constructor(idPrefix: string) {
        this.personSearchTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'personSearch_mdc_text_field'));
        this.personIdInputField = document.querySelector('#' + idPrefix + 'personId');
        this.initialPersonSearchValue = this.personSearchTextField.mdcTextField.value

        this.personSearchTextField.mdcTextField.required = true;

        this.autocomplete(this.personSearchTextField.mdcTextField.root.querySelector('input'));
    }

    public isFormValid(): boolean {
        if (this.personIdInputField.value.length === 0) {
            this.personSearchTextField.mdcTextField.valid = false;

            return false;
        }
    }

    public disableFields(): void {
        this.personSearchTextField.mdcTextField.disabled = true;
    }

    public static checkFormExists(idPrefix: string): boolean {
        return document.querySelector('#' + idPrefix + 'personSearch_mdc_text_field') !== null;
    }

    private autocomplete(inputElement: HTMLInputElement): void {
        inputElement.addEventListener('input', async (event: Event) => {
            const value: string = (event.target as HTMLInputElement).value;
            const persons: Person[] = await this.fetchPersons(value);

            this.closeAllLists();

            if (!value || value.length < 3) {
                return;
            }

            const autocompleteList: HTMLDivElement = document.createElement('div') as HTMLDivElement;

            autocompleteList.setAttribute('id', inputElement.id + 'autocomplete-list');
            autocompleteList.setAttribute('class', 'autocomplete-items');

            this.personSearchTextField.mdcTextField.root.parentNode.appendChild(autocompleteList);

            persons.forEach((person: Person) => {
                const listItem: HTMLDivElement = document.createElement('div') as HTMLDivElement;

                listItem.classList.add('autocompleteItem');

                const displayName: string = person.displayName;

                listItem.innerHTML = displayName;
                listItem.innerHTML += `<input type="hidden" value="${displayName}" data-person-id="${person.id}">`;

                listItem.addEventListener('click', (mouseEvent: MouseEvent): void => {
                    inputElement.value = (mouseEvent.target as HTMLElement).getElementsByTagName('input')[0].value;

                    this.personIdInputField.value = (mouseEvent.target as HTMLElement).getElementsByTagName('input')[0].dataset.personId;
                    this.initialPersonSearchValue = inputElement.value

                    this.closeAllLists();
                });

                autocompleteList.appendChild(listItem);
            });
        });

        document.addEventListener('click', (mouseEvent: MouseEvent): void => {
            this.closeAllLists();

            if ((mouseEvent.target as HTMLElement).classList.contains('autocompleteItem') === false) {
                this.personSearchTextField.mdcTextField.value = this.initialPersonSearchValue;
            }
        });
    }

    private async fetchPersons(query?: string): Promise<Person[]> {
        const response = await fetch(`/api/person-management/persons` + (query ? `?search=${query}` : ''));

        return await response.json();
    }

    private closeAllLists(elementToKeep?: HTMLDivElement): void {
        const lists: HTMLCollectionOf<Element> = document.getElementsByClassName('autocomplete-items');

        for (let i = 0; i < lists.length; i++) {
            if (elementToKeep != lists[i]) {
                lists[i].parentNode.removeChild(lists[i]);
            }
        }
    }

}

export { PersonSelectForm };
