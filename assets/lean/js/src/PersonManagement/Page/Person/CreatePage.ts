import { PersonCreateForm } from '../../Form/Person/PersonCreateForm';
import { MessageBoxComponent } from '../../../Component/MessageBoxComponent/MessageBoxComponent';
import { MessageBoxAlertType } from '../../../Component/MessageBoxComponent/MessageBoxAlertType';

class CreatePage {

    private readonly personCreateForm: PersonCreateForm;
    private readonly personCreateFormElement: HTMLFormElement;
    private readonly personCreateFormSubmitButton: HTMLButtonElement;
    private readonly messageBoxComponent: MessageBoxComponent;

    constructor() {
        const idPrefix: string = 'person_create_';

        this.personCreateForm = new PersonCreateForm(idPrefix);
        this.personCreateFormElement = document.querySelector('#' + idPrefix + 'form');
        this.personCreateFormSubmitButton = document.querySelector('#' + idPrefix + 'save_submit_button');
        this.messageBoxComponent = new MessageBoxComponent();

        this.personCreateFormSubmitButton.addEventListener('click', (): void => {
            if (this.personCreateForm.isFormValid() === false) {
                this.messageBoxComponent.openMessageBox('Die Daten konnten nicht gespeichert werden. Bitte kontrollieren Sie die rot markierten Felder.', MessageBoxAlertType.DANGER);

                return;
            }

            this.personCreateFormElement.submit();
        });
    }

}

export { CreatePage };
