import { MdcTabBarPage } from '../../../MdcTabBarPage';
import { PersonPageTab } from './PersonPageTab';

abstract class PersonPage extends MdcTabBarPage {

    protected constructor(personPageTab: PersonPageTab) {
        super(document.querySelector('#person_mdc_tab_bar'))

        this.activateTab(personPageTab);
    }

}

export { PersonPage };
