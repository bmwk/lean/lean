import { PersonForm } from '../../Form/Person/PersonForm';
import { NaturalPersonForm } from '../../Form/Person/NaturalPersonForm';
import { CompanyForm } from '../../Form/Person/CompanyForm';
import { CommuneForm } from '../../Form/Person/CommuneForm';
import { PersonDeleteForm } from '../../Form/Person/PersonDeleteForm';

class DeletePage {

    private readonly personDeleteForm: PersonDeleteForm;
    private readonly personDeleteFormElement: HTMLFormElement;
    private readonly personDeleteFormSubmitButton: HTMLButtonElement;
    private readonly personForm: PersonForm;

    constructor() {
        const idPrefix: string = 'person_delete_';

        this.personDeleteForm = new PersonDeleteForm(idPrefix);
        this.personDeleteFormElement = document.querySelector('#' + idPrefix + 'form');
        this.personDeleteFormSubmitButton = document.querySelector('#' + idPrefix + 'delete_submit_button');

        if (document.querySelector('#natural_person_personTypeName') !== null) {
            this.personForm = new NaturalPersonForm('natural_person_');
        }

        if (document.querySelector('#company_personTypeName') !== null) {
            this.personForm = new CompanyForm('company_');
        }

        if (document.querySelector('#commune_personTypeName') !== null) {
            this.personForm = new CommuneForm('commune_');
        }

        this.personDeleteFormSubmitButton.addEventListener('click', (): void => {
            this.personDeleteFormElement.submit();
        });
    }

}

export { DeletePage };
