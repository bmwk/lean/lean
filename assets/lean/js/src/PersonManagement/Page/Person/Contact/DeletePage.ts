import { PersonPage } from '../PersonPage';
import { PersonPageTab } from '../PersonPageTab';
import { ContactForm } from '../../../Form/Contact/ContactForm';
import { ContactDeleteForm } from '../../../Form/Contact/ContactDeleteForm';
import { MessageBoxComponent } from '../../../../Component/MessageBoxComponent/MessageBoxComponent';
import { MessageBoxAlertType } from '../../../../Component/MessageBoxComponent/MessageBoxAlertType';

class DeletePage extends PersonPage {

    private readonly contactForm: ContactForm;
    private readonly contactDeleteForm: ContactDeleteForm;
    private readonly contactDeleteFormElement: HTMLFormElement;
    private readonly contactDeleteFormSubmitButton: HTMLButtonElement;
    private readonly messageBoxComponent: MessageBoxComponent;

    constructor() {
        super(PersonPageTab.ContactOverview);

        const idPrefix: string = 'contact_delete_';

        this.contactForm = new ContactForm('contact_');
        this.contactDeleteForm = new ContactDeleteForm(idPrefix);
        this.contactDeleteFormElement = document.querySelector('#' + idPrefix + 'form');
        this.contactDeleteFormSubmitButton = document.querySelector('#' + idPrefix + 'delete_submit_button');
        this.messageBoxComponent = new MessageBoxComponent();

        this.contactDeleteFormSubmitButton.addEventListener('click', (): void => {
            this.contactDeleteFormElement.submit();
        });
    }

}

export { DeletePage };
