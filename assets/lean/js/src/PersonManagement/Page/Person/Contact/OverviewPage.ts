import { PersonPage } from '../PersonPage';
import { PersonPageTab } from '../PersonPageTab';
import { ContactListComponent } from '../../../Component/Contact/ContactListComponent';

class OverviewPage extends PersonPage {

    private readonly contactListComponent: ContactListComponent;

    constructor() {
        super(PersonPageTab.ContactOverview);

        if (ContactListComponent.checkContainerExists('contact_') === true) {
            this.contactListComponent = new ContactListComponent('contact_');
        }
    }

}

export { OverviewPage };
