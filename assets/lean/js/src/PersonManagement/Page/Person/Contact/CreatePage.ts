import { PersonPage } from '../PersonPage';
import { PersonPageTab } from '../PersonPageTab';
import { ContactForm } from '../../../Form/Contact/ContactForm';
import { MessageBoxComponent } from '../../../../Component/MessageBoxComponent/MessageBoxComponent';
import { MessageBoxAlertType } from '../../../../Component/MessageBoxComponent/MessageBoxAlertType';

class CreatePage extends PersonPage {

    private readonly contactForm: ContactForm;
    private readonly contactCreateFormElement: HTMLFormElement;
    private readonly contactCreateFormSubmitButton: HTMLButtonElement;
    private readonly messageBoxComponent: MessageBoxComponent;

    constructor() {
        super(PersonPageTab.ContactOverview);

        const idPrefix: string = 'contact_';

        this.contactForm = new ContactForm(idPrefix);
        this.contactCreateFormElement = document.querySelector('#' + idPrefix + 'form');
        this.contactCreateFormSubmitButton = document.querySelector('#' + idPrefix + 'save_submit_button');
        this.messageBoxComponent = new MessageBoxComponent();

        this.contactCreateFormSubmitButton.addEventListener('click', (): void => {
            if (this.contactForm.isFormValid() === false) {
                this.messageBoxComponent.openMessageBox('Die Daten konnten nicht gespeichert werden. Bitte kontrollieren Sie die rot markierten Felder.', MessageBoxAlertType.DANGER);

                return;
            }

            this.contactCreateFormElement.submit();
        });
    }

}

export { CreatePage };
