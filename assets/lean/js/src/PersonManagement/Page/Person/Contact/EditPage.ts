import { PersonPage } from '../PersonPage';
import { PersonPageTab } from '../PersonPageTab';
import { ContactForm } from '../../../Form/Contact/ContactForm';
import { MdcContextMenuComponent } from '../../../../Component/MdcContextMenuComponent';
import { MessageBoxComponent } from '../../../../Component/MessageBoxComponent/MessageBoxComponent';
import { MessageBoxAlertType } from '../../../../Component/MessageBoxComponent/MessageBoxAlertType';

class EditPage extends PersonPage {

    private readonly contactForm: ContactForm;
    private readonly contactFormElement: HTMLFormElement;
    private readonly contactFormSubmitButton: HTMLButtonElement;
    private readonly contextMenu: MdcContextMenuComponent = null;
    private readonly messageBoxComponent: MessageBoxComponent;

    constructor() {
        super(PersonPageTab.ContactOverview);

        const idPrefix: string = 'contact_';

        this.contactForm = new ContactForm(idPrefix);
        this.contactFormElement = document.querySelector('#' + idPrefix + 'form');
        this.contactFormSubmitButton = document.querySelector('#' + idPrefix + 'save_submit_button');

        if (MdcContextMenuComponent.checkContainerExists(idPrefix) === true) {
            this.contextMenu = new MdcContextMenuComponent(idPrefix);
        }

        this.messageBoxComponent = new MessageBoxComponent();

        if (this.contactFormElement !== null) {
            this.contactFormSubmitButton.addEventListener('click', (): void => {
                if (this.contactForm.isFormValid() === false) {
                    this.messageBoxComponent.openMessageBox('Die Daten konnten nicht gespeichert werden. Bitte kontrollieren Sie die rot markierten Felder.', MessageBoxAlertType.DANGER);

                    return;
                }

                this.contactFormElement.submit();
            });
        }
    }

}

export { EditPage };
