import { PersonPage } from '../PersonPage';
import { PersonPageTab } from '../PersonPageTab';
import { PersonOccurrenceForm } from '../../../Form/Occurrence/PersonOccurrenceForm';
import { OccurrenceAttachmentComponent } from '../../../Component/Occurrence/OccurrenceAttachmentComponent';
import { MdcContextMenuComponent } from '../../../../Component/MdcContextMenuComponent';
import { MessageBoxComponent } from '../../../../Component/MessageBoxComponent/MessageBoxComponent';
import { MessageBoxAlertType } from '../../../../Component/MessageBoxComponent/MessageBoxAlertType';

class EditPage extends PersonPage {

    private readonly personOccurrenceForm: PersonOccurrenceForm;
    private readonly occurrenceFormElement: HTMLFormElement;
    private readonly occurrenceFormSubmitButton: HTMLButtonElement;
    private readonly occurrenceAttachmentComponent: OccurrenceAttachmentComponent;
    private readonly contextMenu: MdcContextMenuComponent = null;
    private readonly messageBoxComponent: MessageBoxComponent;

    constructor() {
        super(PersonPageTab.OccurrenceOverview);

        const idPrefix: string = 'person_occurrence_';

        this.personOccurrenceForm = new PersonOccurrenceForm(idPrefix);
        this.occurrenceFormElement = document.querySelector('#' + idPrefix + 'form');
        this.occurrenceFormSubmitButton = document.querySelector('#' + idPrefix + 'save_submit_button');
        this.occurrenceAttachmentComponent = new OccurrenceAttachmentComponent('person_occurrence_');

        if (MdcContextMenuComponent.checkContainerExists(idPrefix + 'occurrence_') === true) {
            this.contextMenu = new MdcContextMenuComponent(idPrefix + 'occurrence_');
        }

        this.messageBoxComponent = new MessageBoxComponent();

        if (this.occurrenceFormElement !== null) {
            this.occurrenceFormSubmitButton.addEventListener('click', (): void => {
                if (this.personOccurrenceForm.isFormValid() === false) {
                    this.messageBoxComponent.openMessageBox('Die Daten konnten nicht gespeichert werden. Bitte kontrollieren Sie die rot markierten Felder.', MessageBoxAlertType.DANGER);

                    return;
                }

                this.occurrenceFormElement.submit();
            });
        }
    }

}

export { EditPage };
