import { PersonOccurrenceForm } from '../../../Form/Occurrence/PersonOccurrenceForm';
import { OccurrenceDeleteForm } from '../../../Form/Occurrence/OccurrenceDeleteForm';

class DeletePage {

    private readonly occurrenceDeleteForm: OccurrenceDeleteForm;
    private readonly occurrenceDeleteFormElement: HTMLFormElement;
    private readonly occurrenceDeleteFormSubmitButton: HTMLButtonElement;
    private readonly personOccurrenceForm: PersonOccurrenceForm;

    constructor() {
        const idPrefix: string = 'occurrence_delete_';

        this.occurrenceDeleteForm = new OccurrenceDeleteForm(idPrefix);
        this.occurrenceDeleteFormElement = document.querySelector('#' + idPrefix + 'form');
        this.occurrenceDeleteFormSubmitButton = document.querySelector('#' + idPrefix + 'delete_submit_button');
        this.personOccurrenceForm = new PersonOccurrenceForm('person_occurrence_');

        this.personOccurrenceForm.disableFields();

        this.occurrenceDeleteFormSubmitButton.addEventListener('click', (): void => {
            this.occurrenceDeleteFormElement.submit();
        });
    }

}

export { DeletePage };
