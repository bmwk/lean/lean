import { PersonPage } from '../PersonPage';
import { PersonPageTab } from '../PersonPageTab';
import { PersonOccurrenceForm } from '../../../Form/Occurrence/PersonOccurrenceForm';
import { MessageBoxComponent } from '../../../../Component/MessageBoxComponent/MessageBoxComponent';
import { MessageBoxAlertType } from '../../../../Component/MessageBoxComponent/MessageBoxAlertType';

class CreatePage extends PersonPage {

    private readonly personOccurrenceForm: PersonOccurrenceForm;
    private readonly occurrenceCreateFormElement: HTMLFormElement;
    private readonly occurrenceCreateFormSubmitButton: HTMLButtonElement;
    private readonly messageBoxComponent: MessageBoxComponent;

    constructor() {
        super(PersonPageTab.OccurrenceOverview);

        const idPrefix: string = 'person_occurrence_';

        this.personOccurrenceForm = new PersonOccurrenceForm(idPrefix);
        this.occurrenceCreateFormElement = document.querySelector('#' + idPrefix + 'form');
        this.occurrenceCreateFormSubmitButton = document.querySelector('#' + idPrefix + 'save_submit_button');
        this.messageBoxComponent = new MessageBoxComponent();

        this.occurrenceCreateFormSubmitButton.addEventListener('click', (): void => {
            if (this.personOccurrenceForm.isFormValid() === false) {
                this.messageBoxComponent.openMessageBox('Die Daten konnten nicht gespeichert werden. Bitte kontrollieren Sie die rot markierten Felder.', MessageBoxAlertType.DANGER);

                return;
            }

            this.occurrenceCreateFormElement.submit();
        });
    }

}

export { CreatePage };
