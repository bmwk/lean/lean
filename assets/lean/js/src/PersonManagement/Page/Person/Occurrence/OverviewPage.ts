import { PersonPage } from '../PersonPage';
import { PersonPageTab } from '../PersonPageTab';
import { OccurrenceListComponent } from '../../../Component/Occurrence/OccurrenceListComponent';

class OverviewPage extends PersonPage {

    private readonly occurrenceListComponent: OccurrenceListComponent

    constructor() {
        super(PersonPageTab.OccurrenceOverview);

        if (OccurrenceListComponent.checkContainerExists('occurrence_') === true) {
            this.occurrenceListComponent = new OccurrenceListComponent('occurrence_');
        }
    }

}

export { OverviewPage };
