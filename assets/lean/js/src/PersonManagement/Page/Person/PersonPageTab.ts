enum PersonPageTab {
    BaseData = 'person_base_data_tab',
    PropertyOverview = 'person_property_overview_tab',
    LookingForPropertyRequestOverview = 'person_looking_for_property_request_overview_tab',
    OccurrenceOverview = 'person_occurrence_overview_tab',
    ContactOverview = 'person_contact_overview_tab'
}

export { PersonPageTab };
