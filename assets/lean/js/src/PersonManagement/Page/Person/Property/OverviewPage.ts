import { PersonPage } from '../PersonPage';
import { PersonPageTab } from '../PersonPageTab';
import { LazyImageLoader } from '../../../../LazyImageLoader';

class OverviewPage extends PersonPage {

    private readonly lazyImageLoader: LazyImageLoader;

    constructor() {
        super(PersonPageTab.PropertyOverview);

        this.lazyImageLoader = new LazyImageLoader();
    }

}

export { OverviewPage };
