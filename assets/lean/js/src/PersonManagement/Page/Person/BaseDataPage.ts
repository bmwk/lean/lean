import { PersonPage } from './PersonPage';
import { PersonPageTab } from './PersonPageTab';
import { PersonForm } from '../../Form/Person/PersonForm';
import { NaturalPersonForm } from '../../Form/Person/NaturalPersonForm';
import { CompanyForm } from '../../Form/Person/CompanyForm';
import { CommuneForm } from '../../Form/Person/CommuneForm';
import { MdcContextMenuComponent } from '../../../Component/MdcContextMenuComponent';
import { MessageBoxComponent } from '../../../Component/MessageBoxComponent/MessageBoxComponent';
import { MessageBoxAlertType } from '../../../Component/MessageBoxComponent/MessageBoxAlertType';

class BaseDataPage extends PersonPage {

    private readonly personForm: PersonForm;
    private readonly personFormElement: HTMLFormElement;
    private readonly personFormSubmitButton: HTMLButtonElement;
    private readonly contextMenu: MdcContextMenuComponent = null;
    private readonly messageBoxComponent: MessageBoxComponent;

    constructor() {
        super(PersonPageTab.BaseData);

        if (document.querySelector('#natural_person_personTypeName') !== null) {
            this.personForm = new NaturalPersonForm('natural_person_');
            this.personFormElement = document.querySelector('#natural_person_form');
            this.personFormSubmitButton = document.querySelector('#natural_person_save_submit_button');
        }

        if (document.querySelector('#company_personTypeName') !== null) {
            this.personForm = new CompanyForm('company_');
            this.personFormElement = document.querySelector('#company_form');
            this.personFormSubmitButton = document.querySelector('#company_save_submit_button');
        }

        if (document.querySelector('#commune_personTypeName') !== null) {
            this.personForm = new CommuneForm('commune_');
            this.personFormElement = document.querySelector('#commune_form');
            this.personFormSubmitButton = document.querySelector('#commune_save_submit_button');
        }

        if (MdcContextMenuComponent.checkContainerExists('person_base_data_') === true) {
            this.contextMenu = new MdcContextMenuComponent('person_base_data_')
        }

        this.messageBoxComponent = new MessageBoxComponent();

        if (this.personFormElement !== null) {
            this.personFormSubmitButton.addEventListener('click', (): void => {
                if (this.personForm.isFormValid() === false) {
                    this.messageBoxComponent.openMessageBox('Die Daten konnten nicht gespeichert werden. Bitte kontrollieren Sie die rot markierten Felder.', MessageBoxAlertType.DANGER);

                    return;
                }

                this.personFormElement.submit();
            });
        }
    }

}

export { BaseDataPage };
