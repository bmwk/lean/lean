import { PersonFilterForm } from '../../Form/Person/PersonFilterForm';
import { PersonSearchForm } from '../../Form/Person/PersonSearchForm';
import { PersonListComponent } from '../../Component/Person/PersonListComponent';

class OverviewPage {

    private readonly personFilterForm: PersonFilterForm;
    private readonly personSearchForm: PersonSearchForm;
    private readonly personListComponent: PersonListComponent;

    constructor() {
        this.personFilterForm = new PersonFilterForm('person_filter_');
        this.personSearchForm = new PersonSearchForm('person_search_');

        if (PersonListComponent.checkContainerExists('person_') === true) {
            this.personListComponent = new PersonListComponent('person_');
        }
    }

}

export { OverviewPage };
