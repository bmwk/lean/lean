import { PersonPage } from '../PersonPage';
import { PersonPageTab } from '../PersonPageTab';

class OverviewPage extends PersonPage {

    constructor() {
        super(PersonPageTab.LookingForPropertyRequestOverview);
    }

}

export { OverviewPage };
