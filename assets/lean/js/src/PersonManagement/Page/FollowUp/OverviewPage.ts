import { FollowUpListComponent } from '../../Component/FollowUp/FollowUpListComponent';

class OverviewPage {

    private readonly followUpListComponent: FollowUpListComponent;

    constructor() {
        if (FollowUpListComponent.checkContainerExists('follow_up_') === true) {
            this.followUpListComponent = new FollowUpListComponent('follow_up_');
        }
    }

}

export { OverviewPage };
