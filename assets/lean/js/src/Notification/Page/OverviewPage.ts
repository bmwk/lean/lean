import { NotificationFilterForm } from '../Form/NotificationFilterForm';
import { NotificationListComponent } from '../Component/NotificationListComponent';

class OverviewPage {

    private readonly notificationFilterForm: NotificationFilterForm;
    private readonly notificationListComponent: NotificationListComponent;

    constructor() {
        this.notificationFilterForm = new NotificationFilterForm('notification_filter_');

        if (NotificationListComponent.checkContainerExists('notification_') === true) {
            this.notificationListComponent = new NotificationListComponent('notification_');
        }
    }

}

export { OverviewPage };
