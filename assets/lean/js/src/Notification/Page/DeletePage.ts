import { NotificationDeleteForm } from '../Form/NotificationDeleteForm';

class DeletePage {

    private readonly notificationDeleteForm: NotificationDeleteForm;
    private readonly notificationDeleteFormElement: HTMLFormElement;
    private readonly notificationDeleteFormSubmitButton: HTMLButtonElement;

    constructor() {
        const idPrefix: string = 'notification_delete_';

        this.notificationDeleteForm = new NotificationDeleteForm(idPrefix);
        this.notificationDeleteFormElement = document.querySelector('#' + idPrefix + 'form');
        this.notificationDeleteFormSubmitButton = document.querySelector('#' + idPrefix + 'delete_submit_button');

        this.notificationDeleteFormSubmitButton.addEventListener('click', (): void => {
            this.notificationDeleteFormElement.submit();
        });
    }

}

export { DeletePage };
