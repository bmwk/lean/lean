import { DeleteForm } from '../../Form/DeleteForm';

class NotificationDeleteForm extends DeleteForm {

    constructor(idPrefix: string) {
        super(idPrefix);
    }

}

export { NotificationDeleteForm };
