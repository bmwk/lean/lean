import { ListComponent } from '../../Component/ListComponent';

class NotificationListComponent extends ListComponent {

    constructor(idPrefix: string) {
        super(idPrefix);
    }

}

export { NotificationListComponent };
