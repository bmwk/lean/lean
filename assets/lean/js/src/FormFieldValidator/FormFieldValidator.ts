class FormFieldValidator {

    public static validateEmail(email: String): boolean {
        const regExp: RegExp = new RegExp(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);

        const match: RegExpMatchArray = email.match(regExp);

        return match !== null && match.length > 0;
    }

}

export { FormFieldValidator };
