import { FollowUpReminderWidget } from '../../Widget/FollowUpReminderWidget';
import { LookingForPropertyRequestWidget } from '../../Widget/LookingForPropertyRequestWidget';
import { LookingForPropertyRequestReportWidget } from '../../Widget/LookingForPropertyRequestReportWidget';
import { MatchingWidget } from '../../Widget/MatchingWidget';
import { PropertyProviderInformationWidget } from '../../Widget/PropertyProviderInformationWidget';
import { PropertySeekerInformationWidget } from '../../Widget/PropertySeekerInformationWidget';
import { PropertySeekerLookingForPropertyRequestsWidget } from '../../Widget/PropertySeekerLookingForPropertyRequestsWidget';
import { PropertyVacancyManagementWidget } from '../../Widget/PropertyVacancyManagementWidget';
import { PropertyVacancyReportWidget } from '../../Widget/PropertyVacancyReportWidget';
import { HallOfInspirationWidget } from '../../Widget/HallOfInspirationWidget';
import CityGoalApp from '../../CityGoal/CityGoalApp';
import LocationIndicatorsWidget from '../../LocationIndicators/LocationIndicatorsWidget';
import PedestrianFrequencyWidget from '../../PedestrianFrequency/PedestrianFrequencyWidget';
import React from 'react';
import { createRoot, Root } from 'react-dom/client';

class DashboardPage {

    private readonly propertyVacancyManagementWidget: PropertyVacancyManagementWidget = null;
    private readonly propertyVacancyReportWidget: PropertyVacancyReportWidget = null;
    private readonly lookingForPropertyRequestWidget: LookingForPropertyRequestWidget = null;
    private readonly lookingForPropertyRequestReportWidget: LookingForPropertyRequestReportWidget = null;
    private readonly matchingWidget: MatchingWidget = null;
    private readonly followUpReminderWidget: FollowUpReminderWidget = null;
    private readonly hallOfInspirationWidget: HallOfInspirationWidget = null;
    private readonly propertySeekerInformationWidget: PropertySeekerInformationWidget = null;
    private readonly propertyProviderInformationWidget: PropertyProviderInformationWidget = null;
    private readonly propertySeekerLookingForPropertyRequestsWidget: PropertySeekerLookingForPropertyRequestsWidget = null;
    private readonly pedestrianFrequencyWidget: HTMLDivElement;
    private readonly cityGoalsWidget: HTMLDivElement;
    private readonly locationIndicatorsWidget: HTMLDivElement;

    constructor() {
        if (PropertyVacancyManagementWidget.checkContainerExists('dashboard_') === true) {
            this.propertyVacancyManagementWidget = new PropertyVacancyManagementWidget('dashboard_');
        }

        if (PropertyVacancyReportWidget.checkContainerExists('dashboard_') === true) {
            this.propertyVacancyReportWidget = new PropertyVacancyReportWidget('dashboard_');
        }

        if (LookingForPropertyRequestWidget.checkContainerExists('dashboard_') === true) {
            this.lookingForPropertyRequestWidget = new LookingForPropertyRequestWidget('dashboard_');
        }

        if (LookingForPropertyRequestReportWidget.checkContainerExists('dashboard_') === true) {
            this.lookingForPropertyRequestReportWidget = new LookingForPropertyRequestReportWidget('dashboard_');
        }

        if (MatchingWidget.checkContainerExists('dashboard_') === true) {
            this.matchingWidget = new MatchingWidget('dashboard_');
        }

        if (FollowUpReminderWidget.checkContainerExists('dashboard_') === true) {
            this.followUpReminderWidget = new FollowUpReminderWidget('dashboard_');
        }

        if (HallOfInspirationWidget.checkContainerExists('dashboard_') === true) {
            this.hallOfInspirationWidget = new HallOfInspirationWidget('dashboard_');
        }

        if (PropertySeekerInformationWidget.checkContainerExists('dashboard_') === true) {
            this.propertySeekerInformationWidget = new PropertySeekerInformationWidget('dashboard_');
        }

        if (PropertyProviderInformationWidget.checkContainerExists('dashboard_') === true) {
            this.propertyProviderInformationWidget = new PropertyProviderInformationWidget('dashboard_');
        }

        if (PropertySeekerLookingForPropertyRequestsWidget.checkContainerExists('dashboard_') === true) {
            this.propertySeekerLookingForPropertyRequestsWidget = new PropertySeekerLookingForPropertyRequestsWidget('dashboard_');
        }

        this.pedestrianFrequencyWidget = document.querySelector('#pedestrian_frequency_widget');
        this.cityGoalsWidget = document.querySelector('#city_goals_widget');
        this.locationIndicatorsWidget = document.querySelector('#location_indicators_widget');

        if (this.pedestrianFrequencyWidget !== null) {
            const pedestrianFrequencyWidgetRoot: Root = createRoot(this.pedestrianFrequencyWidget!);

            pedestrianFrequencyWidgetRoot.render(
                <React.StrictMode>
                    <PedestrianFrequencyWidget/>
                </React.StrictMode>
            );
        }

        if (this.cityGoalsWidget !== null) {
            const cityGoalsWidgetRoot: Root = createRoot(this.cityGoalsWidget!);

            cityGoalsWidgetRoot.render(
                <React.StrictMode>
                    <CityGoalApp isWidget={true}/>
                </React.StrictMode>
            );
        }

        if (this.locationIndicatorsWidget !== null) {
            const locationIndicatorsWidgetRoot: Root = createRoot(this.locationIndicatorsWidget!);

            locationIndicatorsWidgetRoot.render(
                <React.StrictMode>
                    <LocationIndicatorsWidget/>
                </React.StrictMode>
            );
        }

        if (localStorage.getItem('lean.dashboard.disabledWidgets') !== null) {
            localStorage.getItem('lean.dashboard.disabledWidgets').split(',').forEach(function (widget) {
                if (document.querySelector('#dashboard_' + widget) !== null) {
                    document.querySelector('#dashboard_' + widget).parentElement.classList.add('d-none');
                } else  if (document.querySelector('#' + widget) !== null) {
                    document.querySelector('#' + widget).parentElement.parentElement.classList.add('d-none');
                }

            });
        }
    }

}

export { DashboardPage };
