import { TabBarComponent } from './Component/TabBarComponent';
import { MDCTab } from '@material/tab';

abstract class MdcTabBarPage {

    protected readonly tabBar: TabBarComponent;

    protected constructor(mdcTabBarContainer: HTMLDivElement) {
        this.tabBar = new TabBarComponent(mdcTabBarContainer);

        this.tabBar.getTabs().forEach((mdcTab: MDCTab): void => {
            if ((<HTMLButtonElement>mdcTab.root).dataset.url !== undefined) {
                this.setTabClickEvent((<HTMLButtonElement>mdcTab.root));
            }
        });
    }

    protected activateTab(tabId: string): void {
        this.tabBar.activateTab(tabId);
    }

    private setTabClickEvent(tabButton: HTMLButtonElement): void {
        tabButton.addEventListener('click', (): void => {
            window.location.href = tabButton.dataset.url;
        });
    }

}

export { MdcTabBarPage };
