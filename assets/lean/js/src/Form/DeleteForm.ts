import { TextFieldComponent } from '../Component/TextFieldComponent';

abstract class DeleteForm {

    protected readonly verificationCodeTextField: TextFieldComponent;

    protected constructor(idPrefix: string) {
        this.verificationCodeTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'verificationCode_mdc_text_field'));

        this.verificationCodeTextField.mdcTextField.required = true;
    }

    public isFormValid(): boolean {
        let hasErrors: boolean = false;

        if (this.verificationCodeTextField.mdcTextField.valid === false) {
            this.verificationCodeTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        return (hasErrors === false);
    }

}

export { DeleteForm };
