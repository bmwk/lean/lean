enum LocationCategory {
    OneALocation = 0,
    OneBLocation = 1,
    TwoALocation = 2,
    TwoBLocation = 3,
    Other = 4,
    OneCLocation = 5,
    TwoCLocation = 6,
}

export { LocationCategory };
