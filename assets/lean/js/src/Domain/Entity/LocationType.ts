enum LocationType {
    InnerCity = 0,
    CityDistrict = 1,
    Peripheral = 2,
    HighFrequencySpecialLocation = 3,
    Solitary = 4,
}

export { LocationType };
