import { SupportRequestForm } from '../../Form/CustomerCare/SupportRequestForm';
import { MessageBoxAlertType } from '../../../Component/MessageBoxComponent/MessageBoxAlertType';
import { MessageBoxComponent } from '../../../Component/MessageBoxComponent/MessageBoxComponent';

class SupportRequestPage {

    private readonly supportRequestForm: SupportRequestForm;
    private readonly supportRequestFormElement: HTMLFormElement;
    private readonly supportRequestFormSubmitButton: HTMLButtonElement;
    private readonly messageBoxComponent: MessageBoxComponent;

    constructor() {
        const idPrefix: string = 'support_request_';

        this.supportRequestForm = new SupportRequestForm(idPrefix);
        this.supportRequestFormElement = document.querySelector('#' + idPrefix + 'form');
        this.supportRequestFormSubmitButton = document.querySelector('#' + idPrefix + 'save_submit_button');
        this.messageBoxComponent = new MessageBoxComponent();

        this.supportRequestFormSubmitButton.addEventListener('click', (): void => {
            if (this.supportRequestForm.isFormValid() === false) {
                this.messageBoxComponent.openMessageBox('Die Daten konnten nicht gespeichert werden. Bitte kontrollieren Sie die rot markierten Felder.', MessageBoxAlertType.DANGER);

                return;
            }

            this.supportRequestFormElement.submit();
        });
    }

}

export { SupportRequestPage };
