import { TextFieldComponent } from '../../../Component/TextFieldComponent';
import { FormFieldValidator } from '../../../FormFieldValidator/FormFieldValidator';

class SupportRequestForm {

    public readonly fullNameTextField: TextFieldComponent;
    public readonly emailTextField: TextFieldComponent;
    public readonly subjectTextField: TextFieldComponent;
    public readonly messageTextField: TextFieldComponent;

    constructor(idPrefix: string) {
        this.fullNameTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'fullName_mdc_text_field'));
        this.emailTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'email_mdc_text_field'));
        this.subjectTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'subject_mdc_text_field'));
        this.messageTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'message_mdc_text_field'));

        this.fullNameTextField.mdcTextField.required = true;
        this.emailTextField.mdcTextField.required = true;
        this.subjectTextField.mdcTextField.required = true;
        this.messageTextField.mdcTextField.required = true;
    }

    public isFormValid(): boolean {
        let hasErrors: boolean = false;

        if (this.fullNameTextField.mdcTextField.valid === false) {
            this.fullNameTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (FormFieldValidator.validateEmail(this.emailTextField.mdcTextField.value) === false) {
            this.emailTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (this.subjectTextField.mdcTextField.valid === false) {
            this.subjectTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (this.messageTextField.mdcTextField.valid === false) {
            this.messageTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        return (hasErrors === false);
    }

}

export { SupportRequestForm };
