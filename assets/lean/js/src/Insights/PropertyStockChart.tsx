import Axios from 'axios';
import React, { useEffect, useState } from 'react';
import { Cell, Legend, Pie, PieChart, ResponsiveContainer, Tooltip } from 'recharts';
import LoadingImage from '../../../images/lean-loading.gif';
import { NaceClassification } from '../Property/PropertyApiResponse';

interface PropertyStockData {
    amount: number;
    industryClassification: NaceClassification;
}

interface PropertyStockChartData {
    name: string,
    value: number
}

const PropertyStockChart = () => {

    const COLORS = ['#014A5D', '#1D81A1', '#70A03C', '#B33052',
                    '#F7951A', '#049583', '#62C2D0', '#8AC149',
                    '#E84769', '#FBBD14', '#88927A', '#D2D5D7',
                    '#64AAC8', '#8FC340', '#A3A7A9', '#585F57',
                    '#000000'];

    const [propertyStockChartData, setPropertyStockChartData] = useState<PropertyStockChartData[]>([]);
    const [total, setTotal] = useState<number>(null);
    const [isLoading, setIsLoading] = useState<boolean>(true);
    const [errorMessage, setErrorMessage] = useState<string>(null);

    const fetchPropertyStockData = async (): Promise<PropertyStockData[]> => {
        const response = await Axios('/api/statistics/building-unit/amount-building-units-by-current-usage');
        return response.data;
    };

    useEffect(() => {
        fetchPropertyStockData().then((propertyStockData: PropertyStockData[]) => {
            let chartData: PropertyStockChartData[] = propertyStockData.map((propertyStock): PropertyStockChartData => {
                return {
                    name: propertyStock.industryClassification !== null ? propertyStock.industryClassification.name : 'Keine Nutzung angegeben',
                    value: propertyStock.amount
                };
            });
            chartData = chartData.filter(chartData => chartData.value !== 0);
            chartData = chartData.sort((chartDataA, chartDataB) => chartDataB.value - chartDataA.value);
            setPropertyStockChartData(chartData);
        }).catch(() => {
            setErrorMessage('Beim Laden der Daten ist ein Fehler aufgetreten');
        }).finally(() => {
            setIsLoading(false);
        });
    }, []);

    useEffect(() => {
        if (propertyStockChartData.length === 0) {
            return;
        }
        const total = propertyStockChartData.map((propertyStockChartData) => propertyStockChartData.value)?.reduce((previousValue, currentValue) => previousValue + currentValue);
        setTotal(total);
    }, [propertyStockChartData]);

    if (isLoading === true) {
        return (
            <div style={{height: 500}} className="d-flex justify-content-center align-items-center">
                <img src={LoadingImage} alt="lean-logo"/>
            </div>
        );
    }

    if (errorMessage === null) {
        return (
            <>
                <p className="text-center fs-4 text-primary mb-0">
                    Verteilung der in Nutzung befindlichen Objekte nach ihrer Klassifizierung
                    <span className="text-secondary ms-1">({new Intl.NumberFormat('de-DE').format(total)} insgesamt)</span>
                </p>
                <ResponsiveContainer width="100%" height={500}>
                    <PieChart width={400} height={400}>
                        <Pie data={propertyStockChartData} dataKey="value" nameKey="name" cx="50%" cy="50%" innerRadius="40%">
                            {propertyStockChartData.map((entry, index) => (
                                <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]}/>
                            ))}
                        </Pie>
                        <Tooltip/>
                        <Legend layout="horizontal" align="center" verticalAlign="bottom"
                                formatter={(value: string, entry: any) => `${value} (${entry.payload.value})`}/>
                    </PieChart>
                </ResponsiveContainer>
            </>
        );
    } else {
        return (
            <p>{errorMessage}</p>
        );
    }
};

export { PropertyStockChart };
