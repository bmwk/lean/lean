import { FormControl, InputLabel, MenuItem, Select } from '@mui/material';
import Axios from 'axios';
import React, { useEffect, useState } from 'react';
import { Bar, BarChart, CartesianGrid, ResponsiveContainer, Tooltip, XAxis, YAxis } from 'recharts';
import LoadingImage from '../../../images/lean-loading.gif';

interface LookingForPropertyRequestData {
    year: number;
    month: number;
    amount: number;
}

interface LookingForPropertyRequestsChartData {
    name: string,
    value: number
}

function getMonthName(month: number): string {
    const date = new Date();
    date.setMonth(month);

    return date.toLocaleString('de-DE', {
        month: 'long',
    });
}

function createYearSelectionOptions(): number[] {
    const years: number[] = [];
    const maxDate = new Date().getFullYear();
    for (let i = 2020; i <= maxDate; i++) {
        years.push(i);
    }
    return years;
}

const LookingForPropertyRequestReportChart = () => {

    const [lookingForPropertyRequestChartData, setLookingForPropertyRequestChartData] = useState<LookingForPropertyRequestsChartData[]>([]);
    const [year, setYear] = useState<number>(new Date().getFullYear());
    const [total, setTotal] = useState<number>(null);
    const [isLoading, setIsLoading] = useState<boolean>(true);
    const [errorMessage, setErrorMessage] = useState<string>(null);

    const yearOptions = createYearSelectionOptions();

    const fetchLookingForPropertyRequestsData = async (): Promise<LookingForPropertyRequestData[]> => {
        setIsLoading(true);
        const response = await Axios(`/api/statistics/looking-for-property-request-report/amount-looking-for-property-request-reports?year=${year}`);
        return response.data;
    };

    useEffect(() => {
        fetchLookingForPropertyRequestsData().then((lookingForPropertyRequestData: LookingForPropertyRequestData[]) => {
            let chartData: LookingForPropertyRequestsChartData[] = lookingForPropertyRequestData.map((lookingForPropertyRequests): LookingForPropertyRequestsChartData => {
                return {
                    name: getMonthName(lookingForPropertyRequests.month),
                    value: lookingForPropertyRequests.amount
                };
            });
            setLookingForPropertyRequestChartData(chartData);
        }).catch(() => {
            setErrorMessage('Beim Laden der Daten ist ein Fehler aufgetreten');
        }).finally(() => {
            setIsLoading(false);
        });
    }, [year]);

    useEffect(() => {
        if (lookingForPropertyRequestChartData.length === 0) {
            return;
        }
        const total = lookingForPropertyRequestChartData.map((propertyVacancyReportsChartData) => propertyVacancyReportsChartData.value).reduce((previousValue, currentValue) => previousValue + currentValue);
        setTotal(total);
    }, [lookingForPropertyRequestChartData]);

    if (isLoading === true) {
        return (
            <div style={{height: 500}} className="d-flex justify-content-center align-items-center">
                <img src={LoadingImage} alt="lean-logo"/>
            </div>
        );
    }

    if (errorMessage === null) {
        return (
            <>
                <div className="row gap-3 align-items-center justify-content-center mb-4">
                    <div className="col-12 col-md">
                        <p className="fs-4 text-primary text-center mb-0">
                            Verteilung der gemeldeten Ansiedlungsgesuche
                            <span className="text-secondary ms-1 me-5">({new Intl.NumberFormat('de-DE').format(total)} insgesamt)</span>
                        </p>
                    </div>
                    <div className="col-12 col-md-3">
                        <FormControl variant="filled" fullWidth>
                            <InputLabel id="select_toggler_year" placeholder={'Bitte wählen'}>Jahr</InputLabel>
                            <Select
                                labelId="select_toggler_year"
                                label="Jahr"
                                value={year}
                                onChange={(event) => setYear(event.target.value as number)}
                            >
                                {yearOptions.map((value, index) => <MenuItem style={{whiteSpace: 'normal'}} key={index} value={value}>{value}</MenuItem>)}
                            </Select>
                        </FormControl>
                    </div>
                </div>
                <ResponsiveContainer width="100%" height={500}>
                    <BarChart data={lookingForPropertyRequestChartData}>
                        <XAxis tickFormatter={(label) => getMonthName(label)}/>
                        <YAxis/>
                        <Tooltip labelFormatter={(label) => getMonthName(label)}/>
                        <CartesianGrid strokeDasharray="3 3"/>
                        <Bar dataKey="value" name="Gesuchsmeldungen" fill="#014A5D"/>
                    </BarChart>
                </ResponsiveContainer>
            </>
        );
    } else {
        return (
            <p>{errorMessage}</p>
        );
    }
};

export { LookingForPropertyRequestReportChart };
