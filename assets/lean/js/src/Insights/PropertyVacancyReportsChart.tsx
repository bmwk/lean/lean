import { FormControl, InputLabel, MenuItem, Select } from '@mui/material';
import Axios from 'axios';
import React, { useEffect, useState } from 'react';
import { Bar, BarChart, CartesianGrid, ResponsiveContainer, Tooltip, XAxis, YAxis } from 'recharts';
import LoadingImage from '../../../images/lean-loading.gif';

interface PropertyVacancyReportsData {
    year: number;
    month: number;
    amount: number;
}

interface PropertyVacancyReportsChartData {
    name: string,
    value: number
}

function getMonthName(month: number): string {
    const date = new Date();
    date.setMonth(month);

    return date.toLocaleString('de-DE', {
        month: 'long',
    });
}

function createYearSelectionOptions(): number[] {
    const years: number[] = [];
    const maxDate = new Date().getFullYear();
    for (let i = 2020; i <= maxDate; i++) {
        years.push(i);
    }
    return years;
}

const PropertyVacancyReportsChart = () => {

    const [propertyVacancyReportsChartData, setPropertyVacancyReportsChartData] = useState<PropertyVacancyReportsChartData[]>([]);
    const [year, setYear] = useState<number>(new Date().getFullYear());
    const [total, setTotal] = useState<number>(null);
    const [isLoading, setIsLoading] = useState<boolean>(true);
    const [errorMessage, setErrorMessage] = useState<string>(null);

    const yearOptions = createYearSelectionOptions();

    const fetchPropertyVacancyReportsData = async (): Promise<PropertyVacancyReportsData[]> => {
        setIsLoading(true);
        const response = await Axios(`/api/statistics/property-vacancy-report/amount-property-vacancy-reports?year=${year}`);
        return response.data;
    };

    useEffect(() => {
        fetchPropertyVacancyReportsData().then((propertyVacancyReportsData: PropertyVacancyReportsData[]) => {
            let chartData: PropertyVacancyReportsChartData[] = propertyVacancyReportsData.map((propertyVacancyReports): PropertyVacancyReportsChartData => {
                return {
                    name: getMonthName(propertyVacancyReports.month),
                    value: propertyVacancyReports.amount
                };
            });
            setPropertyVacancyReportsChartData(chartData);
        }).catch(() => {
            setErrorMessage('Beim Laden der Daten ist ein Fehler aufgetreten');
        }).finally(() => {
            setIsLoading(false);
        });
    }, [year]);

    useEffect(() => {
        if (propertyVacancyReportsChartData.length === 0) {
            return;
        }
        const total = propertyVacancyReportsChartData.map((propertyVacancyReportsChartData) => propertyVacancyReportsChartData.value).reduce((previousValue, currentValue) => previousValue + currentValue);
        setTotal(total);
    }, [propertyVacancyReportsChartData]);

    if (isLoading === true) {
        return (
            <div style={{height: 500}} className="d-flex justify-content-center align-items-center">
                <img src={LoadingImage} alt="lean-logo"/>
            </div>
        );
    }

    if (errorMessage === null) {
        return (
            <>
                <div className="row justify-content-center align-items-center gap-3 mb-4">
                    <div className="col-12 col-md">
                        <p className="fs-4 text-primary text-center mb-0">
                            Verteilung der gemeldeten Leerstände
                            <span className="text-secondary ms-1 me-5">({new Intl.NumberFormat('de-DE').format(total)} insgesamt)</span>
                        </p>
                    </div>
                    <div className="col-12 col-md-3">
                        <FormControl variant="filled" fullWidth>
                            <InputLabel id="select_toggler_year" placeholder={'Bitte wählen'}>Jahr</InputLabel>
                            <Select
                                labelId="select_toggler_year"
                                label="Jahr"
                                value={year}
                                onChange={(event) => setYear(event.target.value as number)}
                            >
                                {yearOptions.map((value, index) => <MenuItem style={{whiteSpace: 'normal'}} key={index} value={value}>{value}</MenuItem>)}
                            </Select>
                        </FormControl>
                    </div>
                </div>
                <ResponsiveContainer width="100%" height={500}>
                    <BarChart data={propertyVacancyReportsChartData}>
                        <XAxis tickFormatter={(label) => getMonthName(label)}/>
                        <YAxis/>
                        <Tooltip labelFormatter={(label) => getMonthName(label)}/>
                        <CartesianGrid strokeDasharray="3 3"/>
                        <Bar dataKey="value" name="Leerstandsmeldungen" fill="#014A5D"/>
                    </BarChart>
                </ResponsiveContainer>
            </>
        );
    } else {
        return (
            <p>{errorMessage}</p>
        );
    }
};

export { PropertyVacancyReportsChart };
