import { UserRegistrationForm } from '../Form/UserRegistrationForm';
import { NaturalPersonUserRegistrationForm } from '../Form/NaturalPersonUserRegistrationForm';
import { CompanyUserRegistrationForm } from '../Form/CompanyUserRegistrationForm';
import { MDCTabBar, MDCTabBarActivatedEvent } from '@material/tab-bar';

class UserRegistrationWithoutInvitationPage {

    private readonly naturalPersonUserRegistrationForm: UserRegistrationForm;
    private readonly naturalPersonUserRegistrationFormElement: HTMLFormElement;
    private readonly naturalPersonUserRegistrationFormSubmitButton: HTMLButtonElement;
    private readonly companyUserRegistrationForm: UserRegistrationForm;
    private readonly companyUserRegistrationFormElement: HTMLFormElement;
    private readonly companyUserRegistrationFormSubmitButton: HTMLButtonElement;
    private readonly personTypeMdcTabBar: MDCTabBar;
    private readonly naturalPersonFormContainer: HTMLDivElement;
    private readonly companyFormContainer: HTMLDivElement;

    constructor() {
        this.naturalPersonUserRegistrationForm = new NaturalPersonUserRegistrationForm('natural_person_user_registration_');
        this.naturalPersonUserRegistrationFormElement = document.querySelector('#natural_person_user_registration_form');
        this.naturalPersonUserRegistrationFormSubmitButton = document.querySelector('#natural_person_user_registration_save_submit_button');
        this.companyUserRegistrationForm = new CompanyUserRegistrationForm('company_user_registration_');
        this.companyUserRegistrationFormElement = document.querySelector('#company_user_registration_form');
        this.companyUserRegistrationFormSubmitButton = document.querySelector('#company_user_registration_save_submit_button');
        this.personTypeMdcTabBar = new MDCTabBar(document.querySelector('#user_registration_person_type_mdc_tab_bar'));
        this.naturalPersonFormContainer = document.querySelector('#user_registration_natural_person_form_container');
        this.companyFormContainer = document.querySelector('#user_registration_company_form_container');

        this.naturalPersonUserRegistrationForm.setRequiredFields(true);
        this.companyUserRegistrationForm.setRequiredFields(true);

        this.personTypeMdcTabBar.listen('MDCTabBar:activated', (event: MDCTabBarActivatedEvent): void => {
            this.showOrHideFormContainersByTabIndex(event.detail.index);
        });

        this.personTypeMdcTabBar.activateTab(0);
    }

    private showOrHideFormContainersByTabIndex(tabIndex: number): void {
        this.hideAllFormContainers();

        switch (tabIndex) {
            case 0:
                this.showNaturalPersonUserRegistrationForm();
                break
            case 1:
                this.showCompanyUserRegistrationForm();
                break
        }
    }

    private hideAllFormContainers(): void {
        this.hideNaturalPersonUserRegistrationForm();
        this.hideCompanyUserRegistrationForm();
    }

    private showNaturalPersonUserRegistrationForm(): void {
        this.naturalPersonFormContainer.classList.remove('d-none');
    }

    private hideNaturalPersonUserRegistrationForm(): void {
        this.naturalPersonFormContainer.classList.add('d-none');
    }

    private showCompanyUserRegistrationForm(): void {
        this.companyFormContainer.classList.remove('d-none');
    }

    private hideCompanyUserRegistrationForm(): void {
        this.companyFormContainer.classList.add('d-none');
    }

}

export { UserRegistrationWithoutInvitationPage };
