import { UserRegistrationForm } from '../Form/UserRegistrationForm';
import { NaturalPersonUserRegistrationForm } from '../Form/NaturalPersonUserRegistrationForm';
import { CompanyUserRegistrationForm } from '../Form/CompanyUserRegistrationForm';

class UserRegistrationWithInvitationPage {

    private readonly userRegistrationForm: UserRegistrationForm;
    private readonly userRegistrationFormElement: HTMLFormElement;
    private readonly userRegistrationFormSubmitButton: HTMLButtonElement;

    constructor() {
        if (document.querySelector('#natural_person_user_registration_form') !== null) {
            this.userRegistrationForm = new NaturalPersonUserRegistrationForm('natural_person_user_registration_');
            this.userRegistrationFormElement = document.querySelector('#natural_person_user_registration_form');
            this.userRegistrationFormSubmitButton = document.querySelector('#natural_person_user_registration_save_submit_button');
        }

        if (document.querySelector('#company_user_registration_form') !== null) {
            this.userRegistrationForm = new CompanyUserRegistrationForm('company_user_registration_');
            this.userRegistrationFormElement = document.querySelector('#company_user_registration_form');
            this.userRegistrationFormSubmitButton = document.querySelector('#company_user_registration_save_submit_button');
        }

        this.userRegistrationForm.setRequiredFields(true);
    }

}

export { UserRegistrationWithInvitationPage };
