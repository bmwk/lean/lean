import { TextFieldComponent } from '../../Component/TextFieldComponent';

class LoginForm {

    private readonly usernameTextField: TextFieldComponent;
    private readonly passwordTextField: TextFieldComponent;

    constructor() {
        this.usernameTextField = new TextFieldComponent(document.querySelector('#username-mdc-text-field'));
        this.passwordTextField = new TextFieldComponent(document.querySelector('#password-mdc-text-field'));
    }

}

export { LoginForm };
