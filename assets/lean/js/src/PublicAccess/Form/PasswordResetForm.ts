import { TextFieldComponent } from '../../Component/TextFieldComponent';
import { PasswordStrengthBar } from '../../PasswordStrengthBar';

class PasswordResetForm {

    private readonly passwordTextField: TextFieldComponent;
    private readonly passwordRepetitionTextField: TextFieldComponent;
    private readonly passwordStrengthBar: PasswordStrengthBar;

    constructor(idPrefix: string) {
        this.passwordTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'password_first_mdc_text_field'));
        this.passwordRepetitionTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'password_second_mdc_text_field'));
        this.passwordStrengthBar = new PasswordStrengthBar(this.passwordTextField.mdcTextField);

        this.passwordTextField.mdcTextField.required = true;
        this.passwordRepetitionTextField.mdcTextField.required = true;
    }

    public isFormValid(): boolean {
        return (
            this.passwordTextField.mdcTextField.valid === true
            && this.passwordRepetitionTextField.mdcTextField.valid === true
            && this.passwordTextField.mdcTextField.value === this.passwordRepetitionTextField.mdcTextField.value
        );
    }

}

export { PasswordResetForm };
