import { FormFieldValidator } from '../../FormFieldValidator/FormFieldValidator';
import { TextFieldComponent } from '../../Component/TextFieldComponent';

class PasswordForgottenForm {

    private readonly emailTextField: TextFieldComponent;

    constructor(idPrefix: string) {
        this.emailTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'email_mdc_text_field'));
    }

    public setRequiredFields(isRequired: boolean): void {
        this.emailTextField.mdcTextField.required = isRequired;
    }

    public isFormValid(): boolean {
        let hasErrors: boolean = false;

        if (FormFieldValidator.validateEmail(this.emailTextField.mdcTextField.value) === false) {
            this.emailTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        return (hasErrors === false);
    }

}

export { PasswordForgottenForm };
