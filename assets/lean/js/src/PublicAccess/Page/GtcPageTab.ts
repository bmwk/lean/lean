enum GtcPageTab {
    GtcProvider = 'gtc_provider_tab',
    GtcSeeker = 'gtc_seeker_tab',
}

export { GtcPageTab };
