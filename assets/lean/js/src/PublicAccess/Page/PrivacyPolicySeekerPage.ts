import { PrivacyPolicyPage } from './PrivacyPolicyPage';
import { PrivacyPolicyPageTab } from './PrivacyPolicyPageTab';

class PrivacyPolicySeekerPage extends PrivacyPolicyPage {

    constructor() {
        super(PrivacyPolicyPageTab.PrivacyPolicySeeker);

    }

}

export { PrivacyPolicySeekerPage };
