import { PasswordForgottenForm } from '../Form/PasswordForgottenForm';

class PasswordForgottenPage {

    private readonly passwordForgottenForm: PasswordForgottenForm;
    private readonly userRegistrationFormElement: HTMLFormElement;
    private readonly userRegistrationFormSubmitButtonElement: HTMLButtonElement;

    constructor() {
        const idPrefix = 'password_forgotten_';
        this.passwordForgottenForm = new PasswordForgottenForm(idPrefix);
        this.passwordForgottenForm.setRequiredFields(true);

        this.userRegistrationFormElement = document.querySelector('#' + idPrefix + 'form');
        this.userRegistrationFormSubmitButtonElement = document.querySelector('#' + idPrefix + 'save_submit_button');
    }
}

export { PasswordForgottenPage };
