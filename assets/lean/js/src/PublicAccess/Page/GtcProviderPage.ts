import { GtcPage } from './GtcPage';
import { GtcPageTab } from './GtcPageTab';

class GtcProviderPage extends GtcPage {

    constructor() {
        super(GtcPageTab.GtcProvider);

    }

}

export { GtcProviderPage };
