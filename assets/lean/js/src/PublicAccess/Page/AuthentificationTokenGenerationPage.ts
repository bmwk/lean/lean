class AuthentificationTokenGenerationPage {

    private readonly tokenTextarea: HTMLTextAreaElement;
    private readonly tokenCopyButton: HTMLButtonElement;

    constructor() {
        this.tokenTextarea = document.querySelector('textarea[name="tokenTextarea"]');
        this.tokenCopyButton = document.querySelector('button[name="tokenCopyButton"]');

        this.activateTokenCopyButton();
    }

    private activateTokenCopyButton(): void {
        this.tokenCopyButton.addEventListener('click', (): void => {
            if (!navigator.clipboard) {
                this.tokenTextarea.select();
                document.execCommand('copy');
                this.codeCopiedEvent(true);
            } else {
                navigator.clipboard.writeText(this.tokenTextarea.value).then((): void => {
                    this.codeCopiedEvent(true);
                }, (): void => {
                    this.codeCopiedEvent(false);
                });
            }
        });
    }

    private codeCopiedEvent(success: boolean): void {
        if (success) {
            this.tokenCopyButton.textContent = 'Kopiert';
        } else {
            this.tokenCopyButton.textContent = 'Fehler: Nicht kopiert';
        }

        setTimeout((): void => {
            this.tokenCopyButton.textContent = 'Token kopieren';
        }, 1500);
    }

}

export { AuthentificationTokenGenerationPage };
