import { PrivacyPolicyPage } from './PrivacyPolicyPage';
import { PrivacyPolicyPageTab } from './PrivacyPolicyPageTab';

class PrivacyPolicyProviderPage extends PrivacyPolicyPage {

    constructor() {
        super(PrivacyPolicyPageTab.PrivacyPolicyProvider);

    }

}

export { PrivacyPolicyProviderPage };
