import { BasePage } from './BasePage';
import { LoginForm } from '../Form/LoginForm';
import { MDCTabBar } from '@material/tab-bar';
import { MDCTab } from '@material/tab';

class LoginPage extends BasePage {

    private readonly loginForm: LoginForm;
    private readonly tabBar: MDCTabBar;
    private readonly loginTab: MDCTab;
    private readonly informationTab: MDCTab;
    private readonly loginContainer: HTMLDivElement;
    private readonly informationContainer: HTMLDivElement;

    constructor() {
        super();

        this.loginForm = new LoginForm;
        this.tabBar = new MDCTabBar(document.querySelector('#login_tab_bar'));
        this.loginTab = new MDCTab(document.querySelector('#login_tab'));
        this.informationTab = new MDCTab(document.querySelector('#information_tab'));
        this.loginContainer = document.querySelector('#login_container');
        this.informationContainer = document.querySelector('#information_container');

        this.loginTab.root.addEventListener('click', (mouseEvent: MouseEvent): void => {
            mouseEvent.preventDefault();
            this.selectLogin();
        });

        this.informationTab.root.addEventListener('click', (mouseEvent: MouseEvent): void => {
            mouseEvent.preventDefault();
            this.selectInformation();
        });

        this.selectLogin();
    }

    private selectLogin(): void {
        this.tabBar.activateTab(0);
        this.loginContainer.classList.remove('d-none');
        this.informationContainer.classList.add('d-none')
    }

    private selectInformation(): void {
        this.tabBar.activateTab(2);
        this.informationContainer.classList.remove('d-none');
        this.loginContainer.classList.add('d-none')
    }

}

export { LoginPage };
