import { MdcContextMenuComponent } from '../../Component/MdcContextMenuComponent';
import { PrivacyPolicyPageTab } from './PrivacyPolicyPageTab';
import { MdcTabBarPage } from "../../MdcTabBarPage";

class PrivacyPolicyPage extends MdcTabBarPage {

    private readonly contextMenu: MdcContextMenuComponent;

    constructor(privacyPolicyPageTab: PrivacyPolicyPageTab) {
        super(document.querySelector('#privacy_policy_tab_bar'));
        this.contextMenu = new MdcContextMenuComponent('public_access_base_');

        this.activateTab(privacyPolicyPageTab);
    }

}

export { PrivacyPolicyPage };
