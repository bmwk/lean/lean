import { MdcContextMenuComponent } from '../../Component/MdcContextMenuComponent';
import { GtcPageTab } from './GtcPageTab';
import { MdcTabBarPage } from "../../MdcTabBarPage";

class GtcPage extends MdcTabBarPage {

    private readonly contextMenu: MdcContextMenuComponent;

    constructor(gtcPageTab: GtcPageTab) {
        super(document.querySelector('#gtc_tab_bar'));
        this.contextMenu = new MdcContextMenuComponent('public_access_base_');

        this.activateTab(gtcPageTab);
    }

}

export { GtcPage };
