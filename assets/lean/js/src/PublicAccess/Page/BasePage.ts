import { MdcContextMenuComponent } from '../../Component/MdcContextMenuComponent';

class BasePage {

    private readonly contextMenu: MdcContextMenuComponent;

    constructor() {
        this.contextMenu = new MdcContextMenuComponent('public_access_base_');
    }

}

export { BasePage };
