import { PasswordResetForm } from '../Form/PasswordResetForm';
import { MessageBoxComponent } from '../../Component/MessageBoxComponent/MessageBoxComponent';
import { MessageBoxAlertType } from '../../Component/MessageBoxComponent/MessageBoxAlertType';

class PasswordResetPage {

    private readonly passwordResetForm: PasswordResetForm;
    private readonly passwordResetFormElement: HTMLFormElement;
    private readonly passwordResetFormSubmitButton: HTMLButtonElement;
    private readonly messageBoxComponent: MessageBoxComponent;

    constructor() {
        const idPrefix = 'password_reset_';
        this.passwordResetForm = new PasswordResetForm(idPrefix);

        this.passwordResetFormElement = document.querySelector('#' + idPrefix + 'form');
        this.passwordResetFormSubmitButton = document.querySelector('#' + idPrefix + 'save_submit_button');
        this.messageBoxComponent = new MessageBoxComponent();

        this.passwordResetFormSubmitButton.addEventListener('click', (mouseEvent: MouseEvent): void => {
            if (this.passwordResetForm.isFormValid() === false) {
                mouseEvent.preventDefault();

                this.messageBoxComponent.openMessageBox('Bitte geben Sie in beide Felder das selbe Passwort ein.', MessageBoxAlertType.DANGER);

                return;
            }

            this.passwordResetFormElement.submit();
        });
    }

}

export { PasswordResetPage };
