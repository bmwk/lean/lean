import { GtcPage } from './GtcPage';
import { GtcPageTab } from './GtcPageTab';

class GtcSeekerPage extends GtcPage {

    constructor() {
        super(GtcPageTab.GtcSeeker);

    }

}

export { GtcSeekerPage };
