enum PrivacyPolicyPageTab {
    PrivacyPolicyProvider = 'privacy_policy_provider_tab',
    PrivacyPolicySeeker = 'privacy_policy_seeker_tab',
}

export { PrivacyPolicyPageTab };
