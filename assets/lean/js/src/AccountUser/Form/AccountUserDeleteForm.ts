import { DeleteForm } from '../../Form/DeleteForm';

class AccountUserDeleteForm extends DeleteForm {

    constructor(idPrefix: string) {
        super(idPrefix);
    }

}

export { AccountUserDeleteForm };
