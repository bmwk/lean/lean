import { TextFieldComponent } from '../../Component/TextFieldComponent';
import { FormFieldValidator } from '../../FormFieldValidator/FormFieldValidator';
import { PasswordStrengthBar } from '../../PasswordStrengthBar';
import { PersonForm } from '../../PersonManagement/Form/Person/PersonForm';
import { NaturalPersonForm } from '../../PersonManagement/Form/Person/NaturalPersonForm';
import { CompanyForm } from '../../PersonManagement/Form/Person/CompanyForm';
import { CommuneForm } from '../../PersonManagement/Form/Person/CommuneForm';

class BaseDataForm {

    private readonly fullNameTextField: TextFieldComponent;
    private readonly nameAbbreviationTextField: TextFieldComponent;
    private readonly phoneNumberTextField: TextFieldComponent;
    private readonly emailTextField: TextFieldComponent;
    private readonly usernameTextField: TextFieldComponent;
    private readonly currentPasswordTextField: TextFieldComponent;
    private readonly passwordTextField: TextFieldComponent;
    private readonly passwordRepetitionTextField: TextFieldComponent;
    private readonly changePasswordButton: HTMLButtonElement;
    private readonly changePasswordContainer: HTMLDivElement;
    private readonly passwordStrengthBar: PasswordStrengthBar;
    private readonly personTypeNameInputField: HTMLInputElement;
    private readonly personForm: PersonForm = null;

    constructor(idPrefix: string) {
        this.fullNameTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'fullName_mdc_text_field'));
        this.nameAbbreviationTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'nameAbbreviation_mdc_text_field'));
        this.phoneNumberTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'phoneNumber_mdc_text_field'));
        this.emailTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'email_mdc_text_field'));
        this.usernameTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'username_mdc_text_field'));
        this.currentPasswordTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'currentPassword_mdc_text_field'));
        this.passwordTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'password_first_mdc_text_field'));
        this.passwordRepetitionTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'password_second_mdc_text_field'));
        this.changePasswordButton = document.querySelector('#' + idPrefix + 'change_password_button');
        this.changePasswordContainer = document.querySelector('#' + idPrefix + 'change_password_container');
        this.passwordStrengthBar = new PasswordStrengthBar(this.passwordTextField.mdcTextField);

        this.personTypeNameInputField = document.querySelector('#' + idPrefix + 'person_personTypeName');

        if (this.personTypeNameInputField !== null) {
            switch (this.personTypeNameInputField.value) {
                case 'NATURAL_PERSON':
                    this.personForm = new NaturalPersonForm(idPrefix + 'person_');
                    break;
                case 'COMPANY':
                    this.personForm = new CompanyForm(idPrefix + 'person_');
                    break;
                case 'COMMUNE':
                    this.personForm = new CommuneForm(idPrefix + 'person_');
                    break;
            }
        }

        this.fullNameTextField.mdcTextField.required = true;
        this.emailTextField.mdcTextField.required = true;
        this.usernameTextField.mdcTextField.required = true;
        this.currentPasswordTextField.mdcTextField.required = true;
        this.passwordTextField.mdcTextField.required = true;
        this.passwordRepetitionTextField.mdcTextField.required = true;

        this.changePasswordButton.addEventListener('click', (event: MouseEvent): void => {
            event.preventDefault();
            this.showChangePassword();
        });
    }

    public isFormValid(): boolean {
        let hasErrors: boolean = false;

        if (this.fullNameTextField.mdcTextField.valid === false) {
            this.fullNameTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (FormFieldValidator.validateEmail(this.emailTextField.mdcTextField.value) === false) {
            this.emailTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (this.usernameTextField.mdcTextField.valid === false) {
            this.usernameTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (this.personForm !== null && this.personForm.isFormValid() === false) {
            hasErrors = true;
        }

        return (hasErrors === false);
    }

    private showChangePassword(): void {
        this.changePasswordButton.classList.add('d-none');
        this.changePasswordContainer.classList.remove('d-none');
        this.setChangePasswordFieldsDisabled(false);
    }

    private hideChangePassword(): void {
        this.changePasswordButton.classList.remove('d-none');
        this.changePasswordContainer.classList.add('d-none');
        this.setChangePasswordFieldsDisabled(true);
    }

    private setChangePasswordFieldsDisabled(disabled: boolean): void {
        this.currentPasswordTextField.mdcTextField.disabled = disabled;
        this.passwordTextField.mdcTextField.disabled = disabled;
        this.passwordRepetitionTextField.mdcTextField.disabled = disabled;
    }

}

export { BaseDataForm };
