import { BaseDataForm } from '../Form/BaseDataForm';
import { MdcContextMenuComponent } from '../../Component/MdcContextMenuComponent';
import { MessageBoxComponent } from '../../Component/MessageBoxComponent/MessageBoxComponent';
import { MessageBoxAlertType } from '../../Component/MessageBoxComponent/MessageBoxAlertType';
import { MDCSwitch } from '@material/switch';

class BaseDataPage {

    private readonly baseDataForm: BaseDataForm;
    private readonly baseDataFormElement: HTMLFormElement;
    private readonly baseDataFormSubmitButton: HTMLButtonElement;
    private readonly barrierFreeMdcSwitch: MDCSwitch = null;
    private readonly matchingRelevantFieldsMdcSwitch: MDCSwitch = null;
    private readonly exposeRelevantFieldsMdcSwitch: MDCSwitch = null;
    private readonly followUpReminderWidgetStateMdcSwitch: MDCSwitch = null;
    private readonly propertyVacancyManagementWidgetStateMdcSwitch: MDCSwitch = null;
    private readonly propertyVacancyReportWidgetStateMdcSwitch: MDCSwitch = null;
    private readonly lookingForPropertyRequestReportWidgetStateMdcSwitch: MDCSwitch = null;
    private readonly lookingForPropertyRequestWidgetStateMdcSwitch: MDCSwitch = null;
    private readonly matchingWidgetStateMdcSwitch: MDCSwitch = null;
    private readonly pedestrianFrequencyWidgetStateMdcSwitch: MDCSwitch = null;
    private readonly cityGoalsWidgetStateMdcSwitch: MDCSwitch = null;
    private readonly locationIndicatorsWidgetStateMdcSwitch: MDCSwitch = null;
    private readonly hallOfInspirationWidgetStateMdcSwitch: MDCSwitch = null;
    private readonly contextMenu: MdcContextMenuComponent = null;
    private readonly messageBoxComponent: MessageBoxComponent;

    constructor() {
        const idPrefix: string = 'base_data_';

        this.baseDataForm = new BaseDataForm(idPrefix);
        this.baseDataFormElement = document.querySelector('#' + idPrefix + 'form');
        this.baseDataFormSubmitButton = document.querySelector('#' + idPrefix + 'save_submit_button');

        if (document.querySelector('#' + idPrefix + 'barrier_free_map_colors_mdc_switch') !== null) {
            this.barrierFreeMdcSwitch = new MDCSwitch(document.querySelector('#' + idPrefix + 'barrier_free_map_colors_mdc_switch'));
        }

        if (document.querySelector('#' + idPrefix + 'matching_relevant_fields_mdc_switch') !== null) {
            this.matchingRelevantFieldsMdcSwitch = new MDCSwitch(document.querySelector('#' + idPrefix + 'matching_relevant_fields_mdc_switch'));
        }

        if (document.querySelector('#' + idPrefix + 'expose_relevant_fields_mdc_switch') !== null) {
            this.exposeRelevantFieldsMdcSwitch = new MDCSwitch(document.querySelector('#' + idPrefix + 'expose_relevant_fields_mdc_switch'));
        }

        if (document.querySelector('#follow_up_reminder_widget_state_mdc_switch') !== null) {
            this.followUpReminderWidgetStateMdcSwitch = new MDCSwitch(document.querySelector('#follow_up_reminder_widget_state_mdc_switch'));
        }

        if (document.querySelector('#property_vacancy_management_widget_state_mdc_switch') !== null) {
            this.propertyVacancyManagementWidgetStateMdcSwitch = new MDCSwitch(document.querySelector('#property_vacancy_management_widget_state_mdc_switch'));
        }

        if (document.querySelector('#property_vacancy_report_widget_state_mdc_switch') !== null) {
            this.propertyVacancyReportWidgetStateMdcSwitch = new MDCSwitch(document.querySelector('#property_vacancy_report_widget_state_mdc_switch'));
        }

        if (document.querySelector('#looking_for_property_request_report_widget_state_mdc_switch') !== null) {
            this.lookingForPropertyRequestReportWidgetStateMdcSwitch = new MDCSwitch(document.querySelector('#looking_for_property_request_report_widget_state_mdc_switch'));
        }

        if (document.querySelector('#looking_for_property_request_widget_state_mdc_switch') !== null) {
            this.lookingForPropertyRequestWidgetStateMdcSwitch = new MDCSwitch(document.querySelector('#looking_for_property_request_widget_state_mdc_switch'));
        }

        if (document.querySelector('#matching_widget_state_mdc_switch') !== null) {
            this.matchingWidgetStateMdcSwitch = new MDCSwitch(document.querySelector('#matching_widget_state_mdc_switch'));
        }

        if (document.querySelector('#pedestrian_frequency_widget_state_mdc_switch') !== null) {
            this.pedestrianFrequencyWidgetStateMdcSwitch = new MDCSwitch(document.querySelector('#pedestrian_frequency_widget_state_mdc_switch'));
        }

        if (document.querySelector('#city_goals_widget_state_mdc_switch') !== null) {
            this.cityGoalsWidgetStateMdcSwitch = new MDCSwitch(document.querySelector('#city_goals_widget_state_mdc_switch'));
        }

        if (document.querySelector('#location_indicators_widget_state_mdc_switch') !== null) {
            this.locationIndicatorsWidgetStateMdcSwitch = new MDCSwitch(document.querySelector('#location_indicators_widget_state_mdc_switch'));
        }

        if (document.querySelector('#hall_of_inspiration_widget_state_mdc_switch') !== null) {
            this.hallOfInspirationWidgetStateMdcSwitch = new MDCSwitch(document.querySelector('#hall_of_inspiration_widget_state_mdc_switch'));
        }

        if (MdcContextMenuComponent.checkContainerExists(idPrefix) === true) {
            this.contextMenu = new MdcContextMenuComponent(idPrefix);
        }

        this.messageBoxComponent = new MessageBoxComponent();

        if (this.barrierFreeMdcSwitch !== null) {
            this.barrierFreeMdcSwitch.selected = localStorage.getItem('lean.map.barrierFree') === 'true' ?? false;
            this.barrierFreeMdcSwitch.listen('click', (): void => localStorage.setItem('lean.map.barrierFree', this.barrierFreeMdcSwitch.selected.toString()));
        }

        if (this.matchingRelevantFieldsMdcSwitch !== null) {
            this.matchingRelevantFieldsMdcSwitch.selected = localStorage.getItem('lean.property.matchingRelevantFields') === 'true' ?? false;
            this.matchingRelevantFieldsMdcSwitch.listen('click', (): void => localStorage.setItem('lean.property.matchingRelevantFields', this.matchingRelevantFieldsMdcSwitch.selected.toString()));
        }

        if (this.exposeRelevantFieldsMdcSwitch !== null) {
            this.exposeRelevantFieldsMdcSwitch.selected = localStorage.getItem('lean.property.exposeRelevantFields') === 'true' ?? false;
            this.exposeRelevantFieldsMdcSwitch.listen('click', (): void => localStorage.setItem('lean.property.exposeRelevantFields', this.exposeRelevantFieldsMdcSwitch.selected.toString()));
        }

        if (this.followUpReminderWidgetStateMdcSwitch !== null) {
            if (this.isWidgetDisabled('follow_up_reminder_widget') === false) {
                this.followUpReminderWidgetStateMdcSwitch.selected = true;
            }
            this.followUpReminderWidgetStateMdcSwitch.listen('click', () => this.storeWidgetState('follow_up_reminder_widget', this.followUpReminderWidgetStateMdcSwitch.selected));
        }

        if (this.propertyVacancyManagementWidgetStateMdcSwitch !== null) {
            if (this.isWidgetDisabled('property_vacancy_management_widget') === false) {
                this.propertyVacancyManagementWidgetStateMdcSwitch.selected = true;
            }
            this.propertyVacancyManagementWidgetStateMdcSwitch.listen('click', () => this.storeWidgetState('property_vacancy_management_widget', this.propertyVacancyManagementWidgetStateMdcSwitch.selected));
        }

        if (this.propertyVacancyReportWidgetStateMdcSwitch !== null) {
            if (this.isWidgetDisabled('property_vacancy_report_widget') === false) {
                this.propertyVacancyReportWidgetStateMdcSwitch.selected = true;
            }
            this.propertyVacancyReportWidgetStateMdcSwitch.listen('click', () => this.storeWidgetState('property_vacancy_report_widget', this.propertyVacancyReportWidgetStateMdcSwitch.selected));
        }

        if (this.lookingForPropertyRequestReportWidgetStateMdcSwitch !== null) {
            if (this.isWidgetDisabled('looking_for_property_request_report_widget') === false) {
                this.lookingForPropertyRequestReportWidgetStateMdcSwitch.selected = true;
            }
            this.lookingForPropertyRequestReportWidgetStateMdcSwitch.listen('click', () => this.storeWidgetState('looking_for_property_request_report_widget', this.lookingForPropertyRequestReportWidgetStateMdcSwitch.selected));
        }

        if (this.lookingForPropertyRequestWidgetStateMdcSwitch !== null) {
            if (this.isWidgetDisabled('looking_for_property_request_widget') === false) {
                this.lookingForPropertyRequestWidgetStateMdcSwitch.selected = true;
            }
            this.lookingForPropertyRequestWidgetStateMdcSwitch.listen('click', () => this.storeWidgetState('looking_for_property_request_widget', this.lookingForPropertyRequestWidgetStateMdcSwitch.selected));
        }

        if (this.matchingWidgetStateMdcSwitch !== null) {
            if (this.isWidgetDisabled('matching_widget') === false) {
                this.matchingWidgetStateMdcSwitch.selected = true;
            }
            this.matchingWidgetStateMdcSwitch.listen('click', () => this.storeWidgetState('matching_widget', this.matchingWidgetStateMdcSwitch.selected));
        }

        if (this.pedestrianFrequencyWidgetStateMdcSwitch !== null) {
            if (this.isWidgetDisabled('pedestrian_frequency_widget') === false) {
                this.pedestrianFrequencyWidgetStateMdcSwitch.selected = true;
            }
            this.pedestrianFrequencyWidgetStateMdcSwitch.listen('click', () => this.storeWidgetState('pedestrian_frequency_widget', this.pedestrianFrequencyWidgetStateMdcSwitch.selected));
        }

        if (this.cityGoalsWidgetStateMdcSwitch !== null) {
            if (this.isWidgetDisabled('city_goals_widget') === false) {
                this.cityGoalsWidgetStateMdcSwitch.selected = true;
            }
            this.cityGoalsWidgetStateMdcSwitch.listen('click', () => this.storeWidgetState('city_goals_widget', this.cityGoalsWidgetStateMdcSwitch.selected));
        }

        if (this.locationIndicatorsWidgetStateMdcSwitch !== null) {
            if (this.isWidgetDisabled('location_indicators_widget') === false) {
                this.locationIndicatorsWidgetStateMdcSwitch.selected = true;
            }
            this.locationIndicatorsWidgetStateMdcSwitch.listen('click', () => this.storeWidgetState('location_indicators_widget', this.locationIndicatorsWidgetStateMdcSwitch.selected));
        }

        if (this.hallOfInspirationWidgetStateMdcSwitch !== null) {
            if (this.isWidgetDisabled('hall_of_inspiration_widget') === false) {
                this.hallOfInspirationWidgetStateMdcSwitch.selected = true;
            }
            this.hallOfInspirationWidgetStateMdcSwitch.listen('click', () => this.storeWidgetState('hall_of_inspiration_widget', this.hallOfInspirationWidgetStateMdcSwitch.selected));
        }

        if (this.baseDataFormElement !== null) {
            this.baseDataFormSubmitButton.addEventListener('click', (): void => {
                if (this.baseDataForm.isFormValid() === false) {
                    this.messageBoxComponent.openMessageBox('Die Daten konnten nicht gespeichert werden. Bitte kontrollieren Sie die rot markierten Felder.', MessageBoxAlertType.DANGER);

                    return;
                }

                this.baseDataFormElement.submit();
            });
        }
    }

    public isWidgetDisabled(widget: string): boolean {
        if (localStorage.getItem('lean.dashboard.disabledWidgets') === null) {
            return false;
        }
        return !!localStorage.getItem('lean.dashboard.disabledWidgets').split(',').includes(widget);
    }

    public storeWidgetState(widget: string, widgetState: boolean): void {
        let disabledWidgets:string[] = [];

        if (localStorage.getItem('lean.dashboard.disabledWidgets') !== null) {
            disabledWidgets = localStorage.getItem('lean.dashboard.disabledWidgets').split(',');
        }

        if (widgetState === true && disabledWidgets.includes(widget)) {
            disabledWidgets.forEach((element,index)=>{
                if(element === widget) disabledWidgets.splice(index,1);
            });

            if (disabledWidgets.length === 0){
                localStorage.removeItem('lean.dashboard.disabledWidgets');
            } else {
                localStorage.setItem('lean.dashboard.disabledWidgets', disabledWidgets.join(','));
            }
        }

        if (widgetState === false && !disabledWidgets.includes(widget)) {
            disabledWidgets.push(widget);
            localStorage.setItem('lean.dashboard.disabledWidgets', disabledWidgets.join(','));
        }
    }

}

export { BaseDataPage };
