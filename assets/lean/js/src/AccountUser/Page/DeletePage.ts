import { BaseDataForm } from '../Form/BaseDataForm';
import { AccountUserDeleteForm } from '../Form/AccountUserDeleteForm';
import { MessageBoxComponent } from '../../Component/MessageBoxComponent/MessageBoxComponent';

class DeletePage {

    private readonly baseDataForm: BaseDataForm;
    private readonly accountUserDeleteForm: AccountUserDeleteForm;
    private readonly accountUserDeleteFormElement: HTMLFormElement;
    private readonly accountUserDeleteFormSubmitButton: HTMLButtonElement;
    private readonly messageBoxComponent: MessageBoxComponent;

    constructor() {
        const idPrefix: string = 'delete_';

        this.baseDataForm = new BaseDataForm('base_data_');
        this.accountUserDeleteForm = new AccountUserDeleteForm(idPrefix);
        this.accountUserDeleteFormElement = document.querySelector('#' + idPrefix + 'form');
        this.accountUserDeleteFormSubmitButton = document.querySelector('#' + idPrefix + 'delete_submit_button');
        this.messageBoxComponent = new MessageBoxComponent();

        this.accountUserDeleteFormSubmitButton.addEventListener('click', (): void => {
            this.accountUserDeleteFormElement.submit();
        });
    }

}

export { DeletePage };
