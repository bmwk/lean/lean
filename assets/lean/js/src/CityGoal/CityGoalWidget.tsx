import LoadingImage from '../../../images/lean-loading.gif';
import MdcContextMenuReactComponent from '../Component/MdcContextMenuReactComponent';
import { TargetActualTop10Chart } from './TargetActualTop10Chart';
import { Section, Survey } from './CityGoalApp';
import { FormControl, InputLabel, MenuItem, Select, Tab, Tabs } from '@mui/material';
import React, { useState } from 'react';

interface CityGoalWidgetProps {
    finishedSurveys: Array<Survey>;
    selectedSurveyId: string;
    loading: boolean
    sections: Array<Section>;
    handleTabButtonClick: Function;
    handleSelectChange: Function;
    questionDescription: string,
    answers: [];
    question: any
}

function CityGoalWidget({loading, handleTabButtonClick, sections, questionDescription, question, answers, finishedSurveys, selectedSurveyId, handleSelectChange}: CityGoalWidgetProps) {

    const [tabIndex, setTabIndex] = useState(0)

    return (
        <>
            <div className="card-header d-flex mb-4">
                <div className="icon-thumbnail me-4 ms-0 align-text-top">
                    <span>
                        <i className="material-icons">
                            offline_pin
                        </i>
                    </span>
                </div>
                <div className="card-title align-self-center">
                    <h2 className="mb-1 fs-4">Zielbild</h2>
                    <p className="mb-0">Überblick zum Zielbild unserer Kommune</p>
                </div>
                <div className="ms-auto align-text-top">
                    <MdcContextMenuReactComponent idPrefix={'city_goals_widget_'} listItems={[{
                        label: 'zu den Details',
                        icon: 'arrow_outward',
                        url: '/reports-und-berichte/zielbild'
                    }]}/>
                </div>
            </div>
            {finishedSurveys.length > 0 &&
            <FormControl className="col-11" variant="filled" sx={{ minWidth: 120, my: 1 }}>
                <InputLabel id="survey_select">Umfrage</InputLabel>
                <Select labelId="survey_select" value={selectedSurveyId} onChange={(event) => {
                    setTabIndex(0);
                    handleSelectChange(event);
                }} disabled={loading}>
                    {finishedSurveys?.map( (survey: Survey) => {
                        return (
                            <MenuItem key={survey.id} value={survey.id}>{survey.name}</MenuItem>
                        )
                    })}
                </Select>
            </FormControl>
            }
            {loading ? <div className='d-flex align-items-center justify-content-center h-100'>
                <img className="loading-image" src={LoadingImage} alt="Loading..."/>
            </div> :
                <div>
                    <Tabs
                    className="mb-4"
                    value={tabIndex}
                    onChange={(_, v) => setTabIndex(v)}
                >
                    {sections.map(section => (
                        <Tab sx={{fontSize: '14px'}} label={section.name} key={section.id}
                             onClick={() => handleTabButtonClick(section)}/>
                    ))}

                </Tabs>
                    <p>{questionDescription}</p>
                    {(answers && question) &&
                    <TargetActualTop10Chart question={question} answers={answers} displayOption={'target'}
                                            resultLength={5} height={250}/>}
                </div>}
        </>
    );
}

export default CityGoalWidget;
