import {Answer, AnswerType} from './Answer';
import {AnswerResponse} from './Admin/Answers';

export interface ChartBase {
    // something that changes if the input changes.
    // this is used to determine if the chart needs to be re-rendered in case the chart makes uses of useMemo
    // using the answers is not really possible because you have to drill deep to find something that changes
    // (stringifying the entire answers would be a massive overkill)
    inputId?: string
}

export function shortenLabel(label: string): string {
  const trimmedLabel = label.trim()
  // first 12 characters but wait for the next space to avoid cutting off the last word
  const shortened = trimmedLabel.replace(/^(.{12}[^\s]*).*/, '$1')
  if (shortened.length < trimmedLabel.length) {
    return `${shortened}...`
  } else {
    return shortened
  }
}


export function radarGroupByStakeholder<T extends Answer>(answers: AnswerResponse[], answerType: AnswerType): { [stakeholder: string]: T[] } {
  return answers.reduce((acc, elem) => {
    if (elem.answer.type === answerType) {
      const stakeholder = acc[elem.stakeholder.name]

      if (stakeholder === undefined) {
        return {
          ...acc,
          [elem.stakeholder.name]: [elem.answer as T],
        }
      } else {
        return {
          ...acc,
          [elem.stakeholder.name]: [
            ...stakeholder,
            elem.answer as T,
          ],
        }
      }
    }

    return acc
  }, {} as { [stakeholder: string]: T[] })
}
