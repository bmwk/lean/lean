import React, {FunctionComponent, useContext} from 'react';
import {
  ReferenceArea,
  ResponsiveContainer, ZAxis,
} from 'recharts';
import { Button, Stack} from '@mui/material';
import { ScatterChart } from 'recharts';
import { CartesianGrid } from 'recharts';
import { XAxis } from 'recharts';
import { YAxis } from 'recharts';
import { Scatter } from 'recharts';
import { Tooltip } from 'recharts';
import { Legend } from 'recharts';
import { ColorMapContext } from '../ColorGen';
import { ChartBase, shortenLabel } from './ChartUtils';
import _ from 'lodash'
import { TargetActualQuestion } from './Admin/QuestionsCatalog';
import {AnswerResponse} from './Admin/Answers';
import {TargetActualAnswer} from './Answer';

interface TargetActualDifferenceScatterChartProps {
  question: TargetActualQuestion
  answers: AnswerResponse[]
}

interface RowData {
    text: string
    actuals: number[]
    targets: number[]
}

const LowBounds = (props: any) => {
  const {x, y, width, height, stroke, fill, strokeOpacity, fillOpacity} = props
  return (
    <polygon
      stroke={stroke}
      fill={fill}
      strokeOpacity={strokeOpacity}
      fillOpacity={fillOpacity}
      points={`${x},${y} ${x+width},${y} ${x},${y+height} ${x},${y}`}
    />
  )
}

const MedHighBounds = (props: any) => {
  const {x, y, width, height, stroke, fill, strokeOpacity, fillOpacity} = props
  return (
    <polygon
      stroke={stroke}
      fill={fill}
      strokeOpacity={strokeOpacity}
      fillOpacity={fillOpacity}
      points={`${x},${y} ${x+width},${y} ${x-width},${y+height} ${x},${y}`}
    />
  )
}

export function aggregateScatterRowData(answers: AnswerResponse[], question: TargetActualQuestion): RowData[] {
  const answersByStakeholder = _.chain(answers)
      .filter((el): el is AnswerResponse<TargetActualAnswer> => el.answer.type === 'targetActualAnswer')
      .groupBy((el) => el.stakeholder.name)
      .mapValues((vals) => vals.map((i) => i.answer))
      .value()


  const rows: RowData[] = question.rows.map((row, idx) => {
    const avgByStakeholder = _.chain(answersByStakeholder)
        .mapValues((v) => ({
          actual: v.map((i)=> i.value.actual[idx]),
          target: v.map((i)=> i.value.target[idx]),
        }))
        .mapValues((v) => {
          const actuals = v.actual.filter((_) => _ != null)
          const targets = v.target.filter((_) => _ != null)
          const actualsAvg = actuals.length == 0 ? null : _.mean(actuals)
          const targetsAvg = targets.length == 0 ? null : _.mean(targets)
          return {
            actual: actualsAvg,
            target: targetsAvg,
          }
        })
        .value()

    const avgActual = _.values(avgByStakeholder).map((i) => i.actual).filter((v): v is number => v != null)
    const avgTarget = _.values(avgByStakeholder).map((i) => i.target).filter((v): v is number=> v != null)


    return {text: row, actuals: avgActual, targets: avgTarget}
  })

  return rows
}

export function scatterDatapointsFromAggregated(rows: RowData[]): readonly {diff: number, should: number, text: string}[] {
  return rows.map((row) => {
    const avgActual = _.mean(row.actuals)
    const avgTarget = _.mean(row.targets)

    const diff = Math.max(0, avgTarget - avgActual)

    return {diff: Math.round(diff), should: Math.round(avgTarget), text: row.text}
  })
}

export const TargetActualDifferenceScatterChart: FunctionComponent<TargetActualDifferenceScatterChartProps & ChartBase> = (
    {
      question,
      answers,
      inputId,
    },
) => {
  const colorMap = useContext(ColorMapContext)
  const getColor = React.useCallback(colorMap.get, [JSON.stringify(colorMap.map)])

  const [showAttribute, setShowAttribute] = React.useState<string[] | null>(null)

  const rowData: RowData[] = React.useMemo(() => aggregateScatterRowData(answers, question), [inputId, question])
  const datapoints = React.useMemo(() => scatterDatapointsFromAggregated(rowData), [inputId, question])

  const toggleAttribute = (attribute: string) => {
    let newAttributes: string[]
    if (_.includes(showAttribute, attribute)) {
      newAttributes = _.filter(showAttribute, (i) => i !== attribute)
    } else {
      newAttributes = _.uniq([
        ...(showAttribute|| []),
        attribute,
      ])
    }

    if (_.isEmpty(newAttributes) || _.isEqual(new Set(newAttributes), new Set(datapoints.map((_) => _.text)))) {
      setShowAttribute(null)
    } else {
      setShowAttribute(newAttributes)
    }
  }

  return (
    <Stack direction="column" spacing={1} alignContent="center">
      <ResponsiveContainer width="100%" height={450}>
        <ScatterChart width={900} height={450} margin={{bottom: 40}} >
          <CartesianGrid />
          <ZAxis type='category' dataKey='text' name='Bereich' />
          <XAxis type="number" dataKey="diff" name="Aufholbedarf"
            label={{ value: 'Aufholbedarf', angle: 0, position: 'bottom', fill: 'grey', offset: -5 }}
            ticks={[10, 20, 30, 40, 50, 60, 70, 80, 90, 100]}
            mirror={false}
            domain={[0, 100]} />
          <YAxis type="number" dataKey="should" name="Soll-Stellenwert"
            label={{ value: 'Soll-Stellenwert', angle: -90, offset: -20, position: 'left', fill: 'grey', top: -15 }}
            ticks={[0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100]}
            domain={[0, 100]} />
          <ReferenceArea x1={0} x2={25} y1={0} y2={100} fill="green" fillOpacity={0.1} stroke="green" strokeOpacity={0.2} shape={<LowBounds />} />
          <ReferenceArea x1={25} x2={50} y1={0} y2={100} fill="#DE970B" fillOpacity={0.1} stroke="#DE970B" strokeOpacity={0.2} shape={<MedHighBounds />} />
          <ReferenceArea x1={50} x2={100} y1={0} y2={100} fill="red" fillOpacity={0.1} stroke="red" strokeOpacity={0.2} shape={<MedHighBounds />} />
          {datapoints
              .map((data, idx) => <Scatter
                key={idx}
                animationDuration={500}
                name={data.text}
                data={[data]}
                hide={(_.isEmpty(showAttribute) || showAttribute == null) ? false : !_.includes(showAttribute, data.text)}
                stroke={getColor(data.text, 'attribute')}
                fill={getColor(data.text, 'attribute')}>
              </Scatter>,
              )}
          <Tooltip />
          <Legend
            formatter={(value: string) => shortenLabel(value)}
            onClick={(p) => toggleAttribute(p.value)}
            wrapperStyle={{cursor: 'pointer', bottom: 10}}
          />
        </ScatterChart>
      </ResponsiveContainer>
      {showAttribute &&
          <Button size='small' onClick={() => setShowAttribute(null)}>Filter zurücksetzen</Button>
      }
    </Stack>
  )
}
