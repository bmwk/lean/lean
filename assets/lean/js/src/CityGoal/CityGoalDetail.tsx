import React, { useState } from 'react';
import { TargetActualSummary } from './TargetActualSummary';
import LoadingImage from '../../../images/lean-loading.gif';
import { FormControl, InputLabel, MenuItem, Select, Tab, Tabs, ThemeProvider } from '@mui/material';
import { Section, Survey } from './CityGoalApp';

interface CityGoalDetailProps {
    finishedSurveys: Array<Survey>;
    selectedSurveyId: string;
    loading: boolean
    sections: Array<Section>;
    handleTabButtonClick: Function;
    handleSelectChange: Function;
    questionDescription: string,
    answers: [];
    question: any
}

function CityGoalDetail({finishedSurveys, selectedSurveyId, loading, sections, handleSelectChange, handleTabButtonClick, question, answers, questionDescription}: CityGoalDetailProps) {

    const [tabIndex, setTabIndex] = useState(0);

    return (
            <div className="container-lg my-4">
                    <div className="mdc-card">
                            {finishedSurveys.length > 0 &&
                            <FormControl variant="filled" sx={{ m: 2, minWidth: 120 }}>
                                <InputLabel id="survey_select">Umfrage</InputLabel>
                                <Select labelId="survey_select" value={selectedSurveyId} onChange={(event) => {
                                    setTabIndex(0);
                                    handleSelectChange(event);
                                }} disabled={loading}>
                                    {finishedSurveys?.map( (survey: Survey) => {
                                        return (
                                            <MenuItem key={survey.id} value={survey.id}>{survey.name}</MenuItem>
                                        )
                                    })}
                                </Select>
                            </FormControl>
                            }
                            {loading ? <div className="d-flex align-items-center justify-content-center h-100">
                                    <img src={LoadingImage} alt=""/>
                                </div>
                                :
                                <div>
                                    <Tabs
                                    className="mt-1 bg-white"
                                    value={tabIndex}
                                    onChange={(_, v) => setTabIndex(v)}
                                >
                                    {sections.map(section => (
                                        <Tab label={section.name} key={section.id} className="flex-grow-1"
                                             onClick={() => handleTabButtonClick(section)}/>
                                    ))}
                                    </Tabs>
                                    <div className="p-3">
                                        <p>{questionDescription}</p>
                                        {(answers && question) &&
                                        <TargetActualSummary question={question} answers={answers}/>}
                                    </div>
                                </div>}
                    </div>
            </div>
    );
}

export default CityGoalDetail;