import { Box, Tabs, Tab, Paper, Toolbar, Typography, Stack, Alert } from '@mui/material';
import React, { FunctionComponent, useState } from 'react';
import { TargetActualTop10Chart } from './TargetActualTop10Chart';
import { TargetActualDifferenceScatterChart } from './TargetActualDifferenceScatterChart';
import { TargetActualQuestionRadarChart } from './TargetActualQuestionRadarChart';
import { InfoOutlined } from '@mui/icons-material';
import { TargetActualQuestion } from './Admin/QuestionsCatalog';
import {AnswerResponse} from './Admin/Answers';
import {TabPanel} from './TabPanel';
import _ from 'lodash'

interface TargetActualSummaryProps {
  question: TargetActualQuestion;
  answers: AnswerResponse[];
  inputId?: string;
}

const SummaryTab: FunctionComponent<TargetActualSummaryProps & {displayOption: 'target' | 'actual'}> = ({question, answers, inputId, displayOption}) => {
  const [activeTab, setActiveTab] = useState<'radar' | 'ranking'>('ranking')
  const label = displayOption === 'actual' ? 'Ist' : 'Soll'

  return (
    <>
      <Toolbar component={Paper} sx={{mb: 2}}>
        <Stack direction="row" spacing={2}>
          <Typography
            variant="button"
            color={activeTab=== 'ranking' ? 'primary.600' : 'grey.500'}
            sx={{cursor: 'pointer'}}
            onClick={() => setActiveTab('ranking')}
          >
            Ranking {label}-Stellenwert
          </Typography>
          <Typography
            variant="button"
            color={activeTab=== 'radar' ? 'primary.600' : 'grey.500'}
            sx={{cursor: 'pointer'}}
            onClick={() => setActiveTab('radar')}
          >
            {label}-Stellenwert
          </Typography>
        </Stack>
      </Toolbar>
      {activeTab === 'radar' &&
        <TargetActualQuestionRadarChart question={question} answers={answers} displayOption={displayOption} inputId={inputId} />
      }
      {activeTab === 'ranking' && <>
        <TargetActualTop10Chart question={question} answers={answers} displayOption={displayOption}/>
      </>}
    </>
  )
}

const GapAnalysis: FunctionComponent<TargetActualSummaryProps> = ({answers, question, inputId}) => {
  const [displayOption, setDisplayOption] = useState<'gap' | 'targetActualComparison'>('targetActualComparison')

  return (
    <>
      <Toolbar component={Paper} sx={{mb: 2}}>
        <Stack direction="row" spacing={2}>
          <Typography
            variant="button"
            color={displayOption === 'targetActualComparison' ? 'primary.600' : 'grey.500'}
            sx={{cursor: 'pointer'}}
            onClick={() => setDisplayOption('targetActualComparison')}
          >
          Soll-Ist-Vergleich
          </Typography>
          <Typography
            variant="button"
            color={displayOption === 'gap' ? 'primary.600' : 'grey.500'}
            sx={{cursor: 'pointer'}}
            onClick={() => setDisplayOption('gap')}
          >
            Priorisierung Aufholbedarf
          </Typography>
        </Stack>
      </Toolbar>
      {displayOption === 'gap' &&
        <TargetActualDifferenceScatterChart question={question} answers={answers} inputId={inputId} />
      }
      {displayOption === 'targetActualComparison' && <>
        <Alert
          icon={<InfoOutlined sx={{color: 'grey.600'}} />}
          severity="info"
          sx={{bgcolor: 'grey.50', borderColor: 'grey.600', mb: 2}}>
          Der Gesamtdurchschnitt je Attribut wird aus dem Durchschnitt je Stakeholder-Gruppe ermittelt.
        </Alert>
        <TargetActualQuestionRadarChart question={question} answers={answers} displayOption='avg-columns' inputId={inputId} />
      </>}
    </>
  )
}

export const TargetActualSummary: FunctionComponent<TargetActualSummaryProps> = React.memo(({answers, question, inputId}) => {
  const [tabIndex, setTabIndex] = useState(0)
  return (
    <>
      <Box sx={{borderBottom: 1, borderColor: 'divider', mb: 1}}>
        <Tabs
          value={tabIndex}
          onChange={(_, v) => setTabIndex(v)}
        >
          <Tab sx={{fontSize: '14px'}} label="Vision" />
          <Tab sx={{fontSize: '14px'}} label="Status Quo" />
          <Tab sx={{fontSize: '14px'}} label="Gap-Analyse" />
        </Tabs>
      </Box>

      <TabPanel index={0} value={tabIndex} sx={{mt: -2, pt: 0}}>
        <SummaryTab question={question} answers={answers} inputId={inputId} displayOption="target" />
      </TabPanel>
      <TabPanel index={1} value={tabIndex} sx={{mt: -2, pt: 0}}>
        <SummaryTab question={question} answers={answers} inputId={inputId} displayOption="actual" />
      </TabPanel>
      <TabPanel index={2} value={tabIndex} sx={{mt: -1}} >
        <GapAnalysis question={question} answers={answers} inputId={inputId} />
      </TabPanel>
    </>
  )
}, (prev, curr) => _.isEqual(prev, curr))

TargetActualSummary.displayName = 'TargetActualSummary'
