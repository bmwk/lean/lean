import React, {FunctionComponent, useContext} from 'react';
import {
  Legend,
  PolarAngleAxis,
  PolarGrid,
  PolarRadiusAxis,
  Radar,
  RadarChart,
  ResponsiveContainer,
  Tooltip,
} from 'recharts';
import { Box, Button, List, ListItem, ListItemText, Stack, Typography} from '@mui/material';
import { ColorMapContext } from '../ColorGen';
import { ChartBase, radarGroupByStakeholder, shortenLabel } from './ChartUtils';
import { Customized } from 'recharts';
import _ from 'lodash';
import { Payload } from 'recharts/types/component/DefaultLegendContent';
import { TargetActualQuestion } from './Admin/QuestionsCatalog';
import {AnswerResponse} from './Admin/Answers';
import {TargetActualAnswer} from './Answer';

interface TargetActualQuestionRadarChartProps {
  question: TargetActualQuestion
  answers: AnswerResponse[]
  displayOption: DisplayOption
}

const columns: ('target' | 'actual')[] = ['target', 'actual']

type StakeholderBased = { [stakeholder: string]: TargetActualAnswer[] }

function color(length: number, index: number): string {
  return `hsl(${(360 / length) * index}, 38%, 68%)`
}

function stddev(vals: number[]): number {
  const avg = _.mean(vals)
  return Math.sqrt(_.mean(_.map(vals, (i) => Math.pow((i - avg), 2))));
}

const Reference = (props: any) => {
  const radiusPlacement = props.begin + (props.end - props.begin) / 2
  return (
    <circle
      cx={props.radiusAxisMap[0].cx} cy={props.radiusAxisMap[0].cy} r={props.radiusAxisMap[0].radius * radiusPlacement}
      stroke={props.stroke}
      strokeWidth={props.radiusAxisMap[0].radius * (props.end - props.begin)}
      fill='none'
      strokeOpacity={props.strokeOpacity}
    />
  )
}

interface RadarDatapoint {
  subject: string
  data: {
    [stakeholderOrTargetOrActual: string]: number
  }
}

export function generateRadarData(
    question: TargetActualQuestion,
    displayOption: DisplayOption,
    stakeholderBased: StakeholderBased,
): RadarDatapoint[] {
  let data: RadarDatapoint[]

  if (displayOption === 'avg-columns') {
    data = question.rows.map((row, rowIndex) => {
      const avgByStakeholder = _.chain(stakeholderBased)
          .mapValues((v) => ({
            actual: _.map(v, (i) => i.value.actual[rowIndex]),
            target: _.map(v, (i) => i.value.target[rowIndex]),
          }))
          .map((v) => {
            const actualValues = v.actual.filter((i): i is number => i != null)
            const targetValues = v.target.filter((i): i is number => i != null)

            return {
              actual: _.isEmpty(actualValues) ? null : _.mean(actualValues),
              target: _.isEmpty(targetValues) ? null : _.mean(targetValues),
            }
          })
          .value()

      const actuals = avgByStakeholder.map((v) => v.actual).filter((i): i is number => i != null)
      const targets = avgByStakeholder.map((v) => v.target).filter((i): i is number => i != null)

      const translatedColumnData: { [stakeholderOrTargetOrActual: string]: number } = {
        [question.targetColumnText]: _.chain(targets).mean().round(2).value(),
        [question.actualColumnText]: _.chain(actuals).mean().round(2).value(),
      }

      const result: RadarDatapoint = {
        subject: row,
        data: translatedColumnData,
      }

      return result
    })
  } else {
    data = question.rows.map((row, rowIndex) => {
      const stakeholderData: { [stakeholderName: string]: number } = {}

      const stakeholderFactor: { [stakeholderName: string]: number } = {}

      Object.entries(stakeholderBased).forEach((entry) => {
        const stakeholderName = entry[0]
        const answers = entry[1]

        answers.forEach((answer) => {
          const theAnswer = answer.value?.[displayOption]?.[rowIndex]
          if (theAnswer !== null && theAnswer !== undefined) {
            // only count this participant in the factor if they answered the question
            if (stakeholderFactor[stakeholderName]) {
              stakeholderFactor[stakeholderName] = stakeholderFactor[stakeholderName] + 1
            } else {
              stakeholderFactor[stakeholderName] = 1
            }
          }

          const value = theAnswer || 0

          if (stakeholderData[stakeholderName]) {
            stakeholderData[stakeholderName] = stakeholderData[stakeholderName] + value
          } else {
            stakeholderData[stakeholderName] = value
          }
        })
      })

      // Column with avg value
      Object.entries(stakeholderData).forEach((entry) => {
        stakeholderData[entry[0]] = _.round(entry[1] / stakeholderFactor[entry[0]], 2)
      })

      return {
        subject: row,
        data: stakeholderData,
      }
    })
  }
  return data
}

type DisplayOption = 'avg-columns' | 'target' | 'actual'

export const TargetActualQuestionRadarChart: FunctionComponent<TargetActualQuestionRadarChartProps & ChartBase> = (
    {
      question,
      answers,
      inputId,
      displayOption,
    },
) => {
  const colorMap = useContext(ColorMapContext)
  const getColor = React.useCallback(colorMap.get, [JSON.stringify(colorMap.map)])

  const [activeStakeholder, setActiveStakeholder] = React.useState<string[] | null>(null)

  const filteredAnswers = React.useMemo(() => {
    if (activeStakeholder) {
      return answers.filter((answer) => _.includes(activeStakeholder, answer.stakeholder.name))
    } else {
      return answers
    }
  }, [JSON.stringify(activeStakeholder), inputId, question])

  const allStakeholder = _.chain(answers)
      .map((i) => i.stakeholder.name)
      .uniq()
      .value()

  // group by stakeholder
  const stakeholderBased = React.useMemo(() =>
    radarGroupByStakeholder<TargetActualAnswer>(filteredAnswers, 'targetActualAnswer'),
  [inputId, JSON.stringify(filteredAnswers)],
  )

  const data: RadarDatapoint[] = React.useMemo(() =>
    generateRadarData(question, displayOption, stakeholderBased),
  [question, inputId, displayOption, JSON.stringify(stakeholderBased)],
  )

  const consensus = _.chain(data)
      .groupBy((i) => i.subject)
      .mapValues((i) => 100 - Math.round(stddev(i.flatMap((i) => _.values(i.data)))))
      .value()

  const toggleStakeholder = (stakeholder: string) => {
    let newStakeholder: string[]
    if (_.includes(activeStakeholder, stakeholder)) {
      newStakeholder = _.filter(activeStakeholder, (i) => i !== stakeholder)
    } else {
      newStakeholder = _.uniq([
        ...(activeStakeholder || []),
        stakeholder,
      ])
    }

    if (_.isEmpty(newStakeholder) || _.isEqual(new Set(newStakeholder), new Set(allStakeholder))) {
      setActiveStakeholder(null)
    } else {
      setActiveStakeholder(newStakeholder)
    }
  }

  return (
    <>
      <Stack direction="row" spacing={8}>
        <Stack direction="column" alignContent="center" flex="1 1 auto">
          <ResponsiveContainer height={600}>
            <RadarChart data={data}>
              <PolarGrid gridType='circle'/>
              <PolarAngleAxis dataKey={(t) => t.subject} tickFormatter={(value) => shortenLabel(value)}/>
              <PolarRadiusAxis angle={60} domain={[0, 100]} orientation="left" axisLine={true} />

              <Customized component={<Reference
                stroke="red" strokeOpacity={0.05}
                begin={0} end={0.5}
              />}/>
              <Customized component={<Reference
                stroke="#DE970B" strokeOpacity={0.05}
                begin={0.5} end={0.75}
              />}/>
              <Customized component={<Reference
                fillOpacity={0.1} stroke="green" strokeOpacity={0.05}
                begin={0.75} end={1.0}
              />}/>

              {displayOption === 'avg-columns' ?
                <>
                  <Radar
                    name={question.targetColumnText}
                    dataKey={(t) => t.data[question.targetColumnText]}
                    stroke={color(columns.length, 0)}
                    strokeWidth={2}
                    dot={true}
                    fillOpacity={0}
                    animationDuration={500}
                  />
                  <Radar
                    name={question.actualColumnText}
                    dataKey={(t) => t.data[question.actualColumnText]}
                    stroke={color(columns.length, 1)}
                    strokeWidth={2}
                    dot={true}
                    fillOpacity={0}
                    animationDuration={500}
                  />
                </> :
                Object.keys(stakeholderBased).map((stakeholderName, index) => (
                  <Radar
                    key={`radar-${index}`}
                    name={stakeholderName}
                    dataKey={(t) => t.data[stakeholderName]}
                    stroke={getColor(stakeholderName, 'stakeholder')}
                    strokeWidth={2}
                    dot={true}
                    fillOpacity={0}
                    animationDuration={500}
                  />
                ))
              }
              <Tooltip/>
              {displayOption === 'avg-columns' ?
              <Legend /> :
              <Legend
                payload={allStakeholder
                    .map((label) => {
                      const isActive = _.includes(activeStakeholder, label) || (_.isEmpty(activeStakeholder) || activeStakeholder == null)
                      const payload: Payload = {
                        value: label,
                        color: isActive ? getColor(label, 'stakeholder') : 'lightgrey',
                      }

                      return payload
                    })
                }
                onClick={(i) => toggleStakeholder(i.value)}
                onDoubleClick={() => setActiveStakeholder(null)}
                wrapperStyle={{cursor: 'pointer'}}
              />}
            </RadarChart>
          </ResponsiveContainer>
          {activeStakeholder &&
            <Button size='small' onClick={() => setActiveStakeholder(null)}>Filter zurücksetzen</Button>
          }
        </Stack>
        {displayOption != 'avg-columns' && <Box sx={{pt: 5}}>
          <Typography variant="h6">Konsens</Typography>
          <Box sx={{maxHeight: '500px', overflowY: 'auto', overflowX: 'clip'}}>
            <List sx={{width: '330px', flex: '1 0 auto'}} dense>
              {_.orderBy(Object.entries(consensus),
                  (i) => i[1], 'desc',
              ).map(([key, value]) => {
                return <ListItem
                  key={key}
                  disableGutters
                  disablePadding
                >
                  <ListItemText
                    primary={key}
                    secondary={`${value}%`}
                  />
                </ListItem>
              })}
            </List>
          </Box>
        </Box>}
      </Stack>
    </>
  )
}
