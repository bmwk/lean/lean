import { Answer } from '../Answer';
import { Stakeholder } from './Stakeholder';

export interface AnswerResponse<T extends Answer = Answer> {
  participantId: string;
  stakeholder: Stakeholder;
  answer: T;
}

export interface AnswerResponses {
  [questionId: string]: AnswerResponse[];
}
