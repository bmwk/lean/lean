export interface Stakeholder {
    name: string
    isExternal: boolean
}

export interface SurveyStakeholder {
    name: string
    isExternal: boolean
    participants: string[]
}

export interface StakeholderTemplate {
    id: string
    stakeholder: Stakeholder
}

export interface ProjectStakeholder {
    id: string
    email: string
}

export interface ProjectStakeholderGroup {
    id: string
    name: string
    isExternal: boolean
    members: ProjectStakeholder[]
}

export interface ProjectStakeholderGroups {
    groups: ProjectStakeholderGroup[]
}
