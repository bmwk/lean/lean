export interface Range {
  start: number
  endInclusive: number
}

export const QuestionTypes = [
  'intRangeQuestion',
  'percentQuestion',
  'enumQuestion',
  'rasterIntQuestion',
  'targetActualQuestion',
  'freeTextQuestion',
] as const

export type QuestionTypes = typeof QuestionTypes
export type QuestionType = QuestionTypes[number]

export const QuestionTypeTranslations: Record<QuestionType, { name: string, description: string }> = {
  intRangeQuestion: {
    name: 'Schulnoten-Frage',
    description: 'Antworten in einem Bereich von Zahlen in 1er - Schritten',
  },
  percentQuestion: {
    name: 'Prozent-Frage',
    description: 'Anworten in einem Bereich von 0% - 100% in 1% - Schritten',
  },
  enumQuestion: {
    name: 'Enum-Frage',
    description: 'Anworten aus einer vordefinierten Liste',
  },
  rasterIntQuestion: {
    name: 'Kästchenraster-Zahlen-Frage',
    description: `Antworten in einem Bereich von 0 - 100 in 1er Schritten.
    Spalten geben die Antwortmöglichkeiten an.
    Zeilen geben die Fragestellungen an.`,
  },
  targetActualQuestion: {
    name: 'Soll-Ist-Frage',
    description: `Anworten in einem Bereich von 0 - 100 in 1er Schritten.
    2 Spalten (mit optionaler Beschreibung) geben Soll und Ist vor.
    Zeilen (mit optionaler Beschreibung) geben die Fragestellungen an.`,
  },
  freeTextQuestion: {
    name: 'Freitext-Frage',
    description: `Antworten in einem Freitext-Feld.`,
  },
}


export interface BaseQuestion {
  type: QuestionType;
  id: string;
  text: string;
  description: string;
}

export interface IntRangeQuestion extends BaseQuestion {
  type: 'intRangeQuestion';
  range: Range;
}

export interface PercentQuestion extends BaseQuestion {
  type: 'percentQuestion';
}

export interface EnumQuestion extends BaseQuestion {
  type: 'enumQuestion';
  values: string[];
}

export interface RasterIntQuestion extends BaseQuestion {
  type: 'rasterIntQuestion';
  range: Range;
  rows: string[];
  columns: string[];
}

export interface TargetActualQuestion extends BaseQuestion {
  type: 'targetActualQuestion'
  targetColumnText: string
  targetColumnDescription: string
  actualColumnText: string
  actualColumnDescription: string
  rows: string[]
  rowDescriptions: string[]
}

export interface FreeTextQuestion extends BaseQuestion {
  type: 'freeTextQuestion'
}

export type Question = IntRangeQuestion
  | PercentQuestion
  | EnumQuestion
  | RasterIntQuestion
  | TargetActualQuestion
  | FreeTextQuestion

export interface QuestionSection {
  id: string
  name: string
  description: string | undefined
  questions: Question[]
}

export interface QuestionsCatalog {
  sections: QuestionSection[]
}

export const addQuestionToQuestionSection = (
    questionsCatalog: QuestionsCatalog,
    questionSectionId: string,
    question: Question,
) => {
  const newQuestionsCatalog: QuestionsCatalog = {
    ...questionsCatalog,
    sections: [
      ...questionsCatalog.sections.map((section) => {
        if (section.id === questionSectionId) {
          return {
            ...section,
            questions: [
              ...section.questions,
              question,
            ],
          }
        } else {
          return section
        }
      }),
    ],
  }

  return newQuestionsCatalog
}

export const replaceQuestionInQuestionSection = (
    questionsCatalog: QuestionsCatalog,
    questionSectionId: string,
    question: Question,
) => {
  const newQuestionsCatalog: QuestionsCatalog = {
    ...questionsCatalog,
    sections: [
      ...questionsCatalog.sections.map((section) => {
        if (section.id === questionSectionId) {
          return {
            ...section,
            questions: section.questions.map((q) => {
              if (q.id === question.id) {
                return question
              } else {
                return q
              }
            }),
          }
        } else {
          return section
        }
      }),
    ],
  }

  return newQuestionsCatalog
}

export function isQuestionsCatalogEmpty(questionsCatalog: QuestionsCatalog): boolean {
  // Returns "true" for empty sections array
  return questionsCatalog.sections.every((section) => section.questions.length === 0)
}
