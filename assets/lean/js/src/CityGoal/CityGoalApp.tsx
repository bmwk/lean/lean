import { BaseQuestion } from './Admin/QuestionsCatalog';
import CityGoalDetail from './CityGoalDetail';
import CityGoalWidget from './CityGoalWidget';
import { Alert, SelectChangeEvent } from '@mui/material';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import React, { useEffect, useState } from 'react';

export interface Survey {
    id: string;
    name: string;
}

export interface Section {
    id: string;
    name: string;
    description: string;
    questions: Array<BaseQuestion>;
}

let theme = createTheme({
    palette: {
        primary: {
            main: '#014A5D',
        },
        secondary: {
            main: '#1d81a1',
        },
    },
});

interface CityGoalAppProps {
    isWidget: boolean;
}

function CityGoalApp({isWidget}: CityGoalAppProps) {

    const [finishedSurveys, setFinishedSurveys] = useState<Survey[]>([]);
    const [selectedSurveyId, setSelectedSurveyId] = useState('');
    const [answers, setAnswers] = useState(null);
    const [question, setQuestion] = useState(null);
    const [loading, setLoading] = useState(true);
    const [sections, setSections] = useState([]);
    const [surveyAnswers, setSurveyAnswers] = useState(null);
    const [questionDescription, setQuestionDescription] = useState(null);

    const fetchData = async () => {
        const query = '/api-gateway/zielbildcheck/project/surveys/finished';
        let response = await fetch(query);
        const finishedSurveys = await response.json() as Survey[];
        setFinishedSurveys(finishedSurveys);
        if (finishedSurveys.length == 0) {
            setLoading(false);
            return;
        }
        setSelectedSurveyId(finishedSurveys[0].id);
    };

    const handleTabBarButtonClick = (section: Section) => {
        setQuestion(section.questions[0]);
        setQuestionDescription(section.description);
        setAnswers(surveyAnswers[section.questions[0].id]);
    };

    const handleSelectChange = (event: SelectChangeEvent) => {
        setSelectedSurveyId(event.target.value)
    }

    useEffect(() => {
        const updateSurvey = async () => {
            setLoading(true);
            const response = await fetch(`/api-gateway/zielbildcheck/project/surveys/${selectedSurveyId}/questions`);
            const surveyDetail = await response.json();
            setSections(surveyDetail.questionsCatalog.sections);
            setSurveyAnswers(surveyDetail.answers);
            const answers = surveyDetail.answers[(surveyDetail.questionsCatalog.sections[0].questions[0].id)];
            setAnswers(answers);
            setQuestion(surveyDetail.questionsCatalog.sections[0].questions[0]);
            setQuestionDescription(surveyDetail.questionsCatalog.sections[0].description);
            setLoading(false);
        };
        if (!selectedSurveyId) return;
        updateSurvey();
    }, [selectedSurveyId]);

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <ThemeProvider theme={theme}>
            {(loading == false && finishedSurveys.length == 0) ?
                <Alert severity="info">Sie verfügen derzeit über keine abgeschlossenen Umfragen</Alert>
                : isWidget ?
                    <CityGoalWidget finishedSurveys={finishedSurveys} selectedSurveyId={selectedSurveyId}
                                    loading={loading} sections={sections} handleTabButtonClick={handleTabBarButtonClick}
                                    handleSelectChange={handleSelectChange} questionDescription={questionDescription}
                                    answers={answers} question={question}/>
                    : <CityGoalDetail finishedSurveys={finishedSurveys} selectedSurveyId={selectedSurveyId}
                                      loading={loading} sections={sections}
                                      handleTabButtonClick={handleTabBarButtonClick}
                                      handleSelectChange={handleSelectChange} questionDescription={questionDescription}
                                      answers={answers} question={question}/>
            }
        </ThemeProvider>
    );
}

export default CityGoalApp;