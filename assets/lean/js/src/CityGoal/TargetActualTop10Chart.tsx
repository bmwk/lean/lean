import React, {FunctionComponent} from 'react';
import {
  Bar,
  BarChart,
  ResponsiveContainer,
} from 'recharts';
import { Paper, Stack } from '@mui/material';
import { CartesianGrid } from 'recharts';
import { XAxis } from 'recharts';
import { YAxis } from 'recharts';
import { Tooltip } from 'recharts';
import { ChartBase } from './ChartUtils';
import _ from 'lodash'
import { TooltipProps } from 'recharts/types/component/Tooltip';
import { NameType, ValueType } from 'recharts/types/component/DefaultTooltipContent';
import {TargetActualQuestion} from './Admin/QuestionsCatalog';
import {AnswerResponse} from './Admin/Answers';
import {TargetActualAnswer} from './Answer';

interface TargetActualTop10ChartProps {
  question: TargetActualQuestion
  answers: AnswerResponse[]
  displayOption: 'target' | 'actual',
    resultLength?: number,
    height?: number,
}

interface ConsensusData {
    text: string;
    avg: number;
    stakeholderValues: {
      [stakeholderName: string]: number | null
    }
}

const CustomTooltip = (props: TooltipProps<ValueType, NameType>) => {
  if (props.active) {
    return (
      <Paper sx={{px: 1, py: 0}}>
        <p className="label">{`${props.label} : ${props.payload?.[0].value}`}</p>
      </Paper>
    );
  }

  return null;
};

export function aggregateConsensusData(answers: AnswerResponse[], question: TargetActualQuestion, displayOption: 'target' | 'actual'): ConsensusData[] {
  const answersByStakeholder = _.chain(answers)
      .filter((el): el is AnswerResponse<TargetActualAnswer> => el.answer.type === 'targetActualAnswer')
      .groupBy((el) => el.stakeholder.name)
      .mapValues((vals) => vals.map((i) => i.answer))
      .value()


  const rows: ConsensusData[] = question.rows.map((row, idx) => {
    const avgByStakeholder = _.chain(answersByStakeholder)
        .mapValues((v) => v.map((i)=> i.value[displayOption][idx]))
        .mapValues((v) => {
          const targets = v.filter((_) => _ != null)
          const targetsAvg = targets.length == 0 ? null : _.mean(targets)
          return targetsAvg
        })
        .value()

    const targetValues = _.values(avgByStakeholder).filter((v): v is number=> v != null)
    const avgTarget = _.mean(targetValues)


    return {text: row, avg: avgTarget, stakeholderValues: avgByStakeholder}
  })

  return _.orderBy(rows, (r) => r.avg, 'desc')
}
export const TargetActualTop10Chart: FunctionComponent<TargetActualTop10ChartProps & ChartBase> = (
    {
      question,
      answers,
      displayOption,
        resultLength,
        height
    },
) => {
  const sortedAttributes = aggregateConsensusData(answers, question, displayOption)
  const data = sortedAttributes.map((val) => ({
    name: val.text,
    value: Math.round(val.avg),
  }))

  return (
    <Stack direction="column">
      <ResponsiveContainer height={height ? height : 450} width="100%">
        <BarChart width={730} height={height ? height : 450} data={resultLength ? data.slice(0, resultLength) : data} layout="vertical">
          <CartesianGrid strokeDasharray="3 3" />
          <YAxis dataKey="name"
            type='category'
            interval={0}
            width={250}
            tickFormatter={(val) => _.truncate(val, {length: 30})}
          />
          <XAxis dataKey="value" type='number' />
          <Tooltip content={CustomTooltip}/>
          <Bar
            dataKey="value"
            fill="#33819A"
            label={{position: 'middle', fill: '#fff'}}
          />
        </BarChart>
      </ResponsiveContainer>
    </Stack>
  )
}
