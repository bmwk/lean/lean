import { IndustryClassificationForm } from '../../../Classification/Form/IndustryClassificationForm';
import { SelectComponent } from '../../../Component/SelectComponent';
import { MultiselectComponent } from '../../../Component/MultiselectComponent';
import { CheckboxFieldComponent } from '../../../Component/CheckboxFieldComponent';

class PropertyVacancyDataFilterForm {

    private readonly industryClassificationForm: IndustryClassificationForm;
    private readonly quarterPlaceSelect: SelectComponent;
    private readonly locationCategoriesMultiselect: MultiselectComponent;
    private readonly vacancyReasonsMultiselect: MultiselectComponent;
    private readonly onlyGroundFloorCheckbox: CheckboxFieldComponent;

    constructor(idPrefix: string) {
        this.industryClassificationForm = new IndustryClassificationForm(
            idPrefix + 'industryClassification_',
            (industryClassificationLevelOne: number) => {},
            (industryClassificationLevelOne: number) => {}
        );

        this.industryClassificationForm.setRequired(false);

        this.quarterPlaceSelect = new SelectComponent(document.querySelector('#' + idPrefix + 'quarterPlace_mdc_select'), {clearButton: true});
        this.locationCategoriesMultiselect = new MultiselectComponent(document.querySelector('#' + idPrefix + 'locationCategories_multiselect'));
        this.vacancyReasonsMultiselect = new MultiselectComponent(document.querySelector('#' + idPrefix + 'vacancyReasons_multiselect'));
        this.onlyGroundFloorCheckbox = new CheckboxFieldComponent(document.querySelector('#' + idPrefix + 'onlyGroundFloor_mdc_form_field'));
    }

}

export { PropertyVacancyDataFilterForm };
