import LocationIndicatorsDetail from '../../../LocationIndicators/LocationIndicatorsDetail';
import ReactDOM from 'react-dom';
import React from 'react';

class LocationIndicatorsPage {

    constructor() {
        ReactDOM.render(
            <React.StrictMode>
                <LocationIndicatorsDetail></LocationIndicatorsDetail>
            </React.StrictMode>,
            document.querySelector('#location-indicators-app')
        );
    }

}

export { LocationIndicatorsPage };
