import { InsightsPage } from '../InsightsPage';
import { InsightsPageTab } from '../InsightsPageTab';
import { LazyImageLoader } from '../../../../LazyImageLoader';

class SurveyResultPage extends InsightsPage {

    private readonly lazyImageLoader: LazyImageLoader;

    constructor() {
        super(InsightsPageTab.SurveyResult);

        this.lazyImageLoader = new LazyImageLoader();
    }

}

export { SurveyResultPage };
