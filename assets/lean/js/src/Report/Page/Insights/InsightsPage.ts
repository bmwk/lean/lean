import { InsightsPageTab } from './InsightsPageTab';
import { MdcTabBarPage } from '../../../MdcTabBarPage';

abstract class InsightsPage extends MdcTabBarPage {

    protected constructor(reportPageTab: InsightsPageTab) {
        super(document.querySelector('#report_tab_bar'));

        this.activateTab(reportPageTab);
    }

}

export { InsightsPage };
