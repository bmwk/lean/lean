import { InsightsPage } from './InsightsPage';
import { InsightsPageTab } from './InsightsPageTab';

class SettlementInformationPage extends InsightsPage {

    constructor() {
        super(InsightsPageTab.SettlementInformation);
    }

}

export { SettlementInformationPage };
