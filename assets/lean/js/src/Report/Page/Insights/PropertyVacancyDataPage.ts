import { InsightsPage } from './InsightsPage';
import { InsightsPageTab } from './InsightsPageTab';
import { PropertyVacancyDataFilterForm } from '../../Form/Insights/PropertyVacancyDataFilterForm';

class PropertyVacancyDataPage extends InsightsPage {

    private readonly propertyVacancyDataFilterForm: PropertyVacancyDataFilterForm;
    private readonly propertyVacancyDataFilterFormElement: HTMLFormElement;
    private readonly propertyVacancyDataFilterFormSubmitButton: HTMLButtonElement;

    constructor() {
        super(InsightsPageTab.PropertyVacancyData);

        const idPrefix: string = 'property_vacancy_data_filter_';

        this.propertyVacancyDataFilterForm = new PropertyVacancyDataFilterForm(idPrefix);
        this.propertyVacancyDataFilterFormElement = document.querySelector('#' + idPrefix + 'form');
        this.propertyVacancyDataFilterFormSubmitButton = document.querySelector('#' + idPrefix + 'apply');

        this.propertyVacancyDataFilterFormSubmitButton.addEventListener('click', (): void => {
            this.propertyVacancyDataFilterFormElement.submit();
        });
    }

}

export { PropertyVacancyDataPage };
