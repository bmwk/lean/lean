import { InsightsPage } from './InsightsPage';
import { InsightsPageTab } from './InsightsPageTab';
import { LazyImageLoader } from '../../../LazyImageLoader';

class ImpulsesPage extends InsightsPage {

    private readonly lazyImageLoader: LazyImageLoader;

    constructor() {
        super(InsightsPageTab.Impulses);

        this.lazyImageLoader = new LazyImageLoader();
    }

}

export { ImpulsesPage };
