import { MDCTab } from '@material/tab';
import React from 'react';
import { createRoot, Root } from 'react-dom/client';
import { TabBarComponent } from '../../../Component/TabBarComponent';
import { LookingForPropertyRequestReportChart } from '../../../Insights/LookingForPropertyRequestReportChart';
import { PropertyStockChart } from '../../../Insights/PropertyStockChart';
import { PropertyVacancyReportsChart } from '../../../Insights/PropertyVacancyReportsChart';
import { InsightsPage } from './InsightsPage';
import { InsightsPageTab } from './InsightsPageTab';

class GeneralStatisticsPage extends InsightsPage {

    private readonly generalStatisticsTabBar: TabBarComponent;
    private readonly propertyStockTab: MDCTab;
    private readonly personTab: MDCTab;
    private readonly userTab: MDCTab;
    private readonly propertyVacancyReportTab: MDCTab;
    private propertyVacancyReportsRoot: Root;
    private readonly lookingForPropertyRequestReportTab: MDCTab;
    private lookingForPropertyRequestReportRoot: Root;
    private readonly propertyStockContainer: HTMLDivElement;
    private propertyStockRoot: Root;
    private readonly personContainer: HTMLDivElement;
    private readonly userContainer: HTMLDivElement;
    private readonly propertyVacancyReportContainer: HTMLDivElement;
    private readonly lookingForPropertyRequestReportContainer: HTMLDivElement;

    constructor() {
        super(InsightsPageTab.GeneralStatistics);
        if (document.querySelector('#general_statistics_tab_bar') != null) {
            this.generalStatisticsTabBar = new TabBarComponent(document.querySelector('#general_statistics_tab_bar'));
            this.propertyStockTab = new MDCTab(document.querySelector('#property_stock_tab'));
            this.personTab = new MDCTab(document.querySelector('#person_tab'));
            this.userTab = new MDCTab(document.querySelector('#user_tab'));
            this.propertyVacancyReportTab = new MDCTab(document.querySelector('#property_vacancy_report_tab'));
            this.lookingForPropertyRequestReportTab = new MDCTab(document.querySelector('#looking_for_property_request_report_tab'));
            this.propertyStockContainer = document.querySelector('#property_stock_container');
            this.personContainer = document.querySelector('#person_container');
            this.userContainer = document.querySelector('#user_container');
            this.propertyVacancyReportContainer = document.querySelector('#property_vacancy_report_container');
            this.lookingForPropertyRequestReportContainer = document.querySelector('#looking_for_property_request_report_container');

            this.propertyStockTab.root.addEventListener('click', (mouseEvent: MouseEvent): void => {
                mouseEvent.preventDefault();
                this.selectPropertyStockTab();
            });

            this.personTab.root.addEventListener('click', (mouseEvent: MouseEvent): void => {
                mouseEvent.preventDefault();
                this.selectPersonTab();
            });

            this.userTab.root.addEventListener('click', (mouseEvent: MouseEvent): void => {
                mouseEvent.preventDefault();
                this.selectUserTab();
            });

            this.propertyVacancyReportTab.root.addEventListener('click', (mouseEvent: MouseEvent): void => {
                mouseEvent.preventDefault();
                this.selectPropertyVacancyReportTab();
            });
            this.propertyVacancyReportsRoot = createRoot(this.propertyVacancyReportContainer);
            this.propertyVacancyReportsRoot.render(
                <React.StrictMode>
                    <PropertyVacancyReportsChart/>
                </React.StrictMode>
            )

            this.lookingForPropertyRequestReportTab.root.addEventListener('click', (mouseEvent: MouseEvent): void => {
                mouseEvent.preventDefault();
                this.selectLookingForPropertyRequestReportTab();
            });
            this.lookingForPropertyRequestReportRoot = createRoot(this.lookingForPropertyRequestReportContainer);
            this.lookingForPropertyRequestReportRoot.render(
                <React.StrictMode>
                    <LookingForPropertyRequestReportChart/>
                </React.StrictMode>
            )

            this.selectPropertyStockTab();

            this.propertyStockRoot = createRoot(this.propertyStockContainer);
            this.propertyStockRoot.render(
                <React.StrictMode>
                    <PropertyStockChart/>
                </React.StrictMode>
            );
        }
    }

    private selectPropertyStockTab(): void {
        this.generalStatisticsTabBar.activateTab('property_stock_tab');
        this.propertyStockContainer.classList.remove('d-none');
        this.personContainer.classList.add('d-none');
        this.userContainer.classList.add('d-none');
        this.propertyVacancyReportContainer.classList.add('d-none');
        this.lookingForPropertyRequestReportContainer.classList.add('d-none');
    }

    private selectPersonTab(): void {
        this.tabBar.activateTab('person_tab');
        this.personContainer.classList.remove('d-none');
        this.propertyStockContainer.classList.add('d-none');
        this.userContainer.classList.add('d-none');
        this.propertyVacancyReportContainer.classList.add('d-none');
        this.lookingForPropertyRequestReportContainer.classList.add('d-none');
    }

    private selectUserTab(): void {
        this.tabBar.activateTab('user_tab');
        this.userContainer.classList.remove('d-none');
        this.propertyStockContainer.classList.add('d-none');
        this.personContainer.classList.add('d-none');
        this.propertyVacancyReportContainer.classList.add('d-none');
        this.lookingForPropertyRequestReportContainer.classList.add('d-none');
    }

    private selectPropertyVacancyReportTab(): void {
        this.tabBar.activateTab('user_tab');
        this.propertyVacancyReportContainer.classList.remove('d-none');
        this.propertyStockContainer.classList.add('d-none');
        this.personContainer.classList.add('d-none');
        this.userContainer.classList.add('d-none');
        this.lookingForPropertyRequestReportContainer.classList.add('d-none');
    }

    private selectLookingForPropertyRequestReportTab(): void {
        this.tabBar.activateTab('user_tab');
        this.lookingForPropertyRequestReportContainer.classList.remove('d-none');
        this.propertyStockContainer.classList.add('d-none');
        this.personContainer.classList.add('d-none');
        this.userContainer.classList.add('d-none');
        this.propertyVacancyReportContainer.classList.add('d-none');
    }

}

export { GeneralStatisticsPage };
