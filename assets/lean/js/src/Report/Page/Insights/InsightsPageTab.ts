enum InsightsPageTab {
    PropertyVacancyData = 'report_insights_property_vacancy_data_tab',
    SettlementInformation = 'report_insights_settlement_information_tab',
    GeneralStatistics = 'report_insights_general_statistics_tab',
    SurveyResult = 'report_insights_survey_result_overview_tab',
    Impulses = 'report_insights_impulses_tab',
}

export { InsightsPageTab };
