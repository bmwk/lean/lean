import React from 'react';
import { createRoot, Root } from 'react-dom/client';
import PedestrianFrequencyDetail from '../../../PedestrianFrequency/PedestrianFrequencyDetail';

class PedestrianFrequencyPage {

    private readonly mapContainer: HTMLDivElement;
    private readonly pedestrianFrequencyRoot: Root;

    constructor() {

        this.mapContainer = document.querySelector('#hystreet-detail-app');
        this.pedestrianFrequencyRoot = createRoot(this.mapContainer!);

        this.pedestrianFrequencyRoot.render(
            <React.StrictMode>
                <PedestrianFrequencyDetail/>
            </React.StrictMode>
        );
    }

}

export { PedestrianFrequencyPage };
