import React from 'react';
import { createRoot, Root } from 'react-dom/client';
import CityGoalApp from '../../../CityGoal/CityGoalApp';

class CityGoalPage {

    private readonly cityGoalsContainer: HTMLDivElement;
    private readonly cityGoalsRoot: Root;

    constructor() {

        this.cityGoalsContainer = document.querySelector('#city-goals-app');
        this.cityGoalsRoot = createRoot(this.cityGoalsContainer!);

        this.cityGoalsRoot.render(
            <React.StrictMode>
                <CityGoalApp isWidget={false}/>
            </React.StrictMode>
        );
    }

}

export { CityGoalPage };
