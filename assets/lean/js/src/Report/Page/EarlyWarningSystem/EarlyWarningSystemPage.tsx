import React from 'react';
import { createRoot, Root } from 'react-dom/client';
import { MdcContextMenuComponent } from '../../../Component/MdcContextMenuComponent';
import EarlyWarningSystemMap from '../../../Map/EarlyWarningSystemMap';

class EarlyWarningSystemPage {

    private readonly contextMenu: MdcContextMenuComponent;
    private readonly mapContainer: HTMLDivElement;
    private readonly earlyWarningSystemMapRoot: Root;

    constructor() {
        this.contextMenu = new MdcContextMenuComponent('early_warning_system_');

        this.mapContainer = document.querySelector('#early_warning_system_map');
        this.earlyWarningSystemMapRoot = createRoot(this.mapContainer!);

        const centerPoint = {
            longitude: 0,
            latitude: 0
        };

        if (this.mapContainer.dataset.longitude !== undefined && this.mapContainer.dataset.latitude !== undefined) {
            centerPoint.longitude = parseFloat(this.mapContainer.dataset.longitude);
            centerPoint.latitude = parseFloat(this.mapContainer.dataset.latitude);
        }

        this.earlyWarningSystemMapRoot.render(
            <React.StrictMode>
                <EarlyWarningSystemMap
                    permissions={{canView: true, canEdit: true}}
                    centerPoint={centerPoint}
                    archived={false}
                />
            </React.StrictMode>
        );
    }

}

export { EarlyWarningSystemPage };
