import '../../../styles/multiselect.scss';
import { MDCTextField } from '@material/textfield';

class MultiselectComponent {

    private readonly element: HTMLDivElement;
    private readonly mdcTextField: MDCTextField;

    constructor(element: HTMLDivElement) {
        this.element = element;
        this.mdcTextField = new MDCTextField(element.querySelector('.mdc-text-field'));

        const choices: NodeListOf<HTMLDivElement> = this.element.querySelectorAll('.multiselect-select-results .choice-item');
        const selectedChoices: NodeListOf<HTMLDivElement> = this.element.querySelectorAll('.multiselect-select-results .selected-choice-item');

        this.initializeChoicesAndSelectedChoices(choices, selectedChoices);
        this.initializePlaceSearchbar(choices);
    }

    public markAsMatchingRelevant(): void {
        this.element.classList.add('matching-relevant');
    }

    private initializeChoicesAndSelectedChoices(choices: NodeListOf<HTMLDivElement>, selectedChoices: NodeListOf<HTMLDivElement>): void {
        choices.forEach((choice: HTMLDivElement): void => {
            const choiceInput: HTMLInputElement = choice.querySelector('input');

            if (choiceInput.checked === true) {
                choice.style.display = 'none';

                selectedChoices.forEach((item: HTMLDivElement): void => {
                    if (item.dataset.id === choiceInput.value) {
                        item.style.display = 'flex';
                    }
                });
            }

            choiceInput.addEventListener('click', (event: MouseEvent) => this.handlePlaceChoiceClick(event, choice));
        });

        selectedChoices.forEach((selectedChoice: HTMLDivElement): void => {
            selectedChoice.addEventListener('click', (event: MouseEvent) => this.handlePlaceSelectedChoiceClick(event, selectedChoice));
        });

        this.updateSelectedCount();
    }

    private initializePlaceSearchbar(choices: NodeListOf<Element>): void {
        const searchResultContainer: HTMLDivElement = this.element.querySelector('.multiselect-select-results');

        this.mdcTextField.listen('input', (): void => {
            this.filterChoices(choices, this.mdcTextField.value);
        });

        document.addEventListener('click', (event: MouseEvent): void => {
            const domElement: HTMLElement = event.target as HTMLElement;

            if (this.element !== domElement && this.element.contains(domElement) === false) {
                searchResultContainer.classList.add('d-none');
                this.mdcTextField.value = '';
                this.filterChoices(choices, this.mdcTextField.value);
            } else {
                searchResultContainer.classList.remove('d-none');
            }
        });
    }

    private filterChoices(choices: NodeListOf<Element>, query: string): void {
        choices.forEach((item: HTMLDivElement): void => {
            if (item.querySelector('input').checked === true) {
                item.style.display = 'none';
            } else {
                if (item.textContent.toLowerCase().indexOf(query.toLowerCase()) === -1) {
                    item.style.display = 'none';
                } else {
                    item.style.display = 'block';
                }
            }
        });
    }

    private updateSelectedCount(): void {
        const selectedCount: HTMLDivElement = this.element.querySelector('.selected-count');
        const selectedChoices: NodeListOf<HTMLDivElement> = this.element.querySelectorAll('.multiselect-select-results .selected-choice-item');
        let counter: number = 0;

        selectedChoices.forEach((item: HTMLDivElement): void => {
            if (item.style.display === 'flex') {
                counter++;
            }
        });

        if (counter === 0) {
            selectedCount.textContent = '';
        } else {
            selectedCount.textContent = '(Gewählt: ' + counter + ')';
        }
    }

    private handlePlaceChoiceClick(event: MouseEvent, item: HTMLDivElement): void {
        const choiceId: string = (event.currentTarget as HTMLInputElement).value;

        this.showSelectedChoiceById(choiceId);

        item.style.display = 'none';

        this.updateSelectedCount();
    }

    private showSelectedChoiceById(id: string): void {
        const selectedChoices: NodeListOf<HTMLDivElement> = this.element.querySelectorAll('.multiselect-select-results .selected-choice-item');

        selectedChoices.forEach((item: HTMLDivElement): void => {
            if (item.dataset.id === id) {
                item.style.display = 'flex';
            }
        });
    }

    private handlePlaceSelectedChoiceClick(event: MouseEvent, item: HTMLDivElement): void {
        const choiceId: string = (event.currentTarget as HTMLDivElement).dataset.id;

        this.showChoiceById(choiceId);

        item.style.display = 'none';

        this.updateSelectedCount();
    }

    private showChoiceById(id: string): void {
        const choices: NodeListOf<HTMLDivElement> = this.element.querySelectorAll('.multiselect-select-results .choice-item');

        choices.forEach((item: HTMLDivElement): void => {
            if (item.dataset.id === id) {
                item.querySelector('input').checked = false;
                item.style.display = 'block';
            }
        });
    }
}

export { MultiselectComponent };
