import { MDCSelect } from '@material/select';

interface Options {
    clearButton?: boolean
}

class SelectComponent {

    public readonly element: Element;
    public readonly mdcSelect: MDCSelect;

    constructor(element: Element, options?: Options) {
        this.element = element;
        this.mdcSelect = new MDCSelect(element);

        if (options !== null) {
            if (options?.clearButton === true) {
                this.clearButton();
            }
        }
    }

    private clearButton(): void {
        const clearButton: HTMLElement = document.createElement('i');

        clearButton.classList.add('material-icons');
        clearButton.classList.add('mdc-text-field__icon');
        clearButton.classList.add('mdc-text-field__icon--leading');
        clearButton.classList.add('clear-icon');
        clearButton.classList.add('small');
        clearButton.tabIndex = 0;
        clearButton.innerText = 'clear';
        clearButton.id = 'clearButton_' + this.mdcSelect.root.id;

        clearButton.addEventListener('click', (mouseEvent: MouseEvent): void => {
            mouseEvent.stopPropagation();

            this.mdcSelect.value = null;
        });

        this.mdcSelect.root.querySelector('.mdc-select__anchor').insertBefore(clearButton, this.mdcSelect.root.querySelector('.mdc-select__dropdown-icon'));
    }

    public markAsMatchingRelevant(): void {
        this.element.classList.add('matching-relevant');
    }

    public markAsExposeRelevant(): void {
        this.element.classList.add('expose-relevant');
    }

}

export { SelectComponent };
