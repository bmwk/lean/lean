import { PikadayOptionsBuilder } from '../PikadayOptionsBuilder';
import { MDCTextField } from '@material/textfield';
import Pikaday from 'pikaday';

class DatePickerTextFieldComponent {

    private readonly pikaday: Pikaday;

    constructor(mdcTextField: MDCTextField) {
        this.pikaday = new Pikaday(PikadayOptionsBuilder.build(mdcTextField.root.querySelector('input')));

        const dateIcon: HTMLElement = document.createElement('i');

        dateIcon.classList.add('material-icons');
        dateIcon.classList.add('mdc-text-field__icon');
        dateIcon.classList.add('mdc-text-field__icon--leading');
        dateIcon.tabIndex = 0;
        dateIcon.innerText = 'event';

        mdcTextField.root.appendChild(dateIcon);
    }

    public setMinDate(date: Date): void {
        this.pikaday.setMinDate(date);
    }

    public setMaxDate(date: Date): void {
        this.pikaday.setMaxDate(date);
    }

}

export { DatePickerTextFieldComponent };
