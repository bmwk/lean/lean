import { DatePickerTextFieldComponent } from './DatePickerTextFieldComponent';
import { MDCTextField } from '@material/textfield';

interface Options {
    datePicker?: boolean;
    autoFitTextarea?: boolean;
}

class TextFieldComponent {

    public readonly element: Element;
    public readonly mdcTextField: MDCTextField;

    private readonly datePickerTextFieldComponent: DatePickerTextFieldComponent;

    constructor(element: Element, options?: Options) {
        this.element = element;
        this.mdcTextField = new MDCTextField(element);

        if (options !== null) {
            if (options?.datePicker === true) {
                this.datePickerTextFieldComponent = new DatePickerTextFieldComponent(this.mdcTextField);
            }

            if (options?.autoFitTextarea === true) {
                this.autoFitTextarea();
            }
        }
    }

    public setMinDate(date: Date): void {
        if (this.datePickerTextFieldComponent !== null) {
            this.datePickerTextFieldComponent.setMinDate(date);
        }
    }

    public setMaxDate(date: Date): void {
        if (this.datePickerTextFieldComponent !== null) {
            this.datePickerTextFieldComponent.setMaxDate(date);
        }
    }

    public markAsMatchingRelevant(): void {
        this.element.classList.add('matching-relevant');
    }

    public markAsExposeRelevant(): void {
        this.element.classList.add('expose-relevant');
    }

    private autoFitTextarea(): void {
        const textAreaElement: HTMLTextAreaElement = this.mdcTextField.root.querySelector('textarea');

        if (textAreaElement === null) {
            return;
        }

        textAreaElement.setAttribute('style', 'height: ' + (textAreaElement.scrollHeight) + 'px; overflow-y: hidden;');

        textAreaElement.addEventListener(
            'input',
            (): void => {
                textAreaElement.style.height = textAreaElement.rows * 24 + 'px';
                textAreaElement.style.height = (textAreaElement.scrollHeight) + 'px';
            },
            false
        );
    }

}

export { TextFieldComponent };
