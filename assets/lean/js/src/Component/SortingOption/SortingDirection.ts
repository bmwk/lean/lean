enum SortingDirection {
    Ascending = 0,
    Descending = 1
}

export { SortingDirection };
