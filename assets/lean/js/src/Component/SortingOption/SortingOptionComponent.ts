import { SortingDirection } from './SortingDirection';

class SortingOptionComponent {

    public fetchSortingParametersFromQueryString() {
        const urlSearchParams: URLSearchParams = new URLSearchParams(window.location.search);
        const columnId: string = urlSearchParams.get('sortieren');
        const sortingDirection: SortingDirection = Number(urlSearchParams.get('richtung'));

        return {
            columnId,
            sortingDirection
        };
    }

    public handleSorting(columnId: string, sortingDirection: SortingDirection) {
        const urlSearchParams: URLSearchParams = new URLSearchParams(window.location.search);
        urlSearchParams.set('sortieren', columnId);
        urlSearchParams.set('richtung', sortingDirection.toString());

        window.location.href = window.location.pathname + '?' + urlSearchParams.toString();
    }
}

export { SortingOptionComponent };
