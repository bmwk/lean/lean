import { SortingOptionComponent } from './SortingOption/SortingOptionComponent';
import { SelectComponent } from './SelectComponent';

class SortingSelectComponent {

    private static readonly elementId: string = 'sorting_mdc_select';

    private readonly sortingOptionComponent: SortingOptionComponent;
    private readonly sortingSelect: SelectComponent;

    constructor(idPrefix: string) {
        this.sortingOptionComponent = new SortingOptionComponent();
        this.sortingSelect = new SelectComponent(document.querySelector('#' + idPrefix + SortingSelectComponent.elementId));

        const {columnId, sortingDirection} = this.sortingOptionComponent.fetchSortingParametersFromQueryString();

        if (columnId !== null && sortingDirection !== null) {
            this.sortingSelect.mdcSelect.value = columnId + '_' + sortingDirection;
        }

        this.sortingSelect.mdcSelect.listen('MDCSelect:change', (): void => {
            const [sortingColumnId, sortingDirection] = this.sortingSelect.mdcSelect.value.split('_');
            this.sortingOptionComponent.handleSorting(sortingColumnId, Number(sortingDirection));
        });
    }

    public static checkSelectBoxExists(idPrefix: string): boolean {
        return document.querySelector('#' + idPrefix + SortingSelectComponent.elementId) !== null;
    }

}

export { SortingSelectComponent };
