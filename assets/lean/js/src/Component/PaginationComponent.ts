import { MDCSelect } from '@material/select';

class PaginationComponent {

    private readonly paginationContainer: HTMLDivElement;
    private readonly rowsPerPageSelect: MDCSelect;
    private readonly firstPageButton: HTMLButtonElement;
    private readonly lastPageButton: HTMLButtonElement;
    private readonly previousPageButton: HTMLButtonElement;
    private readonly nextPageButton: HTMLButtonElement;
    private readonly totalCountInputElement: HTMLInputElement;
    private actualPage: number;
    private actualLimit: number;

    constructor(idPrefix: string) {
        this.paginationContainer = document.querySelector('#' + idPrefix + 'pagination');
        this.rowsPerPageSelect = new MDCSelect(this.paginationContainer.querySelector('.mdc-select'));
        this.firstPageButton = this.paginationContainer.querySelector('[data-first-page="true"]');
        this.lastPageButton = this.paginationContainer.querySelector('[data-last-page="true"]');
        this.previousPageButton = this.paginationContainer.querySelector('[data-prev-page="true"]');
        this.nextPageButton = this.paginationContainer.querySelector('[data-next-page="true"]');
        this.totalCountInputElement = this.paginationContainer.querySelector('[name="totalCount"]');

        this.initPageAndLimit();
        this.initRowsPerPageSelect();
        this.initPaginationButtons();

        this.paginationContainer.querySelector('.actual-showed-page').textContent = this.actualPage.toString();
    }

    public static checkContainerExists(idPrefix: string): boolean {
        return document.querySelector('#' + idPrefix + 'pagination') !== null;
    }

    private initPageAndLimit(): void {
        const urlParams: URLSearchParams = new URLSearchParams(window.location.search);
        this.actualPage = parseInt(urlParams.get('seite'));
        this.actualLimit = parseInt(urlParams.get('anzeigen'));

        if (isNaN(this.actualPage) === true) {
            this.actualPage = 1;
        }

        if (isNaN(this.actualLimit) === true) {
            this.actualLimit = parseInt(this.rowsPerPageSelect.value);
        }
    }

    private initRowsPerPageSelect(): void {
        this.rowsPerPageSelect.setValue(this.actualLimit.toString());
        this.rowsPerPageSelect.listen('MDCSelect:change', (): void => {
            PaginationComponent.reloadPage('1', this.rowsPerPageSelect.value);
        });
    }

    private initPaginationButtons(): void {
        const totalCount: number = parseInt(this.totalCountInputElement.value);
        const totalPages: number = Math.ceil(totalCount/this.actualLimit);

        this.firstPageButton.disabled = false;
        this.lastPageButton.disabled = false;
        this.previousPageButton.disabled = false;
        this.nextPageButton.disabled = false;

        if (this.actualPage === 1) {
            this.firstPageButton.disabled = true;
            this.previousPageButton.disabled = true;
        }

        if (this.actualPage === totalPages || totalPages === 0) {
            this.nextPageButton.disabled = true;
            this.lastPageButton.disabled = true;
        }

        this.firstPageButton.addEventListener('click', (): void => {
            PaginationComponent.reloadPage('1', this.actualLimit.toString());
        });

        this.lastPageButton.addEventListener('click', (): void => {
            PaginationComponent.reloadPage(totalPages.toString(), this.actualLimit.toString());
        });

        this.previousPageButton.addEventListener('click', (): void => {
            const prevPage: number = this.actualPage - 1;
            PaginationComponent.reloadPage(prevPage.toString(), this.actualLimit.toString());
        });

        this.nextPageButton.addEventListener('click', (): void => {
            const nextPage: number = this.actualPage + 1;
            PaginationComponent.reloadPage(nextPage.toString(), this.actualLimit.toString());
        });
    }

    private static reloadPage(page : string, limit: string): void {
        const urlParameters: URLSearchParams = new URLSearchParams(window.location.search);
        urlParameters.set('seite', page);
        urlParameters.set('anzeigen', limit);

        window.location.href = window.location.pathname + '?' + urlParameters.toString();
    }

}

export { PaginationComponent };
