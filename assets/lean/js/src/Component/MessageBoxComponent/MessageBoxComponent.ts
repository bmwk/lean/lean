import { MessageBoxAlertType } from './MessageBoxAlertType';

class MessageBoxComponent {

    private readonly messageBox: HTMLDivElement;

    constructor() {
        this.messageBox = document.querySelector('#message-box');
    }

    public openMessageBox(message: string, messageBoxAlertType: MessageBoxAlertType): void {
        this.messageBox.classList.add(messageBoxAlertType);
        this.messageBox.classList.remove('d-none');
        this.messageBox.innerHTML = message;
    }

    public closeMessageBox(): void {
        this.messageBox.classList.add('d-none');
        this.messageBox.innerHTML = '';
    }

}

export { MessageBoxComponent };
