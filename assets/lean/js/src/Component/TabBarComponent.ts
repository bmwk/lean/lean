import { MDCTabBar } from '@material/tab-bar';
import { MDCTab } from '@material/tab';

class TabBarComponent {

    private readonly mdcTabBar: MDCTabBar;
    private readonly mdcTabs: MDCTab[] = [];

    constructor(mdcTabBarContainer: HTMLDivElement) {
        this.mdcTabBar = new MDCTabBar(mdcTabBarContainer);

        this.mdcTabBar.root.querySelectorAll('button.mdc-tab').forEach( (htmlButtonElement: HTMLButtonElement): void => {
            this.addTab(new MDCTab(htmlButtonElement));
        });
    }

    public activateTab(tabId: string): void {
        const mdcTab: MDCTab = this.findTabById(tabId);

        this.mdcTabBar.activateTab(this.mdcTabs.indexOf(mdcTab));
    }

    public getTabs(): MDCTab[] {
        return this.mdcTabs;
    }

    public findTabById(tabId: string): MDCTab {
        return  this.mdcTabs.find((mdcTab: MDCTab): MDCTab => {
            if (mdcTab.root.id === tabId) {
                return mdcTab;
            }
        });
    }

    private addTab(mdcTab: MDCTab): void {
        this.mdcTabs.push(mdcTab);
    }

}

export { TabBarComponent };
