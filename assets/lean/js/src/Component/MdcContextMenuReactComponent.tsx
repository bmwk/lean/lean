import { MdcContextMenuComponent } from './MdcContextMenuComponent';
import React, { useEffect } from 'react';

interface MdcContextMenuProps {
    idPrefix: string;
    listItems: {
        url: string,
        icon: string,
        label: string
    }[]
}

const MdcContextMenuReactComponent = (props: MdcContextMenuProps) => {

    useEffect(() => {
        new MdcContextMenuComponent(props.idPrefix);
    }, []);

    return (
        <div className="mdc-menu-surface--anchor">
            <button id={props.idPrefix + 'menu_button'} className="material-icons mdc-icon-button mdc-card__action mdc-card__action--icon text-primary p-0">
                <div className="mdc-icon-button__ripple"></div>
                more_vert
            </button>
            <div id={props.idPrefix + 'mdc_menu'} className="mdc-menu mdc-menu-surface mdc-menu-surface--fixed">
                <ul className="mdc-list" role="menu" aria-hidden="true" aria-orientation="vertical" tabIndex={-1}>
                    {props.listItems.map((listItem) => <li key={listItem.label} className="mdc-list-item py-2" role="menuitem" data-url={listItem.url}>
                        <span className="mdc-list-item__ripple"></span>
                        <span className="mdc-list-item__text text-nowrap d-flex align-items-center">
                            <i className="material-icons mdc-list-item__graphic me-2" aria-hidden="true">{listItem.icon}</i>
                            {listItem.label}
                        </span>
                    </li>)}
                </ul>
            </div>
        </div>
    );
};

export default MdcContextMenuReactComponent;
