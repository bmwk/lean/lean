import { MDCMenu } from '@material/menu';

class MdcContextMenuComponent {

    private readonly menuButton: HTMLButtonElement;
    private readonly mdcMenu: MDCMenu;

    constructor(idPrefix: string) {
        this.mdcMenu = new MDCMenu(document.querySelector('#' + idPrefix + 'mdc_menu'));
        this.menuButton = document.querySelector('#' + idPrefix + 'menu_button');

        this.menuButton.addEventListener('click', (event): void => {
            event.preventDefault();
            this.mdcMenu.open = true;
        });

        this.mdcMenu.items.forEach((element: HTMLLIElement): void => {
            if (element.dataset.url) {
                element.addEventListener('click', (): void => {
                    if (element.dataset.target !== null && element.dataset.target === '_blank') {
                        window.open(element.dataset.url, element.dataset.target);
                    } else {
                        window.location.href = element.dataset.url;
                    }
                });
            }
        });
    }

    public static checkContainerExists(idPrefix: string): boolean {
        return document.querySelector('#' + idPrefix + 'mdc_menu') !== null;
    }

}

export { MdcContextMenuComponent };
