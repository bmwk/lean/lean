import { TextFieldComponent } from './TextFieldComponent';
import { MDCTextFieldIcon } from '@material/textfield';

class SearchFieldComponent {

    private readonly searchTextField: TextFieldComponent;
    private readonly clearSearchMdcTextFieldIcon: MDCTextFieldIcon;
    private readonly formElement: HTMLFormElement;

    constructor(idPrefix: string, textFieldName: string) {
        this.searchTextField = new TextFieldComponent(document.getElementById(idPrefix + textFieldName + '_mdc_text_field'));

        this.clearSearchMdcTextFieldIcon = new MDCTextFieldIcon(document.getElementById(idPrefix + textFieldName + '_mdc_text_field_clear_button'));

        this.formElement = this.searchTextField.element.closest('form');

        this.clearSearchMdcTextFieldIcon.listen('click', (mouseEvent: MouseEvent): void => {
            if (this.searchTextField.mdcTextField.value === '') {
                return;
            }

            this.searchTextField.mdcTextField.value = '';

            this.formElement.submit();
        });
    }

}

export { SearchFieldComponent };
