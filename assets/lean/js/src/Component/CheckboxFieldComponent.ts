import { MDCFormField } from '@material/form-field';
import { MDCCheckbox } from '@material/checkbox';

class CheckboxFieldComponent {

    public readonly element: Element;
    public readonly mdcFormField: MDCFormField;
    public readonly mdcCheckbox: MDCCheckbox;

    constructor(element: Element) {
        this.element = element;
        this.mdcFormField = new MDCFormField(element);
        this.mdcCheckbox = new MDCCheckbox(element.querySelector('.mdc-checkbox'));

        this.mdcFormField.input = this.mdcCheckbox;
    }

    public markAsMatchingRelevant(): void {
        this.element.classList.add('matching-relevant');
    }

    public markAsExposeRelevant(): void {
        this.element.classList.add('expose-relevant');
    }

    public isChecked(): boolean{
        return this.mdcCheckbox.checked;
    }

    public setChecked(checked: boolean): boolean{
        return this.mdcCheckbox.checked = checked;
    }

}

export { CheckboxFieldComponent };
