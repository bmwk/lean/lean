import { PaginationComponent } from './PaginationComponent';
import { SortingOptionComponent } from './SortingOption/SortingOptionComponent';
import { SortingDirection } from './SortingOption/SortingDirection';
import { MDCDataTable } from '@material/data-table';
import { MDCSelect } from '@material/select';

abstract class ListComponent {

    protected readonly mdcDataTableContainer: HTMLDivElement;
    protected readonly mdcDataTable: MDCDataTable;
    protected readonly paginationComponent: PaginationComponent = null;
    protected readonly rowSelection: RowSelection = null;
    protected readonly sortingOptionComponent: SortingOptionComponent = null;

    protected constructor(idPrefix: string) {
        this.mdcDataTableContainer = document.querySelector('#' + idPrefix + 'list');
        this.mdcDataTable = new MDCDataTable(this.mdcDataTableContainer);

        if (this.mdcDataTableContainer.dataset.pagination !== null && this.mdcDataTableContainer.dataset.pagination === '1') {
            this.paginationComponent = new PaginationComponent(idPrefix);
        }

        if (this.mdcDataTableContainer.dataset.massOperation !== null && this.mdcDataTableContainer.dataset.massOperation === '1') {
            this.rowSelection = new RowSelection(idPrefix, this.mdcDataTable);
        }

        this.sortingOptionComponent = new SortingOptionComponent();

        this.setSortingValuesToHeaderCell();

        this.mdcDataTable.listen('MDCDataTable:sorted', (event: CustomEvent): void => {
            const sortingDirection: SortingDirection = (event.detail.sortValue === 'ascending' ? SortingDirection.Ascending : SortingDirection.Descending);

            this.sortingOptionComponent.handleSorting(event.detail.columnId, sortingDirection);
        });
    }

    public setSortingValuesToHeaderCell(): void {
        const tableHeaderCellElements: NodeListOf<HTMLTableHeaderCellElement> = document.querySelectorAll('.mdc-data-table__header-cell--with-sort');

        const {columnId, sortingDirection} = this.sortingOptionComponent.fetchSortingParametersFromQueryString();

        tableHeaderCellElements.forEach((tableHeaderCellElement: HTMLTableHeaderCellElement): void  => {
            if (tableHeaderCellElement.getAttribute('data-column-id') === columnId) {
                tableHeaderCellElement.setAttribute('aria-sort', (sortingDirection === SortingDirection.Ascending ? 'ascending' : 'descending'));
                tableHeaderCellElement.className = `mdc-data-table__header-cell mdc-data-table__header-cell--with-sort mdc-data-table__header-cell--sorted ${sortingDirection === SortingDirection.Descending ? 'mdc-data-table__header-cell--sorted-descending' : ''}`;
            }
        });
    }

    public static checkContainerExists(idPrefix: string): boolean {
        return document.querySelector('#' + idPrefix + 'list') !== null;
    }

    public show(): void {
        this.mdcDataTable.root.classList.remove('d-none');
    }

    public hide(): void {
        this.mdcDataTable.root.classList.add('d-none');
    }

}

class RowSelection {

    private readonly mdcDataTable: MDCDataTable;
    private readonly actionContainer: HTMLDivElement;
    private readonly actionMdcSelect: MDCSelect
    private readonly actionForm: HTMLFormElement;
    private readonly actionSubmitButton: HTMLButtonElement;
    private readonly selectedRowIdsInput: HTMLInputElement;

    constructor(idPrefix: string, mdcDataTable: MDCDataTable) {
        this.mdcDataTable = mdcDataTable;
        this.actionContainer = this.mdcDataTable.root.querySelector('#' + idPrefix + 'row_selection_action_container');
        this.actionMdcSelect = new MDCSelect(this.actionContainer.querySelector('#' + idPrefix + 'row_selection_action_mdc_select'));
        this.actionForm = this.actionContainer.querySelector('#' + idPrefix + 'row_selection_action_form');
        this.actionSubmitButton = this.actionContainer.querySelector('#' + idPrefix + 'row_selection_action_submit_button');
        this.selectedRowIdsInput = this.actionContainer.querySelector('#' + idPrefix + 'row_selection_selected_row_ids_input');

        this.actionContainer.classList.add('d-none');
        this.actionSubmitButton.classList.add('d-none');

        this.mdcDataTable.listen('MDCDataTable:rowSelectionChanged', (): void => {
            this.toggleCheckboxRowAction();
        });

        this.mdcDataTable.listen('MDCDataTable:selectedAll', (): void => {
            this.toggleCheckboxRowAction();
        });

        this.mdcDataTable.listen('MDCDataTable:unselectedAll', (): void => {
            this.toggleCheckboxRowAction();
        });

        this.actionMdcSelect.listen('MDCSelect:change', (): void => {
            if (this.actionMdcSelect.selectedIndex >= 0) {
                this.actionSubmitButton.classList.remove('d-none');
                this.actionContainer.classList.remove('d-none');
                this.actionForm.setAttribute('action', this.actionMdcSelect.value);
            } else {
                this.actionSubmitButton.classList.add('d-none');
                this.actionForm.removeAttribute('action');
            }
        });

        this.actionSubmitButton.addEventListener('click', (event: MouseEvent): void => {
            event.preventDefault();

            this.selectedRowIdsInput.value = JSON.stringify(this.mdcDataTable.getSelectedRowIds());

            this.actionForm.submit();
        });
    }

    private toggleCheckboxRowAction(): void {
        if (this.mdcDataTable.getSelectedRowIds().length > 0) {
            this.actionContainer.classList.remove('d-none');
        } else {
            this.actionContainer.classList.add('d-none');
            this.actionMdcSelect.value = null;
        }
    }

}

export { ListComponent };
