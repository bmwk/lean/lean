import '../../../styles/dropzone.scss';
import Dropzone from 'dropzone';

class DropzoneUploadComponent {

    private readonly dropzone: Dropzone;
    private readonly dropzoneContainer: HTMLDivElement;
    private readonly submitButton: HTMLButtonElement;

    constructor(
        dropzoneContainer: HTMLDivElement,
        submitButton: HTMLButtonElement,
        paramName: string,
        acceptedFiles: string[]
    ) {
        this.dropzoneContainer = dropzoneContainer;
        this.submitButton = submitButton;

        this.disableSubmitButton();

        this.dropzone = new Dropzone(this.dropzoneContainer, {
            url: this.dropzoneContainer.dataset.uploadUrl,
            paramName: paramName,
            acceptedFiles: acceptedFiles.join(','),
            dictDefaultMessage: this.dropzoneContainer.dataset.dictDefaultMessage,
            dictRemoveFile: this.dropzoneContainer.dataset.dictRemoveFile,
            autoProcessQueue: false,
            parallelUploads: 100,
            addRemoveLinks: true
        });

        this.dropzone.on('queuecomplete', (): void  => {
            window.location.replace(this.dropzoneContainer.dataset.redirectUrl);
        });

        this.dropzone.on('addedfile', (): void  => {
            this.enableSubmitButton();
        });

        this.dropzone.on('removedfile', (): void  => {
            if (this.dropzone.getQueuedFiles().length === 0) {
                this.disableSubmitButton();
            }
        });

        this.submitButton.addEventListener('click', (): void => this.submit());
    }

    private submit(): void {
        this.dropzone.processQueue();
    }

    private disableSubmitButton(): void {
        this.submitButton.disabled = true;
    }

    private enableSubmitButton(): void {
        this.submitButton.disabled = false;
    }

}

export { DropzoneUploadComponent };
