class LazyImageLoader {

    private readonly intersectionObserver: IntersectionObserver;

    constructor() {
        this.intersectionObserver = new IntersectionObserver((intersectionObserverEntries: IntersectionObserverEntry[]): void => {
            intersectionObserverEntries.forEach((intersectionObserverEntry: IntersectionObserverEntry): void => {
                if (intersectionObserverEntry.isIntersecting) {
                    const divElement: HTMLDivElement = (intersectionObserverEntry.target as HTMLDivElement);

                    divElement.style.backgroundImage = 'url(\'' + divElement.dataset.backgroundImage + '\')';

                    this.intersectionObserver.unobserve(divElement);
                }
            });
        });

        document.querySelectorAll('.lazy-image-loader').forEach((element: Element): void => this.intersectionObserver.observe(element));
    }

}

export { LazyImageLoader };
