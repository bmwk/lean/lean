import Axios from 'axios';
import { saveAs } from 'file-saver';
import jsPDF from 'jspdf';
import { Collection, Feature, MapBrowserEvent, Overlay } from 'ol';
import CanvasTitle from 'ol-ext/control/CanvasTitle';
import Print from 'ol-ext/control/Print';
import PrintDialog from 'ol-ext/control/PrintDialog';
import ProgressBar from 'ol-ext/control/ProgressBar';
import SearchNominatim from 'ol-ext/control/SearchNominatim';
import 'ol-ext/dist/ol-ext.css';
import { defaults } from 'ol/control';
import { Coordinate } from 'ol/coordinate';
import { EventsKey } from 'ol/events';
import { click, pointerMove } from 'ol/events/condition';
import BaseEvent from 'ol/events/Event';
import { boundingExtent } from 'ol/extent';
import { FeatureLike } from 'ol/Feature';
import { Circle, Geometry, LineString, Point, Polygon } from 'ol/geom';
import { Type } from 'ol/geom/Geometry';
import { Draw, Modify, Select, Translate } from 'ol/interaction';
import { SelectEvent } from 'ol/interaction/Select';
import { TranslateEvent } from 'ol/interaction/Translate';
import LayerGroup from 'ol/layer/Group';
import TileLayer from 'ol/layer/Tile';
import VectorLayer from 'ol/layer/Vector';
import Map from 'ol/Map';
import { unByKey } from 'ol/Observable';
import 'ol/ol.css';
import { Pixel } from 'ol/pixel';
import { fromLonLat, ProjectionLike, toLonLat } from 'ol/proj';
import { toContext } from 'ol/render';
import { Cluster, OSM, TileWMS } from 'ol/source';
import VectorSource from 'ol/source/Vector';
import { getArea, getLength } from 'ol/sphere';
import { Fill, Icon, Stroke, Style, } from 'ol/style';
import Text from 'ol/style/Text';
import View from 'ol/View';
import LoadingImage from '../../../images/lean-loading.gif';
import Church from '../../../images/usage_icons/church.png';
import Cultural from '../../../images/usage_icons/cultural_sports_leisure.png';
import Education from '../../../images/usage_icons/education.png';
import Healthcare from '../../../images/usage_icons/healthcare.png';
import Hospitality from '../../../images/usage_icons/hospitality.png';
import Living from '../../../images/usage_icons/living.png';
import NonCommercial from '../../../images/usage_icons/non_commercial_use.png';
import OtherCommercial from '../../../images/usage_icons/other_commercial_use.png';
import Palette from '../../../images/usage_icons/palette.png';
import Parking from '../../../images/usage_icons/parking.png';
import ProcuringIndustry from '../../../images/usage_icons/procuring_industry.png';
import PublicFacility from '../../../images/usage_icons/public_facility.png';
import Retail from '../../../images/usage_icons/retail.png';
import Service from '../../../images/usage_icons/service.png';
import Social from '../../../images/usage_icons/social_welfare.png';
import Unknown from '../../../images/usage_icons/unknown_location.png';
import Wholesale from '../../../images/usage_icons/wholesale_trade.png';
import { Address, MarketingInformation, PropertyApiResponse } from '../Property/PropertyApiResponse';
import { Property, ScoredProperty } from './Property';
import { WmsLayer } from './WmsLayer';

interface WmsLayerMapping {

    wmsLayer: WmsLayer;
    wmsTileLayer: TileLayer<TileWMS>;

}

interface PropertyMapping {

    property: Property | ScoredProperty;
    pointFeature: Feature<Point>;
    polygonFeature: Feature<Polygon>;

}

interface GeolocationPolygon {
    id: number;
    polygon: {
        rings: {
            points: {
                x: number,
                y: number,
                longitude: number,
                latitude: number
            }[]
        }[]
    };
}

interface BusinessLocationsApiResponse {
    geolocationPolygon: GeolocationPolygon;
    locationCategory: number;
    id: number;
}

class MapApi {

    public readonly map: Map;
    public readonly view: View;
    private readonly tileLayers: LayerGroup;
    private readonly osmTileLayer: TileLayer<OSM>;
    public readonly pointSource: VectorSource<Point>;
    private readonly pointClusterSource: Cluster;
    private readonly pointLayer: VectorLayer<VectorSource>;
    public readonly polygonSource: VectorSource<Polygon>;
    private readonly polygonLayer: VectorLayer<VectorSource<Polygon>>;
    public readonly earlyWarningSystemPinSource: VectorSource<Point>;
    public readonly earlyWarningSystemClusterSource: Cluster;
    private readonly earlyWarningSystemPinLayer: VectorLayer<Cluster>;
    private readonly polygonStyle: Style;
    public popup: Overlay;
    readonly hoverInteraction: Select;
    public readonly selectInteraction: Select;
    private readonly translateInteraction: Translate;
    private readonly modifyInteraction: Modify;
    private readonly readonlyBusinessLocations: boolean;
    private readonly modifyBusinessLocationsInteraction: Modify;
    private readonly drawInteraction: Draw;
    private readonly drawBusinessLocationInteraction: Draw;
    private readonly businessLocationSource: VectorSource<any>;
    private readonly businessLocationLayer: VectorLayer<any>;
    private readonly scoredBusinessLocationSource: VectorSource<any>;
    private readonly scoredBusinessLocationLayer: VectorLayer<any>;
    private businessLocationId: number;
    private measureInteraction: Draw;
    private measureTooltip: Overlay;
    private readonly measureSource: VectorSource<any>;
    private readonly measureLayer: VectorLayer<any>;
    private readonly measureStyle: Style;
    public overviewPopup: Overlay;
    public printDialog: PrintDialog;
    public printControl: Print;
    private readonly progressBar: ProgressBar;
    private osmAdressSearch: SearchNominatim;
    private readonly propertyUsageMapping: string[];
    private readonly propertyUsageIconMapping: string[];

    private readonly propertyMappings: PropertyMapping[];
    private readonly wmsLayerMappings: WmsLayerMapping[];

    private useMarker: boolean;
    private markerSet: boolean;

    public locationSet: boolean;
    private static instance: MapApi;

    public static LOCATION_CATEGORIES = ['1A-Lage', '2A-Lage', '1B-Lage', '2B-Lage', 'Sonstige-Lage', '1C-Lage', '2C-Lage'];

    private constructor(readonlyBusinessLocations: boolean) {
        this.readonlyBusinessLocations = readonlyBusinessLocations;
        this.propertyMappings = [];
        this.wmsLayerMappings = [];

        this.view = new View({
            zoom: 14,
            maxZoom: 25
        });

        this.pointSource = new VectorSource<Point>();

        this.pointClusterSource = new Cluster({
            distance: 40,
            source: this.pointSource
        });

        this.pointLayer = new VectorLayer(
            {
                source: this.pointClusterSource,
                style: (feature) => {
                    return this.getPointStyle(feature);
                },
                visible: true,
                zIndex: 10,
            }
        );

        this.polygonSource = new VectorSource<Polygon>();

        this.polygonLayer = new VectorLayer<VectorSource<Polygon>>({
            source: this.polygonSource, style: (feature) => this.getPolygonStyle(feature, 0.5, 0.8), visible: true, zIndex: 9
        });

        this.earlyWarningSystemPinSource = new VectorSource<Point>();

        this.earlyWarningSystemClusterSource = new Cluster({
            source: this.earlyWarningSystemPinSource,
            distance: 40
        });

        this.earlyWarningSystemClusterSource.on('addfeature', (evt) => {
            const selected = this.selectInteraction.getFeatures().getArray();
            if (selected.length === 0) {
                return;
            }
            const features = evt.feature.get('features');
            for (let i = selected.length - 1; i >= 0; --i) {
                const sel = selected[i].get('features');
                if (sel === undefined) {
                    return;
                }
                if (sel.some((f: Feature) => features.includes(f))) {
                    if (features.length === sel.length && sel.every((f: Feature) => features.includes(f))) {
                        this.selectInteraction.getFeatures().setAt(i, evt.feature);
                    } else {
                        this.selectInteraction.getFeatures().removeAt(i);
                    }
                }
            }

        });

        this.earlyWarningSystemPinLayer = new VectorLayer<Cluster>(
            {
                source: this.earlyWarningSystemClusterSource,
                style: feature => this.getScoredPointStyle(feature),
                visible: true,
                zIndex: 10
            });

        this.measureStyle = new Style({
            stroke: new Stroke({color: 'rgba(0,0,0,0.3)', width: 2}),
            fill: new Fill({color: 'rgba(0,0,0,0.1)'})
        });

        this.measureSource = new VectorSource<any>();

        this.measureLayer = new VectorLayer<any>({source: this.measureSource, style: this.measureStyle, visible: false});

        this.businessLocationSource = new VectorSource<any>();

        this.businessLocationLayer = new VectorLayer<any>({
            source: this.businessLocationSource, visible: false, style: this.getBusinessLocationStyle, zIndex: 5
        });

        this.scoredBusinessLocationSource = new VectorSource<any>();

        this.scoredBusinessLocationLayer = new VectorLayer<any>({
            source: this.scoredBusinessLocationSource, visible: false, style: this.getScoredBusinessLocationStyle, zIndex: 5
        });

        this.osmTileLayer = new TileLayer({source: new OSM()});

        this.tileLayers = new LayerGroup({
            layers: [
                this.osmTileLayer,
                this.pointLayer,
                this.polygonLayer,
                this.measureLayer,
                this.businessLocationLayer,
                this.earlyWarningSystemPinLayer,
                this.scoredBusinessLocationLayer
            ]
        });

        this.hoverInteraction = new Select({
            condition: pointerMove,
            style: null
        });

        this.selectInteraction = new Select({
            condition: click,
            style: (feature) => {
                if (feature.getGeometry().getType() === 'Polygon') {
                    if (feature.get('pressureClassification') !== undefined) {
                        return this.getBusinessLocationStyleByPressureClassification(feature.get('pressureClassification'), 0.8, 1);
                    } else if (feature.get('locationCategory') !== undefined) {
                        return this.getBusinessLocationStyleByLocationCategoryId(feature.get('locationCategory'), 0.8, 1);
                    } else {
                        return this.getPolygonStyle(feature, 0.8, 1);
                    }
                } else if (feature.get('features') !== undefined && feature.get('features')[0].get('taodBuildingUnitId')) {
                    return this.getScoredPointStyle(feature, false);
                }
                return this.getPointStyle(feature, false);
            },
            layers: [this.polygonLayer, this.pointLayer, this.businessLocationLayer, this.earlyWarningSystemPinLayer, this.scoredBusinessLocationLayer]
        });

        this.translateInteraction = new Translate({
            layers: this.readonlyBusinessLocations ?
                [this.pointLayer, this.polygonLayer] :
                [this.pointLayer, this.polygonLayer, this.businessLocationLayer]
        });

        this.translateInteraction.on('translatestart', (event => {
            event.features.forEach((feature: Feature<Point>) => {
                if (feature.getGeometry().getType() === 'Point') {
                    feature.setGeometry(new Point(event.coordinate));
                }
            });
        }));

        this.translateInteraction.on('translateend', (event: TranslateEvent): void => {
            if ((event.features as Collection<Feature<Geometry>>).getArray()[0].getGeometry().getType() === 'Point') {
                this.getPointFeatures()[0].setGeometry(new Point(event.coordinate));
            }
        });

        this.modifyInteraction = new Modify({
            source: this.polygonSource
        });

        this.modifyBusinessLocationsInteraction = new Modify({
            source: this.businessLocationSource
        });

        this.drawInteraction = new Draw({
            source: this.polygonSource,
            type: 'Polygon'
        });

        this.drawBusinessLocationInteraction = new Draw({
            source: this.businessLocationSource,
            type: 'Polygon'
        });

        this.drawBusinessLocationInteraction.on('drawend', (event => {
            event.feature.set('locationCategory', this.businessLocationId);
        }));

        this.progressBar = new ProgressBar({
            label: '<img src="' + LoadingImage + '" class="ol-loading-image" alt="Loading">Lade Karte',
            layers: [this.osmTileLayer, this.pointLayer, this.polygonLayer]
        });

        this.useMarker = false;
        this.markerSet = false;

        this.propertyUsageMapping = this.getPropertyUsageMapping();

        this.propertyUsageIconMapping = [
            Unknown,
            Retail,
            Service,
            Hospitality,
            Cultural,
            Parking,
            Education,
            Healthcare,
            Social,
            PublicFacility,
            Church,
            ProcuringIndustry,
            Palette,
            Wholesale,
            OtherCommercial,
            Living,
            NonCommercial,
        ];

        this.map = new Map({
            controls: defaults({attribution: false}),
            layers: this.tileLayers,
            view: this.view
        });
        this.printDialog = this.addPrintControl();
        this.printControl = this.addPrint();
    }

    public static getInstance() {
        if (!MapApi.instance) {
            MapApi.instance = new MapApi(false);
        }
        return MapApi.instance;
    }

    public initializeMap = (target: HTMLElement) => {
        this.map.setTarget(target);
        this.osmAdressSearch = new SearchNominatim({
            polygon: false,
            reverse: true,
            onselect: (parameter): void => {
                if (this.useMarker && !this.markerSet) {
                    this.markerSet = true;

                    this.pointSource.addFeature(new Feature({
                        geometry: new Point(parameter.coordinate)
                    }));

                    setTimeout(() => this.locationSet = true, 1000);
                }

                this.map.getView().animate({
                    center: parameter.coordinate,
                    zoom: Math.max(this.map.getView().getZoom(), 17)
                });
            }
        });
        this.map.addControl(this.osmAdressSearch);
        this.map.addControl(this.progressBar);
        this.map.addInteraction(this.hoverInteraction);
        this.map.addInteraction(this.selectInteraction);

        setInterval(() => {
            const mapSize = this.map.getSize();
            const mapParent = this.map.getTargetElement();
            if (mapSize[0] !== mapParent.clientWidth || mapSize[1] !== mapParent.clientHeight) {
                this.map.setSize([mapParent.clientWidth, mapParent.clientHeight]);
            }
        }, 500);

    };

    private getBusinessLocationHighlightStyle = (feature: Feature): Style => {
        const fillOpacity = 0.5;
        const strokeOpacity = 1;
        return this.getBusinessLocationStyleByLocationCategoryId(feature.get('locationCategory'), fillOpacity, strokeOpacity);
    };

    private getBusinessLocationStyle = (feature: Feature): Style => {
        const fillOpacity = 0.25;
        const strokeOpacity = 1;
        return this.getBusinessLocationStyleByLocationCategoryId(feature.get('locationCategory'), fillOpacity, strokeOpacity);
    };

    private getScoredBusinessLocationStyle = (feature: Feature): Style => {
        const fillOpacity = 0.25;
        const strokeOpacity = 1;
        return this.getBusinessLocationStyleByPressureClassification(feature.get('pressureClassification'), fillOpacity, strokeOpacity);
    };

    private getBusinessLocationStyleByLocationCategoryId(locationCategory: number, fillOpacity: number, strokeOpacity: number): Style {
        switch (locationCategory) {
            case 0:
                return new Style({
                    fill: new Fill({color: `rgba(255,0,0,${fillOpacity})`}),
                    stroke: new Stroke({color: `rgba(255,0,0,${strokeOpacity})`, width: 2})
                });

            case 1:
                return new Style({
                    fill: new Fill({color: `rgba(100,0,0,${fillOpacity})`}),
                    stroke: new Stroke({color: `rgba(100,0,0,${strokeOpacity})`, width: 2})
                });

            case 2:
                return new Style({
                    fill: new Fill({color: `rgba(0,0,255,${fillOpacity})`}),
                    stroke: new Stroke({color: `rgba(0,0,255,${strokeOpacity})`, width: 2})
                });

            case 3:
                return new Style({
                    fill: new Fill({color: `rgba(0,0,100,${fillOpacity})`}),
                    stroke: new Stroke({color: `rgba(0,0,100,${strokeOpacity})`, width: 2})
                });

            case 5:
                return new Style({
                    fill: new Fill({color: `rgba(0,255,0,${fillOpacity})`}),
                    stroke: new Stroke({color: `rgba(0,255,0,${strokeOpacity})`, width: 2})
                });

            case 6:
                return new Style({
                    fill: new Fill({color: `rgba(0,100,0,${fillOpacity})`}),
                    stroke: new Stroke({color: `rgba(0,100,0,${strokeOpacity})`, width: 2})
                });

            default:
                return new Style({
                    fill: new Fill({color: `rgba(100,100,100,${fillOpacity})`}),
                    stroke: new Stroke({color: `rgba(100,100,100,${strokeOpacity})`, width: 2})
                });
        }
    }

    private getBusinessLocationStyleByPressureClassification(pressureClassification: number, fillOpacity: number, strokeOpacity: number): Style {
        const colors = MapApi.getPressureClassificationColors;
        if (pressureClassification >= 1.5) {
            return new Style({
                fill: new Fill({color: colors(fillOpacity).success}),
                stroke: new Stroke({color: colors(strokeOpacity).success, width: 2})
            });
        } else if (pressureClassification >= 0.5) {
            return new Style({
                fill: new Fill({color: colors(fillOpacity).warning}),
                stroke: new Stroke({color: colors(strokeOpacity).warning, width: 2})
            });
        } else if (pressureClassification === null) {
            return new Style({
                fill: new Fill({color: colors(fillOpacity).undefined}),
                stroke: new Stroke({color: colors(strokeOpacity).undefined, width: 2})
            });
        } else {
            return new Style({
                fill: new Fill({color: colors(fillOpacity).danger}),
                stroke: new Stroke({color: colors(strokeOpacity).danger, width: 2})
            });
        }
    }

    private getPointStyle = (feature: FeatureLike, includeIcon: boolean = true): Style[] => {
        const clusteredFeatures = feature.get('features');
        const colors = MapApi.getObjectStatusColors();
        let pinColor = 'black';
        let pinIcon = this.propertyUsageIconMapping[0];

        if (clusteredFeatures.length === 1) {
            const objectIsEmpty: Boolean = clusteredFeatures[0].get('objectIsEmpty');
            const objectBecomesEmpty: Boolean = clusteredFeatures[0].get('objectBecomesEmpty');
            const objectReuseAgreed: Boolean = clusteredFeatures[0].get('reuseAgreed');
            const objectInUse: Boolean = !objectIsEmpty && !objectBecomesEmpty && !objectReuseAgreed;

            if (objectInUse === true) {
                pinColor = colors.success;
            } else if (objectBecomesEmpty === true) {
                pinColor = colors.warning;
            } else if (objectReuseAgreed === true) {
                pinColor = colors.info;
            } else if (objectIsEmpty === true) {
                pinColor = colors.danger;
            }

            let propertyUsage = clusteredFeatures[0].get('currentUsageIndustryClassification')?.levelOne;
            if (propertyUsage == undefined) propertyUsage = clusteredFeatures[0].get('pastUsageIndustryClassification')?.levelOne;
            if (propertyUsage == undefined) propertyUsage = clusteredFeatures[0].get('futureUsageIndustryClassification')?.levelOne;

            if (
                propertyUsage == undefined
                && clusteredFeatures[0].get('propertyMarketingInformation')?.possibleUsageIndustryClassifications !== undefined
                && clusteredFeatures[0].get('propertyMarketingInformation')?.possibleUsageIndustryClassifications !== null
            ) {
                propertyUsage = clusteredFeatures[0].get('propertyMarketingInformation')?.possibleUsageIndustryClassifications[0]?.levelOne;
            }

            if (propertyUsage == undefined) propertyUsage = 0;

            pinIcon = this.propertyUsageIconMapping[propertyUsage];
        }

        const size = 30;
        const symbol = [
            [size / 2, size + size / 2],
            [size, size / 2],
            [0, size / 2],
            [size / 2, size + size / 2],
        ];

        let iconStyle: Style;
        const canvas = document.createElement('canvas');
        const canvasContext = canvas.getContext('2d');
        const vectorContext = toContext(canvasContext, {
            size: [50, 100],
            pixelRatio: 1,
        });

        vectorContext.setStyle(new Style({fill: new Fill({color: pinColor})}));
        vectorContext.drawGeometry(new Polygon([symbol]));
        vectorContext.drawGeometry(new Circle([size / 2, size / 2], size / 2));

        if (includeIcon === true) {
            vectorContext.setStyle(
                new Style({
                    fill: new Fill({color: 'white'})
                })
            );
            vectorContext.drawGeometry(new Circle([size / 2, size / 2], size / 2.5));

            if (clusteredFeatures.length > 1) {
                iconStyle = new Style({
                    text: new Text({
                        text: String(clusteredFeatures.length < 100 ? clusteredFeatures.length : '99+'),
                        offsetY: -34,
                        offsetX: clusteredFeatures.length < 100 ? -2 : -6,
                        scale: 1.2,
                    })
                });
            } else {
                iconStyle = new Style({
                    image: new Icon({
                        src: pinIcon,
                        imgSize: [20, 20],
                        anchor: [0.5, 2.2]
                    })
                });
            }
        }

        const baseStyle = new Style({
            image: new Icon({
                img: canvas,
                imgSize: [50, 100],
                anchor: [0.35, 0.5]
            }),
        });

        return iconStyle ? [baseStyle, iconStyle] : [baseStyle];
    };

    public getScoredPointStyle = (feature: FeatureLike, includeIcon: boolean = true): Style[] => {

        const clusteredFeatures = feature.get('features');
        const pressureClassification = clusteredFeatures[0].get('pressureClassification');
        const colors = MapApi.getObjectStatusColors();
        let pinColor = 'black';
        let pinIcon = this.propertyUsageIconMapping[0];

        const size = 30;
        let symbol = [
            [size / 2, size + size / 2],
            [size, size / 2],
            [0, size / 2],
            [size / 2, size + size / 2],
        ];

        if (clusteredFeatures.length === 1) {

            if (pressureClassification < 0.5) {
                pinColor = colors.danger;
            } else if (pressureClassification < 1.5) {
                pinColor = colors.warning;
            } else {
                pinColor = colors.success;
            }

            if (Boolean(clusteredFeatures[0].get('buildingUnitEnriched').objectIsEmpty) === true) {
                pinColor = colors.undefined;
            }

            let propertyUsage = clusteredFeatures[0].get('buildingUnitEnriched').propertyUsersIndustryClassificationLevelOne;
            if (propertyUsage == undefined) propertyUsage = 0;
            pinIcon = this.propertyUsageIconMapping[propertyUsage];

            if (clusteredFeatures[0].get('buildingUnitId') === null) {
                symbol = [
                    [0, 0],
                    [size, 0],
                    [size, size],
                    [size / 2, size + size / 2],
                    [0, size],
                    [0, 0],
                ];
            }

        }

        let iconStyle: Style;
        const canvas = document.createElement('canvas');
        const canvasContext = canvas.getContext('2d');
        const vectorContext = toContext(canvasContext, {
            size: [50, 100],
            pixelRatio: 1,
        });

        vectorContext.setStyle(new Style({fill: new Fill({color: pinColor})}));
        vectorContext.drawGeometry(new Polygon([symbol]));
        vectorContext.drawGeometry(new Circle([size / 2, size / 2], size / 2));

        if (includeIcon === true) {
            vectorContext.setStyle(
                new Style({
                    fill: new Fill({color: 'white'})
                })
            );
            vectorContext.drawGeometry(new Circle([size / 2, size / 2], size / 2.5));

            if (clusteredFeatures.length > 1) {
                iconStyle = new Style({
                    text: new Text({
                        text: String(clusteredFeatures.length < 100 ? clusteredFeatures.length : '99+'),
                        offsetY: -34,
                        offsetX: -2,
                        scale: 1.1,
                        textAlign: 'center'
                    })
                });
            } else {
                iconStyle = new Style({
                    image: new Icon({
                        src: pinIcon,
                        imgSize: [20, 20],
                        anchor: [0.5, 2.2]
                    })
                });
            }
        }

        const baseStyle = new Style({
            image: new Icon({
                img: canvas,
                imgSize: [50, 100],
                anchor: [0.35, 0.5]
            }),
        });

        return iconStyle ? [baseStyle, iconStyle] : [baseStyle];
    };

    public static getObjectStatusColors() {
        return localStorage.getItem('lean.map.barrierFree') === 'true' ?
            {success: '#00918e', danger: '#c603fc', warning: '#f7951a', info: '#0000ff', undefined: '#8f8f8f'} :
            {success: '#70a03c', danger: '#b33052', warning: '#f7951a', info: '#0000ff', undefined: '#8f8f8f'};
    }

    public static getPressureClassificationColors(opacity: number = 1) {
        return {success: `rgba(112,160,60,${opacity})`, danger: `rgba(179, 48, 82, ${opacity})`, warning: `rgba(247, 149, 26,${opacity})`, info: `rgba(0,0,255,${opacity})`, undefined: `rgba(143,143,143,${opacity})`};
    }

    private getPolygonStyle = (feature: FeatureLike, fillOpacity: number, strokeOpacity: number): Style => {
        const objectIsEmpty: Boolean = feature.get('objectIsEmpty');
        const objectBecomesEmpty: Boolean = feature.get('objectBecomesEmpty');
        const objectInUse: Boolean = !objectIsEmpty && !objectBecomesEmpty;

        return new Style({
            fill: new Fill({color: objectInUse ? `rgba(0,255,0,${fillOpacity})` : objectBecomesEmpty ? `rgba(255,255,0,${fillOpacity})` : `rgba(255,0,0,${fillOpacity})`}),
            stroke: new Stroke({color: objectInUse ? `rgba(0,255,0,${strokeOpacity})` : objectBecomesEmpty ? `rgba(255,255,0,${strokeOpacity})` : `rgba(255,0,0,${strokeOpacity})`})
        });
    };

    public onMapMoveEnd(callback: Function) {
        this.map.on(['moveend'], (event) => callback(event));
    }

    public onMapRenderComplete(callback: Function) {
        this.map.on(['rendercomplete'], (event) => callback(event));
    }

    public initializeProperties(properties: Property[] | ScoredProperty[]): void {
        this.propertyMappings.length = 0;
        properties.forEach((property): void => {
            this.addPropertyMapping(property);
        });
    }

    public addPropertyMapping(property: Property | ScoredProperty): void {
        if (this.fetchPropertyMappingByProperty(property) !== null) return;

        const propertyMapping: PropertyMapping = {
            property: property,
            pointFeature: null,
            polygonFeature: null
        };

        if (property.longitude && property.latitude) {
            propertyMapping.pointFeature = MapApi.createPointFeatureFromProperty(property);
        }

        if ('polygonCoordinates' in property && property.polygonCoordinates !== undefined && property.polygonCoordinates !== null) {
            propertyMapping.polygonFeature = MapApi.createPolygonFeatureFromProperty(property, this.polygonStyle);
        }

        this.propertyMappings.push(propertyMapping);
    }

    public static async fetchBuildingUnitsFromApi(archived: boolean): Promise<Property[]> {
        const properties: Property[] = [];

        try {
            const response = await Axios.get('/api/property-vacancy-management/building-units' + (archived ? '?archived=true' : ''));
            response.data.forEach((propertyApiResponse: PropertyApiResponse): void => {
                if (propertyApiResponse.geolocationPoint !== null || propertyApiResponse.geolocationPolygon !== null) {
                    properties.push(MapApi.createPropertyFromApiResponse(propertyApiResponse));
                }
            });
        } catch (error) {
            console.error(error);
        }

        return properties;
    }

    public static async fetchEnrichedBuildingUnitsFromApi(): Promise<ScoredProperty[]> {
        const properties: ScoredProperty[] = [];

        const response = await Axios.get('/api/early-warning-system/building-unit-scorings');
        response.data.forEach((propertyApiResponse: PropertyApiResponse): void => {
            properties.push(MapApi.createScoredPropertyFromApiResponse(propertyApiResponse));
        });

        return properties;
    }

    public static fetchAccountUsers = async (): Promise<any> => {
        const response = await Axios.get('/api/property-vacancy-management/account-users');
        return response.data;
    };

    public static fetchFirstLevelIndustryClassificationsFromApi = async () => {
        const response = await fetch('/api/classifications/industry-classifications?level=1');
        const data = await response.json();
        return data;
    };

    public static fetchSecondLevelIndustryClassificationsFromApi = async () => {
        const response = await fetch('/api/classifications/industry-classifications?level=2');
        const data = await response.json();
        return data;
    };

    private static createPropertyFromApiResponse(propertyApiResponse: PropertyApiResponse): Property {
        let latitude: number = null;
        let longitude: number = null;
        let polygonCoordinates: number[][][] = null;

        if (propertyApiResponse.geolocationPoint !== null) {
            latitude = propertyApiResponse.geolocationPoint.point.latitude;
            longitude = propertyApiResponse.geolocationPoint.point.longitude;
        }

        if (propertyApiResponse.geolocationPolygon !== null) {
            polygonCoordinates = [];

            let points: number[][] = [];

            propertyApiResponse.geolocationPolygon.polygon.rings.forEach((ring: any): void => {
                ring.points.forEach((point: any): void => {
                    points.push(MapApi.fromLonLat([point.longitude, point.latitude]));
                });

                polygonCoordinates.push(points);
            });
        }

        return {
            ...propertyApiResponse,
            href: '/besatz-und-leerstand/objekte/' + propertyApiResponse.id + '/grunddaten',
            latitude: latitude,
            longitude: longitude,
            polygonId: propertyApiResponse.geolocationPolygon?.id,
            polygonCoordinates: polygonCoordinates,
        };
    }

    private static createScoredPropertyFromApiResponse(propertyApiResponse: any): ScoredProperty {
        let latitude: number = null;
        let longitude: number = null;

        if (propertyApiResponse.buildingUnitEnriched !== null && propertyApiResponse.buildingUnitEnriched.geolocationPoint !== null) {
            latitude = propertyApiResponse.buildingUnitEnriched.geolocationPoint.latitude;
            longitude = propertyApiResponse.buildingUnitEnriched.geolocationPoint.longitude;
        } else {
            console.debug('geolocationPoint / buildingUnitEnriched is null');
        }

        return {
            latitude: latitude,
            longitude: longitude,
            ...propertyApiResponse,
        };
    }

    private static createPointFeatureFromProperty(property: Property | ScoredProperty): Feature<Point> {
        const pointFeature: Feature<Point> = new Feature({
            geometry: new Point(fromLonLat([property.longitude, property.latitude])),
            ...property
        });
        return pointFeature;
    }

    private static createPolygonFeatureFromProperty(property: Property, polygonStyle: Style): Feature<Polygon> {
        const polygonFeature = new Feature<Polygon>({
            geometry: new Polygon(property.polygonCoordinates),
            ...property
        });
        polygonFeature.setStyle(polygonStyle);
        return polygonFeature;
    }

    public addPropertyPointsAndPolygonFeatures(propertyMappings: PropertyMapping[], pointSource: VectorSource<Point>, polygonSource: VectorSource<Polygon>): void {
        const pointFeatures: Feature<Point>[] = [];
        const polygonFeatures: Feature<Polygon>[] = [];

        propertyMappings.forEach(propertyMapping => {
            if (propertyMapping === null) return;
            if (propertyMapping.pointFeature !== null && pointSource !== null) pointFeatures.push(propertyMapping.pointFeature);
            if (propertyMapping.polygonFeature !== null && polygonSource !== null) polygonFeatures.push(propertyMapping.polygonFeature);
        });

        if (pointSource !== null) {
            pointSource.addFeatures(pointFeatures);
        }

        if (polygonSource !== null) {
            polygonSource.addFeatures(polygonFeatures);
        }

    }

    public removePropertyPointAndPolygonFeature(property: Property): void {
        const propertyMapping: PropertyMapping = this.fetchPropertyMappingByProperty(property);

        if (propertyMapping === null) {
            return;
        }

        if (
            propertyMapping.pointFeature !== null
            && this.pointSource.hasFeature(propertyMapping.pointFeature) === true
        ) {
            this.pointSource.removeFeature(propertyMapping.pointFeature);
        }

        if (
            propertyMapping.polygonFeature !== null
            && this.polygonSource.hasFeature(propertyMapping.polygonFeature) === true
        ) {
            this.polygonSource.removeFeature(propertyMapping.polygonFeature);
        }
    }

    private fetchPropertyMappingByProperty(property: Property | ScoredProperty): PropertyMapping {
        const propertyMappingIndex: number = this.propertyMappings.map((propertyMapping: PropertyMapping) => propertyMapping.property)
        .indexOf(property);

        if (propertyMappingIndex === -1) {
            return null;
        }

        return this.propertyMappings[propertyMappingIndex];
    }

    public initializeWmsLayers(wmsLayers: WmsLayer[]): void {
        wmsLayers.forEach((wmsLayer: WmsLayer): void => {
            this.addWmsLayerMapping(wmsLayer);
        });
    }

    public addWmsLayerMapping(wmsLayer: WmsLayer): void {
        if (this.getWmsLayerMappingByWmsLayerId(wmsLayer.id) !== null) {
            return;
        }

        this.wmsLayerMappings.push({
            wmsLayer: wmsLayer,
            wmsTileLayer: MapApi.createWmsTileLayerFromWmsLayer(wmsLayer)
        });
    }

    private static createWmsTileLayerFromWmsLayer(wmsLayer: WmsLayer): TileLayer<TileWMS> {
        const tileLayer = new TileLayer<TileWMS>({
            maxZoom: 21,
            source: new TileWMS({
                url: wmsLayer.url,
                params: wmsLayer.parameter
            })
        });
        tileLayer.setZIndex(0);
        return tileLayer;
    }

    public addWmsTileLayer(wmsLayerId: number): void {
        const wmsLayerMapping: WmsLayerMapping = this.getWmsLayerMappingByWmsLayerId(wmsLayerId);

        if (wmsLayerMapping === null) {
            return;
        }

        this.map.addLayer(wmsLayerMapping.wmsTileLayer);
    }

    public removeWmsTileLayer(wmsLayerId: number): void {
        const wmsLayerMapping: WmsLayerMapping = this.getWmsLayerMappingByWmsLayerId(wmsLayerId);

        if (wmsLayerMapping === null) {
            return;
        }

        this.map.removeLayer(wmsLayerMapping.wmsTileLayer);
    }

    public getWmsLayerMappingByWmsLayerId(wmsLayerId: number): WmsLayerMapping {
        const wmsLayerMappingIndex: number = this.wmsLayerMappings.findIndex((wmsLayerMapping: WmsLayerMapping) => wmsLayerMapping.wmsLayer.id === wmsLayerId);
        if (wmsLayerMappingIndex === -1) return null;
        return this.wmsLayerMappings[wmsLayerMappingIndex];
    }

    public setCenter(coordinate: Coordinate) {
        this.view.setCenter(coordinate);
    }

    public setZoom(zoom: number) {
        this.view.setZoom(zoom);
    }

    public async setInitialMapLocation(address: string, searchFailedCallback: Function): Promise<void> {
        this.markerSet = false;
        this.osmAdressSearch.clearHistory();
        this.osmAdressSearch.setInput(address, true);
        this.osmAdressSearch.search();
        let searchRetries = 0;

        await new Promise<void>((resolve, reject) => {
            const selectEvent = setInterval(() => {
                const input = document.querySelector('.nominatim > ul.autocomplete > li');
                if (input) {
                    input.dispatchEvent(new Event('click'));
                    clearInterval(selectEvent);
                    resolve();
                } else if (++searchRetries >= 20) {
                    clearInterval(selectEvent);
                    searchFailedCallback();
                    resolve();
                }
            }, 100);
        });
    }

    public useMarkerForLocation(useMarker: boolean): void {
        this.useMarker = useMarker;
    }

    public addPrint(): Print {
        const print: Print = new Print();
        this.map.addControl(print);

        return print;
    }

    public addPrintEvent(callback: EventListener, eventType: string): void {
        this.printControl.addEventListener(eventType, callback);
    }

    public printMap(printProperties: any): void {
        this.printControl.print(printProperties);
    }

    public addPrintControl(): PrintDialog {
        this.map.addControl(
            new CanvasTitle({
                title: 'LeAn-Karte',
                style: new Style({
                    text: new Text({
                        font: '20px "Lucida Grande",Verdana,Geneva,Lucida,Arial,Helvetica,sans-serif'
                    })
                })
            })
        );

        const printDialog: PrintDialog = new PrintDialog({
            lang: 'de',
            copy: true,
            print: true,
            jsPDF: jsPDF,
            saveAs: saveAs
        });

        printDialog.setSize('A4');

        this.map.addControl(printDialog);

        return printDialog;
    }

    public setPolygonLayerVisibility(visible: boolean) {
        this.polygonLayer.setVisible(visible);
    }

    public setPointLayerVisibility(visible: boolean) {
        this.pointLayer.setVisible(visible);
    }

    public setEarlyWarningSystemLayerVisibility(visible: boolean) {
        this.earlyWarningSystemPinLayer.setVisible(visible);
    }

    public setScoredBusinessLocationLayerVisibility(visible: boolean) {
        this.scoredBusinessLocationLayer.setVisible(visible);
    }

    public setMeasureLayerVisibility(visible: boolean) {
        this.measureLayer.setVisible(visible);
        if (visible === true) {
            document.querySelectorAll('.measure-tooltip').forEach((tooltip: HTMLDivElement) => {
                tooltip.style.visibility = 'visible';
            });
        } else {
            document.querySelectorAll('.measure-tooltip').forEach((tooltip: HTMLDivElement) => {
                tooltip.style.visibility = 'hidden';
            });
        }
    }

    public setBusinessLocationLayerVisibility(visible: boolean) {
        this.businessLocationLayer.setVisible(visible);
    }

    public initializePopup(): void {

        const target = document.createElement('div');
        target.classList.add('mdc-card', 'p-2');
        const title = document.createElement('b');
        title.classList.add('text-primary');
        title.id = 'popup-title';
        const body = document.createElement('span');
        body.id = 'popup-body';

        this.popup = new Overlay({
            element: target,
            offset: [20, -10],
            autoPan: true,
            autoPanAnimation: {
                duration: 250
            },
            className: 'context-popup ol-overlay-container ol-selectable'
        });

        this.map.addOverlay(this.popup);
    }

    public initializeOverviewPopup(target: HTMLElement): void {
        this.overviewPopup = new Overlay({
            element: target,
            offset: [20, -100],
            autoPan: true,
            autoPanAnimation: {
                duration: 250
            }
        });

        this.map.addOverlay(this.overviewPopup);

        this.map.on('click', (event): void => {
            if (this.map.getFeaturesAtPixel(event.pixel).length == 0) {
                this.overviewPopup.setPosition(undefined);
            }
        });
    }

    public static getDescriptionFromPressureClassification(pressureClassification: number): string {
        if (pressureClassification < 0.5) {
            return 'wenig attraktiv';
        } else if (pressureClassification < 1.5) {
            return 'mäßig attraktiv';
        } else {
            return 'attraktiv';
        }
    }

    public showPopupOnFeatureHover(): void {
        this.hoverInteraction.on('select', (event): void => {

            this.popup.getElement().innerText = '';
            if (event.selected.length <= 0) {
                this.popup.setPosition(undefined);
                return;
            }

            const title = document.createElement('b');
            const body = document.createElement('small');

            const layerOfSelectedFeature = this.hoverInteraction.getLayer(event.selected[0]);
            const layerSource = layerOfSelectedFeature.getSource();

            if (layerSource instanceof Cluster) {
                const clusterFeatureCount = event.selected[0].get('features')?.length;

                if (clusterFeatureCount > 1) {
                    title.textContent = clusterFeatureCount + ' Objekte verfügbar';
                    body.innerHTML = 'An dieser Position sind mehrere Objekte verfügbar. <br>Für eine Detailansicht klicken';
                } else {
                    title.textContent = event.selected[0].get('features')[0].get('name');
                    const ids = 'Objekt-Nr: ' + event.selected[0].get('features')[0].get('id') + (event.selected[0].get('features')[0].get('internalNumber') ? ' (' + event.selected[0].get('features')[0].get('internalNumber') + ')' : '');
                    const usage = this.getUsageFromFeature(event.selected[0].get('features')[0]);
                    const address = this.getAddressInfoFromFeature(event.selected[0].get('features')[0]);
                    const inFloors = event.selected[0].get('features')[0].get('inFloors');
                    let dateInformation: string = '';

                    if (event.selected[0].get('features')[0].get('objectIsEmpty') && event.selected[0].get('features')[0].get('objectIsEmptySince')) {
                        dateInformation = '<br>Objekt steht leer seit: ' + this.formatDate(event.selected[0].get('features')[0].get('objectIsEmptySince'));
                    } else if (event.selected[0].get('features')[0].get('objectBecomesEmpty') && event.selected[0].get('features')[0].get('objectBecomesEmptyFrom')) {
                        dateInformation = '<br>Objekt fällt leer zum: ' + this.formatDate(event.selected[0].get('features')[0].get('objectBecomesEmptyFrom'));
                    } else if (event.selected[0].get('features')[0].get('reuseAgreed') && event.selected[0].get('features')[0].get('reuseFrom')) {
                        dateInformation = '<br>Nachnutzung ab: ' + this.formatDate(event.selected[0].get('features')[0].get('reuseFrom'));
                    }

                    body.innerHTML = ids + '<br>' + (usage ? usage + '<br>' : '') + address + (inFloors ? '<br>' + inFloors : '') + dateInformation;
                }
            } else {
                if (event.selected[0].get('businessLocationId')) {
                    title.textContent = MapApi.LOCATION_CATEGORIES[event.selected[0].get('locationCategory')];
                } else {
                    title.textContent = event.selected[0].get('name');
                    const ids: string = 'Objekt-Nr: ' + event.selected[0].get('id') + (event.selected[0].get('internalNumber') ? ' (' + event.selected[0].get('internalNumber') + ')' : '');
                    const usage = this.getUsageFromFeature(event.selected[0]);
                    const address = this.getAddressInfoFromFeature(event.selected[0]);
                    const inFloors = event.selected[0].get('inFloors');
                    let dateInformation: string = '';

                    if (event.selected[0].get('objectIsEmpty') && event.selected[0].get('objectIsEmptySince')) {
                        dateInformation = '<br>Objekt steht leer seit: ' + this.formatDate(event.selected[0].get('objectIsEmptySince'));
                    } else if (event.selected[0].get('objectBecomesEmpty') && event.selected[0].get('objectBecomesEmptyFrom')) {
                        dateInformation = '<br>Objekt fällt leer zum: ' + this.formatDate(event.selected[0].get('objectBecomesEmptyFrom'));
                    } else if (event.selected[0].get('reuseAgreed') && event.selected[0].get('reuseFrom')) {
                        dateInformation = '<br>Nachnutzung ab: ' + this.formatDate(event.selected[0].get('reuseFrom'));
                    }
                    body.innerHTML = ids + '<br>' + (usage ? usage + '<br>' : '') + address + (inFloors ? '<br>' + inFloors : '') + dateInformation;
                }
            }

            const coordinate = event.mapBrowserEvent.coordinate;

            this.popup.getElement().append(title, body);
            this.popup.setPosition(coordinate);
        });
    }

    public getPropertyUsageMapping(): string[] {
        return [
            'Nutzung nicht definiert',
            'Einzelhandel',
            'Dienstleistung',
            'Gastronomie / Beherberung',
            'Kunst, Kultur, Unterhaltung, Sport, Spiel, Freizeit',
            'Parken, Personenbeförderung & Transport',
            'Bildung & Lernen',
            'Gesundheitswesen',
            'Sozialwesen / Gemeinwohl',
            'Einrichtungen der Verwaltung',
            'Interessensvertretungen, Vereine, kirchliche Einrichtungen',
            'Handwerk, Produktion, Manufaktur',
            'Kreativwirtschaft',
            'Großhandel & Handelsvermittlung',
            'Andere gewerbliche Nutzung',
            'Wohnen',
            'Andere nicht-gewerbliche Nutzung'
        ];
    }

    public formatDate = (date: string): string => {
        return new Date(date).toLocaleDateString('de-DE', {
            day: '2-digit',
            month: '2-digit',
            year: '2-digit'
        });
    };

    public getPropertyUsageIconById(id: number) {
        return this.propertyUsageIconMapping[id];
    }

    public forwardOnClick(): void {
        this.selectInteraction.on('select', (event: SelectEvent): void => {
            if (!event.selected || event.selected.length <= 0) {
                return;
            }
            const isClustered = event.selected[0].get('features')?.length > 1;

            if (
                isClustered
                && this.map.getView().getZoom() == this.map.getView().getMaxZoom()
                && this.overviewPopup
            ) {
                const list: HTMLUListElement = document.createElement('ul');
                list.className = 'mdc-list';
                this.createListItemsFromPropertyFeatures(event.selected[0].get('features'))
                .forEach((item: HTMLElement) => list.appendChild(item));
                this.setOverviewPopupContent(list);
                this.overviewPopup.setPosition(event.mapBrowserEvent.coordinate);
            } else if (isClustered) {
                this.zoomToFit(event.selected[0].get('features'));
            } else {
                const selectedFeature: Feature = event.selected[0].get('features') ? event.selected[0].get('features')[0] : event.selected[0];
                const href = selectedFeature.get('href');

                if (href) {
                    window.location.href = href;
                }
            }

            this.selectInteraction.getFeatures().clear();
        });
    }

    public highlightBusinessLocationOnClick() {
        this.selectInteraction.on('select', (event: SelectEvent): void => {
            if (!event.selected) return;
            const selectedFeature: Feature = event.selected[0].get('features') ? event.selected[0]?.get('features')[0] : event.selected[0];
            if (selectedFeature.get('locationCategory') !== undefined) {
                selectedFeature.setStyle(this.getBusinessLocationHighlightStyle);
            }
        });
        this.addClickEvent(() => this.getBusinessLocationFeatures().forEach((feature: Feature) => {
            feature.setStyle(this.getBusinessLocationStyle);
        }));
    }

    public createListItemsFromPropertyFeatures(features: Feature<any>[]): HTMLLIElement[] {
        return features.map((feature: Feature<any>): HTMLLIElement => {
            const name = feature.get('name');
            let usage = this.getUsageFromFeature(feature);
            const address = this.getAddressInfoFromFeature(feature);
            const href = feature.get('href');
            const listItem = document.createElement('li');

            listItem.className = 'mdc-list-item d-flex flex-column';
            listItem.innerHTML = `<b class="text-primary">${name}</b><span>${usage}</span><span>${address}</span>`;
            listItem.setAttribute('tabindex', '0');
            listItem.dataset.mdcDialogAction = name;
            listItem.addEventListener('click', () => window.location = href);

            return listItem;
        });
    }

    public getAddressInfoFromFeature(feature: Feature<any>) {
        const address = feature.get('address') as Address;
        if (address === undefined) return null;
        return address.streetName + ' ' + address.houseNumber + (address.quarterPlace ? ' - ' + address.quarterPlace.placeName : '');
    }

    public getUsageFromFeature(feature: Feature<any>) {
        let usage = this.propertyUsageMapping[feature.get('currentUsageIndustryClassification')?.levelOne];
        if (usage == undefined) usage = this.propertyUsageMapping[feature.get('pastUsageIndustryClassification')?.levelOne];
        if (usage == undefined) usage = this.propertyUsageMapping[feature.get('futureUsageIndustryClassification')?.levelOne];
        if (usage == undefined) usage = this.propertyUsageMapping[(feature.get('propertyMarketingInformation') as MarketingInformation)?.possibleUsageIndustryClassifications[0]?.levelOne];
        if (usage == undefined) usage = this.propertyUsageMapping[0];
        return usage;
    }

    public setOverviewPopupContent(content: HTMLElement): void {
        if (!this.overviewPopup) {
            return;
        }

        this.overviewPopup.getElement().querySelector('#overviewDialog-content').innerHTML = '';
        this.overviewPopup.getElement().querySelector('#overviewDialog-content').appendChild(content);
    }

    public zoomToFit(features: Feature<any>[]): void {
        const extent = boundingExtent(
            features.map((feature: Feature<any>) => feature.getGeometry().getCoordinates())
        );

        this.map.getView().fit(extent, {duration: 1000, padding: [50, 50, 50, 50]});
    }

    public focusFeature(feature: Feature<Point>) {
        this.map.getView().animate({center: feature.getGeometry().getCoordinates(), duration: 600});
    }

    public setMapViewCenterPadding(padding: number[]) {
        this.map.getView().padding = padding;
    }

    public setTranslateInteraction(add: boolean): void {
        if (add) {
            this.map.addInteraction(this.translateInteraction);
        } else {
            this.map.removeInteraction(this.translateInteraction);
        }
    }

    public setModifyInteraction(add: boolean): void {
        if (add) {
            this.map.addInteraction(this.modifyInteraction);
            if (!this.readonlyBusinessLocations) this.map.addInteraction(this.modifyBusinessLocationsInteraction);
        } else {
            this.map.removeInteraction(this.modifyInteraction);
            if (!this.readonlyBusinessLocations) this.map.removeInteraction(this.modifyBusinessLocationsInteraction);
        }
    }

    public setDrawInteraction(add: boolean): void {
        if (add) {
            this.map.addInteraction(this.drawInteraction);
        } else {
            this.map.removeInteraction(this.drawInteraction);
        }
    }

    public createFeature = (event: MapBrowserEvent<any>) => {
            const feature = new Feature({geometry: new Point(this.map.getCoordinateFromPixel(event.pixel)),
            ...this.propertyMappings[0].property});
            this.pointSource.addFeature(feature);
    };

    public setDrawBusinessLocationInteraction(add: boolean, businessLocationId?: number): void {
        if (add) {
            this.businessLocationId = businessLocationId;
            this.map.addInteraction(this.drawBusinessLocationInteraction);
        } else {
            this.map.removeInteraction(this.drawBusinessLocationInteraction);
        }
    }

    public setMeasureInteraction(add: boolean): void {
        if (add) {
            this.map.addInteraction(this.measureInteraction);
            document.body.style.setProperty('cursor', `crosshair`, 'important');
        } else {
            this.map.removeInteraction(this.measureInteraction);
            document.body.style.setProperty('cursor', '');
        }
    }

    public initializeMeasureInteraction(type: Type): void {
        this.createMeasureTooltip();
        this.measureInteraction = new Draw({
            source: this.measureSource,
            type: type,
            style: new Style({
                fill: new Fill({
                    color: 'rgba(0,0,0,0.1)',
                }),
                stroke: new Stroke({
                    color: 'rgba(0,0,0,0.5)',
                    lineDash: [10, 10],
                    width: 1,
                })
            })
        });

        let listener: EventsKey;

        this.measureInteraction.on('drawstart', (event): void => {
            let tooltipCoords: number[];
            const sketch = event.feature;

            listener = sketch.getGeometry().on('change', (event: BaseEvent): void => {
                const geom = event.target;
                let output;

                if (geom instanceof Polygon) {
                    output = this.formatArea(geom);
                    tooltipCoords = geom.getInteriorPoint().getCoordinates();
                } else if (geom instanceof LineString) {
                    output = this.formatLength(geom);
                    tooltipCoords = geom.getLastCoordinate();
                }

                this.measureTooltip.getElement().innerHTML = `<span>${output}</span>`;
                this.measureTooltip.setPosition(tooltipCoords);
            });
        });

        this.measureInteraction.on('drawend', (event): void => {
            this.measureTooltip.setOffset([0, -7]);
            this.createMeasureTooltip();
            unByKey(listener);
        });
    }

    private createMeasureTooltip(): void {
        const measureTooltipElement = document.createElement('div');
        measureTooltipElement.className = 'mdc-card p-2 flex-row measure-tooltip';

        this.measureTooltip = new Overlay({
            element: measureTooltipElement,
            offset: [0, -15],
            positioning: 'bottom-center',
            stopEvent: false,
            insertFirst: false,
        });

        this.map.addOverlay(this.measureTooltip);
    }

    private formatLength(line: LineString): string {
        const length = getLength(line);
        let output;

        if (length > 100) {
            output = Math.round((length / 1000) * 100) / 100 + ' ' + 'km';
        } else {
            output = Math.round(length * 100) / 100 + ' ' + 'm';
        }

        return output;
    };

    private formatArea(polygon: Polygon): string {
        const area: number = getArea(polygon);
        let output: string;

        if (area > 10000) {
            output = Math.round((area / 1000000) * 100) / 100 + ' ' + 'km<sup>2</sup>';
        } else {
            output = Math.round(area * 100) / 100 + ' ' + 'm<sup>2</sup>';
        }

        return output;
    };

    public removeFeatures(featuresToRemove: Feature<any>[]): void {
        this.map.getAllLayers().forEach((layer): void => {
            const source = layer.getSource();

            if (!(source instanceof VectorSource)) {
                return;
            }

            featuresToRemove.forEach(featureToRemove => source.removeFeature(featureToRemove));
        });
    }

    public addClickEvent(callback: Function): void {
        this.map.on('click', (event: MapBrowserEvent<any>): void => callback(event));
    }

    public removeClickEvent(callback: Function): void {
        this.map.un('click', (event: MapBrowserEvent<any>): void => callback(event));
    }

    public getFeaturesAtPixel(pixel: Pixel): FeatureLike[] {
        return this.map.getFeaturesAtPixel(pixel);
    }

    public static toLonLat(coordinate: Coordinate, projection?: ProjectionLike): Coordinate {
        return toLonLat(coordinate, projection);
    }

    public static fromLonLat(coordinate: Coordinate, projection?: ProjectionLike): Coordinate {
        return fromLonLat(coordinate, projection);
    }

    public getPointFeatures(): Feature<Point>[] {
        return this.pointSource.getFeatures();
    }

    public getPolygonFeatures(): Feature<Polygon>[] {
        return this.polygonSource.getFeatures();
    }

    public getBusinessLocationFeatures(): Feature<Polygon>[] {
        return this.businessLocationSource.getFeatures();
    }

    public getEarlyWarningSystemFeatures(): Feature<Point>[] {
        return this.earlyWarningSystemPinSource.getFeatures();
    }

    public getDrawInteraction(): Draw {
        return this.drawInteraction;
    }

    public getTranslateInteraction(): Translate {
        return this.translateInteraction;
    }

    public getPropertyMappings(): PropertyMapping[] {
        return this.propertyMappings;
    }

    clearAllFeatureSources() {
        this.polygonSource.clear();
        this.pointSource.clear();
        this.earlyWarningSystemPinSource.clear();
    }

    updateBuildingUnitPoint = async (feature: Feature<Point>, propertyId: number): Promise<void> => {
        const point: Coordinate = MapApi.toLonLat(feature.getGeometry().getCoordinates());
        const response = await Axios.put('/api/property-vacancy-management/building-units/' + propertyId, {
            longitude: point[0],
            latitude: point[1]
        });
    };

    updateBuildingUnitPolygon = async (feature: Feature<Polygon> = null, propertyId: number): Promise<void> => {
        if (feature === null) {
            const response = await Axios.put('/api/property-vacancy-management/building-units/' + propertyId, {
                polygonCoordinates: null
            });
            return;
        }
        const polygonCoordinates = feature.getGeometry().getCoordinates();
        const lonLatCoords = polygonCoordinates.map(rings => {
            return rings.map((ring) => MapApi.toLonLat(ring));
        });
        const response = await Axios.put('/api/property-vacancy-management/building-units/' + propertyId, {
            polygonCoordinates: lonLatCoords
        });
    };

    initializeBusinessLocations = async () => {
        const response = await Axios.get<BusinessLocationsApiResponse[]>(`/api/business-location-area/business-location-areas`);
        const businessLocations = response.data;
        this.businessLocationSource.clear();
        businessLocations.forEach(location => {

            let points: number[][] = [];
            location.geolocationPolygon.polygon.rings.forEach((ring): void => {
                ring.points.forEach((point): void => {
                    points.push(MapApi.fromLonLat([point.longitude, point.latitude]));
                });
            });

            const businessLocationFeature = new Feature({
                geometry: new Polygon([points]),
                locationCategory: location.locationCategory,
                businessLocationId: location.id
            });

            this.businessLocationSource.addFeature(businessLocationFeature);
        });
    };

    initializeScoredBusinessLocations = async () => {
        const businessLocationResponse = await Axios.get<BusinessLocationsApiResponse[]>(`/api/business-location-area/business-location-areas`);
        const scoredBusinessLocationsResponse = await Axios.get<any[]>(`/api/early-warning-system/business-location-area-scorings`);
        this.scoredBusinessLocationSource.clear();
        scoredBusinessLocationsResponse.data.forEach(scoredLocation => {

            const location = businessLocationResponse.data.find(location => scoredLocation.businessLocationAreaId === location.id);
            let points: number[][] = [];
            location.geolocationPolygon.polygon.rings.forEach((ring): void => {
                ring.points.forEach((point): void => {
                    points.push(MapApi.fromLonLat([point.longitude, point.latitude]));
                });
            });
            const pressureClassification = scoredLocation.pressureClassification;

            const businessLocationFeature = new Feature({
                geometry: new Polygon([points]),
                locationCategory: location.locationCategory,
                businessLocationId: location.id,
                pressureClassification,
            });

            this.scoredBusinessLocationSource.addFeature(businessLocationFeature);
        });
    };

    postBusinessLocationPolygon = async (businessLocationFeature: Feature<Polygon>) => {
        const locationCategory = businessLocationFeature.get('locationCategory');
        const polygonCoordinates = businessLocationFeature.getGeometry().getCoordinates();
        const lonLatCoords = polygonCoordinates.map(rings => {
            return rings.map((ring) => MapApi.toLonLat(ring));
        });

        const response = await Axios.post(`/api/business-location-area/business-location-areas`, {
            locationCategory,
            polygonCoordinates: lonLatCoords
        });
    };

    updateBusinessLocationPolygon = async (businessLocationFeature: Feature<Polygon>) => {
        const businessLocationId = businessLocationFeature.get('businessLocationId');
        const locationCategory = businessLocationFeature.get('locationCategory');
        const polygonCoordinates = businessLocationFeature.getGeometry().getCoordinates();
        const lonLatCoords = polygonCoordinates.map(rings => {
            return rings.map((ring) => MapApi.toLonLat(ring));
        });

        const response = await Axios.put(`/api/business-location-area/business-location-areas/${businessLocationId}`, {
            locationCategory,
            polygonCoordinates: lonLatCoords
        });
    };

    deletePolygonById = async (buildingUnitId: number, polygonId: number) => {
        const response = await Axios.delete(`/api/property-vacancy-management/building-units/${buildingUnitId}/geolocation-polygons/${polygonId}`);
    };

    deleteBusinessLocationById = async (businessLocationId: number) => {
        const response = await Axios.delete(`/api/business-location-area/business-location-areas/${businessLocationId}`);
    };
}

export { MapApi, PropertyMapping };
