import { Address, MarketingInformation, NaceClassification } from '../Property/PropertyApiResponse';

interface Property {
    id: number;
    internalNumber: string;
    name: string;
    currentUsageIndustryClassification: NaceClassification;
    pastUsageIndustryClassification: NaceClassification;
    futureUsageIndustryClassification: NaceClassification;
    objectBecomesEmpty: boolean;
    objectIsEmpty: boolean;
    reuseAgreed: boolean;
    objectBecomesEmptyFrom: string;
    objectIsEmptySince: string;
    reuseFrom: string;
    keyProperty: boolean;
    inFloors: string;
    address: Address;
    href: string;
    latitude: number;
    longitude: number;
    polygonId: number;
    polygonCoordinates: number[][][];
    propertyMarketingInformation: MarketingInformation;
    areaSize: string;
    barrierFreeAccess: number;
    shopWindowFrontWidth: string;
    assignedToAccountUser: { id: number, fullName: string };
    updatedAt: string;
    createdAt: string;
}

interface ScoredProperty {
    latitude: number;
    longitude: number;
    starsPercentile: number;
    numberOfReviews: number;
    localSearchPositionPercentile: number;
    potentialReachHouseholdsPercentile: number;
    vacancyRatePercentile: number;
    purchasingPowerLevelZScore: number;
    centralityLevelZScore: number;
    overallAttractivenessZScore: number;
    attractivenessDevelopmentZScore: number;
    npsZScore: number;
    populationGrowthZScore: number;
    industryDevelopmentLongtermPercentile: number;
    industryDevelopmentShorttermPercentile: number;
    specialtyTradeShareDevelopment: number;
    onlineShareDevelopmentPercentile: number;
    ratioStationaryOnlinePercentile: number;
    buildingUnitEnriched: {
        propertyUsersIndustryClassificationLevelOne: number;
        propertyUsersIndustryClassificationLevelTwo: number;
        objectIsEmpty: number;
        businessLocationAreaId: number;
    }
}

export { Property, ScoredProperty };
