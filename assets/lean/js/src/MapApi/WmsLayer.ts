interface WmsLayer {

    id?: number;
    url: string;
    parameter: {};
    title: string;
    sortNumber: number;

}

export { WmsLayer };
