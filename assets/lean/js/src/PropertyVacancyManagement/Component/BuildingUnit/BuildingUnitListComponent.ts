import { ListComponent } from '../../../Component/ListComponent';
import { MDCTooltip } from '@material/tooltip';

class BuildingUnitListComponent extends ListComponent {

    private readonly mdcTooltips: MDCTooltip[];

    constructor(idPrefix: string) {
        super(idPrefix);

        this.mdcTooltips = [];

        this.mdcDataTableContainer.querySelectorAll('tr.mdc-data-table__row[data-row-id]').forEach((tableRowElement: HTMLTableRowElement): void => {
            if (tableRowElement.querySelector('#building-unit-' + tableRowElement.dataset.rowId + '-status-mdc-tooltip') !== null) {
                this.mdcTooltips.push(new MDCTooltip(tableRowElement.querySelector('#building-unit-' + tableRowElement.dataset.rowId + '-status-mdc-tooltip')));
            }

            if (tableRowElement.querySelector('#building-unit-' + tableRowElement.dataset.rowId + '-name-mdc-tooltip') !== null) {
                this.mdcTooltips.push(new MDCTooltip(tableRowElement.querySelector('#building-unit-' + tableRowElement.dataset.rowId + '-name-mdc-tooltip')));
            }

            if (tableRowElement.querySelector('#building-unit-' + tableRowElement.dataset.rowId + '-type-mdc-tooltip') !== null) {
                this.mdcTooltips.push(new MDCTooltip(tableRowElement.querySelector('#building-unit-' + tableRowElement.dataset.rowId + '-type-mdc-tooltip')));
            }

            if (tableRowElement.querySelector('#building-unit-' + tableRowElement.dataset.rowId + '-accountUser-mdc-tooltip') !== null) {
                this.mdcTooltips.push(new MDCTooltip(tableRowElement.querySelector('#building-unit-' + tableRowElement.dataset.rowId + '-accountUser-mdc-tooltip')));
            }
        });
    }

}

export { BuildingUnitListComponent };
