import { CheckboxFieldComponent } from '../../../Component/CheckboxFieldComponent';
import Axios from 'axios';

class BuildingUnitFeedbackComponent {

    private readonly feedbackCheckboxFields: CheckboxFieldComponent[];

    constructor(idPrefix: string) {
        this.feedbackCheckboxFields = [];

        const buildingUnitId: number = parseInt((<HTMLDivElement>document.querySelector('#building_unit_feedback_container')).dataset.buildingUnitId);

        document.querySelectorAll('div.mdc-form-field[id^=' + idPrefix + 'feedback_]').forEach((value: HTMLDivElement) => {
            const matches: string[] = value.id.match('^' + idPrefix + 'feedback_(\\d{1,11})_mdc_formfield$');

            if (matches === null || matches.length !== 2) {
                return;
            }

            const CheckboxField: CheckboxFieldComponent = new CheckboxFieldComponent(value);

            CheckboxField.mdcFormField.listen('change', async (event) => {
                if (CheckboxField.isChecked() === false) {
                    try {
                        await BuildingUnitFeedbackComponent.deleteBuildingUnitFeedback(
                            buildingUnitId,
                            parseInt(CheckboxField.mdcCheckbox.value)
                        );
                    } catch (error) {
                        CheckboxField.setChecked(true);
                    }
                } else {
                    try {
                        await BuildingUnitFeedbackComponent.postBuildingUnitFeedback(
                            buildingUnitId,
                            parseInt(CheckboxField.mdcCheckbox.value)
                        );
                    } catch (error) {
                        CheckboxField.setChecked(false);
                    }
                }
            });

            this.feedbackCheckboxFields.push(CheckboxField);
        });
    }

    private static async postBuildingUnitFeedback(buildingUnitId: number, feedbackId: number) {
        return await Axios.post('/api/property-vacancy-management/building-units/' + buildingUnitId + '/feedbacks', {
            feedbackId: feedbackId
        });
    }

    private static async deleteBuildingUnitFeedback(buildingUnitId: number, feedbackId: number) {
        return await Axios.delete('/api/property-vacancy-management/building-units/' + buildingUnitId + '/feedbacks/' + feedbackId);
    }

}

export { BuildingUnitFeedbackComponent };
