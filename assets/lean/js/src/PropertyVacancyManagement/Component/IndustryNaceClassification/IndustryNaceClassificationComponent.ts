import '../../../../../styles/property/industry_nace_classification_component.scss';
import { SelectComponent } from '../../../Component/SelectComponent';
import { TextFieldComponent } from '../../../Component/TextFieldComponent';
import Axios, {AxiosResponse} from 'axios';

interface IndustryClassification {
    id: number;
    levelOne: number|null;
    levelThree: number|null;
    levelTwo: number|null;
    naceClassifications?: NaceClassification[];
    name: string;
    parent: IndustryClassification|null;
}

interface NaceClassification {
    code: string;
    id: number;
    level: number;
    name: string;
    industryClassifications?: IndustryClassification[];
    parent?: NaceClassification|null;
}

interface PreparedSearchQueryData {
    naceName: string;
    naceCode: string;
    industryName: string;
    industryCodeLevelOne: string;
    industryCodeLevelTwo: string;
    industryCodeLevelThree: string;
}

class IndustryNaceClassificationComponent {

    private readonly industryUsageSelect: SelectComponent;
    private readonly industryAreaSelect: SelectComponent;
    private readonly industryDetailsSelect: SelectComponent;
    private readonly naceSectionSelect: SelectComponent;
    private readonly naceDepartmentSelect: SelectComponent;
    private readonly naceGroupSelect: SelectComponent;
    private readonly naceClassSelect: SelectComponent;
    private readonly naceSubclassSelect: SelectComponent;
    private readonly industryNaceSearchQueryField: TextFieldComponent;
    private readonly industryNaceSearchAutocompleteContainer: HTMLDivElement;
    private readonly industryClassificationIdField: HTMLInputElement;
    private readonly naceClassificationIdField: HTMLInputElement;
    private readonly industryNaceSpinner: HTMLDivElement;
    private readonly industryDoneAlert: HTMLDivElement;
    private readonly naceDoneAlert: HTMLDivElement;
    private isSettingSelectWithFetching: boolean;
    private isResetting: boolean;

    constructor(idPrefix: string) {
        idPrefix = idPrefix + 'industryNaceClassification_';

        this.industryUsageSelect = new SelectComponent(document.querySelector('#' + idPrefix + 'industryUsage_mdc_select'));
        this.industryAreaSelect = new SelectComponent(document.querySelector('#' + idPrefix + 'industryArea_mdc_select'), {clearButton: true});
        this.industryDetailsSelect = new SelectComponent(document.querySelector('#' + idPrefix + 'industryDetails_mdc_select'), {clearButton: true});
        this.naceSectionSelect = new SelectComponent(document.querySelector('#' + idPrefix + 'naceSection_mdc_select'), {clearButton: true});
        this.naceDepartmentSelect = new SelectComponent(document.querySelector('#' + idPrefix + 'naceDepartment_mdc_select'), {clearButton: true});
        this.naceGroupSelect = new SelectComponent(document.querySelector('#' + idPrefix + 'naceGroup_mdc_select'), {clearButton: true});
        this.naceClassSelect = new SelectComponent(document.querySelector('#' + idPrefix + 'naceClass_mdc_select'), {clearButton: true});
        this.naceSubclassSelect = new SelectComponent(document.querySelector('#' + idPrefix + 'naceSubclass_mdc_select'), {clearButton: true});
        this.industryNaceSearchQueryField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'industryNaceSearchQuery_mdc_text_field'));
        this.industryNaceSearchAutocompleteContainer = document.querySelector('#industry-nace-classification-searchQuery .autocomplete-container');
        this.industryClassificationIdField = document.querySelector('#' + idPrefix + 'industryClassificationId') as HTMLInputElement
        this.naceClassificationIdField = document.querySelector('#' + idPrefix + 'naceClassificationId') as HTMLInputElement
        this.industryNaceSpinner = document.querySelector('.industry-nace-classification-spinner');
        this.industryDoneAlert = document.querySelector('.industry-classification-done-alert');
        this.naceDoneAlert = document.querySelector('.nace-classification-done-alert');
        this.isSettingSelectWithFetching = true;
        this.isResetting = false;

        this.industryUsageSelect.mdcSelect.required = true;

        this.strongNumbersForOptionsInIndustryUsageSelect();
        this.initIndustryAndNaceClassificationIfExist();
        this.initClickAwayToHideIndustryNaceSearchAutocompleteContainer();

        this.industryUsageSelect.mdcSelect.listen('MDCSelect:change', () => this.handleIndustrySelectChange(this.industryUsageSelect, 1));
        this.industryAreaSelect.mdcSelect.listen('MDCSelect:change', () => this.handleIndustrySelectChange(this.industryAreaSelect, 2));
        this.industryDetailsSelect.mdcSelect.listen('MDCSelect:change', () => this.handleIndustrySelectChange(this.industryDetailsSelect, 3));
        this.naceSectionSelect.mdcSelect.listen('MDCSelect:change', () => this.handleNaceSelectChange(this.naceSectionSelect, 2));
        this.naceDepartmentSelect.mdcSelect.listen('MDCSelect:change', () => this.handleNaceSelectChange(this.naceDepartmentSelect, 3));
        this.naceGroupSelect.mdcSelect.listen('MDCSelect:change', () => this.handleNaceSelectChange(this.naceGroupSelect, 4));
        this.naceClassSelect.mdcSelect.listen('MDCSelect:change', () => this.handleNaceSelectChange(this.naceClassSelect, 5));
        this.naceSubclassSelect.mdcSelect.listen('MDCSelect:change', () => this.handleNaceSelectChange(this.naceSubclassSelect, 6));
        this.listenToTextFieldInputChange(() => this.handleIndustryNaceSearchQueryInput(this.industryNaceSearchQueryField.mdcTextField.value), 1000);
    }

    private listenToTextFieldInputChange(handleInputFuncAfterDoneTyping: Function, doneTypingInterval: number = 2000): void {
        let typingTimer: number;

        this.industryNaceSearchQueryField.mdcTextField.listen('keydown', () =>  clearTimeout(typingTimer));

        this.industryNaceSearchQueryField.mdcTextField.listen('keyup', (event) => {
            let inputField = event.currentTarget as HTMLInputElement;

            if (inputField.value === '') {
                typingTimer = setTimeout(handleInputFuncAfterDoneTyping, 0);
            } else {
                clearTimeout(typingTimer);
                typingTimer = setTimeout(handleInputFuncAfterDoneTyping, doneTypingInterval);
            }
        });
    }

    private initClickAwayToHideIndustryNaceSearchAutocompleteContainer(): void {
        document.addEventListener('click', (event: MouseEvent): void => {
            const domElement: HTMLElement = event.target as HTMLElement;
            if (this.industryNaceSearchAutocompleteContainer.parentElement !== domElement
                && this.industryNaceSearchAutocompleteContainer.parentElement.contains(domElement) === false
                || domElement.classList.contains('autocomplete-item') === true
                || domElement.classList.contains('autocompleteItem-code-span') === true
                || domElement.classList.contains('autocompleteItem-text-span') === true
            ) {
                this.industryNaceSearchAutocompleteContainer.classList.add('d-none');
            } else {
                this.industryNaceSearchAutocompleteContainer.classList.remove('d-none');
            }
        });
    }

    private async handleIndustryNaceSearchQueryInput(query: string) {
        const searchQuerySpinner = this.industryNaceSearchAutocompleteContainer.querySelector('.industry-nace-searchQuery-spinner');
        const autocompleteListElement = this.industryNaceSearchAutocompleteContainer.querySelector('.autocomplete-list');

        if (query === '') {
            this.industryNaceSearchAutocompleteContainer.classList.add('d-none');
        } else {
            this.industryNaceSearchAutocompleteContainer.classList.remove('d-none');

            searchQuerySpinner.classList.remove('d-none');
            autocompleteListElement.innerHTML = '';

            const preparedSearchQueryData: PreparedSearchQueryData = this.prepareSearchQueryForFetchingAutocompleteData(query);
            this.disableAllSelectFields();
            await this.fetchingAutocompleteDataForIndustryNaceSearchQuery(preparedSearchQueryData);
            this.enableAllSelectFields();

            searchQuerySpinner.classList.add('d-none');

            if (autocompleteListElement.querySelectorAll('.autocomplete-item').length < 1) {
                autocompleteListElement.innerHTML = '<div class="text-center w-100">Leider konnten wir keine Vorschläge finden.<br> Bitte geben Sie einen neuen Suchbegriff ein.</div>';
            }
        }
    }

    private prepareSearchQueryForFetchingAutocompleteData(query: string): PreparedSearchQueryData {
        const preparedSearchQueryData: PreparedSearchQueryData = {
            naceName: null,
            naceCode: null,
            industryName: null,
            industryCodeLevelOne: null,
            industryCodeLevelTwo: null,
            industryCodeLevelThree: null,
        }

        query = this.replaceUmlautsToNoneUmlauts(query);

        let extractedTextFromQuery: string = this.extractCodeFromQuery(query, 'text').trim();

        if (extractedTextFromQuery != null && extractedTextFromQuery != '') {
            extractedTextFromQuery = this.replaceUmlautsToNoneUmlauts(extractedTextFromQuery, true);
            preparedSearchQueryData.naceName = extractedTextFromQuery;
            preparedSearchQueryData.industryName = extractedTextFromQuery;
        }

        const hasNaceCode: boolean = this.checkQueryForClassificationCode(query, 'nace');

        if (hasNaceCode === true) {
            preparedSearchQueryData.naceCode = this.extractCodeFromQuery(query, 'nace').trim();
            preparedSearchQueryData.naceName = this.extractNameFromQuery(query, preparedSearchQueryData.naceCode);
            preparedSearchQueryData.naceCode.toUpperCase();

            if (preparedSearchQueryData.naceName != null) {
                preparedSearchQueryData.naceName = this.replaceUmlautsToNoneUmlauts(preparedSearchQueryData.naceName, true);
            }
        }

        const hasIndustryCode: boolean = this.checkQueryForClassificationCode(query, 'industry');
        let industryCode: string;

        if (hasIndustryCode === true) {
            industryCode = this.extractCodeFromQuery(query, 'industry');
            preparedSearchQueryData.industryCodeLevelOne = industryCode.split('.')[0] === undefined ? null : industryCode.split('.')[0];
            preparedSearchQueryData.industryCodeLevelTwo = industryCode.split('.')[1] === undefined ? null : industryCode.split('.')[1];
            preparedSearchQueryData.industryCodeLevelThree = industryCode.split('.')[2] === undefined ? null : industryCode.split('.')[2];
            preparedSearchQueryData.industryName = this.extractNameFromQuery(query, industryCode);
        }

        if (preparedSearchQueryData.industryName != null) {
            preparedSearchQueryData.industryName = this.replaceUmlautsToNoneUmlauts(preparedSearchQueryData.industryName, true);
        } else {
            preparedSearchQueryData.industryName =  preparedSearchQueryData.naceName;
        }

        return preparedSearchQueryData;
    }

    private extractNameFromQuery(query: string, classificationCode: string): string {
        let querySplitLength = query.split(classificationCode).length;
        let name: string = query.split(classificationCode)[querySplitLength - 1].trim();

        if (querySplitLength > 2) {
            name = classificationCode + name;
        }

        if (name != '') {
            name = this.extractCodeFromQuery(name, 'text');
        }

        if (name === '') {
            name = null;
        }

        return name;
    }

    private replaceUmlautsToNoneUmlauts(string: string, isReverseDirection: boolean = false): string {
        let result: string;

        if (isReverseDirection) {
            result = string.replace('ae', 'ä');
            result = result.replace('oe', 'ö');
            result = result.replace('ue', 'ü');
        } else {
            result = string.replace('ä', 'ae');
            result = result.replace('ö', 'oe');
            result = result.replace('ü', 'ue');
        }

        return result;
    }

    private checkQueryForClassificationCode(query: string, target: string): boolean {
        let regex: RegExp;

        if (target === 'nace') {
            regex = /\b(\d\d?)(\.\d\d?)?(\.\d\d?)?\b|\b[a-zA-Z](\s)*\b/;
        } else {
            regex = /\b([0-1]?\d)(\.\d\d?)?(\.\d\d?)?\b/;
        }

        return regex.test(query);
    }

    private extractCodeFromQuery(query: string, target: string): string {
        let regex: RegExp;

        if (target === 'nace') {
            regex = /\b(\d\d?)(\.\d\d?)?(\.\d\d?)?\b|\b[a-zA-Z](\s)*\b/;
        } else if (target === 'industry') {
            regex = /\b([0-1]?\d)(\.\d\d?)?(\.\d\d?)?\b/;
        } else {
            regex = /\b[a-zA-Z\.\,\ \-\/\&]*\b/;
        }

        if (regex.exec(query) === null) {
            return null;
        }

        return regex.exec(query)[0];
    }

    private async fetchingAutocompleteDataForIndustryNaceSearchQuery(preparedSearchQueryData: PreparedSearchQueryData) {
        try {
            let naceCodeSearchAutocompleteResponse: AxiosResponse = null;
            let industryCodeSearchAutocompleteResponse: AxiosResponse = null;
            let naceNameSearchAutocompleteResponse: AxiosResponse = null;
            let industryNameSearchAutocompleteResponse: AxiosResponse = null;

            if (preparedSearchQueryData.naceCode != null) {
                naceCodeSearchAutocompleteResponse = await Axios.get('/api/classifications/nace-classifications?code=' + preparedSearchQueryData.naceCode);
            }

            if (preparedSearchQueryData.industryCodeLevelOne != null) {
                industryCodeSearchAutocompleteResponse = await Axios.get('/api/classifications/industry-classifications?levelOne=' + preparedSearchQueryData.industryCodeLevelOne + '&levelTwo' + preparedSearchQueryData.industryCodeLevelTwo + '&levelThree' + preparedSearchQueryData.industryCodeLevelThree);
            }

            if (preparedSearchQueryData.naceName != null && preparedSearchQueryData.naceName.length > 2) {
                naceNameSearchAutocompleteResponse = await Axios.get('/api/classifications/nace-classifications?name=' + preparedSearchQueryData.naceName);
            }

            if (preparedSearchQueryData.industryName != null && preparedSearchQueryData.industryName.length > 2) {
                industryNameSearchAutocompleteResponse = await Axios.get('/api/classifications/industry-classifications?name=' + preparedSearchQueryData.industryName);
            }

            if (naceCodeSearchAutocompleteResponse != null) {
                this.addAutocompleteListToContainer(naceCodeSearchAutocompleteResponse.data);
            }

            if (industryCodeSearchAutocompleteResponse != null) {
                this.addAutocompleteListToContainer(industryCodeSearchAutocompleteResponse.data);
            }

            if (naceNameSearchAutocompleteResponse != null) {
                this.addAutocompleteListToContainer(naceNameSearchAutocompleteResponse.data);
            }

            if (industryNameSearchAutocompleteResponse != null) {
                this.addAutocompleteListToContainer(industryNameSearchAutocompleteResponse.data);
            }
        } catch (err) {
        }
    }

    private addAutocompleteListToContainer(autocompleteList: NaceClassification[]|IndustryClassification[]): void {
        const autocompleteListElement = this.industryNaceSearchAutocompleteContainer.querySelector('.autocomplete-list');

        for (const autocompleteItem of autocompleteList) {
            const divTag: HTMLElement = document.createElement('DIV');
            const classificationInformationTag = document.createElement('span');

            divTag.classList.add('autocomplete-item');
            divTag.classList.add('d-md-flex');
            divTag.classList.add('justify-content-between');
            divTag.dataset.id = autocompleteItem.id.toString();

            if (this.instanceOfNaceClassification(autocompleteItem) === true) {
                const autocompleteNaceItem: NaceClassification = autocompleteItem as NaceClassification;
                divTag.innerHTML = '<span class="autocompleteItem-code-span" style="width:120px; font-weight: bold;">' + autocompleteNaceItem.code + '</span><span class="autocompleteItem-text-span w-100 px-2 text-start">' + autocompleteNaceItem.name + '</span>';
                divTag.dataset.type = 'NaceClassification';

                classificationInformationTag.classList.add('d-none');
                classificationInformationTag.classList.add('d-md-flex');
                classificationInformationTag.classList.add('classificationInformationTag');
                classificationInformationTag.textContent = 'Nace Klassifikation';
                divTag.appendChild(classificationInformationTag);
            } else {
                const autocompleteIndustryItem: IndustryClassification = autocompleteItem as IndustryClassification;
                divTag.innerHTML = '<span class="autocompleteItem-code-span" style="width:105px; font-weight: bold;">' + this.getIndustryCode(autocompleteIndustryItem) + '</span><span class="autocompleteItem-text-span w-100 px-2 text-start">' + autocompleteIndustryItem.name + '</span>';
                divTag.dataset.type = 'IndustryClassification';
            }

            divTag.addEventListener('click', (event: MouseEvent) => this.handleAutocompleteItemClick(event.currentTarget as HTMLDivElement));
            autocompleteListElement.appendChild(divTag);
        }
    }

    private instanceOfNaceClassification(object: any): object is NaceClassification {
        return 'code' in object;
    }

    private async handleAutocompleteItemClick(autocompleteResultItemElement: HTMLDivElement) {
        this.industryNaceSearchAutocompleteContainer.classList.add('d-none');
        this.disableAllSelectFields();

        this.resetAndHideToRootLevel();

        if (autocompleteResultItemElement.dataset.type === 'NaceClassification') {
            await this.fetchAndSetNaceClassificationById(autocompleteResultItemElement.dataset.id, true);
        } else {
            await this.fetchAndSetIndustryClassificationById(autocompleteResultItemElement.dataset.id, true);
        }

        this.enableAllSelectFields();
    }

    private resetAndHideToRootLevel(): void {
        this.industryDoneAlert.classList.add('d-none');
        this.naceDoneAlert.classList.add('d-none');
        this.naceClassificationIdField.value = '';
        this.industryClassificationIdField.value = '';
        this.resetNaceClassificationToLevel(1);
        this.showOrHideIndustryClassificationByLevel(1);
        this.showOrHideNaceClassificationByLevel(1);
    }

    private strongNumbersForOptionsInIndustryUsageSelect(): void {
        this.industryUsageSelect.mdcSelect.root.querySelectorAll('.mdc-list .mdc-list-item__text').forEach((element) => {
            const number: string = '<strong>' + element.textContent.split('!')[0] + '</strong>';
            const text: string = element.textContent.split('!')[1];
            element.innerHTML = number + ' ' + text;
        });

        this.isSettingSelectWithFetching = false;
        this.industryUsageSelect.mdcSelect.value = this.industryUsageSelect.mdcSelect.value;
        this.isSettingSelectWithFetching = true;
    }

    private async initIndustryAndNaceClassificationIfExist() {
        this.disableAllSelectFields();

        if (this.industryClassificationIdField.value != '') {
            await this.fetchAndSetIndustryClassificationById(this.industryClassificationIdField.value);
        }

        if (this.naceClassificationIdField.value != '') {
            await this.fetchLevelOneNaceClassificationsAndFillSelectOptions();
            await this.fetchAndSetNaceClassificationById(this.naceClassificationIdField.value);
        } else if (this.industryUsageSelect.mdcSelect.value != '') {
            await this.fetchLevelOneNaceClassificationsAndFillSelectOptions();
            this.showOrHideNaceClassificationByLevel(1);
        }

        if (this.industryUsageSelect.mdcSelect.value != '') {
            document.querySelector('#industry-nace-classification-searchQuery').parentElement.classList.remove('d-none');
        }

        this.enableAllSelectFields();
    }

    private async fetchAndSetIndustryClassificationById(id: string, checkIfNaceConnectionExist: boolean = false) {
        try {
            const industryClassificationResponse: AxiosResponse = await Axios.get('/api/classifications/industry-classifications/' + id);
            await this.fillAndSetAllIndustryClassificationLevelsByIndustryElement(industryClassificationResponse.data)

            if (checkIfNaceConnectionExist === true) {
                if (industryClassificationResponse.data.naceClassifications.length === 1) {
                    await this.fillAndSetAllNaceClassificationLevelsByNaceElement(industryClassificationResponse.data.naceClassifications[0]);
                } else if (industryClassificationResponse.data.naceClassifications.length > 1) {
                    await this.fillAndSetNaceClassificationLevelsByLowestCommonDenominator(industryClassificationResponse.data.naceClassifications);
                }
            }
        } catch (err) {
        }
    }

    private async fetchAndSetNaceClassificationById(id: string, checkIfIndustryConnectionExist: boolean = false) {
        try {
            const naceClassificationResponse: AxiosResponse = await Axios.get('/api/classifications/nace-classifications/' + id);
            await this.fillAndSetAllNaceClassificationLevelsByNaceElement(naceClassificationResponse.data)

            if (checkIfIndustryConnectionExist === true) {
                if (naceClassificationResponse.data.industryClassifications.length === 1) {
                    await this.fetchAndSetLevelOneNaceClassifications(this.naceSectionSelect.mdcSelect.value);
                    await this.fillAndSetAllIndustryClassificationLevelsByIndustryElement(naceClassificationResponse.data.industryClassifications[0]);
                } else if (naceClassificationResponse.data.industryClassifications.length > 1) {
                    await this.fillAndSetIndustryClassificationLevelsByLowestCommonDenominator(naceClassificationResponse.data.industryClassifications);
                }
            }
        } catch (err) {
        }
    }

    private async handleIndustrySelectChange(target: SelectComponent, level: number = 1) {
        if (this.isSettingSelectWithFetching === false || this.isResetting === true) {
            return;
        }

        this.disableAllSelectFields();

        if (level === 1) {
            this.industryDoneAlert.classList.add('d-none');
            this.naceDoneAlert.classList.add('d-none');
            document.querySelector('#industry-nace-classification-searchQuery').parentElement.classList.remove('d-none');
            this.showOrHideIndustryClassificationByLevel(level);
            this.showOrHideNaceClassificationByLevel(level);
            this.naceClassificationIdField.value = '';
            await this.fetchAndSetLevelOneNaceClassifications('');
            this.resetNaceClassificationToLevel(level);
        }

        await this.fetchIndustryClassification(target.mdcSelect.value, level);

        this.enableAllSelectFields();
    }

    private async handleNaceSelectChange(target: SelectComponent, level: number = 1) {
        if (this.isSettingSelectWithFetching === false || this.isResetting === true) {
            return;
        }

        this.disableAllSelectFields();
        await this.fetchNaceClassification(target.mdcSelect.value, level);
        this.enableAllSelectFields();
    }

    private disableAllSelectFields(): void {
        this.industryUsageSelect.mdcSelect.disabled = true;
        this.industryAreaSelect.mdcSelect.disabled = true;
        this.industryDetailsSelect.mdcSelect.disabled = true;
        this.naceSectionSelect.mdcSelect.disabled = true;
        this.naceDepartmentSelect.mdcSelect.disabled = true;
        this.naceGroupSelect.mdcSelect.disabled = true;
        this.naceClassSelect.mdcSelect.disabled = true;
        this.naceSubclassSelect.mdcSelect.disabled = true;
        this.industryNaceSearchQueryField.mdcTextField.disabled = true;
        this.industryNaceSpinner.classList.remove('d-none');
    }

    private enableAllSelectFields(): void {
        this.industryUsageSelect.mdcSelect.disabled = false;
        this.industryAreaSelect.mdcSelect.disabled = false;
        this.industryDetailsSelect.mdcSelect.disabled = false;
        this.naceSectionSelect.mdcSelect.disabled = false;
        this.naceDepartmentSelect.mdcSelect.disabled = false;
        this.naceGroupSelect.mdcSelect.disabled = false;
        this.naceClassSelect.mdcSelect.disabled = false;
        this.naceSubclassSelect.mdcSelect.disabled = false;
        this.industryNaceSearchQueryField.mdcTextField.disabled = false;
        this.industryNaceSpinner.classList.add('d-none');
    }

    private async fetchAndSetLevelOneNaceClassifications(id: string) {
        await this.fetchLevelOneNaceClassificationsAndFillSelectOptions();
        this.isSettingSelectWithFetching = false;
        this.naceSectionSelect.mdcSelect.value = id;
        this.isSettingSelectWithFetching = true;
    }

    private async fetchLevelOneNaceClassificationsAndFillSelectOptions() {
        const naceClassificationResponseList: AxiosResponse = await Axios.get('/api/classifications/nace-classifications?level=1');
        this.fillNaceClassificationSelectOptionsByLevel(naceClassificationResponseList.data, 1);
    }

    private async fetchNaceClassification(id: string = null, level: number = 1, checkIfIndustryConnectionExist: boolean = true) {
        let naceClassificationList: NaceClassification[];

        try {
            this.showOrHideNaceClassificationByLevel(level);
            this.resetNaceClassificationToLevel(level);
            this.naceClassificationIdField.value = id;
            this.naceDoneAlert.classList.add('d-none');

            const naceClassificationResponseList: AxiosResponse = await Axios.get('/api/classifications/nace-classifications/' + id + '/nace-classifications');
            naceClassificationList = naceClassificationResponseList.data;

            if (naceClassificationList.length === 0) {
                this.naceDoneAlert.classList.remove('d-none');
                this.showOrHideNaceClassificationByLevel(level - 1);
            } else {
                this.fillNaceClassificationSelectOptionsByLevel(naceClassificationList, level);
            }

            if (checkIfIndustryConnectionExist === true) {
                const naceClassificationResponse: AxiosResponse = await Axios.get('/api/classifications/nace-classifications/' + id);

                if (naceClassificationResponse.data.industryClassifications.length === 1) {
                    await this.fetchAndSetLevelOneNaceClassifications(this.naceSectionSelect.mdcSelect.value);
                    await this.fillAndSetAllIndustryClassificationLevelsByIndustryElement(naceClassificationResponse.data.industryClassifications[0]);
                } else if (naceClassificationResponse.data.industryClassifications.length > 1) {
                    await this.fillAndSetIndustryClassificationLevelsByLowestCommonDenominator(naceClassificationResponse.data.industryClassifications);
                }
            }
        } catch (err) {
        }
    }

    private async fetchIndustryClassification(id: string, level: number, checkIfNaceConnectionExist: boolean = true) {
        try {
            this.showOrHideIndustryClassificationByLevel(level);
            this.resetIndustryClassificationToLevel(level);
            this.industryClassificationIdField.value = id;
            this.industryDoneAlert.classList.add('d-none');

            const industryClassificationResponseList: AxiosResponse = await Axios.get('/api/classifications/industry-classifications/' + id + '/industry-classifications');

            if (industryClassificationResponseList.data.length === 0) {

                if (level > 1) {
                    this.industryDoneAlert.classList.remove('d-none');
                }

                this.showOrHideIndustryClassificationByLevel(level - 1);
            } else {
                this.fillIndustryClassificationSelectOptionsByLevel(industryClassificationResponseList.data, level);
            }

            if (checkIfNaceConnectionExist === true) {
                const industryClassificationResponse: AxiosResponse = await Axios.get('/api/classifications/industry-classifications/' + id);

                if (industryClassificationResponse.data.naceClassifications.length === 1) {
                    await this.fillAndSetAllNaceClassificationLevelsByNaceElement(industryClassificationResponse.data.naceClassifications[0]);
                } else if (industryClassificationResponse.data.naceClassifications.length > 1) {
                    await this.fillAndSetNaceClassificationLevelsByLowestCommonDenominator(industryClassificationResponse.data.naceClassifications);
                }
            }
        } catch (err) {
        }

        return true;
    }

    private fillIndustryClassificationSelectOptionsByLevel(industryClassifications: IndustryClassification[], level: number): void {
        switch (level) {
            case 1:
                this.industryAreaSelect.mdcSelect.root.querySelector('.mdc-list').innerHTML = '';
                for (let industryClassification of industryClassifications) {
                    this.industryAreaSelect.mdcSelect.root.querySelector('.mdc-list').innerHTML += this.createMDCSelectListItem(industryClassification.id.toString(), industryClassification.name, this.getIndustryCode(industryClassification));
                }
                this.industryAreaSelect.mdcSelect.layoutOptions();
                break;
            case 2:
                this.industryDetailsSelect.mdcSelect.root.querySelector('.mdc-list').innerHTML = '';
                for (let industryClassification of industryClassifications) {
                    this.industryDetailsSelect.mdcSelect.root.querySelector('.mdc-list').innerHTML += this.createMDCSelectListItem(industryClassification.id.toString(), industryClassification.name, this.getIndustryCode(industryClassification));
                }
                this.industryDetailsSelect.mdcSelect.layoutOptions();
                break;
        }
    }

    private fillNaceClassificationSelectOptionsByLevel(naceClassifications: NaceClassification[], level: number): void {
        switch (level) {
            case 1:
                this.naceSectionSelect.mdcSelect.root.querySelector('.mdc-list').innerHTML = '';
                for (let naceClassification of naceClassifications) {
                    this.naceSectionSelect.mdcSelect.root.querySelector('.mdc-list').innerHTML += this.createMDCSelectListItem(naceClassification.id.toString(), naceClassification.name, naceClassification.code);
                }
                this.naceSectionSelect.mdcSelect.layoutOptions();
                break;
            case 2:
                this.naceDepartmentSelect.mdcSelect.root.querySelector('.mdc-list').innerHTML = '';
                for (let naceClassification of naceClassifications) {
                    this.naceDepartmentSelect.mdcSelect.root.querySelector('.mdc-list').innerHTML += this.createMDCSelectListItem(naceClassification.id.toString(), naceClassification.name, naceClassification.code);
                }
                this.naceDepartmentSelect.mdcSelect.layoutOptions();
                break;
            case 3:
                this.naceGroupSelect.mdcSelect.root.querySelector('.mdc-list').innerHTML = '';
                for (let naceClassification of naceClassifications) {
                    this.naceGroupSelect.mdcSelect.root.querySelector('.mdc-list').innerHTML += this.createMDCSelectListItem(naceClassification.id.toString(), naceClassification.name, naceClassification.code);
                }
                this.naceGroupSelect.mdcSelect.layoutOptions();
                break;
            case 4:
                this.naceClassSelect.mdcSelect.root.querySelector('.mdc-list').innerHTML = '';
                for (let naceClassification of naceClassifications) {
                    this.naceClassSelect.mdcSelect.root.querySelector('.mdc-list').innerHTML += this.createMDCSelectListItem(naceClassification.id.toString(), naceClassification.name, naceClassification.code);
                }
                this.naceClassSelect.mdcSelect.layoutOptions();
                break;
            case 5:
                this.naceSubclassSelect.mdcSelect.root.querySelector('.mdc-list').innerHTML = '';
                for (let naceClassification of naceClassifications) {
                    this.naceSubclassSelect.mdcSelect.root.querySelector('.mdc-list').innerHTML += this.createMDCSelectListItem(naceClassification.id.toString(), naceClassification.name, naceClassification.code);
                }
                this.naceSubclassSelect.mdcSelect.layoutOptions();
                break;
        }
    }

    private async fillAndSetNaceClassificationLevelsByLowestCommonDenominator(naceClassifications: NaceClassification[]) {
        let naceClassificationsArray: NaceClassification[] = [];
        let lowestLevel: number = this.getLowestLevelOfNaceList(naceClassifications);

        if (lowestLevel > 1) {
            lowestLevel = lowestLevel - 1;
        }

        for (const naceClassification of naceClassifications) {
            let naceClassificationToBePushed: NaceClassification = this.findNaceElementAtGivenLevel(naceClassification, lowestLevel);
            const index = naceClassificationsArray.findIndex(object => object.id === naceClassificationToBePushed.id);

            if (index === -1) {
                naceClassificationsArray.push(naceClassificationToBePushed);
            }
        }

        if (naceClassificationsArray.length === 1) {
            await this.fillAndSetAllNaceClassificationLevelsByNaceElement(naceClassificationsArray[0]);
        } else {
            if (lowestLevel === 1) {
                this.fillNaceClassificationSelectOptionsByLevel(naceClassificationsArray, lowestLevel);
                this.showOrHideNaceClassificationByLevel(lowestLevel);
                this.resetNaceClassificationToLevel(lowestLevel);
            }
        }
    }

    private findNaceElementAtGivenLevel(naceClassification: NaceClassification, level: number): NaceClassification {
        if (naceClassification.level === level) {
            return naceClassification;
        } else {
            return this.findNaceElementAtGivenLevel(naceClassification.parent, level);
        }
    }

    private getLowestLevelOfNaceList(naceClassifications: NaceClassification[]): number {
        let lowestLevel: number = naceClassifications[0].level;

        for (const naceClassification of naceClassifications) {
            if (lowestLevel > naceClassification.level) {
                lowestLevel = naceClassification.level;
            }
        }

        return lowestLevel;
    };

    private async fillAndSetAllNaceClassificationLevelsByNaceElement(naceClassification: NaceClassification) {
        this.isSettingSelectWithFetching = false;

        if (naceClassification.level === 5) {
            this.naceSectionSelect.mdcSelect.setValue(naceClassification.parent.parent.parent.parent.id.toString());
            await this.fetchNaceClassification(naceClassification.parent.parent.parent.parent.id.toString(), 2, false);
            this.naceDepartmentSelect.mdcSelect.setValue(naceClassification.parent.parent.parent.id.toString());
            await this.fetchNaceClassification(naceClassification.parent.parent.parent.id.toString(), 3, false);
            this.naceGroupSelect.mdcSelect.value = naceClassification.parent.parent.id.toString();
            await this.fetchNaceClassification(naceClassification.parent.parent.id.toString(), 4, false);
            this.naceClassSelect.mdcSelect.value = naceClassification.parent.id.toString();
            await this.fetchNaceClassification(naceClassification.parent.id.toString(), 5, false);
            this.naceSubclassSelect.mdcSelect.value = naceClassification.id.toString();
            this.naceClassificationIdField.value = this.naceSubclassSelect.mdcSelect.value;
            this.naceDoneAlert.classList.remove('d-none');
        } else if (naceClassification.level === 4) {
            this.naceSectionSelect.mdcSelect.value = naceClassification.parent.parent.parent.id.toString();
            await this.fetchNaceClassification(naceClassification.parent.parent.parent.id.toString(), 2, false);
            this.naceDepartmentSelect.mdcSelect.value = naceClassification.parent.parent.id.toString();
            await this.fetchNaceClassification(naceClassification.parent.parent.id.toString(), 3, false);
            this.naceGroupSelect.mdcSelect.value = naceClassification.parent.id.toString();
            await this.fetchNaceClassification(naceClassification.parent.id.toString(), 4, false);
            this.naceClassSelect.mdcSelect.value = naceClassification.id.toString();
            await this.fetchNaceClassification(naceClassification.id.toString(), 5, false);
        } else if (naceClassification.level === 3) {
            this.naceSectionSelect.mdcSelect.value = naceClassification.parent.parent.id.toString();
            await this.fetchNaceClassification(naceClassification.parent.parent.id.toString(), 2, false);
            this.naceDepartmentSelect.mdcSelect.value = naceClassification.parent.id.toString();
            await this.fetchNaceClassification(naceClassification.parent.id.toString(), 3, false);
            this.naceGroupSelect.mdcSelect.value = naceClassification.id.toString();
            await this.fetchNaceClassification(naceClassification.id.toString(), 4, false);
        } else if (naceClassification.level === 2) {
            this.naceSectionSelect.mdcSelect.value = naceClassification.parent.id.toString();
            await this.fetchNaceClassification(naceClassification.parent.id.toString(), 2, false);
            this.naceDepartmentSelect.mdcSelect.value = naceClassification.id.toString();
            await this.fetchNaceClassification(naceClassification.id.toString(), 3, false);
        } else if (naceClassification.level === 1) {
            this.naceSectionSelect.mdcSelect.value = naceClassification.id.toString();
            await this.fetchNaceClassification(naceClassification.id.toString(), 2, false);
        }

        this.isSettingSelectWithFetching = true;
    }

    private async fillAndSetIndustryClassificationLevelsByLowestCommonDenominator(industryClassifications: IndustryClassification[]) {
        let industryClassificationsArray: IndustryClassification[] = [];
        let lowestLevel: number = this.getLowestLevelOfIndustryList(industryClassifications);

        if (lowestLevel > 1) {
            lowestLevel = lowestLevel - 1;
        }

        for (const industryClassification of industryClassifications) {
            let industryClassificationToBePushed: IndustryClassification = this.findIndustryElementAtGivenLevel(industryClassification, lowestLevel);
            const index = industryClassificationsArray.findIndex(object => object.id === industryClassificationToBePushed.id);

            if (index === -1) {
                industryClassificationsArray.push(industryClassificationToBePushed);
            }
        }

        if (industryClassificationsArray.length === 1) {
            await this.fillAndSetAllIndustryClassificationLevelsByIndustryElement(industryClassificationsArray[0]);
        }
    }

    private findIndustryElementAtGivenLevel(industryClassification: IndustryClassification, level: number): IndustryClassification {
        if (this.getIndustryClassificationLevel(industryClassification) === level) {
            return industryClassification;
        } else {
            return this.findIndustryElementAtGivenLevel(industryClassification.parent, level);
        }
    }

    private getLowestLevelOfIndustryList(industryClassifications: IndustryClassification[]): number {
        let lowestLevel: number = this.getIndustryClassificationLevel(industryClassifications[0]);

        for (const industryClassification of industryClassifications) {
            if (lowestLevel > this.getIndustryClassificationLevel(industryClassification)) {
                lowestLevel = this.getIndustryClassificationLevel(industryClassification);
            }
        }

        return lowestLevel;
    }

    private async fillAndSetAllIndustryClassificationLevelsByIndustryElement(industryClassification: IndustryClassification) {
        this.isSettingSelectWithFetching = false;

        const level: number = this.getIndustryClassificationLevel(industryClassification);

        if (level === 3) {
            this.industryUsageSelect.mdcSelect.value = industryClassification.parent.parent.id.toString();
            await this.fetchIndustryClassification(industryClassification.parent.parent.id.toString(), 1, false);
            this.industryAreaSelect.mdcSelect.value = industryClassification.parent.id.toString();
            await this.fetchIndustryClassification(industryClassification.parent.id.toString(), 2, false);
            this.industryDetailsSelect.mdcSelect.value = industryClassification.id.toString();
            this.industryClassificationIdField.value = this.industryDetailsSelect.mdcSelect.value;
            this.industryDoneAlert.classList.remove('d-none');
        } else if (level === 2) {
            this.industryUsageSelect.mdcSelect.value = industryClassification.parent.id.toString();
            await this.fetchIndustryClassification(industryClassification.parent.id.toString(), 1, false);
            this.industryAreaSelect.mdcSelect.value = industryClassification.id.toString();
            await this.fetchIndustryClassification(industryClassification.id.toString(), 2, false);
        } else if (level === 1) {
            this.industryUsageSelect.mdcSelect.value = industryClassification.id.toString();
            await this.fetchIndustryClassification(industryClassification.id.toString(), 1, false);
        }

        this.isSettingSelectWithFetching = true;
    }

    private getIndustryClassificationLevel(industryClassification: IndustryClassification): number {
        if (industryClassification.levelOne != null && industryClassification.levelTwo === null && industryClassification.levelThree === null) {
            return 1;
        } else if (industryClassification.levelOne != null && industryClassification.levelTwo != null && industryClassification.levelThree === null) {
            return 2;
        } else {
            return 3;
        }
    }

    private getIndustryCode(industryClassification: IndustryClassification): string {
        let code: string = industryClassification.levelOne.toString();

        if (industryClassification.levelTwo != null) {
            code = code + '.' + industryClassification.levelTwo.toString();
        }

        if (industryClassification.levelThree != null) {
            code = code + '.' + industryClassification.levelThree.toString();
        }

        return code;
    }

    private resetNaceClassificationToLevel(level: number): void {
        this.isResetting = true;

        switch (level) {
            case 1:
                this.naceSubclassSelect.mdcSelect.setValue('');
                this.naceClassSelect.mdcSelect.setValue('');
                this.naceGroupSelect.mdcSelect.setValue('');
                this.naceDepartmentSelect.mdcSelect.setValue('');
                this.naceSectionSelect.mdcSelect.setValue('');
                break;
            case 2:
                this.naceSubclassSelect.mdcSelect.setValue('');
                this.naceClassSelect.mdcSelect.setValue('');
                this.naceGroupSelect.mdcSelect.setValue('');
                this.naceDepartmentSelect.mdcSelect.setValue('');
                break;
            case 3:
                this.naceSubclassSelect.mdcSelect.setValue('');
                this.naceClassSelect.mdcSelect.setValue('');
                this.naceGroupSelect.mdcSelect.setValue('');
                break;
            case 4:
                this.naceSubclassSelect.mdcSelect.setValue('');
                this.naceClassSelect.mdcSelect.setValue('');
                break;
            case 5:
                this.naceSubclassSelect.mdcSelect.setValue('');
                break;
            case 6:
                break;
        }

        this.isResetting = false;
    }

    private showOrHideNaceClassificationByLevel(level: number): void {
        switch (level) {
            case 1:
                document.querySelector('.nace-classification-naceSection').classList.remove('d-none');
                document.querySelector('.nace-classification-naceSection').parentElement.classList.remove('d-none');
                document.querySelector('.nace-classification-naceDepartment').classList.add('d-none');
                document.querySelector('.nace-classification-naceGroup').classList.add('d-none');
                document.querySelector('.nace-classification-naceClass').classList.add('d-none');
                document.querySelector('.nace-classification-naceSubclass').classList.add('d-none');
                break;
            case 2:
                document.querySelector('.nace-classification-naceSection').classList.remove('d-none');
                document.querySelector('.nace-classification-naceSection').parentElement.classList.remove('d-none');
                document.querySelector('.nace-classification-naceDepartment').classList.remove('d-none');
                document.querySelector('.nace-classification-naceGroup').classList.add('d-none');
                document.querySelector('.nace-classification-naceClass').classList.add('d-none');
                document.querySelector('.nace-classification-naceSubclass').classList.add('d-none');
                break;
            case 3:
                document.querySelector('.nace-classification-naceSection').classList.remove('d-none');
                document.querySelector('.nace-classification-naceSection').parentElement.classList.remove('d-none');
                document.querySelector('.nace-classification-naceDepartment').classList.remove('d-none');
                document.querySelector('.nace-classification-naceGroup').classList.remove('d-none');
                document.querySelector('.nace-classification-naceClass').classList.add('d-none');
                document.querySelector('.nace-classification-naceSubclass').classList.add('d-none');
                break;
            case 4:
                document.querySelector('.nace-classification-naceSection').classList.remove('d-none');
                document.querySelector('.nace-classification-naceSection').parentElement.classList.remove('d-none');
                document.querySelector('.nace-classification-naceDepartment').classList.remove('d-none');
                document.querySelector('.nace-classification-naceGroup').classList.remove('d-none');
                document.querySelector('.nace-classification-naceClass').classList.remove('d-none');
                document.querySelector('.nace-classification-naceSubclass').classList.add('d-none');
                break;
            case 5:
                document.querySelector('.nace-classification-naceSection').classList.remove('d-none');
                document.querySelector('.nace-classification-naceSection').parentElement.classList.remove('d-none');
                document.querySelector('.nace-classification-naceDepartment').classList.remove('d-none');
                document.querySelector('.nace-classification-naceGroup').classList.remove('d-none');
                document.querySelector('.nace-classification-naceClass').classList.remove('d-none');
                document.querySelector('.nace-classification-naceSubclass').classList.remove('d-none');
                break;
            case 6:
                break
            default:
                document.querySelector('.nace-classification-naceSection').classList.add('d-none');
                document.querySelector('.nace-classification-naceSection').parentElement.classList.add('d-none');
                document.querySelector('.nace-classification-naceDepartment').classList.add('d-none');
                document.querySelector('.nace-classification-naceGroup').classList.add('d-none');
                document.querySelector('.nace-classification-naceClass').classList.add('d-none');
                document.querySelector('.nace-classification-naceSubclass').classList.add('d-none');
        }
    }

    private resetIndustryClassificationToLevel(level: number): void {
        this.isResetting = true;

        switch (level) {
            case 1:
                this.industryAreaSelect.mdcSelect.setValue('');
                this.industryDetailsSelect.mdcSelect.setValue('');
                break;
            case 2:
                this.industryDetailsSelect.mdcSelect.setValue('');
                break;
            case 3:
                break;
            default:
                this.industryUsageSelect.mdcSelect.setValue('');
                this.industryAreaSelect.mdcSelect.setValue('');
                this.industryDetailsSelect.mdcSelect.setValue('');
        }

        this.isResetting = false;
    }

    private showOrHideIndustryClassificationByLevel(level: number): void {
        switch (level) {
            case 1:
                document.querySelector('.industry-classification-industryArea').classList.remove('d-none');
                document.querySelector('.industry-classification-industryArea').parentElement.classList.remove('d-none');
                document.querySelector('.industry-classification-industryDetails').classList.add('d-none');
                break;
            case 2:
                document.querySelector('.industry-classification-industryArea').classList.remove('d-none');
                document.querySelector('.industry-classification-industryArea').parentElement.classList.remove('d-none');
                document.querySelector('.industry-classification-industryDetails').classList.remove('d-none');
                break;
            case 3:
                break;
            default:
                document.querySelector('.industry-classification-industryArea').classList.add('d-none');
                document.querySelector('.industry-classification-industryArea').parentElement.classList.add('d-none');
                document.querySelector('.industry-classification-industryDetails').classList.add('d-none');
        }
    }

    private createMDCSelectListItem(id: string, titel: string, code: string = ''): string {
        return  '<li class="mdc-list-item" aria-selected="false" data-value="' + id + '" role="option" tabindex="-1">' + "\n" +
            '   <span class="mdc-list-item__ripple"></span>' + "\n" +
            '   <span class="mdc-list-item__text"><strong>' + code + '</strong> ' + titel + '</span>' + "\n" +
            '</li>' + "\n";
    }

    public isFormValid(): boolean {
        let hasErrors: boolean = false;

        if (this.industryUsageSelect.mdcSelect.value.length === 0) {
            this.industryUsageSelect.mdcSelect.valid = false;
            hasErrors = true;
        }

        return (hasErrors === false);
    }

    public disableFields(): void {
        this.industryUsageSelect.mdcSelect.disabled = true;
        this.industryAreaSelect.mdcSelect.disabled = true;
        this.industryDetailsSelect.mdcSelect.disabled = true;
        this.naceSectionSelect.mdcSelect.disabled = true;
        this.naceDepartmentSelect.mdcSelect.disabled = true;
        this.naceGroupSelect.mdcSelect.disabled = true;
        this.naceClassSelect.mdcSelect.disabled = true;
        this.naceSubclassSelect.mdcSelect.disabled = true;
        this.industryNaceSearchQueryField.mdcTextField.disabled = true;
    }

}

export { IndustryNaceClassificationComponent };

