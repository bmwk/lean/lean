import { ListComponent } from '../../../Component/ListComponent';
import { MDCTooltip } from '@material/tooltip';

class PropertyExcelImportListComponent extends ListComponent {

    private readonly mdcTooltips: MDCTooltip[] = [];

    constructor(idPrefix: string) {
        super(idPrefix);

        this.mdcDataTableContainer.querySelectorAll('tr.mdc-data-table__row[data-row-id]').forEach((tableRowElement: HTMLTableRowElement): void => {
            if (tableRowElement.querySelector('#excel-import-' + tableRowElement.dataset.rowId + '-status-mdc-tooltip') !== null) {
                this.mdcTooltips.push(new MDCTooltip(tableRowElement.querySelector('#excel-import-' + tableRowElement.dataset.rowId + '-status-mdc-tooltip')));
            }
        });
    }

}

export { PropertyExcelImportListComponent };
