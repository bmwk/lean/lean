import { PropertyOwnerForm } from '../../Form/Property/PropertyOwnerForm';

class PropertyOwnerCreateComponent {

    private readonly propertyOwnerForm: PropertyOwnerForm;
    private readonly propertyOwnerCreateButton: HTMLButtonElement;
    private readonly propertyOwnerCreateContainer: HTMLDivElement;
    private readonly propertyOwnerCreateInputField: HTMLInputElement;

    constructor(idPrefix: string) {
        this.propertyOwnerForm = new PropertyOwnerForm(idPrefix + 'propertyOwner_');
        this.propertyOwnerCreateButton = document.querySelector('#' + idPrefix + 'property_owner_create_button');
        this.propertyOwnerCreateContainer = document.querySelector('#' + idPrefix + 'property_owner_create_container');
        this.propertyOwnerCreateInputField = document.querySelector('#' + idPrefix + 'propertyOwnerCreate');

        this.propertyOwnerCreateButton.addEventListener('click', (mouseEvent: MouseEvent): void => {
            mouseEvent.preventDefault();

            this.propertyOwnerCreateButton.innerText.toLocaleLowerCase() == 'add' ? this.propertyOwnerCreateButton.children[0].innerHTML = 'remove' : this.propertyOwnerCreateButton.children[0].innerHTML = 'add';

            if (this.propertyOwnerCreateInputField.value === 'true') {
                this.hidePropertyOwnerCreateContainer();
                this.propertyOwnerCreateInputField.value = 'false';
            } else {
                this.showPropertyOwnerCreateContainer();
                this.propertyOwnerCreateInputField.value = 'true';
            }
        });

        if (this.propertyOwnerCreateInputField.value === 'true') {
            this.propertyOwnerCreateButton.children[0].innerHTML = 'remove';
            this.showPropertyOwnerCreateContainer();
        } else {
            this.hidePropertyOwnerCreateContainer();
            this.propertyOwnerCreateInputField.value = 'false';
        }
    }

    public showPropertyOwnerCreateContainer(): void {
        this.propertyOwnerCreateContainer.classList.remove('d-none');
    }

    public hidePropertyOwnerCreateContainer(): void {
        this.propertyOwnerCreateContainer.classList.add('d-none');
    }

    public isFormValid(): boolean {
        if (this.propertyOwnerCreateInputField.value === 'false') {
            return true;
        }

        return this.propertyOwnerForm.isFormValid();
    }

}

export { PropertyOwnerCreateComponent };
