import { PropertyUserForm } from '../../Form/Property/PropertyUserForm';

class PropertyUserCreateComponent {

    private readonly propertyUserForm: PropertyUserForm;
    private readonly propertyUserCreateButton: HTMLButtonElement;
    private readonly propertyUserCreateContainer: HTMLDivElement;
    private readonly propertyUserCreateInputField: HTMLInputElement;

    constructor(idPrefix: string) {
        this.propertyUserForm = new PropertyUserForm(idPrefix + 'propertyUser_');
        this.propertyUserCreateButton = document.querySelector('#' + idPrefix + 'property_user_create_button');
        this.propertyUserCreateContainer = document.querySelector('#' + idPrefix + 'property_user_create_container');
        this.propertyUserCreateInputField = document.querySelector('#' + idPrefix + 'propertyUserCreate');

        this.propertyUserCreateButton.addEventListener('click', (mouseEvent: MouseEvent): void => {
            mouseEvent.preventDefault();

            this.propertyUserCreateButton.innerText.toLocaleLowerCase() == 'add' ? this.propertyUserCreateButton.children[0].innerHTML = 'remove' : this.propertyUserCreateButton.children[0].innerHTML = 'add';

            if (this.propertyUserCreateInputField.value == 'true') {
                this.hidePropertyUserCreateContainer();
                this.propertyUserCreateInputField.value = 'false';
            } else {
                this.showPropertyUserCreateContainer();
                this.propertyUserCreateInputField.value = 'true';
            }
        });

        if (this.propertyUserCreateInputField.value == 'true') {
            this.propertyUserCreateButton.children[0].innerHTML = 'remove';
            this.showPropertyUserCreateContainer();
        } else {
            this.hidePropertyUserCreateContainer();
            this.propertyUserCreateInputField.value = 'false';
        }
    }

    public showPropertyUserCreateContainer(): void {
        this.propertyUserCreateContainer.classList.remove('d-none');
    }

    public hidePropertyUserCreateContainer(): void {
        this.propertyUserCreateContainer.classList.add('d-none');
    }

    public isFormValid(): boolean {
        if (this.propertyUserCreateInputField.value == 'false') {
            return true;
        }

        return this.propertyUserForm.isFormValid();
    }

}

export { PropertyUserCreateComponent };
