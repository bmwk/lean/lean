import { SelectComponent } from '../../../Component/SelectComponent';
import { TextFieldComponent } from '../../../Component/TextFieldComponent';

class PropertyRestrictionForm {

    private readonly propertyRestrictionTypeSelect: SelectComponent;
    private readonly descriptionTextField: TextFieldComponent;

    constructor(idPrefix: string) {
        this.propertyRestrictionTypeSelect = new SelectComponent(document.querySelector('#' + idPrefix + 'propertyRestrictionType_mdc_select'));
        this.descriptionTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'description_mdc_text_field'));
    }

}

export { PropertyRestrictionForm };
