import { DeleteForm } from '../../../Form/DeleteForm';

class DocumentDeleteForm extends DeleteForm{

    constructor(idPrefix: string) {
        super(idPrefix);
    }

}

export { DocumentDeleteForm };
