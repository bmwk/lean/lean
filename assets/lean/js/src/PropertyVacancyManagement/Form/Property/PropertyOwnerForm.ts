import { PersonSelectOrPersonCreateForm } from '../../../PersonManagement/Form/Person/PersonSelectOrPersonCreateForm';
import { SelectComponent } from '../../../Component/SelectComponent';
import { TextFieldComponent } from '../../../Component/TextFieldComponent';

class PropertyOwnerForm {

    private readonly personSelectOrPersonCreateForm: PersonSelectOrPersonCreateForm = null;
    private readonly propertyOwnerStatusSelect: SelectComponent;
    private readonly ownershipTypeSelect: SelectComponent;
    private readonly ownershipShareTextField: TextFieldComponent;

    constructor(idPrefix: string) {
        if (PersonSelectOrPersonCreateForm.checkFormExists(idPrefix + 'personSelectOrPersonCreate_') === true) {
            this.personSelectOrPersonCreateForm = new PersonSelectOrPersonCreateForm(idPrefix + 'personSelectOrPersonCreate_');
        }

        this.propertyOwnerStatusSelect = new SelectComponent(document.querySelector('#' + idPrefix + 'propertyOwnerStatus_mdc_select'));
        this.ownershipTypeSelect = new SelectComponent(document.querySelector('#' + idPrefix + 'ownershipType_mdc_select'));
        this.ownershipShareTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'ownershipShare_mdc_text_field'));

        this.propertyOwnerStatusSelect.mdcSelect.required = true;
        this.ownershipTypeSelect.mdcSelect.required = true;
    }

    public isFormValid(): boolean {
        let hasErrors: boolean = false;

        if (this.propertyOwnerStatusSelect.mdcSelect.value.length === 0) {
            this.propertyOwnerStatusSelect.mdcSelect.valid = false;
            hasErrors = true;
        }

        if (this.ownershipTypeSelect.mdcSelect.value.length === 0) {
            this.ownershipTypeSelect.mdcSelect.valid = false;
            hasErrors = true;
        }

        if (this.personSelectOrPersonCreateForm.isFormValid() === false) {
            hasErrors = true;
        }

        return (hasErrors === false);
    }

    public disableFields(): void {
        this.personSelectOrPersonCreateForm.disableFields();
        this.propertyOwnerStatusSelect.mdcSelect.disabled = true;
        this.ownershipShareTextField.mdcTextField.disabled = true;
        this.ownershipTypeSelect.mdcSelect.disabled = true;
    }

}

export { PropertyOwnerForm };
