import { TextFieldComponent } from '../../../Component/TextFieldComponent';
import { SelectComponent } from '../../../Component/SelectComponent';

class AddressForm {

    private readonly placeSelect: SelectComponent;
    private readonly placeTextField: TextFieldComponent;
    private readonly quarterPlaceSelect: SelectComponent;
    private readonly postalCodeSelect: SelectComponent;
    private readonly streetNameTextField: TextFieldComponent;
    private readonly houseNumberTextField: TextFieldComponent;
    private readonly additionalAddressInformationTextField: TextFieldComponent;

    constructor(idPrefix: string) {
        const placeTextFieldContainer: Element = document.querySelector('#' + idPrefix + 'place_mdc_text_field');
        const placeSelectContainer: Element = document.querySelector('#' + idPrefix + 'place_mdc_select');

        this.quarterPlaceSelect = new SelectComponent(document.querySelector('#' + idPrefix + 'quarterPlace_mdc_select'));
        this.postalCodeSelect = new SelectComponent(document.querySelector('#' + idPrefix + 'postalCode_mdc_select'));
        this.streetNameTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'streetName_mdc_text_field'));
        this.houseNumberTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'houseNumber_mdc_text_field'));
        this.additionalAddressInformationTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'additionalAddressInformation_mdc_text_field'));

        if (placeTextFieldContainer !== null) {
            this.placeTextField = new TextFieldComponent(placeTextFieldContainer);
            this.placeTextField.mdcTextField.required = true;
        }

        if (placeSelectContainer !== null) {
            this.placeSelect = new SelectComponent(placeSelectContainer);
            this.placeSelect.mdcSelect.required = true;
        }

        this.postalCodeSelect.mdcSelect.required = true;
        this.streetNameTextField.mdcTextField.required = true;
        this.houseNumberTextField.mdcTextField.required = true;
    }

    public markExposeRelevantFields(): void {
        this.postalCodeSelect.markAsExposeRelevant();
        this.quarterPlaceSelect.markAsExposeRelevant();
        this.streetNameTextField.markAsExposeRelevant();
        this.houseNumberTextField.markAsExposeRelevant();
    }

    public isFormValid(): boolean {
        let hasErrors: boolean = false;

        if (this.postalCodeSelect.mdcSelect.value.length === 0) {
            this.postalCodeSelect.mdcSelect.valid = false;
            hasErrors = true;
        }

        if (this.streetNameTextField.mdcTextField.valid === false) {
            this.streetNameTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (this.houseNumberTextField.mdcTextField.valid === false) {
            this.houseNumberTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        return (hasErrors === false);
    }

}

export { AddressForm };
