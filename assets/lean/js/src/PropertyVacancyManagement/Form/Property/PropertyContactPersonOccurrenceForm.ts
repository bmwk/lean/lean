import { OccurrenceForm } from '../../../PersonManagement/Form/Occurrence/OccurrenceForm';
import { SelectComponent } from '../../../Component/SelectComponent';

class PropertyContactPersonOccurrenceForm extends OccurrenceForm {

    private readonly propertyContactPersonSelect: SelectComponent;

    constructor(idPrefix: string) {
        super(idPrefix);

        this.propertyContactPersonSelect = new SelectComponent(document.querySelector('#' + idPrefix + 'propertyContactPerson_mdc_select'));

        this.propertyContactPersonSelect.mdcSelect.required = true;
    }

    public isFormValid(): boolean {
        let hasErrors: boolean = (super.isFormValid() === false);

        if (this.propertyContactPersonSelect.mdcSelect.value.length === 0) {
            this.propertyContactPersonSelect.mdcSelect.valid = false;
            hasErrors = true;
        }

        return (hasErrors === false);
    }

    public disableFields(): void {
        super.disableFields();
        this.propertyContactPersonSelect.mdcSelect.disabled = true;
    }

}

export { PropertyContactPersonOccurrenceForm };
