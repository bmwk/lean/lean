import { PersonSelectOrPersonCreateForm } from '../../../PersonManagement/Form/Person/PersonSelectOrPersonCreateForm';
import { SelectComponent } from '../../../Component/SelectComponent';
import { CheckboxFieldComponent } from '../../../Component/CheckboxFieldComponent';

class PropertyContactPersonForm {

    private readonly personSelectOrPersonCreateForm: PersonSelectOrPersonCreateForm = null;
    private readonly propertyContactResponsibilitySelect: SelectComponent;
    private readonly visibleContactInformationCheckboxFields: CheckboxFieldComponent[] = [];

    constructor(idPrefix: string) {
        if (PersonSelectOrPersonCreateForm.checkFormExists(idPrefix + 'personSelectOrPersonCreate_') === true) {
            this.personSelectOrPersonCreateForm = new PersonSelectOrPersonCreateForm(idPrefix + 'personSelectOrPersonCreate_');
        }

        this.propertyContactResponsibilitySelect = new SelectComponent(document.querySelector('#' + idPrefix + 'propertyContactResponsibility_mdc_select'));

        document.querySelectorAll('div.mdc-form-field[id^=' + idPrefix + 'visibleContactInformations_]').forEach((element: HTMLDivElement) => {
            this.visibleContactInformationCheckboxFields.push(new CheckboxFieldComponent(element));
        });

        this.propertyContactResponsibilitySelect.mdcSelect.required = true;
    }

    public isFormValid(): boolean {
        let hasErrors: boolean = false;

        if (this.propertyContactResponsibilitySelect.mdcSelect.value.length === 0) {
            this.propertyContactResponsibilitySelect.mdcSelect.valid = false;
            hasErrors = true;
        }

        if (this.personSelectOrPersonCreateForm.isFormValid() === false) {
            hasErrors = true;
        }

        return (hasErrors === false);
    }

    public disableFields(): void {
        this.personSelectOrPersonCreateForm.disableFields();
        this.propertyContactResponsibilitySelect.mdcSelect.disabled = true;

        this.visibleContactInformationCheckboxFields.forEach((checkboxField: CheckboxFieldComponent): void => {
            checkboxField.mdcCheckbox.disabled = true;
        });
    }

}

export { PropertyContactPersonForm };
