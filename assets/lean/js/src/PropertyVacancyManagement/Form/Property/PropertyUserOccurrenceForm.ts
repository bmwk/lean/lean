import { OccurrenceForm } from '../../../PersonManagement/Form/Occurrence/OccurrenceForm';
import { SelectComponent } from '../../../Component/SelectComponent';

class PropertyUserOccurrenceForm extends OccurrenceForm {

    private readonly propertyUserSelect: SelectComponent;

    constructor(idPrefix: string) {
        super(idPrefix);

        this.propertyUserSelect = new SelectComponent(document.querySelector('#' + idPrefix + 'propertyUser_mdc_select'));

        this.propertyUserSelect.mdcSelect.required = true;
    }

    public isFormValid(): boolean {
        let hasErrors: boolean = (super.isFormValid() === false);

        if (this.propertyUserSelect.mdcSelect.value.length === 0) {
            this.propertyUserSelect.mdcSelect.valid = false;
            hasErrors = true;
        }

        return (hasErrors === false);
    }

    public disableFields(): void {
        super.disableFields();
        this.propertyUserSelect.mdcSelect.disabled = true;
    }

}

export { PropertyUserOccurrenceForm };
