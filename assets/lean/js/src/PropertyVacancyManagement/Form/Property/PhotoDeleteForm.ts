import { DeleteForm } from '../../../Form/DeleteForm';

class PhotoDeleteForm extends DeleteForm {

    constructor(idPrefix: string) {
        super(idPrefix);
    }

}

export { PhotoDeleteForm };
