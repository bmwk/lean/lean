import { OccurrenceForm } from '../../../PersonManagement/Form/Occurrence/OccurrenceForm';
import { SelectComponent } from '../../../Component/SelectComponent';

class PropertyOwnerOccurrenceForm extends OccurrenceForm {

    private readonly propertyOwnerSelect: SelectComponent;

    constructor(idPrefix: string) {
        super(idPrefix);

        this.propertyOwnerSelect = new SelectComponent(document.querySelector('#' + idPrefix + 'propertyOwner_mdc_select'));

        this.propertyOwnerSelect.mdcSelect.required = true;
    }

    public isFormValid(): boolean {
        let hasErrors: boolean = (super.isFormValid() === false);

        if (this.propertyOwnerSelect.mdcSelect.value.length === 0) {
            this.propertyOwnerSelect.mdcSelect.valid = false;
            hasErrors = true;
        }

        return (hasErrors === false);
    }

    public disableFields(): void {
        super.disableFields();
        this.propertyOwnerSelect.mdcSelect.disabled = true;
    }

}

export { PropertyOwnerOccurrenceForm };
