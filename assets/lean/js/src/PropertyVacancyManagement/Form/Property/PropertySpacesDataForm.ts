import { TextFieldComponent } from '../../../Component/TextFieldComponent';
import { SelectComponent } from '../../../Component/SelectComponent';
import { MDCSwitch } from '@material/switch';

class PropertySpacesDataForm {

    private readonly roomCountTextField: TextFieldComponent;
    private readonly livingSpaceTextField: TextFieldComponent;
    private readonly subsidiarySpaceTextField: TextFieldComponent;
    private readonly usableSpaceTextField: TextFieldComponent;
    private readonly storeSpaceTextField: TextFieldComponent;
    private readonly storageSpaceTextField: TextFieldComponent;
    private readonly retailSpaceTextField: TextFieldComponent;
    private readonly openSpaceTextField: TextFieldComponent;
    private readonly officeSpaceTextField: TextFieldComponent;
    private readonly officePartSpaceTextField: TextFieldComponent;
    private readonly administrationSpaceTextField: TextFieldComponent;
    private readonly gastronomySpaceTextField: TextFieldComponent;
    private readonly productionSpaceTextField: TextFieldComponent;
    private readonly plotSizeTextField: TextFieldComponent;
    private readonly otherSpaceTextField: TextFieldComponent;
    private readonly balconyTurfSpaceTextField: TextFieldComponent;
    private readonly gardenSpaceTextField: TextFieldComponent;
    private readonly cellarSpaceTextField: TextFieldComponent;
    private readonly atticSpaceTextField: TextFieldComponent;
    private readonly heatableSurfaceTextField: TextFieldComponent;
    private readonly rentableAreaTextField: TextFieldComponent;
    private readonly minimumSpaceForSeparationTextField: TextFieldComponent;
    private readonly usableOutdoorAreaPossibilitySelect: SelectComponent;
    private readonly outdoorAreaDetailsContainer: HTMLDivElement;
    private readonly totalOutdoorAreaTextField: TextFieldComponent;
    private readonly usableOutdoorAreaTextField: TextFieldComponent;
    private readonly outdoorSalesAreaTextField: TextFieldComponent;
    private readonly roofingPresentInputField: HTMLInputElement;
    private readonly roofingPresentMdcSwitch: MDCSwitch;
    private readonly showAllAreaFieldsMdcSwitch: MDCSwitch;

    constructor(idPrefix: string) {
        this.roomCountTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'roomCount_mdc_text_field'));
        this.livingSpaceTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'livingSpace_mdc_text_field'));
        this.subsidiarySpaceTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'subsidiarySpace_mdc_text_field'));
        this.usableSpaceTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'usableSpace_mdc_text_field'));
        this.storeSpaceTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'storeSpace_mdc_text_field'));
        this.storageSpaceTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'storageSpace_mdc_text_field'));
        this.retailSpaceTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'retailSpace_mdc_text_field'));
        this.openSpaceTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'openSpace_mdc_text_field'));
        this.officeSpaceTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'officeSpace_mdc_text_field'));
        this.officePartSpaceTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'officePartSpace_mdc_text_field'));
        this.administrationSpaceTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'administrationSpace_mdc_text_field'));
        this.gastronomySpaceTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'gastronomySpace_mdc_text_field'));
        this.productionSpaceTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'productionSpace_mdc_text_field'));
        this.plotSizeTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'plotSize_mdc_text_field'));
        this.otherSpaceTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'otherSpace_mdc_text_field'));
        this.balconyTurfSpaceTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'balconyTurfSpace_mdc_text_field'));
        this.gardenSpaceTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'gardenSpace_mdc_text_field'));
        this.cellarSpaceTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'cellarSpace_mdc_text_field'));
        this.atticSpaceTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'atticSpace_mdc_text_field'));
        this.heatableSurfaceTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'heatableSurface_mdc_text_field'));
        this.rentableAreaTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'rentableArea_mdc_text_field'));
        this.minimumSpaceForSeparationTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'minimumSpaceForSeparation_mdc_text_field'));
        this.usableOutdoorAreaPossibilitySelect = new SelectComponent(document.querySelector('#' + idPrefix + 'usableOutdoorAreaPossibility_mdc_select'));
        this.outdoorAreaDetailsContainer = document.querySelector('#' + idPrefix + 'outdoor_area_details');
        this.totalOutdoorAreaTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'totalOutdoorArea_mdc_text_field'));
        this.usableOutdoorAreaTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'usableOutdoorArea_mdc_text_field'));
        this.outdoorSalesAreaTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'outdoorSalesArea_mdc_text_field'));
        this.roofingPresentInputField = document.querySelector('#' + idPrefix + 'roofingPresent');
        this.roofingPresentMdcSwitch = new MDCSwitch(document.querySelector('#' + idPrefix + 'roofing_present_mdc_switch'));
        this.showAllAreaFieldsMdcSwitch = new MDCSwitch(document.querySelector('#' + idPrefix + 'showAllAreaFields_mdc_switch'));

        this.showOrHideUsableOutdoorAreaDetails();
        this.setRoofingPresentStatus();
        this.hideEmptyAreaField();
        this.checkToShowAllAreaFields();

        this.usableOutdoorAreaPossibilitySelect.mdcSelect.listen('MDCSelect:change', (): void => this.showOrHideUsableOutdoorAreaDetails());

        this.roofingPresentMdcSwitch.listen('click', (): void => {
            if (this.roofingPresentMdcSwitch.selected === true) {
                this.roofingPresentInputField.value = '1';
            } else {
                this.roofingPresentInputField.value = '0';
            }
        });

        this.showAllAreaFieldsMdcSwitch.listen('click', (): void => {
            if (this.showAllAreaFieldsMdcSwitch.selected === true) {
                this.showAllAreaFields();
            } else {
                this.hideEmptyAreaField();
            }
        });
    }

    public markMatchingRelevantFields(): void {
        this.roomCountTextField.markAsMatchingRelevant();
        this.subsidiarySpaceTextField.markAsMatchingRelevant();
        this.retailSpaceTextField.markAsMatchingRelevant();
    }

    public markExposeRelevantFields(): void {
        this.subsidiarySpaceTextField.markAsExposeRelevant();
        this.retailSpaceTextField.markAsExposeRelevant();
        this.gastronomySpaceTextField.markAsExposeRelevant();
        this.storageSpaceTextField.markAsExposeRelevant();
        this.officeSpaceTextField.markAsExposeRelevant();
    }

    private showOrHideUsableOutdoorAreaDetails(): void {
        if (
            this.usableOutdoorAreaPossibilitySelect.mdcSelect.value === '0'
            || this.usableOutdoorAreaPossibilitySelect.mdcSelect.value === '1'
        ) {
            this.outdoorAreaDetailsContainer.classList.remove('d-none');
        } else {
            this.outdoorAreaDetailsContainer.classList.add('d-none');
        }
    }

    private hideEmptyAreaField(): void {
        if (this.livingSpaceTextField.mdcTextField.value === null || this.livingSpaceTextField.mdcTextField.value === '') {
            this.livingSpaceTextField.mdcTextField.root.parentElement.classList.add('d-none');
        }

        if (this.subsidiarySpaceTextField.mdcTextField.value === null || this.subsidiarySpaceTextField.mdcTextField.value === '') {
            this.subsidiarySpaceTextField.mdcTextField.root.parentElement.classList.add('d-none');
        }

        if (this.usableSpaceTextField.mdcTextField.value === null || this.usableSpaceTextField.mdcTextField.value === '') {
            this.usableSpaceTextField.mdcTextField.root.parentElement.classList.add('d-none');
        }

        if (this.storeSpaceTextField.mdcTextField.value === null || this.storeSpaceTextField.mdcTextField.value === '') {
            this.storeSpaceTextField.mdcTextField.root.parentElement.classList.add('d-none');
        }

        if (this.storageSpaceTextField.mdcTextField.value === null || this.storageSpaceTextField.mdcTextField.value === '') {
            this.storageSpaceTextField.mdcTextField.root.parentElement.classList.add('d-none');
        }

        if (this.retailSpaceTextField.mdcTextField.value === null || this.retailSpaceTextField.mdcTextField.value === '') {
            this.retailSpaceTextField.mdcTextField.root.parentElement.classList.add('d-none');
        }

        if (this.openSpaceTextField.mdcTextField.value === null || this.openSpaceTextField.mdcTextField.value === '') {
            this.openSpaceTextField.mdcTextField.root.parentElement.classList.add('d-none');
        }

        if (this.officeSpaceTextField.mdcTextField.value === null || this.officeSpaceTextField.mdcTextField.value === '') {
            this.officeSpaceTextField.mdcTextField.root.parentElement.classList.add('d-none');
        }

        if (this.officePartSpaceTextField.mdcTextField.value === null || this.officePartSpaceTextField.mdcTextField.value === '') {
            this.officePartSpaceTextField.mdcTextField.root.parentElement.classList.add('d-none');
        }

        if (this.administrationSpaceTextField.mdcTextField.value === null || this.administrationSpaceTextField.mdcTextField.value === '') {
            this.administrationSpaceTextField.mdcTextField.root.parentElement.classList.add('d-none');
        }

        if (this.gastronomySpaceTextField.mdcTextField.value === null || this.gastronomySpaceTextField.mdcTextField.value === '') {
            this.gastronomySpaceTextField.mdcTextField.root.parentElement.classList.add('d-none');
        }

        if (this.productionSpaceTextField.mdcTextField.value === null || this.productionSpaceTextField.mdcTextField.value === '') {
            this.productionSpaceTextField.mdcTextField.root.parentElement.classList.add('d-none');
        }

        if (this.plotSizeTextField.mdcTextField.value === null || this.plotSizeTextField.mdcTextField.value === '') {
            this.plotSizeTextField.mdcTextField.root.parentElement.classList.add('d-none');
        }

        if (this.otherSpaceTextField.mdcTextField.value === null || this.otherSpaceTextField.mdcTextField.value === '') {
            this.otherSpaceTextField.mdcTextField.root.parentElement.classList.add('d-none');
        }

        if (this.balconyTurfSpaceTextField.mdcTextField.value === null || this.balconyTurfSpaceTextField.mdcTextField.value === '') {
            this.balconyTurfSpaceTextField.mdcTextField.root.parentElement.classList.add('d-none');
        }

        if (this.gardenSpaceTextField.mdcTextField.value === null || this.gardenSpaceTextField.mdcTextField.value === '') {
            this.gardenSpaceTextField.mdcTextField.root.parentElement.classList.add('d-none');
        }

        if (this.cellarSpaceTextField.mdcTextField.value === null || this.cellarSpaceTextField.mdcTextField.value === '') {
            this.cellarSpaceTextField.mdcTextField.root.parentElement.classList.add('d-none');
        }

        if (this.atticSpaceTextField.mdcTextField.value === null || this.atticSpaceTextField.mdcTextField.value === '') {
            this.atticSpaceTextField.mdcTextField.root.parentElement.classList.add('d-none');
        }

        if (this.heatableSurfaceTextField.mdcTextField.value === null || this.heatableSurfaceTextField.mdcTextField.value === '') {
            this.heatableSurfaceTextField.mdcTextField.root.parentElement.classList.add('d-none');
        }

        if (this.rentableAreaTextField.mdcTextField.value === null || this.rentableAreaTextField.mdcTextField.value === '') {
            this.rentableAreaTextField.mdcTextField.root.parentElement.classList.add('d-none');
        }

        if (this.minimumSpaceForSeparationTextField.mdcTextField.value === null || this.minimumSpaceForSeparationTextField.mdcTextField.value === '') {
            this.minimumSpaceForSeparationTextField.mdcTextField.root.parentElement.classList.add('d-none');
        }
    }

    private showAllAreaFields(): void {
        this.livingSpaceTextField.mdcTextField.root.parentElement.classList.remove('d-none');
        this.subsidiarySpaceTextField.mdcTextField.root.parentElement.classList.remove('d-none');
        this.usableSpaceTextField.mdcTextField.root.parentElement.classList.remove('d-none');
        this.storeSpaceTextField.mdcTextField.root.parentElement.classList.remove('d-none');
        this.storageSpaceTextField.mdcTextField.root.parentElement.classList.remove('d-none');
        this.retailSpaceTextField.mdcTextField.root.parentElement.classList.remove('d-none');
        this.openSpaceTextField.mdcTextField.root.parentElement.classList.remove('d-none');
        this.officeSpaceTextField.mdcTextField.root.parentElement.classList.remove('d-none');
        this.officePartSpaceTextField.mdcTextField.root.parentElement.classList.remove('d-none');
        this.administrationSpaceTextField.mdcTextField.root.parentElement.classList.remove('d-none');
        this.gastronomySpaceTextField.mdcTextField.root.parentElement.classList.remove('d-none');
        this.productionSpaceTextField.mdcTextField.root.parentElement.classList.remove('d-none');
        this.plotSizeTextField.mdcTextField.root.parentElement.classList.remove('d-none');
        this.otherSpaceTextField.mdcTextField.root.parentElement.classList.remove('d-none');
        this.balconyTurfSpaceTextField.mdcTextField.root.parentElement.classList.remove('d-none');
        this.gardenSpaceTextField.mdcTextField.root.parentElement.classList.remove('d-none');
        this.atticSpaceTextField.mdcTextField.root.parentElement.classList.remove('d-none');
        this.heatableSurfaceTextField.mdcTextField.root.parentElement.classList.remove('d-none');
        this.rentableAreaTextField.mdcTextField.root.parentElement.classList.remove('d-none');
        this.minimumSpaceForSeparationTextField.mdcTextField.root.parentElement.classList.remove('d-none');
        this.showAllAreaFieldsMdcSwitch.selected = true;
    }

    private checkToShowAllAreaFields(): void {
        if (
            (this.livingSpaceTextField.mdcTextField.value === null || this.livingSpaceTextField.mdcTextField.value === '')
            && (this.subsidiarySpaceTextField.mdcTextField.value === null || this.usableSpaceTextField.mdcTextField.value === '')
            && (this.usableSpaceTextField.mdcTextField.value === null || this.usableSpaceTextField.mdcTextField.value === '')
            && (this.storeSpaceTextField.mdcTextField.value === null || this.storeSpaceTextField.mdcTextField.value === '')
            && (this.storageSpaceTextField.mdcTextField.value === null || this.storageSpaceTextField.mdcTextField.value === '')
            && (this.retailSpaceTextField.mdcTextField.value === null || this.retailSpaceTextField.mdcTextField.value === '')
            && (this.openSpaceTextField.mdcTextField.value === null || this.openSpaceTextField.mdcTextField.value === '')
            && (this.officeSpaceTextField.mdcTextField.value === null || this.officeSpaceTextField.mdcTextField.value === '')
            && (this.officePartSpaceTextField.mdcTextField.value === null || this.officePartSpaceTextField.mdcTextField.value === '')
            && (this.administrationSpaceTextField.mdcTextField.value === null || this.administrationSpaceTextField.mdcTextField.value === '')
            && (this.gastronomySpaceTextField.mdcTextField.value === null || this.gastronomySpaceTextField.mdcTextField.value === '')
            && (this.productionSpaceTextField.mdcTextField.value === null || this.productionSpaceTextField.mdcTextField.value === '')
            && (this.plotSizeTextField.mdcTextField.value === null || this.plotSizeTextField.mdcTextField.value === '')
            && (this.otherSpaceTextField.mdcTextField.value === null || this.otherSpaceTextField.mdcTextField.value === '')
            && (this.balconyTurfSpaceTextField.mdcTextField.value === null || this.balconyTurfSpaceTextField.mdcTextField.value === '')
            && (this.gardenSpaceTextField.mdcTextField.value === null || this.gardenSpaceTextField.mdcTextField.value === '')
            && (this.cellarSpaceTextField.mdcTextField.value === null || this.cellarSpaceTextField.mdcTextField.value === '')
            && (this.atticSpaceTextField.mdcTextField.value === null || this.atticSpaceTextField.mdcTextField.value === '')
            && (this.heatableSurfaceTextField.mdcTextField.value === null || this.heatableSurfaceTextField.mdcTextField.value === '')
            && (this.rentableAreaTextField.mdcTextField.value === null || this.rentableAreaTextField.mdcTextField.value === '')
            && (this.minimumSpaceForSeparationTextField.mdcTextField.value === null || this.minimumSpaceForSeparationTextField.mdcTextField.value === '')
        ) {
            this.showAllAreaFields();
        }
    }

    private setRoofingPresentStatus(): void {
        this.roofingPresentMdcSwitch.selected = this.roofingPresentInputField.value === '1';
    }

    public isFormValid(): boolean {
        let hasErrors: boolean = false;

        if (isNaN(Number(this.roomCountTextField.mdcTextField.value.replace(',','.')))) {
            this.roomCountTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.livingSpaceTextField.mdcTextField.value.replace(',','.')))) {
            this.livingSpaceTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.subsidiarySpaceTextField.mdcTextField.value.replace(',','.')))) {
            this.subsidiarySpaceTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.usableSpaceTextField.mdcTextField.value.replace(',','.')))) {
            this.usableSpaceTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.storeSpaceTextField.mdcTextField.value.replace(',','.')))) {
            this.storeSpaceTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.storageSpaceTextField.mdcTextField.value.replace(',','.')))) {
            this.storageSpaceTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.retailSpaceTextField.mdcTextField.value.replace(',','.')))) {
            this.retailSpaceTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.openSpaceTextField.mdcTextField.value.replace(',','.')))) {
            this.openSpaceTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.officeSpaceTextField.mdcTextField.value.replace(',','.')))) {
            this.officeSpaceTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.officePartSpaceTextField.mdcTextField.value.replace(',','.')))) {
            this.officePartSpaceTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.administrationSpaceTextField.mdcTextField.value.replace(',','.')))) {
            this.administrationSpaceTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.gastronomySpaceTextField.mdcTextField.value.replace(',','.')))) {
            this.gastronomySpaceTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.productionSpaceTextField.mdcTextField.value.replace(',','.')))) {
            this.productionSpaceTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.plotSizeTextField.mdcTextField.value.replace(',','.')))) {
            this.plotSizeTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.otherSpaceTextField.mdcTextField.value.replace(',','.')))) {
            this.otherSpaceTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.balconyTurfSpaceTextField.mdcTextField.value.replace(',','.')))) {
            this.balconyTurfSpaceTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.gardenSpaceTextField.mdcTextField.value.replace(',','.')))) {
            this.gardenSpaceTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.cellarSpaceTextField.mdcTextField.value.replace(',','.')))) {
            this.cellarSpaceTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.atticSpaceTextField.mdcTextField.value.replace(',','.')))) {
            this.atticSpaceTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.heatableSurfaceTextField.mdcTextField.value.replace(',','.')))) {
            this.heatableSurfaceTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.rentableAreaTextField.mdcTextField.value.replace(',','.')))) {
            this.rentableAreaTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.minimumSpaceForSeparationTextField.mdcTextField.value.replace(',','.')))) {
            this.minimumSpaceForSeparationTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.totalOutdoorAreaTextField.mdcTextField.value.replace(',','.')))) {
            this.totalOutdoorAreaTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.usableOutdoorAreaTextField.mdcTextField.value.replace(',','.')))) {
            this.usableOutdoorAreaTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.outdoorSalesAreaTextField.mdcTextField.value.replace(',','.')))) {
            this.outdoorSalesAreaTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        return (hasErrors === false);
    }

}

export { PropertySpacesDataForm };
