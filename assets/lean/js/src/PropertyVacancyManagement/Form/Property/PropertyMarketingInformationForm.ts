import { PropertyPricingInformationForm } from './PropertyPricingInformationForm';
import { PropertyRestrictionForm } from './PropertyRestrictionForm';
import { TextFieldComponent } from '../../../Component/TextFieldComponent';
import { SelectComponent } from '../../../Component/SelectComponent';

class PropertyMarketingInformationForm {

    private readonly propertyPricingInformationForm: PropertyPricingInformationForm;
    private readonly propertyRestrictionForms: PropertyRestrictionForm[];
    private readonly propertyRestrictionAddButton: HTMLButtonElement;
    private readonly possibleUsageContent: HTMLDivElement;
    private readonly developmentPotentialSelect: SelectComponent;
    private readonly potentialDescriptionTextField: TextFieldComponent;
    private readonly environmentDescriptionTextField: TextFieldComponent;
    private readonly groundReferenceValueTextField: TextFieldComponent;
    private readonly groundValueTextField: TextFieldComponent;
    private readonly marketValueTextField: TextFieldComponent;
    private readonly valueAffectingCircumstancesTextField: TextFieldComponent;
    private readonly propertyMarketingStatusSelect: SelectComponent;
    private readonly propertyOfferTypeSelect: SelectComponent;
    private readonly propertyPriceTypeSelect: SelectComponent;
    private readonly availableFromTextField: TextFieldComponent;
    private readonly restrictionContainer: HTMLElement;

    constructor(idPrefix: string) {
        this.propertyPricingInformationForm = new PropertyPricingInformationForm(idPrefix + 'propertyPricingInformation_');
        this.propertyRestrictionForms = [];
        this.propertyRestrictionAddButton = document.querySelector('#' + idPrefix + 'propertyRestrictionAddButton');
        this.possibleUsageContent = document.querySelector('#' + idPrefix + 'possible_usage_content');
        this.developmentPotentialSelect = new SelectComponent(document.querySelector('#' + idPrefix + 'developmentPotential_mdc_select'), {clearButton: true});
        this.potentialDescriptionTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'potentialDescription_mdc_text_field'), {autoFitTextarea: true});
        this.environmentDescriptionTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'environmentDescription_mdc_text_field'), {autoFitTextarea: true});
        this.groundReferenceValueTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'groundReferenceValue_mdc_text_field'));
        this.groundValueTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'groundValue_mdc_text_field'));
        this.marketValueTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'marketValue_mdc_text_field'));
        this.valueAffectingCircumstancesTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'valueAffectingCircumstances_mdc_text_field'));
        this.propertyMarketingStatusSelect = new SelectComponent(document.querySelector('#' + idPrefix + 'propertyMarketingStatus_mdc_select'), {clearButton: true});
        this.propertyOfferTypeSelect = new SelectComponent(document.querySelector('#' + idPrefix + 'propertyOfferType_mdc_select'), {clearButton: true});
        this.propertyPriceTypeSelect = new SelectComponent(document.querySelector('#' + idPrefix + 'propertyPriceType_mdc_select'), {clearButton: true});
        this.availableFromTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'availableFrom_mdc_text_field'), {datePicker: true});
        this.restrictionContainer = document.querySelector('#' + idPrefix + 'restriction_container');

        if (localStorage.getItem('lean.property.matchingRelevantFields') === 'true') {
            this.markMatchingRelevantFields();
            this.possibleUsageContent.classList.add('matching-relevant');
        }

        if (localStorage.getItem('lean.property.exposeRelevantFields') === 'true') {
            this.markExposeRelevantFields();
            this.possibleUsageContent.classList.add('expose-relevant');
        }

        this.showOrHidePricingContainerByPropertyOfferType();

        this.propertyOfferTypeSelect.mdcSelect.listen('MDCSelect:change', (): void => this.showOrHidePricingContainerByPropertyOfferType());

        this.restrictionContainer.querySelectorAll('div.property-restriction-form-container[data-row-id]').forEach((propertyRestrictionFormContainer: HTMLDivElement): void => {
            this.propertyRestrictionForms.push(new PropertyRestrictionForm(idPrefix + 'propertyRestrictions_' +  propertyRestrictionFormContainer.dataset.rowId + '_'));

            const deleteButton: HTMLButtonElement = propertyRestrictionFormContainer.querySelector('#' + idPrefix + 'propertyRestrictions_' + propertyRestrictionFormContainer.dataset.rowId + '_delete_button');

            deleteButton.addEventListener('click', (mouseEvent: MouseEvent): void => {
                mouseEvent.preventDefault();

                propertyRestrictionFormContainer.remove();
            });

        });

        if (this.propertyRestrictionAddButton !== null) {
            this.propertyRestrictionAddButton.addEventListener('click', (mouseEvent: MouseEvent): void => {
                mouseEvent.preventDefault();
                this.addPropertyRestriction();
            });
        }
    }

    public showOrHidePricingContainerByPropertyOfferType(): void {
        this.propertyPricingInformationForm.hidePurchasePricingInformationContainer();
        this.propertyPricingInformationForm.hideRentPricingInformationContainer();
        this.propertyPricingInformationForm.hideLeasePricingInformationContainer();
        this.propertyPricingInformationForm.hideDepositPricingInformationContainer();

        switch (this.propertyOfferTypeSelect.mdcSelect.value) {
            case '0':
                this.propertyPricingInformationForm.showPurchasePricingInformationContainer();
                break;
            case '1':
                this.propertyPricingInformationForm.showRentPricingInformationContainer();
                this.propertyPricingInformationForm.showDepositPricingInformationContainer();
                break;
            case '2':
                this.propertyPricingInformationForm.showLeasePricingInformationContainer();
                this.propertyPricingInformationForm.showDepositPricingInformationContainer();
                break;
        }
    }

    public markMatchingRelevantFields(): void {
        this.propertyOfferTypeSelect.markAsMatchingRelevant();
    }

    public markExposeRelevantFields(): void {
        this.propertyPricingInformationForm.markExposeRelevantFields();
        this.propertyOfferTypeSelect.markAsExposeRelevant();
    }

    private addPropertyRestriction(): void {
        const parent = this.restrictionContainer;
        const prototype = parent.dataset.prototype;

        const template = document.createElement('template');
        const childrenCount = parent.children.length.toString();

        template.innerHTML = prototype.replace(/__name__/g, childrenCount).trim();

        const newRestriction = template.content.firstChild as Element;

        const newRestrictionContainer: HTMLDivElement = document.createElement('div');
        const deleteRestrictionButton: HTMLButtonElement = document.createElement('button');

        deleteRestrictionButton.classList.add('mdc-button');
        deleteRestrictionButton.classList.add('text-danger');

        deleteRestrictionButton.innerText = 'Löschen';

        deleteRestrictionButton.addEventListener('click', (mouseEvent): void => {
            mouseEvent.preventDefault();
            parent.removeChild(newRestrictionContainer);
        });

        newRestrictionContainer.appendChild(newRestriction);
        newRestrictionContainer.appendChild(deleteRestrictionButton);

        for (let i = 0; i < newRestriction.children.length; i++) {
            newRestriction.children[i].classList.add('mb-2');
        }

        newRestrictionContainer.classList.add('mb-4');

        new SelectComponent(newRestriction.children[0]).mdcSelect.required = true;
        new TextFieldComponent(newRestriction.children[1]).mdcTextField.required = true;

        parent.appendChild(newRestrictionContainer);
    }

    public isFormValid(): boolean {
        let hasErrors: boolean = false;

        if (isNaN(Number(this.groundReferenceValueTextField.mdcTextField.value.replace(',','.'))) === true) {
            this.groundReferenceValueTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.groundValueTextField.mdcTextField.value.replace(',','.'))) === true) {
            this.groundValueTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.marketValueTextField.mdcTextField.value.replace(',','.'))) === true) {
            this.marketValueTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (this.propertyPricingInformationForm.isFormValid() === false) {
            hasErrors = true;
        }

        return (hasErrors === false);
    }

}

export { PropertyMarketingInformationForm };
