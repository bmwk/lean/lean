import { TextFieldComponent } from '../../../Component/TextFieldComponent';
import { SelectComponent } from '../../../Component/SelectComponent';
import { CheckboxFieldComponent } from '../../../Component/CheckboxFieldComponent';

class PropertyPricingInformationForm {

    private readonly currencySelect: SelectComponent;
    private readonly purchasePriceNetTextField: TextFieldComponent;
    private readonly purchasePriceGrossTextField: TextFieldComponent;
    private readonly purchasePricePerSquareMeterTextField: TextFieldComponent;
    private readonly netColdRentTextField: TextFieldComponent;
    private readonly coldRentTextField: TextFieldComponent;
    private readonly rentIncludingHeatingTextField: TextFieldComponent;
    private readonly additionalCostsTextField: TextFieldComponent;
    private readonly heatingCostsIncludedCheckboxField: CheckboxFieldComponent;
    private readonly rentalPricePerSquareMeterTextField: TextFieldComponent;
    private readonly leaseTextField: TextFieldComponent;
    private readonly leaseholdTextField: TextFieldComponent;
    private readonly houseMoneyTextField: TextFieldComponent;
    private readonly commissionableCheckboxField: CheckboxFieldComponent;
    private readonly insideCommissionTextField: TextFieldComponent;
    private readonly outsideCommissionTextField: TextFieldComponent;
    private readonly commissionNetTextField: TextFieldComponent;
    private readonly commissionGrossTextField: TextFieldComponent;
    private readonly freeTextPriceTextField: TextFieldComponent;
    private readonly depositTextField: TextFieldComponent;
    private readonly depositTextTextField: TextFieldComponent;
    private readonly purchasePricingInformationContainer: HTMLDivElement;
    private readonly rentPricingInformationContainer: HTMLDivElement;
    private readonly leasePricingInformationContainer: HTMLDivElement;
    private readonly commissionPricingInformationContainer: HTMLDivElement;
    private readonly depositPricingInformationContainer: HTMLDivElement;

    constructor(idPrefix: string) {
        this.currencySelect = new SelectComponent(document.querySelector('#' + idPrefix + 'currency_mdc_select'));
        this.purchasePriceNetTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'purchasePriceNet_mdc_text_field'));
        this.purchasePriceGrossTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'purchasePriceGross_mdc_text_field'));
        this.purchasePricePerSquareMeterTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'purchasePricePerSquareMeter_mdc_text_field'));
        this.netColdRentTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'netColdRent_mdc_text_field'));
        this.coldRentTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'coldRent_mdc_text_field'));
        this.rentIncludingHeatingTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'rentIncludingHeating_mdc_text_field'));
        this.additionalCostsTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'additionalCosts_mdc_text_field'));
        this.heatingCostsIncludedCheckboxField = new CheckboxFieldComponent(document.querySelector('#' + idPrefix + 'heatingCostsIncluded_mdc_form_field'));
        this.rentalPricePerSquareMeterTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'rentalPricePerSquareMeter_mdc_text_field'));
        this.leaseTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'lease_mdc_text_field'));
        this.leaseholdTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'leasehold_mdc_text_field'));
        this.houseMoneyTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'houseMoney_mdc_text_field'));
        this.commissionableCheckboxField = new CheckboxFieldComponent(document.querySelector('#' + idPrefix + 'commissionable_mdc_form_field'));
        this.insideCommissionTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'insideCommission_mdc_text_field'));
        this.outsideCommissionTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'outsideCommission_mdc_text_field'));
        this.commissionNetTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'commissionNet_mdc_text_field'));
        this.commissionGrossTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'commissionGross_mdc_text_field'));
        this.freeTextPriceTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'freeTextPrice_mdc_text_field'));
        this.depositTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'deposit_mdc_text_field'));
        this.depositTextTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'depositText_mdc_text_field'));
        this.purchasePricingInformationContainer = document.querySelector('#' + idPrefix + 'purchase_pricing_information');
        this.rentPricingInformationContainer = document.querySelector('#' + idPrefix + 'rent_pricing_information');
        this.leasePricingInformationContainer = document.querySelector('#' + idPrefix + 'lease_pricing_information');
        this.commissionPricingInformationContainer = document.querySelector('#' + idPrefix + 'commission_pricing_information');
        this.depositPricingInformationContainer = document.querySelector('#' + idPrefix + 'deposit_pricing_information');

        this.showOrHideCommissionPricingInformationByCommissionable();

        this.commissionableCheckboxField.mdcFormField.listen('change', (): void => this.showOrHideCommissionPricingInformationByCommissionable());
    }

    public showOrHideCommissionPricingInformationByCommissionable(): void {
        if (this.commissionableCheckboxField.isChecked() === true) {
            this.showCommissionPricingInformationContainer();
        } else {
            this.hideCommissionPricingInformationContainer();
        }
    }

    public showPurchasePricingInformationContainer(): void {
        this.purchasePricingInformationContainer.classList.remove('d-none');
    }

    public hidePurchasePricingInformationContainer(): void {
        this.purchasePricingInformationContainer.classList.add('d-none');
    }

    public showRentPricingInformationContainer(): void {
        this.rentPricingInformationContainer.classList.remove('d-none');
    }

    public hideRentPricingInformationContainer(): void {
        this.rentPricingInformationContainer.classList.add('d-none');
    }

    public showLeasePricingInformationContainer(): void {
        this.leasePricingInformationContainer.classList.remove('d-none');
    }

    public hideLeasePricingInformationContainer(): void {
        this.leasePricingInformationContainer.classList.add('d-none');
    }

    public showCommissionPricingInformationContainer(): void {
        this.commissionPricingInformationContainer.classList.remove('d-none');
    }

    public hideCommissionPricingInformationContainer(): void {
        this.commissionPricingInformationContainer.classList.add('d-none');
    }

    public showDepositPricingInformationContainer(): void {
        this.depositPricingInformationContainer.classList.remove('d-none');
    }

    public hideDepositPricingInformationContainer(): void {
        this.depositPricingInformationContainer.classList.add('d-none');
    }

    public markMatchingRelevantFields(): void {
        this.purchasePriceNetTextField.markAsMatchingRelevant();
        this.purchasePricePerSquareMeterTextField.markAsMatchingRelevant();
        this.netColdRentTextField.markAsMatchingRelevant();
        this.coldRentTextField.markAsMatchingRelevant();
        this.rentalPricePerSquareMeterTextField.markAsMatchingRelevant();
    }

    public markExposeRelevantFields(): void {
        this.depositTextField.markAsExposeRelevant();
        this.commissionableCheckboxField.markAsExposeRelevant();
    }

    public isFormValid(): boolean {
        let hasErrors: boolean = false;

        if (isNaN(Number(this.purchasePriceNetTextField.mdcTextField.value.replace(',','.'))) === true) {
            this.purchasePriceNetTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.purchasePriceGrossTextField.mdcTextField.value.replace(',','.'))) === true) {
            this.purchasePriceGrossTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.purchasePricePerSquareMeterTextField.mdcTextField.value.replace(',','.'))) === true) {
            this.purchasePricePerSquareMeterTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.netColdRentTextField.mdcTextField.value.replace(',','.'))) === true) {
            this.netColdRentTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.coldRentTextField.mdcTextField.value.replace(',','.'))) === true) {
            this.coldRentTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.rentIncludingHeatingTextField.mdcTextField.value.replace(',','.'))) === true) {
            this.rentIncludingHeatingTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.additionalCostsTextField.mdcTextField.value.replace(',','.'))) === true) {
            this.additionalCostsTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.rentalPricePerSquareMeterTextField.mdcTextField.value.replace(',','.'))) === true) {
            this.rentalPricePerSquareMeterTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.leaseTextField.mdcTextField.value.replace(',','.'))) === true) {
            this.leaseTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.leaseholdTextField.mdcTextField.value.replace(',','.'))) === true) {
            this.leaseholdTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.houseMoneyTextField.mdcTextField.value.replace(',','.'))) === true) {
            this.houseMoneyTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.commissionNetTextField.mdcTextField.value.replace(',','.'))) === true) {
            this.commissionNetTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.commissionGrossTextField.mdcTextField.value.replace(',','.'))) === true) {
            this.commissionGrossTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.depositTextField.mdcTextField.value.replace(',','.'))) === true) {
            this.depositTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        return (hasErrors === false);
    }

}

export { PropertyPricingInformationForm };
