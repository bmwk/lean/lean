import { TextFieldComponent } from '../../../Component/TextFieldComponent';

class PhotoForm {

    private readonly titleTextField: TextFieldComponent;

    constructor(idPrefix: string) {
        this.titleTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'title_mdc_text_field'));
    }

}

export { PhotoForm };
