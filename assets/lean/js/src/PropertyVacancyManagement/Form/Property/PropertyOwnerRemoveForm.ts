import { DeleteForm } from '../../../Form/DeleteForm';

class PropertyOwnerRemoveForm extends DeleteForm {

    constructor(idPrefix: string) {
        super(idPrefix);

        this.verificationCodeTextField.mdcTextField.required = true;
    }

    public isFormValid(): boolean {
        let hasErrors: boolean = false;

        if (this.verificationCodeTextField.mdcTextField.valid === false) {
            this.verificationCodeTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        return (hasErrors === false);
    }

}

export { PropertyOwnerRemoveForm };
