import { PersonSelectOrPersonCreateForm } from '../../../PersonManagement/Form/Person/PersonSelectOrPersonCreateForm';
import { IndustryNaceClassificationComponent } from '../../Component/IndustryNaceClassification/IndustryNaceClassificationComponent';
import { TextFieldComponent } from '../../../Component/TextFieldComponent';
import { SelectComponent } from '../../../Component/SelectComponent';
import { CheckboxFieldComponent } from '../../../Component/CheckboxFieldComponent';

class PropertyUserForm {

    private readonly personSelectOrPersonCreateForm: PersonSelectOrPersonCreateForm = null;
    private readonly propertyUserStatus: SelectComponent;
    private readonly propertyRelationship: SelectComponent;
    private readonly isSubtenantCheckboxField: CheckboxFieldComponent;
    private readonly endOfRentalTextField: TextFieldComponent;
    private readonly industryNaceClassificationComponent: IndustryNaceClassificationComponent;
    private readonly industryNameTextField: TextFieldComponent;
    private readonly sectionTextField: TextFieldComponent;
    private readonly branchCheckboxField: CheckboxFieldComponent;
    private readonly branchTypeSelect: SelectComponent;
    private readonly mainAssortmentTextField: TextFieldComponent;
    private readonly sideAssortmentTextField: TextFieldComponent;
    private readonly openingHoursTextField: TextFieldComponent;
    private readonly familybusinessCheckboxField: CheckboxFieldComponent;
    private readonly deliveryServiceTextField: TextFieldComponent;
    private readonly ratingTextField: TextFieldComponent;
    private readonly websiteTextField: TextFieldComponent;
    private readonly googleBusinessMdcTextField: TextFieldComponent;
    private readonly facebookTextField: TextFieldComponent;
    private readonly instagramTextField: TextFieldComponent;
    private readonly newsletterRecipientCheckboxField: CheckboxFieldComponent;

    constructor(idPrefix: string) {
        if (PersonSelectOrPersonCreateForm.checkFormExists(idPrefix + 'personSelectOrPersonCreate_') === true) {
            this.personSelectOrPersonCreateForm = new PersonSelectOrPersonCreateForm(idPrefix + 'personSelectOrPersonCreate_');
        }

        this.propertyUserStatus = new SelectComponent(document.querySelector('#' + idPrefix + 'propertyUserStatus_mdc_select'));
        this.propertyRelationship = new SelectComponent(document.querySelector('#' + idPrefix + 'propertyRelationship_mdc_select'));
        this.isSubtenantCheckboxField = new CheckboxFieldComponent(document.querySelector('#' + idPrefix + 'isSubtenant_mdc_form_field'));
        this.endOfRentalTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'endOfRental_mdc_text_field'), {datePicker: true});
        this.industryNaceClassificationComponent = new IndustryNaceClassificationComponent(idPrefix);
        this.industryNameTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'industryName_mdc_text_field'));
        this.sectionTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'section_mdc_text_field'));
        this.branchCheckboxField = new CheckboxFieldComponent(document.querySelector('#' + idPrefix + 'branch_mdc_form_field'));
        this.branchTypeSelect = new SelectComponent(document.querySelector('#' + idPrefix + 'branchType_mdc_select'),{clearButton: true});
        this.mainAssortmentTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'mainAssortment_mdc_text_field'));
        this.sideAssortmentTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'sideAssortment_mdc_text_field'));
        this.openingHoursTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'openingHours_mdc_text_field'), {autoFitTextarea: true});
        this.familybusinessCheckboxField = new CheckboxFieldComponent(document.querySelector('#' + idPrefix + 'familybusiness_mdc_form_field'));
        this.deliveryServiceTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'deliveryService_mdc_text_field'));
        this.ratingTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'rating_mdc_text_field'));
        this.websiteTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'website_mdc_text_field'));
        this.googleBusinessMdcTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'googleBusiness_mdc_text_field'));
        this.facebookTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'facebook_mdc_text_field'));
        this.instagramTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'instagram_mdc_text_field'));
        this.newsletterRecipientCheckboxField = new CheckboxFieldComponent(document.querySelector('#' + idPrefix + 'receiveNewsletter_mdc_form_field'));

        this.industryNameTextField.mdcTextField.disabled = true;
        this.sectionTextField.mdcTextField.disabled = true;

        this.showOrHideBranchTypeMdcSelect();

        this.branchCheckboxField.mdcFormField.listen('change', (): void => this.showOrHideBranchTypeMdcSelect());

        this.propertyUserStatus.mdcSelect.required = true;
        this.propertyRelationship.mdcSelect.required = true;
    }

    public showOrHideBranchTypeMdcSelect(): void {
        if (this.branchCheckboxField.isChecked() === true) {
            this.showBranchTypeMdcSelect()
        } else {
            this.hideBranchTypeMdcSelect();
        }
    }

    public showBranchTypeMdcSelect(): void {
        this.branchTypeSelect.mdcSelect.root.classList.remove('d-none');
    }

    public hideBranchTypeMdcSelect(): void {
        this.branchTypeSelect.mdcSelect.root.classList.add('d-none');
    }

    public isFormValid(): boolean {
        let hasErrors: boolean = false;

        if (this.propertyUserStatus.mdcSelect.value.length === 0) {
            this.propertyUserStatus.mdcSelect.valid = false;
            hasErrors = true;
        }

        if (this.propertyRelationship.mdcSelect.value.length === 0) {
            this.propertyRelationship.mdcSelect.valid = false;
            hasErrors = true;
        }

        if (this.personSelectOrPersonCreateForm.isFormValid() === false) {
            hasErrors = true;
        }

        if (this.industryNaceClassificationComponent.isFormValid() === false) {
            hasErrors = true;
        }

        return (hasErrors === false);
    }

    public disableFields(): void {
        this.industryNaceClassificationComponent.disableFields();
        this.personSelectOrPersonCreateForm.disableFields();
        this.propertyUserStatus.mdcSelect.disabled = true;
        this.propertyRelationship.mdcSelect.disabled = true;
        this.isSubtenantCheckboxField.mdcCheckbox.disabled = true;
        this.endOfRentalTextField.mdcTextField.disabled = true;
        this.branchCheckboxField.mdcCheckbox.disabled = true;
        this.branchTypeSelect.mdcSelect.disabled = true;
        this.mainAssortmentTextField.mdcTextField.disabled = true;
        this.sideAssortmentTextField.mdcTextField.disabled = true;
        this.openingHoursTextField.mdcTextField.disabled = true;
        this.familybusinessCheckboxField.mdcCheckbox.disabled = true;
        this.deliveryServiceTextField.mdcTextField.disabled = true;
        this.ratingTextField.mdcTextField.disabled = true;
        this.websiteTextField.mdcTextField.disabled = true;
        this.googleBusinessMdcTextField.mdcTextField.disabled = true;
        this.facebookTextField.mdcTextField.disabled = true;
        this.instagramTextField.mdcTextField.disabled = true;
        this.newsletterRecipientCheckboxField.mdcCheckbox.disabled = true;
    }

}

export { PropertyUserForm };
