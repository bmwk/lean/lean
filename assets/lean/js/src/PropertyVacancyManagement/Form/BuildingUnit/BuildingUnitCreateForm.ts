import { PropertyUserCreateComponent } from '../../Component/Property/PropertyUserCreateComponent';
import { BuildingUnitForm } from './BuildingUnitForm';

class BuildingUnitCreateForm extends BuildingUnitForm {

    private readonly propertyUserCreateComponent: PropertyUserCreateComponent;

    constructor(idPrefix: string) {
        super(idPrefix);

        this.propertyUserCreateComponent = new PropertyUserCreateComponent(idPrefix);
    }

    public isFormValid(): boolean {
        return (
            super.isFormValid() === true
            && this.propertyUserCreateComponent.isFormValid() === true
        ) === true;
    }

}

export { BuildingUnitCreateForm };
