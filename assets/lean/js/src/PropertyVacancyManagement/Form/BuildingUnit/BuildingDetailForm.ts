import { TextFieldComponent } from '../../../Component/TextFieldComponent';
import { SelectComponent } from '../../../Component/SelectComponent';
import { MultiselectComponent } from '../../../Component/MultiselectComponent';
import { LocationCategory } from '../../../Domain/Entity/LocationCategory';
import { LocationType } from '../../../Domain/Entity/LocationType';
import { CheckboxFieldComponent } from '../../../Component/CheckboxFieldComponent';

class BuildingDetailForm {

    private readonly locationTypeSelect: SelectComponent;
    private readonly locationCategorySelect: SelectComponent;
    private readonly locationFactorsMultiSelect: MultiselectComponent;
    private readonly buildingTypeSelect: SelectComponent;
    private readonly roofShapeSelect: SelectComponent;
    private readonly buildingConditionSelect: SelectComponent;
    private readonly buildingStandardSelect: SelectComponent;
    private readonly constructionYearTextField: TextFieldComponent;
    private readonly buildingBlockCodeTextField: TextFieldComponent;
    private readonly numberOfFloorsTextField: TextFieldComponent;
    private readonly esgCertificateAvailableCheckboxField: CheckboxFieldComponent;
    private readonly esgContentContainer: HTMLDivElement;
    private readonly esgCertificateMultiselect: MultiselectComponent;
    private readonly esgCertificateDetailInformationTextField: TextFieldComponent;
    private readonly useCalculatedLocationCategoryAction: HTMLAnchorElement;
    private readonly useCalculatedLocationCategoryContainer: HTMLDivElement;

    constructor(idPrefix: string) {
        this.locationTypeSelect = new SelectComponent(document.querySelector('#' + idPrefix + 'locationType_mdc_select'), {clearButton: true});
        this.locationCategorySelect = new SelectComponent(document.querySelector('#' + idPrefix + 'locationCategory_mdc_select'), {clearButton: true});
        this.locationFactorsMultiSelect = new MultiselectComponent(document.querySelector('#' + idPrefix + 'locationFactors_multiselect'));
        this.buildingTypeSelect = new SelectComponent(document.querySelector('#' + idPrefix + 'buildingType_mdc_select'), {clearButton: true});
        this.roofShapeSelect = new SelectComponent(document.querySelector('#' + idPrefix + 'roofShape_mdc_select'), {clearButton: true});
        this.buildingConditionSelect = new SelectComponent(document.querySelector('#' + idPrefix + 'buildingCondition_mdc_select'), {clearButton: true});
        this.buildingStandardSelect = new SelectComponent(document.querySelector('#' + idPrefix + 'buildingStandard_mdc_select'), {clearButton: true});
        this.constructionYearTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'constructionYear_mdc_text_field'));
        this.buildingBlockCodeTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'buildingBlockCode_mdc_text_field'));
        this.numberOfFloorsTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'numberOfFloors_mdc_text_field'));
        this.esgCertificateAvailableCheckboxField = new CheckboxFieldComponent(document.querySelector('#' + idPrefix + 'esgCertificateAvailable_mdc_form_field'));
        this.esgContentContainer = document.querySelector('#' + idPrefix + 'esgContent');
        this.esgCertificateMultiselect = new MultiselectComponent(document.querySelector('#' + idPrefix + 'esgCertificates_multiselect'));
        this.esgCertificateDetailInformationTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'esgCertificateDetailInformation_mdc_text_field'));

        if (document.querySelector('#' + idPrefix + 'useCalculatedLocationCategoryContainer') !== null) {
            this.useCalculatedLocationCategoryAction = document.querySelector('#' + idPrefix + 'useCalculatedLocationCategoryAction');
            this.useCalculatedLocationCategoryContainer = document.querySelector('#' + idPrefix + 'useCalculatedLocationCategoryContainer');

            this.useCalculatedLocationCategoryAction.addEventListener('click', (mouseEvent: MouseEvent): void => {
                mouseEvent.preventDefault();

                this.locationCategorySelect.mdcSelect.value = this.useCalculatedLocationCategoryAction.dataset.locationCategoryValue;

                this.useCalculatedLocationCategoryContainer.classList.add('d-none');
            });
        }

        this.showOrHideEsgContent();

        this.handleLocationTypeChange(null);

        this.esgCertificateAvailableCheckboxField.mdcCheckbox.listen('change', (): void => this.showOrHideEsgContent());
        this.locationTypeSelect.mdcSelect.listen('MDCSelect:change', (customEvent: CustomEvent): void => this.handleLocationTypeChange(Number(customEvent.detail.value)));
    }

    public showOrHideEsgContent(): void {
        if (this.esgCertificateAvailableCheckboxField.mdcCheckbox.checked === true) {
            this.esgContentContainer.classList.remove('d-none');
        } else {
            this.esgContentContainer.classList.add('d-none');
        }
    }

    public handleLocationTypeChange(locationType: LocationType): void {
        switch (locationType) {
            case LocationType.InnerCity:
                this.disableLocationCategorySelectValues([
                    LocationCategory.TwoALocation,
                    LocationCategory.TwoBLocation,
                    LocationCategory.TwoCLocation,
                    LocationCategory.Other
                ]);

                this.enableLocationCategorySelectValues([
                    LocationCategory.OneALocation,
                    LocationCategory.OneBLocation,
                    LocationCategory.OneCLocation
                ]);
                break;
            case LocationType.CityDistrict:
                this.disableLocationCategorySelectValues([
                    LocationCategory.OneALocation,
                    LocationCategory.OneBLocation, LocationCategory.OneCLocation,
                    LocationCategory.Other
                ]);

                this.enableLocationCategorySelectValues([
                    LocationCategory.TwoALocation,
                    LocationCategory.TwoBLocation,
                    LocationCategory.TwoCLocation
                ]);
                break;
            case LocationType.Peripheral:
                this.disableLocationCategorySelectValues([
                    LocationCategory.OneALocation,
                    LocationCategory.OneBLocation,
                    LocationCategory.OneCLocation,
                    LocationCategory.TwoALocation,
                    LocationCategory.TwoBLocation,
                    LocationCategory.TwoCLocation
                ]);

                this.enableLocationCategorySelectValues([LocationCategory.Other]);
                break;
            case LocationType.HighFrequencySpecialLocation:
                this.disableLocationCategorySelectValues([
                    LocationCategory.OneALocation,
                    LocationCategory.OneBLocation,
                    LocationCategory.OneCLocation,
                    LocationCategory.TwoALocation,
                    LocationCategory.TwoBLocation,
                    LocationCategory.TwoCLocation
                ]);

                this.enableLocationCategorySelectValues([LocationCategory.Other]);
                break;
            case LocationType.Solitary:
                this.disableLocationCategorySelectValues([
                    LocationCategory.OneALocation,
                    LocationCategory.OneBLocation,
                    LocationCategory.OneCLocation,
                    LocationCategory.TwoALocation,
                    LocationCategory.TwoBLocation,
                    LocationCategory.TwoCLocation
                ]);

                this.enableLocationCategorySelectValues([LocationCategory.Other]);
                break;
            default:
                this.enableLocationCategorySelectValues([
                    LocationCategory.OneALocation,
                    LocationCategory.OneBLocation,
                    LocationCategory.OneCLocation,
                    LocationCategory.TwoALocation,
                    LocationCategory.TwoBLocation,
                    LocationCategory.TwoCLocation,
                    LocationCategory.Other
                ]);
                break;
        }
    }

    public markMatchingRelevantFields(): void {
        this.locationTypeSelect.markAsMatchingRelevant();
        this.locationCategorySelect.markAsMatchingRelevant();
        this.locationFactorsMultiSelect.markAsMatchingRelevant()
    }

    public markExposeRelevantFields(): void {
        this.locationCategorySelect.markAsExposeRelevant();
    }

    public isFormValid(): boolean {
        let hasErrors: boolean = false;

        if (isNaN(Number(this.constructionYearTextField.mdcTextField.value))) {
            this.constructionYearTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.numberOfFloorsTextField.mdcTextField.value))) {
            this.numberOfFloorsTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        return (hasErrors === false);
    }

    private disableLocationCategorySelectValues(locationCategories: LocationCategory[]): void {
        locationCategories.forEach((locationCategory: LocationCategory) => {
            const mdcListItem: HTMLDivElement = this.locationCategorySelect.mdcSelect.root.querySelector('.mdc-list-item[data-value="' + locationCategory + '"]');

            if (Number(this.locationCategorySelect.mdcSelect.value) === locationCategory) {
                this.locationCategorySelect.mdcSelect.value = null;
            }

            mdcListItem.classList.add('mdc-list-item--disabled');
            mdcListItem.classList.add('text-light');
        });
    }

    private enableLocationCategorySelectValues(locationCategories: LocationCategory[]): void {
        locationCategories.forEach((locationCategory: LocationCategory) => {
            const mdcListItem = this.locationCategorySelect.mdcSelect.root.querySelector('.mdc-list-item[data-value="' + locationCategory + '"]');

            mdcListItem.classList.remove('mdc-list-item--disabled');
            mdcListItem.classList.remove('text-light');
        });
    }

}

export { BuildingDetailForm };
