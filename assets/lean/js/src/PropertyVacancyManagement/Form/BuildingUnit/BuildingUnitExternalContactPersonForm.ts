import { TextFieldComponent } from '../../../Component/TextFieldComponent';
import { SelectComponent } from '../../../Component/SelectComponent';
import { FormFieldValidator } from '../../../FormFieldValidator/FormFieldValidator';

class BuildingUnitExternalContactPersonForm {

    private readonly salutationSelect: SelectComponent;
    private readonly salutationLetterTextField: TextFieldComponent;
    private readonly personTitleTextField: TextFieldComponent;
    private readonly firstNameTextField: TextFieldComponent;
    private readonly lastNameTextField: TextFieldComponent;
    private readonly companyNameTextField: TextFieldComponent;
    private readonly streetNameTextField: TextFieldComponent;
    private readonly houseNumberTextField: TextFieldComponent;
    private readonly postalCodeTextField: TextFieldComponent;
    private readonly placeNameTextField: TextFieldComponent;
    private readonly additionalAddressInformationTextField: TextFieldComponent;
    private readonly emailTextField: TextFieldComponent;
    private readonly mobilePhoneNumberTextField: TextFieldComponent;
    private readonly phoneNumberTextField: TextFieldComponent;
    private readonly faxNumberTextField: TextFieldComponent;
    private readonly websiteTextField: TextFieldComponent;
    private readonly postOfficeBoxTextField: TextFieldComponent;
    private readonly postOfficeBoxPostalCodeTextField: TextFieldComponent;
    private readonly postOfficeBoxPlaceNameTextField: TextFieldComponent;

    constructor(idPrefix: string) {
        this.salutationSelect = new SelectComponent(document.querySelector('#' + idPrefix + 'salutation_mdc_select'));
        this.salutationLetterTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'salutationLetter_mdc_text_field'));
        this.personTitleTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'personTitle_mdc_text_field'));
        this.firstNameTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'firstName_mdc_text_field'));
        this.lastNameTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'lastName_mdc_text_field'));
        this.companyNameTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'companyName_mdc_text_field'));
        this.streetNameTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'streetName_mdc_text_field'));
        this.houseNumberTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'houseNumber_mdc_text_field'));
        this.postalCodeTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'postalCode_mdc_text_field'));
        this.placeNameTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'placeName_mdc_text_field'));
        this.additionalAddressInformationTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'additionalAddressInformation_mdc_text_field'));
        this.emailTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'email_mdc_text_field'));
        this.mobilePhoneNumberTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'mobilePhoneNumber_mdc_text_field'));
        this.phoneNumberTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'phoneNumber_mdc_text_field'));
        this.faxNumberTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'faxNumber_mdc_text_field'));
        this.websiteTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'website_mdc_text_field'));
        this.postOfficeBoxTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'postOfficeBox_mdc_text_field'));
        this.postOfficeBoxPostalCodeTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'postOfficeBoxPostalCode_mdc_text_field'));
        this.postOfficeBoxPlaceNameTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'postOfficeBoxPlaceName_mdc_text_field'));
    }

    public isFormValid(): boolean {
        let hasErrors: boolean = false;

        if (this.emailTextField.mdcTextField.value !== '' && FormFieldValidator.validateEmail(this.emailTextField.mdcTextField.value) === false) {
            this.emailTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        return hasErrors === false;
    }

}

export { BuildingUnitExternalContactPersonForm };
