import { SelectComponent } from '../../../Component/SelectComponent';

class BuildingUnitOpenImmoExportInitializationForm {

    private readonly exportActionTypeSelect: SelectComponent;

    constructor(idPrefix: string) {
        this.exportActionTypeSelect = new SelectComponent(document.querySelector('#' + idPrefix + 'exportActionType_mdc_select'));

        this.exportActionTypeSelect.mdcSelect.required = true;

    }

    public isFormValid(): boolean {
        let hasErrors: boolean = false;

        if (this.exportActionTypeSelect.mdcSelect.valid === false) {
            this.exportActionTypeSelect.mdcSelect.valid = false;
            hasErrors = true;
        }

        return (hasErrors === false);
    }

}

export { BuildingUnitOpenImmoExportInitializationForm };
