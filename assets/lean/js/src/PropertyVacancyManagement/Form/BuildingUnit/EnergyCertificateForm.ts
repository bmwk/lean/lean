import { SelectComponent } from '../../../Component/SelectComponent';
import { TextFieldComponent } from '../../../Component/TextFieldComponent';

class EnergyCertificateForm {

    private readonly energyCertificateTypeSelect: SelectComponent;
    private readonly issueDateTextField: TextFieldComponent;
    private readonly expirationDateTextField: TextFieldComponent;
    private readonly buildingConstructionYearTextField: TextFieldComponent;
    private readonly energyEfficiencyClassSelect: SelectComponent;
    private readonly withHotWaterSelect: SelectComponent;
    private readonly electricityConsumptionTextField: TextFieldComponent;
    private readonly heatingEnergyConsumptionTextField: TextFieldComponent;
    private readonly energyConsumptionTextField: TextFieldComponent;
    private readonly electricityDemandTextField: TextFieldComponent;
    private readonly heatingEnergyDemandTextField: TextFieldComponent;
    private readonly energyDemandTextField: TextFieldComponent;

    constructor(idPrefix: string) {
        this.energyCertificateTypeSelect = new SelectComponent(document.querySelector('#' + idPrefix + 'energyCertificateType_mdc_select'));
        this.issueDateTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'issueDate_mdc_text_field'), {datePicker: true});
        this.expirationDateTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'expirationDate_mdc_text_field'), {datePicker: true});
        this.buildingConstructionYearTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'buildingConstructionYear_mdc_text_field'));
        this.energyEfficiencyClassSelect = new SelectComponent(document.querySelector('#' + idPrefix + 'energyEfficiencyClass_mdc_select'));
        this.withHotWaterSelect = new SelectComponent(document.querySelector('#' + idPrefix + 'withHotWater_mdc_select'));
        this.electricityConsumptionTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'electricityConsumption_mdc_text_field'));
        this.heatingEnergyConsumptionTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'heatingEnergyConsumption_mdc_text_field'));
        this.energyConsumptionTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'energyConsumption_mdc_text_field'));
        this.electricityDemandTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'electricityDemand_mdc_text_field'));
        this.heatingEnergyDemandTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'heatingEnergyDemand_mdc_text_field'));
        this.energyDemandTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'energyDemand_mdc_text_field'));

        this.hideEnergyConsumptionCertificateFields();
        this.hideEnergyRequirementCertificateFields();
        this.displayOrHideFieldsBySelectedEnergyCertificateType();

        this.energyCertificateTypeSelect.mdcSelect.listen('MDCSelect:change', (): void => this.displayOrHideFieldsBySelectedEnergyCertificateType());
    }

    private showEnergyRequirementCertificateFields(): void {
        this.electricityDemandTextField.mdcTextField.root.classList.remove('d-none');
        this.heatingEnergyDemandTextField.mdcTextField.root.classList.remove('d-none');
        this.energyDemandTextField.mdcTextField.root.classList.remove('d-none');
    }

    private hideEnergyRequirementCertificateFields(): void {
        this.electricityDemandTextField.mdcTextField.root.classList.add('d-none');
        this.heatingEnergyDemandTextField.mdcTextField.root.classList.add('d-none');
        this.energyDemandTextField.mdcTextField.root.classList.add('d-none');
    }

    private showEnergyConsumptionCertificateFields(): void {
        this.electricityConsumptionTextField.mdcTextField.root.classList.remove('d-none');
        this.heatingEnergyConsumptionTextField.mdcTextField.root.classList.remove('d-none');
        this.energyConsumptionTextField.mdcTextField.root.classList.remove('d-none');
    }

    private hideEnergyConsumptionCertificateFields(): void {
        this.electricityConsumptionTextField.mdcTextField.root.classList.add('d-none');
        this.heatingEnergyConsumptionTextField.mdcTextField.root.classList.add('d-none');
        this.energyConsumptionTextField.mdcTextField.root.classList.add('d-none');
    }

    private displayOrHideFieldsBySelectedEnergyCertificateType(): void {
        if (this.energyCertificateTypeSelect.mdcSelect.value === '0') {
            this.hideEnergyConsumptionCertificateFields();
            this.showEnergyRequirementCertificateFields();
        } else if (this.energyCertificateTypeSelect.mdcSelect.value === '1') {
            this.hideEnergyRequirementCertificateFields();
            this.showEnergyConsumptionCertificateFields();
        }
    }

    public isFormValid(): boolean {
        let hasErrors: boolean = false;

        if (isNaN(Number(this.buildingConstructionYearTextField.mdcTextField.value))) {
            this.buildingConstructionYearTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.energyDemandTextField.mdcTextField.value.replace(',','.')))) {
            this.energyDemandTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.electricityDemandTextField.mdcTextField.value.replace(',','.')))) {
            this.electricityDemandTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.heatingEnergyDemandTextField.mdcTextField.value.replace(',','.')))) {
            this.heatingEnergyDemandTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.energyConsumptionTextField.mdcTextField.value.replace(',','.')))) {
            this.energyConsumptionTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.electricityConsumptionTextField.mdcTextField.value.replace(',','.')))) {
            this.electricityConsumptionTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.heatingEnergyConsumptionTextField.mdcTextField.value.replace(',','.')))) {
            this.heatingEnergyConsumptionTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        return (hasErrors === false);
    }

}

export { EnergyCertificateForm };
