import { BuildingUnitForm } from './BuildingUnitForm';

class BuildingUnitBasicDataForm extends BuildingUnitForm {

    constructor(idPrefix: string) {
        super(idPrefix);
    }

}

export { BuildingUnitBasicDataForm };
