import { DeleteForm } from '../../../Form/DeleteForm';

class BuildingUnitDeleteForm extends DeleteForm{

    constructor(idPrefix: string) {
        super(idPrefix);
    }

}

export { BuildingUnitDeleteForm };
