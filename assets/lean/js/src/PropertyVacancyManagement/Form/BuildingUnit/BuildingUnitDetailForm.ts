import { BuildingDetailForm } from './BuildingDetailForm';
import { EnergyCertificateForm } from './EnergyCertificateForm';
import { PropertySpacesDataForm } from '../Property/PropertySpacesDataForm';
import { TextFieldComponent } from '../../../Component/TextFieldComponent';
import { SelectComponent } from '../../../Component/SelectComponent';
import { CheckboxFieldComponent } from '../../../Component/CheckboxFieldComponent';
import { MDCSwitch } from '@material/switch';
import { MDCTooltip } from '@material/tooltip';

class BuildingUnitDetailForm {

    private readonly buildingDetailForm: BuildingDetailForm;
    private readonly heatingMethodSelect: SelectComponent;
    private readonly numberOfParkingLotsTextField: TextFieldComponent;
    private readonly entranceDoorWidthTextField: TextFieldComponent;
    private readonly barrierFreeAccessSelect: SelectComponent;
    private readonly wheelchairAccessibleCheckboxField: CheckboxFieldComponent;
    private readonly rampPresentCheckboxField: CheckboxFieldComponent;
    private readonly propertySpacesDataForm: PropertySpacesDataForm;
    private readonly energyCertificateForm: EnergyCertificateForm;
    private readonly particularitiesTextField: TextFieldComponent;
    private readonly inFloorsTextField: TextFieldComponent;
    private readonly neighbourBusinessTextField: TextFieldComponent;
    private readonly passantFrequencyGeneratorTextField: TextFieldComponent;
    private readonly interimUsePossibleInputField: HTMLInputElement;
    private readonly interimUsePossibleMdcSwitch: MDCSwitch;
    private readonly interimUseDetailsContainer: HTMLDivElement;
    private readonly interimUseInformationTextField: TextFieldComponent;
    private readonly energyCertificateIsAvailableInputField: HTMLInputElement;
    private readonly energyCertificateIsAvailableMdcSwitch: MDCSwitch;
    private readonly energyCertificateDetailsContainer: HTMLDivElement;
    private readonly objectForeclosedInputField: HTMLInputElement;
    private readonly objectForeclosedMdcSwitch: MDCSwitch;
    private readonly objectForeclosureDetailsContainer: HTMLDivElement;
    private readonly fileNumberForeclosureTextField: TextFieldComponent;
    private readonly auctionDateTextField: TextFieldComponent;
    private readonly responsibleLocalCourtTextField: TextFieldComponent;
    private readonly shopWidthTextField: TextFieldComponent;
    private readonly shopWindowAvailableCheckboxField: CheckboxFieldComponent;
    private readonly shopWindowContentContainer: HTMLDivElement;
    private readonly shopWindowFrontWidthTextField: TextFieldComponent;
    private readonly groundLevelSalesArea: CheckboxFieldComponent;
    private readonly showroomAvailable: CheckboxFieldComponent;
    private readonly mdcTooltips: MDCTooltip[];

    constructor(idPrefix: string) {
        this.buildingDetailForm = new BuildingDetailForm(idPrefix + 'building_');
        this.heatingMethodSelect = new SelectComponent(document.querySelector('#' + idPrefix + 'heatingMethod_mdc_select'), {clearButton: true});
        this.numberOfParkingLotsTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'numberOfParkingLots_mdc_text_field'));
        this.entranceDoorWidthTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'entranceDoorWidth_mdc_text_field'));
        this.barrierFreeAccessSelect = new SelectComponent(document.querySelector('#' + idPrefix + 'barrierFreeAccess_mdc_select'), {clearButton: true});
        this.wheelchairAccessibleCheckboxField = new CheckboxFieldComponent(document.querySelector('#' + idPrefix + 'wheelchairAccessible_mdc_form_field'));
        this.rampPresentCheckboxField = new CheckboxFieldComponent(document.querySelector('#' + idPrefix + 'rampPresent_mdc_form_field'));
        this.propertySpacesDataForm = new PropertySpacesDataForm(idPrefix + 'propertySpacesData_');
        this.energyCertificateForm = new EnergyCertificateForm(idPrefix + 'energyCertificate_');
        this.particularitiesTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'particularities_mdc_text_field'), {autoFitTextarea: true});
        this.inFloorsTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'inFloors_mdc_text_field'));
        this.neighbourBusinessTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'neighbourBusiness_mdc_text_field'));
        this.passantFrequencyGeneratorTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'passantFrequencyGenerator_mdc_text_field'));
        this.interimUsePossibleInputField = document.querySelector('#' + idPrefix + 'interimUsePossible');
        this.interimUsePossibleMdcSwitch = new MDCSwitch(document.querySelector('#' + idPrefix + 'interim_use_possible_mdc_switch'));
        this.interimUseDetailsContainer = document.querySelector('#' + idPrefix + 'interim_use_details');
        this.interimUseInformationTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'interimUseInformation_mdc_text_field'), {autoFitTextarea: true});
        this.energyCertificateIsAvailableInputField = document.querySelector('#' + idPrefix + 'energyCertificateIsAvailable');
        this.energyCertificateIsAvailableMdcSwitch = new MDCSwitch(document.querySelector('#' + idPrefix + 'energy_certificate_is_available_mdc_switch'));
        this.energyCertificateDetailsContainer = document.querySelector('#' + idPrefix + 'energy_certificate_details');
        this.objectForeclosedInputField = document.querySelector('#' + idPrefix + 'objectForeclosed');
        this.objectForeclosedMdcSwitch = new MDCSwitch(document.querySelector('#' + idPrefix + 'object_foreclosed_mdc_switch'));
        this.objectForeclosureDetailsContainer = document.querySelector('#' + idPrefix + 'object_foreclosure_details');
        this.fileNumberForeclosureTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'fileNumberForeclosure_mdc_text_field'));
        this.auctionDateTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'auctionDate_mdc_text_field'), {datePicker: true});
        this.responsibleLocalCourtTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'responsibleLocalCourt_mdc_text_field'));
        this.shopWindowAvailableCheckboxField = new CheckboxFieldComponent(document.querySelector('#' + idPrefix + 'shopWindowAvailable_mdc_form_field'));
        this.shopWindowContentContainer = document.querySelector('#' + idPrefix + 'shopWindowContent');
        this.shopWindowFrontWidthTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'shopWindowFrontWidth_mdc_text_field'));
        this.shopWidthTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'shopWidth_mdc_text_field'));
        this.groundLevelSalesArea = new CheckboxFieldComponent(document.querySelector('#' + idPrefix + 'groundLevelSalesArea_mdc_form_field'));
        this.showroomAvailable = new CheckboxFieldComponent(document.querySelector('#' + idPrefix + 'showroomAvailable_mdc_form_field'));
        this.mdcTooltips = [];

        this.mdcTooltips.push(new MDCTooltip(document.querySelector('#retailSpace-mdc-tooltip')));
        this.mdcTooltips.push(new MDCTooltip(document.querySelector('#subsidiarySpace-mdc-tooltip')));
        this.mdcTooltips.push(new MDCTooltip(document.querySelector('#storageSpace-mdc-tooltip')));
        this.mdcTooltips.push(new MDCTooltip(document.querySelector('#storeSpace-mdc-tooltip')));
        this.mdcTooltips.push(new MDCTooltip(document.querySelector('#officeSpace-mdc-tooltip')));
        this.mdcTooltips.push(new MDCTooltip(document.querySelector('#officePartSpace-mdc-tooltip')));
        this.mdcTooltips.push(new MDCTooltip(document.querySelector('#administrationSpace-mdc-tooltip')));
        this.mdcTooltips.push(new MDCTooltip(document.querySelector('#gastronomySpace-mdc-tooltip')));
        this.mdcTooltips.push(new MDCTooltip(document.querySelector('#productionSpace-mdc-tooltip')));
        this.mdcTooltips.push(new MDCTooltip(document.querySelector('#plotSize-mdc-tooltip')));
        this.mdcTooltips.push(new MDCTooltip(document.querySelector('#openSpace-mdc-tooltip')));
        this.mdcTooltips.push(new MDCTooltip(document.querySelector('#usableSpace-mdc-tooltip')));

        if (localStorage.getItem('lean.property.matchingRelevantFields') === 'true') {
            this.markMatchingRelevantFields();
        }

        if (localStorage.getItem('lean.property.exposeRelevantFields') === 'true') {
            this.markExposeRelevantFields();
        }

        this.setInterimUsePossibleState();
        this.setEnergyCertificateIsAvailableState();
        this.setObjectForeclosureState();
        this.showOrHideShopWindowWidth();

        this.shopWindowAvailableCheckboxField.mdcFormField.listen('change', (): void => this.showOrHideShopWindowWidth());

        this.interimUsePossibleMdcSwitch.listen('click', (): void => {
            if (this.interimUsePossibleMdcSwitch.selected === true) {
                this.interimUsePossibleInputField.value = '1';
                this.interimUseDetailsContainer.classList.remove('d-none');
            } else {
                this.interimUsePossibleInputField.value = '0';
                this.interimUseDetailsContainer.classList.add('d-none');
            }
        });

        this.energyCertificateIsAvailableMdcSwitch.listen('click', (): void => {
            if (this.energyCertificateIsAvailableMdcSwitch.selected === true) {
                this.energyCertificateIsAvailableInputField.value = '1';
                this.energyCertificateDetailsContainer.classList.remove('d-none');
            } else {
                this.energyCertificateIsAvailableInputField.value = '0';
                this.energyCertificateDetailsContainer.classList.add('d-none');
            }
        });

        this.objectForeclosedMdcSwitch.listen('click', (): void => {
            if (this.objectForeclosedMdcSwitch.selected === true) {
                this.objectForeclosedInputField.value = '1';
                this.objectForeclosureDetailsContainer.classList.remove('d-none');
            } else {
                this.objectForeclosedInputField.value = '0';
                this.objectForeclosureDetailsContainer.classList.add('d-none');
            }
        });
    }

    public showOrHideShopWindowWidth(): void {
        if (this.shopWindowAvailableCheckboxField.isChecked() === true) {
            this.shopWindowContentContainer.classList.remove('d-none');
        } else {
            this.shopWindowContentContainer.classList.add('d-none');
        }
    }

    public markMatchingRelevantFields(): void {
        this.buildingDetailForm.markMatchingRelevantFields();
        this.propertySpacesDataForm.markMatchingRelevantFields();
        this.shopWidthTextField.markAsMatchingRelevant();
        this.shopWindowFrontWidthTextField.markAsMatchingRelevant();
        this.barrierFreeAccessSelect.markAsMatchingRelevant();
        this.groundLevelSalesArea.markAsMatchingRelevant();
        this.numberOfParkingLotsTextField.markAsMatchingRelevant();
    }

    public markExposeRelevantFields(): void {
        this.buildingDetailForm.markExposeRelevantFields();
        this.propertySpacesDataForm.markExposeRelevantFields();
        this.shopWindowFrontWidthTextField.markAsExposeRelevant();
        this.barrierFreeAccessSelect.markAsExposeRelevant();
        this.groundLevelSalesArea.markAsExposeRelevant();
        this.showroomAvailable.markAsExposeRelevant();
        this.numberOfParkingLotsTextField.markAsExposeRelevant();
        this.inFloorsTextField.markAsExposeRelevant();
    }

    public isFormValid(): boolean {
        let hasErrors: boolean = false;

        if (isNaN(Number(this.numberOfParkingLotsTextField.mdcTextField.value)) === true) {
            this.numberOfParkingLotsTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (this.buildingDetailForm.isFormValid() === false) {
            hasErrors = true;
        }

        if (this.propertySpacesDataForm.isFormValid() === false) {
            hasErrors = true;
        }

        if (this.energyCertificateForm.isFormValid() === false) {
            hasErrors = true;
        }

        return (hasErrors === false);
    }

    private setInterimUsePossibleState(): void {
        this.interimUsePossibleMdcSwitch.selected = this.interimUsePossibleInputField.value === '1';

        if (this.interimUsePossibleInputField.value === '1') {
            this.interimUseDetailsContainer.classList.remove('d-none');
        } else {
            this.interimUseDetailsContainer.classList.add('d-none');
        }
    }

    private setEnergyCertificateIsAvailableState(): void {
        this.energyCertificateIsAvailableMdcSwitch.selected = this.energyCertificateIsAvailableInputField.value === '1';

        if (this.energyCertificateIsAvailableInputField.value === '1') {
            this.energyCertificateDetailsContainer.classList.remove('d-none');
        } else {
            this.energyCertificateDetailsContainer.classList.add('d-none');
        }
    }

    private setObjectForeclosureState(): void {
        this.objectForeclosedMdcSwitch.selected = this.objectForeclosedInputField.value === '1';

        if (this.objectForeclosedInputField.value === '1') {
            this.objectForeclosureDetailsContainer.classList.remove('d-none');
        } else {
            this.objectForeclosureDetailsContainer.classList.add('d-none');
        }
    }

}

export { BuildingUnitDetailForm };
