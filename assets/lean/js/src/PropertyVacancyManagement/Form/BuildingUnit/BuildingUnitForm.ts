import { AddressForm } from '../Property/AddressForm';
import { TextFieldComponent } from '../../../Component/TextFieldComponent';
import { SelectComponent } from '../../../Component/SelectComponent';
import { CheckboxFieldComponent } from '../../../Component/CheckboxFieldComponent';

abstract class BuildingUnitForm {

    protected readonly nameTextField: TextFieldComponent;
    protected readonly internalNumberTextField: TextFieldComponent;
    protected readonly areaSizeTextField: TextFieldComponent;
    protected readonly descriptionTextField: TextFieldComponent;
    protected readonly objectIsEmptyCheckboxField: CheckboxFieldComponent;
    protected readonly objectIsEmptySinceTextField: TextFieldComponent;
    protected readonly objectBecomesEmptyCheckboxField: CheckboxFieldComponent;
    protected readonly objectBecomesEmptyFromTextField: TextFieldComponent;
    protected readonly reuseAgreedCheckboxField: CheckboxFieldComponent;
    protected readonly reuseFromTextField: TextFieldComponent;
    protected readonly pastUsageWrapper: HTMLDivElement;
    protected readonly pastUsageIndustryClassificationSelect: SelectComponent;
    protected readonly pastPropertyUsageBusinessTypeSelect: SelectComponent;
    protected readonly pastUsageDescriptionTextField: TextFieldComponent;
    protected readonly futureUsageWrapper: HTMLDivElement;
    protected readonly futureUsageIndustryClassificationSelect: SelectComponent;
    protected readonly futurePropertyUsageBusinessTypeSelect: SelectComponent;
    protected readonly futureUsageDescriptionTextField: TextFieldComponent;
    protected readonly currentUsageWrapper: HTMLDivElement;
    protected readonly currentUsageIndustryClassificationSelect: SelectComponent;
    protected readonly currentPropertyUsageBusinessTypeSelect: SelectComponent;
    protected readonly currentUsageDescriptionTextField: TextFieldComponent;
    protected readonly vacancyReasonSelect: SelectComponent;
    protected readonly vacancyReasonWrapper: HTMLDivElement;
    protected readonly keyPropertyCheckboxField: CheckboxFieldComponent;
    protected readonly internalNoteTextField: TextFieldComponent;
    protected readonly placeDescriptionTextField: TextFieldComponent;
    protected readonly addressForm: AddressForm;

    protected constructor(idPrefix: string) {
        this.nameTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'name_mdc_text_field'));
        this.internalNumberTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'internalNumber_mdc_text_field'));
        this.areaSizeTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'areaSize_mdc_text_field'));
        this.descriptionTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'description_mdc_text_field'), {autoFitTextarea: true});
        this.objectIsEmptyCheckboxField = new CheckboxFieldComponent(document.querySelector('#' + idPrefix + 'objectIsEmpty_mdc_form_field'));
        this.objectIsEmptySinceTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'objectIsEmptySince_mdc_text_field'), {datePicker: true});
        this.objectBecomesEmptyCheckboxField = new CheckboxFieldComponent(document.querySelector('#' + idPrefix + 'objectBecomesEmpty_mdc_form_field'));
        this.objectBecomesEmptyFromTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'objectBecomesEmptyFrom_mdc_text_field'), {datePicker: true});
        this.reuseAgreedCheckboxField = new CheckboxFieldComponent(document.querySelector('#' + idPrefix + 'reuseAgreed_mdc_form_field'));
        this.reuseFromTextField =new TextFieldComponent( document.querySelector('#' + idPrefix + 'reuseFrom_mdc_text_field'), {datePicker: true});
        this.pastUsageWrapper = document.querySelector('#' + idPrefix + 'past_usage_wrapper');
        this.pastUsageIndustryClassificationSelect = new SelectComponent(document.querySelector('#' + idPrefix + 'pastUsageIndustryClassification_mdc_select'), {clearButton: true});
        this.pastPropertyUsageBusinessTypeSelect = new SelectComponent(document.querySelector('#' + idPrefix + 'pastPropertyUsageBusinessType_mdc_select'), {clearButton: true});
        this.pastUsageDescriptionTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'pastUsageDescription_mdc_text_field'), {autoFitTextarea: true});
        this.currentUsageWrapper = document.querySelector('#' + idPrefix + 'current_usage_wrapper');
        this.currentPropertyUsageBusinessTypeSelect = new SelectComponent(document.querySelector('#' + idPrefix + 'currentPropertyUsageBusinessType_mdc_select'), {clearButton: true});
        this.currentUsageIndustryClassificationSelect = new SelectComponent(document.querySelector('#' + idPrefix + 'currentUsageIndustryClassification_mdc_select'), {clearButton: true});
        this.currentUsageDescriptionTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'currentUsageDescription_mdc_text_field'), {autoFitTextarea: true});
        this.futureUsageWrapper = document.querySelector('#' + idPrefix + 'future_usage_wrapper');
        this.futurePropertyUsageBusinessTypeSelect = new SelectComponent(document.querySelector('#' + idPrefix + 'futurePropertyUsageBusinessType_mdc_select'), {clearButton: true});
        this.futureUsageIndustryClassificationSelect = new SelectComponent(document.querySelector('#' + idPrefix + 'futureUsageIndustryClassification_mdc_select'), {clearButton: true});
        this.futureUsageDescriptionTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'futureUsageDescription_mdc_text_field'), {autoFitTextarea: true});
        this.vacancyReasonSelect = new SelectComponent(document.querySelector('#' + idPrefix + 'vacancyReason_mdc_select'), {clearButton: true});
        this.vacancyReasonWrapper = document.querySelector('#' + idPrefix + 'vacancy_reason_wrapper');
        this.keyPropertyCheckboxField = new CheckboxFieldComponent(document.querySelector('#' + idPrefix + 'keyProperty_mdc_form_field'));
        this.internalNoteTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'internalNote_mdc_text_field'));
        this.placeDescriptionTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'placeDescription_mdc_text_field'), {autoFitTextarea: true});
        this.addressForm = new AddressForm(idPrefix + 'address_');

        this.objectIsEmptySinceTextField.setMaxDate(new Date());
        this.objectBecomesEmptyFromTextField.setMinDate(new Date());

        if (localStorage.getItem('lean.property.matchingRelevantFields') === 'true') {
            this.markMatchingRelevantFields();
        }

        if (localStorage.getItem('lean.property.exposeRelevantFields') === 'true') {
            this.markExposeRelevantFields();
        }

        this.hideObjectIsEmptySinceMdcTextField();
        this.hideObjectBecomesEmptyFromMdcTextField();
        this.hideReuseFromMdcTextField();
        this.hidePastUsageWrapper();
        this.hideFutureUsageWrapper();

        if (this.objectIsEmptyCheckboxField.isChecked() === true) {
            this.showObjectIsEmptySinceMdcTextField();
            this.hideObjectBecomesEmptyFromMdcTextField();
            this.showPastUsageWrapper();
            this.hideCurrentUsageWrapper()
            this.hideReuseFromMdcTextField();
            this.hideFutureUsageWrapper();
            this.showVacancyReasonWrapper();
        }

        if (this.objectBecomesEmptyCheckboxField.isChecked() === true) {
            this.showObjectBecomesEmptyFromMdcTextField();
            this.hideObjectIsEmptySinceMdcTextField();
            this.showCurrentUsageWrapper();
            this.hidePastUsageWrapper();
            this.hideReuseFromMdcTextField();
            this.hideFutureUsageWrapper();
            this.showVacancyReasonWrapper();
        }

        if (this.reuseAgreedCheckboxField.isChecked() === true) {
            this.hideObjectBecomesEmptyFromMdcTextField();
            this.hideObjectIsEmptySinceMdcTextField();
            this.hideCurrentUsageWrapper();
            this.hidePastUsageWrapper();
            this.showReuseFromMdcTextField();
            this.showFutureUsageWrapperDiv();
            this.hideVacancyReasonWrapper();
        }

        this.objectIsEmptyCheckboxField.mdcFormField.listen('change', (event: Event): void => {
            if (this.objectIsEmptyCheckboxField.isChecked() === true) {
                this.showObjectIsEmptySinceMdcTextField();
                this.hideObjectBecomesEmptyFromMdcTextField();
                this.showPastUsageWrapper();
                this.hideCurrentUsageWrapper();
                this.hideReuseFromMdcTextField();
                this.hideFutureUsageWrapper();
                this.showVacancyReasonWrapper();

                this.objectBecomesEmptyCheckboxField.mdcCheckbox.checked = false;
                this.reuseAgreedCheckboxField.mdcCheckbox.checked = false;
            } else {
                this.hideObjectIsEmptySinceMdcTextField();
                this.hidePastUsageWrapper();
                this.showCurrentUsageWrapper();
                this.hideVacancyReasonWrapper();
            }
        });

        this.objectBecomesEmptyCheckboxField.mdcFormField.listen('change', (event: Event): void => {
            if (this.objectBecomesEmptyCheckboxField.isChecked() === true) {
                this.showObjectBecomesEmptyFromMdcTextField();
                this.hideObjectIsEmptySinceMdcTextField();
                this.showCurrentUsageWrapper();
                this.hidePastUsageWrapper();
                this.hideReuseFromMdcTextField();
                this.hideFutureUsageWrapper();
                this.showVacancyReasonWrapper();

                this.objectIsEmptyCheckboxField.mdcCheckbox.checked = false;
                this.reuseAgreedCheckboxField.mdcCheckbox.checked = false;
            } else {
                this.hideObjectBecomesEmptyFromMdcTextField();
                this.hideVacancyReasonWrapper();
            }
        });

        this.reuseAgreedCheckboxField.mdcFormField.listen('change', (event: Event): void => {
            if (this.reuseAgreedCheckboxField.isChecked() === true) {
                this.hideObjectBecomesEmptyFromMdcTextField();
                this.hideObjectIsEmptySinceMdcTextField();
                this.hideCurrentUsageWrapper();
                this.hidePastUsageWrapper();
                this.showReuseFromMdcTextField();
                this.showFutureUsageWrapperDiv();
                this.hideVacancyReasonWrapper();

                this.objectIsEmptyCheckboxField.mdcCheckbox.checked = false;
                this.objectBecomesEmptyCheckboxField.mdcCheckbox.checked = false;
            } else {
                this.hideReuseFromMdcTextField();
                this.hideFutureUsageWrapper();
                this.showCurrentUsageWrapper();
                this.hideVacancyReasonWrapper();
            }
        });

        this.nameTextField.mdcTextField.required = true;
    }

    public hideObjectIsEmptySinceMdcTextField(): void {
        this.objectIsEmptySinceTextField.mdcTextField.root.classList.add('d-none');
    }

    public showObjectIsEmptySinceMdcTextField(): void {
        this.objectIsEmptySinceTextField.mdcTextField.root.classList.remove('d-none');
    }

    public hideObjectBecomesEmptyFromMdcTextField(): void {
        this.objectBecomesEmptyFromTextField.mdcTextField.root.classList.add('d-none');
    }

    public showObjectBecomesEmptyFromMdcTextField(): void {
        this.objectBecomesEmptyFromTextField.mdcTextField.root.classList.remove('d-none');
    }

    public hidePastUsageWrapper(): void {
        this.pastUsageWrapper.classList.add('d-none');
    }

    public showPastUsageWrapper(): void {
        this.pastUsageWrapper.classList.remove('d-none');
    }

    public hideCurrentUsageWrapper(): void {
        this.currentUsageWrapper.classList.add('d-none');
    }

    public showCurrentUsageWrapper(): void {
        this.currentUsageWrapper.classList.remove('d-none');
    }

    public hideReuseFromMdcTextField(): void {
        this.reuseFromTextField.mdcTextField.root.classList.add('d-none');
    }

    public showReuseFromMdcTextField(): void {
        this.reuseFromTextField.mdcTextField.root.classList.remove('d-none');
    }

    public hideFutureUsageWrapper(): void {
        this.futureUsageWrapper.classList.add('d-none');
    }

    public showFutureUsageWrapperDiv(): void {
        this.futureUsageWrapper.classList.remove('d-none');
    }

    public hideVacancyReasonWrapper(): void {
        this.vacancyReasonWrapper.classList.add('d-none');
    }

    public showVacancyReasonWrapper(): void {
        this.vacancyReasonWrapper.classList.remove('d-none');
    }

    public markMatchingRelevantFields(): void {
        this.areaSizeTextField.markAsMatchingRelevant();
    }

    public markExposeRelevantFields(): void {
        this.addressForm.markExposeRelevantFields();
        this.areaSizeTextField.markAsExposeRelevant();
        this.internalNumberTextField.markAsExposeRelevant();
        this.descriptionTextField.markAsExposeRelevant();
        this.placeDescriptionTextField.markAsExposeRelevant();
    }

    public isFormValid(): boolean {
        let hasErrors: boolean = false;

        if (this.nameTextField.mdcTextField.valid === false) {
            this.nameTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.areaSizeTextField.mdcTextField.value.replace(',','.'))) === true) {
            this.areaSizeTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (this.addressForm.isFormValid() === false) {
            hasErrors = true;
        }

        return (hasErrors === false);
    }

}

export { BuildingUnitForm };
