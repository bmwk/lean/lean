import { SelectComponent } from '../../../Component/SelectComponent';

class BuildingUnitAssignToAccountUserForm {

    private readonly assignedToAccountUserSelect: SelectComponent;

    constructor(idPrefix: string) {
        this.assignedToAccountUserSelect = new SelectComponent(document.querySelector('#' + idPrefix + 'assignedToAccountUser_mdc_select'),{clearButton: true});

        this.assignedToAccountUserSelect.mdcSelect.required = false;

    }

    public isFormValid(): boolean {
        let hasErrors: boolean = false;

        if (this.assignedToAccountUserSelect.mdcSelect.valid === false) {
            this.assignedToAccountUserSelect.mdcSelect.valid = false;
            hasErrors = true;
        }

        return (hasErrors === false);
    }

}

export { BuildingUnitAssignToAccountUserForm };
