import { SearchFieldComponent } from '../../../Component/SearchFieldComponent';

class BuildingUnitSearchForm {

    private readonly searchFieldComponent: SearchFieldComponent;

    constructor(idPrefix: string) {
        this.searchFieldComponent = new SearchFieldComponent(idPrefix, 'text');
    }

}

export { BuildingUnitSearchForm };
