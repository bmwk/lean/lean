import { DeleteForm } from '../../../Form/DeleteForm';

class PropertyVacancyReportDeleteForm extends DeleteForm {

    constructor(idPrefix: string) {
        super(idPrefix);
    }

}

export { PropertyVacancyReportDeleteForm };
