import { BuildingUnitCreateForm } from '../BuildingUnit/BuildingUnitCreateForm';
import { PropertyOwnerCreateComponent } from '../../Component/Property/PropertyOwnerCreateComponent';
import { TextFieldComponent } from '../../../Component/TextFieldComponent';
import { SelectComponent } from '../../../Component/SelectComponent';

class ReportToBuildingUnitForm extends BuildingUnitCreateForm {

    private readonly propertyOwnerCreateComponent: PropertyOwnerCreateComponent;
    private readonly plotSizeTextField: TextFieldComponent;
    private readonly retailSpaceTextField: TextFieldComponent;
    private readonly subsidiarySpaceTextField: TextFieldComponent;
    private readonly gastronomySpaceTextField: TextFieldComponent;
    private readonly livingSpaceTextField: TextFieldComponent;
    private readonly usableOutdoorAreaPossibilitySelect: SelectComponent;
    private readonly outdoorAreaDetailsContainer: HTMLDivElement;
    private readonly usableOutdoorAreaTextField: TextFieldComponent;
    private readonly numberOfParkingLotsTextField: TextFieldComponent;
    private readonly barrierFreeAccessSelect: SelectComponent;
    private readonly shopWindowFrontWidthTextField: TextFieldComponent;
    private readonly shopWidthTextField: TextFieldComponent;
    private readonly locationCategorySelect: SelectComponent;
    private readonly buildingConditionSelect: SelectComponent;
    private readonly propertyOfferTypeSelect: SelectComponent;

    constructor(idPrefix: string) {
        super(idPrefix);

        this.propertyOwnerCreateComponent = new PropertyOwnerCreateComponent(idPrefix);
        this.plotSizeTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'propertySpacesData_plotSize_mdc_text_field'));
        this.retailSpaceTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'propertySpacesData_retailSpace_mdc_text_field'));
        this.subsidiarySpaceTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'propertySpacesData_subsidiarySpace_mdc_text_field'));
        this.gastronomySpaceTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'propertySpacesData_gastronomySpace_mdc_text_field'));
        this.livingSpaceTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'propertySpacesData_livingSpace_mdc_text_field'));
        this.usableOutdoorAreaPossibilitySelect = new SelectComponent(document.querySelector('#' + idPrefix + 'propertySpacesData_usableOutdoorAreaPossibility_mdc_select'));
        this.outdoorAreaDetailsContainer = document.querySelector('#' + idPrefix + 'propertySpacesData_outdoor_area_details');
        this.usableOutdoorAreaTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'propertySpacesData_usableOutdoorArea_mdc_text_field'));
        this.numberOfParkingLotsTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'numberOfParkingLots_mdc_text_field'));
        this.barrierFreeAccessSelect = new SelectComponent(document.querySelector('#' + idPrefix + 'barrierFreeAccess_mdc_select'));
        this.shopWindowFrontWidthTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'shopWindowFrontWidth_mdc_text_field'));
        this.shopWidthTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'shopWidth_mdc_text_field'));
        this.locationCategorySelect = new SelectComponent(document.querySelector('#' + idPrefix + 'building_locationCategory_mdc_select'));
        this.buildingConditionSelect = new SelectComponent(document.querySelector('#' + idPrefix + 'building_buildingCondition_mdc_select'));
        this.propertyOfferTypeSelect = new SelectComponent(document.querySelector('#' + idPrefix + 'propertyMarketingInformation_propertyOfferType_mdc_select'));

        this.showOrHideUsableOutdoorAreaDetails();

        this.usableOutdoorAreaPossibilitySelect.mdcSelect.listen('MDCSelect:change', (): void => this.showOrHideUsableOutdoorAreaDetails());
    }

    private showOrHideUsableOutdoorAreaDetails(): void {
        if (
            this.usableOutdoorAreaPossibilitySelect.mdcSelect.value === '0'
            || this.usableOutdoorAreaPossibilitySelect.mdcSelect.value === '1'
        ) {
            this.outdoorAreaDetailsContainer.classList.remove('d-none');
        } else {
            this.outdoorAreaDetailsContainer.classList.add('d-none');
        }
    }

    public isFormValid(): boolean {
        let hasErrors: boolean = false;

        if (super.isFormValid() === false) {
            hasErrors = true;
        }

        if (this.propertyOwnerCreateComponent.isFormValid() == false) {
            hasErrors = true;
        }

        if (isNaN(Number(this.plotSizeTextField.mdcTextField.value.replace(',','.'))) === true) {
            this.plotSizeTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.retailSpaceTextField.mdcTextField.value.replace(',','.'))) === true) {
            this.retailSpaceTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.subsidiarySpaceTextField.mdcTextField.value.replace(',','.'))) === true) {
            this.subsidiarySpaceTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.gastronomySpaceTextField.mdcTextField.value.replace(',','.'))) === true) {
            this.gastronomySpaceTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.livingSpaceTextField.mdcTextField.value.replace(',','.'))) === true) {
            this.livingSpaceTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.shopWindowFrontWidthTextField.mdcTextField.value.replace(',','.'))) === true) {
            this.shopWindowFrontWidthTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.shopWidthTextField.mdcTextField.value.replace(',','.'))) === true) {
            this.shopWidthTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        return (hasErrors === false);
    }

}

export { ReportToBuildingUnitForm };
