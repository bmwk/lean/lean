import { DeleteForm } from '../../../Form/DeleteForm';

class PropertyVacancyReportNoteDeleteForm extends DeleteForm {

    constructor(idPrefix: string) {
        super(idPrefix);
    }

}

export { PropertyVacancyReportNoteDeleteForm };
