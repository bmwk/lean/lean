import { PropertyExcelImportListComponent } from '../../Component/PropertyExcelImport/PropertyExcelImportListComponent';

class OverviewPage {

    private readonly propertyExcelImportListComponent: PropertyExcelImportListComponent;

    constructor() {
        this.propertyExcelImportListComponent = new PropertyExcelImportListComponent('property_excel_import_');
    }

}

export { OverviewPage };
