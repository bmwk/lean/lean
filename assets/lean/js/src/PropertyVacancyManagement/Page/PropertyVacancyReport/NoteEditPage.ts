import { PropertyVacancyReportNoteForm } from '../../Form/PropertyVacancyReport/PropertyVacancyReportNoteForm';
import { MdcContextMenuComponent } from '../../../Component/MdcContextMenuComponent';

class NoteEditPage {

    private readonly propertyVacancyReportNoteForm: PropertyVacancyReportNoteForm;
    private readonly propertyVacancyReportNoteFormElement: HTMLFormElement;
    private readonly propertyVacancyReportNoteFormSubmitButton: HTMLButtonElement;
    private readonly contextMenu: MdcContextMenuComponent = null;

    constructor() {
        const idPrefix: string = 'property_vacancy_report_note_';

        this.propertyVacancyReportNoteForm = new PropertyVacancyReportNoteForm(idPrefix);
        this.propertyVacancyReportNoteFormElement = document.querySelector('#' + idPrefix + 'form');
        this.propertyVacancyReportNoteFormSubmitButton = document.querySelector('#' + idPrefix + 'save_submit_button');

        if (MdcContextMenuComponent.checkContainerExists(idPrefix) === true) {
            this.contextMenu = new MdcContextMenuComponent(idPrefix);
        }

        if (this.propertyVacancyReportNoteFormElement !== null)  {
            this.propertyVacancyReportNoteFormSubmitButton.addEventListener('click', (): void => {
                this.propertyVacancyReportNoteFormElement.submit();
            });
        }
    }

}

export { NoteEditPage };
