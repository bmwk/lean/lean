import { PropertyVacancyReportDeleteForm } from '../../Form/PropertyVacancyReport/PropertyVacancyReportDeleteForm';

class DeletePage {

    private readonly propertyVacancyReportDeleteForm: PropertyVacancyReportDeleteForm;
    private readonly propertyVacancyReportDeleteFormElement: HTMLFormElement;
    private readonly propertyVacancyReportDeleteFormSubmitButton: HTMLButtonElement;

    constructor() {
        const idPrefix: string = 'property_vacancy_report_delete_';

        this.propertyVacancyReportDeleteForm = new PropertyVacancyReportDeleteForm(idPrefix);
        this.propertyVacancyReportDeleteFormElement = document.querySelector('#' + idPrefix + 'form');
        this.propertyVacancyReportDeleteFormSubmitButton = document.querySelector('#' + idPrefix + 'submit_button');

        this.propertyVacancyReportDeleteFormSubmitButton.addEventListener('click', (): void => {
            this.propertyVacancyReportDeleteFormElement.submit();
        });
    }

}

export { DeletePage };
