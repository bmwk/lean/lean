import { PropertyVacancyReportNoteDeleteForm } from '../../Form/PropertyVacancyReport/PropertyVacancyReportNoteDeleteForm';
import { PropertyVacancyReportNoteForm } from '../../Form/PropertyVacancyReport/PropertyVacancyReportNoteForm';

class NoteDeletePage {

    private readonly propertyVacancyReportNoteForm: PropertyVacancyReportNoteForm;
    private readonly propertyVacancyReportNoteDeleteForm: PropertyVacancyReportNoteDeleteForm;
    private readonly propertyVacancyReportNoteDeleteFormElement: HTMLFormElement;
    private readonly propertyVacancyReportNoteDeleteFormSubmitButton: HTMLButtonElement;

    constructor() {
        const idPrefix: string = 'property_vacancy_report_note_delete_';

        this.propertyVacancyReportNoteForm = new PropertyVacancyReportNoteForm('property_vacancy_report_note_');

        this.propertyVacancyReportNoteDeleteForm = new PropertyVacancyReportNoteDeleteForm(idPrefix);
        this.propertyVacancyReportNoteDeleteFormElement = document.querySelector('#' + idPrefix + 'form');
        this.propertyVacancyReportNoteDeleteFormSubmitButton = document.querySelector('#' + idPrefix + 'submit_button');

        this.propertyVacancyReportNoteForm.disableFields();

        this.propertyVacancyReportNoteDeleteFormSubmitButton.addEventListener('click', (): void => {
            this.propertyVacancyReportNoteDeleteFormElement.submit();
        });
    }

}

export { NoteDeletePage };
