enum OverviewPageTab {
    UnprocessedOverview = 'property_vacancy_report_unprocessed_overview_tab',
    ProcessedOverview = 'property_vacancy_report_processed_overview_tab',
}

export { OverviewPageTab };
