import { MdcContextMenuComponent } from '../../../Component/MdcContextMenuComponent';

class ReportPage {

    private readonly contextMenu: MdcContextMenuComponent = null;

    constructor() {
        if (MdcContextMenuComponent.checkContainerExists('property_vacancy_report_report_') === true) {
            this.contextMenu = new MdcContextMenuComponent('property_vacancy_report_report_');
        }
    }

}

export { ReportPage };
