import { OverviewPage } from './OverviewPage';
import { OverviewPageTab } from './OverviewPageTab';
import { LazyImageLoader } from '../../../LazyImageLoader';

class ProcessedOverviewPage extends OverviewPage {

    private readonly lazyImageLoader: LazyImageLoader;

    constructor() {
        super(OverviewPageTab.ProcessedOverview);

        this.lazyImageLoader = new LazyImageLoader();
    }

}

export { ProcessedOverviewPage };
