import { OverviewPage } from './OverviewPage';
import { OverviewPageTab } from './OverviewPageTab';
import { LazyImageLoader } from '../../../LazyImageLoader';

class UnprocessedOverviewPage extends OverviewPage {

    private readonly lazyImageLoader: LazyImageLoader;

    constructor() {
        super(OverviewPageTab.UnprocessedOverview);

        this.lazyImageLoader = new LazyImageLoader();
    }

}

export { UnprocessedOverviewPage };
