import { ReportToBuildingUnitForm } from '../../Form/PropertyVacancyReport/ReportToBuildingUnitForm';
import { MessageBoxComponent } from '../../../Component/MessageBoxComponent/MessageBoxComponent';
import { MessageBoxAlertType } from '../../../Component/MessageBoxComponent/MessageBoxAlertType';

class ReportToBuildingUnitPage {

    private readonly reportToBuildingUnitForm: ReportToBuildingUnitForm;
    private readonly reportToBuildingUnitFormElement: HTMLFormElement;
    private readonly reportToBuildingUnitFormSubmitButton: HTMLButtonElement;
    private readonly messageBoxComponent: MessageBoxComponent;

    constructor() {
        const idPrefix: string = 'report_to_building_unit_';

        this.reportToBuildingUnitForm = new ReportToBuildingUnitForm(idPrefix);
        this.reportToBuildingUnitFormElement = document.querySelector('#' + idPrefix + 'form');
        this.reportToBuildingUnitFormSubmitButton = document.querySelector('#' + idPrefix + 'save_submit_button');
        this.messageBoxComponent = new MessageBoxComponent();

        this.reportToBuildingUnitFormSubmitButton.addEventListener('click', (): void => {
            if (this.reportToBuildingUnitForm.isFormValid() === false) {
                this.messageBoxComponent.openMessageBox('Die Daten konnten nicht gespeichert werden. Bitte kontrollieren Sie die rot markierten Felder.', MessageBoxAlertType.DANGER);

                return;
            }

            this.reportToBuildingUnitFormElement.submit();
        });
    }

}

export { ReportToBuildingUnitPage };
