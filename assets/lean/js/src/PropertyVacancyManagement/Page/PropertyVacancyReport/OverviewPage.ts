import { MdcTabBarPage } from '../../../MdcTabBarPage';
import { OverviewPageTab } from './OverviewPageTab';

abstract class OverviewPage extends MdcTabBarPage {

    protected constructor(overviewPageTab: OverviewPageTab) {
        super(document.querySelector('#property_vacancy_report_overview_tab_bar'));

        this.activateTab(overviewPageTab);
    }

}

export { OverviewPage };
