import { BuildingUnitPage } from './BuildingUnitPage';
import { BuildingUnitPageTab } from './BuildingUnitPageTab';
import { OccurrenceForm } from '../../../PersonManagement/Form/Occurrence/OccurrenceForm';
import { OccurrenceAttachmentComponent } from '../../../PersonManagement/Component/Occurrence/OccurrenceAttachmentComponent';
import { MdcContextMenuComponent } from '../../../Component/MdcContextMenuComponent';
import { MessageBoxComponent } from '../../../Component/MessageBoxComponent/MessageBoxComponent';
import { MessageBoxAlertType } from '../../../Component/MessageBoxComponent/MessageBoxAlertType';

abstract class OccurrenceEditPage extends BuildingUnitPage {

    private readonly occurrenceForm: OccurrenceForm;
    private readonly occurrenceFormElement: HTMLFormElement;
    private readonly occurrenceFormSubmitButton: HTMLButtonElement;
    private readonly occurrenceAttachmentComponent: OccurrenceAttachmentComponent = null;
    private readonly contextMenu: MdcContextMenuComponent = null;
    private readonly messageBoxComponent: MessageBoxComponent;

    protected constructor(buildingUnitPageTab: BuildingUnitPageTab, idPrefix: string, occurrenceForm: OccurrenceForm) {
        super(buildingUnitPageTab);

        this.occurrenceForm = occurrenceForm;
        this.occurrenceFormElement = document.querySelector('#' + idPrefix + 'form');
        this.occurrenceFormSubmitButton = document.querySelector('#' + idPrefix + 'save_submit_button');
        this.occurrenceAttachmentComponent = new OccurrenceAttachmentComponent(idPrefix);

        if (MdcContextMenuComponent.checkContainerExists(idPrefix) === true) {
            this.contextMenu = new MdcContextMenuComponent(idPrefix);
        }

        this.messageBoxComponent = new MessageBoxComponent();

        if (this.occurrenceFormElement !== null) {
            this.occurrenceFormSubmitButton.addEventListener('click', (): void => {
                if (this.occurrenceForm.isFormValid() === false) {
                    this.messageBoxComponent.openMessageBox('Die Daten konnten nicht gespeichert werden. Bitte kontrollieren Sie die rot markierten Felder.', MessageBoxAlertType.DANGER);

                    return;
                }

                this.occurrenceFormElement.submit();
            });
        }
    }

}

export { OccurrenceEditPage };
