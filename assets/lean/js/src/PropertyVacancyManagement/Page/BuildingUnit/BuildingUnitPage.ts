import { MdcTabBarPage } from '../../../MdcTabBarPage';
import { BuildingUnitPageTab } from './BuildingUnitPageTab';

abstract class BuildingUnitPage extends MdcTabBarPage{

    protected constructor(buildingUnitPageTab: BuildingUnitPageTab) {
        super(document.querySelector('#building_unit_page_tab_bar'));

        this.activateTab(buildingUnitPageTab);
    }

}

export { BuildingUnitPage };
