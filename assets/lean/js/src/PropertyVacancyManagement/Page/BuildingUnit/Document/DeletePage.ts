import { BuildingUnitPage } from '../BuildingUnitPage';
import { BuildingUnitPageTab } from '../BuildingUnitPageTab';
import { DocumentForm } from '../../../Form/Property/DocumentForm';
import { DocumentDeleteForm } from '../../../Form/Property/DocumentDeleteForm';

class DeletePage extends BuildingUnitPage {

    private readonly documentForm: DocumentForm;
    private readonly documentDeleteForm: DocumentDeleteForm;
    private readonly documentDeleteFormElement: HTMLFormElement;
    private readonly documentDeleteFormSubmitButton: HTMLButtonElement;

    constructor() {
        super(BuildingUnitPageTab.Document);

        const idPrefix: string = 'document_delete_';

        this.documentForm = new DocumentForm('document_');
        this.documentDeleteForm = new DocumentDeleteForm(idPrefix);
        this.documentDeleteFormElement = document.querySelector('#' + idPrefix + 'form');
        this.documentDeleteFormSubmitButton = document.querySelector('#' + idPrefix + 'delete_submit_button');

        this.documentDeleteFormSubmitButton.addEventListener('click', (): void => {
            this.documentDeleteFormElement.submit();
        });
    }

}

export { DeletePage };
