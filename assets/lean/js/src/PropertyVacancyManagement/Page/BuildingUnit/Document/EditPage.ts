import { BuildingUnitPage } from '../BuildingUnitPage';
import { BuildingUnitPageTab } from '../BuildingUnitPageTab';
import { DocumentForm } from '../../../Form/Property/DocumentForm';
import { MdcContextMenuComponent } from '../../../../Component/MdcContextMenuComponent';

class EditPage extends BuildingUnitPage {

    private readonly documentForm: DocumentForm;
    private readonly documentFormElement: HTMLFormElement;
    private readonly documentFormSubmitButton: HTMLButtonElement;
    private readonly contextMenu: MdcContextMenuComponent = null;

    constructor() {
        super(BuildingUnitPageTab.Document);

        const idPrefix: string = 'document_';

        this.documentForm = new DocumentForm(idPrefix);
        this.documentFormElement = document.querySelector('#' + idPrefix + 'form');
        this.documentFormSubmitButton = document.querySelector('#' + idPrefix + 'save_submit_button');

        if (MdcContextMenuComponent.checkContainerExists(idPrefix) === true) {
            this.contextMenu = new MdcContextMenuComponent(idPrefix);
        }

        if (this.documentFormElement !== null) {
            this.documentFormSubmitButton.addEventListener('click', (): void => {
                this.documentFormElement.submit();
            });
        }
    }

}

export { EditPage };
