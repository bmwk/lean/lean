import { BuildingUnitPage } from '../BuildingUnitPage';
import { BuildingUnitPageTab } from '../BuildingUnitPageTab';
import { DropzoneUploadComponent } from '../../../../Component/DropzoneUploadComponent';

class OverviewPage extends BuildingUnitPage {

    private readonly dropzoneContainer: HTMLDivElement;
    private readonly dropzoneUploadComponent: DropzoneUploadComponent = null;

    constructor() {
        super(BuildingUnitPageTab.Document);

        this.dropzoneContainer = document.querySelector('#document_upload_dropzone_container');

        if (this.dropzoneContainer !== null) {
            const acceptedFiles: string[] = [
                'application/pdf',
                'application/msword',
                'application/msexcel',
                'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            ];

            this.dropzoneUploadComponent = new DropzoneUploadComponent(
                this.dropzoneContainer,
                document.querySelector('#document_upload_submit_button'),
                'documentFile',
                acceptedFiles
            );
        }
    }

}

export { OverviewPage };
