import { BuildingUnitPage } from './BuildingUnitPage';
import { BuildingUnitPageTab } from './BuildingUnitPageTab';
import { BuildingUnitExternalContactPersonForm } from '../../Form/BuildingUnit/BuildingUnitExternalContactPersonForm';
import { MessageBoxComponent } from '../../../Component/MessageBoxComponent/MessageBoxComponent';
import { MessageBoxAlertType } from '../../../Component/MessageBoxComponent/MessageBoxAlertType';

class ExternalContactPersonPage  extends BuildingUnitPage {

    private readonly buildingUnitExternalContactPersonForm: BuildingUnitExternalContactPersonForm;
    private readonly buildingUnitExternalContactPersonFormElement: HTMLFormElement;
    private readonly buildingUnitExternalContactPersonFormSubmitButton: HTMLButtonElement;
    private readonly messageBoxComponent: MessageBoxComponent;

    constructor() {
        super(BuildingUnitPageTab.ExternalContactPerson);

        const idPrefix: string = 'building_unit_external_contact_person_';

        this.buildingUnitExternalContactPersonForm = new BuildingUnitExternalContactPersonForm(idPrefix);
        this.buildingUnitExternalContactPersonFormElement = document.querySelector('#' + idPrefix + 'form');
        this.buildingUnitExternalContactPersonFormSubmitButton = document.querySelector('#' + idPrefix + 'save_submit_button');
        this.messageBoxComponent = new MessageBoxComponent();

        if (this.buildingUnitExternalContactPersonFormElement !== null) {
            this.buildingUnitExternalContactPersonFormSubmitButton.addEventListener('click', (): void => {
                if (this.buildingUnitExternalContactPersonForm.isFormValid() === false) {
                    this.messageBoxComponent.openMessageBox('Die Daten konnten nicht gespeichert werden. Bitte kontrollieren Sie die rot markierten Felder.', MessageBoxAlertType.DANGER);

                    return;
                }

                this.buildingUnitExternalContactPersonFormElement.submit();
            });
        }
    }

}

export { ExternalContactPersonPage };
