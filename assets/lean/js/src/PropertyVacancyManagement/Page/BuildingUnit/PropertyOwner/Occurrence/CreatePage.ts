import { OccurrenceCreatePage } from '../../OccurrenceCreatePage';
import { BuildingUnitPageTab } from '../../BuildingUnitPageTab';
import { PropertyOwnerOccurrenceForm } from '../../../../Form/Property/PropertyOwnerOccurrenceForm';

class CreatePage extends OccurrenceCreatePage {

    constructor() {
        const idPrefix: string = 'property_owner_occurrence_';

        super(BuildingUnitPageTab.PropertyOwner, idPrefix, new PropertyOwnerOccurrenceForm(idPrefix));
    }

}

export { CreatePage };
