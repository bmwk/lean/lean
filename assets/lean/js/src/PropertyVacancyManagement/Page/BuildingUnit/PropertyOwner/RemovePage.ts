import { BuildingUnitPage } from '../BuildingUnitPage';
import { BuildingUnitPageTab } from '../BuildingUnitPageTab';
import { PropertyOwnerRemoveForm } from '../../../Form/Property/PropertyOwnerRemoveForm';
import { PropertyOwnerForm } from '../../../Form/Property/PropertyOwnerForm';
import { MessageBoxComponent } from '../../../../Component/MessageBoxComponent/MessageBoxComponent';
import { MessageBoxAlertType } from '../../../../Component/MessageBoxComponent/MessageBoxAlertType';

class RemovePage extends BuildingUnitPage {

    private readonly propertyOwnerForm: PropertyOwnerForm;
    private readonly propertyOwnerRemoveForm: PropertyOwnerRemoveForm;
    private readonly propertyOwnerRemoveFormElement: HTMLFormElement;
    private readonly propertyOwnerRemoveFormSubmitButton: HTMLButtonElement;
    private readonly messageBoxComponent: MessageBoxComponent;

    constructor() {
        super(BuildingUnitPageTab.PropertyOwner);

        const idPrefix: string = 'property_owner_remove_';

        this.propertyOwnerForm = new PropertyOwnerForm('property_owner_');
        this.propertyOwnerRemoveForm = new PropertyOwnerRemoveForm(idPrefix);
        this.propertyOwnerRemoveFormElement = document.querySelector('#' + idPrefix + 'form');
        this.propertyOwnerRemoveFormSubmitButton = document.querySelector('#' + idPrefix + 'remove_submit_button');
        this.messageBoxComponent = new MessageBoxComponent();

        this.propertyOwnerForm.disableFields();

        this.propertyOwnerRemoveFormSubmitButton.addEventListener('click', (): void => {
            if (this.propertyOwnerRemoveForm.isFormValid() === false) {
                this.messageBoxComponent.openMessageBox('Der Eigentümer konnte nicht entfernt werden. Bitte kontrollieren Sie die rot markierten Felder.', MessageBoxAlertType.DANGER);

                return;
            }

            this.propertyOwnerRemoveFormElement.submit();
        });
    }

}

export { RemovePage };
