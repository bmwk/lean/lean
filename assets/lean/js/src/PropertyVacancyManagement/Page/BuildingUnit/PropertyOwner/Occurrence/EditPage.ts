import { OccurrenceEditPage } from '../../OccurrenceEditPage';
import { BuildingUnitPageTab } from '../../BuildingUnitPageTab';
import { PropertyOwnerOccurrenceForm } from '../../../../Form/Property/PropertyOwnerOccurrenceForm';

class EditPage extends OccurrenceEditPage {

    constructor() {
        const idPrefix: string = 'property_owner_occurrence_';

        super(BuildingUnitPageTab.PropertyOwner, idPrefix, new PropertyOwnerOccurrenceForm(idPrefix));
    }

}

export { EditPage };
