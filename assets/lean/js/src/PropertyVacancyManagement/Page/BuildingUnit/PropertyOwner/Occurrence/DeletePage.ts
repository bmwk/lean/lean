import { OccurrenceDeletePage } from '../../OccurrenceDeletePage';
import { BuildingUnitPageTab } from '../../BuildingUnitPageTab';
import { PropertyOwnerOccurrenceForm } from '../../../../Form/Property/PropertyOwnerOccurrenceForm';

class DeletePage extends OccurrenceDeletePage {

    constructor() {
        super(BuildingUnitPageTab.PropertyOwner, 'occurrence_delete_', new PropertyOwnerOccurrenceForm('property_owner_occurrence_'));
    }

}

export { DeletePage };
