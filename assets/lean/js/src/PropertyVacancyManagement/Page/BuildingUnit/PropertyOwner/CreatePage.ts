import { BuildingUnitPage } from '../BuildingUnitPage';
import { BuildingUnitPageTab } from '../BuildingUnitPageTab';
import { PropertyOwnerForm } from '../../../Form/Property/PropertyOwnerForm';
import { MessageBoxComponent } from '../../../../Component/MessageBoxComponent/MessageBoxComponent';
import { MessageBoxAlertType } from '../../../../Component/MessageBoxComponent/MessageBoxAlertType';

class CreatePage extends BuildingUnitPage {

    private readonly propertyOwnerForm: PropertyOwnerForm;
    private readonly propertyOwnerFormElement: HTMLFormElement;
    private readonly propertyOwnerFormSubmitButton: HTMLButtonElement;
    private readonly messageBoxComponent: MessageBoxComponent;

    constructor() {
        super(BuildingUnitPageTab.PropertyOwner);

        const idPrefix: string = 'property_owner_';

        this.propertyOwnerForm = new PropertyOwnerForm(idPrefix);
        this.propertyOwnerFormElement = document.querySelector('#' + idPrefix + 'form');
        this.propertyOwnerFormSubmitButton = document.querySelector('#' + idPrefix + 'save_submit_button');
        this.messageBoxComponent = new MessageBoxComponent();

        this.propertyOwnerFormSubmitButton.addEventListener('click', (): void => {
            if (this.propertyOwnerForm.isFormValid() === false) {
                this.messageBoxComponent.openMessageBox('Die Daten konnten nicht gespeichert werden. Bitte kontrollieren Sie die rot markierten Felder.', MessageBoxAlertType.DANGER);

                return;
            }

            this.propertyOwnerFormElement.submit();
        });
    }

}

export { CreatePage };
