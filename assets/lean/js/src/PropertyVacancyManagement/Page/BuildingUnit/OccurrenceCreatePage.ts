import { BuildingUnitPage } from './BuildingUnitPage';
import { BuildingUnitPageTab } from './BuildingUnitPageTab';
import { OccurrenceForm } from '../../../PersonManagement/Form/Occurrence/OccurrenceForm';
import { MessageBoxComponent } from '../../../Component/MessageBoxComponent/MessageBoxComponent';
import { MessageBoxAlertType } from '../../../Component/MessageBoxComponent/MessageBoxAlertType';

abstract class OccurrenceCreatePage extends BuildingUnitPage {

    private readonly occurrenceForm: OccurrenceForm;
    private readonly occurrenceFormElement: HTMLFormElement;
    private readonly occurrenceFormSubmitButton: HTMLButtonElement;
    private readonly messageBoxComponent: MessageBoxComponent;

    protected constructor(buildingUnitPageTab: BuildingUnitPageTab, idPrefix: string, occurrenceForm: OccurrenceForm) {
        super(buildingUnitPageTab);

        this.occurrenceForm = occurrenceForm;
        this.occurrenceFormElement = document.querySelector('#' + idPrefix + 'form');
        this.occurrenceFormSubmitButton = document.querySelector('#' + idPrefix + 'save_submit_button');
        this.messageBoxComponent = new MessageBoxComponent();

        this.occurrenceFormSubmitButton.addEventListener('click', (): void => {
            if (this.occurrenceForm.isFormValid() === false) {
                this.messageBoxComponent.openMessageBox('Die Daten konnten nicht gespeichert werden. Bitte kontrollieren Sie die rot markierten Felder.', MessageBoxAlertType.DANGER);

                return;
            }

            this.occurrenceFormElement.submit();
        });
    }

}

export { OccurrenceCreatePage };
