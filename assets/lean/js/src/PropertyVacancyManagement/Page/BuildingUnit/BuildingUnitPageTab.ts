enum BuildingUnitPageTab {
    BasicData = 'building_unit_basic_data_tab',
    Detail = 'building_unit_detail_tab',
    LocationMap = 'building_unit_location_map_tab',
    Photo = 'building_unit_photo_overview_tab',
    Document = 'building_unit_document_overview_tab',
    PropertyOwner = 'building_unit_property_owner_tab',
    PropertyUser = 'building_unit_property_user_tab',
    ContactPerson = 'building_unit_property_contact_person_tab',
    ExternalContactPerson = 'building_unit_external_contact_person_tab',
    PropertyMarketingInformation = 'building_unit_property_marketing_information_tab'
}

export { BuildingUnitPageTab };
