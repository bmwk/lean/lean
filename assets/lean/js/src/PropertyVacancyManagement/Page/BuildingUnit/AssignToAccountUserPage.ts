import { BuildingUnitPage } from './BuildingUnitPage';
import { BuildingUnitPageTab } from './BuildingUnitPageTab';
import { BuildingUnitBasicDataForm } from '../../Form/BuildingUnit/BuildingUnitBasicDataForm';
import { BuildingUnitAssignToAccountUserForm } from '../../Form/BuildingUnit/BuildingUnitAssignToAccountUserForm';
import { MessageBoxComponent } from '../../../Component/MessageBoxComponent/MessageBoxComponent';
import { MessageBoxAlertType } from '../../../Component/MessageBoxComponent/MessageBoxAlertType';

class AssignToAccountUserPage extends BuildingUnitPage {

    private readonly buildingUnitBasicDataForm: BuildingUnitBasicDataForm;
    private readonly buildingUnitAssignToAccountUserForm: BuildingUnitAssignToAccountUserForm;
    private readonly buildingUnitAssignToAccountUserFormElement: HTMLFormElement;
    private readonly buildingUnitAssignToAccountUserFormSubmitButton: HTMLButtonElement;
    private readonly messageBoxComponent: MessageBoxComponent;

    constructor() {
        super(BuildingUnitPageTab.BasicData);

        const idPrefix: string = 'building_unit_assign_to_account_user_';

        this.buildingUnitBasicDataForm = new BuildingUnitBasicDataForm('building_unit_basic_data_');
        this.buildingUnitAssignToAccountUserForm = new BuildingUnitAssignToAccountUserForm(idPrefix);
        this.buildingUnitAssignToAccountUserFormElement = document.querySelector('#' + idPrefix + 'form');
        this.buildingUnitAssignToAccountUserFormSubmitButton = document.querySelector('#' + idPrefix + 'save_submit_button');
        this.messageBoxComponent = new MessageBoxComponent();

        this.buildingUnitAssignToAccountUserFormSubmitButton.addEventListener('click', (): void => {
            if (this.buildingUnitAssignToAccountUserForm.isFormValid() === false) {
                this.messageBoxComponent.openMessageBox('Die Daten konnten nicht gespeichert werden. Bitte kontrollieren Sie die rot markierten Felder.', MessageBoxAlertType.DANGER);

                return;
            }

            this.buildingUnitAssignToAccountUserFormElement.submit();
        });
    }

}

export { AssignToAccountUserPage };
