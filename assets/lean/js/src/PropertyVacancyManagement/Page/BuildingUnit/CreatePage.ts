import { BuildingUnitCreateForm } from '../../Form/BuildingUnit/BuildingUnitCreateForm';
import { MessageBoxComponent } from '../../../Component/MessageBoxComponent/MessageBoxComponent';
import { MessageBoxAlertType } from '../../../Component/MessageBoxComponent/MessageBoxAlertType';

class CreatePage {

    private readonly buildingUnitCreateForm: BuildingUnitCreateForm;
    private readonly buildingUnitCreateFormElement: HTMLFormElement;
    private readonly buildingUnitCreateFormSubmitButton: HTMLButtonElement;
    private readonly messageBoxComponent: MessageBoxComponent;

    constructor() {
        const idPrefix: string = 'building_unit_create_';

        this.buildingUnitCreateForm = new BuildingUnitCreateForm(idPrefix);
        this.buildingUnitCreateFormElement = document.querySelector('#' + idPrefix + 'form');
        this.buildingUnitCreateFormSubmitButton = document.querySelector('#' + idPrefix + 'save_submit_button');
        this.messageBoxComponent = new MessageBoxComponent();

        this.buildingUnitCreateFormSubmitButton.addEventListener('click', (): void => {
            if (this.buildingUnitCreateForm.isFormValid() === false) {
                this.messageBoxComponent.openMessageBox('Die Daten konnten nicht gespeichert werden. Bitte kontrollieren Sie die rot markierten Felder.', MessageBoxAlertType.DANGER);

                return;
            }

            this.buildingUnitCreateFormElement.submit();
        });
    }

}

export { CreatePage };
