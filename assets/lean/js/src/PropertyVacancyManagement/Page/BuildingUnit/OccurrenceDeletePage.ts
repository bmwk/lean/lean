import { BuildingUnitPage } from './BuildingUnitPage';
import { BuildingUnitPageTab } from './BuildingUnitPageTab';
import { OccurrenceDeleteForm } from '../../../PersonManagement/Form/Occurrence/OccurrenceDeleteForm';
import { OccurrenceForm } from '../../../PersonManagement/Form/Occurrence/OccurrenceForm';
import { MessageBoxComponent } from '../../../Component/MessageBoxComponent/MessageBoxComponent';
import { MessageBoxAlertType } from '../../../Component/MessageBoxComponent/MessageBoxAlertType';

abstract class OccurrenceDeletePage extends BuildingUnitPage {

    private readonly occurrenceForm: OccurrenceForm;
    private readonly occurrenceDeleteForm: OccurrenceDeleteForm;
    private readonly occurrenceDeleteFormElement: HTMLFormElement;
    private readonly occurrenceDeleteFormSubmitButton: HTMLButtonElement;
    private readonly messageBoxComponent: MessageBoxComponent;

    protected constructor(buildingUnitPageTab: BuildingUnitPageTab, idPrefix: string, occurrenceForm: OccurrenceForm) {
        super(buildingUnitPageTab);

        this.occurrenceForm = occurrenceForm;

        this.occurrenceDeleteForm = new OccurrenceDeleteForm(idPrefix);
        this.occurrenceDeleteFormElement = document.querySelector('#' + idPrefix + 'form');
        this.occurrenceDeleteFormSubmitButton = document.querySelector('#' + idPrefix + 'delete_submit_button');
        this.messageBoxComponent = new MessageBoxComponent();

        this.occurrenceForm.disableFields();

        this.occurrenceDeleteFormSubmitButton.addEventListener('click', (): void => {
            if (this.occurrenceDeleteForm.isFormValid() === false) {
                this.messageBoxComponent.openMessageBox('Der Vorgang konnte nicht gelöscht werden, es wurde kein Bestätigungscode eingegeben.', MessageBoxAlertType.DANGER);

                return;
            }

            this.occurrenceDeleteFormElement.submit();
        });
    }

}

export { OccurrenceDeletePage };
