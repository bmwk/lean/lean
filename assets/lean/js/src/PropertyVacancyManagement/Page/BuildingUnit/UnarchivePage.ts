import { BuildingUnitPage } from './BuildingUnitPage';
import { BuildingUnitPageTab} from './BuildingUnitPageTab';
import { BuildingUnitBasicDataForm } from '../../Form/BuildingUnit/BuildingUnitBasicDataForm';

class UnarchivePage extends BuildingUnitPage {

    private readonly buildingUnitBasicDataForm: BuildingUnitBasicDataForm;
    private readonly buildingUnitUnarchiveFormElement: HTMLFormElement;
    private readonly buildingUnitUnarchiveFormSubmitButton: HTMLButtonElement;

    constructor() {
        super(BuildingUnitPageTab.BasicData);

        const idPrefix: string = 'building_unit_unarchive_';

        this.buildingUnitBasicDataForm = new BuildingUnitBasicDataForm('building_unit_basic_data_');
        this.buildingUnitUnarchiveFormElement = document.querySelector('#' + idPrefix + 'form');
        this.buildingUnitUnarchiveFormSubmitButton = document.querySelector('#' + idPrefix + 'unarchive_submit_button');

        this.buildingUnitUnarchiveFormSubmitButton.addEventListener('click', (): void => {
            this.buildingUnitUnarchiveFormElement.submit();
        });
    }

}

export { UnarchivePage };
