import { BuildingUnitPage } from './BuildingUnitPage';
import { BuildingUnitPageTab } from './BuildingUnitPageTab';
import { PropertyMarketingInformationForm } from '../../Form/Property/PropertyMarketingInformationForm';
import { MessageBoxComponent } from '../../../Component/MessageBoxComponent/MessageBoxComponent';
import { MessageBoxAlertType } from '../../../Component/MessageBoxComponent/MessageBoxAlertType';

class PropertyMarketingInformationPage extends BuildingUnitPage {

    private readonly propertyMarketingInformationForm: PropertyMarketingInformationForm;
    private readonly propertyMarketingInformationFormElement: HTMLFormElement;
    private readonly propertyMarketingInformationFormSubmitButton: HTMLButtonElement;
    private readonly messageBoxComponent: MessageBoxComponent;

    constructor() {
        super(BuildingUnitPageTab.PropertyMarketingInformation);

        const idPrefix: string = 'property_marketing_information_';

        this.propertyMarketingInformationForm = new PropertyMarketingInformationForm(idPrefix);
        this.propertyMarketingInformationFormElement = document.querySelector('#' + idPrefix + 'form');
        this.propertyMarketingInformationFormSubmitButton = document.querySelector('#' + idPrefix + 'save_submit_button');
        this.messageBoxComponent = new MessageBoxComponent();

        if (this.propertyMarketingInformationFormElement !== null) {
            this.propertyMarketingInformationFormSubmitButton.addEventListener('click', (): void => {
                if (this.propertyMarketingInformationForm.isFormValid() === false) {
                    this.messageBoxComponent.openMessageBox('Die Daten konnten nicht gespeichert werden. Bitte kontrollieren Sie die rot markierten Felder.', MessageBoxAlertType.DANGER);

                    return;
                }

                this.propertyMarketingInformationFormElement.submit();
            });
        }
    }

}

export { PropertyMarketingInformationPage };
