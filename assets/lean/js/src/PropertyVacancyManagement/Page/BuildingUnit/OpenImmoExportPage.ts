import { BuildingUnitPage } from './BuildingUnitPage';
import { BuildingUnitPageTab } from './BuildingUnitPageTab';
import { BuildingUnitOpenImmoExportInitializationForm } from '../../Form/BuildingUnit/BuildingUnitOpenImmoExportInitializationForm';
import { MessageBoxComponent } from '../../../Component/MessageBoxComponent/MessageBoxComponent';
import { MessageBoxAlertType } from '../../../Component/MessageBoxComponent/MessageBoxAlertType';

class OpenImmoExportPage extends BuildingUnitPage {

    private readonly buildingUnitOpenImmoExportInitializationForm: BuildingUnitOpenImmoExportInitializationForm;
    private readonly buildingUnitOpenImmoExportInitializationFormElement: HTMLFormElement;
    private readonly buildingUnitOpenImmoExportInitializationFormSubmitButton: HTMLButtonElement;
    private readonly messageBoxComponent: MessageBoxComponent;

    constructor() {
        super(BuildingUnitPageTab.BasicData);

        const idPrefix: string = 'open_immo_export_initialization_';

        this.buildingUnitOpenImmoExportInitializationForm = new BuildingUnitOpenImmoExportInitializationForm(idPrefix);
        this.buildingUnitOpenImmoExportInitializationFormElement = document.querySelector('#' + idPrefix + 'form');
        this.buildingUnitOpenImmoExportInitializationFormSubmitButton = document.querySelector('#' + idPrefix + 'apply_submit_button');
        this.messageBoxComponent = new MessageBoxComponent();

        this.buildingUnitOpenImmoExportInitializationFormSubmitButton.addEventListener('click', (): void => {
            if (this.buildingUnitOpenImmoExportInitializationForm.isFormValid() === false) {
                this.messageBoxComponent.openMessageBox('Der Export konnte nicht gestartet werden. Bitte kontrollieren Sie die rot markierten Felder.', MessageBoxAlertType.DANGER);

                return;
            }

            this.buildingUnitOpenImmoExportInitializationFormElement.submit();
        });
    }

}

export { OpenImmoExportPage };
