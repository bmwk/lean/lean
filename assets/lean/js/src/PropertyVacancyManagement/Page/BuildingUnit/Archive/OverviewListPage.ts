import { OverviewPage } from './OverviewPage';
import { OverviewPageTab } from './OverviewPageTab';
import { BuildingUnitFilterForm } from '../../../Form/BuildingUnit/BuildingUnitFilterForm';
import { BuildingUnitSearchForm } from '../../../Form/BuildingUnit/BuildingUnitSearchForm';
import { BuildingUnitListComponent } from '../../../Component/BuildingUnit/BuildingUnitListComponent';

class OverviewListPage extends OverviewPage {

    private readonly buildingUnitFilterForm: BuildingUnitFilterForm;
    private readonly buildingUnitSearchForm: BuildingUnitSearchForm;
    private readonly buildingUnitListComponent: BuildingUnitListComponent;

    constructor() {
        super(OverviewPageTab.List);

        this.buildingUnitFilterForm = new BuildingUnitFilterForm('building_unit_filter_');
        this.buildingUnitSearchForm = new BuildingUnitSearchForm('building_unit_search_');

        if (BuildingUnitListComponent.checkContainerExists('building_unit_archive_overview_') === true) {
            this.buildingUnitListComponent = new BuildingUnitListComponent('building_unit_archive_overview_');
        }
    }

}

export { OverviewListPage };
