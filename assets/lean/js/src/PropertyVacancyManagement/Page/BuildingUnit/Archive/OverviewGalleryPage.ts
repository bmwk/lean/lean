import { OverviewPage } from './OverviewPage';
import { OverviewPageTab } from './OverviewPageTab';
import { BuildingUnitFilterForm } from '../../../Form/BuildingUnit/BuildingUnitFilterForm';
import { BuildingUnitSearchForm } from '../../../Form/BuildingUnit/BuildingUnitSearchForm';
import { LazyImageLoader } from '../../../../LazyImageLoader';
import { SortingSelectComponent } from '../../../../Component/SortingSelectComponent';
import { PaginationComponent } from '../../../../Component/PaginationComponent';

class OverviewGalleryPage extends OverviewPage {

    private readonly buildingUnitFilterForm: BuildingUnitFilterForm;
    private readonly buildingUnitFilterFormElement: HTMLFormElement;
    private readonly buildingUnitFilterFormSubmitButton: HTMLButtonElement;
    private readonly buildingUnitSearchForm: BuildingUnitSearchForm;
    private readonly lazyImageLoader: LazyImageLoader;
    private readonly sortingSelectComponent: SortingSelectComponent;
    private readonly paginationComponent: PaginationComponent;

    constructor() {
        super(OverviewPageTab.Gallery);

        const idPrefix: string = 'building_unit_filter_';

        this.buildingUnitFilterForm = new BuildingUnitFilterForm(idPrefix);
        this.buildingUnitFilterFormElement = document.querySelector('#' + idPrefix + 'form');
        this.buildingUnitFilterFormSubmitButton = document.querySelector('#' + idPrefix + 'apply');
        this.buildingUnitSearchForm = new BuildingUnitSearchForm('building_unit_search_');
        this.lazyImageLoader = new LazyImageLoader();

        if (SortingSelectComponent.checkSelectBoxExists('gallery_') === true) {
            this.sortingSelectComponent = new SortingSelectComponent('gallery_');
        }

        if (PaginationComponent.checkContainerExists('building_unit_overview_gallery_') === true) {
            this.paginationComponent = new PaginationComponent('building_unit_overview_gallery_');
        }

        this.buildingUnitFilterFormSubmitButton.addEventListener('click', (): void => {
            if (this.buildingUnitFilterForm.isFormValid() === false) {
                return;
            }

            this.buildingUnitFilterFormElement.submit();
        });
    }

}

export { OverviewGalleryPage };
