import { OverviewPageTab } from './OverviewPageTab';
import { MdcTabBarPage } from '../../../../MdcTabBarPage';

abstract class OverviewPage extends MdcTabBarPage {

    protected constructor(overviewPageTab: OverviewPageTab) {
        super(document.querySelector('#building_unit_archive_overview_page_tab_bar'));

        this.activateTab(overviewPageTab);
    }
}

export { OverviewPage };
