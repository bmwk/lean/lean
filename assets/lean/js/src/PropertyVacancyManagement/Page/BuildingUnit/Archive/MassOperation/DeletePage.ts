import { MassOperationPage } from '../../MassOperationPage';
import { TextFieldComponent } from '../../../../../Component/TextFieldComponent';
import { MessageBoxAlertType } from '../../../../../Component/MessageBoxComponent/MessageBoxAlertType';

class DeletePage extends MassOperationPage {

    private readonly verificationCodeTextField: TextFieldComponent;

    constructor() {
        const idPrefix: string = 'building_unit_mass_operation_delete_';

        super(
            document.querySelector('#' + idPrefix + 'form'),
            document.querySelector('#' + idPrefix + 'delete_submit_button')
        );

        this.verificationCodeTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'verificationCode_mdc_text_field'));

        this.verificationCodeTextField.mdcTextField.required = true;
    }

    protected doOnConfirmationFormSubmit(): void {
        if (this.verificationCodeTextField.mdcTextField.value === null || this.verificationCodeTextField.mdcTextField.value === '') {
            this.messageBoxComponent.openMessageBox('Die Objekte konnten nicht gelöscht werden. Bitte geben Sie den Bestätigungscode ein', MessageBoxAlertType.DANGER);

            return;
        }

        this.confirmationFormElement.submit();
    }

}

export { DeletePage };
