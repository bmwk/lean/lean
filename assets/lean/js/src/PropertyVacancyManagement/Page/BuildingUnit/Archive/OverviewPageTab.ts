enum OverviewPageTab {
    Map = 'building_unit_archive_overview_map_tab',
    Gallery = 'building_unit_archive_overview_gallery_tab',
    List = 'building_unit_archive_overview_list_tab',
}

export { OverviewPageTab };
