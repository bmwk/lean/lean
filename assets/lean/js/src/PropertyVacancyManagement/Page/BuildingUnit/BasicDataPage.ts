import { BuildingUnitPage } from './BuildingUnitPage';
import { BuildingUnitPageTab } from './BuildingUnitPageTab';
import { BuildingUnitBasicDataForm } from '../../Form/BuildingUnit/BuildingUnitBasicDataForm';
import { MdcContextMenuComponent } from '../../../Component/MdcContextMenuComponent';
import { MessageBoxComponent } from '../../../Component/MessageBoxComponent/MessageBoxComponent';
import { MessageBoxAlertType } from '../../../Component/MessageBoxComponent/MessageBoxAlertType';

class BasicDataPage extends BuildingUnitPage {

    private readonly buildingUnitBasicDataForm: BuildingUnitBasicDataForm;
    private readonly buildingUnitBasicDataFormElement: HTMLFormElement;
    private readonly buildingUnitBasicDataFormSubmitButton: HTMLButtonElement;
    private readonly contextMenu: MdcContextMenuComponent = null;
    private readonly messageBoxComponent: MessageBoxComponent;

    constructor() {
        super(BuildingUnitPageTab.BasicData);

        const idPrefix: string = 'building_unit_basic_data_';

        this.buildingUnitBasicDataForm = new BuildingUnitBasicDataForm(idPrefix);
        this.buildingUnitBasicDataFormElement = document.querySelector('#' + idPrefix + 'form');
        this.buildingUnitBasicDataFormSubmitButton = document.querySelector('#' + idPrefix + 'save_submit_button');

        if (MdcContextMenuComponent.checkContainerExists(idPrefix) === true) {
            this.contextMenu = new MdcContextMenuComponent(idPrefix);
        }

        this.messageBoxComponent = new MessageBoxComponent();

        if (this.buildingUnitBasicDataFormSubmitButton !== null) {
            this.buildingUnitBasicDataFormSubmitButton.addEventListener('click', (): void => {
                if (this.buildingUnitBasicDataForm.isFormValid() === false) {
                    this.messageBoxComponent.openMessageBox('Die Daten konnten nicht gespeichert werden. Bitte kontrollieren Sie die rot markierten Felder.', MessageBoxAlertType.DANGER);

                    return;
                }

                this.buildingUnitBasicDataFormElement.submit();
            });
        }
    }

}

export { BasicDataPage };
