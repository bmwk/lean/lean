import React from 'react';
import { createRoot, Root } from 'react-dom/client';
import PropertyOverviewMap from '../../../Map/PropertyOverviewMap';
import { OverviewPage } from './OverviewPage';
import { OverviewPageTab } from './OverviewPageTab';

class OverviewMapPage extends OverviewPage {

    private readonly mapContainer: HTMLDivElement;
    private readonly overviewMapRoot: Root;

    constructor() {
        super(OverviewPageTab.Map);

        this.mapContainer = document.querySelector('#building_unit_overview_map');
        this.overviewMapRoot = createRoot(this.mapContainer!);

        const centerPoint = {
            longitude: 0,
            latitude: 0
        };

        if (this.mapContainer.dataset.longitude !== undefined && this.mapContainer.dataset.latitude !== undefined) {
            centerPoint.longitude = parseFloat(this.mapContainer.dataset.longitude);
            centerPoint.latitude = parseFloat(this.mapContainer.dataset.latitude);
        }

        this.overviewMapRoot.render(
            <React.StrictMode>
                <PropertyOverviewMap
                    permissions={{canView: true, canEdit: true}}
                    canCreateBuildingUnit={Boolean(this.mapContainer.dataset.canCreateBuildingUnit)}
                    accountUserType={parseInt(this.mapContainer.dataset.accountUserType)}
                    centerPoint={centerPoint}
                    archived={false}
                />
            </React.StrictMode>
        );
    }

}

export { OverviewMapPage };
