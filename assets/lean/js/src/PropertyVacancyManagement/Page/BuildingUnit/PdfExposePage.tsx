import React from 'react';
import { createRoot, Root } from 'react-dom/client';
import PropertyPdfExposeMap from '../../../Map/PropertyPdfExposeMap';
import { PropertyApiResponse } from '../../../Property/PropertyApiResponse';

interface CenterPoint {
    longitude: number;
    latitude: number;
}

class PdfExposePage {

    private readonly mapContainer: HTMLDivElement;
    private readonly propertyPdfExposeMapRoot: Root;
    private readonly buildingUnit: PropertyApiResponse;

    constructor() {
        this.mapContainer = document.querySelector('#building_unit_map');
        this.propertyPdfExposeMapRoot = createRoot(this.mapContainer!);
        this.buildingUnit = JSON.parse(this.mapContainer.dataset.buildingUnitJsonRepresentation);

        const centerPoint: CenterPoint = {
            longitude: this.buildingUnit.geolocationPoint?.point.longitude,
            latitude: this.buildingUnit.geolocationPoint?.point.latitude
        };

        if (centerPoint.latitude === undefined || centerPoint.longitude === undefined) {
            centerPoint.latitude = 0;
            centerPoint.longitude = 0;
        }

        this.propertyPdfExposeMapRoot.render(
            <React.StrictMode>
                <PropertyPdfExposeMap
                    centerPoint={centerPoint}
                    buildingUnit={this.buildingUnit}
                    useMarker={true}
                    forceSearchAddress={true}
                    usePrintControl={true}
                    usePrintBtn={false}
                />
            </React.StrictMode>
        );
    }

}

export { PdfExposePage };
