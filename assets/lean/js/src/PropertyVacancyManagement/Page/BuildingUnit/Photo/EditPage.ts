import { BuildingUnitPage } from '../BuildingUnitPage';
import { BuildingUnitPageTab } from '../BuildingUnitPageTab';
import { PhotoForm } from '../../../Form/Property/PhotoForm';
import { MdcContextMenuComponent } from '../../../../Component/MdcContextMenuComponent';

class EditPage extends BuildingUnitPage {

    private readonly photoForm: PhotoForm;
    private readonly photoFormElement: HTMLFormElement;
    private readonly photoFormSubmitButton: HTMLButtonElement;
    private readonly contextMenu: MdcContextMenuComponent = null;

    constructor() {
        super(BuildingUnitPageTab.Photo);

        const idPrefix: string = 'photo_';

        this.photoForm = new PhotoForm(idPrefix);
        this.photoFormElement = document.querySelector('#' + idPrefix + 'form');
        this.photoFormSubmitButton = document.querySelector('#' + idPrefix + 'save_submit_button');

        if (MdcContextMenuComponent.checkContainerExists(idPrefix) === true) {
            this.contextMenu = new MdcContextMenuComponent(idPrefix);
        }

        if (this.photoFormSubmitButton !== null) {
            this.photoFormSubmitButton.addEventListener('click', (): void => {
                this.photoFormElement.submit();
            });
        }
    }

}

export { EditPage };
