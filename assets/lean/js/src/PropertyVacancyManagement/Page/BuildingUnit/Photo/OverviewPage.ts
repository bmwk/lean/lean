import { BuildingUnitPage } from '../BuildingUnitPage';
import { BuildingUnitPageTab } from '../BuildingUnitPageTab';
import { LazyImageLoader } from '../../../../LazyImageLoader';
import { DropzoneUploadComponent } from '../../../../Component/DropzoneUploadComponent';

class OverviewPage extends BuildingUnitPage {

    private readonly lazyImageLoader: LazyImageLoader;
    private readonly dropzoneContainer: HTMLDivElement;
    private readonly dropzoneUploadComponent: DropzoneUploadComponent = null;

    constructor() {
        super(BuildingUnitPageTab.Photo);

        this.lazyImageLoader = new LazyImageLoader();
        this.dropzoneContainer = document.querySelector('#photo_upload_dropzone_container');

        if (this.dropzoneContainer !== null) {
            const acceptedFiles: string[] = [
                'image/bmp',
                'image/cis-cod',
                'image/gif',
                'image/ief',
                'image/jpeg',
                'image/png',
                'image/pipeg',
                'image/svg+xml',
                'image/tiff',
                'image/x-cmu-raster',
                'image/x-cmx',
                'image/x-icon',
                'image/x-portable-anymap',
                'image/x-portable-bitmap',
                'image/x-portable-graymap',
                'image/x-portable-pixmap',
                'image/x-rgb',
                'image/x-xbitmap',
                'image/x-xpixmap',
                'image/x-xwindowdump',
            ];

            this.dropzoneUploadComponent = new DropzoneUploadComponent(
                this.dropzoneContainer ,
                document.querySelector('#photo_upload_submit_button'),
                'imageFile',
                acceptedFiles
            );
        }
    }

}

export { OverviewPage };
