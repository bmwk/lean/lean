import { BuildingUnitPage } from '../BuildingUnitPage';
import { BuildingUnitPageTab } from '../BuildingUnitPageTab';
import { PhotoDeleteForm } from '../../../Form/Property/PhotoDeleteForm';
import { PhotoForm } from '../../../Form/Property/PhotoForm';

class DeletePage extends BuildingUnitPage {

    private readonly photoForm: PhotoForm;
    private readonly photoDeleteForm: PhotoDeleteForm;
    private readonly photoDeleteFormElement: HTMLFormElement;
    private readonly photoDeleteFormSubmitButton: HTMLButtonElement;

    constructor() {
        super(BuildingUnitPageTab.Photo);

        const idPrefix: string = 'photo_delete_';

        this.photoForm = new PhotoForm('photo_');
        this.photoDeleteForm = new PhotoDeleteForm(idPrefix);
        this.photoDeleteFormElement = document.querySelector('#' + idPrefix + 'form');
        this.photoDeleteFormSubmitButton = document.querySelector('#' + idPrefix + 'delete_submit_button');

        this.photoDeleteFormSubmitButton.addEventListener('click', (): void => {
            this.photoDeleteFormElement.submit();
        });
    }

}

export { DeletePage };
