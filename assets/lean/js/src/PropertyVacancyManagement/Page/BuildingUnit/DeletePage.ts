import { BuildingUnitPage } from './BuildingUnitPage';
import { BuildingUnitPageTab} from './BuildingUnitPageTab';
import { BuildingUnitDeleteForm } from '../../Form/BuildingUnit/BuildingUnitDeleteForm';
import { BuildingUnitBasicDataForm } from '../../Form/BuildingUnit/BuildingUnitBasicDataForm';

class DeletePage extends BuildingUnitPage {

    private readonly buildingUnitBasicDataForm: BuildingUnitBasicDataForm;
    private readonly buildingUnitDeleteForm: BuildingUnitDeleteForm;
    private readonly buildingUnitDeleteFormElement: HTMLFormElement;
    private readonly buildingUnitDeleteFormSubmitButton: HTMLButtonElement;

    constructor() {
        super(BuildingUnitPageTab.BasicData);

        const idPrefix: string = 'building_unit_delete_';

        this.buildingUnitBasicDataForm = new BuildingUnitBasicDataForm('building_unit_basic_data_');
        this.buildingUnitDeleteForm = new BuildingUnitDeleteForm(idPrefix);
        this.buildingUnitDeleteFormElement = document.querySelector('#' + idPrefix + 'form');
        this.buildingUnitDeleteFormSubmitButton = document.querySelector('#' + idPrefix + 'delete_submit_button');

        this.buildingUnitDeleteFormSubmitButton.addEventListener('click', (): void => {
            this.buildingUnitDeleteFormElement.submit();
        });
    }

}

export { DeletePage };
