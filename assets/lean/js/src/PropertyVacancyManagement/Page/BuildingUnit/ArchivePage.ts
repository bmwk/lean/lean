import { BuildingUnitPage } from './BuildingUnitPage';
import { BuildingUnitPageTab} from './BuildingUnitPageTab';
import { BuildingUnitBasicDataForm } from '../../Form/BuildingUnit/BuildingUnitBasicDataForm';

class ArchivePage extends BuildingUnitPage {

    private readonly buildingUnitBasicDataForm: BuildingUnitBasicDataForm;
    private readonly buildingUnitArchiveFormElement: HTMLFormElement;
    private readonly buildingUnitArchiveFormSubmitButton: HTMLButtonElement;

    constructor() {
        super(BuildingUnitPageTab.BasicData);

        const idPrefix: string = 'building_unit_archive_';

        this.buildingUnitBasicDataForm = new BuildingUnitBasicDataForm('building_unit_basic_data_');
        this.buildingUnitArchiveFormElement = document.querySelector('#' + idPrefix + 'form');
        this.buildingUnitArchiveFormSubmitButton = document.querySelector('#' + idPrefix + 'archive_submit_button');

        this.buildingUnitArchiveFormSubmitButton.addEventListener('click', (): void => {
            this.buildingUnitArchiveFormElement.submit();
        });
    }

}

export { ArchivePage };
