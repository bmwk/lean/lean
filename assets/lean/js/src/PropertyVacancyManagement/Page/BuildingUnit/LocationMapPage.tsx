import React from 'react';

import { createRoot, Root } from 'react-dom/client';
import PropertyMap from '../../../Map/PropertyMap';
import { BuildingUnitPage } from './BuildingUnitPage';
import { BuildingUnitPageTab } from './BuildingUnitPageTab';

class LocationMapPage extends BuildingUnitPage {

    private readonly mapContainer: HTMLDivElement;
    private readonly locationMapRoot: Root;

    constructor() {
        super(BuildingUnitPageTab.LocationMap);

        this.mapContainer = document.querySelector('#building_unit_map');
        this.locationMapRoot = createRoot(this.mapContainer!);

        const centerPoint = {
            longitude: 0,
            latitude: 0
        };

        if (this.mapContainer.dataset.longitude !== undefined && this.mapContainer.dataset.latitude !== undefined) {
            centerPoint.longitude = parseFloat(this.mapContainer.dataset.longitude);
            centerPoint.latitude = parseFloat(this.mapContainer.dataset.latitude);
        }

        this.locationMapRoot.render(
            <React.StrictMode>
                <PropertyMap
                    centerPoint={centerPoint}
                    accountUserType={parseInt(this.mapContainer.dataset.accountUserType)}
                    buildingUnitId={parseInt(this.mapContainer.dataset.buildingUnitId)}
                    permissions={JSON.parse(this.mapContainer.dataset.buildingUnitVoter)}
                />
            </React.StrictMode>
        );
    }

}

export { LocationMapPage };
