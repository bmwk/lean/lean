import { MessageBoxComponent } from '../../../Component/MessageBoxComponent/MessageBoxComponent';

abstract class MassOperationPage {

    protected readonly confirmationFormElement: HTMLFormElement;
    protected readonly confirmationFormSubmitButton: HTMLButtonElement;
    protected readonly messageBoxComponent: MessageBoxComponent;

    protected constructor(confirmationFormElement: HTMLFormElement, confirmationFormButton: HTMLButtonElement) {
        this.confirmationFormElement = confirmationFormElement;
        this.confirmationFormSubmitButton = confirmationFormButton;
        this.messageBoxComponent = new MessageBoxComponent();

        this.confirmationFormSubmitButton.addEventListener('click', (event: MouseEvent): void => {
            event.preventDefault();
            this.doOnConfirmationFormSubmit();
        });
    }

    protected abstract doOnConfirmationFormSubmit(): void;

}


export { MassOperationPage };
