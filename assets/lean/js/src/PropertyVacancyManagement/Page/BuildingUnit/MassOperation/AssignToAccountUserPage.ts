import { MassOperationPage } from '../MassOperationPage';
import { SelectComponent } from '../../../../Component/SelectComponent';

class AssignToAccountUserPage extends MassOperationPage {

    private readonly assignedToAccountUserSelect: SelectComponent;

    constructor() {
        const idPrefix: string = 'building_unit_mass_operation_assign_to_account_user_';

        super(
            document.querySelector('#' + idPrefix + 'form'),
            document.querySelector('#' + idPrefix + 'save_submit_button')
        );

        this.assignedToAccountUserSelect = new SelectComponent(document.querySelector('#' + idPrefix + 'assignedToAccountUser_mdc_select'), {clearButton: true});

        this.assignedToAccountUserSelect.mdcSelect.required = false;
    }

    protected doOnConfirmationFormSubmit(): void {
        this.confirmationFormElement.submit();
    }

}

export { AssignToAccountUserPage };
