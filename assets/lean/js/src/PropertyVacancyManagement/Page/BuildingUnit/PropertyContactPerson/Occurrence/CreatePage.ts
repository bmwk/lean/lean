import { OccurrenceCreatePage } from '../../OccurrenceCreatePage';
import { BuildingUnitPageTab } from '../../BuildingUnitPageTab';
import { PropertyContactPersonOccurrenceForm } from '../../../../Form/Property/PropertyContactPersonOccurrenceForm';

class CreatePage extends OccurrenceCreatePage {

    constructor() {
        const idPrefix: string = 'property_contact_person_occurrence_';

        super(BuildingUnitPageTab.ContactPerson, idPrefix, new PropertyContactPersonOccurrenceForm(idPrefix));
    }

}

export { CreatePage };
