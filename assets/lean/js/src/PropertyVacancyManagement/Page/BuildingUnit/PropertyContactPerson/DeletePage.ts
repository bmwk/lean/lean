import { BuildingUnitPage } from '../BuildingUnitPage';
import { BuildingUnitPageTab } from '../BuildingUnitPageTab';
import { PropertyContactPersonForm } from '../../../Form/Property/PropertyContactPersonForm';
import { PropertyContactPersonDeleteForm } from '../../../Form/Property/PropertyContactPersonDeleteForm';
import { MessageBoxComponent } from '../../../../Component/MessageBoxComponent/MessageBoxComponent';
import { MessageBoxAlertType } from '../../../../Component/MessageBoxComponent/MessageBoxAlertType';

class DeletePage extends BuildingUnitPage {

    private readonly propertyContactPersonForm: PropertyContactPersonForm;
    private readonly propertyContactPersonDeleteForm: PropertyContactPersonDeleteForm;
    private readonly propertyContactPersonDeleteFormElement: HTMLFormElement;
    private readonly propertyContactPersonDeleteFormSubmitButton: HTMLButtonElement;
    private readonly messageBoxComponent: MessageBoxComponent;

    constructor() {
        super(BuildingUnitPageTab.ContactPerson);

        const idPrefix: string = 'property_contact_person_delete_';

        this.propertyContactPersonForm = new PropertyContactPersonForm('property_contact_person_');
        this.propertyContactPersonDeleteForm = new PropertyContactPersonDeleteForm(idPrefix);
        this.propertyContactPersonDeleteFormElement = document.querySelector('#' + idPrefix + 'form');
        this.propertyContactPersonDeleteFormSubmitButton = document.querySelector('#' + idPrefix + 'delete_submit_button');
        this.messageBoxComponent = new MessageBoxComponent();

        this.propertyContactPersonForm.disableFields();

        this.propertyContactPersonDeleteFormSubmitButton.addEventListener('click', (): void => {
            if (this.propertyContactPersonDeleteForm.isFormValid() === false) {
                this.messageBoxComponent.openMessageBox('Die Ansprechperson konnte nicht gelöscht werden. Bitte kontrollieren Sie die rot markierten Felder.', MessageBoxAlertType.DANGER);

                return;
            }

            this.propertyContactPersonDeleteFormElement.submit();
        });
    }

}

export { DeletePage };
