import { BuildingUnitPage } from '../BuildingUnitPage';
import { BuildingUnitPageTab } from '../BuildingUnitPageTab';

class OverviewPage  extends BuildingUnitPage {

    constructor() {
        super(BuildingUnitPageTab.ContactPerson);
    }

}

export { OverviewPage };
