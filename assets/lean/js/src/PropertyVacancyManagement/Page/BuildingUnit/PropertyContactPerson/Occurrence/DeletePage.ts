import { OccurrenceDeletePage } from '../../OccurrenceDeletePage';
import { BuildingUnitPageTab } from '../../BuildingUnitPageTab';
import { PropertyContactPersonOccurrenceForm } from '../../../../Form/Property/PropertyContactPersonOccurrenceForm';

class DeletePage extends OccurrenceDeletePage {

    constructor() {
        super(BuildingUnitPageTab.ContactPerson, 'occurrence_delete_', new PropertyContactPersonOccurrenceForm('property_contact_person_occurrence_'));
    }

}

export { DeletePage };
