import { OccurrenceEditPage } from '../../OccurrenceEditPage';
import { BuildingUnitPageTab } from '../../BuildingUnitPageTab';
import { PropertyContactPersonOccurrenceForm } from '../../../../Form/Property/PropertyContactPersonOccurrenceForm';

class EditPage extends OccurrenceEditPage {

    constructor() {
        const idPrefix: string = 'property_contact_person_occurrence_';

        super(BuildingUnitPageTab.ContactPerson, idPrefix, new PropertyContactPersonOccurrenceForm(idPrefix));
    }

}

export { EditPage };
