import { BuildingUnitPage } from '../BuildingUnitPage';
import { BuildingUnitPageTab } from '../BuildingUnitPageTab';
import { PropertyContactPersonForm } from '../../../Form/Property/PropertyContactPersonForm';
import { MdcContextMenuComponent } from '../../../../Component/MdcContextMenuComponent';
import { MessageBoxComponent } from '../../../../Component/MessageBoxComponent/MessageBoxComponent';
import { MessageBoxAlertType } from '../../../../Component/MessageBoxComponent/MessageBoxAlertType';

class EditPage  extends BuildingUnitPage {

    private readonly propertyContactPersonForm: PropertyContactPersonForm;
    private readonly propertyContactPersonFormElement: HTMLFormElement;
    private readonly propertyContactPersonFormSubmitButton: HTMLButtonElement;
    private readonly contextMenu: MdcContextMenuComponent = null;
    private readonly messageBoxComponent: MessageBoxComponent;

    constructor() {
        super(BuildingUnitPageTab.ContactPerson);

        const idPrefix: string = 'property_contact_person_';

        this.propertyContactPersonForm = new PropertyContactPersonForm(idPrefix);
        this.propertyContactPersonFormElement = document.querySelector('#' + idPrefix + 'form');
        this.propertyContactPersonFormSubmitButton = document.querySelector('#' + idPrefix + 'save_submit_button');

        if (MdcContextMenuComponent.checkContainerExists(idPrefix) === true) {
            this.contextMenu = new MdcContextMenuComponent(idPrefix);
        }

        this.messageBoxComponent = new MessageBoxComponent();

        if (this.propertyContactPersonFormElement !== null) {
            this.propertyContactPersonFormSubmitButton.addEventListener('click', (): void => {
                if (this.propertyContactPersonForm.isFormValid() === false) {
                    this.messageBoxComponent.openMessageBox('Die Daten konnten nicht gespeichert werden. Bitte kontrollieren Sie die rot markierten Felder.', MessageBoxAlertType.DANGER);

                    return;
                }

                this.propertyContactPersonFormElement.submit();
            });
        }
    }

}

export { EditPage };
