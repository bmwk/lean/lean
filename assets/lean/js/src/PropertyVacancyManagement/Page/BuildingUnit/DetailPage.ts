import { BuildingUnitPage } from './BuildingUnitPage';
import { BuildingUnitPageTab } from './BuildingUnitPageTab';
import { BuildingUnitDetailForm } from '../../Form/BuildingUnit/BuildingUnitDetailForm';
import { MessageBoxComponent } from '../../../Component/MessageBoxComponent/MessageBoxComponent';
import { MessageBoxAlertType } from '../../../Component/MessageBoxComponent/MessageBoxAlertType';

class DetailPage extends BuildingUnitPage {

    private readonly buildingUnitDetailForm: BuildingUnitDetailForm;
    private readonly buildingUnitDetailFormElement: HTMLFormElement;
    private readonly buildingUnitDetailFormSubmitButton: HTMLButtonElement;
    private readonly messageBoxComponent: MessageBoxComponent;

    constructor() {
        super(BuildingUnitPageTab.Detail);

        const idPrefix: string = 'building_unit_detail_';

        this.buildingUnitDetailForm = new BuildingUnitDetailForm(idPrefix);
        this.buildingUnitDetailFormElement = document.querySelector('#' + idPrefix + 'form');
        this.buildingUnitDetailFormSubmitButton = document.querySelector('#' + idPrefix + 'save_submit_button');
        this.messageBoxComponent = new MessageBoxComponent();

        if (this.buildingUnitDetailFormSubmitButton !== null) {
            this.buildingUnitDetailFormSubmitButton.addEventListener('click', (): void => {
                if (this.buildingUnitDetailForm.isFormValid() === false) {
                    this.messageBoxComponent.openMessageBox('Die Daten konnten nicht gespeichert werden. Bitte kontrollieren Sie die rot markierten Felder.', MessageBoxAlertType.DANGER);

                    return;
                }

                this.buildingUnitDetailFormElement.submit();
            });
        }
    }

}

export { DetailPage };
