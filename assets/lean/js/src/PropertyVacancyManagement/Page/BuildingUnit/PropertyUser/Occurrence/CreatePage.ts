import { OccurrenceCreatePage } from '../../OccurrenceCreatePage';
import { BuildingUnitPageTab } from '../../BuildingUnitPageTab';
import { PropertyUserOccurrenceForm } from '../../../../Form/Property/PropertyUserOccurrenceForm';

class CreatePage extends OccurrenceCreatePage {

    constructor() {
        const idPrefix: string = 'property_user_occurrence_';

        super(BuildingUnitPageTab.PropertyUser, idPrefix, new PropertyUserOccurrenceForm(idPrefix));
    }

}

export { CreatePage };
