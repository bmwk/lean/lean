import { BuildingUnitPage } from '../BuildingUnitPage';
import { BuildingUnitPageTab } from '../BuildingUnitPageTab';
import { PropertyUserForm } from '../../../Form/Property/PropertyUserForm';
import { MessageBoxComponent } from '../../../../Component/MessageBoxComponent/MessageBoxComponent';
import { MessageBoxAlertType } from '../../../../Component/MessageBoxComponent/MessageBoxAlertType';

class CreatePage extends BuildingUnitPage {

    private readonly propertyUserForm: PropertyUserForm;
    private readonly propertyUserFormElement: HTMLFormElement;
    private readonly propertyUserFormSubmitButton: HTMLButtonElement;
    private readonly messageBoxComponent: MessageBoxComponent;

    constructor() {
        super(BuildingUnitPageTab.PropertyUser);

        const idPrefix: string = 'property_user_';

        this.propertyUserForm = new PropertyUserForm(idPrefix);
        this.propertyUserFormElement = document.querySelector('#' + idPrefix + 'form');
        this.propertyUserFormSubmitButton = document.querySelector('#' + idPrefix + 'save_submit_button');
        this.messageBoxComponent = new MessageBoxComponent();

        this.propertyUserFormSubmitButton.addEventListener('click', (): void => {
            if (this.propertyUserForm.isFormValid() === false) {
                this.messageBoxComponent.openMessageBox('Die Daten konnten nicht gespeichert werden. Bitte kontrollieren Sie die rot markierten Felder.', MessageBoxAlertType.DANGER);

                return;
            }

            this.propertyUserFormElement.submit();
        });
    }

}

export { CreatePage };
