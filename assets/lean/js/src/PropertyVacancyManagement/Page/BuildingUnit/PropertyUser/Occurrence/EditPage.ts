import { OccurrenceEditPage } from '../../OccurrenceEditPage';
import { BuildingUnitPageTab } from '../../BuildingUnitPageTab';
import { PropertyUserOccurrenceForm } from '../../../../Form/Property/PropertyUserOccurrenceForm';

class EditPage extends OccurrenceEditPage {

    constructor() {
        const idPrefix: string = 'property_user_occurrence_';

        super(BuildingUnitPageTab.PropertyUser, idPrefix, new PropertyUserOccurrenceForm(idPrefix));
    }

}

export { EditPage };
