import { BuildingUnitPage } from '../BuildingUnitPage';
import { BuildingUnitPageTab } from '../BuildingUnitPageTab';
import { BuildingUnitFeedbackComponent } from '../../../Component/BuildingUnit/BuildingUnitFeedbackComponent';
import { TabBarComponent } from '../../../../Component/TabBarComponent';
import { MDCTab, MDCTabInteractionEvent } from '@material/tab';

class OverviewPage extends BuildingUnitPage {

    private readonly correspondenceTabBar: TabBarComponent;
    private readonly correspondenceOccurrenceContentContainer: HTMLDivElement;
    private readonly correspondenceFeedbackContentContainer: HTMLDivElement;
    private readonly correspondenceDocumentTemplateContentContainer: HTMLDivElement;
    private readonly buildingUnitFeedbackComponent: BuildingUnitFeedbackComponent;

    constructor() {
        super(BuildingUnitPageTab.PropertyUser);

        this.correspondenceTabBar = new TabBarComponent(document.querySelector('#building_unit_property_user_correspondence_tab_bar'));
        this.correspondenceOccurrenceContentContainer = document.querySelector('#property_user_correspondence_occurrence_content');
        this.correspondenceFeedbackContentContainer = document.querySelector('#property_user_correspondence_feedback_content');
        this.correspondenceDocumentTemplateContentContainer = document.querySelector('#property_user_correspondence_document_template_content');
        this.buildingUnitFeedbackComponent = new BuildingUnitFeedbackComponent('building_unit_');

        this.correspondenceTabBar.getTabs().forEach((mdcTab: MDCTab): void => {
            mdcTab.listen('MDCTab:interacted', (event: MDCTabInteractionEvent): void => {
                this.showCorrespondenceContainerByTab(event.detail.tabId);
            });
        });
    }

    private showCorrespondenceContainerByTab(tabId: string): void {
        this.hideAllCorrespondenceContents();

        switch (tabId) {
            case 'building_unit_property_user_correspondence_occurrence_tab':
                this.correspondenceOccurrenceContentContainer.classList.remove('d-none');
                break
            case 'building_unit_property_user_correspondence_feedback_tab':
                this.correspondenceFeedbackContentContainer.classList.remove('d-none');
                break
            case 'building_unit_property_user_correspondence_document_template_tab':
                this.correspondenceDocumentTemplateContentContainer.classList.remove('d-none');
                break
        }
    }

    private hideAllCorrespondenceContents(): void {
        this.correspondenceOccurrenceContentContainer.classList.add('d-none');
        this.correspondenceFeedbackContentContainer.classList.add('d-none');
        this.correspondenceDocumentTemplateContentContainer.classList.add('d-none');
    }

}

export { OverviewPage };
