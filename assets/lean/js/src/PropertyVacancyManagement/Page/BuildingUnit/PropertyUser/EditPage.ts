import { BuildingUnitPage } from '../BuildingUnitPage';
import { BuildingUnitPageTab } from '../BuildingUnitPageTab';
import { PropertyUserForm } from '../../../Form/Property/PropertyUserForm';
import { MdcContextMenuComponent } from '../../../../Component/MdcContextMenuComponent';
import { MessageBoxComponent } from '../../../../Component/MessageBoxComponent/MessageBoxComponent';
import { MessageBoxAlertType } from '../../../../Component/MessageBoxComponent/MessageBoxAlertType';

class EditPage extends BuildingUnitPage {

    private readonly propertyUserForm: PropertyUserForm;
    private readonly propertyUserFormElement: HTMLFormElement;
    private readonly propertyUserFormSubmitButton: HTMLButtonElement;
    private readonly contextMenu: MdcContextMenuComponent = null;
    private readonly messageBoxComponent: MessageBoxComponent;

    constructor() {
        super(BuildingUnitPageTab.PropertyUser);

        const idPrefix: string = 'property_user_';

        this.propertyUserForm = new PropertyUserForm(idPrefix);
        this.propertyUserFormElement = document.querySelector('#' + idPrefix + 'form');
        this.propertyUserFormSubmitButton = document.querySelector('#' + idPrefix + 'save_submit_button');

        if (MdcContextMenuComponent.checkContainerExists(idPrefix) === true) {
            this.contextMenu = new MdcContextMenuComponent(idPrefix);
        }

        this.messageBoxComponent = new MessageBoxComponent();

        if (this.propertyUserFormElement !== null) {
            this.propertyUserFormSubmitButton.addEventListener('click', (): void => {
                if (this.propertyUserForm.isFormValid() === false) {
                    this.messageBoxComponent.openMessageBox('Die Daten konnten nicht gespeichert werden. Bitte kontrollieren Sie die rot markierten Felder.', MessageBoxAlertType.DANGER);

                    return;
                }

                this.propertyUserFormElement.submit();
            });
        }
    }

}

export { EditPage };
