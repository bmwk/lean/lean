import { BuildingUnitPage } from '../BuildingUnitPage';
import { BuildingUnitPageTab } from '../BuildingUnitPageTab';
import { PropertyUserRemoveForm } from '../../../Form/Property/PropertyUserRemoveForm';
import { PropertyUserForm } from '../../../Form/Property/PropertyUserForm';
import { MessageBoxComponent } from '../../../../Component/MessageBoxComponent/MessageBoxComponent';
import { MessageBoxAlertType } from '../../../../Component/MessageBoxComponent/MessageBoxAlertType';

class RemovePage extends BuildingUnitPage {

    private readonly propertyUserForm: PropertyUserForm;
    private readonly propertyUserRemoveForm: PropertyUserRemoveForm;
    private readonly propertyUserRemoveFormElement: HTMLFormElement;
    private readonly propertyUserRemoveFormSubmitButton: HTMLButtonElement;
    private readonly messageBoxComponent: MessageBoxComponent;

    constructor() {
        super(BuildingUnitPageTab.PropertyUser);

        const idPrefix: string = 'property_user_remove_';

        this.propertyUserForm = new PropertyUserForm('property_user_');
        this.propertyUserRemoveForm = new PropertyUserRemoveForm(idPrefix);
        this.propertyUserRemoveFormElement = document.querySelector('#' + idPrefix + 'form');
        this.propertyUserRemoveFormSubmitButton = document.querySelector('#' + idPrefix + 'remove_submit_button');
        this.messageBoxComponent = new MessageBoxComponent();

        this.propertyUserForm.disableFields();

        this.propertyUserRemoveFormSubmitButton.addEventListener('click', (): void => {
            if (this.propertyUserRemoveForm.isFormValid() === false) {
                this.messageBoxComponent.openMessageBox('Der Nutzer konnte nicht gelöscht werden. Bitte kontrollieren Sie die rot markierten Felder.', MessageBoxAlertType.DANGER);

                return;
            }

            this.propertyUserRemoveFormElement.submit();
        });
    }

}

export { RemovePage };
