import { OccurrenceDeletePage } from '../../OccurrenceDeletePage';
import { BuildingUnitPageTab } from '../../BuildingUnitPageTab';
import { PropertyUserOccurrenceForm } from '../../../../Form/Property/PropertyUserOccurrenceForm';

class DeletePage extends OccurrenceDeletePage {

    constructor() {
        super(BuildingUnitPageTab.PropertyUser, 'occurrence_delete_', new PropertyUserOccurrenceForm('property_user_occurrence_'));
    }

}

export { DeletePage };
