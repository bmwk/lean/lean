import React from 'react';
import chroma from 'chroma-js'
import _ from 'lodash'

type ColorStore = {
  map: Record<string, Record<string, string>>
  get: (key: string, context?: string | undefined) => string
}

const leftScale = chroma.scale([
  '#8e00ec',
  '#f7344d',
  '#00d6f4',
])

const rightScale = chroma.scale([
  '#f0dd35',
  '#00c37f',
  '#0087f0',
])

export function colorStore(): ColorStore {
  const colorMap: Record<string, string> = {}
  const colorMapContext: Record<string, Record<string, string>> = {
    default: colorMap,
  }

  function addColor(key: string, context = 'default') {
    const keys = [
      ..._.keys(colorMapContext[context] ?? {}),
      key,
    ]

    if (colorMapContext[context] == undefined) {
      colorMapContext[context] = {}
    }

    const leftColorsLength = Math.ceil(keys.length / 2)
    const rightColorsLength = keys.length - leftColorsLength

    const leftColors = leftScale.colors(leftColorsLength, 'hex')
    const rightColors = rightColorsLength > 0 ?
      rightScale.colors(rightColorsLength, 'hex') :
      []

    const colors = [
      ...leftColors,
      ...rightColors,
    ]

    keys.forEach((key, idx) => {
      const color = colors[idx]
      colorMapContext[context][key] = color
    })
  }

  return {
    map: colorMapContext,
    get: (key: string, context = 'default'): string => {
      const color = colorMapContext[context]?.[key]
      if (color != undefined) {
        return color
      } else {
        addColor(key, context)
        return colorMapContext[context]?.[key] as string
      }
    },
  }
}

export const ColorMapContext = React.createContext<ColorStore>(colorStore())

export function stableStringColor(stringInput: string, opacity?: number): string {
  const stringUniqueHash = stringInput.split('').reduce((acc, char) => {
    return Math.pow(char.charCodeAt(0), 10) + ((acc << 5) - acc);
  }, 0);

  return `hsl(${stringUniqueHash % 360}, 38%, ${opacity === undefined ? '68' : opacity}%)`;
}
