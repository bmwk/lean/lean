import { UserRegistrationForm } from './UserRegistrationForm';
import { TextFieldComponent } from '../../../Component/TextFieldComponent';

class CompanyUserRegistrationForm extends UserRegistrationForm {

    private readonly companyNameTextField: TextFieldComponent;

    constructor(idPrefix: string) {
        super(idPrefix);

        this.companyNameTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'companyName_mdc_text_field'));
    }

    public setRequiredFields(isRequired: boolean): void {
        super.setRequiredFields(isRequired);

        this.companyNameTextField.mdcTextField.required = isRequired;
    }

}

export { CompanyUserRegistrationForm };
