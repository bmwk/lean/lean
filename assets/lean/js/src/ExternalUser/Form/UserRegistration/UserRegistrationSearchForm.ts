import { SearchFieldComponent } from '../../../Component/SearchFieldComponent';

class UserRegistrationSearchForm {

    private readonly searchFieldComponent: SearchFieldComponent;

    constructor(idPrefix: string) {
        this.searchFieldComponent = new SearchFieldComponent(idPrefix, 'text');
    }

}

export { UserRegistrationSearchForm };
