import { SelectComponent } from '../../../Component/SelectComponent';
import { TextFieldComponent } from '../../../Component/TextFieldComponent';

abstract class UserRegistrationForm {

    protected readonly salutationSelect: SelectComponent;
    protected readonly lastNameTextField: TextFieldComponent;
    protected readonly firstNameTextField: TextFieldComponent;
    protected readonly emailTextField: TextFieldComponent;
    protected readonly streetNameTextField: TextFieldComponent;
    protected readonly houseNumberTextField: TextFieldComponent;
    protected readonly postalCodeTextField: TextFieldComponent;
    protected readonly placeNameTextField: TextFieldComponent;
    protected readonly phoneNumberTextField: TextFieldComponent;

    protected constructor(idPrefix: string) {
        this.salutationSelect = new SelectComponent(document.querySelector('#' + idPrefix + 'salutation_mdc_select'));
        this.lastNameTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'lastName_mdc_text_field'));
        this.firstNameTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'firstName_mdc_text_field'));
        this.emailTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'email_mdc_text_field'));
        this.streetNameTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'streetName_mdc_text_field'));
        this.houseNumberTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'houseNumber_mdc_text_field'));
        this.postalCodeTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'postalCode_mdc_text_field'));
        this.placeNameTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'placeName_mdc_text_field'));
        this.phoneNumberTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'phoneNumber_mdc_text_field'));
    }

    public setRequiredFields(isRequired: boolean): void {
        this.salutationSelect.mdcSelect.required = isRequired;
        this.lastNameTextField.mdcTextField.required = isRequired;
        this.firstNameTextField.mdcTextField.required = isRequired;
        this.emailTextField.mdcTextField.required = isRequired;
        this.streetNameTextField.mdcTextField.required = isRequired;
        this.houseNumberTextField.mdcTextField.required = isRequired;
        this.postalCodeTextField.mdcTextField.required = isRequired;
        this.placeNameTextField.mdcTextField.required = isRequired;
    }

    public isFormValid(): boolean {
        let hasErrors: boolean = false;

        if (this.salutationSelect.mdcSelect.valid === false) {
            this.salutationSelect.mdcSelect.valid = false;
            hasErrors = true;
        }

        if (this.lastNameTextField.mdcTextField.valid === false) {
            this.lastNameTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (this.firstNameTextField.mdcTextField.valid === false) {
            this.firstNameTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (this.emailTextField.mdcTextField.valid === false) {
            this.emailTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (this.streetNameTextField.mdcTextField.valid === false) {
            this.streetNameTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (this.houseNumberTextField.mdcTextField.valid === false) {
            this.houseNumberTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (this.postalCodeTextField.mdcTextField.valid === false) {
            this.postalCodeTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (this.placeNameTextField.mdcTextField.valid === false) {
            this.placeNameTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        return (hasErrors === false);
    }

}

export { UserRegistrationForm };
