import { UserRegistrationForm } from './UserRegistrationForm';

class NaturalPersonUserRegistrationForm extends UserRegistrationForm {

    constructor(idPrefix: string) {
        super(idPrefix);
    }

}

export { NaturalPersonUserRegistrationForm };
