import { DeleteForm } from '../../../Form/DeleteForm';

class UserRegistrationDeleteForm extends DeleteForm {

    constructor(idPrefix: string) {
        super(idPrefix);
    }

}

export { UserRegistrationDeleteForm };
