import { DeleteForm } from '../../../Form/DeleteForm';

class UserInvitationDeleteForm extends DeleteForm {

    constructor(idPrefix: string) {
        super(idPrefix);
    }

}

export { UserInvitationDeleteForm };
