import { SearchFieldComponent } from '../../../Component/SearchFieldComponent';

class UserInvitationSearchForm {

    private readonly searchFieldComponent: SearchFieldComponent;

    constructor(idPrefix: string) {
        this.searchFieldComponent = new SearchFieldComponent(idPrefix, 'text');
    }

}

export { UserInvitationSearchForm };
