import { UserInvitationForm } from './UserInvitationForm';

class NaturalPersonUserInvitationForm extends UserInvitationForm {

    constructor(idPrefix: string) {
        super(idPrefix);
    }

}

export { NaturalPersonUserInvitationForm };
