import { SelectComponent } from '../../../Component/SelectComponent';
import { TextFieldComponent } from '../../../Component/TextFieldComponent';

abstract class UserInvitationForm {

    protected readonly salutationSelect: SelectComponent;
    protected readonly firstNameTextField: TextFieldComponent;
    protected readonly lastNameTextField: TextFieldComponent;
    protected readonly emailTextField: TextFieldComponent;
    protected readonly userRegistrationTypeSelect: SelectComponent;

    protected constructor(idPrefix: string) {
        this.salutationSelect = new SelectComponent(document.querySelector('#' + idPrefix + 'salutation_mdc_select'));
        this.firstNameTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'firstName_mdc_text_field'));
        this.lastNameTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'lastName_mdc_text_field'));
        this.emailTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'email_mdc_text_field'));
        this.userRegistrationTypeSelect = new SelectComponent(document.querySelector('#' + idPrefix + 'userRegistrationType_mdc_select'));
    }

    public setRequiredFields(isRequired: boolean): void {
        this.salutationSelect.mdcSelect.required = isRequired;
        this.lastNameTextField.mdcTextField.required = isRequired;
        this.firstNameTextField.mdcTextField.required = isRequired;
        this.emailTextField.mdcTextField.required = isRequired;
        this.userRegistrationTypeSelect.mdcSelect.required = isRequired;
    }

    public isFormValid(): boolean {
        let hasErrors: boolean = false;

        if (this.salutationSelect.mdcSelect.valid === false) {
            this.salutationSelect.mdcSelect.valid = false;
            hasErrors = true;
        }

        if (this.lastNameTextField.mdcTextField.valid === false) {
            this.lastNameTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (this.firstNameTextField.mdcTextField.valid === false) {
            this.firstNameTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (this.emailTextField.mdcTextField.valid === false) {
            this.emailTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (this.userRegistrationTypeSelect.mdcSelect.valid === false) {
            this.userRegistrationTypeSelect.mdcSelect.valid = false;
            hasErrors = true;
        }

        return (hasErrors === false);
    }

}

export { UserInvitationForm };
