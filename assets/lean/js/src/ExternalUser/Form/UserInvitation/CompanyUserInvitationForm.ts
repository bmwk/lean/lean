import { TextFieldComponent } from '../../../Component/TextFieldComponent';
import { UserInvitationForm } from './UserInvitationForm';

class CompanyUserInvitationForm extends UserInvitationForm {

    private readonly companyNameTextField: TextFieldComponent;

    constructor(idPrefix: string) {
        super(idPrefix);

        this.companyNameTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'companyName_mdc_text_field'));
    }

    public setRequiredFields(isRequired: boolean): void {
        super.setRequiredFields(isRequired);

        this.companyNameTextField.mdcTextField.required = isRequired;
    }

    public isFormValid(): boolean {
        let hasErrors: boolean = (super.isFormValid() === false);

        if (this.companyNameTextField.mdcTextField.valid === false) {
            this.companyNameTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        return (hasErrors === false);
    }

}

export { CompanyUserInvitationForm };
