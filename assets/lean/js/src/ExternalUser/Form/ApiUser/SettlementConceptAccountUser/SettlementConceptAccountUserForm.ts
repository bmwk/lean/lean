import { TextFieldComponent } from "../../../../Component/TextFieldComponent";
import { FormFieldValidator } from "../../../../FormFieldValidator/FormFieldValidator";

class SettlementConceptAccountUserForm {

    private readonly fullNameTextField: TextFieldComponent;
    private readonly emailTextField: TextFieldComponent;

    constructor(idPrefix: string) {
        this.fullNameTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'fullName_mdc_text_field'));
        this.emailTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'email_mdc_text_field'));

        this.fullNameTextField.mdcTextField.required = true;
        this.emailTextField.mdcTextField.required = true;
    }

    public isFormValid(): boolean {
        let hasErrors: boolean = false;

        if (this.fullNameTextField.mdcTextField.valid === false) {
            this.fullNameTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (FormFieldValidator.validateEmail(this.emailTextField.mdcTextField.value) === false) {
            this.emailTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        return (hasErrors === false);
    }

}

export { SettlementConceptAccountUserForm };
