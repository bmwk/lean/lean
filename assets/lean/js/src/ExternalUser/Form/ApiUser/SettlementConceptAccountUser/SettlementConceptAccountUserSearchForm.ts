import { SearchFieldComponent } from '../../../../Component/SearchFieldComponent';

class SettlementConceptAccountUserSearchForm {

    private readonly searchField: SearchFieldComponent;

    constructor(idPrefix: string) {
        this.searchField = new SearchFieldComponent(idPrefix, 'text');
    }

}

export { SettlementConceptAccountUserSearchForm };
