import { DeleteForm } from '../../../../Form/DeleteForm';

class SettlementConceptAccountUserDeleteForm extends DeleteForm {

    constructor(idPrefix: string ) {
        super(idPrefix);
    }

}

export { SettlementConceptAccountUserDeleteForm };
