import { TextFieldComponent } from '../../../Component/TextFieldComponent';
import { FormFieldValidator } from '../../../FormFieldValidator/FormFieldValidator';
import { PersonForm } from '../../../PersonManagement/Form/Person/PersonForm';
import { NaturalPersonForm } from '../../../PersonManagement/Form/Person/NaturalPersonForm';
import { CompanyForm } from '../../../PersonManagement/Form/Person/CompanyForm';
import { CommuneForm } from '../../../PersonManagement/Form/Person/CommuneForm';

class AccountUserForm {

    private readonly fullNameTextField: TextFieldComponent;
    private readonly nameAbbreviationTextField: TextFieldComponent;
    private readonly emailTextField: TextFieldComponent;
    private readonly usernameTextField: TextFieldComponent;
    private readonly personTypeNameInputField: HTMLInputElement;
    private readonly personForm: PersonForm = null;

    constructor(idPrefix: string) {
        this.fullNameTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'fullName_mdc_text_field'));
        this.nameAbbreviationTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'nameAbbreviation_mdc_text_field'));
        this.emailTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'email_mdc_text_field'));
        this.usernameTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'username_mdc_text_field'));
        this.personTypeNameInputField = document.querySelector('#' + idPrefix + 'person_personTypeName');

        if (this.personTypeNameInputField !== null) {
            switch (this.personTypeNameInputField.value) {
                case 'NATURAL_PERSON':
                    this.personForm = new NaturalPersonForm(idPrefix + 'person_');
                    break;
                case 'COMPANY':
                    this.personForm = new CompanyForm(idPrefix + 'person_');
                    break;
                case 'COMMUNE':
                    this.personForm = new CommuneForm(idPrefix + 'person_');
                    break;
            }
        }

        this.fullNameTextField.mdcTextField.required = true;
        this.emailTextField.mdcTextField.required = true;
        this.usernameTextField.mdcTextField.required = true;
    }

    public isFormValid(): boolean {
        let hasErrors: boolean = false;

        if (this.fullNameTextField.mdcTextField.valid === false) {
            this.fullNameTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (FormFieldValidator.validateEmail(this.emailTextField.mdcTextField.value) === false) {
            this.emailTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (this.usernameTextField.mdcTextField.valid === false) {
            this.usernameTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (this.personForm !== null && this.personForm.isFormValid() === false) {
            hasErrors = true;
        }

        return (hasErrors === false);
    }

}

export { AccountUserForm };
