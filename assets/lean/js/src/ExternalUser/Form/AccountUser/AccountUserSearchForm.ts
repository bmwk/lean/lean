import { SearchFieldComponent } from '../../../Component/SearchFieldComponent';

class AccountUserSearchForm {

    private readonly searchField: SearchFieldComponent;

    constructor(idPrefix: string) {
        this.searchField = new SearchFieldComponent(idPrefix, 'text');
    }

}

export { AccountUserSearchForm };
