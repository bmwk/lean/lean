import { ListComponent } from '../../../Component/ListComponent';

class UserRegistrationListComponent extends ListComponent {

    constructor(idPrefix: string) {
        super(idPrefix);
    }

}

export { UserRegistrationListComponent };
