import { ListComponent } from '../../../../Component/ListComponent';

class SettlementConceptAccountUserListComponent extends ListComponent {

    constructor(idPrefix: string) {
        super(idPrefix);
    }

}

export { SettlementConceptAccountUserListComponent };
