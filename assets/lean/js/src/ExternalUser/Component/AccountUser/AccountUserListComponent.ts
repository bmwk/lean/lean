import { ListComponent } from '../../../Component/ListComponent';

class AccountUserListComponent extends ListComponent {

    constructor(idPrefix: string) {
        super(idPrefix);
    }

}

export { AccountUserListComponent };
