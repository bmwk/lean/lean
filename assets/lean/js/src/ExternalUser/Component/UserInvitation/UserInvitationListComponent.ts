import { ListComponent } from '../../../Component/ListComponent';

class UserInvitationListComponent extends ListComponent {

    constructor(idPrefix: string) {
        super(idPrefix);
    }

}

export { UserInvitationListComponent };
