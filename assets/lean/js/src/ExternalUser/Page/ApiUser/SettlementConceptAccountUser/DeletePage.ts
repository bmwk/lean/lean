import { SettlementConceptAccountUserDeleteForm } from '../../../Form/ApiUser/SettlementConceptAccountUser/SettlementConceptAccountUserDeleteForm';
import { SettlementConceptAccountUserForm } from '../../../Form/ApiUser/SettlementConceptAccountUser/SettlementConceptAccountUserForm';

class DeletePage {

    private readonly settlementConceptAccountUserForm: SettlementConceptAccountUserForm;
    private readonly settlementConceptAccountUserDeleteForm: SettlementConceptAccountUserDeleteForm;
    private readonly settlementConceptAccountUserDeleteFormElement: HTMLFormElement;
    private readonly settlementConceptAccountUserDeleteFormSubmitButton: HTMLButtonElement;

    constructor() {
        const idPrefix: string = 'settlement_concept_account_user_delete_';

        this.settlementConceptAccountUserForm = new SettlementConceptAccountUserForm('settlement_concept_account_user_');
        this.settlementConceptAccountUserDeleteForm = new SettlementConceptAccountUserDeleteForm(idPrefix);
        this.settlementConceptAccountUserDeleteFormElement = document.querySelector('#' + idPrefix + 'form');
        this.settlementConceptAccountUserDeleteFormSubmitButton = document.querySelector('#' + idPrefix + 'delete_submit_button');

        this.settlementConceptAccountUserDeleteFormSubmitButton.addEventListener('click', (): void => {
            this.settlementConceptAccountUserDeleteFormElement.submit();
        });
    }

}

export { DeletePage };
