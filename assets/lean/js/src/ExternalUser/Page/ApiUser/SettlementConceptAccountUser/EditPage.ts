import { SettlementConceptAccountUserForm } from '../../../Form/ApiUser/SettlementConceptAccountUser/SettlementConceptAccountUserForm';
import { MdcContextMenuComponent } from '../../../../Component/MdcContextMenuComponent';
import { MessageBoxAlertType } from '../../../../Component/MessageBoxComponent/MessageBoxAlertType';
import { MessageBoxComponent } from '../../../../Component/MessageBoxComponent/MessageBoxComponent';

class EditPage {

    private readonly settlementConceptAccountUserForm: SettlementConceptAccountUserForm;
    private readonly settlementConceptAccountUserFormElement: HTMLFormElement;
    private readonly settlementConceptAccountUserFormSubmitButton: HTMLButtonElement;
    private readonly contextMenu: MdcContextMenuComponent = null;
    private readonly messageBoxComponent: MessageBoxComponent;

    constructor() {
        const idPrefix: string = 'settlement_concept_account_user_';

        this.settlementConceptAccountUserForm = new SettlementConceptAccountUserForm(idPrefix);
        this.settlementConceptAccountUserFormElement = document.querySelector('#' + idPrefix + 'form');
        this.settlementConceptAccountUserFormSubmitButton = document.querySelector('#' + idPrefix + 'save_submit_button');

        if (MdcContextMenuComponent.checkContainerExists(idPrefix) === true) {
            this.contextMenu = new MdcContextMenuComponent(idPrefix);
        }

        if (this.settlementConceptAccountUserFormElement !== null) {
            this.settlementConceptAccountUserFormSubmitButton.addEventListener('click', (): void => {
                if (this.settlementConceptAccountUserForm.isFormValid() === false) {
                    this.messageBoxComponent.openMessageBox('Die Daten konnten nicht gespeichert werden. Bitte kontrollieren Sie die rot markierten Felder.', MessageBoxAlertType.DANGER);

                    return;
                }

                this.settlementConceptAccountUserFormElement.submit();
            });
        }
    }

}

export { EditPage };
