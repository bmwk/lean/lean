import { SettlementConceptAccountUserForm } from '../../../Form/ApiUser/SettlementConceptAccountUser/SettlementConceptAccountUserForm';
import { MessageBoxComponent } from "../../../../Component/MessageBoxComponent/MessageBoxComponent";
import { MessageBoxAlertType } from "../../../../Component/MessageBoxComponent/MessageBoxAlertType";

class CreatePage {

    private readonly settlementConceptAccountUserForm: SettlementConceptAccountUserForm;
    private readonly settlementConceptAccountUserFormElement: HTMLFormElement;
    private readonly settlementConceptAccountUserFormSubmitButton: HTMLButtonElement;
    private readonly messageBoxComponent: MessageBoxComponent;

    constructor() {
        const idPrefix: string = 'settlement_concept_account_user_';

        this.settlementConceptAccountUserForm = new SettlementConceptAccountUserForm(idPrefix);
        this.settlementConceptAccountUserFormElement = document.querySelector('#' + idPrefix + 'form');
        this.settlementConceptAccountUserFormSubmitButton = document.querySelector('#' + idPrefix + 'save_submit_button');
        this.messageBoxComponent = new MessageBoxComponent();

        this.settlementConceptAccountUserFormSubmitButton.addEventListener('click', (): void => {
            if (this.settlementConceptAccountUserForm.isFormValid() === false) {
                this.messageBoxComponent.openMessageBox('Die Daten konnten nicht gespeichert werden. Bitte kontrollieren Sie die rot markierten Felder.', MessageBoxAlertType.DANGER);

                return;
            }

            this.settlementConceptAccountUserFormElement.submit();
        });
    }

}

export { CreatePage };
