import { SettlementConceptAccountUserListComponent } from '../../../Component/ApiUser/SettlementConceptAccountUser/SettlementConceptAccountUserListComponent';
import { SettlementConceptAccountUserSearchForm } from '../../../Form/ApiUser/SettlementConceptAccountUser/SettlementConceptAccountUserSearchForm';

class OverviewPage {

    private readonly settlementConceptAccountUserListComponent: SettlementConceptAccountUserListComponent;
    private readonly settlementConceptAccountUserSearchForm: SettlementConceptAccountUserSearchForm;

    constructor() {
        this.settlementConceptAccountUserSearchForm = new SettlementConceptAccountUserSearchForm('settlement_concept_account_user_search_');

        if (SettlementConceptAccountUserListComponent.checkContainerExists('settlement_concept_account_user_') === true) {
            this.settlementConceptAccountUserListComponent = new SettlementConceptAccountUserListComponent('settlement_concept_account_user_');
        }
    }

}

export { OverviewPage };
