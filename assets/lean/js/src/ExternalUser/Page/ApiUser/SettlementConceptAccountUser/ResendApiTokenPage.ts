class ResendApiTokenPage {

    private readonly resendApiTokenFormElement: HTMLFormElement;
    private readonly resendApiTokenFormSubmitButton: HTMLButtonElement;

    constructor() {
        const idPrefix: string = 'settlement_concept_account_user_resend_api_token_';

        this.resendApiTokenFormElement = document.querySelector('#' + idPrefix + 'form');
        this.resendApiTokenFormSubmitButton = document.querySelector('#' + idPrefix + 'save_submit_button');

        this.resendApiTokenFormSubmitButton.addEventListener('click', (): void => {
            this.resendApiTokenFormElement.submit();
        });
    }

}

export { ResendApiTokenPage };
