import { MdcTabBarPage } from '../../../MdcTabBarPage';
import { OverviewPageTab } from './OverviewPageTab';
import { UserInvitationFilterForm } from '../../Form/UserInvitation/UserInvitationFilterForm';
import { UserInvitationSearchForm } from '../../Form/UserInvitation/UserInvitationSearchForm';
import { UserInvitationListComponent } from '../../Component/UserInvitation/UserInvitationListComponent';

abstract class OverviewPage extends MdcTabBarPage {

    protected readonly userInvitationFilterForm: UserInvitationFilterForm;
    protected readonly userInvitationSearchForm: UserInvitationSearchForm;
    protected readonly userInvitationListComponent: UserInvitationListComponent;

    protected constructor(overviewPageTab: OverviewPageTab) {
        super(document.querySelector('#user_invitation_type_mdc_tab_bar'))

        this.activateTab(overviewPageTab);

        this.userInvitationFilterForm = new UserInvitationFilterForm('user_invitation_filter_');
        this.userInvitationSearchForm = new UserInvitationSearchForm('user_invitation_search_');

        if (UserInvitationListComponent.checkContainerExists('user_invitation_') === true) {
            this.userInvitationListComponent = new UserInvitationListComponent('user_invitation_');
        }
    }

}

export { OverviewPage };
