import { UserInvitationForm } from '../../Form/UserInvitation/UserInvitationForm';
import { NaturalPersonUserInvitationForm } from '../../Form/UserInvitation/NaturalPersonUserInvitationForm';
import { CompanyUserInvitationForm } from '../../Form/UserInvitation/CompanyUserInvitationForm';
import { MdcContextMenuComponent } from '../../../Component/MdcContextMenuComponent';
import { MessageBoxComponent } from '../../../Component/MessageBoxComponent/MessageBoxComponent';
import { MessageBoxAlertType } from '../../../Component/MessageBoxComponent/MessageBoxAlertType';

class EditPage {

    private readonly userInvitationForm: UserInvitationForm;
    private readonly userInvitationFormElement: HTMLFormElement;
    private readonly userInvitationFormSubmitButton: HTMLButtonElement;
    private readonly contextMenu: MdcContextMenuComponent = null;
    private readonly messageBoxComponent: MessageBoxComponent;

    constructor() {
        const idPrefix: string = 'user_invitation_';

        if (document.querySelector('#natural_person_' + idPrefix + 'form') !== null) {
            this.userInvitationForm = new NaturalPersonUserInvitationForm('natural_person_' + idPrefix);
            this.userInvitationFormElement = document.querySelector('#natural_person_' + idPrefix + 'form');
            this.userInvitationFormSubmitButton = document.querySelector('#natural_person_' + idPrefix + 'save_submit_button');
        }

        if (document.querySelector('#company_' + idPrefix + 'form') !== null) {
            this.userInvitationForm = new CompanyUserInvitationForm('company_' + idPrefix);
            this.userInvitationFormElement = document.querySelector('#company_' + idPrefix + 'form');
            this.userInvitationFormSubmitButton = document.querySelector('#company_' + idPrefix + 'save_submit_button');
        }

        if (MdcContextMenuComponent.checkContainerExists(idPrefix) === true) {
            this.contextMenu = new MdcContextMenuComponent(idPrefix);
        }

        this.messageBoxComponent = new MessageBoxComponent();

        if (this.userInvitationFormSubmitButton !== null) {
            this.userInvitationFormSubmitButton.addEventListener('click', (): void => {
                if (this.userInvitationForm.isFormValid() === false) {
                    this.messageBoxComponent.openMessageBox('Die Daten konnten nicht gespeichert werden. Bitte kontrollieren Sie die rot markierten Felder.', MessageBoxAlertType.DANGER);

                    return;
                }

                this.userInvitationFormElement.submit();
            });
        }
    }

}

export { EditPage };
