enum OverviewPageTab {
    PropertySeeker = 'user_invitation_property_seeker_tab',
    PropertyProvider = 'user_invitation_property_provider_tab'
}

export { OverviewPageTab };
