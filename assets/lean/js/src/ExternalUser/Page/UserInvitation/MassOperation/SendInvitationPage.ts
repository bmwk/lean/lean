import { MassOperationPage } from '../../MassOperationPage';

class SendInvitationPage extends MassOperationPage {

    constructor() {
        const idPrefix: string = 'user_invitation_mass_operation_send_invitation_';

        super(
            document.querySelector('#' + idPrefix + 'form'),
            document.querySelector('#' + idPrefix + 'send_invitation_submit_button')
        );
    }

    protected doOnConfirmationFormSubmit(): void {
        this.confirmationFormElement.submit();
    }

}

export { SendInvitationPage };
