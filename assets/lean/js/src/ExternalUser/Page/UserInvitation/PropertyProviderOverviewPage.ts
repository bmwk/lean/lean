import { OverviewPage } from './OverviewPage';
import { OverviewPageTab } from './OverviewPageTab';

class PropertyProviderOverviewPage extends OverviewPage {

    constructor() {
        super(OverviewPageTab.PropertyProvider);
    }

}

export { PropertyProviderOverviewPage };
