import { UserInvitationDeleteForm } from '../../Form/UserInvitation/UserInvitationDeleteForm';
import { UserInvitationForm } from '../../Form/UserInvitation/UserInvitationForm';
import { NaturalPersonUserInvitationForm } from '../../Form/UserInvitation/NaturalPersonUserInvitationForm';
import { CompanyUserInvitationForm } from '../../Form/UserInvitation/CompanyUserInvitationForm';

class DeletePage {

    private readonly userInvitationForm: UserInvitationForm;
    private readonly userInvitationDeleteForm: UserInvitationDeleteForm;
    private readonly userInvitationDeleteFormElement: HTMLFormElement;
    private readonly userInvitationDeleteFormSubmitButton: HTMLButtonElement;

    constructor() {
        const idPrefix: string = 'user_invitation_delete_';

        if (document.querySelector('#natural_person_user_invitation_form') !== null) {
            this.userInvitationForm = new NaturalPersonUserInvitationForm('natural_person_user_invitation_');
        }

        if (document.querySelector('#company_user_invitation_form') !== null) {
            this.userInvitationForm = new CompanyUserInvitationForm('company_user_invitation_');
        }

        this.userInvitationDeleteForm = new UserInvitationDeleteForm(idPrefix);
        this.userInvitationDeleteFormElement = document.querySelector('#' + idPrefix + 'form');
        this.userInvitationDeleteFormSubmitButton = document.querySelector('#' + idPrefix + 'delete_submit_button');

        this.userInvitationDeleteFormSubmitButton.addEventListener('click', (): void => {
            this.userInvitationDeleteFormElement.submit();
        });
    }

}

export { DeletePage };
