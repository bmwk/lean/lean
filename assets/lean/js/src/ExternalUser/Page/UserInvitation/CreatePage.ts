import { UserInvitationForm } from '../../Form/UserInvitation/UserInvitationForm';
import { NaturalPersonUserInvitationForm } from '../../Form/UserInvitation/NaturalPersonUserInvitationForm';
import { CompanyUserInvitationForm } from '../../Form/UserInvitation/CompanyUserInvitationForm';
import { MessageBoxComponent } from '../../../Component/MessageBoxComponent/MessageBoxComponent';
import { MessageBoxAlertType } from '../../../Component/MessageBoxComponent/MessageBoxAlertType';
import { MDCTabBar, MDCTabBarActivatedEvent } from '@material/tab-bar';

class CreatePage {

    private readonly naturalPersonUserInvitationForm: UserInvitationForm;
    private readonly naturalPersonUserInvitationFormElement: HTMLFormElement;
    private readonly naturalPersonUserInvitationFormSubmitButton: HTMLButtonElement;
    private readonly companyUserInvitationForm: UserInvitationForm;
    private readonly companyUserInvitationFormElement: HTMLFormElement;
    private readonly companyUserInvitationFormSubmitButton: HTMLButtonElement;
    private readonly personTypeMdcTabBar: MDCTabBar;
    private readonly naturalPersonFormContainer: HTMLDivElement;
    private readonly companyFormContainer: HTMLDivElement;
    private readonly messageBoxComponent: MessageBoxComponent;

    constructor() {
        const idPrefix: string = 'user_invitation_';

        this.naturalPersonUserInvitationForm = new NaturalPersonUserInvitationForm('natural_person_' + idPrefix);
        this.naturalPersonUserInvitationFormElement = document.querySelector('#natural_person_' + idPrefix + 'form');
        this.naturalPersonUserInvitationFormSubmitButton = document.querySelector('#natural_person_' + idPrefix + 'save_submit_button');
        this.companyUserInvitationForm = new CompanyUserInvitationForm('company_' + idPrefix);
        this.companyUserInvitationFormElement = document.querySelector('#company_' + idPrefix + 'form');
        this.companyUserInvitationFormSubmitButton = document.querySelector('#company_' + idPrefix + 'save_submit_button');
        this.personTypeMdcTabBar = new MDCTabBar(document.querySelector('#' + idPrefix + 'person_type_mdc_tab_bar'));
        this.naturalPersonFormContainer = document.querySelector('#' + idPrefix + 'natural_person_form_container');
        this.companyFormContainer = document.querySelector('#' + idPrefix + 'company_form_container');
        this.messageBoxComponent = new MessageBoxComponent();

        this.naturalPersonUserInvitationForm.setRequiredFields(true);
        this.companyUserInvitationForm.setRequiredFields(true);

        this.naturalPersonUserInvitationFormSubmitButton.addEventListener('click', (): void => {
            if (this.naturalPersonUserInvitationForm.isFormValid() === false) {
                this.messageBoxComponent.openMessageBox('Die Daten konnten nicht gespeichert werden. Bitte kontrollieren Sie die rot markierten Felder.', MessageBoxAlertType.DANGER);

                return;
            }

            this.naturalPersonUserInvitationFormElement.submit();
        });

        this.companyUserInvitationFormSubmitButton.addEventListener('click', (): void => {
            if (this.companyUserInvitationForm.isFormValid() === false) {
                this.messageBoxComponent.openMessageBox('Die Daten konnten nicht gespeichert werden. Bitte kontrollieren Sie die rot markierten Felder.', MessageBoxAlertType.DANGER);

                return;
            }

            this.companyUserInvitationFormElement.submit();
        });

        this.personTypeMdcTabBar.listen('MDCTabBar:activated', (event: MDCTabBarActivatedEvent): void => {
            this.showOrHideFormContainersByTabIndex(event.detail.index);
        });

        this.personTypeMdcTabBar.activateTab(0);
    }

    private showOrHideFormContainersByTabIndex(tabIndex: number): void {
        this.hideAllFormContainers();

        switch (tabIndex) {
            case 0:
                this.naturalPersonUserInvitationFormSubmitButton.classList.remove('d-none');
                this.companyUserInvitationFormSubmitButton.classList.add('d-none');
                this.showNaturalPersonUserRegistrationForm();
                break
            case 1:
                this.companyUserInvitationFormSubmitButton.classList.remove('d-none');
                this.naturalPersonUserInvitationFormSubmitButton.classList.add('d-none');
                this.showCompanyUserRegistrationForm();
                break
        }
    }

    private hideAllFormContainers(): void {
        this.hideNaturalPersonUserRegistrationForm();
        this.hideCompanyUserRegistrationForm();
    }

    private showNaturalPersonUserRegistrationForm(): void {
        this.naturalPersonFormContainer.classList.remove('d-none');
    }

    private hideNaturalPersonUserRegistrationForm(): void {
        this.naturalPersonFormContainer.classList.add('d-none');
    }

    private showCompanyUserRegistrationForm(): void {
        this.companyFormContainer.classList.remove('d-none');
    }

    private hideCompanyUserRegistrationForm(): void {
        this.companyFormContainer.classList.add('d-none');
    }

}

export { CreatePage };
