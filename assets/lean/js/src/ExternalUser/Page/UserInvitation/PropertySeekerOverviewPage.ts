import { OverviewPage } from './OverviewPage';
import { OverviewPageTab } from './OverviewPageTab';

class PropertySeekerOverviewPage extends OverviewPage {

    constructor() {
        super(OverviewPageTab.PropertySeeker);
    }

}

export { PropertySeekerOverviewPage };
