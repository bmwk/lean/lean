import { AccountUserFilterForm } from '../../Form/AccountUser/AccountUserFilterForm';
import { AccountUserSearchForm } from '../../Form/AccountUser/AccountUserSearchForm';
import { AccountUserListComponent } from '../../Component/AccountUser/AccountUserListComponent';

class OverviewPage {

    private readonly accountUserFilterForm: AccountUserFilterForm;
    private readonly accountUserSearchForm: AccountUserSearchForm;
    private readonly accountUserListComponent: AccountUserListComponent;

    constructor() {
        this.accountUserFilterForm = new AccountUserFilterForm('account_user_filter_');
        this.accountUserSearchForm = new AccountUserSearchForm('account_user_search_');

        if (AccountUserListComponent.checkContainerExists('account_user_') === true) {
            this.accountUserListComponent = new AccountUserListComponent('account_user_');
        }
    }

}

export { OverviewPage };
