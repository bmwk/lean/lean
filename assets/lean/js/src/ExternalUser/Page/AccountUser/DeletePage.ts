import { AccountUserDeleteForm } from '../../Form/AccountUser/AccountUserDeleteForm';
import { AccountUserForm } from '../../Form/AccountUser/AccountUserForm';

class DeletePage {

    private readonly accountUserForm: AccountUserForm
    private readonly accountUserDeleteForm: AccountUserDeleteForm;
    private readonly accountUserDeleteFormElement: HTMLFormElement;
    private readonly accountUserDeleteFormSubmitButton: HTMLButtonElement;

    constructor() {
        const idPrefix: string = 'account_user_delete_';

        this.accountUserForm = new AccountUserForm('account_user_');
        this.accountUserDeleteForm = new AccountUserDeleteForm(idPrefix);
        this.accountUserDeleteFormElement = document.querySelector('#' + idPrefix + 'form');
        this.accountUserDeleteFormSubmitButton = document.querySelector('#' + idPrefix + 'delete_submit_button');

        this.accountUserDeleteFormSubmitButton.addEventListener('click', (): void => {
            this.accountUserDeleteFormElement.submit();
        });
    }

}

export { DeletePage };
