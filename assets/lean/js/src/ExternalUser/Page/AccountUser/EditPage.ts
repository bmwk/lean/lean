import { AccountUserForm } from '../../Form/AccountUser/AccountUserForm';
import { MdcContextMenuComponent } from '../../../Component/MdcContextMenuComponent';
import { MessageBoxComponent } from '../../../Component/MessageBoxComponent/MessageBoxComponent';
import { MessageBoxAlertType } from '../../../Component/MessageBoxComponent/MessageBoxAlertType';

class EditPage {

    private readonly accountUserForm: AccountUserForm;
    private readonly accountUserFormElement: HTMLFormElement;
    private readonly accountUserFormSubmitButton: HTMLButtonElement;
    private readonly contextMenu: MdcContextMenuComponent = null;
    private readonly messageBoxComponent: MessageBoxComponent;

    constructor() {
        const idPrefix: string = 'account_user_';

        this.accountUserForm = new AccountUserForm(idPrefix);
        this.accountUserFormElement = document.querySelector('#' + idPrefix + 'form');
        this.accountUserFormSubmitButton = document.querySelector('#' + idPrefix + 'save_submit_button');

        if (MdcContextMenuComponent.checkContainerExists(idPrefix) === true) {
            this.contextMenu = new MdcContextMenuComponent(idPrefix);
        }

        this.messageBoxComponent = new MessageBoxComponent();

        if (this.accountUserFormElement != null) {
            this.accountUserFormSubmitButton.addEventListener('click', (): void => {
                if (this.accountUserForm.isFormValid() === false) {
                    this.messageBoxComponent.openMessageBox('Die Daten konnten nicht gespeichert werden. Bitte kontrollieren Sie die rot markierten Felder.', MessageBoxAlertType.DANGER);

                    return;
                }

                this.accountUserFormElement.submit();
            });
        }
    }

}

export { EditPage };
