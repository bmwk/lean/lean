import { MassOperationPage } from '../../MassOperationPage';
import { TextFieldComponent } from '../../../../Component/TextFieldComponent';
import { MessageBoxAlertType } from '../../../../Component/MessageBoxComponent/MessageBoxAlertType';

class ActivatePage extends MassOperationPage {

    private readonly verificationCodeTextField: TextFieldComponent;

    constructor() {
        const idPrefix: string = 'user_registration_mass_operation_activate_';

        super(
            document.querySelector('#' + idPrefix + 'form'),
            document.querySelector('#' + idPrefix + 'activate_submit_button')
        );

        this.verificationCodeTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'verificationCode_mdc_text_field'));

        this.verificationCodeTextField.mdcTextField.required = true;
    }

    protected doOnConfirmationFormSubmit(): void {
        if (this.verificationCodeTextField.mdcTextField.value === null || this.verificationCodeTextField.mdcTextField.value === '') {
            this.messageBoxComponent.openMessageBox('Die Registrierungen konnten nicht freigeschaltet werden. Bitte geben Sie den Bestätigungscode ein', MessageBoxAlertType.DANGER);

            return;
        }

        this.confirmationFormElement.submit();
    }
}

export { ActivatePage };
