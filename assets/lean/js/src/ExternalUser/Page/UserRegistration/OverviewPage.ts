import { MdcTabBarPage } from '../../../MdcTabBarPage';
import { OverviewPageTab } from './OverviewPageTab';
import { UserRegistrationFilterForm } from '../../Form/UserRegistration/UserRegistrationFilterForm';
import { UserRegistrationSearchForm } from '../../Form/UserRegistration/UserRegistrationSearchForm';
import { UserRegistrationListComponent } from '../../Component/UserRegistration/UserRegistrationListComponent';

abstract class OverviewPage extends MdcTabBarPage {

    protected readonly userRegistrationFilterForm: UserRegistrationFilterForm;
    protected readonly userRegistrationSearchForm: UserRegistrationSearchForm;
    protected readonly userRegistrationListComponent: UserRegistrationListComponent;

    protected constructor(OverviewPageTab: OverviewPageTab) {
        super(document.querySelector('#user_registration_type_mdc_tab_bar'))

        this.activateTab(OverviewPageTab);

        this.userRegistrationFilterForm = new UserRegistrationFilterForm('user_registration_filter_');
        this.userRegistrationSearchForm = new UserRegistrationSearchForm('user_registration_search_');

        if (UserRegistrationListComponent.checkContainerExists('user_registration_overview_') === true) {
            this.userRegistrationListComponent = new UserRegistrationListComponent('user_registration_overview_');
        }
    }

}

export { OverviewPage };
