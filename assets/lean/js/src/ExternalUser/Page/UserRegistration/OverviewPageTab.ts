enum OverviewPageTab {
    PropertySeeker = 'user_registration_property_seeker_tab',
    PropertyProvider = 'user_registration_property_provider_tab'
}

export { OverviewPageTab };
