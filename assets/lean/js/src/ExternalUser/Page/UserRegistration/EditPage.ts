import { UserRegistrationForm } from '../../Form/UserRegistration/UserRegistrationForm';
import { NaturalPersonUserRegistrationForm  } from '../../Form/UserRegistration/NaturalPersonUserRegistrationForm';
import { CompanyUserRegistrationForm  } from '../../Form/UserRegistration/CompanyUserRegistrationForm';
import { MdcContextMenuComponent } from '../../../Component/MdcContextMenuComponent';
import { MessageBoxComponent } from '../../../Component/MessageBoxComponent/MessageBoxComponent';
import { MessageBoxAlertType } from '../../../Component/MessageBoxComponent/MessageBoxAlertType';

class EditPage {

    private readonly userRegistrationForm: UserRegistrationForm;
    private readonly userRegistrationFormElement: HTMLFormElement;
    private readonly userRegistrationFormSubmitButton: HTMLButtonElement;
    private readonly contextMenu: MdcContextMenuComponent = null;
    private readonly messageBoxComponent: MessageBoxComponent;

    constructor() {
        const idPrefix: string = 'user_registration_';

        if (document.querySelector('#natural_person_' + idPrefix + 'form') !== null) {
            this.userRegistrationForm = new NaturalPersonUserRegistrationForm('natural_person_' + idPrefix);
            this.userRegistrationFormElement = document.querySelector('#natural_person_' + idPrefix + 'form');
            this.userRegistrationFormSubmitButton = document.querySelector('#natural_person_' + idPrefix + 'save_submit_button');
        }

        if (document.querySelector('#company_' + idPrefix + 'form') !== null) {
            this.userRegistrationForm = new CompanyUserRegistrationForm('company_' + idPrefix);
            this.userRegistrationFormElement = document.querySelector('#company_' + idPrefix + 'form');
            this.userRegistrationFormSubmitButton = document.querySelector('#company_' + idPrefix + 'save_submit_button');
        }

        if (MdcContextMenuComponent.checkContainerExists(idPrefix) == true) {
            this.contextMenu = new MdcContextMenuComponent(idPrefix);
        }

        this.messageBoxComponent = new MessageBoxComponent();

        this.userRegistrationForm.setRequiredFields(true);

        if (this.userRegistrationFormSubmitButton !== null) {
            this.userRegistrationFormSubmitButton.addEventListener('click', (): void => {
                if (this.userRegistrationForm.isFormValid() === false) {
                    this.messageBoxComponent.openMessageBox('Die Daten konnten nicht gespeichert werden. Bitte kontrollieren Sie die rot markierten Felder.', MessageBoxAlertType.DANGER);

                    return;
                }

                this.userRegistrationFormElement.submit();
            });
        }
    }

}

export { EditPage };
