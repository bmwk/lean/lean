import { UserRegistrationDeleteForm } from '../../Form/UserRegistration/UserRegistrationDeleteForm';
import { UserRegistrationForm } from '../../Form/UserRegistration/UserRegistrationForm';
import { NaturalPersonUserRegistrationForm } from '../../Form/UserRegistration/NaturalPersonUserRegistrationForm';
import { CompanyUserRegistrationForm } from '../../Form/UserRegistration/CompanyUserRegistrationForm';

class DeletePage {

    private readonly userRegistrationDeleteForm: UserRegistrationDeleteForm;
    private readonly userRegistrationDeleteFormElement: HTMLFormElement;
    private readonly userRegistrationDeleteFormSubmitButton: HTMLButtonElement;
    private readonly userRegistrationForm: UserRegistrationForm

    constructor() {
        const idPrefix: string = 'user_registration_delete_';

        this.userRegistrationDeleteForm = new UserRegistrationDeleteForm(idPrefix);
        this.userRegistrationDeleteFormElement = document.querySelector('#' + idPrefix + 'form');
        this.userRegistrationDeleteFormSubmitButton = document.querySelector('#' + idPrefix + 'delete_submit_button');

        if (document.querySelector('#natural_person_user_registration_form') !== null) {
            this.userRegistrationForm = new NaturalPersonUserRegistrationForm('natural_person_user_registration_');
        }

        if (document.querySelector('#company_user_registration_form') !== null) {
            this.userRegistrationForm = new CompanyUserRegistrationForm('company_user_registration_');
        }

        this.userRegistrationDeleteFormSubmitButton.addEventListener('click', (): void => {
            this.userRegistrationDeleteFormElement.submit();
        });
    }

}

export { DeletePage };
