import React from 'react';
import { HystreetLocationDetailApiResponse } from './HystreetApiResponse';

interface PedestrianFrequencyIndikatorProps {
    data: HystreetLocationDetailApiResponse;
    compareData: HystreetLocationDetailApiResponse;
    title: string;
}

const PedestrianFrequencyIndikator = (props: PedestrianFrequencyIndikatorProps) => {
    const difference = props.compareData.statistics.timerangeCount - props.data.statistics.timerangeCount
    const error = new Date(props.data.metadata.earliestMeasurementAt) > new Date(props.data.metadata.measuredFrom)
    return (
        <div className="py-3 border-top">
            <span className="d-block">{props.title}</span>
            {error === false ?
             <div className="d-flex">
                <span className="fs-5 text-primary d-block me-auto">{new Intl.NumberFormat('de-DE').format(props.data.statistics.timerangeCount)}</span>
                <span className="p-2">{(difference < 0 ? '-' : '+') + new Intl.NumberFormat('de-DE').format(Math.abs(props.compareData.statistics.timerangeCount - props.data.statistics.timerangeCount))}</span>
                <span
                    className={(difference < 0 ? 'text-danger bg-danger ' : 'text-success bg-success ') + 'bg-opacity-10 p-2 fw-bold rounded-2'}>{(difference < 0 ? '-' : '+') + Math.abs(Math.round(100 / props.data.statistics.timerangeCount * props.compareData.statistics.timerangeCount - 100))}%</span>
            </div>
            :
             <div className='text-center py-3'>
                 <span className="d-block mb-2 fs-5 text-primary">Daten für diesen Zeitraum unvollständig</span>
                 <small>Frühste Messung : {new Date(props.data.metadata.earliestMeasurementAt).toLocaleDateString()}</small>
             </div>}
        </div>
    );
};

export default PedestrianFrequencyIndikator;
