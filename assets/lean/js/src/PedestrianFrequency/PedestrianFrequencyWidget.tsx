import { MDCSelect } from '@material/select';
import axios from 'axios';
import React, { ChangeEvent, MutableRefObject, useEffect, useRef, useState } from 'react';
import { Bar, BarChart, Tooltip, XAxis, YAxis } from 'recharts';
import LoadingImage from '../../../images/lean-loading.gif';
import MdcContextMenuReactComponent from '../Component/MdcContextMenuReactComponent';
import { HystreetLocation, HystreetLocationDetailApiResponse } from './HystreetApiResponse';

function PedestrianFrequencyWidget(): JSX.Element {

    const getCurrentDate = (): string => {
        const today: Date = new Date();
        const year: number = today.getFullYear();
        const month: number = today.getMonth() + 1;
        const day: number = today.getDate();

        return `${year}-${month < 10 ? '0' + month : month}-${day < 10 ? '0' + day : day}`;
    };

    const [isLoading, setIsLoading] = useState<boolean>(true);
    const [locations, setLocations] = useState<HystreetLocation[]>([]);
    const [selectedLocation, setSelectedLocation] = useState<HystreetLocation>(null);
    const [selectedTime, setSelectedTime] = useState<string>(getCurrentDate());
    const [chartWidth, setChartWidth] = useState<number>(600);
    const [frequencyData, setFrequencyData] = useState<HystreetLocationDetailApiResponse>(null);

    const widgetContainer: MutableRefObject<HTMLDivElement> = useRef<HTMLDivElement>();

    const fetchLocationsFromApi = async (): Promise<HystreetLocation[]> => {
        const response = await axios.get('/api-gateway/hystreet/locations');
        return response.data as HystreetLocation[];
    };

    const locationFromApi = async (locationId: number, date: string): Promise<HystreetLocationDetailApiResponse> => {
        const response = await axios.get(`/api-gateway/hystreet/locations/${locationId}?from=${date}T00:00:00&to=${date}T23:59:59`);
        return response.data as HystreetLocationDetailApiResponse;
    };

    const formatTimestamp = (timestamp: string): string => {
        return new Date(timestamp).toLocaleTimeString('de-DE', {hour: '2-digit', minute: '2-digit'});
    };

    const handleDateChange = (event: ChangeEvent<HTMLInputElement>): void => {
        setSelectedTime(event.target.value);
    };

    useEffect((): void => {
        fetchLocationsFromApi().then(locations => {
            setLocations(locations);
            setIsLoading(false);
        });

        setChartWidth(widgetContainer.current.clientWidth - 30);

        window.addEventListener('resize', (event): void => {
            setChartWidth(widgetContainer.current.clientWidth - 30);
        });
    }, []);

    useEffect((): void => {
        if (selectedTime && selectedLocation) {
            setFrequencyData(null);
            setIsLoading(true);

            locationFromApi(selectedLocation.id, selectedTime).then(frequencyData => {
                setFrequencyData(frequencyData);
                setIsLoading(false);
            });
        }
    }, [selectedLocation, selectedTime]);

    useEffect(()=> {
        let locationMdcSelect = new MDCSelect(document.querySelector('#location-mdc-select'));
        locationMdcSelect.listen('MDCSelect:change', (): void => setSelectedLocation(locations.find((location: HystreetLocation) => location.id === Number(locationMdcSelect.value))));
        locationMdcSelect.setSelectedIndex(0, false)
    }, [locations])

    return (
        <>
            <div className="card-header d-flex mb-4">
                <div className="icon-thumbnail me-4 ms-0 align-text-top">
                    <span>
                        <i className="material-icons">
                            bar_chart
                        </i>
                    </span>
                </div>
                <div className="card-title align-self-center">
                    <h2 className="mb-1 fs-4">Passanten-Frequenz</h2>
                    <p className="mb-0">Passanten-Frequenzen von <a href="https://www.hystreet.com" target="_blank">hystreet.com</a></p>
                </div>
                <div className="ms-auto align-text-top">
                    <MdcContextMenuReactComponent idPrefix={'hystreet_widget_'} listItems={[{
                        label: 'zur Detailstatistik',
                        icon: 'query_stats',
                        url: '/reports-und-berichte/passantenfrequenzen'
                    }]}/>
                </div>
            </div>
            <div>
                <div className="row pt-2">
                    <div className="col-12 col-md-7">
                        <div id="location-mdc-select"
                             className={'mdc-select mdc-select--filled full-width-class ' + (locations.length == 0 ? 'mdc-select--disabled' : '')}
                             style={{width: '100%'}}>
                            <input type="hidden" name="street"/>
                            <div className="mdc-select__anchor"
                                 role="button"
                                 aria-haspopup="listbox"
                                 aria-expanded="false"
                                 aria-labelledby="location-mdc-select-label location-mdc-select-selected-text">
                                <span className="mdc-select__ripple"/>
                                <span id="location-mdc-select-label" className="mdc-floating-label">Standort</span>
                                <span className="mdc-select__selected-text-container">
                                    <span id="location-mdc-select-selected-text" className="mdc-select__selected-text"/>
                                </span>
                                <span className="mdc-select__dropdown-icon">
                                    <svg className="mdc-select__dropdown-icon-graphic" viewBox="7 10 10 5"
                                         focusable="false">
                                        <polygon className="mdc-select__dropdown-icon-inactive" stroke="none"
                                                 fillRule="evenodd" points="7 10 12 15 17 10"/>
                                        <polygon className="mdc-select__dropdown-icon-active" stroke="none"
                                                 fillRule="evenodd" points="7 15 12 10 17 15"/>
                                    </svg>
                                </span>
                                <span className="mdc-line-ripple"/>
                            </div>
                            <div className="mdc-select__menu mdc-menu mdc-menu-surface mdc-menu-surface--fullwidth">
                                <ul className="mdc-list" role="listbox" aria-label="location picker listbox">
                                    {locations.map((location) =>
                                        <li className="mdc-list-item" aria-selected="false" data-value={location.id}
                                            role="option" key={location.id}>
                                            <span className="mdc-list-item__ripple"/>
                                            <span className="mdc-list-item__text">{location.name}</span>
                                        </li>
                                    )}
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-md-5">
                        <input className="datepicker" type="date" name="time" value={selectedTime}
                               max={getCurrentDate()} onChange={handleDateChange}/>
                    </div>
                </div>
            </div>
            <div ref={widgetContainer} className="flex-grow-1 d-flex align-items-center justify-content-center"
                 style={{minHeight: '250px'}}>
                {frequencyData ?
                    <div className="position-relative mt-5">
                        <BarChart width={chartWidth} height={200} data={frequencyData.measurements}>
                            <XAxis dataKey="timestamp" tickFormatter={(label) => `${formatTimestamp(label)} Uhr`}/>
                            <YAxis/>
                            <Tooltip labelFormatter={(label) => `${formatTimestamp(label)} Uhr`}/>
                            <Bar dataKey="pedestriansCount" fill="#1d81a1" name="Frequenz"/>
                        </BarChart>
                        {frequencyData.metadata.weatherCondition &&
                        <div className="d-flex align-items-center position-absolute"
                             style={{top: '-30px', left: '80px'}}>
                            <small
                                className="material-symbols-outlined text-secondary me-2">{frequencyData.metadata.weatherCondition == 'rain' ? 'rainy' : frequencyData.metadata.weatherCondition.replace(/-/g, '_')}</small>
                            <small className="text-secondary">{frequencyData.metadata.minTemperature}°C
                                - {frequencyData.metadata.temperature}°C</small>
                        </div>}
                        <span className="fs-4 mt-3 text-primary">{'∑ ' + new Intl.NumberFormat('de-DE').format(frequencyData.statistics.timerangeCount)} Passant:innen</span>
                    </div>
                    : !isLoading ?
                        <div className="text-center">
                            Bitte wählen Sie den gewünschten Standort und den gewünschten Zeitraum aus, um die
                            Passanten-Frequenz anzuzeigen.
                        </div>
                        : <div className="align-items-center">
                            <img className="loading-image" src={LoadingImage} alt="Loading..."/>
                        </div>
                }
            </div>
        </>
    );
}

export default PedestrianFrequencyWidget;
