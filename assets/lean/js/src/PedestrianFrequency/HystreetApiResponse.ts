interface HystreetLocationDetailApiResponse {
    city: string;
    id: number;
    measurements: HystreetMeasurement[];
    metadata: {
        weatherCondition: string,
        temperature: number,
        minTemperature: number,
        earliestMeasurementAt: string,
        measuredFrom: string,
        measuredTo: string,
    };
    statistics: {
        timerangeCount: number;
    };
}

interface HystreetMeasurement {
    minTemperature: number;
    pedestriansCount: number;
    comparisonPedestriansCount?: number;
    temperature: number;
    timestamp: string;
    unverified: boolean;
    weatherCondition: string;
}

interface HystreetLocation {
    name: string;
    id: number;
    city: string;
    statistics: {
        todayCount: number
    };
}

export {HystreetLocationDetailApiResponse, HystreetMeasurement, HystreetLocation}