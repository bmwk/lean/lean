import React from 'react';
import { HystreetLocationDetailApiResponse } from './HystreetApiResponse';

interface PedestrianFrequencyIndikatorProps {
    data: HystreetLocationDetailApiResponse;
    title: string;
    description: string;
}

const PedestrianFrequencyIndikatorHeading = (props: PedestrianFrequencyIndikatorProps) => {
    const error = new Date(props.data.metadata.earliestMeasurementAt) > new Date(props.data.metadata.measuredFrom);
    if (error === false) {
        return (
            <div className="text-center mb-3">
                <span>{props.title}</span>
                <span className="fs-1 text-primary d-block">{new Intl.NumberFormat('de-DE').format(props.data?.statistics.timerangeCount)}</span>
                <span>{props.description}</span>
            </div>
        );
    } else {
        return (
            <div className="text-center py-3">
                <span>{props.title}</span>
                <span className="d-block mb-2 fs-5 text-primary">Daten für diesen Zeitraum unvollständig</span>
                <small>Frühste Messung : {new Date(props.data.metadata.earliestMeasurementAt).toLocaleDateString()}</small>
            </div>
        )
    }
};

export default PedestrianFrequencyIndikatorHeading;
