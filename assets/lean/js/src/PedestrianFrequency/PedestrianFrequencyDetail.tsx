import { MDCSelect } from '@material/select';
import axios from 'axios';
import React, { ChangeEvent, MutableRefObject, useEffect, useRef, useState } from 'react';
import { Bar, ComposedChart, Line, Tooltip, XAxis, YAxis } from 'recharts';
import LoadingImage from '../../../images/lean-loading.gif';
import { HystreetLocation, HystreetLocationDetailApiResponse, HystreetMeasurement } from './HystreetApiResponse';
import PedestrianFrequencyIndikator from './PedestrianFrequencyIndikator';
import PedestrianFrequencyIndikatorHeading from './PedestrianFrequencyIndikatorHeading';

function PedestrianFrequencyDetail(): JSX.Element {

    const formatDate = (date: Date): string => {
        return date.toISOString().split('T')[0];
    };

    const [isLoading, setIsLoading] = useState<boolean>(true);
    const [isLoadingDetails, setIsLoadingDetails] = useState<boolean>(true);
    const [locations, setLocations] = useState<HystreetLocation[]>([]);
    const [selectedLocation, setSelectedLocation] = useState<HystreetLocation>(null);
    const [selectedComparisonLocation, setSelectedComparisonLocation] = useState<HystreetLocation>(null);
    const [selectedFromTime, setSelectedFromTime] = useState<string>(formatDate(new Date()));
    const [selectedToTime, setSelectedToTime] = useState<string>(formatDate(new Date()));
    const [chartWidth, setChartWidth] = useState<number>(600);
    const [frequencyData, setFrequencyData] = useState<HystreetLocationDetailApiResponse>(null);
    const [frequencyDataYesterday, setFrequencyDataYesterday] = useState<HystreetLocationDetailApiResponse>(null);
    const [frequencyDataTwoDaysPrior, setFrequencyDataTwoDaysPrior] = useState<HystreetLocationDetailApiResponse>(null);
    const [frequencyDataThreeDaysPrior, setFrequencyDataThreeDaysPrior] = useState<HystreetLocationDetailApiResponse>(null);
    const [frequencyDataOneWeekPrior, setFrequencyDataOneWeekPrior] = useState<HystreetLocationDetailApiResponse>(null);
    const [frequencyDataLastWeek, setFrequencyDataLastWeek] = useState<HystreetLocationDetailApiResponse>(null);
    const [frequencyDataLastWeekOneYearPrior, setFrequencyDataLastWeekOneYearPrior] = useState<HystreetLocationDetailApiResponse>(null);
    const [frequencyDataLastWeekTwoYearsPrior, setFrequencyDataLastWeekTwoYearsPrior] = useState<HystreetLocationDetailApiResponse>(null);
    const [frequencyDataLastMonth, setFrequencyDataLastMonth] = useState<HystreetLocationDetailApiResponse>(null);
    const [frequencyDataOneMonthPrior, setFrequencyDataOneMonthPrior] = useState<HystreetLocationDetailApiResponse>(null);
    const [frequencyDataLastMonthOneYearPrior, setFrequencyDataLastMonthOneYearPrior] = useState<HystreetLocationDetailApiResponse>(null);
    const [frequencyDataLastMonthTwoYearsPrior, setFrequencyDataLastMonthTwoYearsPrior] = useState<HystreetLocationDetailApiResponse>(null);
    const [frequencyDataOneWeekPriorWeekly, setFrequencyDataOneWeekPriorWeekly] = useState<HystreetLocationDetailApiResponse>(null);
    const [comparisonFrequencyData, setComparisonFrequencyData] = useState<HystreetLocationDetailApiResponse>(null);
    const [measurements, setMeasurements] = useState<HystreetMeasurement[]>(null);

    const widgetContainer: MutableRefObject<HTMLDivElement> = useRef<HTMLDivElement>();

    const yesterday = new Date();
    yesterday.setDate(yesterday.getDate() - 1);
    const twoDaysPrior = new Date(yesterday);
    twoDaysPrior.setDate(twoDaysPrior.getDate() - 1);
    const threeDaysPrior = new Date(twoDaysPrior);
    threeDaysPrior.setDate(threeDaysPrior.getDate() - 1);
    const oneWeekPrior = new Date();
    oneWeekPrior.setDate(yesterday.getDate() - 7);
    const endOfLastWeek = new Date();
    endOfLastWeek.setDate(endOfLastWeek.getDate() - endOfLastWeek.getDay());
    const startOfLastWeek = new Date(endOfLastWeek);
    startOfLastWeek.setDate(endOfLastWeek.getDate() - 6);
    const endOfOneWeekPrior = new Date(endOfLastWeek);
    endOfOneWeekPrior.setDate(endOfOneWeekPrior.getDate() - 7);
    const startOfOneWeekPrior = new Date(startOfLastWeek);
    startOfOneWeekPrior.setDate(startOfOneWeekPrior.getDate() - 7);
    const endOfLastMonth = new Date();
    endOfLastMonth.setDate(0);
    const startOfLastMonth = new Date();
    startOfLastMonth.setDate(0);
    startOfLastMonth.setDate(1);
    const endOfMonthOneMonthPrior = new Date(endOfLastMonth);
    endOfMonthOneMonthPrior.setDate(0);
    const startOfMonthOneMonthPrior = new Date(startOfLastMonth);
    startOfMonthOneMonthPrior.setDate(0);
    startOfMonthOneMonthPrior.setDate(1);
    const endOfLastMonthOneYearPrior = new Date(endOfLastMonth);
    endOfLastMonthOneYearPrior.setFullYear(endOfLastMonth.getFullYear() - 1);
    const startOfLastMonthOneYearPrior = new Date(startOfLastMonth);
    startOfLastMonthOneYearPrior.setFullYear(startOfLastMonth.getFullYear() - 1);
    const endOfLastMonthTwoYearsPrior = new Date(endOfLastMonthOneYearPrior);
    endOfLastMonthTwoYearsPrior.setFullYear(endOfLastMonthOneYearPrior.getFullYear() - 1);
    const startOfLastMonthTwoYearsPrior = new Date(startOfLastMonthOneYearPrior);
    startOfLastMonthTwoYearsPrior.setFullYear(startOfLastMonthOneYearPrior.getFullYear() - 1);
    const endOfLastWeekOneYearPrior = new Date(endOfLastWeek);
    endOfLastWeekOneYearPrior.setFullYear(endOfLastWeek.getFullYear() - 1);
    endOfLastWeekOneYearPrior.setDate(endOfLastWeekOneYearPrior.getDate() - endOfLastWeekOneYearPrior.getDay() + 7);
    const startOfLastWeekOneYearPrior = new Date(endOfLastWeekOneYearPrior);
    startOfLastWeekOneYearPrior.setDate(endOfLastWeekOneYearPrior.getDate() - 6);
    const endOfLastWeekTwoYearsPrior = new Date(endOfLastWeekOneYearPrior);
    endOfLastWeekTwoYearsPrior.setFullYear(endOfLastWeekOneYearPrior.getFullYear() - 1);
    endOfLastWeekTwoYearsPrior.setDate(endOfLastWeekTwoYearsPrior.getDate() - endOfLastWeekTwoYearsPrior.getDay() + 7);
    const startOfLastWeekTwoYearsPrior = new Date(endOfLastWeekTwoYearsPrior);
    startOfLastWeekTwoYearsPrior.setDate(endOfLastWeekTwoYearsPrior.getDate() - 6);

    const fetchLocations = async (): Promise<HystreetLocation[]> => {
        const response = await axios.get('/api-gateway/hystreet/locations');
        return response.data as HystreetLocation[];
    };

    const fetchPedestrianFrequency = async (locationId: number, dateFrom: string, dateTo?: string): Promise<HystreetLocationDetailApiResponse> => {
        if (!dateTo) dateTo = dateFrom;
        const response = await axios.get(`/api-gateway/hystreet/locations/${locationId}?from=${dateFrom}T00:00:00&to=${dateTo}T23:59:59`);
        return response.data as HystreetLocationDetailApiResponse;
    };

    const formatTimestamp = (timestamp: string): string => {
        return new Date(timestamp).toLocaleTimeString('de-DE', {
            day: '2-digit',
            month: '2-digit',
            year: '2-digit',
            hour: '2-digit',
            minute: '2-digit'
        });
    };

    const handleFromDateChange = (event: ChangeEvent<HTMLInputElement>): void => {
        setSelectedFromTime(event.target.value);
    };

    const handleToDateChange = (event: ChangeEvent<HTMLInputElement>): void => {
        setSelectedToTime(event.target.value);
    };

    useEffect((): void => {

        fetchLocations().then(locations => {
            setLocations(locations);
            setIsLoading(false);
        });

        setChartWidth(widgetContainer.current.clientWidth - 30);

        window.addEventListener('resize', (event): void => {
            setChartWidth(widgetContainer.current.clientWidth - 30);
        });
    }, []);

    useEffect(() => {
        if (locations.length > 0) {
            let locationMdcSelect = new MDCSelect(document.querySelector('#location-mdc-select'));
            let comparisonLocationMdcSelect = new MDCSelect(document.querySelector('#comparison-location-mdc-select'));
            locationMdcSelect.listen('MDCSelect:change', () => setSelectedLocation(locations.find((location: HystreetLocation) => location.id === Number(locationMdcSelect.value))));
            comparisonLocationMdcSelect.listen('MDCSelect:change', () => setSelectedComparisonLocation(locations.find((location: HystreetLocation) => location.id === Number(comparisonLocationMdcSelect.value))));
            return () => {
                locationMdcSelect.unlisten('MDCSelect:change', () => setSelectedLocation(locations.find((location: HystreetLocation) => location.id === Number(locationMdcSelect.value))));
                comparisonLocationMdcSelect.unlisten('MDCSelect:change', () => setSelectedComparisonLocation(locations.find((location: HystreetLocation) => location.id === Number(comparisonLocationMdcSelect.value))));
            };
        }
    }, [locations]);

    useEffect((): void => {
        if (selectedFromTime && selectedLocation) {
            setIsLoading(true);
            setIsLoadingDetails(true);
            fetchPedestrianFrequency(selectedLocation.id, selectedFromTime, selectedToTime).then(frequencyData => {
                setFrequencyData(frequencyData);
                setIsLoading(false);
            });
            const updateDetails = async () => {
                const detailSectionFetches: Promise<any>[] = [];
                detailSectionFetches.push(fetchPedestrianFrequency(selectedLocation.id, formatDate(yesterday), formatDate(yesterday)).then(frequencyData => {
                    setFrequencyDataYesterday(frequencyData);
                }));
                detailSectionFetches.push(fetchPedestrianFrequency(selectedLocation.id, formatDate(twoDaysPrior), formatDate(twoDaysPrior)).then(frequencyData => {
                    setFrequencyDataTwoDaysPrior(frequencyData);
                }));
                detailSectionFetches.push(fetchPedestrianFrequency(selectedLocation.id, formatDate(threeDaysPrior), formatDate(threeDaysPrior)).then(frequencyData => {
                    setFrequencyDataThreeDaysPrior(frequencyData);
                }));
                detailSectionFetches.push(fetchPedestrianFrequency(selectedLocation.id, formatDate(oneWeekPrior), formatDate(oneWeekPrior)).then(frequencyData => {
                    setFrequencyDataOneWeekPrior(frequencyData);
                }));
                detailSectionFetches.push(fetchPedestrianFrequency(selectedLocation.id, formatDate(startOfLastWeek), formatDate(endOfLastWeek)).then(frequencyData => {
                    setFrequencyDataLastWeek(frequencyData);
                }));
                detailSectionFetches.push(fetchPedestrianFrequency(selectedLocation.id, formatDate(startOfOneWeekPrior), formatDate(endOfOneWeekPrior)).then(frequencyData => {
                    setFrequencyDataOneWeekPriorWeekly(frequencyData);
                }));
                detailSectionFetches.push(fetchPedestrianFrequency(selectedLocation.id, formatDate(startOfLastWeekOneYearPrior), formatDate(endOfLastWeekOneYearPrior)).then(frequencyData => {
                    setFrequencyDataLastWeekOneYearPrior(frequencyData);
                }));
                detailSectionFetches.push(fetchPedestrianFrequency(selectedLocation.id, formatDate(startOfLastWeekTwoYearsPrior), formatDate(endOfLastWeekTwoYearsPrior)).then(frequencyData => {
                    setFrequencyDataLastWeekTwoYearsPrior(frequencyData);
                }));
                detailSectionFetches.push(fetchPedestrianFrequency(selectedLocation.id, formatDate(startOfLastMonth), formatDate(endOfLastMonth)).then(frequencyData => {
                    setFrequencyDataLastMonth(frequencyData);
                }));
                detailSectionFetches.push(fetchPedestrianFrequency(selectedLocation.id, formatDate(startOfMonthOneMonthPrior), formatDate(endOfMonthOneMonthPrior)).then(frequencyData => {
                    setFrequencyDataOneMonthPrior(frequencyData);
                }));
                detailSectionFetches.push(fetchPedestrianFrequency(selectedLocation.id, formatDate(startOfLastMonthOneYearPrior), formatDate(endOfLastMonthOneYearPrior)).then(frequencyData => {
                    setFrequencyDataLastMonthOneYearPrior(frequencyData);
                }));
                detailSectionFetches.push(fetchPedestrianFrequency(selectedLocation.id, formatDate(startOfLastMonthTwoYearsPrior), formatDate(endOfLastMonthTwoYearsPrior)).then(frequencyData => {
                    setFrequencyDataLastMonthTwoYearsPrior(frequencyData);
                }));
                await Promise.all(detailSectionFetches);
                setIsLoadingDetails(false);
            };
            updateDetails();
        }
    }, [selectedLocation]);

    useEffect((): void => {
        if (selectedFromTime && selectedComparisonLocation) {
            setIsLoading(true);
            fetchPedestrianFrequency(selectedComparisonLocation.id, selectedFromTime, selectedToTime)
            .then(comparisonFrequencyData => {
                setComparisonFrequencyData(comparisonFrequencyData);
                setIsLoading(false);
            });
        }
    }, [selectedComparisonLocation]);

    useEffect((): void => {
        async function update() {
            if (selectedLocation || selectedComparisonLocation) {
                setIsLoading(true);
                if (selectedFromTime && selectedLocation) {
                    const data = await fetchPedestrianFrequency(selectedLocation.id, selectedFromTime, selectedToTime);
                    setFrequencyData(data);
                }
                if (selectedFromTime && selectedComparisonLocation) {
                    const data = await fetchPedestrianFrequency(selectedComparisonLocation.id, selectedFromTime, selectedToTime);
                    setComparisonFrequencyData(data);
                }
                setIsLoading(false);
            }
        }

        update();

    }, [selectedFromTime, selectedToTime]);

    useEffect((): void => {
        if (!frequencyData) return;
        const measurements: HystreetMeasurement[] = frequencyData.measurements;
        if (comparisonFrequencyData) {
            for (let i = 0; i < measurements.length; i++) {
                measurements[i].comparisonPedestriansCount = comparisonFrequencyData.measurements[i]?.pedestriansCount;
            }
        }
        setMeasurements(measurements);
    }, [frequencyData, comparisonFrequencyData]);

    return (
        <div className="container-lg my-4">
            <div className="row g-3 mb-3">
                <div className="col-12">
                    <div className="mdc-card p-3 h-100 d-flex flex-column">
                        <div className="d-flex align-items-center mb-3">
                            <h2 className="fs-4">Passanten-Frequenz</h2>
                            <a href="https://www.hystreet.com" className="text-primary ms-auto" target="_blank">
                                <svg viewBox="0 0 169 45" height="36">
                                    <g fillRule="evenodd">
                                        <path fill="currentColor"
                                              d="M14.552 31V13.036h3.72v6.612a6.049 6.049 0 011.458-.93c.524-.236 1.15-.354 1.878-.354.68 0 1.282.118 1.806.354s.966.564 1.326.984c.36.42.632.92.816 1.5.184.58.276 1.21.276 1.89V31h-3.72v-7.908c0-.608-.14-1.082-.42-1.422-.28-.34-.692-.51-1.236-.51-.408 0-.792.088-1.152.264a4.48 4.48 0 00-1.032.708V31h-3.72zm19.008 3.228c-.112.248-.248.43-.408.546-.16.116-.416.174-.768.174H29.6l2.4-5.04-4.968-11.352h3.288c.288 0 .512.064.672.192.16.128.272.28.336.456l2.04 5.364c.192.496.348.992.468 1.488.08-.256.168-.508.264-.756s.188-.5.276-.756l1.848-5.34a.91.91 0 01.378-.462 1.08 1.08 0 01.606-.186h3L33.56 34.228zM49.352 21.28a.962.962 0 01-.306.324.803.803 0 01-.414.096c-.176 0-.352-.038-.528-.114l-.57-.246a6.21 6.21 0 00-.69-.246 3.08 3.08 0 00-.876-.114c-.472 0-.834.09-1.086.27a.87.87 0 00-.378.75c0 .232.082.424.246.576.164.152.382.286.654.402.272.116.58.226.924.33.344.104.696.22 1.056.348.36.128.712.278 1.056.45.344.172.652.384.924.636s.49.558.654.918c.164.36.246.792.246 1.296 0 .616-.112 1.184-.336 1.704-.224.52-.558.968-1.002 1.344-.444.376-.992.668-1.644.876-.652.208-1.402.312-2.25.312-.424 0-.848-.04-1.272-.12a8.81 8.81 0 01-1.23-.324 6.7 6.7 0 01-1.098-.486 4.762 4.762 0 01-.864-.606l.864-1.368c.104-.16.228-.286.372-.378.144-.092.332-.138.564-.138.216 0 .41.048.582.144l.558.312c.2.112.434.216.702.312.268.096.606.144 1.014.144.288 0 .532-.03.732-.09.2-.06.36-.142.48-.246A.982.982 0 0046.7 28a.978.978 0 00.084-.396.808.808 0 00-.252-.612 2.195 2.195 0 00-.66-.414 8.046 8.046 0 00-.93-.324c-.348-.1-.702-.214-1.062-.342a8.53 8.53 0 01-1.062-.462 3.653 3.653 0 01-.93-.684 3.178 3.178 0 01-.66-1.014c-.168-.4-.252-.884-.252-1.452 0-.528.102-1.03.306-1.506.204-.476.51-.894.918-1.254.408-.36.92-.646 1.536-.858.616-.212 1.336-.318 2.16-.318.448 0 .882.04 1.302.12.42.08.814.192 1.182.336.368.144.704.314 1.008.51.304.196.572.41.804.642l-.84 1.308zm7.32 9.912c-.592 0-1.114-.086-1.566-.258a3.11 3.11 0 01-1.14-.732 3.12 3.12 0 01-.702-1.146 4.436 4.436 0 01-.24-1.5v-6.432h-1.056a.704.704 0 01-.492-.186c-.136-.124-.204-.306-.204-.546V18.94l1.98-.384.732-3.036c.096-.384.368-.576.816-.576h1.944v3.636h3.024v2.544h-3.024v6.18c0 .288.07.526.21.714.14.188.342.282.606.282a1.19 1.19 0 00.582-.138c.068-.036.132-.068.192-.096a.492.492 0 01.21-.042c.112 0 .202.026.27.078a.966.966 0 01.21.246l1.128 1.764c-.48.36-1.02.63-1.62.81-.6.18-1.22.27-1.86.27zM61.904 31V18.556h2.208c.184 0 .338.016.462.048a.81.81 0 01.312.15.64.64 0 01.192.27c.044.112.082.248.114.408l.204 1.164c.456-.704.968-1.26 1.536-1.668a3.12 3.12 0 011.86-.612c.568 0 1.024.136 1.368.408l-.48 2.736c-.032.168-.096.286-.192.354a.652.652 0 01-.384.102c-.136 0-.296-.018-.48-.054a3.72 3.72 0 00-.696-.054c-.976 0-1.744.52-2.304 1.56V31h-3.72zm15.24-12.636c.824 0 1.578.128 2.262.384a4.917 4.917 0 011.764 1.116 5.094 5.094 0 011.152 1.794c.276.708.414 1.51.414 2.406 0 .28-.012.508-.036.684a1.083 1.083 0 01-.132.42.502.502 0 01-.258.216c-.108.04-.246.06-.414.06H74.72c.12 1.04.436 1.794.948 2.262.512.468 1.172.702 1.98.702.432 0 .804-.052 1.116-.156.312-.104.59-.22.834-.348.244-.128.468-.244.672-.348.204-.104.418-.156.642-.156.296 0 .52.108.672.324l1.08 1.332a5.53 5.53 0 01-1.242 1.074c-.444.276-.9.492-1.368.648a7.415 7.415 0 01-1.404.324c-.468.06-.914.09-1.338.09-.872 0-1.69-.142-2.454-.426a5.639 5.639 0 01-2.004-1.266c-.572-.56-1.024-1.256-1.356-2.088-.332-.832-.498-1.8-.498-2.904 0-.832.142-1.62.426-2.364a5.822 5.822 0 011.224-1.956 5.828 5.828 0 011.932-1.332c.756-.328 1.61-.492 2.562-.492zm.072 2.568c-.712 0-1.268.202-1.668.606-.4.404-.664.986-.792 1.746H79.4c0-.296-.038-.584-.114-.864-.076-.28-.2-.53-.372-.75a1.893 1.893 0 00-.678-.534c-.28-.136-.62-.204-1.02-.204zM90.2 18.364c.824 0 1.578.128 2.262.384a4.917 4.917 0 011.764 1.116 5.094 5.094 0 011.152 1.794c.276.708.414 1.51.414 2.406 0 .28-.012.508-.036.684a1.083 1.083 0 01-.132.42.502.502 0 01-.258.216c-.108.04-.246.06-.414.06h-7.176c.12 1.04.436 1.794.948 2.262.512.468 1.172.702 1.98.702.432 0 .804-.052 1.116-.156.312-.104.59-.22.834-.348.244-.128.468-.244.672-.348.204-.104.418-.156.642-.156.296 0 .52.108.672.324l1.08 1.332a5.53 5.53 0 01-1.242 1.074c-.444.276-.9.492-1.368.648a7.415 7.415 0 01-1.404.324c-.468.06-.914.09-1.338.09-.872 0-1.69-.142-2.454-.426A5.639 5.639 0 0185.91 29.5c-.572-.56-1.024-1.256-1.356-2.088-.332-.832-.498-1.8-.498-2.904 0-.832.142-1.62.426-2.364a5.822 5.822 0 011.224-1.956 5.828 5.828 0 011.932-1.332c.756-.328 1.61-.492 2.562-.492zm.072 2.568c-.712 0-1.268.202-1.668.606-.4.404-.664.986-.792 1.746h4.644c0-.296-.038-.584-.114-.864-.076-.28-.2-.53-.372-.75a1.893 1.893 0 00-.678-.534c-.28-.136-.62-.204-1.02-.204zm12.024 10.26c-.592 0-1.114-.086-1.566-.258a3.11 3.11 0 01-1.14-.732 3.12 3.12 0 01-.702-1.146 4.436 4.436 0 01-.24-1.5v-6.432h-1.056a.704.704 0 01-.492-.186c-.136-.124-.204-.306-.204-.546V18.94l1.98-.384.732-3.036c.096-.384.368-.576.816-.576h1.944v3.636h3.024v2.544h-3.024v6.18c0 .288.07.526.21.714.14.188.342.282.606.282a1.19 1.19 0 00.582-.138c.068-.036.132-.068.192-.096a.492.492 0 01.21-.042c.112 0 .202.026.27.078a.966.966 0 01.21.246l1.128 1.764c-.48.36-1.02.63-1.62.81-.6.18-1.22.27-1.86.27zm4.512-2.076c0-.288.054-.556.162-.804.108-.248.256-.464.444-.648a2.118 2.118 0 011.506-.6c.296 0 .572.054.828.162.256.108.48.254.672.438a1.986 1.986 0 01.612 1.452c0 .288-.054.558-.162.81a1.97 1.97 0 01-.45.654 2.107 2.107 0 01-.672.432 2.179 2.179 0 01-.828.156c-.304 0-.584-.052-.84-.156a2.037 2.037 0 01-.666-.432 2.034 2.034 0 01-.606-1.464zm15.36-7.608c-.112.136-.22.244-.324.324-.104.08-.252.12-.444.12a.956.956 0 01-.498-.132 103.03 103.03 0 00-.498-.294 3.777 3.777 0 00-.648-.294c-.248-.088-.556-.132-.924-.132-.456 0-.85.084-1.182.252a2.189 2.189 0 00-.822.72 3.29 3.29 0 00-.48 1.146 6.846 6.846 0 00-.156 1.53c0 1.192.23 2.108.69 2.748.46.64 1.094.96 1.902.96.432 0 .774-.054 1.026-.162a3.11 3.11 0 00.642-.36l.486-.366a.898.898 0 01.558-.168c.296 0 .52.108.672.324l1.08 1.332a5.876 5.876 0 01-2.514 1.722 6.72 6.72 0 01-1.338.324c-.448.06-.884.09-1.308.09-.76 0-1.486-.144-2.178-.432a5.336 5.336 0 01-1.824-1.254c-.524-.548-.94-1.222-1.248-2.022-.308-.8-.462-1.712-.462-2.736 0-.896.134-1.734.402-2.514a5.708 5.708 0 011.188-2.028 5.524 5.524 0 011.944-1.35c.772-.328 1.666-.492 2.682-.492.976 0 1.832.156 2.568.468.736.312 1.4.768 1.992 1.368l-.984 1.308zm8.232-3.144c.944 0 1.804.148 2.58.444.776.296 1.442.72 1.998 1.272a5.73 5.73 0 011.296 2.01c.308.788.462 1.674.462 2.658 0 .992-.154 1.886-.462 2.682a5.77 5.77 0 01-1.296 2.028 5.686 5.686 0 01-1.998 1.284c-.776.3-1.636.45-2.58.45-.952 0-1.818-.15-2.598-.45a5.798 5.798 0 01-2.016-1.284 5.686 5.686 0 01-1.308-2.028c-.308-.796-.462-1.69-.462-2.682 0-.984.154-1.87.462-2.658a5.647 5.647 0 011.308-2.01 5.77 5.77 0 012.016-1.272c.78-.296 1.646-.444 2.598-.444zm0 10.068c.856 0 1.486-.302 1.89-.906.404-.604.606-1.522.606-2.754s-.202-2.148-.606-2.748c-.404-.6-1.034-.9-1.89-.9-.88 0-1.524.3-1.932.9-.408.6-.612 1.516-.612 2.748 0 1.232.204 2.15.612 2.754.408.604 1.052.906 1.932.906zM138.776 31V18.556h2.304c.232 0 .426.052.582.156.156.104.266.26.33.468l.204.672c.208-.216.424-.414.648-.594a4.04 4.04 0 011.566-.78c.3-.076.63-.114.99-.114.752 0 1.374.194 1.866.582.492.388.866.906 1.122 1.554.208-.384.462-.712.762-.984s.624-.492.972-.66a4.722 4.722 0 011.104-.372c.388-.08.774-.12 1.158-.12.712 0 1.344.106 1.896.318a3.61 3.61 0 011.386.924c.372.404.654.9.846 1.488.192.588.288 1.254.288 1.998V31h-3.72v-7.908c0-1.288-.552-1.932-1.656-1.932-.504 0-.922.166-1.254.498-.332.332-.498.81-.498 1.434V31h-3.72v-7.908c0-.712-.14-1.212-.42-1.5-.28-.288-.692-.432-1.236-.432-.336 0-.654.076-.954.228-.3.152-.582.356-.846.612v9h-3.72z"/>
                                        <path fill="none" stroke="currentColor" strokeWidth="2" d="M1 1h167v43H1z"/>
                                    </g>
                                </svg>
                            </a>
                        </div>
                        <div>
                            <div className="row g-3">
                                <div className="col-12 col-md-6 col-xl-4">
                                    <div id="location-mdc-select"
                                         className={'mdc-select mdc-select--filled full-width-class ' + (locations.length == 0 ? 'mdc-select--disabled' : '')}
                                         style={{width: '100%'}}>
                                        <input type="hidden" name="street"/>
                                        <div className="mdc-select__anchor"
                                             role="button"
                                             aria-haspopup="listbox"
                                             aria-expanded="false"
                                             aria-labelledby="location-mdc-select-label location-mdc-select-selected-text">
                                            <span className="mdc-select__ripple"/>
                                            <span id="location-mdc-select-label" className="mdc-floating-label">1. Standort</span>
                                            <span className="mdc-select__selected-text-container">
                                    <span id="location-mdc-select-selected-text" className="mdc-select__selected-text"/>
                                </span>
                                            <span className="mdc-select__dropdown-icon">
                                    <svg className="mdc-select__dropdown-icon-graphic" viewBox="7 10 10 5"
                                         focusable="false">
                                        <polygon className="mdc-select__dropdown-icon-inactive" stroke="none" fillRule="evenodd" points="7 10 12 15 17 10"/>
                                        <polygon className="mdc-select__dropdown-icon-active" stroke="none" fillRule="evenodd" points="7 15 12 10 17 15"/>
                                    </svg>
                                </span>
                                            <span className="mdc-line-ripple"/>
                                        </div>
                                        <div className="mdc-select__menu mdc-menu mdc-menu-surface mdc-menu-surface--fullwidth">
                                            <ul className="mdc-list" role="listbox" aria-label="location picker listbox">
                                                {locations.map((location) =>
                                                    <li className="mdc-list-item" aria-selected="false" data-value={location.id}
                                                        role="option" key={location.id}>
                                                        <span className="mdc-list-item__ripple"/>
                                                        <span className="mdc-list-item__text">{location.name}</span>
                                                    </li>
                                                )}
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-md-6 col-xl-4">
                                    <div id="comparison-location-mdc-select"
                                         className={'mdc-select mdc-select--filled full-width-class ' + (locations.length == 0 ? 'mdc-select--disabled' : '')}
                                         style={{width: '100%'}}>
                                        <input type="hidden" name="street"/>
                                        <div className="mdc-select__anchor"
                                             role="button"
                                             aria-haspopup="listbox"
                                             aria-expanded="false"
                                             aria-labelledby="comparison-location-mdc-select-label location-mdc-select-selected-text">
                                            <span className="mdc-select__ripple"/>
                                            <span id="comparison-location-mdc-select-label" className="mdc-floating-label">2. Standort (optional)</span>
                                            <span className="mdc-select__selected-text-container">
                                <span id="comparison-location-mdc-select-selected-text" className="mdc-select__selected-text"/>
                            </span>
                                            <span className="mdc-select__dropdown-icon">
                                <svg className="mdc-select__dropdown-icon-graphic" viewBox="7 10 10 5" focusable="false">
                                    <polygon className="mdc-select__dropdown-icon-inactive" stroke="none" fillRule="evenodd" points="7 10 12 15 17 10"/>
                                    <polygon className="mdc-select__dropdown-icon-active" stroke="none" fillRule="evenodd" points="7 15 12 10 17 15"/>
                                </svg>
                            </span>
                                            <span className="mdc-line-ripple"/>
                                        </div>
                                        <div className="mdc-select__menu mdc-menu mdc-menu-surface mdc-menu-surface--fullwidth">
                                            <ul className="mdc-list" role="listbox" aria-label="comparison-location picker listbox">
                                                {locations.map((location) =>
                                                    <li className="mdc-list-item" aria-selected="false" data-value={location.id} role="option"
                                                        key={location.id}>
                                                        <span className="mdc-list-item__ripple"/>
                                                        <span className="mdc-list-item__text">{location.name}</span>
                                                    </li>
                                                )}
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-6 col-xl-2">
                                    <label htmlFor="timeFrom" className="me-2">Von:</label>
                                    <input className="datepicker" type="date" name="timeFrom" value={selectedFromTime} max={formatDate(new Date())}
                                           onChange={handleFromDateChange} disabled={isLoading}/>
                                </div>
                                <div className="col-6 col-xl-2">
                                    <label htmlFor="timeTo" className="me-2">Bis:</label>
                                    <input className="datepicker" type="date" name="timeTo" value={selectedToTime}
                                           min={selectedFromTime} max={formatDate(new Date())} onChange={handleToDateChange}
                                           disabled={isLoading}/>
                                </div>
                            </div>

                        </div>
                        <div ref={widgetContainer} className="flex-grow-1 d-flex align-items-center justify-content-center" style={{minHeight: '250px'}}>
                            {!isLoading && frequencyData ?
                             <div className="position-relative mt-5">
                                 <ComposedChart width={chartWidth} height={400} data={measurements}>
                                     <XAxis dataKey="timestamp" tickFormatter={(label) => `${formatTimestamp(label)} Uhr`}/>
                                     <YAxis/>
                                     <Tooltip labelFormatter={(label) => `${formatTimestamp(label)} Uhr`}/>
                                     <Bar dataKey="pedestriansCount" fill="#1d81a1" name={selectedLocation.name + ' Frequenz'}/>
                                     {selectedComparisonLocation &&
                                     <Line dataKey="comparisonPedestriansCount" dot={false} stroke={'#F7951A'} strokeWidth={3}
                                           name={selectedComparisonLocation.name + ' Frequenz'}/>
                                     }
                                 </ComposedChart>
                                 {frequencyData.metadata.weatherCondition &&
                                 <div className="d-flex align-items-center position-absolute" style={{top: '-30px', left: '80px'}}>
                                     <small
                                         className="material-symbols-outlined text-secondary me-2">{frequencyData.metadata.weatherCondition == 'rain' ? 'rainy' : frequencyData.metadata.weatherCondition.replace(/-/g, '_')}</small>
                                     <small className="text-secondary">{frequencyData.metadata.minTemperature}°C
                                         - {frequencyData.metadata.temperature}°C</small>
                                 </div>
                                 }
                                 <span
                                     className="fs-4 mt-3 text-primary">{'∑ ' + new Intl.NumberFormat('de-DE').format(frequencyData.statistics.timerangeCount)} Passant:innen</span>
                             </div>
                                                         : !isLoading ?
                                                           <div className="text-center">
                                                               Bitte wählen Sie den gewünschten Standort und den gewünschten Zeitraum aus, um die
                                                               Passanten-Frequenz anzuzeigen.
                                                           </div>
                                                                      : <div className="d-flex align-items-center">
                                                               <img src={LoadingImage} alt=""/>
                                                           </div>
                            }
                        </div>
                    </div>
                </div>
            </div>
            {isLoadingDetails === false ?
             <div className="row g-3">
                 <div className="col-12 col-xl-4">
                     <div className="mdc-card p-3">
                         <PedestrianFrequencyIndikatorHeading
                             data={frequencyDataYesterday}
                             title={'Gestern'}
                             description={yesterday.toLocaleDateString('de-DE', {
                                 weekday: 'long',
                                 day: '2-digit',
                                 month: '2-digit',
                                 year: 'numeric',
                             })}/>
                         <PedestrianFrequencyIndikator title={'Selber Wochentag in der Vorwoche'}
                                                       data={frequencyDataOneWeekPrior}
                                                       compareData={frequencyDataYesterday}/>
                         <PedestrianFrequencyIndikator
                             title={twoDaysPrior.toLocaleDateString('de-DE', {
                                 weekday: 'long',
                                 day: '2-digit',
                                 month: '2-digit',
                                 year: 'numeric',
                             })}
                             data={frequencyDataTwoDaysPrior}
                             compareData={frequencyDataYesterday}/>
                         <PedestrianFrequencyIndikator
                             title={threeDaysPrior.toLocaleDateString('de-DE', {
                                 weekday: 'long',
                                 day: '2-digit',
                                 month: '2-digit',
                                 year: 'numeric',
                             })}
                             data={frequencyDataThreeDaysPrior}
                             compareData={frequencyDataYesterday}/>
                     </div>
                 </div>
                 <div className="col-12 col-xl-4">
                     <div className="mdc-card p-3">
                         <PedestrianFrequencyIndikatorHeading
                             data={frequencyDataLastWeek}
                             title={'Letzte Woche'}
                             description={startOfLastWeek.toLocaleDateString('de-DE', {
                                 weekday: 'long',
                                 day: '2-digit',
                                 month: '2-digit',
                                 year: 'numeric',
                             }) + ' bis ' +
                             endOfLastWeek.toLocaleDateString('de-DE', {
                                 weekday: 'long',
                                 day: '2-digit',
                                 month: '2-digit',
                                 year: 'numeric',
                             })}/>
                         <PedestrianFrequencyIndikator title={'Vorwoche'}
                                                       data={frequencyDataOneWeekPriorWeekly}
                                                       compareData={frequencyDataLastWeek}/>
                         <PedestrianFrequencyIndikator
                             title={'Selbe KW im Jahr ' + endOfLastWeekOneYearPrior.getFullYear()}
                             data={frequencyDataLastWeekOneYearPrior}
                             compareData={frequencyDataLastWeek}/>
                         <PedestrianFrequencyIndikator
                             title={'Selbe KW im Jahr ' + endOfLastWeekTwoYearsPrior.getFullYear()}
                             data={frequencyDataLastWeekTwoYearsPrior}
                             compareData={frequencyDataLastWeek}/>
                     </div>
                 </div>
                 <div className="col-12 col-xl-4">
                     <div className="mdc-card p-3">
                         <PedestrianFrequencyIndikatorHeading
                             data={frequencyDataLastMonth}
                             title={'Letzter Monat'}
                             description={startOfLastMonth.toLocaleDateString('de-DE', {
                                 weekday: 'long',
                                 day: '2-digit',
                                 month: '2-digit',
                                 year: 'numeric',
                             }) + ' bis ' +
                             endOfLastMonth.toLocaleDateString('de-DE', {
                                 weekday: 'long',
                                 day: '2-digit',
                                 month: '2-digit',
                                 year: 'numeric',
                             })}/>
                         <PedestrianFrequencyIndikator title={'Vormonat'}
                                                       data={frequencyDataOneMonthPrior}
                                                       compareData={frequencyDataLastMonth}/>
                         <PedestrianFrequencyIndikator
                             title={'Selber Monat im Jahr ' + endOfLastWeekOneYearPrior.getFullYear()}
                             data={frequencyDataLastMonthOneYearPrior}
                             compareData={frequencyDataLastMonth}/>
                         <PedestrianFrequencyIndikator
                             title={'Selber Monat im Jahr ' + endOfLastWeekTwoYearsPrior.getFullYear()}
                             data={frequencyDataLastMonthTwoYearsPrior}
                             compareData={frequencyDataLastMonth}/>
                     </div>
                 </div>
             </div>
                                        : selectedLocation &&
                 <div className="col-12 text-center">
                     <div className="p-5 d-flex align-items-center justify-content-center mdc-card">
                         <img src={LoadingImage} alt="lean-logo-lädt"/>
                     </div>
                 </div>
            }
        </div>
    )
        ;
}

export default PedestrianFrequencyDetail;
