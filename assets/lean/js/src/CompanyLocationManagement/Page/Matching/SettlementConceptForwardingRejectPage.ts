import { SettlementConceptForwardingRejectForm } from '../../Form/Matching/SettlementConceptForwardingRejectForm';

class SettlementConceptForwardingRejectPage {

    private readonly settlementConceptForwardingRejectForm: SettlementConceptForwardingRejectForm;
    private readonly settlementConceptForwardingRejectFormElement: HTMLFormElement;
    private readonly settlementConceptForwardingRejectFormSubmitButton: HTMLButtonElement;

    constructor() {
        const idPrefix: string = 'settlement_concept_expose_forwarding_reject_';

        this.settlementConceptForwardingRejectForm = new SettlementConceptForwardingRejectForm(idPrefix);
        this.settlementConceptForwardingRejectFormElement = document.querySelector('#' + idPrefix + 'form');
        this.settlementConceptForwardingRejectFormSubmitButton = document.querySelector('#' + idPrefix + 'save_submit_button');

        this.settlementConceptForwardingRejectFormSubmitButton.addEventListener('click', (): void => {
            this.settlementConceptForwardingRejectFormElement.submit();
        });
    }

}

export { SettlementConceptForwardingRejectPage };
