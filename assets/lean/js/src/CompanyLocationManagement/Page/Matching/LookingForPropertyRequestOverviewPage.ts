import { MatchingPage } from './MatchingPage';
import { LookingForPropertyRequestSearchForm } from '../../Form/LookingForPropertyRequest/LookingForPropertyRequestSearchForm';
import { LookingForPropertyRequestFilterForm } from '../../Form/LookingForPropertyRequest/LookingForPropertyRequestFilterForm';
import { LookingForPropertyRequestListComponent } from '../../Component/Matching/LookingForPropertyRequestListComponent';

class LookingForPropertyRequestOverviewPage extends MatchingPage {

    private readonly lookingForPropertyRequestSearchForm: LookingForPropertyRequestSearchForm;
    private readonly lookingForPropertyRequestFilterForm: LookingForPropertyRequestFilterForm;
    private readonly lookingForPropertyRequestListComponent: LookingForPropertyRequestListComponent;

    constructor() {
        super();

        super.activateTab('looking_for_property_request_overview_tab');

        this.lookingForPropertyRequestSearchForm = new LookingForPropertyRequestSearchForm('looking_for_property_request_search_');
        this.lookingForPropertyRequestFilterForm = new LookingForPropertyRequestFilterForm('looking_for_property_request_filter_');

        this.lookingForPropertyRequestListComponent = new LookingForPropertyRequestListComponent('looking_for_property_request_');
    }

}

export { LookingForPropertyRequestOverviewPage };
