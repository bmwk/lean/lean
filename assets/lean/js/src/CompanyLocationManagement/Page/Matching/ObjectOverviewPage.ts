import { MatchingPage } from './MatchingPage';
import { LazyImageLoader } from '../../../LazyImageLoader';
import { ObjectCardComponent } from '../../Component/Matching/ObjectCardComponent';
import { MatchingSearchForm } from '../../Form/Matching/MatchingSearchForm';
import { MatchingFilterForm } from '../../Form/Matching/MatchingFilterForm';
import { PaginationComponent } from '../../../Component/PaginationComponent';

class ObjectOverviewPage extends MatchingPage {

    private readonly lazyImageLoader: LazyImageLoader;
    private readonly objectCardComponents: ObjectCardComponent[] = [];
    private readonly matchingFilterForm: MatchingFilterForm;
    private readonly matchingSearchForm: MatchingSearchForm;
    private readonly paginationComponent: PaginationComponent;

    constructor() {
        super();

        super.activateTab('object_overview_tab');

        this.matchingSearchForm = new MatchingSearchForm('matching_search_');
        this.matchingFilterForm = new MatchingFilterForm('matching_filter_');
        this.lazyImageLoader = new LazyImageLoader();

        if (PaginationComponent.checkContainerExists('matching_') === true) {
            this.paginationComponent = new PaginationComponent('matching_');
        }

        document.querySelectorAll('div.object-card-component[data-building-unit-id]').forEach((value: HTMLDivElement): void => {
            this.objectCardComponents.push(new ObjectCardComponent(value));
        });
    }
}

export { ObjectOverviewPage };
