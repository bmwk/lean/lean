import { ObjectDetailCardComponent } from '../../Component/Matching/ObjectDetailCardComponent';
import { LookingForPropertyRequestMatchingListComponent } from '../../Component/Matching/LookingForPropertyRequestMatchingListComponent';
import { SettlementConceptMatchingListComponent } from '../../Component/Matching/SettlementConceptMatchingListComponent';
import { TabBarComponent } from '../../../Component/TabBarComponent';

class ObjectDetailPage {

    private readonly objectDetailCardComponent: ObjectDetailCardComponent;
    private readonly lookingForPropertyRequestMatchingListComponent: LookingForPropertyRequestMatchingListComponent;
    private readonly settlementConceptMatchingListComponent: SettlementConceptMatchingListComponent;
    private readonly resultsTabBar: TabBarComponent;

    constructor() {
        this.objectDetailCardComponent = new ObjectDetailCardComponent(document.querySelector('div.object-detail-card-component[data-building-unit-id]'));
        this.lookingForPropertyRequestMatchingListComponent = new LookingForPropertyRequestMatchingListComponent('looking_for_property_request_matching_');
        this.settlementConceptMatchingListComponent = new SettlementConceptMatchingListComponent('settlement_concept_matching_');

        this.resultsTabBar = new TabBarComponent(document.querySelector('#matching_result_tab_bar'));

        this.resultsTabBar.findTabById('looking_for_property_request_tab').listen('click', () => {
            this.selectLookingForPropertyRequest();
        });

        this.resultsTabBar.findTabById('settlement_concept_tab').listen('click', () => {
            this.selectSettlementConcept();
        });
    }

    private selectLookingForPropertyRequest(): void {
        this.resultsTabBar.activateTab('looking_for_property_request_tab');

        this.lookingForPropertyRequestMatchingListComponent.show();
        this.settlementConceptMatchingListComponent.hide();
    }

    private selectSettlementConcept(): void {
        this.resultsTabBar.activateTab('settlement_concept_tab');

        this.settlementConceptMatchingListComponent.show();
        this.lookingForPropertyRequestMatchingListComponent.hide();
    }

}

export { ObjectDetailPage };
