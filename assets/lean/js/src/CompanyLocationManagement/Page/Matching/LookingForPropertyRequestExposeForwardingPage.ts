import { ObjectDetailCardComponent } from '../../Component/Matching/ObjectDetailCardComponent';

class LookingForPropertyRequestExposeForwardingPage {

    private readonly objectDetailCardComponent: ObjectDetailCardComponent;
    private readonly lookingForPropertyRequestExposeForwardingFormElement: HTMLFormElement;
    private readonly lookingForPropertyRequestExposeForwardingFormSubmitButton: HTMLButtonElement;

    constructor() {
        const idPrefix: string = 'looking_for_property_request_expose_forwarding_';

        this.objectDetailCardComponent = new ObjectDetailCardComponent(document.querySelector('div.object-detail-card-component[data-building-unit-id]'));

        this.lookingForPropertyRequestExposeForwardingFormElement = document.querySelector('#' + idPrefix + 'form');
        this.lookingForPropertyRequestExposeForwardingFormSubmitButton = document.querySelector('#' + idPrefix + 'submit_submit_button');

        this.lookingForPropertyRequestExposeForwardingFormSubmitButton.addEventListener('click', (): void => {
            this.lookingForPropertyRequestExposeForwardingFormElement.submit();
        });
    }

}

export { LookingForPropertyRequestExposeForwardingPage };
