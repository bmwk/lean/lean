import { ObjectDetailCardComponent } from '../../Component/Matching/ObjectDetailCardComponent';

class SettlementConceptExposeForwardingPage {

    private readonly objectDetailCardComponent: ObjectDetailCardComponent;
    private readonly settlementConceptExposeForwardingFormElement: HTMLFormElement;
    private readonly settlementConceptExposeForwardingFormSubmitButton: HTMLButtonElement;

    constructor() {
        const idPrefix: string = 'settlement_concept_expose_forwarding_';

        this.objectDetailCardComponent = new ObjectDetailCardComponent(document.querySelector('div.object-detail-card-component[data-building-unit-id]'));

        this.settlementConceptExposeForwardingFormElement = document.querySelector('#' + idPrefix + 'form');
        this.settlementConceptExposeForwardingFormSubmitButton = document.querySelector('#' + idPrefix + 'submit_submit_button');

        this.settlementConceptExposeForwardingFormSubmitButton.addEventListener('click', (): void => {
            this.settlementConceptExposeForwardingFormElement.submit();
        });
    }

}

export { SettlementConceptExposeForwardingPage };
