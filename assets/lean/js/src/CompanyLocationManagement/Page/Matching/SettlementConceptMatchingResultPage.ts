import { ObjectDetailCardComponent } from '../../Component/Matching/ObjectDetailCardComponent';

class SettlementConceptMatchingResultPage {

    private readonly objectDetailCardComponent: ObjectDetailCardComponent;

    constructor() {
        this.objectDetailCardComponent = new ObjectDetailCardComponent(document.querySelector('div.object-detail-card-component[data-building-unit-id]'));
    }

}

export { SettlementConceptMatchingResultPage };
