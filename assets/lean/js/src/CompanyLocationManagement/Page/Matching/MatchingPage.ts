import { TabBarComponent } from '../../../Component/TabBarComponent';
import { MDCTab } from '@material/tab';

abstract class MatchingPage {

    private readonly matchingTabBar: TabBarComponent;

    protected constructor() {
        const matchingTabBarContainer: HTMLDivElement = document.querySelector('#matching_tab_bar');

        if (matchingTabBarContainer !== null) {
            this.matchingTabBar = new TabBarComponent(matchingTabBarContainer);

            this.matchingTabBar.getTabs().forEach((mdcTab: MDCTab): void => {
                if ((<HTMLButtonElement>mdcTab.root).dataset.url !== undefined) {
                    const tabButton: HTMLButtonElement = <HTMLButtonElement>mdcTab.root;

                    tabButton.addEventListener('click', (): void => {
                        window.location.href = tabButton.dataset.url;
                    });
                }
            });
        }
    }

    protected activateTab(tabId: string): void {
        if (this.matchingTabBar !== null) {
            const matchingTabBarContainer: HTMLDivElement = document.querySelector('#matching_tab_bar');

            if (matchingTabBarContainer !== null) {
                this.matchingTabBar.activateTab(tabId);
            }
        }
    }

}

export { MatchingPage };
