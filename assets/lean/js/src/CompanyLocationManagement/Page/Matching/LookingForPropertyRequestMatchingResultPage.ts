import { MdcContextMenuComponent } from '../../../Component/MdcContextMenuComponent';
import { ObjectMatchingListComponent } from '../../Component/Matching/ObjectMatchingListComponent';
import {LookingForPropertyRequestMatchingListComponent} from "../../Component/Matching/LookingForPropertyRequestMatchingListComponent";

class LookingForPropertyRequestMatchingResultPage {

    private readonly contextMenu: MdcContextMenuComponent;
    private readonly objectMatchingListComponent: ObjectMatchingListComponent;

    constructor() {
        this.contextMenu = new MdcContextMenuComponent('looking_for_property_request_');
        this.objectMatchingListComponent = new ObjectMatchingListComponent('object_matching_');
    }

}

export { LookingForPropertyRequestMatchingResultPage };
