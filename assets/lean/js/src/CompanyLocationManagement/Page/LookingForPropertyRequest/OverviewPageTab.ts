enum OverviewPageTab {
    OverviewActive = 'looking_for_property_request_overview_active_tab',
    OverviewInactive = 'looking_for_property_request_overview_inactive_tab',
    OverviewArchive = 'looking_for_property_request_overview_archive_tab',
}

export { OverviewPageTab };
