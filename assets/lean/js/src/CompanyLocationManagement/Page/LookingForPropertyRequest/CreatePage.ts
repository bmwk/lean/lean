import { LookingForPropertyRequestForm } from '../../Form/LookingForPropertyRequest/LookingForPropertyRequestForm';
import { MessageBoxComponent } from '../../../Component/MessageBoxComponent/MessageBoxComponent';
import {MessageBoxAlertType} from "../../../Component/MessageBoxComponent/MessageBoxAlertType";

class CreatePage {

    private readonly lookingForPropertyRequestForm: LookingForPropertyRequestForm;
    private readonly lookingForPropertyRequestFormElement: HTMLFormElement;
    private readonly lookingForPropertyRequestFormSubmitButton: HTMLButtonElement;
    private readonly messageBoxComponent: MessageBoxComponent;

    constructor() {
        const idPrefix: string = 'looking_for_property_request_';

        this.lookingForPropertyRequestForm = new LookingForPropertyRequestForm(idPrefix);
        this.lookingForPropertyRequestFormElement = document.querySelector('#' + idPrefix + 'form');
        this.lookingForPropertyRequestFormSubmitButton = document.querySelector('#' + idPrefix + 'save_submit_button');
        this.messageBoxComponent = new MessageBoxComponent();

        this.lookingForPropertyRequestFormSubmitButton.addEventListener('click', (): void => {
            if (this.lookingForPropertyRequestForm.isFormValid() === false) {
                this.messageBoxComponent.openMessageBox('Die Daten konnten nicht gespeichert werden. Bitte kontrollieren Sie die rot markierten Felder.', MessageBoxAlertType.DANGER);
                return;
            }

            this.lookingForPropertyRequestFormElement.submit();
        });
    }

}

export { CreatePage };
