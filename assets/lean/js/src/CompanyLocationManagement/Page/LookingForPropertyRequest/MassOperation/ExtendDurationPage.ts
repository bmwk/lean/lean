import { MassOperationPage } from '../MassOperationPage';

class ExtendDurationPage extends MassOperationPage {

    constructor() {
        const idPrefix: string = 'looking_for_property_request_mass_operation_extend_duration_';

        super(
            document.querySelector('#' + idPrefix + 'form'),
            document.querySelector('#' + idPrefix + 'extend_submit_button')
        );
    }

    protected doOnConfirmationFormSubmit(): void {
        this.confirmationFormElement.submit();
    }

}

export { ExtendDurationPage };
