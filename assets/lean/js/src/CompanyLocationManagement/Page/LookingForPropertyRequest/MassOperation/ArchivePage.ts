import { MassOperationPage } from '../MassOperationPage';

class ArchivePage extends MassOperationPage {

    constructor() {
        const idPrefix: string = 'looking_for_property_request_mass_operation_archive_';

        super(
            document.querySelector('#' + idPrefix + 'form'),
            document.querySelector('#' + idPrefix + 'archive_submit_button')
        );
    }

    protected doOnConfirmationFormSubmit(): void {
        this.confirmationFormElement.submit();
    }

}

export { ArchivePage };
