import { MassOperationPage } from '../MassOperationPage';

class UnarchivePage extends MassOperationPage {

    constructor() {
        const idPrefix: string = 'looking_for_property_request_mass_operation_unarchive_';

        super(
            document.querySelector('#' + idPrefix + 'form'),
            document.querySelector('#' + idPrefix + 'unarchive_submit_button')
        );
    }

    protected doOnConfirmationFormSubmit(): void {
        this.confirmationFormElement.submit();
    }

}

export { UnarchivePage };
