import { MassOperationPage } from '../MassOperationPage';
import { SelectComponent } from '../../../../Component/SelectComponent';

class AssignToAccountUserPage extends MassOperationPage {

    private readonly assignedToAccountUserSelect: SelectComponent;

    constructor() {
        const idPrefix: string = 'looking_for_property_request_mass_operation_assign_to_account_user_';

        super(
            document.querySelector('#' + idPrefix + 'form'),
            document.querySelector('#' + idPrefix + 'save_submit_button')
        );

        this.assignedToAccountUserSelect = new SelectComponent(document.querySelector('#' + idPrefix + 'assignedToAccountUser_mdc_select'));

        this.assignedToAccountUserSelect.mdcSelect.required = false;

        this.addClearIcon(this.assignedToAccountUserSelect);
    }

    protected doOnConfirmationFormSubmit(): void {
        this.confirmationFormElement.submit();
    }

    private addClearIcon(select: SelectComponent): void {
        const clearIcon: HTMLElement = document.createElement('i');

        clearIcon.classList.add('material-icons');
        clearIcon.classList.add('mdc-text-field__icon');
        clearIcon.classList.add('mdc-text-field__icon--leading');
        clearIcon.classList.add('clear-icon');
        clearIcon.classList.add('small');
        clearIcon.tabIndex = 0;
        clearIcon.innerText = 'clear';
        clearIcon.id = 'clear_' + select.mdcSelect.root.id;

        clearIcon.addEventListener('click', (event): void => {
            event.stopPropagation();

            select.mdcSelect.value = null;
        });

        select.mdcSelect.root.querySelector('.mdc-select__anchor').insertBefore(clearIcon, select.mdcSelect.root.querySelector('.mdc-select__dropdown-icon'));
    }

}

export { AssignToAccountUserPage };
