import { LookingForPropertyRequestForm } from '../../Form/LookingForPropertyRequest/LookingForPropertyRequestForm';
import { LookingForPropertyRequestAssignToAccountUserForm } from '../../Form/LookingForPropertyRequest/LookingForPropertyRequestAssignToAccountUserForm';
import { MessageBoxComponent } from '../../../Component/MessageBoxComponent/MessageBoxComponent';
import { MessageBoxAlertType } from '../../../Component/MessageBoxComponent/MessageBoxAlertType';

class AssignToAccountUserPage {

    private readonly lookingForPropertyRequestForm: LookingForPropertyRequestForm;
    private readonly lookingForPropertyRequestAssignToAccountUserForm: LookingForPropertyRequestAssignToAccountUserForm;
    private readonly lookingForPropertyRequestAssignToAccountUserFormElement: HTMLFormElement;
    private readonly lookingForPropertyRequestAssignToAccountUserFormSubmitButton: HTMLButtonElement;
    private readonly messageBoxComponent: MessageBoxComponent;

    constructor() {
        const idPrefix: string = 'looking_for_property_request_assign_to_account_user_';

        this.lookingForPropertyRequestForm = new LookingForPropertyRequestForm('looking_for_property_request_');
        this.lookingForPropertyRequestAssignToAccountUserForm = new LookingForPropertyRequestAssignToAccountUserForm(idPrefix);
        this.lookingForPropertyRequestAssignToAccountUserFormElement = document.querySelector('#' + idPrefix + 'form');
        this.lookingForPropertyRequestAssignToAccountUserFormSubmitButton = document.querySelector('#' + idPrefix + 'save_submit_button');
        this.messageBoxComponent = new MessageBoxComponent();

        this.lookingForPropertyRequestAssignToAccountUserFormSubmitButton.addEventListener('click', (): void => {
            if (this.lookingForPropertyRequestAssignToAccountUserForm.isFormValid() === false) {
                this.messageBoxComponent.openMessageBox('Die Daten konnten nicht gespeichert werden. Bitte kontrollieren Sie die rot markierten Felder.', MessageBoxAlertType.DANGER);

                return;
            }

            this.lookingForPropertyRequestAssignToAccountUserFormElement.submit();
        });
    }

}

export { AssignToAccountUserPage };
