import { LookingForPropertyRequestForm } from '../../Form/LookingForPropertyRequest/LookingForPropertyRequestForm';
import { LookingForPropertyRequestDeleteForm } from '../../Form/LookingForPropertyRequest/LookingForPropertyRequestDeleteForm';
import { MessageBoxAlertType } from '../../../Component/MessageBoxComponent/MessageBoxAlertType';
import { MessageBoxComponent } from '../../../Component/MessageBoxComponent/MessageBoxComponent';

class DeletePage {

    private readonly lookingForPropertyRequestForm: LookingForPropertyRequestForm;
    private readonly lookingForPropertyRequestDeleteForm: LookingForPropertyRequestDeleteForm;
    private readonly lookingForPropertyRequestDeleteFormElement: HTMLFormElement;
    private readonly lookingForPropertyRequestDeleteFormSubmitButton: HTMLButtonElement;
    private readonly messageBoxComponent: MessageBoxComponent;

    constructor() {
        const idPrefix: string = 'looking_for_property_request_delete_';

        this.lookingForPropertyRequestForm = new LookingForPropertyRequestForm('looking_for_property_request_');
        this.lookingForPropertyRequestDeleteForm = new LookingForPropertyRequestDeleteForm(idPrefix);
        this.lookingForPropertyRequestDeleteFormElement = document.querySelector('#' + idPrefix + 'form');
        this.lookingForPropertyRequestDeleteFormSubmitButton = document.querySelector('#' + idPrefix + 'delete_submit_button');
        this.messageBoxComponent = new MessageBoxComponent();

        this.lookingForPropertyRequestDeleteFormSubmitButton.addEventListener('click', (event: MouseEvent): void => {
            event.preventDefault();

            if (this.lookingForPropertyRequestDeleteForm.isFormValid() === false) {
                this.messageBoxComponent.openMessageBox('Das Gesuch konnte nicht gelöscht werden. Bitte geben Sie den Bestätigungscode ein.', MessageBoxAlertType.DANGER);
                return;
            }

            this.lookingForPropertyRequestDeleteFormElement.submit();
        });
    }

}

export { DeletePage };
