import { LookingForPropertyRequestPage } from '../LookingForPropertyRequestPage';
import { ExposeForwardingListComponent } from '../../../Component/LookingForPropertyRequest/ExposeForwardingListComponent';

class OverviewPage extends LookingForPropertyRequestPage {

    protected readonly exposeForwardingListComponent: ExposeForwardingListComponent;

    constructor() {
        super();

        const idPrefix: string = 'looking_for_property_request_expose_forwarding_';

        super.activateTab(idPrefix + 'tab');

        if (ExposeForwardingListComponent.checkContainerExists('expose_forwarding_')) {
            this.exposeForwardingListComponent = new ExposeForwardingListComponent('expose_forwarding_');
        }


    }

}

export { OverviewPage };
