import { ForwardingRejectForm } from '../../../Form/LookingForPropertyRequest/ExposeForwarding/ForwardingRejectForm';

class ForwardingRejectPage {

    private readonly forwardingRejectForm: ForwardingRejectForm;
    private readonly forwardingRejectFormElement: HTMLFormElement;
    private readonly forwardingRejectFormSubmitButton: HTMLButtonElement;

    constructor() {
        const idPrefix: string = 'expose_forwarding_reject_';

        this.forwardingRejectForm = new ForwardingRejectForm(idPrefix);
        this.forwardingRejectFormElement = document.querySelector('#' + idPrefix + 'form');
        this.forwardingRejectFormSubmitButton = document.querySelector('#' + idPrefix + 'save_submit_button');

        this.forwardingRejectFormSubmitButton.addEventListener('click', (): void => {
            this.forwardingRejectFormElement.submit();
        });
    }

}

export { ForwardingRejectPage };
