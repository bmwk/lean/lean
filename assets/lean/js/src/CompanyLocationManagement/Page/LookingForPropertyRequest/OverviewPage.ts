import { OverviewPageTab } from './OverviewPageTab';
import { MdcTabBarPage } from '../../../MdcTabBarPage';
import { LookingForPropertyRequestSearchForm } from '../../Form/LookingForPropertyRequest/LookingForPropertyRequestSearchForm';
import { LookingForPropertyRequestFilterForm } from '../../Form/LookingForPropertyRequest/LookingForPropertyRequestFilterForm';
import { LookingForPropertyRequestListComponent } from '../../Component/LookingForPropertyRequest/LookingForPropertyRequestListComponent';

abstract class OverviewPage extends MdcTabBarPage {

    protected readonly lookingForPropertyRequestSearchForm: LookingForPropertyRequestSearchForm;
    protected readonly lookingForPropertyRequestFilterForm: LookingForPropertyRequestFilterForm;
    protected readonly lookingForPropertyRequestListComponent: LookingForPropertyRequestListComponent;

    protected constructor(overviewPageTab: OverviewPageTab) {
        super(document.querySelector('#looking_for_property_request_overview_tab_bar'));

        this.activateTab(overviewPageTab);

        if (document.querySelector('#looking_for_property_request_search') !== null) {
            this.lookingForPropertyRequestSearchForm = new LookingForPropertyRequestSearchForm('looking_for_property_request_search_');
        }

        if (document.querySelector('#looking_for_property_request_filter') !== null) {
            this.lookingForPropertyRequestFilterForm = new LookingForPropertyRequestFilterForm('looking_for_property_request_filter_');
        }

        if (LookingForPropertyRequestListComponent.checkContainerExists('looking_for_property_request_')) {
            this.lookingForPropertyRequestListComponent = new LookingForPropertyRequestListComponent('looking_for_property_request_');
        }
    }

}

export { OverviewPage };
