import { OverviewPage } from './OverviewPage';
import { OverviewPageTab } from './OverviewPageTab';

class OverviewActivePage extends OverviewPage {

    constructor() {
        super(OverviewPageTab.OverviewActive);
    }

}

export { OverviewActivePage };
