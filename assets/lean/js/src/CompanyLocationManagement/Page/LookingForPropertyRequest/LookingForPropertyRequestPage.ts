import { TabBarComponent } from '../../../Component/TabBarComponent';
import { MDCTab } from '@material/tab';

abstract class LookingForPropertyRequestPage {

    private readonly lookingForPropertyRequestTabBar: TabBarComponent;

    protected constructor() {
        const lookingForPropertyRequestTabBarContainer: HTMLDivElement = document.querySelector('#looking_for_property_request_tab_bar');

        if (lookingForPropertyRequestTabBarContainer !== null) {
            this.lookingForPropertyRequestTabBar = new TabBarComponent(lookingForPropertyRequestTabBarContainer);

            this.lookingForPropertyRequestTabBar.getTabs().forEach((mdcTab: MDCTab): void => {
                if ((<HTMLButtonElement>mdcTab.root).dataset.url !== undefined) {
                    const tabButton: HTMLButtonElement = <HTMLButtonElement>mdcTab.root;

                    tabButton.addEventListener('click', (): void => {
                        window.location.href = tabButton.dataset.url;
                    });
                }
            });
        }
    }

    protected activateTab(tabId: string): void {
        if (this.lookingForPropertyRequestTabBar !== null) {
            const lookingForPropertyRequestTabBarContainer: HTMLDivElement = document.querySelector('#looking_for_property_request_tab_bar');

            if (lookingForPropertyRequestTabBarContainer !== null) {
                this.lookingForPropertyRequestTabBar.activateTab(tabId);
            }
        }
    }

}

export { LookingForPropertyRequestPage };
