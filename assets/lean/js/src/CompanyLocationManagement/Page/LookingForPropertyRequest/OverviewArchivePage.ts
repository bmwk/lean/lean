import { OverviewPage } from './OverviewPage';
import { OverviewPageTab } from './OverviewPageTab';

class OverviewArchivePage extends OverviewPage {

    constructor() {
        super(OverviewPageTab.OverviewArchive);
    }

}

export { OverviewArchivePage };
