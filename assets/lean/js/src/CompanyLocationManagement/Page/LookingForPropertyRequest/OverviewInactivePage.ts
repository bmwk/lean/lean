import { OverviewPage } from './OverviewPage';
import { OverviewPageTab } from './OverviewPageTab';

class OverviewInactivePage extends OverviewPage {

    constructor() {
        super(OverviewPageTab.OverviewInactive);
    }

}

export { OverviewInactivePage };
