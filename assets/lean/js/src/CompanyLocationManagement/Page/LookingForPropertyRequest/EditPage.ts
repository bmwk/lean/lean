import { LookingForPropertyRequestPage } from './LookingForPropertyRequestPage';
import { MdcContextMenuComponent } from '../../../Component/MdcContextMenuComponent';
import { LookingForPropertyRequestForm } from '../../Form/LookingForPropertyRequest/LookingForPropertyRequestForm';
import { MessageBoxComponent } from '../../../Component/MessageBoxComponent/MessageBoxComponent';
import { MessageBoxAlertType } from '../../../Component/MessageBoxComponent/MessageBoxAlertType';

class EditPage extends LookingForPropertyRequestPage {

    private readonly lookingForPropertyRequestForm: LookingForPropertyRequestForm;
    private readonly lookingForPropertyRequestFormElement: HTMLFormElement;
    private readonly lookingForPropertyRequestFormSubmitButton: HTMLButtonElement;
    private readonly lookingForPropertyRequestContextMenu: MdcContextMenuComponent = null;
    private readonly messageBoxComponent: MessageBoxComponent;

    constructor() {
        super();

        const idPrefix: string = 'looking_for_property_request_';

        super.activateTab(idPrefix + 'edit_tab');

        this.lookingForPropertyRequestForm = new LookingForPropertyRequestForm(idPrefix);
        this.lookingForPropertyRequestFormElement = document.querySelector('#' + idPrefix + 'form');
        this.lookingForPropertyRequestFormSubmitButton = document.querySelector('#' + idPrefix + 'save_submit_button');

        if (MdcContextMenuComponent.checkContainerExists(idPrefix) === true) {
            this.lookingForPropertyRequestContextMenu = new MdcContextMenuComponent(idPrefix);
        }

        this.messageBoxComponent = new MessageBoxComponent();

        if (this.lookingForPropertyRequestFormElement !== null) {
            this.lookingForPropertyRequestFormSubmitButton.addEventListener('click', (): void => {
                if (this.lookingForPropertyRequestForm.isFormValid() === false) {
                    this.messageBoxComponent.openMessageBox('Die Daten konnten nicht gespeichert werden. Bitte kontrollieren Sie die rot markierten Felder.', MessageBoxAlertType.DANGER);

                    return;
                }

                this.lookingForPropertyRequestFormElement.submit();
            });
        }
    }

}

export { EditPage };
