import {  LookingForPropertyRequestReportDeleteForm } from '../../Form/LookingForPropertyRequestReport/LookingForPropertyRequestReportDeleteForm';

class DeletePage {

    private readonly lookingForPropertyRequestReportDeleteForm: LookingForPropertyRequestReportDeleteForm;
    private readonly lookingForPropertyRequestReportDeleteFormElement: HTMLFormElement;
    private readonly lookingForPropertyRequestReportDeleteFormSubmitButton: HTMLButtonElement;

    constructor() {
        const idPrefix: string = 'report_delete_';

        this.lookingForPropertyRequestReportDeleteForm = new LookingForPropertyRequestReportDeleteForm(idPrefix);
        this.lookingForPropertyRequestReportDeleteFormElement = document.querySelector('#' + idPrefix + 'form');
        this.lookingForPropertyRequestReportDeleteFormSubmitButton = document.querySelector('#' + idPrefix + 'submit_button');

        this.lookingForPropertyRequestReportDeleteFormSubmitButton.addEventListener('click', (): void => {
            this.lookingForPropertyRequestReportDeleteFormElement.submit();
        });
    }

}

export { DeletePage };
