import { MdcContextMenuComponent } from '../../../Component/MdcContextMenuComponent';

class ReportPage {

    private readonly contextMenu: MdcContextMenuComponent = null;

    constructor() {
        if (MdcContextMenuComponent.checkContainerExists('looking_for_property_request_report_') === true) {
            this.contextMenu = new MdcContextMenuComponent('looking_for_property_request_report_');
        }
    }

}

export { ReportPage };
