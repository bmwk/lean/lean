import { MdcTabBarPage } from '../../../MdcTabBarPage';
import { OverviewPageTab } from './OverviewPageTab';

abstract class OverviewPage extends MdcTabBarPage {

    protected constructor(overviewPageTab: OverviewPageTab) {
        super(document.querySelector('#looking_for_property_request_report_overview_tab_bar'));

        this.activateTab(overviewPageTab);
    }

}

export { OverviewPage };
