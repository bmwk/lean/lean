import { LookingForPropertyRequestReportNoteForm } from '../../Form/LookingForPropertyRequestReport/LookingForPropertyRequestReportNoteForm';
import { MdcContextMenuComponent } from '../../../Component/MdcContextMenuComponent';

class NoteEditPage {

    private readonly lookingForPropertyRequestReportNoteForm: LookingForPropertyRequestReportNoteForm;
    private readonly lookingForPropertyRequestReportNoteFormElement: HTMLFormElement;
    private readonly lookingForPropertyRequestReportFormSubmitButton: HTMLButtonElement;
    private readonly contextMenu: MdcContextMenuComponent = null;

    constructor() {
        const idPrefix: string = 'report_note_';

        this.lookingForPropertyRequestReportNoteForm = new LookingForPropertyRequestReportNoteForm(idPrefix);
        this.lookingForPropertyRequestReportNoteFormElement = document.querySelector('#' + idPrefix + 'form');
        this.lookingForPropertyRequestReportFormSubmitButton = document.querySelector('#' + idPrefix + 'save_submit_button');

        if (MdcContextMenuComponent.checkContainerExists(idPrefix) === true) {
            this.contextMenu = new MdcContextMenuComponent(idPrefix);
        }

        if (this.lookingForPropertyRequestReportNoteFormElement !== null) {
            this.lookingForPropertyRequestReportFormSubmitButton.addEventListener('click', (): void => {
                this.lookingForPropertyRequestReportNoteFormElement.submit();
            });
        }
    }

}

export { NoteEditPage };
