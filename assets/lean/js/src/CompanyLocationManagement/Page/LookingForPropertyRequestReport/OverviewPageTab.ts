enum OverviewPageTab {
    UnprocessedOverview = 'looking_for_property_request_report_unprocessed_overview_tab',
    ProcessedOverview = 'looking_for_property_request_report_processed_overview_tab',
}

export { OverviewPageTab };
