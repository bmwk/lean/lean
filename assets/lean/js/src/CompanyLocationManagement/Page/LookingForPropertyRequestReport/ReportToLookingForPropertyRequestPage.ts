import { ReportToLookingForPropertyRequestForm } from '../../Form/LookingForPropertyRequestReport/ReportToLookingForPropertyRequestForm';
import { MessageBoxComponent } from '../../../Component/MessageBoxComponent/MessageBoxComponent';
import { MessageBoxAlertType } from '../../../Component/MessageBoxComponent/MessageBoxAlertType';

class ReportToLookingForPropertyRequestPage {

    private readonly reportToLookingForPropertyRequestForm: ReportToLookingForPropertyRequestForm;
    private readonly reportToLookingForPropertyRequestFormElement: HTMLFormElement;
    private readonly reportToLookingForPropertyRequestFormSubmitButton: HTMLButtonElement;
    private readonly messageBoxComponent: MessageBoxComponent;

    constructor() {
        const idPrefix: string = 'report_to_looking_for_property_request_';

        this.reportToLookingForPropertyRequestForm = new ReportToLookingForPropertyRequestForm(idPrefix);
        this.reportToLookingForPropertyRequestFormElement = document.querySelector('#' + idPrefix + 'form');
        this.reportToLookingForPropertyRequestFormSubmitButton = document.querySelector('#' + idPrefix + 'save_submit_button');

        this.reportToLookingForPropertyRequestFormSubmitButton.addEventListener('click', (): void => {
            if (this.reportToLookingForPropertyRequestForm.isFormValid() === false) {
                this.messageBoxComponent.openMessageBox('Die Daten konnten nicht gespeichert werden. Bitte kontrollieren Sie die rot markierten Felder.', MessageBoxAlertType.DANGER);
                return;
            }

            this.reportToLookingForPropertyRequestFormElement.submit();
        });
    }

}

export { ReportToLookingForPropertyRequestPage };
