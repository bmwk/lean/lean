import { LookingForPropertyRequestReportNoteForm } from '../../Form/LookingForPropertyRequestReport/LookingForPropertyRequestReportNoteForm';
import { LookingForPropertyRequestReportNoteDeleteForm } from '../../Form/LookingForPropertyRequestReport/LookingForPropertyRequestReportNoteDeleteForm';

class NoteDeletePage {

    private readonly lookingForPropertyRequestReportNoteForm: LookingForPropertyRequestReportNoteForm;
    private readonly lookingForPropertyRequestReportNoteDeleteForm: LookingForPropertyRequestReportNoteDeleteForm;
    private readonly lookingForPropertyRequestReportNoteDeleteFormElement: HTMLFormElement;
    private readonly lookingForPropertyRequestReportNoteDeleteFormSubmitButton: HTMLButtonElement;

    constructor() {
        const idPrefix: string = 'report_note_delete_';

        this.lookingForPropertyRequestReportNoteForm = new LookingForPropertyRequestReportNoteForm('report_note_');

        this.lookingForPropertyRequestReportNoteDeleteForm = new LookingForPropertyRequestReportNoteDeleteForm(idPrefix);
        this.lookingForPropertyRequestReportNoteDeleteFormElement = document.querySelector('#' + idPrefix + 'form');
        this.lookingForPropertyRequestReportNoteDeleteFormSubmitButton = document.querySelector('#' + idPrefix + 'submit_button');

        this.lookingForPropertyRequestReportNoteForm.disableFields();

        this.lookingForPropertyRequestReportNoteDeleteFormSubmitButton.addEventListener('click', (): void => {
            this.lookingForPropertyRequestReportNoteDeleteFormElement.submit();
        });
    }

}

export { NoteDeletePage };
