import { LazyImageLoader } from '../../../LazyImageLoader';
import { ConceptTagIconComponent } from '../../Component/HallOfInspiration/ConceptTagIconComponent';

class DetailPage {

    private readonly lazyImageLoader: LazyImageLoader;
    private readonly conceptTagIconComponent: ConceptTagIconComponent;

    constructor() {
        this.lazyImageLoader = new LazyImageLoader();
        this.conceptTagIconComponent = new ConceptTagIconComponent('concept_tag_');
    }

}

export { DetailPage };
