import { LazyImageLoader } from '../../../LazyImageLoader';
import { ConceptTagIconComponent } from '../../Component/HallOfInspiration/ConceptTagIconComponent';

class OverviewPage {

    private readonly lazyImageLoader: LazyImageLoader;
    private readonly hoiDetailDescriptionButton: HTMLButtonElement;
    private readonly hoiDetailDescriptionContainer: HTMLDivElement;

    constructor() {
        this.lazyImageLoader = new LazyImageLoader();
        this.hoiDetailDescriptionButton = document.querySelector('#hoi_detail_description_button');
        this.hoiDetailDescriptionContainer = document.querySelector('#hoi_detail_description_container');

        this.hoiDetailDescriptionButton.addEventListener('click', (): void => {
            this.hoiDetailDescriptionContainer.classList.remove('d-none');
            this.hoiDetailDescriptionButton.classList.add('d-none');
        });

        document.querySelectorAll('div.mdc-card[data-concept-id]').forEach((conceptTagIconContainer: HTMLElement): void => {
            new ConceptTagIconComponent('concept_' +  conceptTagIconContainer.dataset.conceptId + '_tag_');
        });
    }

}

export { OverviewPage };
