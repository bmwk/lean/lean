import { LookingForPropertyRequestPriceRequirementForm } from '../LookingForPropertyRequest/LookingForPropertyRequestPriceRequirementForm';
import { LookingForPropertyRequestPropertyRequirementForm } from '../LookingForPropertyRequest/LookingForPropertyRequestPropertyRequirementForm';
import { IndustryClassificationForm } from '../../../Classification/Form/IndustryClassificationForm';
import { PersonCreateForm } from '../../../PersonManagement/Form/Person/PersonCreateForm';
import { TextFieldComponent } from '../../../Component/TextFieldComponent';
import { MultiselectComponent } from '../../../Component/MultiselectComponent';
import { SelectComponent } from '../../../Component/SelectComponent';
import { CheckboxFieldComponent } from '../../../Component/CheckboxFieldComponent';

class ReportToLookingForPropertyRequestForm {

    private readonly titleTextField: TextFieldComponent;
    private readonly conceptDescriptionTextField: TextFieldComponent;
    private readonly requestReasonSelect: SelectComponent;
    private readonly automaticEndingCheckboxField: CheckboxFieldComponent;
    private readonly requestAvailableTillTextField: TextFieldComponent;
    private readonly lookingForPropertyRequestPriceRequirementForm: LookingForPropertyRequestPriceRequirementForm;
    private readonly lookingForPropertyRequestPropertyRequirementForm: LookingForPropertyRequestPropertyRequirementForm;
    private readonly industryClassificationForm: IndustryClassificationForm;
    private readonly locationFactorsMdcSelect: MultiselectComponent;
    private readonly personCreateForm: PersonCreateForm;

    constructor(idPrefix: string) {
        this.titleTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'title_mdc_text_field'));
        this.conceptDescriptionTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'conceptDescription_mdc_text_field'), {autoFitTextarea: true});
        this.requestReasonSelect = new SelectComponent(document.querySelector('#' + idPrefix + 'requestReason_mdc_select'));
        this.automaticEndingCheckboxField = new CheckboxFieldComponent(document.querySelector('#' + idPrefix + 'automaticEnding_mdc_form_field'));
        this.requestAvailableTillTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'requestAvailableTill_mdc_text_field'),{datePicker: true});
        this.lookingForPropertyRequestPriceRequirementForm = new LookingForPropertyRequestPriceRequirementForm(idPrefix + 'priceRequirement_');
        this.lookingForPropertyRequestPropertyRequirementForm = new LookingForPropertyRequestPropertyRequirementForm(idPrefix + 'propertyRequirement_');

        this.industryClassificationForm = new IndustryClassificationForm(
            idPrefix + 'industryClassification_',
            (industryClassificationLevelOne: number) => { this.lookingForPropertyRequestPropertyRequirementForm.showOrHideSpaceFieldsByIndustryClassification(industryClassificationLevelOne) },
            (industryClassificationLevelOne: number) => { this.lookingForPropertyRequestPropertyRequirementForm.showOrHideSpaceFieldsByIndustryClassification(industryClassificationLevelOne) }
        );

        this.locationFactorsMdcSelect = new MultiselectComponent(document.querySelector('#' + idPrefix + 'locationFactors_multiselect'));
        this.personCreateForm = new PersonCreateForm(idPrefix + 'personCreate_');

        this.titleTextField.mdcTextField.required = true;
        this.requestReasonSelect.mdcSelect.required = true;

        this.showOrHideAvailableTill();
        this.automaticEndingCheckboxField.mdcFormField.listen('change', (): void => this.showOrHideAvailableTill());
    }

    public showOrHideAvailableTill(): void {
        if (this.automaticEndingCheckboxField.isChecked() === true) {
            this.requestAvailableTillTextField.mdcTextField.root.parentElement.classList.remove('d-none');
        } else {
            this.requestAvailableTillTextField.mdcTextField.root.parentElement.classList.add('d-none');
        }
    }

    public isFormValid(): boolean {
        let hasErrors: boolean = false;

        if (this.personCreateForm.isFormValid() === false) {
            hasErrors = true;
        }

        if (this.industryClassificationForm.isFormValid() === false) {
            hasErrors = true;
        }

        if (this.titleTextField.mdcTextField.valid === false) {
            this.titleTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (this.requestReasonSelect.mdcSelect.value.length === 0) {
            this.requestReasonSelect.mdcSelect.valid = false;
            hasErrors = true;
        }

        if (this.lookingForPropertyRequestPropertyRequirementForm.isFormValid() === false) {
            hasErrors = true;
        }

        if (this.lookingForPropertyRequestPriceRequirementForm.isFormValid() === false) {
            hasErrors = true;
        }

        return (hasErrors === false);
    }

}

export { ReportToLookingForPropertyRequestForm };
