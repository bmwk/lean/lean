import { DeleteForm } from '../../../Form/DeleteForm';

class LookingForPropertyRequestReportNoteDeleteForm extends DeleteForm {

    constructor(idPrefix: string) {
        super(idPrefix);
    }

}

export { LookingForPropertyRequestReportNoteDeleteForm };
