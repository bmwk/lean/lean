import { TextFieldComponent } from '../../../Component/TextFieldComponent';

class LookingForPropertyRequestReportNoteForm {

    private readonly noteTextField: TextFieldComponent;

    constructor(idPrefix: string) {
        this.noteTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'note_mdc_text_field'), {autoFitTextarea: true});
    }

    public disableFields(): void {
        this.noteTextField.mdcTextField.disabled = true;
    }

}

export { LookingForPropertyRequestReportNoteForm };
