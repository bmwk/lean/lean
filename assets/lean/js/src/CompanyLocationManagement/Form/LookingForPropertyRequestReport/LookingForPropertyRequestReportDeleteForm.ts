import { DeleteForm } from '../../../Form/DeleteForm';

class LookingForPropertyRequestReportDeleteForm extends DeleteForm {

    constructor(idPrefix: string) {
        super(idPrefix);
    }

}

export { LookingForPropertyRequestReportDeleteForm };
