import { TextFieldComponent } from '../../../Component/TextFieldComponent';

class SettlementConceptForwardingRejectForm {

    private readonly rejectReasonTextField: TextFieldComponent;

    constructor(idPrefix: string) {
        this.rejectReasonTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'rejectReason_mdc_text_field'), {autoFitTextarea: true});
    }

}

export { SettlementConceptForwardingRejectForm };
