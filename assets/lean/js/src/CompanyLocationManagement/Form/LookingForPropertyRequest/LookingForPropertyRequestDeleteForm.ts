import { DeleteForm } from '../../../Form/DeleteForm';

class LookingForPropertyRequestDeleteForm extends DeleteForm {

    constructor(idPrefix: string) {
        super(idPrefix);
    }

}

export { LookingForPropertyRequestDeleteForm };
