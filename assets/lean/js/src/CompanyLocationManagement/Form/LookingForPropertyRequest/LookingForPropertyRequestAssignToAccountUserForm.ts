import { SelectComponent } from '../../../Component/SelectComponent';

class LookingForPropertyRequestAssignToAccountUserForm {

    private readonly assignedToAccountUserSelect: SelectComponent;

    constructor(idPrefix: string) {
        this.assignedToAccountUserSelect = new SelectComponent(document.querySelector('#' + idPrefix + 'assignedToAccountUser_mdc_select'));

        this.assignedToAccountUserSelect.mdcSelect.required = false;

        this.addClearIcon(this.assignedToAccountUserSelect);
    }

    public isFormValid(): boolean {
        let hasErrors: boolean = false;

        if (this.assignedToAccountUserSelect.mdcSelect.valid === false) {
            this.assignedToAccountUserSelect.mdcSelect.valid = false;
            hasErrors = true;
        }

        return (hasErrors === false);
    }

    private addClearIcon(select: SelectComponent): void {
        const clearIcon: HTMLElement = document.createElement('i');

        clearIcon.classList.add('material-icons');
        clearIcon.classList.add('mdc-text-field__icon');
        clearIcon.classList.add('mdc-text-field__icon--leading');
        clearIcon.classList.add('clear-icon');
        clearIcon.classList.add('small');
        clearIcon.tabIndex = 0;
        clearIcon.innerText = 'clear';
        clearIcon.id = 'clear_' + select.mdcSelect.root.id;

        clearIcon.addEventListener('click', (event): void => {
            event.stopPropagation();

            select.mdcSelect.value = null;
        });

        select.mdcSelect.root.querySelector('.mdc-select__anchor').insertBefore(clearIcon, select.mdcSelect.root.querySelector('.mdc-select__dropdown-icon'));
    }

}

export { LookingForPropertyRequestAssignToAccountUserForm };
