import { TextFieldComponent } from '../../../Component/TextFieldComponent';
import { MultiselectComponent } from '../../../Component/MultiselectComponent';
import { CheckboxFieldComponent } from '../../../Component/CheckboxFieldComponent';

class LookingForPropertyRequestPropertyRequirementForm {

    private readonly totalSpaceMinimumValueTextField: TextFieldComponent;
    private readonly totalSpaceMaximumValueTextField: TextFieldComponent;
    private readonly retailSpaceMinimumValueTextField: TextFieldComponent;
    private readonly retailSpaceMaximumValueTextField: TextFieldComponent;
    private readonly usableSpaceMinimumValueTextField: TextFieldComponent;
    private readonly usableSpaceMaximumValueTextField: TextFieldComponent;
    private readonly subsidiarySpaceMinimumValueTextField: TextFieldComponent;
    private readonly subsidiarySpaceMaximumValueTextField: TextFieldComponent;
    private readonly storageSpaceMinimumValueTextField: TextFieldComponent;
    private readonly storageSpaceMaximumValueTextField: TextFieldComponent;
    private readonly outdoorSpaceMinimumValueTextField: TextFieldComponent;
    private readonly outdoorSpaceMaximumValueTextField: TextFieldComponent;
    private readonly outdoorSpaceRequiredCheckboxField: CheckboxFieldComponent;
    private readonly roofingRequiredCheckboxField: CheckboxFieldComponent;
    private readonly shopWindowLengthMinimumValueTextField: TextFieldComponent;
    private readonly shopWindowLengthMaximumValueTextField: TextFieldComponent;
    private readonly showroomRequiredRequiredCheckboxField: CheckboxFieldComponent;
    private readonly shopWindowRequiredCheckboxField: CheckboxFieldComponent;
    private readonly numberOfParkingLotsTextField: TextFieldComponent;
    private readonly roomCountTextField: TextFieldComponent;
    private readonly shopWidthMinimumValueTextField: TextFieldComponent;
    private readonly shopWidthMaximumValueTextField: TextFieldComponent;
    private readonly locationCategoriesMultiselect: MultiselectComponent;
    private readonly barrierFreeAccessRequiredCheckboxField: CheckboxFieldComponent;
    private readonly groundLevelSalesAreaRequiredCheckboxField: CheckboxFieldComponent;
    private readonly placeMultiselect: MultiselectComponent;
    private readonly earliestAvailableFromTextField: TextFieldComponent;
    private readonly latestAvailableFromTextField: TextFieldComponent;

    constructor(idPrefix: string) {
        this.totalSpaceMinimumValueTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'propertyValueRangeRequirement_totalSpace_minimumValue_mdc_text_field'));
        this.totalSpaceMaximumValueTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'propertyValueRangeRequirement_totalSpace_maximumValue_mdc_text_field'));
        this.retailSpaceMinimumValueTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'propertyValueRangeRequirement_retailSpace_minimumValue_mdc_text_field'));
        this.retailSpaceMaximumValueTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'propertyValueRangeRequirement_retailSpace_maximumValue_mdc_text_field'));
        this.subsidiarySpaceMinimumValueTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'propertyValueRangeRequirement_subsidiarySpace_minimumValue_mdc_text_field'));
        this.subsidiarySpaceMaximumValueTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'propertyValueRangeRequirement_subsidiarySpace_maximumValue_mdc_text_field'));
        this.usableSpaceMinimumValueTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'propertyValueRangeRequirement_usableSpace_minimumValue_mdc_text_field'));
        this.usableSpaceMaximumValueTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'propertyValueRangeRequirement_usableSpace_maximumValue_mdc_text_field'));
        this.storageSpaceMinimumValueTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'propertyValueRangeRequirement_storageSpace_minimumValue_mdc_text_field'));
        this.storageSpaceMaximumValueTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'propertyValueRangeRequirement_storageSpace_maximumValue_mdc_text_field'));
        this.outdoorSpaceMinimumValueTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'propertyValueRangeRequirement_outdoorSpace_minimumValue_mdc_text_field'));
        this.outdoorSpaceMaximumValueTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'propertyValueRangeRequirement_outdoorSpace_maximumValue_mdc_text_field'));
        this.outdoorSpaceRequiredCheckboxField = new CheckboxFieldComponent(document.querySelector('#' + idPrefix + 'propertyMandatoryRequirement_outdoorSpaceRequired_mdc_form_field'));
        this.roofingRequiredCheckboxField = new CheckboxFieldComponent(document.querySelector('#' + idPrefix + 'propertyMandatoryRequirement_roofingRequired_mdc_form_field'));
        this.shopWindowLengthMinimumValueTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'propertyValueRangeRequirement_shopWindowLength_minimumValue_mdc_text_field'));
        this.shopWindowLengthMaximumValueTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'propertyValueRangeRequirement_shopWindowLength_maximumValue_mdc_text_field'));
        this.showroomRequiredRequiredCheckboxField = new CheckboxFieldComponent(document.querySelector('#' + idPrefix + 'propertyMandatoryRequirement_showroomRequired_mdc_form_field'));
        this.shopWindowRequiredCheckboxField = new CheckboxFieldComponent(document.querySelector('#' + idPrefix + 'propertyMandatoryRequirement_shopWindowRequired_mdc_form_field'));
        this.numberOfParkingLotsTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'numberOfParkingLots_mdc_text_field'));
        this.roomCountTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'roomCount_mdc_text_field'));
        this.shopWidthMinimumValueTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'propertyValueRangeRequirement_shopWidth_minimumValue_mdc_text_field'));
        this.shopWidthMaximumValueTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'propertyValueRangeRequirement_shopWidth_maximumValue_mdc_text_field'));
        this.locationCategoriesMultiselect = new MultiselectComponent(document.querySelector('#' + idPrefix + 'locationCategories_multiselect'));
        this.barrierFreeAccessRequiredCheckboxField = new CheckboxFieldComponent(document.querySelector('#' + idPrefix + 'propertyMandatoryRequirement_barrierFreeAccessRequired_mdc_form_field'));
        this.groundLevelSalesAreaRequiredCheckboxField = new CheckboxFieldComponent(document.querySelector('#' + idPrefix + 'propertyMandatoryRequirement_groundLevelSalesAreaRequired_mdc_form_field'));
        this.earliestAvailableFromTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'earliestAvailableFrom_mdc_text_field'), {datePicker: true});
        this.latestAvailableFromTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'latestAvailableFrom_mdc_text_field'), {datePicker: true});

        this.totalSpaceMaximumValueTextField.mdcTextField.required = true;

        if (document.querySelector('#' + idPrefix + 'inQuarters_multiselect') !== null) {
            this.placeMultiselect = new MultiselectComponent(document.querySelector('#' + idPrefix + 'inQuarters_multiselect'));
        } else if (document.querySelector('#' + idPrefix + 'inPlaces_multiselect') !== null) {
            this.placeMultiselect = new MultiselectComponent(document.querySelector('#' + idPrefix + 'inPlaces_multiselect'));
        }

        this.showOrHideOutdoorSpace();

        this.outdoorSpaceRequiredCheckboxField.mdcFormField.listen('change', (): void => this.showOrHideOutdoorSpace());

        this.showOrHideShopWindowSpace();

        this.shopWindowRequiredCheckboxField.mdcFormField.listen('change', (): void => this.showOrHideShopWindowSpace());
    }

    public isFormValid(): boolean {
        let hasErrors: boolean = false;

        if (this.totalSpaceMaximumValueTextField.mdcTextField.valid === false) {
            this.totalSpaceMaximumValueTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.totalSpaceMinimumValueTextField.mdcTextField.value.replace(',','.')))) {
            this.totalSpaceMinimumValueTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.totalSpaceMaximumValueTextField.mdcTextField.value.replace(',','.')))) {
            this.totalSpaceMaximumValueTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if ((Number(this.totalSpaceMaximumValueTextField.mdcTextField.value) < Number(this.totalSpaceMinimumValueTextField.mdcTextField.value))) {
            this.totalSpaceMaximumValueTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.numberOfParkingLotsTextField.mdcTextField.value))) {
            this.numberOfParkingLotsTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.roomCountTextField.mdcTextField.value))) {
            this.roomCountTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.shopWindowLengthMinimumValueTextField.mdcTextField.value.replace(',','.')))) {
            this.shopWindowLengthMinimumValueTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.shopWindowLengthMaximumValueTextField.mdcTextField.value.replace(',','.')))) {
            this.shopWindowLengthMaximumValueTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if ((Number(this.shopWindowLengthMaximumValueTextField.mdcTextField.value) < Number(this.shopWindowLengthMinimumValueTextField.mdcTextField.value))) {
            this.shopWindowLengthMaximumValueTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.shopWidthMinimumValueTextField.mdcTextField.value.replace(',','.')))) {
            this.shopWidthMinimumValueTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.shopWidthMaximumValueTextField.mdcTextField.value.replace(',','.')))) {
            this.shopWidthMaximumValueTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if ((Number(this.shopWidthMaximumValueTextField.mdcTextField.value) < Number(this.shopWidthMinimumValueTextField.mdcTextField.value))) {
            this.shopWidthMaximumValueTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.retailSpaceMinimumValueTextField.mdcTextField.value.replace(',','.')))) {
            this.retailSpaceMinimumValueTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.retailSpaceMaximumValueTextField.mdcTextField.value.replace(',','.')))) {
            this.retailSpaceMaximumValueTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if ((Number(this.retailSpaceMaximumValueTextField.mdcTextField.value) < Number(this.retailSpaceMinimumValueTextField.mdcTextField.value))) {
            this.retailSpaceMaximumValueTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.subsidiarySpaceMinimumValueTextField.mdcTextField.value.replace(',','.')))) {
            this.subsidiarySpaceMinimumValueTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.subsidiarySpaceMaximumValueTextField.mdcTextField.value.replace(',','.')))) {
            this.subsidiarySpaceMaximumValueTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if ((Number(this.subsidiarySpaceMaximumValueTextField.mdcTextField.value) < Number(this.subsidiarySpaceMinimumValueTextField.mdcTextField.value))) {
            this.subsidiarySpaceMaximumValueTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.usableSpaceMinimumValueTextField.mdcTextField.value.replace(',','.')))) {
            this.usableSpaceMinimumValueTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.usableSpaceMaximumValueTextField.mdcTextField.value.replace(',','.')))) {
            this.usableSpaceMaximumValueTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if ((Number(this.usableSpaceMaximumValueTextField.mdcTextField.value) < Number(this.usableSpaceMinimumValueTextField.mdcTextField.value))) {
            this.usableSpaceMaximumValueTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.storageSpaceMinimumValueTextField.mdcTextField.value.replace(',','.')))) {
            this.storageSpaceMinimumValueTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.storageSpaceMaximumValueTextField.mdcTextField.value.replace(',','.')))) {
            this.storageSpaceMaximumValueTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if ((Number(this.storageSpaceMaximumValueTextField.mdcTextField.value) < Number(this.storageSpaceMinimumValueTextField.mdcTextField.value))) {
            this.storageSpaceMaximumValueTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.outdoorSpaceMinimumValueTextField.mdcTextField.value.replace(',','.')))) {
            this.outdoorSpaceMinimumValueTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.outdoorSpaceMaximumValueTextField.mdcTextField.value.replace(',','.')))) {
            this.outdoorSpaceMaximumValueTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if ((Number(this.outdoorSpaceMaximumValueTextField.mdcTextField.value) < Number(this.outdoorSpaceMinimumValueTextField.mdcTextField.value))) {
            this.outdoorSpaceMaximumValueTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        return (hasErrors === false);
    }

    public showOrHideShopWindowSpace(): void {
        if (this.shopWindowRequiredCheckboxField.isChecked() === true) {
            this.shopWindowLengthMinimumValueTextField.mdcTextField.root.parentElement.classList.remove('d-none');
            this.shopWindowLengthMaximumValueTextField.mdcTextField.root.parentElement.classList.remove('d-none');
        } else {
            this.shopWindowLengthMinimumValueTextField.mdcTextField.root.parentElement.classList.add('d-none');
            this.shopWindowLengthMaximumValueTextField.mdcTextField.root.parentElement.classList.add('d-none');
        }
    }

    public showOrHideOutdoorSpace(): void {
        if (this.outdoorSpaceRequiredCheckboxField.isChecked() === true) {
            this.outdoorSpaceMinimumValueTextField.mdcTextField.root.parentElement.classList.remove('d-none');
            this.outdoorSpaceMaximumValueTextField.mdcTextField.root.parentElement.classList.remove('d-none');
            this.outdoorSpaceMaximumValueTextField.mdcTextField.root.parentElement.classList.remove('d-none');
            this.roofingRequiredCheckboxField.mdcFormField.root.parentElement.classList.remove('d-none');
        } else {
            this.outdoorSpaceMinimumValueTextField.mdcTextField.root.parentElement.classList.add('d-none');
            this.outdoorSpaceMaximumValueTextField.mdcTextField.root.parentElement.classList.add('d-none');
            this.roofingRequiredCheckboxField.mdcFormField.root.parentElement.classList.add('d-none');
        }
    }

    public showOrHideSpaceFieldsByIndustryClassification(industryClassificationLevelOne: number): void {
        switch (industryClassificationLevelOne) {
            case 1:
                this.showOrHideSpaceFields(true, true, true, true, true);
                break;
            case 2:
                this.showOrHideSpaceFields(false, false, false,  false, false);
                break;
            case 3:
                this.showOrHideSpaceFields(false, true, true,  false, true);
                break;
            case 4:
                this.showOrHideSpaceFields(false, false, false,  false, false);
                break;
            case 5:
                this.showOrHideSpaceFields(false, false, false,  false, false);
                break;
            case 6:
                this.showOrHideSpaceFields(false, true, false,  false, true);
                break;
            case 7:
                this.showOrHideSpaceFields(false, false, false, false, false);
                break;
            case 8:
                this.showOrHideSpaceFields(false, false, false, false, false)
                break;
            case 9:
                this.showOrHideSpaceFields(false, true, false, false, true);
                break;
            case 10:
                this.showOrHideSpaceFields(false, false, false, false, false);
                break;
            case 11:
                this.showOrHideSpaceFields(false, false, true, false, false);
                break;
            case 12:
                this.showOrHideSpaceFields(false, true, false, false, true);
                break;
            case 13:
                this.showOrHideSpaceFields(true, true, true, true, true);
                break;
            case 14:
                this.showOrHideSpaceFields(false, false, false, false, false);
                break;
            case 15:
                this.showOrHideSpaceFields(false, false, false, false, false);
                break;
            case 16:
                this.showOrHideSpaceFields(false, false, false, false, false);
                break;
            default:
                this.showOrHideSpaceFields(false, false, false, false, false);
        }
    }

    private showOrHideSpaceFields(showRetailSpace: boolean, showUsableSpace: boolean, showStorageSpace: boolean, showShopWidthSpace: boolean, showSubsidiarySpace: boolean ): void {
        if (showRetailSpace === true) {
            this.retailSpaceMaximumValueTextField.mdcTextField.root.parentElement.classList.remove('d-none');
            this.retailSpaceMinimumValueTextField.mdcTextField.root.parentElement.classList.remove('d-none');
        } else {
            this.retailSpaceMaximumValueTextField.mdcTextField.root.parentElement.classList.add('d-none');
            this.retailSpaceMinimumValueTextField.mdcTextField.root.parentElement.classList.add('d-none');
        }

        if (showUsableSpace === true) {
            this.usableSpaceMaximumValueTextField.mdcTextField.root.parentElement.classList.remove('d-none');
            this.usableSpaceMinimumValueTextField.mdcTextField.root.parentElement.classList.remove('d-none');
        } else {
            this.usableSpaceMaximumValueTextField.mdcTextField.root.parentElement.classList.add('d-none');
            this.usableSpaceMinimumValueTextField.mdcTextField.root.parentElement.classList.add('d-none');
        }

        if (showSubsidiarySpace === true) {
            this.subsidiarySpaceMaximumValueTextField.mdcTextField.root.parentElement.classList.remove('d-none');
            this.subsidiarySpaceMinimumValueTextField.mdcTextField.root.parentElement.classList.remove('d-none');
        } else {
            this.subsidiarySpaceMaximumValueTextField.mdcTextField.root.parentElement.classList.add('d-none');
            this.subsidiarySpaceMinimumValueTextField.mdcTextField.root.parentElement.classList.add('d-none');
        }

        if (showStorageSpace === true) {
            this.storageSpaceMaximumValueTextField.mdcTextField.root.parentElement.classList.remove('d-none');
            this.storageSpaceMinimumValueTextField.mdcTextField.root.parentElement.classList.remove('d-none');
        } else {
            this.storageSpaceMaximumValueTextField.mdcTextField.root.parentElement.classList.add('d-none');
            this.storageSpaceMinimumValueTextField.mdcTextField.root.parentElement.classList.add('d-none');
        }

        if (showShopWidthSpace === true) {
            this.shopWidthMinimumValueTextField.mdcTextField.root.parentElement.classList.remove('d-none');
            this.shopWidthMaximumValueTextField.mdcTextField.root.parentElement.classList.remove('d-none');
        } else {
            this.shopWidthMinimumValueTextField.mdcTextField.root.parentElement.classList.add('d-none');
            this.shopWidthMaximumValueTextField.mdcTextField.root.parentElement.classList.add('d-none');
        }

        if (showRetailSpace === false) {
            this.totalSpaceMaximumValueTextField.mdcTextField.root.parentElement.classList.remove('col-md-3');
            this.totalSpaceMaximumValueTextField.mdcTextField.root.parentElement.classList.add('col-md-6');
            this.totalSpaceMaximumValueTextField.mdcTextField.root.parentElement.classList.remove('mb-4');
            this.totalSpaceMinimumValueTextField.mdcTextField.root.parentElement.classList.remove('col-md-3');
            this.totalSpaceMinimumValueTextField.mdcTextField.root.parentElement.classList.add('col-md-6');
        } else {
            this.totalSpaceMaximumValueTextField.mdcTextField.root.parentElement.classList.add('col-md-3');
            this.totalSpaceMaximumValueTextField.mdcTextField.root.parentElement.classList.remove('col-md-6');
            this.totalSpaceMaximumValueTextField.mdcTextField.root.parentElement.classList.add('mb-4');
            this.totalSpaceMinimumValueTextField.mdcTextField.root.parentElement.classList.add('col-md-3');
            this.totalSpaceMinimumValueTextField.mdcTextField.root.parentElement.classList.remove('col-md-6');
        }

        if (showStorageSpace === false) {
            this.usableSpaceMaximumValueTextField.mdcTextField.root.parentElement.classList.remove('col-md-3');
            this.usableSpaceMaximumValueTextField.mdcTextField.root.parentElement.classList.add('col-md-6');
            this.usableSpaceMinimumValueTextField.mdcTextField.root.parentElement.classList.remove('col-md-3');
            this.usableSpaceMinimumValueTextField.mdcTextField.root.parentElement.classList.add('col-md-6');
        } else {
            this.usableSpaceMaximumValueTextField.mdcTextField.root.parentElement.classList.add('col-md-3');
            this.usableSpaceMaximumValueTextField.mdcTextField.root.parentElement.classList.remove('col-md-6');
            this.usableSpaceMinimumValueTextField.mdcTextField.root.parentElement.classList.add('col-md-3');
            this.usableSpaceMinimumValueTextField.mdcTextField.root.parentElement.classList.remove('col-md-6');
        }

        if (showUsableSpace === false) {
            this.storageSpaceMinimumValueTextField.mdcTextField.root.parentElement.classList.remove('col-md-3');
            this.storageSpaceMinimumValueTextField.mdcTextField.root.parentElement.classList.add('col-md-6');
            this.storageSpaceMaximumValueTextField.mdcTextField.root.parentElement.classList.remove('col-md-3');
            this.storageSpaceMaximumValueTextField.mdcTextField.root.parentElement.classList.add('col-md-6');
        } else {
            this.storageSpaceMinimumValueTextField.mdcTextField.root.parentElement.classList.add('col-md-3');
            this.storageSpaceMinimumValueTextField.mdcTextField.root.parentElement.classList.remove('col-md-6');
            this.storageSpaceMaximumValueTextField.mdcTextField.root.parentElement.classList.add('col-md-3');
            this.storageSpaceMaximumValueTextField.mdcTextField.root.parentElement.classList.remove('col-md-6');
        }

        if (showSubsidiarySpace === false) {
            this.storageSpaceMinimumValueTextField.mdcTextField.root.parentElement.classList.remove('col-md-3');
            this.storageSpaceMinimumValueTextField.mdcTextField.root.parentElement.classList.add('col-md-6');
            this.storageSpaceMaximumValueTextField.mdcTextField.root.parentElement.classList.remove('col-md-3');
            this.storageSpaceMaximumValueTextField.mdcTextField.root.parentElement.classList.add('col-md-6');
        } else {
            this.storageSpaceMinimumValueTextField.mdcTextField.root.parentElement.classList.add('col-md-3');
            this.storageSpaceMinimumValueTextField.mdcTextField.root.parentElement.classList.remove('col-md-6');
            this.storageSpaceMaximumValueTextField.mdcTextField.root.parentElement.classList.add('col-md-3');
            this.storageSpaceMaximumValueTextField.mdcTextField.root.parentElement.classList.remove('col-md-6');
        }

        if (showShopWidthSpace === false) {
            this.numberOfParkingLotsTextField.mdcTextField.root.parentElement.classList.remove('col-md-3');
            this.numberOfParkingLotsTextField.mdcTextField.root.parentElement.classList.add('col-md-6');
            this.roomCountTextField.mdcTextField.root.parentElement.classList.remove('col-md-3');
            this.roomCountTextField.mdcTextField.root.parentElement.classList.add('col-md-6');
        } else {
            this.numberOfParkingLotsTextField.mdcTextField.root.parentElement.classList.add('col-md-3');
            this.numberOfParkingLotsTextField.mdcTextField.root.parentElement.classList.remove('col-md-6');

            this.roomCountTextField.mdcTextField.root.parentElement.classList.add('col-md-3');
            this.roomCountTextField.mdcTextField.root.parentElement.classList.remove('col-md-6');
        }
    }

}

export { LookingForPropertyRequestPropertyRequirementForm };
