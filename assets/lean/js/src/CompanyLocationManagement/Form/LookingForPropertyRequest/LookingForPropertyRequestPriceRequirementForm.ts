import { TextFieldComponent } from '../../../Component/TextFieldComponent';
import { MDCFormField } from '@material/form-field';
import { MDCCheckbox } from '@material/checkbox';

class LookingForPropertyRequestPriceRequirementForm {

    private readonly nonCommissionableMdcFormField: MDCFormField;
    private readonly propertyOfferTypePurchaseMdcFormField: MDCFormField;
    private readonly propertyOfferTypeRentMdcFormField: MDCFormField;
    private readonly propertyOfferTypeLeaseMdcFormField: MDCFormField;
    private readonly purchasePriceNetMinimumValueTextField: TextFieldComponent;
    private readonly purchasePriceNetMaximumValueTextField: TextFieldComponent;
    private readonly purchasePriceGrossMinimumValueTextField: TextFieldComponent;
    private readonly purchasePriceGrossMaximumValueTextField: TextFieldComponent;
    private readonly purchasePricePerSquareMeterMinimumValueTextField: TextFieldComponent;
    private readonly purchasePricePerSquareMeterMaximumValueTextField: TextFieldComponent;
    private readonly netColdRentMinimumValueTextField: TextFieldComponent;
    private readonly netColdRentMaximumValueTextField: TextFieldComponent;
    private readonly coldRentMinimumValueTextField: TextFieldComponent;
    private readonly coldRentMaximumValueTextField: TextFieldComponent;
    private readonly rentalPricePerSquareMeterMinimumValueTextField: TextFieldComponent;
    private readonly rentalPricePerSquareMeterMaximumValueTextField: TextFieldComponent;
    private readonly leaseMinimumValueTextField: TextFieldComponent;
    private readonly leaseMaximumValueTextField: TextFieldComponent;
    private readonly purchasePricingInformationContainer: HTMLDivElement;
    private readonly rentPricingInformationContainer: HTMLDivElement;
    private readonly leasePricingInformationContainer: HTMLDivElement;
    private readonly propertyOfferTypesLabel: HTMLSpanElement;
    private readonly propertyOfferTypesErrorMessage: HTMLDivElement;

    constructor(idPrefix: string) {

        this.nonCommissionableMdcFormField = new MDCFormField(document.querySelector('#' + idPrefix + 'nonCommissionable_mdc_form_field'));
        this.propertyOfferTypePurchaseMdcFormField = new MDCFormField(document.querySelector('#' + idPrefix + 'propertyOfferTypes_0_mdc_form_field'));
        this.propertyOfferTypeRentMdcFormField = new MDCFormField(document.querySelector('#' + idPrefix + 'propertyOfferTypes_1_mdc_form_field'));
        this.propertyOfferTypeLeaseMdcFormField = new MDCFormField(document.querySelector('#' + idPrefix + 'propertyOfferTypes_2_mdc_form_field'));
        this.purchasePriceNetMinimumValueTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'purchasePriceNet_minimumValue_mdc_text_field'));
        this.purchasePriceNetMaximumValueTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'purchasePriceNet_maximumValue_mdc_text_field'));
        this.purchasePriceGrossMinimumValueTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'purchasePriceGross_minimumValue_mdc_text_field'));
        this.purchasePriceGrossMaximumValueTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'purchasePriceGross_maximumValue_mdc_text_field'));
        this.purchasePricePerSquareMeterMinimumValueTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'purchasePricePerSquareMeter_minimumValue_mdc_text_field'));
        this.purchasePricePerSquareMeterMaximumValueTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'purchasePricePerSquareMeter_maximumValue_mdc_text_field'));
        this.netColdRentMinimumValueTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'netColdRent_minimumValue_mdc_text_field'));
        this.netColdRentMaximumValueTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'netColdRent_maximumValue_mdc_text_field'));
        this.coldRentMinimumValueTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'coldRent_minimumValue_mdc_text_field'));
        this.coldRentMaximumValueTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'coldRent_maximumValue_mdc_text_field'));
        this.rentalPricePerSquareMeterMinimumValueTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'rentalPricePerSquareMeter_minimumValue_mdc_text_field'));
        this.rentalPricePerSquareMeterMaximumValueTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'rentalPricePerSquareMeter_maximumValue_mdc_text_field'));
        this.leaseMinimumValueTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'lease_minimumValue_mdc_text_field'));
        this.leaseMaximumValueTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'lease_maximumValue_mdc_text_field'));
        this.purchasePricingInformationContainer = document.querySelector('#' + idPrefix + 'purchase_pricing_information');
        this.rentPricingInformationContainer = document.querySelector('#' + idPrefix + 'rent_pricing_information');
        this.leasePricingInformationContainer = document.querySelector('#' + idPrefix + 'lease_pricing_information');
        this.propertyOfferTypesLabel = document.querySelector('#' + idPrefix + 'propertyOfferTypes_label');
        this.propertyOfferTypesErrorMessage = document.querySelector('#' + idPrefix + 'propertyOfferTypes_error_message');

        this.propertyOfferTypePurchaseMdcFormField.input = new MDCCheckbox(document.querySelector('#' + idPrefix + 'propertyOfferTypes_0_mdc_checkbox'));
        this.propertyOfferTypeRentMdcFormField.input = new MDCCheckbox(document.querySelector('#' + idPrefix + 'propertyOfferTypes_1_mdc_checkbox'));
        this.propertyOfferTypeLeaseMdcFormField.input = new MDCCheckbox(document.querySelector('#' + idPrefix + 'propertyOfferTypes_2_mdc_checkbox'));

        this.showOrHidePropertyOfferTypeContainer(<MDCCheckbox>this.propertyOfferTypePurchaseMdcFormField.input, this.purchasePricingInformationContainer);
        this.showOrHidePropertyOfferTypeContainer(<MDCCheckbox>this.propertyOfferTypeRentMdcFormField.input, this.rentPricingInformationContainer);
        this.showOrHidePropertyOfferTypeContainer(<MDCCheckbox>this.propertyOfferTypeLeaseMdcFormField.input, this.leasePricingInformationContainer);

        this.propertyOfferTypePurchaseMdcFormField.listen('change', (vent): void => {
            this.showOrHidePropertyOfferTypeContainer(<MDCCheckbox>this.propertyOfferTypePurchaseMdcFormField.input, this.purchasePricingInformationContainer);
        });

        this.propertyOfferTypeRentMdcFormField.listen('change', (vent): void => {
            this.showOrHidePropertyOfferTypeContainer(<MDCCheckbox>this.propertyOfferTypeRentMdcFormField.input, this.rentPricingInformationContainer);
        });

        this.propertyOfferTypeLeaseMdcFormField.listen('change', (vent): void => {
            this.showOrHidePropertyOfferTypeContainer(<MDCCheckbox>this.propertyOfferTypeLeaseMdcFormField.input, this.leasePricingInformationContainer);
        });
    }

    public showPurchasePricingInformationContainer(): void {
        this.purchasePricingInformationContainer.classList.remove('d-none');
    }

    public hidePurchasePricingInformationContainer(): void {
        this.purchasePricingInformationContainer.classList.add('d-none');
    }

    public showRentPricingInformationContainer(): void {
        this.rentPricingInformationContainer.classList.remove('d-none');
    }

    public hideRentPricingInformationContainer(): void {
        this.rentPricingInformationContainer.classList.add('d-none');
    }

    public showLeasePricingInformationContainer(): void {
        this.leasePricingInformationContainer.classList.remove('d-none');
    }

    public hideLeasePricingInformationContainer(): void {
        this.leasePricingInformationContainer.classList.add('d-none');
    }

    public showOrHidePropertyOfferTypeContainer(mdcCheckbox: MDCCheckbox, propertyOfferTypeContainer: HTMLDivElement): void {
        if (mdcCheckbox.checked === true) {
            propertyOfferTypeContainer.classList.remove('d-none');
        } else {
            propertyOfferTypeContainer.classList.add('d-none');
        }
    }

    private checkMinimumSelectedPropertyOfferTypes(minNumber: number = 1): boolean {
        let numberSelected = 0;

        if ((<MDCCheckbox>this.propertyOfferTypePurchaseMdcFormField.input).checked === true) {
            ++numberSelected;
        }

        if ((<MDCCheckbox>this.propertyOfferTypeRentMdcFormField.input).checked === true) {
            ++numberSelected;
        }

        if ((<MDCCheckbox>this.propertyOfferTypeLeaseMdcFormField.input).checked === true) {
            ++numberSelected;
        }

        return numberSelected >= minNumber;
    }

    private showPropertyOfferTypesErrorNotification(): void {
        this.propertyOfferTypesLabel.classList.add('text-danger');
        this.propertyOfferTypesErrorMessage.classList.remove('d-none');
    }

    private hidePropertyOfferTypesErrorNotification(): void {
        this.propertyOfferTypesLabel.classList.remove('text-danger');
        this.propertyOfferTypesErrorMessage.classList.add('d-none');
    }

    public isFormValid(): boolean {
        let hasErrors: boolean = false;

        this.hidePropertyOfferTypesErrorNotification();

        if (this.checkMinimumSelectedPropertyOfferTypes(1) === false) {
            this.showPropertyOfferTypesErrorNotification();
            hasErrors = true;
        }

        if (isNaN(Number(this.purchasePriceNetMinimumValueTextField.mdcTextField.value.replace(',', '.')))) {
            this.purchasePriceNetMinimumValueTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.purchasePriceNetMaximumValueTextField.mdcTextField.value.replace(',', '.')))) {
            this.purchasePriceNetMaximumValueTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (Number(this.purchasePriceNetMaximumValueTextField.mdcTextField.value) < Number(this.purchasePriceNetMinimumValueTextField.mdcTextField.value)) {
            this.purchasePriceNetMaximumValueTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.purchasePriceGrossMinimumValueTextField.mdcTextField.value.replace(',', '.')))) {
            this.purchasePriceGrossMinimumValueTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.purchasePriceGrossMaximumValueTextField.mdcTextField.value.replace(',', '.')))) {
            this.purchasePriceGrossMaximumValueTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (Number(this.purchasePriceGrossMaximumValueTextField.mdcTextField.value) < Number(this.purchasePriceGrossMinimumValueTextField.mdcTextField.value)) {
            this.purchasePriceGrossMaximumValueTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.purchasePricePerSquareMeterMinimumValueTextField.mdcTextField.value.replace(',', '.')))) {
            this.purchasePricePerSquareMeterMinimumValueTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.purchasePricePerSquareMeterMaximumValueTextField.mdcTextField.value.replace(',', '.')))) {
            this.purchasePricePerSquareMeterMaximumValueTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (Number(this.purchasePricePerSquareMeterMaximumValueTextField.mdcTextField.value) < Number(this.purchasePricePerSquareMeterMinimumValueTextField.mdcTextField.value)) {
            this.purchasePricePerSquareMeterMaximumValueTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.netColdRentMinimumValueTextField.mdcTextField.value.replace(',', '.')))) {
            this.netColdRentMinimumValueTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.netColdRentMaximumValueTextField.mdcTextField.value.replace(',', '.')))) {
            this.netColdRentMaximumValueTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (Number(this.netColdRentMaximumValueTextField.mdcTextField.value) < Number(this.netColdRentMinimumValueTextField.mdcTextField.value)) {
            this.netColdRentMaximumValueTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.coldRentMinimumValueTextField.mdcTextField.value.replace(',', '.')))) {
            this.coldRentMinimumValueTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.coldRentMaximumValueTextField.mdcTextField.value.replace(',', '.')))) {
            this.coldRentMaximumValueTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (Number(this.coldRentMaximumValueTextField.mdcTextField.value) < Number(this.coldRentMinimumValueTextField.mdcTextField.value)) {
            this.coldRentMaximumValueTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.rentalPricePerSquareMeterMinimumValueTextField.mdcTextField.value.replace(',', '.')))) {
            this.rentalPricePerSquareMeterMinimumValueTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.rentalPricePerSquareMeterMaximumValueTextField.mdcTextField.value.replace(',', '.')))) {
            this.rentalPricePerSquareMeterMaximumValueTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (Number(this.rentalPricePerSquareMeterMaximumValueTextField.mdcTextField.value) < Number(this.rentalPricePerSquareMeterMinimumValueTextField.mdcTextField.value)) {
            this.rentalPricePerSquareMeterMaximumValueTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.leaseMinimumValueTextField.mdcTextField.value.replace(',', '.')))) {
            this.leaseMinimumValueTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.leaseMaximumValueTextField.mdcTextField.value.replace(',', '.')))) {
            this.leaseMaximumValueTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (Number(this.leaseMaximumValueTextField.mdcTextField.value) < Number(this.leaseMinimumValueTextField.mdcTextField.value)) {
            this.leaseMaximumValueTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        return (hasErrors === false);
    }

}

export { LookingForPropertyRequestPriceRequirementForm };
