import { TextFieldComponent } from '../../../Component/TextFieldComponent';
import { SelectComponent } from '../../../Component/SelectComponent';

class LookingForPropertyRequestFilterForm {

    private readonly areaSizeMinimumTextField: TextFieldComponent;
    private readonly areaSizeMaximumTextField: TextFieldComponent;
    private readonly assignedToAccountUserSelect: SelectComponent;
    private readonly lastUpdatedFromTextField: TextFieldComponent;
    private readonly lastUpdatedTillTextField: TextFieldComponent;

    constructor(idPrefix: string) {
        this.areaSizeMinimumTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'areaSizeMinimum_mdc_text_field'));
        this.areaSizeMaximumTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'areaSizeMaximum_mdc_text_field'));
        this.assignedToAccountUserSelect = new SelectComponent(document.querySelector('#' + idPrefix + 'assignedToAccountUser_mdc_select'), {clearButton: true});
        this.lastUpdatedFromTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'lastUpdatedFrom_mdc_text_field'), {datePicker: true});
        this.lastUpdatedTillTextField = new TextFieldComponent(document.querySelector('#' + idPrefix + 'lastUpdatedTill_mdc_text_field'), {datePicker: true});

        this.lastUpdatedFromTextField.setMaxDate(new Date());
        this.lastUpdatedTillTextField.setMaxDate(new Date());
    }

    public isFormValid(): boolean {
        let hasErrors: boolean = false;

        if (isNaN(Number(this.areaSizeMinimumTextField.mdcTextField.value.replace(',', '.')))) {
            this.areaSizeMinimumTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        if (isNaN(Number(this.areaSizeMaximumTextField.mdcTextField.value.replace(',', '.')))) {
            this.areaSizeMaximumTextField.mdcTextField.valid = false;
            hasErrors = true;
        }

        return (hasErrors === false);
    }

}

export { LookingForPropertyRequestFilterForm };
