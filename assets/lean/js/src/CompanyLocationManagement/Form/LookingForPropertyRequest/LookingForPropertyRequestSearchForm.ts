import { SearchFieldComponent } from '../../../Component/SearchFieldComponent';

class LookingForPropertyRequestSearchForm {

    private readonly searchFieldComponent: SearchFieldComponent;

    constructor(idPrefix: string) {
        this.searchFieldComponent = new SearchFieldComponent(idPrefix, 'text');
    }

}

export { LookingForPropertyRequestSearchForm };
