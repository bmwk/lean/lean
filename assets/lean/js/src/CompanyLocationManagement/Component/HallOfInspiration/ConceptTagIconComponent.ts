import { MDCTooltip } from '@material/tooltip';

class ConceptTagIconComponent {

    private readonly iconDataContainer: HTMLDivElement;
    private readonly mdcTooltips: MDCTooltip[];

    constructor(idPrefix: string) {
        this.mdcTooltips = [];
        this.iconDataContainer = document.querySelector('#' + idPrefix + 'container');

        this.iconDataContainer.querySelectorAll('i.tag-icon[data-tag-id]').forEach((iconElement: HTMLElement): void => {
            if (this.iconDataContainer.querySelector('#' + idPrefix + 'icon-' + iconElement.dataset.tagId + '-mdc-tooltip') !== null) {
                this.mdcTooltips.push(new MDCTooltip(this.iconDataContainer.querySelector('#' + idPrefix + 'icon-' + iconElement.dataset.tagId + '-mdc-tooltip')));
            }
        });
    }

}

export { ConceptTagIconComponent };
