import { ListComponent } from '../../../Component/ListComponent';
import { MdcContextMenuComponent } from '../../../Component/MdcContextMenuComponent';
import { MDCTooltip } from '@material/tooltip';

class SettlementConceptMatchingListComponent extends ListComponent {

    private readonly mdcContextMenuComponents: MdcContextMenuComponent[] = [];
    private readonly mdcTooltips: MDCTooltip[];

    constructor(idPrefix: string) {
        super(idPrefix);

        this.mdcTooltips = [];

        this.mdcDataTableContainer.querySelectorAll('tr.mdc-data-table__row[data-row-id]').forEach((tableRowElement: HTMLTableRowElement): void => {
            if (tableRowElement.querySelector('#settlement_concept_matching_result_' + tableRowElement.dataset.rowId + '_menu_button') !== null) {
                this.mdcContextMenuComponents.push(new MdcContextMenuComponent('settlement_concept_matching_result_' + tableRowElement.dataset.rowId + '_'));
            }
        });

        this.mdcDataTableContainer.querySelectorAll('tr.mdc-data-table__row[data-row-id]').forEach((tableRowElement: HTMLTableRowElement): void => {
            if (tableRowElement.querySelector('#settlement-concept-matching-' + tableRowElement.dataset.rowId + '-send-mdc-tooltip') !== null) {
                this.mdcTooltips.push(new MDCTooltip(tableRowElement.querySelector('#settlement-concept-matching-' + tableRowElement.dataset.rowId + '-send-mdc-tooltip')));
            }

            if (tableRowElement.querySelector('#settlement-concept-matching-' + tableRowElement.dataset.rowId + '-detail-mdc-tooltip') !== null) {
                this.mdcTooltips.push(new MDCTooltip(tableRowElement.querySelector('#settlement-concept-matching-' + tableRowElement.dataset.rowId + '-detail-mdc-tooltip')));
            }

            if (tableRowElement.querySelector('#settlement-concept-matching-' + tableRowElement.dataset.rowId + '-evaluation-mdc-tooltip') !== null) {
                this.mdcTooltips.push(new MDCTooltip(tableRowElement.querySelector('#settlement-concept-matching-' + tableRowElement.dataset.rowId + '-evaluation-mdc-tooltip')));
            }

            if (tableRowElement.querySelector('#settlement-concept-matching-' + tableRowElement.dataset.rowId + '-rejectReason-mdc-tooltip') !== null) {
                this.mdcTooltips.push(new MDCTooltip(tableRowElement.querySelector('#settlement-concept-matching-' + tableRowElement.dataset.rowId + '-rejectReason-mdc-tooltip')));
            }

            if (tableRowElement.querySelector('#settlement-concept-matching-' + tableRowElement.dataset.rowId + '-industryClassification-mdc-tooltip') !== null) {
                this.mdcTooltips.push(new MDCTooltip(tableRowElement.querySelector('#settlement-concept-matching-' + tableRowElement.dataset.rowId + '-industryClassification-mdc-tooltip')));
            }
        });
    }

}

export { SettlementConceptMatchingListComponent };
