import { MdcContextMenuComponent } from '../../../Component/MdcContextMenuComponent';
import { MDCTooltip } from '@material/tooltip';

class ObjectCardComponent {

    private readonly cardContainer: HTMLDivElement;
    private readonly mdcTooltip: MDCTooltip;
    private readonly contextMenu: MdcContextMenuComponent = null;

    constructor(cardContainer: HTMLDivElement) {
        this.cardContainer = cardContainer;

        const contextMenuIdPrefix: string = 'building_unit_' + this.cardContainer.dataset.buildingUnitId + '_';

        if (cardContainer.querySelector('#building-unit-' + this.cardContainer.dataset.buildingUnitId + '-possible-property-usage-mdc-tooltip') !== null){
            this.mdcTooltip = new MDCTooltip(cardContainer.querySelector('#building-unit-' + this.cardContainer.dataset.buildingUnitId + '-possible-property-usage-mdc-tooltip'));
        }

        if (cardContainer.querySelector('#building-unit-' + this.cardContainer.dataset.buildingUnitId + '-missing-matching-relevant-fields-mdc-tooltip') !== null){
            this.mdcTooltip = new MDCTooltip(cardContainer.querySelector('#building-unit-' + this.cardContainer.dataset.buildingUnitId + '-missing-matching-relevant-fields-mdc-tooltip'));
        }

        if (MdcContextMenuComponent.checkContainerExists(contextMenuIdPrefix)) {
            this.contextMenu = new MdcContextMenuComponent(contextMenuIdPrefix);
        }
    }

}

export { ObjectCardComponent };
