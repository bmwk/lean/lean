import { ListComponent } from '../../../Component/ListComponent';
import { MdcContextMenuComponent } from '../../../Component/MdcContextMenuComponent';
import { MDCTooltip } from '@material/tooltip';

class ObjectMatchingListComponent extends ListComponent {

    private readonly mdcContextMenuComponents: MdcContextMenuComponent[] = [];
    private readonly mdcTooltips: MDCTooltip[];

    constructor(idPrefix: string) {
        super(idPrefix);

        this.mdcTooltips = [];

        this.mdcDataTableContainer.querySelectorAll('tr.mdc-data-table__row[data-row-id]').forEach((tableRowElement: HTMLTableRowElement): void => {
            if (tableRowElement.querySelector('#object-matching-' + tableRowElement.dataset.rowId + '-send-mdc-tooltip') !== null) {
                this.mdcTooltips.push(new MDCTooltip(tableRowElement.querySelector('#object-matching-' + tableRowElement.dataset.rowId + '-send-mdc-tooltip')));
            }

            if (tableRowElement.querySelector('#object-matching-' + tableRowElement.dataset.rowId + '-evaluation-mdc-tooltip') !== null) {
                this.mdcTooltips.push(new MDCTooltip(tableRowElement.querySelector('#object-matching-' + tableRowElement.dataset.rowId + '-evaluation-mdc-tooltip')));
            }

            if (tableRowElement.querySelector('#object-matching-' + tableRowElement.dataset.rowId + '-rejectReason-mdc-tooltip') !== null) {
                this.mdcTooltips.push(new MDCTooltip(tableRowElement.querySelector('#object-matching-' + tableRowElement.dataset.rowId + '-rejectReason-mdc-tooltip')));
            }
        });

        this.mdcDataTableContainer.querySelectorAll('tr.mdc-data-table__row[data-row-id]').forEach((tableRowElement: HTMLTableRowElement): void => {
            this.mdcContextMenuComponents.push(new MdcContextMenuComponent('matching_result_' + tableRowElement.dataset.rowId + '_'));
        });
    }

}

export { ObjectMatchingListComponent };
