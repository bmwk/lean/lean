import { MdcContextMenuComponent } from '../../../Component/MdcContextMenuComponent';

class ObjectDetailCardComponent {

    private readonly cardContainer: HTMLDivElement;
    private readonly contextMenu: MdcContextMenuComponent = null;

    constructor(cardContainer: HTMLDivElement) {
        this.cardContainer = cardContainer;

        const contextMenuIdPrefix: string = 'building_unit_' + this.cardContainer.dataset.buildingUnitId + '_';

        if (MdcContextMenuComponent.checkContainerExists(contextMenuIdPrefix) === true) {
            this.contextMenu = new MdcContextMenuComponent(contextMenuIdPrefix);
        }
    }

}

export { ObjectDetailCardComponent };
