import { ListComponent } from '../../../Component/ListComponent';
import { MDCTooltip } from '@material/tooltip';

class LookingForPropertyRequestListComponent extends ListComponent {

    private readonly mdcTooltips: MDCTooltip[];

    constructor(idPrefix: string) {
        super(idPrefix);

        this.mdcTooltips = [];

        this.mdcDataTableContainer.querySelectorAll('tr.mdc-data-table__row[data-row-id]').forEach((tableRowElement: HTMLTableRowElement): void => {
            if (tableRowElement.querySelector('#looking-for-property-request-' + tableRowElement.dataset.rowId + '-type-mdc-tooltip') !== null) {
                this.mdcTooltips.push(new MDCTooltip(tableRowElement.querySelector('#looking-for-property-request-' + tableRowElement.dataset.rowId + '-type-mdc-tooltip')));
            }
        });
    }

}

export { LookingForPropertyRequestListComponent };
