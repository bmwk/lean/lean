import { ListComponent } from '../../../Component/ListComponent';
import { MdcContextMenuComponent } from '../../../Component/MdcContextMenuComponent';
import { MDCTooltip } from '@material/tooltip';

class ExposeForwardingListComponent extends ListComponent {

    private readonly mdcContextMenuComponents: MdcContextMenuComponent[] = [];
    private readonly mdcTooltips: MDCTooltip[];

    constructor(idPrefix: string) {
        super(idPrefix);

        this.mdcTooltips = [];

        this.mdcDataTableContainer.querySelectorAll('tr.mdc-data-table__row[data-row-id]').forEach((tableRowElement: HTMLTableRowElement): void => {
            if (tableRowElement.querySelector('#expose-forwarding-' + tableRowElement.dataset.rowId + '-object-expose-mdc-tooltip') !== null) {
                this.mdcTooltips.push(new MDCTooltip(tableRowElement.querySelector('#expose-forwarding-' + tableRowElement.dataset.rowId + '-object-expose-mdc-tooltip')));
            }
        });

        this.mdcDataTableContainer.querySelectorAll('tr.mdc-data-table__row[data-row-id]').forEach((tableRowElement: HTMLTableRowElement): void => {
            if (tableRowElement.querySelector('#expose-forwarding-' + tableRowElement.dataset.rowId + '-city-expose-mdc-tooltip') !== null) {
                this.mdcTooltips.push(new MDCTooltip(tableRowElement.querySelector('#expose-forwarding-' + tableRowElement.dataset.rowId + '-city-expose-mdc-tooltip')));
            }
        });

        this.mdcDataTableContainer.querySelectorAll('tr.mdc-data-table__row[data-row-id]').forEach((tableRowElement: HTMLTableRowElement): void => {
            this.mdcContextMenuComponents.push(new MdcContextMenuComponent('expose_forwarding_' + tableRowElement.dataset.rowId + '_'));
        });
    }

}

export { ExposeForwardingListComponent };
