import { MdcContextMenuComponent } from '../Component/MdcContextMenuComponent';

class HallOfInspirationWidget {

    private readonly hallOfInspirationWidgetContextMenu: MdcContextMenuComponent;

    constructor(idPrefix: string) {
        this.hallOfInspirationWidgetContextMenu = new MdcContextMenuComponent(idPrefix + 'hall_of_inspiration_widget_');
    }

    public static checkContainerExists(idPrefix: string): boolean {
        return document.querySelector('#' + idPrefix + 'hall_of_inspiration_widget') !== null;
    }

}

export { HallOfInspirationWidget };
