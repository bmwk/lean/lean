import { MdcContextMenuComponent } from '../Component/MdcContextMenuComponent';

class PropertySeekerInformationWidget {

    private readonly propertySeekerInformationWidgetContextMenu: MdcContextMenuComponent;

    constructor(idPrefix: string) {
        this.propertySeekerInformationWidgetContextMenu = new MdcContextMenuComponent(idPrefix + 'property_seeker_information_widget_');
    }

    public static checkContainerExists(idPrefix: string): boolean {
        return document.querySelector('#' + idPrefix + 'property_seeker_information_widget') !== null;
    }

}

export { PropertySeekerInformationWidget };
