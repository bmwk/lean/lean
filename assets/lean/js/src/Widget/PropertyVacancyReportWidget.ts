import { MdcContextMenuComponent } from '../Component/MdcContextMenuComponent';

class PropertyVacancyReportWidget {

    private readonly propertyVacancyReportWidgetContextMenu: MdcContextMenuComponent;

    constructor(idPrefix: string) {
        this.propertyVacancyReportWidgetContextMenu = new MdcContextMenuComponent(idPrefix + 'property_vacancy_report_widget_');
    }

    public static checkContainerExists(idPrefix: string): boolean {
        return document.querySelector('#' + idPrefix + 'property_vacancy_report_widget') !== null;
    }

}

export { PropertyVacancyReportWidget };
