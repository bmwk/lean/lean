import { MdcContextMenuComponent } from '../Component/MdcContextMenuComponent';

class PropertySeekerLookingForPropertyRequestsWidget {

    private readonly propertySeekerLookingForPropertyRequestsWidgetContextMenu: MdcContextMenuComponent;

    constructor(idPrefix: string) {
        this.propertySeekerLookingForPropertyRequestsWidgetContextMenu = new MdcContextMenuComponent(idPrefix + 'property_seeker_looking_for_property_requests_widget_');
    }

    public static checkContainerExists(idPrefix: string): boolean {
        return document.querySelector('#' + idPrefix + 'property_seeker_looking_for_property_requests_widget') !== null;
    }

}

export { PropertySeekerLookingForPropertyRequestsWidget };
