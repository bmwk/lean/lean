import { MdcContextMenuComponent } from '../Component/MdcContextMenuComponent';

class LookingForPropertyRequestReportWidget {

    private readonly lookingForPropertyRequestReportWidgetContextMenu: MdcContextMenuComponent;

    constructor(idPrefix: string) {
        this.lookingForPropertyRequestReportWidgetContextMenu = new MdcContextMenuComponent(idPrefix + 'looking_for_property_request_report_widget_');
    }

    public static checkContainerExists(idPrefix: string): boolean {
        return document.querySelector('#' + idPrefix + 'looking_for_property_request_report_widget') !== null;
    }

}

export { LookingForPropertyRequestReportWidget };
