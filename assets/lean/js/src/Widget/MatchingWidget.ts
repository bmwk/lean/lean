import { MdcContextMenuComponent } from '../Component/MdcContextMenuComponent';

class MatchingWidget {

    private readonly matchingWidgetContextMenu: MdcContextMenuComponent;

    constructor(idPrefix: string) {
        this.matchingWidgetContextMenu = new MdcContextMenuComponent(idPrefix + 'matching_widget_');
    }

    public static checkContainerExists(idPrefix: string): boolean {
        return document.querySelector('#' + idPrefix + 'matching_widget') !== null;
    }

}

export { MatchingWidget };
