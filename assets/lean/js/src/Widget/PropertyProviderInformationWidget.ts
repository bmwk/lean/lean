import { MdcContextMenuComponent } from '../Component/MdcContextMenuComponent';

class PropertyProviderInformationWidget {

    private readonly propertyProviderInformationWidgetContextMenu: MdcContextMenuComponent;

    constructor(idPrefix: string) {
        this.propertyProviderInformationWidgetContextMenu = new MdcContextMenuComponent(idPrefix + 'property_provider_information_widget_');
    }

    public static checkContainerExists(idPrefix: string): boolean {
        return document.querySelector('#' + idPrefix + 'property_provider_information_widget') !== null;
    }

}

export { PropertyProviderInformationWidget };
