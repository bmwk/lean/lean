import { MdcContextMenuComponent } from '../Component/MdcContextMenuComponent';

class FollowUpReminderWidget {

    private readonly followUpReminderWidgetContextMenu: MdcContextMenuComponent;

    constructor(idPrefix: string) {
        this.followUpReminderWidgetContextMenu = new MdcContextMenuComponent(idPrefix + 'follow_up_reminder_widget_');
    }

    public static checkContainerExists(idPrefix: string): boolean {
        return document.querySelector('#' + idPrefix + 'follow_up_reminder_widget') !== null;
    }

}

export { FollowUpReminderWidget };
