import { MdcContextMenuComponent } from '../Component/MdcContextMenuComponent';

class LookingForPropertyRequestWidget {

    private readonly lookingForPropertyRequestWidgetContextMenu: MdcContextMenuComponent;

    constructor(idPrefix: string) {
        this.lookingForPropertyRequestWidgetContextMenu = new MdcContextMenuComponent(idPrefix + 'looking_for_property_request_widget_');
    }

    public static checkContainerExists(idPrefix: string): boolean {
        return document.querySelector('#' + idPrefix + 'looking_for_property_request_widget') !== null;
    }

}

export { LookingForPropertyRequestWidget };
