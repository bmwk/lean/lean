import { MdcContextMenuComponent } from '../Component/MdcContextMenuComponent';

class PropertyVacancyManagementWidget {

    private readonly propertyVacancyManagementWidgetContextMenu: MdcContextMenuComponent;

    constructor(idPrefix: string) {
        this.propertyVacancyManagementWidgetContextMenu = new MdcContextMenuComponent(idPrefix + 'property_vacancy_management_widget_');
    }

    public static checkContainerExists(idPrefix: string): boolean {
        return document.querySelector('#' + idPrefix + 'property_vacancy_management_widget') !== null;
    }

}

export { PropertyVacancyManagementWidget };
