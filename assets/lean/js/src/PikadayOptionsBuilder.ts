import { PikadayOptions } from 'pikaday';

class PikadayOptionsBuilder {

    public static build(field: HTMLElement): PikadayOptions {
        return {
            field: field,
            format: 'DD.MM.YYYY',
            i18n: {
            previousMonth: 'Vorheriger Monat',
                nextMonth: 'Nächster Monat',
                months:  ['Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember'],
                weekdays: ['Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag'],
                weekdaysShort: ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa']
            },
            minDate: null
        };
    }

}

export { PikadayOptionsBuilder };
