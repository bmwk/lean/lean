import React from 'react';

function OverviewPopup(): JSX.Element {
    return (
        <div id="overviewDialog" style={{maxHeight: '300px', maxWidth: '300px', overflow: 'auto'}} className="mdc-card">
            <div id="overviewDialog-content"></div>
        </div>
    );
}

export default OverviewPopup;