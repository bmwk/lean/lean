import * as coreui from '@coreui/coreui';
import React, { useEffect, useRef } from 'react';
import Progressbar from './Progressbar';

interface AttractivenessIndicatorTableProps {
    title: string;
    weighting: number;
    indicators: { name: string, value: number, tooltip?: string }[];
}

function AttractivenessIndicatorTable({title, weighting, indicators}: AttractivenessIndicatorTableProps) {
    const tableRef = useRef<HTMLTableElement>();
    useEffect(() => {
        const tooltipTriggerList = tableRef.current.querySelectorAll('[data-coreui-toggle="tooltip"]');
        const coreUiToolTips: any[] = [];
        for (let i = 0; i < tooltipTriggerList.length; i++) {
            coreUiToolTips.push(new coreui.Tooltip(tooltipTriggerList[i]));
        }
        return () => {
            coreUiToolTips.length = 0;
        }
    }, [indicators]);

    return (
        <div>
            <div className="d-flex align-items-center"><span className="fw-bold">{title}</span><small className="ms-auto px-2">Gewichtung {weighting}%</small>
            </div>
            <table ref={tableRef} className="table align-middle">
                <tbody>
                {indicators.map(indicator => (
                    <tr key={indicator.name}>
                        <td>
                            <small>{indicator.name}
                                {indicator.tooltip !== undefined && <a href="#" className="d-inline-block" data-coreui-toggle="tooltip" data-coreui-placement="top"
                                    data-coreui-title={indicator.tooltip}>
                                    <span className="material-icons-outlined md-14 ms-1">info</span>
                                </a>}
                            </small>
                        </td>
                        <td>
                            <div data-coreui-toggle="tooltip" data-coreui-placement="top"
                               data-coreui-title={indicator.value > 0 ? 'Der Indikator-Wert des Gewerbes ist genauso gut oder besser als bei ' + Math.round(indicator.value * 100) + '% der vergleichbaren Gewerbe.'
                               : 'Für diesen Indikator stehen momentan keine Werte zur Verfügung'}>
                            <Progressbar value={indicator.value}/>
                            </div>
                        </td>
                    </tr>
                ))}
                </tbody>
            </table>
        </div>
    );
}

export default AttractivenessIndicatorTable;
