interface FilterOption {

    id: number;
    name: string;

}

export { FilterOption };
