interface Filter {

    propertyStatus: number[];
    propertyUsageStatus: number[];
    propertyUsages: number[];

}

export { Filter };