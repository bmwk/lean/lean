enum AccountUserType {
    INTERNAL = 0,
    PROPERTY_PROVIDER = 1,
    PROPERTY_SEEKER = 2,
    FTP_USER = 3,
}

export { AccountUserType };