import React from 'react';

interface PopupState {
}

interface PopupProps {
}

class Popup extends React.Component<PopupProps, PopupState> {

    private readonly popupContainer: React.RefObject<HTMLDivElement>;

    constructor(props: PopupProps) {
        super(props);

        this.popupContainer = React.createRef();
    }

    public getPopupContainer(): HTMLDivElement {
        return this.popupContainer.current;
    }

    public render() {
        return (
            <div ref={this.popupContainer} className="mdc-card p-2">
                <b id="popup-title" className="text-primary"></b>
                <span id="popup-body"></span>
            </div>
        );
    }

}

export default Popup;
