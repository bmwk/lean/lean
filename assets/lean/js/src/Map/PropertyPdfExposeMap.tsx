import { Polygon } from 'ol/geom';
import { toLonLat } from 'ol/proj';
import React from 'react';
import { Property } from '../MapApi/Property';
import { PropertyApiResponse } from '../Property/PropertyApiResponse';
import { MapFunctionalitiesState } from './MapFunctionalitiesState';
import MapPdfExpose from './MapPdfExpose';
import { MapToolbarOption } from './MapToolbarOption';

interface PropertyMapState {
    mapFunctionalitiesState?: MapFunctionalitiesState;
    mapToolbarOption?: MapToolbarOption;
    properties?: Property[];
    centerPoint?: {
        longitude: number;
        latitude: number;
    };

}

interface PropertyMapProps {
    centerPoint: {
        longitude: number;
        latitude: number;
    };
    buildingUnit: PropertyApiResponse;
    usePrintControl?: boolean;
    useMarker?: boolean;
    forceSearchAddress?: boolean;
    usePrintBtn?: boolean;
}

class PropertyMap extends React.Component<PropertyMapProps, PropertyMapState> {

    private readonly mapContainer: React.RefObject<HTMLDivElement>;
    private readonly mapRefObject: React.RefObject<MapPdfExpose>;

    constructor(props: PropertyMapProps) {
        super(props);

        this.mapRefObject = React.createRef();

        this.state = {
            mapFunctionalitiesState: {
                pinLayerVisibility: true,
                polygonLayerVisibility: true,
                filterVisibility: false,
                translateInteraction: false,
                modifyInteraction: false,
                drawInteraction: false,
                setPinInteraction: false,
                drawBusinessLocationInteraction: false,
                businessLocationLayerVisibility: false,
                measureLayerVisibility: false,
                measureInteraction: false,
                wmsLayerVisibility: false,
                createObjectByClickInMap: false,
                propertyPopup: false,
                businessLocationReadonly: true,
            },
            mapToolbarOption: {
                pinView: false,
                polygonView: false,
                filterVisibility: false,
                drag: true,
                transform: true,
                draw: true,
                setPin: false,
                measure: true,
                wmsLayer: true,
                createObjectByClickInMap: false,
                print: true,
                delete: true,
                save: true,
                businessLocationDraw: false,
                businessLocationView: true,
            },
            properties: [],
            centerPoint: this.props.centerPoint,
        };

        this.mapContainer = React.createRef();
    }

    public componentDidUpdate(prevProps: Readonly<PropertyMapProps>, prevState: Readonly<PropertyMapState>, snapshot?: any): void {
        if (prevState.properties !== this.state.properties){
            if (this.state.properties[0].longitude !== null && this.state.properties[0].latitude !== null) {
                this.setState({centerPoint: {longitude: this.state.properties[0].longitude, latitude: this.state.properties[0].latitude}})
            } else if (this.state.properties[0].polygonCoordinates !== null) {
                const polygonCenter = toLonLat(new Polygon(this.state.properties[0].polygonCoordinates).getInteriorPoint().getCoordinates())
                this.setState({centerPoint: {longitude: polygonCenter[0], latitude: polygonCenter[1]}})
            }
        }
    }

    public render(): JSX.Element {
        const { usePrintControl,
                useMarker,
                forceSearchAddress,
                usePrintBtn
        } = this.props;

        return (
            <MapPdfExpose
                ref={this.mapRefObject}
                buildingUnit={this.props.buildingUnit}
                mapFunctionalitiesState={this.state.mapFunctionalitiesState}
                mapToolbarOption={this.state.mapToolbarOption}
                centerPoint={this.state.centerPoint}
                zoom={19}
                useSavedView={false}
                useFilter={false}
                properties={this.state.properties}
                forwardOnClick={false}
                usePrintControl={usePrintControl}
                useMarker={useMarker}
                forceSearchAddress={forceSearchAddress}
                usePrintBtn={usePrintBtn}
            />
        );
    }

}

export default PropertyMap;
