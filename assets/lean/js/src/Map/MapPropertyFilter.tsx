import * as coreui from '@coreui/coreui';
import { MDCSelect } from '@material/select';
import { MDCTextField } from '@material/textfield';
import React, { ChangeEvent } from 'react';
import { MapApi, PropertyMapping } from '../MapApi/MapApi';
import { Property, ScoredProperty } from '../MapApi/Property';
import { NaceClassification } from '../Property/PropertyApiResponse';
import { FilterOption } from './FilterOption';
import { PropertyFilter } from './PropertyFilter';
import PropertyUsageFilter from './PropertyUsageFilter';

interface AccountUser {
    id: number;
    fullName: string;
}

interface MapFilterState {
    propertyFilter: PropertyFilter;
    propertyUsagesFirstLevel: FilterOption[];
    propertyUsagesSecondLevel: NaceClassification[];
    accountUsers: AccountUser[];
}

interface MapFilterProps {
    handleClose: Function;
    properties: Property[] | ScoredProperty[];
    filterCanvasVisible: boolean;
    isEarlyWarningSystem: boolean;
    selectedPolygonId: number;
    showAccountUserFilter: boolean;
}

enum PropertyFilterType {
    All = 0,
    PropertyStatus = 1,
    PropertyUsageStatus = 2,
    PropertyUsage = 3,
    PropertyOfferType = 4,
    BarrierFreeAccessType = 5,
}

class MapPropertyFilter extends React.Component<MapFilterProps, MapFilterState> {

    private readonly mapApi: MapApi;
    private readonly offcanvasRefObject: React.RefObject<HTMLDivElement>;
    private readonly propertyMinSpaceRef: React.RefObject<HTMLLabelElement>;
    private readonly propertyMaxSpaceRef: React.RefObject<HTMLLabelElement>;
    private readonly shopWindowFrontWidthMinRef: React.RefObject<HTMLLabelElement>;
    private readonly shopWindowFrontWidthMaxRef: React.RefObject<HTMLLabelElement>;
    private readonly barrierFreeAccessTypeRef: React.RefObject<HTMLDivElement>;
    private readonly assignedAccountUserRef: React.RefObject<HTMLDivElement>;
    private barrierFreeAccessTypeMdcSelect: MDCSelect;
    private assignedAccountUserMdcSelect: MDCSelect;
    private propertyStatusCollapseRef: React.RefObject<HTMLDivElement>;
    private propertyUsageCollapseRef: React.RefObject<HTMLDivElement>;
    private propertyOfferTypeCollapseRef: React.RefObject<HTMLDivElement>;
    private propertySpaceCollapseRef: React.RefObject<HTMLDivElement>;
    private propertyShopWindowWidthCollapseRef: React.RefObject<HTMLDivElement>;
    private propertyBarrierFreeAccessCollapseRef: React.RefObject<HTMLDivElement>;
    private propertyAccountUserCollapseRef: React.RefObject<HTMLDivElement>;
    private propertyLastUpdatedAtCollapseRef: React.RefObject<HTMLDivElement>;

    private readonly propertyStatus: FilterOption[] = [
        {id: 0, name: 'Leerstand'},
        {id: 1, name: 'drohender Leerstand'},
        {id: 4, name: 'Nachnutzung vereinbart'},
        {id: 2, name: 'Bestandsobjekt'},
        {id: 3, name: 'Ankerobjekt'},
    ];

    private readonly propertyUsageStatus: FilterOption[] = [
        {id: 0, name: 'aktuelle'},
        {id: 1, name: 'ehemalige'},
        {id: 2, name: 'mögliche'},
    ];

    private readonly propertyOfferType: FilterOption[] = [
        {id: 0, name: 'zum Kauf'},
        {id: 1, name: 'zur Miete'},
        {id: 2, name: 'zur Pacht'},
    ];

    private readonly barrierFreeAccessTypes: FilterOption[] = [
        {id: 0, name: 'ist vorhanden'},
        {id: 1, name: 'kann gewährleistet werden'},
        {id: 2, name: 'ist derzeit nicht vorhanden'},
        {id: 3, name: 'ist nicht möglich'},
    ];

    constructor(props: MapFilterProps) {
        super(props);
        this.offcanvasRefObject = React.createRef();
        this.propertyMinSpaceRef = React.createRef();
        this.propertyMaxSpaceRef = React.createRef();
        this.shopWindowFrontWidthMaxRef = React.createRef();
        this.shopWindowFrontWidthMinRef = React.createRef();
        this.barrierFreeAccessTypeRef = React.createRef();
        this.assignedAccountUserRef = React.createRef();
        this.state = {
            propertyUsagesFirstLevel: [],
            propertyUsagesSecondLevel: [],
            accountUsers: [],
            propertyFilter: {
                propertyOfferTypes: [],
                propertyUsages: [],
                propertyStatus: [],
                propertyUsageStatus: [],
                propertyMaxSpace: '',
                propertyMinSpace: '',
                shopWindowFrontWidthMax: '',
                shopWindowFrontWidthMin: '',
                barrierFreeAccessTypes: [],
                assignedAccountUser: null,
                lastUpdatedFrom: '',
                lastUpdatedTill: '',
            }
        };
        this.mapApi = MapApi.getInstance();
    }

    public componentDidMount(): void {

        this.propertyStatusCollapseRef = React.createRef();
        this.propertyUsageCollapseRef = React.createRef();
        this.propertyOfferTypeCollapseRef = React.createRef();
        this.propertySpaceCollapseRef = React.createRef();
        this.propertyShopWindowWidthCollapseRef = React.createRef();
        this.propertyBarrierFreeAccessCollapseRef = React.createRef();
        this.propertyAccountUserCollapseRef = React.createRef();
        this.propertyLastUpdatedAtCollapseRef = React.createRef();

        this.offcanvasRefObject.current.addEventListener('hide.coreui.offcanvas', () => {
            this.props.handleClose();
        });

        const propertyFilter = this.initializePropertyFilter();
        this.setState({propertyFilter: propertyFilter});

        if (this.props.isEarlyWarningSystem === false) {
            new MDCTextField(this.propertyMinSpaceRef.current);
            new MDCTextField(this.propertyMaxSpaceRef.current);
            new MDCTextField(this.shopWindowFrontWidthMaxRef.current);
            new MDCTextField(this.shopWindowFrontWidthMinRef.current);

            this.barrierFreeAccessTypeMdcSelect = new MDCSelect(this.barrierFreeAccessTypeRef.current);
            this.barrierFreeAccessTypeMdcSelect.listen('MDCSelect:change', () => this.handleBarrierFreeAccessFilterChange(parseInt(this.barrierFreeAccessTypeMdcSelect.value)));

            const barrierFreeAccessSelectClearButton = document.getElementById('propertyBarrierFreeAccessClearButton');
            barrierFreeAccessSelectClearButton.addEventListener('click', (e) => {
                e.stopPropagation();
                this.setState({propertyFilter: {...this.state.propertyFilter, barrierFreeAccessTypes: []}});
                this.barrierFreeAccessTypeMdcSelect.setSelectedIndex(-1, true);
            });
            if (propertyFilter.barrierFreeAccessTypes.length > 0) {
                this.barrierFreeAccessTypeMdcSelect.setSelectedIndex(this.barrierFreeAccessTypes.indexOf(this.barrierFreeAccessTypes.find(type => type.id === propertyFilter.barrierFreeAccessTypes[0])));
            }

            if (this.props.showAccountUserFilter) {
                const assignedUserSelectClearButton = document.getElementById('assignedUserSelectClearButton');
                assignedUserSelectClearButton.addEventListener('click', (e) => {
                    e.stopPropagation();
                    this.setState({propertyFilter: {...this.state.propertyFilter, assignedAccountUser: null}});
                    this.assignedAccountUserMdcSelect.setSelectedIndex(-1, true);
                });
            }

            if (this.props.showAccountUserFilter === true) {
                MapApi.fetchAccountUsers().then((accountUsers: AccountUser[]): void => {
                    this.setState({accountUsers: accountUsers});

                    this.assignedAccountUserMdcSelect = new MDCSelect(this.assignedAccountUserRef.current);

                    if (propertyFilter.assignedAccountUser !== null) {
                        this.assignedAccountUserMdcSelect.setSelectedIndex(accountUsers.indexOf(accountUsers.find(user => user.id === propertyFilter.assignedAccountUser)));
                    }

                    this.assignedAccountUserMdcSelect.listen('MDCSelect:change', () => this.handleAssignedAccountUserFilterChange(parseInt(this.assignedAccountUserMdcSelect.value)));
                }).catch((e) => {
                });
            }
        }

        MapApi.fetchFirstLevelIndustryClassificationsFromApi().then((firstLevelIndustryClassifications: NaceClassification[]) => {
            this.setState({
                propertyUsagesFirstLevel: firstLevelIndustryClassifications.map((classification: NaceClassification) => {
                    return {id: classification.levelOne, name: classification.name};
                }),
            });

            MapApi.fetchSecondLevelIndustryClassificationsFromApi().then((value: Array<NaceClassification>) => {
                this.setState({
                    propertyUsagesSecondLevel: value
                });
            });
        });
    }

    componentDidUpdate(prevProps: Readonly<MapFilterProps>, prevState: Readonly<MapFilterState>, snapshot?: any): void {
        const offcanvasInstance = coreui.Offcanvas.getOrCreateInstance(this.offcanvasRefObject.current);
        if (this.props.filterCanvasVisible) {
            offcanvasInstance.show();
        } else {
            offcanvasInstance.hide();
        }

        if (prevState.propertyFilter !== this.state.propertyFilter) {
            localStorage.setItem('lean.map.propertyFilter', JSON.stringify(this.state.propertyFilter));
            if (this.props.isEarlyWarningSystem === false) {
                if (this.state.propertyFilter.propertyStatus.length > 0){
                    this.propertyStatusCollapseRef.current.classList.add('show')
                }
                if (this.state.propertyFilter.propertyUsages.length > 0){
                    this.propertyUsageCollapseRef.current.classList.add('show')
                }
                if (this.state.propertyFilter.propertyOfferTypes.length > 0){
                    this.propertyOfferTypeCollapseRef.current.classList.add('show')
                }
                if (this.state.propertyFilter.propertyMaxSpace !== '' || this.state.propertyFilter.propertyMinSpace !== ''){
                    this.propertySpaceCollapseRef.current.classList.add('show')
                }
                if (this.state.propertyFilter.shopWindowFrontWidthMin !== '' || this.state.propertyFilter.shopWindowFrontWidthMax !== ''){
                    this.propertyShopWindowWidthCollapseRef.current.classList.add('show')
                }
                if (this.state.propertyFilter.barrierFreeAccessTypes.length > 0){
                    this.propertyBarrierFreeAccessCollapseRef.current.classList.add('show')
                }
                if (this.state.propertyFilter.assignedAccountUser !== null){
                    this.propertyAccountUserCollapseRef.current.classList.add('show')
                }
                if (this.state.propertyFilter.lastUpdatedFrom !== '' || this.state.propertyFilter.lastUpdatedTill !== ''){
                    this.propertyLastUpdatedAtCollapseRef.current.classList.add('show')
                }
            }
        }
        if (this.props.properties.length > 0 && this.state.propertyFilter) {
            this.filterProperties();
        }
    }

    public render(): JSX.Element {
        return (
            <div className="offcanvas offcanvas-end position-absolute"
                 style={{zIndex: 11}} ref={this.offcanvasRefObject}
                 data-coreui-scroll="true" data-coreui-backdrop="false" tabIndex={-1} id="map_filter_offcanvas"
                 aria-labelledby="map_filter_offcanvas_label">
                <div className="offcanvas-header">
                    <h4 className="offcanvas-title" id="map_filter_offcanvas_label">Filter</h4>
                    <button type="button" className="btn-close text-reset" onClick={() => this.props.handleClose()}
                            aria-label="Close"/>
                </div>
                <div className="offcanvas-body d-flex flex-column accordion">
                    {(this.props.isEarlyWarningSystem === undefined || this.props.isEarlyWarningSystem === false) && (<div className="row">
                        <div className="col-12 d-flex flex-column">
                            <button className="accordion-button py-3 px-0" data-coreui-toggle="collapse" data-coreui-target="#propertyStatusFilter">
                                <span className="fs-5">Objektstatus</span>
                            </button>
                            <div ref={this.propertyStatusCollapseRef} id="propertyStatusFilter" className="collapse show">
                                <div className="d-flex flex-column mb-2">
                                    <button className="mdc-button me-auto" onClick={() => this.selectAll(PropertyFilterType.PropertyStatus)}>
                                        <span className="mdc-button__ripple"/>
                                        <span className="mdc-button__focus-ring"/>
                                        <small className="mdc-button__label">Alle an/abwählen</small>
                                    </button>
                                    {this.propertyStatus?.map((propertyStatus: FilterOption) =>
                                        <div key={propertyStatus.id} className="mdc-form-field">
                                            <div className="mdc-checkbox">
                                                <input
                                                    onChange={(event: ChangeEvent<HTMLInputElement>): void => this.handlePropertyStatusFilterChange(propertyStatus.id, event)}
                                                    type="checkbox"
                                                    checked={this.state.propertyFilter.propertyStatus.includes(propertyStatus.id)}
                                                    className="mdc-checkbox__native-control"/>
                                                <div className="mdc-checkbox__background">
                                                    <svg className="mdc-checkbox__checkmark" viewBox="0 0 24 24">
                                                        <path className="mdc-checkbox__checkmark-path" fill="none"
                                                              d="M1.73,12.91 8.1,19.28 22.79,4.59"/>
                                                    </svg>
                                                    <div className="mdc-checkbox__mixedmark"/>
                                                </div>
                                            </div>
                                            <span className={'usage-color-indicator'}
                                                  style={{backgroundColor: this.getColorByObjectStatusId(propertyStatus.id)}}/>
                                            <label htmlFor="">{propertyStatus.name}</label>
                                        </div>
                                    )}
                                </div>
                            </div>
                        </div>
                    </div>)}
                    <div className="row">
                        <div className="col-12 d-flex flex-column">
                            <button className="accordion-button collapsed py-3 px-0" data-coreui-toggle="collapse" data-coreui-target="#propertyUsageFilter">
                                <span className="fs-5">Nutzung </span>
                            </button>
                            <div ref={this.propertyUsageCollapseRef} id="propertyUsageFilter" className={'collapse' + (this.props.isEarlyWarningSystem ? ' show' : '')}>
                                <div className="d-flex flex-column mb-2">
                                    <button className="mdc-button me-auto" onClick={() => this.selectAll(PropertyFilterType.PropertyUsage)}>
                                        <span className="mdc-button__ripple"/>
                                        <span className="mdc-button__focus-ring"/>
                                        <small className="mdc-button__label">Alle an/abwählen</small>
                                    </button>
                                    {(this.props.isEarlyWarningSystem === undefined || this.props.isEarlyWarningSystem === false) &&
                                    <div className="d-flex border-bottom">
                                        {this.propertyUsageStatus.map((propertyUsageStatus: FilterOption) =>
                                            <div key={propertyUsageStatus.id} className="mdc-form-field">
                                                <div className="mdc-checkbox">
                                                    <input
                                                        onChange={(event: ChangeEvent<HTMLInputElement>): void => this.handlePropertyUsageStatusFilterChange(propertyUsageStatus.id, event)}
                                                        type="checkbox"
                                                        checked={this.state.propertyFilter.propertyUsageStatus.includes(propertyUsageStatus.id)}
                                                        className="mdc-checkbox__native-control"/>
                                                    <div className="mdc-checkbox__background">
                                                        <svg className="mdc-checkbox__checkmark" viewBox="0 0 24 24">
                                                            <path className="mdc-checkbox__checkmark-path" fill="none"
                                                                  d="M1.73,12.91 8.1,19.28 22.79,4.59"/>
                                                        </svg>
                                                        <div className="mdc-checkbox__mixedmark"/>
                                                    </div>
                                                </div>
                                                <label>{propertyUsageStatus.name}</label>
                                            </div>
                                        )}
                                    </div>
                                    }
                                    {this.state.propertyUsagesFirstLevel?.map((propertyUsage: FilterOption) =>
                                        <PropertyUsageFilter
                                            checked={this.state.propertyFilter.propertyUsages.some((usage) => usage.levelOne === propertyUsage.id)}
                                            name={propertyUsage.name} usageIconSrc={this.mapApi.getPropertyUsageIconById(propertyUsage.id)}
                                            levelOne={propertyUsage.id} handleLevelOneChange={this.handlePropertyUsagesFilterChange}
                                            handleLevelTwoChange={this.handlePropertyUsagesLevelTwoFilterChange}
                                            levelTwo={this.state.propertyUsagesSecondLevel?.filter(nace => nace.levelOne === propertyUsage.id)}
                                            selectedIndex={this.state.propertyUsagesSecondLevel?.filter(nace => nace.levelOne === propertyUsage.id).findIndex((usage) => usage.levelTwo === this.state.propertyFilter.propertyUsages.find((usageFilter) => usageFilter.levelOne === usage.levelOne)?.levelTwo)}
                                            key={propertyUsage.id}/>
                                    )}
                                </div>
                            </div>
                        </div>
                    </div>
                    {(this.props.isEarlyWarningSystem === undefined || this.props.isEarlyWarningSystem === false) &&
                    <div className="row">
                        <div className="col-12 d-flex flex-column">
                            <button className="accordion-button collapsed py-3 px-0" data-coreui-toggle="collapse"
                                    data-coreui-target="#propertyOfferTypeFilter">
                                <span className="fs-5">Angebotsart </span>
                            </button>
                            <div ref={this.propertyOfferTypeCollapseRef} id="propertyOfferTypeFilter" className={'collapse' + (this.state.propertyFilter.propertyOfferTypes?.length > 0 ? ' show' : '') }>
                                <div className="d-flex flex-column mb-2">
                                    <button className="mdc-button me-auto" onClick={() => this.selectAll(PropertyFilterType.PropertyOfferType)}>
                                        <span className="mdc-button__ripple"/>
                                        <span className="mdc-button__focus-ring"/>
                                        <small className="mdc-button__label">Alle an/abwählen</small>
                                    </button>
                                    {this.propertyOfferType?.map((propertyOfferType: FilterOption) =>
                                        <div key={propertyOfferType.id} className="mdc-form-field">
                                            <div className="mdc-checkbox">
                                                <input
                                                    onChange={(event: ChangeEvent<HTMLInputElement>): void => this.handlePropertyOfferTypeFilterChange(propertyOfferType.id, event)}
                                                    type="checkbox"
                                                    checked={this.state.propertyFilter.propertyOfferTypes?.includes(propertyOfferType.id)}
                                                    className="mdc-checkbox__native-control"/>
                                                <div className="mdc-checkbox__background">
                                                    <svg className="mdc-checkbox__checkmark" viewBox="0 0 24 24">
                                                        <path className="mdc-checkbox__checkmark-path" fill="none"
                                                              d="M1.73,12.91 8.1,19.28 22.79,4.59"/>
                                                    </svg>
                                                    <div className="mdc-checkbox__mixedmark"/>
                                                </div>
                                            </div>
                                            <label>{propertyOfferType.name}</label>
                                        </div>
                                    )}
                                </div>
                            </div>
                        </div>
                    </div>
                    }
                    {(this.props.isEarlyWarningSystem === undefined || this.props.isEarlyWarningSystem === false) &&
                    <div className="row">
                        <div className="col-12 d-flex flex-column">
                            <button className="accordion-button collapsed py-3 px-0" data-coreui-toggle="collapse"
                                    data-coreui-target="#propertySpaceFilter">
                                <span className="fs-5">Fläche</span>
                            </button>
                            <div ref={this.propertySpaceCollapseRef} id="propertySpaceFilter" className="collapse">
                                <div className="d-flex gap-2 mt-1 mb-2">
                                    <label ref={this.propertyMinSpaceRef} className="mdc-text-field mdc-text-field--filled">
                                        <span className="mdc-text-field__ripple"></span>
                                        <span
                                            className={'mdc-floating-label' + (this.state.propertyFilter.propertyMinSpace !== '' ? ' mdc-floating-label--float-above' : '')}
                                            id="propertySpaceMin">Gesamtfläche von</span>
                                        <input className="mdc-text-field__input" type="number" aria-labelledby="propertySpaceMin"
                                               value={this.state.propertyFilter.propertyMinSpace}
                                               onChange={event => this.setState({propertyFilter: {...this.state.propertyFilter, propertyMinSpace: event.target.value}})}/>
                                        <span className="mdc-line-ripple"></span>
                                    </label>
                                    <label ref={this.propertyMaxSpaceRef} className="mdc-text-field mdc-text-field--filled">
                                        <span className="mdc-text-field__ripple"></span>
                                        <span
                                            className={'mdc-floating-label' + (this.state.propertyFilter.propertyMaxSpace !== '' ? ' mdc-floating-label--float-above' : '')}
                                            id="propertySpaceMax">Gesamtfläche bis</span>
                                        <input className="mdc-text-field__input" type="number" aria-labelledby="propertySpaceMax"
                                               value={this.state.propertyFilter.propertyMaxSpace}
                                               onChange={event => this.setState({propertyFilter: {...this.state.propertyFilter, propertyMaxSpace: event.target.value}})}/>
                                        <span className="mdc-line-ripple"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    }
                    {(this.props.isEarlyWarningSystem === undefined || this.props.isEarlyWarningSystem === false) &&
                    <div className="row">
                        <div className="col-12 d-flex flex-column">
                            <button className="accordion-button collapsed py-3 px-0" data-coreui-toggle="collapse"
                                    data-coreui-target="#propertyShopWindowFrontWidthFilter">
                                <span className="fs-5">Schaufensterbreite</span>
                            </button>
                            <div ref={this.propertyShopWindowWidthCollapseRef} id="propertyShopWindowFrontWidthFilter" className="collapse">
                                <div className="d-flex gap-2 mt-1 mb-2">
                                    <label ref={this.shopWindowFrontWidthMinRef} className="mdc-text-field mdc-text-field--filled">
                                        <span className="mdc-text-field__ripple"></span>
                                        <span
                                            className={'mdc-floating-label' + (this.state.propertyFilter.shopWindowFrontWidthMin !== '' ? ' mdc-floating-label--float-above' : '')}
                                            id="propertySpaceMin">Breite von</span>
                                        <input className="mdc-text-field__input" type="number" aria-labelledby="propertySpaceMin"
                                               value={this.state.propertyFilter.shopWindowFrontWidthMin}
                                               onChange={event => this.setState({propertyFilter: {...this.state.propertyFilter, shopWindowFrontWidthMin: event.target.value}})}/>
                                        <span className="mdc-line-ripple"></span>
                                    </label>
                                    <label ref={this.shopWindowFrontWidthMaxRef} className="mdc-text-field mdc-text-field--filled">
                                        <span className="mdc-text-field__ripple"></span>
                                        <span
                                            className={'mdc-floating-label' + (this.state.propertyFilter.shopWindowFrontWidthMax !== '' ? ' mdc-floating-label--float-above' : '')}
                                            id="propertySpaceMax">Breite bis</span>
                                        <input className="mdc-text-field__input" type="number" aria-labelledby="propertySpaceMax"
                                               value={this.state.propertyFilter.shopWindowFrontWidthMax}
                                               onChange={event => this.setState({propertyFilter: {...this.state.propertyFilter, shopWindowFrontWidthMax: event.target.value}})}/>
                                        <span className="mdc-line-ripple"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    }
                    {(this.props.isEarlyWarningSystem === undefined || this.props.isEarlyWarningSystem === false) &&
                    <>
                        <div className="row">
                            <div className="col-12 d-flex flex-column">
                                <button className="accordion-button collapsed py-3 px-0" data-coreui-toggle="collapse"
                                        data-coreui-target="#propertyBarrierFreeFilter">
                                    <span className="fs-5">Barrierefreiheit</span>
                                </button>
                                <div ref={this.propertyBarrierFreeAccessCollapseRef} id="propertyBarrierFreeFilter" className="collapse">
                                    <div className="d-flex flex-column mb-2">
                                        <div ref={this.barrierFreeAccessTypeRef} id="propertyBarrierFreeFilterSelect"
                                             className="mdc-select mdc-select--filled full-width-class mt-1">
                                            <div className="mdc-select__anchor" role="button">
                                                <span className="mdc-select__ripple"/>
                                                <span id="propertyBarrierFreeFilterLabel" className="mdc-floating-label">Barrierefreiheit</span>
                                                <span className="mdc-select__selected-text-container">
                                                    <span id="propertyBarrierFreeFilterSelect-selected-text" className="mdc-select__selected-text"/>
                                                </span>
                                                <i className="material-icons mdc-text-field__icon mdc-text-field__icon--leading clear-icon small"
                                                   id="propertyBarrierFreeAccessClearButton" tabIndex={0}>clear</i>
                                                <span className="mdc-select__dropdown-icon">
                                                    <svg className="mdc-select__dropdown-icon-graphic" viewBox="7 10 10 5" focusable="false">
                                                        <polygon className="mdc-select__dropdown-icon-inactive" stroke="none" fillRule="evenodd"
                                                                 points="7 10 12 15 17 10"/>
                                                        <polygon className="mdc-select__dropdown-icon-active" stroke="none" fillRule="evenodd"
                                                                 points="7 15 12 10 17 15"/>
                                                    </svg>
                                                </span>
                                                <span className="mdc-line-ripple"/>
                                            </div>
                                            <div className="mdc-select__menu mdc-menu mdc-menu-surface mdc-menu-surface--fullwidth">
                                                <ul className="mdc-list" role="listbox" aria-label="picker listbox">
                                                    {this.barrierFreeAccessTypes?.map((barrierFreeAccessType: FilterOption) =>
                                                        <li className={'mdc-list-item' + (this.state.propertyFilter.barrierFreeAccessTypes.includes(barrierFreeAccessType.id) ? ' mdc-list-item--selected' : '')}
                                                            data-value={barrierFreeAccessType.id} role="option" key={barrierFreeAccessType.id}>
                                                            <span className="mdc-list-item__ripple"/>
                                                            <span className="mdc-list-item__text">{barrierFreeAccessType.name}</span>
                                                        </li>
                                                    )}
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {this.props.showAccountUserFilter && <div className="row">
                            <div className="col-12 d-flex flex-column">
                                <button className="accordion-button collapsed py-3 px-0" data-coreui-toggle="collapse"
                                        data-coreui-target="#propertyAssignedUserFilter">
                                    <span className="fs-5">Sachbearbeiter</span>
                                </button>
                                <div ref={this.propertyAccountUserCollapseRef} id="propertyAssignedUserFilter" className="collapse">
                                    <div className="d-flex flex-column mb-2">
                                        <div ref={this.assignedAccountUserRef} id="propertyAssignedUserFilterSelect"
                                             className="mdc-select mdc-select--filled full-width-class mt-1">
                                            <div className="mdc-select__anchor" role="button">
                                                <span className="mdc-select__ripple"/>
                                                <span id="propertyBarrierFreeFilterLabel" className="mdc-floating-label">Sachbearbeiter</span>
                                                <span className="mdc-select__selected-text-container">
                                                <span id="propertyBarrierFreeFilterSelect-selected-text" className="mdc-select__selected-text"/>
                                            </span>
                                                <i className="material-icons mdc-text-field__icon mdc-text-field__icon--leading clear-icon small"
                                                   id="assignedUserSelectClearButton" tabIndex={0}>clear</i>
                                                <span className="mdc-select__dropdown-icon">
                                                <svg className="mdc-select__dropdown-icon-graphic" viewBox="7 10 10 5" focusable="false">
                                                    <polygon className="mdc-select__dropdown-icon-inactive" stroke="none" fillRule="evenodd"
                                                             points="7 10 12 15 17 10"/>
                                                    <polygon className="mdc-select__dropdown-icon-active" stroke="none" fillRule="evenodd"
                                                             points="7 15 12 10 17 15"/>
                                                </svg>
                                            </span>
                                                <span className="mdc-line-ripple"/>
                                            </div>
                                            <div className="mdc-select__menu mdc-menu mdc-menu-surface mdc-menu-surface--fullwidth">
                                                <ul className="mdc-list" role="listbox" aria-label="picker listbox">
                                                    {this.state.accountUsers?.map((assignedUser) =>
                                                        <li className={'mdc-list-item' + (this.state.propertyFilter.assignedAccountUser === assignedUser.id ? ' mdc-list-item--selected' : '')}
                                                            data-value={assignedUser.id} role="option" key={assignedUser.id}>
                                                            <span className="mdc-list-item__ripple"/>
                                                            <span className="mdc-list-item__text">{assignedUser.fullName}</span>
                                                        </li>
                                                    )}
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>}
                        <div className="row">
                            <div className="col-12 d-flex flex-column">
                                <button className="accordion-button collapsed py-3 px-0" data-coreui-toggle="collapse"
                                        data-coreui-target="#propertyUpdatedAtFilter">
                                    <span className="fs-5">Bearbeitungszeitraum</span>
                                </button>
                                <div ref={this.propertyLastUpdatedAtCollapseRef} id="propertyUpdatedAtFilter" className="collapse mt-1">
                                    <div className="d-flex flex-column mb-2">
                                        <label htmlFor="lastUpdatedFrom">Von:</label>
                                        <input className="datepicker" type="date" name="lastUpdatedFrom" value={this.state.propertyFilter.lastUpdatedFrom}
                                               max={new Date().toISOString().split('T')[0]}
                                               onChange={(event) => this.handleLastUpdatedFromChange(event.target.value)}/>
                                    </div>
                                    <div className="d-flex flex-column mb-2">
                                        <label htmlFor="lastUpdatedTill">Bis:</label>
                                        <input className="datepicker" type="date" name="lastUpdatedTill" value={this.state.propertyFilter.lastUpdatedTill}
                                               max={new Date().toISOString().split('T')[0]}
                                               onChange={(event) => this.handleLastUpdatedTillChange(event.target.value)}/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </>
                    }
                </div>
            </div>
        );
    }

    private handlePropertyStatusFilterChange = (id: number, event: ChangeEvent<HTMLInputElement>): void => {
        let propertyStatus = this.state.propertyFilter.propertyStatus.slice();

        if (event.target.checked === true) {
            MapPropertyFilter.addToFilter(propertyStatus, id);
        } else {
            MapPropertyFilter.removeFromFilter(propertyStatus, id);
        }

        this.setState({
            propertyFilter: {...this.state.propertyFilter, propertyStatus: propertyStatus}
        });
    };

    private handlePropertyUsageStatusFilterChange = (id: number, event: ChangeEvent<HTMLInputElement>): void => {
        let propertyUsageStatus = this.state.propertyFilter.propertyUsageStatus.slice();

        if (event.target.checked === true) {
            MapPropertyFilter.addToFilter(propertyUsageStatus, id);
        } else {
            MapPropertyFilter.removeFromFilter(propertyUsageStatus, id);
        }

        this.setState({
            propertyFilter: {...this.state.propertyFilter, propertyUsageStatus: propertyUsageStatus}
        });
    };

    private handlePropertyUsagesFilterChange = (id: number, event: ChangeEvent<HTMLInputElement>): void => {
        let propertyUsages = this.state.propertyFilter.propertyUsages.slice();

        if (event.target.checked === true) {
            if (propertyUsages.some(usage => usage.levelOne === id) === false) {
                propertyUsages.push({levelOne: id, levelTwo: null});
            }
        } else {
            propertyUsages.splice(propertyUsages.indexOf(propertyUsages.find(usage => usage.levelOne === id)), 1);
        }

        this.setState({
            propertyFilter: {...this.state.propertyFilter, propertyUsages: propertyUsages}
        });
    };

    private handlePropertyUsagesLevelTwoFilterChange = (levelOne: number, levelTwo: number): void => {
        const propertyUsages = this.state.propertyFilter.propertyUsages.slice();
        const index = propertyUsages.findIndex((usage) => usage.levelOne === levelOne);
        propertyUsages[index].levelTwo = levelTwo;
        this.setState({
            propertyFilter: {...this.state.propertyFilter, propertyUsages: propertyUsages}
        });
    };

    private handlePropertyOfferTypeFilterChange = (id: number, event: ChangeEvent<HTMLInputElement>): void => {
        let propertyOfferTypes = this.state.propertyFilter.propertyOfferTypes?.slice();
        if (propertyOfferTypes === undefined) {
            propertyOfferTypes = [];
        }

        if (event.target.checked === true) {
            MapPropertyFilter.addToFilter(propertyOfferTypes, id);
        } else {
            MapPropertyFilter.removeFromFilter(propertyOfferTypes, id);
        }

        this.setState({
            propertyFilter: {...this.state.propertyFilter, propertyOfferTypes: propertyOfferTypes}
        });
    };

    private handleBarrierFreeAccessFilterChange = (id: number): void => {
        this.setState({
            propertyFilter: {...this.state.propertyFilter, barrierFreeAccessTypes: [id]}
        });
    };

    private handleAssignedAccountUserFilterChange(id: number): void {
        this.setState({
            propertyFilter: {...this.state.propertyFilter, assignedAccountUser: id}
        });
    }

    private handleLastUpdatedFromChange(date: string): void {
        this.setState({
            propertyFilter: {...this.state.propertyFilter, lastUpdatedFrom: date}
        });
    }

    private handleLastUpdatedTillChange(date: string): void {
        this.setState({
            propertyFilter: {...this.state.propertyFilter, lastUpdatedTill: date}
        });
    }

    private static addToFilter = (filter: number[], id: number): void => {
        if (filter.indexOf(id) === -1) {
            filter.push(id);
        }
    };

    private static removeFromFilter = (filter: number[], id: number): void => {
        if (filter.indexOf(id) !== -1) {
            filter.splice(filter.indexOf(id), 1);
        }
    };

    private selectAll = (propertyFilterType: PropertyFilterType) => {
        let newPropertyStatus: number[] = [];
        let newPropertyUsage: { levelOne: number, levelTwo: number }[] = [];
        let newPropertyOfferType: number[] = [];
        let newBarrierFreeAccessType: number[] = [];

        switch (propertyFilterType) {
            case PropertyFilterType.All:
                if (this.state.propertyFilter.propertyUsages.length === 0 && this.state.propertyFilter.propertyStatus.length === 0) {
                    newPropertyUsage = this.state.propertyUsagesFirstLevel.map(usage => ({levelOne: usage.id, levelTwo: null}));
                    newPropertyStatus = this.propertyStatus.map(status => status.id);
                }
                this.setState({
                    propertyFilter: {
                        ...this.state.propertyFilter,
                        propertyStatus: newPropertyStatus,
                        propertyUsages: newPropertyUsage,
                    }
                });
                break;
            case PropertyFilterType.PropertyUsage:
                if (this.state.propertyFilter.propertyUsages.length === 0) {
                    newPropertyUsage = this.state.propertyUsagesFirstLevel.map(usage => ({levelOne: usage.id, levelTwo: null}));
                }
                this.setState({
                    propertyFilter: {
                        ...this.state.propertyFilter,
                        propertyUsages: newPropertyUsage,
                    }
                });
                break;
            case PropertyFilterType.PropertyStatus:
                if (this.state.propertyFilter.propertyStatus.length === 0) {
                    newPropertyStatus = this.propertyStatus.map(status => status.id);
                }
                this.setState({
                    propertyFilter: {
                        ...this.state.propertyFilter,
                        propertyStatus: newPropertyStatus,
                    }
                });
                break;
            case PropertyFilterType.PropertyOfferType:
                if (this.state.propertyFilter.propertyOfferTypes.length === 0) {
                    newPropertyOfferType = this.propertyOfferType.map(offerType => offerType.id);
                }
                this.setState({
                    propertyFilter: {
                        ...this.state.propertyFilter,
                        propertyOfferTypes: newPropertyOfferType,
                    }
                });
                break;
            case PropertyFilterType.BarrierFreeAccessType:
                if (this.state.propertyFilter.barrierFreeAccessTypes.length === 0) {
                    newBarrierFreeAccessType = this.barrierFreeAccessTypes.map(barrierFreeAccessType => barrierFreeAccessType.id);
                }
                this.setState({
                    propertyFilter: {
                        ...this.state.propertyFilter,
                        barrierFreeAccessTypes: newBarrierFreeAccessType,
                    }
                });
                break;
        }
    };

    private filterProperties = (): void => {
        const propertyMappingsToAdd: PropertyMapping[] = this.mapApi.getPropertyMappings().filter((propertyMapping: PropertyMapping) => {
            if (this.props.isEarlyWarningSystem === undefined || this.props.isEarlyWarningSystem === false) {
                return this.filterUsageStatus(propertyMapping.property as Property) && this.filterPropertyStatus(propertyMapping.property as Property) && this.filterPropertyOfferType(propertyMapping.property as Property) && this.filterAreaSize(propertyMapping.property as Property) && this.filterShopWindowWidth(propertyMapping.property as Property) && this.filterBarrierFreeAccessType(propertyMapping.property as Property) && this.filterAssignedAccountUser(propertyMapping.property as Property) && this.filterLastUpdated(propertyMapping.property as Property);
            } else {
                if (this.props.selectedPolygonId !== null) {
                    return ((propertyMapping.property as ScoredProperty).buildingUnitEnriched.businessLocationAreaId === this.props.selectedPolygonId && (this.state.propertyFilter.propertyUsages.length === 0 || this.state.propertyFilter.propertyUsages.some((usage) => usage.levelOne === (propertyMapping.property as ScoredProperty).buildingUnitEnriched.propertyUsersIndustryClassificationLevelOne && (usage.levelTwo === null || usage.levelTwo === (propertyMapping.property as ScoredProperty).buildingUnitEnriched.propertyUsersIndustryClassificationLevelTwo))));
                }
                return this.state.propertyFilter.propertyUsages.length === 0 || this.state.propertyFilter.propertyUsages.some((usage) => usage.levelOne === (propertyMapping.property as ScoredProperty).buildingUnitEnriched.propertyUsersIndustryClassificationLevelOne && (usage.levelTwo === null || usage.levelTwo === (propertyMapping.property as ScoredProperty).buildingUnitEnriched.propertyUsersIndustryClassificationLevelTwo));
            }
        });
        this.mapApi.clearAllFeatureSources();
        this.mapApi.addPropertyPointsAndPolygonFeatures(propertyMappingsToAdd, this.props.isEarlyWarningSystem ?
                                                                               this.mapApi.earlyWarningSystemPinSource :
                                                                               this.mapApi.pointSource, this.mapApi.polygonSource);
    };

    private filterUsageStatus = (property: Property): boolean => {

        let isValid = false;
        if (this.state.propertyFilter.propertyUsageStatus.length === 0) {
            isValid = this.state.propertyFilter.propertyUsages.some(usage => usage.levelOne === property.currentUsageIndustryClassification?.levelOne && usage.levelTwo === property.currentUsageIndustryClassification?.levelTwo) ||
                this.state.propertyFilter.propertyUsages.some(usage => usage.levelOne === property.pastUsageIndustryClassification?.levelOne && usage.levelTwo === property.pastUsageIndustryClassification?.levelTwo) ||
                this.state.propertyFilter.propertyUsages.some(propertyUsageFilter => property.propertyMarketingInformation?.possibleUsageIndustryClassifications.some((possibleUsage) => possibleUsage.levelOne === propertyUsageFilter.levelOne && possibleUsage.levelTwo === propertyUsageFilter.levelTwo));
        } else {
            if (this.state.propertyFilter.propertyUsageStatus.includes(0) && this.state.propertyFilter.propertyUsages.some(usage => usage.levelOne === property.currentUsageIndustryClassification?.levelOne && usage.levelTwo === property.currentUsageIndustryClassification?.levelTwo)) {
                return true;
            }
            if (this.state.propertyFilter.propertyUsageStatus.includes(1) && this.state.propertyFilter.propertyUsages.some(usage => usage.levelOne === property.pastUsageIndustryClassification?.levelOne && usage.levelTwo === property.pastUsageIndustryClassification?.levelTwo)) {
                return true;
            }
            if (this.state.propertyFilter.propertyUsageStatus.includes(2) && this.state.propertyFilter.propertyUsages.some(propertyUsageFilter => property.propertyMarketingInformation?.possibleUsageIndustryClassifications.map(classification => ({levelOne: classification.levelOne, levelTwo: classification.levelTwo})).some((classification) => classification.levelOne === propertyUsageFilter.levelOne && classification.levelTwo === propertyUsageFilter.levelTwo))) {
                return true;
            }
        }

        return this.state.propertyFilter.propertyUsages.length === 0 || isValid;
    };

    private filterPropertyStatus = (property: Property): boolean => {
        return (
            this.state.propertyFilter.propertyStatus.length === 0
            || (property.objectIsEmpty && this.state.propertyFilter.propertyStatus.includes(0))
            || (property.objectBecomesEmpty && this.state.propertyFilter.propertyStatus.includes(1))
            || (property.objectBecomesEmpty === false && property.objectIsEmpty === false && this.state.propertyFilter.propertyStatus.includes(2))
            || (property.keyProperty && this.state.propertyFilter.propertyStatus.includes(3))
            || (property.reuseAgreed && this.state.propertyFilter.propertyStatus.includes(4))
        );
    };

    private filterPropertyOfferType = (property: Property): boolean => {
        return (this.state.propertyFilter.propertyOfferTypes.length == 0 || this.state.propertyFilter.propertyOfferTypes.includes(property.propertyMarketingInformation?.propertyOfferType));
    };

    private filterBarrierFreeAccessType = (property: Property): boolean => {
        return this.state.propertyFilter.barrierFreeAccessTypes.length == 0 || this.state.propertyFilter.barrierFreeAccessTypes.includes(property.barrierFreeAccess);
    };

    private filterAssignedAccountUser = (property: Property): boolean => {
        return this.state.propertyFilter.assignedAccountUser == null || this.state.propertyFilter.assignedAccountUser === property.assignedToAccountUser?.id;
    };

    private filterAreaSize = (property: Property): boolean => {
        let isValid: boolean = false;

        if (this.state.propertyFilter.propertyMinSpace !== '' && this.state.propertyFilter.propertyMaxSpace !== '') {
            isValid = parseFloat(property.areaSize) >= parseFloat(this.state.propertyFilter.propertyMinSpace) && parseFloat(property.areaSize) <= parseFloat(this.state.propertyFilter.propertyMaxSpace);
        } else {
            if (this.state.propertyFilter.propertyMinSpace !== '') {
                isValid = parseFloat(property.areaSize) >= parseFloat(this.state.propertyFilter.propertyMinSpace);
            }
            if (this.state.propertyFilter.propertyMaxSpace !== '') {
                isValid = parseFloat(property.areaSize) <= parseFloat(this.state.propertyFilter.propertyMaxSpace);
            }
        }

        return (this.state.propertyFilter.propertyMinSpace === '' && this.state.propertyFilter.propertyMaxSpace === '') || isValid;
    };

    private filterShopWindowWidth = (property: Property): boolean => {
        let isValid: boolean = false;

        if (this.state.propertyFilter.shopWindowFrontWidthMin !== '' && this.state.propertyFilter.shopWindowFrontWidthMax !== '') {
            isValid = parseFloat(property.shopWindowFrontWidth) >= parseFloat(this.state.propertyFilter.shopWindowFrontWidthMin) && parseFloat(property.shopWindowFrontWidth) <= parseFloat(this.state.propertyFilter.shopWindowFrontWidthMax);
        } else {
            if (this.state.propertyFilter.shopWindowFrontWidthMin !== '') {
                isValid = parseFloat(property.shopWindowFrontWidth) >= parseFloat(this.state.propertyFilter.shopWindowFrontWidthMin);
            }
            if (this.state.propertyFilter.shopWindowFrontWidthMax !== '') {
                isValid = parseFloat(property.shopWindowFrontWidth) <= parseFloat(this.state.propertyFilter.shopWindowFrontWidthMax);
            }
        }

        return (this.state.propertyFilter.shopWindowFrontWidthMin === '' && this.state.propertyFilter.shopWindowFrontWidthMax === '') || isValid;
    };

    private filterLastUpdated = (property: Property): boolean => {
        let isValid: boolean = false;
        const propertyCreateOrUpdateDate: Date = new Date(property.updatedAt === null ? property.createdAt.split('T')[0] : property.updatedAt.split('T')[0]);

        if (this.state.propertyFilter.lastUpdatedFrom !== '' && this.state.propertyFilter.lastUpdatedTill !== '') {
            isValid = propertyCreateOrUpdateDate >= new Date(this.state.propertyFilter.lastUpdatedFrom) && propertyCreateOrUpdateDate <= new Date(this.state.propertyFilter.lastUpdatedTill);
        } else {
            if (this.state.propertyFilter.lastUpdatedFrom !== '') {
                isValid = propertyCreateOrUpdateDate >= new Date(this.state.propertyFilter.lastUpdatedFrom);
            }
            if (this.state.propertyFilter.lastUpdatedTill !== '') {
                isValid = propertyCreateOrUpdateDate <= new Date(this.state.propertyFilter.lastUpdatedTill);
            }
        }

        return (this.state.propertyFilter.lastUpdatedFrom === '' && this.state.propertyFilter.lastUpdatedTill === '') || isValid;
    };

    private initializePropertyFilter = (): PropertyFilter => {
        if (localStorage.getItem('lean.map.propertyFilter') === null) {
            localStorage.setItem('lean.map.propertyFilter', JSON.stringify(this.state.propertyFilter));
        }

        return {...this.state.propertyFilter, ...JSON.parse(localStorage.getItem('lean.map.propertyFilter'))};
    };

    private getColorByObjectStatusId(objectStatusId: number): string {
        const colors = MapApi.getObjectStatusColors();
        switch (objectStatusId) {
            case 0:
                return colors.danger;
            case 1:
                return colors.warning;
            case 2:
                return colors.success;
            case 4:
                return colors.info;
            default:
                return '';
        }
    }
}

export default MapPropertyFilter;
