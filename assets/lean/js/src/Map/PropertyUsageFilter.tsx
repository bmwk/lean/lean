import { MDCSelect } from '@material/select';
import React, { ChangeEvent, useEffect, useRef, useState } from 'react';
import { NaceClassification } from '../Property/PropertyApiResponse';

interface PropertyUsageFilterProps {
    checked: boolean,
    name: string,
    usageIconSrc: string,
    levelOne: number,
    levelTwo?: NaceClassification[],
    handleLevelOneChange: Function,
    handleLevelTwoChange: Function,
    selectedIndex: number
}

const PropertyUsageFilter = (props: PropertyUsageFilterProps) => {

    const selectRef = useRef();
    const [mdcSelect, setMdcSelect] = useState<MDCSelect>(null);

    useEffect(() => {
        if (props.levelTwo?.length > 0 && mdcSelect === null) {
            setMdcSelect(new MDCSelect(selectRef.current));
        }
    }, [props.levelTwo]);

    useEffect(() => {
        if (mdcSelect !== null) {
            const eventListener = () => props.handleLevelTwoChange(props.levelOne, parseInt(mdcSelect.value));
            const currentSelectedIndex = props.selectedIndex;
            mdcSelect.setSelectedIndex(currentSelectedIndex);
            mdcSelect.listen('MDCSelect:change', eventListener);
            return () => (mdcSelect.unlisten('MDCSelect:change', eventListener));
        }
    }, [mdcSelect, props.selectedIndex]);

    const handleClear = () => {
        props.handleLevelTwoChange(props.levelOne, null);
        mdcSelect.setSelectedIndex(-1, true);
    };

    return (
        <div>
            <div className="mdc-form-field">
                <div className="mdc-checkbox">
                    <input
                        onChange={(event: ChangeEvent<HTMLInputElement>): void => props.handleLevelOneChange(props.levelOne, event)}
                        type="checkbox"
                        checked={props.checked}
                        className="mdc-checkbox__native-control"/>
                    <div className="mdc-checkbox__background">
                        <svg className="mdc-checkbox__checkmark" viewBox="0 0 24 24">
                            <path className="mdc-checkbox__checkmark-path" fill="none"
                                  d="M1.73,12.91 8.1,19.28 22.79,4.59"/>
                        </svg>
                        <div className="mdc-checkbox__mixedmark"/>
                    </div>
                </div>
                <img src={props.usageIconSrc} alt=""/>
                <label>{props.name}</label>
            </div>
            {props.levelTwo?.length > 0 &&
            <div ref={selectRef} className={'mdc-select mdc-select--filled w-100 property-usage-select mt-1 mb-2' + (props.checked === false ? ' d-none' : '')}
                 data-level-one={props.levelOne}>
                <div className="mdc-select__anchor" role="button">
                    <span className="mdc-select__ripple"/>
                    <span className="mdc-floating-label">Branchenbereich</span>
                    <span className="mdc-select__selected-text-container">
                        <span id="propertyBarrierFreeFilterSelect-selected-text" className="mdc-select__selected-text"/>
                    </span>
                    <i className="material-icons mdc-text-field__icon mdc-text-field__icon--leading clear-icon small property-usage-select-clear-button"
                       tabIndex={0} onClick={() => handleClear()}>clear</i>
                    <span className="mdc-select__dropdown-icon">
                        <svg className="mdc-select__dropdown-icon-graphic" viewBox="7 10 10 5" focusable="false">
                            <polygon className="mdc-select__dropdown-icon-inactive" stroke="none" fillRule="evenodd"
                                     points="7 10 12 15 17 10"/>
                            <polygon className="mdc-select__dropdown-icon-active" stroke="none" fillRule="evenodd"
                                     points="7 15 12 10 17 15"/>
                        </svg>
                    </span>
                    <span className="mdc-line-ripple"/>
                </div>
                <div className="mdc-select__menu mdc-menu mdc-menu-surface mdc-menu-surface--fullwidth">
                    <ul className="mdc-list" role="listbox" aria-label="picker listbox">
                        {props.levelTwo.map((nace: NaceClassification) =>
                            <li className={'mdc-list-item py-2'}
                                data-value={nace.levelTwo} role="option" key={nace.levelTwo}>
                                <span className="mdc-list-item__ripple"/>
                                <span className="mdc-list-item__text">{nace.name}</span>
                            </li>
                        )}
                    </ul>
                </div>
            </div>}
        </div>
    );
};

export default PropertyUsageFilter;
