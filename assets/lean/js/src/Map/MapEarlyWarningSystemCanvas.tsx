import { MDCTabBar } from '@material/tab-bar';
import { Collection, Feature } from 'ol';
import { Point } from 'ol/geom';
import React, { createRef } from 'react';
import { MapApi } from '../MapApi/MapApi';
import AttractivenessIndicatorTable from './AttractivenessIndicatorTable';

interface EarlyWarningSystemState {
    isLoading: boolean;
    clusteredFeatures: Feature[];
    selectedFeatureLocation: string;
}

interface EarlyWarningSystemProps {
    selectedFeatures: Feature[];
    setSelectedPolygonId: Function;
    setStep: Function;
    step: number;
}

class MapEarlyWarningSystemCanvas extends React.Component<EarlyWarningSystemProps, EarlyWarningSystemState> {

    private readonly mapApi: MapApi;
    private readonly tabBarRef: React.RefObject<HTMLDivElement>;
    private tabBar: MDCTabBar;

    constructor(props: EarlyWarningSystemProps) {
        super(props);
        this.state = {
            isLoading: false,
            clusteredFeatures: [],
            selectedFeatureLocation: null,
        };
        this.mapApi = MapApi.getInstance();
        this.tabBarRef = createRef();
    }

    public showPopupOnScoredPropertyHover(): void {
        this.mapApi.hoverInteraction.on('select', (event) => {
            this.mapApi.popup.getElement().innerText = '';
            const title = document.createElement('b');
            const body = document.createElement('small');

            if (event.selected.length <= 0) {
                this.mapApi.popup.setPosition(undefined);
                return;
            }

            if (event.selected[0].getGeometry().getType() === 'Polygon' && event.selected[0].get('businessLocationId')) {
                if (this.props.step === 2) {
                    this.mapApi.popup.setPosition(undefined);
                    return;
                }
                title.textContent = MapApi.LOCATION_CATEGORIES[event.selected[0].get('locationCategory')];
                if (this.props.step === 3) {
                    let pressureClassification = event.selected[0].get('pressureClassification');
                    if (pressureClassification !== null && pressureClassification !== undefined) {
                        body.innerText = 'Lagebewertung: ';
                        body.innerText = body.innerText.concat(MapApi.getDescriptionFromPressureClassification(pressureClassification));
                    } else {
                        body.innerText = 'Keine Lagebewertung verfügbar';
                    }
                }

            } else {
                const clusteredFeatures = event.selected[0].get('features');
                if (clusteredFeatures !== undefined && clusteredFeatures.length > 1) {
                    title.innerHTML = `<small>An dieser Stelle befinden sich ${clusteredFeatures.length} Objekte</small>`;
                    if (this.mapApi.view.getZoom() < this.mapApi.view.getMaxZoom()) {
                        body.innerHTML = 'Sie können näher heranzoomen, <br>um die genaue Lage zu sehen.';
                    }
                } else if (clusteredFeatures !== undefined && clusteredFeatures.length === 1) {
                    title.textContent = clusteredFeatures[0].get('buildingUnitEnriched').personName !== null ? clusteredFeatures[0].get('buildingUnitEnriched').personName : 'Name unbekannt';
                    body.innerHTML = `${clusteredFeatures[0].get('buildingUnitEnriched').propertyUsersIndustryClassificationName !== null ? clusteredFeatures[0].get('buildingUnitEnriched').propertyUsersIndustryClassificationName + '<br>' : ''} Objekt ist ${clusteredFeatures[0].get('buildingUnitId') === null ? 'nicht' : ''} in LeAn erfasst`;
                    if (Boolean(clusteredFeatures[0].get('buildingUnitEnriched').objectIsEmpty) === true) {
                        title.textContent = 'Das Objekt steht leer';
                    }
                } else {
                    title.textContent = event.selected[0].get('buildingUnitEnriched').personName !== null ? event.selected[0].get('buildingUnitEnriched').personName : 'Name unbekannt';
                    body.innerHTML = `${event.selected[0].get('buildingUnitEnriched').propertyUsersIndustryClassificationName !== null ? event.selected[0].get('buildingUnitEnriched').propertyUsersIndustryClassificationName + '<br>' : ''} Objekt ist ${event.selected[0].get('buildingUnitId') === null ? 'nicht' : ''} in LeAn erfasst`;
                    if (Boolean(event.selected[0].get('buildingUnitEnriched').objectIsEmpty) === true) {
                        title.textContent = 'Das Objekt steht leer';
                    }
                }
            }

            this.mapApi.popup.getElement().append(title, body);
            this.mapApi.popup.setPosition(event.mapBrowserEvent.coordinate);
        });
    }

    public createListItemsFromScoredPropertyFeatures(features: Feature<any>[]): HTMLLIElement[] {
        return features.map((feature: Feature<any>): HTMLLIElement => {
            const name = feature.get('buildingUnitEnriched').personName ?? 'Kein Name verfügbar';
            let usage = feature.get('buildingUnitEnriched').propertyUsersIndustryClassificationName ?? this.mapApi.getPropertyUsageMapping()[feature.get('buildingUnitEnriched').propertyUsersIndustryClassificationLevelOne] ?? 'Keine Nutzung angegeben';
            const colors = MapApi.getObjectStatusColors();
            const pressureClassification = feature.get('pressureClassification');
            let textColor: string = '';
            if (pressureClassification < 0.5) {
                textColor = colors.danger;
            } else if (pressureClassification < 1.5) {
                textColor = colors.warning;
            } else {
                textColor = colors.success;
            }

            const listItem = document.createElement('li');
            listItem.className = 'mdc-list-item d-flex flex-column border-bottom pb-2 pt-2';
            if (Boolean(feature.get('buildingUnitEnriched').objectIsEmpty) === true) {
                textColor = colors.undefined;
                usage = 'Objekt steht leer';
            } else {
                listItem.addEventListener('click', () => {
                    this.setState({clusteredFeatures: [feature]});
                });
            }
            listItem.innerHTML = `<b style="color: ${textColor}">${name}</b><span>${usage}</span>`;

            return listItem;
        });
    }

    public componentDidMount(): void {
        this.showPopupOnScoredPropertyHover();
        this.tabBar = new MDCTabBar(this.tabBarRef.current);

        this.mapApi.selectInteraction.on('select', (e) => {
            if (this.props.step === 1) {
                return;
            }
            if (e.selected.length === 1) {
                const feature = e.selected[0];

                if (feature.getGeometry().getType() === 'Polygon') {
                    if (feature.get('businessLocationId') !== undefined) {
                        this.props.setSelectedPolygonId(feature.get('businessLocationId'));
                        this.props.setStep(2);
                        this.tabBar.activateTab(1)
                    }
                    return;
                }

                const clusteredFeatures: Feature<Point>[] = feature.get('features') ? feature.get('features') : [feature];

                if (Boolean(clusteredFeatures[0].get('buildingUnitEnriched').objectIsEmpty) === false) {
                    this.mapApi.focusFeature(clusteredFeatures[0]);
                    if (clusteredFeatures.length === 1) {
                        this.setState({clusteredFeatures});
                    } else {
                        const list: HTMLUListElement = document.createElement('ul');
                        list.className = 'mdc-list';
                        this.createListItemsFromScoredPropertyFeatures(clusteredFeatures).forEach((item: HTMLElement) => list.appendChild(item));
                        this.mapApi.setOverviewPopupContent(list);
                        this.mapApi.overviewPopup.setPosition(clusteredFeatures[0].getGeometry().getCoordinates());
                        this.setState({clusteredFeatures: []});
                    }
                }
            } else {
                this.setState({clusteredFeatures: []});
            }

        });

        this.mapApi.selectInteraction.getFeatures().on('remove', (e) => {
            if ((e.target as Collection<Feature>).getArray().length === 0) {
                this.mapApi.overviewPopup.setPosition(null);
            }
        });
    }

    public componentDidUpdate(prevProps: Readonly<EarlyWarningSystemProps>, prevState: Readonly<EarlyWarningSystemState>, snapshot?: any): void {
        this.mapApi.setBusinessLocationLayerVisibility(this.props.step === 1);
        this.mapApi.setScoredBusinessLocationLayerVisibility(this.props.step === 2 || this.props.step === 3);
        this.mapApi.setEarlyWarningSystemLayerVisibility(this.props.step === 2);

        if (this.state.clusteredFeatures.length > 0) {
            if (this.state.clusteredFeatures[0].get('buildingUnitEnriched').businessLocationAreaId !== prevState.clusteredFeatures[0]?.get('buildingUnitEnriched').businessLocationAreaId) {
                if (this.state.clusteredFeatures[0].get('buildingUnitEnriched').businessLocationAreaId === null) {
                    this.setState({selectedFeatureLocation: null});
                } else {
                    const locationCategoryId = this.mapApi.getBusinessLocationFeatures().find(locationFeature => {
                        return locationFeature.get('businessLocationId') === this.state.clusteredFeatures[0].get('buildingUnitEnriched').businessLocationAreaId;
                    })?.get('locationCategory');
                    this.setState({selectedFeatureLocation: MapApi.LOCATION_CATEGORIES[locationCategoryId]});
                }
            }
        }
    }

    public componentWillUnmount(): void {
        this.tabBar.destroy();
    }

    public render(): JSX.Element {
        return (
            <div id="mapEarlyWarningSystemCanvas" className="d-flex flex-column position-absolute bottom-0 top-0 end-0">
                <div className="mdc-tab-bar" role="tablist" ref={this.tabBarRef}>
                    <div className="mdc-tab-scroller">
                        <div className="mdc-tab-scroller__scroll-area">
                            <div className="mdc-tab-scroller__scroll-content">
                                <button className="mdc-tab mdc-tab--active" role="tab" aria-selected="true"
                                        onClick={() => {
                                            this.props.setSelectedPolygonId(null);
                                            this.mapApi.selectInteraction.getFeatures().clear();
                                            this.setState({clusteredFeatures: []});
                                            this.props.setStep(1);
                                        }}>
                                      <span className="mdc-tab__content">
                                        <span className="mdc-tab__text-label">Einführung</span>
                                      </span>
                                    <span className="mdc-tab-indicator mdc-tab-indicator--active">
                                            <span className="mdc-tab-indicator__content mdc-tab-indicator__content--underline"/>
                                          </span>
                                    <span className="mdc-tab__ripple"/>
                                </button>
                                <button className="mdc-tab" role="tab" aria-selected="true"
                                        onClick={() => {
                                            this.props.setStep(2);
                                        }}>
                                      <span className="mdc-tab__content">
                                        <span className="mdc-tab__text-label">Gewerbe</span>
                                      </span>
                                    <span className="mdc-tab-indicator">
                                        <span className="mdc-tab-indicator__content mdc-tab-indicator__content--underline"/>
                                    </span>
                                    <span className="mdc-tab__ripple"/>
                                </button>
                                <button className="mdc-tab" role="tab" aria-selected="true"
                                        onClick={() => {
                                            this.props.setSelectedPolygonId(null);
                                            this.mapApi.selectInteraction.getFeatures().clear();
                                            this.setState({clusteredFeatures: []});
                                            this.props.setStep(3);
                                        }}>
                                      <span className="mdc-tab__content">
                                        <span className="mdc-tab__text-label">Standortlagen</span>
                                      </span>
                                    <span className="mdc-tab-indicator">
                                        <span className="mdc-tab-indicator__content mdc-tab-indicator__content--underline"/>
                                      </span>
                                    <span className="mdc-tab__ripple"/>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                {this.props.step === 1 &&
                <div className="p-3">
                    <div className="d-flex align-items-center mb-1">
                        <p className="fs-5">Einführung</p>
                    </div>
                    <p className="mb-1">
                        Mit dem <b>Frühwarnsystem</b> die <b>Attraktivität der Innenstadt</b> immer im Blick haben.
                    </p>
                    <div className="mb-1">
                        Attraktivität auf zwei Ebenen verfolgen:
                        <ul>
                            <li>
                                <div>
                                    <b>Attraktivität von Gewerben</b>
                                </div>
                                <span>
                                    basierend auf Indikatoren aus den Bereichen „Online-Sichtbarkeit“, „Branchenentwicklung“, „Gewerbeumfeld“ und
                                    „Rahmenbedingungen“.
                                </span>
                            </li>
                            <li>
                                <div>
                                    <b>Attraktivität von Standortlagen</b>
                                </div>
                                <span>
                                     als Aggregation der dortigen Gewerbeattraktivität
                                </span>
                            </li>
                        </ul>
                    </div>
                    <p>
                        Die in LeAn <b>definierten</b> und im Frühwarnsystem unterschiedenen <b>Standortlagen</b> sind der Karte zu entnehmen.
                    </p>
                </div>}
                {this.props.step === 2 &&
                <div className="overflow-auto p-3 pe-2">
                    {this.state.clusteredFeatures?.length !== 1 ?
                     <div>
                         <p className="fs-5">Attraktivität von Gewerben</p>
                         <p className="mb-1">
                             Die <b>Karte</b> weist die in LeAn erfassten
                             <svg className="mx-1" width="15px" height="22.5px" viewBox="0 0 30 45" xmlns="http://www.w3.org/2000/svg">
                                 <path d="M0 15 L15 45 L30 15 z" fill="#014A5D"/>
                                 <circle cx="15" cy="15" r="15" fill="#014A5D"/>
                             </svg>
                             und noch nicht erfassten
                             <svg className="mx-1" width="15px" height="22.5px" viewBox="0 0 30 45" xmlns="http://www.w3.org/2000/svg">
                                 <path d="M0 0 L0 30 L15 45 L30 30 L30 0 z" fill="#014A5D"/>
                                 <circle cx="15" cy="15" r="15" fill="#014A5D"/>
                             </svg>
                             Gewerbeobjekte in der Innenstadt aus.
                         </p>
                         <p>
                             Durch <b>Klick auf ein Symbol</b> öffnet sich die Detailansicht der Gewerbeattraktivität.
                         </p>
                     </div> :
                     <div>
                         <div className="d-flex mb-3 align-items-center">
                             <div className="d-flex flex-column">
                                 <span
                                     className="fs-5 fw-bold">{this.state.clusteredFeatures[0].get('buildingUnitEnriched').personName ?? 'Kein Name angegeben'}</span>
                                 <span className="d-flex align-items-center">
                                        <img
                                            src={this.mapApi.getPropertyUsageIconById(this.state.clusteredFeatures[0].get('buildingUnitEnriched').propertyUsersIndustryClassificationLevelOne)}
                                            alt="Branchen-Icon" className="me-2"/>
                                     {this.state.clusteredFeatures[0].get('buildingUnitEnriched').propertyUsersIndustryClassificationName ?? this.mapApi.getPropertyUsageMapping()[this.state.clusteredFeatures[0].get('buildingUnitEnriched').propertyUsersIndustryClassificationLevelOne]}</span>
                             </div>
                             {this.state.clusteredFeatures[0].get('buildingUnitId') ?
                              <a className="mdc-button mdc-button--raised ms-auto flex-shrink-0 me-1"
                                 href={`/besatz-und-leerstand/objekte/${this.state.clusteredFeatures[0].get('buildingUnitId')}/lagekarte`} target="_blank">
                                  Zum Objekt
                              </a> :
                              <a className="mdc-button mdc-button--raised ms-auto flex-shrink-0 me-1" href={`#`}>
                                  Objekt erfassen
                              </a>}
                         </div>
                         <div>
                             <p>
                                 Das gewählte Gewerbe gilt als <span
                                 className="fw-bold">{MapApi.getDescriptionFromPressureClassification(this.state.clusteredFeatures[0].get('pressureClassification'))}</span>
                                 {this.state.selectedFeatureLocation !== null ?
                                  <span> und liegt in <span className="fw-bold">{this.state.selectedFeatureLocation}</span>. </span> : '. '}
                                 Die Bewertung beruht auf der Analyse der folgenden Indikatoren (das Geschäftsmodell und die Unternehmenspolitik des Gewerbes
                                 werden nicht bewertet):
                             </p>
                         </div>
                         <AttractivenessIndicatorTable title="Online-Sichtbarkeit" weighting={40} indicators={[
                             {name: 'Google Sterne-Bewertungen', value: this.state.clusteredFeatures[0].get('starsPercentile'), tooltip: 'Durchschnittliche Sternebewertung auf Google.'},
                             {name: 'Anzahl Google-Bewertungen', value: this.state.clusteredFeatures[0].get('numberOfReviewsPercentile'), tooltip: 'Anzahl der Sternebewertungen auf Google.'},
                             {name: 'Google-Suchrang', value: this.state.clusteredFeatures[0].get('localSearchPositionPercentile'), tooltip: 'Die Relevanz des Gewerbes bei der Google-Suche in direkter Umgebung des Gewerbes.'},
                             {name: 'Potentielle Reichweite', value: this.state.clusteredFeatures[0].get('potentialReachHouseholdsPercentile'), tooltip: 'Die stadtweite Relevanz des Gewerbes bei der Google-Suche.'},
                         ]}/>
                         <AttractivenessIndicatorTable title="Branchenentwicklung" weighting={30} indicators={[
                             {name: 'Umsatzentwicklung der Branche (Prognose)', value: this.state.clusteredFeatures[0].get('industryDevelopmentLongtermPercentile'), tooltip: 'Bewertung der langfristigen Entwicklung der Branche (Zehnjahreszeitraum), Indikator setzt sich aus der Entwicklung in der Vergangenheit und der Prognose zusammen.'},
                             {name: 'Umsatzentwicklung der Branche (Aktuell)', value: this.state.clusteredFeatures[0].get('industryDevelopmentShorttermPercentile'), tooltip: 'Bewertung der kurzfristigen Entwicklung der Branche, d.h. die Entwicklung der letzten Monate verglichen mit dem identischen Zeitraum des Vorjahres.'},
                             {name: 'Entwicklung des Umsatzanteils über Fachhändler', value: this.state.clusteredFeatures[0].get('specialtyTradeShareDevelopmentPercentile'), tooltip: 'Umsatzanteil der Distributionswege, die jeweiligen Branchenfachhandel zuzuordnen sind, am Marktvolumen insgesamt im letzten vollen Kalenderjahr.'},
                             {name: 'aktuelle Entwicklung des Umsatzanteils über Onlinehandel', value: this.state.clusteredFeatures[0].get('onlineShareDevelopmentPercentile'), tooltip: `Retrospektive Entwicklung des Umsatzanteils online am Marktvolumen insgesamt. Der aktuelle Onlineanteil beträgt ${Math.round(this.state.clusteredFeatures[0].get('onlineShareActual') * 100)}%.`},
                             {name: 'Anteil stationärer Händler am Onlinehandel', value: this.state.clusteredFeatures[0].get('ratioStationaryOnlinePercentile'), tooltip: 'Umsatzanteil der stationären Händler online am Marktvolumen insgesamt im letzten vollen Kalenderjahr.'},
                         ]}/>
                         <AttractivenessIndicatorTable title="Gewerbeumfeld" weighting={10} indicators={[
                             {name: 'Leerstandsquote', value: this.state.clusteredFeatures[0].get('vacancyRatePercentile'), tooltip: 'Bekannte Leerstandsquote der in LeAn erfassten Gewerbe in der eingezeichneten Standortlage. Die Leerstandquote ist erst ab 10 erfassten Gewerben in einer Standortlage verfügbar.'},
                         ]}/>
                         <AttractivenessIndicatorTable title="Rahmenbedingungen" weighting={20} indicators={[
                             {name: 'Kaufkraftniveau der Bevölkerung', value: this.state.clusteredFeatures[0].get('purchasingPowerLevelPercentile'), tooltip: 'Kaufkraftniveau der lokalen Bevölkerung.'},
                             {name: 'Zentralitätsniveau', value: this.state.clusteredFeatures[0].get('centralityLevelPercentile'), tooltip: 'Zentralitätsniveau der Stadt.'},
                             {name: 'Attraktivitätsempfinden gegenüber der Innenstadt von Besuchenden', value: this.state.clusteredFeatures[0].get('overallAttractivenessPercentile'), tooltip: 'Gesamtattraktivität der Stadt nach Befragung von Stadtbesuchenden.'},
                             {name: 'Entwicklung des Attraktivitätsempfindens', value: this.state.clusteredFeatures[0].get('attractivenessDevelopmentPercentile'), tooltip: 'Entwicklung der Gesamtattraktivität der Stadt nach Befragung von Stadtbesuchenden (Nicht Entwicklung der Befragung.'},
                             {name: 'Net Promoter Score', value: this.state.clusteredFeatures[0].get('npsPercentile'), tooltip: 'Net Promoter Score der Innenstadt nach Befragung von Stadtbesuchenden.'},
                             {name: 'Entwicklung der Population', value: this.state.clusteredFeatures[0].get('populationGrowthPercentile'), tooltip: 'Bevölkerungswachstum der Stadt in den letzten 5 Jahren.'},
                         ]}/>
                     </div>
                    }
                </div>}
                {this.props.step === 3 &&
                <div className="p-3">
                    <p className="fs-5">Attraktivität von Standortlagen</p>
                    <p className="mb-1">
                        Die Karte informiert über die Attraktivität der in LeAn definierten und im Frühwarnsystem unterschiedenen Standortlagen.
                    </p>
                    <p className="mb-1">
                        Die Attraktivität einer Standortlage ergibt sich aus der Aggregation der Attraktivitätsbewertungen der in dieser Lage angesiedelten
                        Gewerbe.
                    </p>
                    <p className="mb-1">
                        Die farbliche Kennzeichnung einer Standortlage informiert über die ermittelte Gesamtattraktivität.
                    </p>
                </div>}
            </div>
        );
    }

}

export default MapEarlyWarningSystemCanvas;
