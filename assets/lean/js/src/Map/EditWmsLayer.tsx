import { MDCTextField } from '@material/textfield';
import React, { ChangeEvent } from 'react';
import { WmsLayer } from '../MapApi/WmsLayer';

interface EditWmsLayerState {
    wmsLayer: WmsLayer;
    wmsLayerTitle: string;
}

interface EditWmsLayerProps {
    handleClose: Function;
    wmsLayer: WmsLayer;
    saveWmsLayer: Function;
    deleteWmsLayer: Function;
}

class EditWmsLayer extends React.Component<EditWmsLayerProps, EditWmsLayerState> {

    private readonly wmsLayerTitleMdcTextFieldRefObject: React.RefObject<HTMLLabelElement>;
    private wmsLayerTitleMdcTextField: MDCTextField;

    constructor(props: EditWmsLayerProps) {
        super(props);
        this.state = {
            wmsLayer: this.props.wmsLayer,
            wmsLayerTitle: this.props.wmsLayer.title
        };
        this.wmsLayerTitleMdcTextFieldRefObject = React.createRef();
    }

    componentDidMount() {
        this.wmsLayerTitleMdcTextField = new MDCTextField(this.wmsLayerTitleMdcTextFieldRefObject.current);
    }

    render(): JSX.Element {
        return (
            <div style={{
                position: "absolute",
                height: '100%',
                width: '100%',
                top: 0,
                left: 0,
                zIndex: 11,
                background: "white"
            }} className="p-3 d-flex flex-column">
                <div className="d-flex align-items-center">
                    <h3>WMS-Layer bearbeiten</h3>
                    <button className="ms-auto btn-close text-reset" type="button" aria-label="Close"
                            onClick={() => this.props.handleClose()}/>
                </div>
                <div style={{overflowX: "auto"}} className="d-flex flex-column flex-wrap mt-4">
                    <div className="d-flex align-items-center flex-grow-1">
                        <div className="me-auto">
                            <label ref={this.wmsLayerTitleMdcTextFieldRefObject}
                                   className="mdc-text-field mdc-text-field--filled">
                                <span className="mdc-text-field__ripple"/>
                                <span className="mdc-floating-label" id="my-label-id">WMS-Layer Bezeichnung</span>
                                <input className="mdc-text-field__input" type="text" value={this.state.wmsLayer.title}
                                       onChange={this.handleInput} aria-labelledby="my-label-id"/>
                                <span className="mdc-line-ripple"/>
                            </label>
                        </div>
                        <div>
                            <button type="button" className="btn btn-danger text-white"
                                    onClick={() => this.props.deleteWmsLayer(this.state.wmsLayer)}>Löschen
                            </button>
                        </div>
                    </div>
                </div>
                <div className="mt-auto">
                    <button className="mdc-button mdc-button--raised"
                            onClick={() => this.props.saveWmsLayer(this.state.wmsLayer)}>
                        <span className="mdc-button__label">Speichern</span>
                    </button>
                </div>
            </div>
        );
    }

    private handleInput = (event: ChangeEvent<HTMLInputElement>): void => {
        this.setState({
            wmsLayer: {
                ...this.state.wmsLayer,
                title: event.target.value
            }
        }, () => console.log(this.state.wmsLayer));
    };

}

export default EditWmsLayer;