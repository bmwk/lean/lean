import { Feature, MapBrowserEvent, MapEvent } from 'ol';
import { FullScreen } from 'ol/control';
import { FeatureLike } from 'ol/Feature';
import { fromLonLat } from 'ol/proj';
import React from 'react';
import { MapApi } from '../MapApi/MapApi';
import { Property } from '../MapApi/Property';
import { AccountUserType } from './AccountUserType';
import Map from './Map';
import { MapFunctionalitiesState } from './MapFunctionalitiesState';
import MapPropertyFilter from './MapPropertyFilter';
import MapToolbar from './MapToolbar';
import { MapToolbarOption } from './MapToolbarOption';
import MapWmsLayer from './MapWmsLayer';
import OverviewPopup from './OverviewPopup';

interface PropertyOverviewMapState {

    mapFunctionalitiesState?: MapFunctionalitiesState;
    mapToolbarOption?: MapToolbarOption;
    properties?: Property[];
    selectedFeatures: Feature[];
    alertText?: string;
    alertContext?: string;
    alertVisible: boolean;
}

interface PropertyOverviewMapProps {
    centerPoint: {
        longitude: number;
        latitude: number;
    };
    archived: boolean;
    canCreateBuildingUnit: boolean;
    permissions: {canView: boolean, canEdit: boolean};
    accountUserType: AccountUserType;
}

function getPinLayerVisibilityFromLocaleStorage(): boolean {
    if (localStorage.getItem('lean.map.pinLayerVisibility') === null) {
        return true;
    }

    return JSON.parse(localStorage.getItem('lean.map.pinLayerVisibility'));
}

function getPolygonLayerVisibilityFromLocaleStorage(): boolean {
    if (localStorage.getItem('lean.map.polygonLayerVisibility') === null) {
        return true;
    }

    return JSON.parse(localStorage.getItem('lean.map.polygonLayerVisibility'));
}

class PropertyOverviewMap extends React.Component<PropertyOverviewMapProps, PropertyOverviewMapState> {

    private readonly mapApi: MapApi;
    private readonly mapToolbarRefObject: React.RefObject<MapToolbar>;

    constructor(props: PropertyOverviewMapProps) {
        super(props);

        const pinLayerVisibility = getPinLayerVisibilityFromLocaleStorage();
        const polygonLayerVisibility = getPolygonLayerVisibilityFromLocaleStorage();

        this.state = {
            mapFunctionalitiesState: {
                pinLayerVisibility: pinLayerVisibility,
                polygonLayerVisibility: polygonLayerVisibility,
                filterVisibility: false,
                translateInteraction: false,
                modifyInteraction: false,
                drawInteraction: false,
                setPinInteraction: false,
                drawBusinessLocationInteraction: false,
                businessLocationLayerVisibility: false,
                measureLayerVisibility: false,
                measureInteraction: false,
                wmsLayerVisibility: false,
                createObjectByClickInMap: false,
                propertyPopup: true,
                businessLocationReadonly: true,
            },
            mapToolbarOption: {
                pinView: true,
                polygonView: true,
                filterVisibility: true,
                drag: false,
                transform: false,
                draw: false,
                setPin: false,
                measure: true,
                wmsLayer: true,
                createObjectByClickInMap: this.props.canCreateBuildingUnit,
                print: true,
                delete: false,
                save: false,
                businessLocationDraw: false,
                businessLocationView: this.props.accountUserType !== AccountUserType.PROPERTY_PROVIDER,
            },
            properties: [],
            selectedFeatures: [],
            alertVisible: false,
        };

        this.mapApi = MapApi.getInstance();
    }

    private setFilterCanvasVisibility = (filterVisibility: boolean): void => {
        this.setState({
            mapFunctionalitiesState: {
                ...this.state.mapFunctionalitiesState,
                wmsLayerVisibility: false,
                filterVisibility,
            }
        });
    };

    private closeFilterCanvas = () => {
        this.setState({mapFunctionalitiesState: {...this.state.mapFunctionalitiesState, filterVisibility: false}});
    };

    private closeWmsCanvas = () => {
        this.setState({mapFunctionalitiesState: {...this.state.mapFunctionalitiesState, wmsLayerVisibility: false}});
    };

    private setLayerCanvasVisibility = (wmsLayerVisibility: boolean): void => {
        this.setState({
            mapFunctionalitiesState: {
                ...this.state.mapFunctionalitiesState,
                filterVisibility: false,
                wmsLayerVisibility,
            }
        });
    };

    private showMessage = (text: string, context?: string): void => {
        this.setState({alertText: text, alertContext: context, alertVisible: true});
        setTimeout(() => {
            this.setState({alertText: null, alertContext: null, alertVisible: false});
        }, 5000);
    };

    private setCreateObjectByClickInMap = (createObjectByClickInMap: boolean): void => {
        if (createObjectByClickInMap === true) {
            document.body.style.setProperty('cursor', 'crosshair', 'important');
        } else {
            document.body.style.setProperty('cursor', '');
        }
        this.setState({
            mapFunctionalitiesState: {...this.state.mapFunctionalitiesState, createObjectByClickInMap}
        });
    };

    public componentDidMount(): void {
        MapApi.fetchBuildingUnitsFromApi(this.props.archived).then((properties: Property[]) => {
            this.mapApi.initializeProperties(properties);
            this.setState({properties: properties});
        });

        let centerPoint = Map.getMapCenterPointFromLocalStorage();
        if (centerPoint === null) {
            centerPoint = fromLonLat([this.props.centerPoint.longitude, this.props.centerPoint.latitude]);
        }
        this.mapApi.setCenter(centerPoint);

        const zoom = Map.getMapZoomFromLocalStorage();

        if (zoom) {
            this.mapApi.setZoom(zoom);
        }

        if (this.state.mapToolbarOption.businessLocationView === true) {
            this.mapApi.initializeBusinessLocations();
        }

        this.mapApi.forwardOnClick();

        this.mapApi.onMapMoveEnd((event: MapEvent) => {
            Map.saveMapCenterAndZoomToLocalStorage(event.map.getView().getCenter(), event.map.getView().getZoom());
        });

        this.mapApi.addClickEvent((event: MapBrowserEvent<any>) => {
            const features: FeatureLike[] = this.mapApi.getFeaturesAtPixel(event.pixel);

            if (features.length > 0 || this.state.mapFunctionalitiesState.createObjectByClickInMap === false) {
                return;
            }

            Map.createObjectByClickInMap(features, event.coordinate);
        });

        this.mapApi.selectInteraction.on('select', (event): void => {
            this.setState({
                selectedFeatures: event.selected
            });
        });

        this.mapApi.initializePopup();
        this.mapApi.showPopupOnFeatureHover();
        this.mapApi.initializeOverviewPopup(document.getElementById('overviewDialog'));
        this.mapApi.setPointLayerVisibility(this.state.mapFunctionalitiesState.pinLayerVisibility);
        this.mapApi.setPolygonLayerVisibility(this.state.mapFunctionalitiesState.polygonLayerVisibility);
        this.mapApi.setDrawInteraction(this.state.mapFunctionalitiesState.drawInteraction);
        this.mapApi.setTranslateInteraction(this.state.mapFunctionalitiesState.translateInteraction);
        this.mapApi.setModifyInteraction(this.state.mapFunctionalitiesState.modifyInteraction);
        this.mapApi.setMeasureLayerVisibility(this.state.mapFunctionalitiesState.measureLayerVisibility);
        this.mapApi.setBusinessLocationLayerVisibility(this.state.mapFunctionalitiesState.businessLocationLayerVisibility);
        this.mapApi.setMeasureInteraction(this.state.mapFunctionalitiesState.measureInteraction);

        this.mapApi.map.addControl(new FullScreen);
    }

    public render(): JSX.Element {
        return (
            <Map>
                <>
                    {this.state.alertVisible &&
                    <div className={`alert alert-${this.state.alertContext ? this.state.alertContext : 'info'} mb-0`}>{this.state.alertText}</div>}
                    <OverviewPopup/>
                    <MapToolbar
                        ref={this.mapToolbarRefObject}
                        mapToolbarOption={this.state.mapToolbarOption}
                        mapFunctionalitiesState={this.state.mapFunctionalitiesState}
                        setFilterVisibility={this.setFilterCanvasVisibility}
                        setWmsLayerVisibility={this.setLayerCanvasVisibility}
                        setCreateObjectByClickInMap={this.setCreateObjectByClickInMap}
                        printMap={() => (this.mapApi.printDialog.print())}
                        selectedFeatures={this.state.selectedFeatures}
                        showMessage={this.showMessage}
                    />
                    <MapWmsLayer
                        handleClose={this.closeWmsCanvas}
                        wmsLayerCanvasVisible={this.state.mapFunctionalitiesState.wmsLayerVisibility}
                        showGlobalWmsLayer={this.props.accountUserType !== AccountUserType.PROPERTY_PROVIDER}
                    />
                    <MapPropertyFilter
                        showAccountUserFilter={this.props.accountUserType !== AccountUserType.PROPERTY_PROVIDER}
                        selectedPolygonId={null}
                        isEarlyWarningSystem={false}
                        handleClose={this.closeFilterCanvas}
                        properties={this.state.properties}
                        filterCanvasVisible={this.state.mapFunctionalitiesState.filterVisibility}
                    />
                </>
            </Map>
        );
    }

}

export default PropertyOverviewMap;
