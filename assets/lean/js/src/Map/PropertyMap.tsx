import Axios, {AxiosRequestConfig} from 'axios';
import {Feature, MapBrowserEvent} from 'ol';
import {FullScreen} from 'ol/control';
import {Polygon} from 'ol/geom';
import {fromLonLat, toLonLat} from 'ol/proj';
import React from 'react';
import {MapApi} from '../MapApi/MapApi';
import {Property} from '../MapApi/Property';
import {PropertyApiResponse} from '../Property/PropertyApiResponse';
import {AccountUserType} from './AccountUserType';
import Map from './Map';
import {MapFunctionalitiesState} from './MapFunctionalitiesState';
import MapToolbar from './MapToolbar';
import {MapToolbarOption} from './MapToolbarOption';
import MapWmsLayer from './MapWmsLayer';
import OverviewPopup from './OverviewPopup';

interface PropertyMapState {
    mapFunctionalitiesState?: MapFunctionalitiesState;
    mapToolbarOption?: MapToolbarOption;
    properties?: Property[];
    centerPoint?: {
        longitude: number;
        latitude: number;
    };
    selectedFeatures: Feature[];
    alertText?: string;
    alertContext?: string;
    alertVisible: boolean;
}

interface PropertyMapProps {
    centerPoint: {
        longitude: number;
        latitude: number;
    };
    buildingUnitId: number;
    permissions: { canView: boolean, canEdit: boolean },
    accountUserType: AccountUserType;
}

class PropertyMap extends React.Component<PropertyMapProps, PropertyMapState> {

    private readonly mapApi: MapApi;
    private readonly mapToolbarRefObject: React.RefObject<MapToolbar>;

    constructor(props: PropertyMapProps) {
        super(props);
        this.mapApi = MapApi.getInstance();
        let properties: Property[] = [];

        this.state = {
            mapFunctionalitiesState: {
                pinLayerVisibility: true,
                polygonLayerVisibility: true,
                filterVisibility: false,
                translateInteraction: false,
                modifyInteraction: false,
                drawInteraction: false,
                setPinInteraction: false,
                drawBusinessLocationInteraction: false,
                businessLocationLayerVisibility: false,
                measureLayerVisibility: false,
                measureInteraction: false,
                wmsLayerVisibility: false,
                createObjectByClickInMap: false,
                propertyPopup: false,
                businessLocationReadonly: true,
            },
            mapToolbarOption: {
                pinView: false,
                polygonView: false,
                filterVisibility: false,
                drag: this.props.permissions.canEdit,
                transform: this.props.permissions.canEdit,
                draw: this.props.permissions.canEdit,
                measure: true,
                setPin: false,
                wmsLayer: true,
                createObjectByClickInMap: false,
                print: true,
                delete: this.props.permissions.canEdit,
                save: this.props.permissions.canEdit,
                businessLocationDraw: false,
                businessLocationView: this.props.accountUserType !== AccountUserType.PROPERTY_PROVIDER,
            },
            properties: properties,
            centerPoint: this.props.centerPoint,
            selectedFeatures: [],
            alertVisible: false
        };
    }

    private closeWmsCanvas = () => {
        this.setState({mapFunctionalitiesState: {...this.state.mapFunctionalitiesState, wmsLayerVisibility: false}});
    };

    private setLayerCanvasVisibility = (wmsLayerVisibility: boolean): void => {
        this.setState({
            mapFunctionalitiesState: {
                ...this.state.mapFunctionalitiesState,
                filterVisibility: false,
                wmsLayerVisibility,
            }
        });
    };

    private showMessage = (text: string, context?: string): void => {
        this.setState({alertText: text, alertContext: context, alertVisible: true});
        setTimeout(() => {
            this.setState({alertText: null, alertContext: null, alertVisible: false});
        }, 5000);
    };

    private async fetchBuildingUnitFromApi(buildingUnitId: number, config?: AxiosRequestConfig): Promise<Property> {
        const response = await Axios.get('/api/property-vacancy-management/building-units/' + buildingUnitId, config);
        const propertyData: PropertyApiResponse = response.data;

        let latitude: number = null;
        let longitude: number = null;
        let polygonCoordinates: number[][][] = null;

        if (propertyData.geolocationPoint !== null) {
            latitude = propertyData.geolocationPoint.point.latitude;
            longitude = propertyData.geolocationPoint.point.longitude;
        }

        if (propertyData.geolocationPolygon !== null) {
            polygonCoordinates = [];

            let points: number[][] = [];

            propertyData.geolocationPolygon.polygon.rings.forEach((ring: any): void => {
                ring.points.forEach((point: any): void => {
                    points.push(MapApi.fromLonLat([point.longitude, point.latitude]));
                });

                polygonCoordinates.push(points);
            });
        }

        return {
            ...propertyData,
            href: '/besatz-und-leerstand/objekte/' + propertyData.id + '/grunddaten',
            latitude: latitude,
            longitude: longitude,
            polygonCoordinates: polygonCoordinates,
            polygonId: propertyData.geolocationPolygon?.id,
        };
    }

    public componentDidMount(): void {
        this.fetchBuildingUnitFromApi(this.props.buildingUnitId).then(property => {
            this.mapApi.initializeProperties([property]);

            if (property.longitude !== null && property.latitude !== null) {
                this.setState({centerPoint: {longitude: property.longitude, latitude: property.latitude}});
                this.mapApi.setCenter(fromLonLat([property.longitude, property.latitude]));
            } else if (property.polygonCoordinates !== null) {
                const polygonCenter = toLonLat(new Polygon(property.polygonCoordinates).getInteriorPoint().getCoordinates());
                this.setState({centerPoint: {longitude: polygonCenter[0], latitude: polygonCenter[1]}});
                this.mapApi.setCenter(fromLonLat(polygonCenter));
            }

            this.mapApi.setZoom(18);

            this.setState({
                properties: [property],
                mapToolbarOption: {...this.state.mapToolbarOption, draw: property.polygonCoordinates === null}
            });
        });

        this.mapApi.setCenter(fromLonLat([this.props.centerPoint.longitude, this.props.centerPoint.latitude]));

        if (this.state.mapToolbarOption.businessLocationView === true) {
            this.mapApi.initializeBusinessLocations();
        }

        this.mapApi.setPointLayerVisibility(this.state.mapFunctionalitiesState.pinLayerVisibility);
        this.mapApi.setPolygonLayerVisibility(this.state.mapFunctionalitiesState.polygonLayerVisibility);
        this.mapApi.setDrawInteraction(this.state.mapFunctionalitiesState.drawInteraction);
        this.mapApi.setTranslateInteraction(this.state.mapFunctionalitiesState.translateInteraction);
        this.mapApi.setModifyInteraction(this.state.mapFunctionalitiesState.modifyInteraction);
        this.mapApi.setMeasureLayerVisibility(this.state.mapFunctionalitiesState.measureLayerVisibility);
        this.mapApi.setBusinessLocationLayerVisibility(this.state.mapFunctionalitiesState.businessLocationLayerVisibility);
        this.mapApi.setMeasureInteraction(this.state.mapFunctionalitiesState.measureInteraction);

        this.mapApi.map.addControl(new FullScreen);

        this.mapApi.selectInteraction.on('select', (event): void => {
            if (event.selected?.length > 1 ?? (event.selected[0].get('businessLocationId'))) {
                return;
            }

            this.setState({
                selectedFeatures: event.selected
            });
        });

        this.mapApi.map.on('click', this.handleClickEvent);

    }

    private handleClickEvent = (event: MapBrowserEvent<any>) => {
        if (this.state.mapFunctionalitiesState.setPinInteraction) {
            this.mapApi.createFeature(event)
            this.setState({mapToolbarOption: {...this.state.mapToolbarOption, setPin: false}, mapFunctionalitiesState: {...this.state.mapFunctionalitiesState, setPinInteraction: false}})

        }
    }

    public componentDidUpdate(prevProps: Readonly<PropertyMapProps>, prevState: Readonly<PropertyMapState>, snapshot?: any): void {
        if (this.state.properties.length > 0 && prevState.properties.length === 0) {
            this.setState({mapToolbarOption: {...this.state.mapToolbarOption, setPin: this.state.properties[0].latitude === null}})
            this.mapApi.addPropertyPointsAndPolygonFeatures(this.mapApi.getPropertyMappings(), this.mapApi.pointSource, this.mapApi.polygonSource);
        }
        if (this.state.mapFunctionalitiesState.setPinInteraction === true) {
            document.body.style.setProperty('cursor', 'crosshair', 'important');
        } else {
            document.body.style.setProperty('cursor', '');
        }
    }

    public componentWillUnmount(): void {
        this.mapApi.map.un('click', this.handleClickEvent)
    }

    public render(): JSX.Element {
        return (
            <Map>
                <>
                    {this.state.alertVisible &&
                    <div className={`alert alert-${this.state.alertContext ? this.state.alertContext : 'info'} mb-0`}>{this.state.alertText}</div>}
                    <OverviewPopup/>
                    <MapToolbar
                        ref={this.mapToolbarRefObject}
                        mapToolbarOption={this.state.mapToolbarOption}
                        mapFunctionalitiesState={this.state.mapFunctionalitiesState}
                        setWmsLayerVisibility={this.setLayerCanvasVisibility}
                        printMap={() => (this.mapApi.printDialog.print())}
                        selectedFeatures={this.state.selectedFeatures}
                        showMessage={this.showMessage}
                        setSetPin={(setPin: boolean) => this.setState({mapFunctionalitiesState: {...this.state.mapFunctionalitiesState, setPinInteraction: setPin}})}
                        setDrawOption={(setDraw: boolean) => this.setState({mapToolbarOption: {...this.state.mapToolbarOption, draw: setDraw}})}
                    />
                    <MapWmsLayer
                        handleClose={this.closeWmsCanvas}
                        showGlobalWmsLayer={this.props.accountUserType !== AccountUserType.PROPERTY_PROVIDER}
                        wmsLayerCanvasVisible={this.state.mapFunctionalitiesState.wmsLayerVisibility}
                    />
                </>
            </Map>
        );
    }

}

export default PropertyMap;
