import React, { ChangeEvent } from 'react';
import { WmsLayer } from '../MapApi/WmsLayer';

interface AddWmsLayerProps {
    wmsLayer: WmsLayer[];
    handleClose: Function;
    saveNewLayers: Function;
}

interface AddWmsLayerState {
    selectedWmsLayers: WmsLayer[];
}

class AddWmsLayer extends React.Component<AddWmsLayerProps, AddWmsLayerState> {
    constructor(props: AddWmsLayerProps) {
        super(props);

        this.state = {
            selectedWmsLayers: []
        };
    }

    componentDidMount() {
        this.setState({selectedWmsLayers: this.props.wmsLayer});
    }

    render(): JSX.Element {
        return (
            <div style={{
                position: "absolute",
                height: '100%',
                width: '100%',
                top: 0,
                left: 0,
                zIndex: 11,
                background: "white"
            }} className={'p-3 d-flex flex-column'}>
                <div className={'d-flex align-items-center'}>
                    <h3>Verfügbare WMS-Layer</h3>
                    <button className="ms-auto btn-close text-reset" type="button" aria-label="Close"
                            onClick={() => this.props.handleClose()}/>
                </div>
                <div style={{overflowX: "auto"}} className="d-flex flex-column">
                    {this.props.wmsLayer.map(layer =>
                        <div key={layer.title} className="mdc-form-field">
                            <div className="mdc-checkbox">
                                <input type="checkbox" defaultChecked className="mdc-checkbox__native-control"
                                       onChange={(event) => this.handleChange(event, layer)}/>
                                <div className="mdc-checkbox__background">
                                    <svg className="mdc-checkbox__checkmark" viewBox="0 0 24 24">
                                        <path className="mdc-checkbox__checkmark-path" fill="none"
                                              d="M1.73,12.91 8.1,19.28 22.79,4.59"/>
                                    </svg>
                                    <div className="mdc-checkbox__mixedmark"/>
                                </div>
                            </div>
                            <label>{layer.title}</label>
                        </div>
                    )}
                </div>
                <div>
                    <button className="mdc-button mdc-button--raised"
                            onClick={() => this.props.saveNewLayers(this.state.selectedWmsLayers)}>
                        <span className="mdc-button__label">Speichern</span>
                    </button>
                </div>
            </div>
        );
    }

    private handleChange = (event: ChangeEvent<HTMLInputElement>, changedLayer: WmsLayer) => {
        let checkedLayers: WmsLayer[];
        if (event.target.checked) {
            checkedLayers = [changedLayer, ...this.state.selectedWmsLayers];
        } else {
            checkedLayers = this.state.selectedWmsLayers.filter((layer) => layer.title != changedLayer.title);
        }
        this.setState({selectedWmsLayers: checkedLayers});
    };


}

export default AddWmsLayer;