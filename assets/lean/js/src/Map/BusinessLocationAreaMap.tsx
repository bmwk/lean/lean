import { Feature } from 'ol';
import { FullScreen } from 'ol/control';
import { fromLonLat } from 'ol/proj';
import React from 'react';
import { MapApi } from '../MapApi/MapApi';
import { Property } from '../MapApi/Property';
import Map from './Map';
import { MapFunctionalitiesState } from './MapFunctionalitiesState';
import MapToolbar from './MapToolbar';
import { MapToolbarOption } from './MapToolbarOption';
import MapWmsLayer from './MapWmsLayer';
import OverviewPopup from './OverviewPopup';

interface BusinessLocationAreaMapState {

    mapFunctionalitiesState?: MapFunctionalitiesState;
    mapToolbarOption?: MapToolbarOption;
    properties?: Property[];
    alertText?: string;
    alertContext?: string;
    alertVisible: boolean;
    selectedFeatures: Feature[];

}

interface BusinessLocationAreaMapProps {
    centerPoint: {
        longitude: number;
        latitude: number;
    };
    archived: boolean;
    permissions: {canView: boolean, canEdit: boolean};
}

class BusinessLocationAreaMap extends React.Component<BusinessLocationAreaMapProps, BusinessLocationAreaMapState> {

    private readonly mapApi: MapApi;
    private readonly mapToolbarRefObject: React.RefObject<MapToolbar>;

    constructor(props: BusinessLocationAreaMapProps) {
        super(props);
        this.mapToolbarRefObject = React.createRef();
        this.state = {
            mapFunctionalitiesState: {
                pinLayerVisibility: false,
                polygonLayerVisibility: true,
                filterVisibility: false,
                translateInteraction: false,
                modifyInteraction: false,
                drawInteraction: false,
                setPinInteraction: false,
                drawBusinessLocationInteraction: false,
                businessLocationLayerVisibility: true,
                measureLayerVisibility: false,
                measureInteraction: false,
                wmsLayerVisibility: false,
                createObjectByClickInMap: false,
                propertyPopup: false,
                businessLocationReadonly: false,
            },
            mapToolbarOption: {
                pinView: false,
                polygonView: false,
                filterVisibility: false,
                drag: true,
                transform: true,
                draw: false,
                setPin: false,
                measure: true,
                wmsLayer: true,
                createObjectByClickInMap: false,
                print: true,
                delete: true,
                save: true,
                businessLocationDraw: true,
                businessLocationView: false,
            },
            properties: [],
            alertVisible: false,
            selectedFeatures: [],
        };

        this.mapApi = MapApi.getInstance();
    }

    private closeWmsCanvas = () => {
        this.setState({mapFunctionalitiesState: {...this.state.mapFunctionalitiesState, wmsLayerVisibility: false}});
    };

    private setLayerCanvasVisibility = (wmsLayerVisibility: boolean): void => {
        this.setState({
            mapFunctionalitiesState: {
                ...this.state.mapFunctionalitiesState,
                filterVisibility: false,
                wmsLayerVisibility,
            }
        });
    };

    private showMessage = (text: string, context?: string): void => {
        this.setState({alertText: text, alertContext: context, alertVisible: true});
        setTimeout(() => {
            this.setState({alertText: null, alertContext: null, alertVisible: false});
        }, 5000);
    };

    public componentDidMount(): void {
        this.mapApi.setCenter(fromLonLat([this.props.centerPoint.longitude, this.props.centerPoint.latitude]));
        this.mapApi.initializeBusinessLocations();

        this.mapApi.setPointLayerVisibility(this.state.mapFunctionalitiesState.pinLayerVisibility);
        this.mapApi.setPolygonLayerVisibility(this.state.mapFunctionalitiesState.polygonLayerVisibility);
        this.mapApi.setDrawInteraction(this.state.mapFunctionalitiesState.drawInteraction);
        this.mapApi.setTranslateInteraction(this.state.mapFunctionalitiesState.translateInteraction);
        this.mapApi.setModifyInteraction(this.state.mapFunctionalitiesState.modifyInteraction);
        this.mapApi.setMeasureLayerVisibility(this.state.mapFunctionalitiesState.measureLayerVisibility);
        this.mapApi.setBusinessLocationLayerVisibility(this.state.mapFunctionalitiesState.businessLocationLayerVisibility);
        this.mapApi.setMeasureInteraction(this.state.mapFunctionalitiesState.measureInteraction);

        this.mapApi.selectInteraction.on('select', (event): void => {
            this.setState({
                selectedFeatures: event.selected
            });
        });

        this.mapApi.map.addControl(new FullScreen);
    }

    public render(): JSX.Element {
        return (
            <Map>
                <>
                    {this.state.alertVisible &&
                    <div className={`alert alert-${this.state.alertContext ? this.state.alertContext : 'info'} mb-0`}>{this.state.alertText}</div>}
                    <OverviewPopup/>
                    <MapToolbar
                        ref={this.mapToolbarRefObject}
                        mapToolbarOption={this.state.mapToolbarOption}
                        mapFunctionalitiesState={this.state.mapFunctionalitiesState}
                        setWmsLayerVisibility={this.setLayerCanvasVisibility}
                        printMap={() => (this.mapApi.printDialog.print())}
                        selectedFeatures={this.state.selectedFeatures}
                        showMessage={this.showMessage}
                    />
                    <MapWmsLayer
                        showGlobalWmsLayer={true}
                        handleClose={this.closeWmsCanvas}
                        wmsLayerCanvasVisible={this.state.mapFunctionalitiesState.wmsLayerVisibility}
                    />
                </>
            </Map>
        );
    }

}

export default BusinessLocationAreaMap;
