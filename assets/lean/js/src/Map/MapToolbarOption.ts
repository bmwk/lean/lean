interface MapToolbarOption {

    pinView: boolean;
    polygonView: boolean;
    filterVisibility: boolean,
    drag: boolean;
    transform: boolean;
    draw: boolean;
    setPin: boolean;
    businessLocationDraw: boolean,
    businessLocationView: boolean,
    measure: boolean;
    wmsLayer: boolean;
    createObjectByClickInMap: boolean;
    print: boolean;
    delete: boolean;
    save: boolean;

}

export { MapToolbarOption };
