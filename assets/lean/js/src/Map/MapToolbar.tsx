import { MDCIconButtonToggle } from '@material/icon-button';
import { MDCMenu } from '@material/menu';
import { Feature } from 'ol';
import { Type } from 'ol/geom/Geometry';
import React from 'react';
import { MapApi } from '../MapApi/MapApi';
import { Property } from '../MapApi/Property';
import { MapFunctionalitiesState } from './MapFunctionalitiesState';
import { MapToolbarOption } from './MapToolbarOption';
import Tooltip from './Tooltip';

interface MapToolbarProps {

    mapToolbarOption: MapToolbarOption;
    mapFunctionalitiesState: MapFunctionalitiesState;
    setFilterVisibility?: Function;
    setWmsLayerVisibility: Function;
    setCreateObjectByClickInMap?: Function;
    setSetPin?: Function;
    setDrawOption?: Function;
    printMap: Function;
    selectedFeatures: Feature[];
    showMessage: Function;
}

interface BusinessLocation {
    id: number;
    name: string;
}

class MapToolbar extends React.Component<MapToolbarProps, any> {

    private readonly pinViewButtonRefObject: React.RefObject<HTMLButtonElement>;
    private readonly polygonViewButtonRefObject: React.RefObject<HTMLButtonElement>;
    private readonly businessLocationViewButtonRefObject: React.RefObject<HTMLButtonElement>;
    private readonly filterVisibilityButtonRefObject: React.RefObject<HTMLButtonElement>;
    private readonly dragButtonRefObject: React.RefObject<HTMLButtonElement>;
    private readonly transformButtonRefObject: React.RefObject<HTMLButtonElement>;
    private readonly drawButtonRefObject: React.RefObject<HTMLButtonElement>;
    private readonly setPinButtonRefObject: React.RefObject<HTMLButtonElement>;
    private readonly businessLocationDrawButtonRefObject: React.RefObject<HTMLButtonElement>;
    private readonly businessLocationDrawMenuContainerRefObject: React.RefObject<HTMLDivElement>;
    private readonly measureButtonRefObject: React.RefObject<HTMLButtonElement>;
    private readonly measureMenuContainerRefObject: React.RefObject<HTMLDivElement>;
    private readonly wmsLayerButtonRefObject: React.RefObject<HTMLButtonElement>;
    private readonly createObjectByClickInMapButtonRefObject: React.RefObject<HTMLButtonElement>;
    private readonly printButtonRefObject: React.RefObject<HTMLButtonElement>;
    private readonly deleteButtonRefObject: React.RefObject<HTMLButtonElement>;
    private readonly saveButtonRefObject: React.RefObject<HTMLButtonElement>;

    private pinViewMdcIconButtonToggle: MDCIconButtonToggle;
    private polygonViewMdcIconButtonToggle: MDCIconButtonToggle;
    private businessLocationViewMdcIconButtonToggle: MDCIconButtonToggle;
    private filterVisibilityMdcIconButtonToggle: MDCIconButtonToggle;
    private dragMdcIconButtonToggle: MDCIconButtonToggle;
    private transformMdcIconButtonToggle: MDCIconButtonToggle;
    private drawMdcIconButtonToggle: MDCIconButtonToggle;
    private setPinMdcIconButtonToggle: MDCIconButtonToggle;
    private measureMdcIconButtonToggle: MDCIconButtonToggle;
    private measureMdcMenu: MDCMenu;
    private businessLocationDrawMdcIconButtonToggle: MDCIconButtonToggle;
    private businessLocationDrawMdcMenu: MDCMenu;
    private wmsLayerMdcIconButtonToggle: MDCIconButtonToggle;
    private createObjectByClickInMapMdcIconButtonToggle: MDCIconButtonToggle;

    private businessLocations: BusinessLocation[];

    private mapApi = MapApi.getInstance();

    public constructor(props: MapToolbarProps) {
        super(props);

        this.pinViewButtonRefObject = React.createRef();
        this.polygonViewButtonRefObject = React.createRef();
        this.businessLocationViewButtonRefObject = React.createRef();
        this.filterVisibilityButtonRefObject = React.createRef();
        this.dragButtonRefObject = React.createRef();
        this.transformButtonRefObject = React.createRef();
        this.drawButtonRefObject = React.createRef();
        this.setPinButtonRefObject = React.createRef();
        this.businessLocationDrawButtonRefObject = React.createRef();
        this.businessLocationDrawMenuContainerRefObject = React.createRef();
        this.measureButtonRefObject = React.createRef();
        this.measureMenuContainerRefObject = React.createRef();
        this.wmsLayerButtonRefObject = React.createRef();
        this.createObjectByClickInMapButtonRefObject = React.createRef();
        this.deleteButtonRefObject = React.createRef();
        this.saveButtonRefObject = React.createRef();

        this.businessLocations = [
            {id: 0, name: '1A'},
            {id: 2, name: '1B'},
            {id: 5, name: '1C'},
            {id: 1, name: '2A'},
            {id: 3, name: '2B'},
            {id: 6, name: '2C'},
            {id: 4, name: 'Sonstige'},
        ];
    }

    public componentDidMount(): void {
        this.pinViewMdcIconButtonToggle = new MDCIconButtonToggle(this.pinViewButtonRefObject.current);
        this.pinViewMdcIconButtonToggle.on = this.props.mapFunctionalitiesState.pinLayerVisibility;

        this.polygonViewMdcIconButtonToggle = new MDCIconButtonToggle(this.polygonViewButtonRefObject.current);
        this.polygonViewMdcIconButtonToggle.on = this.props.mapFunctionalitiesState.polygonLayerVisibility;

        this.businessLocationViewMdcIconButtonToggle = new MDCIconButtonToggle(this.businessLocationViewButtonRefObject.current);
        this.businessLocationViewMdcIconButtonToggle.on = this.props.mapFunctionalitiesState.businessLocationLayerVisibility;

        this.filterVisibilityMdcIconButtonToggle = new MDCIconButtonToggle(this.filterVisibilityButtonRefObject.current);
        this.filterVisibilityMdcIconButtonToggle.on = this.props.mapFunctionalitiesState.filterVisibility;

        this.dragMdcIconButtonToggle = new MDCIconButtonToggle(this.dragButtonRefObject.current);
        this.dragMdcIconButtonToggle.on = this.props.mapFunctionalitiesState.translateInteraction;

        this.transformMdcIconButtonToggle = new MDCIconButtonToggle(this.transformButtonRefObject.current);
        this.transformMdcIconButtonToggle.on = this.props.mapFunctionalitiesState.translateInteraction;

        this.drawMdcIconButtonToggle = new MDCIconButtonToggle(this.drawButtonRefObject.current);
        this.drawMdcIconButtonToggle.on = this.props.mapFunctionalitiesState.drawInteraction;

        this.measureMdcIconButtonToggle = new MDCIconButtonToggle(this.measureButtonRefObject.current);
        this.measureMdcMenu = new MDCMenu(this.measureMenuContainerRefObject.current);
        this.measureMdcIconButtonToggle.on = this.props.mapFunctionalitiesState.measureInteraction;

        this.businessLocationDrawMdcIconButtonToggle = new MDCIconButtonToggle(this.businessLocationDrawButtonRefObject.current);
        this.businessLocationDrawMdcMenu = new MDCMenu(this.businessLocationDrawMenuContainerRefObject.current);
        this.businessLocationDrawMdcIconButtonToggle.on = this.props.mapFunctionalitiesState.drawInteraction;

        this.wmsLayerMdcIconButtonToggle = new MDCIconButtonToggle(this.wmsLayerButtonRefObject.current);

        this.createObjectByClickInMapMdcIconButtonToggle = new MDCIconButtonToggle(this.createObjectByClickInMapButtonRefObject.current);
        this.createObjectByClickInMapMdcIconButtonToggle.on = this.props.mapFunctionalitiesState.createObjectByClickInMap;

        this.mapApi.getDrawInteraction().on('drawend', () => {
            this.removeDraw();
        });

        this.deleteButtonRefObject.current.setAttribute('disabled', 'true');
    }

    public componentWillUnmount(): void {
        this.pinViewMdcIconButtonToggle.destroy();
        this.polygonViewMdcIconButtonToggle.destroy();
        this.businessLocationViewMdcIconButtonToggle.destroy();
        this.filterVisibilityMdcIconButtonToggle.destroy();
        this.measureMdcIconButtonToggle.destroy();
        this.dragMdcIconButtonToggle.destroy();
        this.drawMdcIconButtonToggle.destroy();
        this.transformMdcIconButtonToggle.destroy();
        this.businessLocationDrawMdcIconButtonToggle.destroy();
        this.wmsLayerMdcIconButtonToggle.destroy();
        this.createObjectByClickInMapMdcIconButtonToggle.destroy();

    }

    public componentDidUpdate(prevProps: Readonly<MapToolbarProps>, prevState: Readonly<any>, snapshot?: any) {
        if (this.props.selectedFeatures?.length > 0) {
            this.deleteButtonRefObject.current.removeAttribute('disabled');
        } else {
            this.deleteButtonRefObject.current.setAttribute('disabled', 'true');
        }
        if (this.props.mapToolbarOption.setPin && this.setPinMdcIconButtonToggle === undefined) {
            this.setPinMdcIconButtonToggle = new MDCIconButtonToggle(this.setPinButtonRefObject.current);
            this.setPinMdcIconButtonToggle.on = this.props.mapFunctionalitiesState.setPinInteraction;
        }
        this.wmsLayerMdcIconButtonToggle.on = this.props.mapFunctionalitiesState.wmsLayerVisibility;
        this.filterVisibilityMdcIconButtonToggle.on = this.props.mapFunctionalitiesState.filterVisibility;
    }

    private savePinLayerVisibilityToLocalStorage(isVisible: boolean): void {
        localStorage.setItem('lean.map.pinLayerVisibility', JSON.stringify(isVisible));
    }

    private savePolygonLayerVisibilityToLocalStorage(isVisible: boolean): void {
        localStorage.setItem('lean.map.polygonLayerVisibility', JSON.stringify(isVisible));
    }

    public removeDraw = (): void => {
        this.drawMdcIconButtonToggle.on = false;
        this.props.setDrawOption(false)
        this.mapApi.setDrawInteraction(false);
    };

    public addDraw = () => {
        this.props.setDrawOption(true)
    };

    private handlePinViewClick = (): void => {
        this.mapApi.setPointLayerVisibility(this.pinViewMdcIconButtonToggle.on);
        this.savePinLayerVisibilityToLocalStorage(this.pinViewMdcIconButtonToggle.on);
    };

    private handlePolygonViewClick = (): void => {
        this.mapApi.setPolygonLayerVisibility(this.polygonViewMdcIconButtonToggle.on);
        this.savePolygonLayerVisibilityToLocalStorage(this.polygonViewMdcIconButtonToggle.on);
    };

    private handleBusinessLocationViewClick = (): void => {
        this.mapApi.setBusinessLocationLayerVisibility(this.businessLocationViewMdcIconButtonToggle.on);
    };

    private handleFilterVisibilityClick = (): void => {
        this.props.setFilterVisibility(!this.props.mapFunctionalitiesState.filterVisibility);
    };

    private handleLayerVisibilityClick = (): void => {
        this.props.setWmsLayerVisibility(!this.props.mapFunctionalitiesState.wmsLayerVisibility);
    };

    private handleDragClick = (): void => {
        this.mapApi.setTranslateInteraction(this.dragMdcIconButtonToggle.on);
    };

    private handleTransformClick = (): void => {
        this.mapApi.setModifyInteraction(this.transformMdcIconButtonToggle.on);
    };

    private handleDrawClick = (): void => {
        this.mapApi.setDrawInteraction(this.drawMdcIconButtonToggle.on);
    };

    private handleSetPinClick = (): void => {
        this.props.setSetPin(!this.props.mapFunctionalitiesState.setPinInteraction);
    };

    private handleMeasureClick = (): void => {
        if (this.measureMdcIconButtonToggle.on !== true) {
            this.mapApi.setMeasureLayerVisibility(false);
            this.mapApi.setMeasureInteraction(false);

            return;
        }

        this.mapApi.setMeasureLayerVisibility(true);
        this.measureMdcMenu.open = true;
    };

    private handleBusinessLocationDrawClick = (): void => {
        if (this.businessLocationDrawMdcIconButtonToggle.on !== true) {
            this.mapApi.setDrawBusinessLocationInteraction(false);
            return;
        }

        this.businessLocationDrawMdcMenu.open = true;
    };

    private handleBusinessLocationDrawSelectClick = (businessLocationId: number): void => {
        this.mapApi.setDrawBusinessLocationInteraction(true, businessLocationId);
    };

    private handleMeasureTypeSelectClick = (measureType: Type): void => {
        this.mapApi.initializeMeasureInteraction(measureType);
        this.mapApi.setMeasureInteraction(true);
    };

    private handleCreateObjectByClickInMapClick = (): void => {
        this.props.setCreateObjectByClickInMap(!this.props.mapFunctionalitiesState.createObjectByClickInMap);
    };

    private handlePrintClick = (): void => {
        this.props.printMap();
    };

    private handleDeleteClick = async (): Promise<void> => {
        if (this.props.selectedFeatures[0].getGeometry().getType() === 'Polygon') {
            this.mapApi.removeFeatures(this.props.selectedFeatures);

            const polygonId: number = this.props.selectedFeatures[0].get('polygonId');

            if (polygonId) await this.mapApi.deletePolygonById(this.props.selectedFeatures[0].get('id'), polygonId);

            const businessLocationId = this.props.selectedFeatures[0].get('businessLocationId');

            if (businessLocationId) await this.mapApi.deleteBusinessLocationById(businessLocationId);

            this.mapApi.selectInteraction.getFeatures().clear();
            this.mapApi.selectInteraction.dispatchEvent('select');

            if (this.props.mapToolbarOption.draw && this.mapApi.getPolygonFeatures().length <= 0) {
                this.addDraw();
            }

            this.props.showMessage('Umriss wurde gelöscht', 'success');
        }
    };

    private handleSaveClick = async (): Promise<void> => {
        try {
            document.body.style.setProperty('cursor', 'wait', 'important');
            this.saveButtonRefObject.current.style.setProperty('cursor', 'wait', 'important');

            const promises = [];
            const propertyId = (this.mapApi.getPropertyMappings()[0]?.property as Property)?.id;

            if (propertyId) {
                if (this.mapApi.getPointFeatures().length > 0) {
                    promises.push(this.mapApi.updateBuildingUnitPoint(this.mapApi.getPointFeatures()[0], propertyId));
                }

                if (this.mapApi.getPolygonFeatures().length > 0) {
                    promises.push(this.mapApi.updateBuildingUnitPolygon(this.mapApi.getPolygonFeatures()[0], propertyId));
                }
            }

            if (this.props.mapToolbarOption.businessLocationDraw === true) {
                const businessLocations = this.mapApi.getBusinessLocationFeatures();

                for (let i = 0; i < businessLocations.length; i++) {
                    const businessLocationId = businessLocations[i].get('businessLocationId');
                    if (businessLocationId) {
                        promises.push(this.mapApi.updateBusinessLocationPolygon(businessLocations[i]));
                    } else {
                        promises.push(this.mapApi.postBusinessLocationPolygon(businessLocations[i]));
                    }
                }
            }

            await Promise.all(promises);
            this.props.showMessage('Änderungen wurden gespeichert', 'success');
        } catch (error) {
            console.log(error);
        } finally {
            document.body.style.setProperty('cursor', '');
            this.saveButtonRefObject.current.style.setProperty('cursor', '');
        }
    };

    public render(): JSX.Element {
        return (
            <>
                <div className="map-toolbar">
                    <div className="d-flex">
                        <button ref={this.pinViewButtonRefObject} onClick={this.handlePinViewClick}
                                style={{display: this.props.mapToolbarOption.pinView ? 'flex' : 'none'}}
                                className="mdc-icon-button align-items-center"
                                aria-describedby="map_toolbar_pin_view_tooltip">
                            <i className="material-icons mdc-icon-button__icon">room</i>
                            <i className="material-icons mdc-icon-button__icon mdc-icon-button__icon--on">room</i>
                        </button>

                        <button ref={this.polygonViewButtonRefObject} onClick={this.handlePolygonViewClick}
                                style={{display: this.props.mapToolbarOption.polygonView ? 'flex' : 'none'}}
                                className="mdc-icon-button align-items-center"
                                aria-describedby="map_toolbar_polygonView_tooltip">
                            <i className="material-icons mdc-icon-button__icon">format_shapes</i>
                            <i className="material-icons mdc-icon-button__icon mdc-icon-button__icon--on">format_shapes</i>
                        </button>

                        <button ref={this.businessLocationViewButtonRefObject}
                                onClick={this.handleBusinessLocationViewClick}
                                style={{display: this.props.mapToolbarOption.businessLocationView ? 'flex' : 'none'}}
                                className="mdc-icon-button align-items-center"
                                aria-describedby="map_toolbar_businessLocationView_tooltip">
                            <i className="material-icons mdc-icon-button__icon">business</i>
                            <i className="material-icons mdc-icon-button__icon mdc-icon-button__icon--on">business</i>
                        </button>
                    </div>

                    <div className="d-flex">
                        <button ref={this.filterVisibilityButtonRefObject} onClick={this.handleFilterVisibilityClick}
                                style={{display: this.props.mapToolbarOption.filterVisibility ? 'flex' : 'none'}}
                                className="mdc-icon-button align-items-center"
                                aria-describedby="map_toolbar_filter_visibility_tooltip"
                                aria-controls="map_filter_offcanvas">
                            <i className="material-icons mdc-icon-button__icon">filter_alt</i>
                            <i className="material-icons mdc-icon-button__icon mdc-icon-button__icon--on">filter_alt</i>
                        </button>

                        <button ref={this.wmsLayerButtonRefObject} className="mdc-icon-button align-items-center"
                                style={{display: this.props.mapToolbarOption.wmsLayer ? 'flex' : 'none'}}
                                aria-describedby="map_toolbar_wms_layers_tooltip" aria-controls="map_wms_layer_offcanvas"
                                onClick={this.handleLayerVisibilityClick}>
                            <i className="material-icons mdc-icon-button__icon">layers</i>
                            <i className="material-icons mdc-icon-button__icon mdc-icon-button__icon--on">layers</i>
                        </button>
                    </div>

                    <div className="d-flex">
                        <button ref={this.dragButtonRefObject} onClick={this.handleDragClick}
                                style={{display: this.props.mapToolbarOption.drag ? 'flex' : 'none'}}
                                className="mdc-icon-button align-items-center"
                                aria-describedby="map_toolbar_drag_tooltip">
                            <i className="material-icons mdc-icon-button__icon">open_with</i>
                            <i className="material-icons mdc-icon-button__icon mdc-icon-button__icon--on">open_with</i>
                        </button>

                        <button ref={this.transformButtonRefObject} onClick={this.handleTransformClick}
                                style={{display: this.props.mapToolbarOption.transform ? 'flex' : 'none'}}
                                className="mdc-icon-button align-items-center"
                                aria-describedby="map_toolbar_transform_tooltip">
                            <i className="material-icons mdc-icon-button__icon">transform</i>
                            <i className="material-icons mdc-icon-button__icon mdc-icon-button__icon--on">transform</i>
                        </button>

                        <button ref={this.drawButtonRefObject} onClick={this.handleDrawClick}
                                style={{display: this.props.mapToolbarOption.draw ? 'flex' : 'none'}}
                                className="mdc-icon-button align-items-center"
                                aria-describedby="map_toolbar_draw_tooltip">
                            <i className="material-icons mdc-icon-button__icon">gesture</i>
                            <i className="material-icons mdc-icon-button__icon mdc-icon-button__icon--on">gesture</i>
                        </button>

                        <div className="mdc-menu-surface--anchor"
                             style={{display: this.props.mapToolbarOption.measure ? 'inline-block' : 'none'}}>
                            <button ref={this.measureButtonRefObject} onClick={this.handleMeasureClick}
                                    className="mdc-icon-button d-flex align-items-center"
                                    aria-describedby="map_toolbar_measure_tooltip">
                                <i className="material-icons mdc-icon-button__icon">square_foot</i>
                                <i className="material-icons mdc-icon-button__icon mdc-icon-button__icon--on">square_foot</i>
                            </button>
                            <div ref={this.measureMenuContainerRefObject} className="mdc-menu mdc-menu-surface">
                                <ul className="mdc-list" role="menu" aria-hidden="true" aria-orientation="vertical"
                                    tabIndex={-1}>
                                    <li>
                                        <ul className="mdc-menu__selection-group">
                                            <li onClick={(e) => this.handleMeasureTypeSelectClick('LineString')}
                                                className="mdc-list-item" role="menuitem">
                                                <span className="mdc-list-item__ripple"/>
                                                <span className="mdc-list-item__text">Distanz</span>
                                            </li>
                                            <li onClick={(e) => this.handleMeasureTypeSelectClick('Polygon')}
                                                className="mdc-list-item" role="menuitem">
                                                <span className="mdc-list-item__ripple"/>
                                                <span className="mdc-list-item__text">Fläche</span>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div className="mdc-menu-surface--anchor"
                             style={{display: this.props.mapToolbarOption.businessLocationDraw ? 'inline-block' : 'none'}}>
                            <button ref={this.businessLocationDrawButtonRefObject}
                                    onClick={this.handleBusinessLocationDrawClick}
                                    className="mdc-icon-button d-flex align-items-center"
                                    aria-describedby="map_toolbar_businessLocationDraw_tooltip">
                                <i className="material-icons mdc-icon-button__icon">gesture</i>
                                <i className="material-icons mdc-icon-button__icon mdc-icon-button__icon--on">gesture</i>
                            </button>
                            <div ref={this.businessLocationDrawMenuContainerRefObject}
                                 className="mdc-menu mdc-menu-surface">
                                <ul className="mdc-list" role="menu" aria-hidden="true" aria-orientation="vertical"
                                    tabIndex={-1}>
                                    <li>
                                        <ul className="mdc-menu__selection-group">
                                            {
                                                this.businessLocations.map((location: BusinessLocation) => {
                                                    return (
                                                        <li onClick={(e) => this.handleBusinessLocationDrawSelectClick(location.id)}
                                                            className="mdc-list-item py-2" role="menuitem"
                                                            key={location.id}>
                                                            <span className="mdc-list-item__ripple"/>
                                                            <span className="mdc-list-item__text">
                                                            {location.name !== 'Sonstige' ? location.name + '-Lage' : location.name}
                                                        </span>
                                                        </li>
                                                    );
                                                })
                                            }
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <button ref={this.setPinButtonRefObject} onClick={this.handleSetPinClick}
                                style={{display: this.props.mapToolbarOption.setPin ? 'flex' : 'none'}}
                                className="mdc-icon-button align-items-center"
                                aria-describedby="map_toolbar_set_pin_tooltip">
                            <i className="material-icons mdc-icon-button__icon">add_location</i>
                            <i className="material-icons mdc-icon-button__icon mdc-icon-button__icon--on">add_location</i>
                        </button>

                        <button ref={this.createObjectByClickInMapButtonRefObject}
                                style={{display: this.props.mapToolbarOption.createObjectByClickInMap ? 'flex' : 'none'}}
                                onClick={this.handleCreateObjectByClickInMapClick}
                                className="mdc-icon-button align-items-center"
                                aria-describedby="map_toolbar_create_object_by_click_in_map_tooltip">
                            <i className="material-icons mdc-icon-button__icon">add_location</i>
                            <i className="material-icons mdc-icon-button__icon mdc-icon-button__icon--on">add_location</i>
                        </button>

                        <button ref={this.printButtonRefObject} onClick={this.handlePrintClick}
                                style={{display: this.props.mapToolbarOption.print ? 'flex' : 'none'}}
                                className="mdc-icon-button align-items-center"
                                aria-describedby="map_toolbar_print_tooltip">
                            <i className="material-icons mdc-icon-button__icon">print</i>
                            <i className="material-icons mdc-icon-button__icon mdc-icon-button__icon--on">print</i>
                        </button>

                        <button ref={this.deleteButtonRefObject} onClick={this.handleDeleteClick}
                                style={{display: this.props.mapToolbarOption.delete ? 'flex' : 'none'}}
                                className="mdc-icon-button align-items-center"
                                aria-describedby="map_toolbar_delete_tooltip">
                            <i className="material-icons">delete_forever</i>
                        </button>

                        <button ref={this.saveButtonRefObject} onClick={this.handleSaveClick}
                                style={{display: this.props.mapToolbarOption.save ? 'flex' : 'none'}}
                                className="mdc-icon-button align-items-center"
                                aria-describedby="map_toolbar_save_tooltip">
                            <i className="material-icons">save</i>
                        </button>
                    </div>
                </div>

                <div className="map-create-object-by-click-in-map-notification"
                     style={{display: this.props.mapFunctionalitiesState.createObjectByClickInMap ? 'inline-block' : 'none'}}>
                    <div className="mdc-card p-2">
                        <span className="d-lg-inline">Klicken Sie nun mit der Maus auf die Lage des zu erfassenden Objekts um den Pin-Point zu setzen und mit der Erfassung fortzufahren.</span>
                    </div>
                </div>

                <Tooltip id={'map_toolbar_pin_view_tooltip'} value={'Pin-Ansicht'}/>
                <Tooltip id={'map_toolbar_polygonView_tooltip'} value={'Umrisse-Ansicht'}/>
                <Tooltip id={'map_toolbar_businessLocationView_tooltip'} value={'Geschäftslagen'}/>
                <Tooltip id={'map_toolbar_filter_visibility_tooltip'} value={'Filter'}/>
                <Tooltip id={'map_toolbar_drag_tooltip'} value={'Verschieben'}/>
                <Tooltip id={'map_toolbar_transform_tooltip'} value={'Modifizieren'}/>
                <Tooltip id={'map_toolbar_draw_tooltip'} value={'Zeichnen'}/>
                <Tooltip id={'map_toolbar_set_pin_tooltip'} value={'Pin setzen'}/>
                <Tooltip id={'map_toolbar_businessLocationDraw_tooltip'} value={'Geschäftslage einzeichnen'}/>
                <Tooltip id={'map_toolbar_measure_tooltip'} value={'Messen'}/>
                <Tooltip id={'map_toolbar_wms_layers_tooltip'} value={'Ebenen'}/>
                <Tooltip id={'map_toolbar_create_object_by_click_in_map_tooltip'} value={'Objekt erfassen'}/>
                <Tooltip id={'map_toolbar_print_tooltip'} value={'Karte drucken'}/>
                <Tooltip id={'map_toolbar_delete_tooltip'} value={'Löschen'}/>
                <Tooltip id={'map_toolbar_save_tooltip'} value={'Speichern'}/>
            </>
        );
    }

}

export default MapToolbar;
