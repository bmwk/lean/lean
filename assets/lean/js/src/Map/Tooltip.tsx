import { MDCTooltip } from '@material/tooltip';
import React from 'react';

interface TooltipState {

}

interface TooltipProps {

    id: string;
    value: string;

}

class Tooltip extends React.Component<TooltipProps, TooltipState> {

    private readonly tooltipRefObject: React.RefObject<HTMLDivElement>;
    private mdcTooltip: MDCTooltip;

    public constructor(props: TooltipProps) {
        super(props);

        this.tooltipRefObject = React.createRef();
    }

    public componentDidMount(): void {
        this.mdcTooltip = new MDCTooltip(this.tooltipRefObject.current);

        this.mdcTooltip.setHideDelay(100);
    }

    public render(): JSX.Element {
        return (
            <div ref={this.tooltipRefObject} id={this.props.id} className="mdc-tooltip" role="tooltip" aria-hidden="true">
                <div className="mdc-tooltip__surface mdc-tooltip__surface-animation">
                    {this.props.value}
                </div>
            </div>
        );
    }
}

export default Tooltip;
