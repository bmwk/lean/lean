interface MapFunctionalitiesState {

    pinLayerVisibility: boolean;
    polygonLayerVisibility: boolean;
    filterVisibility: boolean;
    translateInteraction: boolean;
    modifyInteraction: boolean;
    drawInteraction: boolean;
    setPinInteraction: boolean;
    drawBusinessLocationInteraction: boolean;
    businessLocationLayerVisibility: boolean;
    businessLocationReadonly: boolean;
    measureLayerVisibility: boolean;
    measureInteraction: boolean;
    wmsLayerVisibility: boolean;
    createObjectByClickInMap: boolean;
    propertyPopup: boolean;
    scoredPropertyPopup?: boolean;
}

export { MapFunctionalitiesState };
