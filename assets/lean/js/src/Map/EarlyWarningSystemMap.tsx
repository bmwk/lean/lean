import { Feature } from 'ol';
import { fromLonLat } from 'ol/proj';
import React from 'react';
import { MapApi } from '../MapApi/MapApi';
import { ScoredProperty } from '../MapApi/Property';
import Map from './Map';
import MapEarlyWarningSystemCanvas from './MapEarlyWarningSystemCanvas';
import { MapFunctionalitiesState } from './MapFunctionalitiesState';
import MapPropertyFilter from './MapPropertyFilter';
import MapToolbar from './MapToolbar';
import { MapToolbarOption } from './MapToolbarOption';
import MapWmsLayer from './MapWmsLayer';
import OverviewPopup from './OverviewPopup';

interface EarlyWarningSystemMapState {

    mapFunctionalitiesState?: MapFunctionalitiesState;
    mapToolbarOption?: MapToolbarOption;
    properties?: ScoredProperty[];
    selectedFeatures: Feature[];
    alertText?: string;
    alertContext?: string;
    alertVisible: boolean;
    selectedPolygonId: number;
    step: number;

}

interface EarlyWarningSystemMapProps {

    centerPoint: {
        longitude: number;
        latitude: number;
    };
    archived: boolean;
    permissions: {canView: boolean, canEdit: boolean};
}

class EarlyWarningSystemMap extends React.Component<EarlyWarningSystemMapProps, EarlyWarningSystemMapState> {

    private readonly mapApi: MapApi;
    private readonly mapToolbarRefObject: React.RefObject<MapToolbar>;

    constructor(props: EarlyWarningSystemMapProps) {
        super(props);

        this.state = {
            mapFunctionalitiesState: {
                pinLayerVisibility: true,
                polygonLayerVisibility: false,
                filterVisibility: false,
                translateInteraction: false,
                modifyInteraction: false,
                drawInteraction: false,
                setPinInteraction: false,
                drawBusinessLocationInteraction: false,
                businessLocationLayerVisibility: true,
                measureLayerVisibility: false,
                measureInteraction: false,
                wmsLayerVisibility: false,
                createObjectByClickInMap: false,
                propertyPopup: false,
                businessLocationReadonly: true,
                scoredPropertyPopup: true,
            },
            mapToolbarOption: {
                pinView: false,
                polygonView: false,
                filterVisibility: true,
                drag: false,
                transform: false,
                draw: false,
                setPin: false,
                measure: true,
                wmsLayer: true,
                createObjectByClickInMap: false,
                print: true,
                delete: false,
                save: false,
                businessLocationDraw: false,
                businessLocationView: false,
            },
            properties: [],
            selectedFeatures: [],
            alertVisible: false,
            selectedPolygonId: null,
            step: 1
        };

        this.mapApi = MapApi.getInstance();
    }

    private setFilterCanvasVisibility = (filterVisibility: boolean): void => {
        this.setState({
            mapFunctionalitiesState: {
                ...this.state.mapFunctionalitiesState,
                wmsLayerVisibility: false,
                filterVisibility,
            }
        });
    };

    private closeWmsCanvas = () => {
        this.setState({mapFunctionalitiesState: {...this.state.mapFunctionalitiesState, wmsLayerVisibility: false}});
    };

    private closeFilterCanvas = () => {
        this.setState({mapFunctionalitiesState: {...this.state.mapFunctionalitiesState, filterVisibility: false}});
    };

    private setLayerCanvasVisibility = (wmsLayerVisibility: boolean): void => {
        this.setState({
            mapFunctionalitiesState: {
                ...this.state.mapFunctionalitiesState,
                filterVisibility: false,
                wmsLayerVisibility,
            }
        });
    };

    private showMessage = (text: string, context?: string): void => {
        this.setState({alertText: text, alertContext: context, alertVisible: true});
        setTimeout(() => {
            this.setState({alertText: null, alertContext: null, alertVisible: false});
        }, 5000);
    };

    private setCreateObjectByClickInMap = (createObjectByClickInMap: boolean): void => {
        this.setState({
            mapFunctionalitiesState: {...this.state.mapFunctionalitiesState, createObjectByClickInMap}
        });
    };

    public componentDidMount(): void {
        MapApi.fetchEnrichedBuildingUnitsFromApi().then((properties: ScoredProperty[]) => {
            this.mapApi.initializeProperties(properties);
            this.setState({properties: properties});
        });
        this.mapApi.initializeScoredBusinessLocations();

        this.mapApi.setCenter(fromLonLat([this.props.centerPoint.longitude, this.props.centerPoint.latitude]));
        this.mapApi.initializeBusinessLocations();

        this.mapApi.initializePopup();
        this.mapApi.initializeOverviewPopup(document.getElementById('overviewDialog'));

        this.mapApi.setPointLayerVisibility(this.state.mapFunctionalitiesState.pinLayerVisibility);
        this.mapApi.setPolygonLayerVisibility(this.state.mapFunctionalitiesState.polygonLayerVisibility);
        this.mapApi.setDrawInteraction(this.state.mapFunctionalitiesState.drawInteraction);
        this.mapApi.setTranslateInteraction(this.state.mapFunctionalitiesState.translateInteraction);
        this.mapApi.setModifyInteraction(this.state.mapFunctionalitiesState.modifyInteraction);
        this.mapApi.setMeasureLayerVisibility(this.state.mapFunctionalitiesState.measureLayerVisibility);
        this.mapApi.setBusinessLocationLayerVisibility(this.state.mapFunctionalitiesState.businessLocationLayerVisibility);
        this.mapApi.setMeasureInteraction(this.state.mapFunctionalitiesState.measureInteraction);
    }

    public render(): JSX.Element {
        return (
            <div className="position-relative h-100">
                <Map isEarlyWarningSystemMap={true}>
                    <>
                        {this.state.alertVisible &&
                        <div className={`alert alert-${this.state.alertContext ? this.state.alertContext : 'info'} mb-0`}>{this.state.alertText}</div>}
                        <OverviewPopup/>
                        <MapToolbar
                            ref={this.mapToolbarRefObject}
                            mapToolbarOption={this.state.mapToolbarOption}
                            mapFunctionalitiesState={this.state.mapFunctionalitiesState}
                            setFilterVisibility={this.setFilterCanvasVisibility}
                            setWmsLayerVisibility={this.setLayerCanvasVisibility}
                            setCreateObjectByClickInMap={this.setCreateObjectByClickInMap}
                            printMap={() => (this.mapApi.printDialog.print())}
                            selectedFeatures={this.state.selectedFeatures}
                            showMessage={this.showMessage}
                        />
                        <MapWmsLayer
                            handleClose={this.closeWmsCanvas}
                            showGlobalWmsLayer={true}
                            wmsLayerCanvasVisible={this.state.mapFunctionalitiesState.wmsLayerVisibility}
                        />
                        <MapPropertyFilter
                            selectedPolygonId={this.state.selectedPolygonId}
                            isEarlyWarningSystem={true}
                            showAccountUserFilter={true}
                            handleClose={this.closeFilterCanvas}
                            properties={this.state.properties}
                            filterCanvasVisible={this.state.mapFunctionalitiesState.filterVisibility}
                        />
                        <div id="legend" className="mdc-card position-absolute">
                            <div className="collapse" id="legend-content">
                                {this.state.step === 1 && <div className="px-3 py-1">
                                    <ul className="mdc-list">
                                        <li><span className="usage-color-indicator" style={{backgroundColor: 'rgb(255, 0, 0)'}}></span> 1A-Lage</li>
                                        <li><span className="usage-color-indicator" style={{backgroundColor: 'rgb(0, 0, 255)'}}></span> 1B-Lage</li>
                                        <li><span className="usage-color-indicator" style={{backgroundColor: 'rgb(0, 255, 0)'}}></span> 1C-Lage</li>
                                        <li><span className="usage-color-indicator" style={{backgroundColor: 'rgb(100, 0, 0)'}}></span> 2A-Lage</li>
                                        <li><span className="usage-color-indicator" style={{backgroundColor: 'rgb(0, 0, 100)'}}></span> 2B-Lage</li>
                                        <li><span className="usage-color-indicator" style={{backgroundColor: 'rgb(0, 100, 0)'}}></span> 2C-Lage</li>
                                    </ul>
                                </div>}
                                {this.state.step === 2 && <div className="p-2">
                                    <table>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <svg width="15px" height="22.5px" viewBox="0 0 30 45" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M0 15 L15 45 L30 15 z" fill="#014A5D"/>
                                                        <circle cx="15" cy="15" r="15" fill="#014A5D"/>
                                                    </svg>
                                                </td>
                                                <td>
                                                    In LeAn erfasstes Objekt
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <svg width="15px" height="22.5px" viewBox="0 0 30 45" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M0 0 L0 30 L15 45 L30 30 L30 0 z" fill="#014A5D"/>
                                                        <circle cx="15" cy="15" r="15" fill="#014A5D"/>
                                                    </svg>
                                                </td>
                                                <td>
                                                    Nicht in LeAn erfasstes Objekt
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span className="usage-color-indicator" style={{backgroundColor: MapApi.getPressureClassificationColors().success}}></span>
                                                </td>
                                                <td>
                                                    Objekt ist attraktiv
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span className="usage-color-indicator" style={{backgroundColor: MapApi.getPressureClassificationColors().warning}}></span>
                                                </td>
                                                <td>
                                                    Objekt ist mäßig attraktiv
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span className="usage-color-indicator" style={{backgroundColor: MapApi.getPressureClassificationColors().danger}}></span>
                                                </td>
                                                <td>
                                                    Objekt ist wenig attraktiv
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span className="usage-color-indicator" style={{backgroundColor: MapApi.getPressureClassificationColors().undefined}}></span>
                                                </td>
                                                <td>
                                                    Objekt steht leer
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>}
                                {this.state.step === 3 && <div className="p-2">
                                    <table>
                                        <tbody>
                                        <tr>
                                            <td>
                                                <span className="usage-color-indicator" style={{backgroundColor: MapApi.getPressureClassificationColors().success}}></span>
                                            </td>
                                            <td>
                                                Lage ist attraktiv
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span className="usage-color-indicator" style={{backgroundColor: MapApi.getPressureClassificationColors().warning}}></span>
                                            </td>
                                            <td>
                                                Lage ist mäßig attraktiv
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span className="usage-color-indicator" style={{backgroundColor: MapApi.getPressureClassificationColors().danger}}></span>
                                            </td>
                                            <td>
                                                Lage ist wenig attraktiv
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span className="usage-color-indicator" style={{backgroundColor: MapApi.getPressureClassificationColors().undefined}}></span>
                                            </td>
                                            <td>
                                                Keine Lagebewertung verfügbar
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>}
                            </div>
                            <a className="btn" data-coreui-toggle="collapse" href="#legend-content" role="button" aria-expanded="false"
                               aria-controls="collapseExample">
                                Legende
                            </a>
                        </div>
                    </>
                </Map>
                <MapEarlyWarningSystemCanvas selectedFeatures={this.state.selectedFeatures}
                                             step={this.state.step}
                                             setStep={(step: number) => this.setState({step})}
                                             setSelectedPolygonId={(id: number) => this.setState({selectedPolygonId: id})}/>
            </div>
        );
    }

}

export default EarlyWarningSystemMap;
