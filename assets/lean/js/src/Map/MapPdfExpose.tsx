import { Feature, MapBrowserEvent, MapEvent } from 'ol';
import { Coordinate } from 'ol/coordinate';
import { FeatureLike } from 'ol/Feature';
import { Point, Polygon } from 'ol/geom';
import { fromLonLat } from 'ol/proj';
import React from 'react';
import { MapApi } from '../MapApi/MapApi';
import { Property } from '../MapApi/Property';
import { PropertyApiResponse } from '../Property/PropertyApiResponse';
import { MapFunctionalitiesState } from './MapFunctionalitiesState';
import MapToolbar from './MapToolbar';
import { MapToolbarOption } from './MapToolbarOption';
import Popup from './Popup';

interface MapState {
    mapFunctionalitiesState?: MapFunctionalitiesState;
    selectedFeatures?: Feature[];
    mapApi?: MapApi;
    propertiesInitialized: boolean;
    mapRenderedFlags: mapRenderingFlags;
    alertText?: string;
    alertContext?: string;
    alertVisible: boolean;
}

interface mapRenderingFlags {
    initLocation: boolean;
    printMikro: boolean;
    printMakro: boolean;
}

interface MapProps {

    mapFunctionalitiesState: MapFunctionalitiesState;
    mapToolbarOption: MapToolbarOption;
    centerPoint: {
        longitude: number;
        latitude: number;
    };
    buildingUnit: PropertyApiResponse;
    zoom?: number;
    properties: Property[];
    forwardOnClick: boolean;
    useSavedView: boolean;
    useFilter: boolean;
    usePrintControl?: boolean;
    useMarker?: boolean;
    forceSearchAddress?: boolean;
    usePrintBtn?: boolean;
}

interface PrintProperties {
    imageType: string;
    quality: number;
    immediate: boolean;
    format: string;
    orient: string;
    margin: number;
    any?: any;
}

interface PrintEvent extends Event {
    type: 'print',
    print: {
        format: string,
        orientation: 'landscape' | 'portrait',
        unit: 'mm',
        size: number[],
        position: number[],
        imageWidth: number,
        imageHeight: number
    },
    image: string,
    imageType: string,
    quality: number,
    immediate: boolean;
    format: string;
    orient: string;
    margin: number;
    any: any;
    canvas: any;
}

class MapPdfExpose extends React.Component<MapProps, MapState> {

    private readonly mapContainer: React.RefObject<HTMLDivElement>;
    private readonly mapToolbarRefObject: React.RefObject<MapToolbar>;
    private readonly popupRefObject: React.RefObject<Popup>;

    private retries: number;
    private currentCoords: number[];

    private readonly MIKRO_ZOOM = 18;
    private readonly MAKRO_ZOOM = 16;

    private printMapTrials = {
        'mikro': 0,
        'makro': 0
    };
    private readonly maxPrintMapTrials = 2;

    private readonly DEFAULT_PRINT_PROPERTIES: PrintProperties = {
        'imageType': 'image/png',
        'quality': 0.4,
        'immediate': false,
        'format': 'a4',
        'orient': 'portrait',
        'margin': 50,
        'any': null,
    };

    constructor(props: MapProps) {
        super(props);

        this.state = {
            mapFunctionalitiesState: this.props.mapFunctionalitiesState,
            selectedFeatures: [],
            mapApi: MapApi.getInstance(),
            propertiesInitialized: false,
            mapRenderedFlags: {
                initLocation: false,
                printMikro: false,
                printMakro: false,
            },
            alertVisible: false,
        };

        this.retries = 0;
        this.currentCoords = [0, 0];

        this.mapContainer = React.createRef();
        this.mapToolbarRefObject = React.createRef();
        this.popupRefObject = React.createRef();
    }

    public componentDidMount(): void {
        const {
            usePrintControl,
            useMarker,
            forceSearchAddress
        } = this.props;

        this.state.mapApi.initializeMap(this.mapContainer.current);

        this.state.mapApi.setPointLayerVisibility(this.state.mapFunctionalitiesState.pinLayerVisibility);
        this.state.mapApi.setPolygonLayerVisibility(this.state.mapFunctionalitiesState.polygonLayerVisibility);
        this.state.mapApi.setDrawInteraction(this.state.mapFunctionalitiesState.drawInteraction);
        this.state.mapApi.setTranslateInteraction(this.state.mapFunctionalitiesState.translateInteraction);
        this.state.mapApi.setModifyInteraction(this.state.mapFunctionalitiesState.modifyInteraction);
        this.state.mapApi.setMeasureLayerVisibility(this.state.mapFunctionalitiesState.measureLayerVisibility);
        this.state.mapApi.setBusinessLocationLayerVisibility(this.state.mapFunctionalitiesState.businessLocationLayerVisibility);
        this.state.mapApi.setMeasureInteraction(this.state.mapFunctionalitiesState.measureInteraction);

        if (usePrintControl) {
            this.setPrintControl();
        }

        if (forceSearchAddress) {
            this.state.mapApi.onMapMoveEnd((event: MapEvent) => {
                this.currentCoords = event.map.getView().getCenter();
            });

            this.state.mapApi.useMarkerForLocation(useMarker);
            this.initMapBuildingLocation();
        }
    }

    private clearMap() {
        document.querySelectorAll('.map-img').forEach(elem => elem.remove());
    }


    private initMapBuildingLocation() {
        if (this.props.buildingUnit.geolocationPoint !== null) {
            this.state.mapApi.setCenter(fromLonLat([this.props.buildingUnit.geolocationPoint.point.longitude, this.props.buildingUnit.geolocationPoint.point.latitude]))
            this.state.mapApi.pointSource.addFeature(new Feature({
                geometry: new Point(fromLonLat([this.props.buildingUnit.geolocationPoint.point.longitude,
                                                this.props.buildingUnit.geolocationPoint.point.latitude])),
                ...this.props.buildingUnit
            }));
        } else {
            const buildingLocation = `${this.props.buildingUnit.address?.streetName} ${this.props.buildingUnit.address?.houseNumber} ${this.props.buildingUnit.address?.postalCode} ${this.props.buildingUnit.address?.place.placeName}`
            if (buildingLocation !== undefined) {
                new Promise<void>((resolve, reject) => {
                    setTimeout(async () => {
                        await this.state.mapApi.setInitialMapLocation(buildingLocation, () => this.clearMap());
                        resolve();
                        }, 1000);
                }).then(()=> {
                    this.printMap(true)
                })
            } else {
                this.clearMap();
            }
        }

        if (this.props.buildingUnit.geolocationPolygon !== null) {

            const polygonCoordinates: number[][][] = [];

            let points: number[][] = [];

            this.props.buildingUnit.geolocationPolygon?.polygon.rings.forEach((ring: any): void => {
                ring.points.forEach((point: any): void => {
                    points.push(MapApi.fromLonLat([point.longitude, point.latitude]));
                });

                polygonCoordinates.push(points);
            });
            this.state.mapApi.polygonSource.addFeature(new Feature({
                geometry: new Polygon(polygonCoordinates),
                ...this.props.buildingUnit
            }));
        }
    }

    private printEventListener(printEvent: PrintEvent) {
        const {mapRenderedFlags} = this.state;

        const imgSource = printEvent.image;
        const imagePrintType = printEvent.any.printImageType;
        const updateImg = printEvent.any.updateImg;

        if (!updateImg) {
            const w = window.open('', 'image');
            const image = new Image();
            image.src = imgSource;
            w.document.write(image.outerHTML);
        } else {
            document.querySelector('.object_images > #' + imagePrintType + '-img')?.setAttribute('src', imgSource);

            if (mapRenderedFlags.printMakro) {
                const [latitude, longitude] = this.currentCoords;

                if (Math.abs(latitude) < 10 || Math.abs(longitude) < 10) {
                    document.querySelectorAll('.map-img').forEach(elem => elem.remove());
                }
                this.generatePageLoadedDiv();
            }
        }
    }

    private generatePageLoadedDiv() {
        const pgLoaded = document.createElement('div');
        pgLoaded.setAttribute('id', 'page-loaded');
        document.querySelector('#placeholder').append(pgLoaded);
    }

    private generatePrintProperties(printType: 'mikro' | 'makro', updateImg: boolean): PrintProperties {
        return {
            ...this.DEFAULT_PRINT_PROPERTIES,
            'any': {
                'updateImg': updateImg,
                'printImageType': printType,
            }
        };
    }

    private determineZoomLevel(printMikro: boolean): number {
        return printMikro ? this.MIKRO_ZOOM : this.MAKRO_ZOOM;
    }

    private handleMapRenderComplete(updateImgTag: boolean) {
        if (this.state.mapRenderedFlags.printMikro === false && this.props.buildingUnit.geolocationPoint) {
            this.printMap(updateImgTag)
            this.setState({mapRenderedFlags: {...this.state.mapRenderedFlags, printMikro: true}});
        }
    }

    private printMap(updateImgTag: boolean){
        if (updateImgTag) {
            this.mikroMakroMapPrint('mikro', updateImgTag);
            setTimeout(() => {
                this.mikroMakroMapPrint('makro', updateImgTag);
                this.generatePageLoadedDiv();
            }, 500);
        }
    }

    private mikroMakroMapPrint(printType: 'mikro' | 'makro', updateImg: boolean) {
        this.state.mapApi.setZoom(this.determineZoomLevel(printType === 'mikro'));
        this.state.mapApi.printMap(this.generatePrintProperties(printType, updateImg));
    }

    private setPrintControl() {
        const {usePrintBtn} = this.props;
        let triggerAutomaticImageGeneration = true;

        this.state.mapApi.addPrintEvent((event: PrintEvent) => this.printEventListener(event), 'print');

        if (usePrintBtn) {
            const printBtn = document.querySelector('#printProperty');
            triggerAutomaticImageGeneration = false;

            printBtn?.addEventListener('click', () => {
                this.mikroMakroMapPrint('mikro', false);
            });
        }

        this.state.mapApi.onMapRenderComplete((event: Event) => this.handleMapRenderComplete(triggerAutomaticImageGeneration));
    }

    public componentDidUpdate(prevProps: Readonly<MapProps>, prevState: Readonly<MapState>, snapshot?: any): void {

        if (prevProps.properties !== this.props.properties) {
            this.state.mapApi.initializeProperties(this.props.properties);
            this.setState({propertiesInitialized: true});
            if (!this.props.useFilter) {
                this.state.mapApi.addPropertyPointsAndPolygonFeatures(this.state.mapApi.getPropertyMappings(), this.state.mapApi.pointSource, this.state.mapApi.polygonSource);
            }
        }

        if (prevProps.centerPoint !== this.props.centerPoint) {
            const centerPoint: number[] = this.props.useSavedView ? this.getMapCenterPointFromLocalStorage() : fromLonLat([this.props.centerPoint.longitude,
                                                                                                                           this.props.centerPoint.latitude]);
            this.state.mapApi.setCenter(centerPoint);
        }
    }

    public render(): JSX.Element {
        return (
            <div ref={this.mapContainer} className="map"></div>
        );
    }

    private saveMapCenterAndZoomToLocalStorage = (centerPoint: Coordinate, zoom: number) => {
        const storedCenterPoint = this.getMapCenterPointFromLocalStorage();
        if (storedCenterPoint === null || (centerPoint[0] !== storedCenterPoint[0] || centerPoint[1] !== storedCenterPoint[1])) {
            this.saveMapCenterPointToLocalStorage(centerPoint);
        }

        const storedZoom = this.getMapZoomFromLocalStorage();
        if (storedZoom === null || zoom !== storedZoom) {
            this.saveMapZoomToLocalStorage(zoom);
        }
    };

    private enableCreateObjectByClickInMap = (): void => {
        this.state.mapApi.addClickEvent(this.createObjectByClickInMap);
    };

    private createObjectByClickInMap = (event: MapBrowserEvent<any>): void => {
        if (this.state.mapFunctionalitiesState.createObjectByClickInMap !== true) {
            return;
        }

        const features: FeatureLike[] = this.state.mapApi.getFeaturesAtPixel(event.pixel);

        if (features.length > 0) {
            return;
        }

        const coordinate: Coordinate = MapApi.toLonLat(event.coordinate);
        const longitudeInput: HTMLInputElement = document.createElement('input');
        const latitudeInput: HTMLInputElement = document.createElement('input');
        const form: HTMLFormElement = document.createElement('form');

        longitudeInput.name = 'longitude';
        longitudeInput.value = coordinate[0].toString();

        latitudeInput.name = 'latitude';
        latitudeInput.value = coordinate[1].toString();

        form.method = 'POST';
        form.action = '/besatz-und-leerstand/objekte/erfassen';

        form.appendChild(longitudeInput);
        form.appendChild(latitudeInput);

        document.body.appendChild(form);

        form.submit();
    };

    public getPointFeatures = (): Feature<Point>[] => {
        return this.state.mapApi.getPointFeatures();
    };

    public getPolygonFeatures = (): Feature<Polygon>[] => {
        return this.state.mapApi.getPolygonFeatures();
    };

    private getMapCenterPointFromLocalStorage = (): number[] => {
        if (localStorage.getItem('lean.map.centerPoint') === null) {
            return null;
        }

        return JSON.parse(localStorage.getItem('lean.map.centerPoint'));
    };

    private saveMapCenterPointToLocalStorage = (centerPoint: number[]): void => {
        localStorage.setItem('lean.map.centerPoint', JSON.stringify(centerPoint));
    };

    private getMapZoomFromLocalStorage = (): number => {
        if (localStorage.getItem('lean.map.zoom') === null) {
            return null;
        }

        return JSON.parse(localStorage.getItem('lean.map.zoom'));
    };

    private saveMapZoomToLocalStorage = (zoom: number): void => {
        localStorage.setItem('lean.map.zoom', JSON.stringify(zoom));
    };
}

export default MapPdfExpose;
