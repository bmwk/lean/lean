interface PropertyFilter {

    propertyStatus: number[];
    propertyUsageStatus: number[];
    propertyUsages: {levelOne: number, levelTwo: number}[];
    propertyOfferTypes: number[];
    propertyMinSpace: string;
    propertyMaxSpace: string;
    shopWindowFrontWidthMin: string;
    shopWindowFrontWidthMax: string;
    barrierFreeAccessTypes: number[];
    assignedAccountUser: number;
    lastUpdatedFrom: string;
    lastUpdatedTill: string;
}

export { PropertyFilter };
