import * as coreui from '@coreui/coreui';
import { MDCTextField } from '@material/textfield';
import Axios from 'axios';
import React, { ChangeEvent } from 'react';
import LoadingImage from '../../../images/lean-loading.gif';
import { MapApi } from '../MapApi/MapApi';
import { WmsLayer } from '../MapApi/WmsLayer';
import AddWmsLayer from './AddWmsLayer';
import EditWmsLayer from './EditWmsLayer';

interface WmsLayerState {
    privateWmsLayers?: WmsLayer[];
    globalWmsLayers?: WmsLayer[];
    activePrivateWmsLayers?: number[];
    newWmsLayersToAdd: WmsLayer[];
    showAddWmsLayer: boolean;
    showEditWmsLayer: boolean;
    wmsLayerToEdit?: WmsLayer;
    isLoading: boolean;
}

interface WmsLayerProps {
    handleClose: Function;
    wmsLayerCanvasVisible: boolean;
    showGlobalWmsLayer: boolean;
}

class MapWmsLayer extends React.Component<WmsLayerProps, WmsLayerState> {

    private readonly abortController: AbortController;
    private readonly mapApi: MapApi;
    private readonly capabilitiesUrlMdcTextFieldRefObject: React.RefObject<HTMLLabelElement>;
    private readonly offcanvasRefObject: React.RefObject<HTMLDivElement>;
    private capabilitiesUrlMdcTextField: MDCTextField;

    constructor(props: WmsLayerProps) {
        super(props);
        this.state = {
            isLoading: true,
            privateWmsLayers: [],
            activePrivateWmsLayers: [],
            newWmsLayersToAdd: [],
            showAddWmsLayer: false,
            showEditWmsLayer: false,
        };
        this.capabilitiesUrlMdcTextFieldRefObject = React.createRef();
        this.offcanvasRefObject = React.createRef();
        this.mapApi = MapApi.getInstance();
        this.abortController = new AbortController();
    }

    public componentDidMount(): void {
        this.offcanvasRefObject.current.addEventListener('hide.coreui.offcanvas', () => {
            this.props.handleClose();
        });

        this.capabilitiesUrlMdcTextField = new MDCTextField(this.capabilitiesUrlMdcTextFieldRefObject.current);

        const initialize = async (): Promise<void> => {
            let privateWmsLayers: WmsLayer[] = [];
            let globalWmsLayers: WmsLayer[] = [];

            try {
                privateWmsLayers = await this.fetchWmsLayersFromApi();

                if (this.props.showGlobalWmsLayer === true) {
                    globalWmsLayers = await this.fetchGlobalWmsLayersFromApi();
                }

                this.mapApi.initializeWmsLayers([...privateWmsLayers, ...globalWmsLayers]);

                this.initializeActivePrivateWmsLayers();
            } catch (e) {
                console.error(e);
            } finally {
                this.setState({
                    privateWmsLayers: privateWmsLayers,
                    globalWmsLayers: globalWmsLayers,
                    activePrivateWmsLayers: this.getActivePrivateWmsLayersFromLocalStorage(),
                    isLoading: false,
                });
            }
        };
        initialize();
    }

    componentDidUpdate(prevProps: Readonly<WmsLayerProps>, prevState: Readonly<WmsLayerState>, snapshot?: any): void {
        this.updateWmsLayerDisplayed(prevState.activePrivateWmsLayers, this.state.activePrivateWmsLayers);

        const offcanvasInstance = coreui.Offcanvas.getOrCreateInstance(this.offcanvasRefObject.current);
        if (this.props.wmsLayerCanvasVisible) {
            offcanvasInstance.show();
        } else {
            offcanvasInstance.hide();
        }
    }

    public render(): JSX.Element {
        return (
            <>
                <div className="offcanvas offcanvas-end position-absolute" style={{zIndex: 11}}
                     data-coreui-scroll="true" data-coreui-backdrop="false" tabIndex={-1} id="map_wms_layer_offcanvas"
                     aria-labelledby="map_wms_layer_offcanvas_label" ref={this.offcanvasRefObject}>
                    <div className="offcanvas-header">
                        <h4 className="offcanvas-title" id="map_wms_layer_offcanvas_label">Ebenen</h4>
                        <button type="button" className="btn-close text-reset" aria-label="Close"
                                onClick={() => this.props.handleClose()}/>
                    </div>
                    <div className="offcanvas-body d-flex flex-column accordion">
                        {this.state.isLoading ?
                         (<div className="d-flex align-items-center">
                             <img src={LoadingImage} alt=""/>
                         </div>) :
                         <>
                             <button className="accordion-button collapsed py-3 px-0" data-coreui-toggle="collapse"
                                     data-coreui-target="#wmsLayerSelection">
                                 <span className="fs-5">Ebenenauswahl</span>
                             </button>
                             <div id="wmsLayerSelection" className="collapse">
                                 <div className="d-flex flex-column gap-2 mt-1 mb-2">
                                     {this.props.showGlobalWmsLayer && <>
                                         <span className="text-primary fs-5">Globale Layer</span>
                                         <ul className="mdc-list mb-2" role="group" aria-label="List with checkbox items">
                                             {this.state.globalWmsLayers.map((wmsLayer: WmsLayer) =>
                                                 <li key={wmsLayer.id} className="mdc-list-item" role="checkbox"
                                                     aria-checked="false">
                                                     <span className="mdc-list-item__ripple"/>
                                                     <span className="mdc-list-item__graphic d-flex align-items-center">
                                                             <div className="mdc-checkbox">
                                                                 <input
                                                                     onChange={(event: ChangeEvent<HTMLInputElement>): void => this.handleActivePrivateWmsLayerChange(wmsLayer.id, event)}
                                                                     defaultChecked={this.state.activePrivateWmsLayers.indexOf(wmsLayer.id) !== -1}
                                                                     type="checkbox" className="mdc-checkbox__native-control"/>
                                                                 <div className="mdc-checkbox__background">
                                                                     <svg className="mdc-checkbox__checkmark" viewBox="0 0 24 24">
                                                                         <path className="mdc-checkbox__checkmark-path" fill="none"
                                                                               d="M1.73,12.91 8.1,19.28 22.79,4.59"/>
                                                                     </svg>
                                                                     <div className="mdc-checkbox__mixedmark"/>
                                                                 </div>
                                                             </div>
                                                             <label className="mdc-list-item__text"
                                                                    htmlFor={'wms-layer-list-checkbox-item-'.concat(wmsLayer.id.toString())}>{wmsLayer.title}</label>
                                                         </span>
                                                 </li>
                                             )}
                                         </ul>
                                     </>}
                                     <span className="text-primary fs-5">Benutzerdefinierte Layer</span>
                                     <ul className="mdc-list mb-2" role="group" aria-label="List with checkbox items">
                                         {this.state.privateWmsLayers.map((wmsLayer: WmsLayer) =>
                                             <li key={wmsLayer.id} className="mdc-list-item" role="checkbox"
                                                 aria-checked="false">
                                                 <span className="mdc-list-item__ripple"/>
                                                 <span className="mdc-list-item__graphic d-flex align-items-center">
                                                    <div className="mdc-checkbox">
                                                        <input
                                                            onChange={(event: ChangeEvent<HTMLInputElement>): void => this.handleActivePrivateWmsLayerChange(wmsLayer.id, event)}
                                                            defaultChecked={this.state.activePrivateWmsLayers.indexOf(wmsLayer.id) !== -1}
                                                            type="checkbox" className="mdc-checkbox__native-control"/>
                                                        <div className="mdc-checkbox__background">
                                                            <svg className="mdc-checkbox__checkmark" viewBox="0 0 24 24">
                                                                <path className="mdc-checkbox__checkmark-path" fill="none"
                                                                      d="M1.73,12.91 8.1,19.28 22.79,4.59"/>
                                                            </svg>
                                                            <div className="mdc-checkbox__mixedmark"/>
                                                        </div>
                                                    </div>
                                                    <button className="edit-layer-button material-icons mdc-icon-button"
                                                            aria-hidden="true"
                                                            onClick={() => this.editWmsLayer(wmsLayer)}>edit</button>
                                                    <label className="mdc-list-item__text"
                                                           htmlFor={'wms-layer-list-checkbox-item-'.concat(wmsLayer.id.toString())}>{wmsLayer.title}</label>
                                                </span>
                                             </li>
                                         )}
                                     </ul>
                                 </div>
                             </div>
                         </>
                        }
                        <>
                            <button className="accordion-button collapsed py-3 px-0" data-coreui-toggle="collapse"
                                    data-coreui-target="#wmsLayerAddition">
                                <span className="fs-5">Ebene hinzufügen</span>
                            </button>
                            <div id="wmsLayerAddition" className="collapse">
                                <p className="mt-2">
                                    Hier können Sie eigene Ebenen in Form von WebMapServices (WMS) zur Karte hinzufügen. Bitte geben Sie dazu die
                                    Capabilities-URL
                                    des WMS-Dienstes an.
                                </p>

                                <label ref={this.capabilitiesUrlMdcTextFieldRefObject}
                                       className="mdc-text-field mdc-text-field--filled w-100">
                                    <span className="mdc-text-field__ripple"/>
                                    <span className="mdc-floating-label" id="my-label-id">Capabilities-URL</span>
                                    <input className="mdc-text-field__input" type="text" aria-labelledby="my-label-id"/>
                                    <span className="mdc-line-ripple"/>
                                </label>

                                <button onClick={this.handleAddWmsLayerClick} className="mdc-button mdc-button--raised mt-2">
                                    <span className="mdc-button__ripple"/>
                                    <span className="mdc-button__label">Layer hinzufügen</span>
                                </button>

                            </div>
                        </>
                        {this.state.showAddWmsLayer &&
                        <AddWmsLayer saveNewLayers={this.saveNewWmsLayers} wmsLayer={this.state.newWmsLayersToAdd}
                                     handleClose={() => this.setState({showAddWmsLayer: false})}/>}
                        {this.state.showEditWmsLayer &&
                        <EditWmsLayer wmsLayer={this.state.wmsLayerToEdit} saveWmsLayer={this.handleWmsLayerUpdate}
                                      deleteWmsLayer={this.handleWmsLayerDelete}
                                      handleClose={() => this.setState({showEditWmsLayer: false})}/>}
                    </div>
                </div>
            </>
        );
    }

    handleAddWmsLayerClick = (): void => {
        this.fetchParsedWmsLayersByUrlFromApi(this.capabilitiesUrlMdcTextField.value);
    };

    private handleWmsLayerUpdate = async (updatedLayer: WmsLayer) => {
        await this.updateWmsLayer(updatedLayer);
        const currentWmsLayers = this.state.privateWmsLayers.slice();
        currentWmsLayers[currentWmsLayers.findIndex((wmsLayer) => wmsLayer.id === updatedLayer.id)].title = updatedLayer.title;
        this.setState({
            showEditWmsLayer: false,
            privateWmsLayers: currentWmsLayers,
        });
    };

    private handleWmsLayerDelete = async (deletedLayer: WmsLayer) => {
        await this.deleteWmsLayer(deletedLayer);
        const currentWmsLayers = this.state.privateWmsLayers.slice()
        .filter(wmsLayer => deletedLayer.id !== wmsLayer.id);
        this.setState({
            showEditWmsLayer: false,
            privateWmsLayers: currentWmsLayers,
        });
    };

    private saveNewWmsLayers = async (layers: WmsLayer[]) => {
        const newWmsLayers: WmsLayer[] = [];
        for (let i = 0; i < layers.length; i++) {
            newWmsLayers.push(await this.saveWmsLayer(layers[i]));
        }
        const currentWmsLayers: WmsLayer[] = this.state.privateWmsLayers.concat(newWmsLayers);
        this.setState({
            showAddWmsLayer: false,
            privateWmsLayers: currentWmsLayers,
        });
    };

    private saveWmsLayer = async (wmsLayer: WmsLayer): Promise<WmsLayer> => {
        const response = await Axios.post('/api/map/account-user/wms-layers', wmsLayer);
        return response.data;
    };

    private updateWmsLayer = async (wmsLayer: WmsLayer): Promise<void> => {
        const response = await Axios.put(`/api/map/account-user/wms-layers/${wmsLayer.id}`, wmsLayer);
    };

    private deleteWmsLayer = async (wmsLayer: WmsLayer): Promise<void> => {
        const response = await Axios.delete(`/api/map/account-user/wms-layers/${wmsLayer.id}`, wmsLayer);
    };

    private editWmsLayer = (layer: WmsLayer) => {
        this.setState({wmsLayerToEdit: layer});
        this.setState({showEditWmsLayer: true});
    };

    private handleActivePrivateWmsLayerChange = (id: number, event: ChangeEvent<HTMLInputElement>): void => {
        let activePrivateWmsLayers: number[] = this.state.activePrivateWmsLayers.slice();

        if (event.target.checked === true) {
            MapWmsLayer.addToList(activePrivateWmsLayers, id);
        } else {
            MapWmsLayer.removeFromList(activePrivateWmsLayers, id);
        }

        this.setState({
            activePrivateWmsLayers: activePrivateWmsLayers
        });

        localStorage.setItem('lean.map.activePrivateWmsLayers', JSON.stringify(activePrivateWmsLayers));
    };

    private static addToList = (activePrivateWmsLayers: number[], idToAdd: number): void => {
        if (activePrivateWmsLayers.indexOf(idToAdd) === -1) {
            activePrivateWmsLayers.push(idToAdd);
        }
    };

    private static removeFromList = (activePrivateWmsLayers: number[], idToRemove: number): void => {
        if (activePrivateWmsLayers.indexOf(idToRemove) !== -1) {
            activePrivateWmsLayers.splice(activePrivateWmsLayers.findIndex((layerId) => layerId === idToRemove), 1);
        }
    };

    private fetchWmsLayersFromApi = async (): Promise<WmsLayer[]> => {
        const response = await Axios.get<WmsLayer[]>('/api/map/account-user/wms-layers', {signal: this.abortController.signal});
        return response.data;
    };

    private fetchGlobalWmsLayersFromApi = async (): Promise<WmsLayer[]> => {
        const response = await Axios.get<WmsLayer[]>('/api/map/account/wms-layers', {signal: this.abortController.signal});

        return response.data;
    };

    private getActivePrivateWmsLayersFromLocalStorage = (): number[] => {
        if (localStorage.getItem('lean.map.activePrivateWmsLayers') === null) return null;

        return JSON.parse(localStorage.getItem('lean.map.activePrivateWmsLayers'));
    };

    private initializeActivePrivateWmsLayers = (): void => {
        if (localStorage.getItem('lean.map.activePrivateWmsLayers') !== null) return;
        localStorage.setItem('lean.map.activePrivateWmsLayers', JSON.stringify([]));
    };

    private updateWmsLayerDisplayed = (oldPrivateWmsLayers: number[], currentPrivatWmsLayers: number[]): void => {
        if (oldPrivateWmsLayers.length > currentPrivatWmsLayers.length) {
            const idsToRemove = oldPrivateWmsLayers.filter(id => !currentPrivatWmsLayers.includes(id));
            idsToRemove.forEach(id => this.mapApi.removeWmsTileLayer(id));
        } else if (oldPrivateWmsLayers.length < currentPrivatWmsLayers.length) {
            const idsToAdd = currentPrivatWmsLayers.filter(id => !oldPrivateWmsLayers.includes(id));
            idsToAdd.forEach(id => this.mapApi.addWmsTileLayer(id));
        }
    };

    private fetchParsedWmsLayersByUrlFromApi = async (url: string): Promise<void> => {
        const response = await Axios.post<WmsLayer[]>('/api/map/parser/wms-capabilities', {'url': url});
        const wmsLayers: WmsLayer[] = response.data;
        this.setState({newWmsLayersToAdd: wmsLayers});
        this.setState({showAddWmsLayer: true});
    };

}

export default MapWmsLayer;
