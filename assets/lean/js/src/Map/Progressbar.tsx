import React from 'react';
import { MapApi } from '../MapApi/MapApi';

interface ProgressbarProperties {
    value: number
}

const Progressbar = ({value}: ProgressbarProperties) => {
    const roundedValue = Math.round(value * 100)
    const colors = MapApi.getObjectStatusColors()
    return (
            <div className="progress align-items-center">
                <div className={'progress-bar h-100'} role="progressbar"
                     style={{width: Math.abs(roundedValue) + '%', backgroundColor: (roundedValue < 25 ? colors.danger : roundedValue > 50 ? colors.success : colors.warning)}}>{Math.abs(roundedValue) >= 25 && <span>{roundedValue}%</span>}</div>
                {Math.abs(roundedValue) < 25 && <span className="ms-2">{value !== null ? roundedValue + '%' : 'Keine Werte verfügbar'}</span>}
            </div>
    );
};

export default Progressbar;
