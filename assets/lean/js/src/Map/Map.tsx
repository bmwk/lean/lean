import { Coordinate } from 'ol/coordinate';
import { FeatureLike } from 'ol/Feature';
import React from 'react';
import { MapApi } from '../MapApi/MapApi';

class Map extends React.Component<any, any> {

    private readonly mapContainer: React.RefObject<HTMLDivElement>;
    private readonly mapApi: MapApi;

    constructor(props: any) {
        super(props);
        this.mapContainer = React.createRef();
        this.mapApi = MapApi.getInstance();
    }

    public componentDidMount(): void {
        this.mapApi.initializeMap(this.mapContainer.current);
    }

    public render(): JSX.Element {
        return (
            <div ref={this.mapContainer} className="map flex-grow-1" style={{marginRight: this.props.isEarlyWarningSystemMap ? '550px' : 0}}>
                {this.props.children}
            </div>
        );
    }

    public static saveMapCenterAndZoomToLocalStorage = (centerPoint: Coordinate, zoom: number) => {
        const storedCenterPoint = Map.getMapCenterPointFromLocalStorage();
        if (storedCenterPoint === null || (centerPoint[0] !== storedCenterPoint[0] || centerPoint[1] !== storedCenterPoint[1])) {
            Map.saveMapCenterPointToLocalStorage(centerPoint);
        }

        const storedZoom = Map.getMapZoomFromLocalStorage();
        if (storedZoom === null || zoom !== storedZoom) {
            Map.saveMapZoomToLocalStorage(zoom);
        }
    };

    public static createObjectByClickInMap = (features: FeatureLike[], eventCoordinate: number[]): void => {

        const coordinate: Coordinate = MapApi.toLonLat(eventCoordinate);
        const longitudeInput: HTMLInputElement = document.createElement('input');
        const latitudeInput: HTMLInputElement = document.createElement('input');
        const form: HTMLFormElement = document.createElement('form');

        longitudeInput.name = 'longitude';
        longitudeInput.value = coordinate[0].toString();

        latitudeInput.name = 'latitude';
        latitudeInput.value = coordinate[1].toString();

        form.method = 'POST';
        form.action = '/besatz-und-leerstand/objekte/erfassen';

        form.appendChild(longitudeInput);
        form.appendChild(latitudeInput);

        document.body.appendChild(form);

        form.submit();
    };

    public static getMapCenterPointFromLocalStorage = (): number[] => {
        if (localStorage.getItem('lean.map.centerPoint') === null) {
            return null;
        }

        return JSON.parse(localStorage.getItem('lean.map.centerPoint'));
    };

    public static saveMapCenterPointToLocalStorage = (centerPoint: number[]): void => {
        localStorage.setItem('lean.map.centerPoint', JSON.stringify(centerPoint));
    };

    public static getMapZoomFromLocalStorage = (): number => {
        if (localStorage.getItem('lean.map.zoom') === null) {
            return null;
        }

        return JSON.parse(localStorage.getItem('lean.map.zoom'));
    };

    public static saveMapZoomToLocalStorage = (zoom: number): void => {
        localStorage.setItem('lean.map.zoom', JSON.stringify(zoom));
    };

}

export default Map;
