import '../styles/app.scss';
import '@coreui/coreui';
import * as coreui from '@coreui/coreui';

const toggleSidebarButton: HTMLButtonElement = document.querySelector('#app_toggle_sidebar_button');
const informationBox: HTMLDivElement = document.querySelector('#information-box');
const sidebarElement: HTMLDivElement = document.querySelector('#collapseSidebar');
const sidebar = new coreui.Sidebar(sidebarElement);

if (toggleSidebarButton !== null) {
    toggleSidebarButton.addEventListener('click', (): void => {
        let width: number = (window.innerWidth > 0) ? window.innerWidth : screen.width;

        if (width >= 777) {
            if (sidebarElement.classList.contains('hide')) {
                sidebar.show();
            } else {
                sidebar.hide();
            }
        } else {
            sidebar.toggle();
        }

        toggleSidebarButton.blur();
    });
}

Array.from(document.querySelectorAll('.nav-group-toggle')).forEach((element: HTMLAnchorElement) => {
    element.addEventListener('click', (): void => {
        if (element.classList.contains('open')) {
            element.classList.remove('open');
        } else {
            element.classList.add('open');
            closeOpenedMenuItems(element);
        }
    });
});

function closeOpenedMenuItems(choosenElement: HTMLAnchorElement) {
    Array.from(document.querySelectorAll('.nav-group-toggle')).forEach((element: HTMLAnchorElement) => {
       if (element != choosenElement) {
           const ul = <HTMLUListElement>element.parentElement.querySelector('.collapse');
           ul.classList.remove('show');
           element.classList.remove('open');
       }
    });
}

if (informationBox !== null) {
    setInterval(fadeOutInformationBox,3000);
}

function fadeOutInformationBox() {
    const fadeEffect = setInterval(() => {
        if (!informationBox.style.opacity) {
            informationBox.setAttribute('style','opacity: 1');
        }

        const opacity: number = Number(informationBox.style.opacity);

        if (opacity > 0.1) {
            informationBox.setAttribute('style', 'opacity: ' + (opacity - 0.1));
        } else if (opacity == 0.1) {
            informationBox.classList.add('d-none');
        } else {
            clearInterval(fadeEffect);
        }
    }, 120);
}
