import PopoverField from '../PopoverField';
import { Autocomplete, TextField } from '@mui/material';
import React from 'react';

interface Option {
    label: string;
    value: string | number;
}

interface AutocompleteSelectFieldProps {
    name: string;
    label: string;
    value: string | number;
    isRequired: boolean;
    handleInputChange: Function;
    options: Option[];
    error?: string;
    popoverText?: string | JSX.Element;
}

export default function AutocompleteSelectField({name, label, error, value, isRequired, handleInputChange, popoverText, options}: AutocompleteSelectFieldProps) {
    return (
        <div style={{position: 'relative'}}>
            <Autocomplete
                options={options}
                onChange={(e, value: Option) => {
                    if (value !== null) {
                        handleInputChange(name, value.label);
                    }
                }}
                loading={options?.length < 1}
                loadingText={'Wird geladen...'}
                noOptionsText={'Keine Auswahl verfügbar'}
                isOptionEqualToValue={(option, value) => option.label === value.label}
                renderInput={(params) => {
                    return <TextField
                        {...params}
                        label={label}
                        variant="outlined"
                        required={isRequired}
                        helperText={error ?? ''}
                        name={name}
                        value={value}
                        onChange={(e) => {
                          handleInputChange(name, e.target.value);
                        }}
                        error={error !== undefined}
                    />;
                }}
            />
            {popoverText !== undefined && popoverText !== '' && <PopoverField text={popoverText}/>}
        </div>);
}
