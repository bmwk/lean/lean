import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import React from 'react';

interface ModalFieldStepProps {
    name: string;
    buttonLabel: string;
    title: string;
    text: string;
    submitModal?: Function;
    submitModalButtonText?: string;
}

export default function ModalField({name, buttonLabel, title, text, submitModal, submitModalButtonText}: ModalFieldStepProps) {
    const [open, setOpen] = React.useState(false);
    const handleOpen = (): void => setOpen(true);
    const handleClose = (): void => setOpen(false);

    const style = {
        position: 'absolute' as 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: '85vw',
        maxHeight: '90vh',
        overflowY: 'scroll',
        bgcolor: 'background.paper',
        border: '2px solid #000',
        boxShadow: 24,
        p: 4,
    };

    const handleSubmitClick = (): void => {
        setOpen(false);
        submitModal();
    }

    return (
        <>
            <Button color="secondary" onClick={handleOpen}>{buttonLabel}</Button>
            <Modal
                open={open}
                onClose={handleClose}
                aria-labelledby={name + '-title'}
                aria-describedby={name + '-description'}
            >
                <Box sx={style}>
                    <Typography id={name + '-title'} variant='h6' component='h2'>
                        {title}
                    </Typography>
                    <Typography id={name + '-description'} sx={{ my: 2 }}>
                        <span dangerouslySetInnerHTML={{ __html: text }} />
                    </Typography>
                    <div style={{float: 'right'}}>
                        <Button color="secondary" sx={{ mx: 2}} onClick={() => setOpen(false)}>Schließen</Button>
                        {submitModal != undefined &&  <Button variant="contained" color="secondary" disableElevation onClick={handleSubmitClick}>{submitModalButtonText}</Button>}
                    </div>
                </Box>
            </Modal>
        </>
    );
}
