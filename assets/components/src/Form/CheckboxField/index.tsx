import { Checkbox, FormControl, FormControlLabel, FormHelperText, Typography } from '@mui/material';
import React, { ChangeEvent } from 'react';

interface CheckboxFieldProps {
    name?: string;
    label: string;
    checked?: boolean;
    handleInputChange: Function;
    isRequired: boolean;
    error?: string;
    isFontSmall?: boolean;
    group?: string;
}

export default function Index({name, label, checked, error, isRequired, handleInputChange, isFontSmall, group}: CheckboxFieldProps) {
    return (
        <FormControl error={error != undefined}>
            <FormControlLabel
                style={{width: '100%', textAlign: 'left'}}
                control={<Checkbox
                    required={isRequired}
                    name={name}
                    checked={checked}
                    style={error && {color:'#d32f2f'}}
                    onChange={(event: ChangeEvent<HTMLInputElement>) => handleInputChange(name, event.target.checked, group)}
                />}
                label={isFontSmall ? <Typography variant="caption">{label}</Typography> : <Typography variant="body1">{label}</Typography> }
            />
            {error && <FormHelperText style={{color: '#d32f2f', margin: '0 2.5em'}}>{error}</FormHelperText>}
        </FormControl>
    );
}
