import PopoverField from '../PopoverField';
import { InputAdornment, TextField } from '@mui/material';
import React, { ChangeEvent } from 'react';

interface InputFieldProps {
    name: string;
    type: string;
    label: string;
    handleInputChange: Function;
    isRequired: boolean;
    value: string | number;
    error?: string;
    popoverText?: string;
    adornment?: string;
    readonly?: boolean;
}

export default function InputField({name, type, isRequired, label, readonly, handleInputChange, value, error, popoverText, adornment}: InputFieldProps) {
    return (
        <div style={{position: 'relative'}}>
            <TextField
                type={type}
                name={name}
                label={label}
                variant="outlined"
                fullWidth
                required={isRequired}
                defaultValue={value}
                error={error !== undefined}
                helperText={error ?? ''}
                onChange={(event: ChangeEvent<HTMLInputElement>) => handleInputChange(name, event.target.value)}
                InputProps={{
                    endAdornment: <InputAdornment position="end">{adornment}</InputAdornment>,
                    readOnly: readonly
                }}
            />
            {popoverText != undefined && popoverText != '' && <PopoverField text={popoverText} />}
        </div>
    )
}