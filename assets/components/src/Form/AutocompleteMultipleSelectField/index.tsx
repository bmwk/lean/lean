import { Option } from '../../../../looking_for_property_request_reporter/src/InterfaceLibrary';
import Autocomplete from '@mui/material/Autocomplete';
import TextField from '@mui/material/TextField';
import { Checkbox } from '@mui/material';
import CheckBoxOutlineBlankIcon from '@mui/icons-material/CheckBoxOutlineBlank';
import CheckBoxIcon from '@mui/icons-material/CheckBox';
import * as React from 'react';
import { useEffect, useState } from 'react';

interface AutocompleteMultipleSelectFieldProps {
    name: string;
    label: string;
    isRequired: boolean;
    handleInputChange: Function;
    options: Option[];
    selectedOptionIds: any[];
    error?: string;
}

export default function AutocompleteMultipleSelectField({name, label, isRequired, handleInputChange, options, selectedOptionIds, error}: AutocompleteMultipleSelectFieldProps) {
    const [selectedOptions, setSelectedOptions] = useState<Option[]>([]);
    const icon = <CheckBoxOutlineBlankIcon fontSize="small" />;
    const checkedIcon = <CheckBoxIcon fontSize="small" />;

    useEffect(() => {
        let selectedOptions: Option[] = [];

        for (const option of options) {
            if (selectedOptionIds.includes(option.value) === true) {
                selectedOptions.push(option)
            }
        }

        setSelectedOptions(selectedOptions)
    }, [])

    const handleChange = (selectedOptions: Option[]): void => {
        setSelectedOptions(selectedOptions);

        let selectedValues: any[] = [];

        for (const option of selectedOptions) {
            selectedValues.push(option.value);
        }

        handleInputChange(name, selectedValues);
    }

    return (
        <Autocomplete
            multiple
            id={'search-multiple-select-' + name}
            loadingText={'Wird geladen...'}
            options={options}
            value={selectedOptions}
            onChange={(event: any, newValue: Option[]) => {
                handleChange(newValue)
            }}
            disableCloseOnSelect
            getOptionLabel={(option: Option) => option.label}
            isOptionEqualToValue={(option: Option, value) => option.value === value.value}
            renderOption={(props, option, { selected }) => (
                <li {...props}>
                    <Checkbox
                        icon={icon}
                        checkedIcon={checkedIcon}
                        style={{ marginRight: 8 }}
                        checked={selected}
                    />
                    {option.label}
                </li>
            )}
            renderInput={(params) => (
                <TextField
                    {...params}
                    required={isRequired}
                    label={label}
                    variant="outlined"
                    error={error !== undefined}
                    helperText={error ?? ''}
                />
            )}
        />
    );
}
