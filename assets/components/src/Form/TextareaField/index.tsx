import { TextField } from '@mui/material';
import React, { ChangeEvent } from 'react';

interface TextareaFieldProps {
    name: string;
    label: string;
    handleInputChange: Function;
    isRequired: boolean;
    value: string;
    error?: string;
}

export default function TextareaField({name, value, error, label, handleInputChange, isRequired}: TextareaFieldProps) {
    return (
        <TextField
            name={name}
            label={label}
            variant="outlined"
            fullWidth
            defaultValue={value}
            error={error !== undefined}
            helperText={error ?? ''}
            required={isRequired}
            onChange={(event: ChangeEvent<HTMLTextAreaElement>) => handleInputChange(name, event.target.value)}
            multiline
            rows={4}
        />
    )
}