import PopoverField from '../PopoverField';
import { FormControl, FormHelperText, InputLabel, MenuItem, Select } from '@mui/material';
import React from 'react';

interface Option {
    label: string;
    value: string | number;
    levelOne?: number;
}

interface SelectFieldProps {
    name: string;
    label: string;
    value: string | number;
    isRequired: boolean;
    handleInputChange: Function;
    options: Option[];
    error?: string;
    popoverText?: string | JSX.Element;
    disabled?: boolean;
}

export default function SelectField({name, label, value, isRequired, handleInputChange, error, popoverText, options, disabled}: SelectFieldProps) {
    return (
        <div style={{position: 'relative'}}>
            <FormControl fullWidth variant="outlined" required={isRequired} error={error !== undefined}>
                <InputLabel id={'select_toggler_' + name} disabled={disabled} placeholder={'Bitte wählen'}>{label}</InputLabel>
                <Select
                    labelId={'select_toggler_' + name}
                    label={label}
                    name={name}
                    value={value}
                    onChange={(event) => handleInputChange(name, event.target.value)}
                    disabled={disabled}
                >

                    {options.map(({label, value}) => <MenuItem style={{whiteSpace: 'normal'}} key={label} value={value}>{label}</MenuItem>)}

                </Select>
                {error !== undefined && <FormHelperText>{error}</FormHelperText>}
            </FormControl>
            {popoverText != undefined && popoverText != '' && <PopoverField text={popoverText}/>}
        </div>
    );
}