import popoverStyle from './index.module.scss';
import Button from '@mui/material/Button';
import Popover from '@mui/material/Popover';
import Typography from '@mui/material/Typography';
import * as React from 'react';

interface PopoverFieldProps {
    text?: string | JSX.Element;
}

export default function PopoverField({text}: PopoverFieldProps) {
    const [anchorEl, setAnchorEl] = React.useState<HTMLButtonElement | null>(null);

    const handleClick = (event: React.MouseEvent<HTMLButtonElement>): void => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = (): void => {
        setAnchorEl(null);
    };

    const open: boolean = Boolean(anchorEl);
    const id: string = open ? 'simple-popover' : undefined;

    return (
        <div style={{position: 'absolute', top: '-15px', right: '-30px'}}>
            <Button aria-describedby={id} onClick={handleClick}>
               <span className={popoverStyle['material-icons']}>info</span>
            </Button>
            <Popover
                id={id}
                open={open}
                anchorEl={anchorEl}
                onClose={handleClose}
                anchorOrigin={{vertical: 'bottom', horizontal: 'left'}}
            >
                {text && <Typography sx={{p: 2}}>{text}</Typography>}
            </Popover>
        </div>
    );
}
