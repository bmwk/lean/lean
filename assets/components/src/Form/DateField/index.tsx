import TextField from '@mui/material/TextField';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { DesktopDatePicker } from '@mui/x-date-pickers/DesktopDatePicker';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import dayjs, { Dayjs } from 'dayjs';
import * as React from 'react';
import PopoverField from '../PopoverField';

interface DateFieldProps {
    name: string;
    label: string;
    value: string | null;
    handleInputChange: Function;
    minDate?: string;
    maxDate?: string;
    required?: boolean;
    popoverText?: string | JSX.Element;
    error?: string;
}

export default function DateField({name, label, value, minDate, maxDate, required, popoverText, error, handleInputChange}: DateFieldProps) {

    const handleChange = (newValue: Dayjs | null): void => {
        if (newValue === null){
            handleInputChange(name, newValue);
        } else {
            handleInputChange(name, newValue.format('YYYY-MM-DD'));
        }
    };

    return (
        <div style={{position: 'relative'}}>
            <LocalizationProvider dateAdapter={AdapterDayjs}>
                <DesktopDatePicker
                    label={label}
                    inputFormat="DD.MM.YYYY"
                    value={value ? dayjs(value) : null}
                    minDate={minDate ? dayjs(minDate) : null}
                    maxDate={maxDate ? dayjs(maxDate) : null}
                    onChange={handleChange}
                    renderInput={(params) => <TextField {...params} error={error !== undefined} helperText={error ?? ''} required={required} fullWidth/>}
                />
            </LocalizationProvider>
            {popoverText !== undefined && popoverText !== '' && <PopoverField text={popoverText}/>}
        </div>
    );
}
