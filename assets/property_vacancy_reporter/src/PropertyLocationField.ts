interface PropertyLocationFormData {
    streetName: string;
    houseNumber: string;
    postalCode: string;
    placeUuid: string;
    quarterPlaceUuid: string;
    placeDescription: string;
    boroughPlaceUuid: string;
}

enum PropertyLocationField {

    StreetName = 0,
    HouseNumber = 1,
    PostalCode = 2,
    PlaceId = 3,
    QuarterId = 4,
    PlaceDescription = 5,
    BoroughId = 6

}

enum PropertyLocationFieldName {

    StreetName = 'streetName',
    HouseNumber = 'houseNumber',
    PostalCode = 'postalCode',
    PlaceId = 'placeUuid',
    QuarterId = 'quarterPlaceUuid',
    BoroughId = 'boroughPlaceUuid',
    PlaceDescription = 'placeDescription',

}

enum PropertyLocationFieldNameKey {

    'streetName' = 0,
    'houseNumber' = 1,
    'postalCode' = 2,
    'placeUuid' = 3,
    'quarterPlaceUuid' = 4,
    'placeDescription' = 5,
    'boroughPlaceUuid' = 6

}

export { PropertyLocationField, PropertyLocationFormData, PropertyLocationFieldName, PropertyLocationFieldNameKey };
