import React from 'react';
import { createRoot, Root } from 'react-dom/client';
import Api from './Api';

const currentScript: HTMLScriptElement = (document.currentScript as HTMLScriptElement);

// @ts-ignore
window.initializePVR = (accountKey: string, elementId: string): void => {
    const root: Root = createRoot(document.getElementById(elementId)!);

    root.render(
        <React.StrictMode>
            <Api
                accountKey={accountKey}
                apiUrl={currentScript.src.replace('/property_vacancy_reporter/build/api.js', '')}
            />
        </React.StrictMode>
    );
}
