import apiStyle from './Api.module.css';
import bootstrapStyle from './Bootstrap.module.scss';
import CheckboxField from '../../components/src/Form/CheckboxField';
import DateField from '../../components/src/Form/DateField';
import InputField from '../../components/src/Form/InputField';
import SelectField from '../../components/src/Form/SelectField';
import TextareaField from '../../components/src/Form/TextareaField';
import { BuildingCondition, IndustryClassification } from './InterfaceLibrary';
import { PropertyInformationField, PropertyInformationFieldName, PropertyInformationFormData } from './PropertyInformationField';
import React, { useEffect } from 'react';

interface PropertyInformationStepProps {

    buildingConditions: BuildingCondition[];
    industryClassifications: IndustryClassification[];
    requiredPropertyInformationFormFields: PropertyInformationField[];
    propertyInformationFormData: PropertyInformationFormData;
    setPropertyInformationFormData: Function;
    formRef: React.MutableRefObject<HTMLFormElement>;
    errors: { [key: string]: string };

}

export default function PropertyInformationStep({buildingConditions, industryClassifications, requiredPropertyInformationFormFields, propertyInformationFormData, setPropertyInformationFormData, formRef, errors}: PropertyInformationStepProps) {

    useEffect(() => {
        if (propertyInformationFormData.objectBecomesEmpty) {
            setPropertyInformationFormData({...propertyInformationFormData, objectIsEmptySince: ''});
        } else {
            setPropertyInformationFormData({...propertyInformationFormData, objectBecomesEmptyFrom: ''});
        }
    }, [propertyInformationFormData.objectIsEmpty, propertyInformationFormData.objectBecomesEmpty]);

    const handleInputChange = (name: string, value: string | boolean) => {
        if (name === PropertyInformationFieldName.StoreWindowAvailable) {
            value = Boolean(parseInt(value as string));
        }
        setPropertyInformationFormData({...propertyInformationFormData, [name]: value});
    };

    const checkRequired = (propertyInformationField: number): boolean => {
        return requiredPropertyInformationFormFields.includes(propertyInformationField);
    };

    const showPlotAreaField = [0, 3, 5, 6, 9, 11,
                               15].includes(parseInt(propertyInformationFormData.industryClassificationId));
    const showRetailAreaField = [1, 13].includes(parseInt(propertyInformationFormData.industryClassificationId));
    const showGastroAreaField = [3].includes(parseInt(propertyInformationFormData.industryClassificationId));
    const showLivingAreaField = [14].includes(parseInt(propertyInformationFormData.industryClassificationId));
    const showUsageAreaField = ![5].includes(parseInt(propertyInformationFormData.industryClassificationId));
    const showGroundLevelSalesAreaField = [1].includes(parseInt(propertyInformationFormData.industryClassificationId));

    const getCurrentDate = (separator: string = '-'): string => {
        let newDate = new Date();
        let date = newDate.getDate();
        let month = newDate.getMonth() + 1;
        let year = newDate.getFullYear();
        return `${year}${separator}${month < 10 ? `0${month}` : `${month}`}${separator}${date < 10 ? `0${date}` : `${date}`}`;
    };

    const getPropertyUsageToolTipText = (): JSX.Element => {

        const industryClassificationIds = industryClassifications.map(industryClassification => industryClassification.levelOne)

        const text: JSX.Element = (<span>
            {propertyInformationFormData.objectIsEmpty === true ? <span>Bitte geben Sie an, welche Nutzung das Objekt zuletzt hatte.<br/><br/></span>  : <span>Bitte geben Sie an, welche Nutzung das Objekt derzeit noch hat.<br/><br/></span>}
            {industryClassificationIds.includes(1) && <span><strong>*Einzelhandel:</strong><br/>Klassische Einzelhandelsgeschäft, wie Buchläden, Lebensmitteleinzelhandel, Textilwarengeschäfte<br/></span>}
            {industryClassificationIds.includes(2) && <span><strong>*Dienstleistung:</strong><br/>Ein Klassischer Dienstleistungsbetrieb, wie ein Friseur, ein Architekt aber auch Banken, Reinigungen oder Anwaltsbüros<br/></span>}
            {industryClassificationIds.includes(3) && <span><strong>*Gastronomie/Beherbergung:</strong><br/>Beherbergungsbetriebe wie Hotels und Gästehäuser sowie Restaurants, Bars oder Cafés<br/></span>}
            {industryClassificationIds.includes(4) && <span><strong>*Kunst, Kultur, Unterhaltung, Sport, Spiel, Freizeit:</strong><br/>Theater, Museen, Kinos, Kirchen, Sporthallen<br/></span>}
            {industryClassificationIds.includes(5) && <span><strong>*Parken, Personenbeförderung & Transport:</strong><br/>z. B. Parkhäuser, Tiefgaragen<br/></span>}
            {industryClassificationIds.includes(6) && <span><strong>*Bildung & Lernen:</strong><br/>z. B. Schulen, Kindergärten/-tagesstätten, Fort- und Weiterbildungsangebote, Erwachsenenbildung<br/></span>}
            {industryClassificationIds.includes(7) && <span><strong>*Gesundheitswesen:</strong><br/>z. B. Krankenhäuser/Kliniken, Tageskliniken, Arzt- und Zahnarztpraxen, Orthopädie<br/></span>}
            {industryClassificationIds.includes(8) && <span><strong>*Sozialwesen/Gemeinwohl:</strong><br/>z. B. Jugendzentrum, Bürgerbüro/-haus, Obdachlosen-, Jugend-, Gemeinschafts- und Nachbarschaftshilfe<br/></span>}
            {industryClassificationIds.includes(9) && <span><strong>*Einrichtungen der Verwaltung:</strong><br/>z. B. Rathaus, Finanzamt, Arbeitsamt und andere Verwaltungseinrichtungen<br/></span>}
            {industryClassificationIds.includes(10) && <span><strong>*Interessensvertretungen, Vereine, kirchliche Einrichtungen:</strong><br/>z. B. Gewerkschaften, Parteien, Verbände, Jugendorganisationen, Kammern<br/></span>}
            {industryClassificationIds.includes(11) && <span><strong>*Handwerk, Produktion, Manufaktur:</strong><br/>Produktions- und Handwerksbetriebe, beispielsweise für Goldschmiedearbeiten, Sanitär- und Elektroinstallationen<br/></span>}
            {industryClassificationIds.includes(12) && <span><strong>*Kreativwirtschaft:</strong><br/>z. B. Verlagswesen, Film, Hörfunk und TV, Tonstudios<br/></span>}
            {industryClassificationIds.includes(13) && <span><strong>*Großhandel & Handelsvermittlung:</strong><br/>Unternehmen, die Waren verschiedener Hersteller beschaffen und an gewerbliche Kunden und Großabnehmer weiterverkaufen<br/></span>}
            {industryClassificationIds.includes(14) && <span><strong>*Andere gewerbliche Nutzung\t:</strong><br/>z. B. als Büro für andere als die aufgelisteten Zwecke<br/></span>}
            {industryClassificationIds.includes(15) && <span><strong>*Wohnen:</strong><br/>z. B. Wohnungen<br/></span>}
            {industryClassificationIds.includes(16) && <span><strong>*Andere nicht-gewerbliche Nutzung:</strong><br/>Sonstiges<br/></span>}
        </span>)
        return text;
    };

    return (
        <form ref={formRef}>
            <div className={bootstrapStyle['row']}>
                <div className={[apiStyle['subtitle'], bootstrapStyle['mb-1']].join(' ')}>Nutzungsinformationen</div>
                <div className={[bootstrapStyle['col-12'], bootstrapStyle['mb-5']].join(' ')}>
                    <button className={[propertyInformationFormData.objectIsEmpty && apiStyle['btn-primary-plain'],
                                        bootstrapStyle['col-6'],
                                        bootstrapStyle['border-0'], bootstrapStyle['p-3']].join(' ')}
                            name={PropertyInformationFieldName.ObjectIsEmpty}
                            onClick={(event: React.SyntheticEvent) => {
                                event.preventDefault();
                                setPropertyInformationFormData({...propertyInformationFormData, objectBecomesEmpty: false, objectIsEmpty: true});
                            }}>
                        Objekt steht leer
                    </button>
                    <button className={[propertyInformationFormData.objectBecomesEmpty && apiStyle['btn-primary-plain'],
                                        bootstrapStyle['col-6'],
                                        bootstrapStyle['border-0'], bootstrapStyle['p-3']].join(' ')}
                            name={PropertyInformationFieldName.ObjectBecomesEmpty}
                            onClick={(event: React.SyntheticEvent) => {
                                event.preventDefault();
                                setPropertyInformationFormData({...propertyInformationFormData, objectBecomesEmpty: true, objectIsEmpty: false});
                            }}>
                        Objekt fällt leer
                    </button>
                </div>

                <div className={[bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['col-lg-4'],
                                 bootstrapStyle['mb-4']].join(' ')}>
                    <SelectField name={PropertyInformationFieldName.IndustryClassification}
                                 label={propertyInformationFormData.objectBecomesEmpty ? 'Aktuelle Nutzung wählen' : 'Letzte Nutzung wählen'}
                                 value={propertyInformationFormData.industryClassificationId}
                                 isRequired={checkRequired(PropertyInformationField.IndustryClassification)}
                                 error={errors[PropertyInformationFieldName.IndustryClassification]}
                                 handleInputChange={handleInputChange}
                                 popoverText={getPropertyUsageToolTipText()}
                                 options={industryClassifications.map((industryClassification) => ({label: industryClassification.name, value: industryClassification.levelOne}))}/>
                </div>

                <div className={[bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['col-lg-4'],
                                 bootstrapStyle['mb-4']].join(' ')}>
                    <InputField
                        value={propertyInformationFormData.propertyUserCompanyName}
                        label={propertyInformationFormData.objectBecomesEmpty ? 'Aktueller Nutzer' : 'Letzter Nutzer'}
                        type={'text'}
                        name={PropertyInformationFieldName.PropertyUserCompanyName}
                        error={errors[PropertyInformationFieldName.PropertyUserCompanyName]}
                        isRequired={checkRequired(PropertyInformationField.PropertyUserCompanyName)}
                        handleInputChange={handleInputChange}
                    />
                </div>

                <div className={[bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['col-lg-4'],
                                 bootstrapStyle['mb-4']].join(' ')}>
                    {propertyInformationFormData.objectBecomesEmpty === true ?
                     <DateField
                         minDate={getCurrentDate()}
                         value={propertyInformationFormData.objectBecomesEmptyFrom}
                         name={PropertyInformationFieldName.ObjectBecomesEmptyFrom}
                         label={'Leerstand ab'}
                         error={errors && errors[PropertyInformationFieldName.ObjectBecomesEmptyFrom]}
                         required={checkRequired(PropertyInformationField.ObjectBecomesEmptyFrom)}
                         handleInputChange={handleInputChange}
                         popoverText={'Bitte geben Sie an, zu welchem Datum das Objekt voraussichtlich leer fällt.'}
                     />
                                                                             :
                     <DateField
                         maxDate={getCurrentDate()}
                         value={propertyInformationFormData.objectIsEmptySince}
                         name={PropertyInformationFieldName.ObjectIsEmptySince}
                         label={'Leerstand seit'}
                         error={errors && errors[PropertyInformationFieldName.ObjectIsEmptySince]}
                         required={checkRequired(PropertyInformationField.ObjectIsEmptySince)}
                         handleInputChange={handleInputChange}
                         popoverText={'Bitte geben Sie an, seit wann das Objekt leer steht, sofern Ihnen diese Information bekannt ist.'}
                     />
                    }
                </div>
            </div>

            <div className={bootstrapStyle['row']}>
                <div className={apiStyle['subtitle']}>Flächenangaben</div>
                <div className={[bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['col-lg-4'],
                                 bootstrapStyle['mb-4']].join(' ')}>
                    <InputField
                        type={'number'}
                        value={propertyInformationFormData.areaSize}
                        name={PropertyInformationFieldName.AreaSize}
                        error={errors && errors[PropertyInformationFieldName.AreaSize]}
                        isRequired={checkRequired(PropertyInformationField.AreaSize)}
                        label="Gesamtfläche ( m² )"
                        handleInputChange={handleInputChange}
                        popoverText={'Bitte geben Sie die ungefähre Gesamtfläche des Objekts an.'}
                    />
                </div>
                {showPlotAreaField === true &&
                <div className={[bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['col-lg-4'],
                                 bootstrapStyle['mb-4']].join(' ')}>
                    <InputField
                        type={'number'}
                        value={propertyInformationFormData.plotSize}
                        name={PropertyInformationFieldName.PlotSize}
                        error={errors && errors[PropertyInformationFieldName.PlotSize]}
                        isRequired={checkRequired(PropertyInformationField.PlotSize)}
                        label="Grundstücksfläche ( m² )"
                        handleInputChange={handleInputChange}
                    />
                </div>
                }
                {showRetailAreaField === true &&
                <div className={[bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['col-lg-4'],
                                 bootstrapStyle['mb-4']].join(' ')}>
                    <InputField
                        type={'number'}
                        value={propertyInformationFormData.retailSpace}
                        name={PropertyInformationFieldName.RetailSpace}
                        error={errors && errors[PropertyInformationFieldName.RetailSpace]}
                        isRequired={checkRequired(PropertyInformationField.RetailSpace)}
                        label={'Verkaufsfläche ( m² )'}
                        handleInputChange={handleInputChange}
                    />
                </div>
                }
                {showLivingAreaField === true &&
                <div className={[bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['col-lg-4'],
                                 bootstrapStyle['mb-4']].join(' ')}>
                    <InputField
                        type={'number'}
                        value={propertyInformationFormData.livingSpace}
                        name={PropertyInformationFieldName.LivingSpace}
                        error={errors && errors[PropertyInformationFieldName.LivingSpace]}
                        isRequired={checkRequired(PropertyInformationField.LivingSpace)}
                        label={'Wohnfläche ( m² )'}
                        handleInputChange={handleInputChange}
                    />
                </div>
                }
                {showGastroAreaField === true &&
                <div className={[bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['col-lg-4'],
                                 bootstrapStyle['mb-4']].join(' ')}>
                    <InputField
                        type={'number'}
                        value={propertyInformationFormData.gastronomySpace}
                        name={PropertyInformationFieldName.GastroSpace}
                        error={errors && errors[PropertyInformationFieldName.GastroSpace]}
                        isRequired={checkRequired(PropertyInformationField.GastroSpace)}
                        label={'Gastrofläche ( m² )'}
                        handleInputChange={handleInputChange}
                    />
                </div>
                }
                {showUsageAreaField === true &&
                <div className={[bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['col-lg-4'],
                                 bootstrapStyle['mb-4']].join(' ')}>
                    <InputField
                        type={'number'}
                        value={propertyInformationFormData.subsidiarySpace}
                        name={PropertyInformationFieldName.SubsidiarySpace}
                        error={errors && errors[PropertyInformationFieldName.SubsidiarySpace]}
                        isRequired={checkRequired(PropertyInformationField.SubsidiarySpace)}
                        label={'Nebenfläche ( m² )'}
                        handleInputChange={handleInputChange}
                    />
                </div>
                }
                <div className={[bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['col-lg-4'],
                                 bootstrapStyle['mb-4']].join(' ')}>
                    <SelectField label={'Freifläche'}
                                 name={PropertyInformationFieldName.OpenSpace}
                                 value={propertyInformationFormData.openSpace}
                                 isRequired={checkRequired(PropertyInformationField.OpenSpace)}
                                 error={errors[PropertyInformationFieldName.OpenSpace]}
                                 handleInputChange={handleInputChange}
                                 options={[
                                     {value: 0, label: 'Nicht vorhanden'},
                                     {value: 1, label: 'Kann genehmigt werden'},
                                     {value: 2, label: 'Ist vorhanden'},
                                 ]}/>
                </div>
                {[1, 2].includes(parseInt(propertyInformationFormData.openSpace)) &&
                <div className={[bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['col-lg-4'],
                                 bootstrapStyle['mb-4']].join(' ')}>
                    <InputField
                        type={'number'}
                        value={propertyInformationFormData.usableOpenSpace}
                        name={PropertyInformationFieldName.UsableOpenSpace}
                        error={errors && errors[PropertyInformationFieldName.UsableOpenSpace]}
                        isRequired={checkRequired(PropertyInformationField.UsableOpenSpace)}
                        label={'Nutzbare Freifläche ( m² )'}
                        handleInputChange={handleInputChange}
                    />
                </div>
                }
                {showGroundLevelSalesAreaField === true &&
                <div className={[bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['col-lg-4'],
                                 bootstrapStyle['mb-4']].join(' ')}>
                    <CheckboxField
                        name={PropertyInformationFieldName.GroundLevelSalesArea}
                        checked={propertyInformationFormData.groundLevelSalesArea}
                        label={'ebenerdige Verkaufsfläche'}
                        isRequired={false}
                        handleInputChange={handleInputChange}
                    />
                </div>
                }
            </div>

            <div className={bootstrapStyle['row']}>
                <div className={apiStyle['subtitle']}>Weitere Informationen</div>

                <div className={[bootstrapStyle['col-12'], bootstrapStyle['col-md-4'], bootstrapStyle['col-lg-4'],
                                 bootstrapStyle['mb-4']].join(' ')}>
                    <div>
                        <SelectField label={'Objektzustand wählen'}
                                     name={PropertyInformationFieldName.BuildingCondition}
                                     value={propertyInformationFormData.buildingCondition}
                                     error={errors[PropertyInformationFieldName.BuildingCondition]}
                                     isRequired={checkRequired(PropertyInformationField.BuildingCondition)}
                                     handleInputChange={handleInputChange}
                                     options={buildingConditions.map((buildingCondition) => ({value: buildingCondition.id, label: buildingCondition.name}))}/>
                    </div>
                </div>

                <div className={[bootstrapStyle['col-12'], bootstrapStyle['col-md-4'], bootstrapStyle['col-lg-4'],
                                 bootstrapStyle['mb-4']].join(' ')}>
                    <div>
                        <SelectField label={'Schaufenster'}
                                     name={PropertyInformationFieldName.StoreWindowAvailable}
                                     value={propertyInformationFormData.storeWindowAvailable ? 1 : 0}
                                     isRequired={checkRequired(PropertyInformationField.StoreWindowAvailable)}
                                     error={errors[PropertyInformationFieldName.StoreWindowAvailable]}
                                     handleInputChange={handleInputChange}
                                     options={[
                                         {value: 0, label: 'Nein'},
                                         {value: 1, label: 'Ja'},
                                     ]}/>
                    </div>
                </div>

                <div className={[bootstrapStyle['col-12'], bootstrapStyle['col-md-4'], bootstrapStyle['col-lg-4'],
                                 bootstrapStyle['mb-4']].join(' ')}>
                    {propertyInformationFormData.storeWindowAvailable &&
                    <div>
                        <InputField
                            type={'number'}
                            value={propertyInformationFormData.storeWindowWidth}
                            name={PropertyInformationFieldName.StoreWindowWidth}
                            error={errors && errors[PropertyInformationFieldName.StoreWindowWidth]}
                            isRequired={checkRequired(PropertyInformationField.StoreWindowWidth)}
                            label={'Gesamtbreite Schaufenster ( m )'}
                            handleInputChange={handleInputChange}
                        />
                    </div>
                    }
                </div>
            </div>
            <div className={bootstrapStyle['row']}>
                {showRetailAreaField === true &&
                <div className={[bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['col-lg-4'],
                                 bootstrapStyle['mb-4']].join(' ')}>
                    <InputField
                        type={'number'}
                        value={propertyInformationFormData.shopWidth}
                        name={PropertyInformationFieldName.ShopWidth}
                        error={errors && errors[PropertyInformationFieldName.ShopWidth]}
                        isRequired={checkRequired(PropertyInformationField.ShopWidth)}
                        label={'Ladenbreite ( m )'}
                        handleInputChange={handleInputChange}
                    />
                </div>
                }
                <div className={[bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['col-lg-4'],
                                 bootstrapStyle['mb-4']].join(' ')}>

                    <SelectField
                        label={'Barrierefreier Zugang'}
                        name={PropertyInformationFieldName.BarrierFreeAccess}
                        value={propertyInformationFormData.barrierFreeAccess}
                        isRequired={false}
                        error={errors[PropertyInformationFieldName.BarrierFreeAccess]}
                        handleInputChange={handleInputChange}
                        options={[
                            {value: 0, label: 'Ist vorhanden'},
                            {value: 1, label: 'Kann gewährleistet werden'},
                            {value: 2, label: 'Ist nicht vorhanden'},
                            {value: 3, label: 'Ist nicht möglich'},
                        ]}
                    />
                </div>
                <div className={[bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['col-lg-4'],
                                 bootstrapStyle['mb-4']].join(' ')}>
                    <InputField
                        type={'number'}
                        value={propertyInformationFormData.numberOfParkingLots}
                        name={PropertyInformationFieldName.NumberOfParkingLots}
                        error={errors && errors[PropertyInformationFieldName.NumberOfParkingLots]}
                        isRequired={checkRequired(PropertyInformationField.NumberOfParkingLots)}
                        label={'Anzahl Stellplätze'}
                        handleInputChange={handleInputChange}
                    />
                </div>
                <div className={[bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['col-lg-4'],
                                 bootstrapStyle['mb-4']].join(' ')}>
                    <SelectField
                        label={'Angebotsart'}
                        name={PropertyInformationFieldName.PropertyOfferType}
                        value={propertyInformationFormData.propertyOfferType}
                        isRequired={checkRequired(PropertyInformationField.PropertyOfferType)}
                        error={errors[PropertyInformationFieldName.PropertyOfferType]}
                        handleInputChange={handleInputChange}
                        options={[
                            {value: 0, label: 'Zum Kauf'},
                            {value: 1, label: 'Zur Miete'},
                            {value: 2, label: 'Zur Pacht'},
                        ]}
                    />
                </div>
                <div className={[bootstrapStyle['col-12'], bootstrapStyle['mb-4']].join(' ')}>
                    <TextareaField
                        label={'Objektbeschreibung'}
                        name={PropertyInformationFieldName.PropertyDescription}
                        isRequired={checkRequired(PropertyInformationField.PropertyDescription)}
                        error={errors[PropertyInformationFieldName.PropertyDescription]}
                        value={propertyInformationFormData.propertyDescription}
                        handleInputChange={handleInputChange}
                    />
                </div>
            </div>
        </form>
    );
}
