import bootstrapStyle from './Bootstrap.module.scss';
import CheckboxField from '../../components/src/Form/CheckboxField';
import InputField from '../../components/src/Form/InputField';
import SelectField from '../../components/src/Form/SelectField';
import ModalField from '../../components/src/Form/ModalField';
import { ReporterField, ReporterFieldName, ReporterFormData } from './ReporterField';
import React from 'react';

interface ReporterStepProps {
    requiredReporterFormFields: ReporterField[];
    formRef: React.MutableRefObject<HTMLFormElement>;
    reporterFormData: ReporterFormData;
    setReporterFormData: Function;
    privacyPolicy: string;
    errors: { [key: string]: string };
}

export default function ReporterStep({
    requiredReporterFormFields,
    errors,
    formRef,
    reporterFormData,
    setReporterFormData,
    privacyPolicy
}: ReporterStepProps) {

    const checkRequired = (reporterField: ReporterField): boolean => {
        return requiredReporterFormFields.includes(reporterField);
    };

    const handleInputChange = (name: string, value: string | boolean): void => {
        if (name === 'alsoPropertyOwner' && value != null) {
            value = Boolean(parseInt(value as string));
        }
        setReporterFormData({...reporterFormData, [name]: value});
    };

    const setDsgvoConfirm = (): void => {
        setReporterFormData({...reporterFormData, privacyPolicyAccept: true});
    };

    return (
        <>
            <form ref={formRef} className={bootstrapStyle['row']}>
                <div className={[bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['mb-4']].join(' ')}>
                    <InputField
                        label={'Unternehmen'}
                        type={'text'}
                        name={ReporterFieldName.CompanyName}
                        value={reporterFormData.companyName}
                        isRequired={checkRequired(ReporterField.CompanyName)}
                        handleInputChange={handleInputChange}
                        error={errors[ReporterFieldName.CompanyName]}
                    />
                </div>

                <div className={[bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['mb-4']].join(' ')}>
                    <SelectField
                        label={'Anrede'}
                        value={reporterFormData.salutation}
                        name={ReporterFieldName.Salutation}
                        isRequired={checkRequired(ReporterField.Salutation)}
                        error={errors[ReporterFieldName.Salutation]}
                        handleInputChange={handleInputChange}
                        options={[
                            {value: 0, label: 'Herr'},
                            {value: 1, label: 'Frau'},
                            {value: 2, label: 'Divers'}
                        ]}
                    />
                </div>

                <div className={[bootstrapStyle['col-12'], bootstrapStyle['col-md-6'],
                                 bootstrapStyle['mb-4']].join(' ')}>
                    <InputField
                        label={'Vorname'}
                        type={'text'}
                        value={reporterFormData.firstName}
                        name={ReporterFieldName.FirstName}
                        error={errors[ReporterFieldName.FirstName]}
                        isRequired={checkRequired(ReporterField.FirstName)}
                        handleInputChange={handleInputChange}
                    />
                </div>

                <div className={[bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['mb-4']].join(' ')}>
                    <InputField
                        label={'Nachname'}
                        type={'text'}
                        value={reporterFormData.name}
                        name={ReporterFieldName.Name}
                        error={errors[ReporterFieldName.Name]}
                        isRequired={checkRequired(ReporterField.Name)}
                        handleInputChange={handleInputChange}
                    />
                </div>

                <div className={[bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['mb-4']].join(' ')}>
                    <InputField
                        label={'Telefon'}
                        type={'text'}
                        value={reporterFormData.phoneNumber}
                        name={ReporterFieldName.PhoneNumber}
                        error={errors[ReporterFieldName.PhoneNumber]}
                        isRequired={checkRequired(ReporterField.PhoneNumber)}
                        handleInputChange={handleInputChange}
                    />
                </div>

                <div className={[bootstrapStyle['col-12'], bootstrapStyle['col-md-6'], bootstrapStyle['mb-4']].join(' ')}>
                    <InputField
                        label={'E-Mail-Adresse'}
                        type={'email'}
                        value={reporterFormData.email}
                        name={ReporterFieldName.Email}
                        error={errors[ReporterFieldName.Email]}
                        isRequired={true}
                        handleInputChange={handleInputChange}
                    />
                </div>

                <div className={[bootstrapStyle['col-12'], bootstrapStyle['col-xxl-4'], bootstrapStyle['mb-4']].join(' ')}>
                    <SelectField
                        name={'alsoPropertyOwner'}
                        label={'Eigentümer:in'}
                        value={reporterFormData.alsoPropertyOwner ? 1 : 0}
                        options={[
                            {
                                value: 1,
                                label: 'Ich bin Eigentümer:in'
                            },
                            {
                                value: 0,
                                label: 'Ich bin nicht Eigentümer:in'
                            }
                        ]}
                        handleInputChange={handleInputChange}
                        isRequired={false}
                    />
                </div>

                <div className={[bootstrapStyle['col-12'], bootstrapStyle['col-xxl-8']].join(' ')}>
                    {reporterFormData.alsoPropertyOwner === false &&
                        <div className={[bootstrapStyle['mb-2'], bootstrapStyle['mb-md-0']].join(' ')}>
                            <CheckboxField
                                isRequired={true}
                                name={'ownerConfirmation'}
                                checked={reporterFormData.ownerConfirmation}
                                label={'* Ich bestätige, dass ich vom Eigentümer beauftragt bin, das Objekt zu melden'}
                                handleInputChange={handleInputChange}
                                error={errors['ownerConfirmation']}
                                isFontSmall={true}
                            />
                        </div>
                    }

                    <CheckboxField
                        isRequired={true}
                        name={ReporterFieldName.PrivacyPolicyAccept}
                        checked={reporterFormData.privacyPolicyAccept}
                        error={errors[ReporterFieldName.PrivacyPolicyAccept]}
                        label={'* Mit Absenden des Kontaktformulars werden Ihre Angaben zum Objekt und Ihre personenbezogenen Daten auf elektronischem Weg an den zuständigen Sachbearbeiter übermittelt. Gleichzeitig stimmen Sie zu, dass diese gemäß unserer Datenschutzbestimmungen zur Bearbeitung Ihrer Meldung erhoben und verarbeitet werden dürfen.'}
                        handleInputChange={handleInputChange}
                        isFontSmall={true}
                    />

                    <div className={[ bootstrapStyle['mt-2'], bootstrapStyle['mt-md-0'], bootstrapStyle['px-0'], bootstrapStyle['px-md-4'], bootstrapStyle['text-md-end'], bootstrapStyle['text-center'] ].join(' ')}>
                        <ModalField name={'dsgvoModal'} buttonLabel={'Informationen zum Datenschutz'} title={'Nutzungsbeschreibung und Datenschutzerklärung'} text={privacyPolicy} submitModal={setDsgvoConfirm} submitModalButtonText={'Akzeptieren'}/>
                    </div>
                </div>
            </form>
        </>
    );
}
