import apiStyle from './Api.module.css';
import bootstrapStyle from './Bootstrap.module.scss';
import LoadingImage from '../../lean/images/lean-loading.gif';
import PropertyImagesStep from './PropertyImagesStep';
import PropertyInformationStep from './PropertyInformationStep';
import PropertyLocationStep from './PropertyLocationStep';
import ReporterStep from './ReporterStep';
import ReportResponseStep from './ReportResponseStep';
import { BuildingCondition, IndustryClassification, Place } from './InterfaceLibrary';
import { PropertyInformationFieldName, PropertyInformationFieldNameKey, PropertyInformationFormData } from './PropertyInformationField';
import { PropertyLocationFieldNameKey, PropertyLocationFormData } from './PropertyLocationField';
import { ReporterFieldName, ReporterFieldNameKey, ReporterFormData } from './ReporterField';
import { createTheme, Theme, ThemeProvider } from '@mui/material/styles';
import {Button} from '@mui/material';
import Axios, { AxiosResponse } from 'axios';
import dayjs from "dayjs";
import Dropzone from 'dropzone';
import React, { useEffect, useState } from 'react';

interface ApiProps {
    accountKey: string;
    apiUrl: string;
}

export default function Api({accountKey, apiUrl}: ApiProps) {
    const apiContainer = React.useRef<HTMLDivElement>();
    const progressBar = React.useRef<HTMLDivElement>();
    const propertyInformationFrom = React.useRef<HTMLFormElement>();
    const propertyLocationFrom = React.useRef<HTMLFormElement>();
    const reporterFrom = React.useRef<HTMLFormElement>();
    const [currentStepNumber, setCurrentStepNumber] = useState<number>(0);

    const initialPropertyInformationFormData = {
        industryClassificationId: '',
        propertyUserCompanyName: '',
        areaSize: '',
        plotSize: '',
        retailSpace: '',
        livingSpace: '',
        gastronomySpace: '',
        subsidiarySpace: '',
        openSpace: '',
        usableOpenSpace: '',
        storeWindowAvailable: false,
        storeWindowWidth: '',
        shopWidth: '',
        numberOfParkingLots: '',
        objectIsEmpty: true,
        objectBecomesEmpty: false,
        objectBecomesEmptyFrom: '',
        objectIsEmptySince: '',
        buildingCondition: '',
        propertyOfferType: '',
        barrierFreeAccess: '',
        groundLevelSalesArea: false,
        propertyDescription: ''
    };

    const initialPropertyLocationFormData = {
        boroughPlaceUuid: '',
        houseNumber: '',
        quarterPlaceUuid: '',
        placeDescription: '',
        placeUuid: '',
        postalCode: '',
        streetName: ''
    };

    const initialReporterFormData = {
        name: '',
        email: '',
        firstName: '',
        phoneNumber: '',
        salutation: '',
        companyName: '',
        privacyPolicyAccept: false,
        ownerConfirmation: false,
        alsoPropertyOwner: true
    };

    const [propertyInformationFormData, setPropertyInformationFormData] = useState<PropertyInformationFormData>(initialPropertyInformationFormData);

    const [propertyLocationFormData, setPropertyLocationFormData] = useState<PropertyLocationFormData>(initialPropertyLocationFormData);

    const [reporterFormData, setReporterFormData] = useState<ReporterFormData>(initialReporterFormData);

    const [propertyUsageList, setPropertyUsageList] = useState<IndustryClassification[]>([]);
    const [places, setPlaces] = useState<Place[]>();
    const [isResponseSuccessful, setIsResponseSuccessful] = useState<boolean>(false);
    const [onReportSend, setOnReportSend] = useState<boolean>(false);
    const [dropzone, setDropzone] = useState<Dropzone>(null);
    const [config, setConfig] = useState<any>();
    const [errors, setErrors] = useState<{ [key: string]: string }>({});
    const [theme, setTheme] = useState<Theme>(createTheme());

    useEffect(() => {
        fetchConfiguration();
    }, []);

    useEffect(() => {
        updateProgressBar();
        if (currentStepNumber > 1 && currentStepNumber < 5 && progressBar != undefined) {
            progressBar.current.parentElement.parentElement.scrollIntoView({behavior: 'smooth', block: 'start'});
        }
    }, [currentStepNumber]);

    useEffect(() => {
        if (config != undefined) {
            setCurrentStepNumber(1);
        }
    }, [config]);

    const gotoNextStep = (): void => {
        if (validateFormDataStep() === true) {
            if (currentStepNumber < 5) {
                setCurrentStepNumber(currentStepNumber + 1);
            }
        }
    };

    const gotoPreviousStep = (): void => {
        if (currentStepNumber > 1) {
            setCurrentStepNumber(currentStepNumber - 1);
            setErrors({})
        }
    };

    const sendVacancyReport = async (): Promise<void> => {
        if (validateFormDataStep() === false) return;

        let formData = prepareFormDataToSend();

        setOnReportSend(true);

        try {
            const response = await Axios.post(apiUrl + '/api/property-vacancy-reporter/reports', formData);

            if (response.status === 200) {
                setIsResponseSuccessful(true);
            }

            if (dropzone.getQueuedFiles().length > 0) {
                uploadImages(response.data.uuid);
                dropzone.on('queuecomplete', (): void => {
                    gotoNextStep();
                    resetVacancyValuesForNewVacancyReport();
                });
            } else {
                gotoNextStep();
                resetVacancyValuesForNewVacancyReport();
            }
        } catch (error) {
            setIsResponseSuccessful(false);
            gotoNextStep();
            resetVacancyValuesForNewVacancyReport();
        }
    };

    const resetVacancyValuesForNewVacancyReport = (): void => {
        setOnReportSend(false);
        setPropertyInformationFormData(initialPropertyInformationFormData);
        setPropertyLocationFormData(initialPropertyLocationFormData);
        setReporterFormData(initialReporterFormData);
        dropzone.removeAllFiles();
    };

    const uploadImages = (uuid: string): void => {
        dropzone.options.url = apiUrl + '/api/property-vacancy-reporter/reports/' + uuid + '/photo/upload';
        dropzone.processQueue();
    };

    const newVacancyReport = (): void => {
        setCurrentStepNumber(1);
    };

    const showStep = (): JSX.Element => {
        switch (currentStepNumber) {
            case 0:
                return <div className={[bootstrapStyle['row'], bootstrapStyle['justify-content-center'], bootstrapStyle['text-center'],
                    bootstrapStyle['m-5']].join(' ')}>
                    <div className={[bootstrapStyle['col-6'], bootstrapStyle['col-md-3'], bootstrapStyle['justify-content-center'], bootstrapStyle['text-center'],
                        bootstrapStyle['m-5']].join(' ')}>
                        <img className={bootstrapStyle['col-12']} src={LoadingImage} alt="Loading..."/>
                    </div>
                    <p className={bootstrapStyle['mt-3']}>Leerstandsmelder wird vorbereitet... </p>
                </div>;
            case 1:
                return <PropertyInformationStep
                    formRef={propertyInformationFrom}
                    requiredPropertyInformationFormFields={config.requiredPropertyInformationFormFields}
                    propertyInformationFormData={propertyInformationFormData}
                    setPropertyInformationFormData={(propertyInformationFormData: PropertyInformationFormData) => {
                        setPropertyInformationFormData(propertyInformationFormData);
                    }}
                    industryClassifications={propertyUsageList}
                    buildingConditions={fetchBuildingConditionsFromApi()}
                    errors={errors}
                />;
            case 2:
                return <PropertyLocationStep
                    formRef={propertyLocationFrom}
                    requiredPropertyLocationFormFields={config.requiredPropertyLocationFormFields}
                    propertyLocationFormData={propertyLocationFormData}
                    setPropertyLocationFormData={(propertyLocationFormData: PropertyLocationFormData) => setPropertyLocationFormData(propertyLocationFormData)}
                    placeList={places}
                    apiUrl={apiUrl}
                    errors={errors}
                />;
            case 3:
                return <PropertyImagesStep setDropzone={setDropzone} dropzone={dropzone}/>;
            case 4:
                return <ReporterStep
                    formRef={reporterFrom}
                    requiredReporterFormFields={config.requiredReporterFormFields}
                    reporterFormData={reporterFormData}
                    setReporterFormData={(reporterFormData: ReporterFormData) => setReporterFormData(reporterFormData)}
                    privacyPolicy={config.privacyPolicy}
                    errors={errors}
                />;
            case 5:
                return <ReportResponseStep newVacancyReport={newVacancyReport}
                                           isResponseSuccessful={isResponseSuccessful}/>;
        }
    };

    const validateEmail = (mail: string): boolean => {
        return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,})+$/.test(mail);
    };

    const validatePhoneNumber = (phone: string): boolean => {
        return /(?:\(\+?\d+\)|\+?\d+)(?:\s*[\-\/]*\s*\d+)+/.test(phone);
    };

    const checkRequired = (fieldName: string): boolean => {
        let fieldId: number;
        switch (currentStepNumber) {
            case 1:
                fieldId = PropertyInformationFieldNameKey[fieldName as keyof typeof PropertyInformationFieldNameKey];
                return config.requiredPropertyInformationFormFields.includes(fieldId) && propertyInformationFrom.current[fieldName] !== undefined;
            case 2:
                fieldId = PropertyLocationFieldNameKey[fieldName as keyof typeof PropertyLocationFieldNameKey];
                return config.requiredPropertyLocationFormFields.includes(fieldId) && propertyLocationFrom.current[fieldName] !== undefined;
            case 4:
                fieldId = ReporterFieldNameKey[fieldName as keyof typeof ReporterFieldNameKey];
                return (config.requiredReporterFormFields.includes(fieldId) || fieldName === 'ownerConfirmation') && reporterFrom.current[fieldName] !== undefined;
        }
    };

    const validateFrom = (formData: any): boolean => {
        let validationErrors: { [key: string]: string } = {};
        const validationResults = Object.keys(formData).map(fieldName => {
            const value = formData[fieldName];
            let required = checkRequired(fieldName);
            if ((parseFloat(value) < 0 || value === '' || value === null) && required) {
                validationErrors[fieldName] = 'Dieses Feld ist ein Pflichtfeld';
                return false;
            }
            if (fieldName === PropertyInformationFieldName.ObjectIsEmptySince || fieldName === PropertyInformationFieldName.ObjectBecomesEmptyFrom) {
                if (value !== '' && value !== null && dayjs(value).isValid() === false) {
                    validationErrors[fieldName] = 'Bitte geben Sie ein korrektes Datum ein';
                    return false;
                }
            }
            if (fieldName === ReporterFieldName.Email) {
                if (validateEmail(value.toString()) === false) {
                    validationErrors[fieldName] = 'E-Mail hat nicht das richtige Format';
                    return false;
                }
            }
            if (fieldName === ReporterFieldName.PhoneNumber) {
                if ((validatePhoneNumber(value.toString()) === false || value.length > 30) && required) {
                    validationErrors[fieldName] = 'Telefonnummer hat nicht das richtige Format';
                    return false;
                }
            }
            if (fieldName === ReporterFieldName.PrivacyPolicyAccept && value === false) {
                validationErrors[fieldName] = 'Bitte stimmen Sie den Datenschutzbestimmungen zu';
                return false;
            }
            if (fieldName === 'ownerConfirmation' && value === false && formData['alsoPropertyOwner'] === false) {
                validationErrors[fieldName] = 'Bitte betätigen Sie, dass Sie vom Eigentümer beauftragt worden sind, das Objekt zu melden';
                return false;
            }
            return true;
        });
        setErrors(validationErrors);
        return !validationResults.includes(false);
    };

    const validateFormDataStep = (): boolean => {
        let isValid: boolean = true;
        switch (currentStepNumber) {
            case 1:
                isValid = validateFrom(propertyInformationFormData);
                break;
            case 2:
                isValid = validateFrom(propertyLocationFormData);
                break;
            case 4:
                isValid = validateFrom(reporterFormData);
                break;
        }
        return isValid;
    };

    const prepareFormDataToSend = (): { [key: string]: any } => {
        let form: { [key: string]: any } = {};

        form['accountKey'] = accountKey;
        Object.assign(form['propertyInformation'] = {}, propertyInformationFormData);
        Object.keys(form['propertyInformation']).forEach(key => {
            if (isNaN(parseInt(form['propertyInformation'][key])) === false && (key !== PropertyInformationFieldName.ObjectBecomesEmptyFrom && key !== PropertyInformationFieldName.ObjectIsEmptySince)) {
                form['propertyInformation'][key] = parseInt(form['propertyInformation'][key]);
            } else if (form['propertyInformation'][key] === '') {
                form['propertyInformation'][key] = null;
            }
        });
        Object.assign(form['propertyLocation'] = {}, propertyLocationFormData);
        Object.keys(form['propertyLocation']).forEach(key => {
            if (form['propertyLocation'][key] === '') {
                form['propertyLocation'][key] = null;
            }
        });
        Object.assign(form['reporter'] = {}, reporterFormData);
        Object.keys(form['reporter']).forEach(key => {
            if (form['reporter'][key] === '') {
                form['reporter'][key] = null;
            } else if (key === ReporterFieldName.Salutation) {
                form['reporter'][key] = parseInt(form['reporter'][key]);
            }
        });

        return form;
    };

    const fetchConfiguration = async (): Promise<void> => {
        const response = await Axios.get(apiUrl + '/api/property-vacancy-reporter/configurations/' + accountKey);
        initializeApi(response);
    };

    const initializeApi = (response: AxiosResponse): void => {
        initializeStyles(response);
        initializeStep(response);
    };

    const initializeStyles = (response: AxiosResponse): void => {
        apiContainer.current.style.setProperty('--main-bg-color', response.data.configuration.firstColor);
        apiContainer.current.style.setProperty('--main-font-color', response.data.configuration.firstFontColor);
        apiContainer.current.style.setProperty('--second-bg-color', response.data.configuration.secondaryColor);
        apiContainer.current.style.setProperty('--second-font-color', response.data.configuration.secondaryFontColor);
        apiContainer.current.style.setProperty('--check-box-color', response.data.configuration.firstColor);
        apiContainer.current.style.setProperty('--input-color-focus', response.data.configuration.firstColor);
        setTheme(
            createTheme({
                palette: {
                    primary: {
                        main: response.data.configuration.firstColor
                    },
                    secondary: {
                        main: response.data.configuration.secondaryColor
                    }
                }
            })
        );
    };

    const initializeStep = (response: AxiosResponse): void => {
        setPlaces(response.data.places);
        setPropertyUsageList(getPropertyUsageFromConfig(response.data.configuration.industryClassifications));
        setConfig(response.data.configuration);
    };

    const getPropertyUsageFromConfig = (industryClassifications: IndustryClassification[]): IndustryClassification[] => {
        return industryClassifications;
    };

    const fetchBuildingConditionsFromApi = (): BuildingCondition[] => {
        return [
            {id: 0, name: 'neuwertig'},
            {id: 1, name: 'saniert'},
            {id: 2, name: 'teilweise saniert'},
            {id: 3, name: 'modernisiert'},
            {id: 4, name: 'unrenoviert'},
            {id: 5, name: 'sanierungsbedürftig'},
            {id: 6, name: 'Abrissobjekt'},
            {id: 7, name: 'baufällig'},
            {id: 8, name: 'gepflegt'},
            {id: 9, name: 'entkernt'},
            {id: 10, name: 'Rohbau'},
            {id: 11, name: 'Neubau'},
        ];
    };

    const getHeadlineText = (): string => {
        switch (currentStepNumber) {
            case 1:
                return 'Objektinformationen';
            case 2:
                return 'Lage des Objekts';
            case 3:
                return 'Bilder des Objekts';
            case 4:
                return 'Persönliche Angaben';
            default:
                return '';
        }
    };

    const updateProgressBar = (): void => {
        let progress = progressBar.current;

        if (progress != null) {
            let value = ((currentStepNumber / 4) * 100);
            progress.style.width = value.toString() + '%';
        }
    };

    let headlineText: string = getHeadlineText();

    return (
        <ThemeProvider theme={theme}>
            <div ref={apiContainer}
                 className={[apiStyle['main-container'], bootstrapStyle['mb-5'], bootstrapStyle['container']].join(' ')}>
                <div className={bootstrapStyle['mb-5']}>
                    {currentStepNumber < 2 && currentStepNumber > 0 &&
                    <>
                        <p className={apiStyle.headline}>{config.title}</p>
                        <p className={apiStyle.content}>{config.text}</p>
                    </>
                    }
                </div>


                {headlineText != '' &&
                <div className={[bootstrapStyle['text-center'], bootstrapStyle['text-md-start'], bootstrapStyle['px-2'],
                                 bootstrapStyle['mb-4']].join(' ')}>
                    <div className={[apiStyle['headline-2'], bootstrapStyle['d-md-flex']].join(' ')}>
                        <span className={apiStyle['step-label']}>{currentStepNumber} <span style={{fontSize: '14px'}}>/ 4</span></span>
                        <span>{headlineText}</span>
                    </div>
                    <div className={[apiStyle['progress'], bootstrapStyle['progress']].join(' ')}>
                        <div ref={progressBar} className={[bootstrapStyle['progress-bar'],
                                                           bootstrapStyle['progress-bar-striped']].join(' ')}
                             role="progressbar">
                        </div>
                    </div>
                </div>
                }
                <div>
                    {showStep()}
                </div>


                <div className={[bootstrapStyle['container'], bootstrapStyle['p-2'],
                                 bootstrapStyle['mt-4']].join(' ')}>
                    <div className={bootstrapStyle['row']}>
                        <div className={[bootstrapStyle['col-md-6'], bootstrapStyle['mb-3'], bootstrapStyle['text-start']].join(' ')}>
                            {currentStepNumber > 0 &&
                            <small>
                                Die mit * gekennzeichneten Felder sind Pflichtangaben</small>
                            }
                        </div>
                        <div className={[bootstrapStyle['col-md-6'], bootstrapStyle['text-end'], bootstrapStyle['justify-content-end']].join(' ')}>
                            {currentStepNumber > 1 &&
                            currentStepNumber < 5 &&
                            onReportSend === false &&
                            <Button
                                className={[bootstrapStyle['col-6'], bootstrapStyle['col-md-4'], bootstrapStyle['col-lg-3'], bootstrapStyle['mx-md-3']].join(' ')}
                                variant="outlined" color="secondary" size="medium" onClick={gotoPreviousStep}>Zurück</Button>
                            }
                            {currentStepNumber > 0 &&
                            currentStepNumber < 4 &&
                            <Button
                                className={[bootstrapStyle['col-6'], bootstrapStyle['col-md-4'], bootstrapStyle['col-lg-3'], bootstrapStyle['mx-md-3']].join(' ')}
                                variant="contained" color="secondary" size="medium" onClick={gotoNextStep}>Weiter</Button>
                            }

                            {currentStepNumber === 4 &&
                             <>
                                 {onReportSend === false ?
                                 <Button className={[bootstrapStyle['col-6'], bootstrapStyle['col-md-4'], bootstrapStyle['col-lg-3'],
                                     bootstrapStyle['mx-md-3']].join(' ')} variant="contained" color="secondary" size="medium"
                                         onClick={sendVacancyReport}>Absenden</Button>
                                 :
                                 <Button className={[bootstrapStyle['col-6'], bootstrapStyle['col-md-4'], bootstrapStyle['col-lg-3'],
                                     bootstrapStyle['mx-md-3']].join(' ')} variant="contained" color="secondary" size="medium" onClick={sendVacancyReport}>
                                     <div className={bootstrapStyle['spinner-border']} role="status"><span className="sr-only"/></div>
                                 </Button>
                                }
                            </>
                            }
                        </div>
                    </div>
                </div>
            </div>
        </ThemeProvider>
    );
}
