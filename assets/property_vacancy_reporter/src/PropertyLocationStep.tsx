import bootstrapStyle from './Bootstrap.module.scss';
import AutocompleteSelectField from '../../components/src/Form/AutocompleteSelectField';
import InputField from '../../components/src/Form/InputField';
import TextareaField from '../../components/src/Form/TextareaField';
import { Place } from './InterfaceLibrary';
import { PropertyLocationField, PropertyLocationFieldName, PropertyLocationFormData } from './PropertyLocationField';
import Axios from 'axios';
import React, { useEffect, useState } from 'react';

interface PropertyLocationStepProps {
    requiredPropertyLocationFormFields: PropertyLocationField[];
    formRef: React.MutableRefObject<HTMLFormElement>;
    propertyLocationFormData: PropertyLocationFormData;
    setPropertyLocationFormData: Function;
    placeList: Place[];
    apiUrl: string;
    errors: { [key: string]: string };
}

export default function PropertyLocationStep({requiredPropertyLocationFormFields, formRef, propertyLocationFormData, setPropertyLocationFormData, errors, placeList, apiUrl}: PropertyLocationStepProps) {

    const [postalCodeList, setPostalCodeList] = useState<Place[]>([]);
    const [quarterList, setQuarterList] = useState<Place[]>([]);
    const [boroughList, setBoroughList] = useState<Place[]>(null);
    const [borough, setBorough] = useState<string>(null);
    const [quarter, setQuarter] = useState<string>(null);
    const [place, setPlace] = useState<string>(null);

    useEffect(() => {
        if (placeList.length <= 1) {
            setSelectedPlace(placeList[0].uuid);
        }
    }, []);

    const checkRequired = (propertyLocationField: PropertyLocationField): boolean => {
        return requiredPropertyLocationFormFields.includes(propertyLocationField);
    };

    const handleInputChange = (name: string, value: string | boolean) => {
        let formValue = value;
        if (name === 'place') {
            formValue = placeList.find((place) => place.placeName.includes(value as string))?.uuid;
            setPropertyLocationFormData({...propertyLocationFormData, [PropertyLocationFieldName.PlaceId]: formValue});
            setSelectedPlace(formValue);
            return;
        } else if (name === 'borough') {
            formValue = boroughList.find((borough) => borough.placeName.includes(value as string))?.uuid;
            setPropertyLocationFormData({...propertyLocationFormData, [PropertyLocationFieldName.BoroughId]: formValue});
            setBorough(value as string);
            return;
        } else if (name === 'quarter') {
            formValue = quarterList.find((quarter) => quarter.placeName.includes(value as string))?.uuid;
            setPropertyLocationFormData({...propertyLocationFormData, [PropertyLocationFieldName.QuarterId]: formValue});
            setQuarter(value as string);
            return;
        }
        setPropertyLocationFormData({...propertyLocationFormData, [name]: formValue});
    };

    const setSelectedPlace = (uuid: string): void => {
        setPropertyLocationFormData({...propertyLocationFormData, placeUuid: uuid});
        setPlace(placeList.find(place => place.uuid === uuid)?.placeName);
        fetchPostalCodeList(uuid);
        fetchAndCheckBoroughList(uuid);
    };

    const fetchPostalCodeList = async (placeUuid: string): Promise<void> => {
        const response = await Axios.get(apiUrl + '/api/location/places/' + placeUuid + '/postal-codes');
        setPostalCodeList(response.data);
    };

    const fetchQuarterList = async (placeUuid: string): Promise<void> => {
        const response = await Axios.get(apiUrl + '/api/location/places/' + placeUuid + '/city-quarters');
        setQuarterList(response.data);
    };

    const fetchAndCheckBoroughList = async (placeUuid: string): Promise<void> => {
        const response = await Axios.get(apiUrl + '/api/location/places/' + placeUuid + '/boroughs');
        if (response.data.length < 1) {
            fetchQuarterList(placeUuid);
        } else {
            buildBoroughList(response.data);
        }
    };

    const buildBoroughList = (list: Place[]): void => {
        let resultList: Place[] = [];
        list.map(async (place, index) => {
            const response = await Axios.get(apiUrl + '/api/location/places/' + place.uuid + '/city-quarters');
            resultList.push(place);
            response.data.forEach((item: Place) => resultList.push(item));
            if (index === list.length - 1) {
                setBoroughList(resultList);
            }
        });
    };

    return (
        <form ref={formRef} className={bootstrapStyle['row']}>
            <div className={[bootstrapStyle['col-12'], bootstrapStyle['col-md-5'], bootstrapStyle['mb-4']].join(' ')}>
                {placeList.length > 1 ?
                    <AutocompleteSelectField
                        label={'Ort'}
                        value={place}
                        name={'place'}
                        error={errors[PropertyLocationFieldName.PlaceId]}
                        isRequired={checkRequired(PropertyLocationField.PlaceId)}
                        options={placeList.map(place => ({label: place.placeName, value: place.uuid}))}
                        handleInputChange={handleInputChange}
                        popoverText={'Sie müssen einen Ort auswählen, um die dazugehörigen Postleitzahlen und Stadtteile zu erhalten'}
                    />
                :
                    <InputField
                        label={'Ort'} value={placeList[0].placeName}
                        name={PropertyLocationFieldName.PlaceId}
                        isRequired={true}
                        type="text"
                        handleInputChange={handleInputChange}
                        readonly={true}
                    />
                }
            </div>
            <div className={[bootstrapStyle['col-12'], bootstrapStyle['col-md-3'], bootstrapStyle['mb-4']].join(' ')}>
                <AutocompleteSelectField
                    name={PropertyLocationFieldName.PostalCode} label={'PLZ'}
                    value={propertyLocationFormData.postalCode}
                    isRequired={checkRequired(PropertyLocationField.PostalCode)}
                    error={errors[PropertyLocationFieldName.PostalCode]}
                    handleInputChange={handleInputChange}
                    options={postalCodeList.map((postalCode) => ({label: postalCode.placeName, value: postalCode.uuid}))}
                />
            </div>
            <div className={[bootstrapStyle['col-12'], bootstrapStyle['col-md-4'], bootstrapStyle['mb-4']].join(' ')}>
                {boroughList !== null ?
                    <AutocompleteSelectField
                        name="borough" label={'Stadtteil'} value={borough}
                        isRequired={checkRequired(PropertyLocationField.BoroughId)}
                        error={errors[PropertyLocationFieldName.BoroughId]}
                        handleInputChange={handleInputChange}
                        options={boroughList.map((borough) => ({label: borough.placeName, value: borough.uuid}))}
                    />
                :
                    <AutocompleteSelectField
                        name="quarter" label={'Stadtteil'} value={quarter}
                        isRequired={checkRequired(PropertyLocationField.QuarterId)}
                        error={errors[PropertyLocationFieldName.QuarterId]}
                        handleInputChange={handleInputChange}
                        options={quarterList.map((quarter) => ({label: quarter.placeName, value: quarter.uuid}))}
                     />
                }
            </div>
            <div className={[bootstrapStyle['col-12'], bootstrapStyle['col-md-8'], bootstrapStyle['mb-4']].join(' ')}>
                <InputField
                    label={'Straße'}
                    type={'text'}
                    value={propertyLocationFormData.streetName}
                    name={PropertyLocationFieldName.StreetName}
                    error={errors[PropertyLocationFieldName.StreetName]}
                    isRequired={checkRequired(PropertyLocationField.StreetName)}
                    handleInputChange={handleInputChange}
                />
            </div>
            <div className={[bootstrapStyle['col-12'], bootstrapStyle['col-md-4'], bootstrapStyle['mb-4']].join(' ')}>
                <InputField
                    label={'Hausnummer'}
                    type={'text'}
                    value={propertyLocationFormData.houseNumber}
                    name={PropertyLocationFieldName.HouseNumber}
                    error={errors[PropertyLocationFieldName.HouseNumber]}
                    isRequired={checkRequired(PropertyLocationField.HouseNumber)}
                    handleInputChange={handleInputChange}
                />
            </div>
            <div className={[bootstrapStyle['col-12'], bootstrapStyle['mb-4']].join(' ')}>
                <TextareaField
                    label={'Lagebeschreibung'}
                    value={propertyLocationFormData.placeDescription}
                    error={errors[PropertyLocationFieldName.PlaceDescription]}
                    name={PropertyLocationFieldName.PlaceDescription}
                    isRequired={checkRequired(PropertyLocationField.PlaceDescription)}
                    handleInputChange={handleInputChange}
                />
            </div>
        </form>
    );
}
