interface BuildingCondition {

    id: number;
    name: string;

}

interface IndustryClassification {

    id: number;
    name: string;
    levelOne: number;

}

interface FormData{

    name: string;
    value: string | boolean | number;
    isValid: boolean;

}

interface Place{

    uuid: string;
    placeName: string;
    placeShortName?: string;
    placeType: number;

}

interface ShowField {

    [key: string]: boolean;

}

export { BuildingCondition, IndustryClassification, FormData, Place, ShowField };