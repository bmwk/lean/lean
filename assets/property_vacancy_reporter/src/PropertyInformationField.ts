interface PropertyInformationFormData {
    industryClassificationId: string;
    buildingCondition: string;
    plotSize: string;
    areaSize: string;
    retailSpace: string;
    livingSpace: string;
    gastronomySpace: string;
    subsidiarySpace: string;
    openSpace: string;
    usableOpenSpace: string;
    propertyDescription: string;
    propertyUserCompanyName: string;
    objectIsEmpty: boolean;
    objectBecomesEmpty: boolean;
    objectIsEmptySince: string;
    objectBecomesEmptyFrom: string;
    storeWindowAvailable: boolean;
    storeWindowWidth: string;
    shopWidth: string;
    barrierFreeAccess: string;
    propertyOfferType: string;
    numberOfParkingLots: string;
    groundLevelSalesArea: boolean;
}

enum PropertyInformationField {

    IndustryClassification = 0,
    BuildingCondition = 1,
    PlotSize = 2,
    AreaSize = 3,
    RetailSpace = 4,
    LivingSpace = 5,
    GastroSpace = 6,
    PropertyDescription = 7,
    SubsidiarySpace = 8,
    OpenSpace = 9,
    UsableOpenSpace = 10,
    StoreWindowWidth = 11,
    ShopWidth = 12,
    NumberOfParkingLots = 13,
    PropertyOfferType = 14,
    PropertyUserCompanyName = 15,
    ObjectIsEmpty = 16,
    ObjectBecomesEmpty = 17,
    ObjectIsEmptySince = 18,
    ObjectBecomesEmptyFrom = 19,
    StoreWindowAvailable = 20,
}

enum PropertyInformationFieldName {

    IndustryClassification = 'industryClassificationId',
    BuildingCondition = 'buildingCondition',
    PlotSize = 'plotSize',
    AreaSize = 'areaSize',
    RetailSpace = 'retailSpace',
    GroundLevelSalesArea = 'groundLevelSalesArea',
    LivingSpace = 'livingSpace',
    GastroSpace = 'gastronomySpace',
    SubsidiarySpace = 'subsidiarySpace',
    OpenSpace = 'openSpace',
    UsableOpenSpace = 'usableOpenSpace',
    PropertyDescription = 'propertyDescription',
    PropertyUserCompanyName = 'propertyUserCompanyName',
    ObjectIsEmpty = 'objectIsEmpty',
    ObjectBecomesEmpty = 'objectBecomesEmpty',
    ObjectIsEmptySince = 'objectIsEmptySince',
    ObjectBecomesEmptyFrom = 'objectBecomesEmptyFrom',
    StoreWindowAvailable = 'storeWindowAvailable',
    StoreWindowWidth = 'storeWindowWidth',
    ShopWidth = 'shopWidth',
    BarrierFreeAccess = 'barrierFreeAccess',
    NumberOfParkingLots = 'numberOfParkingLots',
    PropertyOfferType = 'propertyOfferType',

}

enum PropertyInformationFieldNameKey {

    'industryClassificationId' = PropertyInformationField.IndustryClassification,
    'buildingCondition' = PropertyInformationField.BuildingCondition,
    'plotSize' = PropertyInformationField.PlotSize,
    'areaSize' = PropertyInformationField.AreaSize,
    'retailSpace' = PropertyInformationField.RetailSpace,
    'livingSpace' = PropertyInformationField.LivingSpace,
    'gastronomySpace' = PropertyInformationField.GastroSpace,
    'propertyDescription' = PropertyInformationField.PropertyDescription,
    'propertyUserCompanyName' = PropertyInformationField.PropertyUserCompanyName,
    'objectIsEmpty' = PropertyInformationField.ObjectIsEmpty,
    'objectBecomesEmpty' = PropertyInformationField.ObjectBecomesEmpty,
    'objectIsEmptySince' = PropertyInformationField.ObjectIsEmptySince,
    'objectBecomesEmptyFrom' = PropertyInformationField.ObjectBecomesEmptyFrom,
    'storeWindowAvailable' = PropertyInformationField.StoreWindowAvailable,
    'subsidiarySpace' = PropertyInformationField.SubsidiarySpace,
    'openSpace' = PropertyInformationField.OpenSpace,
    'usableOpenSpace' = PropertyInformationField.UsableOpenSpace,
    'storeWindowWidth' = PropertyInformationField.StoreWindowWidth,
    'shopWidth' = PropertyInformationField.ShopWidth,
    'numberOfParkingLots' = PropertyInformationField.NumberOfParkingLots,
    'propertyOfferType' = PropertyInformationField.PropertyOfferType,


}

export { PropertyInformationFormData, PropertyInformationField, PropertyInformationFieldName, PropertyInformationFieldNameKey };
