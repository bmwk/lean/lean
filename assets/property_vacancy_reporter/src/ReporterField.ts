interface ReporterFormData {
    salutation: string;
    name: string;
    firstName: string;
    email: string;
    phoneNumber: string;
    companyName: string;
    privacyPolicyAccept: boolean;
    ownerConfirmation: boolean;
    alsoPropertyOwner: boolean;
}

enum ReporterField {
    Salutation = 0,
    Name = 1,
    FirstName = 2,
    Email = 3,
    PhoneNumber = 4,
    PrivacyPolicyAccept = 5,
    CompanyName = 6 ,
}

enum ReporterFieldName {
    Salutation = 'salutation',
    Name = 'name',
    FirstName = 'firstName',
    Email = 'email',
    PhoneNumber = 'phoneNumber',
    PrivacyPolicyAccept = 'privacyPolicyAccept',
    CompanyName = 'companyName',
}

enum ReporterFieldNameKey {
    'salutation' = 0,
    'name' = 1,
    'firstName' = 2,
    'email' = 3,
    'phoneNumber' = 4,
    'privacyPolicyAccept' = 5,
    'companyName'= 6 ,
}

export { ReporterField, ReporterFormData, ReporterFieldName, ReporterFieldNameKey };
