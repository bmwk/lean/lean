import apiStyle from './Api.module.css';
import bootstrapStyle from './Bootstrap.module.scss';
import {Button} from '@mui/material';
import React from 'react';

interface ReportResponseStepProps {
    isResponseSuccessful: boolean;
    newVacancyReport: Function;
}

export default function ReportResponseStep({isResponseSuccessful, newVacancyReport}: ReportResponseStepProps) {

    const clickNewVacancyReport = (): void => {
        newVacancyReport();
    }
    return (
        <div className={bootstrapStyle['container']}>
            {isResponseSuccessful === true ?
                <>
                    <p className={apiStyle['headline-2']}>Ihre Meldung war <strong>erfolgreich</strong></p>
                    <p style={{textAlign: "justify"}}><strong>Vielen Dank</strong> für die Meldung des Objekts. <br/>
                        Sie erhalten in Kürze eine <strong>E-Mail mit
                        einem Bestätigungs-Link</strong> von uns.
                        Bitte klicken Sie auf diesen Link, <strong>um Ihre Meldung abzuschließen</strong>.
                        Ihre Angaben werden im Anschluss an den zuständigen Sachbearbeiter weitergeleitet und dieser wird sich
                        ggf. für Rückfragen mit Ihnen in Verbindung setzen.
                    </p>
                    <div className={[ bootstrapStyle['row'], bootstrapStyle['justify-content-end'] ].join(' ')}>
                        <Button className={[bootstrapStyle['col-6'], bootstrapStyle['col-md-4'], bootstrapStyle['col-lg-3'], bootstrapStyle['mx-md-3']].join(' ')}
                                variant="contained" color="secondary" size="medium" onClick={clickNewVacancyReport}>
                            Weiteren Leerstand melden
                        </Button>
                    </div>
                </>
            :
                <>
                    <p className={apiStyle['headline-2']}>Das Absenden der Meldung ist leider <strong>fehlgeschlagen</strong></p>
                    <div style={{textAlign: "justify"}}>
                        Leider konnte der Leerstand aufgrund eines technischen Problems nicht gemeldet werden.<br/>
                        Bitte versuchen Sie, die Meldung erneut einzureichen.
                        Sollte der Fehler weiterhin bestehen, so schreiben Sie uns bitte eine E-Mail. <br/>
                        Vielen Dank für Ihr Verständnis.
                    </div>
                    <div className={[ bootstrapStyle['row'], bootstrapStyle['justify-content-end'] ].join(' ')}>
                        <Button className={[bootstrapStyle['col-6'], bootstrapStyle['col-md-4'], bootstrapStyle['col-lg-3'], bootstrapStyle['mx-md-3']].join(' ')}
                                variant="contained" color="secondary" size="medium" onClick={clickNewVacancyReport}>
                            Erneut versuchen
                        </Button>
                    </div>
                </>
            }
        </div>
    );
}
