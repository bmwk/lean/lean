import Dropzone, { DropzoneFile } from 'dropzone';
import React, { useEffect } from 'react';
import dropzoneStyle from './Dropzone.module.scss';

interface PropertyImagesStepProps {
    dropzone: Dropzone;
    setDropzone: Function;
}

export default function PropertyImagesStep({dropzone, setDropzone}: PropertyImagesStepProps) {
    const dropzoneElement = React.useRef<HTMLDivElement>();

    useEffect(() => {
        if (dropzone === null) {
            setDropzone(initializeDropzone());
        }
    }, []);

    const initializeDropzone = (): Dropzone => {
        if (Dropzone.instances.length > 0) {
            return Dropzone.instances[0]
        }
        return new Dropzone(dropzoneElement.current, {
            url: '/',
            paramName: 'imageFile',
            dictDefaultMessage: '',
            dictRemoveFile: 'x',
            autoProcessQueue: false,
            parallelUploads: 100,
            addRemoveLinks: true,
            clickable: true,
            acceptedFiles: 'image/*',
            createImageThumbnails: false,
            method: 'post',
            accept: (file: DropzoneFile, done): void => {
                if (dropzone !== null) {
                    dropzone.getQueuedFiles().forEach((acceptedFile: File): void => {
                        if (acceptedFile.name === file.name) {
                            dropzone.removeFile(file);
                        }
                    });
                }

                done();
            },
            init: (): void => {
                if (dropzone !== null) {
                    dropzone.getQueuedFiles().forEach((file: DropzoneFile): void => {
                        dropzone.emit('addedfile', file);
                        dropzone.files.push(file);
                    });
                }
            }
        });
    }

    return (
        <>
            <p className="text-center">Optional können Sie Bilder des Objektes hochladen und zusammen mit Ihrer Meldung absenden.</p>
            <div ref={dropzoneElement}
                 className={dropzoneStyle['dropzone-container']}>
                <div className={[dropzoneStyle['dz-default'], dropzoneStyle['dz-message']].join(' ') }>Bilder zum Hochladen hierher ziehen.</div>
            </div>
        </>
    )
}