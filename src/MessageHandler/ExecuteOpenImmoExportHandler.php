<?php

declare(strict_types=1);

namespace App\MessageHandler;

use App\Domain\OpenImmoExporter\OpenImmoExportService;
use App\Message\ExecuteOpenImmoExport;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
class ExecuteOpenImmoExportHandler
{
    public function __construct(
        private readonly OpenImmoExportService $openImmoExporterService,
    ) {
    }

    public function __invoke(ExecuteOpenImmoExport $executeOpenImmoExport): void
    {
        $openImmoExport = $this->openImmoExporterService->fetchOpenImmoExportById(id: $executeOpenImmoExport->getOpenImmoExportId());

        $this->openImmoExporterService->executeOpenImmoExport(openImmoExport: $openImmoExport);
    }
}
