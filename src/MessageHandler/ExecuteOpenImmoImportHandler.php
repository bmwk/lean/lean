<?php

declare(strict_types=1);

namespace App\MessageHandler;

use App\Domain\OpenImmoImporter\Exception\ImportForAccountIsAlreadyRunningException;
use App\Domain\OpenImmoImporter\OpenImmoImportService;
use App\Message\ExecuteOpenImmoImport;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\DelayStamp;

#[AsMessageHandler]
class ExecuteOpenImmoImportHandler
{
    public function __construct(
        private readonly OpenImmoImportService $openImmoImportService,
        private readonly MessageBusInterface $messageBus
    ) {
    }

    public function __invoke(ExecuteOpenImmoImport $executeOpenImmoImport): void
    {
        $openImmoImport = $this->openImmoImportService->fetchOpenImmoImportById(id: $executeOpenImmoImport->getOpenImmoImportId());

        try {
            $this->openImmoImportService->executeOpenImmoImport(openImmoImport: $openImmoImport);
        } catch (ImportForAccountIsAlreadyRunningException $importForAccountIsAlreadyRunningException) {
            $this->messageBus->dispatch($executeOpenImmoImport, [new DelayStamp(delay: 3000)]);
        }
    }
}
