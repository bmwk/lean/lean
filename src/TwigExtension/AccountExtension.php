<?php

declare(strict_types=1);

namespace App\TwigExtension;

use App\Domain\Account\AccountService;
use App\Domain\Entity\Account;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class AccountExtension extends AbstractExtension
{
    public function __construct(
        private readonly AccountService $accountService
    ) {
    }

    /**
     * @return TwigFunction[]
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('get_operator_account', [$this, 'getOperatorAccount']),
            new TwigFunction('base64_decode', [$this, 'base64Decode']),
        ];
    }

    public function getOperatorAccount(): ?Account
    {
        return $this->accountService->getOperatorAccount();
    }

    public function base64Decode(string $value): string
    {
        return base64_decode($value);
    }
}
