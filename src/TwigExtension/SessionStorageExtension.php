<?php

declare(strict_types=1);

namespace App\TwigExtension;

use App\SessionStorage\SessionStorageService;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class SessionStorageExtension extends AbstractExtension
{
    public function __construct(
        private readonly SessionStorageService $sessionStorageService
    ) {
    }

    /**
     * @return TwigFunction[]
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('fetch_object_from_session', [$this, 'fetchObjectFromSession']),
        ];
    }

    public function fetchObjectFromSession(string $dataClassName, string $sessionKeyName): ?object
    {
        return $this->sessionStorageService->fetchObject(dataClassName: $dataClassName, sessionKeyName: $sessionKeyName);
    }
}
