<?php

namespace App\TwigExtension;

use App\Domain\InterfaceConfiguration\InterfaceConfigurationService;
use Symfony\Bundle\SecurityBundle\Security;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class InterfaceConfigurationExtension extends AbstractExtension
{
    public function __construct(
        private readonly InterfaceConfigurationService $interfaceConfigurationService,
        private readonly Security $security
    ) {
    }

    /**
     * @return TwigFunction[]
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('has_hall_of_inspiration_configuration', [$this, 'hasHallOfInspirationConfiguration']),
            new TwigFunction('has_hystreet_configuration', [$this, 'hasHystreetConfiguration']),
            new TwigFunction('has_zielbildcheck_configuration', [$this, 'hasZielbildcheckConfiguration']),
            new TwigFunction('has_wegweiser_kommune_place_mapping', [$this, 'hasWegweiserKommunePlaceMapping']),
        ];
    }

    public function hasHallOfInspirationConfiguration(): bool
    {
        $user = $this->security->getUser();
        $account = $user->getAccount();

        return $this->interfaceConfigurationService->hasHallOfInspirationConfiguration(account: $account);
    }

    public function hasHystreetConfiguration(): bool
    {
        $user = $this->security->getUser();
        $account = $user->getAccount();

        return $this->interfaceConfigurationService->hasHystreetConfiguration(account: $account);
    }

    public function hasZielbildcheckConfiguration(): bool
    {
        $user = $this->security->getUser();
        $account = $user->getAccount();

        return $this->interfaceConfigurationService->hasZielbildcheckConfiguration(account: $account);
    }

    public function hasWegweiserKommunePlaceMapping(): bool
    {
        $user = $this->security->getUser();
        $account = $user->getAccount();

        return $this->interfaceConfigurationService->hasWegweiserKommunePlaceMapping(account: $account, place: $account->getAssignedPlace());
    }
}
