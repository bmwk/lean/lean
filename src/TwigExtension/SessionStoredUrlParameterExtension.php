<?php

declare(strict_types=1);

namespace App\TwigExtension;

use App\Domain\Entity\PaginationParameter;
use App\Domain\Entity\SortingOption\SortingOption;
use App\Domain\SessionStoredUrlParameter\SessionStoredUrlParameterService;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class SessionStoredUrlParameterExtension extends AbstractExtension
{
    public function __construct(
        private readonly SessionStoredUrlParameterService $sessionStoredUrlParameterService
    ) {
    }

    /**
     * @return TwigFunction[]
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction(name: 'fetch_merged_url_parameters_by_session_key_name', callable: [$this, 'fetchMergedUrlParametersBySessionKeyName']),
        ];
    }

    public function fetchMergedUrlParametersBySessionKeyName(string $sessionKeyName): array
    {
        $urlParameterObjectClasses = [
            PaginationParameter::class,
            SortingOption::class,
        ];

        return $this->sessionStoredUrlParameterService->fetchMergedUrlParameters(
            urlParameterObjectClasses: $urlParameterObjectClasses,
            sessionKeyName:  $sessionKeyName
        );
    }
}
