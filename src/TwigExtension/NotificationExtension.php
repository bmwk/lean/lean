<?php

declare(strict_types=1);

namespace App\TwigExtension;

use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\Notification\Notification;
use App\Domain\Entity\Notification\NotificationFilter;
use App\Domain\Notification\NotificationService;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class NotificationExtension extends AbstractExtension
{
    public function __construct(
        private readonly NotificationService $notificationService
    ) {
    }

    /**
     * @return TwigFunction[]
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction(name: 'fetch_unread_notifications', callable: [$this, 'fetchUnreadNotifications']),
            new TwigFunction(name: 'count_unread_notifications', callable: [$this, 'countUnreadNotifications']),
        ];
    }

    /**
     * @return Notification[]
     */
    public function fetchUnreadNotifications(AccountUser $accountUser, ?int $limit = null):array
    {
        $notificationFilter = new NotificationFilter();

        $notificationFilter->setReadStatus([false]);

        return $this->notificationService->fetchNotificationsByAccountUser(
            accountUser: $accountUser,
            notificationFilter: $notificationFilter,
            limit: $limit
        );
    }

    public function countUnreadNotifications(AccountUser $accountUser): int
    {
        $notificationFilter = new NotificationFilter();

        $notificationFilter->setReadStatus([false]);

        return $this->notificationService->countNotificationsByAccountUser(accountUser: $accountUser, notificationFilter: $notificationFilter);
    }
}
