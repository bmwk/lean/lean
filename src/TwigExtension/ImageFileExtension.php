<?php

declare(strict_types=1);

namespace App\TwigExtension;

use App\Domain\Entity\File;
use App\Domain\File\FileService;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class ImageFileExtension extends AbstractExtension
{
    public function __construct(
        private readonly FileService $fileService
    ) {
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('base64_image_encode', [$this, 'base64ImageEncode']),
            new TwigFunction('base64_image_from_url_encode', [$this, 'base64ImageFromUrlEncode']),
            new TwigFunction('check_image_file_exists', [$this, 'checkImageFileExists']),
        ];
    }

    public function base64ImageEncode(File $file): string
    {
        if ($this->checkImageFileExists($file)) {
            return 'data:' . $file->getMimeType() . ';base64,' . base64_encode($this->fileService->fetchFileContentFromFile($file));
        } else {
            return '';
        }
    }

    public function base64ImageFromUrlEncode(string $file): string
    {
        $image = file_get_contents($file);
        if ($image !== false) {
            return 'data:image/svg+xml;base64,' . base64_encode($image);
        } else {
            return '';
        }
    }

    public function checkImageFileExists(File $file): bool
    {
        return $this->fileService->checkFileExists(file: $file);
    }
}
