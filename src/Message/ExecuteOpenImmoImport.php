<?php

declare(strict_types=1);

namespace App\Message;

use App\Domain\Entity\OpenImmoImporter\OpenImmoImport;

class ExecuteOpenImmoImport
{
    private readonly int $openImmoImportId;

    public function __construct(OpenImmoImport $openImmoImport)
    {
        $this->openImmoImportId = $openImmoImport->getId();
    }

    public function getOpenImmoImportId(): int
    {
        return $this->openImmoImportId;
    }
}
