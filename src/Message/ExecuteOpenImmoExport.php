<?php

declare(strict_types=1);

namespace App\Message;

use App\Domain\Entity\OpenImmoExporter\OpenImmoExport;

class ExecuteOpenImmoExport
{
    private int $openImmoExportId;

    public function __construct(OpenImmoExport $openImmoExport)
    {
        $this->openImmoExportId = $openImmoExport->getId();
    }

    public function getOpenImmoExportId(): int
    {
        return $this->openImmoExportId;
    }
}
