<?php

declare(strict_types=1);

namespace App\HallOfInspiration;

use App\HallOfInspiration\Exception\HallOfInspirationException;
use App\HallOfInspiration\Response\HallOfInspirationConcept;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class HallOfInspirationService
{
    public function __construct(
        private readonly string $hallOfInspirationApiUrl,
        private readonly HttpClientInterface $httpClient
    ) {
    }

    /**
     * @return HallOfInspirationConcept[]
     */
    public function getConcepts(string $apiAccessToken): array
    {
        $conceptsResponse = $this->doGetRequest(apiAccessToken: $apiAccessToken, url: $this->hallOfInspirationApiUrl . '/concepts');

        $hallOfInspirationConcepts = [];

        foreach (json_decode($conceptsResponse->getContent()) as $hallOfInspirationConcept) {
            $hallOfInspirationConcepts[] = HallOfInspirationConcept::createFromJsonObject($hallOfInspirationConcept);
        }

        return $hallOfInspirationConcepts;
    }

    public function getConcept(string $apiAccessToken, int $id): ?HallOfInspirationConcept
    {
        try {
            $conceptResponse = $this->doGetRequest(apiAccessToken: $apiAccessToken, url: $this->hallOfInspirationApiUrl . '/concepts/' . $id);

            return HallOfInspirationConcept::createFromJsonObject(json_decode($conceptResponse->getContent()));
        } catch (HallOfInspirationException $hallOfInspirationException) {
            return null;
        }
    }

    public function getConceptRandomized(string $apiAccessToken): ?HallOfInspirationConcept
    {
        try {
            $conceptResponse = $this->doGetRequest(apiAccessToken: $apiAccessToken, url: $this->hallOfInspirationApiUrl . '/concepts/randomized');

            return HallOfInspirationConcept::createFromJsonObject(jsonObject: json_decode($conceptResponse->getContent()));
        } catch (HallOfInspirationException $hallOfInspirationException) {
            return null;
        }
    }

    private function doGetRequest(string $apiAccessToken, string $url): ResponseInterface
    {
        try {
            $response = $this->httpClient->request(
                method: 'GET',
                url: $url,
                options: [
                    'headers' => [
                        'Authorization' => 'Bearer ' . $apiAccessToken,
                        'accept'        => 'application/json',
                    ],
                ]
            );

            if ($response->getStatusCode() !== 200) {
                throw new HallOfInspirationException('Http status code is ' . $response->getStatusCode() . ' for ' . $url);
            }
        } catch (TransportExceptionInterface $transportException) {
            throw new HallOfInspirationException('Transport exception ' . $transportException->getMessage());
        }

        return $response;
    }
}
