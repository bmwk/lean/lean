<?php

declare(strict_types=1);

namespace App\HallOfInspiration\Response;

use App\Domain\Entity\HallOfInspiration\CompanyLocation;

class HallOfInspirationConcept
{
    private int $id;

    private string $brandName;

    private array $hallOfInspirationConceptBranches;

    private array $companyValues;

    private ?int $numberOfCompanyLocations = null;

    private ?array $companyLocations = null;

    private string $conceptDescription;

    private ?string $usp = null;

    private ?string $experience = null;

    private ?string $successStory = null;

    private ?string $expansionPlans = null;

    private ?bool $flagShipstore = null;

    private HallOfInspirationConceptAddress $hallOfInspirationConceptAddress;

    private ?string $instagram = null;

    private ?string $linkedIn = null;

    private ?string $website = null;

    private ?array $videos = null;

    private ?HallOfInspirationConceptImage $mainImage = null;

    private ?array $images = null;

    private \DateTimeImmutable $createdAt;

    private ?\DateTime $updatedAt = null;

    public static function createFromJsonObject(object $jsonObject): self
    {
        $hallOfInspirationConcept = new self();

        $hallOfInspirationConcept->id = $jsonObject->id;
        $hallOfInspirationConcept->brandName = $jsonObject->brandName;

        foreach ($jsonObject->hallOfInspirationConceptBranches as $branch) {
            $hallOfInspirationConcept->hallOfInspirationConceptBranches[] = HallOfInspirationConceptBranch::createFromJsonObject($branch);
        }

        if (isset($jsonObject->companyValues)) {
            $hallOfInspirationConcept->companyValues = $jsonObject->companyValues;
        }

        $hallOfInspirationConcept->conceptDescription = $jsonObject->conceptDescription;
        $hallOfInspirationConcept->mainImage = HallOfInspirationConceptImage::createFromJsonObject($jsonObject->mainImage);
        $hallOfInspirationConcept->createdAt = new \DateTimeImmutable($jsonObject->createdAt);
        $hallOfInspirationConcept->updatedAt = new \DateTime($jsonObject->updatedAt);

        if (isset($jsonObject->numberOfCompanyLocations)) {
            $hallOfInspirationConcept->numberOfCompanyLocations = $jsonObject->numberOfCompanyLocations;
        }

        if (isset($jsonObject->companyLocations)) {
            foreach ($jsonObject->companyLocations as $companyLocation) {
                $hallOfInspirationConcept->companyLocations[] = CompanyLocation::from($companyLocation);
            }
        }

        if (isset($jsonObject->usp)) {
            $hallOfInspirationConcept->usp = $jsonObject->usp;
        }

        if (isset($jsonObject->experience)) {
            $hallOfInspirationConcept->experience = $jsonObject->experience;
        }

        if (isset($jsonObject->successStory)) {
            $hallOfInspirationConcept->successStory = $jsonObject->successStory;
        }

        if (isset($jsonObject->expansionPlans)) {
            $hallOfInspirationConcept->expansionPlans = $jsonObject->expansionPlans;
        }

        if (isset($jsonObject->flagShipstore)) {
            $hallOfInspirationConcept->flagShipstore = $jsonObject->flagShipstore;
        }

        if (isset($jsonObject->hallOfInspirationConceptAddress)) {
            $hallOfInspirationConcept->hallOfInspirationConceptAddress = HallOfInspirationConceptAddress::createFromJsonObject($jsonObject->hallOfInspirationConceptAddress);
        }

        if (isset($jsonObject->website)) {
            $hallOfInspirationConcept->website = $jsonObject->website;
        }

        if (isset($jsonObject->instagram)) {
            $hallOfInspirationConcept->instagram = $jsonObject->instagram;
        }

        if (isset($jsonObject->linkedIn)) {
            $hallOfInspirationConcept->linkedIn = $jsonObject->linkedIn;
        }

        if (isset($jsonObject->mainImage)) {
            $hallOfInspirationConcept->mainImage = HallOfInspirationConceptImage::createFromJsonObject($jsonObject->mainImage);
        }

        if (isset($jsonObject->images)) {
            foreach ($jsonObject->images as $image) {
                $hallOfInspirationConcept->images[] = HallOfInspirationConceptImage::createFromJsonObject($image);
            }
        }

        if (isset($jsonObject->videos)) {
            $hallOfInspirationConcept->videos = $jsonObject->videos;
        }

        return $hallOfInspirationConcept;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getBrandName(): string
    {
        return $this->brandName;
    }

    public function setBrandName(string $brandName): self
    {
        $this->brandName = $brandName;

        return $this;
    }

    public function getHallOfInspirationConceptBranches(): array
    {
        return $this->hallOfInspirationConceptBranches;
    }

    public function setHallOfInspirationConceptBranches(array $hallOfInspirationConceptBranches): self
    {
        $this->hallOfInspirationConceptBranches = $hallOfInspirationConceptBranches;

        return $this;
    }

    public function getCompanyValues(): array
    {
        return $this->companyValues;
    }

    public function setCompanyValues(array $companyValues): self
    {
        $this->companyValues = $companyValues;

        return $this;
    }

    public function getNumberOfCompanyLocations(): ?int
    {
        return $this->numberOfCompanyLocations;
    }

    public function setNumberOfCompanyLocations(?int $numberOfCompanyLocations): self
    {
        $this->numberOfCompanyLocations = $numberOfCompanyLocations;

        return $this;
    }

    public function getCompanyLocations(): ?array
    {
        return $this->companyLocations;
    }

    public function setCompanyLocations(?array $companyLocations): self
    {
        $this->companyLocations = $companyLocations;

        return $this;
    }

    public function getConceptDescription(): string
    {
        return $this->conceptDescription;
    }

    public function setConceptDescription(string $conceptDescription): self
    {
        $this->conceptDescription = $conceptDescription;

        return $this;
    }

    public function getUsp(): ?string
    {
        return $this->usp;
    }

    public function setUsp(?string $usp): self
    {
        $this->usp = $usp;

        return $this;
    }

    public function getExperience(): ?string
    {
        return $this->experience;
    }

    public function setExperience(?string $experience): self
    {
        $this->experience = $experience;

        return $this;
    }

    public function getSuccessStory(): ?string
    {
        return $this->successStory;
    }

    public function setSuccessStory(?string $successStory): self
    {
        $this->successStory = $successStory;

        return $this;
    }

    public function getExpansionPlans(): ?string
    {
        return $this->expansionPlans;
    }

    public function setExpansionPlans(?string $expansionPlans): self
    {
        $this->expansionPlans = $expansionPlans;

        return $this;
    }

    public function getFlagShipstore(): ?bool
    {
        return $this->flagShipstore;
    }

    public function setFlagShipstore(?bool $flagShipstore): self
    {
        $this->flagShipstore = $flagShipstore;

        return $this;
    }


    public function getHallOfInspirationConceptAddress(): HallOfInspirationConceptAddress
    {
        return $this->hallOfInspirationConceptAddress;
    }

    public function setAddress(HallOfInspirationConceptAddress $hallOfInspirationConceptAddress): self
    {
        $this->hallOfInspirationConceptAddress = $hallOfInspirationConceptAddress;

        return $this;
    }

    public function getInstagram(): ?string
    {
        return $this->instagram;
    }

    public function setInstagram(?string $instagram): self
    {
        $this->instagram = $instagram;

        return $this;
    }

    public function getLinkedIn(): ?string
    {
        return $this->linkedIn;
    }

    public function setLinkedIn(?string $linkedIn): self
    {
        $this->linkedIn = $linkedIn;

        return $this;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    public function setWebsite(?string $website): self
    {
        $this->website = $website;

        return $this;
    }

    public function getVideos(): ?array
    {
        return $this->videos;
    }

    public function setVideos(?array $videos): self
    {
        $this->videos = $videos;

        return $this;
    }

    public function getMainImage(): ?HallOfInspirationConceptImage
    {
        return $this->mainImage;
    }

    public function setMainImage(?HallOfInspirationConceptImage $mainImage): self
    {
        $this->mainImage = $mainImage;

        return $this;
    }

    public function getImages(): ?array
    {
        return $this->images;
    }

    public function setImages(?array $images): self
    {
        $this->images = $images;

        return $this;
    }

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}
