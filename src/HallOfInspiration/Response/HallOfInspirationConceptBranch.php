<?php

declare(strict_types=1);

namespace App\HallOfInspiration\Response;

class HallOfInspirationConceptBranch
{
    private int $id;

    private string $name;

    public static function createFromJsonObject(object $jsonObject): self
    {
        $hallOfInspirationConceptBranch= new self();

        $hallOfInspirationConceptBranch->id = $jsonObject->id;
        $hallOfInspirationConceptBranch->name = $jsonObject->name;

        return $hallOfInspirationConceptBranch;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
}
