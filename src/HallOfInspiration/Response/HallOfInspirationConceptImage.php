<?php

declare(strict_types=1);

namespace App\HallOfInspiration\Response;

class HallOfInspirationConceptImage
{
    private ?string $title = null;

    private string $publicUrl;

    private ?string $publicThumbnailUrl = null;

    public static function createFromJsonObject(object $jsonObject): self
    {
        $hallOfInspirationConceptImage= new self();

        if (isset($jsonObject->title)) {
            $hallOfInspirationConceptImage->title = $jsonObject->title;
        }

        $hallOfInspirationConceptImage->publicUrl = $jsonObject->publicUrl;

        if (isset($jsonObject->publicThumbnailUrl)) {
            $hallOfInspirationConceptImage->publicThumbnailUrl = $jsonObject->publicThumbnailUrl;
        }

        return $hallOfInspirationConceptImage;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getPublicUrl(): string
    {
        return $this->publicUrl;
    }

    public function setPublicUrl(string $publicUrl): self
    {
        $this->publicUrl = $publicUrl;

        return $this;
    }

    public function getPublicThumbnailUrl(): ?string
    {
        return $this->publicThumbnailUrl;
    }

    public function setPublicThumbnailUrl(?string $publicThumbnailUrl): self
    {
        $this->publicThumbnailUrl = $publicThumbnailUrl;

        return $this;
    }
}
