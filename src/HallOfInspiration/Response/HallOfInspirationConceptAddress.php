<?php

declare(strict_types=1);

namespace App\HallOfInspiration\Response;

class HallOfInspirationConceptAddress
{
    private ?string $streetName = null;

    private ?string $houseNumber = null;

    private string $postalCode;

    private string $placeName;

    private ?string $federalState = null;

    public static function createFromJsonObject(object $jsonObject): self
    {
        $hallOfInspirationConceptAddress = new self();

        $hallOfInspirationConceptAddress->streetName = $jsonObject->streetName;
        $hallOfInspirationConceptAddress->houseNumber = $jsonObject->houseNumber;
        $hallOfInspirationConceptAddress->postalCode = $jsonObject->postalCode;
        $hallOfInspirationConceptAddress->placeName = $jsonObject->placeName;
        $hallOfInspirationConceptAddress->federalState = $jsonObject->federalState;

        return $hallOfInspirationConceptAddress;
    }

    public function getStreetName(): ?string
    {
        return $this->streetName;
    }

    public function setStreetName(?string $streetName): self
    {
        $this->streetName = $streetName;

        return $this;
    }

    public function getHouseNumber(): ?string
    {
        return $this->houseNumber;
    }

    public function setHouseNumber(?string $houseNumber): self
    {
        $this->houseNumber = $houseNumber;

        return $this;
    }

    public function getPostalCode(): string
    {
        return $this->postalCode;
    }

    public function setPostalCode(string $postalCode): self
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    public function getPlaceName(): string
    {
        return $this->placeName;
    }

    public function setPlaceName(string $placeName): self
    {
        $this->placeName = $placeName;

        return $this;
    }

    public function getFederalState(): ?string
    {
        return $this->federalState;
    }

    public function setFederalState(?string $federalState): self
    {
        $this->federalState = $federalState;

        return $this;
    }
}
