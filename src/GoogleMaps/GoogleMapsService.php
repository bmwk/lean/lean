<?php

declare(strict_types=1);

namespace App\GoogleMaps;

use App\GoogleMaps\Exception\GoogleMapsException;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class GoogleMapsService
{
    private bool $serviceAvailable;

    public function __construct(
        private readonly HttpClientInterface $httpClient,
        private readonly string $googleMapsApiKey,
    ) {
        if ($googleMapsApiKey && strlen($googleMapsApiKey) > 35) {
            $this->serviceAvailable = true;
        } else {
            $this->serviceAvailable = false;
        }
    }

    public static function fetchAddressComponentsFromApiResult(object $apiResult): ?array
    {
        foreach ($apiResult->results as $result) {
            if (count($result->types) === 1 && $result->types[0] === 'street_address') {
                return $result->address_components;
            }
        }

        return null;
    }

    public static function fetchStreetNameFromAddressComponents(array $addressComponents): ?string
    {
        $streetName = null;

        foreach ($addressComponents as $addressComponent) {
            if (count($addressComponent->types) === 1 && $addressComponent->types[0] === 'route') {
                $streetName = $addressComponent->long_name;
            }
        }

        return $streetName;
    }

    public static function fetchStreetNumberFromAddressComponents(array $addressComponents): ?string
    {
        $streetNumber = null;

        foreach ($addressComponents as $addressComponent) {
            if (count($addressComponent->types) === 1 && $addressComponent->types[0] === 'street_number') {
                $streetNumber = $addressComponent->long_name;
            }
        }

        return $streetNumber;
    }

    public static function fetchPostalCodeFromAddressComponents(array $addressComponents): ?string
    {
        $postalCode = null;

        foreach ($addressComponents as $addressComponent) {
            if (count($addressComponent->types) === 1 && $addressComponent->types[0] === 'postal_code') {
                $postalCode = $addressComponent->long_name;
            }
        }

        return $postalCode;
    }

    public function fetchGeoDataByAddress(
        string $streetName,
        string $houseNumber,
        string $postalCode,
        string $placeName
    ): object {
        $addressUrlString = $streetName .  '+' . $houseNumber . '+' . $postalCode . '+' . $placeName;

        $queryParameters = [
            'address=' . $addressUrlString,
            'key=' . $this->googleMapsApiKey,
        ];

        $url = 'https://maps.googleapis.com/maps/api/geocode/json?' . implode('&', $queryParameters);

        try {
            $response = $this->httpClient->request(method: 'GET', url: $url);

            if ($response->getStatusCode() !== 200) {
                throw new GoogleMapsException();
            }
        } catch (TransportExceptionInterface $transportException) {
            throw new GoogleMapsException();
        }

        return json_decode($response->getContent());
    }

    public function fetchAddressByLatitudeAndLongitude(float $latitude, float $longitude): object
    {
        $queryParameters = [
            'latlng=' . $latitude . ',' . $longitude,
            'key=' . $this->googleMapsApiKey,
        ];

        $url = 'https://maps.googleapis.com/maps/api/geocode/json?' . implode('&', $queryParameters);

        try {
            $response = $this->httpClient->request(method: 'GET', url: $url);

            if ($response->getStatusCode() !== 200) {
                throw new GoogleMapsException();
            }
        } catch (TransportExceptionInterface $transportException) {
            throw new GoogleMapsException();
        }

        return json_decode($response->getContent());
    }

    public function isServiceAvailable(): bool
    {
        return $this->serviceAvailable;
    }
}
