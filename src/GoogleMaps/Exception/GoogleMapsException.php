<?php

declare(strict_types=1);

namespace App\GoogleMaps\Exception;

class GoogleMapsException extends \Exception
{
}
