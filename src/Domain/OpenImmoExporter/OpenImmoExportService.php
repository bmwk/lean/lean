<?php

declare(strict_types=1);

namespace App\Domain\OpenImmoExporter;

use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\File;
use App\Domain\Entity\OpenImmoExporter\ExportActionType;
use App\Domain\Entity\OpenImmoExporter\ExportMode;
use App\Domain\Entity\OpenImmoExporter\ExportScope;
use App\Domain\Entity\OpenImmoExporter\ExportStatus;
use App\Domain\Entity\OpenImmoExporter\ExportType;
use App\Domain\Entity\OpenImmoExporter\OpenImmoExport;
use App\Domain\Entity\Property\BarrierFreeAccess;
use App\Domain\Entity\Property\Building;
use App\Domain\Entity\Property\BuildingUnit;
use App\Domain\Entity\Property\ExternalContactPerson;
use App\Domain\Entity\Property\HeatingMethod;
use App\Domain\Entity\Property\LiftType;
use App\Domain\Entity\Property\PropertyContactPerson;
use App\Domain\Entity\Property\PropertyPricingInformation;
use App\Domain\Entity\Property\RoofShape;
use App\Domain\Entity\PropertyOfferType;
use App\Domain\File\FileService;
use App\Domain\Filesystem\FilesystemService;
use App\Domain\OpenImmoExporter\Exception\ExportAlreadyInProgressException;
use App\Domain\OpenImmoExporter\Exception\ExportAlreadyProcessedException;
use App\Message\ExecuteOpenImmoExport;
use App\OpenImmo\Entity\Aktion;
use App\OpenImmo\Entity\Anbieter;
use App\OpenImmo\Entity\Anhaenge;
use App\OpenImmo\Entity\Anhang;
use App\OpenImmo\Entity\AnhangGruppe;
use App\OpenImmo\Entity\AnhangLocation;
use App\OpenImmo\Entity\AussenCourtage;
use App\OpenImmo\Entity\Ausstattung;
use App\OpenImmo\Entity\Befeuerung;
use App\OpenImmo\Entity\Dachform;
use App\OpenImmo\Entity\Daten;
use App\OpenImmo\Entity\Energiepass;
use App\OpenImmo\Entity\Flaechen;
use App\OpenImmo\Entity\Freitexte;
use App\OpenImmo\Entity\Geo;
use App\OpenImmo\Entity\Heizungsart;
use App\OpenImmo\Entity\Immobilie;
use App\OpenImmo\Entity\InnenCourtage;
use App\OpenImmo\Entity\Kaufpreisnetto;
use App\OpenImmo\Entity\Kontaktperson;
use App\OpenImmo\Entity\Objektkategorie;
use App\OpenImmo\Entity\OpenImmo;
use App\OpenImmo\Entity\Preise;
use App\OpenImmo\Entity\Uebertragung;
use App\OpenImmo\Entity\Vermarktungsart;
use App\OpenImmo\Entity\Versteigerung;
use App\OpenImmo\Entity\VerwaltungObjekt;
use App\OpenImmo\Entity\VerwaltungTechn;
use App\OpenImmo\Entity\Waehrung;
use App\OpenImmo\Entity\Zustand;
use App\OpenImmo\Entity\ZustandAngaben;
use App\Repository\OpenImmoExporter\OpenImmoExportRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Validator\Constraints\Collection;

class OpenImmoExportService
{
    public function __construct(
        private readonly string $openImmoExportTempPath,
        private readonly OpenImmoExportRepository $openImmoExporterRepository,
        private readonly FilesystemService $filesystemService,
        private readonly FileService $fileService,
        private readonly EntityManagerInterface $entityManager,
        private readonly MessageBusInterface $messageBus
    ) {
    }

    public function createAndSaveOpenImmoExport(
        BuildingUnit $buildingUnit,
        AccountUser $accountUser,
        ExportActionType $exportActionType,
        ExportMode $exportMode,
        ExportScope $exportScope,
        ExportType $exportType,
        string $portalProviderNumber
    ): OpenImmoExport {
        $openImmoExport = new OpenImmoExport();

        $openImmoExport
            ->setBuildingUnit($buildingUnit)
            ->setExportActionType($exportActionType)
            ->setExportMode($exportMode)
            ->setExportScope($exportScope)
            ->setExportType($exportType)
            ->setExportStatus(ExportStatus::CREATED)
            ->setAccount($accountUser->getAccount())
            ->setCreatedByAccountUser($accountUser)
            ->setPortalProviderNumber($portalProviderNumber);

        $this->entityManager->persist($openImmoExport);
        $this->entityManager->flush();

        return $openImmoExport;
    }

    public function dispatchExecuteOpenImmoExportMessage(OpenImmoExport $openImmoExport): void
    {
        $this->messageBus->dispatch(new ExecuteOpenImmoExport(openImmoExport: $openImmoExport));
    }

    public function executeOpenImmoExports(): void
    {
        $openImmoExports = $this->openImmoExporterRepository->findByExportStatus(exportStatus: ExportStatus::CREATED, limit: 5);

        foreach ($openImmoExports as $openImmoExport) {
            $this->executeOpenImmoExport(openImmoExport: $openImmoExport);
        }
    }

    public function executeOpenImmoExport(OpenImmoExport $openImmoExport): void
    {
        $exportStatus = $openImmoExport->getExportStatus();

        if ($exportStatus === ExportStatus::IN_PROGRESS) {
            throw new ExportAlreadyInProgressException();
        }

        if (in_array(needle: $exportStatus, haystack: [ExportStatus::DONE, ExportStatus::FAILED]) === true) {
            throw new ExportAlreadyProcessedException();
        }

        $openImmoExport->setExportStatus(ExportStatus::IN_PROGRESS);

        $this->entityManager->flush();

        $this->makeExportDirectory(openImmoExport: $openImmoExport);

        $anbieter = new Anbieter();
        $immobilie = new Immobilie();

        $anbieter
            ->addImmobilie($immobilie)
            ->setAnbieternr($openImmoExport->getPortalProviderNumber());

        $immobilie->setVerwaltungTechn(new VerwaltungTechn());

        $this->mapBuildingUnitToOpenImmoVerwaltungTechn(openImmoExport: $openImmoExport, immobilie: $immobilie);

        $immobilie->setFreitexte(new Freitexte());

        $this->mapBuildingUnitToOpenImmoFreitexte(
            buildingUnit: $openImmoExport->getBuildingUnit(),
            freitexte: $immobilie->getFreitexte()
        );

        $immobilie->setAusstattung(new Ausstattung());

        $this->mapBuildingUnitToOpenImmoAusstattung(
            buildingUnit: $openImmoExport->getBuildingUnit(),
            ausstattung: $immobilie->getAusstattung()
        );

        $immobilie->setGeo(new Geo());
        $this->mapBuildingUnitToOpenImmoGeo(buildingUnit: $openImmoExport->getBuildingUnit(), geo: $immobilie->getGeo());

        $immobilie->setVersteigerung(new Versteigerung());
        $this->mapBuildingUnitToOpenImmoVersteigerung(buildingUnit: $openImmoExport->getBuildingUnit(), versteigerung: $immobilie->getVersteigerung());

        $immobilie->setFlaechen(new Flaechen());
        $this->mapBuildingUnitToOpenImmoFlaechen(buildingUnit: $openImmoExport->getBuildingUnit(), flaechen: $immobilie->getFlaechen());

        if ($openImmoExport->getBuildingUnit()->getPropertyContactPersons()->count() !== 0) {
            $immobilie->setKontaktperson(new Kontaktperson());

            $this->mapBuildingUnitPropertyContactPersonToOpenImmoKontaktperson(
                propertyContactPerson: $openImmoExport->getBuildingUnit()->getPropertyContactPersons()[0],
                kontaktperson: $immobilie->getKontaktperson()
            );
        } elseif ($openImmoExport->getBuildingUnit()->getExternalContactPerson() !== null) {
            $immobilie->setKontaktperson(new Kontaktperson());

            $this->mapBuildingUnitExternalContactPersonToOpenImmoKontaktperson(
                externalContactPerson: $openImmoExport->getBuildingUnit()->getExternalContactPerson(),
                kontaktperson: $immobilie->getKontaktperson()
            );
        }

        if (
            $openImmoExport->getBuildingUnit()->getMainImage() !== null
            || $openImmoExport->getBuildingUnit()->getImages()->count() > 0
        ) {
            $immobilie->setAnhaenge(new Anhaenge());

            $this->mapBuildingUnitToOpenImmoAnhaenge(
                buildingUnit: $openImmoExport->getBuildingUnit(),
                anhaenge: $immobilie->getAnhaenge(),
                openImmoExport: $openImmoExport
            );
        }

        if ($openImmoExport->getBuildingUnit()->getDocuments()->count() > 0) {
            if ($immobilie->getAnhaenge() === null) {
                $immobilie->setAnhaenge(new Anhaenge());
            }

            $documents = $openImmoExport->getBuildingUnit()->getDocuments();

            $this->mapBuildingUnitDocumentsToOpenImmoAnhaenge(
                documents: $documents->toArray(),
                anhaenge: $immobilie->getAnhaenge(),
                openImmoExport: $openImmoExport
            );
        }

        $this->mapBuildingUnitToOpenImmoImmobilie($openImmoExport->getBuildingUnit(), $immobilie);

        $this->mapBuildingToOpenImmo($openImmoExport->getBuildingUnit()->getBuilding(), $immobilie);

        $openImmo = $this->createOpenImmo(openImmoExport: $openImmoExport, anbieter: $anbieter);
        $file = $this->createAndCompressOpenImmoXmlFile(openImmoExport: $openImmoExport, openImmo: $openImmo);

        $openImmoExport
            ->setFile($file)
            ->setExportStatus(ExportStatus::DONE);

        $this->removeTempExportDirectory(openImmoExport: $openImmoExport);

        $this->entityManager->flush();
    }

    public function fetchOpenImmoExportById(int $id): ?OpenImmoExport
    {
        return $this->openImmoExporterRepository->findOneBy(criteria: ['id' => $id]);
    }


    public function buildPathToOpenImmoExportDirectoryFromOpenImmoExport(OpenImmoExport $openImmoExport): \SplFileInfo
    {
        return new \SplFileInfo(filename: $this->openImmoExportTempPath . '/' . $openImmoExport->getAccount()->getId() . '/' . $openImmoExport->getId());
    }

    private function copyFileToOpenImmoExportDirectory(File $file, OpenImmoExport $openImmoExport): bool
    {
        if ($this->fileService->checkFileExists(file: $file) === false) {
            return false;
        }

        $originFile = $this->fileService->getRealPathFromFile(file: $file);

        $openImmoExportPath = $this->buildPathToOpenImmoExportDirectoryFromOpenImmoExport(openImmoExport: $openImmoExport);

        $this->filesystemService->copy(
            originFile: $originFile,
            targetFile: $openImmoExportPath->getPathname() . '/' . $file->getFileName()
        );

        return file_exists(filename: $openImmoExportPath->getPathname() . '/' . $file->getFileName());
    }

    private function makeExportDirectory(OpenImmoExport $openImmoExport): \SplFileInfo
    {
        $targetDirectory = $this->filesystemService->makeDirectory(
            directoryName: (string) $openImmoExport->getAccount()->getId(),
            targetDirectory: new \SplFileInfo($this->openImmoExportTempPath)
        );

        return $this->filesystemService->makeDirectory(directoryName: (string)$openImmoExport->getId(), targetDirectory: $targetDirectory);
    }


    private function removeTempExportDirectory(OpenImmoExport $openImmoExport): void
    {
        $tempExportDirectory = $this->buildPathToOpenImmoExportDirectoryFromOpenImmoExport(openImmoExport: $openImmoExport);

        if ($tempExportDirectory->isDir() === true) {
            $this->filesystemService->remove(files: $tempExportDirectory->getRealPath());
        }
    }

    private function createAndCompressOpenImmoXmlFile(OpenImmoExport $openImmoExport, OpenImmo $openImmo): File
    {
        $tempPath = $this->buildPathToOpenImmoExportDirectoryFromOpenImmoExport(openImmoExport: $openImmoExport);

        file_put_contents(
            filename: $tempPath->getPathname() . '/OpenImmoExport.xml',
            data: $openImmo->toXml()
        );

        $this->compressOpenImmoExport(openImmoExport: $openImmoExport);

        return $this->fileService->saveFileFromFileContent(
            fileContent: file_get_contents(filename: $tempPath->getPathname() . '/OpenImmoExport.zip'),
            filename: 'OpenImmoExport.zip',
            fileExtension: 'zip',
            account: $openImmoExport->getAccount(),
            public: false
        );
    }

    private function createAnhang(string $filename, AnhangGruppe $anhangGruppe, AnhangLocation $anhangLocation, ?string $title = null): Anhang
    {
        $anhang = new Anhang();
        $daten = new Daten();
        $daten->setPfad($filename);

        $anhang
            ->setAnhangtitel($title)
            ->setGruppe($anhangGruppe->value)
            ->setLocation($anhangLocation->value)
            ->setDaten($daten);

        return $anhang;
    }

    private function createOpenImmo(OpenImmoExport $openImmoExport, Anbieter $anbieter): OpenImmo
    {
        $openImmo = new OpenImmo();
        $uebertragung = new Uebertragung();
        $dateTime = new \DateTime();

        $uebertragung
            ->setArt($openImmoExport->getExportType()->getName())
            ->setUmfang($openImmoExport->getExportScope()->getName())
            ->setModus($openImmoExport->getExportMode()->getName())
            ->setVersion('1.2.7')
            ->setSendersoftware('LeAn')
            ->setTechnEmail($openImmoExport->getAccount()->getEmail())
            ->setTimestamp($dateTime->format('c'));

        $openImmo
            ->addAnbieter($anbieter)
            ->setUebertragung($uebertragung);

        return $openImmo;
    }

    private function mapBuildingUnitToOpenImmoFreitexte(BuildingUnit $buildingUnit, Freitexte $freitexte): void
    {
        $freitexte
            ->setObjekttitel($buildingUnit->getName())
            ->setLage($buildingUnit->getPlaceDescription())
            ->setObjektbeschreibung($buildingUnit->getDescription());
    }

    private function mapBuildingUnitToOpenImmoAusstattung(BuildingUnit $buildingUnit, Ausstattung $ausstattung): void
    {
        $ausstattung
            ->setBarrierefrei(
                (
                    $buildingUnit->getBarrierFreeAccess() === BarrierFreeAccess::IS_AVAILABLE
                    || $buildingUnit->getBarrierFreeAccess() === BarrierFreeAccess::CAN_BE_GUARANTEED
                )
            )
            ->setRollstuhlgerecht($buildingUnit->isWheelchairAccessible())
            ->setRampe($buildingUnit->isRampPresent());

        if( $buildingUnit->getHeatingMethod() !== null) {
            $ausstattung->setHeizungsart(new Heizungsart());

            if ($buildingUnit->getHeatingMethod() === HeatingMethod::DISTRICT_HEATING) {
                $ausstattung->getHeizungsart()->setFern(true);
            } elseif ($buildingUnit->getHeatingMethod() === HeatingMethod::FURNACE_HEATING) {
                $ausstattung->getHeizungsart()->setOfen(true);
            } elseif ($buildingUnit->getHeatingMethod() === HeatingMethod::FLOOR_HEATING) {
                $ausstattung->getHeizungsart()->setEtage(true);
            } elseif ($buildingUnit->getHeatingMethod() === HeatingMethod::CENTRAL_HEATING) {
                $ausstattung->getHeizungsart()->setZentral(true);
            } elseif ($buildingUnit->getHeatingMethod() === HeatingMethod::UNDERFLOOR_HEATING) {
                $ausstattung->getHeizungsart()->setFussboden(true);
            }
        }
    }

    private function mapBuildingUnitInFloorsToOpenImmoGeoEtage(array $inFloors, Geo $geo): void
    {
        if (count($inFloors) !== 1) {
            return;
        }

        if (preg_match(pattern: '/^\d+$/', subject: trim($inFloors[0])) === false) {
            return;
        }

        $geo->setEtage(intval($inFloors[0]));
    }

    private function mapBuildingUnitToOpenImmoGeo(BuildingUnit $buildingUnit, Geo $geo): void
    {
        if ($buildingUnit->getInFloors() !== null) {
            $this->mapBuildingUnitInFloorsToOpenImmoGeoEtage(inFloors: $buildingUnit->getInFloors(), geo: $geo);
        }

        if ($buildingUnit->getAddress() !== null) {
            $geo
                ->setPlz($buildingUnit->getAddress()->getPostalCode())
                ->setStrasse($buildingUnit->getAddress()->getStreetName())
                ->setHausnummer($buildingUnit->getAddress()->getHouseNumber())
                ->setOrt($buildingUnit->getAddress()->getPlace()->getPlaceName());
        }
    }

    private function mapBuildingUnitToOpenImmoFlaechen(BuildingUnit $buildingUnit, Flaechen $flaechen): void
    {
        if ($buildingUnit->getShopWindowFrontWidth() !== null) {
            $flaechen->setFensterfront($buildingUnit->getShopWindowFrontWidth());
        }

        if ($buildingUnit->getPropertySpacesData() === null) {
            return;
        }

        $flaechen
            ->setAnzahlZimmer($buildingUnit->getPropertySpacesData()->getRoomCount())
            ->setWohnflaeche($buildingUnit->getPropertySpacesData()->getLivingSpace())
            ->setNutzflaeche($buildingUnit->getPropertySpacesData()->getUsableSpace())
            ->setLagerflaeche($buildingUnit->getPropertySpacesData()->getStorageSpace())
            ->setLadenflaeche($buildingUnit->getPropertySpacesData()->getRetailSpace())
            ->setFreiflaeche($buildingUnit->getPropertySpacesData()->getOpenSpace())
            ->setBueroflaeche($buildingUnit->getPropertySpacesData()->getOfficeSpace())
            ->setBueroteilflaeche($buildingUnit->getPropertySpacesData()->getOfficePartSpace())
            ->setVerwaltungsflaeche($buildingUnit->getPropertySpacesData()->getAdministrationSpace())
            ->setGastroflaeche($buildingUnit->getPropertySpacesData()->getGastronomySpace())
            ->setGrundstuecksflaeche($buildingUnit->getPropertySpacesData()->getPlotSize())
            ->setBalkonTerrasseFlaeche($buildingUnit->getPropertySpacesData()->getBalconyTurfSpace())
            ->setGartenflaeche($buildingUnit->getPropertySpacesData()->getGardenSpace())
            ->setKellerflaeche($buildingUnit->getPropertySpacesData()->getCellarSpace())
            ->setDachbodenflaeche($buildingUnit->getPropertySpacesData()->getAtticSpace())
            ->setBeheizbareFlaeche($buildingUnit->getPropertySpacesData()->getHeatableSurface())
            ->setVermietbareFlaeche($buildingUnit->getPropertySpacesData()->getRentableArea());
    }

    private function mapBuildingUnitToOpenImmoVersteigerung(BuildingUnit $buildingUnit, Versteigerung $versteigerung): void
    {
        $versteigerung
            ->setAktenzeichen($buildingUnit->getFileNumberForeclosure())
            ->setZvtermin($buildingUnit->getAuctionDate())
            ->setAmtsgericht($buildingUnit->getResponsibleLocalCourt());
    }

    private function mapBuildingUnitPropertyContactPersonToOpenImmoKontaktperson(
        PropertyContactPerson $propertyContactPerson,
        Kontaktperson $kontaktperson
    ): void {
        $salutation = ($propertyContactPerson->getPerson()->getSalutation() !== null)
            ? $propertyContactPerson->getPerson()->getSalutation()->getName()
            : null;

        $title = ($propertyContactPerson->getPerson()->getPersonTitle() !== null)
            ? $propertyContactPerson->getPerson()->getPersonTitle()->getName()
            : null;

        $kontaktperson
            ->setAnrede($salutation)
            ->setTitel($title)
            ->setName($propertyContactPerson->getPerson()->getName())
            ->setVorname($propertyContactPerson->getPerson()->getFirstName())
            ->setStrasse($propertyContactPerson->getPerson()->getStreetName())
            ->setHausnummer($propertyContactPerson->getPerson()->getHouseNumber())
            ->setPlz($propertyContactPerson->getPerson()->getPostalCode())
            ->setOrt($propertyContactPerson->getPerson()->getPlaceName())
            ->setEmailDirekt($propertyContactPerson->getPerson()->getEmail())
            ->setTelHandy($propertyContactPerson->getPerson()->getMobilePhoneNumber())
            ->setTelDurchw($propertyContactPerson->getPerson()->getPhoneNumber())
            ->setTelFax($propertyContactPerson->getPerson()->getFaxNumber())
            ->setUrl($propertyContactPerson->getPerson()->getWebsite());
    }

    private function mapBuildingUnitExternalContactPersonToOpenImmoKontaktperson(
        ExternalContactPerson $externalContactPerson,
        Kontaktperson $kontaktperson
    ): void {
        $salutation = ($externalContactPerson->getSalutation() !== null)
            ? $externalContactPerson->getSalutation()->getName()
            : null;

        $kontaktperson
            ->setAnrede($salutation)
            ->setTitel($externalContactPerson->getPersonTitle())
            ->setName($externalContactPerson->getLastName())
            ->setVorname($externalContactPerson->getFirstName())
            ->setStrasse($externalContactPerson->getStreetName())
            ->setHausnummer($externalContactPerson->getHouseNumber())
            ->setPlz($externalContactPerson->getPostalCode())
            ->setOrt($externalContactPerson->getPlaceName())
            ->setEmailDirekt($externalContactPerson->getEmail())
            ->setTelHandy($externalContactPerson->getMobilePhoneNumber())
            ->setTelDurchw($externalContactPerson->getPhoneNumber())
            ->setTelFax($externalContactPerson->getFaxNumber())
            ->setUrl($externalContactPerson->getWebsite());
    }

    private function mapBuildingUnitToOpenImmoAnhaenge(BuildingUnit $buildingUnit, Anhaenge $anhaenge, OpenImmoExport $openImmoExport): void
    {
        $fileCopied = false;

        if ($buildingUnit->getMainImage() !== null) {
            $fileCopied = $this->copyFileToOpenImmoExportDirectory(
                file: $buildingUnit->getMainImage()->getFile(),
                openImmoExport: $openImmoExport
            );
        }

        if ($fileCopied) {
            $anhang = new Anhang();
            $daten = new Daten();
            $daten->setPfad($buildingUnit->getMainImage()->getFile()->getFileName());

            $anhang
                ->setAnhangtitel($buildingUnit->getMainImage()->getTitle())
                ->setGruppe(AnhangGruppe::MAIN_IMAGE->value)
                ->setLocation(AnhangLocation::EXTERN->value)
                ->setDaten($daten);

            $anhaenge->addAnhang($anhang);
        }

        foreach ($buildingUnit->getImages() as $image) {
            $fileCopied = $this->copyFileToOpenImmoExportDirectory(file: $image->getFile(), openImmoExport: $openImmoExport);

            if ($fileCopied === false) {
                continue;
            }

            $anhang = $this->createAnhang(
                filename: $image->getFile()->getFileName(),
                anhangGruppe: AnhangGruppe::IMAGE,
                anhangLocation: AnhangLocation::EXTERN,
                title: $image->getTitle()
            );

            $anhaenge->addAnhang($anhang);
        }
    }

    private function mapBuildingUnitDocumentsToOpenImmoAnhaenge(Collection|array $documents, Anhaenge $anhaenge, OpenImmoExport $openImmoExport): void
    {
        foreach ($documents as $document) {
            $fileCopied = $this->copyFileToOpenImmoExportDirectory(file: $document->getFile(), openImmoExport: $openImmoExport);

            if ($fileCopied === false) {
                continue;
            }

            $anhang = $this->createAnhang(
                filename: $document->getFile()->getFileName(),
                anhangGruppe: AnhangGruppe::DOCUMENT,
                anhangLocation: AnhangLocation::EXTERN,
                title: $document->getTitle()
            );

            $anhaenge->addAnhang($anhang);
        }
    }

    private function mapBuildingUnitEnergyCertificateToImmobileEnergiepass(BuildingUnit $buildingUnit, Immobilie $immobilie): void
    {
        $energiePass = new Energiepass();
        $zustandAngaben = ($immobilie->getZustandAngaben() !== null) ? $immobilie->getZustandAngaben() : new ZustandAngaben();
        $primaerenergietraeger = null;

        foreach ($buildingUnit->getEnergyCertificate()->getEnergyCertificateHeatingTypes() as $energyCertificateHeatingType) {
            $primaerenergietraeger .= (($primaerenergietraeger !== null) ? ', ' : '')
                . $energyCertificateHeatingType->getName();
        }

        $energiePass
            ->setAusstelldatum($buildingUnit->getEnergyCertificate()->getIssueDate())
            ->setGueltigBis($buildingUnit->getEnergyCertificate()->getExpirationDate()->format('d.m.Y'))
            ->setBaujahr(
                ($buildingUnit->getEnergyCertificate()->getBuildingConstructionYear() !== null)
                    ? strval($buildingUnit->getEnergyCertificate()->getBuildingConstructionYear())
                    : null
            )
            ->setPrimaerenergietraeger($primaerenergietraeger)
            ->setMitwarmwasser($buildingUnit->getEnergyCertificate()->isWithHotWater())
            ->setEnergieverbrauchkennwert(
                ($buildingUnit->getEnergyCertificate()->getEnergyConsumption() !== null)
                    ? strval($buildingUnit->getEnergyCertificate()->getEnergyConsumption())
                    : null
            );

        $zustandAngaben->setEnergiepass($energiePass);
        $immobilie->setZustandAngaben($zustandAngaben);
    }

    private function mapBuildingUnitPropertyMarketingInformationToImmobile(BuildingUnit $buildingUnit, Immobilie $immobilie): void
    {
        $objektkategroie = ($immobilie->getObjektkategorie() !== null) ? $immobilie->getObjektkategorie() : new Objektkategorie();
        $vermarktungsart = ($objektkategroie->getVermarktungsart() !== null) ? $objektkategroie->getVermarktungsart() : new Vermarktungsart();
        $versteigerung = ($immobilie->getVersteigerung() !== null) ? $immobilie->getVersteigerung() : new Versteigerung();
        $verwaltungObjekt = ($immobilie->getVerwaltungObjekt() !== null) ? $immobilie->getVerwaltungObjekt() : new VerwaltungObjekt();

        $versteigerung->setVerkehrswert($buildingUnit->getPropertyMarketingInformation()->getMarketValue());
        $verwaltungObjekt->setVerfuegbarAb($buildingUnit->getPropertyMarketingInformation()?->getAvailableFrom()?->format(format: 'd.m.Y'));

        if ($buildingUnit->getPropertyMarketingInformation()->getPropertyOfferType() !== null) {
            $verwaltungObjekt->setVerfuegbarAb($buildingUnit->getPropertyMarketingInformation()?->getAvailableFrom()?->format(format: 'd.m.Y'));

            if ($buildingUnit->getPropertyMarketingInformation()->getPropertyOfferType() == PropertyOfferType::LEASE) {
                $vermarktungsart->setLeasing(true);
            } elseif ($buildingUnit->getPropertyMarketingInformation()->getPropertyOfferType() == PropertyOfferType::RENT) {
                $vermarktungsart->setMietePacht(true);
            } elseif ($buildingUnit->getPropertyMarketingInformation()->getPropertyOfferType() == PropertyOfferType::PURCHASE) {
                $vermarktungsart->setKauf(true);
            }

            $objektkategroie->setVermarktungsart($vermarktungsart);
            $immobilie->setObjektkategorie($objektkategroie);
        }

        if ($buildingUnit->getPropertyMarketingInformation()?->getPropertyPricingInformation() !== null) {
            $immobilie->setPreise(new Preise());

            $this->mapPropertyPricingInformationToOpenImmoImmobilePreise(
                propertyPricingInformation: $buildingUnit->getPropertyMarketingInformation()->getPropertyPricingInformation(),
                preise: $immobilie->getPreise()
            );
        }

        $immobilie
            ->setVersteigerung($versteigerung)
            ->setVerwaltungObjekt($verwaltungObjekt);
    }

    private function mapBuildingUnitToOpenImmoImmobilie(BuildingUnit $buildingUnit, Immobilie $immobilie): void
    {
        if ($buildingUnit->getEnergyCertificate() !== null) {
            $this->mapBuildingUnitEnergyCertificateToImmobileEnergiepass(buildingUnit: $buildingUnit, immobilie: $immobilie);
        }

        if ($buildingUnit->getPropertyMarketingInformation() !== null) {
            $this->mapBuildingUnitPropertyMarketingInformationToImmobile(buildingUnit: $buildingUnit, immobilie: $immobilie);
        }
    }

    private function mapPropertyPricingInformationToOpenImmoImmobilePreise(
        PropertyPricingInformation $propertyPricingInformation,
        Preise $preise
    ): void {
        $innenCourtage = ($preise->getInnenCourtage() !== null) ? $preise->getInnenCourtage() : new InnenCourtage();
        $aussenCourtage = ($preise->getAussenCourtage() !== null) ? $preise->getAussenCourtage() : new AussenCourtage();
        $waehrung = null;

        if ($propertyPricingInformation->getCurrency() !== null) {
            $waehrung = new Waehrung();
            $waehrung->setIsoWaehrung($propertyPricingInformation->getCurrency()->getIsoName());
        }

        $innenCourtage->setValue($propertyPricingInformation->getInsideCommission());

        $aussenCourtage->setValue($propertyPricingInformation->getOutsideCommission());

        $kaufpreisNetto = new Kaufpreisnetto();
        $kaufpreisNetto->setValue($propertyPricingInformation->getPurchasePriceNet());

        $preise
            ->setKaufpreisnetto($kaufpreisNetto)
            ->setKaufpreisbrutto($propertyPricingInformation->getPurchasePriceGross())
            ->setNettokaltmiete($propertyPricingInformation->getNetColdRent())
            ->setKaltmiete($propertyPricingInformation->getColdRent())
            ->setWarmmiete($propertyPricingInformation->getRentIncludingHeating())
            ->setNebenkosten($propertyPricingInformation->getAdditionalCosts())
            ->setHeizkostenEnthalten($propertyPricingInformation->getHeatingCostsIncluded())
            ->setMietpreisProQm($propertyPricingInformation->getRentalPricePerSquareMeter())
            ->setPacht($propertyPricingInformation->getLease())
            ->setErbpacht($propertyPricingInformation->getLeasehold())
            ->setHausgeld($propertyPricingInformation->getHouseMoney())
            ->setProvisionspflichtig($propertyPricingInformation->isCommissionable())
            ->setInnenCourtage($innenCourtage)
            ->setAussenCourtage($aussenCourtage)
            ->setWaehrung($waehrung)
            ->setFreitextPreis($propertyPricingInformation->getFreeTextPrice())
            ->setKaution($propertyPricingInformation->getDeposit())
            ->setKautionText($propertyPricingInformation->getDepositText());
    }

    private function mapBuildingToOpenImmo(Building $building, Immobilie $immobilie): void
    {
        $zustandAngaben = ($immobilie->getZustandAngaben() !== null) ? $immobilie->getZustandAngaben() : new ZustandAngaben();
        $verwaltungObjekt = ($immobilie->getVerwaltungObjekt() !== null) ? $immobilie->getVerwaltungObjekt() : new VerwaltungObjekt();
        $geo = ($immobilie->getGeo() !== null) ? $immobilie->getGeo() : new Geo();
        $energiePass = ($zustandAngaben->getEnergiepass() !== null) ? $zustandAngaben->getEnergiepass() : new Energiepass();
        $ausstattung = ($immobilie->getAusstattung() !== null) ? $immobilie->getAusstattung() : new Ausstattung();

        $dachform = new Dachform();

        if ($building->getRoofShape() === RoofShape::FLAT_ROOF) {
            $dachform->setFlachdach(true);
        } elseif ($building->getRoofShape() === RoofShape::CRESTED_HIP_OR_CREST_HIP_ROOF) {
            $dachform->setKrueppelwalmdach(true);
        } elseif ($building->getRoofShape() === RoofShape::MANSARD_ROOF) {
            $dachform->setMansarddach(true);
        } elseif ($building->getRoofShape() === RoofShape::PENT_ROOF) {
            $dachform->setPultdach(true);
        } elseif ($building->getRoofShape() === RoofShape::TENT_ROOF) {
            $dachform->setPyramidendach(true);
        } elseif ($building->getRoofShape() === RoofShape::SADDLE_OR_GABLE_ROOF) {
            $dachform->setSatteldach(true);
        } elseif ($building->getRoofShape() === RoofShape::HIP_ROOF) {
            $dachform->setWalmdach(true);
        } else {
            $dachform = null;
        }

        $zustand = new Zustand();
        $zustand->setZustandArt(($building->getBuildingCondition() != null) ? $building->getBuildingCondition()->getName() : null);

        $zustandAngaben
            ->setZustand($zustand)
            ->setBaujahr(($building->getConstructionYear() !== null) ? strval($building->getConstructionYear()) : null);

        $verwaltungObjekt->setDenkmalgeschuetzt(
            ($building->getMonumentProtectionTypes() !== null && count($building->getMonumentProtectionTypes()) > 0)
        );

        $geo->setAnzahlEtagen($building->getNumberOfFloors());

        $energiePass->setGebaeudeart(($building->getBuildingType() !== null) ? $building->getBuildingType()->getName() : null);

        $zustandAngaben->setEnergiepass($energiePass);

        if ($building->getLiftTypes() !== null) {
            foreach ($building->getLiftTypes() as $liftType) {
                if ($liftType === LiftType::PASSENGER_LIFT) {
                    $ausstattung->getFahrstuhl()->setPersonen(true);
                } elseif ($liftType === LiftType::CARGO_LIFT) {
                    $ausstattung->getFahrstuhl()->setLasten(true);
                }
            }
        }

        $ausstattung->setDachform($dachform);

        $immobilie
            ->setZustandAngaben($zustandAngaben)
            ->setVerwaltungObjekt($verwaltungObjekt)
            ->setGeo($geo);
    }

    private function compressOpenImmoExport(OpenImmoExport $openImmoExport): void
    {
        $splFileInfo = $this->buildPathToOpenImmoExportDirectoryFromOpenImmoExport(openImmoExport: $openImmoExport);

        $finder = Finder::create()->files()->in(dirs: $splFileInfo->getPathname());

        $zipArchive = new \ZipArchive();

        if ($zipArchive->open(filename: $splFileInfo->getPathname() . '/OpenImmoExport.zip', flags: \ZipArchive::CREATE) !== true) {
            return;
        }

        foreach ($finder as $tempFile) {
            if ($tempFile->isFile() !== true) {
                continue;
            }

            $zipArchive->addFile(filepath: $tempFile->getPathname(), entryname: $tempFile->getFilename());
        }

        $zipArchive->close();
    }

    private function mapBuildingUnitToOpenImmoVerwaltungTechn(OpenImmoExport $openImmoExport, Immobilie $immobilie): void
    {
        $aktion = new Aktion();
        $aktion->setAktionart($openImmoExport->getExportActionType()->getName());

        $immobilie->getVerwaltungTechn()
            ->setObjektnrIntern($openImmoExport->getBuildingUnit()->getInternalNumber())
            ->setStandVom(new \DateTime())
            ->setAktion($aktion);
    }
}
