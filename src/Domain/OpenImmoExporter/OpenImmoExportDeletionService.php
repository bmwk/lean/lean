<?php

declare(strict_types=1);

namespace App\Domain\OpenImmoExporter;

use App\Domain\Entity\OpenImmoExporter\OpenImmoExport;
use App\Domain\File\FileDeletionService;
use App\Domain\OpenImmoFtpTransfer\OpenImmoFtpTransferTaskDeletionService;
use Doctrine\ORM\EntityManagerInterface;

class OpenImmoExportDeletionService
{
    public function __construct(
        private readonly FileDeletionService $fileDeletionService,
        private readonly OpenImmoFtpTransferTaskDeletionService $openImmoFtpTransferTaskDeletionService,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function deleteOpenImmoExport(OpenImmoExport $openImmoExport): void
    {
        $this->hardDeleteOpenImmoExport(openImmoExport: $openImmoExport);
    }

    private function hardDeleteOpenImmoExport(OpenImmoExport $openImmoExport): void
    {
        foreach ($openImmoExport->getOpenImmoFtpTransferTasks() as $openImmoFtpTransferTask) {
            $this->openImmoFtpTransferTaskDeletionService->deleteOpenImmoFtpTransferTask(openImmoFtpTransferTask: $openImmoFtpTransferTask);
        }

        $exportFile = $openImmoExport->getFile();

        $this->entityManager->remove(entity: $openImmoExport);
        $this->entityManager->flush();

        $this->fileDeletionService->deleteFile(file: $exportFile);
    }
}
