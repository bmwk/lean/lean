<?php

declare(strict_types=1);

namespace App\Domain\OpenImmoExporter\Exception;

interface OpenImmoExportExceptionInterface extends \Throwable
{
}
