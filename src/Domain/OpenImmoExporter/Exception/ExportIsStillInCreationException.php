<?php

declare(strict_types=1);

namespace App\Domain\OpenImmoExporter\Exception;

class ExportIsStillInCreationException extends \RuntimeException implements OpenImmoExportExceptionInterface
{
}
