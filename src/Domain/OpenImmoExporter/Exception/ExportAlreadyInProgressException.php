<?php

declare(strict_types=1);

namespace App\Domain\OpenImmoExporter\Exception;

class ExportAlreadyInProgressException extends \RuntimeException implements OpenImmoExportExceptionInterface
{
}
