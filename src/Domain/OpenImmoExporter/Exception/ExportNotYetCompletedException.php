<?php

declare(strict_types=1);

namespace App\Domain\OpenImmoExporter\Exception;

class ExportNotYetCompletedException extends \RuntimeException implements OpenImmoExportExceptionInterface
{
}
