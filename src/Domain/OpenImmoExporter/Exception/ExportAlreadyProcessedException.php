<?php

declare(strict_types=1);

namespace App\Domain\OpenImmoExporter\Exception;

class ExportAlreadyProcessedException extends \RuntimeException implements OpenImmoExportExceptionInterface
{
}
