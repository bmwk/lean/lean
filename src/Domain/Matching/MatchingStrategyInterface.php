<?php

declare(strict_types=1);

namespace App\Domain\Matching;

use App\Domain\Entity\LookingForPropertyRequest\MatchingResult;
use App\Domain\Entity\Property\BuildingUnit;

interface MatchingStrategyInterface
{
    /**
     * @return MatchingResult[]
     */
    public function doMatching(array $requests, BuildingUnit $buildingUnit): array;
}
