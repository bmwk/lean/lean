<?php

declare(strict_types=1);

namespace App\Domain\Feedback;

use App\Domain\Entity\BuildingUnitFeedback;
use App\Domain\Entity\Feedback;
use App\Domain\Entity\FeedbackGroup;
use App\Domain\Entity\FeedbackRelationship;
use App\Domain\Entity\Property\BuildingUnit;
use App\Repository\BuildingUnitFeedbackRepository;
use App\Repository\FeedbackGroupRepository;
use App\Repository\FeedbackRepository;

class FeedbackService
{
    public function __construct(
        private readonly FeedbackRepository $feedbackRepository,
        private readonly FeedbackGroupRepository $feedbackGroupRepository,
        private readonly BuildingUnitFeedbackRepository $buildingUnitFeedbackRepository
    ) {
    }

    /**
     * @return FeedbackGroup[]
     */
    public function fetchFeedbackGroupsByFeedbackRelationship(FeedbackRelationship $feedbackRelationship): array
    {
        return $this->feedbackGroupRepository->findBy(criteria: [
            'feedbackRelationship' => $feedbackRelationship,
            'deleted'              => false,
        ]);
    }

    public function fetchFeedbackById(int $id): ?Feedback
    {
        return $this->feedbackRepository->findOneBy(criteria: [
            'id'      => $id,
            'deleted' => false,
        ]);
    }

    /**
     * @return BuildingUnitFeedback[]
     */
    public function fetchBuildingUnitFeedbacksByBuildingUnit(BuildingUnit $buildingUnit): array
    {
        return $this->buildingUnitFeedbackRepository->findByBuildingUnit(buildingUnit: $buildingUnit);
    }

    public function fetchBuildingUnitFeedbackByBuildingUnitAndFeedback(BuildingUnit $buildingUnit, Feedback $feedback): ?BuildingUnitFeedback
    {
        return $this->buildingUnitFeedbackRepository->findOneByBuildingUnitAndFeedback(buildingUnit: $buildingUnit, feedback: $feedback);
    }
}
