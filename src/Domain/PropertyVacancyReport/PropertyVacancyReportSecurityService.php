<?php

declare(strict_types=1);

namespace App\Domain\PropertyVacancyReport;

use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\AccountUser\AccountUserRole;
use App\Domain\Entity\PropertyVacancyReport\PropertyVacancyReport;
use Symfony\Bundle\SecurityBundle\Security;

class PropertyVacancyReportSecurityService
{
    public function __construct(
        private readonly Security $security
    ) {
    }

    public function canViewPropertyVacancyReport(PropertyVacancyReport $propertyVacancyReport, AccountUser $accountUser): bool
    {
        $account = $accountUser->getAccount();

        if (
            $propertyVacancyReport->getAccount() === $account
            && (
                $this->security->isGranted(attributes: AccountUserRole::ROLE_USER->value) === true
                || $this->security->isGranted(attributes: AccountUserRole::ROLE_PROPERTY_VACANCY_REPORT_MANAGER->value) === true
                || $this->security->isGranted(attributes: AccountUserRole::ROLE_VIEWER->value) === true
                || $this->security->isGranted(attributes: AccountUserRole::ROLE_RESTRICTED_VIEWER->value) === true
            )
        ) {
            return true;
        }

        return false;
    }

    public function canEditPropertyVacancyReport(PropertyVacancyReport $propertyVacancyReport, AccountUser $accountUser): bool
    {
        $account = $accountUser->getAccount();

        if (
            $propertyVacancyReport->getAccount() === $account
            && (
                $this->security->isGranted(attributes: AccountUserRole::ROLE_USER->value) === true
                || $this->security->isGranted(attributes: AccountUserRole::ROLE_PROPERTY_VACANCY_REPORT_MANAGER->value) === true
            )
        ) {
            return true;
        }

        return false;
    }

    public function canDeletePropertyVacancyReport(PropertyVacancyReport $propertyVacancyReport, AccountUser $accountUser): bool
    {
        return $this->canEditPropertyVacancyReport(propertyVacancyReport: $propertyVacancyReport, accountUser: $accountUser);
    }
}
