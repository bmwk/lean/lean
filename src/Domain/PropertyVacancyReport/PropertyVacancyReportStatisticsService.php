<?php

declare(strict_types=1);

namespace App\Domain\PropertyVacancyReport;

use App\Domain\Entity\Account;
use App\Domain\Entity\Statistics\AmountPerMonth;
use App\Repository\PropertyVacancyReport\PropertyVacancyReportRepository;

class PropertyVacancyReportStatisticsService
{
    public function __construct(
        private readonly PropertyVacancyReportRepository $propertyVacancyReportRepository
    ) {
    }

    public function buildAmountPropertyVacancyReportsByMonth(Account $account, int $year, int $month): AmountPerMonth
    {
        $dateTimeStart = \DateTime::createFromFormat(format: 'Y-n-j H:i:s', datetime: $year . '-' . $month . '-' . '1' . ' 00:00:00');
        $dateTimeEnd = \DateTime::createFromFormat(format: 'Y-n-j H:i:s', datetime: $year . '-' . $month . '-' . '1' . ' 23:59:59')->modify(modifier: 'last day of');

        return new AmountPerMonth(
            year: (int) $dateTimeStart->format(format: 'Y'),
            month: (int) $dateTimeStart->format(format: 'n'),
            amount: $this->propertyVacancyReportRepository->countByCreatedAtRange(account: $account, dateTimeStart: $dateTimeStart, dateTimeEnd: $dateTimeEnd)
        );
    }

    /**
     * @return AmountPerMonth[]
     */
    public function buildAmountPropertyVacancyReportsByMonthFromYear(Account $account, int $year): array
    {
        $amountPropertyVacancyReportsByMonthFromYear = [];

        foreach (range(start: 1, end: 12) as $month) {
            $amountPropertyVacancyReportsByMonthFromYear[] = $this->buildAmountPropertyVacancyReportsByMonth(
                account: $account,
                year: $year,
                month: $month
            );
        }

        return $amountPropertyVacancyReportsByMonthFromYear;
    }
}
