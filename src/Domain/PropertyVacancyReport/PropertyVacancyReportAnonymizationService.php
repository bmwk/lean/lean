<?php

declare(strict_types=1);

namespace App\Domain\PropertyVacancyReport;

use App\Domain\Anonymize\AnonymizeService;
use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\PropertyVacancyReport\AnonymizedPropertyVacancyReport;
use App\Domain\Entity\PropertyVacancyReport\PropertyVacancyReport;
use Doctrine\ORM\EntityManagerInterface;

class PropertyVacancyReportAnonymizationService
{
    public function __construct(
        private readonly bool $anonymizeBackupEnabled,
        private readonly AnonymizeService $anonymizeService,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function anonymizePropertyVacancyReport(
        PropertyVacancyReport $propertyVacancyReport,
        AccountUser $anonymizedByAccountUser
    ): void {
        if ($this->anonymizeBackupEnabled === true) {
            $this->anonymizeService->backupObject($propertyVacancyReport);
        }

        $this->entityManager->beginTransaction();
        $anonymizedPropertyVacancyReport = new AnonymizedPropertyVacancyReport(
            propertyVacancyReport: $propertyVacancyReport
        );

        $propertyVacancyReport
            ->setReporterSalutation($anonymizedPropertyVacancyReport->getReporterSalutation())
            ->setReporterName(reporterName: $anonymizedPropertyVacancyReport->getReporterName())
            ->setReporterFirstName($anonymizedPropertyVacancyReport->getReporterFirstName())
            ->setReporterEmail($anonymizedPropertyVacancyReport->getReporterEmail())
            ->setReporterPhoneNumber($anonymizedPropertyVacancyReport->getReporterPhoneNumber())
            ->setReporterAlsoPropertyOwner($anonymizedPropertyVacancyReport->isReporterAlsoPropertyOwner())
            ->setObjectIsEmpty($anonymizedPropertyVacancyReport->isObjectIsEmpty())
            ->setObjectIsEmptySince($anonymizedPropertyVacancyReport->getObjectIsEmptySince())
            ->setObjectBecomesEmpty($anonymizedPropertyVacancyReport->isObjectIsEmpty())
            ->setObjectBecomesEmptyFrom($anonymizedPropertyVacancyReport->getObjectBecomesEmptyFrom())
            ->setIndustryClassification($anonymizedPropertyVacancyReport->getIndustryClassification())
            ->setPropertyUserCompanyName($anonymizedPropertyVacancyReport->getPropertyUserCompanyName())
            ->setBuildingCondition($anonymizedPropertyVacancyReport->getBuildingCondition())
            ->setAreaSize($anonymizedPropertyVacancyReport->getAreaSize())
            ->setPlotSize($anonymizedPropertyVacancyReport->getPlotSize())
            ->setRetailSpace($anonymizedPropertyVacancyReport->getRetailSpace())
            ->setGastronomySpace($anonymizedPropertyVacancyReport->getGastronomySpace())
            ->setLivingSpace($anonymizedPropertyVacancyReport->getLivingSpace())
            ->setUsableSpace($anonymizedPropertyVacancyReport->getUsableSpace())
            ->setSubsidiarySpace($anonymizedPropertyVacancyReport->getSubsidiarySpace())
            ->setUsableOutdoorAreaPossibility($anonymizedPropertyVacancyReport->getUsableOutdoorAreaPossibility())
            ->setUsableOutdoorArea($anonymizedPropertyVacancyReport->getUsableOutdoorArea())
            ->setShopWindowFrontWidth($anonymizedPropertyVacancyReport->getShopWindowFrontWidth())
            ->setShopWidth($anonymizedPropertyVacancyReport->getShopWidth())
            ->setGroundLevelSalesArea($anonymizedPropertyVacancyReport->getGroundLevelSalesArea())
            ->setStoreWindowAvailable($anonymizedPropertyVacancyReport->isStoreWindowAvailable())
            ->setNumberOfParkingLots($anonymizedPropertyVacancyReport->getNumberOfParkingLots())
            ->setBarrierFreeAccess($anonymizedPropertyVacancyReport->getBarrierFreeAccess())
            ->setLocationCategory($anonymizedPropertyVacancyReport->getLocationCategory())
            ->setLocationFactors($anonymizedPropertyVacancyReport->getLocationFactors())
            ->setPropertyOfferType($anonymizedPropertyVacancyReport->getPropertyOfferType())
            ->setPropertyDescription($anonymizedPropertyVacancyReport->getPropertyDescription())
            ->setPlace($anonymizedPropertyVacancyReport->getPlace())
            ->setPostalCode($anonymizedPropertyVacancyReport->getPostalCode())
            ->setStreetName($anonymizedPropertyVacancyReport->getStreetName())
            ->setHouseNumber($anonymizedPropertyVacancyReport->getHouseNumber())
            ->setBoroughPlace($anonymizedPropertyVacancyReport->getBoroughPlace())
            ->setQuarterPlace($anonymizedPropertyVacancyReport->getQuarterPlace())
            ->setNeighbourhoodPlace($anonymizedPropertyVacancyReport->getNeighbourhoodPlace())
            ->setPlaceDescription($anonymizedPropertyVacancyReport->getPlaceDescription())
            ->setReportedAt($anonymizedPropertyVacancyReport->getReportedAt())
            ->setProcessed($anonymizedPropertyVacancyReport->isProcessed())
            ->setProcessedAt($anonymizedPropertyVacancyReport->getProcessedAt())
            ->setAdmitted($anonymizedPropertyVacancyReport->isAdmitted())
            ->setRejected($anonymizedPropertyVacancyReport->isRejected())
            ->setConfirmationToken($anonymizedPropertyVacancyReport->getConfirmationToken())
            ->setEmailConfirmed($anonymizedPropertyVacancyReport->isEmailConfirmed())
            ->setEmailConfirmedAt($anonymizedPropertyVacancyReport->getEmailConfirmedAt())
            ->setBuildingUnit($anonymizedPropertyVacancyReport->getBuildingUnit())
            ->setProcessedByAccountUser($anonymizedPropertyVacancyReport->getProcessedByAccountUser())
            ->setPropertyVacancyReportImages($anonymizedPropertyVacancyReport->getPropertyVacancyReportImages())
            ->setPropertyVacancyReportNotes($anonymizedPropertyVacancyReport->getPropertyVacancyReportNotes())
            ->setAnonymized(anonymized: true)
            ->setAnonymizedByAccountUser(anonymizedByAccountUser: $anonymizedByAccountUser)
            ->setAnonymizedAt(anonymizedAt: new \DateTimeImmutable());

        $this->entityManager->flush();
        $this->entityManager->commit();
    }
}
