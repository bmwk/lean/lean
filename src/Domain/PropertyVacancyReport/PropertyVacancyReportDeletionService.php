<?php

declare(strict_types=1);

namespace App\Domain\PropertyVacancyReport;

use App\Domain\Entity\Account;
use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\DeletionType;
use App\Domain\Entity\PropertyVacancyReport\PropertyVacancyReport;
use App\Domain\Entity\PropertyVacancyReport\PropertyVacancyReportImage;
use App\Domain\Entity\PropertyVacancyReport\PropertyVacancyReportNote;
use App\Domain\File\FileDeletionService;
use App\Repository\PropertyVacancyReport\PropertyVacancyReportRepository;
use Doctrine\ORM\EntityManagerInterface;

class PropertyVacancyReportDeletionService
{
    public function __construct(
        private readonly FileDeletionService $fileDeletionService,
        private readonly PropertyVacancyReportAnonymizationService $propertyVacancyReportAnonymizationService,
        private readonly PropertyVacancyReportRepository $propertyVacancyReportRepository,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function deletePropertyVacancyReport(
        PropertyVacancyReport $propertyVacancyReport,
        DeletionType $deletionType = DeletionType::SOFT,
        ?AccountUser $deletedByAccountUser = null
    ): void {
        switch ($deletionType) {
            case DeletionType::SOFT:
                $this->softDeletePropertyVacancyReport(propertyVacancyReport: $propertyVacancyReport, deletedByAccountUser: $deletedByAccountUser);
                break;
            case DeletionType::HARD:
                $this->hardDeletePropertyVacancyReport(propertyVacancyReport: $propertyVacancyReport);
                break;
            case DeletionType::ANONYMIZATION:
                $this->anonymizationDeletePropertyVacancyReport(propertyVacancyReport: $propertyVacancyReport, deletedByAccountUser: $deletedByAccountUser);
                break;
            default:
                throw new \RuntimeException(message: 'Unsupported DeletionType');
        }
    }

    public function deletePropertyVacancyReportsByAccount(
        Account $account,
        DeletionType $deletionType = DeletionType::SOFT,
        ?AccountUser $deletedByAccountUser = null
    ): void {
        switch ($deletionType) {
            case DeletionType::SOFT:
                $this->softDeletePropertyVacancyReportsByAccount(account: $account, deletedByAccountUser: $deletedByAccountUser);
                break;
            case DeletionType::HARD:
                $this->hardDeletePropertyVacancyReportsByAccount(account: $account);
                break;
            case DeletionType::ANONYMIZATION:
                $this->anonymizationDeletePropertyVacancyReportsByAccount(account: $account, deletedByAccountUser: $deletedByAccountUser);
                break;
            default:
                throw new \RuntimeException(message: 'Unsupported DeletionType');
        }
    }

    public function deletePropertyVacancyReportImage(PropertyVacancyReportImage $propertyVacancyReportImage): void
    {
        $this->hardDeletePropertyVacancyReportImage(propertyVacancyReportImage: $propertyVacancyReportImage);
    }

    public function deleteAllPropertyVacancyReportImages(PropertyVacancyReport $propertyVacancyReport): void
    {
        $this->hardDeleteAllPropertyVacancyReportImages(propertyVacancyReport: $propertyVacancyReport);
    }

    public function deletePropertyVacancyReportNote(PropertyVacancyReportNote $propertyVacancyReportNote): void
    {
        $this->hardDeletePropertyVacancyReportNote(propertyVacancyReportNote: $propertyVacancyReportNote);
    }

    public function deleteAllPropertyVacancyReportNotes(PropertyVacancyReport $propertyVacancyReport): void
    {
        $this->hardDeleteAllPropertyVacancyReportNotes(propertyVacancyReport: $propertyVacancyReport);
    }

    private function softDeletePropertyVacancyReport(PropertyVacancyReport $propertyVacancyReport, ?AccountUser $deletedByAccountUser): void
    {
        $propertyVacancyReport
            ->setDeleted(deleted: true)
            ->setDeletedAt(deletedAt: new \DateTimeImmutable())
            ->setDeletedByAccountUser(deletedByAccountUser: $deletedByAccountUser);

        $this->entityManager->flush();
    }

    private function hardDeletePropertyVacancyReport(PropertyVacancyReport $propertyVacancyReport): void
    {
        $this->deleteAllPropertyVacancyReportImages(propertyVacancyReport: $propertyVacancyReport);
        $this->deleteAllPropertyVacancyReportNotes(propertyVacancyReport: $propertyVacancyReport);

        $this->entityManager->remove(entity: $propertyVacancyReport);
        $this->entityManager->flush();
    }

    private function anonymizationDeletePropertyVacancyReport(PropertyVacancyReport $propertyVacancyReport, ?AccountUser $deletedByAccountUser): void
    {
        $this->propertyVacancyReportAnonymizationService->anonymizePropertyVacancyReport(
            propertyVacancyReport: $propertyVacancyReport,
            anonymizedByAccountUser: $deletedByAccountUser
        );

        $this->softDeletePropertyVacancyReport(propertyVacancyReport: $propertyVacancyReport, deletedByAccountUser: $deletedByAccountUser);
    }

    private function softDeletePropertyVacancyReportsByAccount(Account $account, AccountUser $deletedByAccountUser): void
    {
        $propertyVacancyReports = $this->propertyVacancyReportRepository->findByAccount(account: $account);

        foreach ($propertyVacancyReports as $propertyVacancyReport) {
            $this->softDeletePropertyVacancyReport(propertyVacancyReport: $propertyVacancyReport, deletedByAccountUser:  $deletedByAccountUser);
        }
    }

    private function hardDeletePropertyVacancyReportsByAccount(Account $account): void
    {
        $propertyVacancyReports = $this->propertyVacancyReportRepository->findByAccount(account: $account);

        foreach ($propertyVacancyReports as $propertyVacancyReport) {
            $this->hardDeletePropertyVacancyReport(propertyVacancyReport: $propertyVacancyReport);
        }
    }

    private function anonymizationDeletePropertyVacancyReportsByAccount(Account $account, AccountUser $deletedByAccountUser): void
    {
        $propertyVacancyReports = $this->propertyVacancyReportRepository->findByAccount(account: $account);

        foreach ($propertyVacancyReports as $propertyVacancyReport) {
            $this->anonymizationDeletePropertyVacancyReport(propertyVacancyReport: $propertyVacancyReport, deletedByAccountUser:  $deletedByAccountUser);
        }
    }

    private function hardDeletePropertyVacancyReportImage(PropertyVacancyReportImage $propertyVacancyReportImage): void
    {
        $imageFile = $propertyVacancyReportImage->getFile();
        $imageThumbnailFile = $propertyVacancyReportImage->getThumbnailFile();

        $this->entityManager->remove(entity: $propertyVacancyReportImage);

        $this->entityManager->flush();

        if ($imageThumbnailFile !== null) {
            $this->fileDeletionService->deleteFile(file: $imageThumbnailFile);
        }

        $this->fileDeletionService->deleteFile(file: $imageFile);
    }

    private function hardDeleteAllPropertyVacancyReportImages(PropertyVacancyReport $propertyVacancyReport): void
    {
        foreach ($propertyVacancyReport->getPropertyVacancyReportImages() as $propertyVacancyReportImage) {
            $this->hardDeletePropertyVacancyReportImage(propertyVacancyReportImage: $propertyVacancyReportImage);
        }
    }

    private function hardDeletePropertyVacancyReportNote(PropertyVacancyReportNote $propertyVacancyReportNote): void
    {
        $this->entityManager->remove(entity: $propertyVacancyReportNote);
        $this->entityManager->flush();
    }

    private function hardDeleteAllPropertyVacancyReportNotes(PropertyVacancyReport $propertyVacancyReport): void
    {
        foreach ($propertyVacancyReport->getPropertyVacancyReportNotes() as $propertyVacancyReportNote) {
            $this->deletePropertyVacancyReportNote(propertyVacancyReportNote: $propertyVacancyReportNote);
        }
    }
}
