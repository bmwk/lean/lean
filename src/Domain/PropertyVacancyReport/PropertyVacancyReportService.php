<?php

declare(strict_types=1);

namespace App\Domain\PropertyVacancyReport;

use App\Domain\Classification\ClassificationService;
use App\Domain\Entity\Account;
use App\Domain\Entity\PropertyVacancyReport\PropertyVacancyReport;
use App\Domain\Entity\PropertyVacancyReport\PropertyVacancyReportFilter;
use App\Domain\Entity\PropertyVacancyReport\PropertyVacancyReportImage;
use App\Domain\Entity\PropertyVacancyReport\PropertyVacancyReportNote;
use App\Domain\Entity\PropertyVacancyReport\PropertyVacancyReportStatistics;
use App\Domain\File\FileService;
use App\Domain\Location\LocationService;
use App\Domain\PropertyVacancyReporter\Api\Entity\Request\ReportCreateRequest;
use App\Repository\PropertyVacancyReport\PropertyVacancyReportNoteRepository;
use App\Repository\PropertyVacancyReport\PropertyVacancyReportRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Uid\Uuid;

class PropertyVacancyReportService
{
    public function __construct(
        private readonly LocationService $locationService,
        private readonly ClassificationService $classificationService,
        private readonly FileService $fileService,
        private readonly PropertyVacancyReportRepository $propertyVacancyReportRepository,
        private readonly PropertyVacancyReportNoteRepository $propertyVacancyReportNoteRepository,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function fetchPropertyVacancyReportByIdAndAccount(int $id, Account $account): ?PropertyVacancyReport
    {
        return $this->propertyVacancyReportRepository->findOneBy(criteria: [
            'id'             => $id,
            'account'        => $account,
            'emailConfirmed' => true,
            'deleted'        => false,
        ]);
    }

    /**
     * @return PropertyVacancyReport[]
     */
    public function fetchUnprocessedPropertyVacancyReportsByAccount(
        Account $account,
        ?bool $emailConfirmed = null,
        ?PropertyVacancyReportFilter $propertyVacancyReportFilter = null
    ): array {
        return $this->propertyVacancyReportRepository->findByAccount(
            account: $account,
            processed: false,
            emailConfirmed: $emailConfirmed,
            propertyVacancyReportFilter: $propertyVacancyReportFilter
        );
    }

    /**
     * @return PropertyVacancyReport[]
     */
    public function fetchProcessedPropertyVacancyReportsByAccount(
        Account $account,
        ?bool $emailConfirmed = null,
        ?PropertyVacancyReportFilter $propertyVacancyReportFilter = null
    ): array {
        return $this->propertyVacancyReportRepository->findByAccount(
            account: $account,
            processed: true,
            emailConfirmed: $emailConfirmed,
            propertyVacancyReportFilter: $propertyVacancyReportFilter
        );
    }

    public function fetchPropertyVacancyReportStatisticsByAccount(Account $account): PropertyVacancyReportStatistics
    {
        $propertyVacancyReportStatistics = new PropertyVacancyReportStatistics();

        $propertyVacancyReportStatistics
            ->setUnprocessed(
                $this->propertyVacancyReportRepository->count(criteria: [
                    'account'        => $account,
                    'deleted'        => false,
                    'processed'      => false,
                    'emailConfirmed' => true,
                ])
            )
            ->setProcessed(
                $this->propertyVacancyReportRepository->count(criteria: [
                    'account'        => $account,
                    'deleted'        => false,
                    'processed'      => true,
                    'emailConfirmed' => true,
                ])
            );

        return $propertyVacancyReportStatistics;
    }

    public function fetchPropertyVacancyReportNoteById(int $id): ?PropertyVacancyReportNote
    {
        return $this->propertyVacancyReportNoteRepository->find(id: $id);
    }

    public function fetchPropertyVacancyReportByUuid(Uuid $uuid): ?PropertyVacancyReport
    {
        return $this->propertyVacancyReportRepository->findOneBy(criteria: [
            'uuid'    => $uuid,
            'deleted' => false,
        ]);
    }

    public function createPropertyVacancyReportFromReportCreateRequest(ReportCreateRequest $reportCreateRequest): PropertyVacancyReport
    {
        $propertyVacancyReport = new PropertyVacancyReport();

        $reporter = $reportCreateRequest->getReporter();
        $propertyLocation = $reportCreateRequest->getPropertyLocation();
        $propertyInformation = $reportCreateRequest->getPropertyInformation();

        $place = $this->locationService->fetchPlaceByUuid($propertyLocation->getPlaceUuid());

        $borough = null;

        if ($propertyLocation->getBoroughPlaceUuid() !== null) {
            $borough = $this->locationService->fetchPlaceByUuid($propertyLocation->getBoroughPlaceUuid());
        }

        $quarter = null;

        if ($propertyLocation->getQuarterPlaceUuid() !== null) {
            $quarter = $this->locationService->fetchPlaceByUuid($propertyLocation->getQuarterPlaceUuid());
        }

        $industryClassificationLevelOne = $propertyInformation->getIndustryClassificationId();

        if ($industryClassificationLevelOne !== null) {
            $industryClassification = $this->classificationService->fetchIndustryClassificationByLevels(levelOne: $industryClassificationLevelOne, levelTwo: null, levelThree: null);
        } else {
            $industryClassification = null;
        }

        $propertyVacancyReport
            ->setUuid(Uuid::v6())
            ->setReporterCompanyName($reporter->getCompanyName())
            ->setReporterSalutation($reporter->getSalutation())
            ->setReporterName($reporter->getName())
            ->setReporterFirstName($reporter->getFirstName())
            ->setReporterEmail($reporter->getEmail())
            ->setReporterPhoneNumber($reporter->getPhoneNumber())
            ->setReporterAlsoPropertyOwner($reporter->isAlsoPropertyOwner())
            ->setObjectIsEmpty($propertyInformation->isObjectIsEmpty())
            ->setObjectIsEmptySince($propertyInformation->getObjectIsEmptySince())
            ->setObjectBecomesEmpty($propertyInformation->isObjectBecomesEmpty())
            ->setObjectBecomesEmptyFrom($propertyInformation->getObjectBecomesEmptyFrom())
            ->setIndustryClassification($industryClassification)
            ->setPropertyUserCompanyName($propertyInformation->getPropertyUserCompanyName())
            ->setBuildingCondition($propertyInformation->getBuildingCondition())
            ->setAreaSize($propertyInformation->getAreaSize())
            ->setPlotSize($propertyInformation->getPlotSize())
            ->setRetailSpace($propertyInformation->getRetailSpace())
            ->setGastronomySpace($propertyInformation->getGastronomySpace())
            ->setLivingSpace($propertyInformation->getLivingSpace())
            ->setUsableSpace($propertyInformation->getUsableSpace())
            ->setSubsidiarySpace($propertyInformation->getSubsidiarySpace())
            ->setUsableOutdoorAreaPossibility($propertyInformation->getUsableOutdoorAreaPossibility())
            ->setUsableOutdoorArea($propertyInformation->getUsableOutdoorArea())
            ->setShopWindowFrontWidth($propertyInformation->getShopWindowFrontWidth())
            ->setShopWidth($propertyInformation->getShopWidth())
            ->setGroundLevelSalesArea($propertyInformation->getGroundLevelSalesArea())
            ->setStoreWindowAvailable($propertyInformation->isStoreWindowAvailable())
            ->setNumberOfParkingLots($propertyInformation->getNumberOfParkingLots())
            ->setBarrierFreeAccess($propertyInformation->getBarrierFreeAccess())
            ->setLocationCategory($propertyInformation->getLocationCategory())
            ->setLocationFactors($propertyInformation->getLocationFactors())
            ->setPropertyOfferType($propertyInformation->getPropertyOfferType())
            ->setPropertyDescription($propertyInformation->getPropertyDescription())
            ->setPlace($place)
            ->setPostalCode($propertyLocation->getPostalCode())
            ->setStreetName($propertyLocation->getStreetName())
            ->setHouseNumber($propertyLocation->getHouseNumber())
            ->setBoroughPlace($borough)
            ->setQuarterPlace($quarter)
            ->setNeighbourhoodPlace(null)
            ->setPlaceDescription($propertyLocation->getPlaceDescription())
            ->setReportedAt(new \DateTimeImmutable())
            ->setProcessed(false)
            ->setAdmitted(false)
            ->setRejected(false)
            ->setConfirmationToken(Uuid::v6())
            ->setEmailConfirmed(false)
            ->setDeleted(false)
            ->setAnonymized(false);

        return $propertyVacancyReport;
    }

    public function createAndSavePropertyVacancyReportImageFromFileContent(
        string $fileContent,
        string $fileName,
        string $fileExtension,
        PropertyVacancyReport $propertyVacancyReport
    ): PropertyVacancyReportImage {
        $thumbnailImagick = new \Imagick();
        $thumbnailImagick->readImageBlob($fileContent);

        if (
            $thumbnailImagick->getImageWidth() > 480
            || $thumbnailImagick->getImageHeight() > 270
        ) {
            $thumbnailImagick->resizeImage(
                columns: 480,
                rows: 270,
                filter: \Imagick::FILTER_LANCZOS2,
                blur: 0,
                bestfit: true,
                legacy: false
            );
        }

        $thumbnailImageFile = $this->fileService->saveFileFromFileContent(
            fileContent: $thumbnailImagick->getImageBlob(),
            filename: 'thumbnail_' . $fileName,
            fileExtension: $fileExtension,
            account: $propertyVacancyReport->getAccount(),
            public: false
        );

        $thumbnailImagick->destroy();

        $imageImagick = new \Imagick();
        $imageImagick->readImageBlob($fileContent);

        if (
            $imageImagick->getImageWidth() > 1920
            || $imageImagick->getImageHeight() > 1080
        ) {
            $imageImagick->resizeImage(
                columns: 1920,
                rows: 1080,
                filter: \Imagick::FILTER_LANCZOS2,
                blur: 0,
                bestfit: true,
                legacy: false
            );
        }

        $imageFile = $this->fileService->saveFileFromFileContent(
            fileContent: $imageImagick->getImageBlob(),
            filename: $fileName,
            fileExtension: $fileExtension,
            account: $propertyVacancyReport->getAccount(),
            public: false
        );

        $imageImagick->destroy();

        $propertyVacancyReportImage = new PropertyVacancyReportImage();

        $propertyVacancyReportImage
            ->setPropertyVacancyReport($propertyVacancyReport)
            ->setAccount($propertyVacancyReport->getAccount())
            ->setFile($imageFile)
            ->setThumbnailFile($thumbnailImageFile)
            ->setDeleted(false);

        $this->entityManager->persist(entity: $propertyVacancyReportImage);

        $this->entityManager->flush();

        return $propertyVacancyReportImage;
    }
}
