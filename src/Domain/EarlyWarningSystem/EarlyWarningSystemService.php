<?php

declare(strict_types=1);

namespace App\Domain\EarlyWarningSystem;

use App\Domain\Entity\Account;
use App\TaodEarlyWarningSystem\Entity\BuildingUnitScoring;
use App\TaodEarlyWarningSystem\Entity\BusinessLocationAreaScoring;
use App\TaodEarlyWarningSystem\TaodEarlyWarningSystemService;

class EarlyWarningSystemService
{
    public function __construct(
        private readonly TaodEarlyWarningSystemService $taodEarlyWarningSystemService
    ) {
    }

    /**
     * @return BuildingUnitScoring[]
     */
    public function fetchBuildingUnitScorings(Account $account): array
    {
        return $this->taodEarlyWarningSystemService->fetchBuildingUnitScoringsByAccountId(accountId: $account->getId());
    }

    public function fetchBuildingUnitScoringByTaodBuildingUnitId(int $taodBuildingUnitId, Account $account): ?BuildingUnitScoring
    {
        return $this->taodEarlyWarningSystemService->fetchBuildingUnitScoringByTaodBuildingUnitIdAndAccountId(
            taodBuildingUnitId: $taodBuildingUnitId,
            accountId: $account->getId()
        );
    }

    /**
     * @return BusinessLocationAreaScoring[]
     */
    public function fetchBusinessLocationAreaScorings(Account $account): array
    {
        return $this->taodEarlyWarningSystemService->fetchBusinessLocationAreaScoringsByAccountId(accountId: $account->getId());
    }

    public function fetchBusinessLocationAreaScoringByBusinessLocationAreaId(int $businessLocationAreaId, Account $account): ?BusinessLocationAreaScoring
    {
        return $this->taodEarlyWarningSystemService->fetchBusinessLocationAreaScoringByBusinessLocationAreaIdAndAccountId(
            businessLocationAreaId: $businessLocationAreaId,
            accountId: $account->getId()
        );
    }

    public function truncateBusinessLocationAreaTable(): void
    {
        $this->taodEarlyWarningSystemService->truncateBusinessLocationAreaTable();
    }

    public function truncateBuildingUnitTable(): void
    {

    }
}
