<?php

declare(strict_types=1);

namespace App\Domain\EarlyWarningSystem\JsonRepresentation;

use App\Domain\Entity\LocationFactor;
use App\Domain\Entity\Property\BuildingUnit;
use App\Domain\Entity\Property\InterimUseType;
use App\TaodEarlyWarningSystem\Entity\JsonRepresentation\BuildingUnit as BuildingUnitJsonRepresentation;

class BuildingUnitFactory
{
    public static function createFromBuildingUnit(BuildingUnit $buildingUnit): BuildingUnitJsonRepresentation
    {
        $buildingUnitJsonRepresentation = new BuildingUnitJsonRepresentation();

        $buildingUnitJsonRepresentation
            ->setId($buildingUnit->getId())
            ->setAreaSize($buildingUnit->getAreaSize())
            ->setEntranceDoorWidth($buildingUnit->getEntranceDoorWidth())
            ->setBarrierFreeAccess($buildingUnit->getBarrierFreeAccess()?->name)
            ->setObjectForeclosed($buildingUnit->isObjectForeclosed());

        if ($buildingUnit->getLocationFactors() !== null) {
            $buildingUnitJsonRepresentation->setLocationFactors(
                array_map(
                    callback: function (int $locationFactorValue): string {
                        return LocationFactor::from($locationFactorValue)->name;
                    },
                    array: $buildingUnit->getLocationFactors()
                )
            );
        }

        $buildingUnitJsonRepresentation->setAddress(AddressFactory::createFromAddress($buildingUnit->getAddress()));

        if ($buildingUnit->getPropertySpacesData() !== null) {
            $buildingUnitJsonRepresentation->setPropertySpacesData(PropertySpacesDataFactory::createFromPropertySpacesData($buildingUnit->getPropertySpacesData()));
        }

        if ($buildingUnit->getGeolocationPoint() !== null) {
            $buildingUnitJsonRepresentation->setGeolocationPoint(GeolocationPointFactory::createFromGeolocationPoint($buildingUnit->getGeolocationPoint()));
        }

        $buildingUnitJsonRepresentation
            ->setObjectIsEmpty($buildingUnit->isObjectIsEmpty())
            ->setObjectIsEmptySince($buildingUnit->getObjectIsEmptySince())
            ->setObjectBecomesEmpty($buildingUnit->isObjectBecomesEmpty())
            ->setObjectBecomesEmptyFrom($buildingUnit->getObjectBecomesEmptyFrom())
            ->setReuseAgreed($buildingUnit->isReuseAgreed())
            ->setReuseFrom($buildingUnit->getReuseFrom())
            ->setNumberOfParkingLots($buildingUnit->getNumberOfParkingLots())
            ->setInFloors($buildingUnit->getInFloors())
            ->setInterimUsePossible($buildingUnit->isInterimUsePossible());

        if ($buildingUnit->getCurrentUsageIndustryClassification() !== null) {
            $buildingUnitJsonRepresentation->setCurrentUsageIndustryClassification(
                IndustryClassificationFactory::createFromIndustryClassification($buildingUnit->getCurrentUsageIndustryClassification())
            );
        }

        if ($buildingUnit->getPastUsageIndustryClassification() !== null) {
            $buildingUnitJsonRepresentation->setPastUsageIndustryClassification(
                IndustryClassificationFactory::createFromIndustryClassification($buildingUnit->getPastUsageIndustryClassification())
            );
        }

        if ($buildingUnit->getFutureUsageIndustryClassification() !== null) {
            $buildingUnitJsonRepresentation->setFutureUsageIndustryClassification(
                IndustryClassificationFactory::createFromIndustryClassification($buildingUnit->getFutureUsageIndustryClassification())
            );
        }

        if ($buildingUnit->getInterimUseTypes() !== null) {
            $buildingUnitJsonRepresentation->setInterimUseTypes(
                array_map(
                    callback: function (InterimUseType $interimUseType): string {
                        return $interimUseType->name;
                    },
                    array: $buildingUnit->getInterimUseTypes()
                )
            );
        }

        $buildingUnitJsonRepresentation
            ->setShopWindowFrontWidth($buildingUnit->getShopWindowFrontWidth())
            ->setShowroomAvailable($buildingUnit->isShowroomAvailable())
            ->setShopWidth($buildingUnit->getShopWidth())
            ->setGroundLevelSalesArea($buildingUnit->isGroundLevelSalesArea())
            ->setKeyProperty($buildingUnit->isKeyProperty())
            ->setBuilding(BuildingFactory::createFromBuilding($buildingUnit->getBuilding()));

        $propertyUsers = [];

        foreach ($buildingUnit->getPropertyUsers() as $propertyUser) {
            $propertyUsers[] = PropertyUserFactory::createFromPropertyUser($propertyUser);
        }

        $buildingUnitJsonRepresentation->setPropertyUsers($propertyUsers);
        $buildingUnitJsonRepresentation->setAccountId($buildingUnit->getAccount()->getId());

        return $buildingUnitJsonRepresentation;
    }
}
