<?php

declare(strict_types=1);

namespace App\Domain\EarlyWarningSystem\JsonRepresentation;

use App\Domain\Entity\GeolocationPoint;
use App\TaodEarlyWarningSystem\Entity\JsonRepresentation\GeolocationPoint as GeolocationPointJsonRepresentation;

class GeolocationPointFactory
{
    public static function createFromGeolocationPoint(GeolocationPoint $geolocationPoint): GeolocationPointJsonRepresentation
    {
        $geolocationPointJsonRepresentation = new GeolocationPointJsonRepresentation();

        $geolocationPointJsonRepresentation->setPoint($geolocationPoint->getPoint());

        return $geolocationPointJsonRepresentation;
    }
}
