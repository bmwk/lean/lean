<?php

declare(strict_types=1);

namespace App\Domain\EarlyWarningSystem\JsonRepresentation;

use App\Domain\Entity\GeolocationPolygon;
use \App\TaodEarlyWarningSystem\Entity\JsonRepresentation\GeolocationPolygon as GeolocationPolygonJsonRepresentation;

class GeolocationPolygonFactory
{
    public static function createFromGeolocationPolygon(GeolocationPolygon $geolocationPolygon): GeolocationPolygonJsonRepresentation
    {
        $geolocationPolygonJsonRepresentation = new GeolocationPolygonJsonRepresentation();

        $geolocationPolygonJsonRepresentation->setPolygon($geolocationPolygon->getPolygon());

        return $geolocationPolygonJsonRepresentation;
    }
}
