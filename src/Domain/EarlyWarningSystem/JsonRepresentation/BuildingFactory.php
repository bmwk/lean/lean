<?php

declare(strict_types=1);

namespace App\Domain\EarlyWarningSystem\JsonRepresentation;

use App\Domain\Entity\Property\Building;
use App\Domain\Entity\Property\InternetConnectionType;
use App\TaodEarlyWarningSystem\Entity\JsonRepresentation\Building as BuildingJsonRepresentation;

class BuildingFactory
{
    public static function createFromBuilding(Building $building): BuildingJsonRepresentation
    {
        $buildingJsonRepresentation = new BuildingJsonRepresentation();

        $buildingJsonRepresentation
            ->setLocationCategory($building->getLocationCategory()?->name)
            ->setBuildingStandard($building->getBuildingStandard()?->name)
            ->setConstructionYear($building->getConstructionYear())
            ->setNumberOfFloors($building->getNumberOfFloors())
            ->setBuildingType($building->getBuildingType()?->name);

        if ($building->getInternetConnectionTypes() !== null) {
            $buildingJsonRepresentation->setInternetConnectionTypes(
                array_map(
                    callback: function (InternetConnectionType $internetConnectionType): string {
                        return $internetConnectionType->name;
                    },
                    array: $building->getInternetConnectionTypes()
                )
            );
        }

        return $buildingJsonRepresentation;
    }
}
