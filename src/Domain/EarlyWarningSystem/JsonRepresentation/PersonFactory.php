<?php

declare(strict_types=1);

namespace App\Domain\EarlyWarningSystem\JsonRepresentation;

use App\Domain\Entity\Person\Person;
use App\Domain\Entity\Person\PersonType;
use App\TaodEarlyWarningSystem\Entity\JsonRepresentation\Person as PersonJsonRepresentation;

class PersonFactory
{
    public static function createFromPerson(Person $person): PersonJsonRepresentation
    {
        $personJsonRepresentation = new PersonJsonRepresentation();

        $personType = $person->getPersonType();

        $personJsonRepresentation->setPersonType($personType->name);

        if ($personType === PersonType::COMPANY || $personType === PersonType::COMMUNE) {
            $personJsonRepresentation->setName($person->getName());
        }

        return $personJsonRepresentation;
    }
}
