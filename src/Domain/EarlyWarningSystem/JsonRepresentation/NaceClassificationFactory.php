<?php

declare(strict_types=1);

namespace App\Domain\EarlyWarningSystem\JsonRepresentation;

use App\Domain\Entity\Classification\NaceClassification;
use App\TaodEarlyWarningSystem\Entity\JsonRepresentation\NaceClassification as NaceClassificationJsonRepresentation;

class NaceClassificationFactory
{
    public static function createFromNaceClassification(NaceClassification $naceClassification): NaceClassificationJsonRepresentation
    {
        $naceClassificationJsonRepresentation = new NaceClassificationJsonRepresentation();

        $naceClassificationJsonRepresentation
            ->setName($naceClassification->getName())
            ->setCode($naceClassification->getCode())
            ->setLevel($naceClassification->getLevel()->name);

        return $naceClassificationJsonRepresentation;
    }
}
