<?php

declare(strict_types=1);

namespace App\Domain\EarlyWarningSystem\JsonRepresentation;

use App\Domain\Entity\Place;
use App\TaodEarlyWarningSystem\Entity\JsonRepresentation\Place as PlaceJsonRepresentation;

class PlaceFactory
{
    public static function createFromPlace(Place $place): PlaceJsonRepresentation
    {
        $placeJsonRepresentation = new PlaceJsonRepresentation();

        $placeJsonRepresentation
            ->setUuid($place->getUuid())
            ->setPlaceName($place->getPlaceName())
            ->setPlaceShortName($place->getPlaceShortName())
            ->setPlaceType($place->getPlaceType()->name);

        return $placeJsonRepresentation;
    }
}
