<?php

declare(strict_types=1);

namespace App\Domain\EarlyWarningSystem\JsonRepresentation;

use App\Domain\Entity\BusinessLocationArea;
use App\TaodEarlyWarningSystem\Entity\JsonRepresentation\BusinessLocationArea as BusinessLocationAreaJsonRepresentation;

class BusinessLocationAreaFactory
{
    public static function createFromBusinessLocationArea(BusinessLocationArea $businessLocationArea): BusinessLocationAreaJsonRepresentation
    {
        $businessLocationAreaJsonRepresentation = new BusinessLocationAreaJsonRepresentation();

        $businessLocationAreaJsonRepresentation
            ->setId($businessLocationArea->getId())
            ->setLocationCategory($businessLocationArea->getLocationCategory()->name)
            ->setAccountId($businessLocationArea->getAccount()->getId())
            ->setGeolocationPolygon(GeolocationPolygonFactory::createFromGeolocationPolygon(geolocationPolygon: $businessLocationArea->getGeolocationPolygon()));

        return $businessLocationAreaJsonRepresentation;
    }
}
