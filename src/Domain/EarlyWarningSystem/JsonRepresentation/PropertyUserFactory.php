<?php

declare(strict_types=1);

namespace App\Domain\EarlyWarningSystem\JsonRepresentation;

use App\Domain\Entity\Property\PropertyUser;
use App\TaodEarlyWarningSystem\Entity\JsonRepresentation\PropertyUser as PropertyUserJsonRepresentation;

class PropertyUserFactory
{
    public static function createFromPropertyUser(PropertyUser $propertyUser): PropertyUserJsonRepresentation
    {
        $propertyUserJsonRepresentation = new PropertyUserJsonRepresentation();

        $propertyUserJsonRepresentation
            ->setPerson(PersonFactory::createFromPerson($propertyUser->getPerson()))
            ->setPropertyUserStatus($propertyUser->getPropertyUserStatus()->name)
            ->setEndOfRental($propertyUser->getEndOfRental())
            ->setOpeningHours($propertyUser->getOpeningHours())
            ->setRating($propertyUser->getRating())
            ->setWebsite($propertyUser->getWebsite());

        if ($propertyUser->getIndustryClassification() !== null) {
            $propertyUserJsonRepresentation->setIndustryClassification(
                IndustryClassificationFactory::createFromIndustryClassification($propertyUser->getIndustryClassification())
            );
        }

        if ($propertyUser->getNaceClassification() !== null) {
            $propertyUserJsonRepresentation->setNaceClassification(
                NaceClassificationFactory::createFromNaceClassification($propertyUser->getNaceClassification())
            );
        }

        return $propertyUserJsonRepresentation;
    }
}
