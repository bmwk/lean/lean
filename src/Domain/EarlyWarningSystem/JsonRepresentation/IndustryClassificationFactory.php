<?php

declare(strict_types=1);

namespace App\Domain\EarlyWarningSystem\JsonRepresentation;

use App\Domain\Entity\Classification\IndustryClassification;
use App\TaodEarlyWarningSystem\Entity\JsonRepresentation\IndustryClassification as IndustryClassificationJsonRepresentation;

class IndustryClassificationFactory
{
    public static function createFromIndustryClassification(IndustryClassification $industryClassification): IndustryClassificationJsonRepresentation
    {
        $industryClassificationJsonRepresentation = new IndustryClassificationJsonRepresentation();

        $industryClassificationJsonRepresentation
            ->setName($industryClassification->getName())
            ->setLevelOne($industryClassification->getLevelOne())
            ->setLevelTwo($industryClassification->getLevelTwo())
            ->setLevelThree($industryClassification->getLevelThree());

        return $industryClassificationJsonRepresentation;
    }
}
