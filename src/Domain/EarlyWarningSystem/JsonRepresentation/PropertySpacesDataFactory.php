<?php

declare(strict_types=1);

namespace App\Domain\EarlyWarningSystem\JsonRepresentation;

use App\Domain\Entity\Property\PropertySpacesData;
use App\TaodEarlyWarningSystem\Entity\JsonRepresentation\PropertySpacesData as PropertySpacesDataJsonRepresentation;

class PropertySpacesDataFactory
{
    public static function createFromPropertySpacesData(PropertySpacesData $propertySpacesData): PropertySpacesDataJsonRepresentation
    {
        $propertySpacesDataJsonRepresentation = new PropertySpacesDataJsonRepresentation();

        $propertySpacesDataJsonRepresentation
            ->setRoomCount($propertySpacesData->getRoomCount())
            ->setLivingSpace($propertySpacesData->getLivingSpace())
            ->setUsableSpace($propertySpacesData->getUsableSpace())
            ->setStoreSpace($propertySpacesData->getStoreSpace())
            ->setStorageSpace($propertySpacesData->getStorageSpace())
            ->setRetailSpace($propertySpacesData->getRetailSpace())
            ->setOpenSpace($propertySpacesData->getOpenSpace())
            ->setOfficeSpace($propertySpacesData->getOfficeSpace())
            ->setOfficePartSpace($propertySpacesData->getOfficePartSpace())
            ->setAdministrationSpace($propertySpacesData->getAdministrationSpace())
            ->setGastronomySpace($propertySpacesData->getGastronomySpace())
            ->setProductionSpace($propertySpacesData->getProductionSpace())
            ->setPlotSize($propertySpacesData->getPlotSize())
            ->setOtherSpace($propertySpacesData->getOtherSpace())
            ->setBalconyTurfSpace($propertySpacesData->getBalconyTurfSpace())
            ->setGardenSpace($propertySpacesData->getGardenSpace())
            ->setCellarSpace($propertySpacesData->getCellarSpace())
            ->setAtticSpace($propertySpacesData->getAtticSpace())
            ->setHeatableSurface($propertySpacesData->getHeatableSurface())
            ->setRentableArea($propertySpacesData->getRentableArea())
            ->setMinimumSpaceForSeparation($propertySpacesData->getMinimumSpaceForSeparation())
            ->setUsableOutdoorAreaPossibility($propertySpacesData->getUsableOutdoorAreaPossibility()?->name)
            ->setTotalOutdoorArea($propertySpacesData->getTotalOutdoorArea())
            ->setUsableOutdoorArea($propertySpacesData->getUsableOutdoorArea())
            ->setOutdoorSalesArea($propertySpacesData->getOutdoorSalesArea())
            ->setRoofingPresent($propertySpacesData->isRoofingPresent());

        return $propertySpacesDataJsonRepresentation;
    }
}
