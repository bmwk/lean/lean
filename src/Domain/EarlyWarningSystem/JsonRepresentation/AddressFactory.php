<?php

declare(strict_types=1);

namespace App\Domain\EarlyWarningSystem\JsonRepresentation;

use App\Domain\Entity\Property\Address;
use \App\TaodEarlyWarningSystem\Entity\JsonRepresentation\Address as AddressJsonRepresentation;

class AddressFactory
{
    public static function createFromAddress(Address $address): AddressJsonRepresentation
    {
        $addressJsonRepresentation = new AddressJsonRepresentation();

        $addressJsonRepresentation
            ->setPlace(PlaceFactory::createFromPlace($address->getPlace()))
            ->setPostalCode($address->getPostalCode())
            ->setStreetName($address->getStreetName())
            ->setHouseNumber($address->getHouseNumber());

        if ($address->getBoroughPlace() !== null) {
            $addressJsonRepresentation->setBoroughPlace(PlaceFactory::createFromPlace($address->getBoroughPlace()));
        }

        if ($address->getQuarterPlace() !== null) {
            $addressJsonRepresentation->setQuarterPlace(PlaceFactory::createFromPlace($address->getQuarterPlace()));
        }

        if ($address->getNeighbourhoodPlace() !== null) {
            $addressJsonRepresentation->setNeighbourhoodPlace(PlaceFactory::createFromPlace($address->getNeighbourhoodPlace()));
        }

        $addressJsonRepresentation->setAdditionalAddressInformation($address->getAdditionalAddressInformation());

        return $addressJsonRepresentation;
    }
}
