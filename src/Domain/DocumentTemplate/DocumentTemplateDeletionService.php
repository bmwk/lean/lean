<?php

declare(strict_types=1);

namespace App\Domain\DocumentTemplate;

use App\Domain\Entity\Account;
use App\Domain\Entity\DocumentTemplate;
use App\Domain\File\FileDeletionService;
use App\Repository\DocumentTemplateRepository;
use Doctrine\ORM\EntityManagerInterface;

class DocumentTemplateDeletionService
{
    public function __construct(
        private readonly DocumentTemplateRepository $documentTemplateRepository,
        private readonly FileDeletionService $fileDeletionService,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function deleteDocumentTemplate(DocumentTemplate $documentTemplate): void
    {
        $this->hardDeleteDocumentTemplate(documentTemplate: $documentTemplate);
    }

    public function deleteDocumentTemplatesByAccount(Account $account): void
    {
        $this->hardDeleteDocumentTemplatesByAccount(account: $account);
    }

    private function hardDeleteDocumentTemplate(DocumentTemplate $documentTemplate): void
    {
        $file = $documentTemplate->getFile();

        $this->entityManager->remove(entity: $documentTemplate);
        $this->entityManager->flush();

        $this->fileDeletionService->deleteFile(file: $file);
    }

    private function hardDeleteDocumentTemplatesByAccount(Account $account): void
    {
        $documentTemplates = $this->documentTemplateRepository->findBy(criteria: ['account' => $account]);

        foreach ($documentTemplates as $documentTemplate) {
            $this->hardDeleteDocumentTemplate(documentTemplate: $documentTemplate);
        }
    }
}
