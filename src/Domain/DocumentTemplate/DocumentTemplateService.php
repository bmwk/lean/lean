<?php

declare(strict_types=1);

namespace App\Domain\DocumentTemplate;

use App\Domain\Entity\Account;
use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\DocumentTemplate;
use App\Domain\Entity\DocumentTemplateType;
use App\Domain\File\FileService;
use App\Repository\DocumentTemplateRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class DocumentTemplateService
{
    public function __construct(
        private readonly FileService $fileService,
        private readonly DocumentTemplateRepository $documentTemplateRepository,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function createAndSaveDocumentTemplateFromUploadedFile(UploadedFile $uploadedFile, AccountUser $accountUser): DocumentTemplate
    {
        $file = $this->fileService->saveFileFromUploadedFile(
            uploadedFile: $uploadedFile,
            account: $accountUser->getAccount(),
            public: false,
            accountUser: $accountUser
        );

        $documentTemplate = new DocumentTemplate();

        $documentTemplate
            ->setAccount($accountUser->getAccount())
            ->setCreatedByAccountUser($accountUser)
            ->setDocumentTemplateType(DocumentTemplateType::PROPERTY_OWNER_CORRESPONDENCE)
            ->setFile($file);

        $this->entityManager->persist(entity: $documentTemplate);

        $this->entityManager->flush();

        return $documentTemplate;
    }

    /**
     * @return DocumentTemplate[]
     */
    public function fetchDocumentTemplatesByAccount(Account $account): array
    {
        return $this->documentTemplateRepository->findByAccount(account: $account);
    }

    public function fetchDocumentTemplateById(Account $account, int $id): ?DocumentTemplate
    {
        return $this->documentTemplateRepository->findOneById(account: $account, id: $id);
    }
}
