<?php

declare(strict_types=1);

namespace App\Domain\Anonymize;

use App\Domain\Entity\AnonymizedDataBackup;
use App\Domain\Entity\AnonymizedDataBackupInterface;
use Doctrine\ORM\EntityManagerInterface;

class AnonymizeService
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function backupObject(AnonymizedDataBackupInterface $object): AnonymizedDataBackup
    {
        $anonymizedDataBackup = AnonymizedDataBackup::createFromObject(object: $object);

        $this->entityManager->persist(entity: $anonymizedDataBackup);
        $this->entityManager->flush();

        return $anonymizedDataBackup;
    }
}
