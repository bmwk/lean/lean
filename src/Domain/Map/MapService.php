<?php

declare(strict_types=1);

namespace App\Domain\Map;

use App\Domain\Entity\WmsLayer;
use App\Repository\WmsLayerRepository;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class MapService
{
    public function __construct(
        private readonly WmsLayerRepository $wmsLayerRepository,
        private readonly HttpClientInterface $httpClient
    ) {
    }

    /**
     * @return WmsLayer[]
     */
    public function parserWmsCapabilitiesFromUrl(string $url): array
    {
        $response = $this->httpClient->request(method: 'GET', url: $url);

        $responseInfo = $response->getInfo();

        $urlFragments = parse_url(url: $responseInfo['url']);

        $queryParameters = [];

        parse_str(string: $urlFragments['query'], result: $queryParameters);

        $responseXml = simplexml_load_string(data: $response->getContent());

        if ($responseXml === false) {
            throw new \LogicException();
        }

        if (
            isset($responseXml->Capability) === false
            || isset($responseXml->Capability->Layer) === false
            || isset($responseXml->Capability->Layer->Layer) === false
        ) {
            throw new \LogicException();
        }

        $parameter = [];

        foreach ($queryParameters as $queryParameterKey => $queryParameterValue) {
            $parameter[strtoupper(string: $queryParameterKey)] = $queryParameterValue;
        }

        $parameter['REQUEST'] = 'GetMap';

        $wmsLayers = [];

        foreach ($responseXml->Capability->Layer->Layer as $layer) {
            $wmsLayer = new WmsLayer();

            $wmsLayer
                ->setUrl($urlFragments['scheme'] . '://' . $urlFragments['host'] . $urlFragments['path'])
                ->setTitle((string) $layer->Title)
                ->setParameter(array_merge($parameter, ['LAYERS' => (string) $layer->Name]))
                ->setSortNumber(0);

            $wmsLayers[] = $wmsLayer;
        }

        return $wmsLayers;
    }

    public function fetchWmsLayerById(int $id): ?WmsLayer
    {
        return $this->wmsLayerRepository->find(id: $id);
    }
}
