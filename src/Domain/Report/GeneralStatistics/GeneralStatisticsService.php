<?php

declare(strict_types=1);

namespace App\Domain\Report\GeneralStatistics;

use App\Domain\Account\AccountUserService;
use App\Domain\Entity\Account;
use App\Domain\Entity\AccountUser\AccountUserType;
use App\Domain\Entity\Person\PersonFilter;
use App\Domain\Entity\Person\PersonType;
use App\Domain\Entity\Statistics\PersonStatistics;
use App\Domain\Entity\Statistics\UserStatistics;
use App\Domain\Entity\UserRegistration\UserRegistrationStatus;
use App\Domain\Entity\UserRegistration\UserRegistrationType;
use App\Domain\Person\PersonService;
use App\Domain\UserRegistration\UserInvitationService;
use App\Domain\UserRegistration\UserRegistrationService;

class GeneralStatisticsService
{
    public function __construct(
        private readonly AccountUserService $accountUserService,
        private readonly PersonService $personService,
        private readonly UserInvitationService $userInvitationService,
        private readonly UserRegistrationService $userRegistrationService,
    ) {
    }

    public function buildUserStatisticsByAccount(Account $account, bool $withFromSubAccounts): UserStatistics
    {
        $userStatistic = new UserStatistics();

        $userStatistic->setActiveInternalAccountUser(
            activeInternalAccountUser: $this->accountUserService->countAccountUsersByAccount(
                account: $account,
                withFromSubAccounts: false,
                enabled: true,
                accountUserTypes: [AccountUserType::INTERNAL]
            )
        );

        $userStatistic->setBlockedInternalAccountUser(
            blockedInternalAccountUser: $this->accountUserService->countAccountUsersByAccount(
                account: $account,
                withFromSubAccounts: false,
                enabled: false,
                accountUserTypes: [AccountUserType::INTERNAL]
            )
        );

        $userStatistic->setActivePropertyProvider(
            activePropertyProvider: $this->accountUserService->countAccountUsersByAccount(
                account: $account,
                withFromSubAccounts: true,
                enabled: true,
                accountUserTypes: [AccountUserType::PROPERTY_PROVIDER]
            )
        );

        $userStatistic->setInvitedPropertyProvider(
            invitedPropertyProvider: $this->userInvitationService->countUserInvitationsByAccount(
                account: $account,
                userRegistrationTypes: [UserRegistrationType::PROVIDER]
            )
        );

        $userStatistic->setRegisteredPropertyProvider(
            registeredPropertyProvider: $this->userRegistrationService->countUserRegistrationsByAccount(
                account: $account,
                userRegistrationTypes: [UserRegistrationType::PROVIDER],
                userRegistrationStatus: [UserRegistrationStatus::NOT_ACTIVE, UserRegistrationStatus::NOT_UNLOCKED]
            )
        );

        $userStatistic->setActivePropertySeeker(
            activePropertySeeker: $this->accountUserService->countAccountUsersByAccount(
                account: $account,
                withFromSubAccounts: true,
                enabled: true,
                accountUserTypes: [AccountUserType::PROPERTY_SEEKER]
            )
        );

        $userStatistic->setInvitedPropertySeeker(
            invitedPropertySeeker: $this->userInvitationService->countUserInvitationsByAccount(
                account: $account,
                userRegistrationTypes: [UserRegistrationType::SEEKER]
            )
        );

        $userStatistic->setRegisteredPropertySeeker(
            registeredPropertySeeker: $this->userRegistrationService->countUserRegistrationsByAccount(
                account: $account,
                userRegistrationTypes: [UserRegistrationType::SEEKER],
                userRegistrationStatus: [UserRegistrationStatus::NOT_ACTIVE, UserRegistrationStatus::NOT_UNLOCKED]
            )
        );

        return $userStatistic;
    }

    public function buildPersonStatisticsByAccount(Account $account, bool $withFromSubAccounts): PersonStatistics
    {
        $personStatistics = new PersonStatistics();

        $naturalPersonPersonFilter = new PersonFilter();

        $naturalPersonPersonFilter->setPersonTypes([PersonType::NATURAL_PERSON]);

        $personStatistics->setNaturalPerson(
            naturalPerson: $this->personService->countPersonsByAccount(
                account: $account,
                anonymized: false,
                personFilter: $naturalPersonPersonFilter
            )
        );

        $companyPersonFilter = new PersonFilter();
        $companyPersonFilter->setPersonTypes([PersonType::COMPANY]);

        $personStatistics->setCompany(
            company: $this->personService->countPersonsByAccount(
                account: $account,
                anonymized: false,
                personFilter: $companyPersonFilter
            )
        );

        $communePersonFilter = new PersonFilter();
        $communePersonFilter->setPersonTypes([PersonType::COMMUNE]);

        $personStatistics->setCommune(
            commune: $this->personService->countPersonsByAccount(
                account: $account,
                anonymized: false,
                personFilter: $communePersonFilter
            )
        );

        $propertyOwnerPersonFilter = new PersonFilter();
        $propertyOwnerPersonFilter->setUsageTypes(usageTypes: [0]);

        $personStatistics->setPropertyOwner(
            propertyOwner: $this->personService->countPersonsByAccount(
                account: $account,
                anonymized: false,
                personFilter: $propertyOwnerPersonFilter
            )
        );

        $propertyUserPersonFilter = new PersonFilter();
        $propertyUserPersonFilter->setUsageTypes(usageTypes: [1]);

        $personStatistics->setPropertyUser(
            propertyUser: $this->personService->countPersonsByAccount(
                account: $account,
                anonymized: false,
                personFilter: $propertyUserPersonFilter
            )
        );

        $lookingForPropertyRequestPersonFilter = new PersonFilter();
        $lookingForPropertyRequestPersonFilter->setUsageTypes(usageTypes: [4]);

        $personStatistics->setPropertySeeker(
            propertySeeker: $this->personService->countPersonsByAccount(
                account: $account,
                anonymized: false,
                personFilter: $lookingForPropertyRequestPersonFilter
            )
        );

        $propertyContactPersonPersonFilter = new PersonFilter();
        $propertyContactPersonPersonFilter->setUsageTypes(usageTypes: [2]);

        $personStatistics->setPropertyContactPerson(
            propertyContactPerson: $this->personService->countPersonsByAccount(
                account: $account,
                anonymized: false,
                personFilter: $propertyContactPersonPersonFilter
            )
        );

        $propertyContactMarketingPersonPersonFilter = new PersonFilter();
        $propertyContactMarketingPersonPersonFilter->setUsageTypes(usageTypes: [3]);

        $personStatistics->setPropertyMarketingContactPerson(
            propertyMarketingContactPerson: $this->personService->countPersonsByAccount(
                account: $account,
                anonymized: false,
                personFilter: $propertyContactMarketingPersonPersonFilter
            )
        );

        return $personStatistics;
    }
}
