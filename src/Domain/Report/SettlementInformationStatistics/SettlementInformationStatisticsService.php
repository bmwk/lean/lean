<?php

declare(strict_types=1);

namespace App\Domain\Report\SettlementInformationStatistics;

use App\Domain\Entity\Account;
use App\Domain\Entity\ForwardingResponse;
use App\Domain\Entity\LookingForPropertyRequest\MatchingStatistics;
use App\Domain\Entity\Statistics\SettlementStatistics;
use App\Domain\Entity\TaskStatus;
use App\Domain\LookingForPropertyRequest\LookingForPropertyRequestService;
use App\Domain\LookingForPropertyRequest\Matching\MatchingService;
use App\Domain\SettlementConcept\SettlementConceptMatchingService;
use App\Domain\SettlementConcept\SettlementConceptService;
use App\Repository\LookingForPropertyRequest\MatchingTaskRepository;

class SettlementInformationStatisticsService
{
    public function __construct(
        private readonly LookingForPropertyRequestService $lookingForPropertyRequestService,
        private readonly SettlementConceptService $settlementConceptService,
        private readonly MatchingService $matchingService,
        private readonly MatchingTaskRepository $matchingTaskRepository,
        private readonly SettlementConceptMatchingService $settlementConceptMatchingService
    ) {
    }

    public function buildMatchingStatisticsByAccount(Account $account): ?MatchingStatistics
    {
        $lastMatchingTask = $this->matchingTaskRepository->findOneBy(criteria: ['taskStatus' => TaskStatus::DONE], orderBy: ['updatedAt' => 'DESC']);

        if ($lastMatchingTask === null) {
            return null;
        }

        $matchingStatistics = new MatchingStatistics();

        if ($lastMatchingTask->getUpdatedAt() === null) {
            $lastMatchingDate = $lastMatchingTask->getCreatedAt();
        } else {
            $lastMatchingDate = $lastMatchingTask->getUpdatedAt();
        }

        $matchingStatistics
            ->setLastMatchingDate($lastMatchingDate)
            ->setExposeForwardings(
                $this->lookingForPropertyRequestService->countLookingForPropertyRequestExposeForwardingsByAccount(account: $account)
                + $this->settlementConceptService->countSettlementConceptExposeForwardingsByAccount(account: $account)
            )
            ->setAverageNumberOfResultsPerMatching($this->countAverageNumberOfResultsPerMatchingByAccount(account: $account));

        return $matchingStatistics;
    }

    public function buildSettlementStatisticsByAccount(Account $account, bool $withFromSubAccounts): SettlementStatistics
    {
        $settlementStatistics = new SettlementStatistics();

        $settlementStatistics->setActiveLookingForPropertyRequest(
            activeLookingForPropertyRequest: $this->lookingForPropertyRequestService->countLookingForPropertyRequestsByAccount(
                account: $account,
                withFromSubAccounts: $withFromSubAccounts,
                archived: false,
                active: true
            )
        );

        $settlementStatistics->setInactiveLookingForPropertyRequest(
            inactiveLookingForPropertyRequest: $this->lookingForPropertyRequestService->countLookingForPropertyRequestsByAccount(
                account: $account,
                withFromSubAccounts: $withFromSubAccounts,
                archived: false,
                active: false
            )
        );

        $settlementStatistics->setArchivedLookingForPropertyRequest(
            archivedLookingForPropertyRequest: $this->lookingForPropertyRequestService->countLookingForPropertyRequestsByAccount(
                account: $account,
                withFromSubAccounts: $withFromSubAccounts,
                archived: true
            )
        );

        $settlementStatistics->setMatchingTask(
            matchingTask: $this->matchingService->countMatchingTasksByAccountAndTaskStatus(account: $account, taskStatus: TaskStatus::DONE)
        );

        $settlementStatistics->setAverageNumberOfResultsPerMatching(
            averageNumberOfResultsPerMatching: $this->countAverageNumberOfResultsPerMatchingByAccount(account: $account)
        );

        $settlementStatistics->setLookingForPropertyRequestExposeForwarding(
            lookingForPropertyRequestExposeForwarding: $this->lookingForPropertyRequestService->countLookingForPropertyRequestExposeForwardingsByAccount(
                account: $account
            )
        );

        $settlementStatistics->setSettlementConceptExposeForwarding(
            settlementConceptExposeForwarding: $this->settlementConceptService->countSettlementConceptExposeForwardingsByAccount(account: $account)
        );

        $settlementStatistics->setSuccessfulSettlement(
            successfulSettlement: ($this->lookingForPropertyRequestService->countLookingForPropertyRequestExposeForwardingsByAccountAndForwardingResponse(
                account: $account,
                forwardingResponse: ForwardingResponse::CONTRACT_CONCLUDED
            ) + $this->settlementConceptService->countSettlementConceptExposeForwardingsByAccountAndForwardingResponse(
                    account: $account,
                    forwardingResponse: ForwardingResponse::CONTRACT_CONCLUDED
                )
            )
        );

        return $settlementStatistics;
    }

    public function countAverageNumberOfResultsPerMatchingByAccount(Account $account): int
    {
        $matchingResultsStatistics = $this->matchingService->countMatchingResultsGroupedByBuildingUnitAndEliminated(account: $account);
        $settlementConceptMatchingResultsStatistics = $this->settlementConceptMatchingService->countSettlementConceptMatchingResultsGroupedByBuildingUnitAndEliminated(account: $account);

        $matchingResults = [];

        foreach ($matchingResultsStatistics as $matchingResultStatistic) {
            $buildingUnitId = $matchingResultStatistic['buildingUnitId'];

            if (isset($matchingResults[$buildingUnitId]) === true) {
                if ($matchingResultStatistic['eliminated'] === false) {
                    $matchingResults[$buildingUnitId] = $matchingResults[$buildingUnitId] + $matchingResultStatistic['matches'];
                } else {
                    $matchingResults[$buildingUnitId] = $matchingResults[$buildingUnitId];
                }
            } else {
                if ($matchingResultStatistic['eliminated'] === false) {
                    $matchingResults[$buildingUnitId] = $matchingResultStatistic['matches'];
                } else {
                    $matchingResults[$buildingUnitId] = 0;
                }
            }
        }

        foreach ($settlementConceptMatchingResultsStatistics as $settlementConceptMatchingResultStatistic) {
            $buildingUnitId = $settlementConceptMatchingResultStatistic['buildingUnitId'];

            if (isset($matchingResults[$buildingUnitId]) === true) {
                if ($settlementConceptMatchingResultStatistic['eliminated'] === false) {
                    $matchingResults[$buildingUnitId] = $matchingResults[$buildingUnitId] + $settlementConceptMatchingResultStatistic['matches'];
                } else {
                    $matchingResults[$buildingUnitId] = $matchingResults[$buildingUnitId];
                }
            } else {
                if ($settlementConceptMatchingResultStatistic['eliminated'] === false) {
                    $matchingResults[$buildingUnitId] = $settlementConceptMatchingResultStatistic['matches'];
                } else {
                    $matchingResults[$buildingUnitId] = 0;
                }
            }
        }

        if (count($matchingResults) === 0) {
            return 0;
        }

        return (int) ceil(num: array_sum($matchingResults) / count($matchingResults));
    }
}
