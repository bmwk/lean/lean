<?php

declare(strict_types=1);

namespace App\Domain\Report\PropertyVacancy;

use App\Domain\Entity\Account;
use App\Domain\Entity\Classification\IndustryClassification;
use App\Domain\Entity\LocationCategory;
use App\Domain\Entity\Report\PropertyVacancy\GeneralPropertyVacancyData;
use App\Domain\Entity\Report\PropertyVacancy\IndividualPropertyVacancyData;
use App\Domain\Entity\Report\PropertyVacancy\PropertyVacancyDataFilter;
use App\Domain\Entity\Report\PropertyVacancy\QuotaType;
use App\Repository\Property\BuildingUnitRepository;

class PropertyVacancyDataService
{
    public function __construct(
        private readonly BuildingUnitRepository $buildingUnitRepository
    ) {
    }

    public function fetchGeneralPropertyVacancyDataByAccount(Account $account, bool $withFromSubAccounts): GeneralPropertyVacancyData
    {
        $generalPropertyVacancyData = new GeneralPropertyVacancyData();

        $generalPropertyVacancyData
            ->setTotalPropertyVacancy($this->fetchTotalPropertyVacancyData(account: $account, withFromSubAccounts: $withFromSubAccounts))
            ->setGroundFloorPropertyVacancy($this->fetchGroundFloorPropertyVacancy(account: $account, withFromSubAccounts: $withFromSubAccounts))
            ->setALocationPropertyVacancy($this->fetchALocationPropertyVacancy(account: $account, withFromSubAccounts: $withFromSubAccounts))
            ->setBLocationPropertyVacancy($this->fetchBLocationPropertyVacancy(account: $account, withFromSubAccounts: $withFromSubAccounts))
            ->setLongestPropertyVacancyInDays($this->fetchLongestPropertyVacancy(account: $account, withFromSubAccounts: $withFromSubAccounts))
            ->setShortestPropertyVacancyInDays($this->fetchShortestPropertyVacancy(account: $account, withFromSubAccounts: $withFromSubAccounts))
            ->setAveragePropertyVacancyInDays($this->fetchAveragePropertyVacancy(account: $account, withFromSubAccounts: $withFromSubAccounts));

        return $generalPropertyVacancyData;
    }

    public function fetchIndividualPropertyDataByFilterAndAccount(
        Account $account,
        bool $withFromSubAccounts,
        PropertyVacancyDataFilter $propertyVacancyDataFilter,
        ?IndustryClassification $industryClassification
    ): IndividualPropertyVacancyData {
        $propertyVacancyDataFilter->setIndustryClassification($industryClassification);

        $propertyVacancyData = new QuotaType();

        $total = $this->buildingUnitRepository->countByPropertyVacancyDataFilter(
            account: $account,
            withFromSubAccounts: $withFromSubAccounts,
            archived: false,
            propertyVacancyDataFilter: $propertyVacancyDataFilter
        );

        $empty = $this->buildingUnitRepository->countByPropertyVacancyDataFilter(
            account: $account,
            withFromSubAccounts: $withFromSubAccounts,
            archived: false,
            objectIsEmpty: true,
            propertyVacancyDataFilter: $propertyVacancyDataFilter,
        );

        $propertyVacancyData
            ->setEmpty($empty)
            ->setTotal($total);

        $individualPropertyVacancyData = new IndividualPropertyVacancyData();

        $individualPropertyVacancyData
            ->setPropertyVacancy($propertyVacancyData)
            ->setTitle('Leerstandsquote')
            ->setSubTitle(
                $this->generateSubTitleByPropertyVacancyDataFilter(propertyVacancyDataFilter: $propertyVacancyDataFilter)
            );

        return $individualPropertyVacancyData;
    }

    public function fetchTotalPropertyVacancyData(Account $account, bool $withFromSubAccounts): QuotaType
    {
        $totalPropertyVacancyData = new QuotaType();

        $totalPropertyVacancyData
            ->setTotal($this->buildingUnitRepository->countByAccount(account: $account, withFromSubAccounts: $withFromSubAccounts, archived: false))
            ->setEmpty($this->buildingUnitRepository->countByAccount(account: $account, withFromSubAccounts: $withFromSubAccounts, archived: false, objectIsEmpty: true));

        return $totalPropertyVacancyData;
    }

    public function fetchGroundFloorPropertyVacancy(Account $account, bool $withFromSubAccounts): QuotaType
    {
        $groundLevelPropertyVacancyData = new QuotaType();

        $groundLevelPropertyVacancyData
            ->setTotal($this->buildingUnitRepository->countByAtFloorGround(account: $account, withFromSubAccounts: $withFromSubAccounts, archived: false))
            ->setEmpty($this->buildingUnitRepository->countByAtFloorGround(account: $account, withFromSubAccounts: $withFromSubAccounts, archived: false, objectIsEmpty: true));

        return $groundLevelPropertyVacancyData;
    }

    public function fetchALocationPropertyVacancy(Account $account, bool $withFromSubAccounts): QuotaType
    {
        $aLocationPropertyVacancyData = new QuotaType();

        $aLocationPropertyVacancyData
            ->setTotal(
                $this->buildingUnitRepository->countByLocationCategories(
                    account: $account,
                    withFromSubAccounts: $withFromSubAccounts,
                    locationCategories: [LocationCategory::ONE_A_LOCATION, LocationCategory::TWO_A_LOCATION],
                    archived: false
               )
            )
            ->setEmpty(
                $this->buildingUnitRepository->countByLocationCategories(
                    account: $account,
                    withFromSubAccounts: $withFromSubAccounts,
                    locationCategories: [LocationCategory::ONE_A_LOCATION, LocationCategory::TWO_A_LOCATION],
                    archived: false,
                    objectIsEmpty: true
                )
            );

        return $aLocationPropertyVacancyData;
    }

    public function fetchBLocationPropertyVacancy(Account $account, bool $withFromSubAccounts): QuotaType
    {
        $bLocationPropertyVacancyData = new QuotaType();

        $bLocationPropertyVacancyData
            ->setTotal(
                $this->buildingUnitRepository->countByLocationCategories(
                    account: $account,
                    withFromSubAccounts: $withFromSubAccounts,
                    locationCategories: [LocationCategory::ONE_B_LOCATION, LocationCategory::TWO_B_LOCATION],
                    archived: false
                )
            )
            ->setEmpty(
                $this->buildingUnitRepository->countByLocationCategories(
                    account: $account,
                    withFromSubAccounts: $withFromSubAccounts,
                    locationCategories: [LocationCategory::ONE_B_LOCATION, LocationCategory::TWO_B_LOCATION],
                    archived: false,
                    objectIsEmpty: true
                )
            );

        return $bLocationPropertyVacancyData;
    }

    public function fetchLongestPropertyVacancy(Account $account, bool $withFromSubAccounts): ?int
    {
        $oldestPropertyVacencyBuildingUnit = $this->buildingUnitRepository->findOneByLongestTimeEmpty(account: $account, withFromSubAccounts: $withFromSubAccounts);

        if ($oldestPropertyVacencyBuildingUnit === null) {
            return null;
        } else {
          return date_diff(new \DateTimeImmutable(), $oldestPropertyVacencyBuildingUnit->getObjectIsEmptySince())->days;
        }
    }

    public function fetchShortestPropertyVacancy(Account $account, bool $withFromSubAccounts): ?int
    {
        $shortestPropertyVacencyBuildingUnit = $this->buildingUnitRepository->findOneByShortestTimeEmpty(account: $account, withFromSubAccounts: $withFromSubAccounts);

        if ($shortestPropertyVacencyBuildingUnit === null) {
            return null;
        } else {
            return date_diff(new \DateTimeImmutable(), $shortestPropertyVacencyBuildingUnit->getObjectIsEmptySince())->days;
        }
    }

    public function fetchAveragePropertyVacancy(Account $account, bool $withFromSubAccounts): ?int
    {
        return $this->buildingUnitRepository->fetchAverageTimeEmpty(account: $account, withFromSubAccounts: $withFromSubAccounts);
    }

    public function generateSubTitleByPropertyVacancyDataFilter(PropertyVacancyDataFilter $propertyVacancyDataFilter): string
    {
        $subTitles = [];

        if ($propertyVacancyDataFilter->getOnlyGroundFloor() === true) {
            $subTitles[] = 'in Erdgeschoss-Lagen';
        }

        if ($propertyVacancyDataFilter->getQuarterPlace() !== null) {
            $subTitles[] = 'im Stadt-/Ortsteil "' . $propertyVacancyDataFilter->getQuarterPlace()->getPlaceName(). '"';
        }

        if (empty($propertyVacancyDataFilter->getVacancyReasons()) === false) {

            if (count($propertyVacancyDataFilter->getVacancyReasons()) > 1) {
                $subTitleVacancyReasonIntro = 'mit den Leerstandsgründen ';
            } else {
                $subTitleVacancyReasonIntro = 'mit dem Leerstandsgrund ';
            }

            foreach ($propertyVacancyDataFilter->getVacancyReasons() as $vacancyReason) {
                $subTitleVacancyReasons[] = $vacancyReason->getName();
            }

            $subTitleVacancyReason  = implode(separator: ' + ', array: $subTitleVacancyReasons);

            $subTitles[] = $subTitleVacancyReasonIntro . '"' . $subTitleVacancyReason . '"';
        }

        if (empty($propertyVacancyDataFilter->getLocationCategories()) === false) {
            if (count($propertyVacancyDataFilter->getLocationCategories()) > 1) {
                $subTitleLocationCategoryIntro = 'in den Geschäftslagen ';
            } else {
                $subTitleLocationCategoryIntro = 'in ';
            }

            foreach ($propertyVacancyDataFilter->getLocationCategories() as $locationCategory) {
                $subTitleLocationCategories[] =  $locationCategory->getName();
            }

            $subTitleLocationCategory  = implode(separator: ' + ', array: $subTitleLocationCategories);

            $subTitles[] = $subTitleLocationCategoryIntro . '"' . $subTitleLocationCategory . '"';
        }

        if ($propertyVacancyDataFilter->getIndustryClassification() !== null) {

            $subTitleIndustryClassificationIntro = 'im Segment ';

            if ($propertyVacancyDataFilter->getIndustryClassification()->getLevelThree() !== null) {
                $subTitleIndustryClassification = $propertyVacancyDataFilter->getIndustryClassification()->getParent()->getParent()->getName() .' » '.$propertyVacancyDataFilter->getIndustryClassification()->getParent()->getName() .' » '.$propertyVacancyDataFilter->getIndustryClassification()->getName();
            } elseif ($propertyVacancyDataFilter->getIndustryClassification()->getLevelTwo() !== null) {
                $subTitleIndustryClassification = $propertyVacancyDataFilter->getIndustryClassification()->getParent()->getName() .' » '.$propertyVacancyDataFilter->getIndustryClassification()->getName();
            } else {
                $subTitleIndustryClassification = $propertyVacancyDataFilter->getIndustryClassification()->getName();
            }

            $subTitles[] = $subTitleIndustryClassificationIntro . '"' . $subTitleIndustryClassification . '"';
        }

        return implode(separator: ', ', array: $subTitles);
    }
}
