<?php

declare(strict_types=1);

namespace App\Domain\OpenImmoImporter;

use App\Domain\Entity\OpenImmoImporter\ImportStatus;
use App\Domain\Entity\OpenImmoImporter\OpenImmoImport;
use App\Domain\Entity\OpenImmoImporter\OpenImmoUpload;
use App\Domain\Filesystem\FilesystemService;
use App\Domain\OpenImmoImporter\UploadFile\UploadFileXml;
use App\Message\ExecuteOpenImmoImport;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\MessageBusInterface;

class OpenImmoImportCreationService
{
    private readonly \SplFileInfo $importsDirectory;

    public function __construct(
        string $openImmoImportsPath,
        private readonly FilesystemService $filesystemService,
        private readonly EntityManagerInterface $entityManager,
        private readonly MessageBusInterface $messageBus
    ) {
        $this->importsDirectory = new \SplFileInfo(filename: $openImmoImportsPath);
    }

    public function createAndSaveOpenImmoImportFromOpenImmoUploadAndUploadFileXml(
        OpenImmoUpload $openImmoUpload,
        UploadFileXml $uploadFileXml,
        ?\SplFileInfo $sourceDirectory
    ): OpenImmoImport {
        $openImmoImport = new OpenImmoImport();

        $openImmoImport
            ->setAccount($openImmoUpload->getAccount())
            ->setImportStatus(ImportStatus::IN_CREATION)
            ->setFilename($uploadFileXml->getFilename())
            ->setOpenImmoUpload($openImmoUpload);

        $this->entityManager->persist(entity: $openImmoImport);

        $this->entityManager->flush();

        $importDirectory = $this->makeImportDirectory(openImmoImport: $openImmoImport);

        if ($sourceDirectory !== null) {
            $this->filesystemService->mirror(originDir: $sourceDirectory->getRealPath(), targetDir: $importDirectory->getRealPath());
        } else {
            $this->filesystemService->copy(
                originFile: $uploadFileXml->getPath() . '/' . $uploadFileXml->getFilename(),
                targetFile: $importDirectory->getRealPath() . '/' . $uploadFileXml->getFilename());
        }

        $openImmoImport->setImportStatus(ImportStatus::CREATED);

        $this->entityManager->flush();

        return $openImmoImport;
    }

    public function dispatchExecuteOpenImmoImportMessage(OpenImmoImport $openImmoImport): void
    {
        $this->messageBus->dispatch(new ExecuteOpenImmoImport(openImmoImport: $openImmoImport));
    }

    private function makeImportDirectory(OpenImmoImport $openImmoImport): \SplFileInfo
    {
        $targetDirectory = $this->filesystemService->makeDirectory(
            directoryName: (string)$openImmoImport->getAccount()->getId(),
            targetDirectory: $this->importsDirectory
        );

        return $this->filesystemService->makeDirectory(directoryName: (string)$openImmoImport->getId(), targetDirectory: $targetDirectory);
    }
}
