<?php

declare(strict_types=1);

namespace App\Domain\OpenImmoImporter\UploadFile;

class UploadFileZip extends AbstractUploadFile
{
    final public function extract(\SplFileInfo $destinationDirectory): bool
    {
        if ($destinationDirectory->isDir() !== true) {
            throw new \RuntimeException(message: 'The given destination path is not a directory');
        }

        if ($destinationDirectory->isWritable() !== true) {
            throw new \RuntimeException(message: 'The destination path is not writable');
        }

        $zipArchive = new \ZipArchive();

        if ($zipArchive->open($this->getFile()->getRealPath()) !== true) {
            return false;
        }

        $zipArchive->extractTo($destinationDirectory->getRealPath());

        $zipArchive->close();

        return true;
    }

    /**
     * @return string[]
     */
    final protected function getFileMimeTypes(): array
    {
        return ['application/zip'];
    }
}
