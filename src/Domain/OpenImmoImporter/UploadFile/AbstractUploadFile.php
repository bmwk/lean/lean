<?php

declare(strict_types=1);

namespace App\Domain\OpenImmoImporter\UploadFile;

use Symfony\Component\HttpFoundation\File\File;

abstract class AbstractUploadFile
{
    private readonly string $filename;

    private readonly int $size;

    private readonly string $path;

    private readonly \DateTimeImmutable $fileCreatedAt;

    final public function __construct(
        private readonly File $file
    ) {
        if (empty($this->getFileMimeTypes())) {
            throw new \RuntimeException(message: 'file mine type is nod defined');
        }

        if ($this->file->isFile() !== true) {
            throw new \RuntimeException(message: 'The given destination is not a file');
        }

        if ($this->checkFileMimeTypes() === false) {
            throw new \RuntimeException(message: 'Wrong file mine type');
        }

        $this->filename = $this->file->getFilename();
        $this->size = $this->file->getSize();
        $this->path = $this->file->getPath();
        $this->fileCreatedAt = new \DateTimeImmutable();

        $this->fileCreatedAt->setTimestamp($this->file->getMTime());
    }

    final public function getFilename(): string
    {
        return $this->filename;
    }

    final public function getFileInfo(): \SplFileInfo
    {
        return $this->file->getFileInfo();
    }

    final public function getSize(): int
    {
        return $this->size;
    }

    final public function getPath(): string
    {
        return $this->path;
    }

    final public function getFileCreatedAt(): \DateTimeImmutable
    {
        return $this->fileCreatedAt;
    }

    final protected function getFile(): File
    {
        return $this->file;
    }

    private function checkFileMimeTypes(): bool
    {
        return in_array(needle: $this->file->getMimeType() , haystack: $this->getFileMimeTypes());
    }

    /**
     * @return string[]
     */
    abstract protected function getFileMimeTypes(): array;
}
