<?php

declare(strict_types=1);

namespace App\Domain\OpenImmoImporter\UploadFile;

class UploadFileXml extends AbstractUploadFile
{
    final public function isValid(): bool
    {
        $xmlReader = \XMLReader::open($this->getFile()->getRealPath());

        $xmlReader->setParserProperty(\XMLReader::VALIDATE, true);

        return $xmlReader->isValid();
    }

    final public function isXmlParsable(): bool
    {
        if (simplexml_load_file($this->getFile()->getRealPath()) === false) {
            return false;
        }

        return true;
    }

    final public function parse(): \SimpleXMLElement
    {
        $xml = simplexml_load_file($this->getFile()->getRealPath());

        if ($xml === false) {
            throw new \RuntimeException(message: 'XML is invalid');
        }

        return $xml;
    }

    /**
     * @return string[]
     */
    final protected function getFileMimeTypes(): array
    {
        return ['application/xml', 'text/xml'];
    }
}
