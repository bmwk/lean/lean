<?php

declare(strict_types=1);

namespace App\Domain\OpenImmoImporter;

use App\Domain\Account\AccountService;
use App\Domain\Entity\Account;
use App\Domain\Entity\OpenImmoImporter\OpenImmoUpload;
use App\Domain\Filesystem\FilesystemService;
use App\Domain\OpenImmoImporter\UploadFile\AbstractUploadFile;
use App\Domain\OpenImmoImporter\UploadFile\UploadFileXml;
use App\Domain\OpenImmoImporter\UploadFile\UploadFileZip;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Uid\Uuid;

class OpenImmoUploadCaptureService
{
    private readonly \SplFileInfo $uploadsDirectory;

    private readonly \SplFileInfo $tempDirectory;

    public function __construct(
        string $openImmoUploadsPath,
        string $openImmoTempPath,
        private readonly AccountService $accountService,
        private readonly OpenImmoImportCreationService $openImmoImportCreationService,
        private readonly FilesystemService $filesystemService,
        private readonly EntityManagerInterface $entityManager
    ) {
        $this->uploadsDirectory = new \SplFileInfo(filename: $openImmoUploadsPath);

        $this->filesystemService->checkIsDirectoryAndPermissions(targetDirectory: $this->uploadsDirectory);

        $this->tempDirectory = new \SplFileInfo(filename: $openImmoTempPath);

        $this->filesystemService->checkIsDirectoryAndPermissions(targetDirectory: $this->tempDirectory);
    }

    public function captureAndProcessUploadedFilesFromAllAccounts(): void
    {
        foreach ($this->accountService->fetchAllAccounts() as $account) {
            $accountUploadDirectory = new \SplFileInfo(filename: $this->uploadsDirectory->getRealPath() . '/' . $account->getId());

            if ($accountUploadDirectory->isDir() === true) {
                $this->captureAndProcessUploadedFilesFromAccount(account: $account);
            }
        }
    }

    public function captureAndProcessUploadedFilesFromAccount(Account $account): void
    {
        $accountUploadDirectory = new \SplFileInfo(filename: $this->uploadsDirectory->getRealPath() . '/' . $account->getId());

        $this->filesystemService->checkIsDirectoryAndPermissions(targetDirectory: $accountUploadDirectory);

        $finder = Finder::create()->files()->name(patterns: '/^(?!\.in\.)(.+(\.zip|\.xml))$/i')->in(dirs: $accountUploadDirectory->getRealPath());

        foreach ($finder as $uploadedFile) {
            if ($uploadedFile->isFile() !== true) {
                continue;
            }

            $file = new File($uploadedFile->getRealPath());

            $fileMineType = $file->getMimeType();

            if ($fileMineType === 'application/zip') {
                $this->processUploadedFile(uploadFile: new UploadFileZip($file), account: $account);
            }

            if (in_array(needle: $fileMineType, haystack: ['application/xml', 'text/xml']) === true) {
                $this->processUploadedFile(uploadFile: new UploadFileXml($file), account: $account);
            }
        }
    }

    private function processUploadedFile(AbstractUploadFile $uploadFile, Account $account): void
    {
        $openImmoUpload = OpenImmoUpload::createFromAccountAndSplFileInfo(account: $account, splFileInfo: $uploadFile->getFileInfo());

        $openImmoUpload->setCapturedAt(new \DateTimeImmutable());

        $this->entityManager->persist(entity: $openImmoUpload);
        $this->entityManager->flush();

        if ($uploadFile instanceof UploadFileXml) {
            $this->processXmlFile(uploadFileXml: $uploadFile, openImmoUpload: $openImmoUpload, sourceDirectory: null);
        }

        if ($uploadFile instanceof UploadFileZip) {
            $this->processZipFile(uploadFileZip: $uploadFile, openImmoUpload: $openImmoUpload);
        }

        $this->removeUploadedFile(openImmoUpload: $openImmoUpload);
        $this->removeUploadTempDirectory(openImmoUpload: $openImmoUpload);
    }

    private function processXmlFile(UploadFileXml $uploadFileXml, OpenImmoUpload $openImmoUpload, ?\SplFileInfo $sourceDirectory): void
    {
        $openImmoImport = $this->openImmoImportCreationService->createAndSaveOpenImmoImportFromOpenImmoUploadAndUploadFileXml(
            openImmoUpload: $openImmoUpload,
            uploadFileXml: $uploadFileXml,
            sourceDirectory: $sourceDirectory
        );

        $this->openImmoImportCreationService->dispatchExecuteOpenImmoImportMessage(openImmoImport: $openImmoImport);
    }

    private function processZipFile(UploadFileZip $uploadFileZip, OpenImmoUpload $openImmoUpload): void
    {
        $tempDirectory = $this->makeUploadTempDirectory(openImmoUpload: $openImmoUpload);

        $uploadFileZip->extract(destinationDirectory: $tempDirectory);

        $finder = Finder::create()->files()->name(patterns: '/(\.zip)$/i')->in(dirs: $tempDirectory->getRealPath());

        foreach ($finder as $fileInfo) {
            if (empty($fileInfo->getRealPath())) {
                continue;
            }

            $file = new File(path: $fileInfo->getRealPath());

            $fileMineType = $file->getMimeType();

            if ($fileMineType === 'application/zip') {
                $this->processZipFile(uploadFileZip: new UploadFileZip(file: $file), openImmoUpload: $openImmoUpload);
            }
        }

        $finder = Finder::create()->files()->name(patterns: '/(\.xml)$/i')->in(dirs: $tempDirectory->getRealPath());

        foreach ($finder as $fileInfo) {
            if (empty($fileInfo->getRealPath())) {
                continue;
            }

            $file = new File(path: $fileInfo->getRealPath());

            $fileMineType = $file->getMimeType();

            if (in_array(needle: $fileMineType, haystack: ['application/xml', 'text/xml']) === true) {
                $this->processXmlFile(
                    uploadFileXml: new UploadFileXml(file: $file),
                    openImmoUpload: $openImmoUpload,
                    sourceDirectory: $tempDirectory
                );
            }
        }
    }

    private function removeUploadedFile(OpenImmoUpload $openImmoUpload): void
    {
        $filePath = $this->uploadsDirectory->getRealPath() . '/' . $openImmoUpload->getAccount()->getId() . '/' . $openImmoUpload->getFilename();

        $targetDirectory = new \SplFileInfo(filename: $filePath);

        if ($targetDirectory->isFile() === true) {
            $this->filesystemService->remove(files: $targetDirectory->getRealPath());
        }
    }

    private function removeUploadTempDirectory(OpenImmoUpload $openImmoUpload): void
    {
        $targetDirectory = new \SplFileInfo(filename:  $this->tempDirectory->getRealPath() . '/' . $openImmoUpload->getId());

        if ($targetDirectory->isDir() === true) {
            $this->filesystemService->remove(files: $targetDirectory->getRealPath());
        }
    }

    private function makeUploadTempDirectory(OpenImmoUpload $openImmoUpload): \SplFileInfo
    {
        $targetDirectory = $this->filesystemService->makeDirectory(directoryName: (string)$openImmoUpload->getId(), targetDirectory: $this->tempDirectory);

        return $this->filesystemService->makeDirectory(directoryName: Uuid::v6()->toRfc4122(), targetDirectory: $targetDirectory);
    }
}
