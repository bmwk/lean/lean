<?php

declare(strict_types=1);

namespace App\Domain\OpenImmoImporter;

use App\Domain\Entity\Account;
use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\AccountUser\AccountUserType;
use App\Domain\Entity\OpenImmoImporter\AccountOpenImmoFtpUser;
use App\FtpUser\FtpUserService;
use App\Repository\OpenImmoImporter\AccountOpenImmoFtpUserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AccountOpenImmoFtpUserService
{
    public function __construct(
        private readonly AccountOpenImmoFtpUserRepository $accountOpenImmoFtpUserRepository,
        private readonly FtpUserService $ftpUserService,
        private readonly UserPasswordHasherInterface $passwordHasher,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function fetchAccountOpenImmoFtpUserByAccount(Account $account): ?AccountOpenImmoFtpUser
    {
        return $this->accountOpenImmoFtpUserRepository->findOneByAccount(account: $account);
    }

    public function createAndSaveAccountOpenImmoFtpUser(
        Account $account,
        AccountUser $creatorAccountUser,
        string $plainPassword
    ): AccountOpenImmoFtpUser {
        $accountOpenImmoFtpUser = $this->fetchAccountOpenImmoFtpUserByAccount(account: $account);

        if ($accountOpenImmoFtpUser !== null) {
            throw new \RuntimeException(message: 'AccountOpenImmoFtpUser already exists');
        }

        $ftpUserId = $this->buildFtpUserId();

        $this->ftpUserService->createFtpUser(userid: $ftpUserId, plainPassword: $plainPassword, homedirPath: (string) $account->getId());

        $accountOpenImmoFtpUser = new AccountOpenImmoFtpUser();

        $ftpAccountUser = new AccountUser();

        $ftpAccountUser
            ->setAccount($account)
            ->setAccountUserType(AccountUserType::FTP_USER)
            ->setFullName('OpenImmo Ftp User')
            ->setEmail($account->getEmail())
            ->setUsername($ftpUserId)
            ->setRoles([])
            ->setEnabled(true)
            ->setDeleted(false)
            ->setAnonymized(false)
            ->setPassword($this->passwordHasher->hashPassword(user: $ftpAccountUser, plainPassword: bin2hex(string: random_bytes(length: 20))));

        $this->entityManager->persist(entity: $ftpAccountUser);

        $accountOpenImmoFtpUser
            ->setFtpUsername($ftpUserId)
            ->setAccount($account)
            ->setAccountUser($ftpAccountUser)
            ->setCreatedByAccountUser($creatorAccountUser);

        $this->entityManager->persist(entity: $accountOpenImmoFtpUser);

        $this->entityManager->flush();

        return $accountOpenImmoFtpUser;
    }

    public function changeFtpUserPassword(AccountOpenImmoFtpUser $accountOpenImmoFtpUser, string $plainPassword): void
    {
        if ($this->ftpUserService->checkFtpUserExists(userid: $accountOpenImmoFtpUser->getFtpUsername()) === false) {
            throw new \RuntimeException(message: 'ftpUser does not exists');
        }

        $this->ftpUserService->changeFtpUserPassword(userid: $accountOpenImmoFtpUser->getFtpUsername(), plainPassword: $plainPassword);
    }

    public static function generateRandomString(int $length): string
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-_';
        $charactersLength = strlen(string: $characters);
        $randomString = '';

        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(min: 0, max: $charactersLength - 1)];
        }

        return $randomString;
    }

    private function buildFtpUserId(): string
    {
        $userId = self::generateRandomString(length: 4) . '#' . self::generateRandomString(length: 6);

        if ($this->ftpUserService->checkFtpUserExists(userid: $userId) === true) {
            return $this->buildFtpUserId();
        }

        return $userId;
    }
}
