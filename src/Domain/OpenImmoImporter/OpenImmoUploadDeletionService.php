<?php

declare(strict_types=1);

namespace App\Domain\OpenImmoImporter;

use App\Domain\Entity\Account;
use App\Domain\Entity\OpenImmoImporter\OpenImmoUpload;
use App\Repository\OpenImmoImporter\OpenImmoUploadRepository;
use Doctrine\ORM\EntityManagerInterface;

class OpenImmoUploadDeletionService
{
    public function __construct(
        private readonly OpenImmoUploadRepository $openImmoUploadRepository,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function deleteOpenImmoUpload(OpenImmoUpload $openImmoUpload): void
    {
        $this->hardDeleteOpenImmoUpload(openImmoUpload: $openImmoUpload);
    }

    public function deleteOpenImmoUploadsByAccount(Account $account): void
    {
        $this->hardDeleteOpenImmoUploadsByAccount(account: $account);
    }

    private function hardDeleteOpenImmoUpload(OpenImmoUpload $openImmoUpload): void
    {
        foreach ($openImmoUpload->getOpenImmoImports() as $openImmoImport) {
            $openImmoImport->setOpenImmoUpload(openImmoUpload: null);
        }

        $this->entityManager->flush();
    }

    private function hardDeleteOpenImmoUploadsByAccount(Account $account): void
    {
        $openImmoUploads = $this->openImmoUploadRepository->findBy(criteria: ['account' => $account]);

        foreach ($openImmoUploads as $openImmoUpload) {
            $this->hardDeleteOpenImmoUpload(openImmoUpload: $openImmoUpload);
        }
    }
}
