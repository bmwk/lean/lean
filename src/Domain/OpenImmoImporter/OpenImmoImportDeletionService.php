<?php

declare(strict_types=1);

namespace App\Domain\OpenImmoImporter;

use App\Domain\Entity\Account;
use App\Domain\Entity\OpenImmoImporter\OpenImmoImport;
use App\Repository\OpenImmoImporter\OpenImmoImportRepository;
use Doctrine\ORM\EntityManagerInterface;

class OpenImmoImportDeletionService
{
    public function __construct(
        private readonly OpenImmoImportRepository $openImmoImportRepository,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function deleteOpenImmoImport(OpenImmoImport $openImmoImport): void
    {
        $this->hardDeleteOpenImmoImport(openImmoImport: $openImmoImport);
    }

    public function deleteOpenImmoImportsByAccount(Account $account): void
    {
        $this->hardDeleteOpenImmoImportsByAccount(account: $account);
    }

    private function hardDeleteOpenImmoImport(OpenImmoImport $openImmoImport): void
    {
        $this->entityManager->remove($openImmoImport);
        $this->entityManager->flush();
    }

    private function hardDeleteOpenImmoImportsByAccount(Account $account): void
    {
        $openImmoImports = $this->openImmoImportRepository->findBy(criteria: ['account' => $account]);

        foreach ($openImmoImports as $openImmoImport) {
            $this->hardDeleteOpenImmoImport(openImmoImport: $openImmoImport);
        }
    }
}
