<?php

declare(strict_types=1);

namespace App\Domain\OpenImmoImporter\Exception;

class ImportAlreadyInProgressException extends \RuntimeException implements OpenImmoImportExceptionInterface
{
}
