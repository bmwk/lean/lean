<?php

declare(strict_types=1);

namespace App\Domain\OpenImmoImporter\Exception;

class ImportAlreadyProcessedException extends \RuntimeException implements OpenImmoImportExceptionInterface
{
}
