<?php

declare(strict_types=1);

namespace App\Domain\OpenImmoImporter\Exception;

class ImportForAccountIsAlreadyRunningException extends \RuntimeException implements OpenImmoImportExceptionInterface
{
}
