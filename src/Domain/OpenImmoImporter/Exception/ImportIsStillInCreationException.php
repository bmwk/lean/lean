<?php

declare(strict_types=1);

namespace App\Domain\OpenImmoImporter\Exception;

class ImportIsStillInCreationException extends \RuntimeException implements OpenImmoImportExceptionInterface
{
}
