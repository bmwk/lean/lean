<?php

declare(strict_types=1);

namespace App\Domain\OpenImmoImporter\Exception;

interface OpenImmoImportExceptionInterface extends \Throwable
{
}
