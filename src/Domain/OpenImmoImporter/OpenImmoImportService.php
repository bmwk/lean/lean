<?php

declare(strict_types=1);

namespace App\Domain\OpenImmoImporter;

use App\Domain\Classification\ClassificationService;
use App\Domain\Document\DocumentService;
use App\Domain\Entity\Account;
use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\DeletionType;
use App\Domain\Entity\GeolocationPoint;
use App\Domain\Entity\OpenImmoImporter\AccountOpenImmoFtpUserNotFoundException;
use App\Domain\Entity\OpenImmoImporter\ImportStatus;
use App\Domain\Entity\OpenImmoImporter\OpenImmoImport;
use App\Domain\Entity\Person\Salutation;
use App\Domain\Entity\Place;
use App\Domain\Entity\PlaceType;
use App\Domain\Entity\Property\Address;
use App\Domain\Entity\Property\BarrierFreeAccess;
use App\Domain\Entity\Property\Building;
use App\Domain\Entity\Property\BuildingCondition;
use App\Domain\Entity\Property\BuildingUnit;
use App\Domain\Entity\Property\Currency;
use App\Domain\Entity\Property\EnergyCertificate;
use App\Domain\Entity\Property\EnergyCertificateHeatingType;
use App\Domain\Entity\Property\EnergyCertificateType;
use App\Domain\Entity\Property\EnergyEfficiencyClass;
use App\Domain\Entity\Property\ExternalContactPerson;
use App\Domain\Entity\Property\HeatingMethod;
use App\Domain\Entity\Property\LiftType;
use App\Domain\Entity\Property\OpenImmoAdditionalInformation;
use App\Domain\Entity\Property\PropertyMarketingInformation;
use App\Domain\Entity\Property\PropertyPricingInformation;
use App\Domain\Entity\Property\PropertySpacesData;
use App\Domain\Entity\Property\RoofShape;
use App\Domain\Entity\PropertyOfferType;
use App\Domain\Filesystem\FilesystemService;
use App\Domain\Image\ImageService;
use App\Domain\Location\LocationService;
use App\Domain\OpenImmoExporter\Exception\ExportAlreadyInProgressException;
use App\Domain\OpenImmoImporter\Exception\ImportAlreadyProcessedException;
use App\Domain\OpenImmoImporter\Exception\ImportForAccountIsAlreadyRunningException;
use App\Domain\OpenImmoImporter\Exception\ImportIsStillInCreationException;
use App\Domain\OpenImmoImporter\Exception\OpenImmoImportExceptionInterface;
use App\Domain\Property\BuildingUnitDeletionService;
use App\GoogleMaps\Exception\GoogleMapsException;
use App\GoogleMaps\GoogleMapsService;
use App\OpenImmo\Entity\Anhaenge;
use App\OpenImmo\Entity\Energiepass;
use App\OpenImmo\Entity\Flaechen;
use App\OpenImmo\Entity\Geo;
use App\OpenImmo\Entity\Immobilie;
use App\OpenImmo\Entity\Kontaktperson;
use App\OpenImmo\Entity\Objektart;
use App\OpenImmo\Entity\OpenImmo;
use App\OpenImmo\Entity\OpenImmoState;
use App\OpenImmo\Entity\Preise;
use App\Repository\OpenImmoImporter\OpenImmoImportRepository;
use App\Repository\Property\OpenImmoAdditionalInformationRepository;
use DateTime;
use Doctrine\DBAL\Driver\Exception;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Exception\ORMException;
use Doctrine\Persistence\ManagerRegistry;

class OpenImmoImportService
{
    private readonly \SplFileInfo $importsDirectory;

    public function __construct(
        string $openImmoImportsPath,
        private readonly OpenImmoImportRepository $openImmoImportRepository,
        private readonly OpenImmoAdditionalInformationRepository $openImmoAdditionalInformationRepository,
        private readonly FilesystemService $filesystemService,
        private readonly LocationService $locationService,
        private readonly ImageService $imageService,
        private readonly DocumentService $documentService,
        private readonly BuildingUnitDeletionService $buildingUnitDeletionService,
        private readonly ClassificationService $classificationService,
        private readonly EntityManagerInterface $entityManager,
        private readonly GoogleMapsService $googleMapsService,
        private readonly ManagerRegistry $registry
    ) {
        $this->importsDirectory = new \SplFileInfo(filename: $openImmoImportsPath);

        $this->filesystemService->checkIsDirectoryAndPermissions(targetDirectory: $this->importsDirectory);
    }

    public function executeOpenImmoImports(bool $dryRun = false): void
    {
        $imports = $this->openImmoImportRepository->findByImportStatus(importStatus: ImportStatus::CREATED, limit: 5);

        foreach ($imports as $import) {
            try {
                $this->executeOpenImmoImport(openImmoImport: $import, dryRun: $dryRun);
            } catch (OpenImmoImportExceptionInterface $openImmoImport) {
            } catch (Exception $exception) {
            } catch (ORMException $ormException) {
            }
        }
    }

    /**
     * @throws Exception
     * @throws ORMException
     * @throws OpenImmoImportExceptionInterface
     */
    public function executeOpenImmoImport(OpenImmoImport $openImmoImport, bool $dryRun = false): void
    {
        $importStatus = $openImmoImport->getImportStatus();
        $openImmoState = new OpenImmoState();

        if ($importStatus === ImportStatus::IN_CREATION) {
            throw new ImportIsStillInCreationException();
        }

        if ($importStatus === ImportStatus::IN_PROGRESS) {
            throw new ExportAlreadyInProgressException();
        }

        if (in_array(needle: $importStatus, haystack: [ImportStatus::DONE, ImportStatus::FAILED]) === true) {
            throw new ImportAlreadyProcessedException();
        }

        if ($this->countOpenImmoImportsByAccountAndImportStatus(account: $openImmoImport->getAccount(), importStatus: ImportStatus::IN_PROGRESS) > 0) {
            throw new ImportForAccountIsAlreadyRunningException();
        }

        if ($openImmoImport->getAccount()->getAccountOpenImmoFtpUser() === null) {
            $this->handleFailedImport(openImmoImport: $openImmoImport);
            throw new AccountOpenImmoFtpUserNotFoundException();
        }

        if ($dryRun === false) {
            $openImmoImport->setImportStatus(ImportStatus::IN_PROGRESS);

            $this->entityManager->flush();
        }

        try {
            $this->importOpenImmoImport(openImmoImport: $openImmoImport, openImmoState: $openImmoState, dryRun: $dryRun);
        } catch (ORMException $ormException) {
            if ($dryRun === false) {
                $this->entityManager->rollback();

                $this->handleFailedImport(openImmoImport: $openImmoImport);
            }

            throw $ormException;
        } catch (Exception $exception) {
            if ($dryRun === false) {
                $this->entityManager->rollback();

                $this->handleFailedImport(openImmoImport: $openImmoImport);
            }

            throw $exception;
        }

        if ($dryRun === false) {
            $openImmoImport->setImportStatus(importStatus: ImportStatus::DONE);
            $this->entityManager->flush();
            $this->entityManager->commit();
        }
    }


    public function countOpenImmoImportsByAccountAndImportStatus(Account $account, ImportStatus $importStatus): int
    {
        return $this->openImmoImportRepository->countByAccountAndImportStatus(account: $account, importStatus: $importStatus);
    }

    public function buildPathToImportFileFromOpenImmoImport(OpenImmoImport $openImmoImport): \SplFileInfo
    {
        $importDirectory = $this->buildPathToImportDirectoryFromOpenImmoImport(openImmoImport: $openImmoImport);

        return new \SplFileInfo(filename: $importDirectory->getRealPath() . '/' . $openImmoImport->getFilename());
    }

    public function buildPathToImportDirectoryFromOpenImmoImport(OpenImmoImport $openImmoImport): \SplFileInfo
    {
        return new \SplFileInfo(filename: $this->importsDirectory . '/' . $openImmoImport->getAccount()->getId() . '/' . $openImmoImport->getId());
    }

    public function importOpenImmoImport(
        OpenImmoImport $openImmoImport,
        OpenImmoState $openImmoState,
        bool $dryRun = false
    ): void {
        $importFile = $this->buildPathToImportFileFromOpenImmoImport(openImmoImport: $openImmoImport);

        $openImmo = OpenImmo::createFromOpenImmoXml(importFile: $importFile);

        if ($openImmo->getUebertragung()->getUmfang() === 'VOLL') {
            $buildingUnitAdditonalInformations = $this->openImmoAdditionalInformationRepository->findBy(['account' => $openImmoImport->getAccount()]);

            if ($buildingUnitAdditonalInformations !== null && count($buildingUnitAdditonalInformations) > 0) {
                foreach ($buildingUnitAdditonalInformations as $buildingUnitAdditonalInformation) {
                    $openImmoState->addBuildingUnitToDelete($buildingUnitAdditonalInformation->getBuildingUnit());
                }
            }
        }

        if ($dryRun === false) {
            $this->entityManager->beginTransaction();
        }

        foreach ($openImmo->getAnbieter() as $anbieter) {
            foreach ($anbieter->getImmobilie() as $immobilie) {
                $this->importBuildingUnitFromImmobillie(
                    openImmoImport: $openImmoImport,
                    openImmo: $openImmo,
                    immobilie: $immobilie,
                    openImmoState: $openImmoState,
                    dryRun: $dryRun
                );
            }
        }

        foreach ($openImmoState->getBuildingUnitsToDelete() as $buildingUnit) {
            $this->buildingUnitDeletionService->deleteBuildingUnit(buildingUnit: $buildingUnit, deletionType: DeletionType::HARD);
        }

        $this->removeImportDirectory(openImmoImport: $openImmoImport);
    }

    public function fetchOpenImmoImportById(int $id): ?OpenImmoImport
    {
        return $this->openImmoImportRepository->find(id: $id);
    }

    private function mapImmobilieToBuildingUnit(Immobilie $immobilie, BuildingUnit $buildingUnit, Place $place): void
    {
        $barrierFreeAccess = ($immobilie->getAusstattung()->getBarrierefrei())
            ? BarrierFreeAccess::IS_AVAILABLE
            : BarrierFreeAccess::IS_CURRENTLY_NOT_AVAILABLE;

        $heatingMethod = null;
        $floors = null;
        $geoLocationPoint = null;
        $internalNumber = null;

        if ($immobilie->getFreitexte() !== null) {
            $buildingUnit
                ->setName($immobilie->getFreitexte()->getObjekttitel())
                ->setDescription($immobilie->getFreitexte()->getObjektbeschreibung())
                ->setPlaceDescription($immobilie->getFreitexte()->getLage())
                ->setParticularities($immobilie->getFreitexte()->getAusstattBeschr());
        }

        if ($immobilie->getFlaechen() !== null) {
            $buildingUnit->setAreaSize($immobilie->getFlaechen()->getGesamtflaeche());
            if ($immobilie->getFlaechen()->getFensterfront() !== null) {
                $buildingUnit
                    ->setShopWindowAvailable(shopWindowAvailable: true)
                    ->setShopWindowFrontWidth($immobilie->getFlaechen()->getFensterfront());
            }
            if ($immobilie->getFlaechen()->getAnzahlStellplaetze() !== null) {
                $buildingUnit
                    ->setNumberOfParkingLots($immobilie->getFlaechen()->getAnzahlStellplaetze());
            }
        }

        if ($immobilie->getAusstattung() !== null) {
            $buildingUnit
                ->setWheelchairAccessible(
                    wheelchairAccessible:($immobilie->getAusstattung()->getRollstuhlgerecht() !== null)
                        ? $immobilie->getAusstattung()->getRollstuhlgerecht()
                        : false
                )
                ->setRampPresent(
                    rampPresent: ($immobilie->getAusstattung()->getRampe() !== null)
                    ? $immobilie->getAusstattung()->getRampe()
                        : false
                );

            if ($immobilie->getAusstattung()->getHeizungsart() !== null) {

                if ($immobilie->getAusstattung()->getHeizungsart()->getFern() === true) {
                    $heatingMethod = HeatingMethod::DISTRICT_HEATING;
                } elseif ($immobilie->getAusstattung()->getHeizungsart()->getOfen() === true) {
                    $heatingMethod = HeatingMethod::FURNACE_HEATING;
                } elseif ($immobilie->getAusstattung()->getHeizungsart()->getEtage() === true) {
                    $heatingMethod = HeatingMethod::FLOOR_HEATING;
                } elseif ($immobilie->getAusstattung()->getHeizungsart()->getZentral() === true) {
                    $heatingMethod = HeatingMethod::CENTRAL_HEATING;
                } elseif ($immobilie->getAusstattung()->getHeizungsart()->getFussboden() === true) {
                    $heatingMethod = HeatingMethod::UNDERFLOOR_HEATING;
                }

            }

        }

        if ($immobilie->getVersteigerung() !== null) {
            $buildingUnit
                ->setFileNumberForeclosure($immobilie->getVersteigerung()->getAktenzeichen())
                ->setAuctionDate($immobilie->getVersteigerung()->getZvtermin())
                ->setResponsibleLocalCourt($immobilie->getVersteigerung()->getAmtsgericht());
        }

        if ($immobilie->getGeo() !== null) {
            if ($immobilie->getGeo()->getEtage() !== null) {
                if ($immobilie->getGeo()->getEtage() > 0) {
                    $floors = [ $immobilie->getGeo()->getEtage() . '.' ];
                } elseif ($immobilie->getGeo()->getEtage() < 0) {
                    $floors = [ ($immobilie->getGeo()->getEtage() * -1) . '. UG' ];
                } else {
                    $floors = [ 'EG' ];
                }
            }

            if (
                $immobilie->getGeo()->getGeokoordinaten() !== null
                && $immobilie->getGeo()->getGeokoordinaten()->getBreitengrad() !== null
                && $immobilie->getGeo()->getGeokoordinaten()->getLaengengrad() !== null
            ) {
                $geoLocationPoint = GeolocationPoint::createFromLatitudeAndLongitude(
                    latitude: $immobilie->getGeo()->getGeokoordinaten()->getBreitengrad(),
                    longitude: $immobilie->getGeo()->getGeokoordinaten()->getLaengengrad()
                );

                if ($geoLocationPoint->getId() === null) {
                    $this->entityManager->persist($geoLocationPoint);
                }
            } elseif ($immobilie->getGeo()->getStrasse() !== null && $immobilie->getGeo()->getHausnummer() !== null && $immobilie->getGeo()->getPlz() !== null) {
                $address = new Address();
                $address
                    ->setStreetName($immobilie->getGeo()->getStrasse())
                    ->setHouseNumber($immobilie->getGeo()->getHausnummer())
                    ->setPostalCode($immobilie->getGeo()->getPlz())
                    ->setPlace($place);

                $geoLocationPoint = $this->createAndSaveGeolocationPointWithGoogleMapsFromAddress(address: $address);

                if ($geoLocationPoint !== null) {
                    $this->entityManager->persist($geoLocationPoint);
                }
            }

        }

        if (
            $immobilie->getVerwaltungTechn()->getObjektnrExtern() !== null
            && strlen($immobilie->getVerwaltungTechn()->getObjektnrExtern()) <= 60
        ) {
            $internalNumber = $immobilie->getVerwaltungTechn()->getObjektnrExtern();
        }

        $buildingUnit
            ->setInternalNumber($internalNumber)
            ->setHeatingMethod($heatingMethod)
            ->setInFloors($floors)
            ->setBarrierFreeAccess($barrierFreeAccess)
            ->setGeolocationPoint($geoLocationPoint);
    }

    private function mapImmobileToBuildingUnitPropertyMarketingInformation(
        Immobilie $immobilie,
        PropertyMarketingInformation $propertyMarketingInformation
    ): void {
        $versteigerung = $immobilie->getVersteigerung();
        $propertyOfferType = null;

        if ($immobilie->getObjektkategorie()->getVermarktungsart() !== null) {
            if ($immobilie->getObjektkategorie()->getVermarktungsart()->getLeasing()) {
                $propertyOfferType = PropertyOfferType::LEASE;
            } elseif ($immobilie->getObjektkategorie()->getVermarktungsart()->getKauf()) {
                $propertyOfferType = PropertyOfferType::PURCHASE;
            } elseif ($immobilie->getObjektkategorie()->getVermarktungsart()->getMietePacht()) {
                $propertyOfferType = PropertyOfferType::RENT;
            }
        }

        $propertyMarketingInformation
            ->setMarketValue(($versteigerung !== null) ? $versteigerung->getVerkehrswert() : null)
            ->setPropertyOfferType($propertyOfferType);
    }

    private function mapObjektartToBuildingUnitPastUsageIndustryClassification(BuildingUnit $buildingUnit, Objektart $objektart): void
    {
        $pastUsageIndustryClassification = null;

        if (empty($objektart->getZimmer()) === false || empty($objektart->getWohnung()) === false || empty($objektart->getHaus()) === false) {
            $pastUsageIndustryClassification = $this->classificationService->fetchIndustryClassificationByLevels(
                levelOne: 15,
                levelTwo: null,
                levelThree: null
            );
        } elseif (empty($objektart->getGrundstueck()) === false || empty($objektart->getSonstige()) === false) {
            $pastUsageIndustryClassification = $this->classificationService->fetchIndustryClassificationByLevels(
                levelOne: 14,
                levelTwo: null,
                levelThree: null
            );
        } elseif (empty($objektart->getBueroPraxen()) === false) {
            $pastUsageIndustryClassification = $this->classificationService->fetchIndustryClassificationByLevels(
                levelOne: 2,
                levelTwo: null,
                levelThree: null
            );
        } elseif (empty($objektart->getEinzelhandel()) === false) {
            $pastUsageIndustryClassification = $this->classificationService->fetchIndustryClassificationByLevels(
                levelOne: 1,
                levelTwo: null,
                levelThree: null
            );
        } elseif (empty($objektart->getGastgewerbe()) === false) {
            $pastUsageIndustryClassification = $this->classificationService->fetchIndustryClassificationByLevels(
                levelOne: 3,
                levelTwo: null,
                levelThree: null
            );
        } elseif (empty($objektart->getHallenLagerProd()) === false) {
            $pastUsageIndustryClassification = $this->classificationService->fetchIndustryClassificationByLevels(
                levelOne: 11,
                levelTwo: null,
                levelThree: null
            );
        } elseif (empty($objektart->getParken()) === false) {
            $pastUsageIndustryClassification = $this->classificationService->fetchIndustryClassificationByLevels(
                levelOne: 5,
                levelTwo: null,
                levelThree: null
            );
        } elseif (empty($objektart->getFreizeitimmobilieGewerblich()) === false) {
            $pastUsageIndustryClassification = $this->classificationService->fetchIndustryClassificationByLevels(
                levelOne: 4,
                levelTwo: null,
                levelThree: null
            );
        }

        $buildingUnit->setPastUsageIndustryClassification($pastUsageIndustryClassification);
        if ($pastUsageIndustryClassification !== null) {
            $buildingUnit->getPropertyMarketingInformation()->setPossibleUsageIndustryClassifications([$pastUsageIndustryClassification]);
        }

    }

    private function mapPreiseToBuildingUnitPropertyMarketingInformationPropertyPricingInformation(
        Preise $preise,
        PropertyPricingInformation $propertyPricingInformation
    ): void {
        $purchasePriceNet = ($preise->getKaufpreisnetto() !== null) ? $preise->getKaufpreisnetto()->getValue() : null;
        $insideCommission = ($preise->getInnenCourtage() !== null) ? $preise->getInnenCourtage()->getValue() : null;
        $outsideCommission = ($preise->getAussenCourtage() !== null) ? $preise->getAussenCourtage()->getValue() : null;
        $currency = null;

        if ($preise->getWaehrung() !== null) {
            if ($preise->getWaehrung()->getIsoWaehrung() === 'EUR') {
                $currency = Currency::EURO;
            } elseif ($preise->getWaehrung()->getIsoWaehrung() === 'USD') {
                $currency = Currency::DOLLAR;
            }
        }
        $propertyPricingInformation
            ->setPurchasePriceNet($purchasePriceNet)
            ->setPurchasePriceGross($preise->getKaufpreisbrutto())
            ->setNetColdRent($preise->getNettokaltmiete())
            ->setColdRent($preise->getKaltmiete())
            ->setRentIncludingHeating($preise->getWarmmiete())
            ->setAdditionalCosts($preise->getNebenkosten())
            ->setHeatingCostsIncluded($preise->getHeizkostenEnthalten())
            ->setRentalPricePerSquareMeter($preise->getMietpreisProQm())
            ->setLease($preise->getPacht())
            ->setLeasehold($preise->getErbpacht())
            ->setHouseMoney($preise->getHausgeld())
            ->setCommissionable($preise->getProvisionspflichtig())
            ->setInsideCommission($insideCommission)
            ->setOutsideCommission($outsideCommission)
            ->setCurrency($currency)
            ->setFreeTextPrice($preise->getFreitextPreis())
            ->setDeposit($preise->getKaution())
            ->setDepositText($preise->getKautionText());
    }

    private function mapImmobilieZusatzAngabenEnergiepassToBuildingUnitEnergyCertificate(
        Energiepass $energiepass,
        EnergyCertificate $energyCertificate
    ): void {
        $expirationDate = null;
        $electricityConsumption = null;
        $heatingEnergyConsumption = null;
        $energyConsumption = null;
        $energyDemand = null;
        $electricityDemand = null;
        $heatingEnergyDemand = null;

        if ($energiepass->getGueltigBis() !== null) {
            $validTill = DateTime::createFromFormat(format: 'm-Y', datetime: $energiepass->getGueltigBis());
            if ($validTill !== false) {
                $expirationDate = $validTill;
            }
        }


        if ($energiepass->getEpart() !== null && strtolower($energiepass->getEpart()) === 'bedarf') {
            $energyCertificateType = EnergyCertificateType::ENERGY_REQUIREMENT_CERTIFICATE;
        } else {
            $energyCertificateType = EnergyCertificateType::ENERGY_CONSUMPTION_CERTIFICATE;
        }

        if ($energiepass->getEnergieverbrauchkennwert() !== null) {
            $energyConsumption = floatval($energiepass->getEnergieverbrauchkennwert());
        }

        if ($energiepass->getEndenergiebedarf() !== null) {
            $energyDemand = floatval($energiepass->getEndenergiebedarf());
        }

        if ($energiepass->getStromwert() !== null) {
            $electricityDemand = floatval($energiepass->getStromwert());
            $electricityConsumption = floatval($energiepass->getStromwert());
        }

        if ($energiepass->getWaermewert() !== null) {
            $heatingEnergyDemand = floatval($energiepass->getWaermewert());
            $heatingEnergyConsumption = floatval($energiepass->getWaermewert());
        }

        $energyCertificateHeatingTypes = null;

        if ($energiepass->getPrimaerenergietraeger() !== null) {
            if (strtolower($energiepass->getPrimaerenergietraeger()) === 'öl') {
                $energyCertificateHeatingTypes = [EnergyCertificateHeatingType::OIL];
            } elseif (strtolower($energiepass->getPrimaerenergietraeger()) === 'gas') {
                $energyCertificateHeatingTypes = [EnergyCertificateHeatingType::OIL];
            } elseif (strtolower($energiepass->getPrimaerenergietraeger()) === 'elektro') {
                $energyCertificateHeatingTypes = [EnergyCertificateHeatingType::ELECTRIC];
            } elseif (str_starts_with(strtolower($energiepass->getPrimaerenergietraeger()), 'solar')) {
                $energyCertificateHeatingTypes = [EnergyCertificateHeatingType::SOLAR_SYSTEM];
            } elseif (strtolower($energiepass->getPrimaerenergietraeger()) === 'fernwärme') {
                $energyCertificateHeatingTypes = [EnergyCertificateHeatingType::DISTRICT_HEATING];
            } elseif (strtolower($energiepass->getPrimaerenergietraeger()) === 'blockheizkraftwerk') {
                $energyCertificateHeatingTypes = [EnergyCertificateHeatingType::COMBINED_HEAT_AND_POWER_PLANT];
            } elseif (strtolower($energiepass->getPrimaerenergietraeger()) === 'pelletheizung') {
                $energyCertificateHeatingTypes = [EnergyCertificateHeatingType::PELLET_HEATING];
            }
        }

        $energyEfficiencyClass = null;
        if ($energiepass->getWertklasse() !== null) {
            if (strtolower($energiepass->getWertklasse()) === 'a-plus') {
                $energyCertificateHeatingTypes = [EnergyEfficiencyClass::A_PLUS];
            } elseif (strtolower($energiepass->getWertklasse()) === 'a') {
                $energyCertificateHeatingTypes = [EnergyEfficiencyClass::A];
            } elseif (strtolower($energiepass->getWertklasse()) === 'b') {
                $energyCertificateHeatingTypes = [EnergyEfficiencyClass::B];
            } elseif (strtolower($energiepass->getWertklasse()) === 'c') {
                $energyCertificateHeatingTypes = [EnergyEfficiencyClass::C];
            } elseif (strtolower($energiepass->getWertklasse()) === 'd') {
                $energyCertificateHeatingTypes = [EnergyEfficiencyClass::D];
            } elseif (strtolower($energiepass->getWertklasse()) === 'e') {
                $energyCertificateHeatingTypes = [EnergyEfficiencyClass::E];
            } elseif (strtolower($energiepass->getWertklasse()) === 'f') {
                $energyCertificateHeatingTypes = [EnergyEfficiencyClass::F];
            } elseif (strtolower($energiepass->getWertklasse()) === 'g') {
                $energyCertificateHeatingTypes = [EnergyEfficiencyClass::G];
            } elseif (strtolower($energiepass->getWertklasse()) === 'h') {
                $energyCertificateHeatingTypes = [EnergyEfficiencyClass::H];
            }
        }

        $energyCertificate
            ->setEnergyCertificateType($energyCertificateType)
            ->setIssueDate($energiepass->getAusstelldatum())
            ->setExpirationDate($expirationDate)
            ->setBuildingConstructionYear((int) $energiepass->getBaujahr())
            ->setEnergyCertificateHeatingTypes($energyCertificateHeatingTypes)
            ->setEnergyConsumption($energyConsumption)
            ->setElectricityConsumption($electricityConsumption)
            ->setHeatingEnergyConsumption($heatingEnergyConsumption)
            ->setEnergyDemand($energyDemand)
            ->setElectricityDemand($electricityDemand)
            ->setHeatingEnergyDemand($heatingEnergyDemand)
            ->setEnergyEfficiencyClass($energyEfficiencyClass);

        if ($energiepass->getMitwarmwasser() !== null){
            $energyCertificate->setWithHotWater(withHotWater: $energiepass->getMitwarmwasser());
        } else {
            $energyCertificate->setWithHotWater(withHotWater: false);
        }
    }

    private function mapImmobileGeoToBuildingUnitAddress(Geo $geo, Address $address, Place $place): void
    {
        $streetName = ($geo->getStrasse() !== null) ? $geo->getStrasse() : '';
        $houseNumber = ($geo->getHausnummer() !== null) ? $geo->getHausnummer() : '';
        $address
            ->setPlace($place)
            ->setPostalCode($geo->getPlz())
            ->setStreetName($streetName)
            ->setHouseNumber($houseNumber);
    }

    private function mapImmobilieFlaecheToBuildingUnitPropertySpaceData(
        Flaechen $flaeche,
        PropertySpacesData $propertySpacesData
    ): void {
        $propertySpacesData
            ->setRoomCount($flaeche->getAnzahlZimmer())
            ->setLivingSpace($flaeche->getWohnflaeche())
            ->setUsableSpace($flaeche->getNutzflaeche())
            ->setStorageSpace($flaeche->getLagerflaeche())
            ->setStoreSpace($flaeche->getLadenflaeche())
            ->setRetailSpace($flaeche->getVerkaufsflaeche())
            ->setOpenSpace($flaeche->getFreiflaeche())
            ->setOfficeSpace($flaeche->getBueroflaeche())
            ->setOfficePartSpace($flaeche->getBueroteilflaeche())
            ->setAdministrationSpace($flaeche->getVerwaltungsflaeche())
            ->setGastronomySpace($flaeche->getGastroflaeche())
            ->setPlotSize($flaeche->getGrundstuecksflaeche())
            ->setBalconyTurfSpace($flaeche->getBalkonTerrasseFlaeche())
            ->setGardenSpace($flaeche->getGartenflaeche())
            ->setCellarSpace($flaeche->getKellerflaeche())
            ->setAtticSpace($flaeche->getDachbodenflaeche())
            ->setHeatableSurface($flaeche->getBeheizbareFlaeche())
            ->setRentableArea($flaeche->getVermietbareFlaeche());
    }

    private function mapImmobilieToBuilding(Immobilie $immobilie, Building $building): void
    {
        $buildingCondition = null;
        $constructionYear = null;
        $roofShape = null;
        $internalNumber = null;
        $liftTypes = null;

        if ($immobilie->getZustandAngaben()?->getZustand() !== null) {
            if ($immobilie->getZustandAngaben()->getZustand()->getZustandArt() === 'TEIL_VOLLRENOVIERUNGSBED') {
                $buildingCondition = BuildingCondition::RENOVATION_NEEDED;
            } elseif ($immobilie->getZustandAngaben()->getZustand()->getZustandArt() === 'NEUWERTIG') {
                $buildingCondition = BuildingCondition::LIKE_NEW;
            } elseif ($immobilie->getZustandAngaben()->getZustand()->getZustandArt() === 'TEIL_VOLLSANIERT') {
                $buildingCondition = BuildingCondition::SANITIZED;
            } elseif ($immobilie->getZustandAngaben()->getZustand()->getZustandArt() === 'TEIL_SANIERT') {
                $buildingCondition = BuildingCondition::PARTIALLY_RENOVATED;
            } elseif ($immobilie->getZustandAngaben()->getZustand()->getZustandArt() === 'SANIERUNGSBEDUERFTIG') {
                $buildingCondition = BuildingCondition::RENOVATION_NEEDED;
            } elseif ($immobilie->getZustandAngaben()->getZustand()->getZustandArt() === 'BAUFAELLIG') {
                $buildingCondition = BuildingCondition::DILAPIDATED;
            } elseif ($immobilie->getZustandAngaben()->getZustand()->getZustandArt() === 'MODERNISIERT') {
                $buildingCondition = BuildingCondition::MODERNIZED;
            } elseif ($immobilie->getZustandAngaben()->getZustand()->getZustandArt() === 'GEPFLEGT') {
                $buildingCondition = BuildingCondition::GROOMED;
            } elseif ($immobilie->getZustandAngaben()->getZustand()->getZustandArt() === 'ROHBAU') {
                $buildingCondition = BuildingCondition::SHELL;
            } elseif ($immobilie->getZustandAngaben()->getZustand()->getZustandArt() === 'ENTKERNT') {
                $buildingCondition = BuildingCondition::GUTTED;
            } elseif ($immobilie->getZustandAngaben()->getZustand()->getZustandArt() === 'ABRISSOBJEKT') {
                $buildingCondition = BuildingCondition::DEMOLITION_OBJECT;
            }
        }

        if ($immobilie->getZustandAngaben() !== null) {
            $constructionYear = ($immobilie->getZustandAngaben()->getBaujahr() !== null)
                ? intval($immobilie->getZustandAngaben()->getBaujahr())
                : null;
        }

        if ($immobilie->getAusstattung()?->getDachform() !== null) {
            if ($immobilie->getAusstattung()->getDachform()->getFlachdach()) {
                $roofShape = RoofShape::FLAT_ROOF;
            } elseif ($immobilie->getAusstattung()->getDachform()->getKrueppelwalmdach()) {
                $roofShape = RoofShape::CRESTED_HIP_OR_CREST_HIP_ROOF;
            } elseif ($immobilie->getAusstattung()->getDachform()->getMansarddach()) {
                $roofShape = RoofShape::MANSARD_ROOF;
            } elseif ($immobilie->getAusstattung()->getDachform()->getPultdach()) {
                $roofShape = RoofShape::PENT_ROOF;
            } elseif ($immobilie->getAusstattung()->getDachform()->getPyramidendach()) {
                $roofShape = RoofShape::TENT_ROOF;
            } elseif ($immobilie->getAusstattung()->getDachform()->getSatteldach()) {
                $roofShape = RoofShape::SADDLE_OR_GABLE_ROOF;
            } elseif ($immobilie->getAusstattung()->getDachform()->getWalmdach()) {
                $roofShape = RoofShape::HIP_ROOF;
            }
        }

        if ($immobilie->getAusstattung()?->getFahrstuhl() !== null) {
            $liftTypes = [];

            if ($immobilie->getAusstattung()->getFahrstuhl()->getLasten() === true) {
                $liftTypes[] = LiftType::CARGO_LIFT;
            }

            if ($immobilie->getAusstattung()->getFahrstuhl()->getPersonen() === true) {
                $liftTypes[] = LiftType::PASSENGER_LIFT;
            }
        }

        if (
            $immobilie->getVerwaltungTechn()->getObjektnrExtern() !== null
            && strlen($immobilie->getVerwaltungTechn()->getObjektnrExtern()) <= 60
        ) {
            $internalNumber = $immobilie->getVerwaltungTechn()->getObjektnrExtern();
        }

        $building
            ->setInternalNumber($internalNumber)
            ->setBuildingCondition($buildingCondition)
            ->setConstructionYear($constructionYear)
            ->setNumberOfFloors($immobilie->getGeo()->getAnzahlEtagen())
            ->setRoofShape($roofShape)
            ->setLiftTypes($liftTypes);
    }

    private function mapImmobilieKontaktToOpenImmoExternalContactPerson(
        Kontaktperson $kontaktperson,
        ExternalContactPerson $externalContactPerson
    ): void {
        $salutation = null;

        if ($kontaktperson->getAnrede() !== null) {
            if (strtolower($kontaktperson->getAnrede()) === 'herr') {
                $salutation = Salutation::HERR;
            } elseif (strtolower($kontaktperson->getAnrede()) === 'frau') {
                $salutation = Salutation::FRAU;
            } else {
                $salutation = Salutation::DIVERS;
            }
        } elseif ($kontaktperson->getAnrede() !== null) {
            $salutation = Salutation::DIVERS;
        }

        $email = null;
        if ($kontaktperson->getEmailDirekt() !== null) {
            $email = $kontaktperson->getEmailDirekt();
        } elseif ($kontaktperson->getEmailZentrale() !== null) {
            $email = $kontaktperson->getEmailZentrale();
        } elseif ($kontaktperson->getEmailPrivat() !== null) {
            $email = $kontaktperson->getEmailPrivat();
        } elseif ($kontaktperson->getEmailSonstige() !== null) {
            $email = current($kontaktperson->getEmailSonstige());
        } elseif ($kontaktperson->getEmailFeedback() !== null) {
            $email = $kontaktperson->getEmailFeedback();
        }

        $phoneNumber = null;
        if ($kontaktperson->getTelDurchw() !== null) {
            $phoneNumber = $kontaktperson->getTelDurchw();
        } elseif ($kontaktperson->getTelZentrale() !== null) {
            $phoneNumber = $kontaktperson->getTelZentrale();
        } elseif ($kontaktperson->getTelPrivat() !== null) {
            $phoneNumber = $kontaktperson->getTelPrivat();
        } elseif ($kontaktperson->getTelSonstige() !== null) {
            $phoneNumber = current($kontaktperson->getTelSonstige());
        }

        $externalContactPerson
            ->setSalutation($salutation)
            ->setSalutationLetter($kontaktperson->getAnredeBrief())
            ->setFirstName($kontaktperson->getVorname())
            ->setLastName($kontaktperson->getName())
            ->setStreetName($kontaktperson->getStrasse())
            ->setHouseNumber($kontaktperson->getHausnummer())
            ->setPostalCode($kontaktperson->getPlz())
            ->setPlaceName($kontaktperson->getOrt())
            ->setMobilePhoneNumber($kontaktperson->getTelHandy())
            ->setFaxNumber($kontaktperson->getTelFax())
            ->setWebsite($kontaktperson->getUrl())
            ->setAdditionalAddressInformation($kontaktperson->getFreitextfeld())
            ->setPersonTitle($kontaktperson->getTitel())
            ->setCompanyName($kontaktperson->getFirma())
            ->setPostOfficeBox($kontaktperson->getPostfach())
            ->setPostOfficeBoxPostalCode($kontaktperson->getPostfPlz())
            ->setPostOfficeBoxPlaceName($kontaktperson->getPostfOrt())
            ->setPhoneNumber($phoneNumber)
            ->setEmail($email);
    }

    private function immobileIsValid(Immobilie $immobilie, Account $account): bool
    {
        $place = $this->determinePlace(immobilie: $immobilie);

        $placeValid = false;

        $assignedPlace = $account->getAssignedPlace();

        if (in_array($account->getAssignedPlace()->getPlaceType(), [PlaceType::COUNTY, PlaceType::MUNICIPAL_ASSOCIATION])) {
            $assignedPlaces = $assignedPlace->getChildrenPlaces();
        } else {
            $assignedPlaces = [$assignedPlace];
        }

        foreach ($assignedPlaces as $assignedPlace) {
            if ($assignedPlace === $place) {
                $placeValid = true;
                break;
            }
        }

        if ($immobilie->getObjektkategorie()->getNutzungsart()->getGewerbe() === false) {
            return false;
        }

        return $placeValid;
    }

    private function saveBuildingUnit(bool $dryRun): void
    {
        if ($dryRun === false) {
            $this->entityManager->flush();
        }
    }

    private function handleFailedImport(OpenImmoImport $openImmoImport): void
    {
        $entityManager = $this->entityManager;

        if ($entityManager->isOpen() === false) {
            $this->registry->resetManager();
            $entityManager = $this->registry->getManagerForClass(class: OpenImmoImport::class);

            $openImmoImport = $this->openImmoImportRepository->find(id: $openImmoImport->getId());
        }

        $openImmoImport->setImportStatus(ImportStatus::FAILED);

        $entityManager->flush();
    }

    private function removeImportDirectory(OpenImmoImport $openImmoImport): void
    {
        $importDirectory = $this->buildPathToImportDirectoryFromOpenImmoImport(openImmoImport: $openImmoImport);

        if ($importDirectory->isDir() === true) {
            $this->filesystemService->remove(files: $importDirectory->getRealPath());
        }
    }

    private function fetchBuildingUnitFromOpenImmoImportAndImmobilie(
        Account $account,
        Immobilie $immobilie
    ): ?BuildingUnit {
        $openImmoAdditionalInformation = $this->openImmoAdditionalInformationRepository->findOneBy(criteria:[
            'objectNumberInternal' => $immobilie->getVerwaltungTechn()->getObjektnrExtern(),
            'account'              => $account,
        ]);

        return ($openImmoAdditionalInformation !== null) ? $openImmoAdditionalInformation->getBuildingUnit() : null;
    }

    private function createAndSaveBuildingUnitFromOpenImmoImportAndImmobilie(
        Account $account,
        AccountUser $accountUser,
        Immobilie $immobilie,
        Place $place
    ): BuildingUnit {
        $address = new Address();

        $this->mapImmobileGeoToBuildingUnitAddress(
            geo: $immobilie->getGeo(),
            address: $address,
            place: $place
        );

        $building = new Building();
        $building
            ->setAddress($address)
            ->setWheelchairAccessible(false)
            ->setRampPresent(false)
            ->setObjectForeclosed(false)
            ->setWheelchairAccessible(false)
            ->setName($immobilie->getFreitexte()->getObjekttitel())
            ->setDeleted(false)
            ->setCreatedByAccountUser($accountUser)
            ->setAccount($account);


        $buildingUnit = new BuildingUnit();

        $buildingUnit
            ->setAddress($address)
            ->setWheelchairAccessible(false)
            ->setRampPresent(false)
            ->setObjectForeclosed(false)
            ->setWheelchairAccessible(false)
            ->setName($immobilie->getFreitexte()->getObjekttitel())
            ->setDeleted(false)
            ->setObjectIsEmpty(true)
            ->setObjectBecomesEmpty(false)
            ->setReuseAgreed(false)
            ->setArchived(false)
            ->setBuilding($building)
            ->setCreatedByAccountUser($accountUser)
            ->setAccount($account);

        $buildingPropertySpacesData = new PropertySpacesData();

        $openImmoAdditionalInformation = new OpenImmoAdditionalInformation();

        $openImmoAdditionalInformation
            ->setBuildingUnit($buildingUnit)
            ->setAccount($account)
            ->setObjectNumberInternal($immobilie->getVerwaltungTechn()->getObjektnrExtern());

        $propertyMarketingInformation = new PropertyMarketingInformation();
        $propertyPricingInformation = new PropertyPricingInformation();
        $propertySpacesData = new PropertySpacesData();

        $buildingUnit
            ->setPropertyMarketingInformation($propertyMarketingInformation)
            ->setPropertySpacesData($propertySpacesData);

        $building->setPropertySpacesData($buildingPropertySpacesData);

        $propertyMarketingInformation->setPropertyPricingInformation($propertyPricingInformation);

        $this->entityManager->persist($address);
        $this->entityManager->persist($buildingPropertySpacesData);
        $this->entityManager->persist($propertyMarketingInformation);
        $this->entityManager->persist($propertyPricingInformation);
        $this->entityManager->persist($propertySpacesData);
        $this->entityManager->persist($openImmoAdditionalInformation);
        $this->entityManager->persist($building);
        $this->entityManager->persist($buildingUnit);

        $this->entityManager->flush();

        return $buildingUnit;
    }

    private function isUrlReachable(string $url): bool
    {
        $headers = get_headers($url);
        $responseCodeLine = explode(separator: ' ', string: $headers[0]);

        return $responseCodeLine[1] === '200';
    }

    private function saveAnhaengeToBuildingUnitImageOrDocument(Anhaenge $anhaenge, BuildingUnit $buildingUnit, OpenImmoImport $openImmoImport): void
    {
        $documentsToDelete = [];
        $imagesToDelete = [];

        if ($buildingUnit->getDocuments()->count() > 0) {
            foreach ($buildingUnit->getDocuments() as $document) {
                $documentsToDelete[$document->getFile()->getFilename()] = $document;
            }
        }

        if ($buildingUnit->getImages()->count() > 0) {
            foreach ($buildingUnit->getImages() as $image) {
                $imagesToDelete[$image->getFile()->getFilename()] = $image;
            }
        }

        foreach ($anhaenge->getAnhang() as $anhang) {
            $splFileInfoAttachment = new \SplFileInfo($anhang->getDaten()->getPfad());

            if (
                str_starts_with(haystack: $anhang->getDaten()->getPfad(), needle: 'https://')
                || str_starts_with(haystack: $anhang->getDaten()->getPfad(), needle: 'http://')
            ) {
                $pathToData = $anhang->getDaten()->getPfad();

                if ($this->isUrlReachable($pathToData) === false) {
                    continue;
                }
            } else {
                $splFileInfo = $this->buildPathToImportFileFromOpenImmoImport($openImmoImport);
                $pathToData = $splFileInfo->getPath() . '/' . $anhang->getDaten()->getPfad();
            }

            $fileContent = file_get_contents($pathToData);

            if (
                $anhang->getGruppe() === 'BILD'
                || $anhang->getGruppe() === 'TITELBILD'
                || $anhang->getGruppe() === 'INNENANSICHTEN'
                || $anhang->getGruppe() === 'AUSSENANSICHTEN'
                || $anhang->getGruppe() === 'GRUNDRISS'
                || $anhang->getGruppe() === 'KARTEN_LAGEPLAN'
                || $anhang->getGruppe() === 'ANBIETERLOGO'
            ) {
                if (array_key_exists(key: $splFileInfoAttachment->getFilename(), array: $imagesToDelete)) {
                    $image = $imagesToDelete[$splFileInfoAttachment->getFilename()];

                    if (strlen($fileContent) === $image->getFile()->getFileSize()) {
                        unset($imagesToDelete[$splFileInfoAttachment->getFilename()]);
                        continue;
                    }
                }

                $image = $this->imageService->createAndSaveImageFromFileContent(
                    fileContent: $fileContent,
                    filename: $splFileInfoAttachment->getFilename(),
                    fileExtension: $splFileInfoAttachment->getExtension(),
                    public: false,
                    resized: true,
                    withThumbnail: true,
                    accountUser: $openImmoImport->getAccount()->getAccountOpenImmoFtpUser()->getAccountUser(),
                    title: $anhang->getAnhangtitel()
                );

                if ($anhang->getGruppe() === 'TITELBILD') {
                    $buildingUnit->setMainImage($image);
                }

                $buildingUnit->getImages()->add($image);
            } else {
                if (array_key_exists(key: $splFileInfoAttachment->getFilename(), array: $documentsToDelete)) {
                    $document = $documentsToDelete[$splFileInfoAttachment->getFilename()];

                    if (strlen($fileContent) === $document->getFile()->getFileSize()) {
                        unset($documentsToDelete[$splFileInfoAttachment->getFilename()]);
                        continue;
                    }
                }

                $document = $this->documentService->createAndSaveDocumentFromFileContent(
                    fileContent: $fileContent,
                    filename: $splFileInfoAttachment->getFilename(),
                    fileExtension: $splFileInfoAttachment->getExtension(),
                    public: false,
                    accountUser: $openImmoImport->getAccount()->getAccountOpenImmoFtpUser()->getAccountUser(),
                    title: $anhang->getAnhangtitel()
                );

                $buildingUnit->getDocuments()->add($document);
            }
        }

        foreach ($imagesToDelete as $image) {
            $this->buildingUnitDeletionService->deleteImage(image: $image, property: $buildingUnit);
        }

        foreach ($documentsToDelete as $document) {
            $this->buildingUnitDeletionService->deleteDocument(document: $document, property: $buildingUnit);
        }

        if ($buildingUnit->getMainImage() === null && $buildingUnit->getImages()->count() > 0) {
            $buildingUnit->setMainImage($buildingUnit->getImages()->first());
        }
    }

    public function importBuildingUnitFromImmobillie(
        OpenImmoImport $openImmoImport,
        OpenImmo $openImmo,
        Immobilie $immobilie,
        OpenImmoState $openImmoState,
        bool $dryRun = false
    ): void {
        if ($immobilie->getVerwaltungTechn()->getAktion()->getAktionart() !== 'DELETE') {
            $place = $this->determinePlace(immobilie: $immobilie);

            if ($this->immobileIsValid(immobilie: $immobilie, account: $openImmoImport->getAccount()) === false) {
                return;
            }
        }

        $buildingUnit = $this->fetchBuildingUnitFromOpenImmoImportAndImmobilie(account: $openImmoImport->getAccount(), immobilie: $immobilie);

        if ($buildingUnit !== null && $immobilie->getVerwaltungTechn()->getAktion()->getAktionart() === 'DELETE') {
            $openImmoState->addBuildingUnitToDelete($buildingUnit);
            return;
        } elseif ($immobilie->getVerwaltungTechn()->getAktion()->getAktionart() === 'DELETE') {
            return;
        }

        if ($buildingUnit === null) {
            $buildingUnit = $this->createAndSaveBuildingUnitFromOpenImmoImportAndImmobilie(
                account: $openImmoImport->getAccount(),
                accountUser: $openImmoImport->getAccount()->getAccountOpenImmoFtpUser()->getAccountUser(),
                immobilie: $immobilie,
                place: $place
            );
        } else {
            $openImmoState->removeBuildingUnitToDelete($buildingUnit);

            $this->mapImmobileGeoToBuildingUnitAddress(
                geo: $immobilie->getGeo(),
                address: $buildingUnit->getAddress(),
                place: $place
            );
        }

        $this->mapImmobilieToBuildingUnit(immobilie: $immobilie, buildingUnit: $buildingUnit, place: $place);

        $this->mapImmobilieToBuilding(immobilie: $immobilie, building: $buildingUnit->getBuilding());

        $this->mapImmobileToBuildingUnitPropertyMarketingInformation(
            immobilie: $immobilie,
            propertyMarketingInformation: $buildingUnit->getPropertyMarketingInformation()
        );

        if ($immobilie->getObjektkategorie()?->getObjektart() !== null) {
            $this->mapObjektartToBuildingUnitPastUsageIndustryClassification(
                buildingUnit: $buildingUnit,
                objektart: $immobilie->getObjektkategorie()->getObjektart()
            );
        }

        if ($immobilie->getFlaechen() !== null) {
            $this->mapImmobilieFlaecheToBuildingUnitPropertySpaceData(
                flaeche: $immobilie->getFlaechen(),
                propertySpacesData: $buildingUnit->getPropertySpacesData()
            );
        }

        if ($immobilie->getPreise() !== null) {
            $this->mapPreiseToBuildingUnitPropertyMarketingInformationPropertyPricingInformation(
                preise: $immobilie->getPreise(),
                propertyPricingInformation: $buildingUnit
                    ->getPropertyMarketingInformation()
                    ->getPropertyPricingInformation()
            );
        }

        if (empty($immobilie->getZustandAngaben()?->getEnergiepass()) === false) {
            if ($buildingUnit->getEnergyCertificate() === null) {
                $energyCertificate = new EnergyCertificate();
                $buildingUnit->setEnergyCertificate($energyCertificate);
                $this->entityManager->persist($energyCertificate);
            }

            $this->mapImmobilieZusatzAngabenEnergiepassToBuildingUnitEnergyCertificate(
                energiepass: $immobilie->getZustandAngaben()->getEnergiepass(),
                energyCertificate: $buildingUnit->getEnergyCertificate()
            );
        }

        if ($immobilie->getKontaktperson() !== null) {
            if ($buildingUnit->getExternalContactPerson() === null) {
                $externalContactPerson = new ExternalContactPerson();
                $externalContactPerson->setBuildingUnit($buildingUnit);
                $buildingUnit->setExternalContactPerson($externalContactPerson);
                $this->entityManager->persist($externalContactPerson);
            }
            $this->mapImmobilieKontaktToOpenImmoExternalContactPerson(
                kontaktperson: $immobilie->getKontaktperson(),
                externalContactPerson: $buildingUnit->getExternalContactPerson()
            );
        }

        if ($immobilie->getAnhaenge() !== null) {
            $this->saveAnhaengeToBuildingUnitImageOrDocument(
                anhaenge: $immobilie->getAnhaenge(),
                buildingUnit: $buildingUnit,
                openImmoImport: $openImmoImport
            );
        }

        $this->saveBuildingUnit(dryRun: $dryRun);
    }

    private function determinePlace(Immobilie $immobilie): ?Place
    {
        $geo = $immobilie->getGeo();

        if ($geo->getPlz() !== null) {
            $place = $this->locationService->fetchPlaceByPlaceNameAndPostalCode(placeName: $geo->getOrt(), postalCode:  $geo->getPlz());
        } else {
            $place = $this->locationService->fetchPlaceByPlaceName(placeName: $geo->getOrt());
        }

        return $place;
    }

    private function createAndSaveGeolocationPointWithGoogleMapsFromAddress(Address $address): ?GeolocationPoint
    {
        try {
            $googleApiResponse = $this->googleMapsService->fetchGeoDataByAddress(
                streetName: $address->getStreetName(),
                houseNumber: $address->getHouseNumber(),
                postalCode: $address->getPostalCode(),
                placeName: $address->getPlace()->getPlaceName()
            );

            if (
                count($googleApiResponse->results) === 0
                || isset($googleApiResponse->results[0]->geometry) === false
                || isset($googleApiResponse->results[0]->geometry->location) === false
            ) {
                return null;
            }

            $geolocationPoint = GeolocationPoint::createFromLatitudeAndLongitude(
                latitude: $googleApiResponse->results[0]->geometry->location->lat,
                longitude: $googleApiResponse->results[0]->geometry->location->lng
            );

            $this->entityManager->persist(entity: $geolocationPoint);
            $this->entityManager->flush();

            return $geolocationPoint;
        } catch (GoogleMapsException $googleMapsException) {
            return null;
        }
    }
}
