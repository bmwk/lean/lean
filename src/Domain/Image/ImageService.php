<?php

declare(strict_types=1);

namespace App\Domain\Image;

use App\Domain\Entity\Account;
use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\Image;
use App\Domain\Entity\Property\BuildingUnit;
use App\Domain\File\FileService;
use App\Repository\ImageRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ImageService
{
    public function __construct(
        private readonly ImageRepository $imageRepository,
        private readonly FileService $fileService,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function fetchImageByIdAndBuildingUnit(
        Account $account,
        bool $withFromSubAccounts,
        int $id,
        BuildingUnit $buildingUnit
    ): ?Image {
        return $this->imageRepository->findOneByIdAndBuildingUnit(
            account: $account,
            withFromSubAccounts: $withFromSubAccounts,
             id: $id,
            buildingUnit: $buildingUnit
        );
    }

    public function createAndSaveImageFromFileContent(
        string $fileContent,
        string $filename,
        string $fileExtension,
        bool $public,
        bool $resized,
        bool $withThumbnail,
        AccountUser $accountUser,
        ?string $title = null
    ): Image {
        $image = new Image();

        $image
           ->setAccount($accountUser->getAccount())
            ->setCreatedByAccountUser($accountUser)
            ->setPublic($public)
            ->setTitle($title);

        if ($withThumbnail === true) {
            $thumbnailImageFile = $this->fileService->saveFileFromFileContent(
                fileContent: $this->resizeImageFromImageFileContent(imageFileContent: $fileContent, imageWidth: 480, imageHeight: 270),
                filename: 'thumbnail_' . $filename,
                fileExtension: $fileExtension,
                account: $accountUser->getAccount(),
                public: $public,
                accountUser: $accountUser
            );

            $image->setThumbnailFile($thumbnailImageFile);
        }

        if ($resized === true) {
            $imageFile = $this->fileService->saveFileFromFileContent(
                fileContent: $this->resizeImageFromImageFileContent(imageFileContent: $fileContent, imageWidth: 1920, imageHeight: 1080),
                filename: $filename,
                fileExtension: $fileExtension,
                account: $accountUser->getAccount(),
                public: $public,
                accountUser: $accountUser
            );
        } else {
            $imageFile = $this->fileService->saveFileFromFileContent(
                fileContent: $fileContent,
                filename: $filename,
                fileExtension: $fileExtension,
                account: $accountUser->getAccount(),
                public: $public,
                accountUser: $accountUser
            );
        }

        $image->setFile($imageFile);

        $this->entityManager->persist(entity: $image);
        $this->entityManager->flush();

        return $image;
    }

    public function createAndSaveImageFromUploadedFile(
        UploadedFile $uploadedFile,
        bool $public,
        bool $resized,
        bool $withThumbnail,
        AccountUser $accountUser,
        ?string $title = null
    ): Image {
        return $this->createAndSaveImageFromFileContent(
            fileContent: $uploadedFile->getContent(),
            filename: $uploadedFile->getFilename(),
            fileExtension: $uploadedFile->getExtension(),
            public: $public,
            resized: $resized,
            withThumbnail: $withThumbnail,
            accountUser: $accountUser,
            title: $title
        );
    }

    public function resizeImageFromImageFileContent(string $imageFileContent, int $imageWidth, int $imageHeight): string
    {
        $imagick = new \Imagick();

        $imagick->readImageBlob(image: $imageFileContent);

        if ($imagick->getImageWidth() > $imageWidth || $imagick->getImageHeight() > $imageHeight) {
            $imagick->resizeImage(
                columns: $imageWidth,
                rows: $imageHeight,
                filter: \Imagick::FILTER_LANCZOS2,
                blur: 0,
                bestfit: true,
                legacy: false
            );
        }

        $resizedImageFileContent = $imagick->getImageBlob();

        $imagick->destroy();

        return $resizedImageFileContent;
    }
}
