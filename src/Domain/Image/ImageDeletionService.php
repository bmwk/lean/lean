<?php

declare(strict_types=1);

namespace App\Domain\Image;

use App\Domain\Entity\Image;
use App\Domain\File\FileDeletionService;
use Doctrine\ORM\EntityManagerInterface;

class ImageDeletionService
{
    public function __construct(
        private readonly FileDeletionService $fileDeletionService,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function deleteImage(Image $image): void
    {
        $this->hardDeleteImage(image: $image);
    }

    private function hardDeleteImage(Image $image): void
    {
        $imageFile = $image->getFile();
        $imageThumbnailFile = $image->getThumbnailFile();

        $this->entityManager->remove(entity: $image);

        $this->entityManager->flush();

        if ($imageThumbnailFile !== null) {
            $this->fileDeletionService->deleteFile(file: $imageThumbnailFile);
        }

        $this->fileDeletionService->deleteFile(file: $imageFile);
    }
}
