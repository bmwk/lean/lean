<?php

declare(strict_types=1);

namespace App\Domain\File;

use App\Domain\Entity\Account;
use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\File;
use App\Domain\Filesystem\Exception\IsNotFileException;
use App\Domain\Filesystem\Exception\IsNotReadableException;
use App\Domain\Filesystem\FilesystemService;
use App\Repository\FileRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Uid\Uuid;

class FileService
{
    private readonly \SplFileInfo $filesDirectory;

    public function __construct(
        string $filesDirectoryPath,
        private readonly FileRepository $fileRepository,
        private readonly FilesystemService $filesystemService,
        private readonly EntityManagerInterface $entityManager
    ) {
        $this->filesDirectory = new \SplFileInfo(filename: $filesDirectoryPath);

        $this->filesystemService->checkIsDirectoryAndPermissions(targetDirectory: $this->filesDirectory);
    }

    public function saveFileFromUploadedFile(
        UploadedFile $uploadedFile,
        Account $account,
        bool $public,
        ?AccountUser $accountUser = null
    ): File {
        $file = $this->createFileFromUploadedFile(
            uploadedFile: $uploadedFile,
            account: $account,
            public: $public,
            accountUser: $accountUser
        );

        $uploadedFile->move(directory: $this->filesDirectory->getRealPath(), name: $file->getUuid()->toRfc4122());

        $this->entityManager->persist(entity: $file);

        $this->entityManager->flush();

        return $file;
    }

    public function copyFile(File $file, bool $public, ?AccountUser $accountUser = null): File
    {
        $newFile = new File();

        $newFile
            ->setUuid(Uuid::v6())
            ->setFileName($file->getFileName())
            ->setFileExtension($file->getFileExtension())
            ->setFileSize($file->getFileSize())
            ->setMimeType($file->getMimeType())
            ->setPublic($public)
            ->setAccount($file->getAccount())
            ->setCreatedByAccountUser($accountUser);

        $this->filesystemService->copy(
            originFile: $this->filesDirectory->getRealPath() . '/' . $file->getUuid()->toRfc4122(),
            targetFile: $this->filesDirectory->getRealPath() . '/' . $newFile->getUuid()->toRfc4122()
        );

        $this->entityManager->persist(entity: $newFile);

        $this->entityManager->flush();

        return $newFile;
    }

    public function createFileFromUploadedFile(
        UploadedFile $uploadedFile,
        Account $account,
        bool $public,
        ?AccountUser $accountUser = null
    ): File {
        $file = new File();

        $file
            ->setUuid(Uuid::v6())
            ->setFileName($uploadedFile->getClientOriginalName())
            ->setFileExtension($uploadedFile->guessExtension())
            ->setFileSize($uploadedFile->getSize())
            ->setMimeType($uploadedFile->getMimeType())
            ->setPublic($public)
            ->setAccount($account)
            ->setCreatedByAccountUser($accountUser);

        return $file;
    }

    public function saveFileFromFileContent(
        string $fileContent,
        string $filename,
        string $fileExtension,
        Account $account,
        bool $public,
        ?AccountUser $accountUser = null
    ): File {
        $uuid = Uuid::v6();

        file_put_contents(filename: $this->filesDirectory->getRealPath() . '/' . $uuid->toRfc4122(), data: $fileContent);

        $splFileInfo = new \SplFileInfo(filename: $this->filesDirectory->getRealPath() . '/' . $uuid->toRfc4122());

        if ($splFileInfo->isFile() !== true) {
            throw new IsNotFileException();
        }

        if ($splFileInfo->isReadable() !== true) {
            throw new IsNotReadableException();
        }

        $file = new File();

        $file
            ->setUuid($uuid)
            ->setFileName($filename)
            ->setFileExtension($fileExtension)
            ->setFileSize($splFileInfo->getSize())
            ->setMimeType(mime_content_type(filename: $splFileInfo->getRealPath()))
            ->setPublic($public)
            ->setAccount($account)
            ->setCreatedByAccountUser($accountUser);

        $this->entityManager->persist(entity: $file);

        $this->entityManager->flush();

        return $file;
    }

    public function fetchFileByUuidAndAccount(Uuid $uuid, Account $account): ?File
    {
        return $this->fileRepository->findOneByUuidAndAccount(uuid: $uuid, account: $account);
    }

    public function fetchPublicFileByUuid(Uuid $uuid): ?File
    {
        return $this->fileRepository->findOneByUuidAndPublic(uuid: $uuid, public: true);
    }

    public function fetchFileContentFromFile(File $file): string
    {
        return $this->fetchFileContent($file->getUuid());
    }

    public function getRealPathFromFile(File $file): string
    {
        $splFileInfo = new \SplFileInfo(filename: $this->filesDirectory->getRealPath() . '/' . $file->getUuid()->toRfc4122());

        if ($splFileInfo->isFile() === false) {
            throw new IsNotFileException();
        }

        return $splFileInfo->getRealPath();
    }

    public function checkFileExists(File $file): bool
    {
        $splFileInfo = new \SplFileInfo(filename: $this->filesDirectory->getRealPath() . '/' . $file->getUuid()->toRfc4122());

        if ($splFileInfo->isFile() === true) {
            return true;
        }

        return false;
    }

    private function fetchFileContent(Uuid $uuid): string
    {
        $file = new \SplFileInfo(filename: $this->filesDirectory->getRealPath() . '/' . $uuid->toRfc4122());

        if ($file->isFile() !== true) {
            throw new IsNotFileException();
        }

        if ($file->isReadable() !== true) {
           throw new IsNotReadableException();
        }

        return file_get_contents(filename: $file->getRealPath());
    }
}
