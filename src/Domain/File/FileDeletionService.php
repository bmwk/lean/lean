<?php

declare(strict_types=1);

namespace App\Domain\File;

use App\Domain\Entity\File;
use App\Domain\Filesystem\Exception\IsNotDirectoryException;
use App\Domain\Filesystem\Exception\IsNotReadableException;
use App\Domain\Filesystem\Exception\IsNotWritableException;
use App\Domain\Filesystem\FilesystemService;
use Doctrine\ORM\EntityManagerInterface;

class FileDeletionService
{
    private readonly \SplFileInfo $filesDirectory;

    public function __construct(
        string $filesDirectoryPath,
        private readonly FilesystemService $filesystemService,
        private readonly EntityManagerInterface $entityManager
    ) {
        $this->filesDirectory = new \SplFileInfo(filename: $filesDirectoryPath);

        $this->checkIsDirectoryAndPermissions(targetDirectory: $this->filesDirectory);
    }

    public function deleteFile(File $file): void
    {
        $this->hardDeleteFile(file: $file);
    }

    private function hardDeleteFile(File $file): void
    {
        $splFileInfo = new \SplFileInfo(filename: $this->filesDirectory->getRealPath() . '/' . $file->getUuid()->toRfc4122());

        if ($splFileInfo->isFile() === true && $splFileInfo->isWritable() !== true) {
            throw new IsNotWritableException();
        }

        if ($splFileInfo->isFile() === true) {
            $this->filesystemService->remove(files: $splFileInfo->getRealPath());
        }

        $this->entityManager->remove($file);
        $this->entityManager->flush();
    }

    private function checkIsDirectoryAndPermissions(\SplFileInfo $targetDirectory): void
    {
        if ($targetDirectory->isDir() !== true) {
            throw new IsNotDirectoryException(message: 'The target directory: \'' . $targetDirectory->getRealPath() . '\' is not a directory');
        }

        if ($targetDirectory->isReadable() !== true) {
            throw new IsNotReadableException(message: 'The target directory: \'' . $targetDirectory->getRealPath() . '\' is not readable');
        }

        if ($targetDirectory->isWritable() !== true) {
            throw new IsNotWritableException(message: 'The target directory: \'' . $targetDirectory->getRealPath() . '\' is not writable');
        }
    }
}
