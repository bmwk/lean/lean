<?php

declare(strict_types=1);

namespace App\Domain\Location;

use App\Domain\Entity\GeolocationMultiPolygon;
use App\Domain\Entity\GeolocationPoint;
use App\Domain\Entity\GeolocationPolygon;
use Doctrine\ORM\EntityManagerInterface;

class LocationDeletionService
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function deleteGeolocationPoint(GeolocationPoint $geolocationPoint): void
    {
        $this->hardDeleteGeolocationPoint(geolocationPoint: $geolocationPoint);
    }

    public function deleteGeolocationPolygon(GeolocationPolygon $geolocationPolygon): void
    {
        $this->hardDeleteGeolocationPolygon(geolocationPolygon: $geolocationPolygon);
    }

    public function deleteGeolocationMultiPolygon(GeolocationMultiPolygon $geolocationMultiPolygon): void
    {
        $this->hardDeleteGeolocationMultiPolygon(geolocationMultiPolygon: $geolocationMultiPolygon);
    }

    private function hardDeleteGeolocationPoint(GeolocationPoint $geolocationPoint): void
    {
        $this->entityManager->remove(entity: $geolocationPoint);

        $this->entityManager->flush();
    }

    private function hardDeleteGeolocationPolygon(GeolocationPolygon $geolocationPolygon): void
    {
        $this->entityManager->remove(entity: $geolocationPolygon);

        $this->entityManager->flush();
    }

    private function hardDeleteGeolocationMultiPolygon(GeolocationMultiPolygon $geolocationMultiPolygon): void
    {
        $this->entityManager->remove(entity: $geolocationMultiPolygon);

        $this->entityManager->flush();
    }
}
