<?php

declare(strict_types=1);

namespace App\Domain\Location\Api\Entity;

use App\Domain\Entity\PlaceType;

class ResourceNamePlaceTypeMapping
{
    public static function getPlaceType(string $resourceName): ?PlaceType
    {
        return match ($resourceName) {
            'states' => PlaceType::STATE,
            'counties' => PlaceType::COUNTY,
            'municipal-associations' => PlaceType::MUNICIPAL_ASSOCIATION,
            'municipalities' => PlaceType::MUNICIPALITY,
            'cities' => PlaceType::CITY,
            'county-seats' => PlaceType::COUNTY_SEAT,
            'market-municipals' => PlaceType::MARKET_MUNICIPAL,
            'county-boroughs' => PlaceType::COUNTY_BOROUGH,
            'county-capitals' => PlaceType::COUNTY_CAPITAL,
            'hanse-cities' => PlaceType::HANSE_CITY,
            'flecken' => PlaceType::FLECKEN,
            'unified-municipalities' => PlaceType::UNIFIED_MUNICIPALITY,
            'local-congregations' => PlaceType::LOCAL_CONGREGATION,
            'world-heritage-cities' => PlaceType::WORLD_HERITAGE_CITY,
            'city-quarters' => PlaceType::CITY_QUARTER,
            'neighbourhoods' => PlaceType::NEIGHBOURHOOD,
            'boroughs' => PlaceType::BOROUGH,
            'postal-codes' => PlaceType::POSTAL_CODE,
            'countries' => PlaceType::COUNTRY,
            default => null
        };
    }
}
