<?php

declare(strict_types=1);

namespace App\Domain\Location;

use App\Domain\Entity\Place;
use App\Domain\Entity\PlaceRelationType;
use App\Domain\Entity\PlaceType;
use App\Repository\PlaceRepository;
use Symfony\Component\Uid\Uuid;

class LocationService
{
    private const COMMUNE_PLACE_TYPES = [
        PlaceType::MUNICIPALITY,
        PlaceType::CITY,
        PlaceType::COUNTY_SEAT,
        PlaceType::MARKET_MUNICIPAL,
        PlaceType::COUNTY_BOROUGH,
        PlaceType::COUNTY_CAPITAL,
        PlaceType::HANSE_CITY,
        PlaceType::FLECKEN,
        PlaceType::UNIFIED_MUNICIPALITY,
        PlaceType::LOCAL_CONGREGATION,
        PlaceType::WORLD_HERITAGE_CITY,
    ];

    public function __construct(
        private readonly PlaceRepository $placeRepository
    ) {
    }

    public function fetchPlaceByPlaceName(string $placeName): ?Place
    {
        return $this->placeRepository->findOneByPlaceName(placeName: $placeName);
    }

    public function fetchPlaceByUuid(Uuid $uuid): ?Place
    {
        return $this->placeRepository->findOneByUuid(uuid: $uuid);
    }

    /**
     * @param PlaceType[] $placeTypes
     */
    public function fetchPlaceByIdAndChildPlacesByPlaceTypes(int $placeId, array $placeTypes): ?Place
    {
        return $this->placeRepository->findOnePlaceByIdAndChildPlacesByPlaceTypes(id: $placeId, placeTypes: $placeTypes);
    }

    /**
     * @return Place[]
     */
    public function fetchPlacesByPlaceType(PlaceType $placeType): array
    {
        return $this->placeRepository->findByPlaceType(placeType: $placeType);
    }

    /**
     * @param PlaceType[] $placeTypes
     * @return Place[]
     */
    public function fetchChildPlacesByPlaceType(Place $place, array $placeTypes): array
    {
        return $this->placeRepository->findChildPlacesByPlaceTypes(place: $place, placeTypes: $placeTypes);
    }

    public function fetchCityQuartersFromPlace(Place $place): array
    {
        $cityQuarters = $this->fetchChildPlacesByPlaceType(place: $place, placeTypes: [PlaceType::CITY_QUARTER]);

        $boroughs = $this->fetchChildPlacesByPlaceType(place: $place, placeTypes: [PlaceType::BOROUGH]);

        foreach ($boroughs as $borough) {
            $cityQuarters = array_merge($cityQuarters, $this->fetchChildPlacesByPlaceType(place: $borough, placeTypes: [PlaceType::CITY_QUARTER]));
        }

        return $cityQuarters;
    }

    /**
     * @param Place[] $places
     */
    public function fetchPlaceByPlaceNameAndPlaceTypeFromChildPlaces(string $placeName, PlaceType $placeType, array $places): ?Place
    {
        foreach ($places as $place) {
            if ($place->getPlaceName() === $placeName) {
                return $place;
            }

            $parentPlaces = $place->getParentPlaces();

            if ($parentPlaces->count() > 1) {
                $placeResult = $this->fetchPlaceByPlaceNameAndPlaceTypeFromChildPlaces(
                    placeName: $placeName,
                    placeType: $placeType,
                    places: $parentPlaces
                );

                if ($placeResult !== null) {
                    return $placeResult;
                }
            } elseif ($parentPlaces->count() === 1) {
                return $parentPlaces->first();
            }
        }

        return null;
    }

    public function fetchPlaceByPlaceNameAndPostalCode(string $placeName, string $postalCode): ?Place
    {
        $postalCodePlace = $this->placeRepository->findOneBy(criteria: [
            'placeName' => trim($postalCode),
            'placeType' => PlaceType::POSTAL_CODE,
        ]);

        if ($postalCodePlace === null) {
            return null;
        }

        $parentPlaces = $this->fetchRecursivePlacesByPlacesAndPlaceRelationTypeAndPlaceType(
            places: [$postalCodePlace],
            placeTypes: $this->getCommunePlaceTypes(),
            placeRelationType: PlaceRelationType::PARENT,
        );

        if (empty($parentPlaces) === true) {
            return null;
        }

        if (count($parentPlaces) === 1) {
            return $parentPlaces[0];
        }

        $mostSimilarPlace = null;
        $currentSmallestDistance = null;

        foreach ($parentPlaces as $parentPlace) {
            $distance = levenshtein($parentPlace->getPlaceName(), $placeName);

            if ($distance < $currentSmallestDistance || $currentSmallestDistance === null) {
                $currentSmallestDistance = $distance;
                $mostSimilarPlace = $parentPlace;
            }
        }

        return $mostSimilarPlace;
    }

    /**
     * @param Place[] $places
     * @param PlaceType[] $placeTypes
     * @return Place[]
     */
    public function fetchRecursivePlacesByPlacesAndPlaceRelationTypeAndPlaceType(
        array $places,
        array $placeTypes,
        PlaceRelationType $placeRelationType = PlaceRelationType::CHILD
    ): array {
        return $this->placeRepository->findRecursiveByPlacesAndPlaceRelationTypeAndPlaceTypes(
            places: $places,
            placeTypes: $placeTypes,
            placeRelationType: $placeRelationType
        );
    }

    /**
     * @return PlaceType[]
     */
    public function getCommunePlaceTypes(): array
    {
        return self::COMMUNE_PLACE_TYPES;
    }
}
