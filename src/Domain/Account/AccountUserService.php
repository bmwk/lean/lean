<?php

declare(strict_types=1);

namespace App\Domain\Account;

use App\Domain\Entity\Account;
use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\AccountUser\AccountUserRole;
use App\Domain\Entity\AccountUser\AccountUserType;
use App\Domain\Entity\AccountUser\AccountUserFilter;
use App\Domain\Entity\AccountUser\AccountUserSearch;
use App\Domain\Entity\SortingOption\SortingOption;
use App\Domain\Entity\UserRegistration\UserRegistration;
use App\Repository\AccountUser\AccountUserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AccountUserService
{
    public function __construct(
        private readonly AccountUserRepository $accountUserRepository,
        private readonly UserPasswordHasherInterface $passwordHasher,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    /**
     * @param AccountUserType[]|null $accountUserTypes
     * @param AccountUserRole[]|null $accountUserRoles
     * @return AccountUser[]
     */
    public function fetchAccountUsersByAccount(
        Account $account,
        bool $withFromSubAccounts,
        ?array $accountUserTypes = null,
        ?array $accountUserRoles = null,
        ?AccountUserFilter $accountUserFilter = null,
        ?AccountUserSearch $accountUserSearch = null
    ): array {
        return $this->accountUserRepository->findByAccount(
            account: $account,
            withFromSubAccounts: $withFromSubAccounts,
            accountUserTypes: $accountUserTypes,
            accountUserRoles: $accountUserRoles,
            accountUserFilter: $accountUserFilter,
            accountUserSearch: $accountUserSearch
        );
    }

    /**
     * @param AccountUserType[]|null $accountUserTypes
     * @param AccountUserRole[]|null $accountUserRoles
     */
    public function fetchAccountUsersPaginatedByAccount(
        Account $account,
        bool $withFromSubAccounts,
        int $firstResult,
        int $maxResults,
        ?array $accountUserTypes = null,
        ?array $accountUserRoles = null,
        ?AccountUserFilter $accountUserFilter = null,
        ?AccountUserSearch $accountUserSearch = null,
        ?SortingOption $sortingOption = null
    ): Paginator {
        return $this->accountUserRepository->findPaginatedByAccount(
            account: $account,
            withFromSubAccounts: $withFromSubAccounts,
            firstResult: $firstResult,
            maxResults: $maxResults,
            accountUserTypes: $accountUserTypes,
            accountUserRoles: $accountUserRoles,
            accountUserFilter: $accountUserFilter,
            accountUserSearch: $accountUserSearch,
            sortingOption: $sortingOption
        );
    }

    /**
     * @param int[] $ids
     * @return AccountUser[]
     */
    public function fetchAccountUsersByIds(Account $account, bool $withFromSubAccounts, array $ids): array
    {
        return $this->accountUserRepository->findByIds(account: $account, withFromSubAccounts: $withFromSubAccounts, ids: $ids);
    }

    public function fetchAccountUserById(Account $account, bool $withFromSubAccounts, int $id): ?AccountUser
    {
        return $this->accountUserRepository->findOneById(account: $account, withFromSubAccounts: $withFromSubAccounts, id: $id);
    }

    public function fetchAccountUserFromAllAccountsByEmail(string $email): ?AccountUser
    {
        return $this->accountUserRepository->findOneBy(criteria: [
            'email'   => $email,
            'deleted' => false,
        ]);
    }

    public function fetchAccountUserByIdFromAllAccounts(int $id): ?AccountUser
    {
        return $this->accountUserRepository->findOneByIdFromAllAccounts(id: $id);
    }

    /**
     * @param AccountUserType[] $accountUserTypes
     * @return AccountUser[]
     */
    public function fetchToBeDeletedAccountUsersByAccountUserTypesFromAllAccounts(array $accountUserTypes, ?int $timeTillHardDelete = null): array
    {
        return $this->accountUserRepository->findToBeDeletedByAccountUserTypesFromAllAccounts(
            accountUserTypes: $accountUserTypes,
            timeTillHardDelete: $timeTillHardDelete
        );
    }

    /**
     * @param AccountUserType[]|null $accountUserTypes
     */
    public function countAccountUsersByAccount(
        Account $account,
        bool $withFromSubAccounts,
        ?bool $enabled = null,
        ?array $accountUserTypes = null
    ): int {
        return $this->accountUserRepository->countByAccount(
            account: $account,
            withFromSubAccounts: $withFromSubAccounts,
            enabled: $enabled,
            accountUserTypes: $accountUserTypes
        );
    }

    public function isPasswordEqual(AccountUser $accountUser, string $plainPassword): bool
    {
        return $this->passwordHasher->isPasswordValid(user: $accountUser, plainPassword: $plainPassword);
    }

    public function hashPassword(AccountUser $accountUser, string $plainPassword): string
    {
        return $this->passwordHasher->hashPassword(user: $accountUser, plainPassword: $plainPassword);
    }

    public function isUsernameExistentInDatabase(string $userIdentifier): bool
    {
        return $this->accountUserRepository->findOneBy(criteria: ['username' => $userIdentifier]) !== null;
    }

    public function updatePasswordFromAccountUser(AccountUser $accountUser, string $plainPassword): void
    {
        if ($plainPassword === '') {
            throw new \InvalidArgumentException(message: 'invalid password');
        }

        $hashedPassword = $this->hashPassword(accountUser: $accountUser, plainPassword: $plainPassword);

        $accountUser->setPassword($hashedPassword);

        $this->entityManager->flush();
    }

    public function createAccountUserFromUserRegistration(UserRegistration $userRegistration): AccountUser
    {
        $accountUser = AccountUser::createFromUserRegistration(userRegistration: $userRegistration);

        $accountUser
            ->setPassword($this->passwordHasher->hashPassword(user: $accountUser, plainPassword: bin2hex(string: random_bytes(length: 10))))
            ->setAnonymized(false)
            ->setDeleted(false)
            ->setEnabled(true);

        return $accountUser;
    }

    public function isThePasswordSecure(string $plainPassword): bool
    {
        if (strlen(string: $plainPassword) < 8) {
            return false;
        }

        if (
            preg_match(pattern: '/[A-Z]+/', subject: $plainPassword) < 1
            || preg_match(pattern: '/[a-z]+/', subject: $plainPassword) < 1
            || preg_match(pattern: '/[0-9]+/', subject: $plainPassword) < 1
        ) {
            return false;
        }

        return true;
    }
}
