<?php

declare(strict_types=1);

namespace App\Domain\Account;

use App\Domain\Entity\Account;
use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\AccountUser\AccountUserPasswordReset;
use App\Domain\Entity\DeletionType;
use App\Domain\Entity\WmsLayer;
use App\Domain\Person\PersonDeletionService;
use App\Repository\AccountUser\AccountUserPasswordResetRepository;
use App\Repository\AccountUser\AccountUserRepository;
use Doctrine\ORM\EntityManagerInterface;

class AccountUserDeletionService
{
    public function __construct(
        private readonly AccountUserRepository $accountUserRepository,
        private readonly AccountUserPasswordResetRepository $accountUserPasswordResetRepository,
        private readonly PersonDeletionService $personDeletionService,
        private readonly AccountUserAnonymizationService $accountUserAnonymizationService,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function deleteAccountUser(
        AccountUser $accountUser,
        DeletionType $deletionType = DeletionType::ANONYMIZATION,
        ?AccountUser $deletedByAccountUser = null
    ): void {
        switch ($deletionType) {
            case DeletionType::ANONYMIZATION:
                $this->anonymizationDeleteAccountUser(accountUser: $accountUser, deletedByAccountUser: $deletedByAccountUser);
                break;
            case DeletionType::HARD:
                $this->hardDeleteAccountUser(accountUser: $accountUser);
                break;
            default:
                throw new \RuntimeException(message: 'Unsupported DeletionType');
        }
    }

    public function deleteAccountUsersByAccount(
        Account $account,
        DeletionType $deletionType = DeletionType::ANONYMIZATION,
        ?AccountUser $deletedByAccountUser = null
    ): void {
        switch ($deletionType) {
            case DeletionType::ANONYMIZATION:
                $this->anonymizationDeleteAccountUsersByAccount(account: $account, deletedByAccountUser: $deletedByAccountUser);
                break;
            case DeletionType::HARD:
                $this->hardDeleteAccountUsersByAccount(account: $account);
                break;

            default:
                throw new \RuntimeException(message: 'Unsupported DeletionType');
        }
    }

    public function deleteAccountUserPasswordReset(AccountUserPasswordReset $accountUserPasswordReset): void
    {
        $this->hardDeleteAccountUserPasswordReset(accountUserPasswordReset: $accountUserPasswordReset);
    }

    public function deleteAccountUserPasswordResetByAccountUser(AccountUser $accountUser): void
    {
        $this->hardDeleteAccountUserPasswordResetByAccountUser(accountUser: $accountUser);
    }

    public function deleteWmsLayer(AccountUser $accountUser, WmsLayer $wmsLayer): void
    {
        $this->hardDeleteWmsLayer(accountUser: $accountUser, wmsLayer: $wmsLayer);
    }

    private function anonymizationDeleteAccountUser(AccountUser $accountUser, AccountUser $deletedByAccountUser): void
    {
        $this->accountUserAnonymizationService->anonymizeAccountUser(accountUser: $accountUser, anonymizedByAccountUser: $deletedByAccountUser);

        if ($accountUser->getPerson() !== null) {
            $this->personDeletionService->deletePerson(
                person: $accountUser->getPerson(),
                deletionType: DeletionType::ANONYMIZATION,
                deletedByAccountUser: $deletedByAccountUser
            );
        }

        $accountUser
            ->setDeleted(deleted: true)
            ->setDeletedAt(deletedAt: new \DateTimeImmutable())
            ->setDeletedByAccountUser(deletedByAccountUser: $deletedByAccountUser);

        $this->entityManager->flush();
    }

    private function hardDeleteAccountUser(AccountUser $accountUser): void
    {
        $this->hardDeleteAllWmsLayers(accountUser: $accountUser);

        $this->deleteAccountUserPasswordResetByAccountUser(accountUser: $accountUser);

        $person = $accountUser->getPerson();

        $person->setCreatedByAccountUser(null);

        $this->entityManager->remove($accountUser);
        $this->entityManager->flush();

        $person->setAccountUser(null);

        if ($person->isAnonymized() === false) {
            $this->personDeletionService->deletePerson(
                person: $person,
                deletionType: DeletionType::ANONYMIZATION,
                deletedByAccountUser: $accountUser->getDeletedByAccountUser()
            );
        }
    }

    private function anonymizationDeleteAccountUsersByAccount(Account $account, AccountUser $deletedByAccountUser): void
    {
        foreach ($this->accountUserRepository->findBy(criteria: ['account' => $account]) as $accountUser) {
            $this->anonymizationDeleteAccountUser(accountUser: $accountUser, deletedByAccountUser: $deletedByAccountUser);
        }
    }

    private function hardDeleteAccountUsersByAccount(Account $account): void
    {
        $accountUsers = $this->accountUserRepository->findBy(criteria: ['account' => $account]);

        foreach ($accountUsers as $accountUser) {
            $this->personDeletionService->unsetContactCreatedByAccountUserForAccountUser(accountUser: $accountUser);
            $this->hardDeleteAccountUser(accountUser: $accountUser);
        }
    }

    private function hardDeleteAccountUserPasswordReset(AccountUserPasswordReset $accountUserPasswordReset): void
    {
        $this->entityManager->remove($accountUserPasswordReset);

        $this->entityManager->flush();
    }

    private function hardDeleteAccountUserPasswordResetByAccountUser(AccountUser $accountUser): void
    {
        $accountUserPasswordReset = $this->accountUserPasswordResetRepository->findOneByAccountUser(accountUser: $accountUser);

        if ($accountUserPasswordReset === null) {
            return;
        }

        $this->hardDeleteAccountUserPasswordReset(accountUserPasswordReset: $accountUserPasswordReset);
    }

    private function hardDeleteWmsLayer(AccountUser $accountUser, WmsLayer $wmsLayer): void
    {
        if ($accountUser->getWmsLayers()->contains(element: $wmsLayer) === false) {
            return;
        }

        $accountUser->getWmsLayers()->removeElement(element: $wmsLayer);

        $this->entityManager->remove(entity: $wmsLayer);

        $this->entityManager->flush();
    }

    private function hardDeleteAllWmsLayers(AccountUser $accountUser): void
    {
        foreach ($accountUser->getWmsLayers() as $wmsLayer) {
            $this->hardDeleteWmsLayer(accountUser: $accountUser, wmsLayer: $wmsLayer);
        }
    }
}
