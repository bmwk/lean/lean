<?php

declare(strict_types=1);

namespace App\Domain\Account;

use App\Domain\BusinessLocationArea\BusinessLocationAreaDeletionService;
use App\Domain\Document\DocumentDeletionService;
use App\Domain\DocumentTemplate\DocumentTemplateDeletionService;
use App\Domain\Entity\Account;
use App\Domain\Entity\AccountConfiguration;
use App\Domain\Entity\AccountLegalTextsConfiguration;
use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\DeletionType;
use App\Domain\Entity\WmsLayer;
use App\Domain\Image\ImageDeletionService;
use App\Domain\LocationIndicators\WegweiserKommunePlaceMappingDeletionService;
use App\Domain\LookingForPropertyRequest\LookingForPropertyRequestDeletionService;
use App\Domain\LookingForPropertyRequest\Matching\MatchingTaskDeletionService;
use App\Domain\LookingForPropertyRequestReport\LookingForPropertyRequestReportDeletionService;
use App\Domain\OpenImmoFtpTransfer\OpenImmoFtpCredentialDeletionService;
use App\Domain\OpenImmoFtpTransfer\OpenImmoFtpTransferTaskDeletionService;
use App\Domain\OpenImmoImporter\OpenImmoImportDeletionService;
use App\Domain\OpenImmoImporter\OpenImmoUploadDeletionService;
use App\Domain\Person\OccurrenceDeletionService;
use App\Domain\Person\PersonDeletionService;
use App\Domain\Property\BuildingUnitDeletionService;
use App\Domain\Property\ProcessPropertyExcelImportTaskDeletionService;
use App\Domain\Property\PropertyExcelImportDeletionService;
use App\Domain\Property\PropertyOwnerDeletionService;
use App\Domain\Property\PropertyUserDeletionService;
use App\Domain\PropertyVacancyReport\PropertyVacancyReportDeletionService;
use App\Domain\SettlementConcept\SettlementConceptAccountUserDeletionService;
use App\Domain\SettlementConcept\SettlementConceptDeletionService;
use App\Domain\SettlementConcept\SettlementConceptMatchingTaskDeletionService;
use App\Domain\UserRegistration\UserInvitationDeletionService;
use App\Domain\UserRegistration\UserRegistrationDeletionService;
use App\Repository\AccountConfigurationRepository;
use App\Repository\AccountLegalTextsConfigurationRepository;
use Doctrine\ORM\EntityManagerInterface;

class AccountDeletionService
{
    public function __construct(
        private readonly AccountConfigurationRepository $accountConfigurationRepository,
        private readonly AccountLegalTextsConfigurationRepository $accountLegalTextsConfigurationRepository,
        private readonly AccountUserDeletionService $accountUserDeletionService,
        private readonly BusinessLocationAreaDeletionService $businessLocationAreaDeletionService,
        private readonly BuildingUnitDeletionService $buildingUnitDeletionService,
        private readonly DocumentTemplateDeletionService $documentTemplateDeletionService,
        private readonly LookingForPropertyRequestDeletionService $lookingForPropertyRequestDeletionService,
        private readonly LookingForPropertyRequestReportDeletionService $lookingForPropertyRequestReportDeletionService,
        private readonly MatchingTaskDeletionService $matchingTaskDeletionService,
        private readonly OccurrenceDeletionService $occurrenceDeletionService,
        private readonly OpenImmoImportDeletionService $openImmoImportDeletionService,
        private readonly OpenImmoUploadDeletionService $openImmoUploadDeletionService,
        private readonly OpenImmoFtpTransferTaskDeletionService $openImmoFtpTransferTaskDeletionService,
        private readonly OpenImmoFtpCredentialDeletionService $openImmoFtpDeletionService,
        private readonly PersonDeletionService $personDeletionService,
        private readonly ProcessPropertyExcelImportTaskDeletionService $processPropertyExcelImportTaskDeletionService,
        private readonly PropertyExcelImportDeletionService $propertyExcelImportDeletionService,
        private readonly PropertyOwnerDeletionService $propertyOwnerDeletionService,
        private readonly PropertyUserDeletionService $propertyUserDeletionService,
        private readonly PropertyVacancyReportDeletionService $propertyVacancyReportDeletionService,
        private readonly SettlementConceptAccountUserDeletionService $settlementConceptAccountUserDeletionService,
        private readonly SettlementConceptDeletionService $settlementConceptDeletionService,
        private readonly SettlementConceptMatchingTaskDeletionService $settlementConceptMatchingTaskDeletionService,
        private readonly UserInvitationDeletionService $userInvitationDeletionService,
        private readonly WegweiserKommunePlaceMappingDeletionService $wegweiserKommunePlaceMappingDeletionService,
        private readonly UserRegistrationDeletionService $userRegistrationDeletionService,
        private readonly ImageDeletionService $imageDeletionService,
        private readonly DocumentDeletionService $documentDeletionService,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function deleteAccount(
        Account $account,
        DeletionType $deletionType = DeletionType::SOFT,
        ?AccountUser $deletedByAccountUser = null
    ): void {
        switch ($deletionType) {
            case DeletionType::SOFT:
                $this->softDeleteAccount(account: $account, deletedByAccountUser: $deletedByAccountUser);
                break;
            case DeletionType::HARD:
                $this->hardDeleteAccount(account: $account);
                break;
            default:
                throw new \RuntimeException(message: 'Unsupported DeletionType');
        }
    }

    public function deleteWmsLayer(Account $account, WmsLayer $wmsLayer): void
    {
        $this->hardDeleteWmsLayer(account: $account, wmsLayer: $wmsLayer);
    }

    private function softDeleteAccount(Account $account, AccountUser $deletedByAccountUser): void
    {
        if ($account->getParentAccount() === null) {
            throw new \RuntimeException(message: 'Can not delete a parent account');
        }

        $account
            ->setDeleted(deleted: true)
            ->setDeletedAt(deletedAt: new \DateTimeImmutable())
            ->setDeletedByAccountUser(deletedByAccountUser: $deletedByAccountUser);

        $this->entityManager->flush();
    }

    private function hardDeleteAccount(Account $account): void
    {
        if ($account->getParentAccount() === null) {
            throw new \RuntimeException(message: 'Can not delete a parent account');
        }

        $this->buildingUnitDeletionService->deleteBuildingUnitsByAccount(account: $account, deletionType: DeletionType::HARD);

        $this->documentTemplateDeletionService->deleteDocumentTemplatesByAccount(account: $account);

        $this->propertyExcelImportDeletionService->deletePropertyExcelImportsByAccount(account: $account);

        $this->settlementConceptMatchingTaskDeletionService->deleteSettlementConceptMatchingTasksByAccount(account: $account);

        $this->matchingTaskDeletionService->deleteMatchingTasksByAccount(account: $account);

        $this->processPropertyExcelImportTaskDeletionService->deleteProcessPropertyExcelImportTasksByAccount(account: $account);

        $this->lookingForPropertyRequestReportDeletionService->deleteLookingForPropertyRequestsByAccount(account: $account, deletionType: DeletionType::HARD);

        $this->lookingForPropertyRequestDeletionService->deleteLookingForPropertyRequestsByAccount(account: $account, deletionType: DeletionType::HARD);

        $this->businessLocationAreaDeletionService->deleteBusinessLocationAreasByAccount(account: $account);

        $this->openImmoImportDeletionService->deleteOpenImmoImportsByAccount(account: $account);

        $this->openImmoUploadDeletionService->deleteOpenImmoUploadsByAccount(account: $account);

        $this->openImmoFtpTransferTaskDeletionService->deleteOpenImmoFtpTransferTasksByAccount(account: $account);

        $this->openImmoFtpDeletionService->deleteOpenImmoFtpCredentialsByAccount(account: $account);

        $this->personDeletionService->deleteContactsByAccount(account: $account, deletionType: DeletionType::HARD, deletedByAccountUser: $account->getDeletedByAccountUser());

        $this->occurrenceDeletionService->deleteOccurrencesByAccount(account: $account, deletionType: DeletionType::HARD);

        $this->propertyOwnerDeletionService->deletePropertyOwnersByAccount(account: $account);

        $this->propertyUserDeletionService->deletePropertyUsersByAccount(account: $account);

        $this->propertyVacancyReportDeletionService->deletePropertyVacancyReportsByAccount(account: $account, deletionType: DeletionType::HARD);

        $this->settlementConceptDeletionService->deleteSettlementConceptByAccounts(account: $account);

        $this->settlementConceptAccountUserDeletionService->deleteSettlementConceptAccountUsersByAccount(account: $account, deletionType: DeletionType::HARD);

        $this->userInvitationDeletionService->deleteUserInvitationsByAccount(account: $account);

        $this->wegweiserKommunePlaceMappingDeletionService->deleteWegweiserKommunePlaceMappingsByAccount(account: $account);

        $this->accountUserDeletionService->deleteAccountUsersByAccount(account: $account, deletionType: DeletionType::HARD);

        $this->personDeletionService->deletePersonsByAccount(account: $account, deletionType: DeletionType::HARD);

        $this->userRegistrationDeletionService->deleteUserRegistrationsByAccount(account: $account);

        $this->hardDeleteAccountConfigurationByAccount(account: $account);

        $this->hardDeleteAccountLegalTextsConfigurationByAccount(account: $account);

        $this->hardDeleteAllWmsLayers(account: $account);

        $this->entityManager->remove(entity: $account);

        $this->entityManager->flush();
    }

    private function hardDeleteAccountConfiguration(AccountConfiguration $accountConfiguration): void
    {
        $logoImage = $accountConfiguration->getLogoImage();
        $cityExpose = $accountConfiguration->getCityExpose();

        if ($logoImage !== null) {
            $accountConfiguration->setLogoImage(null);

            $this->entityManager->flush();

            $this->imageDeletionService->deleteImage(image: $logoImage);
        }

        if ($cityExpose !== null) {
            $accountConfiguration->setCityExpose(null);

            $this->entityManager->flush();

            $this->documentDeletionService->deleteDocument(document: $cityExpose);
        }

        $this->entityManager->remove($accountConfiguration);
        $this->entityManager->flush();
    }

    private function hardDeleteAccountLegalTextsConfiguration(AccountLegalTextsConfiguration $accountLegalTextsConfiguration): void
    {
        $this->entityManager->remove($accountLegalTextsConfiguration);
        $this->entityManager->flush();
    }

    private function hardDeleteAccountConfigurationByAccount(Account $account): void
    {
        $accountConfiguration = $this->accountConfigurationRepository->findOneBy(criteria: ['account' => $account]);

        if ($accountConfiguration === null) {
            return;
        }

        $this->hardDeleteAccountConfiguration(accountConfiguration: $accountConfiguration);
    }

    private function hardDeleteAccountLegalTextsConfigurationByAccount(Account $account): void
    {
        $accountLegalTextsConfiguration = $this->accountLegalTextsConfigurationRepository->findOneBy(criteria: ['account' => $account]);

        if ($accountLegalTextsConfiguration === null) {
            return;
        }

        $this->hardDeleteAccountLegalTextsConfiguration(accountLegalTextsConfiguration: $accountLegalTextsConfiguration);
    }

    private function hardDeleteWmsLayer(Account $account, WmsLayer $wmsLayer): void
    {
        if ($account->getWmsLayers()->contains(element: $wmsLayer) === false) {
            return;
        }

        $account->getWmsLayers()->removeElement(element: $wmsLayer);

        $this->entityManager->remove(entity: $wmsLayer);

        $this->entityManager->flush();
    }

    private function hardDeleteAllWmsLayers(Account $account): void
    {
        foreach ($account->getWmsLayers() as $wmsLayer) {
            $this->hardDeleteWmsLayer(account: $account, wmsLayer: $wmsLayer);
        }
    }
}
