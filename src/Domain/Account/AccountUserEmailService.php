<?php

declare(strict_types=1);

namespace App\Domain\Account;

use App\Domain\Entity\AccountUser\AccountUserPasswordReset;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;

class AccountUserEmailService
{
    public function __construct(
        private readonly string $noReplyEmailAddress,
        private readonly MailerInterface $mailer
    ) {
    }

    public function sendAccountUserPasswordResetCreatedMail(AccountUserPasswordReset $accountUserPasswordReset): void
    {
        $accountUser = $accountUserPasswordReset->getAccountUser();

        if ($accountUser->getAccount()->getParentAccount() !== null) {
            $account = $accountUser->getAccount()->getParentAccount();
        } else {
            $account = $accountUser->getAccount();
        }

        $email = (new TemplatedEmail())
            ->from(addresses: $this->noReplyEmailAddress)
            ->to(
                addresses: new Address(
                    address: $accountUser->getEmail(),
                    name: $accountUser->getFullName()
                )
            )
            ->subject(subject: 'Neues Kennwort setzen')
            ->htmlTemplate(template: 'account_user/email/password_reset_created_email.html.twig')
            ->textTemplate(template: 'account_user/email/password_reset_created_email.txt.twig')
            ->context(context: [
                'passwordResetToken' => $accountUserPasswordReset->getPasswordToken(),
                'fullName'           => $accountUser->getFullName(),
                'account'            => $account,
            ]);

        $this->mailer->send(message: $email);
    }

    public function sendAccountUserCreatedMail(AccountUserPasswordReset $accountUserPasswordReset): void
    {
        $accountUser = $accountUserPasswordReset->getAccountUser();

        if ($accountUser->getAccount()->getParentAccount() !== null) {
            $account = $accountUser->getAccount()->getParentAccount();
        } else {
            $account = $accountUser->getAccount();
        }

        $email = (new TemplatedEmail())
            ->from(addresses: $this->noReplyEmailAddress)
            ->to(
                addresses: new Address(
                    address: $accountUser->getEmail(),
                    name: $accountUser->getFullName()
                )
            )
            ->subject(subject: 'Für Sie wurde ein Account in der Leerstands- und Ansiedlungsmanagementplattform der ' . $account->getName() . ' eingerichtet')
            ->htmlTemplate(template: 'account_user/email/account_user_created.html.twig')
            ->textTemplate(template: 'account_user/email/account_user_created.txt.twig')
            ->context(context: [
                'accountUser'              => $accountUser,
                'account'                  => $account,
                'accountUserPasswordReset' => $accountUserPasswordReset,
            ]);

        $this->mailer->send(message: $email);
    }

    public function sendAccountUserPasswordChangeSuccessfulMail(AccountUserPasswordReset $accountUserPasswordReset): void
    {
        $accountUser = $accountUserPasswordReset->getAccountUser();

        if ($accountUser->getAccount()->getParentAccount() !== null) {
            $account = $accountUser->getAccount()->getParentAccount();
        } else {
            $account = $accountUser->getAccount();
        }

        $email = (new TemplatedEmail())
            ->from(addresses: $this->noReplyEmailAddress)
            ->to(
                addresses: new Address(
                    address: $accountUser->getEmail(),
                    name: $accountUser->getFullName(),
                )
            )
            ->subject(subject: 'Kennwort geändert')
            ->htmlTemplate(template: 'account_user/email/password_reset_success_email.html.twig')
            ->textTemplate(template: 'account_user/email/password_reset_success_email.txt.twig')
            ->context(context: [
                'fullName' => $accountUser->getFullName(),
                'account'  => $account,
            ]);

        $this->mailer->send($email);
    }
}
