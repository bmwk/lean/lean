<?php

declare(strict_types=1);

namespace App\Domain\Account;

use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\AccountUser\AccountUserRole;
use App\Domain\Entity\AccountUser\AccountUserType;
use Symfony\Bundle\SecurityBundle\Security;

class AccountUserSecurityService
{
    public function __construct(
        private readonly Security $security
    ) {
    }

    /**
     * @return AccountUserType[]|null
     */
    public function getGrantedAccountUserTypes(): ?array
    {
        $accountUserTypes = [];

        $accountUserRoles = [
            AccountUserRole::ROLE_ACCOUNT_ADMIN,
            AccountUserRole::ROLE_USER,
            AccountUserRole::ROLE_VIEWER,
            AccountUserRole::ROLE_PROPERTY_FEEDER,
            AccountUserRole::ROLE_LOOKING_FOR_PROPERTY_REQUEST_FEEDER,
        ];

        foreach ($accountUserRoles as $accountUserRole) {
            if ($this->security->isGranted(attributes: $accountUserRole->value)) {
                $grantedAccountUserTypes = $this->getGrantedAccountUserTypesForAccountUserRole(accountUserRole: $accountUserRole);

                foreach ($grantedAccountUserTypes as $accountUserType) {
                    if (in_array(needle: $accountUserType, haystack: $accountUserTypes) === false) {
                        $accountUserTypes[] = $accountUserType;
                    }
                }
            }
        }

        if (empty($accountUserTypes) === true) {
            return null;
        }

        return $accountUserTypes;
    }

    public function canViewAccountUser(AccountUser $accountUser, AccountUser $user): bool
    {
        if ($accountUser === $user) {
            return true;
        }

        $account = $user->getAccount();

        if (
            $user->getAccountUserType() === AccountUserType::INTERNAL
            && $accountUser->getAccountUserType() === AccountUserType::INTERNAL
            && $this->security->isGranted(attributes: AccountUserRole::ROLE_ACCOUNT_ADMIN->value) === true
            && $accountUser->getAccount() === $account
        ) {
            return true;
        }

        if (
            $user->getAccountUserType() === AccountUserType::INTERNAL
            && $accountUser->getAccountUserType() !== AccountUserType::INTERNAL
            && (
                $this->security->isGranted(attributes: AccountUserRole::ROLE_ACCOUNT_ADMIN->value) === true
                || $this->security->isGranted(attributes: AccountUserRole::ROLE_VIEWER->value) === true
            )
            && (
                $accountUser->getAccount() === $account
                || $account->getSubAccounts()->contains(element: $accountUser->getAccount()) === true
            )
        ) {
            return true;
        }

        return false;
    }

    public function canEditAccountUser(AccountUser $accountUser, AccountUser $user): bool
    {
        if ($accountUser === $user) {
            return true;
        }

        $account = $user->getAccount();

        if (
            $user->getAccountUserType() === AccountUserType::INTERNAL
            && $accountUser->getAccountUserType() === AccountUserType::INTERNAL
            && $this->security->isGranted(attributes: AccountUserRole::ROLE_ACCOUNT_ADMIN->value) === true
            && $accountUser->getAccount() === $account
        ) {
            return true;
        }

        if (
            $user->getAccountUserType() === AccountUserType::INTERNAL
            && $accountUser->getAccountUserType() !== AccountUserType::INTERNAL
            && $this->security->isGranted(attributes: AccountUserRole::ROLE_ACCOUNT_ADMIN->value) === true
            && (
                $accountUser->getAccount() === $account
                || $account->getSubAccounts()->contains(element: $accountUser->getAccount()) === true
            )
        ) {
            return true;
        }

        return false;
    }

    public function canDeleteAccountUser(AccountUser $accountUser, AccountUser $user): bool
    {
        if (
            in_array(needle: $user->getAccountUserType(), haystack: [AccountUserType::PROPERTY_SEEKER, AccountUserType::PROPERTY_PROVIDER]) === true
            && $accountUser === $user
        ) {
            return true;
        }

        if (
            $user->getAccountUserType() === AccountUserType::INTERNAL
            && $this->security->isGranted(attributes: AccountUserRole::ROLE_ACCOUNT_ADMIN->value) === true
            && $accountUser !== $user
            && (
                $accountUser->getAccount() === $user->getAccount()
                || $user->getAccount()->getSubAccounts()->contains(element: $accountUser->getAccount()) === true
            )
        ) {
            return true;
        }

        return false;
    }

    /**
     * @param AccountUserRole $accountUserRole
     * @return AccountUserType[]|null
     */
    private function getGrantedAccountUserTypesForAccountUserRole(AccountUserRole $accountUserRole): ?array
    {
        return match ($accountUserRole) {
            AccountUserRole::ROLE_ACCOUNT_ADMIN => [
                AccountUserType::INTERNAL,
                AccountUserType::PROPERTY_PROVIDER,
                AccountUserType::PROPERTY_SEEKER,
            ],
            AccountUserRole::ROLE_USER, AccountUserRole::ROLE_VIEWER => [
                AccountUserType::PROPERTY_PROVIDER,
                AccountUserType::PROPERTY_SEEKER,
            ],
            AccountUserRole::ROLE_PROPERTY_FEEDER => [AccountUserType::PROPERTY_PROVIDER],
            AccountUserRole::ROLE_LOOKING_FOR_PROPERTY_REQUEST_FEEDER => [AccountUserType::PROPERTY_SEEKER],
            default => null
        };
    }
}
