<?php

declare(strict_types=1);

namespace App\Domain\Account;

use App\Domain\Anonymize\AnonymizeService;
use App\Domain\Entity\AccountUser\AccountUser;
use Doctrine\ORM\EntityManagerInterface;

class AccountUserAnonymizationService
{
    public function __construct(
        private readonly bool $anonymizeBackupEnabled,
        private readonly AnonymizeService $anonymizeService,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function anonymizeAccountUser(AccountUser $accountUser, AccountUser $anonymizedByAccountUser): void
    {
        $this->entityManager->beginTransaction();

        if ($this->anonymizeBackupEnabled === true) {
            $this->anonymizeService->backupObject($accountUser);
        }

        $anonymizedAccountUser = new AnonymizedAccountUser(accountUser: $accountUser);

        $accountUser
            ->setFullName($anonymizedAccountUser->getFullName())
            ->setNameAbbreviation($anonymizedAccountUser->getNameAbbreviation())
            ->setEmail($anonymizedAccountUser->getEmail())
            ->setPhoneNumber($anonymizedAccountUser->getPhoneNumber())
            ->setUsername($anonymizedAccountUser->getUsername())
            ->setPassword($anonymizedAccountUser->getPassword())
            ->setAnonymized(true)
            ->setAnonymizedAt(new \DateTimeImmutable())
            ->setAnonymizedByAccountUser($anonymizedByAccountUser);

        $this->entityManager->flush();
        $this->entityManager->commit();
    }
}
