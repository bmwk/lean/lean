<?php

declare(strict_types=1);

namespace App\Domain\Account;

use App\Domain\Entity\Account;
use App\Domain\Entity\AccountConfiguration;
use App\Domain\Entity\AccountLegalTextsConfiguration;
use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\Document;
use App\Domain\Entity\Image;
use App\Domain\Entity\UserRegistration\UserRegistration;
use App\Domain\File\FileService;
use App\Repository\AccountConfigurationRepository;
use App\Repository\AccountLegalTextsConfigurationRepository;
use App\Repository\AccountRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\RequestStack;


class AccountService
{
    public function __construct(
        private readonly FileService $fileService,
        private readonly AccountConfigurationRepository $accountConfigurationRepository,
        private readonly AccountLegalTextsConfigurationRepository $accountLegalTextsConfigurationRepository,
        private readonly AccountRepository $accountRepository,
        private readonly RequestStack $requestStack,
        private readonly EntityManagerInterface $entityManager,
        private readonly Security $security,
        private readonly string $projectDir
    ) {
    }

    /**
     * @return Account[]
     */
    public function fetchAllAccounts(): array
    {
        return $this->accountRepository->findBy(criteria: ['deleted' => false]);
    }

    /**
     * @return Account[]
     */
    public function fetchAllMainAccounts(): array
    {
        return $this->accountRepository->findBy(criteria: ['deleted' => false, 'parentAccount' => null]);
    }

    public function fetchAccountByAppHostname(string $appHostname): ?Account
    {
        return $this->accountRepository->findOneBy(criteria: ['appHostname' => $appHostname]);
    }

    public function createAndSaveLogoImageFromUploadedFile(UploadedFile $uploadedFile, AccountUser $accountUser): Image
    {
        $file = $this->fileService->saveFileFromUploadedFile(
            uploadedFile: $uploadedFile,
            account: $accountUser->getAccount(),
            public: true,
            accountUser: $accountUser
        );

        $image = new Image();

        $image
            ->setAccount($accountUser->getAccount())
            ->setCreatedByAccountUser($accountUser)
            ->setPublic(true)
            ->setFile($file);

        $this->entityManager->persist(entity: $image);

        $this->entityManager->flush();

        return $image;
    }

    public function createAndSaveCityExposeFromUploadedFile(UploadedFile $uploadedFile, AccountUser $accountUser): Document
    {
        $file = $this->fileService->saveFileFromUploadedFile(
            uploadedFile: $uploadedFile,
            account: $accountUser->getAccount(),
            public: true,
            accountUser: $accountUser
        );

        $document = new Document();

        $document
            ->setAccount($accountUser->getAccount())
            ->setCreatedByAccountUser($accountUser)
            ->setPublic(true)
            ->setFile($file);

        $this->entityManager->persist(entity: $document);

        $this->entityManager->flush();

        return $document;
    }

    public function getOperatorAccount(): ?Account
    {
        $user = $this->security->getUser();

        if ($user !== null) {
            if ($user->getAccount()->getParentAccount() !== null) {
                return $user->getAccount()->getParentAccount();
            } else {
                return $user->getAccount();
            }
        }

        return $this->fetchAccountByAppHostname($this->requestStack->getCurrentRequest()->getHost());
    }

    public function createAccountFromUserRegistration(UserRegistration $userRegistration): Account
    {
        $account = Account::createFromUserRegistration(userRegistration: $userRegistration);

        $account
            ->setEnabled(true)
            ->setDeleted(false)
            ->setAnonymized(false);

        return $account;
    }

    /**
     * @return Account[]
     */
    public function fetchSubAccountsFromAccount(Account $account): array
    {
        return $this->accountRepository->findBy(criteria: [
            'parentAccount' => $account,
            'deleted'       => false,
            'enabled'       => true,
        ]);
    }

    public function fetchAccountLegalTextsConfigurationByAccount(Account $account): ?AccountLegalTextsConfiguration
    {
        return $this->accountLegalTextsConfigurationRepository->findByAccount(account: $account);
    }

    public function fetchAccountConfigurationByAccount(Account $account): ?AccountConfiguration
    {
        return $this->accountConfigurationRepository->findByAccount(account: $account);
    }

    public function defaultPrivacyPolicyProviderText(): string
    {
        return file_get_contents(filename: $this->projectDir . '/assets/files/text_templates/default_privacy_policy_provider.html');
    }

    public function defaultPrivacyPolicySeekerText(): string
    {
        return file_get_contents(filename: $this->projectDir . '/assets/files/text_templates/default_privacy_policy_seeker.html');
    }

    public function defaultGtcProviderText(): string
    {
        return file_get_contents(filename: $this->projectDir . '/assets/files/text_templates/default_gtc_provider.html');
    }

    public function defaultGtcSeekerText(): string
    {
        return file_get_contents(filename: $this->projectDir . '/assets/files/text_templates/default_gtc_seeker.html');
    }

    public function defaultPdfExposeDisclaimerText(Account $account): string
    {
        return str_replace('Stadt Musterstadt', $account->getName(), file_get_contents(filename: $this->projectDir . '/assets/files/text_templates/default_pdf_expose_disclaimer.txt'));
    }
}
