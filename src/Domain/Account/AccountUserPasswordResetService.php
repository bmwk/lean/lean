<?php

declare(strict_types=1);

namespace App\Domain\Account;

use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\AccountUser\AccountUserPasswordReset;
use App\Repository\AccountUser\AccountUserPasswordResetRepository;
use Doctrine\ORM\EntityManagerInterface;

class AccountUserPasswordResetService
{
    private const PASSWORD_RESET_DAYS_UNTIL_EXPIRED = 7;

    public function __construct(
        private readonly AccountUserEmailService $accountUserEmailService,
        private readonly AccountUserPasswordResetRepository $accountUserPasswordResetRepository,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function doPasswordResetForAccountUser(
        AccountUser $accountUser,
        AccountUser $createdByAccountUser,
        int $daysUntilExpired = self::PASSWORD_RESET_DAYS_UNTIL_EXPIRED
    ): void {
        $accountUserPasswordReset = $this->fetchAccountUserPasswordResetByAccountUser(accountUser: $accountUser);

        if ($accountUserPasswordReset === null) {
            $accountUserPasswordReset = $this->createAccountUserPasswordReset(
                accountUser: $accountUser,
                createdByAccountUser: $createdByAccountUser,
                daysUntilExpired: $daysUntilExpired
            );

            $this->entityManager->persist(entity: $accountUserPasswordReset);
        } else {
            $accountUserPasswordReset->setExpirationDate(self::createExpirationDate(daysUntilExpired: $daysUntilExpired));
        }

        $this->entityManager->flush();

        $this->accountUserEmailService->sendAccountUserPasswordResetCreatedMail(accountUserPasswordReset: $accountUserPasswordReset);
    }

    public function createAccountUserPasswordReset(
        AccountUser $accountUser,
        AccountUser $createdByAccountUser,
        int $daysUntilExpired
    ): AccountUserPasswordReset {
        $expirationDate = self::createExpirationDate(daysUntilExpired: $daysUntilExpired);

        $passwordToken = self::generateActivationToken(uniqueIdentifier: $expirationDate->getTimestamp(), expirationDate: $expirationDate);

        $accountUserPasswordReset = new AccountUserPasswordReset();

        $accountUserPasswordReset
            ->setAccountUser($accountUser)
            ->setPasswordToken($passwordToken)
            ->setExpirationDate($expirationDate)
            ->setCreatedByAccountUser($createdByAccountUser);

        return $accountUserPasswordReset;
    }

    public function fetchAccountUserPasswordResetByToken(string $token, bool $active): ?AccountUserPasswordReset
    {
        return $this->accountUserPasswordResetRepository->findOneByToken(token: $token, active: $active);
    }

    public function fetchActiveAccountUserPasswordResetByToken(string $token): ?AccountUserPasswordReset
    {
        return $this->fetchAccountUserPasswordResetByToken(token: $token, active: true);
    }

    public function fetchAccountUserPasswordResetByAccountUser(AccountUser $accountUser, ?bool $active = null): ?AccountUserPasswordReset
    {
        return $this->accountUserPasswordResetRepository->findOneByAccountUser(accountUser: $accountUser, active: $active);
    }

    /**
     * @throws \Exception
     */
    private static function generateActivationToken(int $uniqueIdentifier, \DateTime $expirationDate): string
    {
        $algo = 'sha256';
        $salt = random_bytes(length: 22);
        $plain = sprintf('%s%s%s', $salt, $uniqueIdentifier, $expirationDate->format(format: 'Y-m-d H:i:s'));

        return hash(algo: $algo, data: $plain);
    }

    private static function createExpirationDate(int $daysUntilExpired): \DateTime
    {
        return (new \DateTime())->add(new \DateInterval(duration: 'P' . $daysUntilExpired . 'D'));
    }
}
