<?php

declare(strict_types=1);

namespace App\Domain\Account;

use App\Domain\Entity\AccountUser\AbstractAccountUser;
use App\Domain\Entity\AccountUser\AccountUser;

class AnonymizedAccountUser extends AbstractAccountUser
{
    public function __construct(AccountUser $accountUser)
    {
        $this->id = $accountUser->getId();

        $this->accountUserType = $accountUser->getAccountUserType();
        $this->fullName = self::createRandomString(length: 20);
        $this->nameAbbreviation = self::createRandomString(length: 20);
        $this->email = self::createRandomString(length: 20);
        $this->phoneNumber = self::createRandomString(length: 20);
        $this->username = self::createRandomString(length: 20);
        $this->password = self::createRandomString(length: 20);
        $this->roles = $accountUser->getRoles();
        $this->enabled = $accountUser->isEnabled();
        $this->account = $accountUser->getAccount();
        $this->deleted = $accountUser->isDeleted();
        $this->deletedAt = $accountUser->getDeletedAt();
        $this->anonymized = $accountUser->isAnonymized();
        $this->anonymizedAt = $accountUser->getAnonymizedAt();
        $this->anonymizedByAccountUser = $accountUser->getAnonymizedByAccountUser();
        $this->createdAt = $accountUser->getCreatedAt();
        $this->updatedAt = $accountUser->getUpdatedAt();
    }

    private static function createRandomString(int $length): string
    {
        return substr(string: bin2hex(string: random_bytes(length: $length)), offset: 0, length: $length);
    }
}
