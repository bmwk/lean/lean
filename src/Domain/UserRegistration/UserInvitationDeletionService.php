<?php

declare(strict_types=1);

namespace App\Domain\UserRegistration;

use App\Domain\Entity\Account;
use App\Domain\Entity\UserRegistration\UserInvitation;
use App\Repository\UserRegistration\UserInvitationRepository;
use Doctrine\ORM\EntityManagerInterface;

class UserInvitationDeletionService
{
    public function __construct(
        private readonly UserInvitationRepository $userInvitationRepository,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function deleteUserInvitation(UserInvitation $userInvitation): void
    {
        $this->hardDeleteUserInvitation(userInvitation: $userInvitation);
    }

    public function deleteUserInvitationsByAccount(Account $account): void
    {
        $this->hardDeleteUserInvitationsByAccount(account: $account);
    }

    private function hardDeleteUserInvitation(UserInvitation $userInvitation): void
    {
        $userInvitation->getUserRegistration()?->setUserInvitation(null);

        $this->entityManager->remove(entity: $userInvitation);
        $this->entityManager->flush();
    }

    private function hardDeleteUserInvitationsByAccount(Account $account): void
    {
        $userInvitations = $this->userInvitationRepository->findBy(criteria: ['account' => $account]);

        foreach ($userInvitations as $userInvitation) {
            $this->hardDeleteUserInvitation(userInvitation: $userInvitation);
        }
    }
}
