<?php

declare(strict_types=1);

namespace App\Domain\UserRegistration;

use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\AccountUser\AccountUserRole;
use App\Domain\Entity\UserRegistration\UserRegistration;

class UserRegistrationSecurityService extends AbstractUserRegistrationSecurityService
{
    public function canViewUserRegistration(UserRegistration $userRegistration, AccountUser $accountUser): bool
    {
        $account = $accountUser->getAccount();

        if (
            $account->getParentAccount() === null
            && (
                $this->security->isGranted(attributes: AccountUserRole::ROLE_USER->value) === true
                || $this->security->isGranted(attributes: AccountUserRole::ROLE_VIEWER->value) === true
            )
            && (
                $userRegistration->getAccount() === $account
                || $account->getSubAccounts()->contains(element: $userRegistration->getAccount()) === true
            )
        ) {
            return true;
        }

        return false;
    }

    public function canEditUserRegistration(UserRegistration $userRegistration, AccountUser $accountUser): bool
    {
        $account = $accountUser->getAccount();

        if (
            $account->getParentAccount() === null
            && $this->security->isGranted(attributes: AccountUserRole::ROLE_USER->value) === true
            && (
                $userRegistration->getAccount() === $account
                || $account->getSubAccounts()->contains(element: $userRegistration->getAccount()) === true
            )
        ) {
            return true;
        }

        return false;
    }

    public function canDeleteUserRegistration(UserRegistration $userRegistration, AccountUser $accountUser): bool
    {
        return $this->canEditUserRegistration(userRegistration: $userRegistration, accountUser: $accountUser);
    }
}
