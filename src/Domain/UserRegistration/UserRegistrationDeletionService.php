<?php

declare(strict_types=1);

namespace App\Domain\UserRegistration;

use App\Domain\Entity\Account;
use App\Domain\Entity\UserRegistration\UserRegistration;
use App\Repository\UserRegistration\UserRegistrationRepository;
use Doctrine\ORM\EntityManagerInterface;

class UserRegistrationDeletionService
{
    public function __construct(
        private readonly UserRegistrationRepository $userRegistrationRepository,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function deleteUserRegistration(UserRegistration $userRegistration): void
    {
        $this->hardDeleteUserRegistration(userRegistration: $userRegistration);
    }

    public function deleteUserRegistrationsByAccount(Account $account): void
    {
        $this->hardDeleteUserRegistrationsByAccount(account: $account);
    }

    private function hardDeleteUserRegistration(UserRegistration $userRegistration): void
    {
        $this->entityManager->remove(entity: $userRegistration);
        $this->entityManager->flush();
    }

    private function hardDeleteUserRegistrationsByAccount(Account $account): void
    {
        $userRegistrations = $this->userRegistrationRepository->findBy(criteria: ['account' => $account]);

        foreach ($userRegistrations as $userRegistration) {
            $this->hardDeleteUserRegistration(userRegistration: $userRegistration);
        }
    }
}
