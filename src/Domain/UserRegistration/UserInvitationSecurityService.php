<?php

declare(strict_types=1);

namespace App\Domain\UserRegistration;

use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\AccountUser\AccountUserRole;
use App\Domain\Entity\UserRegistration\UserInvitation;

class UserInvitationSecurityService extends AbstractUserRegistrationSecurityService
{
    public function canViewUserInvitation(UserInvitation $userInvitation, AccountUser $accountUser): bool
    {
        $account = $accountUser->getAccount();

        if (
            $account->getParentAccount() === null
            && (
                $this->security->isGranted(attributes: AccountUserRole::ROLE_USER->value) === true
                || $this->security->isGranted(attributes: AccountUserRole::ROLE_VIEWER->value) === true
            )
            && (
                $userInvitation->getAccount() === $account
                || $account->getSubAccounts()->contains(element: $userInvitation->getAccount()) === true
            )
        ) {
            return true;
        }

        return false;
    }

    public function canEditUserInvitation(UserInvitation $userInvitation, AccountUser $accountUser): bool
    {
        $account = $accountUser->getAccount();

        if (
            $account->getParentAccount() === null
            && $this->security->isGranted(attributes: AccountUserRole::ROLE_USER->value) === true
            && (
                $userInvitation->getAccount() === $account
                || $account->getSubAccounts()->contains(element: $userInvitation->getAccount()) === true
            )
        ) {
            return true;
        }

        return false;
    }

    public function canDeleteUserInvitation(UserInvitation $userInvitation, AccountUser $accountUser): bool
    {
        return $this->canEditUserInvitation(userInvitation: $userInvitation, accountUser: $accountUser);
    }
}
