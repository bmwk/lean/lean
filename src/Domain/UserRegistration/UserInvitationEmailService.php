<?php

declare(strict_types=1);

namespace App\Domain\UserRegistration;

use App\Domain\Entity\UserRegistration\UserInvitation;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;

class UserInvitationEmailService
{
    public function __construct(
        private readonly string $noReplyEmailAddress,
        private readonly MailerInterface $mailer
    ) {
    }

    public function sendUserInvitationEmail(UserInvitation $userInvitation): void
    {
        $email = (new TemplatedEmail())
            ->from(addresses: $this->noReplyEmailAddress)
            ->to(
                addresses: new Address(
                    address: $userInvitation->getEmail(),
                    name: $userInvitation->getFirstName() . ' ' . $userInvitation->getLastName()
                )
            )
            ->subject(subject: 'Einladung zur Registrierung in LeAn®')
            ->htmlTemplate(template: 'user_registration/email/invitation_email.html.twig')
            ->textTemplate(template: 'user_registration/email/invitation_email.txt.twig')
            ->context(context: ['userInvitation' => $userInvitation]);

        $this->mailer->send(message: $email);
    }

    public function sendUserInvitationAcceptedEmail(UserInvitation $userInvitation): void
    {
        $email = (new TemplatedEmail())
            ->from(addresses: $this->noReplyEmailAddress)
            ->to(
                addresses: new Address(
                    $userInvitation->getCreatedByAccountUser()->getEmail(),
                    $userInvitation->getCreatedByAccountUser()->getFullName()
                )
            )
            ->subject(subject: 'Eine Einladung zu LeAn® wurde angenommen')
            ->htmlTemplate(template: 'user_registration/email/invitation_accepted_email.html.twig')
            ->textTemplate(template: 'user_registration/email/invitation_accepted_email.txt.twig')
            ->context(context : ['userInvitation' => $userInvitation]);

        $this->mailer->send(message: $email);
    }

    public function sendUserInvitationDeletedEmail(UserInvitation $userInvitation): void
    {
        $email = (new TemplatedEmail())
            ->from(addresses: $this->noReplyEmailAddress)
            ->to(
                addresses: new Address(
                    address: $userInvitation->getEmail(),
                    name: $userInvitation->getFirstName() . ' ' . $userInvitation->getLastName()
                )
            )
            ->subject(subject: 'Ihre Einladung zu LeAn® wurde zurückgezogen')
            ->textTemplate(template: 'user_registration/email/invitation_deleted_email.txt.twig')
            ->htmlTemplate(template: 'user_registration/email/invitation_deleted_email.html.twig')
            ->context(context: ['userInvitation' => $userInvitation]);

        $this->mailer->send(message: $email);
    }

    public function sendUserInvitationOverdueEmail(UserInvitation $userInvitation): void
    {
        $email = (new TemplatedEmail())
            ->from(addresses: $this->noReplyEmailAddress)
            ->to(addresses: $userInvitation->getCreatedByAccountUser()->getEmail())
            ->subject(subject: 'Die Annahme einer Einladung zu LeAn® steht noch aus')
            ->textTemplate(template: 'user_registration/email/invitation_overdue_email.txt.twig')
            ->htmlTemplate(template: 'user_registration/email/invitation_overdue_email.html.twig')
            ->context(context: ['userInvitation' => $userInvitation]);

        $this->mailer->send(message: $email);
    }
}
