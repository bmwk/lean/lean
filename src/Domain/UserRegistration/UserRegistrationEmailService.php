<?php

declare(strict_types=1);

namespace App\Domain\UserRegistration;

use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\AccountUser\AccountUserRole;
use App\Domain\Entity\UserRegistration\UserRegistration;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;

class UserRegistrationEmailService
{
    public function __construct(
        private readonly string $noReplyEmailAddress,
        private readonly MailerInterface $mailer
    ) {
    }

    public function sendUserRegistrationEmail(UserRegistration $userRegistration): void
    {
        $email = (new TemplatedEmail())
            ->from(addresses: $this->noReplyEmailAddress)
            ->to(
                addresses: new Address(
                    address: $userRegistration->getEmail(),
                    name: $userRegistration->getFirstName() . ' ' . $userRegistration->getLastName()
                )
            )
            ->subject(subject: 'Bestätigung Ihrer LeAn®-Registrierung')
            ->htmlTemplate(template: 'user_registration/email/registration_email.html.twig')
            ->textTemplate(template: 'user_registration/email/registration_email.txt.twig')
            ->context(context: ['userRegistration' => $userRegistration]);

        $this->mailer->send(message: $email);
    }

    public function sendUserRegistrationConfirmationEmail(UserRegistration $userRegistration): void
    {
        $email = (new TemplatedEmail())
            ->from(addresses: $this->noReplyEmailAddress)
            ->to(
                addresses: new Address(
                    address: $userRegistration->getEmail(),
                    name: $userRegistration->getFirstName() . ' ' . $userRegistration->getLastName()
                )
            )
            ->subject(subject: 'Ihr Account in LeAn® wurde freigeschaltet')
            ->htmlTemplate(template: 'user_registration/email/registration_confirmation_email.html.twig')
            ->textTemplate(template: 'user_registration/email/registration_confirmation_email.txt.twig')
            ->context(context: ['userRegistration' => $userRegistration]);

        $this->mailer->send(message: $email);
    }

    public function sendUserRegistrationDeletedEmail(UserRegistration $userRegistration): void
    {
        $email = (new TemplatedEmail())
            ->from(addresses: $this->noReplyEmailAddress)
            ->to(
                addresses: new Address(
                    address: $userRegistration->getEmail(),
                    name: $userRegistration->getFirstName() . ' ' . $userRegistration->getLastName()
                )
            )
            ->subject(subject: 'Ihre Registrierung in LeAn® wurde gelöscht')
            ->htmlTemplate(template: 'user_registration/email/registration_dismissed_email.html.twig')
            ->textTemplate(template: 'user_registration/email/registration_dismissed_email.txt.twig')
            ->context(context: ['userRegistration' => $userRegistration]);

        $this->mailer->send(message: $email);
    }

    /**
     * @param AccountUser[] $adminAccountUsers
     */
    public function sendAdminInformationEmail(UserRegistration $userRegistration, array $adminAccountUsers): void
    {
        $adminAccountUsers = array_filter(
            array: $adminAccountUsers,
            callback: function (AccountUser $accountUser): bool {
                return (
                    in_array(needle: AccountUserRole::ROLE_ACCOUNT_ADMIN->value, haystack: $accountUser->getRoles()) === true
                    && $accountUser->isEnabled() === true
                );
            }
        );

        foreach ($adminAccountUsers as $adminAccountUser) {
            try {
                $email = (new TemplatedEmail())
                    ->from(addresses: $this->noReplyEmailAddress)
                    ->to(addresseses: new Address(address: $adminAccountUser->getEmail(), name: $adminAccountUser->getFullName()))
                    ->subject(subject: 'Neue Registrierung zur Freischaltung')
                    ->htmlTemplate(template: 'user_registration/email/new_registration_information_email.html.twig')
                    ->textTemplate(template: 'user_registration/email/new_registration_information_email.txt.twig')
                    ->context(context: ['userRegistration' => $userRegistration, 'account' => $adminAccountUser->getAccount()]);
                $this->mailer->send(message: $email);
            } catch (TransportExceptionInterface $transportException) {
            }
        }
    }
}
