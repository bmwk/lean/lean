<?php

declare(strict_types=1);

namespace App\Domain\UserRegistration;

use App\Domain\Entity\Account;
use App\Domain\Entity\SortingOption\SortingOption;
use App\Domain\Entity\UserRegistration\UserInvitation;
use App\Domain\Entity\UserRegistration\UserInvitationFilter;
use App\Domain\Entity\UserRegistration\UserInvitationSearch;
use App\Domain\Entity\UserRegistration\UserRegistrationType;
use App\Repository\UserRegistration\UserInvitationRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Component\Uid\Uuid;

class UserInvitationService
{
    public function __construct(
        private readonly UserInvitationEmailService $userInvitationEmailService,
        private readonly UserInvitationRepository $userInvitationRepository,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    /**
     * @param UserRegistrationType[]|null $userRegistrationTypes
     */
    public function fetchUserInvitationById(Account $account, int $id, ?array $userRegistrationTypes = null): ?UserInvitation
    {
        return $this->userInvitationRepository->findOneById(account: $account, id: $id, userRegistrationTypes: $userRegistrationTypes);
    }

    public function fetchUserInvitationByUuid(Uuid $uuid): ?UserInvitation
    {
        return $this->userInvitationRepository->findOneBy(criteria: ['uuid' => $uuid]);
    }

    /**
     * @param UserRegistrationType[]|null $userRegistrationTypes
     * @return UserInvitation[]
     */
    public function fetchUserInvitationsByAccount(
        Account $account,
        ?array $userRegistrationTypes = null,
        ?UserInvitationFilter $userInvitationFilter = null,
        ?UserInvitationSearch $userInvitationSearch = null
    ): array {
        return $this->userInvitationRepository->findByAccount(
            account: $account,
            userRegistrationTypes: $userRegistrationTypes,
            userInvitationFilter: $userInvitationFilter,
            userInvitationSearch: $userInvitationSearch
        );
    }

    /**
     * @param UserRegistrationType[]|null $userRegistrationTypes
     */
    public function fetchUserInvitationsPaginatedByAccount(
        Account $account,
        int $firstResult,
        int $maxResults,
        ?array $userRegistrationTypes = null,
        ?UserInvitationFilter $userInvitationFilter = null,
        ?UserInvitationSearch $userInvitationSearch = null,
        ?SortingOption $sortingOption = null
    ): Paginator {
        return $this->userInvitationRepository->findPaginatedByAccount(
            account: $account,
            firstResult: $firstResult,
            maxResults: $maxResults,
            userRegistrationTypes: $userRegistrationTypes,
            userInvitationFilter: $userInvitationFilter,
            userInvitationSearch: $userInvitationSearch,
            sortingOption: $sortingOption
        );
    }

    /**
     * @param int[] $ids
     * @param UserRegistrationType[]|null $userRegistrationTypes
     * @return UserInvitation[]
     */
    public function fetchUserInvitationsByIds(
        Account $account,
        array $ids,
        ?array $userRegistrationTypes = null,
        ?UserInvitationFilter $userInvitationFilter = null,
        ?UserInvitationSearch $userInvitationSearch = null
    ): array {
        return $this->userInvitationRepository->findByIds(
            account: $account,
            ids: $ids,
            userRegistrationTypes: $userRegistrationTypes,
            userInvitationFilter: $userInvitationFilter,
            userInvitationSearch: $userInvitationSearch
        );
    }

    /**
     * @return UserInvitation[]
     */
    public function fetchOverdueUserInvitations(): array
    {
        return $this->userInvitationRepository->findOverdueUserInvitations(daysUntilOverdue: 14);
    }

    public function countUserInvitationsByAccount(Account $account, ?array $userRegistrationTypes = null): int
    {
        return $this->userInvitationRepository->countByAccount(account: $account, userRegistrationTypes: $userRegistrationTypes);
    }

    public function resendUserInvitationToUser(UserInvitation $userInvitation): void
    {
        $this->userInvitationEmailService->sendUserInvitationEmail(userInvitation: $userInvitation);

        $userInvitation->setResendDate(new \DateTime());

        $this->entityManager->persist(entity: $userInvitation);

        $this->entityManager->flush();
    }
}
