<?php

declare(strict_types=1);

namespace App\Domain\UserRegistration;

use App\Domain\Entity\AccountUser\AccountUserRole;
use App\Domain\Entity\UserRegistration\UserRegistrationType;
use Symfony\Bundle\SecurityBundle\Security;

class AbstractUserRegistrationSecurityService
{
    public function __construct(
        protected readonly Security $security
    ) {
    }

    /**
     * @return UserRegistrationType[]|null
     */
    public function getGrantedUserRegistrationTypes(): ?array
    {
        $userRegistrationTypes = [];

        $accountUserRoles = [
            AccountUserRole::ROLE_USER,
            AccountUserRole::ROLE_VIEWER,
        ];

        foreach ($accountUserRoles as $accountUserRole) {
            if ($this->security->isGranted(attributes: $accountUserRole->value)) {
                $grantedUserRegistrationTypes = $this->getGrantedUserRegistrationTypesForAccountUserRole(accountUserRole: $accountUserRole);

                foreach ($grantedUserRegistrationTypes as $userRegistrationType) {
                    if (in_array(needle: $userRegistrationType, haystack: $userRegistrationTypes) === false) {
                        $userRegistrationTypes[] = $userRegistrationType;
                    }
                }
            }
        }

        if (empty($userRegistrationTypes) === true) {
            return null;
        }

        return $userRegistrationTypes;
    }

    /**
     * @param AccountUserRole $accountUserRole
     * @return UserRegistrationType[]|null
     */
    protected function getGrantedUserRegistrationTypesForAccountUserRole(AccountUserRole $accountUserRole): ?array
    {
        return match ($accountUserRole) {
            AccountUserRole::ROLE_USER, AccountUserRole::ROLE_VIEWER => [
                UserRegistrationType::PROVIDER,
                UserRegistrationType::SEEKER,
            ],
            default => null
        };
    }
}
