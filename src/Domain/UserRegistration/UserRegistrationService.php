<?php

declare(strict_types=1);

namespace App\Domain\UserRegistration;

use App\Domain\Account\AccountService;
use App\Domain\Account\AccountUserPasswordResetService;
use App\Domain\Account\AccountUserService;
use App\Domain\Entity\Account;
use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\Person\Contact;
use App\Domain\Entity\Person\Person;
use App\Domain\Entity\Person\PersonType;
use App\Domain\Entity\SortingOption\SortingOption;
use App\Domain\Entity\UserRegistration\UserRegistration;
use App\Domain\Entity\UserRegistration\UserRegistrationFilter;
use App\Domain\Entity\UserRegistration\UserRegistrationSearch;
use App\Domain\Entity\UserRegistration\UserRegistrationStatus;
use App\Domain\Entity\UserRegistration\UserRegistrationType;
use App\Repository\UserRegistration\UserRegistrationRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;

class UserRegistrationService
{
    public function __construct(
        private readonly UserRegistrationRepository $userRegistrationRepository,
        private readonly AccountService $accountService,
        private readonly AccountUserService $accountUserService,
        private readonly AccountUserPasswordResetService $accountUserPasswordResetService,
        private readonly UserRegistrationEmailService $userRegistrationEmailService,
        private readonly UserRegistrationDeletionService $userRegistrationDeletionService,
        private readonly UserInvitationDeletionService $userInvitationDeletionService,
        private readonly UserInvitationEmailService $userInvitationEmailService,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    /**
     * @param UserRegistrationType[]|null $userRegistrationTypes
     */
    public function fetchUserRegistrationById(Account $account, int $id, ?array $userRegistrationTypes = null): ?UserRegistration
    {
        return $this->userRegistrationRepository->findOneById(account: $account, id: $id, userRegistrationTypes: $userRegistrationTypes);
    }

    /**
     * @param UserRegistrationType[]|null $userRegistrationTypes
     */
    public function fetchUserRegistrationsPaginatedByAccount(
        Account $account,
        bool $withoutInvitation,
        int $firstResult,
        int $maxResults,
        ?array $userRegistrationTypes = null,
        ?UserRegistrationFilter $userRegistrationFilter = null,
        ?UserRegistrationSearch $userRegistrationSearch = null,
        ?SortingOption $sortingOption = null
    ): Paginator {
        return $this->userRegistrationRepository->findPaginatedByAccount(
            account: $account,
            withoutInvitation: $withoutInvitation,
            firstResult: $firstResult,
            maxResults: $maxResults,
            userRegistrationTypes: $userRegistrationTypes,
            userRegistrationFilter: $userRegistrationFilter,
            userRegistrationSearch: $userRegistrationSearch,
            sortingOption: $sortingOption
        );
    }

    /**
     * @param int[] $ids
     * @param UserRegistrationType[]|null $userRegistrationTypes
     * @return UserRegistration[]
     */
    public function fetchUserRegistrationsByIds(
        Account $account,
        array $ids,
        bool $withoutInvitation,
        ?array $userRegistrationTypes = null,
        ?UserRegistrationFilter $userRegistrationFilter = null,
        ?UserRegistrationSearch $userRegistrationSearch = null
    ): array {
        return $this->userRegistrationRepository->findByIds(
            account: $account,
            ids: $ids,
            withoutInvitation: $withoutInvitation,
            userRegistrationTypes: $userRegistrationTypes,
            userRegistrationFilter: $userRegistrationFilter,
            userRegistrationSearch: $userRegistrationSearch
        );
    }

    public function fetchUserRegistrationByValidActivationToken(string $activationToken): ?UserRegistration
    {
        return $this->userRegistrationRepository->findOneByValidActivationToken(activationToken: $activationToken);
    }

    /**
     * @return UserRegistration[]
     */
    public function fetchExpiredInactiveUserRegistrationsFromAllAccounts(): array
    {
        return $this->userRegistrationRepository->findExpiredInactiveUserRegistrationsFromAllAccounts();
    }

    /**
     * @param UserRegistrationType[]|null $userRegistrationTypes
     * @param UserRegistrationStatus[]|null $userRegistrationStatus
     */
    public function countUserRegistrationsByAccount(
        Account $account,
        ?array $userRegistrationTypes = null,
        ?array $userRegistrationStatus = null,
    ): int {
        return $this->userRegistrationRepository->countByAccount(
            account: $account,
            userRegistrationTypes: $userRegistrationTypes,
            userRegistrationStatus: $userRegistrationStatus
        );
    }

    /**
     * @throws \Exception
     */
    public function generateActivationToken(int $uniqueIdentifier, \DateTime $expirationDate): string
    {
        $salt = random_bytes(length: 22);
        $plain = sprintf('%s%s%s', $salt, $uniqueIdentifier, $expirationDate->format(format: 'Y-m-d H:i:s'));

        return hash(algo: 'sha256', data: $plain);
    }

    public function activateUser(UserRegistration $userRegistration, ?AccountUser $createdByAccountUser = null): void
    {
        $accountUser = $this->accountUserService->createAccountUserFromUserRegistration(userRegistration: $userRegistration);

        if ($userRegistration->getUserRegistrationType() === UserRegistrationType::PROVIDER) {
            $subAccount = $this->accountService->createAccountFromUserRegistration(userRegistration: $userRegistration);

            $this->entityManager->persist(entity: $subAccount);

            $accountUser->setAccount($subAccount);
        } else {
            $accountUser->setAccount($userRegistration->getAccount());
        }

        $userRegistration->setExpirationDate((new \DateTime())->sub(new \DateInterval(duration: 'PT48H')));

        $this->entityManager->persist(entity: $accountUser);

        $userRegistration->setUserRegistrationStatus(UserRegistrationStatus::ACTIVE);

        $this->entityManager->flush();

        $person = Person::createFromUserRegistration(userRegistration: $userRegistration);

        $person
            ->setAccount($userRegistration->getAccount())
            ->setCreatedByAccountUser($createdByAccountUser ?? $accountUser)
            ->setDeleted(false)
            ->setAnonymized(false);

        $this->entityManager->persist(entity: $person);

        if ($userRegistration->getPersonType() === PersonType::COMPANY) {
            $contact = Contact::createFromUserRegistration(userRegistration: $userRegistration);

            $contact
                ->setPerson($person)
                ->setAccount($userRegistration->getAccount())
                ->setCreatedByAccountUser($createdByAccountUser ?? $accountUser)
                ->setDeleted(false)
                ->setAnonymized(false);

            $this->entityManager->persist(entity: $contact);
        }

        $accountUser->setPerson($person);

        $this->entityManager->flush();

        if ($userRegistration->getUserInvitation() !== null) {
            $this->userInvitationEmailService->sendUserInvitationAcceptedEmail(userInvitation: $userRegistration->getUserInvitation());
        }

        $this->userRegistrationEmailService->sendUserRegistrationConfirmationEmail(userRegistration: $userRegistration);

        $this->accountUserPasswordResetService->doPasswordResetForAccountUser(
            accountUser: $accountUser,
            createdByAccountUser: $createdByAccountUser ?? $accountUser
        );

        if ($userRegistration->getUserInvitation() !== null) {
            $this->userInvitationDeletionService->deleteUserInvitation(userInvitation: $userRegistration->getUserInvitation());
        }

        $this->userRegistrationDeletionService->deleteUserRegistration(userRegistration: $userRegistration);
    }
}
