<?php

declare(strict_types=1);

namespace App\Domain\Person;

use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\AccountUser\AccountUserRole;
use App\Domain\Entity\Person\Person;
use Symfony\Bundle\SecurityBundle\Security;

class PersonSecurityService
{
    public function __construct(
        private readonly Security $security
    ) {
    }

    public function canCreatePerson(): bool
    {
        if (
            $this->security->isGranted(AccountUserRole::ROLE_USER->value) === true
            || $this->security->isGranted(AccountUserRole::ROLE_PROPERTY_FEEDER->value) === true
            || $this->security->isGranted(AccountUserRole::ROLE_LOOKING_FOR_PROPERTY_REQUEST_FEEDER->value) === true
        ) {
            return true;
        }

        return false;
    }

    public function canViewOnly(): bool
    {
        if (
            $this->security->isGranted(AccountUserRole::ROLE_USER->value) === true
            || $this->security->isGranted(AccountUserRole::ROLE_PROPERTY_FEEDER->value) === true
            || $this->security->isGranted(AccountUserRole::ROLE_LOOKING_FOR_PROPERTY_REQUEST_FEEDER->value) === true
        ) {
            return false;
        }

        return true;
    }

    public function canViewOnlyOwnPersons(): bool
    {
        if (
            $this->security->isGranted(AccountUserRole::ROLE_USER->value) === true
            || $this->security->isGranted(AccountUserRole::ROLE_VIEWER->value) === true
        ) {
            return false;
        }

        return true;
    }

    public function canViewPerson(Person $person, AccountUser $accountUser): bool
    {
        $account = $accountUser->getAccount();

        if (
            (
                $this->security->isGranted(AccountUserRole::ROLE_USER->value) === true
                || $this->security->isGranted(AccountUserRole::ROLE_VIEWER->value) === true
                || $this->security->isGranted(AccountUserRole::ROLE_PROPERTY_FEEDER->value) === true
                || $this->security->isGranted(AccountUserRole::ROLE_LOOKING_FOR_PROPERTY_REQUEST_FEEDER->value) === true
            )
            && (
                $person->getAccount() === $account
                || $account->getSubAccounts()->contains(element: $person->getAccount()) === true
            )
        ) {
            return true;
        }

        return false;
    }

    public function canEditPerson(Person $person, AccountUser $accountUser): bool
    {
        $account = $accountUser->getAccount();

        if (
            $this->security->isGranted(AccountUserRole::ROLE_USER->value) === true
            && (
                $person->getAccount() === $account
                || $account->getSubAccounts()->contains(element: $person->getAccount()) === true
            )
        ) {
            return true;
        }

        if (
            (
                $this->security->isGranted(AccountUserRole::ROLE_PROPERTY_FEEDER->value) === true
                || $this->security->isGranted(AccountUserRole::ROLE_LOOKING_FOR_PROPERTY_REQUEST_FEEDER->value) === true
            )
            && (
                $person->getAccount() === $account
                && $person->getCreatedByAccountUser() === $accountUser
            )
        ) {
            return true;
        }

        return false;
    }

    public function canDeletePerson(Person $person, AccountUser $accountUser): bool
    {
        return $this->canEditPerson(person: $person, accountUser: $accountUser);
    }
}
