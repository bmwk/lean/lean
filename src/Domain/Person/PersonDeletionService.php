<?php

declare(strict_types=1);

namespace App\Domain\Person;

use App\Domain\Entity\Account;
use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\DeletionType;
use App\Domain\Entity\Person\Contact;
use App\Domain\Entity\Person\Person;
use App\Repository\Person\ContactRepository;
use App\Repository\Person\PersonRepository;
use Doctrine\ORM\EntityManagerInterface;

class PersonDeletionService
{
    public function __construct(
        private readonly ContactRepository $contactRepository,
        private readonly PersonAnonymizationService $personAnonymizationService,
        private readonly PersonRepository $personRepository,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function deletePerson(
        Person $person,
        DeletionType $deletionType = DeletionType::ANONYMIZATION,
        ?AccountUser $deletedByAccountUser = null
    ): void {
        switch ($deletionType) {
            case DeletionType::ANONYMIZATION:
                $this->anonymizationDeletePerson(person: $person, deletedByAccountUser: $deletedByAccountUser);
                break;
            case DeletionType::HARD:
                $this->hardDeletePerson(person: $person);
                break;
            default:
                throw new \RuntimeException(message: 'Unsupported DeletionType');
        }
    }

    public function deletePersonsByAccount(
        Account $account,
        DeletionType $deletionType = DeletionType::ANONYMIZATION,
        ?AccountUser $deletedByAccountUser = null
    ): void {
        switch ($deletionType) {
            case DeletionType::ANONYMIZATION:
                $this->anonymizationDeletePersonsByAccount(account: $account, deletedByAccountUser: $deletedByAccountUser);
                break;
            case DeletionType::HARD:
                $this->hardDeletePersonsByAccount(account: $account);
                break;
            default:
                throw new \RuntimeException(message: 'Unsupported DeletionType');
        }
    }

    public function deleteContact(
        Contact $contact,
        DeletionType $deletionType = DeletionType::ANONYMIZATION,
        ?AccountUser $deletedByAccountUser = null
    ): void {
        switch ($deletionType) {
            case DeletionType::ANONYMIZATION:
                $this->anonymizationDeleteContact(contact: $contact, deletedByAccountUser: $deletedByAccountUser);
                break;
            case DeletionType::HARD:
                $this->hardDeleteContact(contact: $contact);
                break;

            default:
                throw new \RuntimeException(message: 'Unsupported DeletionType');
        }
    }

    public function deleteContactsByAccount(
        Account $account,
        DeletionType $deletionType = DeletionType::ANONYMIZATION,
        ?AccountUser $deletedByAccountUser = null
    ): void {
        switch ($deletionType) {
            case DeletionType::ANONYMIZATION:
                $this->anonymizationDeleteContactsByAccount(account: $account, deletedByAccountUser: $deletedByAccountUser);
                break;
            case DeletionType::HARD:
                $this->hardDeleteContactsByAccount(account: $account);
                break;
            default:
                throw new \RuntimeException(message: 'Unsupported DeletionType');
        }
    }

    private function anonymizationDeletePerson(Person $person, AccountUser $deletedByAccountUser): void
    {
        $this->personAnonymizationService->anonymizePerson(person: $person, anonymizedByAccountUser: $deletedByAccountUser);

        foreach ($person->getContacts() as $contact) {
            $this->anonymizationDeleteContact(contact: $contact, deletedByAccountUser: $deletedByAccountUser);
        }

        $person
            ->setDeletedByAccountUser($deletedByAccountUser)
            ->setDeletedAt(deletedAt: new \DateTimeImmutable())
            ->setDeleted(deleted: true);

        $this->entityManager->flush();
    }

    private function hardDeletePerson(Person $person): void
    {
        $this->entityManager->remove(entity: $person);
        $this->entityManager->flush();
    }

    private function anonymizationDeletePersonsByAccount(Account $account, AccountUser $deletedByAccountUser): void
    {
        foreach ($this->personRepository->findBy(criteria: ['account' => $account]) as $person) {
            $this->anonymizationDeletePerson(person: $person, deletedByAccountUser: $deletedByAccountUser);
        }
    }

    private function hardDeletePersonsByAccount(Account $account): void
    {
        foreach ($this->personRepository->findBy(criteria: ['account' => $account]) as $person) {
            $this->hardDeletePerson(person: $person);
        }
    }

    private function hardDeleteContact(Contact $contact): void
    {
        $this->entityManager->remove(entity: $contact);
        $this->entityManager->flush();
    }

    private function anonymizationDeleteContact(Contact $contact, AccountUser $deletedByAccountUser): void
    {
        $this->personAnonymizationService->anonymizeContact(contact: $contact, anoymizedByAccountUser: $deletedByAccountUser);

        $contact
            ->setDeleted(deleted: true)
            ->setDeletedAt(deletedAt: new \DateTimeImmutable())
            ->setDeletedByAccountUser(deletedByAccountUser: $deletedByAccountUser);

        $this->entityManager->flush();
    }

    private function anonymizationDeleteContactsByAccount(Account $account, AccountUser $deletedByAccountUser): void
    {
        foreach ($this->contactRepository->findBy(criteria: ['account' => $account]) as $contact) {
            $this->anonymizationDeleteContact(contact: $contact, deletedByAccountUser: $deletedByAccountUser);
        }
    }

    private function hardDeleteContactsByAccount(Account $account): void
    {
        $contacts = $this->contactRepository->findBy(criteria: ['account' => $account]);

        foreach ($contacts as $contact) {
            $this->hardDeleteContact(contact: $contact);
        }
    }

    public function unsetContactCreatedByAccountUserForAccountUser(AccountUser $accountUser): void
    {
        $contacts = $this->contactRepository->findBy(criteria: ['createdByAccountUser' => $accountUser]);

        foreach ($contacts as $contact) {
            $contact->setCreatedByAccountUser(null);
        }

        $this->entityManager->flush();
    }
}
