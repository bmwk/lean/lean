<?php

declare(strict_types=1);

namespace App\Domain\Person;

use App\Domain\Anonymize\AnonymizeService;
use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\Person\AnonymizedContact;
use App\Domain\Entity\Person\AnonymizedPerson;
use App\Domain\Entity\Person\Contact;
use App\Domain\Entity\Person\Person;
use Doctrine\ORM\EntityManagerInterface;

class PersonAnonymizationService
{
    public function __construct(
        private readonly bool $anonymizeBackupEnabled,
        private readonly AnonymizeService $anonymizeService,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function anonymizePerson(Person $person, AccountUser $anonymizedByAccountUser): void
    {
        $this->entityManager->beginTransaction();

        if ($this->anonymizeBackupEnabled === true) {
            $this->anonymizeService->backupObject($person);
        }

        $anonymizedPerson = new AnonymizedPerson(person: $person);

        $person
            ->setLegalEntityType($anonymizedPerson->getLegalEntityType())
            ->setSalutation($anonymizedPerson->getSalutation())
            ->setPersonTitle($anonymizedPerson->getPersonTitle())
            ->setName($anonymizedPerson->getName())
            ->setFirstName($anonymizedPerson->getFirstName())
            ->setBirthName($anonymizedPerson->getBirthName())
            ->setStreetName($anonymizedPerson->getStreetName())
            ->setHouseNumber($anonymizedPerson->getHouseNumber())
            ->setPostalCode($anonymizedPerson->getPostalCode())
            ->setPlaceName($anonymizedPerson->getPlaceName())
            ->setAdditionalAddressInformation($anonymizedPerson->getAdditionalAddressInformation())
            ->setEmail($anonymizedPerson->getEmail())
            ->setMobilePhoneNumber($anonymizedPerson->getMobilePhoneNumber())
            ->setPhoneNumber($anonymizedPerson->getPhoneNumber())
            ->setFaxNumber($anonymizedPerson->getFaxNumber())
            ->setWebsite($anonymizedPerson->getWebsite())
            ->setAnonymized(true)
            ->setAnonymizedAt(new \DateTimeImmutable())
            ->setAnonymizedByAccountUser($anonymizedByAccountUser);

        $this->entityManager->flush();
        $this->entityManager->commit();
    }

    public function anonymizeContact(Contact $contact, AccountUser $anoymizedByAccountUser): void
    {
        $this->entityManager->beginTransaction();

        if ($this->anonymizeBackupEnabled === true) {
            $this->anonymizeService->backupObject($contact);
        }

        $anoymizedContact = new AnonymizedContact(contact: $contact);

        $contact
            ->setId(id: $anoymizedContact->getId())
            ->setPerson(person: $anoymizedContact->getPerson())
            ->setSalutation(salutation: $anoymizedContact->getSalutation())
            ->setPersonTitle(personTitle: $anoymizedContact->getPersonTitle())
            ->setFirstName(firstName: $anoymizedContact->getFirstName())
            ->setName(name: $anoymizedContact->getName())
            ->setEmail(email: $anoymizedContact->getEmail())
            ->setMobilePhoneNumber(mobilePhoneNumber: $anoymizedContact->getMobilePhoneNumber())
            ->setPhoneNumber(phoneNumber: $anoymizedContact->getPhoneNumber())
            ->setFaxNumber(faxNumber: $anoymizedContact->getFaxNumber())
            ->setDepartment(department: $anoymizedContact->getDepartment())
            ->setPosition(position: $anoymizedContact->getPosition())
            ->setAnonymizedByAccountUser($anoymizedByAccountUser)
            ->setAnonymized(anonymized: true)
            ->setAnonymizedAt(anonymizedAt: new \DateTimeImmutable());

        $this->entityManager->commit();
    }
}
