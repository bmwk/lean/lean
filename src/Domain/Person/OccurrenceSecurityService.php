<?php

declare(strict_types=1);

namespace App\Domain\Person;

use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\AccountUser\AccountUserRole;
use App\Domain\Entity\Person\Occurrence;
use Symfony\Bundle\SecurityBundle\Security;

class OccurrenceSecurityService
{
    public function __construct(
        private readonly Security $security
    ) {
    }

    public function canViewOnlyOwnOccurrences(): bool
    {
        if (
            $this->security->isGranted(AccountUserRole::ROLE_USER->value) === true
            || $this->security->isGranted(AccountUserRole::ROLE_VIEWER->value) === true
        ) {
            return false;
        }

        return true;
    }

    public function canViewOccurrence(Occurrence $occurrence, AccountUser $accountUser): bool
    {
        $account = $accountUser->getAccount();

        if (
            $account->getParentAccount() === null
            && (
                $this->security->isGranted(attributes: AccountUserRole::ROLE_USER->value) === true
                || $this->security->isGranted(attributes: AccountUserRole::ROLE_VIEWER->value) === true
            )
            && (
                $occurrence->getAccount() === $account
                || $account->getSubAccounts()->contains(element: $occurrence->getAccount()) === true
            )
        ) {
            return true;
        }

        if (
            $account->getParentAccount() === null
            && (
                $this->security->isGranted(attributes: AccountUserRole::ROLE_PROPERTY_FEEDER->value) === true
                || $this->security->isGranted(attributes: AccountUserRole::ROLE_LOOKING_FOR_PROPERTY_REQUEST_FEEDER->value) === true
            )
            && (
                $occurrence->getAccount() === $account
                || $account->getSubAccounts()->contains(element: $occurrence->getAccount()) === true
            )
            && $occurrence->getPerformedWithAccountUser() === $accountUser
        ) {
            return true;
        }

        return false;
    }

    public function canEditOccurrence(Occurrence $occurrence, AccountUser $accountUser): bool
    {
        $account = $accountUser->getAccount();

        if (
            $accountUser->getAccount()->getParentAccount() === null
            && $this->security->isGranted(attributes: AccountUserRole::ROLE_USER->value) === true
            && (
                $occurrence->getAccount() === $account
                || $account->getSubAccounts()->contains(element: $occurrence->getAccount()) === true
            )
        ) {
            return true;
        }

        if (
            $account->getParentAccount() === null
            && (
                $this->security->isGranted(attributes: AccountUserRole::ROLE_PROPERTY_FEEDER->value) === true
                || $this->security->isGranted(attributes: AccountUserRole::ROLE_LOOKING_FOR_PROPERTY_REQUEST_FEEDER->value) === true
            )
            && (
                $occurrence->getAccount() === $account
                || $account->getSubAccounts()->contains(element: $occurrence->getAccount()) === true
            )
            && $occurrence->getPerformedWithAccountUser() === $accountUser
        ) {
            return true;
        }

        return false;
    }

    public function canDeleteOccurrence(Occurrence $occurrence, AccountUser $accountUser): bool
    {
        $account = $accountUser->getAccount();

        if (
            $accountUser->getAccount()->getParentAccount() === null
            && $this->security->isGranted(attributes: AccountUserRole::ROLE_ACCOUNT_ADMIN->value) === true
            && (
                $occurrence->getAccount() === $account
                || $account->getSubAccounts()->contains(element: $occurrence->getAccount()) === true
            )
        ) {
            return true;
        }

        if (
            $accountUser->getAccount()->getParentAccount() === null
            && (
                $this->security->isGranted(attributes: AccountUserRole::ROLE_USER->value) === true
                || $this->security->isGranted(attributes: AccountUserRole::ROLE_PROPERTY_FEEDER->value) === true
                || $this->security->isGranted(attributes: AccountUserRole::ROLE_LOOKING_FOR_PROPERTY_REQUEST_FEEDER->value) === true
            )
            && (
                $occurrence->getAccount() === $account
                || $account->getSubAccounts()->contains(element: $occurrence->getAccount()) === true
            )
            && $occurrence->getPerformedWithAccountUser() === $accountUser
        ) {
            return true;
        }

        return false;
    }
}
