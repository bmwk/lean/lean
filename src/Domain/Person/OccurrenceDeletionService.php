<?php

declare(strict_types=1);

namespace App\Domain\Person;

use App\Domain\Entity\Account;
use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\DeletionType;
use App\Domain\Entity\Person\Occurrence;
use App\Domain\Entity\Person\OccurrenceAttachment;
use App\Domain\File\FileDeletionService;
use App\Domain\Notification\NotificationDeletionService;
use App\Repository\Person\OccurrenceRepository;
use Doctrine\ORM\EntityManagerInterface;

class OccurrenceDeletionService
{
    public function __construct(
        private readonly FileDeletionService $fileDeletionService,
        private readonly NotificationDeletionService $notificationDeletionService,
        private readonly OccurrenceRepository $occurrenceRepository,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function deleteOccurrence(
        Occurrence $occurrence,
        DeletionType $deletionType = DeletionType::SOFT,
        ?AccountUser $deletedByAccountUser = null
    ): void {
        switch ($deletionType) {
            case DeletionType::SOFT:
                $this->softDeleteOccurrence(occurrence: $occurrence, deletedByAccountUser: $deletedByAccountUser);
                break;
            case DeletionType::HARD:
                $this->hardDeleteOccurrence(occurrence: $occurrence);
                break;
            default:
                throw new \RuntimeException(message: 'Invalid DeletionType');
        }
    }

    public function deleteOccurrencesByAccount(
        Account $account,
        DeletionType $deletionType = DeletionType::SOFT,
        ?AccountUser $deletedByAccountUser = null
    ): void {
        switch ($deletionType) {
            case DeletionType::SOFT:
                $this->softDeleteOccurrencesByAccount(account: $account, deletedByAccountUser: $deletedByAccountUser);
                break;
            case DeletionType::HARD:
                $this->hardDeleteOccurrencesByAccount(account: $account);
                break;
            default:
                throw new \RuntimeException(message: 'Unsupported deletionType');
        }
    }

    public function deleteOccurrenceAttachment(OccurrenceAttachment $occurrenceAttachment): void
    {
        $this->hardDeleteOccurrenceAttachment(occurrenceAttachment: $occurrenceAttachment);
    }

    private function softDeleteOccurrence(Occurrence $occurrence, AccountUser $deletedByAccountUser): void
    {
        $occurrence
            ->setDeleted(deleted: true)
            ->setDeletedAt(deletedAt: new \DateTimeImmutable())
            ->setDeletedByAccountUser(deletedByAccountUser: $deletedByAccountUser);

        $this->entityManager->flush();
    }

    private function hardDeleteOccurrence(Occurrence $occurrence): void
    {
        foreach ($occurrence->getOccurrenceAttachments() as $occurrenceAttachment) {
            $this->hardDeleteOccurrenceAttachment(occurrenceAttachment: $occurrenceAttachment);
        }

        $this->notificationDeletionService->deleteNotificationsByOccurrence(occurrence: $occurrence);

        $this->entityManager->remove($occurrence);
        $this->entityManager->flush();
    }

    private function softDeleteOccurrencesByAccount(Account $account, AccountUser $deletedByAccountUser): void
    {
        $occurrences = $this->occurrenceRepository->findBy(criteria: ['account' => $account]);

        foreach ($occurrences as $occurrence) {
            $this->softDeleteOccurrence(occurrence: $occurrence, deletedByAccountUser: $deletedByAccountUser);
        }
    }

    private function hardDeleteOccurrencesByAccount(Account $account): void
    {
        $occurrences = $this->occurrenceRepository->findBy(criteria: ['account' => $account]);

        foreach ($occurrences as $occurrence) {
            $this->hardDeleteOccurrence(occurrence: $occurrence);
        }
    }

    private function hardDeleteOccurrenceAttachment(OccurrenceAttachment $occurrenceAttachment): void
    {
        $this->entityManager->remove($occurrenceAttachment);
        $this->entityManager->flush();

        $this->fileDeletionService->deleteFile(file: $occurrenceAttachment->getFile());
    }
}
