<?php

declare(strict_types=1);

namespace App\Domain\Person;

use App\Domain\Entity\Account;
use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\Notification\NotificationType;
use App\Domain\Entity\Person\Contact;
use App\Domain\Entity\Person\Occurrence;
use App\Domain\Entity\Person\OccurrenceAttachment;
use App\Domain\Entity\Person\Person;
use App\Domain\Entity\Person\PersonFilter;
use App\Domain\Entity\Person\PersonSearch;
use App\Domain\Entity\Person\PersonType;
use App\Domain\Entity\Property\BuildingUnit;
use App\Domain\Entity\SortingOption\SortingOption;
use App\Domain\File\FileService;
use App\Domain\Notification\NotificationService;
use App\Repository\Person\ContactRepository;
use App\Repository\Person\OccurrenceRepository;
use App\Repository\Person\PersonRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class PersonService
{
    public function __construct(
        private readonly PersonRepository $personRepository,
        private readonly OccurrenceRepository $occurrenceRepository,
        private readonly ContactRepository $contactRepository,
        private readonly FileService $fileService,
        private readonly NotificationService $notificationService,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    /**
     * @return Person[]
     */
    public function fetchPersonsByAccount(
        Account $account,
        ?bool $anonymized = null,
        ?AccountUser $createdByAccountUser = null,
        ?PersonFilter $personFilter = null,
        ?PersonSearch $personSearch = null,
        ?SortingOption $sortingOption = null
    ): array {
        return $this->personRepository->findByAccount(
            account: $account,
            anonymized: $anonymized,
            createdByAccountUser: $createdByAccountUser,
            personFilter: $personFilter,
            personSearch: $personSearch,
            sortingOption: $sortingOption
        );
    }

    public function fetchPersonsPaginatedByAccount(
        Account $account,
        int $firstResult,
        int $maxResults,
        ?bool $anonymized = null,
        ?AccountUser $createdByAccountUser = null,
        ?PersonFilter $personFilter = null,
        ?PersonSearch $personSearch = null,
        ?SortingOption $sortingOption = null
    ): Paginator {
        return $this->personRepository->findPaginatedByAccount(
            account: $account,
            firstResult: $firstResult,
            maxResults: $maxResults,
            anonymized: $anonymized,
            createdByAccountUser: $createdByAccountUser,
            personFilter: $personFilter,
            personSearch: $personSearch,
            sortingOption: $sortingOption
        );
    }

    /**
     * @param int[] $ids
     * @return Person[]
     */
    public function fetchPersonsByIds(Account $account, array $ids): array
    {
        return $this->personRepository->findByIds(account: $account, ids: $ids);
    }

    public function fetchPersonById(Account $account, int $id): ?Person
    {
        return $this->personRepository->findOneById(account: $account, id: $id);
    }

    /**
     * @param Account[] $accounts
     */
    public function fetchPersonByPerson(array $accounts, Person $person): ?Person
    {
        $criteria = [
            'account'    => $accounts,
            'deleted'    => false,
            'personType' => $person->getPersonType(),
            'name'       => $person->getName(),
        ];

        if ($person->getPersonType() === PersonType::NATURAL_PERSON) {
            $criteria['salutation'] = $person->getSalutation();
            $criteria['personTitle'] = $person->getPersonTitle();
            $criteria['firstName'] = $person->getFirstName();
        }

        $criteria['phoneNumber'] = $person->getPhoneNumber();
        $criteria['email'] = $person->getEmail();

        if ($person->getStreetName() !== null) {
            $criteria['streetName'] = $person->getStreetName();
        }

        if ($person->getHouseNumber() !== null) {
            $criteria['houseNumber'] = $person->getHouseNumber();
        }

        if ($person->getPostalCode() !== null) {
            $criteria['postalCode'] = $person->getPostalCode();
        }

        if ($person->getPlaceName() !== null) {
            $criteria['placeName'] = $person->getPlaceName();
        }

        return $this->personRepository->findOneBy(criteria: $criteria);
    }

    public function countPersonsByAccount(Account $account, bool $anonymized, ?PersonFilter $personFilter = null): int
    {
        return $this->personRepository->countByAccount(
            account: $account,
            anonymized: $anonymized,
            personFilter: $personFilter
        );
    }

    public function fetchPaginatedContactsByPerson(
        Person $person,
        int $firstResult,
        int $maxResults,
        ?SortingOption $sortingOption = null
    ): Paginator {
        return $this->contactRepository->findPaginatedByPerson(
            person: $person,
            firstResult: $firstResult,
            maxResults: $maxResults,
            sortingOption: $sortingOption
        );
    }

    /**
     * @param Account[] $accounts
     */
    public function fetchContactByIdAndPerson(int $id, Person $person, array $accounts): ?Contact
    {
        return $this->contactRepository->findOneBy(criteria: [
            'id'      => $id,
            'person'  => $person,
            'account' => $accounts,
            'deleted' => false,
        ]);
    }

    public function fetchOccurrencesPaginatedByAccount(
        Account $account,
        bool $withFromSubAccounts,
        int $firstResult,
        int $maxResults,
        ?AccountUser $performedWithAccountUser = null,
        ?bool $followUp = null,
        ?bool $followUpDone = null,
        ?\DateTime $followUpAt = null,
        ?SortingOption $sortingOption = null
    ): Paginator {
        return $this->occurrenceRepository->findPaginatedByAccount(
            account: $account,
            withFromSubAccounts: $withFromSubAccounts,
            firstResult: $firstResult,
            maxResults: $maxResults,
            performedWithAccountUser: $performedWithAccountUser,
            followUp: $followUp,
            followUpDone: $followUpDone,
            followUpAt: $followUpAt,
            sortingOption: $sortingOption
        );
    }

    public function fetchPaginatedOccurrencesByPerson(
        Account $account,
        bool $withFromSubAccounts,
        Person $person,
        int $firstResult,
        int $maxResults,
        ?AccountUser $performedWithAccountUser = null,
        ?bool $followUp = null,
        ?bool $followUpDone = null,
        ?\DateTime $followUpAt = null,
        ?SortingOption $sortingOption = null
    ): Paginator {
        return $this->occurrenceRepository->findPaginatedByPerson(
            account: $account,
            withFromSubAccounts: $withFromSubAccounts,
            person: $person,
            firstResult: $firstResult,
            maxResults: $maxResults,
            performedWithAccountUser: $performedWithAccountUser,
            followUp: $followUp,
            followUpDone: $followUpDone,
            followUpAt: $followUpAt,
            sortingOption: $sortingOption
        );
    }

    /**
     * @return Occurrence[]
     */
    public function fetchOccurrencesWithUndoneFollowUpsByAccount(
        Account $account,
        bool $withFromSubAccounts,
        ?AccountUser $performedWithAccountUser = null,
        ?\DateTime $followUpAt = null,
        ?SortingOption $sortingOption = null
    ): array {
        return $this->occurrenceRepository->findByAccount(
            account: $account,
            withFromSubAccounts: $withFromSubAccounts,
            performedWithAccountUser: $performedWithAccountUser,
            followUp: true,
            followUpDone: false,
            followUpAt: $followUpAt,
            sortingOption: $sortingOption
        );
    }

    /**
     * @return Occurrence[]
     */
    public function fetchOccurrencesByPropertyOwnersFromBuildingUnit(Account $account, bool $withFromSubAccounts, BuildingUnit $buildingUnit): array
    {
        return $this->occurrenceRepository->findByPropertyOwnersFromBuildingUnit(
            account: $account,
            withFromSubAccounts: $withFromSubAccounts,
            buildingUnit: $buildingUnit
        );
    }

    /**
     * @return Occurrence[]
     */
    public function fetchOccurrencesByPropertyUsersFromBuildingUnit(Account $account, bool $withFromSubAccounts, BuildingUnit $buildingUnit): array
    {
        return $this->occurrenceRepository->findByPropertyUsersFromBuildingUnit(
            account: $account,
            withFromSubAccounts: $withFromSubAccounts,
            buildingUnit: $buildingUnit
        );
    }

    /**
     * @return Occurrence[]
     */
    public function fetchOccurrencesByPropertyContactPersonsFromBuildingUnit(Account $account, bool $withFromSubAccounts, BuildingUnit $buildingUnit): array
    {
        return $this->occurrenceRepository->findByPropertyContactPersonsFromBuildingUnit(
            account: $account,
            withFromSubAccounts: $withFromSubAccounts,
            buildingUnit: $buildingUnit
        );
    }

    public function fetchOccurrenceById(Account $account, bool $withFromSubAccounts, int $id): ?Occurrence
    {
        return $this->occurrenceRepository->findOneById(account: $account, withFromSubAccounts: $withFromSubAccounts, id: $id);
    }

    public function fetchOccurrenceByIdAndPerson(Account $account, bool $withFromSubAccounts, int $id, Person $person): ?Occurrence
    {
        return $this->occurrenceRepository->findOneByIdAndPerson(account: $account,withFromSubAccounts: $withFromSubAccounts,id: $id,person: $person);
    }

    public function createAndSaveOccurrenceAttachmentFromUploadedFile(
        UploadedFile $uploadedFile,
        AccountUser $accountUser,
        Occurrence $occurrence
    ): OccurrenceAttachment {
        $file = $this->fileService->saveFileFromUploadedFile(
            uploadedFile: $uploadedFile,
            account: $accountUser->getAccount(),
            public: false,
            accountUser: $accountUser
        );

        $occurrenceAttachment = new OccurrenceAttachment();

        $occurrenceAttachment
            ->setAccount($accountUser->getAccount())
            ->setCreatedByAccountUser($accountUser)
            ->setFile($file)
            ->setOccurrence($occurrence);

        $this->entityManager->persist(entity: $occurrenceAttachment);
        $this->entityManager->flush();

        return $occurrenceAttachment;
    }

    public function createOccurrenceResubmissionDueNotificationsByAccount(Account $account): void
    {
        $notificationType = NotificationType::OCCURRENCE_RESUBMISSION_DUE;

        $occurrences = $this->occurrenceRepository->findByAccount(
            account: $account,
            withFromSubAccounts: false,
            followUp: true,
            followUpDone: false,
            followUpAt: new \DateTime()
        );

        foreach ($occurrences as $occurrence) {
            $notification = $this->notificationService->fetchNotificationByNotificationTypeAndObject(
                accountUser: $occurrence->getPerformedWithAccountUser(),
                notificationType: $notificationType,
                object: $occurrence
            );

            if ($notification === null) {
                $this->notificationService->createAndSaveNotification(
                    accountUser: $occurrence->getPerformedWithAccountUser(),
                    notificationType: $notificationType,
                    beenRead: false,
                    occurrence: $occurrence
                );
            }
        }
    }
}
