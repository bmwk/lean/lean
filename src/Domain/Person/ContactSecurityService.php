<?php

declare(strict_types=1);

namespace App\Domain\Person;

use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\AccountUser\AccountUserRole;
use App\Domain\Entity\Person\Contact;
use Symfony\Bundle\SecurityBundle\Security;

class ContactSecurityService
{
    public function __construct(
        private readonly Security $security
    ) {
    }

    public function canViewContact(Contact $contact, AccountUser $accountUser): bool
    {
        $account = $accountUser->getAccount();

        if (
            (
                $this->security->isGranted(AccountUserRole::ROLE_USER->value) === true
                || $this->security->isGranted(AccountUserRole::ROLE_VIEWER->value) === true
            )
            && (
                $contact->getAccount() === $account
                || $account->getSubAccounts()->contains(element: $contact->getAccount()) === true
            )
        ) {
            return true;
        }

        return false;
    }

    public function canEditContact(Contact $contact, AccountUser $accountUser): bool
    {
        $account = $accountUser->getAccount();

        if (
            $this->security->isGranted(AccountUserRole::ROLE_USER->value) === true
            && (
                $contact->getAccount() === $account
                || $account->getSubAccounts()->contains(element: $contact->getAccount()) === true
            )
        ) {
            return true;
        }

        return false;
    }

    public function canDeleteContact(Contact $contact, AccountUser $accountUser): bool
    {
        return $this->canEditContact(contact: $contact, accountUser: $accountUser);
    }
}
