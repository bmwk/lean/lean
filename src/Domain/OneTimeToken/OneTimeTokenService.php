<?php

declare(strict_types=1);

namespace App\Domain\OneTimeToken;

use App\Domain\Entity\OneTimeToken;
use App\Repository\OneTimeTokenRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Uid\Uuid;

class OneTimeTokenService
{
    private const ONE_TIME_TOKEN_LIFETIME_MINUTES = 60;

    public function __construct(
        private readonly OneTimeTokenRepository $oneTimeTokenRepository,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function createAndSaveOneTimeToken(array $data): OneTimeToken
    {
        $oneTimeToken = new OneTimeToken();

        $oneTimeToken->setJsonData($data);

        $this->entityManager->persist(entity: $oneTimeToken);
        $this->entityManager->flush();

        return $oneTimeToken;
    }

    public function fetchAndDeleteOneTimeToken(Uuid $token): ?OneTimeToken
    {
        $oneTimeToken = $this->oneTimeTokenRepository->find(id: $token);

        if ($oneTimeToken === null) {
            return null;
        }

        $this->entityManager->remove(entity: $oneTimeToken);
        $this->entityManager->flush();

        return self::checkTokenValid(oneTimeToken: $oneTimeToken) ? $oneTimeToken : null;
    }

    private static function checkTokenValid(OneTimeToken $oneTimeToken): bool
    {
        return $oneTimeToken->getCreatedAt()->diff(targetObject: new \DateTime())->i <= self::ONE_TIME_TOKEN_LIFETIME_MINUTES;
    }
}
