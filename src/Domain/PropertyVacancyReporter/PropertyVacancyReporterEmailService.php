<?php

declare(strict_types=1);

namespace App\Domain\PropertyVacancyReporter;

use App\Domain\Entity\PropertyVacancyReport\PropertyVacancyReport;
use App\Domain\Entity\PropertyVacancyReporter\PropertyVacancyReporterConfiguration;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class PropertyVacancyReporterEmailService
{
    public function __construct(
        private readonly string $noReplyEmailAddress,
        private readonly MailerInterface $mailer,
        private readonly UrlGeneratorInterface $urlGenerator
    ) {
    }

    public function sendPropertyVacancyReportCreatedConfirmationEmail(PropertyVacancyReport $propertyVacancyReport): void
    {
        $name = $propertyVacancyReport->getReporterFirstName() . ' ' . $propertyVacancyReport->getReporterName();
        $name = trim(string: $name);

        $account = $propertyVacancyReport->getAccount();

        $confirmationUrl = $this->urlGenerator->generate(
            name: 'property_vacancy_reporter_report_confirmation',
            parameters: [
                'reportUuid'        => $propertyVacancyReport->getUuid()->toRfc4122(),
                'confirmationToken' => $propertyVacancyReport->getConfirmationToken()->toRfc4122(),
            ],
            referenceType: UrlGeneratorInterface::ABSOLUTE_URL
        );

        $email = (new TemplatedEmail())
            ->from(addresses: $this->noReplyEmailAddress)
            ->to(new Address(address: $propertyVacancyReport->getReporterEmail(), name: $name))
            ->subject(subject: 'Ihre Meldung eines Leerstands')
            ->htmlTemplate(template: 'property_vacancy_management/email/property_vacancy_report/confirmation_email.html.twig')
            ->textTemplate(template: 'property_vacancy_management/email/property_vacancy_report/confirmation_email.txt.twig')
            ->context(context: [
                'confirmationUrl' => $confirmationUrl,
                'placeName'       => $account->getAssignedPlace()->getPlaceName(),
                'account'         => $account,
            ]);

        $this->mailer->send($email);
    }

    public function sendNewPropertyVacancyReportNotificationEmail(
        PropertyVacancyReport $propertyVacancyReport,
        PropertyVacancyReporterConfiguration $propertyVacancyReporterConfiguration
    ): void {
        $account = $propertyVacancyReport->getAccount();
        $reportUrl = $this->urlGenerator->generate(
            name: 'property_vacancy_reporter_report_confirmation',
            parameters: [
                'reportUuid' => $propertyVacancyReport->getUuid(),
                'confirmationToken' => $propertyVacancyReport->getConfirmationToken(),
            ],
            referenceType: UrlGeneratorInterface::ABSOLUTE_URL
        );

        $email = (new TemplatedEmail())
            ->from(addresses: $this->noReplyEmailAddress)
            ->to(new Address(address: $propertyVacancyReporterConfiguration->getReportNotificationEmail()))
            ->subject(subject: 'Eine neue Leerstandsmeldung ist eingegangen')
            ->htmlTemplate(template: 'property_vacancy_management/email/property_vacancy_report/notification_email.html.twig')
            ->textTemplate(template: 'property_vacancy_management/email/property_vacancy_report/notification_email.txt.twig')
            ->context(context: [
                'reportUrl'             => $reportUrl,
                'propertyVacancyReport' => $propertyVacancyReport,
                'account'               => $account,
            ]);

        $this->mailer->send($email);
    }
}
