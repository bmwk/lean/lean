<?php

declare(strict_types=1);

namespace App\Domain\PropertyVacancyReporter\Api\Entity\Response;

use Symfony\Component\Uid\Uuid;

class ReportCreateResponse
{
    public function __construct(
        private readonly Uuid $uuid
    ) {
    }

    public function getUuid(): Uuid
    {
        return $this->uuid;
    }
}
