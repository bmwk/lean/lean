<?php

declare(strict_types=1);

namespace App\Domain\PropertyVacancyReporter\Api\Entity;

use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

class PropertyLocation
{
    #[Assert\NotBlank]
    private ?string $streetName = null;

    #[Assert\NotBlank]
    private ?string $houseNumber = null;

    #[Assert\NotBlank]
    private ?string $postalCode = null;

    #[Assert\Uuid]
    #[Assert\NotBlank]
    private Uuid $placeUuid;

    #[Assert\Uuid]
    private ?Uuid $boroughPlaceUuid = null;

    #[Assert\Uuid]
    private ?Uuid $quarterPlaceUuid = null;

    private ?string $placeDescription = null;

    public static function createFormApiRequestJsonContent(object $jsonContent): self
    {
        $propertyLocation = new self();

        $propertyLocation->streetName = $jsonContent->streetName;
        $propertyLocation->houseNumber = $jsonContent->houseNumber;
        $propertyLocation->postalCode = $jsonContent->postalCode;
        $propertyLocation->placeUuid = Uuid::fromString($jsonContent->placeUuid);
        $propertyLocation->boroughPlaceUuid = empty($jsonContent->boroughPlaceUuid) === false ? Uuid::fromString($jsonContent->boroughPlaceUuid) : null;
        $propertyLocation->quarterPlaceUuid = empty($jsonContent->quarterPlaceUuid) === false ? Uuid::fromString($jsonContent->quarterPlaceUuid) : null;
        $propertyLocation->placeDescription = $jsonContent->placeDescription;

        return $propertyLocation;
    }

    public function getStreetName(): ?string
    {
        return $this->streetName;
    }

    public function setStreetName(?string $streetName): self
    {
        $this->streetName = $streetName;

        return $this;
    }

    public function getHouseNumber(): ?string
    {
        return $this->houseNumber;
    }

    public function setHouseNumber(?string $houseNumber): self
    {
        $this->houseNumber = $houseNumber;

        return $this;
    }

    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }

    public function setPostalCode(?string $postalCode): self
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    public function getPlaceDescription(): ?string
    {
        return $this->placeDescription;
    }

    public function setPlaceDescription(?string $placeDescription): self
    {
        $this->placeDescription = $placeDescription;

        return $this;
    }

    public function getPlaceUuid(): Uuid
    {
        return $this->placeUuid;
    }

    public function setPlaceUuid(Uuid $placeUuid): self
    {
        $this->placeUuid = $placeUuid;

        return $this;
    }

    public function getBoroughPlaceUuid(): ?Uuid
    {
        return $this->boroughPlaceUuid;
    }

    public function setBoroughPlaceUuid(?Uuid $boroughPlaceUuid): self
    {
        $this->boroughPlaceUuid = $boroughPlaceUuid;

        return $this;
    }

    public function getQuarterPlaceUuid(): ?Uuid
    {
        return $this->quarterPlaceUuid;
    }

    public function setQuarterPlaceUuid(?Uuid $quarterPlaceUuid): self
    {
        $this->quarterPlaceUuid = $quarterPlaceUuid;

        return $this;
    }
}
