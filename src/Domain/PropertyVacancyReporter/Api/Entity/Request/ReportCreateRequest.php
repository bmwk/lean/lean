<?php

declare(strict_types=1);

namespace App\Domain\PropertyVacancyReporter\Api\Entity\Request;

use App\Domain\PropertyVacancyReporter\Api\Entity\PropertyInformation;
use App\Domain\PropertyVacancyReporter\Api\Entity\PropertyLocation;
use App\Domain\PropertyVacancyReporter\Api\Entity\Reporter;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

class ReportCreateRequest
{
    #[Assert\NotBlank]
    private Uuid $accountKey;

    private PropertyInformation $propertyInformation;

    private PropertyLocation $propertyLocation;

    #[Assert\Valid]
    private Reporter $reporter;

    public static function createFormApiRequestJsonContent(object $jsonContent): self
    {
        $reportCreateRequest = new self();

        $reportCreateRequest->accountKey = Uuid::fromString($jsonContent->accountKey);
        $reportCreateRequest->propertyInformation = PropertyInformation::createFormApiRequestJsonContent($jsonContent->propertyInformation);
        $reportCreateRequest->propertyLocation = PropertyLocation::createFormApiRequestJsonContent($jsonContent->propertyLocation);
        $reportCreateRequest->reporter = Reporter::createFormApiRequestJsonContent($jsonContent->reporter);

        return $reportCreateRequest;
    }

    public function getAccountKey(): Uuid
    {
        return $this->accountKey;
    }

    public function setAccountKey(Uuid $accountKey): self
    {
        $this->accountKey = $accountKey;

        return $this;
    }

    public function getPropertyInformation(): PropertyInformation
    {
        return $this->propertyInformation;
    }

    public function setPropertyInformation(PropertyInformation $propertyInformation): self
    {
        $this->propertyInformation = $propertyInformation;

        return $this;
    }

    public function getPropertyLocation(): PropertyLocation
    {
        return $this->propertyLocation;
    }

    public function setPropertyLocation(PropertyLocation $propertyLocation): self
    {
        $this->propertyLocation = $propertyLocation;

        return $this;
    }

    public function getReporter(): Reporter
    {
        return $this->reporter;
    }

    public function setReporter(Reporter $reporter): self
    {
        $this->reporter = $reporter;

        return $this;
    }
}
