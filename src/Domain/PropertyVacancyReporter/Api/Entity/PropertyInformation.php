<?php

declare(strict_types=1);

namespace App\Domain\PropertyVacancyReporter\Api\Entity;

use App\Domain\Entity\LocationCategory;
use App\Domain\Entity\Property\BarrierFreeAccess;
use App\Domain\Entity\Property\BuildingCondition;
use App\Domain\Entity\Property\UsableOutdoorAreaPossibility;
use App\Domain\Entity\PropertyOfferType;

class PropertyInformation
{
    private ?BuildingCondition $buildingCondition = null;

    private ?float $areaSize = null;

    private ?float $plotSize = null;

    private ?float $retailSpace = null;

    private ?float $gastronomySpace = null;

    private ?float $livingSpace = null;

    private ?float $usableSpace = null;

    private ?float $subsidiarySpace = null;

    private ?UsableOutdoorAreaPossibility $usableOutdoorAreaPossibility = null;

    private ?float $usableOutdoorArea = null;

    private ?float $shopWindowFrontWidth = null;

    private ?float $shopWidth = null;

    private ?bool $groundLevelSalesArea = null;

    private bool $storeWindowAvailable;

    private ?int $numberOfParkingLots = null;

    private ?BarrierFreeAccess $barrierFreeAccess = null;

    private ?LocationCategory $locationCategory = null;

    private ?array $locationFactors = null;

    private ?PropertyOfferType $propertyOfferType = null;

    private ?string $propertyDescription = null;

    private ?bool $objectIsEmpty = null;

    private ?\DateTime $objectIsEmptySince = null;

    private ?bool $objectBecomesEmpty = null;

    private ?\DateTime $objectBecomesEmptyFrom = null;

    private ?int $industryClassificationId = null;

    private ?string $propertyUserCompanyName = null;

    public static function createFormApiRequestJsonContent(object $jsonContent): self
    {
        $propertyInformation = new self();

        $propertyInformation->buildingCondition = isset($jsonContent->buildingCondition) ? BuildingCondition::from($jsonContent->buildingCondition) : null;
        $propertyInformation->areaSize = $jsonContent->areaSize;
        $propertyInformation->plotSize = $jsonContent->plotSize;
        $propertyInformation->retailSpace = $jsonContent->retailSpace;
        $propertyInformation->gastronomySpace = $jsonContent->gastronomySpace;
        $propertyInformation->livingSpace = $jsonContent->livingSpace;
        $propertyInformation->usableSpace = isset($jsonContent->usableSpace) ? $jsonContent->usableSpace : null;
        $propertyInformation->subsidiarySpace = isset($jsonContent->subsidiarySpace) ? $jsonContent->subsidiarySpace : null;
        $propertyInformation->usableOutdoorAreaPossibility = isset($jsonContent->usableOutdoorAreaPossibility) ? UsableOutdoorAreaPossibility::from($jsonContent->usableOutdoorAreaPossibility) : null;
        $propertyInformation->usableOutdoorArea = isset($jsonContent->usableOutdoorArea) ? $jsonContent->usableOutdoorArea : null;
        $propertyInformation->shopWindowFrontWidth = isset($jsonContent->shopWindowFrontWidth) ? $jsonContent->shopWindowFrontWidth : null;
        $propertyInformation->shopWidth = isset($jsonContent->shopWidth) ? $jsonContent->shopWidth : null;
        $propertyInformation->groundLevelSalesArea = isset($jsonContent->groundLevelSalesArea) ? $jsonContent->groundLevelSalesArea : null;
        $propertyInformation->storeWindowAvailable = $jsonContent->storeWindowAvailable;
        $propertyInformation->numberOfParkingLots = isset($jsonContent->numberOfParkingLots) ? $jsonContent->numberOfParkingLots : null;
        $propertyInformation->barrierFreeAccess = isset($jsonContent->barrierFreeAccess) ? BarrierFreeAccess::from($jsonContent->barrierFreeAccess) : null;
        $propertyInformation->locationCategory = isset($jsonContent->locationCategory) ? LocationCategory::from($jsonContent->locationCategory) : null;
        $propertyInformation->locationFactors = isset($jsonContent->locationFactors) ? $jsonContent->locationFactors : null;
        $propertyInformation->propertyOfferType = isset($jsonContent->propertyOfferType) ? PropertyOfferType::from($jsonContent->propertyOfferType) : null;
        $propertyInformation->propertyDescription = $jsonContent->propertyDescription;
        $propertyInformation->objectIsEmpty = $jsonContent->objectIsEmpty;
        $propertyInformation->objectIsEmptySince = !empty($jsonContent->objectIsEmptySince) ? new \DateTime($jsonContent->objectIsEmptySince) : null;
        $propertyInformation->objectBecomesEmpty = $jsonContent->objectBecomesEmpty;
        $propertyInformation->objectBecomesEmptyFrom = !empty($jsonContent->objectBecomesEmptyFrom) ? new \DateTime($jsonContent->objectBecomesEmptyFrom) : null;
        $propertyInformation->industryClassificationId = $jsonContent->industryClassificationId;
        $propertyInformation->propertyUserCompanyName = $jsonContent->propertyUserCompanyName;

        return $propertyInformation;
    }

    public function getBuildingCondition(): ?BuildingCondition
    {
        return $this->buildingCondition;
    }

    public function setBuildingCondition(?BuildingCondition $buildingCondition): self
    {
        $this->buildingCondition = $buildingCondition;

        return $this;
    }

    public function getAreaSize(): ?float
    {
        return $this->areaSize;
    }

    public function setAreaSize(?float $areaSize): self
    {
        $this->areaSize = $areaSize;

        return $this;
    }

    public function getPlotSize(): ?float
    {
        return $this->plotSize;
    }

    public function setPlotSize(?float $plotSize): self
    {
        $this->plotSize = $plotSize;

        return $this;
    }

    public function getRetailSpace(): ?float
    {
        return $this->retailSpace;
    }

    public function setRetailSpace(?float $retailSpace): self
    {
        $this->retailSpace = $retailSpace;

        return $this;
    }

    public function getGastronomySpace(): ?float
    {
        return $this->gastronomySpace;
    }

    public function setGastronomySpace(?float $gastronomySpace): self
    {
        $this->gastronomySpace = $gastronomySpace;

        return $this;
    }

    public function getLivingSpace(): ?float
    {
        return $this->livingSpace;
    }

    public function setLivingSpace(?float $livingSpace): self
    {
        $this->livingSpace = $livingSpace;

        return $this;
    }

    public function getUsableSpace(): ?float
    {
        return $this->usableSpace;
    }

    public function setUsableSpace(?float $usableSpace): self
    {
        $this->usableSpace = $usableSpace;

        return $this;
    }

    public function getSubsidiarySpace(): ?float
    {
        return $this->subsidiarySpace;
    }

    public function setSubsidiarySpace(?float $subsidiarySpace): self
    {
        $this->subsidiarySpace = $subsidiarySpace;

        return $this;
    }

    public function getUsableOutdoorAreaPossibility(): ?UsableOutdoorAreaPossibility
    {
        return $this->usableOutdoorAreaPossibility;
    }

    public function setUsableOutdoorAreaPossibility(?UsableOutdoorAreaPossibility $usableOutdoorAreaPossibility): self
    {
        $this->usableOutdoorAreaPossibility = $usableOutdoorAreaPossibility;

        return $this;
    }

    public function getUsableOutdoorArea(): ?float
    {
        return $this->usableOutdoorArea;
    }

    public function setUsableOutdoorArea(?float $usableOutdoorArea): self
    {
        $this->usableOutdoorArea = $usableOutdoorArea;

        return $this;
    }

    public function getShopWindowFrontWidth(): ?float
    {
        return $this->shopWindowFrontWidth;
    }

    public function setShopWindowFrontWidth(?float $shopWindowFrontWidth): self
    {
        $this->shopWindowFrontWidth = $shopWindowFrontWidth;

        return $this;
    }

    public function getShopWidth(): ?float
    {
        return $this->shopWidth;
    }

    public function setShopWidth(?float $shopWidth): self
    {
        $this->shopWidth = $shopWidth;

        return $this;
    }

    public function getGroundLevelSalesArea(): ?bool
    {
        return $this->groundLevelSalesArea;
    }

    public function setGroundLevelSalesArea(?bool $groundLevelSalesArea): self
    {
        $this->groundLevelSalesArea = $groundLevelSalesArea;

        return $this;
    }

    public function isStoreWindowAvailable(): bool
    {
        return $this->storeWindowAvailable;
    }

    public function setStoreWindowAvailable(bool $storeWindowAvailable): self
    {
        $this->storeWindowAvailable = $storeWindowAvailable;

        return $this;
    }

    public function getNumberOfParkingLots(): ?int
    {
        return $this->numberOfParkingLots;
    }

    public function setNumberOfParkingLots(?int $numberOfParkingLots): self
    {
        $this->numberOfParkingLots = $numberOfParkingLots;

        return $this;
    }

    public function getBarrierFreeAccess(): ?BarrierFreeAccess
    {
        return $this->barrierFreeAccess;
    }

    public function setBarrierFreeAccess(?BarrierFreeAccess $barrierFreeAccess): self
    {
        $this->barrierFreeAccess = $barrierFreeAccess;

        return $this;
    }

    public function getLocationCategory(): ?LocationCategory
    {
        return $this->locationCategory;
    }

    public function setLocationCategory(?LocationCategory $locationCategory): self
    {
        $this->locationCategory = $locationCategory;

        return $this;
    }

    public function getLocationFactors(): ?array
    {
        return $this->locationFactors;
    }

    public function setLocationFactors(?array $locationFactors): self
    {
        $this->locationFactors = $locationFactors;

        return $this;
    }

    public function getPropertyOfferType(): ?PropertyOfferType
    {
        return $this->propertyOfferType;
    }

    public function setPropertyOfferType(?PropertyOfferType $propertyOfferType): self
    {
        $this->propertyOfferType = $propertyOfferType;

        return $this;
    }

    public function getPropertyDescription(): ?string
    {
        return $this->propertyDescription;
    }

    public function setPropertyDescription(?string $propertyDescription): self
    {
        $this->propertyDescription = $propertyDescription;

        return $this;
    }

    public function isObjectIsEmpty(): ?bool
    {
        return $this->objectIsEmpty;
    }

    public function setObjectIsEmpty(?bool $objectIsEmpty): self
    {
        $this->objectIsEmpty = $objectIsEmpty;

        return $this;
    }

    public function getObjectIsEmptySince(): ?\DateTime
    {
        return $this->objectIsEmptySince;
    }

    public function setObjectIsEmptySince(?\DateTime $objectIsEmptySince): self
    {
        $this->objectIsEmptySince = $objectIsEmptySince;

        return $this;
    }

    public function isObjectBecomesEmpty(): ?bool
    {
        return $this->objectBecomesEmpty;
    }

    public function setObjectBecomesEmpty(?bool $objectBecomesEmpty): self
    {
        $this->objectBecomesEmpty = $objectBecomesEmpty;

        return $this;
    }

    public function getObjectBecomesEmptyFrom(): ?\DateTime
    {
        return $this->objectBecomesEmptyFrom;
    }

    public function setObjectBecomesEmptyFrom(?\DateTime $objectBecomesEmptyFrom): self
    {
        $this->objectBecomesEmptyFrom = $objectBecomesEmptyFrom;

        return $this;
    }

    public function getIndustryClassificationId(): ?int
    {
        return $this->industryClassificationId;
    }

    public function setIndustryClassificationId(?int $industryClassificationId): self
    {
        $this->industryClassificationId = $industryClassificationId;

        return $this;
    }

    public function getPropertyUserCompanyName(): ?string
    {
        return $this->propertyUserCompanyName;
    }

    public function setPropertyUserCompanyName(?string $propertyUserCompanyName): self
    {
        $this->propertyUserCompanyName = $propertyUserCompanyName;

        return $this;
    }
}
