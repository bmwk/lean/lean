<?php

declare(strict_types=1);

namespace App\Domain\PropertyVacancyReporter\Api\Entity;

use App\Domain\Entity\Person\Salutation;
use Symfony\Component\Validator\Constraints as Assert;

class Reporter
{
    private ?string $companyName = null;

    private ?Salutation $salutation = null;

    #[Assert\NotNull]
    private string $name;

    private ?string $firstName = null;

    #[Assert\NotBlank]
    #[Assert\Email]
    private string $email;

    private ?string $phoneNumber = null;

    #[Assert\NotNull]
    private bool $alsoPropertyOwner;

    public static function createFormApiRequestJsonContent(object $jsonContent): self
    {
        $reporter = new self();

        $reporter->companyName = isset($jsonContent->companyName) === true && empty($jsonContent->companyName) === false ? $jsonContent->companyName : null;
        $reporter->salutation = $jsonContent->salutation !== null ? Salutation::from($jsonContent->salutation) : null;
        $reporter->name = $jsonContent->name;
        $reporter->firstName = isset($jsonContent->firstName) === true && empty($jsonContent->firstName) === false ? $jsonContent->firstName : null;
        $reporter->email = $jsonContent->email;
        $reporter->phoneNumber = isset($jsonContent->phoneNumber) === true && empty($jsonContent->phoneNumber) === false ? $jsonContent->phoneNumber : null;
        $reporter->alsoPropertyOwner = $jsonContent->alsoPropertyOwner;

        return $reporter;
    }

    public function getCompanyName(): ?string
    {
        return $this->companyName;
    }

    public function setCompanyName(?string $companyName): self
    {
        $this->companyName = $companyName;

        return $this;
    }

    public function getSalutation(): ?Salutation
    {
        return $this->salutation;
    }

    public function setSalutation(?Salutation $salutation): self
    {
        $this->salutation = $salutation;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(?string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;
        return $this;
    }

    public function isAlsoPropertyOwner(): bool
    {
        return $this->alsoPropertyOwner;
    }

    public function setAlsoPropertyOwner(bool $alsoPropertyOwner): self
    {
        $this->alsoPropertyOwner = $alsoPropertyOwner;

        return $this;
    }
}
