<?php

declare(strict_types=1);

namespace App\Domain\Entity;

use App\Domain\Entity\AccountUser\AccountUser;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;

trait DeletedTrait
{
    #[Column(type: 'boolean', nullable: false)]
    protected bool $deleted;

    #[Column(type: 'datetime_immutable', nullable: true)]
    protected ?\DateTimeImmutable $deletedAt = null;

    #[ManyToOne]
    #[JoinColumn(nullable: true)]
    protected ?AccountUser $deletedByAccountUser = null;

    public function isDeleted(): bool
    {
        return $this->deleted;
    }

    public function setDeleted(bool $deleted): self
    {
        $this->deleted = $deleted;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeImmutable
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeImmutable $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function getDeletedByAccountUser(): ?AccountUser
    {
        return $this->deletedByAccountUser;
    }

    public function setDeletedByAccountUser(?AccountUser $deletedByAccountUser): self
    {
        $this->deletedByAccountUser = $deletedByAccountUser;

        return $this;
    }
}
