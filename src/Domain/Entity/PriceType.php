<?php

declare(strict_types=1);

namespace App\Domain\Entity;

enum PriceType: int
{
    case PER_MONTH_PER_SQUARE_METER = 0;
    case PRICE_TOTAL = 1;

    public function getName(): string
    {
        return match ($this) {
            self::PER_MONTH_PER_SQUARE_METER => 'Preis pro m²',
            self::PRICE_TOTAL => 'Preis Gesamt',
        };
    }
}
