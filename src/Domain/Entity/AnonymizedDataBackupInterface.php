<?php

declare(strict_types=1);

namespace App\Domain\Entity;

interface AnonymizedDataBackupInterface
{
    public function getId(): ?int;

    public function getAnonymizableDataToBackup(): array;
}
