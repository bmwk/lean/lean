<?php

declare(strict_types=1);

namespace App\Domain\Entity;

use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\Person\Contact;
use App\Domain\Entity\Person\Person;
use App\Domain\Entity\PropertyVacancyReport\PropertyVacancyReport;

enum AnonymizedDataClass: int
{
    case PERSON = 0;
    case CONTACT = 1;
    case PROPERTY_VACANCY_REPORT = 2;
    case ACCOUNT_USER = 3;

    public static function fromClassName(string $className): self
    {
        return match ($className) {
            Person::class => self::PERSON,
            Contact::class => self::CONTACT,
            PropertyVacancyReport::class => self::PROPERTY_VACANCY_REPORT,
            AccountUser::class => self::ACCOUNT_USER
        };
    }
}
