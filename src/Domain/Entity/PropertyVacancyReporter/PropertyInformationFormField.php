<?php

declare(strict_types=1);

namespace App\Domain\Entity\PropertyVacancyReporter;

enum PropertyInformationFormField: int
{
    case PROPERTY_USAGE_TYPE = 0;
    case BUILDING_CONDITION = 1;
    case PLOT_SIZE = 2;
    case AREA_SIZE = 3;
    case RETAIL_SPACE = 4;
    case LIVING_SPACE = 5;
    case GASTRONOMY_SPACE = 6;
    case PROPERTY_DESCRIPTION = 7;
    case SUBSIDIARY_SPACE = 8;
    case OPEN_SPACE = 9;
    case USABLE_OPEN_SPACE = 10;
    case STORE_WINDOW_WIDTH = 11;
    case SHOP_WIDTH = 12;
    case NUMBER_OF_PARKING_LOTS = 13;
    case PROPERTY_OFFER_TYPE = 14;
    case PROPERTY_USER_COMPANY_NAME = 15;

    public function getName(): string
    {
        return match ($this) {
            self::PROPERTY_USAGE_TYPE => 'Nutzungsart',
            self::BUILDING_CONDITION => 'Gebäudezustand',
            self::PLOT_SIZE => 'Grundstücksfläche',
            self::AREA_SIZE => 'Gesamtfläche',
            self::RETAIL_SPACE => 'Verkaufsfläche',
            self::LIVING_SPACE => 'Wohnfläche',
            self::GASTRONOMY_SPACE => 'Gastrofläche',
            self::PROPERTY_DESCRIPTION => 'Objektbeschreibung',
            self::SUBSIDIARY_SPACE => 'Nebenfläche',
            self::OPEN_SPACE => 'Freifläche',
            self::USABLE_OPEN_SPACE => 'Nutzbare Freifläche',
            self::STORE_WINDOW_WIDTH => 'Schaufensterbreite',
            self::SHOP_WIDTH => 'Ladenbreite',
            self::NUMBER_OF_PARKING_LOTS => 'Anzahl Stellplätze',
            self::PROPERTY_OFFER_TYPE => 'Angebotsart',
            self::PROPERTY_USER_COMPANY_NAME => 'Letzter / Aktueller Nutzer'
        };
    }
}
