<?php

declare(strict_types=1);

namespace App\Domain\Entity\PropertyVacancyReporter;

enum PropertyLocationFormField: int
{
    case STREET_NAME = 0;
    case HOUSE_NUMBER = 1;
    case POSTAL_CODE = 2;
    case PLACE = 3;
    case QUARTER = 4;
    case PLACE_DESCRIPTION = 5;

    public function getName(): string
    {
        return match ($this) {
            self::STREET_NAME => 'Strasse',
            self::HOUSE_NUMBER => 'Hausnummer',
            self::POSTAL_CODE => 'Postleitzahl',
            self::PLACE => 'Gemeinde',
            self::QUARTER => 'Ortsteil',
            self::PLACE_DESCRIPTION => 'Lagebeschreibung'
        };
    }
}
