<?php

declare(strict_types=1);

namespace App\Domain\Entity;

use App\Domain\Entity\Property\BuildingUnit;
use App\Repository\BuildingUnitFeedbackRepository;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\UniqueConstraint;

#[Entity(repositoryClass: BuildingUnitFeedbackRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[UniqueConstraint(name: 'building_unit_feedback', columns: ['building_unit_id', 'feedback_id'])]
#[HasLifecycleCallbacks]
class BuildingUnitFeedback
{
    use CreatedAtTrait;

    #[Id]
    #[GeneratedValue]
    #[Column(type: 'integer', nullable: false, options: ['unsigned' => true])]
    private ?int $id = null;

    #[ManyToOne]
    #[JoinColumn(nullable: false)]
    private BuildingUnit $buildingUnit;

    #[ManyToOne]
    #[JoinColumn(nullable: false)]
    private Feedback $feedback;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getBuildingUnit(): BuildingUnit
    {
        return $this->buildingUnit;
    }

    public function setBuildingUnit(BuildingUnit $buildingUnit): self
    {
        $this->buildingUnit = $buildingUnit;

        return $this;
    }

    public function getFeedback(): Feedback
    {
        return $this->feedback;
    }

    public function setFeedback(Feedback $feedback): self
    {
        $this->feedback = $feedback;

        return $this;
    }
}
