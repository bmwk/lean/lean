<?php

declare(strict_types=1);

namespace App\Domain\Entity\Property;

enum UsableOutdoorAreaPossibility: int
{
    case IS_AVAILABLE = 0;
    case CAN_BE_APPROVED = 1;
    case IS_NOT_PRESENT = 2;

    public function getName(): string
    {
        return match ($this) {
            self::IS_AVAILABLE => 'ist vorhanden',
            self::CAN_BE_APPROVED => 'kann genehmigt werden',
            self::IS_NOT_PRESENT => 'ist nicht vorhanden'
        };
    }
}
