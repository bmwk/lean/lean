<?php

declare(strict_types=1);

namespace App\Domain\Entity\Property;

enum PropertyRestrictionType: int
{
    case TRAFFIC_NOISE = 0;
    case RAIL_NOISE = 1;
    case AVIATION_NOISE = 2;
    case INDUSTRIAL_NOISE = 3;
    case SMELL = 4;
    case CLIMATE = 5;
    case FLOOD_PROTECTION = 6;
    case SPECIES_PROTECTION = 7;
    case NATURE_AND_LANDSCAPE_CONSERVATION = 8;
    case SEVESO_ATTENTION_DISTANCE = 9;
    case LEGACY_INFORMATION = 10;

    public function getName(): string
    {
        return match ($this) {
            self::TRAFFIC_NOISE => 'Verkehrslärm',
            self::RAIL_NOISE => 'Schienenlärm',
            self::AVIATION_NOISE => 'Fluglärm',
            self::INDUSTRIAL_NOISE => 'Gewerbelärm',
            self::SMELL => 'Geruch',
            self::CLIMATE => 'Klima',
            self::FLOOD_PROTECTION => 'Hochwasserschutz',
            self::SPECIES_PROTECTION => 'Artenschutz',
            self::NATURE_AND_LANDSCAPE_CONSERVATION => 'Natur- und Landschaftsschutz',
            self::SEVESO_ATTENTION_DISTANCE => 'Seveso Achtungsabstand',
            self::LEGACY_INFORMATION => 'Altlast-Informationen'
        };
    }
}
