<?php

declare(strict_types=1);

namespace App\Domain\Entity\Property;

enum BuildingType: int
{
    case SINGLE_FAMILY_HOUSE = 0;
    case APARTMENT_HOUSE = 1;
    case RESIDENTIAL_AND_COMMERCIAL_BUILDING_MIXED_USE = 2;
    case ADMINISTRATION_BUILDING = 3;
    case SPECIAL_BUILDING = 4;
    case OTHER_REAL_ESTATE = 5;
    case FARMHOUSE = 6;
    case SEMI_DETACHED_HOUSE = 7;
    case VILLA = 8;
    case TERRACED_HOUSE = 9;
    case BUNGALOW = 10;
    case OFFICE_BUILDING = 11;
    case BUSINESS_HOUSE = 12;

    public function getName(): string
    {
        return match ($this) {
            self::SINGLE_FAMILY_HOUSE => 'Einfamilienhaus',
            self::APARTMENT_HOUSE => 'Mehrfamilienwohnhaus',
            self::RESIDENTIAL_AND_COMMERCIAL_BUILDING_MIXED_USE => 'Wohn- und Geschäftshaus (gemischt genutzt)',
            self::ADMINISTRATION_BUILDING => 'Verwaltungsgebäude',
            self::SPECIAL_BUILDING => 'Spezialgebäude',
            self::OTHER_REAL_ESTATE => 'sonstige Immobilie',
            self::FARMHOUSE => 'Bauernhaus',
            self::SEMI_DETACHED_HOUSE => 'Doppelhaushälfte',
            self::VILLA => 'Villa',
            self::TERRACED_HOUSE => 'Reihenhaus',
            self::BUNGALOW => 'Bungalow',
            self::OFFICE_BUILDING => 'Bürohaus',
            self::BUSINESS_HOUSE => 'Geschäftshaus'
        };
    }
}
