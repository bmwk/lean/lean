<?php

declare(strict_types=1);

namespace App\Domain\Entity\Property;

enum PropertyRelationship: int
{
    case OWNER = 0;
    case TENANT = 1;

    public function getName(): string
    {
        return match ($this) {
            self::OWNER => 'Eigentümer',
            self::TENANT => 'Mieter'
        };
    }
}
