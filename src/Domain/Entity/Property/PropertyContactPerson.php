<?php

declare(strict_types=1);

namespace App\Domain\Entity\Property;

use App\Domain\Entity\AccountTrait;
use App\Domain\Entity\CreatedAtTrait;
use App\Domain\Entity\CreatedByAccountUserTrait;
use App\Domain\Entity\Person\Contact;
use App\Domain\Entity\Person\Occurrence;
use App\Domain\Entity\Person\Person;
use App\Domain\Entity\UpdatedAtTrait;
use App\Repository\Property\PropertyContactPersonRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\Table;

#[Entity(repositoryClass: PropertyContactPersonRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[HasLifecycleCallbacks]
class PropertyContactPerson
{
    use AccountTrait;
    use CreatedByAccountUserTrait;
    use CreatedAtTrait;
    use UpdatedAtTrait;

    #[Id]
    #[GeneratedValue]
    #[Column(type: Types::INTEGER, nullable: false, options: ['unsigned' => true])]
    private ?int $id = null;

    #[Column(type: Types::SMALLINT, nullable: false, enumType: PropertyContactResponsibility::class, options: ['unsigned' => true])]
    private PropertyContactResponsibility $propertyContactResponsibility;

    #[ManyToOne(inversedBy: 'propertyContactPersons')]
    #[JoinColumn(nullable: false)]
    private Person $person;

    #[ManyToOne]
    #[JoinColumn(nullable: true)]
    private ?Contact $contact = null;

    /**
     * @var VisibleContactInformation[]
     */
    #[Column(type: Types::JSON,  nullable: false, enumType: VisibleContactInformation::class)]
    private array $visibleContactInformations;

    /**
     * @var Collection<int, BuildingUnit>|BuildingUnit[]
     */
    #[ManyToMany(targetEntity: BuildingUnit::class, mappedBy: 'propertyContactPersons')]
    private Collection|array $buildingUnits;

    /**
     * @var Collection<int, Occurrence>|Occurrence[]
     */
    #[ManyToMany(targetEntity: Occurrence::class, mappedBy: 'propertyContactPersons')]
    private Collection|array $occurrences;

    public function __construct()
    {
        $this->buildingUnits = new ArrayCollection();
        $this->occurrences = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getPropertyContactResponsibility(): PropertyContactResponsibility
    {
        return $this->propertyContactResponsibility;
    }

    public function setPropertyContactResponsibility(PropertyContactResponsibility $propertyContactResponsibility): self
    {
        $this->propertyContactResponsibility = $propertyContactResponsibility;

        return $this;
    }

    public function getPerson(): Person
    {
        return $this->person;
    }

    public function setPerson(Person $person): self
    {
        $this->person = $person;

        return $this;
    }

    public function getContact(): ?Contact
    {
        return $this->contact;
    }

    public function setContact(?Contact $contact): self
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * @return VisibleContactInformation[]
     */
    public function getVisibleContactInformations(): array
    {
        return $this->visibleContactInformations;
    }

    /**
     * @param VisibleContactInformation[] $visibleContactInformations
     */
    public function setVisibleContactInformations(array $visibleContactInformations): self
    {
        $this->visibleContactInformations = $visibleContactInformations;

        return $this;
    }

    /**
     * @return Collection<int, BuildingUnit>|BuildingUnit[]
     */
    public function getBuildingUnits(): Collection|array
    {
        return $this->buildingUnits;
    }

    /**
     * @param Collection<int, BuildingUnit>|BuildingUnit[] $buildingUnits
     */
    public function setBuildingUnits(Collection|array $buildingUnits): void
    {
        $this->buildingUnits = $buildingUnits;
    }

    /**
     * @return Collection<int, Occurrence>|Occurrence[]
     */
    public function getOccurrences(): Collection|array
    {
        return $this->occurrences;
    }

    /**
     * @param Collection<int, Occurrence>|Occurrence[] $occurrences
     */
    public function setOccurrences(Collection|array $occurrences): self
    {
        $this->occurrences = $occurrences;

        return $this;
    }
}
