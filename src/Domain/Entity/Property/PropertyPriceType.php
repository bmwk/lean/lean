<?php

declare(strict_types=1);

namespace App\Domain\Entity\Property;

enum PropertyPriceType: int
{
    case FIXED_PRICE = 0;
    case NEGOTIATING_BASIS = 1;
    case ON_DEMAND = 2;
    case MINIMUM_PRICE = 3;
    case BIDDING_PROCESS = 4;

    public function getName(): string
    {
        return match ($this) {
            self::FIXED_PRICE => 'Festpreis',
            self::NEGOTIATING_BASIS => 'Verhandlungsbasis',
            self::ON_DEMAND => 'Auf Anfrage',
            self::MINIMUM_PRICE => 'Mindestpreis',
            self::BIDDING_PROCESS => 'Bieterverfahren'
        };
    }
}
