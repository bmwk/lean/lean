<?php

declare(strict_types=1);

namespace App\Domain\Entity\Property;

enum EsgCertificate: int
{
    case BNB_BRONZE = 0;
    case BNB_SILVER = 1;
    case BNB_GOLD = 2;
    case BREEAM = 3;
    case DGNB_BRONZE = 4;
    case DGNB_SILVER = 5;
    case DGNB_GOLD = 6;
    case DGNB_PLATINUM = 7;
    case HQE = 8;
    case NAHWOH = 9;
    case GREEN_POWER = 10;
    case EMICODE = 11;
    case WOOD_FROM_HERE = 12;
    case EPD = 13;

    public function getName(): string
    {
        return match ($this) {
            self::BNB_BRONZE => 'BNB-Zertifikat in Bronze',
            self::BNB_SILVER => 'BNB-Zertifikat in Silber',
            self::BNB_GOLD => 'BNB-Zertifikat in Gold',
            self::BREEAM => 'BREEAM®-Gütesiegel',
            self::DGNB_BRONZE => 'DGNB-Zertifikat in Bronze',
            self::DGNB_SILVER => 'DGNB-Zertifikat in Silber',
            self::DGNB_GOLD => 'DGNB-Zertifikat in Gold',
            self::DGNB_PLATINUM => 'DGNB-Zertifikat in Platin',
            self::HQE => 'HQE-Zertifikat',
            self::NAHWOH => 'Qualitätssiegel Nachhaltiger Wohnungsbau',
            self::GREEN_POWER => 'Ökostrom-Gütesiegel: Grüner Strom Label',
            self::EMICODE => 'Prüfzeichen EMICODE',
            self::WOOD_FROM_HERE => 'Umweltlabel Holz von Hier',
            self::EPD => 'EPD-Zertifikat'
        };
    }
}
