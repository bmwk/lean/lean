<?php

declare(strict_types=1);

namespace App\Domain\Entity\Property;

use App\Domain\Entity\AccountTrait;
use App\Repository\Property\OpenImmoAdditionalInformationRepository;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\UniqueConstraint;

#[Entity(repositoryClass: OpenImmoAdditionalInformationRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[UniqueConstraint(name: 'account_object_number_internal', columns: ['account_id', 'object_number_internal'])]
class OpenImmoAdditionalInformation
{
    use AccountTrait;

    #[Id]
    #[GeneratedValue]
    #[Column(type: 'integer', nullable: false, options: ['unsigned' => true])]
    private ?int $id;

    #[OneToOne(inversedBy: 'openImmoAdditionalInformation')]
    #[JoinColumn(nullable:  false)]
    private BuildingUnit $buildingUnit;

    #[Column(type: 'string', nullable: false)]
    private string $objectNumberInternal;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getBuildingUnit(): BuildingUnit
    {
        return $this->buildingUnit;
    }

    public function setBuildingUnit(BuildingUnit $buildingUnit): self
    {
        $this->buildingUnit = $buildingUnit;

        return $this;
    }

    public function getObjectNumberInternal(): string
    {
        return $this->objectNumberInternal;
    }

    public function setObjectNumberInternal(string $objectNumberInternal): self
    {
        $this->objectNumberInternal = $objectNumberInternal;

        return $this;
    }
}
