<?php

declare(strict_types=1);

namespace App\Domain\Entity\Property;

enum OwnershipType: int
{
    case PROPERTY = 0;
    case LEASEHOLD = 1;

    public function getName(): string
    {
        return match ($this) {
            self::PROPERTY => 'Eigentum',
            self::LEASEHOLD => 'Erbpacht'
        };
    }
}
