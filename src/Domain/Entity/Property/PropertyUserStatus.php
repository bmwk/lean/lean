<?php

declare(strict_types=1);

namespace App\Domain\Entity\Property;

enum PropertyUserStatus: int
{
    case CURRENT_PROPERTY_USER = 0;
    case FORMER_PROPERTY_USER = 1;

    public function getName(): string
    {
        return match ($this) {
            self::CURRENT_PROPERTY_USER => 'Aktueller Nutzer',
            self::FORMER_PROPERTY_USER => 'Ehemaliger Nutzer'
        };
    }
}
