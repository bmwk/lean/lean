<?php

declare(strict_types=1);

namespace App\Domain\Entity\Property;

enum BarrierFreeAccess: int
{
    case IS_AVAILABLE = 0;
    case CAN_BE_GUARANTEED = 1;
    case IS_CURRENTLY_NOT_AVAILABLE = 2;
    case IS_NOT_POSSIBLE = 3;

    public function getName(): string
    {
        return match ($this) {
            self::IS_AVAILABLE => 'ist vorhanden',
            self::CAN_BE_GUARANTEED => 'kann gewährleistet werden',
            self::IS_CURRENTLY_NOT_AVAILABLE => 'ist derzeit nicht vorhanden',
            self::IS_NOT_POSSIBLE => 'ist nicht möglich'
        };
    }
}
