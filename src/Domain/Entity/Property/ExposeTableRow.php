<?php

declare(strict_types=1);

namespace App\Domain\Entity\Property;

class ExposeTableRow
{
    /** @var ExposeValue[]  */
    private array $exposeValues = [];

    public function getExposeValues(): array
    {
        return $this->exposeValues;
    }

    public function setExposeValues(array $exposeValues): self
    {
        $this->exposeValues = $exposeValues;

        return $this;
    }

    public function addExposePropertySpacesValue(?ExposeValue $exposePropertySpacesValue): void
    {
        $this->exposeValues[] = $exposePropertySpacesValue;
    }
}
