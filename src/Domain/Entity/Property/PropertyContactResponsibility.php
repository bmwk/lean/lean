<?php

declare(strict_types=1);

namespace App\Domain\Entity\Property;

enum PropertyContactResponsibility: int
{
    case PROPERTY = 0;
    case MARKETING = 1;

    public function getName(): string
    {
        return match ($this) {
            self::PROPERTY => 'Objekt',
            self::MARKETING => 'Vermarktung'
        };
    }
}
