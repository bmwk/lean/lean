<?php

declare(strict_types=1);

namespace App\Domain\Entity\Property;

use App\Domain\Entity\Classification\IndustryClassification;
use App\Domain\Entity\CreatedAtTrait;
use App\Domain\Entity\PropertyOfferType;
use App\Domain\Entity\PropertyVacancyReport\PropertyVacancyReport;
use App\Domain\Entity\UpdatedAtTrait;
use App\Repository\Property\PropertyMarketingInformationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\Table;

#[Entity(repositoryClass: PropertyMarketingInformationRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[HasLifecycleCallbacks]
class PropertyMarketingInformation
{
    use CreatedAtTrait;
    use UpdatedAtTrait;

    #[Id]
    #[GeneratedValue]
    #[Column(type: Types::INTEGER, nullable: false, options: ['unsigned' => true])]
    private ?int $id = null;

    #[Column(type: Types::TEXT, nullable: true)]
    private ?string $potentialDescription = null;

    #[ManyToMany(targetEntity: IndustryClassification::class)]
    private Collection|array $possibleUsageIndustryClassifications;

    #[Column(type: Types::TEXT, nullable: true)]
    private ?string $environmentDescription = null;

    #[Column(type: Types::FLOAT, nullable: true)]
    private ?float $groundReferenceValue = null;

    #[Column(type: Types::FLOAT, nullable: true)]
    private ?float $groundValue = null;

    #[Column(type: Types::FLOAT, nullable: true)]
    private ?float $marketValue = null;

    #[Column(type: Types::TEXT, nullable: true)]
    private ?string $valueAffectingCircumstances = null;

    #[Column(type: Types::SMALLINT, nullable: true, enumType: PropertyMarketingStatus::class,  options: ['unsigned' => true])]
    private ?PropertyMarketingStatus $propertyMarketingStatus = null;

    #[Column(type: Types::SMALLINT, nullable: true, enumType: PropertyOfferType::class, options: ['unsigned' => true])]
    private ?PropertyOfferType $propertyOfferType = null;

    #[Column(type: Types::SMALLINT, nullable: true, enumType: PropertyPriceType::class, options: ['unsigned' => true])]
    private ?PropertyPriceType $propertyPriceType = null;

    #[Column(type: Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTime $availableFrom = null;

    #[Column(type: Types::SMALLINT, nullable: true, enumType: DevelopmentPotential::class, options: ['unsigned' => true])]
    private ?DevelopmentPotential $developmentPotential = null;

    #[OneToOne]
    #[JoinColumn(nullable: false)]
    private PropertyPricingInformation $propertyPricingInformation;

    /**
     * @var Collection<int, PropertyRestriction>|PropertyRestriction[]
     */
    #[ManyToMany(targetEntity: PropertyRestriction::class, inversedBy: 'propertyMarketingInformations')]
    private Collection|array $propertyRestrictions;

    public static function createFromPropertyVacancyReport(PropertyVacancyReport $propertyVacancyReport): self
    {
        $propertyMarketingInformation = new self();

        $propertyMarketingInformation->propertyOfferType = $propertyVacancyReport->getPropertyOfferType();

        return $propertyMarketingInformation;
    }

    public function __construct()
    {
        $this->propertyRestrictions = new ArrayCollection();
        $this->possibleUsageIndustryClassifications = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getPotentialDescription(): ?string
    {
        return $this->potentialDescription;
    }

    public function setPotentialDescription(?string $potentialDescription): self
    {
        $this->potentialDescription = $potentialDescription;

        return $this;
    }

    public function getPossibleUsageIndustryClassifications(): Collection|array
    {
        return $this->possibleUsageIndustryClassifications;
    }

    public function setPossibleUsageIndustryClassifications(Collection|array $possibleUsageIndustryClassifications): self
    {
        $this->possibleUsageIndustryClassifications = $possibleUsageIndustryClassifications;

        return $this;
    }

    public function getEnvironmentDescription(): ?string
    {
        return $this->environmentDescription;
    }

    public function setEnvironmentDescription(?string $environmentDescription): self
    {
        $this->environmentDescription = $environmentDescription;

        return $this;
    }

    public function getGroundReferenceValue(): ?float
    {
        return $this->groundReferenceValue;
    }

    public function setGroundReferenceValue(?float $groundReferenceValue): self
    {
        $this->groundReferenceValue = $groundReferenceValue;

        return $this;
    }

    public function getGroundValue(): ?float
    {
        return $this->groundValue;
    }

    public function setGroundValue(?float $groundValue): self
    {
        $this->groundValue = $groundValue;

        return $this;
    }

    public function getMarketValue(): ?float
    {
        return $this->marketValue;
    }

    public function setMarketValue(?float $marketValue): self
    {
        $this->marketValue = $marketValue;

        return $this;
    }

    public function getValueAffectingCircumstances(): ?string
    {
        return $this->valueAffectingCircumstances;
    }

    public function setValueAffectingCircumstances(?string $valueAffectingCircumstances): self
    {
        $this->valueAffectingCircumstances = $valueAffectingCircumstances;

        return $this;
    }

    public function getPropertyMarketingStatus(): ?PropertyMarketingStatus
    {
        return $this->propertyMarketingStatus;
    }

    public function setPropertyMarketingStatus(?PropertyMarketingStatus $propertyMarketingStatus): self
    {
        $this->propertyMarketingStatus = $propertyMarketingStatus;

        return $this;
    }

    public function getPropertyOfferType(): ?PropertyOfferType
    {
        return $this->propertyOfferType;
    }

    public function setPropertyOfferType(?PropertyOfferType $propertyOfferType): self
    {
        $this->propertyOfferType = $propertyOfferType;

        return $this;
    }

    public function getPropertyPriceType(): ?PropertyPriceType
    {
        return $this->propertyPriceType;
    }

    public function setPropertyPriceType(?PropertyPriceType $propertyPriceType): self
    {
        $this->propertyPriceType = $propertyPriceType;

        return $this;
    }

    public function getAvailableFrom(): ?\DateTime
    {
        return $this->availableFrom;
    }

    public function setAvailableFrom(?\DateTime $availableFrom): self
    {
        $this->availableFrom = $availableFrom;

        return $this;
    }

    public function getDevelopmentPotential(): ?DevelopmentPotential
    {
        return $this->developmentPotential;
    }

    public function setDevelopmentPotential(?DevelopmentPotential $developmentPotential): self
    {
        $this->developmentPotential = $developmentPotential;

        return $this;
    }

    public function getPropertyPricingInformation(): PropertyPricingInformation
    {
        return $this->propertyPricingInformation;
    }

    public function setPropertyPricingInformation(PropertyPricingInformation $propertyPricingInformation): self
    {
        $this->propertyPricingInformation = $propertyPricingInformation;

        return $this;
    }

    /**
     * @return Collection<int, PropertyRestriction>|PropertyRestriction[]
     */
    public function getPropertyRestrictions(): Collection|array
    {
        return $this->propertyRestrictions;
    }

    /**
     * @param Collection<int, PropertyRestriction>|PropertyRestriction[] $propertyRestrictions
     */
    public function setPropertyRestrictions(Collection|array $propertyRestrictions): self
    {
        $this->propertyRestrictions = $propertyRestrictions;

        return $this;
    }

    public function addPropertyRestriction(PropertyRestriction $propertyRestriction): self
    {
        $propertyRestriction->addPropertyMarketingInformation($this);

        $this->propertyRestrictions->add($propertyRestriction);

        return $this;
    }

    public function removePropertyRestriction(PropertyRestriction $propertyRestriction): self
    {
        $this->propertyRestrictions->removeElement($propertyRestriction);

        return $this;
    }
}
