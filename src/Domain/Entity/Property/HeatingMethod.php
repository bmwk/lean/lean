<?php

declare(strict_types=1);

namespace App\Domain\Entity\Property;

enum HeatingMethod: int
{
    case FLOOR_HEATING = 0;
    case FURNACE_HEATING = 1;
    case CENTRAL_HEATING = 2;
    case DISTRICT_HEATING = 3;
    case UNDERFLOOR_HEATING = 4;

    public function getName(): string
    {
        return match ($this) {
            self::FLOOR_HEATING => 'Etagenheizung',
            self::FURNACE_HEATING => 'Ofenheizung',
            self::CENTRAL_HEATING => 'Zentralheizung',
            self::DISTRICT_HEATING => 'Fernwärme',
            self::UNDERFLOOR_HEATING => 'Fußbodenheizung'
        };
    }
}
