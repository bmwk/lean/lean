<?php

declare(strict_types=1);

namespace App\Domain\Entity\Property;

enum InternetConnectionType: int
{
    case BROADBAND_CONNECTION = 0;
    case FIBER_OPTIC_CONNECTION = 1;

    public function getName(): string
    {
        return match ($this) {
            self::BROADBAND_CONNECTION => 'Breitbandanschluss',
            self::FIBER_OPTIC_CONNECTION => 'Glasfaseranschluss'
        };
    }
}
