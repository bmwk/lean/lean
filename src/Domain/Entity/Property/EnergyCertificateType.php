<?php

declare(strict_types=1);

namespace App\Domain\Entity\Property;

enum EnergyCertificateType: int
{
    case ENERGY_REQUIREMENT_CERTIFICATE = 0;
    case ENERGY_CONSUMPTION_CERTIFICATE = 1;

    public function getName(): string
    {
        return match ($this) {
            self::ENERGY_REQUIREMENT_CERTIFICATE => 'Energiebedarfsausweis',
            self::ENERGY_CONSUMPTION_CERTIFICATE => 'Energieverbrauchsausweis'
        };
    }
}
