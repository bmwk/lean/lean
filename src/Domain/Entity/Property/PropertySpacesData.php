<?php

declare(strict_types=1);

namespace App\Domain\Entity\Property;

use App\Domain\Entity\CreatedAtTrait;
use App\Domain\Entity\PropertyVacancyReport\PropertyVacancyReport;
use App\Domain\Entity\UpdatedAtTrait;
use App\Repository\Property\PropertySpacesDataRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;

#[Entity(repositoryClass: PropertySpacesDataRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[HasLifecycleCallbacks]
class PropertySpacesData
{
    use CreatedAtTrait;
    use UpdatedAtTrait;

    #[Id]
    #[GeneratedValue]
    #[Column(type: Types::INTEGER, nullable: false, options: ['unsigned' => true])]
    private ?int $id = null;

    #[Column(type: Types::FLOAT, nullable: true, options: ['unsigned' => true])]
    private ?float $roomCount = null;

    #[Column(type: Types::FLOAT, nullable: true, options: ['unsigned' => true])]
    private ?float $livingSpace = null;

    #[Column(type: Types::FLOAT, nullable: true, options: ['unsigned' => true])]
    private ?float $usableSpace = null;

    #[Column(type: Types::FLOAT, nullable: true, options: ['unsigned' => true])]
    private ?float $storeSpace = null;

    #[Column(type: Types::FLOAT, nullable: true, options: ['unsigned' => true])]
    private ?float $storageSpace = null;

    #[Column(type: Types::FLOAT, nullable: true, options: ['unsigned' => true])]
    private ?float $retailSpace = null;

    #[Column(type: Types::FLOAT, nullable: true, options: ['unsigned' => true])]
    private ?float $openSpace = null;

    #[Column(type: Types::FLOAT, nullable: true, options: ['unsigned' => true])]
    private ?float $officeSpace = null;

    #[Column(type: Types::FLOAT, nullable: true, options: ['unsigned' => true])]
    private ?float $officePartSpace = null;

    #[Column(type: Types::FLOAT, nullable: true, options: ['unsigned' => true])]
    private ?float $administrationSpace = null;

    #[Column(type: Types::FLOAT, nullable: true, options: ['unsigned' => true])]
    private ?float $gastronomySpace = null;

    #[Column(type: Types::FLOAT, nullable: true, options: ['unsigned' => true])]
    private ?float $productionSpace = null;

    #[Column(type: Types::FLOAT, nullable: true, options: ['unsigned' => true])]
    private ?float $plotSize = null;

    #[Column(type: Types::FLOAT, nullable: true, options: ['unsigned' => true])]
    private ?float $otherSpace = null;

    #[Column(type: Types::FLOAT, nullable: true, options: ['unsigned' => true])]
    private ?float $balconyTurfSpace = null;

    #[Column(type: Types::FLOAT, nullable: true, options: ['unsigned' => true])]
    private ?float $gardenSpace = null;

    #[Column(type: Types::FLOAT, nullable: true, options: ['unsigned' => true])]
    private ?float $cellarSpace = null;

    #[Column(type: Types::FLOAT, nullable: true, options: ['unsigned' => true])]
    private ?float $atticSpace = null;

    #[Column(type: Types::FLOAT, nullable: true, options: ['unsigned' => true])]
    private ?float $subsidiarySpace = null;

    #[Column(type: Types::FLOAT, nullable: true, options: ['unsigned' => true])]
    private ?float $heatableSurface = null;

    #[Column(type: Types::FLOAT, nullable: true, options: ['unsigned' => true])]
    private ?float $rentableArea = null;

    #[Column(type: Types::FLOAT, nullable: true, options: ['unsigned' => true])]
    private ?float $minimumSpaceForSeparation = null;

    #[Column(type: Types::SMALLINT, nullable: true, enumType: UsableOutdoorAreaPossibility::class, options: ['unsigned' => true])]
    private ?UsableOutdoorAreaPossibility $usableOutdoorAreaPossibility = null;

    #[Column(type: Types::FLOAT, nullable: true, options: ['unsigned' => true])]
    private ?float $totalOutdoorArea = null;

    #[Column(type: Types::FLOAT, nullable: true, options: ['unsigned' => true])]
    private ?float $usableOutdoorArea = null;

    #[Column(type: Types::FLOAT, nullable: true, options: ['unsigned' => true])]
    private ?float $outdoorSalesArea = null;

    #[Column(type: Types::BOOLEAN, nullable: true)]
    private ?bool $roofingPresent = null;

    public static function createFromPropertyVacancyReport(PropertyVacancyReport $propertyVacancyReport): self
    {
        $propertySpacesData = new self();

        $propertySpacesData->plotSize = $propertyVacancyReport->getPlotSize();
        $propertySpacesData->retailSpace = $propertyVacancyReport->getRetailSpace();
        $propertySpacesData->gastronomySpace = $propertyVacancyReport->getGastronomySpace();
        $propertySpacesData->livingSpace = $propertyVacancyReport->getLivingSpace();
        $propertySpacesData->usableSpace = $propertyVacancyReport->getUsableSpace();
        $propertySpacesData->subsidiarySpace = $propertyVacancyReport->getSubsidiarySpace();
        $propertySpacesData->usableOutdoorAreaPossibility = $propertyVacancyReport->getUsableOutdoorAreaPossibility();
        $propertySpacesData->usableOutdoorArea = $propertyVacancyReport->getUsableOutdoorArea();

        return $propertySpacesData;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getRoomCount(): ?float
    {
        return $this->roomCount;
    }

    public function setRoomCount(?float $roomCount): self
    {
        $this->roomCount = $roomCount;

        return $this;
    }

    public function getLivingSpace(): ?float
    {
        return $this->livingSpace;
    }

    public function setLivingSpace(?float $livingSpace): self
    {
        $this->livingSpace = $livingSpace;

        return $this;
    }

    public function getUsableSpace(): ?float
    {
        return $this->usableSpace;
    }

    public function setUsableSpace(?float $usableSpace): self
    {
        $this->usableSpace = $usableSpace;

        return $this;
    }

    public function getStoreSpace(): ?float
    {
        return $this->storeSpace;
    }

    public function setStoreSpace(?float $storeSpace): self
    {
        $this->storeSpace = $storeSpace;

        return $this;
    }

    public function getStorageSpace(): ?float
    {
        return $this->storageSpace;
    }

    public function setStorageSpace(?float $storageSpace): self
    {
        $this->storageSpace = $storageSpace;

        return $this;
    }

    public function getRetailSpace(): ?float
    {
        return $this->retailSpace;
    }

    public function setRetailSpace(?float $retailSpace): self
    {
        $this->retailSpace = $retailSpace;

        return $this;
    }

    public function getOpenSpace(): ?float
    {
        return $this->openSpace;
    }

    public function setOpenSpace(?float $openSpace): self
    {
        $this->openSpace = $openSpace;

        return $this;
    }

    public function getOfficeSpace(): ?float
    {
        return $this->officeSpace;
    }

    public function setOfficeSpace(?float $officeSpace): self
    {
        $this->officeSpace = $officeSpace;

        return $this;
    }

    public function getOfficePartSpace(): ?float
    {
        return $this->officePartSpace;
    }

    public function setOfficePartSpace(?float $officePartSpace): self
    {
        $this->officePartSpace = $officePartSpace;

        return $this;
    }

    public function getAdministrationSpace(): ?float
    {
        return $this->administrationSpace;
    }

    public function setAdministrationSpace(?float $administrationSpace): self
    {
        $this->administrationSpace = $administrationSpace;

        return $this;
    }

    public function getGastronomySpace(): ?float
    {
        return $this->gastronomySpace;
    }

    public function setGastronomySpace(?float $gastronomySpace): self
    {
        $this->gastronomySpace = $gastronomySpace;

        return $this;
    }

    public function getProductionSpace(): ?float
    {
        return $this->productionSpace;
    }

    public function setProductionSpace(?float $productionSpace): self
    {
        $this->productionSpace = $productionSpace;

        return $this;
    }

    public function getPlotSize(): ?float
    {
        return $this->plotSize;
    }

    public function setPlotSize(?float $plotSize): self
    {
        $this->plotSize = $plotSize;

        return $this;
    }

    public function getOtherSpace(): ?float
    {
        return $this->otherSpace;
    }

    public function setOtherSpace(?float $otherSpace): self
    {
        $this->otherSpace = $otherSpace;

        return $this;
    }

    public function getBalconyTurfSpace(): ?float
    {
        return $this->balconyTurfSpace;
    }

    public function setBalconyTurfSpace(?float $balconyTurfSpace): self
    {
        $this->balconyTurfSpace = $balconyTurfSpace;

        return $this;
    }

    public function getGardenSpace(): ?float
    {
        return $this->gardenSpace;
    }

    public function setGardenSpace(?float $gardenSpace): self
    {
        $this->gardenSpace = $gardenSpace;

        return $this;
    }

    public function getCellarSpace(): ?float
    {
        return $this->cellarSpace;
    }

    public function setCellarSpace(?float $cellarSpace): self
    {
        $this->cellarSpace = $cellarSpace;

        return $this;
    }

    public function getAtticSpace(): ?float
    {
        return $this->atticSpace;
    }

    public function setAtticSpace(?float $atticSpace): self
    {
        $this->atticSpace = $atticSpace;

        return $this;
    }

    public function getSubsidiarySpace(): ?float
    {
        return $this->subsidiarySpace;
    }

    public function setSubsidiarySpace(?float $subsidiarySpace): self
    {
        $this->subsidiarySpace = $subsidiarySpace;

        return $this;
    }

    public function getHeatableSurface(): ?float
    {
        return $this->heatableSurface;
    }

    public function setHeatableSurface(?float $heatableSurface): self
    {
        $this->heatableSurface = $heatableSurface;

        return $this;
    }

    public function getRentableArea(): ?float
    {
        return $this->rentableArea;
    }

    public function setRentableArea(?float $rentableArea): self
    {
        $this->rentableArea = $rentableArea;

        return $this;
    }

    public function getMinimumSpaceForSeparation(): ?float
    {
        return $this->minimumSpaceForSeparation;
    }

    public function setMinimumSpaceForSeparation(?float $minimumSpaceForSeparation): self
    {
        $this->minimumSpaceForSeparation = $minimumSpaceForSeparation;

        return $this;
    }

    public function getUsableOutdoorAreaPossibility(): ?UsableOutdoorAreaPossibility
    {
        return $this->usableOutdoorAreaPossibility;
    }

    public function setUsableOutdoorAreaPossibility(?UsableOutdoorAreaPossibility $usableOutdoorAreaPossibility): self
    {
        $this->usableOutdoorAreaPossibility = $usableOutdoorAreaPossibility;

        return $this;
    }

    public function getTotalOutdoorArea(): ?float
    {
        return $this->totalOutdoorArea;
    }

    public function setTotalOutdoorArea(?float $totalOutdoorArea): self
    {
        $this->totalOutdoorArea = $totalOutdoorArea;

        return $this;
    }

    public function getUsableOutdoorArea(): ?float
    {
        return $this->usableOutdoorArea;
    }

    public function setUsableOutdoorArea(?float $usableOutdoorArea): self
    {
        $this->usableOutdoorArea = $usableOutdoorArea;

        return $this;
    }

    public function getOutdoorSalesArea(): ?float
    {
        return $this->outdoorSalesArea;
    }

    public function setOutdoorSalesArea(?float $outdoorSalesArea): self
    {
        $this->outdoorSalesArea = $outdoorSalesArea;

        return $this;
    }

    public function isRoofingPresent(): ?bool
    {
        return $this->roofingPresent;
    }

    public function setRoofingPresent(?bool $roofingPresent): self
    {
        $this->roofingPresent = $roofingPresent;

        return $this;
    }
}
