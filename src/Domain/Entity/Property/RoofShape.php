<?php

declare(strict_types=1);

namespace App\Domain\Entity\Property;

enum RoofShape: int
{
    case FLAT_ROOF = 0;
    case PENT_ROOF = 1;
    case PITCHED_ROOF = 2;
    case SHED_ROOF = 3;
    case SADDLE_OR_GABLE_ROOF = 4;
    case BUTTERFLY_ROOF = 5;
    case TRENCH_ROOF = 6;
    case HIP_ROOF = 7;
    case CRESTED_HIP_OR_CREST_HIP_ROOF = 8;
    case TENT_ROOF = 9;
    case FOOT_HIP_ROOF = 10;
    case MANSARD_ROOF = 11;
    case ZOLLINGER_ROOF = 12;
    case BARREL_ROOF = 13;
    case ARCHED_ROOF = 14;
    case RHOMBIC_ROOF = 15;
    case FOLDED_ROOF = 16;
    case WELSH_HOOD = 17;
    case CONICAL_ROOF = 18;
    case HELMET = 19;
    case ONION_HELMET = 20;
    case PAGODA_ROOF = 21;

    public function getName(): string
    {
        return match ($this) {
            self::FLAT_ROOF => 'Flachdach',
            self::PENT_ROOF => 'Pultdach',
            self::PITCHED_ROOF => 'Schleppdach',
            self::SHED_ROOF => 'Sheddach',
            self::SADDLE_OR_GABLE_ROOF => 'Sattel-/Giebeldach',
            self::BUTTERFLY_ROOF => 'Schmetterlingsdach',
            self::TRENCH_ROOF => 'Grabendach',
            self::HIP_ROOF => 'Walmdach',
            self::CRESTED_HIP_OR_CREST_HIP_ROOF => 'Schopfwalm-/Krüppelwalmdach',
            self::TENT_ROOF => 'Zeltdach',
            self::FOOT_HIP_ROOF => 'Fußwalmdach',
            self::MANSARD_ROOF => 'Mansarddach',
            self::ZOLLINGER_ROOF => 'Zollingerdach',
            self::BARREL_ROOF => 'Tonnendach',
            self::ARCHED_ROOF => 'Bogendach',
            self::RHOMBIC_ROOF => 'Rhombendach',
            self::FOLDED_ROOF => 'Faltdach',
            self::WELSH_HOOD => 'Welsche Haube',
            self::CONICAL_ROOF => 'Kegeldach',
            self::HELMET => 'Helm',
            self::ONION_HELMET => 'Zwiebelhelm',
            self::PAGODA_ROOF => 'Pagodendach'
        };
    }
}