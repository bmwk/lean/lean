<?php

declare(strict_types=1);

namespace App\Domain\Entity\Property;

use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\ValueRequirement;

abstract class AbstractBuildingUnitFilter
{
    protected ?array $propertyStatus = null;

    protected ?array $industryClassifications = null;

    protected ?float $areaSizeMinimum = null;

    protected ?float $areaSizeMaximum = null;

    protected ?float $shopWindowFrontWidthMinimum = null;

    protected ?float $shopWindowFrontWidthMaximum = null;

    protected ?BarrierFreeAccess $barrierFreeAccess = null;

    protected ?AccountUser $assignedToAccountUser = null;

    protected ?array $propertyOfferTypes = null;

    protected ?ValueRequirement $purchasePriceGross = null;

    protected ?ValueRequirement $purchasePricePerSquareMeter = null;

    protected ?ValueRequirement $coldRent = null;

    protected ?ValueRequirement $rentalPricePerSquareMeter = null;

    protected ?ValueRequirement $lease = null;

    public function getPropertyStatus(): ?array
    {
        return $this->propertyStatus;
    }

    public function setPropertyStatus(?array $propertyStatus): self
    {
        $this->propertyStatus = $propertyStatus;

        return $this;
    }

    public function getIndustryClassifications(): ?array
    {
        return $this->industryClassifications;
    }

    public function setIndustryClassifications(?array $industryClassifications): self
    {
        $this->industryClassifications = $industryClassifications;

        return $this;
    }

    public function getAreaSizeMinimum(): ?float
    {
        return $this->areaSizeMinimum;
    }

    public function setAreaSizeMinimum(?float $areaSizeMinimum): self
    {
        $this->areaSizeMinimum = $areaSizeMinimum;

        return $this;
    }

    public function getAreaSizeMaximum(): ?float
    {
        return $this->areaSizeMaximum;
    }

    public function setAreaSizeMaximum(?float $areaSizeMaximum): self
    {
        $this->areaSizeMaximum = $areaSizeMaximum;

        return $this;
    }

    public function getShopWindowFrontWidthMinimum(): ?float
    {
        return $this->shopWindowFrontWidthMinimum;
    }

    public function setShopWindowFrontWidthMinimum(?float $shopWindowFrontWidthMinimum): self
    {
        $this->shopWindowFrontWidthMinimum = $shopWindowFrontWidthMinimum;

        return $this;
    }

    public function getShopWindowFrontWidthMaximum(): ?float
    {
        return $this->shopWindowFrontWidthMaximum;
    }

    public function setShopWindowFrontWidthMaximum(?float $shopWindowFrontWidthMaximum): self
    {
        $this->shopWindowFrontWidthMaximum = $shopWindowFrontWidthMaximum;

        return $this;
    }

    public function getBarrierFreeAccess(): ?BarrierFreeAccess
    {
        return $this->barrierFreeAccess;
    }

    public function setBarrierFreeAccess(?BarrierFreeAccess $barrierFreeAccess): self
    {
        $this->barrierFreeAccess = $barrierFreeAccess;

        return $this;
    }

    public function getAssignedToAccountUser(): ?AccountUser
    {
        return $this->assignedToAccountUser;
    }

    public function setAssignedToAccountUser(?AccountUser $assignedToAccountUser): self
    {
        $this->assignedToAccountUser = $assignedToAccountUser;

        return $this;
    }

    public function getPropertyOfferTypes(): ?array
    {
        return $this->propertyOfferTypes;
    }

    public function setPropertyOfferTypes(?array $propertyOfferTypes): self
    {
        $this->propertyOfferTypes = $propertyOfferTypes;

        return $this;
    }

    public function getPurchasePriceGross(): ?ValueRequirement
    {
        return $this->purchasePriceGross;
    }

    public function setPurchasePriceGross(?ValueRequirement $purchasePriceGross): self
    {
        $this->purchasePriceGross = $purchasePriceGross;

        return $this;
    }

    public function getPurchasePricePerSquareMeter(): ?ValueRequirement
    {
        return $this->purchasePricePerSquareMeter;
    }

    public function setPurchasePricePerSquareMeter(?ValueRequirement $purchasePricePerSquareMeter): self
    {
        $this->purchasePricePerSquareMeter = $purchasePricePerSquareMeter;

        return $this;
    }

    public function getColdRent(): ?ValueRequirement
    {
        return $this->coldRent;
    }

    public function setColdRent(?ValueRequirement $coldRent): self
    {
        $this->coldRent = $coldRent;

        return $this;
    }

    public function getRentalPricePerSquareMeter(): ?ValueRequirement
    {
        return $this->rentalPricePerSquareMeter;
    }

    public function setRentalPricePerSquareMeter(?ValueRequirement $rentalPricePerSquareMeter): self
    {
        $this->rentalPricePerSquareMeter = $rentalPricePerSquareMeter;

        return $this;
    }

    public function getLease(): ?ValueRequirement
    {
        return $this->lease;
    }

    public function setLease(?ValueRequirement $lease): self
    {
        $this->lease = $lease;

        return $this;
    }

    public abstract function isFilterActive(): bool;
}
