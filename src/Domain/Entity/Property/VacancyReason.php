<?php

declare(strict_types=1);

namespace App\Domain\Entity\Property;

enum VacancyReason: int
{
    case BUSINESS_CLOSURE = 0;
    case FUTURE_CONTRACT = 1;
    case UNECONOMIC = 2;
    case STRUCTURAL_REASON = 3;
    case DEMOLITION = 4;
    case RECONSTRUCTION = 5;
    case RENOVATION = 6;
    case OTHER = 7;
    case INTENTIONAL = 8;

    public function getName(): string
    {
        return match ($this) {
            self::BUSINESS_CLOSURE => 'Geschäftsaufgabe',
            self::FUTURE_CONTRACT => 'Zukünftiger Vertrag',
            self::UNECONOMIC => 'Unwirtschaftlich',
            self::STRUCTURAL_REASON => 'Struktureller Grund',
            self::DEMOLITION => 'Abriss',
            self::RECONSTRUCTION => 'Umbau',
            self::RENOVATION => 'Renovierung/Sanierung',
            self::OTHER => 'Anderer Grund',
            self::INTENTIONAL => 'Beabsichtigter Leerstand'
        };
    }
}
