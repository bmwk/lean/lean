<?php

declare(strict_types=1);

namespace App\Domain\Entity\Property;

enum WindowType: int
{
    case ALUMINIUM_WINDOW = 0;
    case WOODEN_WINDOW = 1;
    case PLASTIC_WINDOW = 2;
    case MUNTIN_WINDOW = 3;

    public function getName(): string
    {
        return match ($this) {
            self::ALUMINIUM_WINDOW => 'Aluminiumfenster',
            self::WOODEN_WINDOW => 'Holzfenster',
            self::PLASTIC_WINDOW => 'Kunststofffenster',
            self::MUNTIN_WINDOW => 'Sprossenfenster',
        };
    }
}
