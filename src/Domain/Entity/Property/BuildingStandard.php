<?php

declare(strict_types=1);

namespace App\Domain\Entity\Property;

enum BuildingStandard: int
{
    case LOW = 0;
    case MEDIUM = 1;
    case HIGH = 2;

    public function getName(): string
    {
        return match ($this) {
            self::LOW => 'niedrig',
            self::MEDIUM => 'mittel',
            self::HIGH => 'hoch'
        };
    }
}
