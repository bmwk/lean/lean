<?php

declare(strict_types=1);

namespace App\Domain\Entity\Property;

enum Currency: int
{
    case EURO = 0;
    case DOLLAR = 1;

    public function getName(): string
    {
        return match ($this) {
            self::EURO => '€',
            self::DOLLAR => '$'
        };
    }

    public function getIsoName(): string
    {
        return match ($this) {
            self::EURO => 'EUR',
            self::DOLLAR => 'USD'
        };
    }
}
