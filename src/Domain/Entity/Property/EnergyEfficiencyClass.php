<?php

declare(strict_types=1);

namespace App\Domain\Entity\Property;

enum EnergyEfficiencyClass: int
{
    case A_PLUS = 0;
    case A = 1;
    case B = 2;
    case C = 3;
    case D = 4;
    case E = 5;
    case F = 6;
    case G = 7;
    case H = 8;

    public function getName(): string
    {
        return match ($this) {
            self::A_PLUS => 'A+',
            self::A => 'A',
            self::B => 'B',
            self::C => 'C',
            self::D => 'D',
            self::E => 'E',
            self::F => 'F',
            self::G => 'G',
            self::H => 'H'
        };
    }
}
