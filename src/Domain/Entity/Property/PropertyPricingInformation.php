<?php

declare(strict_types=1);

namespace App\Domain\Entity\Property;

use App\Domain\Entity\CreatedAtTrait;
use App\Domain\Entity\UpdatedAtTrait;
use App\Repository\Property\PropertyPricingInformationRepository;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;

#[Entity(repositoryClass: PropertyPricingInformationRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[HasLifecycleCallbacks]
class PropertyPricingInformation
{
    use CreatedAtTrait;
    use UpdatedAtTrait;

    #[Id]
    #[GeneratedValue]
    #[Column(type: 'integer', nullable: false, options: ['unsigned' => true])]
    private ?int $id = null;

    #[Column(type: 'float', nullable: true, options: ['unsigned' => true])]
    private ?float $purchasePriceNet = null;

    #[Column(type: 'float', nullable: true, options: ['unsigned' => true])]
    private ?float $purchasePriceGross = null;

    #[Column(type: 'float', nullable: true, options: ['unsigned' => true])]
    private ?float $purchasePricePerSquareMeter = null;

    #[Column(type: 'float', nullable: true, options: ['unsigned' => true])]
    private ?float $netColdRent = null;

    #[Column(type: 'float', nullable: true, options: ['unsigned' => true])]
    private ?float $coldRent = null;

    #[Column(type: 'float', nullable: true, options: ['unsigned' => true])]
    private ?float $rentIncludingHeating = null;

    #[Column(type: 'float', nullable: true, options: ['unsigned' => true])]
    private ?float $additionalCosts = null;

    #[Column(type: 'boolean', nullable: true)]
    private ?bool $heatingCostsIncluded = null;

    #[Column(type: 'float', nullable: true, options: ['unsigned' => true])]
    private ?float $rentalPricePerSquareMeter = null;

    #[Column(type: 'float', nullable: true, options: ['unsigned' => true])]
    private ?float $lease = null;

    #[Column(type: 'float', nullable: true, options: ['unsigned' => true])]
    private ?float $leasehold = null;

    #[Column(type: 'float', nullable: true, options: ['unsigned' => true])]
    private ?float $houseMoney = null;

    #[Column(type: 'boolean', nullable: true)]
    private ?bool $commissionable = null;

    #[Column(type: 'string', length: 100, nullable: true)]
    private ?string $insideCommission = null;

    #[Column(type: 'string', length: 100, nullable: true)]
    private ?string $outsideCommission = null;

    #[Column(type: 'float', nullable: true, options: ['unsigned' => true])]
    private ?float $commissionNet = null;

    #[Column(type: 'float', nullable: true, options: ['unsigned' => true])]
    private ?float $commissionGross = null;

    #[Column(type: 'smallint', nullable: true, enumType: Currency::class, options: ['unsigned' => true])]
    private ?Currency $currency = null;

    #[Column(type: 'string', length: 100, nullable: true)]
    private ?string $freeTextPrice = null;

    #[Column(type: 'float', nullable: true, options: ['unsigned' => true])]
    private ?float $deposit = null;

    #[Column(type: 'string', length: 100, nullable: true)]
    private ?string $depositText = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getPurchasePriceNet(): ?float
    {
        return $this->purchasePriceNet;
    }

    public function setPurchasePriceNet(?float $purchasePriceNet): self
    {
        $this->purchasePriceNet = $purchasePriceNet;

        return $this;
    }

    public function getPurchasePriceGross(): ?float
    {
        return $this->purchasePriceGross;
    }

    public function setPurchasePriceGross(?float $purchasePriceGross): self
    {
        $this->purchasePriceGross = $purchasePriceGross;

        return $this;
    }

    public function getPurchasePricePerSquareMeter(): ?float
    {
        return $this->purchasePricePerSquareMeter;
    }

    public function setPurchasePricePerSquareMeter(?float $purchasePricePerSquareMeter): self
    {
        $this->purchasePricePerSquareMeter = $purchasePricePerSquareMeter;

        return $this;
    }

    public function getNetColdRent(): ?float
    {
        return $this->netColdRent;
    }

    public function setNetColdRent(?float $netColdRent): self
    {
        $this->netColdRent = $netColdRent;

        return $this;
    }

    public function getColdRent(): ?float
    {
        return $this->coldRent;
    }

    public function setColdRent(?float $coldRent): self
    {
        $this->coldRent = $coldRent;

        return $this;
    }

    public function getRentIncludingHeating(): ?float
    {
        return $this->rentIncludingHeating;
    }

    public function setRentIncludingHeating(?float $rentIncludingHeating): self
    {
        $this->rentIncludingHeating = $rentIncludingHeating;

        return $this;
    }

    public function getAdditionalCosts(): ?float
    {
        return $this->additionalCosts;
    }

    public function setAdditionalCosts(?float $additionalCosts): self
    {
        $this->additionalCosts = $additionalCosts;

        return $this;
    }

    public function getHeatingCostsIncluded(): ?bool
    {
        return $this->heatingCostsIncluded;
    }

    public function setHeatingCostsIncluded(?bool $heatingCostsIncluded): self
    {
        $this->heatingCostsIncluded = $heatingCostsIncluded;

        return $this;
    }

    public function getRentalPricePerSquareMeter(): ?float
    {
        return $this->rentalPricePerSquareMeter;
    }

    public function setRentalPricePerSquareMeter(?float $rentalPricePerSquareMeter): self
    {
        $this->rentalPricePerSquareMeter = $rentalPricePerSquareMeter;

        return $this;
    }

    public function getLease(): ?float
    {
        return $this->lease;
    }

    public function setLease(?float $lease): self
    {
        $this->lease = $lease;

        return $this;
    }

    public function getLeasehold(): ?float
    {
        return $this->leasehold;
    }

    public function setLeasehold(?float $leasehold): self
    {
        $this->leasehold = $leasehold;

        return $this;
    }

    public function getHouseMoney(): ?float
    {
        return $this->houseMoney;
    }

    public function setHouseMoney(?float $houseMoney): self
    {
        $this->houseMoney = $houseMoney;

        return $this;
    }

    public function isCommissionable(): ?bool
    {
        return $this->commissionable;
    }

    public function setCommissionable(?bool $commissionable): self
    {
        $this->commissionable = $commissionable;

        return $this;
    }

    public function getInsideCommission(): ?string
    {
        return $this->insideCommission;
    }

    public function setInsideCommission(?string $insideCommission): self
    {
        $this->insideCommission = $insideCommission;

        return $this;
    }

    public function getOutsideCommission(): ?string
    {
        return $this->outsideCommission;
    }

    public function setOutsideCommission(?string $outsideCommission): self
    {
        $this->outsideCommission = $outsideCommission;

        return $this;
    }

    public function getCommissionNet(): ?float
    {
        return $this->commissionNet;
    }

    public function setCommissionNet(?float $commissionNet): self
    {
        $this->commissionNet = $commissionNet;

        return $this;
    }

    public function getCommissionGross(): ?float
    {
        return $this->commissionGross;
    }

    public function setCommissionGross(?float $commissionGross): self
    {
        $this->commissionGross = $commissionGross;

        return $this;
    }

    public function getCurrency(): ?Currency
    {
        return $this->currency;
    }

    public function setCurrency(?Currency $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function getFreeTextPrice(): ?string
    {
        return $this->freeTextPrice;
    }

    public function setFreeTextPrice(?string $freeTextPrice): self
    {
        $this->freeTextPrice = $freeTextPrice;

        return $this;
    }

    public function getDeposit(): ?float
    {
        return $this->deposit;
    }

    public function setDeposit(?float $deposit): self
    {
        $this->deposit = $deposit;

        return $this;
    }

    public function getDepositText(): ?string
    {
        return $this->depositText;
    }

    public function setDepositText(?string $depositText): self
    {
        $this->depositText = $depositText;

        return $this;
    }
}
