<?php

declare(strict_types=1);

namespace App\Domain\Entity\Property;

class ExposeAddress
{
    private ?string $streetName = null;
    private ?string $houseNumber = null;
    private ?string $placeName = null;
    private ?string $quarterPlaceName = null;
    private ?string $postalCode = null;

    public static function createFromAddress(Address $address): self
    {
        $exposeAddress = new self();

        $exposeAddress->streetName = $address->getStreetName();
        $exposeAddress->houseNumber = $address->getHouseNumber();
        $exposeAddress->placeName = $address->getPlace()->getPlaceName();

        if ($address->getQuarterPlace() !== null) {
            $exposeAddress->quarterPlaceName = '(' . $address->getQuarterPlace()->getPlaceName() . ')';
        }

        $exposeAddress->postalCode = $address->getPostalCode();

        return $exposeAddress;
    }

    public function getStreetName(): ?string
    {
        return $this->streetName;
    }

    public function getHouseNumber(): ?string
    {
        return $this->houseNumber;
    }

    public function getPlaceName(): ?string
    {
        return $this->placeName;
    }

    public function getQuarterPlaceName(): ?string
    {
        return $this->quarterPlaceName;
    }

    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }
}
