<?php

declare(strict_types=1);

namespace App\Domain\Entity\Property;

use App\Domain\Entity\CreatedAtTrait;
use App\Domain\Entity\UpdatedAtTrait;
use App\Repository\Property\PropertyRestrictionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\Table;

#[Entity(repositoryClass: PropertyRestrictionRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[HasLifecycleCallbacks]
class PropertyRestriction
{
    use CreatedAtTrait;
    use UpdatedAtTrait;

    #[Id]
    #[GeneratedValue]
    #[Column(type: 'integer', nullable: false, options: ['unsigned' => true])]
    private ?int $id = null;

    #[Column(type: 'smallint', nullable: false, enumType: PropertyRestrictionType::class, options: ['unsigned' => true])]
    private PropertyRestrictionType $propertyRestrictionType;

    #[Column(type: 'text', nullable: true)]
    private ?string $description = null;

    /**
     * @var Collection<int, PropertyMarketingInformation>|PropertyMarketingInformation[]
     */
    #[ManyToMany(targetEntity: PropertyMarketingInformation::class, mappedBy: 'propertyRestrictions')]
    private Collection|array $propertyMarketingInformations;

    public function __construct()
    {
        $this->propertyMarketingInformations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getPropertyRestrictionType(): PropertyRestrictionType
    {
        return $this->propertyRestrictionType;
    }

    public function setPropertyRestrictionType(PropertyRestrictionType $propertyRestrictionType): self
    {
        $this->propertyRestrictionType = $propertyRestrictionType;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection<int, PropertyMarketingInformation>|PropertyMarketingInformation[]
     */
    public function getPropertyMarketingInformations(): Collection|array
    {
        return $this->propertyMarketingInformations;
    }

    /**
     * @param Collection<int, PropertyMarketingInformation>|PropertyMarketingInformation[] $propertyMarketingInformations
     */
    public function setPropertyMarketingInformations(Collection|array $propertyMarketingInformations): self
    {
        $this->propertyMarketingInformations = $propertyMarketingInformations;

        return $this;
    }

    public function addPropertyMarketingInformation(PropertyMarketingInformation $propertyMarketingInformation): self
    {
        if ($this->propertyMarketingInformations->contains($propertyMarketingInformation) === false) {
            $this->propertyMarketingInformations->add($propertyMarketingInformation);
        }

        return $this;
    }

    public function removePropertyMarketingInformation(PropertyMarketingInformation $propertyMarketingInformation): self
    {
        if ($this->propertyMarketingInformations->contains($propertyMarketingInformation) === true) {
            $this->propertyMarketingInformations->removeElement($propertyMarketingInformation);
        }

        return $this;
    }
}
