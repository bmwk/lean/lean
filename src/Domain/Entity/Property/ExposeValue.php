<?php

declare(strict_types=1);

namespace App\Domain\Entity\Property;

class ExposeValue
{
    private string $label;
    private string $value;
    private bool $squareMeterValue = false;

    public static function createFromLabelValueAndIsSquareMeterValue(
        string $label,
        string $value,
        bool $isSquareMeterValue = false
    ): self {
        $exposePropertySpacesValue = new ExposeValue();

        $exposePropertySpacesValue->label = $label;
        $exposePropertySpacesValue->value = $value;
        $exposePropertySpacesValue->squareMeterValue = $isSquareMeterValue;

        return $exposePropertySpacesValue;
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function isSquareMeterValue(): bool
    {
        return $this->squareMeterValue;
    }

    public function setSquareMeterValue(bool $squareMeterValue): self
    {
        $this->squareMeterValue = $squareMeterValue;

        return $this;
    }
}
