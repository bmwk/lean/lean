<?php

declare(strict_types=1);

namespace App\Domain\Entity\Property;

enum MonumentProtectionType: int
{
    case SINGLE_MONUMENT = 0;
    case ENSEMBLE_MONUMENT = 1;

    public function getName(): string
    {
        return match ($this) {
            self::SINGLE_MONUMENT => 'Einzeldenkmal',
            self::ENSEMBLE_MONUMENT => 'Ensembledenkmal'
        };
    }
}
