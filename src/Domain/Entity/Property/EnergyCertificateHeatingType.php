<?php

declare(strict_types=1);

namespace App\Domain\Entity\Property;

enum EnergyCertificateHeatingType: int
{
    case OIL = 0;
    case GAS = 1;
    case ELECTRIC = 2;
    case ALTERNATIVE_ENERGY_SOURCES = 3;
    case SOLAR_SYSTEM = 4;
    case HEAT_PUMP_BRINE_WATER = 5;
    case HEAT_PUMP_AIR_WATER = 6;
    case DISTRICT_HEATING = 7;
    case COMBINED_HEAT_AND_POWER_PLANT = 8;
    case SUPPLEMENTARY_DECENTRALIZED_HOT_WATER = 9;
    case PELLET_HEATING = 10;

    public function getName(): string
    {
        return match ($this) {
            self::OIL => 'Öl',
            self::GAS => 'Gas',
            self::ELECTRIC => 'Elektro',
            self::ALTERNATIVE_ENERGY_SOURCES => 'Alternative Energieträger',
            self::SOLAR_SYSTEM => 'Solaranlage',
            self::HEAT_PUMP_BRINE_WATER => 'Wärmepumpe (Sole-Wasser)',
            self::HEAT_PUMP_AIR_WATER => 'Wärmepumpe (Luft-Wasser)',
            self::DISTRICT_HEATING => 'Fernwärme',
            self::COMBINED_HEAT_AND_POWER_PLANT => 'Blockheizkraftwerk',
            self::SUPPLEMENTARY_DECENTRALIZED_HOT_WATER => 'Ergänzendes dezentrales Warmwasser',
            self::PELLET_HEATING => 'Pelletheizung'
        };
    }
}
