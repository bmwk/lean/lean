<?php

declare(strict_types=1);

namespace App\Domain\Entity\Property;

class BuildingUnitFilter extends AbstractBuildingUnitFilter
{
    private ?array $propertyUsageStatus = null;

    private ?array $industryClassificationsLevelTwo = null;

    private ?\DateTime $lastUpdatedFrom = null;

    private ?\DateTime $lastUpdatedTill = null;

    public function getPropertyUsageStatus(): ?array
    {
        return $this->propertyUsageStatus;
    }

    public function setPropertyUsageStatus(?array $propertyUsageStatus): self
    {
        $this->propertyUsageStatus = $propertyUsageStatus;

        return $this;
    }

    public function getIndustryClassificationsLevelTwo(): ?array
    {
        return $this->industryClassificationsLevelTwo;
    }

    public function setIndustryClassificationsLevelTwo(?array $industryClassificationsLevelTwo): self
    {
        $this->industryClassificationsLevelTwo = $industryClassificationsLevelTwo;

        return $this;
    }

    public function getLastUpdatedFrom(): ?\DateTime
    {
        return $this->lastUpdatedFrom;
    }

    public function setLastUpdatedFrom(?\DateTime $lastUpdatedFrom): self
    {
        $this->lastUpdatedFrom = $lastUpdatedFrom;

        return $this;
    }

    public function getLastUpdatedTill(): ?\DateTime
    {
        return $this->lastUpdatedTill;
    }

    public function setLastUpdatedTill(?\DateTime $lastUpdatedTill): self
    {
        $this->lastUpdatedTill = $lastUpdatedTill;

        return $this;
    }

    public function isFilterActive(): bool
    {
        if (
            $this->areaSizeMinimum === null
            && $this->areaSizeMaximum === null
            && $this->shopWindowFrontWidthMinimum === null
            && $this->shopWindowFrontWidthMaximum === null
            && $this->lastUpdatedFrom === null
            && $this->lastUpdatedTill === null
            && $this->assignedToAccountUser === null
            && $this->barrierFreeAccess === null
            && empty($this->propertyOfferTypes) === true
            && empty($this->industryClassifications) === true
            && empty($this->propertyStatus) === true
            && empty($this->propertyUsageStatus) === true
        ) {
            return false;
        }

        return true;
    }
}
