<?php

declare(strict_types=1);

namespace App\Domain\Entity\Property;

enum DevelopmentPotential: int
{
    case LOW_POTENTIAL = 0;
    case MODERATE_POTENTIAL = 1;
    case HIGH_POTENTIAL = 2;

    public function getName(): string
    {
        return match ($this) {
            self::LOW_POTENTIAL => 'geringes Entwicklungspotenzial',
            self::MODERATE_POTENTIAL => 'mäßiges Entwicklungspotenzial',
            self::HIGH_POTENTIAL => 'hohes Entwicklungspotenzial'
        };
    }
}
