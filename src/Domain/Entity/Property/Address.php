<?php

declare(strict_types=1);

namespace App\Domain\Entity\Property;

use App\Domain\Entity\CreatedAtTrait;
use App\Domain\Entity\Place;
use App\Domain\Entity\PropertyVacancyReport\PropertyVacancyReport;
use App\Domain\Entity\UpdatedAtTrait;
use App\Repository\Property\AddressRepository;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\Table;

#[Entity(repositoryClass: AddressRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[HasLifecycleCallbacks]
class Address
{
    use CreatedAtTrait;
    use UpdatedAtTrait;

    #[Id]
    #[GeneratedValue]
    #[Column(type: 'integer', nullable: false, options: ['unsigned' => true])]
    private ?int $id = null;

    #[ManyToOne]
    #[JoinColumn(nullable: false)]
    private Place $place;

    #[Column(type: 'string', length: 20, nullable: false)]
    private string $postalCode;

    #[Column(type: 'string', length: 190, nullable: false)]
    private string $streetName;

    #[Column(type: 'string', length: 20, nullable: false)]
    private string $houseNumber;

    #[ManyToOne]
    #[JoinColumn(nullable: true)]
    private ?Place $boroughPlace = null;

    #[ManyToOne]
    #[JoinColumn(nullable: true)]
    private ?Place $quarterPlace = null;

    #[ManyToOne]
    #[JoinColumn(nullable: true)]
    private ?Place $neighbourhoodPlace = null;

    #[Column(type: 'text', nullable: true)]
    private ?string $additionalAddressInformation = null;

    public static function createFromPropertyVacancyReport(PropertyVacancyReport $propertyVacancyReport): self
    {
        $address = new self();

        $address->streetName = $propertyVacancyReport->getStreetName();
        $address->houseNumber = $propertyVacancyReport->getHouseNumber();
        $address->postalCode = $propertyVacancyReport->getPostalCode();
        $address->place = $propertyVacancyReport->getPlace();
        $address->boroughPlace = $propertyVacancyReport->getBoroughPlace();
        $address->quarterPlace = $propertyVacancyReport->getQuarterPlace();

        return $address;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getPlace(): Place
    {
        return $this->place;
    }

    public function setPlace(Place $place): self
    {
        $this->place = $place;

        return $this;
    }

    public function getPostalCode(): string
    {
        return $this->postalCode;
    }

    public function setPostalCode(string $postalCode): self
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    public function getStreetName(): string
    {
        return $this->streetName;
    }

    public function setStreetName(string $streetName): self
    {
        $this->streetName = $streetName;

        return $this;
    }

    public function getHouseNumber(): string
    {
        return $this->houseNumber;
    }

    public function setHouseNumber(string $houseNumber): self
    {
        $this->houseNumber = $houseNumber;

        return $this;
    }

    public function getBoroughPlace(): ?Place
    {
        return $this->boroughPlace;
    }

    public function setBoroughPlace(?Place $boroughPlace): self
    {
        $this->boroughPlace = $boroughPlace;

        return $this;
    }

    public function getQuarterPlace(): ?Place
    {
        return $this->quarterPlace;
    }

    public function setQuarterPlace(?Place $quarterPlace): self
    {
        $this->quarterPlace = $quarterPlace;

        return $this;
    }

    public function getNeighbourhoodPlace(): ?Place
    {
        return $this->neighbourhoodPlace;
    }

    public function setNeighbourhoodPlace(?Place $neighbourhoodPlace): self
    {
        $this->neighbourhoodPlace = $neighbourhoodPlace;

        return $this;
    }

    public function getAdditionalAddressInformation(): ?string
    {
        return $this->additionalAddressInformation;
    }

    public function setAdditionalAddressInformation(?string $additionalAddressInformation): self
    {
        $this->additionalAddressInformation = $additionalAddressInformation;

        return $this;
    }
}
