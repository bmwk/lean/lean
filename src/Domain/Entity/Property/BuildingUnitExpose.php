<?php

declare(strict_types=1);

namespace App\Domain\Entity\Property;

use App\Domain\Entity\AccountConfiguration;
use App\Domain\Entity\AccountLegalTextsConfiguration;
use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\Classification\IndustryClassification;
use App\Domain\Entity\Image;
use App\Domain\Entity\Person\PersonType;
use App\Domain\Entity\PropertyOfferType;

class BuildingUnitExpose
{
    private ?string $inFloors = null;
    private ?string $locationCategoryName = null;
    private ?string $concatenatedPossibleUsageIndustryClassificationNames = null;
    private ?string $propertyOfferTypeName = null;
    private ?string $coldRentWithCurrency = null;
    private ?string $depositWithCurrency = null;
    private ?bool $commissionable = null;
    private ?string $shopWindowFrontWidth = null;
    private ?string $shopWidth = null;
    private ?bool $showroomAvailable = null;
    private ?bool $groundLevelSalesArea = null;
    private ?int $numberOfParkingLots = null;
    private ?string $barrierFreeAccessName = null;
    private ?string $description = null;
    private ?string $placeDescription = null;
    private ExposeContact $exposeContact;
    private ExposeAddress $exposeAddress;
    private bool $propertyExposeContactIsCompany = false;
    private ?ExposeContact $propertyExposeContact = null;
    private string $disclaimer;
    private ?Image $mainImage = null;
    private ?array $images = null;

    private string $primaryColor;
    private string $secondaryColor;

    private array $exposePropertySpaceDataTable = [];

    private array $exposeConditionDataTable = [];

    public static function createFromBuildingUnitAndAccountConfigurationAndAccountUserAndAccountLegalTextsConfiguration(
        BuildingUnit $buildingUnit,
        AccountUser $accountUser,
        AccountConfiguration $accountConfiguration,
        AccountLegalTextsConfiguration $accountLegalTextsConfiguration
    ): self {
        $buildingUnitExpose = new self();

        $currencyName = $buildingUnit->getPropertyMarketingInformation()?->getPropertyPricingInformation()?->getCurrency()?->getName();

        if ($currencyName === null) {
            $currencyName = Currency::EURO->getName();
        }

        if ($buildingUnit->getAddress() !== null) {
            $buildingUnitExpose->exposeAddress = ExposeAddress::createFromAddress(address: $buildingUnit->getAddress());
        }

        if (empty($buildingUnit->getInFloors()) === false) {
            $buildingUnitExpose->inFloors = join(separator: ',', array: $buildingUnit->getInFloors());
        }

        $buildingUnitExpose->locationCategoryName = $buildingUnit->getBuilding()->getLocationCategory()?->getName();

        /** @var IndustryClassification $possibleUsageIndustryClassification */
        $concatenatedPossibleUsageIndustryClassificationNames = '';

        foreach ($buildingUnit->getPropertyMarketingInformation()?->getPossibleUsageIndustryClassifications() as $possibleUsageIndustryClassification) {
            $concatenatedPossibleUsageIndustryClassificationNames .= '/ ' . $possibleUsageIndustryClassification->getName() . ' ';
        }

        $buildingUnitExpose->concatenatedPossibleUsageIndustryClassificationNames = substr(
            string: $concatenatedPossibleUsageIndustryClassificationNames,
            offset: 1,
            length: strlen($concatenatedPossibleUsageIndustryClassificationNames) - 1
        );

        if ($buildingUnit->getPropertyMarketingInformation()?->getPropertyOfferType() !== null) {
            $buildingUnitExpose->propertyOfferTypeName = $buildingUnit->getPropertyMarketingInformation()->getPropertyOfferType()->getName();
        }

        if ($buildingUnit->getPropertyMarketingInformation()->getPropertyPricingInformation()->getColdRent() !== null) {
            $buildingUnitExpose->coldRentWithCurrency =
                self::formatNumber($buildingUnit->getPropertyMarketingInformation()->getPropertyPricingInformation()->getColdRent())
                . $currencyName;
        }

        if ($buildingUnit->getPropertyMarketingInformation()->getPropertyPricingInformation()->getDeposit() !== null) {
            $buildingUnitExpose->depositWithCurrency =
                self::formatNumber($buildingUnit->getPropertyMarketingInformation()->getPropertyPricingInformation()->getDeposit())
                . $currencyName;
        }

        $buildingUnitExpose->commissionable = $buildingUnit->getPropertyMarketingInformation()->getPropertyPricingInformation()->isCommissionable();

        $buildingUnitExpose->shopWindowFrontWidth = self::formatNumber($buildingUnit->getShopWindowFrontWidth());
        $buildingUnitExpose->shopWidth = self::formatNumber($buildingUnit->getShopWidth());
        $buildingUnitExpose->showroomAvailable = $buildingUnit->isShowroomAvailable();
        $buildingUnitExpose->groundLevelSalesArea = $buildingUnit->isGroundLevelSalesArea();
        $buildingUnitExpose->numberOfParkingLots = $buildingUnit->getNumberOfParkingLots();
        $buildingUnitExpose->barrierFreeAccessName = $buildingUnit->getBarrierFreeAccess()?->getName();
        $buildingUnitExpose->description = $buildingUnit->getDescription();
        $buildingUnitExpose->placeDescription = $buildingUnit->getPlaceDescription();
        $buildingUnitExpose->disclaimer = $accountLegalTextsConfiguration->getPdfExposeDisclaimer();
        $buildingUnitExpose->mainImage = $buildingUnit->getMainImage();
        $buildingUnitExpose->images = $buildingUnit->getImages()->toArray();
        $buildingUnitExpose->exposeContact = ExposeContact::createFromAccountUser(accountUser: $accountUser);
        $buildingUnitExpose->primaryColor = $accountConfiguration->getPrimaryColor();
        $buildingUnitExpose->secondaryColor = $accountConfiguration->getSecondaryColor();

        $propertyContactPerson = self::determinePropertyContactPerson(buildingUnit: $buildingUnit);

        $buildingUnitExpose->propertyExposeContactIsCompany = $propertyContactPerson?->getPerson()?->getPersonType() === PersonType::COMPANY;
        $buildingUnitExpose->propertyExposeContact = $propertyContactPerson !== null
            ? ExposeContact::createFromPropertyContactPerson(propertyContactPerson: $propertyContactPerson)
            : null;

        $buildingUnitExpose->exposePropertySpaceDataTable = self::createExposePropertySpaceDataTable(buildingUnit: $buildingUnit);

        $buildingUnitExpose->exposeConditionDataTable = self::createExposeConditionDataTable(buildingUnit: $buildingUnit);

        return $buildingUnitExpose;
    }

    private static function createExposePropertySpaceDataTable(BuildingUnit $buildingUnit): array
    {
        $exposePropertySpaceDataTable = [];
        $propertySpacesData = $buildingUnit->getPropertySpacesData();

        $exposePropertySpacesDataRow = new ExposeTableRow();

        if ($buildingUnit->getAreaSize() !== null) {
            $exposePropertySpacesDataRow = self::createAndAddExposeTableValue(
                label: 'Gesamtfläche',
                value: self::formatNumber($buildingUnit->getAreaSize()),
                exposeTableRow: $exposePropertySpacesDataRow,
                exposeTable: $exposePropertySpaceDataTable,
                unit: 'm',
                unitIsSquared: true
            );
        }

        if ($propertySpacesData->getSubsidiarySpace() !== null) {
            $exposePropertySpacesDataRow = self::createAndAddExposeTableValue(
                label: 'Nebenfläche',
                value: self::formatNumber($propertySpacesData->getSubsidiarySpace()),
                exposeTableRow: $exposePropertySpacesDataRow,
                exposeTable: $exposePropertySpaceDataTable,
                unit: 'm',
                unitIsSquared: true
            );
        }

        if ($propertySpacesData->getStorageSpace() !== null) {
            $exposePropertySpacesDataRow = self::createAndAddExposeTableValue(
                label: 'Lagerfläche',
                value: self::formatNumber($propertySpacesData->getStorageSpace()),
                exposeTableRow: $exposePropertySpacesDataRow,
                exposeTable: $exposePropertySpaceDataTable,
                unit: 'm',
                unitIsSquared: true
            );
        }

        if ($propertySpacesData->getRetailSpace() !== null) {
            $exposePropertySpacesDataRow = self::createAndAddExposeTableValue(
                label: 'Verkaufsfläche',
                value: self::formatNumber($propertySpacesData->getRetailSpace()),
                exposeTableRow: $exposePropertySpacesDataRow,
                exposeTable: $exposePropertySpaceDataTable,
                unit: 'm',
                unitIsSquared: true
            );
        }

        if ($propertySpacesData->getGastronomySpace() !== null) {
            $exposePropertySpacesDataRow = self::createAndAddExposeTableValue(
                label: 'Gastrofläche',
                value: self::formatNumber($propertySpacesData->getGastronomySpace()),
                exposeTableRow: $exposePropertySpacesDataRow,
                exposeTable: $exposePropertySpaceDataTable,
                unit: 'm',
                unitIsSquared: true
            );
        }

        if ($buildingUnit->getShopWindowFrontWidth() !== null) {
            $exposePropertySpacesDataRow = self::createAndAddExposeTableValue(
                label: 'Schaufenster',
                value: self::formatNumber($buildingUnit->getShopWindowFrontWidth()),
                exposeTableRow: $exposePropertySpacesDataRow,
                exposeTable: $exposePropertySpaceDataTable,
                unit: 'm'
            );
        }

        if ($buildingUnit->getShopWidth() !== null) {
            $exposePropertySpacesDataRow = self::createAndAddExposeTableValue(
                label: 'Ladenbreite',
                value: self::formatNumber($buildingUnit->getShopWidth()),
                exposeTableRow: $exposePropertySpacesDataRow,
                exposeTable: $exposePropertySpaceDataTable,
                unit: 'm',
            );
        }

        if ($buildingUnit->isShowroomAvailable() === true) {
            $exposePropertySpacesDataRow = self::createAndAddExposeTableValue(
                label: 'Showroom vorhanden',
                value: 'ja',
                exposeTableRow: $exposePropertySpacesDataRow,
                exposeTable: $exposePropertySpaceDataTable
            );
        }

        if ($buildingUnit->isGroundLevelSalesArea() === true) {
            $exposePropertySpacesDataRow = self::createAndAddExposeTableValue(
                label: 'ebenerdige Verkaufsfläche',
                value: 'ja',
                exposeTableRow: $exposePropertySpacesDataRow,
                exposeTable: $exposePropertySpaceDataTable
            );
        }

        if ($buildingUnit->getBarrierFreeAccess() !== null) {
            $exposePropertySpacesDataRow = self::createAndAddExposeTableValue(
                label: 'barrierefreier Zugang',
                value: $buildingUnit->getBarrierFreeAccess()->getName(),
                exposeTableRow: $exposePropertySpacesDataRow,
                exposeTable: $exposePropertySpaceDataTable
            );
        }

        if (
            count($exposePropertySpacesDataRow->getExposeValues()) >= 1
            && count($exposePropertySpacesDataRow->getExposeValues()) !== 3
        ) {
            while (count($exposePropertySpacesDataRow->getExposeValues()) !== 3) {
                $exposePropertySpacesDataRow->addExposePropertySpacesValue(exposePropertySpacesValue: null);
            }
        }

        if (count($exposePropertySpacesDataRow->getExposeValues()) > 0) {
            $exposePropertySpaceDataTable[] = $exposePropertySpacesDataRow;
        }

        return $exposePropertySpaceDataTable;
    }

    private static function createExposeConditionDataTable(BuildingUnit $buildingUnit): array
    {
        $exposeConditionDataTable = [];
        $exposeConditionDataRow = new ExposeTableRow();

        if ($buildingUnit->getPropertyMarketingInformation()?->getPropertyOfferType() !== null) {
            $exposeConditionDataRow = self::createAndAddExposeTableValue(
                label: 'Angebotsart',
                value: $buildingUnit->getPropertyMarketingInformation()->getPropertyOfferType()->getName(),
                exposeTableRow: $exposeConditionDataRow,
                exposeTable: $exposeConditionDataTable
            );

            switch ($buildingUnit->getPropertyMarketingInformation()->getPropertyOfferType()) {
                case PropertyOfferType::LEASE:
                    $exposeConditionDataRow = self::createAndAddExposeTableValue(
                        label: 'Pacht',
                        value: 'auf Anfrage',
                        exposeTableRow: $exposeConditionDataRow,
                        exposeTable: $exposeConditionDataTable
                    );
                    break;
                case PropertyOfferType::PURCHASE:
                    $exposeConditionDataRow = self::createAndAddExposeTableValue(
                        label: 'Kaufpreis',
                        value: 'auf Anfrage',
                        exposeTableRow: $exposeConditionDataRow,
                        exposeTable: $exposeConditionDataTable
                    );
                    break;
                case PropertyOfferType::RENT:
                    $exposeConditionDataRow = self::createAndAddExposeTableValue(
                        label: 'Kaltmiete',
                        value: 'auf Anfrage',
                        exposeTableRow: $exposeConditionDataRow,
                        exposeTable: $exposeConditionDataTable
                    );
                    break;
            }
        }

        if ($buildingUnit?->getPropertyMarketingInformation()?->getPropertyPricingInformation()?->isCommissionable() === true) {
            $exposeConditionDataRow = self::createAndAddExposeTableValue(
                label: 'provisionspflichtig',
                value: 'ja',
                exposeTableRow: $exposeConditionDataRow,
                exposeTable: $exposeConditionDataTable
            );
        }

        if (
            count($exposeConditionDataRow->getExposeValues()) >= 1
            && count($exposeConditionDataRow->getExposeValues()) !== 3
        ) {
            while (count($exposeConditionDataRow->getExposeValues()) !== 3) {
                $exposeConditionDataRow->addExposePropertySpacesValue(exposePropertySpacesValue: null);
            }
        }

        if (count($exposeConditionDataRow->getExposeValues()) > 0) {
            $exposeConditionDataTable[] = $exposeConditionDataRow;
        }

        return $exposeConditionDataTable;
    }

    private static function formatNumber(?float $value): ?string
    {
        if ($value === null) {
            return null;
        }

        return number_format(num: $value, decimals: 2, decimal_separator: ',', thousands_separator: '');
    }

    private static function createAndAddExposeTableValue(
        string $label,
        string $value,
        ExposeTableRow $exposeTableRow,
        array &$exposeTable,
        ?string $unit = null,
        bool $unitIsSquared = false
    ): ExposeTableRow {
        $value = $unit !== null ? $value . ' ' . $unit : $value;

        $exposePropertySpacesValue = ExposeValue::createFromLabelValueAndIsSquareMeterValue(
            label: $label,
            value: $value,
            isSquareMeterValue: $unitIsSquared
        );

        $exposeTableRow->addExposePropertySpacesValue($exposePropertySpacesValue);

        if (count($exposeTableRow->getExposeValues()) === 3) {
            $exposeTable[] = $exposeTableRow;
            $exposeTableRow = new ExposeTableRow();
        }

        return $exposeTableRow;
    }

    private static function determinePropertyContactPerson(BuildingUnit $buildingUnit): ?PropertyContactPerson
    {
        if ($buildingUnit->getPropertyContactPersons()->isEmpty() === true) {
            return null;
        }

        $propertyContactPersons = $buildingUnit->getPropertyContactPersons()->filter(
            p: function (PropertyContactPerson $propertyContactPerson): bool {
                return $propertyContactPerson->getPropertyContactResponsibility() === PropertyContactResponsibility::MARKETING;
            }
        );

        if ($propertyContactPersons->isEmpty() === false) {
            return $propertyContactPersons->first();
        }

        $propertyContactPersons = $buildingUnit->getPropertyContactPersons()->filter(
            p: function (PropertyContactPerson $propertyContactPerson): bool {
                return $propertyContactPerson->getPropertyContactResponsibility() === PropertyContactResponsibility::PROPERTY;
            }
        );

        if ($propertyContactPersons->isEmpty() === false) {
            return $propertyContactPersons->first();
        }

        return null;
    }

    public function getExposeAddress(): ExposeAddress
    {
        return $this->exposeAddress;
    }

    public function getInFloors(): ?string
    {
        return $this->inFloors;
    }

    public function getConcatenatedPossibleUsageIndustryClassificationNames(): ?string
    {
        return $this->concatenatedPossibleUsageIndustryClassificationNames;
    }

    public function getLocationCategoryName(): ?string
    {
        return $this->locationCategoryName;
    }

    public function getPropertyOfferTypeName(): ?string
    {
        return $this->propertyOfferTypeName;
    }

    public function getColdRentWithCurrency(): ?string
    {
        return $this->coldRentWithCurrency;
    }

    public function getDepositWithCurrency(): ?string
    {
        return $this->depositWithCurrency;
    }

    public function getCommissionable(): ?bool
    {
        return $this->commissionable;
    }

    public function getShopWindowFrontWidth(): ?string
    {
        return $this->shopWindowFrontWidth;
    }

    public function getShopWidth(): ?string
    {
        return $this->shopWidth;
    }

    public function getShowroomAvailable(): ?bool
    {
        return $this->showroomAvailable;
    }

    public function getGroundLevelSalesArea(): ?bool
    {
        return $this->groundLevelSalesArea;
    }

    public function getNumberOfParkingLots(): ?int
    {
        return $this->numberOfParkingLots;
    }

    public function getBarrierFreeAccessName(): ?string
    {
        return $this->barrierFreeAccessName;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getExposeContact(): ExposeContact
    {
        return $this->exposeContact;
    }

    public function getExposePropertySpaceDataTable(): array
    {
        return $this->exposePropertySpaceDataTable;
    }

    public function getPlaceDescription(): ?string
    {
        return $this->placeDescription;
    }

    public function getDisclaimer(): string
    {
        return $this->disclaimer;
    }

    public function getMainImage(): ?Image
    {
        return $this->mainImage;
    }

    public function getImages(): ?array
    {
        return $this->images;
    }

    public function getPropertyExposeContact(): ?ExposeContact
    {
        return $this->propertyExposeContact;
    }

    public function isPropertyExposeContactIsCompany(): bool
    {
        return $this->propertyExposeContactIsCompany;
    }

    public function getPrimaryColor(): string
    {
        return $this->primaryColor;
    }

    public function getSecondaryColor(): string
    {
        return $this->secondaryColor;
    }

    public function getExposeConditionDataTable(): array
    {
        return $this->exposeConditionDataTable;
    }
}
