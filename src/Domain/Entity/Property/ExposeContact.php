<?php

declare(strict_types=1);

namespace App\Domain\Entity\Property;

use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\Person\PersonType;

class ExposeContact
{
    private ?string $name = null;
    private string $accountName;
    private ?string $placeName = null;
    private ?string $streetName = null;
    private ?string $houseNumber = null;
    private ?string $postalCode = null;
    private ?string $email = null;
    private ?string $phoneNumber = null;

    public static function createFromAccountUser(AccountUser $accountUser): self
    {
        $exposeContact = new self();
        $exposeContact
            ->setName($accountUser->getFullName())
            ->setEmail($accountUser->getEmail())
            ->setAccountName($accountUser->getAccount()->getName());

        if ($accountUser->getAccount()->getPlaceName() !== null) {
            $exposeContact
                ->setStreetName($accountUser->getAccount()->getStreetName())
                ->setHouseNumber($accountUser->getAccount()->getHouseNumber())
                ->setPostalCode($accountUser->getAccount()->getPostalCode())
                ->setPlaceName($accountUser->getAccount()->getPlaceName());
        }

        $exposeContact->phoneNumber = $accountUser->getPhoneNumber();

        return $exposeContact;
    }

    public static function createFromPropertyContactPerson(PropertyContactPerson $propertyContactPerson): self
    {
        $exposeContact = new self();

        if (
            in_array(needle: VisibleContactInformation::COMPANY_NAME, haystack: $propertyContactPerson->getVisibleContactInformations())
            && $propertyContactPerson->getPerson()->getPersonType() === PersonType::COMPANY
        ) {
            $exposeContact->name = $propertyContactPerson->getPerson()->getName();
        }

        if (in_array(needle: VisibleContactInformation::NAME, haystack: $propertyContactPerson->getVisibleContactInformations())) {
            $exposeContact->name = $propertyContactPerson->getPerson()->getFirstName() . ' ' . $propertyContactPerson->getPerson()->getName();
        }

        if (in_array(needle: VisibleContactInformation::ADDRESS, haystack: $propertyContactPerson->getVisibleContactInformations())) {
            $exposeContact
                ->setStreetName($propertyContactPerson->getPerson()->getStreetName())
                ->setHouseNumber($propertyContactPerson->getPerson()->getHouseNumber())
                ->setPostalCode($propertyContactPerson->getPerson()->getPostalCode())
                ->setPlaceName($propertyContactPerson->getPerson()->getPlaceName());
        }

        if (in_array(needle: VisibleContactInformation::EMAIL, haystack: $propertyContactPerson->getVisibleContactInformations())) {
            $exposeContact->email = $propertyContactPerson->getPerson()->getEmail();
        }

        if (in_array(needle: VisibleContactInformation::PHONE_NUMBER, haystack: $propertyContactPerson->getVisibleContactInformations())) {
            $exposeContact->phoneNumber = $propertyContactPerson->getPerson()->getPhoneNumber();
        }

        return $exposeContact;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAccountName(): string
    {
        return $this->accountName;
    }

    public function setAccountName(string $accountName): self
    {
        $this->accountName = $accountName;
        return $this;
    }

    public function getPlaceName(): ?string
    {
        return $this->placeName;
    }

    public function setPlaceName(?string $placeName): self
    {
        $this->placeName = $placeName;

        return $this;
    }

    public function getStreetName(): ?string
    {
        return $this->streetName;
    }

    public function setStreetName(?string $streetName): self
    {
        $this->streetName = $streetName;

        return $this;
    }

    public function getHouseNumber(): ?string
    {
        return $this->houseNumber;
    }

    public function setHouseNumber(?string $houseNumber): self
    {
        $this->houseNumber = $houseNumber;

        return $this;
    }

    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }

    public function setPostalCode(?string $postalCode): self
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }
}
