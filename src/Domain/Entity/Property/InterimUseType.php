<?php

declare(strict_types=1);

namespace App\Domain\Entity\Property;

enum InterimUseType: int
{
    case POP_UP_STORE = 0;
    case SHOW_WORKSHOP = 1;
    case GASTRONOMIC_USE = 2;
    case CULTURAL_PROJECTS = 3;
    case ANY_USE = 4;

    public function getName(): string
    {
        return match ($this) {
            self::POP_UP_STORE => 'als PopUp-Store geeignet',
            self::SHOW_WORKSHOP => 'als Schauwerkstatt geeignet',
            self::GASTRONOMIC_USE => 'für eine gastronomische Nutzung geeignet',
            self::CULTURAL_PROJECTS => 'für kulturelle Projekte geeignet',
            self::ANY_USE => 'jegliche Nutzung ist möglich'
        };
    }
}
