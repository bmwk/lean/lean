<?php

declare(strict_types=1);

namespace App\Domain\Entity\Property;

enum BuildingCondition: int
{
    case LIKE_NEW = 0;
    case SANITIZED = 1;
    case PARTIALLY_RENOVATED = 2;
    case MODERNIZED = 3;
    case UNRENOVATED = 4;
    case RENOVATION_NEEDED = 5;
    case DEMOLITION_OBJECT = 6;
    case DILAPIDATED = 7;
    case GROOMED = 8;
    case GUTTED = 9;
    case SHELL = 10;
    case NEW_BUILDING = 11;

    public function getName(): string
    {
        return match ($this) {
            self::LIKE_NEW => 'neuwertig',
            self::SANITIZED => 'saniert',
            self::PARTIALLY_RENOVATED => 'teilweise saniert',
            self::MODERNIZED => 'modernisiert',
            self::UNRENOVATED => 'unrenoviert',
            self::RENOVATION_NEEDED => 'sanierungsbedürftig',
            self::DEMOLITION_OBJECT => 'Abrissobjekt',
            self::DILAPIDATED => 'baufällig',
            self::GROOMED => 'gepflegt',
            self::GUTTED => 'entkernt',
            self::SHELL => 'Rohbau',
            self::NEW_BUILDING => 'Neubau'
        };
    }
}
