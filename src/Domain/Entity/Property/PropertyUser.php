<?php

declare(strict_types=1);

namespace App\Domain\Entity\Property;

use App\Domain\Entity\AccountTrait;
use App\Domain\Entity\Classification\IndustryClassification;
use App\Domain\Entity\Classification\NaceClassification;
use App\Domain\Entity\CreatedAtTrait;
use App\Domain\Entity\CreatedByAccountUserTrait;
use App\Domain\Entity\File;
use App\Domain\Entity\Image;
use App\Domain\Entity\Person\Occurrence;
use App\Domain\Entity\Person\Person;
use App\Domain\Entity\UpdatedAtTrait;
use App\Repository\Property\PropertyUserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\Table;

#[Entity(repositoryClass: PropertyUserRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[HasLifecycleCallbacks]
class PropertyUser
{
    use AccountTrait;
    use CreatedByAccountUserTrait;
    use CreatedAtTrait;
    use UpdatedAtTrait;

    #[Id]
    #[GeneratedValue]
    #[Column(type: Types::INTEGER, nullable: false, options: ['unsigned' => true])]
    private ?int $id = null;

    #[ManyToOne(inversedBy: 'propertyUsers')]
    #[JoinColumn(nullable: false)]
    private Person $person;

    #[Column(type: Types::SMALLINT, nullable: false, enumType: PropertyUserStatus::class, options: ['unsigned' => true])]
    private PropertyUserStatus $propertyUserStatus;

    #[Column(type: Types::SMALLINT, nullable: false, enumType: PropertyRelationship::class, options: ['unsigned' => true])]
    private PropertyRelationship $propertyRelationship;

    #[Column(type: Types::BOOLEAN, nullable: true)]
    private ?bool $isSubtenant = null;

    #[ManyToOne]
    #[JoinColumn(nullable: true)]
    private ?PropertyOwner $propertyOwner = null;

    #[Column(type: 'datetime', nullable: true)]
    private ?\DateTime $endOfRental = null;

    #[OneToOne]
    #[JoinColumn(nullable: true)]
    private ?File $tenantContractFile = null;

    #[ManyToOne]
    #[JoinColumn(nullable: true)]
    private ?IndustryClassification $industryClassification = null;

    #[ManyToOne]
    #[JoinColumn(nullable: true)]
    private ?NaceClassification $naceClassification = null;

    #[OneToOne]
    #[JoinColumn(nullable: true)]
    private ?Image $companyLogoImage = null;

    #[Column(type: Types::STRING, length: 190, nullable: true)]
    private ?string $section = null;

    #[Column(type: Types::STRING, length: 190, nullable: true)]
    private ?string $industryName = null;

    #[Column(type: Types::BOOLEAN, nullable: false)]
    private bool $branch;

    #[Column(type: Types::SMALLINT, nullable: true, enumType: BranchType::class, options: ['unsigned' => true])]
    private ?BranchType $branchType = null;

    #[Column(type: Types::STRING, length: 250, nullable: true)]
    private ?string $mainAssortment = null;

    #[Column(type: Types::STRING, length: 250, nullable: true)]
    private ?string $sideAssortment = null;

    #[Column(type: Types::TEXT, nullable: true)]
    private ?string $openingHours = null;

    #[Column(type: Types::BOOLEAN, nullable: false)]
    private bool $familybusiness;

    #[Column(type: Types::STRING, length: 250, nullable: true)]
    private ?string $deliveryService = null;

    #[Column(type: Types::STRING, length: 250, nullable: true)]
    private ?string $rating = null;

    #[Column(type: Types::STRING, length: 255, nullable: true)]
    private ?string $website = null;

    #[Column(type: Types::STRING, length: 190, nullable: true)]
    private ?string $googleBusiness = null;

    #[Column(type: Types::STRING, length: 190, nullable: true)]
    private ?string $facebook = null;

    #[Column(type: Types::STRING, length: 190, nullable: true)]
    private ?string $instagram = null;

    #[Column(type: Types::BOOLEAN, nullable: false)]
    private bool $receiveNewsletter;

    /**
     * @var Collection<int, BuildingUnit>|BuildingUnit[]
     */
    #[ManyToMany(targetEntity: BuildingUnit::class, mappedBy: 'propertyUsers')]
    private Collection|array $buildingUnits;

    /**
     * @var Collection<int, Occurrence>|Occurrence[]
     */
    #[ManyToMany(targetEntity: Occurrence::class, mappedBy: 'propertyUsers')]
    private Collection|array $occurrences;

    public function __construct()
    {
        $this->buildingUnits = new ArrayCollection();
        $this->occurrences = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getPerson(): Person
    {
        return $this->person;
    }

    public function setPerson(Person $person): self
    {
        $this->person = $person;

        return $this;
    }

    public function getPropertyUserStatus(): PropertyUserStatus
    {
        return $this->propertyUserStatus;
    }

    public function setPropertyUserStatus(PropertyUserStatus $propertyUserStatus): self
    {
        $this->propertyUserStatus = $propertyUserStatus;

        return $this;
    }

    public function getPropertyRelationship(): PropertyRelationship
    {
        return $this->propertyRelationship;
    }

    public function setPropertyRelationship(PropertyRelationship $propertyRelationship): self
    {
        $this->propertyRelationship = $propertyRelationship;

        return $this;
    }

    public function isIsSubtenant(): ?bool
    {
        return $this->isSubtenant;
    }

    public function setIsSubtenant(?bool $isSubtenant): self
    {
        $this->isSubtenant = $isSubtenant;

        return $this;
    }

    public function getPropertyOwner(): ?PropertyOwner
    {
        return $this->propertyOwner;
    }

    public function setPropertyOwner(?PropertyOwner $propertyOwner): self
    {
        $this->propertyOwner = $propertyOwner;

        return $this;
    }

    public function getEndOfRental(): ?\DateTime
    {
        return $this->endOfRental;
    }

    public function setEndOfRental(?\DateTime $endOfRental): self
    {
        $this->endOfRental = $endOfRental;

        return $this;
    }

    public function getTenantContractFile(): ?File
    {
        return $this->tenantContractFile;
    }

    public function setTenantContractFile(?File $tenantContractFile): self
    {
        $this->tenantContractFile = $tenantContractFile;

        return $this;
    }

    public function getIndustryClassification(): ?IndustryClassification
    {
        return $this->industryClassification;
    }

    public function setIndustryClassification(?IndustryClassification $industryClassification): self
    {
        $this->industryClassification = $industryClassification;

        return $this;
    }

    public function getNaceClassification(): ?NaceClassification
    {
        return $this->naceClassification;
    }

    public function setNaceClassification(?NaceClassification $naceClassification): self
    {
        $this->naceClassification = $naceClassification;

        return $this;
    }

    public function getCompanyLogoImage(): ?Image
    {
        return $this->companyLogoImage;
    }

    public function setCompanyLogoImage(?Image $companyLogoImage): self
    {
        $this->companyLogoImage = $companyLogoImage;

        return $this;
    }

    public function getSection(): ?string
    {
        return $this->section;
    }

    public function setSection(?string $section): self
    {
        $this->section = $section;

        return $this;
    }

    public function getIndustryName(): ?string
    {
        return $this->industryName;
    }

    public function setIndustryName(?string $industryName): self
    {
        $this->industryName = $industryName;

        return $this;
    }

    public function isBranch(): bool
    {
        return $this->branch;
    }

    public function setBranch(bool $branch): self
    {
        $this->branch = $branch;

        return $this;
    }

    public function getBranchType(): ?BranchType
    {
        return $this->branchType;
    }

    public function setBranchType(?BranchType $branchType): self
    {
        $this->branchType = $branchType;

        return $this;
    }

    public function getMainAssortment(): ?string
    {
        return $this->mainAssortment;
    }

    public function setMainAssortment(?string $mainAssortment): self
    {
        $this->mainAssortment = $mainAssortment;

        return $this;
    }

    public function getSideAssortment(): ?string
    {
        return $this->sideAssortment;
    }

    public function setSideAssortment(?string $sideAssortment): self
    {
        $this->sideAssortment = $sideAssortment;

        return $this;
    }

    public function getOpeningHours(): ?string
    {
        return $this->openingHours;
    }

    public function setOpeningHours(?string $openingHours): self
    {
        $this->openingHours = $openingHours;

        return $this;
    }

    public function isFamilybusiness(): bool
    {
        return $this->familybusiness;
    }

    public function setFamilybusiness(bool $familybusiness): self
    {
        $this->familybusiness = $familybusiness;

        return $this;
    }

    public function getDeliveryService(): ?string
    {
        return $this->deliveryService;
    }

    public function setDeliveryService(?string $deliveryService): self
    {
        $this->deliveryService = $deliveryService;

        return $this;
    }

    public function getRating(): ?string
    {
        return $this->rating;
    }

    public function setRating(?string $rating): self
    {
        $this->rating = $rating;

        return $this;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    public function setWebsite(?string $website): self
    {
        $this->website = $website;

        return $this;
    }

    public function getGoogleBusiness(): ?string
    {
        return $this->googleBusiness;
    }

    public function setGoogleBusiness(?string $googleBusiness): self
    {
        $this->googleBusiness = $googleBusiness;

        return $this;
    }

    public function getFacebook(): ?string
    {
        return $this->facebook;
    }

    public function setFacebook(?string $facebook): self
    {
        $this->facebook = $facebook;

        return $this;
    }

    public function getInstagram(): ?string
    {
        return $this->instagram;
    }

    public function setInstagram(?string $instagram): self
    {
        $this->instagram = $instagram;

        return $this;
    }

    public function isReceiveNewsletter(): bool
    {
        return $this->receiveNewsletter;
    }

    public function setReceiveNewsletter(bool $receiveNewsletter): self
    {
        $this->receiveNewsletter = $receiveNewsletter;

        return $this;
    }

    /**
     * @return Collection<int, BuildingUnit>|BuildingUnit[]
     */
    public function getBuildingUnits(): Collection|array
    {
        return $this->buildingUnits;
    }

    /**
     * @param Collection<int, BuildingUnit>|BuildingUnit[] $buildingUnits
     */
    public function setBuildingUnits(Collection|array $buildingUnits): self
    {
        $this->buildingUnits = $buildingUnits;

        return $this;
    }

    /**
     * @return Collection<int, Occurrence>|Occurrence[]
     */
    public function getOccurrences(): Collection|array
    {
        return $this->occurrences;
    }

    /**
     * @param Collection<int, Occurrence>|Occurrence[] $occurrences
     */
    public function setOccurrences(Collection|array $occurrences): self
    {
        $this->occurrences = $occurrences;

        return $this;
    }
}
