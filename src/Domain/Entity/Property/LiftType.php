<?php

declare(strict_types=1);

namespace App\Domain\Entity\Property;

enum LiftType: int
{
    case CARGO_LIFT = 0;
    case PASSENGER_LIFT = 1;
    case ESCALATOR = 2;

    public function getName(): string
    {
        return match ($this) {
            self::CARGO_LIFT => 'Lastenaufzug',
            self::PASSENGER_LIFT => 'Personenaufzug',
            self::ESCALATOR => 'Rolltreppe'
        };
    }
}
