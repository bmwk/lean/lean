<?php

declare(strict_types=1);

namespace App\Domain\Entity\Property;

use App\Domain\Entity\CreatedAtTrait;
use App\Domain\Entity\UpdatedAtTrait;
use App\Repository\Property\EnergyCertificateRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;

#[Entity(repositoryClass: EnergyCertificateRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[HasLifecycleCallbacks]
class EnergyCertificate
{
    use CreatedAtTrait;
    use UpdatedAtTrait;

    #[Id]
    #[GeneratedValue]
    #[Column(type: Types::INTEGER, nullable: false, options: ['unsigned' => true])]
    private ?int $id = null;

    #[Column(type: Types::SMALLINT, nullable: false, enumType: EnergyCertificateType::class, options: ['unsigned' => true])]
    private EnergyCertificateType $energyCertificateType;

    #[Column(type: Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTime $issueDate = null;

    #[Column(type: Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTime $expirationDate = null;

    #[Column(type: Types::SMALLINT, nullable: true, options: ['unsigned' => true])]
    private ?int $buildingConstructionYear = null;

    #[Column(type: Types::SMALLINT, nullable: true, enumType: EnergyEfficiencyClass::class, options: ['unsigned' => true])]
    private ?EnergyEfficiencyClass $energyEfficiencyClass = null;

    /**
     * @var EnergyCertificateHeatingType[]
     */
    #[Column(type: Types::JSON, nullable: true, enumType: EnergyCertificateHeatingType::class)]
    private ?array $energyCertificateHeatingTypes = null;

    #[Column(type: Types::BOOLEAN, nullable: false)]
    private bool $withHotWater;

    #[Column(type: Types::FLOAT, nullable: true, options: ['unsigned' => true])]
    private ?float $energyDemand = null;

    #[Column(type: Types::FLOAT, nullable: true, options: ['unsigned' => true])]
    private ?float $electricityDemand = null;

    #[Column(type: Types::FLOAT, nullable: true, options: ['unsigned' => true])]
    private ?float $heatingEnergyDemand = null;

    #[Column(type: Types::FLOAT, nullable: true, options: ['unsigned' => true])]
    private ?float $energyConsumption = null;

    #[Column(type: Types::FLOAT, nullable: true, options: ['unsigned' => true])]
    private ?float $electricityConsumption = null;

    #[Column(type: Types::FLOAT, nullable: true, options: ['unsigned' => true])]
    private ?float $heatingEnergyConsumption = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getEnergyCertificateType(): EnergyCertificateType
    {
        return $this->energyCertificateType;
    }

    public function setEnergyCertificateType(EnergyCertificateType $energyCertificateType): self
    {
        $this->energyCertificateType = $energyCertificateType;

        return $this;
    }

    public function getIssueDate(): ?\DateTime
    {
        return $this->issueDate;
    }

    public function setIssueDate(?\DateTime $issueDate): self
    {
        $this->issueDate = $issueDate;

        return $this;
    }

    public function getExpirationDate(): ?\DateTime
    {
        return $this->expirationDate;
    }

    public function setExpirationDate(?\DateTime $expirationDate): self
    {
        $this->expirationDate = $expirationDate;

        return $this;
    }

    public function getBuildingConstructionYear(): ?int
    {
        return $this->buildingConstructionYear;
    }

    public function setBuildingConstructionYear(?int $buildingConstructionYear): self
    {
        $this->buildingConstructionYear = $buildingConstructionYear;

        return $this;
    }

    public function getEnergyEfficiencyClass(): ?EnergyEfficiencyClass
    {
        return $this->energyEfficiencyClass;
    }

    public function setEnergyEfficiencyClass(?EnergyEfficiencyClass $energyEfficiencyClass): self
    {
        $this->energyEfficiencyClass = $energyEfficiencyClass;

        return $this;
    }

    public function getEnergyCertificateHeatingTypes(): ?array
    {
        return $this->energyCertificateHeatingTypes;
    }

    public function setEnergyCertificateHeatingTypes(?array $energyCertificateHeatingTypes): self
    {
        $this->energyCertificateHeatingTypes = $energyCertificateHeatingTypes;

        return $this;
    }

    public function isWithHotWater(): bool
    {
        return $this->withHotWater;
    }

    public function setWithHotWater(bool $withHotWater): self
    {
        $this->withHotWater = $withHotWater;

        return $this;
    }

    public function getEnergyDemand(): ?float
    {
        return $this->energyDemand;
    }

    public function setEnergyDemand(?float $energyDemand): self
    {
        $this->energyDemand = $energyDemand;

        return $this;
    }

    public function getElectricityDemand(): ?float
    {
        return $this->electricityDemand;
    }

    public function setElectricityDemand(?float $electricityDemand): self
    {
        $this->electricityDemand = $electricityDemand;

        return $this;
    }

    public function getHeatingEnergyDemand(): ?float
    {
        return $this->heatingEnergyDemand;
    }

    public function setHeatingEnergyDemand(?float $heatingEnergyDemand): self
    {
        $this->heatingEnergyDemand = $heatingEnergyDemand;

        return $this;
    }

    public function getEnergyConsumption(): ?float
    {
        return $this->energyConsumption;
    }

    public function setEnergyConsumption(?float $energyConsumption): self
    {
        $this->energyConsumption = $energyConsumption;

        return $this;
    }

    public function getElectricityConsumption(): ?float
    {
        return $this->electricityConsumption;
    }

    public function setElectricityConsumption(?float $electricityConsumption): self
    {
        $this->electricityConsumption = $electricityConsumption;

        return $this;
    }

    public function getHeatingEnergyConsumption(): ?float
    {
        return $this->heatingEnergyConsumption;
    }

    public function setHeatingEnergyConsumption(?float $heatingEnergyConsumption): self
    {
        $this->heatingEnergyConsumption = $heatingEnergyConsumption;

        return $this;
    }
}
