<?php

declare(strict_types=1);

namespace App\Domain\Entity\Property;

enum PropertyOwnerStatus: int
{
    case CURRENT_OWNER = 0;
    case FORMER_OWNER = 1;

    public function getName(): string
    {
        return match ($this) {
            self::CURRENT_OWNER => 'Aktueller Eigentümer',
            self::FORMER_OWNER => 'Ehemaliger Eigentümer'
        };
    }
}
