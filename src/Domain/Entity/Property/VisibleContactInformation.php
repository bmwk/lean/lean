<?php

declare(strict_types=1);

namespace App\Domain\Entity\Property;

enum VisibleContactInformation: int
{
    case ADDRESS = 0;
    case EMAIL = 1;
    case PHONE_NUMBER = 2;
    case MOBILE_PHONE_NUMBER = 3;
    case NAME = 4;
    case COMPANY_NAME = 5;

    public function getName(): string
    {
        return match ($this) {
            self::ADDRESS => 'Adresse',
            self::EMAIL => 'E-Mail',
            self::PHONE_NUMBER => 'Telefon',
            self::MOBILE_PHONE_NUMBER => 'Mobil',
            self::NAME => 'Name',
            self::COMPANY_NAME => 'Firma'
        };
    }
}
