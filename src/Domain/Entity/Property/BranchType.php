<?php

declare(strict_types=1);

namespace App\Domain\Entity\Property;

enum BranchType: int
{
    case REGIONAL = 0;
    case NATIONAL = 1;
    case INTERNATIONAL = 2;

    public function getName(): string
    {
        return match ($this) {
            self::REGIONAL => 'Regional',
            self::NATIONAL => 'National',
            self::INTERNATIONAL => 'International'
        };
    }
}
