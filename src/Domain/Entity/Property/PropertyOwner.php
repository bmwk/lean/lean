<?php

declare(strict_types=1);

namespace App\Domain\Entity\Property;

use App\Domain\Entity\AccountTrait;
use App\Domain\Entity\CreatedAtTrait;
use App\Domain\Entity\CreatedByAccountUserTrait;
use App\Domain\Entity\Person\Occurrence;
use App\Domain\Entity\Person\Person;
use App\Domain\Entity\UpdatedAtTrait;
use App\Repository\Property\PropertyOwnerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\Table;

#[Entity(repositoryClass: PropertyOwnerRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[HasLifecycleCallbacks]
class PropertyOwner
{
    use AccountTrait;
    use CreatedByAccountUserTrait;
    use CreatedAtTrait;
    use UpdatedAtTrait;

    #[Id]
    #[GeneratedValue]
    #[Column(type: Types::INTEGER, nullable: false, options: ['unsigned' => true])]
    private ?int $id = null;

    #[ManyToOne(inversedBy: 'propertyOwners')]
    #[JoinColumn(nullable: false)]
    private Person $person;

    #[Column(type: Types::SMALLINT, nullable: false, enumType: PropertyOwnerStatus::class, options: ['unsigned' => true])]
    private PropertyOwnerStatus $propertyOwnerStatus;

    #[Column(type: Types::SMALLINT, nullable: false, enumType: OwnershipType::class, options: ['unsigned' => true])]
    private OwnershipType $ownershipType;

    #[Column(type: Types::STRING, length: 100, nullable: true)]
    private ?string $ownershipShare = null;

    /**
     * @var Collection<int, BuildingUnit>|BuildingUnit[]
     */
    #[ManyToMany(targetEntity: BuildingUnit::class, mappedBy: 'propertyOwners')]
    private Collection|array $buildingUnits;

    /**
     * @var Collection<int, Occurrence>|Occurrence[]
     */
    #[ManyToMany(targetEntity: Occurrence::class, mappedBy: 'propertyOwners')]
    private Collection|array $occurrences;

    public function __construct()
    {
        $this->buildingUnits = new ArrayCollection();
        $this->occurrences = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getPerson(): Person
    {
        return $this->person;
    }

    public function setPerson(Person $person): self
    {
        $this->person = $person;

        return $this;
    }

    public function getPropertyOwnerStatus(): PropertyOwnerStatus
    {
        return $this->propertyOwnerStatus;
    }

    public function setPropertyOwnerStatus(PropertyOwnerStatus $propertyOwnerStatus): self
    {
        $this->propertyOwnerStatus = $propertyOwnerStatus;

        return $this;
    }

    public function getOwnershipType(): OwnershipType
    {
        return $this->ownershipType;
    }

    public function setOwnershipType(OwnershipType $ownershipType): self
    {
        $this->ownershipType = $ownershipType;

        return $this;
    }

    public function getOwnershipShare(): ?string
    {
        return $this->ownershipShare;
    }

    public function setOwnershipShare(?string $ownershipShare): self
    {
        $this->ownershipShare = $ownershipShare;

        return $this;
    }

    /**
     * @return Collection<int, BuildingUnit>|BuildingUnit[]
     */
    public function getBuildingUnits(): Collection|array
    {
        return $this->buildingUnits;
    }

    /**
     * @param Collection<int, BuildingUnit>|BuildingUnit[] $buildingUnits
     */
    public function setBuildingUnits(Collection|array $buildingUnits): self
    {
        $this->buildingUnits = $buildingUnits;

        return $this;
    }

    /**
     * @return Collection<int, Occurrence>|Occurrence[]
     */
    public function getOccurrences(): Collection|array
    {
        return $this->occurrences;
    }

    /**
     * @param Collection<int, Occurrence>|Occurrence[] $occurrences
     */
    public function setOccurrences(Collection|array $occurrences): self
    {
        $this->occurrences = $occurrences;

        return $this;
    }
}
