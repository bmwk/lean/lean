<?php

declare(strict_types=1);

namespace App\Domain\Entity\Property;

use App\Domain\Entity\AccountTrait;
use App\Domain\Entity\CreatedAtTrait;
use App\Domain\Entity\CreatedByAccountUserTrait;
use App\Domain\Entity\DeletedTrait;
use App\Domain\Entity\Document;
use App\Domain\Entity\GeolocationMultiPolygon;
use App\Domain\Entity\GeolocationPoint;
use App\Domain\Entity\GeolocationPolygon;
use App\Domain\Entity\Image;
use App\Domain\Entity\LocationFactor;
use App\Domain\Entity\UpdatedAtTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\OneToOne;

abstract class AbstractProperty
{
    use AccountTrait;
    use CreatedByAccountUserTrait;
    use DeletedTrait;
    use CreatedAtTrait;
    use UpdatedAtTrait;

    #[Column(type: 'string', length: 190, nullable: false)]
    protected string $name;

    #[Column(type: 'string', length: 60, nullable: true)]
    protected ?string $internalNumber = null;

    #[Column(type: 'float', nullable: true, options: ['unsigned' => true])]
    protected ?float $areaSize = null;

    #[Column(type: 'text', nullable: true)]
    protected ?string $placeDescription = null;

    #[Column(type: 'float', nullable: true, options: ['unsigned' => true])]
    protected ?float $entranceDoorWidth = null;

    #[Column(type: 'smallint', nullable: true, enumType: BarrierFreeAccess::class, options: ['unsigned' => true])]
    protected ?BarrierFreeAccess $barrierFreeAccess = null;

    #[Column(type: 'boolean', nullable: false)]
    protected bool $wheelchairAccessible;

    #[Column(type: 'boolean', nullable: false)]
    protected bool $rampPresent;

    #[Column(type: 'boolean', nullable: false)]
    protected bool $objectForeclosed;

    #[Column(type: 'string', length: 190, nullable: true)]
    protected ?string $fileNumberForeclosure = null;

    #[Column(type: 'date', nullable: true)]
    protected ?\DateTime $auctionDate = null;

    #[Column(type: 'string', length: 190, nullable: true)]
    protected ?string $responsibleLocalCourt = null;

    #[Column(type: 'json', nullable: true, enumType: LocationFactor::class)]
    protected ?array $locationFactors = null;

    #[OneToOne]
    #[JoinColumn(nullable: false)]
    protected Address $address;

    #[OneToOne]
    #[JoinColumn(nullable: false)]
    protected PropertySpacesData $propertySpacesData;

    #[OneToOne]
    #[JoinColumn(nullable: true)]
    protected ?Image $mainImage = null;

    #[OneToOne]
    #[JoinColumn(nullable: true)]
    protected ?GeolocationPoint $geolocationPoint = null;

    #[OneToOne]
    #[JoinColumn(nullable: true)]
    protected ?GeolocationPolygon $geolocationPolygon = null;

    #[OneToOne]
    #[JoinColumn(nullable: true)]
    protected ?GeolocationMultiPolygon $geolocationMultiPolygon = null;

    #[ManyToMany(targetEntity: Image::class)]
    protected Collection|array $images;

    #[ManyToMany(targetEntity: Document::class)]
    protected Collection|array $documents;

    #[ManyToMany(targetEntity: PropertyUser::class)]
    protected Collection|array $propertyUsers;

    #[ManyToMany(targetEntity: PropertyOwner::class)]
    protected Collection|array $propertyOwners;

    protected function __construct()
    {
        $this->images = new ArrayCollection();
        $this->documents = new ArrayCollection();
        $this->propertyUsers = new ArrayCollection();
        $this->propertyOwners = new ArrayCollection();
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getInternalNumber(): ?string
    {
        return $this->internalNumber;
    }

    public function setInternalNumber(?string $internalNumber): self
    {
        $this->internalNumber = $internalNumber;

        return $this;
    }

    public function getAreaSize(): ?float
    {
        return $this->areaSize;
    }

    public function setAreaSize(?float $areaSize): self
    {
        $this->areaSize = $areaSize;

        return $this;
    }

    public function getPlaceDescription(): ?string
    {
        return $this->placeDescription;
    }

    public function setPlaceDescription(?string $placeDescription): self
    {
        $this->placeDescription = $placeDescription;

        return $this;
    }

    public function getEntranceDoorWidth(): ?float
    {
        return $this->entranceDoorWidth;
    }

    public function setEntranceDoorWidth(?float $entranceDoorWidth): self
    {
        $this->entranceDoorWidth = $entranceDoorWidth;

        return $this;
    }

    public function getBarrierFreeAccess(): ?BarrierFreeAccess
    {
        return $this->barrierFreeAccess;
    }

    public function setBarrierFreeAccess(?BarrierFreeAccess $barrierFreeAccess): self
    {
        $this->barrierFreeAccess = $barrierFreeAccess;

        return $this;
    }

    public function isWheelchairAccessible(): bool
    {
        return $this->wheelchairAccessible;
    }

    public function setWheelchairAccessible(bool $wheelchairAccessible): self
    {
        $this->wheelchairAccessible = $wheelchairAccessible;

        return $this;
    }

    public function isRampPresent(): bool
    {
        return $this->rampPresent;
    }

    public function setRampPresent(bool $rampPresent): self
    {
        $this->rampPresent = $rampPresent;

        return $this;
    }

    public function isObjectForeclosed(): bool
    {
        return $this->objectForeclosed;
    }

    public function setObjectForeclosed(bool $objectForeclosed): self
    {
        $this->objectForeclosed = $objectForeclosed;

        return $this;
    }

    public function getFileNumberForeclosure(): ?string
    {
        return $this->fileNumberForeclosure;
    }

    public function setFileNumberForeclosure(?string $fileNumberForeclosure): self
    {
        $this->fileNumberForeclosure = $fileNumberForeclosure;

        return $this;
    }

    public function getAuctionDate(): ?\DateTime
    {
        return $this->auctionDate;
    }

    public function setAuctionDate(?\DateTime $auctionDate): self
    {
        $this->auctionDate = $auctionDate;

        return $this;
    }

    public function getResponsibleLocalCourt(): ?string
    {
        return $this->responsibleLocalCourt;
    }

    public function setResponsibleLocalCourt(?string $responsibleLocalCourt): self
    {
        $this->responsibleLocalCourt = $responsibleLocalCourt;

        return $this;
    }

    public function getLocationFactors(): ?array
    {
        return $this->locationFactors;
    }

    public function setLocationFactors(?array $locationFactors): self
    {
        $this->locationFactors = $locationFactors;

        return $this;
    }

    public function getAddress(): Address
    {
        return $this->address;
    }

    public function setAddress(Address $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getPropertySpacesData(): PropertySpacesData
    {
        return $this->propertySpacesData;
    }

    public function setPropertySpacesData(PropertySpacesData $propertySpacesData): self
    {
        $this->propertySpacesData = $propertySpacesData;

        return $this;
    }

    public function getMainImage(): ?Image
    {
        return $this->mainImage;
    }

    public function setMainImage(?Image $mainImage): self
    {
        $this->mainImage = $mainImage;

        return $this;
    }

    public function getGeolocationPoint(): ?GeolocationPoint
    {
        return $this->geolocationPoint;
    }

    public function setGeolocationPoint(?GeolocationPoint $geolocationPoint): self
    {
        $this->geolocationPoint = $geolocationPoint;

        return $this;
    }

    public function getGeolocationPolygon(): ?GeolocationPolygon
    {
        return $this->geolocationPolygon;
    }

    public function setGeolocationPolygon(?GeolocationPolygon $geolocationPolygon): self
    {
        $this->geolocationPolygon = $geolocationPolygon;

        return $this;
    }

    public function getGeolocationMultiPolygon(): ?GeolocationMultiPolygon
    {
        return $this->geolocationMultiPolygon;
    }

    public function setGeolocationMultiPolygon(?GeolocationMultiPolygon $geolocationMultiPolygon): self
    {
        $this->geolocationMultiPolygon = $geolocationMultiPolygon;

        return $this;
    }

    public function getImages(): Collection|array
    {
        return $this->images;
    }

    public function setImages(Collection|array $images): self
    {
        $this->images = $images;

        return $this;
    }

    public function getDocuments(): Collection|array
    {
        return $this->documents;
    }

    public function setDocuments(Collection|array $documents): self
    {
        $this->documents = $documents;

        return $this;
    }

    public function getPropertyUsers(): Collection|array
    {
        return $this->propertyUsers;
    }

    public function setPropertyUsers(Collection|array $propertyUsers): self
    {
        $this->propertyUsers = $propertyUsers;

        return $this;
    }

    public function getPropertyOwners(): Collection|array
    {
        return $this->propertyOwners;
    }

    public function setPropertyOwners(Collection|array $propertyOwners): self
    {
        $this->propertyOwners = $propertyOwners;

        return $this;
    }
}
