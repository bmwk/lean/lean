<?php

declare(strict_types=1);

namespace App\Domain\Entity\Property;

use App\Domain\Entity\LocationCategory;
use App\Domain\Entity\LocationType;
use App\Domain\Entity\PropertyVacancyReport\PropertyVacancyReport;
use App\Repository\Property\BuildingRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;

#[Entity(repositoryClass: BuildingRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[HasLifecycleCallbacks]
class Building extends AbstractProperty
{
    #[Id]
    #[GeneratedValue]
    #[Column(type: Types::INTEGER, nullable: false, options: ['unsigned' => true])]
    private ?int $id = null;

    #[Column(type: Types::SMALLINT, nullable: true, enumType: LocationCategory::class, options: ['unsigned' => true])]
    private ?LocationCategory $locationCategory = null;

    #[Column(type: Types::SMALLINT, nullable: true, enumType: BuildingCondition::class, options: ['unsigned' => true])]
    private ?BuildingCondition $buildingCondition = null;

    #[Column(type: Types::SMALLINT, nullable: true, enumType: BuildingStandard::class, options: ['unsigned' => true])]
    private ?BuildingStandard $buildingStandard = null;

    #[Column(type: Types::SMALLINT, nullable: true, options: ['unsigned' => true])]
    private ?int $constructionYear = null;

    /**
     * @var MonumentProtectionType[]|null
     */
    #[Column(type: Types::JSON, nullable: true, enumType: MonumentProtectionType::class)]
    private ?array $monumentProtectionTypes = null;

    /**
     * @var LiftType[]|null
     */
    #[Column(type: Types::JSON, nullable: true, enumType: LiftType::class)]
    private ?array $liftTypes = null;

    /**
     * @var WindowType[]|null
     */
    #[Column(type: Types::JSON, nullable: true, enumType: WindowType::class)]
    private ?array $windowTypes = null;

    /**
     * @var InternetConnectionType[]|null
     */
    #[Column(type: Types::JSON, nullable: true, enumType: InternetConnectionType::class)]
    private ?array $internetConnectionTypes = null;

    #[Column(type: Types::SMALLINT, nullable: true)]
    private ?int $numberOfFloors = null;

    #[Column(type: Types::STRING, length: 100, nullable: true)]
    private ?string $buildingBlockCode = null;

    #[Column(type: Types::SMALLINT, nullable: true, enumType: BuildingType::class, options: ['unsigned' => true])]
    private ?BuildingType $buildingType = null;

    #[Column(type: Types::SMALLINT, nullable: true, enumType: RoofShape::class, options: ['unsigned' => true])]
    private ?RoofShape $roofShape = null;

    #[Column(type: Types::STRING, length: 190, nullable: true)]
    private ?string $gisBuildingId = null;

    #[Column(type: Types::SMALLINT, nullable: true, enumType: LocationType::class, options: ['unsigned' => true])]
    private ?LocationType $locationType = null;

    #[Column(type: Types::BOOLEAN, nullable: true)]
    private ?bool $esgCertificateAvailable = null;

    /**
     * @var EsgCertificate[]|null
     */
    #[Column(type: Types::JSON, nullable: true, enumType: EsgCertificate::class)]
    private ?array $esgCertificates = null;

    #[Column(type: Types::TEXT, nullable: true)]
    private ?string $esgCertificateDetailInformation = null;

    public static function createFromPropertyVacancyReport(PropertyVacancyReport $propertyVacancyReport): self
    {
        $building = new self();

        $building->name = $propertyVacancyReport->getStreetName() . ' ' .
            $propertyVacancyReport->getHouseNumber() . ' ' .
            $propertyVacancyReport->getPostalCode() . ' ' .
            $propertyVacancyReport->getPlace()->getPlaceName();

        $building->address = Address::createFromPropertyVacancyReport(propertyVacancyReport: $propertyVacancyReport);

        $building->propertySpacesData = new PropertySpacesData();
        $building->locationCategory = $propertyVacancyReport->getLocationCategory();
        $building->wheelchairAccessible = false;
        $building->rampPresent = false;
        $building->objectForeclosed = false;
        $building->deleted = false;

        return $building;
    }

    public function __construct()
    {
        parent::__construct();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getLocationCategory(): ?LocationCategory
    {
        return $this->locationCategory;
    }

    public function setLocationCategory(?LocationCategory $locationCategory): self
    {
        $this->locationCategory = $locationCategory;

        return $this;
    }

    public function getBuildingCondition(): ?BuildingCondition
    {
        return $this->buildingCondition;
    }

    public function setBuildingCondition(?BuildingCondition $buildingCondition): self
    {
        $this->buildingCondition = $buildingCondition;

        return $this;
    }

    public function getBuildingStandard(): ?BuildingStandard
    {
        return $this->buildingStandard;
    }

    public function setBuildingStandard(?BuildingStandard $buildingStandard): self
    {
        $this->buildingStandard = $buildingStandard;

        return $this;
    }

    public function getConstructionYear(): ?int
    {
        return $this->constructionYear;
    }

    public function setConstructionYear(?int $constructionYear): self
    {
        $this->constructionYear = $constructionYear;

        return $this;
    }

    /**
     * @return MonumentProtectionType[]|null
     */
    public function getMonumentProtectionTypes(): ?array
    {
        return $this->monumentProtectionTypes;
    }

    /**
     * @param MonumentProtectionType[]|null $monumentProtectionTypes
     */
    public function setMonumentProtectionTypes(?array $monumentProtectionTypes): self
    {
        $this->monumentProtectionTypes = $monumentProtectionTypes;

        return $this;
    }

    /**
     * @return LiftType[]|null
     */
    public function getLiftTypes(): ?array
    {
        return $this->liftTypes;
    }

    /**
     * @param LiftType[]|null $liftTypes
     */
    public function setLiftTypes(?array $liftTypes): self
    {
        $this->liftTypes = $liftTypes;

        return $this;
    }

    /**
     * @return WindowType[]|null
     */
    public function getWindowTypes(): ?array
    {
        return $this->windowTypes;
    }

    /**
     * @param WindowType[]|null $windowTypes
     */
    public function setWindowTypes(?array $windowTypes): self
    {
        $this->windowTypes = $windowTypes;

        return $this;
    }

    /**
     * @return InternetConnectionType[]|null
     */
    public function getInternetConnectionTypes(): ?array
    {
        return $this->internetConnectionTypes;
    }

    /**
     * @param InternetConnectionType[]|null $internetConnectionTypes
     */
    public function setInternetConnectionTypes(?array $internetConnectionTypes): self
    {
        $this->internetConnectionTypes = $internetConnectionTypes;

        return $this;
    }

    public function getNumberOfFloors(): ?int
    {
        return $this->numberOfFloors;
    }

    public function setNumberOfFloors(?int $numberOfFloors): self
    {
        $this->numberOfFloors = $numberOfFloors;

        return $this;
    }

    public function getBuildingBlockCode(): ?string
    {
        return $this->buildingBlockCode;
    }

    public function setBuildingBlockCode(?string $buildingBlockCode): self
    {
        $this->buildingBlockCode = $buildingBlockCode;

        return $this;
    }

    public function getBuildingType(): ?BuildingType
    {
        return $this->buildingType;
    }

    public function setBuildingType(?BuildingType $buildingType): self
    {
        $this->buildingType = $buildingType;

        return $this;
    }

    public function getRoofShape(): ?RoofShape
    {
        return $this->roofShape;
    }

    public function setRoofShape(?RoofShape $roofShape): self
    {
        $this->roofShape = $roofShape;

        return $this;
    }

    public function getGisBuildingId(): ?string
    {
        return $this->gisBuildingId;
    }

    public function setGisBuildingId(?string $gisBuildingId): self
    {
        $this->gisBuildingId = $gisBuildingId;

        return $this;
    }

    public function getLocationType(): ?LocationType
    {
        return $this->locationType;
    }

    public function setLocationType(?LocationType $locationType): self
    {
        $this->locationType = $locationType;

        return $this;
    }

    public function isEsgCertificateAvailable(): ?bool
    {
        return $this->esgCertificateAvailable;
    }

    public function setEsgCertificateAvailable(?bool $esgCertificateAvailable): self
    {
        $this->esgCertificateAvailable = $esgCertificateAvailable;

        return $this;
    }

    /**
     * @return EsgCertificate[]|null
     */
    public function getEsgCertificates(): ?array
    {
        return $this->esgCertificates;
    }

    /**
     * @param EsgCertificate[]|null $esgCertificates
     */
    public function setEsgCertificates(?array $esgCertificates): self
    {
        $this->esgCertificates = $esgCertificates;

        return $this;
    }

    public function getEsgCertificateDetailInformation(): ?string
    {
        return $this->esgCertificateDetailInformation;
    }

    public function setEsgCertificateDetailInformation(?string $esgCertificateDetailInformation): self
    {
        $this->esgCertificateDetailInformation = $esgCertificateDetailInformation;

        return $this;
    }
}
