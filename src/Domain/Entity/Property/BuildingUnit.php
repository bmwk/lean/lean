<?php

declare(strict_types=1);

namespace App\Domain\Entity\Property;

use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\Classification\IndustryClassification;
use App\Domain\Entity\Document;
use App\Domain\Entity\Image;
use App\Domain\Entity\LookingForPropertyRequest\LookingForPropertyRequestExposeForwarding;
use App\Domain\Entity\LookingForPropertyRequest\LookingForPropertyRequestMatchingResponse;
use App\Domain\Entity\LookingForPropertyRequest\MatchingResult;
use App\Domain\Entity\LookingForPropertyRequest\MatchingTask;
use App\Domain\Entity\OpenImmoExporter\OpenImmoExport;
use App\Domain\Entity\OpenImmoFtpTransfer\OpenImmoTransferLog;
use App\Domain\Entity\PropertyVacancyReport\PropertyVacancyReport;
use App\Domain\Entity\SettlementConcept\SettlementConceptExposeForwarding;
use App\Domain\Entity\SettlementConcept\SettlementConceptMatchingResponse;
use App\Domain\Entity\SettlementConcept\SettlementConceptMatchingResult;
use App\Domain\Entity\SettlementConcept\SettlementConceptMatchingTask;
use App\Repository\Property\BuildingUnitRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Index;
use Doctrine\ORM\Mapping\InverseJoinColumn;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\Table;

#[Entity(repositoryClass: BuildingUnitRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[Index(columns: ['account_id', 'archived', 'deleted'], name: 'account_id_archived_deleted_idx')]
#[HasLifecycleCallbacks]
class BuildingUnit extends AbstractProperty
{
    #[Id]
    #[GeneratedValue]
    #[Column(type: Types::INTEGER, nullable: false, options: ['unsigned' => true])]
    private ?int $id = null;

    #[Column(type: Types::TEXT, nullable: true)]
    private ?string $description = null;

    #[Column(type: Types::BOOLEAN, nullable: false)]
    private bool $objectIsEmpty;

    #[Column(type: Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTime $objectIsEmptySince = null;

    #[Column(type: Types::BOOLEAN, nullable: false)]
    private bool $objectBecomesEmpty;

    #[Column(type: Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTime $objectBecomesEmptyFrom = null;

    #[Column(type: Types::BOOLEAN, nullable: false)]
    private bool $reuseAgreed;

    #[Column(type: Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTime $reuseFrom = null;

    #[Column(type: Types::TEXT, nullable: true)]
    private ?string $currentUsageDescription = null;

    #[Column(type: Types::TEXT, nullable: true)]
    private ?string $pastUsageDescription = null;

    #[Column(type: Types::TEXT, nullable: true)]
    private ?string $futureUsageDescription = null;

    #[Column(type: Types::TEXT, nullable: true)]
    private ?string $internalNote = null;

    #[ManyToOne]
    #[JoinColumn(nullable: true)]
    private ?IndustryClassification $currentUsageIndustryClassification = null;

    #[ManyToOne]
    #[JoinColumn(nullable: true)]
    private ?IndustryClassification $pastUsageIndustryClassification = null;

    #[ManyToOne]
    #[JoinColumn(nullable: true)]
    private ?IndustryClassification $futureUsageIndustryClassification = null;

    #[Column(type: Types::SMALLINT, nullable: true, enumType: PropertyUsageBusinessType::class, options: ['unsigned' => true])]
    private ?PropertyUsageBusinessType $currentPropertyUsageBusinessType = null;

    #[Column(type: Types::SMALLINT, nullable: true, enumType: PropertyUsageBusinessType::class, options: ['unsigned' => true])]
    private ?PropertyUsageBusinessType $pastPropertyUsageBusinessType = null;

    #[Column(type: Types::SMALLINT, nullable: true, enumType: PropertyUsageBusinessType::class, options: ['unsigned' => true])]
    private ?PropertyUsageBusinessType $futurePropertyUsageBusinessType = null;

    #[Column(type: Types::SMALLINT, nullable: true, enumType: HeatingMethod::class, options: ['unsigned' => true])]
    private ?HeatingMethod $heatingMethod = null;

    #[Column(type: Types::INTEGER, nullable: true, options: ['unsigned' => true])]
    private ?int $numberOfParkingLots = null;

    #[Column(type: Types::TEXT, nullable: true)]
    private ?string $particularities = null;

    #[Column(type: Types::JSON, nullable: true)]
    private ?array $inFloors = null;

    #[Column(type: Types::TEXT, nullable: true)]
    private ?string $neighbourBusiness = null;

    #[Column(type: Types::TEXT, nullable: true)]
    private ?string $passantFrequencyGenerator = null;

    #[Column(type: Types::BOOLEAN, nullable: true)]
    private ?bool $roofingPresent = null;

    #[Column(type: Types::BOOLEAN, nullable: true)]
    private ?bool $interimUsePossible = null;

    #[Column(type: Types::TEXT, nullable: true)]
    private ?string $interimUseInformation = null;

    #[Column(type: Types::JSON, nullable: true, enumType: InterimUseType::class)]
    private ?array $interimUseTypes = null;

    #[Column(type: Types::BOOLEAN, nullable: false)]
    private bool $archived;

    #[Column(type: Types::BOOLEAN, nullable: true, options: ['unsigned' => true])]
    private ?bool $shopWindowAvailable = null;

    #[Column(type: Types::FLOAT, nullable: true, options: ['unsigned' => true])]
    private ?float $shopWindowFrontWidth = null;

    #[Column(type: Types::BOOLEAN, nullable: true)]
    private ?bool $showroomAvailable = null;

    #[Column(type: Types::FLOAT, nullable: true, options: ['unsigned' => true])]
    private ?float $shopWidth = null;

    #[Column(type: Types::BOOLEAN, nullable: true)]
    private ?bool $groundLevelSalesArea = null;

    #[Column(type: Types::BOOLEAN, nullable: true)]
    private ?bool $keyProperty = null;

    #[Column(type: Types::BOOLEAN, nullable: true)]
    private ?bool $airConditioned = null;

    #[Column(type: Types::SMALLINT, nullable: true, enumType: VacancyReason::class, options: ['unsigned' => true])]
    private ?VacancyReason $vacancyReason = null;

    #[ManyToOne]
    #[JoinColumn(nullable: true)]
    private ?AccountUser $assignedToAccountUser = null;

    #[ManyToOne]
    #[JoinColumn(nullable: false)]
    private Building $building;

    #[OneToOne]
    #[JoinColumn(nullable: true)]
    private ?EnergyCertificate $energyCertificate = null;

    #[OneToOne]
    #[JoinColumn(nullable: false)]
    private PropertyMarketingInformation $propertyMarketingInformation;

    #[OneToOne(mappedBy: 'buildingUnit')]
    private ?MatchingTask $matchingTask = null;

    #[OneToOne(mappedBy: 'buildingUnit')]
    private ?SettlementConceptMatchingTask $settlementConceptMatchingTask = null;

    #[OneToOne(mappedBy: 'buildingUnit')]
    private ?PropertyVacancyReport $propertyVacancyReport = null;

    #[OneToOne(mappedBy: 'buildingUnit')]
    private ?OpenImmoAdditionalInformation $openImmoAdditionalInformation = null;

    #[OneToOne(mappedBy: 'buildingUnit')]
    private ?ExternalContactPerson $externalContactPerson = null;

    /**
     * @var Collection<int, PropertyContactPerson>|PropertyContactPerson[]
     */
    #[ManyToMany(targetEntity: PropertyContactPerson::class, inversedBy: 'buildingUnits')]
    #[InverseJoinColumn(unique: true)]
    private Collection|array $propertyContactPersons;

    /**
     * @var Collection<int, OpenImmoExport>|OpenImmoExport[]
     */
    #[OneToMany(mappedBy: 'buildingUnit', targetEntity: OpenImmoExport::class)]
    private Collection|array $openImmoExports;

    /**
     * @var Collection<int, OpenImmoTransferLog>|OpenImmoTransferLog[]
     */
    #[OneToMany(mappedBy: 'buildingUnit', targetEntity: OpenImmoTransferLog::class)]
    private Collection|array $openImmoTransferLogs;

    /**
     * @var Collection<int, MatchingResult>|MatchingResult[]
     */
    #[OneToMany(mappedBy: 'buildingUnit', targetEntity: MatchingResult::class)]
    private Collection|array $matchingResults;

    /**
     * @var Collection<int, SettlementConceptMatchingResult>|SettlementConceptMatchingResult[]
     */
    #[OneToMany(mappedBy: 'buildingUnit', targetEntity: SettlementConceptMatchingResult::class)]
    private Collection|array $settlementConceptMatchingResults;

    /**
     * @var Collection<int, LookingForPropertyRequestExposeForwarding>|LookingForPropertyRequestExposeForwarding[]
     */
    #[OneToMany(mappedBy: 'buildingUnit', targetEntity: LookingForPropertyRequestExposeForwarding::class)]
    private Collection|array $lookingForPropertyRequestExposeForwardings;

    /**
     * @var Collection<int, SettlementConceptExposeForwarding>|SettlementConceptExposeForwarding[]
     */
    #[OneToMany(mappedBy: 'buildingUnit', targetEntity: SettlementConceptExposeForwarding::class)]
    private Collection|array $settlementConceptExposeForwardings;

    /**
     * @var Collection<int, LookingForPropertyRequestMatchingResponse>|LookingForPropertyRequestMatchingResponse[]
     */
    #[OneToMany(mappedBy: 'buildingUnit', targetEntity: LookingForPropertyRequestMatchingResponse::class)]
    private Collection|array $lookingForPropertyRequestMatchingResponses;

    /**
     * @var Collection<int, SettlementConceptMatchingResponse>|SettlementConceptMatchingResponse[]
     */
    #[OneToMany(mappedBy: 'buildingUnit', targetEntity: SettlementConceptMatchingResponse::class)]
    private Collection|array $settlementConceptMatchingResponses;

    /**
     * @var Collection<int, PropertyOwner>|PropertyOwner[]
     */
    #[ManyToMany(targetEntity: PropertyOwner::class, inversedBy: 'buildingUnits')]
    protected Collection|array $propertyOwners;

    /**
     * @var Collection<int, PropertyUser>|PropertyUser[]
     */
    #[ManyToMany(targetEntity: PropertyUser::class, inversedBy: 'buildingUnits')]
    protected Collection|array $propertyUsers;

    /**
     * @var Collection<int, Image>|Image[]
     */
    #[ManyToMany(targetEntity: Image::class, inversedBy: 'buildingUnits')]
    protected Collection|array $images;

    /**
     * @var Collection<int, Document>|Document[]
     */
    #[ManyToMany(targetEntity: Document::class, inversedBy: 'buildingUnits')]
    protected Collection|array $documents;

    public static function createFromPropertyVacancyReport(PropertyVacancyReport $propertyVacancyReport): self
    {
        $buildingUnit = new self();

        $buildingUnit->name = $propertyVacancyReport->getStreetName() . ' ' .
            $propertyVacancyReport->getHouseNumber() . ' ' .
            $propertyVacancyReport->getPostalCode() . ' ' .
            $propertyVacancyReport->getPlace()->getPlaceName();

        $buildingUnit->address = Address::createFromPropertyVacancyReport(propertyVacancyReport: $propertyVacancyReport);
        $buildingUnit->building = Building::createFromPropertyVacancyReport(propertyVacancyReport: $propertyVacancyReport);
        $buildingUnit->propertySpacesData = PropertySpacesData::createFromPropertyVacancyReport(propertyVacancyReport: $propertyVacancyReport);
        $buildingUnit->propertyMarketingInformation = PropertyMarketingInformation::createFromPropertyVacancyReport(propertyVacancyReport: $propertyVacancyReport);
        $buildingUnit->areaSize = $propertyVacancyReport->getAreaSize();
        $buildingUnit->placeDescription = $propertyVacancyReport->getPlaceDescription();
        $buildingUnit->description = $propertyVacancyReport->getPropertyDescription();
        $buildingUnit->objectIsEmpty = $propertyVacancyReport->isObjectIsEmpty();
        $buildingUnit->objectIsEmptySince = $propertyVacancyReport->getObjectIsEmptySince();
        $buildingUnit->objectBecomesEmpty = $propertyVacancyReport->isObjectBecomesEmpty();
        $buildingUnit->objectBecomesEmptyFrom = $propertyVacancyReport->getObjectBecomesEmptyFrom();
        $buildingUnit->shopWindowFrontWidth = $propertyVacancyReport->getShopWindowFrontWidth();
        $buildingUnit->shopWidth = $propertyVacancyReport->getShopWidth();
        $buildingUnit->groundLevelSalesArea = $propertyVacancyReport->getGroundLevelSalesArea();
        $buildingUnit->numberOfParkingLots = $propertyVacancyReport->getNumberOfParkingLots();
        $buildingUnit->barrierFreeAccess = $propertyVacancyReport->getBarrierFreeAccess();
        $buildingUnit->locationFactors = $propertyVacancyReport->getLocationFactors();
        $buildingUnit->reuseAgreed = false;
        $buildingUnit->wheelchairAccessible = false;
        $buildingUnit->rampPresent = false;
        $buildingUnit->objectForeclosed = false;
        $buildingUnit->archived = false;
        $buildingUnit->deleted = false;

        if ($propertyVacancyReport->isObjectIsEmpty() === true && $propertyVacancyReport->isObjectBecomesEmpty() === false) {
            $buildingUnit->pastUsageIndustryClassification = $propertyVacancyReport->getIndustryClassification();
            $buildingUnit->currentUsageIndustryClassification = null;
        } elseif ($propertyVacancyReport->isObjectBecomesEmpty() === true && $propertyVacancyReport->isObjectIsEmpty() === false) {
            $buildingUnit->pastUsageIndustryClassification = null;
            $buildingUnit->currentUsageIndustryClassification = $propertyVacancyReport->getIndustryClassification();
        } else {
            $buildingUnit->pastUsageIndustryClassification = null;
            $buildingUnit->currentUsageIndustryClassification = null;
        }

        return $buildingUnit;
    }

    public function __construct()
    {
        $this->propertyContactPersons = new ArrayCollection();
        $this->openImmoExports = new ArrayCollection();
        $this->openImmoTransferLogs = new ArrayCollection();
        $this->matchingResults = new ArrayCollection();
        $this->settlementConceptMatchingResults = new ArrayCollection();
        $this->lookingForPropertyRequestExposeForwardings = new ArrayCollection();
        $this->settlementConceptExposeForwardings = new ArrayCollection();
        $this->lookingForPropertyRequestMatchingResponses = new ArrayCollection();
        $this->settlementConceptMatchingResponses = new ArrayCollection();

        parent::__construct();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function isObjectIsEmpty(): bool
    {
        return $this->objectIsEmpty;
    }

    public function setObjectIsEmpty(bool $objectIsEmpty): self
    {
        $this->objectIsEmpty = $objectIsEmpty;

        return $this;
    }

    public function getObjectIsEmptySince(): ?\DateTime
    {
        return $this->objectIsEmptySince;
    }

    public function setObjectIsEmptySince(?\DateTime $objectIsEmptySince): self
    {
        $this->objectIsEmptySince = $objectIsEmptySince;

        return $this;
    }

    public function isObjectBecomesEmpty(): bool
    {
        return $this->objectBecomesEmpty;
    }

    public function setObjectBecomesEmpty(bool $objectBecomesEmpty): self
    {
        $this->objectBecomesEmpty = $objectBecomesEmpty;

        return $this;
    }

    public function getObjectBecomesEmptyFrom(): ?\DateTime
    {
        return $this->objectBecomesEmptyFrom;
    }

    public function setObjectBecomesEmptyFrom(?\DateTime $objectBecomesEmptyFrom): self
    {
        $this->objectBecomesEmptyFrom = $objectBecomesEmptyFrom;

        return $this;
    }

    public function isReuseAgreed(): bool
    {
        return $this->reuseAgreed;
    }

    public function setReuseAgreed(bool $reuseAgreed): self
    {
        $this->reuseAgreed = $reuseAgreed;

        return $this;
    }

    public function getReuseFrom(): ?\DateTime
    {
        return $this->reuseFrom;
    }

    public function setReuseFrom(?\DateTime $reuseFrom): self
    {
        $this->reuseFrom = $reuseFrom;

        return $this;
    }

    public function getCurrentUsageDescription(): ?string
    {
        return $this->currentUsageDescription;
    }

    public function setCurrentUsageDescription(?string $currentUsageDescription): self
    {
        $this->currentUsageDescription = $currentUsageDescription;

        return $this;
    }

    public function getPastUsageDescription(): ?string
    {
        return $this->pastUsageDescription;
    }

    public function setPastUsageDescription(?string $pastUsageDescription): self
    {
        $this->pastUsageDescription = $pastUsageDescription;

        return $this;
    }

    public function getFutureUsageDescription(): ?string
    {
        return $this->futureUsageDescription;
    }

    public function setFutureUsageDescription(?string $futureUsageDescription): self
    {
        $this->futureUsageDescription = $futureUsageDescription;

        return $this;
    }

    public function getInternalNote(): ?string
    {
        return $this->internalNote;
    }

    public function setInternalNote(?string $internalNote): self
    {
        $this->internalNote = $internalNote;

        return $this;
    }

    public function getCurrentUsageIndustryClassification(): ?IndustryClassification
    {
        return $this->currentUsageIndustryClassification;
    }

    public function setCurrentUsageIndustryClassification(?IndustryClassification $currentUsageIndustryClassification): self
    {
        $this->currentUsageIndustryClassification = $currentUsageIndustryClassification;

        return $this;
    }

    public function getPastUsageIndustryClassification(): ?IndustryClassification
    {
        return $this->pastUsageIndustryClassification;
    }

    public function setPastUsageIndustryClassification(?IndustryClassification $pastUsageIndustryClassification): self
    {
        $this->pastUsageIndustryClassification = $pastUsageIndustryClassification;

        return $this;
    }

    public function getFutureUsageIndustryClassification(): ?IndustryClassification
    {
        return $this->futureUsageIndustryClassification;
    }

    public function setFutureUsageIndustryClassification(?IndustryClassification $futureUsageIndustryClassification): self
    {
        $this->futureUsageIndustryClassification = $futureUsageIndustryClassification;

        return $this;
    }

    public function getCurrentPropertyUsageBusinessType(): ?PropertyUsageBusinessType
    {
        return $this->currentPropertyUsageBusinessType;
    }

    public function setCurrentPropertyUsageBusinessType(?PropertyUsageBusinessType $currentPropertyUsageBusinessType): self
    {
        $this->currentPropertyUsageBusinessType = $currentPropertyUsageBusinessType;

        return $this;
    }

    public function getPastPropertyUsageBusinessType(): ?PropertyUsageBusinessType
    {
        return $this->pastPropertyUsageBusinessType;
    }

    public function setPastPropertyUsageBusinessType(?PropertyUsageBusinessType $pastPropertyUsageBusinessType): self
    {
        $this->pastPropertyUsageBusinessType = $pastPropertyUsageBusinessType;

        return $this;
    }

    public function getFuturePropertyUsageBusinessType(): ?PropertyUsageBusinessType
    {
        return $this->futurePropertyUsageBusinessType;
    }

    public function setFuturePropertyUsageBusinessType(?PropertyUsageBusinessType $futurePropertyUsageBusinessType): self
    {
        $this->futurePropertyUsageBusinessType = $futurePropertyUsageBusinessType;

        return $this;
    }

    public function getHeatingMethod(): ?HeatingMethod
    {
        return $this->heatingMethod;
    }

    public function setHeatingMethod(?HeatingMethod $heatingMethod): self
    {
        $this->heatingMethod = $heatingMethod;

        return $this;
    }

    public function getNumberOfParkingLots(): ?int
    {
        return $this->numberOfParkingLots;
    }

    public function setNumberOfParkingLots(?int $numberOfParkingLots): self
    {
        $this->numberOfParkingLots = $numberOfParkingLots;

        return $this;
    }

    public function getParticularities(): ?string
    {
        return $this->particularities;
    }

    public function setParticularities(?string $particularities): self
    {
        $this->particularities = $particularities;

        return $this;
    }

    public function getInFloors(): ?array
    {
        return $this->inFloors;
    }

    public function setInFloors(?array $inFloors): self
    {
        $this->inFloors = $inFloors;

        return $this;
    }

    public function getNeighbourBusiness(): ?string
    {
        return $this->neighbourBusiness;
    }

    public function setNeighbourBusiness(?string $neighbourBusiness): self
    {
        $this->neighbourBusiness = $neighbourBusiness;

        return $this;
    }

    public function getPassantFrequencyGenerator(): ?string
    {
        return $this->passantFrequencyGenerator;
    }

    public function setPassantFrequencyGenerator(?string $passantFrequencyGenerator): self
    {
        $this->passantFrequencyGenerator = $passantFrequencyGenerator;

        return $this;
    }

    public function getRoofingPresent(): ?bool
    {
        return $this->roofingPresent;
    }

    public function setRoofingPresent(?bool $roofingPresent): self
    {
        $this->roofingPresent = $roofingPresent;

        return $this;
    }

    public function isInterimUsePossible(): ?bool
    {
        return $this->interimUsePossible;
    }

    public function setInterimUsePossible(?bool $interimUsePossible): self
    {
        $this->interimUsePossible = $interimUsePossible;

        return $this;
    }

    public function getInterimUseInformation(): ?string
    {
        return $this->interimUseInformation;
    }

    public function setInterimUseInformation(?string $interimUseInformation): self
    {
        $this->interimUseInformation = $interimUseInformation;

        return $this;
    }

    public function getInterimUseTypes(): ?array
    {
        return $this->interimUseTypes;
    }

    public function setInterimUseTypes(?array $interimUseTypes): self
    {
        $this->interimUseTypes = $interimUseTypes;

        return $this;
    }

    public function isArchived(): bool
    {
        return $this->archived;
    }

    public function setArchived(bool $archived): self
    {
        $this->archived = $archived;

        return $this;
    }

    public function isShopWindowAvailable(): ?bool
    {
        return $this->shopWindowAvailable;
    }

    public function setShopWindowAvailable(?bool $shopWindowAvailable): self
    {
        $this->shopWindowAvailable = $shopWindowAvailable;

        return $this;
    }

    public function getShopWindowFrontWidth(): ?float
    {
        return $this->shopWindowFrontWidth;
    }

    public function setShopWindowFrontWidth(?float $shopWindowFrontWidth): self
    {
        $this->shopWindowFrontWidth = $shopWindowFrontWidth;

        return $this;
    }

    public function isShowroomAvailable(): ?bool
    {
        return $this->showroomAvailable;
    }

    public function setShowroomAvailable(?bool $showroomAvailable): self
    {
        $this->showroomAvailable = $showroomAvailable;

        return $this;
    }

    public function getShopWidth(): ?float
    {
        return $this->shopWidth;
    }

    public function setShopWidth(?float $shopWidth): self
    {
        $this->shopWidth = $shopWidth;

        return $this;
    }

    public function isGroundLevelSalesArea(): ?bool
    {
        return $this->groundLevelSalesArea;
    }

    public function setGroundLevelSalesArea(?bool $groundLevelSalesArea): self
    {
        $this->groundLevelSalesArea = $groundLevelSalesArea;

        return $this;
    }

    public function isKeyProperty(): ?bool
    {
        return $this->keyProperty;
    }

    public function setKeyProperty(?bool $keyProperty): self
    {
        $this->keyProperty = $keyProperty;

        return $this;
    }

    public function isAirConditioned(): ?bool
    {
        return $this->airConditioned;
    }

    public function setAirConditioned(?bool $airConditioned): self
    {
        $this->airConditioned = $airConditioned;

        return $this;
    }

    public function getVacancyReason(): ?VacancyReason
    {
        return $this->vacancyReason;
    }

    public function setVacancyReason(?VacancyReason $vacancyReason): self
    {
        $this->vacancyReason = $vacancyReason;

        return $this;
    }

    public function getAssignedToAccountUser(): ?AccountUser
    {
        return $this->assignedToAccountUser;
    }

    public function setAssignedToAccountUser(?AccountUser $assignedToAccountUser): self
    {
        $this->assignedToAccountUser = $assignedToAccountUser;

        return $this;
    }

    public function getBuilding(): Building
    {
        return $this->building;
    }

    public function setBuilding(Building $building): self
    {
        $this->building = $building;

        return $this;
    }

    public function getEnergyCertificate(): ?EnergyCertificate
    {
        return $this->energyCertificate;
    }

    public function setEnergyCertificate(?EnergyCertificate $energyCertificate): self
    {
        $this->energyCertificate = $energyCertificate;

        return $this;
    }

    public function getPropertyMarketingInformation(): PropertyMarketingInformation
    {
        return $this->propertyMarketingInformation;
    }

    public function setPropertyMarketingInformation(PropertyMarketingInformation $propertyMarketingInformation): self
    {
        $this->propertyMarketingInformation = $propertyMarketingInformation;

        return $this;
    }

    public function getMatchingTask(): ?MatchingTask
    {
        return $this->matchingTask;
    }

    public function setMatchingTask(?MatchingTask $matchingTask): self
    {
        $this->matchingTask = $matchingTask;

        return $this;
    }

    public function getSettlementConceptMatchingTask(): ?SettlementConceptMatchingTask
    {
        return $this->settlementConceptMatchingTask;
    }

    public function setSettlementConceptMatchingTask(?SettlementConceptMatchingTask $settlementConceptMatchingTask): self
    {
        $this->settlementConceptMatchingTask = $settlementConceptMatchingTask;

        return $this;
    }

    public function getPropertyVacancyReport(): ?PropertyVacancyReport
    {
        return $this->propertyVacancyReport;
    }

    public function setPropertyVacancyReport(?PropertyVacancyReport $propertyVacancyReport): self
    {
        $this->propertyVacancyReport = $propertyVacancyReport;

        return $this;
    }

    public function getOpenImmoAdditionalInformation(): ?OpenImmoAdditionalInformation
    {
        return $this->openImmoAdditionalInformation;
    }

    public function setOpenImmoAdditionalInformation(?OpenImmoAdditionalInformation $openImmoAdditionalInformation): self
    {
        $this->openImmoAdditionalInformation = $openImmoAdditionalInformation;

        return $this;
    }

    public function getExternalContactPerson(): ?ExternalContactPerson
    {
        return $this->externalContactPerson;
    }

    public function setExternalContactPerson(?ExternalContactPerson $externalContactPerson): self
    {
        $this->externalContactPerson = $externalContactPerson;

        return $this;
    }

    /**
     * @return Collection<int, PropertyContactPerson>|PropertyContactPerson[]
     */
    public function getPropertyContactPersons(): Collection|array
    {
        return $this->propertyContactPersons;
    }

    /**
     * @param Collection<int, PropertyContactPerson>|PropertyContactPerson[] $propertyContactPersons
     */
    public function setPropertyContactPersons(Collection|array $propertyContactPersons): self
    {
        $this->propertyContactPersons = $propertyContactPersons;

        return $this;
    }

    /**
     * @return Collection<int, OpenImmoExport>|OpenImmoExport[]
     */
    public function getOpenImmoExports(): Collection|array
    {
        return $this->openImmoExports;
    }

    /**
     * @param Collection<int, OpenImmoExport>|OpenImmoExport[] $openImmoExports
     */
    public function setOpenImmoExports(Collection|array $openImmoExports): self
    {
        $this->openImmoExports = $openImmoExports;

        return $this;
    }

    /**
     * @return Collection<int, OpenImmoTransferLog>|OpenImmoTransferLog[]
     */
    public function getOpenImmoTransferLogs(): Collection|array
    {
        return $this->openImmoTransferLogs;
    }

    /**
     * @param Collection<int, OpenImmoTransferLog>|OpenImmoTransferLog[] $openImmoTransferLogs
     */
    public function setOpenImmoTransferLogs(Collection|array $openImmoTransferLogs): self
    {
        $this->openImmoTransferLogs = $openImmoTransferLogs;

        return $this;
    }

    /**
     * @return  Collection<int, MatchingResult>|MatchingResult[]
     */
    public function getMatchingResults(): Collection|array
    {
        return $this->matchingResults;
    }

    /**
     * @param Collection<int, MatchingResult>|MatchingResult[] $matchingResults
     */
    public function setMatchingResults(Collection|array $matchingResults): self
    {
        $this->matchingResults = $matchingResults;

        return $this;
    }

    /**
     * @return  Collection<int, SettlementConceptMatchingResult>|SettlementConceptMatchingResult[]
     */
    public function getSettlementConceptMatchingResults(): Collection|array
    {
        return $this->settlementConceptMatchingResults;
    }

    /**
     * @param Collection<int, SettlementConceptMatchingResult>|SettlementConceptMatchingResult[] $settlementConceptMatchingResults
     */
    public function setSettlementConceptMatchingResults(Collection|array $settlementConceptMatchingResults): self
    {
        $this->settlementConceptMatchingResults = $settlementConceptMatchingResults;

        return $this;
    }

    /**
     * @return  Collection<int, LookingForPropertyRequestExposeForwarding>|LookingForPropertyRequestExposeForwarding[]
     */
    public function getLookingForPropertyRequestExposeForwardings(): Collection|array
    {
        return $this->lookingForPropertyRequestExposeForwardings;
    }

    /**
     * @param Collection<int, LookingForPropertyRequestExposeForwarding>|LookingForPropertyRequestExposeForwarding[] $lookingForPropertyRequestExposeForwardings
     */
    public function setLookingForPropertyRequestExposeForwardings(Collection|array $lookingForPropertyRequestExposeForwardings): self
    {
        $this->lookingForPropertyRequestExposeForwardings = $lookingForPropertyRequestExposeForwardings;

        return $this;
    }

    /**
     * @return  Collection<int, SettlementConceptExposeForwarding>|SettlementConceptExposeForwarding[]
     */
    public function getSettlementConceptExposeForwardings(): Collection|array
    {
        return $this->settlementConceptExposeForwardings;
    }

    /**
     * @param Collection<int, SettlementConceptExposeForwarding>|SettlementConceptExposeForwarding[] $settlementConceptExposeForwardings
     */
    public function setSettlementConceptExposeForwardings(Collection|array $settlementConceptExposeForwardings): self
    {
        $this->settlementConceptExposeForwardings = $settlementConceptExposeForwardings;

        return $this;
    }

    /**
     * @return  Collection<int, LookingForPropertyRequestMatchingResponse>|LookingForPropertyRequestMatchingResponse[]
     */
    public function getLookingForPropertyRequestMatchingResponses(): Collection|array
    {
        return $this->lookingForPropertyRequestMatchingResponses;
    }

    /**
     * @param Collection<int, LookingForPropertyRequestMatchingResponse>|LookingForPropertyRequestMatchingResponse[] $lookingForPropertyRequestMatchingResponses
     */
    public function setLookingForPropertyRequestMatchingResponses(Collection|array $lookingForPropertyRequestMatchingResponses): self
    {
        $this->lookingForPropertyRequestMatchingResponses = $lookingForPropertyRequestMatchingResponses;

        return $this;
    }

    /**
     * @return  Collection<int, SettlementConceptMatchingResponse>|SettlementConceptMatchingResponse[]
     */
    public function getSettlementConceptMatchingResponses(): Collection|array
    {
        return $this->settlementConceptMatchingResponses;
    }

    /**
     * @param Collection<int, SettlementConceptMatchingResponse>|SettlementConceptMatchingResponse[] $settlementConceptMatchingResponses
     */
    public function setSettlementConceptMatchingResponses(Collection|array $settlementConceptMatchingResponses): self
    {
        $this->settlementConceptMatchingResponses = $settlementConceptMatchingResponses;

        return $this;
    }
}
