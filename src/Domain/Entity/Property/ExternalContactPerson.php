<?php

declare(strict_types=1);

namespace App\Domain\Entity\Property;

use App\Domain\Entity\CreatedAtTrait;
use App\Domain\Entity\Person\Salutation;
use App\Domain\Entity\UpdatedAtTrait;
use App\Repository\Property\ExternalContactPersonRepository;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\Table;

#[Entity(repositoryClass: ExternalContactPersonRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[HasLifecycleCallbacks]
class ExternalContactPerson
{
    use CreatedAtTrait;
    use UpdatedAtTrait;

    #[Id]
    #[GeneratedValue]
    #[Column(type: 'integer', nullable: false, options: ['unsigned' => true])]
    private ?int $id = null;

    #[OneToOne(inversedBy: 'externalContactPerson')]
    #[JoinColumn(nullable: false)]
    private BuildingUnit $buildingUnit;

    #[Column(type: 'smallint', nullable: true, enumType: Salutation::class, options: ['unsigned' => true])]
    private ?Salutation $salutation = null;

    #[Column(type: 'string', length: 190, nullable: true)]
    private ?string $salutationLetter = null;

    #[Column(type: 'string', length: 190, nullable: true)]
    private ?string $personTitle = null;

    #[Column(type: 'string', length: 190, nullable: true)]
    private ?string $firstName = null;

    #[Column(type: 'string', length: 190, nullable: true)]
    private ?string $lastName = null;

    #[Column(type: 'string', length: 190, nullable: true)]
    private ?string $companyName = null;

    #[Column(type: 'string', length: 190, nullable: true)]
    private ?string $streetName = null;

    #[Column(type: 'string', length: 20, nullable: true)]
    private ?string $houseNumber = null;

    #[Column(type: 'string', length: 20, nullable: true)]
    private ?string $postalCode = null;

    #[Column(type: 'string', length: 190, nullable: true)]
    private ?string $placeName = null;

    #[Column(type: 'text', nullable: true)]
    private ?string $additionalAddressInformation = null;

    #[Column(type: 'string', length: 190, nullable: true)]
    private ?string $email = null;

    #[Column(type: 'string', length: 30, nullable: true)]
    private ?string $mobilePhoneNumber = null;

    #[Column(type: 'string', length: 30, nullable: true)]
    private ?string $phoneNumber = null;

    #[Column(type: 'string', length: 30, nullable: true)]
    private ?string $faxNumber = null;

    #[Column(type: 'string', length: 255, nullable: true)]
    private ?string $website = null;

    #[Column(type: 'string', length: 190, nullable: true)]
    private ?string $postOfficeBox = null;

    #[Column(type: 'string', length: 20, nullable: true)]
    private ?string $postOfficeBoxPostalCode = null;

    #[Column(type: 'string', length: 190, nullable: true)]
    private ?string $postOfficeBoxPlaceName = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getBuildingUnit(): BuildingUnit
    {
        return $this->buildingUnit;
    }

    public function setBuildingUnit(BuildingUnit $buildingUnit): self
    {
        $this->buildingUnit = $buildingUnit;

        return $this;
    }

    public function getSalutation(): ?Salutation
    {
        return $this->salutation;
    }

    public function setSalutation(?Salutation $salutation): self
    {
        $this->salutation = $salutation;

        return $this;
    }

    public function getSalutationLetter(): ?string
    {
        return $this->salutationLetter;
    }

    public function setSalutationLetter(?string $salutationLetter): self
    {
        $this->salutationLetter = $salutationLetter;

        return $this;
    }

    public function getPersonTitle(): ?string
    {
        return $this->personTitle;
    }

    public function setPersonTitle(?string $personTitle): self
    {
        $this->personTitle = $personTitle;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getCompanyName(): ?string
    {
        return $this->companyName;
    }

    public function setCompanyName(?string $companyName): self
    {
        $this->companyName = $companyName;

        return $this;
    }

    public function getStreetName(): ?string
    {
        return $this->streetName;
    }

    public function setStreetName(?string $streetName): self
    {
        $this->streetName = $streetName;

        return $this;
    }

    public function getHouseNumber(): ?string
    {
        return $this->houseNumber;
    }

    public function setHouseNumber(?string $houseNumber): self
    {
        $this->houseNumber = $houseNumber;

        return $this;
    }

    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }

    public function setPostalCode(?string $postalCode): self
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    public function getPlaceName(): ?string
    {
        return $this->placeName;
    }

    public function setPlaceName(?string $placeName): self
    {
        $this->placeName = $placeName;

        return $this;
    }

    public function getAdditionalAddressInformation(): ?string
    {
        return $this->additionalAddressInformation;
    }

    public function setAdditionalAddressInformation(?string $additionalAddressInformation): self
    {
        $this->additionalAddressInformation = $additionalAddressInformation;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getMobilePhoneNumber(): ?string
    {
        return $this->mobilePhoneNumber;
    }

    public function setMobilePhoneNumber(?string $mobilePhoneNumber): self
    {
        $this->mobilePhoneNumber = $mobilePhoneNumber;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(?string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function getFaxNumber(): ?string
    {
        return $this->faxNumber;
    }

    public function setFaxNumber(?string $faxNumber): self
    {
        $this->faxNumber = $faxNumber;

        return $this;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    public function setWebsite(?string $website): self
    {
        $this->website = $website;

        return $this;
    }

    public function getPostOfficeBox(): ?string
    {
        return $this->postOfficeBox;
    }

    public function setPostOfficeBox(?string $postOfficeBox): self
    {
        $this->postOfficeBox = $postOfficeBox;

        return $this;
    }

    public function getPostOfficeBoxPostalCode(): ?string
    {
        return $this->postOfficeBoxPostalCode;
    }

    public function setPostOfficeBoxPostalCode(?string $postOfficeBoxPostalCode): self
    {
        $this->postOfficeBoxPostalCode = $postOfficeBoxPostalCode;

        return $this;
    }

    public function getPostOfficeBoxPlaceName(): ?string
    {
        return $this->postOfficeBoxPlaceName;
    }

    public function setPostOfficeBoxPlaceName(?string $postOfficeBoxPlaceName): self
    {
        $this->postOfficeBoxPlaceName = $postOfficeBoxPlaceName;

        return $this;
    }
}
