<?php

declare(strict_types=1);

namespace App\Domain\Entity\Property;

enum PropertyMarketingStatus: int
{
    case ACQUISITION_OPEN = 0;
    case DIALOG_WITH_OWNER = 1;
    case MARKETING_PUBLIC = 2;
    case SUCCESSFULLY_MARKETED = 3;
    case ACQUISITION_COMPLETED = 4;
    case RESUBMISSION = 5;
    case MARKETING_INTERNAL = 6;

    public function getName(): string
    {
        return match ($this) {
            self::ACQUISITION_OPEN => 'Erfassung offen',
            self::DIALOG_WITH_OWNER => 'Dialogphase mit Eigentümer',
            self::MARKETING_PUBLIC => 'Vermarktung öffentlich',
            self::SUCCESSFULLY_MARKETED => 'Erfolgreich vermarktet',
            self::ACQUISITION_COMPLETED => 'Erfassung abgeschlossen',
            self::RESUBMISSION => 'Auf Wiedervorlage',
            self::MARKETING_INTERNAL => 'Vermarktung intern'
        };
    }
}
