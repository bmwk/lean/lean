<?php

declare(strict_types=1);

namespace App\Domain\Entity;

enum ParkingLotRequirementType: int
{
    case OPTIONALLY = 0;
    case MANDATORY = 1;

    public function getName(): string
    {
        return match ($this) {
            self::OPTIONALLY => 'optional',
            self::MANDATORY => 'verpflichtend',
        };
    }
}


