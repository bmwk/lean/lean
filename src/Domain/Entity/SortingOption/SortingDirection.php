<?php

declare(strict_types=1);

namespace App\Domain\Entity\SortingOption;

enum SortingDirection: int
{
    case ASCENDING = 0;
    case DESCENDING = 1;

    public function getName(): string
    {
        return match ($this) {
            self::ASCENDING => 'aufsteigend',
            self::DESCENDING => 'absteigend'
        };
    }

    public function getOrderByDirection(): string
    {
        return match ($this) {
            self::ASCENDING => 'ASC',
            self::DESCENDING => 'DESC'
        };
    }
}
