<?php

declare(strict_types=1);

namespace App\Domain\Entity\SortingOption;

use App\Domain\Entity\UrlParameterInterface;
use Symfony\Component\HttpFoundation\Request;

class SortingOption implements UrlParameterInterface
{
    /**
     * @deprecated
     */
    public static function create(string $sortingBy, ?SortingDirection $sortingDirection): self
    {
        return new self(sortingBy: $sortingBy, sortingDirection: $sortingDirection);
    }

    public static function createFromRequest(Request $request): self
    {
        if ($request->query->get(key: 'sortieren') === null) {
            throw new \RuntimeException(message: 'parameter: "sortieren" is missing');
        }

        if ($request->query->get(key: 'richtung') !== null) {
            $sortingDirection = SortingDirection::tryFrom((int) $request->query->get(key: 'richtung'));
        } else {
            $sortingDirection = null;
        }

        return new self(sortingBy: $request->query->get(key: 'sortieren'), sortingDirection: $sortingDirection);
    }


    public function __construct(
        private string $sortingBy,
        private ?SortingDirection $sortingDirection = null
    ) {
    }

    public function getSortingBy(): string
    {
        return $this->sortingBy;
    }

    public function setSortingBy(string $sortingBy): self
    {
        $this->sortingBy = $sortingBy;

        return $this;
    }

    public function getSortingDirection(): ?SortingDirection
    {
        return $this->sortingDirection;
    }

    public function setSortingDirection(?SortingDirection $sortingDirection): self
    {
        $this->sortingDirection = $sortingDirection;

        return $this;
    }

    public function toUrlParameters(): array
    {
        return [
            'sortieren' => $this->sortingBy,
            'richtung'  => $this->sortingDirection?->value,
        ];
    }
}
