<?php

declare(strict_types=1);

namespace App\Domain\Entity;

enum ForwardingResponse: int
{
    case PUT_BACK = 0;
    case REJECTED = 1;
    case CONTACT_ESTABLISHED = 2;
    case CONTRACT_CONCLUDED = 3;

    public function getName(): string
    {
        return match ($this) {
            self::PUT_BACK => 'zurückgestellt',
            self::REJECTED => 'abgelehnt',
            self::CONTACT_ESTABLISHED => 'Kontakt aufgenommen',
            self::CONTRACT_CONCLUDED => 'Vertrag geschlossen',
        };
    }
}
