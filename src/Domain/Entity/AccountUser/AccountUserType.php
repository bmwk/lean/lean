<?php

declare(strict_types=1);

namespace App\Domain\Entity\AccountUser;

enum AccountUserType: int
{
    case INTERNAL = 0;
    case PROPERTY_PROVIDER = 1;
    case PROPERTY_SEEKER = 2;
    case FTP_USER = 3;

    /**
     * @return AccountUserRole[]
     */
    public function getPossibleAccountUserRoles(): array
    {
        return match ($this) {
            self::INTERNAL => [
                AccountUserRole::ROLE_ACCOUNT_ADMIN,
                AccountUserRole::ROLE_USER,
                AccountUserRole::ROLE_PROPERTY_FEEDER,
                AccountUserRole::ROLE_PROPERTY_VACANCY_REPORT_MANAGER,
                AccountUserRole::ROLE_PROPERTY_EXCEL_IMPORT_MANAGER,
                AccountUserRole::ROLE_LOOKING_FOR_PROPERTY_REQUEST_FEEDER,
                AccountUserRole::ROLE_LOOKING_FOR_PROPERTY_REQUEST_REPORT_MANAGER,
                AccountUserRole::ROLE_VIEWER,
                AccountUserRole::ROLE_RESTRICTED_VIEWER,
                AccountUserRole::ROLE_EARLY_WARNING_SYSTEM_VIEWER,
                AccountUserRole::ROLE_CONFIGURATOR,
            ],
            self::PROPERTY_PROVIDER => [
                AccountUserRole::ROLE_PROPERTY_PROVIDER,
                AccountUserRole::ROLE_PROPERTY_SEEKER,
            ],
            self::PROPERTY_SEEKER => [
                AccountUserRole::ROLE_PROPERTY_SEEKER,
            ],
            self::FTP_USER => [],
        };
    }
}
