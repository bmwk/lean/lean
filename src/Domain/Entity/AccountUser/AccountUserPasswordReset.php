<?php

declare(strict_types=1);

namespace App\Domain\Entity\AccountUser;

use App\Domain\Entity\CreatedAtTrait;
use App\Domain\Entity\CreatedByAccountUserTrait;
use App\Domain\Entity\UpdatedAtTrait;
use App\Repository\AccountUser\AccountUserPasswordResetRepository;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\Table;

#[Entity(repositoryClass: AccountUserPasswordResetRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[HasLifecycleCallbacks]
class AccountUserPasswordReset
{
    use CreatedByAccountUserTrait;
    use CreatedAtTrait;
    use UpdatedAtTrait;

    #[Id]
    #[GeneratedValue]
    #[Column(type: 'integer', nullable: false, options: ['unsigned' => true])]
    private ?int $id = null;

    #[OneToOne]
    #[JoinColumn(nullable: false)]
    private AccountUser $accountUser;

    #[Column(type: 'string', length: 255, nullable: false)]
    private string $passwordToken;

    #[Column(type: 'datetime', nullable: false)]
    private \DateTime $expirationDate;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getAccountUser(): AccountUser
    {
        return $this->accountUser;
    }

    public function setAccountUser(AccountUser $accountUser): self
    {
        $this->accountUser = $accountUser;

        return $this;
    }

    public function getPasswordToken(): string
    {
        return $this->passwordToken;
    }

    public function setPasswordToken(string $passwordToken): self
    {
        $this->passwordToken = $passwordToken;

        return $this;
    }

    public function getExpirationDate(): \Datetime
    {
        return $this->expirationDate;
    }

    public function setExpirationDate(\DateTime $expirationDate): self
    {
        $this->expirationDate = $expirationDate;

        return $this;
    }
}
