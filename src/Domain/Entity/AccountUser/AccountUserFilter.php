<?php

declare(strict_types=1);

namespace App\Domain\Entity\AccountUser;

use App\Domain\Entity\Person\PersonType;

class AccountUserFilter
{
    /**
     * @var PersonType[]|null
     */
    private ?array $personTypes = null;

    /**
     * @return PersonType[]|null
     */
    public function getPersonTypes(): ?array
    {
        return $this->personTypes;
    }

    /**
     * @param PersonType[]|null $personTypes
     */
    public function setPersonTypes(?array $personTypes): self
    {
        $this->personTypes = $personTypes;

        return $this;
    }
}
