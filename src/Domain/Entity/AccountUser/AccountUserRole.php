<?php

declare(strict_types=1);

namespace App\Domain\Entity\AccountUser;

enum AccountUserRole: string
{
    case ROLE_ACCOUNT_ADMIN = 'ROLE_ACCOUNT_ADMIN';
    case ROLE_USER = 'ROLE_USER';
    case ROLE_PROPERTY_FEEDER = 'ROLE_PROPERTY_FEEDER';
    case ROLE_PROPERTY_VACANCY_REPORT_MANAGER = 'ROLE_PROPERTY_VACANCY_REPORT_MANAGER';
    case ROLE_PROPERTY_EXCEL_IMPORT_MANAGER = 'ROLE_PROPERTY_EXCEL_IMPORT_MANAGER';
    case ROLE_LOOKING_FOR_PROPERTY_REQUEST_FEEDER = 'ROLE_LOOKING_FOR_PROPERTY_REQUEST_FEEDER';
    case ROLE_LOOKING_FOR_PROPERTY_REQUEST_REPORT_MANAGER = 'ROLE_LOOKING_FOR_PROPERTY_REQUEST_REPORT_MANAGER';
    case ROLE_VIEWER = 'ROLE_VIEWER';
    case ROLE_RESTRICTED_VIEWER = 'ROLE_RESTRICTED_VIEWER';
    case ROLE_EARLY_WARNING_SYSTEM_VIEWER = 'ROLE_EARLY_WARNING_SYSTEM_VIEWER';
    case ROLE_CONFIGURATOR = 'ROLE_CONFIGURATOR';
    case ROLE_PROPERTY_PROVIDER = 'ROLE_PROPERTY_PROVIDER';
    case ROLE_PROPERTY_SEEKER = 'ROLE_PROPERTY_SEEKER';

    public function getName(): string
    {
        return match ($this) {
            self::ROLE_ACCOUNT_ADMIN => 'Administrator:in',
            self::ROLE_USER => 'Standard-Nutzer:in',
            self::ROLE_PROPERTY_FEEDER => 'Objekt-Erfasser:in',
            self::ROLE_PROPERTY_VACANCY_REPORT_MANAGER => 'Verwalter:in von Leerstandsmeldungen',
            self::ROLE_PROPERTY_EXCEL_IMPORT_MANAGER => 'Excel-Importeur:in',
            self::ROLE_LOOKING_FOR_PROPERTY_REQUEST_FEEDER => 'Gesuch-Erfasser:in',
            self::ROLE_LOOKING_FOR_PROPERTY_REQUEST_REPORT_MANAGER => 'Verwalter:in von Gesuchsmeldungen',
            self::ROLE_VIEWER => 'Betrachter:in mit personenbezogenen Daten',
            self::ROLE_RESTRICTED_VIEWER => 'Betrachter:in ohne personenbezogene Daten',
            self::ROLE_EARLY_WARNING_SYSTEM_VIEWER => 'Frühwarnsystem-Betrachter:in',
            self::ROLE_CONFIGURATOR => 'Konfigurator'
        };
    }

    public function getDescription(): string
    {
        return match ($this) {
            self::ROLE_ACCOUNT_ADMIN => 'Adminitrator:innen haben Vollzugriff auf alle Bereiche von LeAn®. Sie können 
                grundlegende Konfigurationen vornehmen und auch andere Sachbearbeiter:innen zur Nutzung von LeAn® 
                einladen und sie administrieren.',
            self::ROLE_USER => 'Diese Rolle hat Lese- und Schreibzugriff auf alle operativen Bereiche von LeAn®. 
                Allerdings kann ein/e Standard-Nutzer:in keine grundlegenden Konfigurationen vornehmen und auch 
                keine anderen kommunalen Nutzer:innen verwalten. Allerdings können mit dieser Rolle sowohl Suchende 
                als auch Anbietende auf die Plattform eingeladen werden.',
            self::ROLE_PROPERTY_FEEDER => 'Objekt-Erfasser:innen können im Bereich Leerstandsmanagement neue 
                Objekte erfassen. Außerdem können Sie alle erfassten Objekte in der Übersicht sehen, aber nur ihnen 
                zugeordnete, bzw. selbst erfasste Objekte bearbeiten und löschen. Dabei haben Sie auch nur Zugriff auf 
                Kontakte, die sie selbst erfasst haben, bzw. die ihnen zugeordnet sind.',
            self::ROLE_PROPERTY_VACANCY_REPORT_MANAGER => 'Ein/e Nutzer:in mit dieser Rolle kann eingehende Leerstandsmeldungen einsehen und bearbeiten.',
            self::ROLE_PROPERTY_EXCEL_IMPORT_MANAGER => 'Diese Rolle kann zusätzlich zu einer bestehenden Rolle zugewiesen werden.
                Nach Zuweisung hat ein:e Nutzer:in die Möglichkeit, den Excel-Import auszuführen.',
            self::ROLE_LOOKING_FOR_PROPERTY_REQUEST_FEEDER => 'Nutzer:innen mit dieser Rolle können Ansiedlungsgesuche erfassen.',
            self::ROLE_LOOKING_FOR_PROPERTY_REQUEST_REPORT_MANAGER => 'Ein/e Nutzer:in mit dieser Rolle kann  eingehende Gesuchssmeldungen einsehen und bearbeiten.',
            self::ROLE_VIEWER => 'Nutzer:innen mit dieser Rolle haben die selben Zugriffsrechte wie Standard-Nutzer:innen, allerdings nur lesenden Zugriff.',
            self::ROLE_RESTRICTED_VIEWER => 'Nutzer:innen dieser Rolle können erfasste Objekte und Ansiedlungsgesuche einsehen, allerdings sind alle personenbezogenen Daten, wie Eigentümer, suchende Unternehmen, etc. nicht sichtbar.',
            self::ROLE_EARLY_WARNING_SYSTEM_VIEWER => 'Diese Rolle kann zusätzlich zu einer bestehenden Rolle zugewiesen werden. Nach Zuweisung hat ein:e Nutzer:in die Möglichkeit, das Frühwarnsystem aufzurufen.',
            self::ROLE_CONFIGURATOR => 'Ein/e Nutzer:in mit dieser Rolle kann LeAn® konfigurieren, hat aber sonst keinen Zugriff auf die Anwendung.'
        };
    }
}
