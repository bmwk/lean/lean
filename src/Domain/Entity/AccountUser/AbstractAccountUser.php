<?php

declare(strict_types=1);

namespace App\Domain\Entity\AccountUser;

use App\Domain\Entity\AccountTrait;
use App\Domain\Entity\AnonymizedTrait;
use App\Domain\Entity\CreatedAtTrait;
use App\Domain\Entity\DeletedTrait;
use App\Domain\Entity\Person\Person;
use App\Domain\Entity\UpdatedAtTrait;
use App\Domain\Entity\WmsLayer;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\OneToOne;

abstract class AbstractAccountUser
{
    use AccountTrait;
    use DeletedTrait;
    use AnonymizedTrait;
    use CreatedAtTrait;
    use UpdatedAtTrait;

    #[Id]
    #[GeneratedValue]
    #[Column(type: Types::INTEGER, nullable: false, options: ['unsigned' => true])]
    protected ?int $id = null;

    #[Column(type: Types::SMALLINT, nullable: false, enumType: AccountUserType::class, options: ['unsigned' => true])]
    protected AccountUserType $accountUserType;

    #[Column(type: Types::STRING, length: 190, nullable: false)]
    protected string $fullName;

    #[Column(type: Types::STRING, length: 100, nullable: true)]
    protected ?string $nameAbbreviation = null;

    #[Column(type: Types::STRING, length: 190, nullable: false)]
    protected string $email;

    #[Column(type: Types::STRING, length: 30, nullable: true)]
    protected ?string $phoneNumber = null;

    #[Column(type: Types::STRING, length: 190,  unique: true, nullable: false)]
    protected string $username;

    #[Column(type: Types::STRING, length: 255, nullable: false)]
    protected string $password;

    #[Column(type: Types::JSON, nullable: false)]
    protected array $roles = [];

    #[Column(type: Types::BOOLEAN, nullable: false)]
    protected bool $enabled;

    #[OneToOne(inversedBy: 'accountUser', fetch: 'LAZY')]
    protected ?Person $person = null;

    #[ManyToMany(targetEntity: WmsLayer::class)]
    protected Collection|array $wmsLayers;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAccountUserType(): AccountUserType
    {
        return $this->accountUserType;
    }

    public function getFullName(): string
    {
        return $this->fullName;
    }

    public function getNameAbbreviation(): ?string
    {
        return $this->nameAbbreviation;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function getRoles(): array
    {
        return $this->roles;
    }

    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    public function getPerson(): ?Person
    {
        return $this->person;
    }

    public function getWmsLayers(): Collection|array
    {
        return $this->wmsLayers;
    }
}
