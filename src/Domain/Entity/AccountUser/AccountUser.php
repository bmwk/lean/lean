<?php

declare(strict_types=1);

namespace App\Domain\Entity\AccountUser;

use App\Domain\Entity\AnonymizedDataBackupInterface;
use App\Domain\Entity\Person\Person;
use App\Domain\Entity\UserRegistration\UserRegistration;
use App\Domain\Entity\UserRegistration\UserRegistrationType;
use App\Repository\AccountUser\AccountUserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Table;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

#[Entity(repositoryClass: AccountUserRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[HasLifecycleCallbacks]
class AccountUser extends AbstractAccountUser implements AnonymizedDataBackupInterface, UserInterface, PasswordAuthenticatedUserInterface
{
    public static function createFromUserRegistration(UserRegistration $userRegistration): self
    {
        $accountUser = new self();

        $accountUser->fullName = $userRegistration->getFirstName() . ' ' . $userRegistration->getLastName();
        $accountUser->username = $userRegistration->getEmail();
        $accountUser->email = $userRegistration->getEmail();

        if ($userRegistration->getUserRegistrationType() === UserRegistrationType::PROVIDER) {
            $accountUser->accountUserType = AccountUserType::PROPERTY_PROVIDER;
            $accountUser->roles = [AccountUserRole::ROLE_PROPERTY_PROVIDER->value];
        } elseif ($userRegistration->getUserRegistrationType() === UserRegistrationType::SEEKER) {
            $accountUser->accountUserType = AccountUserType::PROPERTY_SEEKER;
            $accountUser->roles = [AccountUserRole::ROLE_PROPERTY_SEEKER->value];
        }

        return $accountUser;
    }

    public function __construct()
    {
        $this->wmsLayers = new ArrayCollection();
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function setAccountUserType(AccountUserType $accountUserType): self
    {
        $this->accountUserType = $accountUserType;

        return $this;
    }

    public function setFullName(string $fullName): self
    {
        $this->fullName = $fullName;

        return $this;
    }

    public function setNameAbbreviation(?string $nameAbbreviation): self
    {
        $this->nameAbbreviation = $nameAbbreviation;

        return $this;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function setPhoneNumber(?string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    public function setPerson(?Person $person): self
    {
        $this->person = $person;

        return $this;
    }

    public function setWmsLayers(Collection|array $wmsLayers): self
    {
        $this->wmsLayers = $wmsLayers;

        return $this;
    }

    public function eraseCredentials(): void
    {
    }

    public function getUserIdentifier(): string
    {
        return $this->username;
    }

    public function getAnonymizableDataToBackup(): array
    {
        return [
            'fullName'         => $this->fullName,
            'nameAbbreviation' => $this->nameAbbreviation,
            'email'            => $this->email,
            'phoneNumber'      => $this->phoneNumber,
            'username'         => $this->username,
            'password'         => $this->password,
        ];
    }
}
