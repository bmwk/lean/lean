<?php

declare(strict_types=1);

namespace App\Domain\Entity;

use App\Repository\EliminationCriteriaRepository;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\Table;

#[Entity(repositoryClass: EliminationCriteriaRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
class EliminationCriteria
{
    #[Id]
    #[GeneratedValue]
    #[Column(type: 'integer', nullable: false, options: ['unsigned' => true])]
    private ?int $id = null;

    #[OneToOne(fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private EliminationCriterion $inPlace;

    #[OneToOne(fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private EliminationCriterion $inQuarter;

    #[OneToOne(fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private EliminationCriterion $commissionable;

    #[OneToOne(fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private EliminationCriterion $propertyOfferTypes;

    #[OneToOne(fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private EliminationCriterion $maximumPrice;

    #[OneToOne(fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private EliminationCriterion $minimumPrice;

    #[OneToOne(fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private EliminationCriterion $roofing;

    #[OneToOne(fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private EliminationCriterion $outdoorSpace;

    #[OneToOne(fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private EliminationCriterion $showroom;

    #[OneToOne(fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private EliminationCriterion $shopWindow;

    #[OneToOne(fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private EliminationCriterion $groundLevelSalesArea;

    #[OneToOne(fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private EliminationCriterion $barrierFreeAccess;

    #[OneToOne(fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private EliminationCriterion $minimumSpace;

    #[OneToOne(fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private EliminationCriterion $maximumSpace;

    #[OneToOne(fetch: 'EAGER')]
    #[JoinColumn(nullable: true)]
    private ?EliminationCriterion $industryClassification = null;

    public function isEliminated(): bool
    {
        if ($this->inPlace->isSet() && !$this->inPlace->isEvaluationResult()) {
            return true;
        }

        if ($this->inQuarter->isSet() && !$this->inQuarter->isEvaluationResult()) {
            return true;
        }

        if ($this->commissionable->isSet() && !$this->commissionable->isEvaluationResult()) {
            return true;
        }

        if ($this->propertyOfferTypes->isSet() && !$this->propertyOfferTypes->isEvaluationResult()) {
            return true;
        }

        if ($this->maximumPrice->isSet() && !$this->maximumPrice->isEvaluationResult()) {
            return true;
        }

        if ($this->minimumPrice->isSet() && !$this->minimumPrice->isEvaluationResult()) {
            return true;
        }

        if ($this->roofing->isSet() && !$this->roofing->isEvaluationResult()) {
            return true;
        }

        if ($this->outdoorSpace->isSet() && !$this->outdoorSpace->isEvaluationResult()) {
            return true;
        }

        if ($this->showroom->isSet() && !$this->showroom->isEvaluationResult()) {
            return true;
        }

        if ($this->shopWindow->isSet() && !$this->shopWindow->isEvaluationResult()) {
            return true;
        }

        if ($this->groundLevelSalesArea->isSet() && !$this->groundLevelSalesArea->isEvaluationResult()) {
            return true;
        }

        if ($this->barrierFreeAccess->isSet() && !$this->barrierFreeAccess->isEvaluationResult()) {
            return true;
        }

        if ($this->minimumSpace->isSet() && !$this->minimumSpace->isEvaluationResult()) {
            return true;
        }

        if ($this->maximumSpace->isSet() && !$this->maximumSpace->isEvaluationResult()) {
            return true;
        }

        if ($this->industryClassification->isSet() && !$this->industryClassification->isEvaluationResult()) {
            return true;
        }

        return false;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getInPlace(): EliminationCriterion
    {
        return $this->inPlace;
    }

    public function setInPlace(EliminationCriterion $inPlace): self
    {
        $this->inPlace = $inPlace;

        return $this;
    }

    public function getInQuarter(): EliminationCriterion
    {
        return $this->inQuarter;
    }

    public function setInQuarter(EliminationCriterion $inQuarter): self
    {
        $this->inQuarter = $inQuarter;

        return $this;
    }

    public function getCommissionable(): EliminationCriterion
    {
        return $this->commissionable;
    }

    public function setCommissionable(EliminationCriterion $commissionable): self
    {
        $this->commissionable = $commissionable;

        return $this;
    }

    public function getPropertyOfferTypes(): EliminationCriterion
    {
        return $this->propertyOfferTypes;
    }

    public function setPropertyOfferTypes(EliminationCriterion $propertyOfferTypes): self
    {
        $this->propertyOfferTypes = $propertyOfferTypes;

        return $this;
    }

    public function getMaximumPrice(): EliminationCriterion
    {
        return $this->maximumPrice;
    }

    public function setMaximumPrice(EliminationCriterion $maximumPrice): self
    {
        $this->maximumPrice = $maximumPrice;

        return $this;
    }

    public function getMinimumPrice(): EliminationCriterion
    {
        return $this->minimumPrice;
    }

    public function setMinimumPrice(EliminationCriterion $minimumPrice): self
    {
        $this->minimumPrice = $minimumPrice;

        return $this;
    }

    public function getRoofing(): EliminationCriterion
    {
        return $this->roofing;
    }

    public function setRoofing(EliminationCriterion $roofing): self
    {
        $this->roofing = $roofing;

        return $this;
    }

    public function getOutdoorSpace(): EliminationCriterion
    {
        return $this->outdoorSpace;
    }

    public function setOutdoorSpace(EliminationCriterion $outdoorSpace): self
    {
        $this->outdoorSpace = $outdoorSpace;

        return $this;
    }

    public function getShowroom(): EliminationCriterion
    {
        return $this->showroom;
    }

    public function setShowroom(EliminationCriterion $showroom): self
    {
        $this->showroom = $showroom;

        return $this;
    }

    public function getShopWindow(): EliminationCriterion
    {
        return $this->shopWindow;
    }

    public function setShopWindow(EliminationCriterion $shopWindow): self
    {
        $this->shopWindow = $shopWindow;

        return $this;
    }

    public function getGroundLevelSalesArea(): EliminationCriterion
    {
        return $this->groundLevelSalesArea;
    }

    public function setGroundLevelSalesArea(EliminationCriterion $groundLevelSalesArea): self
    {
        $this->groundLevelSalesArea = $groundLevelSalesArea;

        return $this;
    }

    public function getBarrierFreeAccess(): EliminationCriterion
    {
        return $this->barrierFreeAccess;
    }

    public function setBarrierFreeAccess(EliminationCriterion $barrierFreeAccess): self
    {
        $this->barrierFreeAccess = $barrierFreeAccess;

        return $this;
    }

    public function getMinimumSpace(): EliminationCriterion
    {
        return $this->minimumSpace;
    }

    public function setMinimumSpace(EliminationCriterion $minimumSpace): self
    {
        $this->minimumSpace = $minimumSpace;

        return $this;
    }

    public function getMaximumSpace(): EliminationCriterion
    {
        return $this->maximumSpace;
    }

    public function setMaximumSpace(EliminationCriterion $maximumSpace): self
    {
        $this->maximumSpace = $maximumSpace;

        return $this;
    }

    public function getIndustryClassification(): ?EliminationCriterion
    {
        return $this->industryClassification;
    }

    public function setIndustryClassification(?EliminationCriterion $industryClassification): self
    {
        $this->industryClassification = $industryClassification;

        return $this;
    }
}
