<?php

declare(strict_types=1);

namespace App\Domain\Entity;

use App\Repository\ScoreCriterionRepository;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;

#[Entity(repositoryClass: ScoreCriterionRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
class ScoreCriterion implements ScoreCriterionInterface
{
    #[Id]
    #[GeneratedValue]
    #[Column(type: 'integer', nullable: false, options: ['unsigned' => true])]
    private ?int $id = null;

    #[Column(type: 'string', nullable: false)]
    private string $name;

    #[Column(name: '`set`', type: 'boolean', nullable: false)]
    private bool $set = false;

    #[Column(type: 'boolean', nullable: false)]
    private bool $evaluationResult = false;

    #[Column(type: 'integer', nullable: false, options: ['unsigned' => true])]
    private int $points;

    #[Column(type: 'integer', nullable: true, options: ['unsigned' => true])]
    private ?int $pointPenalty = null;

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function isSet(): bool
    {
        return $this->set;
    }

    public function setSet(bool $set): self
    {
        $this->set = $set;
        return $this;
    }

    public function isEvaluationResult(): bool
    {
        return $this->evaluationResult;
    }

    public function setEvaluationResult(bool $evaluationResult): self
    {
        $this->evaluationResult = $evaluationResult;
        return $this;
    }

    public function getPoints(): int
    {
        return $this->points;
    }

    public function setPoints(int $points): self
    {
        $this->points = $points;
        return $this;
    }

    public function getPointPenalty(): ?int
    {
        return $this->pointPenalty;
    }

    public function setPointPenalty(?int $pointPenalty): self
    {
        $this->pointPenalty = $pointPenalty;
        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getMaxPoints(): int
    {
        return $this->points;
    }
}