<?php

declare(strict_types=1);

namespace App\Domain\Entity;

use App\Repository\Property\BuildingRepository;
use Doctrine\Common\Util\ClassUtils;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\UniqueConstraint;

#[Entity(repositoryClass: BuildingRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[UniqueConstraint(name: 'anonymized_data_class_object_id', columns: ['anonymized_data_class', 'object_id'])]
#[HasLifecycleCallbacks]
class AnonymizedDataBackup
{
    use CreatedAtTrait;

    #[Id]
    #[GeneratedValue]
    #[Column(type: 'integer', nullable: false, options: ['unsigned' => true])]
    private ?int $id = null;

    #[Column(type: 'smallint', nullable: false, enumType: AnonymizedDataClass::class, options: ['unsigned' => true])]
    private AnonymizedDataClass $anonymizedDataClass;

    #[Column(type: 'integer', nullable: false, options: ['unsigned' => true])]
    private int $objectId;

    #[Column(type: 'json', nullable: false)]
    private array $backup;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getAnonymizedDataClass(): AnonymizedDataClass
    {
        return $this->anonymizedDataClass;
    }

    public function setAnonymizedDataClass(AnonymizedDataClass $anonymizedDataClass): self
    {
        $this->anonymizedDataClass = $anonymizedDataClass;

        return $this;
    }

    public function getObjectId(): int
    {
        return $this->objectId;
    }

    public function setObjectId(int $objectId): self
    {
        $this->objectId = $objectId;

        return $this;
    }

    public function getBackup(): array
    {
        return $this->backup;
    }

    public function setBackup(array $backup): self
    {
        $this->backup = $backup;

        return $this;
    }

    public static function createFromObject(AnonymizedDataBackupInterface $object): AnonymizedDataBackup
    {
        if ($object->getId() === null) {
            throw new \RuntimeException();
        }

        $anonymizedDataBackup = new self();

        $anonymizedDataBackup->anonymizedDataClass = AnonymizedDataClass::fromClassName(ClassUtils::getClass($object));
        $anonymizedDataBackup->objectId = $object->getId();
        $anonymizedDataBackup->backup = $object->getAnonymizableDataToBackup();

        return $anonymizedDataBackup;
    }
}
