<?php

declare(strict_types=1);

namespace App\Domain\Entity\PropertyVacancyReport;

class PropertyVacancyReportFilter
{
    private ?array $industryClassifications = null;

    public function getIndustryClassifications(): ?array
    {
        return $this->industryClassifications;
    }

    public function setIndustryClassifications(?array $industryClassifications): self
    {
        $this->industryClassifications = $industryClassifications;

        return $this;
    }
}
