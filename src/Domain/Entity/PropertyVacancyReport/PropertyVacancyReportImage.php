<?php

declare(strict_types=1);

namespace App\Domain\Entity\PropertyVacancyReport;

use App\Domain\Entity\AccountTrait;
use App\Domain\Entity\CreatedAtTrait;
use App\Domain\Entity\DeletedTrait;
use App\Domain\Entity\File;
use App\Domain\Entity\UpdatedAtTrait;
use App\Repository\PropertyVacancyReport\PropertyVacancyReportImageRepository;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\Table;

#[Entity(repositoryClass: PropertyVacancyReportImageRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[HasLifecycleCallbacks]
class PropertyVacancyReportImage
{
    use AccountTrait;
    use DeletedTrait;
    use CreatedAtTrait;
    use UpdatedAtTrait;

    #[Id]
    #[GeneratedValue]
    #[Column(type: 'integer', nullable: false, options: ['unsigned' => true])]
    private ?int $id = null;

    #[OneToOne]
    #[JoinColumn(nullable: false)]
    private File $file;

    #[OneToOne]
    #[JoinColumn(nullable: true)]
    private ?File $thumbnailFile = null;

    #[ManyToOne(inversedBy: 'propertyVacancyReportImages')]
    #[JoinColumn(nullable: false)]
    private PropertyVacancyReport $propertyVacancyReport;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getFile(): File
    {
        return $this->file;
    }

    public function setFile(File $file): self
    {
        $this->file = $file;

        return $this;
    }

    public function getThumbnailFile(): ?File
    {
        return $this->thumbnailFile;
    }

    public function setThumbnailFile(?File $thumbnailFile): self
    {
        $this->thumbnailFile = $thumbnailFile;

        return $this;
    }

    public function getPropertyVacancyReport(): PropertyVacancyReport
    {
        return $this->propertyVacancyReport;
    }

    public function setPropertyVacancyReport(PropertyVacancyReport $propertyVacancyReport): self
    {
        $this->propertyVacancyReport = $propertyVacancyReport;

        return $this;
    }
}
