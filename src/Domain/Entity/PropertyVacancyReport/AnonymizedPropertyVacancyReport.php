<?php

declare(strict_types=1);

namespace App\Domain\Entity\PropertyVacancyReport;

class AnonymizedPropertyVacancyReport extends AbstractPropertyVacancyReport
{
    public function __construct(PropertyVacancyReport $propertyVacancyReport)
    {
        parent::__construct();

        $this->id = $propertyVacancyReport->getId();
        $this->uuid = $propertyVacancyReport->getUuid();
        $this->reporterSalutation = null;
        $this->reporterName = self::createRandomString(length: 15);
        $this->reporterFirstName = null;
        $this->reporterEmail = self::createRandomString(length: 15);
        $this->reporterPhoneNumber = null;
        $this->reporterAlsoPropertyOwner = $propertyVacancyReport->isReporterAlsoPropertyOwner();
        $this->objectIsEmpty = $propertyVacancyReport->isObjectIsEmpty();
        $this->objectIsEmptySince = $propertyVacancyReport->getObjectIsEmptySince();
        $this->objectBecomesEmpty = $propertyVacancyReport->isObjectBecomesEmpty();
        $this->objectBecomesEmptyFrom = $propertyVacancyReport->getObjectBecomesEmptyFrom();
        $this->industryClassification = $propertyVacancyReport->getIndustryClassification();
        $this->propertyUserCompanyName = null;
        $this->buildingCondition = $propertyVacancyReport->getBuildingCondition();
        $this->areaSize = $propertyVacancyReport->getAreaSize();
        $this->plotSize = $propertyVacancyReport->getPlotSize();
        $this->retailSpace = $propertyVacancyReport->getRetailSpace();
        $this->gastronomySpace = $propertyVacancyReport->getGastronomySpace();
        $this->livingSpace = $propertyVacancyReport->getLivingSpace();
        $this->usableSpace = $propertyVacancyReport->getUsableSpace();
        $this->subsidiarySpace = $propertyVacancyReport->getSubsidiarySpace();
        $this->usableOutdoorAreaPossibility = $propertyVacancyReport->getUsableOutdoorAreaPossibility();
        $this->usableOutdoorArea = $propertyVacancyReport->getUsableOutdoorArea();
        $this->shopWindowFrontWidth = $propertyVacancyReport->getShopWindowFrontWidth();
        $this->shopWidth = $propertyVacancyReport->getShopWidth();
        $this->groundLevelSalesArea = $propertyVacancyReport->getGroundLevelSalesArea();
        $this->storeWindowAvailable = $propertyVacancyReport->isStoreWindowAvailable();
        $this->numberOfParkingLots = $propertyVacancyReport->getNumberOfParkingLots();
        $this->barrierFreeAccess = $propertyVacancyReport->getBarrierFreeAccess();
        $this->locationCategory = $propertyVacancyReport->getLocationCategory();
        $this->locationFactors = $propertyVacancyReport->getLocationFactors();
        $this->propertyOfferType = $propertyVacancyReport->getPropertyOfferType();
        $this->propertyDescription = $propertyVacancyReport->getPropertyDescription();
        $this->place = $propertyVacancyReport->getPlace();
        $this->postalCode = $propertyVacancyReport->getPostalCode();
        $this->streetName = $propertyVacancyReport->getStreetName();
        $this->houseNumber = $propertyVacancyReport->getHouseNumber();
        $this->boroughPlace = $propertyVacancyReport->getBoroughPlace();
        $this->quarterPlace = $propertyVacancyReport->getQuarterPlace();
        $this->neighbourhoodPlace = $propertyVacancyReport->getNeighbourhoodPlace();
        $this->placeDescription = $propertyVacancyReport->getPlaceDescription();
        $this->reportedAt = $propertyVacancyReport->getReportedAt();
        $this->processed = $propertyVacancyReport->isProcessed();
        $this->processedAt = $propertyVacancyReport->getProcessedAt();
        $this->admitted = $propertyVacancyReport->isAdmitted();
        $this->rejected = $propertyVacancyReport->isRejected();
        $this->confirmationToken = $propertyVacancyReport->getConfirmationToken();
        $this->emailConfirmed = $propertyVacancyReport->isEmailConfirmed();
        $this->emailConfirmedAt = $propertyVacancyReport->getEmailConfirmedAt();
        $this->buildingUnit = $propertyVacancyReport->getBuildingUnit();
        $this->processedByAccountUser = $propertyVacancyReport->getProcessedByAccountUser();
        $this->propertyVacancyReportImages = $propertyVacancyReport->getPropertyVacancyReportImages();
        $this->propertyVacancyReportNotes = $propertyVacancyReport->getPropertyVacancyReportNotes();
    }

    private static function createRandomString(int $length): string
    {
        return substr(string: bin2hex(string: random_bytes(length: $length)), offset: 0, length: $length);
    }
}
