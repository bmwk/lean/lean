<?php

declare(strict_types=1);

namespace App\Domain\Entity\PropertyVacancyReport;

use App\Domain\Entity\AccountTrait;
use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\AnonymizedDataBackupInterface;
use App\Domain\Entity\AnonymizedTrait;
use App\Domain\Entity\Classification\IndustryClassification;
use App\Domain\Entity\CreatedAtTrait;
use App\Domain\Entity\DeletedTrait;
use App\Domain\Entity\LocationCategory;
use App\Domain\Entity\Person\Salutation;
use App\Domain\Entity\Place;
use App\Domain\Entity\Property\BarrierFreeAccess;
use App\Domain\Entity\Property\BuildingCondition;
use App\Domain\Entity\Property\BuildingUnit;
use App\Domain\Entity\Property\UsableOutdoorAreaPossibility;
use App\Domain\Entity\PropertyOfferType;
use App\Domain\Entity\UpdatedAtTrait;
use App\Repository\PropertyVacancyReport\PropertyVacancyReportRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Table;
use Symfony\Component\Uid\Uuid;

#[Entity(repositoryClass: PropertyVacancyReportRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[HasLifecycleCallbacks]
class PropertyVacancyReport extends AbstractPropertyVacancyReport implements AnonymizedDataBackupInterface
{
    use AccountTrait;
    use DeletedTrait;
    use AnonymizedTrait;
    use CreatedAtTrait;
    use UpdatedAtTrait;

    public function __construct()
    {
        parent::__construct();
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function setUuid(Uuid $uuid): self
    {
        $this->uuid = $uuid;

        return $this;
    }

    public function setReporterCompanyName(?string $reporterCompanyName): self
    {
        $this->reporterCompanyName = $reporterCompanyName;

        return $this;
    }

    public function setReporterSalutation(?Salutation $reporterSalutation): self
    {
        $this->reporterSalutation = $reporterSalutation;

        return $this;
    }

    public function setReporterName(string $reporterName): self
    {
        $this->reporterName = $reporterName;

        return $this;
    }

    public function setReporterFirstName(?string $reporterFirstName): self
    {
        $this->reporterFirstName = $reporterFirstName;

        return $this;
    }

    public function setReporterEmail(string $reporterEmail): self
    {
        $this->reporterEmail = $reporterEmail;

        return $this;
    }

    public function setReporterPhoneNumber(?string $reporterPhoneNumber): self
    {
        $this->reporterPhoneNumber = $reporterPhoneNumber;

        return $this;
    }

    public function setReporterAlsoPropertyOwner(bool $reporterAlsoPropertyOwner): self
    {
        $this->reporterAlsoPropertyOwner = $reporterAlsoPropertyOwner;

        return $this;
    }

    public function setObjectIsEmpty(bool $objectIsEmpty): self
    {
        $this->objectIsEmpty = $objectIsEmpty;

        return $this;
    }

    public function setObjectIsEmptySince(?\DateTime $objectIsEmptySince): self
    {
        $this->objectIsEmptySince = $objectIsEmptySince;

        return $this;
    }

    public function setObjectBecomesEmpty(bool $objectBecomesEmpty): self
    {
        $this->objectBecomesEmpty = $objectBecomesEmpty;

        return $this;
    }

    public function setObjectBecomesEmptyFrom(?\DateTime $objectBecomesEmptyFrom): self
    {
        $this->objectBecomesEmptyFrom = $objectBecomesEmptyFrom;

        return $this;
    }

    public function setIndustryClassification(?IndustryClassification $industryClassification): self
    {
        $this->industryClassification = $industryClassification;

        return $this;
    }

    public function setPropertyUserCompanyName(?string $propertyUserCompanyName): self
    {
        $this->propertyUserCompanyName = $propertyUserCompanyName;

        return $this;
    }

    public function setBuildingCondition(?BuildingCondition $buildingCondition): self
    {
        $this->buildingCondition = $buildingCondition;

        return $this;
    }

    public function setAreaSize(?float $areaSize): self
    {
        $this->areaSize = $areaSize;

        return $this;
    }

    public function setPlotSize(?float $plotSize): self
    {
        $this->plotSize = $plotSize;

        return $this;
    }

    public function setRetailSpace(?float $retailSpace): self
    {
        $this->retailSpace = $retailSpace;

        return $this;
    }

    public function setGastronomySpace(?float $gastronomySpace): self
    {
        $this->gastronomySpace = $gastronomySpace;

        return $this;
    }

    public function setLivingSpace(?float $livingSpace): self
    {
        $this->livingSpace = $livingSpace;

        return $this;
    }

    public function setUsableSpace(?float $usableSpace): self
    {
        $this->usableSpace = $usableSpace;

        return $this;
    }

    public function setSubsidiarySpace(?float $subsidiarySpace): self
    {
        $this->subsidiarySpace = $subsidiarySpace;

        return $this;
    }

    public function setUsableOutdoorAreaPossibility(?UsableOutdoorAreaPossibility $usableOutdoorAreaPossibility): self
    {
        $this->usableOutdoorAreaPossibility = $usableOutdoorAreaPossibility;

        return $this;
    }

    public function setUsableOutdoorArea(?float $usableOutdoorArea): self
    {
        $this->usableOutdoorArea = $usableOutdoorArea;

        return $this;
    }

    public function setShopWindowFrontWidth(?float $shopWindowFrontWidth): self
    {
        $this->shopWindowFrontWidth = $shopWindowFrontWidth;

        return $this;
    }

    public function setShopWidth(?float $shopWidth): self
    {
        $this->shopWidth = $shopWidth;

        return $this;
    }

    public function setGroundLevelSalesArea(?bool $groundLevelSalesArea): self
    {
        $this->groundLevelSalesArea = $groundLevelSalesArea;

        return $this;
    }

    public function setStoreWindowAvailable(bool $storeWindowAvailable): self
    {
        $this->storeWindowAvailable = $storeWindowAvailable;

        return $this;
    }

    public function setNumberOfParkingLots(?int $numberOfParkingLots): self
    {
        $this->numberOfParkingLots = $numberOfParkingLots;

        return $this;
    }

    public function setBarrierFreeAccess(?BarrierFreeAccess $barrierFreeAccess): self
    {
        $this->barrierFreeAccess = $barrierFreeAccess;

        return $this;
    }

    public function setLocationCategory(?LocationCategory $locationCategory): self
    {
        $this->locationCategory = $locationCategory;

        return $this;
    }

    public function setLocationFactors(?array $locationFactors): self
    {
        $this->locationFactors = $locationFactors;

        return $this;
    }

    public function setPropertyOfferType(?PropertyOfferType $propertyOfferType): self
    {
        $this->propertyOfferType = $propertyOfferType;

        return $this;
    }

    public function setPropertyDescription(?string $propertyDescription): self
    {
        $this->propertyDescription = $propertyDescription;

        return $this;
    }

    public function setPlace(Place $place): self
    {
        $this->place = $place;

        return $this;
    }

    public function setPostalCode(string $postalCode): self
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    public function setStreetName(string $streetName): self
    {
        $this->streetName = $streetName;

        return $this;
    }

    public function setHouseNumber(string $houseNumber): self
    {
        $this->houseNumber = $houseNumber;

        return $this;
    }

    public function setBoroughPlace(?Place $boroughPlace): self
    {
        $this->boroughPlace = $boroughPlace;

        return $this;
    }

    public function setQuarterPlace(?Place $quarterPlace): self
    {
        $this->quarterPlace = $quarterPlace;

        return $this;
    }

    public function setNeighbourhoodPlace(?Place $neighbourhoodPlace): self
    {
        $this->neighbourhoodPlace = $neighbourhoodPlace;

        return $this;
    }

    public function setPlaceDescription(?string $placeDescription): self
    {
        $this->placeDescription = $placeDescription;

        return $this;
    }

    public function setReportedAt(\DateTimeImmutable $reportedAt): self
    {
        $this->reportedAt = $reportedAt;

        return $this;
    }

    public function setProcessed(bool $processed): self
    {
        $this->processed = $processed;

        return $this;
    }

    public function setProcessedAt(?\DateTimeImmutable $processedAt): self
    {
        $this->processedAt = $processedAt;

        return $this;
    }

    public function setAdmitted(bool $admitted): self
    {
        $this->admitted = $admitted;

        return $this;
    }

    public function setRejected(bool $rejected): self
    {
        $this->rejected = $rejected;

        return $this;
    }

    public function setConfirmationToken(Uuid $confirmationToken): self
    {
        $this->confirmationToken = $confirmationToken;

        return $this;
    }

    public function setEmailConfirmed(bool $emailConfirmed): self
    {
        $this->emailConfirmed = $emailConfirmed;

        return $this;
    }

    public function setEmailConfirmedAt(?\DateTimeImmutable $emailConfirmedAt): self
    {
        $this->emailConfirmedAt = $emailConfirmedAt;

        return $this;
    }

    public function setBuildingUnit(?BuildingUnit $buildingUnit): self
    {
        $this->buildingUnit = $buildingUnit;

        return $this;
    }

    public function setProcessedByAccountUser(?AccountUser $processedByAccountUser): self
    {
        $this->processedByAccountUser = $processedByAccountUser;

        return $this;
    }

    public function setPropertyVacancyReportImages(Collection|array $propertyVacancyReportImages): self
    {
        $this->propertyVacancyReportImages = $propertyVacancyReportImages;

        return $this;
    }

    public function setPropertyVacancyReportNotes(Collection|array $propertyVacancyReportNotes): self
    {
        $this->propertyVacancyReportNotes = $propertyVacancyReportNotes;

        return $this;
    }

    public function getAnonymizableDataToBackup(): array
    {
        return [
            'reporterSalutation'      => $this->reporterSalutation,
            'reporterName'            => $this->reporterName,
            'reporterFirstName'       => $this->reporterFirstName,
            'reporterEmail'           => $this->reporterEmail,
            'reporterPhoneNumber'     => $this->reporterPhoneNumber,
            'propertyUserCompanyName' => $this->propertyUserCompanyName,
        ];
    }
}
