<?php

declare(strict_types=1);

namespace App\Domain\Entity\PropertyVacancyReport;

use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\Classification\IndustryClassification;
use App\Domain\Entity\LocationCategory;
use App\Domain\Entity\Person\Salutation;
use App\Domain\Entity\Place;
use App\Domain\Entity\Property\BarrierFreeAccess;
use App\Domain\Entity\Property\BuildingCondition;
use App\Domain\Entity\Property\BuildingUnit;
use App\Domain\Entity\Property\UsableOutdoorAreaPossibility;
use App\Domain\Entity\PropertyOfferType;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\OneToOne;
use Symfony\Component\Uid\Uuid;

abstract class AbstractPropertyVacancyReport
{
    #[Id]
    #[GeneratedValue]
    #[Column(type: 'integer', nullable: false, options: ['unsigned' => true])]
    protected ?int $id = null;

    #[Column(type: 'uuid', unique: true, nullable: false)]
    protected Uuid $uuid;

    #[Column(type: 'string', length: 190, nullable: true)]
    protected ?string $reporterCompanyName = null;

    #[Column(type: 'smallint', nullable: true, enumType: Salutation::class, options: ['unsigned' => true])]
    protected ?Salutation $reporterSalutation = null;

    #[Column(type: 'string', length: 190, nullable: false)]
    protected string $reporterName;

    #[Column(type: 'string', length: 190, nullable: true)]
    protected ?string $reporterFirstName = null;

    #[Column(type: 'string', length: 190, nullable: true)]
    protected ?string $reporterEmail = null;

    #[Column(type: 'string', length: 30, nullable: true)]
    protected ?string $reporterPhoneNumber = null;

    #[Column(type: 'boolean', nullable: false)]
    protected bool $reporterAlsoPropertyOwner;

    #[Column(type: 'boolean', nullable: false)]
    protected bool $objectIsEmpty;

    #[Column(type: 'date', nullable: true)]
    protected ?\DateTime $objectIsEmptySince = null;

    #[Column(type: 'boolean', nullable: false)]
    protected bool $objectBecomesEmpty;

    #[Column(type: 'date', nullable: true)]
    protected ?\DateTime $objectBecomesEmptyFrom = null;

    #[ManyToOne]
    #[JoinColumn(nullable: true)]
    protected ?IndustryClassification $industryClassification = null;

    #[Column(type: 'string', length: 190, nullable: true)]
    protected ?string $propertyUserCompanyName = null;

    #[Column(type: 'smallint', nullable: true, enumType: BuildingCondition::class, options: ['unsigned' => true])]
    protected ?BuildingCondition $buildingCondition = null;

    #[Column(type: 'float', nullable: true, options: ['unsigned' => true])]
    protected ?float $areaSize = null;

    #[Column(type: 'float', nullable: true, options: ['unsigned' => true])]
    protected ?float $plotSize = null;

    #[Column(type: 'float', nullable: true, options: ['unsigned' => true])]
    protected ?float $retailSpace = null;

    #[Column(type: 'float', nullable: true, options: ['unsigned' => true])]
    protected ?float $gastronomySpace = null;

    #[Column(type: 'float', nullable: true, options: ['unsigned' => true])]
    protected ?float $livingSpace = null;

    #[Column(type: 'float', nullable: true, options: ['unsigned' => true])]
    protected ?float $usableSpace = null;

    #[Column(type: 'float', nullable: true, options: ['unsigned' => true])]
    protected ?float $subsidiarySpace = null;

    #[Column(type: 'smallint', nullable: true, enumType: UsableOutdoorAreaPossibility::class, options: ['unsigned' => true])]
    protected ?UsableOutdoorAreaPossibility $usableOutdoorAreaPossibility = null;

    #[Column(type: 'float', nullable: true, options: ['unsigned' => true])]
    protected ?float $usableOutdoorArea = null;

    #[Column(type: 'float', nullable: true, options: ['unsigned' => true])]
    protected ?float $shopWindowFrontWidth = null;

    #[Column(type: 'float', nullable: true, options: ['unsigned' => true])]
    protected ?float $shopWidth = null;

    #[Column(type: 'boolean', nullable: true)]
    protected ?bool $groundLevelSalesArea = null;

    #[Column(type: 'boolean', nullable: false)]
    protected bool $storeWindowAvailable;

    #[Column(type: 'integer', nullable: true, options: ['unsigned' => true])]
    protected ?int $numberOfParkingLots = null;

    #[Column(type: 'smallint', nullable: true, enumType: BarrierFreeAccess::class, options: ['unsigned' => true])]
    protected ?BarrierFreeAccess $barrierFreeAccess = null;

    #[Column(type: 'smallint', nullable: true, enumType: LocationCategory::class, options: ['unsigned' => true])]
    protected ?LocationCategory $locationCategory = null;

    #[Column(type: 'json', nullable: true)]
    protected ?array $locationFactors = null;

    #[Column(type: 'smallint', nullable: true, enumType: PropertyOfferType::class, options: ['unsigned' => true])]
    protected ?PropertyOfferType $propertyOfferType = null;

    #[Column(type: 'text', nullable: true)]
    protected ?string $propertyDescription = null;

    #[ManyToOne]
    #[JoinColumn(nullable: false)]
    protected Place $place;

    #[Column(type: 'string', length: 20, nullable: false)]
    protected string $postalCode;

    #[Column(type: 'string', length: 190, nullable: false)]
    protected string $streetName;

    #[Column(type: 'string', length: 20, nullable: false)]
    protected string $houseNumber;

    #[ManyToOne]
    #[JoinColumn(nullable: true)]
    protected ?Place $boroughPlace = null;

    #[ManyToOne]
    #[JoinColumn(nullable: true)]
    protected ?Place $quarterPlace = null;

    #[ManyToOne]
    #[JoinColumn(nullable: true)]
    protected ?Place $neighbourhoodPlace = null;

    #[Column(type: 'text', nullable: true)]
    protected ?string $placeDescription = null;

    #[Column(type: 'datetime_immutable', nullable: false)]
    protected \DateTimeImmutable $reportedAt;

    #[Column(type: 'boolean', nullable: false)]
    protected bool $processed;

    #[Column(type: 'datetime_immutable', nullable: true)]
    protected ?\DateTimeImmutable $processedAt = null;

    #[Column(type: 'boolean', nullable: false)]
    protected bool $admitted;

    #[Column(type: 'boolean', nullable: false)]
    protected bool $rejected;

    #[Column(type: 'uuid', unique:true, nullable: false)]
    protected Uuid $confirmationToken;

    #[Column(type: 'boolean', nullable: false)]
    protected bool $emailConfirmed;

    #[Column(type: 'datetime_immutable', nullable: true)]
    protected ?\DateTimeImmutable $emailConfirmedAt = null;

    #[OneToOne(inversedBy: 'propertyVacancyReport')]
    #[JoinColumn(nullable: true)]
    protected ?BuildingUnit $buildingUnit = null;

    #[ManyToOne]
    #[JoinColumn(nullable: true)]
    protected ?AccountUser $processedByAccountUser = null;

    #[OneToMany(mappedBy: 'propertyVacancyReport', targetEntity: PropertyVacancyReportImage::class)]
    protected Collection|array $propertyVacancyReportImages;

    #[OneToMany(mappedBy: 'propertyVacancyReport', targetEntity: PropertyVacancyReportNote::class)]
    protected Collection|array $propertyVacancyReportNotes;

    public function __construct()
    {
        $this->propertyVacancyReportImages = new ArrayCollection();
        $this->propertyVacancyReportNotes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUuid(): Uuid
    {
        return $this->uuid;
    }

    public function getReporterCompanyName(): ?string
    {
        return $this->reporterCompanyName;
    }

    public function getReporterSalutation(): ?Salutation
    {
        return $this->reporterSalutation;
    }

    public function getReporterName(): string
    {
        return $this->reporterName;
    }

    public function getReporterFirstName(): ?string
    {
        return $this->reporterFirstName;
    }

    public function getReporterEmail(): ?string
    {
        return $this->reporterEmail;
    }

    public function getReporterPhoneNumber(): ?string
    {
        return $this->reporterPhoneNumber;
    }

    public function isReporterAlsoPropertyOwner(): bool
    {
        return $this->reporterAlsoPropertyOwner;
    }

    public function isObjectIsEmpty(): bool
    {
        return $this->objectIsEmpty;
    }

    public function getObjectIsEmptySince(): ?\DateTime
    {
        return $this->objectIsEmptySince;
    }

    public function isObjectBecomesEmpty(): bool
    {
        return $this->objectBecomesEmpty;
    }

    public function getObjectBecomesEmptyFrom(): ?\DateTime
    {
        return $this->objectBecomesEmptyFrom;
    }

    public function getIndustryClassification(): ?IndustryClassification
    {
        return $this->industryClassification;
    }

    public function getPropertyUserCompanyName(): ?string
    {
        return $this->propertyUserCompanyName;
    }

    public function getBuildingCondition(): ?BuildingCondition
    {
        return $this->buildingCondition;
    }

    public function getAreaSize(): ?float
    {
        return $this->areaSize;
    }

    public function getPlotSize(): ?float
    {
        return $this->plotSize;
    }

    public function getRetailSpace(): ?float
    {
        return $this->retailSpace;
    }

    public function getGastronomySpace(): ?float
    {
        return $this->gastronomySpace;
    }

    public function getLivingSpace(): ?float
    {
        return $this->livingSpace;
    }

    public function getUsableSpace(): ?float
    {
        return $this->usableSpace;
    }

    public function getSubsidiarySpace(): ?float
    {
        return $this->subsidiarySpace;
    }

    public function getUsableOutdoorAreaPossibility(): ?UsableOutdoorAreaPossibility
    {
        return $this->usableOutdoorAreaPossibility;
    }

    public function getUsableOutdoorArea(): ?float
    {
        return $this->usableOutdoorArea;
    }

    public function getShopWindowFrontWidth(): ?float
    {
        return $this->shopWindowFrontWidth;
    }

    public function getShopWidth(): ?float
    {
        return $this->shopWidth;
    }

    public function getGroundLevelSalesArea(): ?bool
    {
        return $this->groundLevelSalesArea;
    }

    public function isStoreWindowAvailable(): bool
    {
        return $this->storeWindowAvailable;
    }

    public function getNumberOfParkingLots(): ?int
    {
        return $this->numberOfParkingLots;
    }

    public function getBarrierFreeAccess(): ?BarrierFreeAccess
    {
        return $this->barrierFreeAccess;
    }

    public function getLocationCategory(): ?LocationCategory
    {
        return $this->locationCategory;
    }

    public function getLocationFactors(): ?array
    {
        return $this->locationFactors;
    }

    public function getPropertyOfferType(): ?PropertyOfferType
    {
        return $this->propertyOfferType;
    }

    public function getPropertyDescription(): ?string
    {
        return $this->propertyDescription;
    }

    public function getPlace(): Place
    {
        return $this->place;
    }

    public function getPostalCode(): string
    {
        return $this->postalCode;
    }

    public function getStreetName(): string
    {
        return $this->streetName;
    }

    public function getHouseNumber(): string
    {
        return $this->houseNumber;
    }

    public function getBoroughPlace(): ?Place
    {
        return $this->boroughPlace;
    }

    public function getQuarterPlace(): ?Place
    {
        return $this->quarterPlace;
    }

    public function getNeighbourhoodPlace(): ?Place
    {
        return $this->neighbourhoodPlace;
    }

    public function getPlaceDescription(): ?string
    {
        return $this->placeDescription;
    }

    public function getReportedAt(): \DateTimeImmutable
    {
        return $this->reportedAt;
    }

    public function isProcessed(): bool
    {
        return $this->processed;
    }

    public function getProcessedAt(): ?\DateTimeImmutable
    {
        return $this->processedAt;
    }

    public function isAdmitted(): bool
    {
        return $this->admitted;
    }

    public function isRejected(): bool
    {
        return $this->rejected;
    }

    public function getConfirmationToken(): Uuid
    {
        return $this->confirmationToken;
    }

    public function isEmailConfirmed(): bool
    {
        return $this->emailConfirmed;
    }

    public function getEmailConfirmedAt(): ?\DateTimeImmutable
    {
        return $this->emailConfirmedAt;
    }

    public function getBuildingUnit(): ?BuildingUnit
    {
        return $this->buildingUnit;
    }

    public function getProcessedByAccountUser(): ?AccountUser
    {
        return $this->processedByAccountUser;
    }

    public function getPropertyVacancyReportImages(): Collection|array
    {
        return $this->propertyVacancyReportImages;
    }

    public function getPropertyVacancyReportNotes(): Collection|array
    {
        return $this->propertyVacancyReportNotes;
    }
}
