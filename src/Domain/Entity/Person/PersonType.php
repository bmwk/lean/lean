<?php

declare(strict_types=1);

namespace App\Domain\Entity\Person;

enum PersonType: int
{
    case NATURAL_PERSON = 0;
    case COMPANY = 1;
    case COMMUNE = 2;

    public function getName(): string
    {
        return match ($this) {
            self::NATURAL_PERSON => 'Privatperson',
            self::COMPANY => 'Firma',
            self::COMMUNE => 'Kommune'
        };
    }
}
