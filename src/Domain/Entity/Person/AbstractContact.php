<?php

declare(strict_types=1);

namespace App\Domain\Entity\Person;

use App\Domain\Entity\AccountTrait;
use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\AnonymizedTrait;
use App\Domain\Entity\CreatedAtTrait;
use App\Domain\Entity\DeletedTrait;
use App\Domain\Entity\UpdatedAtTrait;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;

abstract class AbstractContact
{
    use AccountTrait;
    use DeletedTrait;
    use AnonymizedTrait;
    use CreatedAtTrait;
    use UpdatedAtTrait;

    #[Id]
    #[GeneratedValue]
    #[Column(type: 'integer', nullable: false, options: ['unsigned' => true])]
    protected ?int $id = null;

    #[ManyToOne(inversedBy: 'contacts')]
    #[JoinColumn(nullable: false)]
    protected Person $person;

    #[Column(type: 'smallint', nullable: true, enumType: Salutation::class, options: ['unsigned' => true])]
    protected ?Salutation $salutation = null;

    #[Column(type: 'smallint', nullable: true, enumType: PersonTitle::class, options: ['unsigned' => true])]
    protected ?PersonTitle $personTitle = null;

    #[Column(type: 'string', length: 190, nullable: true)]
    protected ?string $firstName = null;

    #[Column(type: 'string', length: 190, nullable: false)]
    protected string $name;

    #[Column(type: 'string', length: 190, nullable: true)]
    protected ?string $email = null;

    #[Column(type: 'string', length: 30, nullable: true)]
    protected ?string $mobilePhoneNumber = null;

    #[Column(type: 'string', length: 30, nullable: true)]
    protected ?string $phoneNumber = null;

    #[Column(type: 'string', length: 30, nullable: true)]
    protected ?string $faxNumber = null;

    #[Column(type: 'string', length: 190, nullable: true)]
    protected ?string $department = null;

    #[Column(type: 'string', length: 190, nullable: true)]
    protected ?string $position = null;

    #[ManyToOne]
    #[JoinColumn(nullable: true)]
    protected ?AccountUser $createdByAccountUser = null;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPerson(): Person
    {
        return $this->person;
    }

    public function getSalutation(): ?Salutation
    {
        return $this->salutation;
    }

    public function getPersonTitle(): ?PersonTitle
    {
        return $this->personTitle;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function getMobilePhoneNumber(): ?string
    {
        return $this->mobilePhoneNumber;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function getFaxNumber(): ?string
    {
        return $this->faxNumber;
    }

    public function getDepartment(): ?string
    {
        return $this->department;
    }

    public function getPosition(): ?string
    {
        return $this->position;
    }

    public function getCreatedByAccountUser(): ?AccountUser
    {
        return $this->createdByAccountUser;
    }

    public function setCreatedByAccountUser(?AccountUser $createdByAccountUser): self
    {
        $this->createdByAccountUser = $createdByAccountUser;

        return $this;
    }
}
