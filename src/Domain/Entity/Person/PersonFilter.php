<?php

declare(strict_types=1);

namespace App\Domain\Entity\Person;

class PersonFilter
{
    /**
     * @var PersonType[]|null
     */
    private ?array $personTypes = null;

    private ?array $usageTypes = null;

    /**
     * @return PersonType[]|null
     */
    public function getPersonTypes(): ?array
    {
        return $this->personTypes;
    }

    /**
     * @param PersonType[]|null $personTypes
     */
    public function setPersonTypes(?array $personTypes): self
    {
        $this->personTypes = $personTypes;

        return $this;
    }

    public function getUsageTypes(): ?array
    {
        return $this->usageTypes;
    }

    public function setUsageTypes(?array $usageTypes): self
    {
        $this->usageTypes = $usageTypes;

        return $this;
    }

    public function isFilterActive(): bool
    {
        if (empty($this->getPersonTypes()) === true && empty($this->getUsageTypes()) === true) {
            return false;
        }

        return true;
    }
}
