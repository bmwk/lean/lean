<?php

declare(strict_types=1);

namespace App\Domain\Entity\Person;

use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\AnonymizedDataBackupInterface;
use App\Domain\Entity\LookingForPropertyRequest\LookingForPropertyRequest;
use App\Domain\Entity\LookingForPropertyRequestReport\LookingForPropertyRequestReport;
use App\Domain\Entity\Property\PropertyContactPerson;
use App\Domain\Entity\Property\PropertyOwner;
use App\Domain\Entity\Property\PropertyUser;
use App\Domain\Entity\PropertyVacancyReport\PropertyVacancyReport;
use App\Domain\Entity\UserRegistration\UserRegistration;
use App\Repository\Person\PersonRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Index;
use Doctrine\ORM\Mapping\Table;

#[Entity(repositoryClass: PersonRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[Index(columns: ['account_id', 'deleted'], name: 'account_id_deleted_idx')]
#[Index(columns: ['account_id', 'person_type', 'deleted'], name: 'account_id_person_type_deleted_idx')]
#[HasLifecycleCallbacks]
class Person extends AbstractPerson implements AnonymizedDataBackupInterface
{
    public function __construct()
    {
        $this->occurrences = new ArrayCollection();
        $this->contacts = new ArrayCollection();
        $this->propertyOwners = new ArrayCollection();
        $this->propertyUsers = new ArrayCollection();
        $this->propertyContactPersons = new ArrayCollection();
        $this->lookingForPropertyRequests = new ArrayCollection();
    }

    public static function createFromUserRegistration(UserRegistration $userRegistration): self
    {
        $person = new self();

        $person->personType = $userRegistration->getPersonType();

        if ($person->personType === PersonType::NATURAL_PERSON) {
            $person->name = $userRegistration->getLastName();
            $person->firstName = $userRegistration->getFirstName();
            $person->salutation = $userRegistration->getSalutation();
        }

        if ($person->personType === PersonType::COMPANY) {
            $person->name = $userRegistration->getCompanyName();
        }

        $person->streetName = $userRegistration->getStreetName();
        $person->houseNumber = $userRegistration->getHouseNumber();
        $person->postalCode = $userRegistration->getPostalCode();
        $person->placeName = $userRegistration->getPlaceName();
        $person->email = $userRegistration->getEmail();
        $person->phoneNumber = $userRegistration->getPhoneNumber();

        return $person;
    }

    public static function createFromLookingForPropertyRequestReport(LookingForPropertyRequestReport $lookingForPropertyRequestReport): self
    {
        $person = new self();

        if (empty($lookingForPropertyRequestReport->getReporterCompanyName()) === false) {
            $person->personType = PersonType::COMPANY;
            $person->name = $lookingForPropertyRequestReport->getReporterCompanyName();

            $contact = Contact::createFromLookingForPropertyRequestReport(lookingForPropertyRequestReport: $lookingForPropertyRequestReport);
            $contact->setPerson($person);

            $person->contacts->add(element: $contact);
        } else {
            $person->personType = PersonType::NATURAL_PERSON;
            $person->name = $lookingForPropertyRequestReport->getReporterLastName();
            $person->firstName = $lookingForPropertyRequestReport->getReporterFirstName();

            if (empty($lookingForPropertyRequestReport->getReporterSalutation()) === false) {
                $person->salutation = $lookingForPropertyRequestReport->getReporterSalutation();
            }

            if (empty($lookingForPropertyRequestReport->getReporterPhoneNumber()) === false) {
                $person->phoneNumber = $lookingForPropertyRequestReport->getReporterPhoneNumber();
            }
        }

        $person->email = $lookingForPropertyRequestReport->getReporterEmail();

        return $person;
    }

    public static function createFromPropertyVacencyReport(PropertyVacancyReport $propertyVacancyReport): self
    {
        $person = new self();

        if (empty($propertyVacancyReport->getReporterCompanyName()) === false) {
            $person->personType = PersonType::COMPANY;
            $person->name = $propertyVacancyReport->getReporterCompanyName();

            $contact = Contact::createFromPropertyVacancyReport(propertyVacancyReport: $propertyVacancyReport);
            $contact->setPerson($person);

            $person->contacts->add(element: $contact);
        } else {
            $person->personType = PersonType::NATURAL_PERSON;
            $person->name = $propertyVacancyReport->getReporterName();
            $person->firstName = $propertyVacancyReport->getReporterFirstName();

            if (empty($propertyVacancyReport->getReporterSalutation()) === false) {
                $person->salutation = $propertyVacancyReport->getReporterSalutation();
            }

            if (empty($propertyVacancyReport->getReporterPhoneNumber()) === false) {
                $person->phoneNumber = $propertyVacancyReport->getReporterPhoneNumber();
            }
        }

        $person->email = $propertyVacancyReport->getReporterEmail();

        return $person;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function setPersonType(PersonType $personType): self
    {
        $this->personType = $personType;

        return $this;
    }

    public function setLegalEntityType(?LegalEntityType $legalEntityType): self
    {
        $this->legalEntityType = $legalEntityType;

        return $this;
    }

    public function setSalutation(?Salutation $salutation): self
    {
        $this->salutation = $salutation;

        return $this;
    }

    public function setPersonTitle(?PersonTitle $personTitle): self
    {
        $this->personTitle = $personTitle;

        return $this;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function setBirthName(?string $birthName): self
    {
        $this->birthName = $birthName;

        return $this;
    }

    public function setStreetName(?string $streetName): self
    {
        $this->streetName = $streetName;

        return $this;
    }

    public function setHouseNumber(?string $houseNumber): self
    {
        $this->houseNumber = $houseNumber;

        return $this;
    }

    public function setPostalCode(?string $postalCode): self
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    public function setPlaceName(?string $placeName): self
    {
        $this->placeName = $placeName;

        return $this;
    }

    public function setAdditionalAddressInformation(?string $additionalAddressInformation): self
    {
        $this->additionalAddressInformation = $additionalAddressInformation;

        return $this;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function setMobilePhoneNumber(?string $mobilePhoneNumber): self
    {
        $this->mobilePhoneNumber = $mobilePhoneNumber;

        return $this;
    }

    public function setPhoneNumber(?string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function setFaxNumber(?string $faxNumber): self
    {
        $this->faxNumber = $faxNumber;

        return $this;
    }

    public function setWebsite(?string $website): self
    {
        $this->website = $website;

        return $this;
    }

    public function setCreatedByAccountUser(?AccountUser $createdByAccountUser): self
    {
        $this->createdByAccountUser = $createdByAccountUser;

        return $this;
    }

    public function setAccountUser(?AccountUser $accountUser): self
    {
        $this->accountUser = $accountUser;

        return $this;
    }

    /**
     * @param Collection<int, Occurrence>|Occurrence[] $occurrences
     */
    public function setOccurrences(Collection|array $occurrences): self
    {
        $this->occurrences = $occurrences;

        return $this;
    }

    /**
     * @param Collection<int, Contact>|Contact[] $contacts
     */
    public function setContacts(Collection|array $contacts): self
    {
        $this->contacts = $contacts;

        return $this;
    }

    /**
     * @param Collection<int, PropertyOwner>|PropertyOwner[] $propertyOwners
     */
    public function setPropertyOwners(Collection|array $propertyOwners): self
    {
        $this->propertyOwners = $propertyOwners;

        return $this;
    }

    /**
     * @param Collection<int, PropertyUser>|PropertyUser[] $propertyUsers
     */
    public function setPropertyUsers(Collection|array $propertyUsers): self
    {
        $this->propertyUsers = $propertyUsers;

        return $this;
    }

    /**
     * @param Collection<int, PropertyContactPerson>|PropertyContactPerson[] $propertyContactPersons
     */
    public function setPropertyContactPersons(Collection|array $propertyContactPersons): self
    {
        $this->propertyContactPersons = $propertyContactPersons;

        return $this;
    }

    /**
     * @param Collection<int, LookingForPropertyRequest>|LookingForPropertyRequest[] $lookingForPropertyRequests
     */
    public function setLookingForPropertyRequests(Collection|array $lookingForPropertyRequests): self
    {
        $this->lookingForPropertyRequests = $lookingForPropertyRequests;

        return $this;
    }

    public function createChecksum(): string
    {
        $checksumParameters = [];

        $checksumParameters[] = $this->personType->value;
        $checksumParameters[] = $this->name;

        if ($this->personType === PersonType::COMPANY) {
            if ($this->legalEntityType !== null) {
                $checksumParameters[] = $this->legalEntityType;
            }
        }

        if ($this->personType === PersonType::NATURAL_PERSON) {
            if ($this->salutation !== null) {
                $checksumParameters[] = $this->salutation->value;
            }

            if ($this->personTitle !== null) {
                $checksumParameters[] = $this->personTitle->value;
            }

            if ($this->firstName !== null && $this->firstName !== '') {
                $checksumParameters[] = $this->firstName;
            }

            if ($this->birthName !== null && $this->birthName !== '') {
                $checksumParameters[] = $this->birthName;
            }
        }

        if ($this->streetName !== null && $this->streetName !== '') {
            $checksumParameters[] = $this->streetName;
        }

        if ($this->houseNumber !== null && $this->houseNumber !== '') {
            $checksumParameters[] = $this->houseNumber;
        }

        if ($this->postalCode !== null && $this->postalCode !== '') {
            $checksumParameters[] = $this->postalCode;
        }

        if ($this->placeName !== null && $this->placeName !== '') {
            $checksumParameters[] = $this->placeName;
        }

        if ($this->additionalAddressInformation !== null && $this->additionalAddressInformation !== '') {
            $checksumParameters[] = $this->additionalAddressInformation ;
        }

        if ($this->email !== null && $this->email !== '') {
            $checksumParameters[] = $this->email;
        }

        if ($this->mobilePhoneNumber !== null && $this->mobilePhoneNumber !== '') {
            $checksumParameters[] = $this->mobilePhoneNumber;
        }

        if ($this->phoneNumber !== null && $this->phoneNumber !== '') {
            $checksumParameters[] = $this->phoneNumber;
        }

        if ($this->faxNumber !== null && $this->faxNumber !== '') {
            $checksumParameters[] = $this->faxNumber;
        }

        if ($this->website !== null && $this->website !== '') {
            $checksumParameters[] = $this->website;
        }

        return md5(implode(separator: '-', array: $checksumParameters));
    }

    public function getAnonymizableDataToBackup(): array
    {
        return [
            'legalEntityType'              => $this->legalEntityType?->value,
            'salutation'                   => $this->salutation?->value,
            'personTitle'                  => $this->personTitle?->value,
            'name'                         => $this->name,
            'firstName'                    => $this->firstName,
            'birthName'                    => $this->birthName,
            'streetName'                   => $this->streetName,
            'houseNumber'                  => $this->houseNumber,
            'postalCode'                   => $this->postalCode,
            'placeName'                    => $this->placeName,
            'additionalAddressInformation' => $this->additionalAddressInformation,
            'email'                        => $this->email,
            'mobilePhoneNumber'            => $this->mobilePhoneNumber,
            'phoneNumber'                  => $this->phoneNumber,
            'faxNumber'                    => $this->faxNumber,
            'website'                      => $this->website,
        ];
    }
}
