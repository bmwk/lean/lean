<?php

declare(strict_types=1);

namespace App\Domain\Entity\Person;

class PersonSearch
{
    private ?string $text = null;

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(?string $text): self
    {
        $this->text = $text;

        return $this;
    }
}
