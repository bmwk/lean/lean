<?php

declare(strict_types=1);

namespace App\Domain\Entity\Person;

use App\Domain\Entity\AccountTrait;
use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\AnonymizedTrait;
use App\Domain\Entity\CreatedAtTrait;
use App\Domain\Entity\DeletedTrait;
use App\Domain\Entity\LookingForPropertyRequest\LookingForPropertyRequest;
use App\Domain\Entity\Property\PropertyContactPerson;
use App\Domain\Entity\Property\PropertyOwner;
use App\Domain\Entity\Property\PropertyUser;
use App\Domain\Entity\UpdatedAtTrait;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\OneToOne;

abstract class AbstractPerson
{
    use AccountTrait;
    use DeletedTrait;
    use AnonymizedTrait;
    use CreatedAtTrait;
    use UpdatedAtTrait;

    #[Id]
    #[GeneratedValue]
    #[Column(type: Types::INTEGER, nullable: false, options: ['unsigned' => true])]
    protected ?int $id = null;

    #[Column(type: Types::SMALLINT, nullable: false, enumType: PersonType::class, options: ['unsigned' => true])]
    protected PersonType $personType;

    #[Column(type: Types::SMALLINT, nullable: true, enumType: LegalEntityType::class, options: ['unsigned' => true])]
    protected ?LegalEntityType $legalEntityType = null;

    #[Column(type: Types::SMALLINT, nullable: true, enumType: Salutation::class, options: ['unsigned' => true])]
    protected ?Salutation $salutation = null;

    #[Column(type: Types::SMALLINT, nullable: true, enumType: PersonTitle::class, options: ['unsigned' => true])]
    protected ?PersonTitle $personTitle = null;

    #[Column(type: Types::STRING, length: 190, nullable: false)]
    protected string $name;

    #[Column(type: Types::STRING, length: 190, nullable: true)]
    protected ?string $firstName = null;

    #[Column(type: Types::STRING, length: 190, nullable: true)]
    protected ?string $birthName = null;

    #[Column(type: Types::STRING, length: 190, nullable: true)]
    protected ?string $streetName = null;

    #[Column(type: Types::STRING, length: 20, nullable: true)]
    protected ?string $houseNumber = null;

    #[Column(type: Types::STRING, length: 20, nullable: true)]
    protected ?string $postalCode = null;

    #[Column(type: Types::STRING, length: 190, nullable: true)]
    protected ?string $placeName = null;

    #[Column(type: 'text', nullable: true)]
    protected ?string $additionalAddressInformation = null;

    #[Column(type: Types::STRING, length: 190, nullable: true)]
    protected ?string $email = null;

    #[Column(type: Types::STRING, length: 30, nullable: true)]
    protected ?string $mobilePhoneNumber = null;

    #[Column(type: Types::STRING, length: 30, nullable: true)]
    protected ?string $phoneNumber = null;

    #[Column(type: Types::STRING, length: 30, nullable: true)]
    protected ?string $faxNumber = null;

    #[Column(type: Types::STRING, length: 255, nullable: true)]
    protected ?string $website = null;

    #[ManyToOne]
    #[JoinColumn(nullable: true)]
    protected ?AccountUser $createdByAccountUser = null;

    #[OneToOne(mappedBy: 'person')]
    protected ?AccountUser $accountUser = null;

    /**
     * @var Collection<int, Occurrence>|Occurrence[]
     */
    #[OneToMany(mappedBy: 'person', targetEntity: Occurrence::class)]
    protected Collection|array $occurrences;

    /**
     * @var Collection<int, Contact>|Contact[]
     */
    #[OneToMany(mappedBy: 'person', targetEntity: Contact::class)]
    protected Collection|array $contacts;

    /**
     * @var Collection<int, PropertyOwner>|PropertyOwner[]
     */
    #[OneToMany(mappedBy: 'person', targetEntity: PropertyOwner::class)]
    protected Collection|array $propertyOwners;

    /**
     * @var Collection<int, PropertyUser>|PropertyUser[]
     */
    #[OneToMany(mappedBy: 'person', targetEntity: PropertyUser::class)]
    protected Collection|array $propertyUsers;

    /**
     * @var Collection<int, PropertyContactPerson>|PropertyContactPerson[]
     */
    #[OneToMany(mappedBy: 'person', targetEntity: PropertyContactPerson::class)]
    protected Collection|array $propertyContactPersons;

    /**
     * @var Collection<int, LookingForPropertyRequest>|LookingForPropertyRequest[]
     */
    #[OneToMany(mappedBy: 'person', targetEntity: LookingForPropertyRequest::class)]
    protected Collection|array $lookingForPropertyRequests;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPersonType(): PersonType
    {
        return $this->personType;
    }

    public function getLegalEntityType(): ?LegalEntityType
    {
        return $this->legalEntityType;
    }

    public function getSalutation(): ?Salutation
    {
        return $this->salutation;
    }

    public function getPersonTitle(): ?PersonTitle
    {
        return $this->personTitle;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function getBirthName(): ?string
    {
        return $this->birthName;
    }

    public function getStreetName(): ?string
    {
        return $this->streetName;
    }

    public function getHouseNumber(): ?string
    {
        return $this->houseNumber;
    }

    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }

    public function getPlaceName(): ?string
    {
        return $this->placeName;
    }

    public function getAdditionalAddressInformation(): ?string
    {
        return $this->additionalAddressInformation;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function getMobilePhoneNumber(): ?string
    {
        return $this->mobilePhoneNumber;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function getFaxNumber(): ?string
    {
        return $this->faxNumber;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    public function getCreatedByAccountUser(): ?AccountUser
    {
        return $this->createdByAccountUser;
    }

    public function getAccountUser(): ?AccountUser
    {
        return $this->accountUser;
    }

    /**
     * @return  Collection<int, Occurrence>|Occurrence[]
     */
    public function getOccurrences(): Collection|array
    {
        return $this->occurrences;
    }

    /**
     * @return  Collection<int, Contact>|Contact[]
     */
    public function getContacts(): Collection|array
    {
        return $this->contacts;
    }

    /**
     * @return Collection<int, PropertyOwner>|PropertyOwner[]
     */
    public function getPropertyOwners(): Collection|array
    {
        return $this->propertyOwners;
    }

    /**
     * @return Collection<int, PropertyUser>|PropertyUser[]
     */
    public function getPropertyUsers(): Collection|array
    {
        return $this->propertyUsers;
    }

    /**
     * @return Collection<int, PropertyContactPerson>|PropertyContactPerson[]
     */
    public function getPropertyContactPersons(): Collection|array
    {
        return $this->propertyContactPersons;
    }

    /**
     * @return Collection<int, LookingForPropertyRequest>|LookingForPropertyRequest[]
     */
    public function getLookingForPropertyRequests(): Collection|array
    {
        return $this->lookingForPropertyRequests;
    }

    public function getDisplayName(): string
    {
        return $this->buildDisplayName();
    }

    public function buildDisplayName(): string
    {
        if ($this->deleted === true) {
            return 'Gelöschter Kontakt';
        }

        if ($this->getPersonType() !== PersonType::NATURAL_PERSON) {
            return $this->name;
        }

        $displayName = '';

        if ($this->salutation !== null && $this->salutation !== Salutation::DIVERS) {
            $displayName .= $this->salutation->getName();
        }

        if ($this->personTitle !== null) {
            $displayName .= ' ' . $this->personTitle->getName();
        }

        if ($this->firstName !== null) {
            $displayName .= ' ' . $this->firstName;
        }

        $displayName .= ' ' . $this->name;

        return $displayName;
    }

    public function buildShortDisplayName(): string
    {
        if ($this->deleted === true) {
            return 'Gelöschter Kontakt';
        }

        if ($this->getPersonType() !== PersonType::NATURAL_PERSON) {
            return $this->name;
        }

        if ($this->firstName !== null) {
            return $this->firstName . ' ' . $this->name;
        }

        return $this->name;
    }
}
