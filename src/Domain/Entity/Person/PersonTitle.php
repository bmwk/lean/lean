<?php

declare(strict_types=1);

namespace App\Domain\Entity\Person;

enum PersonTitle: int
{
    case DR = 0;
    case PROF = 1;
    case PROF_DR = 2;

    public function getName(): string
    {
        return match ($this) {
            self::DR => 'Dr.',
            self::PROF => 'Prof.',
            self::PROF_DR => 'Prof. Dr.'
        };
    }
}
