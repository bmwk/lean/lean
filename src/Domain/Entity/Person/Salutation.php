<?php

declare(strict_types=1);

namespace App\Domain\Entity\Person;

enum Salutation: int
{
    case HERR = 0;
    case FRAU = 1;
    case DIVERS = 2;

    public function getName(): string
    {
        return match ($this) {
            self::HERR => 'Herr',
            self::FRAU => 'Frau',
            self::DIVERS => 'Divers'
        };
    }
}
