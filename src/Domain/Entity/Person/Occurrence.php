<?php

declare(strict_types=1);

namespace App\Domain\Entity\Person;

use App\Domain\Entity\AccountTrait;
use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\AnonymizedTrait;
use App\Domain\Entity\CreatedAtTrait;
use App\Domain\Entity\CreatedByAccountUserTrait;
use App\Domain\Entity\DeletedTrait;
use App\Domain\Entity\Property\PropertyContactPerson;
use App\Domain\Entity\Property\PropertyOwner;
use App\Domain\Entity\Property\PropertyUser;
use App\Domain\Entity\UpdatedAtTrait;
use App\Repository\Person\OccurrenceRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\Table;

#[Entity(repositoryClass: OccurrenceRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[HasLifecycleCallbacks]
class Occurrence
{
    use AccountTrait;
    use CreatedByAccountUserTrait;
    use DeletedTrait;
    use AnonymizedTrait;
    use CreatedAtTrait;
    use UpdatedAtTrait;

    #[Id]
    #[GeneratedValue]
    #[Column(type: Types::INTEGER, nullable: false, options: ['unsigned' => true])]
    private ?int $id = null;

    #[Column(type: Types::SMALLINT, nullable: false, enumType: OccurrenceType::class, options: ['unsigned' => true])]
    private OccurrenceType $occurrenceType;

    #[ManyToOne(inversedBy: 'occurrences')]
    #[JoinColumn(nullable: false)]
    private Person $person;

    #[ManyToOne]
    #[JoinColumn(nullable: true)]
    private ?Contact $contact = null;

    #[Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTime $occurrenceAt = null;

    #[Column(type: Types::TEXT, nullable: true)]
    private ?string $note = null;

    #[Column(type: Types::BOOLEAN, nullable: false)]
    private bool $followUp;

    #[Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTime $followUpAt = null;

    #[Column(type: Types::BOOLEAN, nullable: false)]
    private bool $followUpDone;

    #[Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTime $followUpDoneAt = null;

    #[ManyToOne]
    #[JoinColumn(nullable: false)]
    private AccountUser $performedWithAccountUser;

    /**
     * @var Collection<int, PropertyOwner>|PropertyOwner[]
     */
    #[ManyToMany(targetEntity: PropertyOwner::class, inversedBy: 'occurrences')]
    #[JoinColumn(unique: true)]
    private Collection|array $propertyOwners;

    /**
     * @var Collection<int, PropertyUser>|PropertyUser[]
     */
    #[ManyToMany(targetEntity: PropertyUser::class, inversedBy: 'occurrences')]
    #[JoinColumn(unique: true)]
    private Collection|array $propertyUsers;

    /**
     * @var Collection<int, PropertyContactPerson>|PropertyContactPerson[]
     */
    #[ManyToMany(targetEntity: PropertyContactPerson::class, inversedBy: 'occurrences')]
    #[JoinColumn(unique: true)]
    private Collection|array $propertyContactPersons;

    /**
     * @var Collection<int, OccurrenceAttachment>|OccurrenceAttachment[]
     */
    #[OneToMany(mappedBy: 'occurrence', targetEntity: OccurrenceAttachment::class)]
    private Collection|array $occurrenceAttachments;

    public function __construct()
    {
        $this->propertyOwners = new ArrayCollection();
        $this->propertyUsers = new ArrayCollection();
        $this->propertyContactPersons = new ArrayCollection();
        $this->occurrenceAttachments = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getOccurrenceType(): ?OccurrenceType
    {
        return $this->occurrenceType;
    }

    public function setOccurrenceType(OccurrenceType $occurrenceType): self
    {
        $this->occurrenceType = $occurrenceType;

        return $this;
    }

    public function getPerson(): Person
    {
        return $this->person;
    }

    public function setPerson(Person $person): self
    {
        $this->person = $person;

        return $this;
    }

    public function getContact(): ?Contact
    {
        return $this->contact;
    }

    public function setContact(?Contact $contact): self
    {
        $this->contact = $contact;

        return $this;
    }

    public function getOccurrenceAt(): ?\DateTime
    {
        return $this->occurrenceAt;
    }

    public function setOccurrenceAt(?\DateTime $occurrenceAt): self
    {
        $this->occurrenceAt = $occurrenceAt;

        return $this;
    }

    public function getNote(): ?string
    {
        return $this->note;
    }

    public function setNote(?string $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function isFollowUp(): bool
    {
        return $this->followUp;
    }

    public function setFollowUp(bool $followUp): self
    {
        $this->followUp = $followUp;

        return $this;
    }

    public function getFollowUpAt(): ?\DateTime
    {
        return $this->followUpAt;
    }

    public function setFollowUpAt(?\DateTime $followUpAt): self
    {
        $this->followUpAt = $followUpAt;

        return $this;
    }

    public function isFollowUpDone(): bool
    {
        return $this->followUpDone;
    }

    public function setFollowUpDone(bool $followUpDone): self
    {
        $this->followUpDone = $followUpDone;

        return $this;
    }

    public function getFollowUpDoneAt(): ?\DateTime
    {
        return $this->followUpDoneAt;
    }

    public function setFollowUpDoneAt(?\DateTime $followUpDoneAt): self
    {
        $this->followUpDoneAt = $followUpDoneAt;

        return $this;
    }

    public function getPerformedWithAccountUser(): AccountUser
    {
        return $this->performedWithAccountUser;
    }

    public function setPerformedWithAccountUser(AccountUser $performedWithAccountUser): self
    {
        $this->performedWithAccountUser = $performedWithAccountUser;

        return $this;
    }

    /**
     * @return Collection<int, PropertyOwner>|PropertyOwner[]
     */
    public function getPropertyOwners(): Collection|array
    {
        return $this->propertyOwners;
    }

    /**
     * @param Collection<int, PropertyOwner>|PropertyOwner[] $propertyOwners
     */
    public function setPropertyOwners(Collection|array $propertyOwners): self
    {
        $this->propertyOwners = $propertyOwners;

        return $this;
    }

    /**
     * @return Collection<int, PropertyUser>|PropertyUser[]
     */
    public function getPropertyUsers(): Collection|array
    {
        return $this->propertyUsers;
    }

    /**
     * @param Collection<int, PropertyUser>|PropertyUser[] $propertyUsers
     */
    public function setPropertyUsers(Collection|array $propertyUsers): self
    {
        $this->propertyUsers = $propertyUsers;

        return $this;
    }

    /**
     * @return Collection<int, PropertyContactPerson>|PropertyContactPerson[]
     */
    public function getPropertyContactPersons(): Collection|array
    {
        return $this->propertyContactPersons;
    }

    /**
     * @param Collection<int, PropertyContactPerson>|PropertyContactPerson[] $propertyContactPersons
     */
    public function setPropertyContactPersons(Collection|array $propertyContactPersons): self
    {
        $this->propertyContactPersons = $propertyContactPersons;

        return $this;
    }

    /**
     * @return Collection<int, OccurrenceAttachment>|OccurrenceAttachment[]
     */
    public function getOccurrenceAttachments(): Collection|array
    {
        return $this->occurrenceAttachments;
    }

    /**
     * @param Collection<int, OccurrenceAttachment>|OccurrenceAttachment[] $occurrenceAttachments
     */
    public function setOccurrenceAttachments(Collection|array $occurrenceAttachments): self
    {
        $this->occurrenceAttachments = $occurrenceAttachments;

        return $this;
    }
}
