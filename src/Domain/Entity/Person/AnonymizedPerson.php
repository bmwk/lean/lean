<?php

declare(strict_types=1);

namespace App\Domain\Entity\Person;

class AnonymizedPerson extends AbstractPerson
{
    public function __construct(Person $person)
    {
        $this->id = $person->getId();
        $this->personType = $person->getPersonType();
        $this->legalEntityType = $person->getLegalEntityType();
        $this->salutation = null;
        $this->personTitle = null;
        $this->name = self::createRandomString(length: 20);
        $this->firstName = self::createRandomString(length: 20);
        $this->birthName = self::createRandomString(length: 20);
        $this->streetName = self::createRandomString(length: 20);
        $this->houseNumber = self::createRandomString(length: 20);
        $this->postalCode = self::createRandomString(length: 20);
        $this->placeName = self::createRandomString(length: 20);
        $this->additionalAddressInformation = self::createRandomString(length: 20);
        $this->email = self::createRandomString(length: 20);
        $this->mobilePhoneNumber = self::createRandomString(length: 20);
        $this->phoneNumber = self::createRandomString(length: 20);
        $this->faxNumber = self::createRandomString(length: 20);
        $this->website = self::createRandomString(length: 20);
        $this->account = $person->getAccount();
        $this->createdByAccountUser = $person->getCreatedByAccountUser();
        $this->deleted = $person->isDeleted();
        $this->deletedAt = $person->getDeletedAt();
        $this->anonymized = $person->isAnonymized();
        $this->anonymizedAt = $person->getAnonymizedAt();
        $this->anonymizedByAccountUser = $person->getAnonymizedByAccountUser();
        $this->createdAt = $person->getCreatedAt();
        $this->updatedAt = $person->getUpdatedAt();
    }

    private static function createRandomString(int $length): string
    {
        return substr(string: bin2hex(string: random_bytes(length: $length)), offset: 0, length: $length);
    }
}
