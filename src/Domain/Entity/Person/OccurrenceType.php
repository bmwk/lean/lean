<?php

declare(strict_types=1);

namespace App\Domain\Entity\Person;

enum OccurrenceType: int
{
    case RECEIVED_FEEDBACK = 0;
    case LETTER_SEND = 1;
    case PHONE_CALL_MADE = 2;
    case QUESTIONNAIRE_SENT = 3;
    case MEMO = 4;
    case OTHER = 5;
    case EMAIL_CORRESPONDENT = 6;

    public function getName(): string
    {
        return match ($this) {
            self::RECEIVED_FEEDBACK => 'Feedback erhalten',
            self::LETTER_SEND => 'Anschreiben versendet',
            self::PHONE_CALL_MADE =>'Telefonat geführt',
            self::QUESTIONNAIRE_SENT => 'Fragebogen versendet',
            self::MEMO => 'Aktennotiz',
            self::OTHER => 'Sonstiges',
            self::EMAIL_CORRESPONDENT => 'E-Mail-Korrespondenz'
        };
    }
}
