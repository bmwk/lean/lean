<?php

declare(strict_types=1);

namespace App\Domain\Entity\Person;

enum LegalEntityType: int
{
    case SOLE_PROPRIETORSHIP = 0;
    case SOLE_TRADER = 1;
    case SOCIETY_OF_CIVIL_RIGHTS = 2;
    case ORDINARY_PARTNERSHIP = 3;
    case LIMITED_PARTNERSHIP = 4;
    case PARTNERSHIP_COMPANY = 5;
    case LIMITED_PARTNERSHIP_COMPANY = 6;
    case REGISTERED_ASSOCIATION = 7;
    case LIMITED_LIABILITY_COMPANY = 8;
    case BUSINESS_COMPANY = 9;
    case PARTNERSHIP_LIMITED_BY_SHARES = 10;
    case CORPORATION = 11;
    case REGISTERED_COOPERATIVE = 12;
    case PUBLIC_CORPORATION = 13;
    case PRIVATE_FOUNDATION = 14;
    case PUBLIC_FOUNDATION = 15;
    case PUBLIC_LAW_INSTITUTION = 16;

    public function getName(): string
    {
        return match ($this) {
            self::SOLE_PROPRIETORSHIP => 'Einzelunternehmen',
            self::SOLE_TRADER => 'Eingetragene/r Kaufmann/frau (e.K.)',
            self::SOCIETY_OF_CIVIL_RIGHTS => 'Gesellschaft bürgerlichen Rechts (GbR)',
            self::ORDINARY_PARTNERSHIP => 'Offene Handelsgesellschaft (OHG)',
            self::LIMITED_PARTNERSHIP => 'Kommanditgesellschaft (KG)',
            self::PARTNERSHIP_COMPANY => 'Partnerschaftsgesellschaft (PartG)',
            self::LIMITED_PARTNERSHIP_COMPANY => 'Partnerschaftsgesellschaft mit beschränkter Haftung (PartGmbBH)',
            self::REGISTERED_ASSOCIATION => 'eingetragener Verein (e. V.)',
            self::LIMITED_LIABILITY_COMPANY => 'Gesellschaft mit beschränkter Haftung (GmbH)',
            self::BUSINESS_COMPANY => 'Unternehmergesellschaft (haftungsbeschränkt)',
            self::PARTNERSHIP_LIMITED_BY_SHARES => 'Kommanditgesellschaft auf Aktien (KGaA)',
            self::CORPORATION => 'Aktiengesellschaft (AG)',
            self::REGISTERED_COOPERATIVE => 'eingetragene Genossenschaft (eG)',
            self::PUBLIC_CORPORATION => 'Körperschaft des Öffentlichen Rechts',
            self::PRIVATE_FOUNDATION => 'private Stiftung',
            self::PUBLIC_FOUNDATION => 'Stiftung des öffentlichen Rechts',
            self::PUBLIC_LAW_INSTITUTION => 'Anstalt des öffentlichen Rechts (AöR)'
        };
    }
}
