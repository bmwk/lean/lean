<?php

declare(strict_types=1);

namespace App\Domain\Entity\Person;

use App\Domain\Entity\AbstractFileLink;
use App\Repository\Person\OccurrenceAttachmentRepository;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\Table;

#[Entity(repositoryClass: OccurrenceAttachmentRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[HasLifecycleCallbacks]
class OccurrenceAttachment extends AbstractFileLink
{
    #[ManyToOne(inversedBy: 'occurrenceAttachments')]
    #[JoinColumn(nullable: false)]
    private Occurrence $occurrence;

    public function getOccurrence(): Occurrence
    {
        return $this->occurrence;
    }

    public function setOccurrence(Occurrence $occurrence): self
    {
        $this->occurrence = $occurrence;

        return $this;
    }
}
