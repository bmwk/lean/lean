<?php

declare(strict_types=1);

namespace App\Domain\Entity\Person;

class AnonymizedContact extends AbstractContact
{
    public function __construct(Contact $contact)
    {
        $this->id = $contact->getId();
        $this->person = $contact->person;
        $this->salutation = $contact->salutation;
        $this->personTitle = $contact->personTitle;
        $this->firstName = self::createRandomString(length: 190);
        $this->name = self::createRandomString(length: 190);
        $this->email = self::createRandomString(length: 190);
        $this->mobilePhoneNumber = self::createRandomString(length: 30);
        $this->phoneNumber = self::createRandomString(length: 30);
        $this->faxNumber = self::createRandomString(length: 30);
        $this->department = self::createRandomString(length: 190);
        $this->position = self::createRandomString(length: 190);
    }

    private static function createRandomString(int $length): string
    {
        return substr(string: bin2hex(string: random_bytes(length: $length)), offset: 0, length: $length);
    }
}
