<?php

declare(strict_types=1);

namespace App\Domain\Entity\Person;

use App\Domain\Entity\AnonymizedDataBackupInterface;
use App\Domain\Entity\LookingForPropertyRequestReport\LookingForPropertyRequestReport;
use App\Domain\Entity\PropertyVacancyReport\PropertyVacancyReport;
use App\Domain\Entity\UserRegistration\UserRegistration;
use App\Repository\Person\ContactRepository;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Table;

#[Entity(repositoryClass: ContactRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[HasLifecycleCallbacks]
class Contact extends AbstractContact implements AnonymizedDataBackupInterface
{
    public static function createFromUserRegistration(UserRegistration $userRegistration): self
    {
        $contact = new self();

        $contact->firstName = $userRegistration->getFirstName();
        $contact->name = $userRegistration->getLastName();
        $contact->salutation = $userRegistration->getSalutation();
        $contact->email =  $userRegistration->getEmail();

        return $contact;
    }

    public static function createFromLookingForPropertyRequestReport(LookingForPropertyRequestReport $lookingForPropertyRequestReport): self
    {
        $contact = new self();

        $contact->firstName = $lookingForPropertyRequestReport->getReporterFirstName();
        $contact->name = $lookingForPropertyRequestReport->getReporterLastName();

        if (empty($lookingForPropertyRequestReport->getReporterSalutation()) === false) {
            $contact->salutation = $lookingForPropertyRequestReport->getReporterSalutation();
        }

        return $contact;
    }

    public static function createFromPropertyVacancyReport(PropertyVacancyReport $propertyVacancyReport): self
    {
        $contact = new self();

        $contact->firstName = $propertyVacancyReport->getReporterFirstName();
        $contact->name = $propertyVacancyReport->getReporterName();

        if (empty($propertyVacancyReport->getReporterSalutation()) === false) {
            $contact->salutation = $propertyVacancyReport->getReporterSalutation();
        }

        return $contact;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function setPerson(Person $person): self
    {
        $this->person = $person;

        return $this;
    }

    public function setSalutation(?Salutation $salutation): self
    {
        $this->salutation = $salutation;

        return $this;
    }

    public function setPersonTitle(?PersonTitle $personTitle): self
    {
        $this->personTitle = $personTitle;

        return $this;
    }

    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function setMobilePhoneNumber(?string $mobilePhoneNumber): self
    {
        $this->mobilePhoneNumber = $mobilePhoneNumber;

        return $this;
    }

    public function setPhoneNumber(?string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function setFaxNumber(?string $faxNumber): self
    {
        $this->faxNumber = $faxNumber;

        return $this;
    }

    public function setDepartment(?string $department): self
    {
        $this->department = $department;

        return $this;
    }

    public function setPosition(?string $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function getAnonymizableDataToBackup(): array
    {
        return [
            'salutation'        => $this->salutation?->value,
            'personTitle'       => $this->personTitle?->value,
            'firstName'         => $this->firstName,
            'name'              => $this->name,
            'email'             => $this->email,
            'mobilePhoneNumber' => $this->mobilePhoneNumber,
            'phoneNumber'       => $this->phoneNumber,
            'faxNumber'         => $this->faxNumber,
            'department'        => $this->department,
            'position'          => $this->position,
        ];
    }
}
