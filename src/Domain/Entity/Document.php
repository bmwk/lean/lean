<?php

declare(strict_types=1);

namespace App\Domain\Entity;

use App\Domain\Entity\Property\BuildingUnit;
use App\Repository\DocumentRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\Table;

#[Entity(repositoryClass: DocumentRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[HasLifecycleCallbacks]
class Document extends AbstractFileLink
{
    #[Column(type: 'string', length: 255, nullable: true)]
    private ?string $title = null;

    #[Column(type: 'boolean', nullable: false)]
    private bool $public;

    #[ManyToMany(targetEntity: BuildingUnit::class, mappedBy: 'documents')]
    private Collection|array $buildingUnits;

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function isPublic(): bool
    {
        return $this->public;
    }

    public function setPublic(bool $public): self
    {
        $this->public = $public;

        return $this;
    }

    public function getBuildingUnits(): Collection|array
    {
        return $this->buildingUnits;
    }

    public function setBuildingUnits(Collection|array $buildingUnits): self
    {
        $this->buildingUnits = $buildingUnits;

        return $this;
    }
}
