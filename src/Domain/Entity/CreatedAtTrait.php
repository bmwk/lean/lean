<?php

declare(strict_types=1);

namespace App\Domain\Entity;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\PrePersist;

trait CreatedAtTrait
{
    #[Column(type: 'datetime_immutable', nullable: false)]
    protected ?\DateTimeImmutable $createdAt = null;

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    #[PrePersist]
    public function setCreatedAtOnPrePersist(): void
    {
        $this->createdAt = new \DateTimeImmutable();
    }
}
