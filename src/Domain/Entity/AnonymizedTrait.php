<?php

declare(strict_types=1);

namespace App\Domain\Entity;

use App\Domain\Entity\AccountUser\AccountUser;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;

trait AnonymizedTrait
{
    #[Column(type: 'boolean', nullable: false)]
    protected bool $anonymized;

    #[Column(type: 'datetime_immutable', nullable: true)]
    protected ?\DateTimeImmutable $anonymizedAt = null;

    #[ManyToOne]
    #[JoinColumn(nullable: true)]
    protected ?AccountUser $anonymizedByAccountUser = null;

    public function isAnonymized(): bool
    {
        return $this->anonymized;
    }

    public function setAnonymized(bool $anonymized): self
    {
        $this->anonymized = $anonymized;

        return $this;
    }

    public function getAnonymizedAt(): ?\DateTimeImmutable
    {
        return $this->anonymizedAt;
    }

    public function setAnonymizedAt(?\DateTimeImmutable $anonymizedAt): self
    {
        $this->anonymizedAt = $anonymizedAt;

        return $this;
    }

    public function getAnonymizedByAccountUser(): ?AccountUser
    {
        return $this->anonymizedByAccountUser;
    }

    public function setAnonymizedByAccountUser(?AccountUser $anonymizedByAccountUser): self
    {
        $this->anonymizedByAccountUser = $anonymizedByAccountUser;

        return $this;
    }
}
