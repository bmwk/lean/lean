<?php

declare(strict_types=1);

namespace App\Domain\Entity;

use App\Domain\Entity\AccountUser\AccountUser;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;

trait CreatedByAccountUserTrait
{
    #[ManyToOne]
    #[JoinColumn(nullable: false)]
    protected AccountUser $createdByAccountUser;

    public function getCreatedByAccountUser(): AccountUser
    {
        return $this->createdByAccountUser;
    }

    public function setCreatedByAccountUser(AccountUser $createdByAccountUser): self
    {
        $this->createdByAccountUser = $createdByAccountUser;

        return $this;
    }
}
