<?php

declare(strict_types=1);

namespace App\Domain\Entity;

class PropertyVacancyManagementStatistics
{
    private int $empty;
    private int $becomesEmpty;
    private int $reuseAgreed;
    private int $inUse;
    private int $retail;
    private int $service;
    private int $gastronomy;
    private int $artCulture;
    private int $other;
    private int $assignedToCurrentUser;
    private int $createdByCurrentUser;

    public function getEmpty(): int
    {
        return $this->empty;
    }

    public function setEmpty(int $empty): self
    {
        $this->empty = $empty;

        return $this;
    }

    public function getBecomesEmpty(): int
    {
        return $this->becomesEmpty;
    }

    public function setBecomesEmpty(int $becomesEmpty): self
    {
        $this->becomesEmpty = $becomesEmpty;

        return $this;
    }

    public function getReuseAgreed(): int
    {
        return $this->reuseAgreed;
    }

    public function setReuseAgreed(int $reuseAgreed): self
    {
        $this->reuseAgreed = $reuseAgreed;

        return $this;
    }

    public function getInUse(): int
    {
        return $this->inUse;
    }

    public function setInUse(int $inUse): self
    {
        $this->inUse = $inUse;

        return $this;
    }

    public function getRetail(): int
    {
        return $this->retail;
    }

    public function setRetail(int $retail): self
    {
        $this->retail = $retail;

        return $this;
    }

    public function getService(): int
    {
        return $this->service;
    }

    public function setService(int $service): self
    {
        $this->service = $service;

        return $this;
    }

    public function getGastronomy(): int
    {
        return $this->gastronomy;
    }

    public function setGastronomy(int $gastronomy): self
    {
        $this->gastronomy = $gastronomy;

        return $this;
    }

    public function getArtCulture(): int
    {
        return $this->artCulture;
    }

    public function setArtCulture(int $artCulture): self
    {
        $this->artCulture = $artCulture;

        return $this;
    }

    public function getOther(): int
    {
        return $this->other;
    }

    public function setOther(int $other): self
    {
        $this->other = $other;

        return $this;
    }

    public function getAssignedToCurrentUser(): int
    {
        return $this->assignedToCurrentUser;
    }

    public function setAssignedToCurrentUser(int $assignedToCurrentUser): self
    {
        $this->assignedToCurrentUser = $assignedToCurrentUser;

        return $this;
    }

    public function getCreatedByCurrentUser(): int
    {
        return $this->createdByCurrentUser;
    }

    public function setCreatedByCurrentUser(int $createdByCurrentUser): self
    {
        $this->createdByCurrentUser = $createdByCurrentUser;

        return $this;
    }
}
