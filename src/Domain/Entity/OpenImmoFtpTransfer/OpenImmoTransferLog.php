<?php

declare(strict_types=1);

namespace App\Domain\Entity\OpenImmoFtpTransfer;

use App\Domain\Entity\CreatedAtTrait;
use App\Domain\Entity\OpenImmoExporter\ExportActionType;
use App\Domain\Entity\Property\BuildingUnit;
use App\Repository\OpenImmoFtpTransfer\OpenImmoTransferLogRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\Table;

#[Entity(repositoryClass: OpenImmoTransferLogRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[HasLifecycleCallbacks]
class OpenImmoTransferLog
{
    use CreatedAtTrait;

    #[Id]
    #[GeneratedValue]
    #[Column(type: Types::INTEGER, nullable: false, options: ['unsigned' => true])]
    private ?int $id = null;

    #[Column(type: Types::SMALLINT, nullable: false, enumType: OpenImmoTransferStatus::class, options: ['unsigned' => true])]
    private OpenImmoTransferStatus $openImmoTransferStatus;

    #[Column(type: Types::DATETIME_IMMUTABLE, nullable: false)]
    private \DateTimeImmutable $transferredAt;

    #[Column(type: Types::SMALLINT, nullable: false, enumType: ExportActionType::class, options: ['unsigned' => true])]
    private ExportActionType $exportActionType;

    #[ManyToOne(inversedBy: 'openImmoTransferLogs') ]
    #[JoinColumn(nullable: false)]
    private OpenImmoFtpCredential $openImmoFtpCredential;

    #[ManyToOne(inversedBy: 'openImmoTransferLogs')]
    #[JoinColumn(nullable: false)]
    private BuildingUnit $buildingUnit;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getOpenImmoTransferStatus(): OpenImmoTransferStatus
    {
        return $this->openImmoTransferStatus;
    }

    public function setOpenImmoTransferStatus(OpenImmoTransferStatus $openImmoTransferStatus): self
    {
        $this->openImmoTransferStatus = $openImmoTransferStatus;

        return $this;
    }

    public function getTransferredAt(): \DateTimeImmutable
    {
        return $this->transferredAt;
    }

    public function setTransferredAt(\DateTimeImmutable $transferredAt): self
    {
        $this->transferredAt = $transferredAt;

        return $this;
    }

    public function getExportActionType(): ExportActionType
    {
        return $this->exportActionType;
    }

    public function setExportActionType(ExportActionType $exportActionType): self
    {
        $this->exportActionType = $exportActionType;

        return $this;
    }

    public function getOpenImmoFtpCredential(): OpenImmoFtpCredential
    {
        return $this->openImmoFtpCredential;
    }

    public function setOpenImmoFtpCredential(OpenImmoFtpCredential $openImmoFtpCredential): self
    {
        $this->openImmoFtpCredential = $openImmoFtpCredential;

        return $this;
    }

    public function getBuildingUnit(): BuildingUnit
    {
        return $this->buildingUnit;
    }

    public function setBuildingUnit(BuildingUnit $buildingUnit): self
    {
        $this->buildingUnit = $buildingUnit;

        return $this;
    }
}
