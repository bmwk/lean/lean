<?php

declare(strict_types=1);

namespace App\Domain\Entity\OpenImmoFtpTransfer;

use App\Domain\Entity\AccountTrait;
use App\Domain\Entity\CreatedAtTrait;
use App\Domain\Entity\CreatedByAccountUserTrait;
use App\Domain\Entity\UpdatedAtTrait;
use App\Repository\OpenImmoFtpTransfer\OpenImmoFtpCredentialRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\Table;

#[Entity(repositoryClass: OpenImmoFtpCredentialRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[HasLifecycleCallbacks]
class OpenImmoFtpCredential
{
    use AccountTrait;
    use CreatedByAccountUserTrait;
    use CreatedAtTrait;
    use UpdatedAtTrait;

    #[Id]
    #[GeneratedValue]
    #[Column(type: Types::INTEGER, nullable: false, options: ['unsigned' => true])]
    private ?int $id = null;

    #[Column(type: Types::STRING, length: 190, nullable: false)]
    private string $portalName;

    #[Column(type: Types::STRING, length: 190, nullable: false)]
    private string $portalProviderNumber;

    #[Column(type: Types::STRING, length: 255, nullable: false)]
    private string $hostname;

    #[Column(type: Types::INTEGER, nullable: true)]
    private ?int $port = null;

    #[Column(type: Types::STRING, length: 190, nullable: false)]
    private string $username;

    #[Column(type: Types::STRING, length: 255, nullable: false)]
    private string $password;

    #[Column(type: Types::SMALLINT, nullable: false, enumType: FtpProtocol::class, options: ['unsigned' => true])]
    private FtpProtocol $ftpProtocol;

    /**
     * @var Collection<int, OpenImmoFtpTransferTask>|OpenImmoFtpTransferTask[]
     */
    #[OneToMany(mappedBy: 'openImmoFtpCredential', targetEntity: OpenImmoFtpTransferTask::class)]
    private Collection|array $openImmoFtpTransferTasks;

    /**
     * @var Collection<int, OpenImmoTransferLog>|OpenImmoTransferLog[]
     */
    #[OneToMany(mappedBy: 'openImmoFtpCredential', targetEntity: OpenImmoTransferLog::class)]
    private Collection|array $openImmoTransferLogs;

    public function __construct()
    {
        $this->openImmoFtpTransferTasks = new ArrayCollection();
        $this->openImmoTransferLogs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getPortalName(): string
    {
        return $this->portalName;
    }

    public function setPortalName(string $portalName): self
    {
        $this->portalName = $portalName;

        return $this;
    }

    public function getPortalProviderNumber(): string
    {
        return $this->portalProviderNumber;
    }

    public function setPortalProviderNumber(string $portalProviderNumber): self
    {
        $this->portalProviderNumber = $portalProviderNumber;

        return $this;
    }

    public function getHostname(): string
    {
        return $this->hostname;
    }

    public function setHostname(string $hostname): self
    {
        $this->hostname = $hostname;

        return $this;
    }

    public function getPort(): ?int
    {
        return $this->port;
    }

    public function setPort(?int $port): self
    {
        $this->port = $port;

        return $this;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getFtpProtocol(): FtpProtocol
    {
        return $this->ftpProtocol;
    }

    public function setFtpProtocol(FtpProtocol $ftpProtocol): self
    {
        $this->ftpProtocol = $ftpProtocol;

        return $this;
    }

    /**
     * @return Collection<int, OpenImmoFtpTransferTask>|OpenImmoFtpTransferTask[]
     */
    public function getOpenImmoFtpTransferTasks(): Collection|array
    {
        return $this->openImmoFtpTransferTasks;
    }

    /**
     * @param Collection<int, OpenImmoFtpTransferTask>|OpenImmoFtpTransferTask[] $openImmoFtpTransferTasks
     */
    public function setOpenImmoFtpTransferTasks(Collection|array $openImmoFtpTransferTasks): self
    {
        $this->openImmoFtpTransferTasks = $openImmoFtpTransferTasks;

        return $this;
    }

    /**
     * @return Collection<int, OpenImmoTransferLog>|OpenImmoTransferLog[]
     */
    public function getOpenImmoTransferLogs(): Collection|array
    {
        return $this->openImmoTransferLogs;
    }

    /**
     * @param Collection<int, OpenImmoTransferLog>|OpenImmoTransferLog[] $openImmoTransferLogs
     */
    public function setOpenImmoTransferLogs(Collection|array $openImmoTransferLogs): self
    {
        $this->openImmoTransferLogs = $openImmoTransferLogs;

        return $this;
    }
}
