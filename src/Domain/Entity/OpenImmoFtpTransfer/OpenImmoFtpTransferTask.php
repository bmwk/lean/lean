<?php

declare(strict_types=1);

namespace App\Domain\Entity\OpenImmoFtpTransfer;

use App\Domain\Entity\AbstractTask;
use App\Domain\Entity\OpenImmoExporter\OpenImmoExport;
use App\Repository\OpenImmoFtpTransfer\OpenImmoFtpTransferTaskRepository;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\UniqueConstraint;

#[Entity(repositoryClass: OpenImmoFtpTransferTaskRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[UniqueConstraint(name: 'open_immo_export_open_immo_ftp_credential', columns: ['open_immo_export_id', 'open_immo_ftp_credential_id'])]
#[HasLifecycleCallbacks]
class OpenImmoFtpTransferTask extends AbstractTask
{
    #[ManyToOne(inversedBy: 'openImmoFtpTransferTasks')]
    #[JoinColumn(nullable: false)]
    private OpenImmoExport $openImmoExport;

    #[ManyToOne(inversedBy: 'openImmoFtpTransferTasks')]
    #[JoinColumn(nullable: false)]
    private OpenImmoFtpCredential $openImmoFtpCredential;

    public function getOpenImmoExport(): OpenImmoExport
    {
        return $this->openImmoExport;
    }

    public function setOpenImmoExport(OpenImmoExport $openImmoExport): self
    {
        $this->openImmoExport = $openImmoExport;

        return $this;
    }

    public function getOpenImmoFtpCredential(): OpenImmoFtpCredential
    {
        return $this->openImmoFtpCredential;
    }

    public function setOpenImmoFtpCredential(OpenImmoFtpCredential $openImmoFtpCredential): self
    {
        $this->openImmoFtpCredential = $openImmoFtpCredential;

        return $this;
    }
}
