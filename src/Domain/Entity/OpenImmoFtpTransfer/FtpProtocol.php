<?php

declare(strict_types=1);

namespace App\Domain\Entity\OpenImmoFtpTransfer;

enum FtpProtocol: int
{
    case FTP_FTPS = 0;
    case SFTP = 1;

    public function getName(): string
    {
        return match ($this) {
            self::FTP_FTPS => 'FTP/FTPS',
            self::SFTP => 'SFTP'
        };
    }
}
