<?php

declare(strict_types=1);

namespace App\Domain\Entity\OpenImmoFtpTransfer;

enum OpenImmoTransferStatus: int
{
    case DONE = 0;
    case FAILED = 1;

    public function getName(): string
    {
        return match ($this) {
            self::DONE => 'Abgeschlossen',
            self::FAILED => 'Fehlerhaft'
        };
    }
}
