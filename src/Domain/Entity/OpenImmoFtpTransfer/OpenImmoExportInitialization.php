<?php

declare(strict_types=1);

namespace App\Domain\Entity\OpenImmoFtpTransfer;

use App\Domain\Entity\OpenImmoExporter\ExportActionType;

class OpenImmoExportInitialization
{
    /**
     * @var OpenImmoFtpCredential[]|null
     */
    private ?array $openImmoFtpCredentials = null;

    private ?ExportActionType $exportActionType = null;

    /**
     * @return OpenImmoFtpCredential[]|null
     */
    public function getOpenImmoFtpCredentials(): ?array
    {
        return $this->openImmoFtpCredentials;
    }

    /**
     * @param OpenImmoFtpCredential[]|null $openImmoFtpCredentials
     */
    public function setOpenImmoFtpCredentials(?array $openImmoFtpCredentials): self
    {
        $this->openImmoFtpCredentials = $openImmoFtpCredentials;

        return $this;
    }

    public function getExportActionType(): ?ExportActionType
    {
        return $this->exportActionType;
    }

    public function setExportActionType(?ExportActionType $exportActionType): self
    {
        $this->exportActionType = $exportActionType;

        return $this;
    }
}
