<?php

declare(strict_types=1);

namespace App\Domain\Entity;

use App\Domain\Entity\LookingForPropertyRequestReport\LookingForPropertyRequestReport;
use App\Repository\PropertyMandatoryRequirementRepository;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;

#[Entity(repositoryClass: PropertyMandatoryRequirementRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[HasLifecycleCallbacks]
class PropertyMandatoryRequirement
{
    use CreatedAtTrait;
    use UpdatedAtTrait;

    #[Id]
    #[GeneratedValue]
    #[Column(type: 'integer', nullable: false, options: ['unsigned' => true])]
    private ?int $id = null;

    #[Column(type: 'boolean', nullable: false)]
    private bool $roofingRequired = false;

    #[Column(type: 'boolean', nullable: false)]
    private bool $outdoorSpaceRequired = false;

    #[Column(type: 'boolean', nullable: false)]
    private bool $showroomRequired = false;

    #[Column(type: 'boolean', nullable: false)]
    private bool $shopWindowRequired = false;

    #[Column(type: 'boolean', nullable: false)]
    private bool $groundLevelSalesAreaRequired = false;

    #[Column(type: 'boolean', nullable: false)]
    private bool $barrierFreeAccessRequired = false;

    public static function createFromLookingForPropertyRequestReport(LookingForPropertyRequestReport $lookingForPropertyRequestReport): self
    {
        $propertyMandatoryRequirementFromLookingForPropertyRequestReport = $lookingForPropertyRequestReport->getPropertyRequirement()->getPropertyMandatoryRequirement();

        $propertyMandatoryRequirement = new self();

        $propertyMandatoryRequirement->roofingRequired = $propertyMandatoryRequirementFromLookingForPropertyRequestReport->isRoofingRequired();
        $propertyMandatoryRequirement->outdoorSpaceRequired = $propertyMandatoryRequirementFromLookingForPropertyRequestReport->isOutdoorSpaceRequired();
        $propertyMandatoryRequirement->showroomRequired = $propertyMandatoryRequirementFromLookingForPropertyRequestReport->isShowroomRequired();
        $propertyMandatoryRequirement->shopWindowRequired = $propertyMandatoryRequirementFromLookingForPropertyRequestReport->isShopWindowRequired();
        $propertyMandatoryRequirement->groundLevelSalesAreaRequired = $propertyMandatoryRequirementFromLookingForPropertyRequestReport->isGroundLevelSalesAreaRequired();
        $propertyMandatoryRequirement->barrierFreeAccessRequired = $propertyMandatoryRequirementFromLookingForPropertyRequestReport->isBarrierFreeAccessRequired();

        return $propertyMandatoryRequirement;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function isRoofingRequired(): bool
    {
        return $this->roofingRequired;
    }

    public function setRoofingRequired(bool $roofingRequired): self
    {
        $this->roofingRequired = $roofingRequired;
        return $this;
    }

    public function isOutdoorSpaceRequired(): bool
    {
        return $this->outdoorSpaceRequired;
    }

    public function setOutdoorSpaceRequired(bool $outdoorSpaceRequired): self
    {
        $this->outdoorSpaceRequired = $outdoorSpaceRequired;
        return $this;
    }

    public function isShowroomRequired(): bool
    {
        return $this->showroomRequired;
    }

    public function setShowroomRequired(bool $showroomRequired): self
    {
        $this->showroomRequired = $showroomRequired;
        return $this;
    }

    public function isShopWindowRequired(): bool
    {
        return $this->shopWindowRequired;
    }

    public function setShopWindowRequired(bool $shopWindowRequired): self
    {
        $this->shopWindowRequired = $shopWindowRequired;
        return $this;
    }

    public function isGroundLevelSalesAreaRequired(): bool
    {
        return $this->groundLevelSalesAreaRequired;
    }

    public function setGroundLevelSalesAreaRequired(bool $groundLevelSalesAreaRequired): self
    {
        $this->groundLevelSalesAreaRequired = $groundLevelSalesAreaRequired;
        return $this;
    }

    public function isBarrierFreeAccessRequired(): bool
    {
        return $this->barrierFreeAccessRequired;
    }

    public function setBarrierFreeAccessRequired(bool $barrierFreeAccessRequired): self
    {
        $this->barrierFreeAccessRequired = $barrierFreeAccessRequired;
        return $this;
    }
}