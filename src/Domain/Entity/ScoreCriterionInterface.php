<?php

declare(strict_types=1);

namespace App\Domain\Entity;

interface ScoreCriterionInterface
{
    public function isSet(): bool;

    public function isEvaluationResult(): bool;

    public function getPoints(): int;

    public function getMaxPoints(): int;
}