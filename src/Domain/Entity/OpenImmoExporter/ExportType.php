<?php

declare(strict_types=1);

namespace App\Domain\Entity\OpenImmoExporter;

enum ExportType: int
{
    CASE ONLINE = 0;
    case OFFLINE = 1;

    public function getName(): string
    {
        return match($this) {
            self::ONLINE => 'ONLINE',
            self::OFFLINE => 'OFFLINE'
        };
    }

}
