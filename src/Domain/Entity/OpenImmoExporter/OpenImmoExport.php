<?php

declare(strict_types=1);

namespace App\Domain\Entity\OpenImmoExporter;

use App\Domain\Entity\AccountTrait;
use App\Domain\Entity\CreatedAtTrait;
use App\Domain\Entity\CreatedByAccountUserTrait;
use App\Domain\Entity\File;
use App\Domain\Entity\OpenImmoFtpTransfer\OpenImmoFtpTransferTask;
use App\Domain\Entity\Property\BuildingUnit;
use App\Domain\Entity\UpdatedAtTrait;
use App\Repository\OpenImmoExporter\OpenImmoExportRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\Table;

#[Entity(repositoryClass: OpenImmoExportRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[HasLifecycleCallbacks]
class OpenImmoExport
{
    use AccountTrait;
    use CreatedByAccountUserTrait;
    use CreatedAtTrait;
    use UpdatedAtTrait;

    #[Id]
    #[GeneratedValue]
    #[Column(type: Types::INTEGER, nullable: false, options: ['unsigned' => true])]
    private ?int $id = null;

    #[OneToOne]
    #[JoinColumn(nullable: true)]
    private ?File $file = null;

    #[Column(type: Types::STRING, length: 190, nullable: false)]
    private string $portalProviderNumber;

    #[Column(type: Types::SMALLINT, nullable: false, enumType: ExportStatus::class, options: ['unsigned' => true])]
    private ExportStatus $exportStatus;

    #[Column(type: Types::SMALLINT, nullable: false, enumType: ExportScope::class, options: ['unsigned' => true])]
    private ExportScope $exportScope;

    #[Column(type: Types::SMALLINT, nullable: false, enumType: ExportType::class, options: ['unsigned' => true])]
    private ExportType $exportType;

    #[Column(type: Types::SMALLINT, nullable: false, enumType: ExportMode::class, options: ['unsigned' => true])]
    private ExportMode $exportMode;

    #[Column(type: Types::SMALLINT, nullable: false, enumType: ExportActionType::class, options: ['unsigned' => true])]
    private ExportActionType $exportActionType;

    #[ManyToOne(inversedBy: 'openImmoExports')]
    #[JoinColumn(nullable: false)]
    private BuildingUnit $buildingUnit;

    /**
     * @var Collection<int, openImmoFtpTransferTask>|openImmoFtpTransferTask[]
     */
    #[OneToMany(mappedBy: 'openImmoExport', targetEntity: openImmoFtpTransferTask::class)]
    private Collection|array $openImmoFtpTransferTasks;

    public function __construct()
    {
        $this->openImmoFtpTransferTasks = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getFile(): ?File
    {
        return $this->file;
    }

    public function setFile(?File $file): self
    {
        $this->file = $file;

        return $this;
    }

    public function getPortalProviderNumber(): string
    {
        return $this->portalProviderNumber;
    }

    public function setPortalProviderNumber(string $portalProviderNumber): self
    {
        $this->portalProviderNumber = $portalProviderNumber;

        return $this;
    }

    public function getExportStatus(): ExportStatus
    {
        return $this->exportStatus;
    }

    public function setExportStatus(ExportStatus $exportStatus): self
    {
        $this->exportStatus = $exportStatus;

        return $this;
    }

    public function setBuildingUnit(BuildingUnit $buildingUnit): self
    {
        $this->buildingUnit = $buildingUnit;

        return $this;
    }

    public function getExportScope(): ExportScope
    {
        return $this->exportScope;
    }

    public function setExportScope(ExportScope $exportScope): self
    {
        $this->exportScope = $exportScope;

        return $this;
    }

    public function getExportType(): ExportType
    {
        return $this->exportType;
    }

    public function setExportType(ExportType $exportArt): self
    {
        $this->exportType = $exportArt;

        return $this;
    }

    public function getExportMode(): ExportMode
    {
        return $this->exportMode;
    }

    public function setExportMode(ExportMode $exportMode): self
    {
        $this->exportMode = $exportMode;

        return $this;
    }

    public function getExportActionType(): ExportActionType
    {
        return $this->exportActionType;
    }

    public function setExportActionType(ExportActionType $exportActionArt): self
    {
        $this->exportActionType = $exportActionArt;

        return $this;
    }

    public function getBuildingUnit(): BuildingUnit
    {
        return $this->buildingUnit;
    }

    /**
     * @return Collection<int, OpenImmoFtpTransferTask>|OpenImmoFtpTransferTask[]
     */
    public function getOpenImmoFtpTransferTasks(): Collection|array
    {
        return $this->openImmoFtpTransferTasks;
    }

    /**
     * @param Collection<int, OpenImmoFtpTransferTask>|OpenImmoFtpTransferTask[] $openImmoFtpTransferTasks
     */
    public function setOpenImmoFtpTransferTasks(Collection|array $openImmoFtpTransferTasks): self
    {
        $this->openImmoFtpTransferTasks = $openImmoFtpTransferTasks;

        return $this;
    }
}
