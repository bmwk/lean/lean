<?php

declare(strict_types=1);

namespace App\Domain\Entity\OpenImmoExporter;

enum ExportScope: int
{
    case PART = 0;
    case FULL = 1;

    public function getName(): string
    {
        return match($this){
            self::PART => 'TEIL',
            self::FULL => 'VOLL'
        };
    }

}
