<?php

declare(strict_types=1);

namespace App\Domain\Entity\OpenImmoExporter;

enum ExportActionType: int
{
    case CHANGE = 0;
    case DELETE = 1;
    case REFERENCE = 2;

    public function getName(): string
    {
        return match($this) {
            self::CHANGE => 'CHANGE',
            self::DELETE => 'DELETE',
            self::REFERENCE => 'REFERENZ'
        };
    }

    public function getDescription(): string
    {
        return match($this) {
            self::CHANGE => 'Objekt an Portal(e) übertragen',
            self::DELETE => 'Objekt aus Portal(en) löschen',
            self::REFERENCE => 'REFERENZ'
        };
    }
}
