<?php

declare(strict_types=1);

namespace App\Domain\Entity\OpenImmoExporter;

enum ExportMode: int
{
    case NEW = 0;
    case CHANGE = 1;
    case DELETE = 2;

    public function getName(): string
    {
        return match ($this) {
            self::NEW => 'NEW',
            self::CHANGE => 'CHANGE',
            self::DELETE => 'DELETE'
        };
    }
}
