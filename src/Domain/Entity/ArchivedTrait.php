<?php

declare(strict_types=1);

namespace App\Domain\Entity;

use Doctrine\ORM\Mapping\Column;

trait ArchivedTrait
{
    #[Column(type: 'boolean', nullable: false)]
    protected bool $archived = false;

    #[Column(type: 'datetime_immutable', nullable: true)]
    protected ?\DateTimeImmutable $archivedAt = null;

    public function isArchived(): bool
    {
        return $this->archived;
    }

    public function setArchived(bool $archived): self
    {
        $this->archived = $archived;

        return $this;
    }

    public function getArchivedAt(): ?\DateTimeImmutable
    {
        return $this->archivedAt;
    }

    public function setArchivedAt(?\DateTimeImmutable $archivedAt): self
    {
        $this->archivedAt = $archivedAt;

        return $this;
    }
}
