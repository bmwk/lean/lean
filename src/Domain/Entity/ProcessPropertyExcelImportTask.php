<?php

declare(strict_types=1);

namespace App\Domain\Entity;

use App\Repository\ProcessPropertyExcelImportTaskRepository;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\Table;

#[Entity(repositoryClass: ProcessPropertyExcelImportTaskRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[HasLifecycleCallbacks]
class ProcessPropertyExcelImportTask extends AbstractTask
{
    #[OneToOne(inversedBy: 'processPropertyExcelImportTask')]
    #[JoinColumn(nullable: false)]
    private PropertyExcelImport $propertyExcelImport;

    public function getPropertyExcelImport(): PropertyExcelImport
    {
        return $this->propertyExcelImport;
    }

    public function setPropertyExcelImport(PropertyExcelImport $propertyExcelImport): self
    {
        $this->propertyExcelImport = $propertyExcelImport;

        return $this;
    }
}
