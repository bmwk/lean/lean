<?php

declare(strict_types=1);

namespace App\Domain\Entity;

use Symfony\Component\HttpFoundation\Request;

class PaginationParameter implements UrlParameterInterface
{
    public static function createFromRequest(Request $request, int $amountEntriesPerPage = 10): self
    {
        $page =  $request->query->get(key: 'seite') === null ? 1 : (int) $request->query->get(key: 'seite');
        $limit = $request->query->get(key: 'anzeigen') === null ? $amountEntriesPerPage : (int) $request->query->get(key: 'anzeigen');

        return new self(page:$page, limit: $limit);
    }

    public function __construct(
       private int $page,
       private int $limit
    ) {
    }

    public function getPage(): int
    {
        return $this->page;
    }

    public function setPage(int $page): self
    {
        $this->page = $page;

        return $this;
    }

    public function getLimit(): int
    {
        return $this->limit;
    }

    public function setLimit(int $limit): self
    {
        $this->limit = $limit;

        return $this;
    }

    public function getFirstResult(): int
    {
        return ($this->page - 1) * $this->limit;
    }

    public function toUrlParameters(): array
    {
        return [
            'seite'    => $this->page,
            'anzeigen' => $this->limit,
        ];
    }
}
