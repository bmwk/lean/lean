<?php

declare(strict_types=1);

namespace App\Domain\Entity\SurveyResult;

use App\Domain\Entity\AccountTrait;
use App\Repository\SurveyResult\SurveyResultRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\Table;

#[Entity(repositoryClass: SurveyResultRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[HasLifecycleCallbacks]
class SurveyResult extends AbstractSurveyResult
{
    use AccountTrait;

    #[Column(type: Types::TEXT, nullable: true)]
    private ?string $shortDescription = null;

    /**
     * @var Collection<int, SurveyResultChapter>|SurveyResultChapter[]
     */
    #[OneToMany(mappedBy: 'surveyResult', targetEntity: SurveyResultChapter::class)]
    private Collection|array $surveyResultChapters;

    public function __construct()
    {
        $this->surveyResultChapters = new ArrayCollection();
    }

    public function getShortDescription(): ?string
    {
        return $this->shortDescription;
    }

    public function setShortDescription(?string $shortDescription): self
    {
        $this->shortDescription = $shortDescription;

        return $this;
    }

    /**
     * @return Collection<int, SurveyResultChapter>|SurveyResultChapter[]
     */
    public function getSurveyResultChapters(): Collection|array
    {
        return $this->surveyResultChapters;
    }

    /**
     * @param Collection<int, SurveyResultChapter>|SurveyResultChapter[] $surveyResultChapters
     */
    public function setSurveyResultChapters(Collection|array $surveyResultChapters): self
    {
        $this->surveyResultChapters = $surveyResultChapters;

        return $this;
    }
}
