<?php

declare(strict_types=1);

namespace App\Domain\Entity\SurveyResult;

use App\Repository\SurveyResult\SurveyResultChapterRepository;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\Table;

#[Entity(repositoryClass: SurveyResultChapterRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[HasLifecycleCallbacks]
class SurveyResultChapter extends AbstractSurveyResult
{
    #[ManyToOne(inversedBy: 'surveyResultChapters')]
    #[JoinColumn(nullable: false)]
    private SurveyResult $surveyResult;

    public function getSurveyResult(): SurveyResult
    {
        return $this->surveyResult;
    }

    public function setSurveyResult(SurveyResult $surveyResult): self
    {
        $this->surveyResult = $surveyResult;

        return $this;
    }
}
