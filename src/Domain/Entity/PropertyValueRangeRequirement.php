<?php

declare(strict_types=1);

namespace App\Domain\Entity;

use App\Domain\Entity\LookingForPropertyRequestReport\LookingForPropertyRequestReport;
use App\Repository\PropertyValueRangeRequirementRepository;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\Table;

#[Entity(repositoryClass: PropertyValueRangeRequirementRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[HasLifecycleCallbacks]
class PropertyValueRangeRequirement
{
    use CreatedAtTrait;
    use UpdatedAtTrait;

    #[Id]
    #[GeneratedValue]
    #[Column(type: 'integer', nullable: false, options: ['unsigned' => true])]
    private ?int $id = null;

    #[OneToOne(fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private ValueRequirement $totalSpace;

    #[OneToOne(fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private ValueRequirement $retailSpace;

    #[OneToOne(fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private ValueRequirement $outdoorSpace;

    #[OneToOne(fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private ValueRequirement $usableSpace;

    #[OneToOne(fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private ValueRequirement $storageSpace;

    #[OneToOne(fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private ValueRequirement $shopWindowLength;

    #[OneToOne(fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private ValueRequirement $shopWidth;

    #[OneToOne(fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private ValueRequirement $subsidiarySpace;

    public static function createFromLookingForPropertyRequestReport(LookingForPropertyRequestReport $lookingForPropertyRequestReport): self
    {
        $propertyValueRangeRequirementFromLookingForPropertyRequestReport = $lookingForPropertyRequestReport->getPropertyRequirement()->getPropertyValueRangeRequirement();

        $propertyValueRangeRequirement = new self();

        $propertyValueRangeRequirement->totalSpace = clone $propertyValueRangeRequirementFromLookingForPropertyRequestReport->getTotalSpace();
        $propertyValueRangeRequirement->totalSpace->setId(null);

        $propertyValueRangeRequirement->retailSpace = clone $propertyValueRangeRequirementFromLookingForPropertyRequestReport->getRetailSpace();
        $propertyValueRangeRequirement->retailSpace->setId(null);

        $propertyValueRangeRequirement->outdoorSpace = clone $propertyValueRangeRequirementFromLookingForPropertyRequestReport->getOutdoorSpace();
        $propertyValueRangeRequirement->outdoorSpace->setId(null);

        $propertyValueRangeRequirement->usableSpace = clone $propertyValueRangeRequirementFromLookingForPropertyRequestReport->getUsableSpace();
        $propertyValueRangeRequirement->usableSpace->setId(null);

        $propertyValueRangeRequirement->storageSpace = clone $propertyValueRangeRequirementFromLookingForPropertyRequestReport->getStorageSpace();
        $propertyValueRangeRequirement->storageSpace->setId(null);

        $propertyValueRangeRequirement->shopWindowLength = clone $propertyValueRangeRequirementFromLookingForPropertyRequestReport->getShopWindowLength();
        $propertyValueRangeRequirement->shopWindowLength->setId(null);

        $propertyValueRangeRequirement->shopWidth = clone $propertyValueRangeRequirementFromLookingForPropertyRequestReport->getShopWidth();
        $propertyValueRangeRequirement->shopWidth->setId(null);

        $propertyValueRangeRequirement->subsidiarySpace = clone $propertyValueRangeRequirementFromLookingForPropertyRequestReport->getSubsidiarySpace();
        $propertyValueRangeRequirement->subsidiarySpace->setId(null);

        return $propertyValueRangeRequirement;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getTotalSpace(): ValueRequirement
    {
        return $this->totalSpace;
    }

    public function setTotalSpace(ValueRequirement $totalSpace): self
    {
        $this->totalSpace = $totalSpace;

        return $this;
    }

    public function getRetailSpace(): ValueRequirement
    {
        return $this->retailSpace;
    }

    public function setRetailSpace(ValueRequirement $retailSpace): self
    {
        $this->retailSpace = $retailSpace;

        return $this;
    }

    public function getOutdoorSpace(): ValueRequirement
    {
        return $this->outdoorSpace;
    }

    public function setOutdoorSpace(ValueRequirement $outdoorSpace): self
    {
        $this->outdoorSpace = $outdoorSpace;

        return $this;
    }

    public function getUsableSpace(): ValueRequirement
    {
        return $this->usableSpace;
    }

    public function setUsableSpace(ValueRequirement $usableSpace): self
    {
        $this->usableSpace = $usableSpace;

        return $this;
    }

    public function getStorageSpace(): ValueRequirement
    {
        return $this->storageSpace;
    }

    public function setStorageSpace(ValueRequirement $storageSpace): self
    {
        $this->storageSpace = $storageSpace;

        return $this;
    }

    public function getShopWindowLength(): ValueRequirement
    {
        return $this->shopWindowLength;
    }

    public function setShopWindowLength(ValueRequirement $shopWindowLength): self
    {
        $this->shopWindowLength = $shopWindowLength;

        return $this;
    }

    public function getShopWidth(): ValueRequirement
    {
        return $this->shopWidth;
    }

    public function setShopWidth(ValueRequirement $shopWidth): self
    {
        $this->shopWidth = $shopWidth;

        return $this;
    }

    public function getSubsidiarySpace(): ValueRequirement
    {
        return $this->subsidiarySpace;
    }

    public function setSubsidiarySpace(ValueRequirement $subsidiarySpace): self
    {
        $this->subsidiarySpace = $subsidiarySpace;

        return $this;
    }
}
