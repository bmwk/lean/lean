<?php

declare(strict_types=1);

namespace App\Domain\Entity;

enum RequestReason: int
{
    case EXTENSION = 0;
    case BRANCH = 1;
    case MOVE_IN = 2;
    case MOVE = 3;
    case BUSINESS_START_UP = 4;
    case OTHER = 5;

    public function getName(): string
    {
        return match ($this) {
            self::EXTENSION => 'Erweiterung',
            self::BRANCH => 'Zweigstelle',
            self::MOVE_IN => 'Zuzug',
            self::MOVE => 'Umzug',
            self::BUSINESS_START_UP => 'Existenzgründung',
            self::OTHER => 'Sonstiges'
        };
    }
}
