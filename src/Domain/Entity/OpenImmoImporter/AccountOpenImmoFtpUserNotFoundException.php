<?php

declare(strict_types=1);

namespace App\Domain\Entity\OpenImmoImporter;

use App\Domain\OpenImmoImporter\Exception\OpenImmoImportExceptionInterface;

class AccountOpenImmoFtpUserNotFoundException extends \RuntimeException implements OpenImmoImportExceptionInterface
{
}
