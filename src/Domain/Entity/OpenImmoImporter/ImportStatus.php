<?php

declare(strict_types=1);

namespace App\Domain\Entity\OpenImmoImporter;

enum ImportStatus: int
{
    case IN_CREATION = 0;
    case CREATED = 1;
    case IN_PROGRESS = 2;
    case DONE = 3;
    case FAILED = 4;

    public function getName(): string
    {
        return match ($this) {
            self::IN_CREATION => 'in Erstellung',
            self::CREATED => 'Erstellt',
            self::IN_PROGRESS => 'in Verarbeitung',
            self::DONE => 'Abgeschlossen',
            self::FAILED => 'Fehlerhaft'
        };
    }
}
