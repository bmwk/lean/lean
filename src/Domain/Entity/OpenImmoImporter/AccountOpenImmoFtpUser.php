<?php

declare(strict_types=1);

namespace App\Domain\Entity\OpenImmoImporter;

use App\Domain\Entity\Account;
use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\CreatedAtTrait;
use App\Domain\Entity\CreatedByAccountUserTrait;
use App\Domain\Entity\UpdatedAtTrait;
use App\Repository\OpenImmoImporter\AccountOpenImmoFtpUserRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\Table;

#[Entity(repositoryClass: AccountOpenImmoFtpUserRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[HasLifecycleCallbacks]
class AccountOpenImmoFtpUser
{
    use CreatedByAccountUserTrait;
    use CreatedAtTrait;
    use UpdatedAtTrait;

    #[Id]
    #[GeneratedValue]
    #[Column(type: Types::INTEGER, nullable: false, options: ['unsigned' => true])]
    private ?int $id = null;

    #[OneToOne(inversedBy: 'accountOpenImmoFtpUser')]
    #[JoinColumn(nullable: false)]
    private Account $account;

    #[OneToOne]
    #[JoinColumn(nullable: false)]
    private AccountUser $accountUser;

    #[Column(type: Types::STRING, length: 30, unique: true, nullable: false)]
    private string $ftpUsername;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getAccount(): Account
    {
        return $this->account;
    }

    public function setAccount(Account $account): self
    {
        $this->account = $account;

        return $this;
    }

    public function getAccountUser(): AccountUser
    {
        return $this->accountUser;
    }

    public function setAccountUser(AccountUser $accountUser): self
    {
        $this->accountUser = $accountUser;

        return $this;
    }

    public function getFtpUsername(): string
    {
        return $this->ftpUsername;
    }

    public function setFtpUsername(string $ftpUsername): self
    {
        $this->ftpUsername = $ftpUsername;

        return $this;
    }
}
