<?php

declare(strict_types=1);

namespace App\Domain\Entity\OpenImmoImporter;

use App\Domain\Entity\AccountTrait;
use App\Domain\Entity\CreatedAtTrait;
use App\Domain\Entity\UpdatedAtTrait;
use App\Repository\OpenImmoImporter\OpenImmoImportRepository;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\Table;

#[Entity(repositoryClass: OpenImmoImportRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[HasLifecycleCallbacks]
class OpenImmoImport
{
    use AccountTrait;
    use CreatedAtTrait;
    use UpdatedAtTrait;

    #[Id]
    #[GeneratedValue]
    #[Column(type: 'integer', nullable: false, options: ['unsigned' => true])]
    private ?int $id = null;

    #[Column(type: 'smallint', nullable: false, enumType: ImportStatus::class, options: ['unsigned' => true])]
    private ImportStatus $importStatus;

    #[Column(type: 'string', length: 190, nullable: true)]
    private string $filename;

    #[ManyToOne(inversedBy: 'openImmoImports')]
    #[JoinColumn(nullable: true)]
    private ?OpenImmoUpload $openImmoUpload = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getImportStatus(): ImportStatus
    {
        return $this->importStatus;
    }

    public function setImportStatus(ImportStatus $importStatus): self
    {
        $this->importStatus = $importStatus;

        return $this;
    }

    public function getFilename(): string
    {
        return $this->filename;
    }

    public function setFilename(string $filename): self
    {
        $this->filename = $filename;

        return $this;
    }

    public function getOpenImmoUpload(): ?OpenImmoUpload
    {
        return $this->openImmoUpload;
    }

    public function setOpenImmoUpload(?OpenImmoUpload $openImmoUpload): self
    {
        $this->openImmoUpload = $openImmoUpload;

        return $this;
    }
}
