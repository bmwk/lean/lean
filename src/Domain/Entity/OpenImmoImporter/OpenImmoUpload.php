<?php

declare(strict_types=1);

namespace App\Domain\Entity\OpenImmoImporter;

use App\Domain\Entity\Account;
use App\Domain\Entity\AccountTrait;
use App\Domain\Entity\CreatedAtTrait;
use App\Repository\OpenImmoImporter\OpenImmoUploadRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\Table;

#[Entity(repositoryClass: OpenImmoUploadRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[HasLifecycleCallbacks]
class OpenImmoUpload
{
    use AccountTrait;
    use CreatedAtTrait;

    #[Id]
    #[GeneratedValue]
    #[Column(type: Types::INTEGER, nullable: false, options: ['unsigned' => true])]
    private ?int $id = null;

    #[Column(type: Types::STRING, length: 250, nullable: false)]
    private string $filename;

    #[Column(type: Types::DATETIME_IMMUTABLE, nullable: false)]
    private \DateTimeImmutable $uploadedAt;

    #[Column(type: Types::DATETIME_IMMUTABLE, nullable: false)]
    private \DateTimeImmutable $capturedAt;

    /**
     * @var Collection<int, OpenImmoImport>|OpenImmoImport[]
     */
    #[OneToMany(mappedBy: 'openImmoUpload', targetEntity: OpenImmoImport::class)]
    private Collection|array $openImmoImports;

    public static function createFromAccountAndSplFileInfo(Account $account, \SplFileInfo $splFileInfo): self
    {
        if ($splFileInfo->isFile() === false) {
            throw new \RuntimeException(message: 'splFileInfo is not from type \'File\'');
        }

        $openImmoUpload = new self();

        $openImmoUpload->account = $account;
        $openImmoUpload->filename = $splFileInfo->getFilename();
        $openImmoUpload->uploadedAt = (new \DateTimeImmutable())->setTimestamp($splFileInfo->getMTime());

        return $openImmoUpload;
    }

    public function __construct()
    {
        $this->openImmoImports = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getFilename(): string
    {
        return $this->filename;
    }

    public function setFilename(string $filename): self
    {
        $this->filename = $filename;

        return $this;
    }

    public function getUploadedAt(): \DateTimeImmutable
    {
        return $this->uploadedAt;
    }

    public function setUploadedAt(\DateTimeImmutable $uploadedAt): self
    {
        $this->uploadedAt = $uploadedAt;

        return $this;
    }

    public function getCapturedAt(): \DateTimeImmutable
    {
        return $this->capturedAt;
    }

    public function setCapturedAt(\DateTimeImmutable $capturedAt): self
    {
        $this->capturedAt = $capturedAt;

        return $this;
    }

    /**
     * @return Collection<int, OpenImmoImport>|OpenImmoImport[]
     */
    public function getOpenImmoImports(): Collection|array
    {
        return $this->openImmoImports;
    }

    /**
     * @var Collection<int, OpenImmoImport>|OpenImmoImport[] $openImmoImports
     */
    public function setOpenImmoImports(Collection|array $openImmoImports): self
    {
        $this->openImmoImports = $openImmoImports;

        return $this;
    }
}
