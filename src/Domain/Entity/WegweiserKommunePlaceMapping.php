<?php

declare(strict_types=1);

namespace App\Domain\Entity;

use App\Repository\WegweiserKommunePlaceMappingRepository;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\UniqueConstraint;

#[Entity(repositoryClass: WegweiserKommunePlaceMappingRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[UniqueConstraint(name: 'account_place', columns: ['account_id', 'place_id'])]
#[HasLifecycleCallbacks]
class WegweiserKommunePlaceMapping
{
    use AccountTrait;
    use CreatedAtTrait;
    use UpdatedAtTrait;

    #[Id]
    #[GeneratedValue]
    #[Column(type: 'integer', nullable: false, options: ['unsigned' => true])]
    private ?int $id = null;

    #[ManyToOne]
    #[JoinColumn(nullable: false)]
    private Place $place;

    #[Column(type: 'integer', nullable: true, options: ['unsigned' => true])]
    private ?int $wegweiserKommuneRegionId = null;

    #[Column(type: 'string', length: 190, nullable: true)]
    private ?string $wegweiserKommuneRegionFriendlyUrl = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getPlace(): Place
    {
        return $this->place;
    }

    public function setPlace(Place $place): self
    {
        $this->place = $place;

        return $this;
    }

    public function getWegweiserKommuneRegionId(): ?int
    {
        return $this->wegweiserKommuneRegionId;
    }

    public function setWegweiserKommuneRegionId(?int $wegweiserKommuneRegionId): self
    {
        $this->wegweiserKommuneRegionId = $wegweiserKommuneRegionId;

        return $this;
    }

    public function getWegweiserKommuneRegionFriendlyUrl(): ?string
    {
        return $this->wegweiserKommuneRegionFriendlyUrl;
    }

    public function setWegweiserKommuneRegionFriendlyUrl(?string $wegweiserKommuneRegionFriendlyUrl): self
    {
        $this->wegweiserKommuneRegionFriendlyUrl = $wegweiserKommuneRegionFriendlyUrl;

        return $this;
    }
}
