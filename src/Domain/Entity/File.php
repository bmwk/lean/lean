<?php

declare(strict_types=1);

namespace App\Domain\Entity;

use App\Domain\Entity\AccountUser\AccountUser;
use App\Repository\FileRepository;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\Table;
use Symfony\Component\Uid\Uuid;

#[Entity(repositoryClass: FileRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[HasLifecycleCallbacks]
class File
{
    use AccountTrait;
    use CreatedAtTrait;
    use UpdatedAtTrait;

    #[Id]
    #[GeneratedValue]
    #[Column(type: 'integer', nullable: false, options: ['unsigned' => true])]
    private ?int $id = null;

    #[Column(type: 'uuid', unique: true, nullable: false)]
    private Uuid $uuid;

    #[Column(type: 'string', length: 190, nullable: false)]
    private string $fileName;

    #[Column(type: 'string', length: 10, nullable: true)]
    private ?string $fileExtension = null;

    #[Column(type: 'integer', nullable: false, options: ['unsigned' => true])]
    private int $fileSize;

    #[Column(type: 'string', length: 190, nullable: false)]
    private string $mimeType;

    #[Column(type: 'boolean', nullable: false)]
    private bool $public;

    #[ManyToOne]
    #[JoinColumn(nullable: true)]
    private ?AccountUser $createdByAccountUser = null;

    private ?string $fileContent = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getUuid(): Uuid
    {
        return $this->uuid;
    }

    public function setUuid(Uuid $uuid): self
    {
        $this->uuid = $uuid;

        return $this;
    }

    public function getFileName(): string
    {
        return $this->fileName;
    }

    public function setFileName(string $fileName): self
    {
        $this->fileName = $fileName;

        return $this;
    }

    public function getFileExtension(): ?string
    {
        return $this->fileExtension;
    }

    public function setFileExtension(?string $fileExtension): self
    {
        $this->fileExtension = $fileExtension;

        return $this;
    }

    public function getFileSize(): int
    {
        return $this->fileSize;
    }

    public function setFileSize(int $fileSize): self
    {
        $this->fileSize = $fileSize;

        return $this;
    }

    public function getMimeType(): string
    {
        return $this->mimeType;
    }

    public function setMimeType(string $mimeType): self
    {
        $this->mimeType = $mimeType;

        return $this;
    }

    public function isPublic(): bool
    {
        return $this->public;
    }

    public function setPublic(bool $public): self
    {
        $this->public = $public;

        return $this;
    }

    public function getCreatedByAccountUser(): ?AccountUser
    {
        return $this->createdByAccountUser;
    }

    public function setCreatedByAccountUser(?AccountUser $createdByAccountUser): self
    {
        $this->createdByAccountUser = $createdByAccountUser;

        return $this;
    }

    public function getFileContent(): ?string
    {
        return $this->fileContent;
    }

    public function setFileContent(?string $fileContent): self
    {
        $this->fileContent = $fileContent;

        return $this;
    }
}
