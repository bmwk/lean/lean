<?php

declare(strict_types=1);

namespace App\Domain\Entity;

enum FeedbackRelationship: int
{
    case PROPERTY_USER = 0;
    case PROPERTY_OWNER = 1;
}
