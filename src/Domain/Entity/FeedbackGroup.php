<?php

declare(strict_types=1);

namespace App\Domain\Entity;

use App\Repository\FeedbackGroupRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\UniqueConstraint;

#[Entity(repositoryClass: FeedbackGroupRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[UniqueConstraint(name: 'name_feedback_relationship', columns: ['name', 'feedback_relationship'])]
#[HasLifecycleCallbacks]
class FeedbackGroup
{
    use DeletedTrait;
    use CreatedAtTrait;
    use UpdatedAtTrait;

    #[Id]
    #[GeneratedValue]
    #[Column(type: 'integer', nullable: false, options: ['unsigned' => true])]
    private ?int $id = null;

    #[Column(type: 'string', length: 190, nullable: false)]
    private string $name;

    #[OneToMany(mappedBy: 'feedbackGroup', targetEntity: Feedback::class)]
    private Collection|array $feedbacks;

    #[Column(type: 'smallint', nullable: false, enumType: FeedbackRelationship::class, options: ['unsigned' => true])]
    private FeedbackRelationship $feedbackRelationship;

    #[Column(type: 'smallint', nullable: false, options: ['unsigned' => true])]
    private int $sortNumber;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getFeedbacks(): Collection|array
    {
        return $this->feedbacks;
    }

    public function setFeedbacks(Collection|array $feedbacks): self
    {
        $this->feedbacks = $feedbacks;

        return $this;
    }

    public function getFeedbackRelationship(): FeedbackRelationship
    {
        return $this->feedbackRelationship;
    }

    public function setFeedbackRelationship(FeedbackRelationship $feedbackRelationship): self
    {
        $this->feedbackRelationship = $feedbackRelationship;

        return $this;
    }

    public function getSortNumber(): int
    {
        return $this->sortNumber;
    }

    public function setSortNumber(int $sortNumber): self
    {
        $this->sortNumber = $sortNumber;

        return $this;
    }
}