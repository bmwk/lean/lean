<?php

declare(strict_types=1);

namespace App\Domain\Entity;

enum LocationCategory: int
{
    case ONE_A_LOCATION = 0;
    case ONE_B_LOCATION = 1;
    case TWO_A_LOCATION = 2;
    case TWO_B_LOCATION = 3;
    case OTHER = 4;
    case ONE_C_LOCATION = 5;
    case TWO_C_LOCATION = 6;

    public function getName(): string
    {
        return match ($this) {
            self::ONE_A_LOCATION => '1A-Lage',
            self::ONE_B_LOCATION => '1B-Lage',
            self::TWO_A_LOCATION => '2A-Lage',
            self::TWO_B_LOCATION => '2B-Lage',
            self::OTHER => 'Sonstige',
            self::ONE_C_LOCATION => '1C-Lage',
            self::TWO_C_LOCATION => '2C-Lage'
        };
    }
}
