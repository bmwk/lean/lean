<?php

declare(strict_types=1);

namespace App\Domain\Entity;

use App\Repository\DocumentTemplateRepository;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Table;

#[Entity(repositoryClass: DocumentTemplateRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[HasLifecycleCallbacks]
class DocumentTemplate extends AbstractFileLink
{
    #[Column(type: 'smallint', nullable: false, enumType: DocumentTemplateType::class, options: ['unsigned' => true])]
    private DocumentTemplateType $documentTemplateType;

    #[Column(type: 'string', length: 255, nullable: true)]
    private ?string $name = null;

    public function getDocumentTemplateType(): DocumentTemplateType
    {
        return $this->documentTemplateType;
    }

    public function setDocumentTemplateType(DocumentTemplateType $documentTemplateType): self
    {
        $this->documentTemplateType = $documentTemplateType;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }
}
