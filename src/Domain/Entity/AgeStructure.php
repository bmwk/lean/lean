<?php

declare(strict_types=1);

namespace App\Domain\Entity;

enum AgeStructure: int
{
    case YOUNG_PEOPLE = 0;
    case OLDER_PEOPLE = 1;
    case FAMILIES = 2;
    case CHILDREN = 3;

    public function getLabel(): string
    {
        return match ($this) {
            self::YOUNG_PEOPLE => 'Junge Menschen',
            self::OLDER_PEOPLE => 'Ältere Menschen',
            self::FAMILIES => 'Familien',
            self::CHILDREN => 'Kinder',
        };
    }
}
