<?php

declare(strict_types=1);

namespace App\Domain\Entity;

enum PropertyOfferType: int
{
    case PURCHASE = 0;
    case RENT = 1;
    case LEASE = 2;

    public function getName(): string
    {
        return match ($this) {
            self::PURCHASE => 'zum Kauf',
            self::RENT => 'zur Miete',
            self::LEASE => 'zur Pacht'
        };
    }
}
