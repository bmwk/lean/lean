<?php

declare(strict_types=1);

namespace App\Domain\Entity;

use App\Repository\FeedbackRepository;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\UniqueConstraint;

#[Entity(repositoryClass: FeedbackRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[UniqueConstraint(name: 'name_feedback_group', columns: ['name', 'feedback_group_id'])]
#[HasLifecycleCallbacks]
class Feedback
{
    use DeletedTrait;
    use CreatedAtTrait;
    use UpdatedAtTrait;

    #[Id]
    #[GeneratedValue]
    #[Column(type: 'integer', nullable: false, options: ['unsigned' => true])]
    private ?int $id = null;

    #[Column(type: 'string', length: 190, nullable: false)]
    private string $name;

    #[ManyToOne(inversedBy: 'feedbacks')]
    #[JoinColumn(nullable: false)]
    private FeedbackGroup $feedbackGroup;

    #[Column(type: 'smallint', nullable: false, options: ['unsigned' => true])]
    private int $sortNumber;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getFeedbackGroup(): FeedbackGroup
    {
        return $this->feedbackGroup;
    }

    public function setFeedbackGroup(FeedbackGroup $feedbackGroup): self
    {
        $this->feedbackGroup = $feedbackGroup;

        return $this;
    }

    public function getSortNumber(): int
    {
        return $this->sortNumber;
    }

    public function setSortNumber(int $sortNumber): self
    {
        $this->sortNumber = $sortNumber;

        return $this;
    }
}
