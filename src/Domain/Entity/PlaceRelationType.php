<?php

declare(strict_types=1);

namespace App\Domain\Entity;

enum PlaceRelationType: int
{
    case CHILD = 0;
    case PARENT = 1;
}
