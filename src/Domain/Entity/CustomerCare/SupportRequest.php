<?php

declare(strict_types=1);

namespace App\Domain\Entity\CustomerCare;

use App\Domain\Entity\Account;
use App\Domain\Entity\AccountUser\AccountUser;

class SupportRequest
{
    private string $fullName;

    private string $email;

    private Account $account;

    private AccountUser $accountUser;

    private string $subject;

    private string $message;

    public function getFullName(): string
    {
        return $this->fullName;
    }

    public function setFullName(string $fullName): self
    {
        $this->fullName = $fullName;

        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getAccount(): Account
    {
        return $this->account;
    }

    public function setAccount(Account $account): self
    {
        $this->account = $account;

        return $this;
    }

    public function getAccountUser(): AccountUser
    {
        return $this->accountUser;
    }

    public function setAccountUser(AccountUser $accountUser): self
    {
        $this->accountUser = $accountUser;

        return $this;
    }


    public function getSubject(): string
    {
        return $this->subject;
    }

    public function setSubject(string $subject): self
    {
        $this->subject = $subject;

        return $this;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }
}
