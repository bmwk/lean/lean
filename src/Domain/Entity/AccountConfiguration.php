<?php

declare(strict_types=1);

namespace App\Domain\Entity;

use App\Repository\AccountConfigurationRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\Table;

#[Entity(repositoryClass: AccountConfigurationRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[HasLifecycleCallbacks]
class AccountConfiguration
{
    #[Id]
    #[GeneratedValue]
    #[Column(type: Types::INTEGER, nullable: false, options: ['unsigned' => true])]
    private ?int $id = null;

    #[OneToOne(inversedBy: 'accountConfiguration')]
    #[JoinColumn(nullable: false)]
    private Account $account;

    #[Column(type: Types::BOOLEAN, nullable: true)]
    private ?bool $allowSeekerRegistration = null;

    #[Column(type: Types::BOOLEAN, nullable: true)]
    private ?bool $allowProviderRegistration = null;

    #[OneToOne]
    #[JoinColumn(nullable: true)]
    private ?Image $logoImage = null;

    #[Column(type: Types::STRING, length: 12, nullable: true)]
    private ?string $primaryColor = null;

    #[Column(type: Types::STRING, length: 12, nullable: true)]
    private ?string $secondaryColor = null;

    #[OneToOne]
    #[JoinColumn(nullable: true)]
    private ?Document $cityExpose = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getAccount(): Account
    {
        return $this->account;
    }

    public function setAccount(Account $account): self
    {
        $this->account = $account;

        return $this;
    }

    public function isAllowSeekerRegistration(): ?bool
    {
        return $this->allowSeekerRegistration;
    }

    public function setAllowSeekerRegistration(?bool $allowSeekerRegistration): self
    {
        $this->allowSeekerRegistration = $allowSeekerRegistration;

        return $this;
    }

    public function isAllowProviderRegistration(): ?bool
    {
        return $this->allowProviderRegistration;
    }

    public function setAllowProviderRegistration(?bool $allowProviderRegistration): self
    {
        $this->allowProviderRegistration = $allowProviderRegistration;

        return $this;
    }

    public function getLogoImage(): ?Image
    {
        return $this->logoImage;
    }

    public function setLogoImage(?Image $logoImage): self
    {
        $this->logoImage = $logoImage;

        return $this;
    }

    public function getPrimaryColor(): ?string
    {
        return $this->primaryColor;
    }

    public function setPrimaryColor(?string $primaryColor): self
    {
        $this->primaryColor = $primaryColor;

        return $this;
    }

    public function getSecondaryColor(): ?string
    {
        return $this->secondaryColor;
    }

    public function setSecondaryColor(?string $secondaryColor): self
    {
        $this->secondaryColor = $secondaryColor;

        return $this;
    }

    public function getCityExpose(): ?Document
    {
        return $this->cityExpose;
    }

    public function setCityExpose(?Document $cityExpose): self
    {
        $this->cityExpose = $cityExpose;

        return $this;
    }
}
