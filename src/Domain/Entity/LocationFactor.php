<?php

declare(strict_types=1);

namespace App\Domain\Entity;

enum LocationFactor: int
{
    case MEDICAL_CENTER = 0;
    case NEAR_FREEWAY = 1;
    case STATION = 2;
    case HOME_IMPROVEMENTS_STORE = 3;
    case CORNER_LOCATIONS = 4;
    case ENTRANCE_EXIT_ROADS_RING_ROADS = 5;
    case SHOPPING_STREETS = 6;
    case SHOPPING_CENTER_MALL = 7;
    case RETAIL_PARK = 8;
    case AIR_PORT = 9;
    case PEDESTRIAN_ZONE = 10;
    case INDUSTRIAL_PARK = 11;
    case METROPOLITAN_LOCATION = 12;
    case BUS_STOPS = 13;
    case MAIN_STREET = 14;
    case HIGH_TRAFFIC_LOCATION = 15;
    case DOWNTOWN = 16;
    case TRUCK_DELIVERY_POSSIBILITY = 17;
    case MARKETPLACE = 18;
    case LOCAL_SUPPLY_CENTER = 19;
    case SIDE_STREET = 20;
    case PUBLIC_TRANSPORT_CONNECTION = 21;
    case SELF_SERVICE_DEPARTMENT_STORES = 22;
    case SCHOOL_PROXIMITY = 23;
    case IN_CITY_DISTRICT = 24;
    case TOURIST_PLACES = 25;
    case PRE_CHECKOUT_ZONES = 26;
    case RESIDENTIAL_AREA_PROXIMITY = 27;
    case SUBURBAN_LOCATION = 28;
    case RETAIL_AGGLOMERATION = 29;

    public function getName(): string
    {
        return match ($this) {
            self::MEDICAL_CENTER => 'Ärztehaus',
            self::NEAR_FREEWAY => 'Autobahnnähe',
            self::STATION => 'Bahnhof',
            self::HOME_IMPROVEMENTS_STORE => 'Baumärkte',
            self::CORNER_LOCATIONS => 'Ecklage',
            self::ENTRANCE_EXIT_ROADS_RING_ROADS => 'Ein-/Ausfallstraßen; Ringstraßen',
            self::SHOPPING_STREETS => 'Einkaufsstraßen',
            self::SHOPPING_CENTER_MALL => 'Einkaufszentrum/Mall',
            self::RETAIL_PARK => 'Fachmarktzentrum',
            self::AIR_PORT => 'Flughafen',
            self::PEDESTRIAN_ZONE => 'Fußgängerzone',
            self::INDUSTRIAL_PARK => 'Gewerbegebiet',
            self::METROPOLITAN_LOCATION => 'Großstadtlage',
            self::BUS_STOPS => 'Haltestellen',
            self::MAIN_STREET => 'Hauptstraße',
            self::HIGH_TRAFFIC_LOCATION => 'Hochfrequentierte Verkehrslage',
            self::DOWNTOWN => 'Innenstadt',
            self::TRUCK_DELIVERY_POSSIBILITY => 'LKW-Anliefermöglichkeit',
            self::MARKETPLACE => 'Marktplatz',
            self::LOCAL_SUPPLY_CENTER => 'Nahversorgungszentrum',
            self::SIDE_STREET => 'Nebenstraße',
            self::PUBLIC_TRANSPORT_CONNECTION => 'ÖPNV-Anschluss',
            self::SELF_SERVICE_DEPARTMENT_STORES => 'SB-Warenhäuser',
            self::SCHOOL_PROXIMITY => 'Schulnähe',
            self::IN_CITY_DISTRICT => 'Stadtteillage',
            self::TOURIST_PLACES => 'Touristische Plätze',
            self::PRE_CHECKOUT_ZONES => 'Vorkassenzonen',
            self::RESIDENTIAL_AREA_PROXIMITY => 'Wohngebietsnähe',
            self::SUBURBAN_LOCATION => 'Stadtrandlage',
            self::RETAIL_AGGLOMERATION => 'Fachmarktagglomeration'
        };
    }
}
