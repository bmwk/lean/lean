<?php

declare(strict_types=1);

namespace App\Domain\Entity;

use App\Repository\GeolocationMultiPolygonRepository;
use CrEOF\Spatial\PHP\Types\Geometry\MultiPolygon;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;

#[Entity(repositoryClass: GeolocationMultiPolygonRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[HasLifecycleCallbacks]
class GeolocationMultiPolygon
{
    use CreatedAtTrait;
    use UpdatedAtTrait;

    #[Id]
    #[GeneratedValue]
    #[Column(type: 'integer', nullable: false, options: ['unsigned' => true])]
    private ?int $id = null;

    #[Column(type: 'multi_polygon', nullable: false)]
    private MultiPolygon $multiPolygon;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getMultiPolygon(): MultiPolygon
    {
        return $this->multiPolygon;
    }

    public function setMultiPolygon(MultiPolygon $multiPolygon): self
    {
        $this->multiPolygon = $multiPolygon;

        return $this;
    }
}
