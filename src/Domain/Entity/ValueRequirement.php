<?php

declare(strict_types=1);

namespace App\Domain\Entity;

use App\Repository\ValueRequirementRepository;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;

#[Entity(repositoryClass: ValueRequirementRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
class ValueRequirement
{
    #[Id]
    #[GeneratedValue]
    #[Column(type: 'integer', nullable: false, options: ['unsigned' => true])]
    private ?int $id = null;

    #[Column(type: 'string', length: 190, nullable: false)]
    private string $name;

    #[Column(type: 'float', nullable: true)]
    private ?float $minimumValue = null;

    #[Column(type: 'float', nullable: true)]
    private ?float $maximumValue = null;

    public static function create(string $name, ?float $minimumValue, ?float $maximumValue): ValueRequirement
    {
        $valueRequirement = new self();

        $valueRequirement->name = $name;
        $valueRequirement->minimumValue = $minimumValue;
        $valueRequirement->maximumValue = $maximumValue;

        return $valueRequirement;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getMinimumValue(): ?float
    {
        return $this->minimumValue;
    }

    public function setMinimumValue(?float $minimumValue): self
    {
        $this->minimumValue = $minimumValue;

        return $this;
    }

    public function getMaximumValue(): ?float
    {
        return $this->maximumValue;
    }

    public function setMaximumValue(?float $maximumValue): self
    {
        $this->maximumValue = $maximumValue;

        return $this;
    }
}
