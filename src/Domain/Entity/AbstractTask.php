<?php

declare(strict_types=1);

namespace App\Domain\Entity;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;

class AbstractTask
{
    use AccountTrait;
    use CreatedByAccountUserTrait;
    use CreatedAtTrait;
    use UpdatedAtTrait;

    #[Id]
    #[GeneratedValue]
    #[Column(type: 'integer', nullable: false, options: ['unsigned' => true])]
    protected ?int $id = null;

    #[Column(type: 'smallint', nullable: false, enumType: TaskStatus::class, options: ['unsigned' => true])]
    protected TaskStatus $taskStatus;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getTaskStatus(): TaskStatus
    {
        return $this->taskStatus;
    }

    public function setTaskStatus(TaskStatus $taskStatus): self
    {
        $this->taskStatus = $taskStatus;

        return $this;
    }
}
