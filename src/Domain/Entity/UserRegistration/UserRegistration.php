<?php

declare(strict_types=1);

namespace App\Domain\Entity\UserRegistration;

use App\Domain\Entity\AccountTrait;
use App\Domain\Entity\CreatedAtTrait;
use App\Domain\Entity\Person\PersonType;
use App\Domain\Entity\Person\Salutation;
use App\Domain\Entity\UpdatedAtTrait;
use App\Repository\UserRegistration\UserRegistrationRepository;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\Table;

#[Entity(repositoryClass: UserRegistrationRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[HasLifecycleCallbacks]
class UserRegistration
{
    use AccountTrait;
    use CreatedAtTrait;
    use UpdatedAtTrait;

    #[Id]
    #[GeneratedValue]
    #[Column(type: 'integer', nullable: false, options: ['unsigned' => true])]
    private ?int $id = null;

    #[Column(type: 'smallint', nullable: false, enumType: UserRegistrationType::class, options: ['unsigned' => true])]
    private UserRegistrationType $userRegistrationType;

    #[Column(type: 'smallint', nullable: false, enumType: UserRegistrationStatus::class, options: ['unsigned' => true])]
    private UserRegistrationStatus $userRegistrationStatus;

    #[OneToOne(inversedBy: 'userRegistration')]
    #[JoinColumn(nullable: true)]
    private ?UserInvitation $userInvitation = null;

    #[Column(type: 'smallint', nullable: false, enumType: PersonType::class, options: ['unsigned' => true])]
    private PersonType $personType;

    #[Column(type: 'string', length: 190, nullable: true)]
    private ?string $companyName = null;

    #[Column(type: 'smallint', nullable: true, enumType: Salutation::class, options: ['unsigned' => true])]
    private ?Salutation $salutation = null;

    #[Column(type: 'string', length: 190, nullable: false)]
    private string $lastName;

    #[Column(type: 'string', length: 190, nullable: false)]
    private string $firstName;

    #[Column(type: 'string', length: 190, nullable: false)]
    private string $streetName;

    #[Column(type: 'string', length: 20, nullable: false)]
    private string $houseNumber;

    #[Column(type: 'string', length: 20, nullable: false)]
    private string $postalCode;

    #[Column(type: 'string', length: 190, nullable: false)]
    private string $placeName;

    #[Column(type: 'string', length: 190, nullable: false)]
    private string $email;

    #[Column(type: 'string', length: 30, nullable: true)]
    private ?string $phoneNumber = null;

    #[Column(type: 'boolean', nullable: false)]
    private bool $gtcPrivacy;

    #[Column(type: 'datetime', nullable: true)]
    private ?\DateTime $expirationDate = null;

    #[Column(type: 'string', nullable: true)]
    private ?string $activationToken = null;

    #[Column(type: 'boolean', nullable: false)]
    private bool $emailConfirmed;

    public static function createFromUserInvitation(UserInvitation $userInvitation): self
    {
        $userRegistration = new self();

        $userRegistration->userInvitation = $userInvitation;
        $userRegistration->account = $userInvitation->getAccount();
        $userRegistration->userRegistrationType = $userInvitation->getUserRegistrationType();
        $userRegistration->userRegistrationStatus = UserRegistrationStatus::NOT_UNLOCKED;
        $userRegistration->personType = $userInvitation->getPersonType();
        $userRegistration->lastName = $userInvitation->getLastName();
        $userRegistration->firstName = $userInvitation->getFirstName();
        $userRegistration->email = $userInvitation->getEmail();
        $userRegistration->emailConfirmed = false;

        if ($userInvitation->getSalutation() !== null) {
            $userRegistration->salutation = $userInvitation->getSalutation();
        }

        if ($userInvitation->getCompanyName() !== null) {
            $userRegistration->companyName = $userInvitation->getCompanyName();
        }

        return $userRegistration;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getUserRegistrationType(): UserRegistrationType
    {
        return $this->userRegistrationType;
    }

    public function setUserRegistrationType(UserRegistrationType $userRegistrationType): self
    {
        $this->userRegistrationType = $userRegistrationType;

        return $this;
    }

    public function getUserRegistrationStatus(): UserRegistrationStatus
    {
        return $this->userRegistrationStatus;
    }

    public function setUserRegistrationStatus(UserRegistrationStatus $userRegistrationStatus): self
    {
        $this->userRegistrationStatus = $userRegistrationStatus;

        return $this;
    }

    public function getUserInvitation(): ?UserInvitation
    {
        return $this->userInvitation;
    }

    public function setUserInvitation(?UserInvitation $userInvitation): self
    {
        $this->userInvitation = $userInvitation;

        return $this;
    }

    public function getPersonType(): PersonType
    {
        return $this->personType;
    }

    public function setPersonType(PersonType $personType): self
    {
        $this->personType = $personType;

        return $this;
    }

    public function getCompanyName(): ?string
    {
        return $this->companyName;
    }

    public function setCompanyName(?string $companyName): self
    {
        $this->companyName = $companyName;

        return $this;
    }

    public function getSalutation(): ?Salutation
    {
        return $this->salutation;
    }

    public function setSalutation(?Salutation $salutation): self
    {
        $this->salutation = $salutation;

        return $this;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getStreetName(): string
    {
        return $this->streetName;
    }

    public function setStreetName(string $streetName): self
    {
        $this->streetName = $streetName;

        return $this;
    }

    public function getHouseNumber(): string
    {
        return $this->houseNumber;
    }

    public function setHouseNumber(string $houseNumber): self
    {
        $this->houseNumber = $houseNumber;

        return $this;
    }

    public function getPostalCode(): string
    {
        return $this->postalCode;
    }

    public function setPostalCode(string $postalCode): self
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    public function getPlaceName(): string
    {
        return $this->placeName;
    }

    public function setPlaceName(string $placeName): self
    {
        $this->placeName = $placeName;

        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(?string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function isGtcPrivacy(): bool
    {
        return $this->gtcPrivacy;
    }

    public function setGtcPrivacy(bool $gtcPrivacy): self
    {
        $this->gtcPrivacy = $gtcPrivacy;

        return $this;
    }

    public function getExpirationDate(): ?\DateTime
    {
        return $this->expirationDate;
    }

    public function setExpirationDate(?\DateTime $expirationDate): self
    {
        $this->expirationDate = $expirationDate;

        return $this;
    }

    public function getActivationToken(): ?string
    {
        return $this->activationToken;
    }

    public function setActivationToken(?string $activationToken): self
    {
        $this->activationToken = $activationToken;

        return $this;
    }

    public function isEmailConfirmed(): bool
    {
        return $this->emailConfirmed;
    }

    public function setEmailConfirmed(bool $emailConfirmed): self
    {
        $this->emailConfirmed = $emailConfirmed;

        return $this;
    }

    public function isAccountUserNotUnlocked(): bool
    {
        return ($this->getUserRegistrationStatus() === UserRegistrationStatus::NOT_UNLOCKED);
    }

    public function validateActivationData(): bool
    {
        return (
            $this->activationToken !== null
            && $this->expirationDate !== null
            && self::calculateDifferenceInHours(
                sourceDate: $this->expirationDate,
                targetDate: new \Datetime()
            ) >= 0
        );
    }

    private static function calculateDifferenceInHours(\DateTime $sourceDate, \DateTime $targetDate): int
    {
        $diffInterval = $sourceDate->diff($targetDate);

        return $diffInterval->h + $diffInterval->days * 24;
    }
}
