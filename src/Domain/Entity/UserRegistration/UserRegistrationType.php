<?php

declare(strict_types=1);

namespace App\Domain\Entity\UserRegistration;

enum UserRegistrationType: int
{
    case PROVIDER = 0;
    case SEEKER = 1;

    public function getName(): string
    {
        return match ($this) {
            self::PROVIDER => 'Anbietende',
            self::SEEKER => 'Suchende'
        };
    }
}
