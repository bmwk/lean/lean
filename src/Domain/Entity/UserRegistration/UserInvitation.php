<?php

declare(strict_types=1);

namespace App\Domain\Entity\UserRegistration;

use App\Domain\Entity\AccountTrait;
use App\Domain\Entity\CreatedAtTrait;
use App\Domain\Entity\CreatedByAccountUserTrait;
use App\Domain\Entity\Person\PersonType;
use App\Domain\Entity\Person\Salutation;
use App\Domain\Entity\UpdatedAtTrait;
use App\Repository\UserRegistration\UserInvitationRepository;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\Table;
use Symfony\Component\Uid\Uuid;

#[Entity(repositoryClass: UserInvitationRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[HasLifecycleCallbacks]
class UserInvitation
{
    private const DAYS_UNTIL_USER_INVITATION_OVERDUE = 7;

    use AccountTrait;
    use CreatedByAccountUserTrait;
    use CreatedAtTrait;
    use UpdatedAtTrait;

    #[Id]
    #[GeneratedValue]
    #[Column(type: 'integer', nullable: false, options: ['unsigned' => true])]
    private ?int $id = null;

    #[Column(type: 'uuid', unique: true, nullable: false)]
    private Uuid $uuid;

    #[Column(type: 'smallint', nullable: false, enumType: UserRegistrationType::class, options: ['unsigned' => true])]
    private UserRegistrationType $userRegistrationType;

    #[OneToOne(mappedBy: 'userInvitation')]
    private ?UserRegistration $userRegistration = null;

    #[Column(type: 'smallint', nullable: false, enumType: PersonType::class, options: ['unsigned' => true])]
    private PersonType $personType;

    #[Column(type: 'string', length: 190, nullable: true)]
    private ?string $companyName = null;

    #[Column(type: 'smallint', nullable: true, enumType: Salutation::class, options: ['unsigned' => true])]
    private ?Salutation $salutation = null;

    #[Column(type: 'string', length: 190, nullable: false)]
    private string $lastName;

    #[Column(type: 'string', length: 190, nullable: false)]
    private string $firstName;

    #[Column(type: 'string', length: 190, nullable: false)]
    private string $email;

    #[Column(type: 'datetime', nullable: true)]
    private ?\DateTime $resendDate = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getUuid(): Uuid
    {
        return $this->uuid;
    }

    public function setUuid(Uuid $uuid): self
    {
        $this->uuid = $uuid;

        return $this;
    }

    public function getUserRegistrationType(): UserRegistrationType
    {
        return $this->userRegistrationType;
    }

    public function setUserRegistrationType(UserRegistrationType $userRegistrationType): self
    {
        $this->userRegistrationType = $userRegistrationType;

        return $this;
    }

    public function getUserRegistration(): ?UserRegistration
    {
        return $this->userRegistration;
    }

    public function setUserRegistration(?UserRegistration $userRegistration): self
    {
        $this->userRegistration = $userRegistration;

        return $this;
    }

    public function getPersonType(): PersonType
    {
        return $this->personType;
    }

    public function setPersonType(PersonType $personType): self
    {
        $this->personType = $personType;

        return $this;
    }

    public function getCompanyName(): ?string
    {
        return $this->companyName;
    }

    public function setCompanyName(?string $companyName): self
    {
        $this->companyName = $companyName;

        return $this;
    }

    public function getSalutation(): ?Salutation
    {
        return $this->salutation;
    }

    public function setSalutation(?Salutation $salutation): self
    {
        $this->salutation = $salutation;

        return $this;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getResendDate(): ?\DateTime
    {
        return $this->resendDate;
    }

    public function setResendDate(?\DateTime $resendDate): self
    {
        $this->resendDate = $resendDate;

        return $this;
    }

    public function checkOverdue(): bool
    {
        if ($this->userRegistration !== null) {
            return false;
        }

        $currentDateTimeSubtracted = (new \DateTime())->sub(new \DateInterval(duration: 'P' . self::DAYS_UNTIL_USER_INVITATION_OVERDUE . 'D'));

        if (
            ($this->resendDate !== null && $this->resendDate > $currentDateTimeSubtracted)
            || ($this->resendDate === null && $this->createdAt > $currentDateTimeSubtracted)
        ) {
            return false;
        }

        return true;
    }
}
