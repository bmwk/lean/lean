<?php

declare(strict_types=1);

namespace App\Domain\Entity\UserRegistration;

enum UserRegistrationStatus: int
{
    case NOT_UNLOCKED = 0;
    case NOT_ACTIVE = 1;
    case ACTIVE = 2;
    case BLOCKED = 3;

    public function getName(): string
    {
        return match ($this) {
            self::NOT_UNLOCKED => 'Nicht freigeschaltet',
            self::NOT_ACTIVE => 'Nicht aktiviert',
            self::ACTIVE => 'Aktiv',
            self::BLOCKED => 'Gesperrt'
        };
    }
}
