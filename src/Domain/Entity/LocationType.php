<?php

declare(strict_types=1);

namespace App\Domain\Entity;

enum LocationType: int
{
    case INNER_CITY = 0;
    case CITY_DISTRICT = 1;
    case PERIPHERAL = 2;
    case HIGH_FREQUENCY_SPECIAL_LOCATION = 3;
    case SOLITARY = 4;

    public function getName(): string
    {
        return match ($this) {
            self::INNER_CITY => 'Innenstadtlage / City / Hauptzentrum (1er-Lage)',
            self::CITY_DISTRICT => 'Stadtteillage / Nebenzentrum / Nahversorgungszentrum (2er-Lage)',
            self::PERIPHERAL => 'Periphere Lage',
            self::HIGH_FREQUENCY_SPECIAL_LOCATION => 'Frequenzstarke Sonderlage',
            self::SOLITARY => 'Streu- und Solitärlagen'
        };
    }
}
