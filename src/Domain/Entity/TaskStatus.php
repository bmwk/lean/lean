<?php

declare(strict_types=1);

namespace App\Domain\Entity;

enum TaskStatus: int
{
    case CREATED = 0;
    case IN_PROGRESS = 1;
    case DONE = 2;
    case FAILED = 3;

    public function getName(): string
    {
        return match ($this) {
            self::CREATED => 'Erstellt',
            self::IN_PROGRESS => 'in Verarbeitung',
            self::DONE => 'Abgeschlossen',
            self::FAILED => 'Fehlerhaft'
        };
    }
}
