<?php

declare(strict_types=1);

namespace App\Domain\Entity\LookingForPropertyRequestReport;

class LookingForPropertyRequestReportStatistics
{
    private int $unprocessed;
    private int $processed;

    public function getUnprocessed(): int
    {
        return $this->unprocessed;
    }

    public function setUnprocessed(int $unprocessed): self
    {
        $this->unprocessed = $unprocessed;

        return $this;
    }

    public function getProcessed(): int
    {
        return $this->processed;
    }

    public function setProcessed(int $processed): self
    {
        $this->processed = $processed;

        return $this;
    }
}
