<?php

declare(strict_types=1);

namespace App\Domain\Entity\LookingForPropertyRequestReport;

use App\Domain\Entity\CreatedAtTrait;
use App\Domain\Entity\CreatedByAccountUserTrait;
use App\Domain\Entity\UpdatedAtTrait;
use App\Repository\LookingForPropertyRequestReport\LookingForPropertyRequestReportNoteRepository;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\Table;

#[Entity(repositoryClass: LookingForPropertyRequestReportNoteRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[HasLifecycleCallbacks]
class LookingForPropertyRequestReportNote
{
    use CreatedByAccountUserTrait;
    use CreatedAtTrait;
    use UpdatedAtTrait;

    #[Id]
    #[GeneratedValue]
    #[Column(type: 'integer', nullable: false, options: ['unsigned' => true])]
    private ?int $id = null;

    #[Column(type: 'text', nullable: false)]
    private string $note;

    #[ManyToOne(inversedBy: 'lookingForPropertyRequestReportNotes')]
    #[JoinColumn(nullable: false)]
    private LookingForPropertyRequestReport $lookingForPropertyRequestReport;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getNote(): string
    {
        return $this->note;
    }

    public function setNote(string $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function getLookingForPropertyRequestReport(): LookingForPropertyRequestReport
    {
        return $this->lookingForPropertyRequestReport;
    }

    public function setLookingForPropertyRequestReport(LookingForPropertyRequestReport $lookingForPropertyRequestReport): self
    {
        $this->lookingForPropertyRequestReport = $lookingForPropertyRequestReport;

        return $this;
    }
}
