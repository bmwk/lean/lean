<?php

declare(strict_types=1);

namespace App\Domain\Entity\LookingForPropertyRequestReport;

use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\AnonymizedTrait;
use App\Domain\Entity\LookingForPropertyRequest\AbstractLookingForPropertyRequest;
use App\Domain\Entity\LookingForPropertyRequest\LookingForPropertyRequest;
use App\Domain\Entity\Person\Salutation;
use App\Repository\LookingForPropertyRequestReport\LookingForPropertyRequestReportRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\Table;
use Symfony\Component\Uid\Uuid;

#[Entity(repositoryClass: LookingForPropertyRequestReportRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[HasLifecycleCallbacks]
class LookingForPropertyRequestReport extends AbstractLookingForPropertyRequest
{
    use AnonymizedTrait;

    #[Id]
    #[GeneratedValue]
    #[Column(type: 'integer', nullable: false, options: ['unsigned' => true])]
    private ?int $id = null;

    #[Column(type: 'uuid', unique: true, nullable: false)]
    private Uuid $uuid;

    #[Column(type: 'string', length: 190, nullable: true)]
    private ?string $reporterCompanyName = null;

    #[Column(type: 'smallint', nullable: true, enumType: Salutation::class, options: ['unsigned' => true])]
    private ?Salutation $reporterSalutation = null;

    #[Column(type: 'string', length: 190, nullable: true)]
    private ?string $reporterFirstName = null;

    #[Column(type: 'string', length: 190, nullable: false)]
    private string $reporterLastName;

    #[Column(type: 'string', length: 30, nullable: true)]
    private ?string $reporterPhoneNumber = null;

    #[Column(type: 'string', length: 190, nullable: true)]
    private ?string $reporterEmail = null;

    #[Column(type: 'datetime_immutable', nullable: false)]
    private \DateTimeImmutable $reportedAt;

    #[Column(type: 'boolean', nullable: false)]
    private bool $processed;

    #[Column(type: 'datetime_immutable', nullable: true)]
    private ?\DateTimeImmutable $processedAt = null;

    #[Column(type: 'boolean', nullable: false)]
    private bool $admitted;

    #[Column(type: 'boolean', nullable: false)]
    private bool $rejected;

    #[Column(type: 'uuid', unique:true, nullable: false)]
    private Uuid $confirmationToken;

    #[Column(type: 'boolean', nullable: false)]
    private bool $emailConfirmed;

    #[Column(type: 'datetime_immutable', nullable: true)]
    private ?\DateTimeImmutable $emailConfirmedAt = null;

    #[OneToOne(inversedBy: 'lookingForPropertyRequestReport')]
    #[JoinColumn(nullable: true)]
    private ?LookingForPropertyRequest $lookingForPropertyRequest = null;

    #[ManyToOne]
    #[JoinColumn(nullable: true)]
    private ?AccountUser $processedByAccountUser = null;

    #[OneToMany(mappedBy: 'lookingForPropertyRequestReport', targetEntity: LookingForPropertyRequestReportNote::class)]
    private Collection|array $lookingForPropertyRequestReportNotes;

    public function __construct()
    {
        $this->lookingForPropertyRequestReportNotes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getUuid(): Uuid
    {
        return $this->uuid;
    }

    public function setUuid(Uuid $uuid): self
    {
        $this->uuid = $uuid;

        return $this;
    }

    public function getReporterCompanyName(): ?string
    {
        return $this->reporterCompanyName;
    }

    public function setReporterCompanyName(?string $reporterCompanyName): self
    {
        $this->reporterCompanyName = $reporterCompanyName;

        return $this;
    }

    public function getReporterSalutation(): ?Salutation
    {
        return $this->reporterSalutation;
    }


    public function setReporterSalutation(?Salutation $reporterSalutation): self
    {
        $this->reporterSalutation = $reporterSalutation;

        return $this;
    }

    public function getReporterFirstName(): ?string
    {
        return $this->reporterFirstName;
    }


    public function setReporterFirstName(?string $reporterFirstName): self
    {
        $this->reporterFirstName = $reporterFirstName;

        return $this;
    }

    public function getReporterLastName(): string
    {
        return $this->reporterLastName;
    }

    public function setReporterLastName(string $reporterLastName): self
    {
        $this->reporterLastName = $reporterLastName;

        return $this;
    }

    public function getReporterPhoneNumber(): ?string
    {
        return $this->reporterPhoneNumber;
    }

    public function setReporterPhoneNumber(?string $reporterPhoneNumber): self
    {
        $this->reporterPhoneNumber = $reporterPhoneNumber;

        return $this;
    }

    public function getReporterEmail(): ?string
    {
        return $this->reporterEmail;
    }

    public function setReporterEmail(?string $reporterEmail): self
    {
        $this->reporterEmail = $reporterEmail;

        return $this;
    }

    public function getReportedAt(): \DateTimeImmutable
    {
        return $this->reportedAt;
    }

    public function setReportedAt(\DateTimeImmutable $reportedAt): self
    {
        $this->reportedAt = $reportedAt;

        return $this;
    }

    public function isProcessed(): bool
    {
        return $this->processed;
    }

    public function setProcessed(bool $processed): self
    {
        $this->processed = $processed;

        return $this;
    }

    public function getProcessedAt(): ?\DateTimeImmutable
    {
        return $this->processedAt;
    }

    public function setProcessedAt(?\DateTimeImmutable $processedAt): self
    {
        $this->processedAt = $processedAt;

        return $this;
    }

    public function isAdmitted(): bool
    {
        return $this->admitted;
    }

    public function setAdmitted(bool $admitted): self
    {
        $this->admitted = $admitted;

        return $this;
    }

    public function isRejected(): bool
    {
        return $this->rejected;
    }

    public function setRejected(bool $rejected): self
    {
        $this->rejected = $rejected;

        return $this;
    }

    public function getConfirmationToken(): Uuid
    {
        return $this->confirmationToken;
    }

    public function setConfirmationToken(Uuid $confirmationToken): self
    {
        $this->confirmationToken = $confirmationToken;

        return $this;
    }

    public function isEmailConfirmed(): bool
    {
        return $this->emailConfirmed;
    }

    public function setEmailConfirmed(bool $emailConfirmed): self
    {
        $this->emailConfirmed = $emailConfirmed;

        return $this;
    }

    public function getEmailConfirmedAt(): ?\DateTimeImmutable
    {
        return $this->emailConfirmedAt;
    }

    public function setEmailConfirmedAt(?\DateTimeImmutable $emailConfirmedAt): self
    {
        $this->emailConfirmedAt = $emailConfirmedAt;

        return $this;
    }

    public function getLookingForPropertyRequest(): ?LookingForPropertyRequest
    {
        return $this->lookingForPropertyRequest;
    }

    public function setLookingForPropertyRequest(?LookingForPropertyRequest $lookingForPropertyRequest): self
    {
        $this->lookingForPropertyRequest = $lookingForPropertyRequest;

        return $this;
    }

    public function getProcessedByAccountUser(): ?AccountUser
    {
        return $this->processedByAccountUser;
    }

    public function setProcessedByAccountUser(?AccountUser $processedByAccountUser): self
    {
        $this->processedByAccountUser = $processedByAccountUser;

        return $this;
    }

    public function getLookingForPropertyRequestReportNotes(): Collection|array
    {
        return $this->lookingForPropertyRequestReportNotes;
    }

    public function setLookingForPropertyRequestReportNotes(Collection|array $lookingForPropertyRequestReportNotes): self
    {
        $this->lookingForPropertyRequestReportNotes = $lookingForPropertyRequestReportNotes;

        return $this;
    }
}
