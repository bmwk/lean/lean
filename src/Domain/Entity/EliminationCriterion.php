<?php

declare(strict_types=1);

namespace App\Domain\Entity;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;

#[Entity(repositoryClass: EliminationCriterion::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
class EliminationCriterion
{
    #[Id]
    #[GeneratedValue]
    #[Column(type: 'integer', nullable: false, options: ['unsigned' => true])]
    private ?int $id = null;

    #[Column(type: 'string', length: 190, nullable: false)]
    private string $name;

    #[Column(name:'`set`', type: 'boolean', nullable: false)]
    private bool $set = false;

    #[Column(type: 'boolean', nullable: false)]
    private bool $evaluationResult = false;

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function isSet(): bool
    {
        return $this->set;
    }

    public function setSet(bool $set): self
    {
        $this->set = $set;
        return $this;
    }

    public function isEvaluationResult(): bool
    {
        return $this->evaluationResult;
    }

    public function setEvaluationResult(bool $evaluationResult): self
    {
        $this->evaluationResult = $evaluationResult;
        return $this;
    }
}