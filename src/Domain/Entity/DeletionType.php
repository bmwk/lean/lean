<?php

declare(strict_types=1);

namespace App\Domain\Entity;

enum DeletionType: int
{
    case SOFT = 0;
    case HARD = 1;
    case ANONYMIZATION = 2;
}
