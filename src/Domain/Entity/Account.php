<?php

declare(strict_types=1);

namespace App\Domain\Entity;

use App\Domain\Entity\OpenImmoImporter\AccountOpenImmoFtpUser;
use App\Domain\Entity\Person\PersonType;
use App\Domain\Entity\UserRegistration\UserRegistration;
use App\Repository\AccountRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\Table;

#[Entity(repositoryClass: AccountRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[HasLifecycleCallbacks]
class Account
{
    use DeletedTrait;
    use AnonymizedTrait;
    use CreatedAtTrait;
    use UpdatedAtTrait;

    #[Id]
    #[GeneratedValue]
    #[Column(type:  Types::INTEGER, nullable: false, options: ['unsigned' => true])]
    private ?int $id = null;

    #[Column(type: Types::STRING, length: 190, nullable: false)]
    private string $name;

    #[Column(type: Types::STRING, length: 190, nullable: false)]
    private string $email;

    #[Column(type: Types::BOOLEAN, nullable: false)]
    private bool $enabled;

    #[ManyToOne]
    #[JoinColumn(nullable: false)]
    private Place $assignedPlace;

    #[ManyToOne(inversedBy: 'subAccounts')]
    #[JoinColumn(nullable: true)]
    private ?Account $parentAccount = null;

    #[Column(type: Types::STRING, length: 190, nullable: true)]
    private ?string $streetName = null;

    #[Column(type: Types::STRING, length: 20, nullable: true)]
    private ?string $houseNumber = null;

    #[Column(type: Types::STRING, length: 20, nullable: true)]
    private ?string $postalCode = null;

    #[Column(type: Types::STRING, length: 190, nullable: true)]
    private ?string $placeName = null;

    #[Column(type: Types::STRING, length: 30, nullable: true)]
    private ?string $phoneNumber = null;

    #[Column(type: Types::STRING, length: 255, nullable: true)]
    private ?string $website = null;

    #[Column(type: Types::STRING, length: 190, unique: true, nullable: true)]
    private ?string $appHostname = null;

    #[OneToOne(mappedBy: 'account')]
    private ?AccountConfiguration $accountConfiguration = null;

    #[OneToOne(mappedBy: 'account', fetch: 'LAZY')]
    private ?AccountOpenImmoFtpUser $accountOpenImmoFtpUser = null;

    #[ManyToMany(targetEntity: WmsLayer::class)]
    private Collection|array $wmsLayers;

    #[OneToMany(mappedBy: 'parentAccount', targetEntity: Account::class)]
    private Collection|array $subAccounts;

    public static function createFromUserRegistration(UserRegistration $userRegistration): self
    {
        $account = new self();

        if ($userRegistration->getPersonType() === PersonType::COMPANY ) {
            $account->name = $userRegistration->getCompanyName();
        }

        if ($userRegistration->getPersonType() === PersonType::NATURAL_PERSON ) {
            $account->name = $userRegistration->getFirstName() . ' ' . $userRegistration->getLastName();
        }

        $account->assignedPlace = $userRegistration->getAccount()->getAssignedPlace();
        $account->email = $userRegistration->getEmail();
        $account->parentAccount = $userRegistration->getAccount();

        return $account;
    }

    public function __construct()
    {
        $this->wmsLayers = new ArrayCollection();
        $this->subAccounts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    public function getAssignedPlace(): Place
    {
        return $this->assignedPlace;
    }

    public function setAssignedPlace(Place $assignedPlace): self
    {
        $this->assignedPlace = $assignedPlace;

        return $this;
    }

    public function getParentAccount(): ?Account
    {
        return $this->parentAccount;
    }

    public function setParentAccount(?Account $parentAccount): self
    {
        $this->parentAccount = $parentAccount;

        return $this;
    }

    public function getStreetName(): ?string
    {
        return $this->streetName;
    }

    public function setStreetName(?string $streetName): self
    {
        $this->streetName = $streetName;

        return $this;
    }

    public function getHouseNumber(): ?string
    {
        return $this->houseNumber;
    }

    public function setHouseNumber(?string $houseNumber): self
    {
        $this->houseNumber = $houseNumber;

        return $this;
    }

    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }

    public function setPostalCode(?string $postalCode): self
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    public function getPlaceName(): ?string
    {
        return $this->placeName;
    }

    public function setPlaceName(?string $placeName): self
    {
        $this->placeName = $placeName;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(?string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    public function setWebsite(?string $website): self
    {
        $this->website = $website;

        return $this;
    }

    public function getAppHostname(): ?string
    {
        return $this->appHostname;
    }

    public function setAppHostname(?string $appHostname): self
    {
        $this->appHostname = $appHostname;

        return $this;
    }

    public function getAccountConfiguration(): ?AccountConfiguration
    {
        return $this->accountConfiguration;
    }

    public function setAccountConfiguration(?AccountConfiguration $accountConfiguration): self
    {
        $this->accountConfiguration = $accountConfiguration;

        return $this;
    }

    public function getAccountOpenImmoFtpUser(): ?AccountOpenImmoFtpUser
    {
        return $this->accountOpenImmoFtpUser;
    }

    public function setAccountOpenImmoFtpUser(?AccountOpenImmoFtpUser $accountOpenImmoFtpUser): self
    {
        $this->accountOpenImmoFtpUser = $accountOpenImmoFtpUser;

        return $this;
    }

    public function getWmsLayers(): Collection|array
    {
        return $this->wmsLayers;
    }

    public function setWmsLayers(Collection|array $wmsLayers): self
    {
        $this->wmsLayers = $wmsLayers;

        return $this;
    }

    public function getSubAccounts(): Collection|array
    {
        return $this->subAccounts;
    }

    public function setSubAccounts(Collection|array $subAccounts): self
    {
        $this->subAccounts = $subAccounts;

        return $this;
    }
}
