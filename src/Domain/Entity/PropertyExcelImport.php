<?php

declare(strict_types=1);

namespace App\Domain\Entity;

use App\Repository\PropertyExcelImportRepository;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\Table;

#[Entity(repositoryClass: PropertyExcelImportRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[HasLifecycleCallbacks]
class PropertyExcelImport extends AbstractFileLink
{
    #[OneToOne(mappedBy: 'propertyExcelImport')]
    private ?ProcessPropertyExcelImportTask $processPropertyExcelImportTask = null;

    #[Column(type: 'json', nullable: true)]
    private ?array $validationErrors = null;

    public function getProcessPropertyExcelImportTask(): ?ProcessPropertyExcelImportTask
    {
        return $this->processPropertyExcelImportTask;
    }

    public function setProcessPropertyExcelImportTask(?ProcessPropertyExcelImportTask $processPropertyExcelImportTask): self
    {
        $this->processPropertyExcelImportTask = $processPropertyExcelImportTask;

        return $this;
    }

    public function getValidationErrors(): ?array
    {
        return $this->validationErrors;
    }

    public function setValidationErrors(?array $validationErrors): self
    {
        $this->validationErrors = $validationErrors;

        return $this;
    }
}
