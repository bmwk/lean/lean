<?php

declare(strict_types=1);

namespace App\Domain\Entity\Classification;

use App\Repository\Classification\ClassificationExampleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\Table;

#[Entity(repositoryClass: ClassificationExampleRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[HasLifecycleCallbacks]
class ClassificationExample
{
    #[Id]
    #[GeneratedValue]
    #[Column(type: 'smallint', nullable: false, options: ['unsigned' => true])]
    private ?int $id = null;

    #[Column(type: 'string', length: 255, nullable: false)]
    private string $brandName;

    /**
     * @var Collection<int, IndustryClassification>|IndustryClassification[]
     */
    #[ManyToMany(targetEntity: IndustryClassification::class)]
    private Collection|array $industryClassifications;

    /**
     * @var Collection<int, NaceClassification>|NaceClassification[]
     */
    #[ManyToMany(targetEntity: NaceClassification::class)]
    private Collection|array $naceClassifications;

    public function __construct()
    {
        $this->industryClassifications = new ArrayCollection();
        $this->naceClassifications = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getBrandName(): string
    {
        return $this->brandName;
    }

    public function setBrandName(string $brandName): self
    {
        $this->brandName = $brandName;

        return $this;
    }

    /**
     * @return Collection<int, IndustryClassification>|IndustryClassification[]
     */
    public function getIndustryClassifications(): Collection|array
    {
        return $this->industryClassifications;
    }

    /**
     * @param Collection<int, IndustryClassification>|IndustryClassification[] $industryClassifications
     */
    public function setIndustryClassifications(Collection|array $industryClassifications): self
    {
        $this->industryClassifications = $industryClassifications;

        return $this;
    }

    /**
     * @return Collection<int, NaceClassification>|NaceClassification[]
     */
    public function getNaceClassifications(): Collection|array
    {
        return $this->naceClassifications;
    }

    /**
     * @param Collection<int, NaceClassification>|NaceClassification[] $naceClassifications
     */
    public function setNaceClassifications(Collection|array $naceClassifications): self
    {
        $this->naceClassifications = $naceClassifications;

        return $this;
    }
}
