<?php

declare(strict_types=1);

namespace App\Domain\Entity\Classification;

enum NaceClassificationLevel: int
{
    case LEVEL_ONE = 1;
    case LEVEL_TWO = 2;
    case LEVEL_THREE = 3;
    case LEVEL_FOUR = 4;
    case LEVEL_FIVE = 5;
}
