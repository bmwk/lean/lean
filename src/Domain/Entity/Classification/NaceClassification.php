<?php

declare(strict_types=1);

namespace App\Domain\Entity\Classification;

use App\Repository\Classification\NaceClassificationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\Table;

#[Entity(repositoryClass: NaceClassificationRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[HasLifecycleCallbacks]
class NaceClassification
{
    #[Id]
    #[GeneratedValue]
    #[Column(type: 'integer', nullable: false, options: ['unsigned' => true])]
    private ?int $id = null;

    #[Column(type: 'string', nullable: false)]
    private string $name;

    #[Column(type: 'string', length: 10, nullable: false)]
    private string $code;

    #[Column(type: 'smallint', nullable: false, enumType: NaceClassificationLevel::class, options: ['unsigned' => true])]
    private NaceClassificationLevel $level;

    #[ManyToOne(inversedBy: 'children')]
    #[JoinColumn(nullable: true)]
    private ?NaceClassification $parent = null;

    /**
     * @var Collection<int, NaceClassification>|NaceClassification[]
     */
    #[OneToMany(mappedBy: 'parent', targetEntity: NaceClassification::class)]
    private Collection|array $children;

    /**
     * @var Collection<int, IndustryClassification>|IndustryClassification[]
     */
    #[ManyToMany(targetEntity: IndustryClassification::class, mappedBy: 'naceClassifications')]
    private Collection|array $industryClassifications;

    public function __construct() {
        $this->children = new ArrayCollection();
        $this->industryClassifications = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getLevel(): NaceClassificationLevel
    {
        return $this->level;
    }

    public function setLevel(NaceClassificationLevel $level): self
    {
        $this->level = $level;

        return $this;
    }

    public function getParent(): ?NaceClassification
    {
        return $this->parent;
    }

    public function setParent(?NaceClassification $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return  Collection<int, NaceClassification>|NaceClassification[]
     */
    public function getChildren(): Collection|array
    {
        return $this->children;
    }

    /**
     * @param Collection<int, NaceClassification>|NaceClassification[] $children
     */
    public function setChildren(Collection|array $children): self
    {
        $this->children = $children;

        return $this;
    }

    /**
     * @return  Collection<int, IndustryClassification>|IndustryClassification[]
     */
    public function getIndustryClassifications(): Collection|array
    {
        return $this->industryClassifications;
    }

    /**
     * @param Collection<int, IndustryClassification>|IndustryClassification[] $industryClassifications
     */
    public function setIndustryClassifications(Collection|array $industryClassifications): self
    {
        $this->industryClassifications = $industryClassifications;

        return $this;
    }
}
