<?php

declare(strict_types=1);

namespace App\Domain\Entity\Classification;

use App\Repository\Classification\IndustryClassificationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\Table;

#[Entity(repositoryClass: IndustryClassificationRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[HasLifecycleCallbacks]
class IndustryClassification
{
    #[Id]
    #[GeneratedValue]
    #[Column(type: 'integer', nullable: false, options: ['unsigned' => true])]
    private ?int $id = null;

    #[Column(type: 'text', nullable: false)]
    private string $name;

    #[Column(type: 'integer', nullable: false, options: ['unsigned' => true])]
    private int $levelOne;

    #[Column(type: 'integer', nullable: true, options: ['unsigned' => true])]
    private ?int $levelTwo = null;

    #[Column(type: 'integer', nullable: true, options: ['unsigned' => true])]
    private ?int $levelThree = null;

    #[Column(type: 'text', nullable: true)]
    private ?string $exampleSubSector = null;

    #[ManyToOne(inversedBy: 'children')]
    #[JoinColumn(nullable: true)]
    private ?IndustryClassification $parent = null;

    /**
     * @var Collection<int, IndustryClassification>|IndustryClassification[]
     */
    #[OneToMany(mappedBy: 'parent', targetEntity: self::class)]
    private Collection|array $children;

    /**
     * @var Collection<int, NaceClassification>|NaceClassification[]
     */
    #[ManyToMany(targetEntity: NaceClassification::class, inversedBy: 'industryClassifications')]
    private Collection|array $naceClassifications;

    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->naceClassifications = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLevelOne(): int
    {
        return $this->levelOne;
    }

    public function setLevelOne(int $levelOne): self
    {
        $this->levelOne = $levelOne;

        return $this;
    }

    public function getLevelTwo(): ?int
    {
        return $this->levelTwo;
    }

    public function setLevelTwo(?int $levelTwo): self
    {
        $this->levelTwo = $levelTwo;

        return $this;
    }

    public function getLevelThree(): ?int
    {
        return $this->levelThree;
    }

    public function setLevelThree(?int $levelThree): self
    {
        $this->levelThree = $levelThree;

        return $this;
    }

    public function getExampleSubSector(): ?string
    {
        return $this->exampleSubSector;
    }

    public function setExampleSubSector(?string $exampleSubSector): self
    {
        $this->exampleSubSector = $exampleSubSector;

        return $this;
    }

    public function getParent(): ?IndustryClassification
    {
        return $this->parent;
    }

    public function setParent(?IndustryClassification $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return Collection<int, IndustryClassification>|IndustryClassification[]
     */
    public function getChildren(): Collection|array
    {
        return $this->children;
    }

    /**
     * @param Collection<int, IndustryClassification>|IndustryClassification[] $children
     */
    public function setChildren(Collection|array $children): self
    {
        $this->children = $children;

        return $this;
    }

    /**
     * @return  Collection<int, NaceClassification>|NaceClassification[]
     */
    public function getNaceClassifications(): Collection|array
    {
        return $this->naceClassifications;
    }

    /**
     * @param Collection<int, NaceClassification>|NaceClassification[] $naceClassifications
     */
    public function setNaceClassifications(Collection|array $naceClassifications): self
    {
        $this->naceClassifications = $naceClassifications;

        return $this;
    }
}
