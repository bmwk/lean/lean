<?php

declare(strict_types=1);

namespace App\Domain\Entity\LookingForPropertyRequest;

use App\Domain\Entity\AccountTrait;
use App\Domain\Entity\CreatedAtTrait;
use App\Domain\Entity\DeletedTrait;
use App\Domain\Entity\LocationFactor;
use App\Domain\Entity\PriceRequirement;
use App\Domain\Entity\RequestReason;
use App\Domain\Entity\UpdatedAtTrait;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\OneToOne;

abstract class AbstractLookingForPropertyRequest
{
    use AccountTrait;
    use DeletedTrait;
    use CreatedAtTrait;
    use UpdatedAtTrait;

    #[OneToOne(fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    protected PropertyRequirement $propertyRequirement;

    #[OneToOne(fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    protected PriceRequirement $priceRequirement;

    #[Column(type: Types::JSON, nullable: true, enumType: LocationFactor::class)]
    protected ?array $locationFactors = null;

    #[Column(type: Types::STRING, nullable: false)]
    protected string $title;

    #[Column(type: Types::TEXT, nullable: true)]
    protected ?string $conceptDescription = null;

    #[Column(type: Types::SMALLINT, nullable: false, enumType: RequestReason::class, options: ['unsigned' => true])]
    protected RequestReason $requestReason;

    #[Column(type: Types::DATE_MUTABLE, nullable: true)]
    protected ?\DateTime $requestAvailableTill = null;

    #[Column(type: Types::BOOLEAN, nullable: false)]
    protected bool $automaticEnding = false;

    public function getPropertyRequirement(): PropertyRequirement
    {
        return $this->propertyRequirement;
    }

    public function setPropertyRequirement(PropertyRequirement $propertyRequirement): self
    {
        $this->propertyRequirement = $propertyRequirement;

        return $this;
    }

    public function getPriceRequirement(): PriceRequirement
    {
        return $this->priceRequirement;
    }

    public function setPriceRequirement(PriceRequirement $priceRequirement): self
    {
        $this->priceRequirement = $priceRequirement;

        return $this;
    }

    public function getLocationFactors(): ?array
    {
        return $this->locationFactors;
    }

    public function setLocationFactors(?array $locationFactors): self
    {
        $this->locationFactors = $locationFactors;

        return $this;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getConceptDescription(): ?string
    {
        return $this->conceptDescription;
    }

    public function setConceptDescription(?string $conceptDescription): self
    {
        $this->conceptDescription = $conceptDescription;

        return $this;
    }

    public function getRequestReason(): RequestReason
    {
        return $this->requestReason;
    }

    public function setRequestReason(RequestReason $requestReason): self
    {
        $this->requestReason = $requestReason;

        return $this;
    }

    public function getRequestAvailableTill(): ?\DateTime
    {
        return $this->requestAvailableTill;
    }

    public function setRequestAvailableTill(?\DateTime $requestAvailableTill): self
    {
        $this->requestAvailableTill = $requestAvailableTill;

        return $this;
    }

    public function isAutomaticEnding(): bool
    {
        return $this->automaticEnding;
    }

    public function setAutomaticEnding(bool $automaticEnding): self
    {
        $this->automaticEnding = $automaticEnding;

        return $this;
    }
}
