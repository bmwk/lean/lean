<?php

declare(strict_types=1);

namespace App\Domain\Entity\LookingForPropertyRequest;

class LookingForPropertyRequestsStatistics
{
    private int $active;
    private int $inactive;
    private int $archived;
    private int $assignedToCurrentUser;
    private int $createdByCurrentUser;

    public function getActive(): int
    {
        return $this->active;
    }

    public function setActive(int $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getInactive(): int
    {
        return $this->inactive;
    }

    public function setInactive(int $inactive): self
    {
        $this->inactive = $inactive;

        return $this;
    }

    public function getArchived(): int
    {
        return $this->archived;
    }

    public function setArchived(int $archived): self
    {
        $this->archived = $archived;

        return $this;
    }

    public function getAssignedToCurrentUser(): int
    {
        return $this->assignedToCurrentUser;
    }

    public function setAssignedToCurrentUser(int $assignedToCurrentUser): self
    {
        $this->assignedToCurrentUser = $assignedToCurrentUser;

        return $this;
    }

    public function getCreatedByCurrentUser(): int
    {
        return $this->createdByCurrentUser;
    }

    public function setCreatedByCurrentUser(int $createdByCurrentUser): self
    {
        $this->createdByCurrentUser = $createdByCurrentUser;

        return $this;
    }
}
