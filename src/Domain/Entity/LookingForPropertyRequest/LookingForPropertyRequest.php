<?php

declare(strict_types=1);

namespace App\Domain\Entity\LookingForPropertyRequest;

use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\ArchivedTrait;
use App\Domain\Entity\CreatedByAccountUserTrait;
use App\Domain\Entity\LookingForPropertyRequestReport\LookingForPropertyRequestReport;
use App\Domain\Entity\Person\Person;
use App\Domain\Entity\PriceRequirement;
use App\Repository\LookingForPropertyRequest\LookingForPropertyRequestRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\Table;

#[Entity(repositoryClass: LookingForPropertyRequestRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[HasLifecycleCallbacks]
class LookingForPropertyRequest extends AbstractLookingForPropertyRequest
{
    use CreatedByAccountUserTrait;
    use ArchivedTrait;

    #[Id]
    #[GeneratedValue]
    #[Column(type: Types::INTEGER, nullable: false, options: ['unsigned' => true])]
    private ?int $id = null;

    #[ManyToOne(inversedBy: 'lookingForPropertyRequests')]
    #[JoinColumn(nullable: false)]
    private Person $person;

    #[ManyToOne]
    #[JoinColumn(nullable: true)]
    private ?AccountUser $assignedToAccountUser = null;

    /**
     * @var Collection<int, MatchingResult >|MatchingResult[]
     */
    #[OneToMany(mappedBy: 'lookingForPropertyRequest', targetEntity: MatchingResult::class)]
    private Collection|array $matchingResults;

    /**
     * @var Collection<int, LookingForPropertyRequestExposeForwarding >|LookingForPropertyRequestExposeForwarding[]
     */
    #[OneToMany(mappedBy: 'lookingForPropertyRequest', targetEntity: LookingForPropertyRequestExposeForwarding::class)]
    private Collection|array $lookingForPropertyRequestExposeForwardings;

    /**
     * @var Collection<int, LookingForPropertyRequestMatchingResponse>|LookingForPropertyRequestMatchingResponse[]
     */
    #[OneToMany(mappedBy: 'lookingForPropertyRequest', targetEntity: LookingForPropertyRequestMatchingResponse::class)]
    private Collection|array $lookingForPropertyRequestMatchingResponses;

    #[OneToOne(mappedBy: 'lookingForPropertyRequest')]
    private ?LookingForPropertyRequestReport $lookingForPropertyRequestReport = null;

    public static function createFromLookingForPropertyRequestReport(LookingForPropertyRequestReport $lookingForPropertyRequestReport): self
    {
        $lookingForPropertyRequest = new self();

        $lookingForPropertyRequest->propertyRequirement = PropertyRequirement::createFromLookingForPropertyRequestReport(lookingForPropertyRequestReport: $lookingForPropertyRequestReport);
        $lookingForPropertyRequest->priceRequirement = PriceRequirement::createFromLookingForPropertyRequestReport(lookingForPropertyRequestReport: $lookingForPropertyRequestReport);
        $lookingForPropertyRequest->locationFactors = $lookingForPropertyRequestReport->getLocationFactors();
        $lookingForPropertyRequest->title = $lookingForPropertyRequestReport->getTitle();
        $lookingForPropertyRequest->conceptDescription = $lookingForPropertyRequestReport->getConceptDescription();
        $lookingForPropertyRequest->requestReason = $lookingForPropertyRequestReport->getRequestReason();
        $lookingForPropertyRequest->requestAvailableTill = $lookingForPropertyRequestReport->getRequestAvailableTill();
        $lookingForPropertyRequest->automaticEnding = $lookingForPropertyRequestReport->isAutomaticEnding();
        $lookingForPropertyRequest->person = Person::createFromLookingForPropertyRequestReport(lookingForPropertyRequestReport: $lookingForPropertyRequestReport);

        return $lookingForPropertyRequest;
    }

    public function __construct()
    {
        $this->matchingResults = new ArrayCollection();
        $this->lookingForPropertyRequestExposeForwardings = new ArrayCollection();
        $this->lookingForPropertyRequestMatchingResponses = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getPerson(): Person
    {
        return $this->person;
    }

    public function setPerson(Person $person): self
    {
        $this->person = $person;

        return $this;
    }

    public function getAssignedToAccountUser(): ?AccountUser
    {
        return $this->assignedToAccountUser;
    }

    public function setAssignedToAccountUser(?AccountUser $assignedToAccountUser): self
    {
        $this->assignedToAccountUser = $assignedToAccountUser;

        return $this;
    }

    /**
     * @return  Collection<int, MatchingResult >|MatchingResult[]
     */
    public function getMatchingResults(): Collection|array
    {
        return $this->matchingResults;
    }

    /**
     * @param  Collection<int, MatchingResult >|MatchingResult[] $matchingResults
     */
    public function setMatchingResults(Collection|array $matchingResults): self
    {
        $this->matchingResults = $matchingResults;

        return $this;
    }

    /**
     * @return  Collection<int, LookingForPropertyRequestExposeForwarding >|LookingForPropertyRequestExposeForwarding[]
     */
    public function getLookingForPropertyRequestExposeForwardings(): Collection|array
    {
        return $this->lookingForPropertyRequestExposeForwardings;
    }

    /**
     * @param  Collection<int, LookingForPropertyRequestExposeForwarding >|LookingForPropertyRequestExposeForwarding[] $lookingForPropertyRequestExposeForwardings
     */
    public function setLookingForPropertyRequestExposeForwardings(Collection|array $lookingForPropertyRequestExposeForwardings): self
    {
        $this->lookingForPropertyRequestExposeForwardings = $lookingForPropertyRequestExposeForwardings;

        return $this;
    }

    /**
     * @return  Collection<int, LookingForPropertyRequestMatchingResponse>|LookingForPropertyRequestMatchingResponse[]
     */
    public function getLookingForPropertyRequestMatchingResponses(): Collection|array
    {
        return $this->lookingForPropertyRequestMatchingResponses;
    }

    /**
     * @param Collection<int, LookingForPropertyRequestMatchingResponse>|LookingForPropertyRequestMatchingResponse[] $lookingForPropertyRequestMatchingResponses
     */
    public function setLookingForPropertyRequestMatchingResponses(Collection|array $lookingForPropertyRequestMatchingResponses): self
    {
        $this->lookingForPropertyRequestMatchingResponses = $lookingForPropertyRequestMatchingResponses;

        return $this;
    }

    public function getLookingForPropertyRequestReport(): ?LookingForPropertyRequestReport
    {
        return $this->lookingForPropertyRequestReport;
    }

    public function setLookingForPropertyRequestReport(?LookingForPropertyRequestReport $lookingForPropertyRequestReport): self
    {
        $this->lookingForPropertyRequestReport = $lookingForPropertyRequestReport;

        return $this;
    }

    public function fetchLastMatchingResult(): ?MatchingResult
    {
        $lastMatchingResult = null;

        $matchingResults = $this->matchingResults->filter(
            p: function (MatchingResult $matchingResult): bool {
                return $matchingResult->isEliminated() == false;
            }
        )->toArray();

        foreach ($matchingResults as $matchingResult) {
            if ($lastMatchingResult === null || $lastMatchingResult->getCreatedAt() < $matchingResult->getCreatedAt()) {
                $lastMatchingResult = $matchingResult;
            }
        }

        return $lastMatchingResult;
    }

    public function numberOfMatches(): int
    {
        return $this->matchingResults->filter(
            p: function (MatchingResult $matchingResult): bool {
                return $matchingResult->isEliminated() == false;
            }
        )->count();
    }
}
