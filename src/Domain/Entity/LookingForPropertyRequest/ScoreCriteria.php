<?php

declare(strict_types=1);

namespace App\Domain\Entity\LookingForPropertyRequest;

use App\Domain\Entity\MinMaxScoreCriterion;
use App\Domain\Entity\ScoreCriterion;
use App\Domain\Entity\ScoreCriterionInterface;
use App\Repository\ScoreCriteriaRepository;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\Table;

#[Entity(repositoryClass: ScoreCriteriaRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
class ScoreCriteria
{
    #[Id]
    #[GeneratedValue]
    #[Column(type: 'integer', nullable: false, options: ['unsigned' => true])]
    private ?int $id = null;

    #[OneToOne(targetEntity: MinMaxScoreCriterion::class, fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private ScoreCriterionInterface $space;

    #[OneToOne(targetEntity: ScoreCriterion::class, fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private ScoreCriterionInterface $roomCount;

    #[OneToOne(targetEntity: ScoreCriterion::class, fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private ScoreCriterionInterface $numberOfParkingLots;

    #[OneToOne(targetEntity: ScoreCriterion::class, fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private ScoreCriterionInterface $availableFrom;

    #[OneToOne(targetEntity: MinMaxScoreCriterion::class, fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private ScoreCriterionInterface $subsidiarySpace;

    #[OneToOne(targetEntity: MinMaxScoreCriterion::class, fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private ScoreCriterionInterface $retailSpace;

    #[OneToOne(targetEntity: MinMaxScoreCriterion::class, fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private ScoreCriterionInterface $outdoorSpace;

    #[OneToOne(targetEntity: MinMaxScoreCriterion::class, fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private ScoreCriterionInterface $storageSpace;

    #[OneToOne(targetEntity: MinMaxScoreCriterion::class, fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private ScoreCriterionInterface $shopWindowLength;

    #[OneToOne(targetEntity: MinMaxScoreCriterion::class, fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private ScoreCriterionInterface $shopWidth;

    #[OneToOne(targetEntity: ScoreCriterion::class, fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private ScoreCriterionInterface $locationCategory;

    #[OneToOne(targetEntity: ScoreCriterion::class, fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private ScoreCriterionInterface $maximumPrice;

    #[OneToOne(targetEntity: ScoreCriterion::class, fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private ScoreCriterionInterface $minimumPrice;

    #[OneToOne(targetEntity: ScoreCriterion::class, fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private ScoreCriterionInterface $industryDensity;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getRoomCount(): ScoreCriterionInterface
    {
        return $this->roomCount;
    }

    public function setRoomCount(ScoreCriterionInterface $roomCount): self
    {
        $this->roomCount = $roomCount;

        return $this;
    }

    public function getNumberOfParkingLots(): ScoreCriterionInterface
    {
        return $this->numberOfParkingLots;
    }

    public function setNumberOfParkingLots(ScoreCriterionInterface $numberOfParkingLots): self
    {
        $this->numberOfParkingLots = $numberOfParkingLots;

        return $this;
    }

    public function getAvailableFrom(): ScoreCriterionInterface
    {
        return $this->availableFrom;
    }

    public function setAvailableFrom(ScoreCriterionInterface $availableFrom): self
    {
        $this->availableFrom = $availableFrom;

        return $this;
    }

    public function getSubsidiarySpace(): ScoreCriterionInterface
    {
        return $this->subsidiarySpace;
    }

    public function setSubsidiarySpace(ScoreCriterionInterface $subsidiarySpace): self
    {
        $this->subsidiarySpace = $subsidiarySpace;

        return $this;
    }

    public function getRetailSpace(): ScoreCriterionInterface
    {
        return $this->retailSpace;
    }

    public function setRetailSpace(ScoreCriterionInterface $retailSpace): self
    {
        $this->retailSpace = $retailSpace;

        return $this;
    }

    public function getSpace(): ScoreCriterionInterface
    {
        return $this->space;
    }

    public function setSpace(ScoreCriterionInterface $space): self
    {
        $this->space = $space;

        return $this;
    }

    public function getOutdoorSpace(): ScoreCriterionInterface
    {
        return $this->outdoorSpace;
    }

    public function setOutdoorSpace(ScoreCriterionInterface $outdoorSpace): self
    {
        $this->outdoorSpace = $outdoorSpace;

        return $this;
    }

    public function getStorageSpace(): ScoreCriterionInterface
    {
        return $this->storageSpace;
    }

    public function setStorageSpace(ScoreCriterionInterface $storageSpace): self
    {
        $this->storageSpace = $storageSpace;

        return $this;
    }

    public function getShopWindowLength(): ScoreCriterionInterface
    {
        return $this->shopWindowLength;
    }

    public function setShopWindowLength(ScoreCriterionInterface $shopWindowLength): self
    {
        $this->shopWindowLength = $shopWindowLength;

        return $this;
    }

    public function getShopWidth(): ScoreCriterionInterface
    {
        return $this->shopWidth;
    }

    public function setShopWidth(ScoreCriterionInterface $shopWidth): self
    {
        $this->shopWidth = $shopWidth;

        return $this;
    }

    public function getLocationCategory(): ScoreCriterionInterface
    {
        return $this->locationCategory;
    }

    public function setLocationCategory(ScoreCriterionInterface $locationCategory): self
    {
        $this->locationCategory = $locationCategory;

        return $this;
    }

    public function getMaximumPrice(): ScoreCriterionInterface
    {
        return $this->maximumPrice;
    }

    public function setMaximumPrice(ScoreCriterionInterface $maximumPrice): self
    {
        $this->maximumPrice = $maximumPrice;

        return $this;
    }

    public function getMinimumPrice(): ScoreCriterionInterface
    {
        return $this->minimumPrice;
    }

    public function setMinimumPrice(ScoreCriterionInterface $minimumPrice): self
    {
        $this->minimumPrice = $minimumPrice;

        return $this;
    }

    public function getIndustryDensity(): ScoreCriterionInterface
    {
        return $this->industryDensity;
    }

    public function setIndustryDensity(ScoreCriterionInterface $industryDensity): self
    {
        $this->industryDensity = $industryDensity;

        return $this;
    }

    public function calculatePoints(): int
    {
        $pointSum = 0;

        if ($this->space->isSet() && $this->space->isEvaluationResult()) {
            $pointSum += $this->space->getPoints();
        }

        if ($this->roomCount->isSet() && $this->roomCount->isEvaluationResult()) {
            $pointSum += $this->roomCount->getPoints();
        }

        if ($this->numberOfParkingLots->isSet() && $this->numberOfParkingLots->isEvaluationResult()) {
            $pointSum += $this->numberOfParkingLots->getPoints();
        }

        if ($this->availableFrom->isSet() && $this->availableFrom->isEvaluationResult()) {
            $pointSum += $this->availableFrom->getPoints();
        }

        if ($this->subsidiarySpace->isSet() && $this->subsidiarySpace->isEvaluationResult()) {
            $pointSum += $this->subsidiarySpace->getPoints();
        }

        if ($this->retailSpace->isSet() && $this->retailSpace->isEvaluationResult()) {
            $pointSum += $this->retailSpace->getPoints();
        }

        if ($this->outdoorSpace->isSet() && $this->outdoorSpace->isEvaluationResult()) {
            $pointSum += $this->outdoorSpace->getPoints();
        }

        if ($this->storageSpace->isSet() && $this->storageSpace->isEvaluationResult()) {
            $pointSum += $this->storageSpace->getPoints();
        }

        if ($this->shopWindowLength->isSet() && $this->shopWindowLength->isEvaluationResult()) {
            $pointSum += $this->shopWindowLength->getPoints();
        }

        if ($this->shopWidth->isSet() && $this->shopWidth->isEvaluationResult()) {
            $pointSum += $this->shopWidth->getPoints();
        }

        if ($this->locationCategory->isSet() && $this->locationCategory->isEvaluationResult()) {
            $pointSum += $this->locationCategory->getPoints();
        }

        if ($this->maximumPrice->isSet() && $this->maximumPrice->isEvaluationResult()) {
            $pointSum += $this->maximumPrice->getPoints();
        }

        if ($this->minimumPrice->isSet() && $this->minimumPrice->isEvaluationResult()) {
            $pointSum += $this->minimumPrice->getPoints();
        }

        if ($this->industryDensity->isSet() && $this->industryDensity->isEvaluationResult()) {
            $pointSum += $this->industryDensity->getPoints();
        }

        return $pointSum;
    }

    public function calculateMaxPoints(): int
    {
        $pointSum = 0;

        if ($this->space->isSet()) {
            $pointSum += $this->space->getMaxPoints();
        }

        if ($this->roomCount->isSet()) {
            $pointSum += $this->roomCount->getMaxPoints();
        }

        if ($this->numberOfParkingLots->isSet()) {
            $pointSum += $this->numberOfParkingLots->getMaxPoints();
        }

        if ($this->availableFrom->isSet()) {
            $pointSum += $this->availableFrom->getMaxPoints();
        }

        if ($this->subsidiarySpace->isSet()) {
            $pointSum += $this->subsidiarySpace->getMaxPoints();
        }

        if ($this->retailSpace->isSet()) {
            $pointSum += $this->retailSpace->getMaxPoints();
        }

        if ($this->outdoorSpace->isSet()) {
            $pointSum += $this->outdoorSpace->getMaxPoints();
        }

        if ($this->storageSpace->isSet()) {
            $pointSum += $this->storageSpace->getMaxPoints();
        }

        if ($this->shopWindowLength->isSet()) {
            $pointSum += $this->shopWindowLength->getMaxPoints();
        }

        if ($this->shopWidth->isSet()) {
            $pointSum += $this->shopWidth->getMaxPoints();
        }

        if ($this->locationCategory->isSet()) {
            $pointSum += $this->locationCategory->getMaxPoints();
        }

        if ($this->maximumPrice->isSet()) {
            $pointSum += $this->maximumPrice->getMaxPoints();
        }

        if ($this->minimumPrice->isSet()) {
            $pointSum += $this->minimumPrice->getMaxPoints();
        }

        if ($this->industryDensity->isSet()) {
            $pointSum += $this->industryDensity->getMaxPoints();
        }

        return $pointSum;
    }
}
