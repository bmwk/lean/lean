<?php

declare(strict_types=1);

namespace App\Domain\Entity\LookingForPropertyRequest;

use App\Domain\Entity\Property\AbstractBuildingUnitFilter;

class MatchingFilter extends AbstractBuildingUnitFilter
{
    private ?\DateTime $lastMatchingFrom = null;

    private ?\DateTime $lastMatchingTill = null;

    private ?array $matchingStatus = null;

    public function getLastMatchingFrom(): ?\DateTime
    {
        return $this->lastMatchingFrom;
    }

    public function setLastMatchingFrom(?\DateTime $lastMatchingFrom): self
    {
        $this->lastMatchingFrom = $lastMatchingFrom;

        return $this;
    }

    public function getLastMatchingTill(): ?\DateTime
    {
        return $this->lastMatchingTill;
    }

    public function setLastMatchingTill(?\DateTime $lastMatchingTill): self
    {
        $this->lastMatchingTill = $lastMatchingTill;

        return $this;
    }

    public function getMatchingStatus(): ?array
    {
        return $this->matchingStatus;
    }

    public function setMatchingStatus(?array $matchingStatus): self
    {
        $this->matchingStatus = $matchingStatus;

        return $this;
    }

    public function isFilterActive(): bool
    {
        if (
            $this->areaSizeMinimum === null
            && $this->areaSizeMaximum === null
            && $this->shopWindowFrontWidthMinimum === null
            && $this->shopWindowFrontWidthMaximum === null
            && $this->lastMatchingFrom === null
            && $this->lastMatchingTill === null
            && $this->assignedToAccountUser === null
            && $this->barrierFreeAccess === null
            && empty($this->propertyOfferTypes) === true
            && empty($this->industryClassifications) === true
            && empty($this->propertyStatus) === true
            && empty($this->matchingStatus) === true
        ) {
            return false;
        }

        return true;
    }
}
