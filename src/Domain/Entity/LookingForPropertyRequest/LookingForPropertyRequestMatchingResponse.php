<?php

declare(strict_types=1);

namespace App\Domain\Entity\LookingForPropertyRequest;

use App\Domain\Entity\Matching\AbstractMatchingResponse;
use App\Domain\Entity\Property\BuildingUnit;
use App\Repository\LookingForPropertyRequest\LookingForPropertyRequestMatchingResponseRepository;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\UniqueConstraint;

#[Entity(repositoryClass: LookingForPropertyRequestMatchingResponseRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[UniqueConstraint(name: 'looking_for_property_request_building_unit', columns: ['looking_for_property_request_id', 'building_unit_id'])]
#[HasLifecycleCallbacks]
class LookingForPropertyRequestMatchingResponse extends AbstractMatchingResponse
{
    #[ManyToOne(inversedBy: 'lookingForPropertyRequestMatchingResponses')]
    #[JoinColumn(nullable: false)]
    protected BuildingUnit $buildingUnit;

    #[ManyToOne(inversedBy: 'lookingForPropertyRequestMatchingResponses')]
    #[JoinColumn(nullable: false)]
    private LookingForPropertyRequest $lookingForPropertyRequest;

    public function getLookingForPropertyRequest(): LookingForPropertyRequest
    {
        return $this->lookingForPropertyRequest;
    }

    public function setLookingForPropertyRequest(LookingForPropertyRequest $lookingForPropertyRequest): self
    {
        $this->lookingForPropertyRequest = $lookingForPropertyRequest;

        return $this;
    }
}
