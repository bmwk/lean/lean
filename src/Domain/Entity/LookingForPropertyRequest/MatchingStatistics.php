<?php

declare(strict_types=1);

namespace App\Domain\Entity\LookingForPropertyRequest;

class MatchingStatistics
{
    private \DateTime $lastMatchingDate;
    private int $exposeForwardings;
    private int $averageNumberOfResultsPerMatching;

    public function getLastMatchingDate(): \DateTime
    {
        return $this->lastMatchingDate;
    }

    public function setLastMatchingDate(\DateTime $lastMatchingDate): self
    {
        $this->lastMatchingDate = $lastMatchingDate;

        return $this;
    }

    public function getExposeForwardings(): int
    {
        return $this->exposeForwardings;
    }

    public function setExposeForwardings(int $exposeForwardings): self
    {
        $this->exposeForwardings = $exposeForwardings;

        return $this;
    }

    public function getAverageNumberOfResultsPerMatching(): int
    {
        return $this->averageNumberOfResultsPerMatching;
    }

    public function setAverageNumberOfResultsPerMatching(int $averageNumberOfResultsPerMatching): self
    {
        $this->averageNumberOfResultsPerMatching = $averageNumberOfResultsPerMatching;

        return $this;
    }
}
