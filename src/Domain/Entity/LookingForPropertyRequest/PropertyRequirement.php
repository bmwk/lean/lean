<?php

declare(strict_types=1);

namespace App\Domain\Entity\LookingForPropertyRequest;

use App\Domain\Entity\Classification\IndustryClassification;
use App\Domain\Entity\CreatedAtTrait;
use App\Domain\Entity\LocationCategory;
use App\Domain\Entity\LookingForPropertyRequestReport\LookingForPropertyRequestReport;
use App\Domain\Entity\Place;
use App\Domain\Entity\PropertyMandatoryRequirement;
use App\Domain\Entity\PropertyValueRangeRequirement;
use App\Domain\Entity\UpdatedAtTrait;
use App\Repository\PropertyRequirementRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\Table;

#[Entity(repositoryClass: PropertyRequirementRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[HasLifecycleCallbacks]
class PropertyRequirement
{
    use CreatedAtTrait;
    use UpdatedAtTrait;

    #[Id]
    #[GeneratedValue]
    #[Column(type: 'integer', nullable: false, options: ['unsigned' => true])]
    private ?int $id = null;

    #[OneToOne(fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private PropertyMandatoryRequirement $propertyMandatoryRequirement;

    #[OneToOne(fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private PropertyValueRangeRequirement $propertyValueRangeRequirement;

    #[ManyToOne(fetch: 'EAGER')]
    #[JoinColumn(nullable: true)]
    private ?IndustryClassification $industryClassification = null;

    #[Column(type: 'integer', nullable: true, options: ['unsigned' => true])]
    private ?int $numberOfParkingLots = null;

    #[Column(type: 'integer', nullable: true, options: ['unsigned' => true])]
    private ?int $roomCount = null;

    #[Column(type: 'json', nullable: true, enumType: LocationCategory::class)]
    private ?array $locationCategories = null;

    #[Column(type: 'date', nullable: true)]
    private ?\DateTime $earliestAvailableFrom = null;

    #[Column(type: 'date', nullable: true)]
    private ?\DateTime $latestAvailableFrom = null;

    #[ManyToMany(targetEntity: Place::class)]
    #[JoinTable(name: 'property_requirement_quarter')]
    private Collection|array $inQuarters;

    #[ManyToMany(targetEntity: Place::class)]
    private Collection|array $inPlaces;

    public static function createFromLookingForPropertyRequestReport(LookingForPropertyRequestReport $lookingForPropertyRequestReport): self
    {
        $propertyRequirementFromLookingForPropertyRequestReport = $lookingForPropertyRequestReport->getPropertyRequirement();

        $propertyRequirement = new self();

        $propertyRequirement->propertyMandatoryRequirement = PropertyMandatoryRequirement::createFromLookingForPropertyRequestReport(lookingForPropertyRequestReport: $lookingForPropertyRequestReport);
        $propertyRequirement->propertyValueRangeRequirement = PropertyValueRangeRequirement::createFromLookingForPropertyRequestReport(lookingForPropertyRequestReport: $lookingForPropertyRequestReport);
        $propertyRequirement->industryClassification = $propertyRequirementFromLookingForPropertyRequestReport->getIndustryClassification();
        $propertyRequirement->numberOfParkingLots = $propertyRequirementFromLookingForPropertyRequestReport->getNumberOfParkingLots();
        $propertyRequirement->roomCount = $propertyRequirementFromLookingForPropertyRequestReport->getRoomCount();
        $propertyRequirement->locationCategories = $propertyRequirementFromLookingForPropertyRequestReport->getLocationCategories();
        $propertyRequirement->earliestAvailableFrom = $propertyRequirementFromLookingForPropertyRequestReport->getEarliestAvailableFrom();
        $propertyRequirement->latestAvailableFrom = $propertyRequirementFromLookingForPropertyRequestReport->getLatestAvailableFrom();
        $propertyRequirement->inQuarters = $propertyRequirementFromLookingForPropertyRequestReport->getInQuarters();
        $propertyRequirement->inPlaces = $propertyRequirementFromLookingForPropertyRequestReport->getInPlaces();

        return $propertyRequirement;
    }

    public function __construct()
    {
        $this->inQuarters = new ArrayCollection();
        $this->inPlaces = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getPropertyMandatoryRequirement(): PropertyMandatoryRequirement
    {
        return $this->propertyMandatoryRequirement;
    }

    public function setPropertyMandatoryRequirement(PropertyMandatoryRequirement $propertyMandatoryRequirement): self
    {
        $this->propertyMandatoryRequirement = $propertyMandatoryRequirement;

        return $this;
    }

    public function getPropertyValueRangeRequirement(): PropertyValueRangeRequirement
    {
        return $this->propertyValueRangeRequirement;
    }

    public function setPropertyValueRangeRequirement(PropertyValueRangeRequirement $propertyValueRangeRequirement): self
    {
        $this->propertyValueRangeRequirement = $propertyValueRangeRequirement;

        return $this;
    }

    public function getIndustryClassification(): ?IndustryClassification
    {
        return $this->industryClassification;
    }

    public function setIndustryClassification(?IndustryClassification $industryClassification): self
    {
        $this->industryClassification = $industryClassification;

        return $this;
    }

    public function getNumberOfParkingLots(): ?int
    {
        return $this->numberOfParkingLots;
    }

    public function setNumberOfParkingLots(?int $numberOfParkingLots): self
    {
        $this->numberOfParkingLots = $numberOfParkingLots;

        return $this;
    }

    public function getRoomCount(): ?int
    {
        return $this->roomCount;
    }

    public function setRoomCount(?int $roomCount): self
    {
        $this->roomCount = $roomCount;

        return $this;
    }

    public function getLocationCategories(): ?array
    {
        return $this->locationCategories;
    }

    public function setLocationCategories(?array $locationCategories): self
    {
        $this->locationCategories = $locationCategories;

        return $this;
    }

    public function getEarliestAvailableFrom(): ?\DateTime
    {
        return $this->earliestAvailableFrom;
    }

    public function setEarliestAvailableFrom(?\DateTime $earliestAvailableFrom): self
    {
        $this->earliestAvailableFrom = $earliestAvailableFrom;

        return $this;
    }

    public function getLatestAvailableFrom(): ?\DateTime
    {
        return $this->latestAvailableFrom;
    }

    public function setLatestAvailableFrom(?\DateTime $latestAvailableFrom): self
    {
        $this->latestAvailableFrom = $latestAvailableFrom;

        return $this;
    }

    public function getInQuarters(): Collection|array
    {
        return $this->inQuarters;
    }

    public function setInQuarters(Collection|array $inQuarters): self
    {
        $this->inQuarters = $inQuarters;

        return $this;
    }

    public function getInPlaces(): Collection|array
    {
        return $this->inPlaces;
    }

    public function setInPlaces(Collection|array $inPlaces): self
    {
        $this->inPlaces = $inPlaces;

        return $this;
    }
}
