<?php

declare(strict_types=1);

namespace App\Domain\Entity\LookingForPropertyRequest;

use App\Domain\Entity\AbstractExposeForwarding;
use App\Domain\Entity\EliminationCriteria;
use App\Domain\Entity\Matching\AbstractMatchingResponse;
use App\Domain\Entity\Matching\AbstractMatchingResult;
use App\Domain\Entity\Property\BuildingUnit;
use App\Repository\LookingForPropertyRequest\MatchingResultRepository;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\UniqueConstraint;

#[Entity(repositoryClass: MatchingResultRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[UniqueConstraint(name: 'building_unit_looking_for_property_request', columns: ['building_unit_id', 'looking_for_property_request_id'])]
#[HasLifecycleCallbacks]
class MatchingResult extends AbstractMatchingResult
{
    #[ManyToOne(inversedBy: 'matchingResults')]
    #[JoinColumn(nullable: false)]
    protected BuildingUnit $buildingUnit;

    #[ManyToOne(inversedBy: 'matchingResults')]
    #[JoinColumn(nullable: false)]
    private LookingForPropertyRequest $lookingForPropertyRequest;

    #[OneToOne(fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private EliminationCriteria $eliminationCriteria;

    #[OneToOne(fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private ScoreCriteria $scoreCriteria;

    public function getLookingForPropertyRequest(): LookingForPropertyRequest
    {
        return $this->lookingForPropertyRequest;
    }

    public function setLookingForPropertyRequest(LookingForPropertyRequest $lookingForPropertyRequest): self
    {
        $this->lookingForPropertyRequest = $lookingForPropertyRequest;

        return $this;
    }

    public function getEliminationCriteria(): EliminationCriteria
    {
        return $this->eliminationCriteria;
    }

    public function setEliminationCriteria(EliminationCriteria $eliminationCriteria): self
    {
        $this->eliminationCriteria = $eliminationCriteria;

        return $this;
    }

    public function getScoreCriteria(): ScoreCriteria
    {
        return $this->scoreCriteria;
    }

    public function setScoreCriteria(ScoreCriteria $scoreCriteria): self
    {
        $this->scoreCriteria = $scoreCriteria;

        return $this;
    }

    public function fetchExposeForwarding(): ?AbstractExposeForwarding
    {
        return $this->lookingForPropertyRequest->getLookingForPropertyRequestExposeForwardings()->findFirst(
            p: function (int $key, LookingForPropertyRequestExposeForwarding $lookingForPropertyRequestExposeForwarding): bool {
                return $lookingForPropertyRequestExposeForwarding->getBuildingUnit() === $this->getBuildingUnit();
            }
        );
    }

    public function fetchMatchingResponse(): ?AbstractMatchingResponse
    {
        return $this->lookingForPropertyRequest->getLookingForPropertyRequestMatchingResponses()->findFirst(
            p: function (int $key, LookingForPropertyRequestMatchingResponse $lookingForPropertyRequestMatchingResponse): bool {
                return $lookingForPropertyRequestMatchingResponse->getBuildingUnit() === $this->getBuildingUnit();
            }
        );
    }
}
