<?php

declare(strict_types=1);

namespace App\Domain\Entity\LookingForPropertyRequest;

use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\Classification\IndustryClassification;
use App\Domain\Entity\RequestReason;

class LookingForPropertyRequestFilter
{
    /**
     * @var RequestReason[]|null
     */
    private ?array $requestReasons = null;

    /**
     * @var IndustryClassification[]|null
     */
    private ?array $industryClassifications = null;

    private ?float $areaSizeMinimum = null;

    private ?float $areaSizeMaximum = null;

    private ?\DateTime $lastUpdatedFrom = null;

    private ?\DateTime $lastUpdatedTill = null;

    private ?AccountUser $assignedToAccountUser = null;

    /**
     * @return RequestReason[]|null
     */
    public function getRequestReasons(): ?array
    {
        return $this->requestReasons;
    }

    /**
     * @param RequestReason[]|null $requestReasons
     */
    public function setRequestReasons(?array $requestReasons): self
    {
        $this->requestReasons = $requestReasons;

        return $this;
    }

    /**
     * @return IndustryClassification[]|null
     */
    public function getIndustryClassifications(): ?array
    {
        return $this->industryClassifications;
    }

    /**
     * @param IndustryClassification[]|null $industryClassifications
     */
    public function setIndustryClassifications(?array $industryClassifications): self
    {
        $this->industryClassifications = $industryClassifications;

        return $this;
    }

    public function getAreaSizeMinimum(): ?float
    {
        return $this->areaSizeMinimum;
    }

    public function setAreaSizeMinimum(?float $areaSizeMinimum): self
    {
        $this->areaSizeMinimum = $areaSizeMinimum;

        return $this;
    }

    public function getAreaSizeMaximum(): ?float
    {
        return $this->areaSizeMaximum;
    }

    public function setAreaSizeMaximum(?float $areaSizeMaximum): self
    {
        $this->areaSizeMaximum = $areaSizeMaximum;

        return $this;
    }

    public function getLastUpdatedFrom(): ?\DateTime
    {
        return $this->lastUpdatedFrom;
    }

    public function setLastUpdatedFrom(?\DateTime $lastUpdatedFrom): self
    {
        $this->lastUpdatedFrom = $lastUpdatedFrom;

        return $this;
    }

    public function getLastUpdatedTill(): ?\DateTime
    {
        return $this->lastUpdatedTill;
    }

    public function setLastUpdatedTill(?\DateTime $lastUpdatedTill): self
    {
        $this->lastUpdatedTill = $lastUpdatedTill;

        return $this;
    }

    public function getAssignedToAccountUser(): ?AccountUser
    {
        return $this->assignedToAccountUser;
    }

    public function setAssignedToAccountUser(?AccountUser $assignedToAccountUser): self
    {
        $this->assignedToAccountUser = $assignedToAccountUser;

        return $this;
    }

    public function isFilterActive(): bool
    {
        if (
            empty($this->requestReasons) === true
            && empty($this->industryClassifications) === true
            && $this->assignedToAccountUser === null
        ) {
            return false;
        }

        return true;
    }
}
