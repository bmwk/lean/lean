<?php

declare(strict_types=1);

namespace App\Domain\Entity;

use App\Repository\GeolocationPointRepository;
use CrEOF\Spatial\PHP\Types\Geometry\Point;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;

#[Entity(repositoryClass: GeolocationPointRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[HasLifecycleCallbacks]
class GeolocationPoint
{
    use CreatedAtTrait;
    use UpdatedAtTrait;

    #[Id]
    #[GeneratedValue]
    #[Column(type: 'integer', nullable: false, options: ['unsigned' => true])]
    private ?int $id = null;

    #[Column(type: 'point', nullable: false)]
    private Point $point;

    public static function createFromLatitudeAndLongitude(float $latitude, float $longitude): self
    {
        $geolocationPoint = new self();

        $geolocationPoint->point = new Point($longitude, $latitude);

        return $geolocationPoint;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getPoint(): Point
    {
        return $this->point;
    }

    public function setPoint(Point $point): self
    {
        $this->point = $point;

        return $this;
    }
}
