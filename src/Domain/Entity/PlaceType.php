<?php

declare(strict_types=1);

namespace App\Domain\Entity;

enum PlaceType: int
{
    case STATE = 0;
    case COUNTY = 1;
    case MUNICIPAL_ASSOCIATION = 2;
    case MUNICIPALITY = 3;
    case CITY = 4;
    case COUNTY_SEAT = 5;
    case MARKET_MUNICIPAL = 6;
    case COUNTY_BOROUGH = 7;
    case COUNTY_CAPITAL = 8;
    case HANSE_CITY = 9;
    case FLECKEN = 10;
    case UNIFIED_MUNICIPALITY = 11;
    case LOCAL_CONGREGATION = 12;
    case WORLD_HERITAGE_CITY = 13;
    case CITY_QUARTER = 14;
    case NEIGHBOURHOOD = 15;
    case BOROUGH = 16;
    case POSTAL_CODE = 17;
    case COUNTRY = 18;

    public function getName(): string
    {
        return match ($this) {
            self::STATE => 'Bundesland',
            self::COUNTY => 'Landkreis',
            self::MUNICIPAL_ASSOCIATION => 'Gemeindeverband',
            self::MUNICIPALITY => 'Gemeinde',
            self::CITY => 'Stadt',
            self::COUNTY_SEAT => 'Kreisstadt',
            self::MARKET_MUNICIPAL => 'Marktgemeinde',
            self::COUNTY_BOROUGH => 'kreisfreie Stadt',
            self::COUNTY_CAPITAL => 'Landeshauptstadt',
            self::HANSE_CITY => 'Hansestadt',
            self::FLECKEN => 'Flecken',
            self::UNIFIED_MUNICIPALITY => 'Einheitsgemeinde',
            self::LOCAL_CONGREGATION => 'Ortsgemeinde',
            self::WORLD_HERITAGE_CITY => 'Welterbestadt',
            self::CITY_QUARTER => 'Stadtteil',
            self::NEIGHBOURHOOD => 'Ortsteil',
            self::BOROUGH => 'Stadtbezirk',
            self::POSTAL_CODE => 'Postleitzahl',
            self::COUNTRY => 'Land'
        };
    }
}
