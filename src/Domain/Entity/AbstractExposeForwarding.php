<?php

declare(strict_types=1);

namespace App\Domain\Entity;

use App\Domain\Entity\Property\BuildingUnit;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;

abstract class AbstractExposeForwarding
{
    use CreatedByAccountUserTrait;
    use CreatedAtTrait;
    use UpdatedAtTrait;

    #[Id]
    #[GeneratedValue]
    #[Column(type: Types::INTEGER, nullable: false, options: ['unsigned' => true])]
    protected ?int $id = null;

    #[ManyToOne]
    #[JoinColumn(nullable: false)]
    protected BuildingUnit $buildingUnit;

    #[Column(type: Types::SMALLINT, nullable: true, enumType: ForwardingResponse::class, options: ['unsigned' => true])]
    protected ?ForwardingResponse $forwardingResponse = null;

    #[Column(type: Types::TEXT, nullable: true)]
    protected ?string $rejectReason = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getBuildingUnit(): BuildingUnit
    {
        return $this->buildingUnit;
    }

    public function setBuildingUnit(BuildingUnit $buildingUnit): self
    {
        $this->buildingUnit = $buildingUnit;

        return $this;
    }

    public function getForwardingResponse(): ?ForwardingResponse
    {
        return $this->forwardingResponse;
    }

    public function setForwardingResponse(?ForwardingResponse $forwardingResponse): self
    {
        $this->forwardingResponse = $forwardingResponse;

        return $this;
    }

    public function getRejectReason(): ?string
    {
        return $this->rejectReason;
    }

    public function setRejectReason(?string $rejectReason): self
    {
        $this->rejectReason = $rejectReason;

        return $this;
    }
}
