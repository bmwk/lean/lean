<?php

declare(strict_types=1);

namespace App\Domain\Entity;

class LastVisitedPage
{
    public function __construct(
       private readonly string $routeName,
       private array $parameters = []
   ) {
   }

    public function getRouteName(): string
    {
        return $this->routeName;
    }

    public function getParameters(): array
    {
        return $this->parameters;
    }

    public function setParameters(array $parameters): self
    {
        $this->parameters = $parameters;

        return $this;
    }
}
