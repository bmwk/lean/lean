<?php

declare(strict_types=1);

namespace App\Domain\Entity;

enum DocumentTemplateType: int
{
    case PROPERTY_OWNER_CORRESPONDENCE = 0;

    public function getName(): string
    {
        return match ($this) {
            self::PROPERTY_OWNER_CORRESPONDENCE => 'Eigentümerkorrespondenz'
        };
    }
}
