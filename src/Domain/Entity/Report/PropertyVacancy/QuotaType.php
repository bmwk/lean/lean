<?php

declare(strict_types=1);

namespace App\Domain\Entity\Report\PropertyVacancy;

class QuotaType
{
    private int $empty;
    private int $total;
    private ?float $quota = null;

    public function getEmpty(): int
    {
        return $this->empty;
    }

    public function setEmpty(int $empty): self
    {
        $this->empty = $empty;

        return $this;
    }

    public function getTotal(): int
    {
        return $this->total;
    }

    public function setTotal(int $total): self
    {
        $this->total = $total;

        return $this;
    }

    public function getQuota(): ?float
    {
        if ($this->quota === null && $this->total !== 0) {
            $this->setQuota($this->empty / $this->total);
        }

        return $this->quota;
    }

    public function setQuota(?float $quota): self
    {
        $this->quota = $quota;

        return $this;
    }

    public function fetchQuotaInPercent(): ?float
    {
        if ($this->getQuota() === null) {
            return null;
        }

        return $this->getQuota() * 100;
    }
}
