<?php

declare(strict_types=1);

namespace App\Domain\Entity\Report\PropertyVacancy;

class GeneralPropertyVacancyData
{
    private QuotaType $totalPropertyVacancy;
    private QuotaType $groundFloorPropertyVacancy;
    private QuotaType $aLocationPropertyVacancy;
    private QuotaType $bLocationPropertyVacancy;
    private ?int $longestPropertyVacancyInDays = null;
    private ?int $shortestPropertyVacancyInDays = null;
    private ?int $averagePropertyVacancyInDays = null;

    public function getTotalPropertyVacancy(): QuotaType
    {
        return $this->totalPropertyVacancy;
    }

    public function setTotalPropertyVacancy(QuotaType $totalPropertyVacancy): self
    {
        $this->totalPropertyVacancy = $totalPropertyVacancy;

        return $this;
    }

    public function getGroundFloorPropertyVacancy(): QuotaType
    {
        return $this->groundFloorPropertyVacancy;
    }

    public function setGroundFloorPropertyVacancy(QuotaType $groundFloorPropertyVacancy): self
    {
        $this->groundFloorPropertyVacancy = $groundFloorPropertyVacancy;

        return $this;
    }

    public function getALocationPropertyVacancy(): QuotaType
    {
        return $this->aLocationPropertyVacancy;
    }

    public function setALocationPropertyVacancy(QuotaType $aLocationPropertyVacancy): self
    {
        $this->aLocationPropertyVacancy = $aLocationPropertyVacancy;

        return $this;
    }

    public function getBLocationPropertyVacancy(): QuotaType
    {
        return $this->bLocationPropertyVacancy;
    }

    public function setBLocationPropertyVacancy(QuotaType $bLocationPropertyVacancy): self
    {
        $this->bLocationPropertyVacancy = $bLocationPropertyVacancy;

        return $this;
    }

    public function getLongestPropertyVacancyInDays(): ?int
    {
        return $this->longestPropertyVacancyInDays;
    }

    public function setLongestPropertyVacancyInDays(?int $longestPropertyVacancyInDays): self
    {
        $this->longestPropertyVacancyInDays = $longestPropertyVacancyInDays;

        return $this;
    }

    public function getShortestPropertyVacancyInDays(): ?int
    {
        return $this->shortestPropertyVacancyInDays;
    }

    public function setShortestPropertyVacancyInDays(?int $shortestPropertyVacancyInDays): self
    {
        $this->shortestPropertyVacancyInDays = $shortestPropertyVacancyInDays;

        return $this;
    }

    public function getAveragePropertyVacancyInDays(): ?int
    {
        return $this->averagePropertyVacancyInDays;
    }

    public function setAveragePropertyVacancyInDays(?int $averagePropertyVacancyInDays): self
    {
        $this->averagePropertyVacancyInDays = $averagePropertyVacancyInDays;

        return $this;
    }
}
