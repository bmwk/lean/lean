<?php

declare(strict_types=1);

namespace App\Domain\Entity\Report\PropertyVacancy;

use App\Domain\Entity\Classification\IndustryClassification;
use App\Domain\Entity\Place;

class PropertyVacancyDataFilter
{
    private ?Place $quarterPlace = null;

    private ?array $locationCategories = null;

    private IndustryClassification|null $industryClassification = null;

    private ?array $vacancyReasons = null;

    private ?bool $onlyGroundFloor = null;

    public function getQuarterPlace(): ?Place
    {
        return $this->quarterPlace;
    }

    public function setQuarterPlace(?Place $quarterPlace): self
    {
        $this->quarterPlace = $quarterPlace;

        return $this;
    }

    public function getLocationCategories(): ?array
    {
        return $this->locationCategories;
    }

    public function setLocationCategories(?array $locationCategories): self
    {
        $this->locationCategories = $locationCategories;

        return $this;
    }

    public function getVacancyReasons(): ?array
    {
        return $this->vacancyReasons;
    }

    public function setVacancyReasons(?array $vacancyReasons): self
    {
        $this->vacancyReasons = $vacancyReasons;

        return $this;
    }

    public function getIndustryClassification(): ?IndustryClassification
    {
        return $this->industryClassification;
    }

    public function setIndustryClassification(?IndustryClassification $industryClassification): self
    {
        $this->industryClassification = $industryClassification;

        return $this;
    }

    public function getOnlyGroundFloor(): ?bool
    {
        return $this->onlyGroundFloor;
    }

    public function setOnlyGroundFloor(?bool $onlyGroundFloor): self
    {
        $this->onlyGroundFloor = $onlyGroundFloor;

        return $this;
    }
}
