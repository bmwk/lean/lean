<?php

declare(strict_types=1);

namespace App\Domain\Entity\Report\PropertyVacancy;

class IndividualPropertyVacancyData
{
    private QuotaType $propertyVacancy;
    private string $title;
    private string $subTitle;

    public function getPropertyVacancy(): QuotaType
    {
        return $this->propertyVacancy;
    }

    public function setPropertyVacancy(QuotaType $propertyVacancy): self
    {
        $this->propertyVacancy = $propertyVacancy;

        return $this;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getSubTitle(): string
    {
        return $this->subTitle;
    }

    public function setSubTitle(string $subTitle): self
    {
        $this->subTitle = $subTitle;

        return $this;
    }

}
