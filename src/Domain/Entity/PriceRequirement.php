<?php

declare(strict_types=1);

namespace App\Domain\Entity;

use App\Domain\Entity\LookingForPropertyRequestReport\LookingForPropertyRequestReport;
use App\Repository\PriceRequirementRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\Table;

#[Entity(repositoryClass: PriceRequirementRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[HasLifecycleCallbacks]
class PriceRequirement
{
    use CreatedAtTrait;
    use UpdatedAtTrait;

    #[Id]
    #[GeneratedValue]
    #[Column(type: Types::INTEGER, nullable: false, options: ['unsigned' => true])]
    private ?int $id = null;

    /**
     * @var PropertyOfferType[]
     */
    #[Column(type: Types::JSON, nullable: false, enumType: PropertyOfferType::class)]
    private array $propertyOfferTypes;

    #[OneToOne(fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private ValueRequirement $purchasePriceNet;

    #[OneToOne(fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private ValueRequirement $purchasePriceGross;

    #[OneToOne(fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private ValueRequirement $purchasePricePerSquareMeter;

    #[OneToOne(fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private ValueRequirement $netColdRent;

    #[OneToOne(fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private ValueRequirement $coldRent;

    #[OneToOne(fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private ValueRequirement $rentalPricePerSquareMeter;

    #[OneToOne(fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private ValueRequirement $lease;

    #[Column(type: Types::BOOLEAN, nullable: true)]
    private ?bool $nonCommissionable = null;

    public static function createFromLookingForPropertyRequestReport(LookingForPropertyRequestReport $lookingForPropertyRequestReport): self
    {
        $priceRequirementFromLookingForPropertyRequestReport = $lookingForPropertyRequestReport->getPriceRequirement();

        $priceRequirement = new self();

        $priceRequirement->propertyOfferTypes = $priceRequirementFromLookingForPropertyRequestReport->getPropertyOfferTypes();

        $priceRequirement->purchasePriceNet = clone $priceRequirementFromLookingForPropertyRequestReport->getPurchasePriceNet();
        $priceRequirement->purchasePriceNet->setId(null);

        $priceRequirement->purchasePriceGross = clone $priceRequirementFromLookingForPropertyRequestReport->getPurchasePriceGross();
        $priceRequirement->purchasePriceGross->setId(null);

        $priceRequirement->purchasePricePerSquareMeter = clone $priceRequirementFromLookingForPropertyRequestReport->getPurchasePricePerSquareMeter();
        $priceRequirement->purchasePricePerSquareMeter->setId(null);

        $priceRequirement->netColdRent = clone $priceRequirementFromLookingForPropertyRequestReport->getNetColdRent();
        $priceRequirement->netColdRent->setId(null);

        $priceRequirement->coldRent = clone $priceRequirementFromLookingForPropertyRequestReport->getColdRent();
        $priceRequirement->coldRent->setId(null);

        $priceRequirement->rentalPricePerSquareMeter = clone $priceRequirementFromLookingForPropertyRequestReport->getRentalPricePerSquareMeter();
        $priceRequirement->rentalPricePerSquareMeter->setId(null);

        $priceRequirement->lease = clone $priceRequirementFromLookingForPropertyRequestReport->getLease();
        $priceRequirement->lease->setId(null);

        $priceRequirement->nonCommissionable = $priceRequirementFromLookingForPropertyRequestReport->getNonCommissionable();

        return $priceRequirement;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return PropertyOfferType[]
     */
    public function getPropertyOfferTypes(): array
    {
        return $this->propertyOfferTypes;
    }

    /**
     * @param PropertyOfferType[] $propertyOfferTypes
     */
    public function setPropertyOfferTypes(array $propertyOfferTypes): self
    {
        $this->propertyOfferTypes = $propertyOfferTypes;

        return $this;
    }

    public function getPurchasePriceNet(): ValueRequirement
    {
        return $this->purchasePriceNet;
    }

    public function setPurchasePriceNet(ValueRequirement $purchasePriceNet): self
    {
        $this->purchasePriceNet = $purchasePriceNet;

        return $this;
    }

    public function getPurchasePriceGross(): ValueRequirement
    {
        return $this->purchasePriceGross;
    }

    public function setPurchasePriceGross(ValueRequirement $purchasePriceGross): self
    {
        $this->purchasePriceGross = $purchasePriceGross;

        return $this;
    }

    public function getPurchasePricePerSquareMeter(): ValueRequirement
    {
        return $this->purchasePricePerSquareMeter;
    }

    public function setPurchasePricePerSquareMeter(ValueRequirement $purchasePricePerSquareMeter): self
    {
        $this->purchasePricePerSquareMeter = $purchasePricePerSquareMeter;

        return $this;
    }

    public function getNetColdRent(): ValueRequirement
    {
        return $this->netColdRent;
    }

    public function setNetColdRent(ValueRequirement $netColdRent): self
    {
        $this->netColdRent = $netColdRent;

        return $this;
    }

    public function getColdRent(): ValueRequirement
    {
        return $this->coldRent;
    }

    public function setColdRent(ValueRequirement $coldRent): self
    {
        $this->coldRent = $coldRent;

        return $this;
    }

    public function getRentalPricePerSquareMeter(): ValueRequirement
    {
        return $this->rentalPricePerSquareMeter;
    }

    public function setRentalPricePerSquareMeter(ValueRequirement $rentalPricePerSquareMeter): self
    {
        $this->rentalPricePerSquareMeter = $rentalPricePerSquareMeter;

        return $this;
    }

    public function getLease(): ValueRequirement
    {
        return $this->lease;
    }

    public function setLease(ValueRequirement $lease): self
    {
        $this->lease = $lease;

        return $this;
    }

    public function getNonCommissionable(): ?bool
    {
        return $this->nonCommissionable;
    }

    public function setNonCommissionable(?bool $nonCommissionable): self
    {
        $this->nonCommissionable = $nonCommissionable;

        return $this;
    }
}
