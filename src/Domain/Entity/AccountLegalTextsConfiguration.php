<?php

declare(strict_types=1);

namespace App\Domain\Entity;

use App\Repository\AccountLegalTextsConfigurationRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\Table;

#[Entity(repositoryClass: AccountLegalTextsConfigurationRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[HasLifecycleCallbacks]
class AccountLegalTextsConfiguration
{
    #[Id]
    #[GeneratedValue]
    #[Column(type: Types::INTEGER, nullable: false, options: ['unsigned' => true])]
    private ?int $id = null;

    #[OneToOne]
    #[JoinColumn(nullable: false)]
    private Account $account;

    #[Column(type: Types::TEXT, nullable: true)]
    private ?string $privacyPolicyProvider = null;

    #[Column(type: Types::TEXT, nullable: true)]
    private ?string $privacyPolicySeeker = null;

    #[Column(type: Types::TEXT, nullable: true)]
    private ?string $generalTermsAndConditionsProvider = null;

    #[Column(type: Types::TEXT, nullable: true)]
    private ?string $generalTermsAndConditionsSeeker = null;

    #[Column(type: Types::TEXT, nullable: true)]
    private ?string $pdfExposeDisclaimer = null;

    #[Column(type: Types::TEXT, nullable: true)]
    private ?string $imprint = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getAccount(): Account
    {
        return $this->account;
    }

    public function setAccount(Account $account): self
    {
        $this->account = $account;

        return $this;
    }

    public function getPrivacyPolicyProvider(): ?string
    {
        return $this->privacyPolicyProvider;
    }

    public function setPrivacyPolicyProvider(?string $privacyPolicyProvider): self
    {
        $this->privacyPolicyProvider = $privacyPolicyProvider;

        return $this;
    }

    public function getPrivacyPolicySeeker(): ?string
    {
        return $this->privacyPolicySeeker;
    }

    public function setPrivacyPolicySeeker(?string $privacyPolicySeeker): self
    {
        $this->privacyPolicySeeker = $privacyPolicySeeker;

        return $this;
    }

    public function getGeneralTermsAndConditionsProvider(): ?string
    {
        return $this->generalTermsAndConditionsProvider;
    }

    public function setGeneralTermsAndConditionsProvider(?string $generalTermsAndConditionsProvider): self
    {
        $this->generalTermsAndConditionsProvider = $generalTermsAndConditionsProvider;

        return $this;
    }

    public function getGeneralTermsAndConditionsSeeker(): ?string
    {
        return $this->generalTermsAndConditionsSeeker;
    }

    public function setGeneralTermsAndConditionsSeeker(?string $generalTermsAndConditionsSeeker): self
    {
        $this->generalTermsAndConditionsSeeker = $generalTermsAndConditionsSeeker;

        return $this;
    }

    public function getPdfExposeDisclaimer(): ?string
    {
        return $this->pdfExposeDisclaimer;
    }

    public function setPdfExposeDisclaimer(?string $pdfExposeDisclaimer): self
    {
        $this->pdfExposeDisclaimer = $pdfExposeDisclaimer;

        return $this;
    }

    public function getImprint(): ?string
    {
        return $this->imprint;
    }

    public function setImprint(?string $imprint): self
    {
        $this->imprint = $imprint;

        return $this;
    }
}
