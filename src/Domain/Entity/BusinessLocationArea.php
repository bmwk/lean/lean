<?php

declare(strict_types=1);

namespace App\Domain\Entity;

use App\Repository\BusinessLocationAreaRepository;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\Table;

#[Entity(repositoryClass: BusinessLocationAreaRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[HasLifecycleCallbacks]
class BusinessLocationArea
{
    use AccountTrait;
    use CreatedAtTrait;
    use UpdatedAtTrait;

    #[Id]
    #[GeneratedValue]
    #[Column(type: 'integer', nullable: false, options: ['unsigned' => true])]
    private ?int $id = null;

    #[Column(type: 'smallint', nullable: true, enumType: LocationCategory::class, options: ['unsigned' => true])]
    private LocationCategory $locationCategory;

    #[OneToOne]
    #[JoinColumn(nullable: true)]
    private GeolocationPolygon $geolocationPolygon;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getLocationCategory(): LocationCategory
    {
        return $this->locationCategory;
    }

    public function setLocationCategory(LocationCategory $locationCategory): self
    {
        $this->locationCategory = $locationCategory;

        return $this;
    }

    public function getGeolocationPolygon(): GeolocationPolygon
    {
        return $this->geolocationPolygon;
    }

    public function setGeolocationPolygon(GeolocationPolygon $geolocationPolygon): self
    {
        $this->geolocationPolygon = $geolocationPolygon;

        return $this;
    }
}
