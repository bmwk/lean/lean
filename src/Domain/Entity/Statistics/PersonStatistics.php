<?php

declare(strict_types=1);

namespace App\Domain\Entity\Statistics;

class PersonStatistics
{
    private int $naturalPerson = 0;
    private int $company = 0;
    private int $commune = 0;
    private int $propertyOwner = 0;
    private int $propertyUser = 0;
    private int $propertySeeker = 0;
    private int $propertyContactPerson = 0;
    private int $propertyMarketingContactPerson = 0;

    public function getNaturalPerson(): int
    {
        return $this->naturalPerson;
    }

    public function setNaturalPerson(int $naturalPerson): self
    {
        $this->naturalPerson = $naturalPerson;

        return $this;
    }

    public function getCompany(): int
    {
        return $this->company;
    }

    public function setCompany(int $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getCommune(): int
    {
        return $this->commune;
    }

    public function setCommune(int $commune): self
    {
        $this->commune = $commune;

        return $this;
    }

    public function getPropertyOwner(): int
    {
        return $this->propertyOwner;
    }

    public function setPropertyOwner(int $propertyOwner): self
    {
        $this->propertyOwner = $propertyOwner;

        return $this;
    }

    public function getPropertyUser(): int
    {
        return $this->propertyUser;
    }

    public function setPropertyUser(int $propertyUser): self
    {
        $this->propertyUser = $propertyUser;

        return $this;
    }

    public function getPropertySeeker(): int
    {
        return $this->propertySeeker;
    }

    public function setPropertySeeker(int $propertySeeker): self
    {
        $this->propertySeeker = $propertySeeker;

        return $this;
    }

    public function getPropertyContactPerson(): int
    {
        return $this->propertyContactPerson;
    }

    public function setPropertyContactPerson(int $propertyContactPerson): self
    {
        $this->propertyContactPerson = $propertyContactPerson;

        return $this;
    }

    public function getPropertyMarketingContactPerson(): int
    {
        return $this->propertyMarketingContactPerson;
    }

    public function setPropertyMarketingContactPerson(int $propertyMarketingContactPerson): self
    {
        $this->propertyMarketingContactPerson = $propertyMarketingContactPerson;

        return $this;
    }

    public function getTotal(): int
    {
        $total = 0;

        $total += $this->naturalPerson;
        $total += $this->company;
        $total += $this->commune;

        return $total;
    }
}
