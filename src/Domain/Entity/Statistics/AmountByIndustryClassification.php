<?php

declare(strict_types=1);

namespace App\Domain\Entity\Statistics;

use App\Domain\Entity\Classification\IndustryClassification;

class AmountByIndustryClassification
{
    public function __construct(
        private readonly int $amount,
        private readonly ?IndustryClassification $industryClassification = null
    ) {
    }

    public function getAmount(): int
    {
        return $this->amount;
    }

    public function getIndustryClassification(): ?IndustryClassification
    {
        return $this->industryClassification;
    }
}
