<?php

declare(strict_types=1);

namespace App\Domain\Entity\Statistics;

class AmountPerMonth
{
    public function __construct(
        private readonly int $year,
        private readonly int $month,
        private readonly int $amount
    ) {
    }

    public function getYear(): int
    {
        return $this->year;
    }

    public function getMonth(): int
    {
        return $this->month;
    }

    public function getAmount(): int
    {
        return $this->amount;
    }
}
