<?php

declare(strict_types=1);

namespace App\Domain\Entity\Statistics;

class UserStatistics
{
    private int $registeredPropertyProvider = 0;
    private int $invitedPropertyProvider = 0;
    private int $activePropertyProvider = 0;
    private int $registeredPropertySeeker = 0;
    private int $invitedPropertySeeker = 0;
    private int $activePropertySeeker = 0;
    private int $activeInternalAccountUser = 0;
    private int $blockedInternalAccountUser = 0;

    public function getRegisteredPropertyProvider(): int
    {
        return $this->registeredPropertyProvider;
    }

    public function setRegisteredPropertyProvider(int $registeredPropertyProvider): self
    {
        $this->registeredPropertyProvider = $registeredPropertyProvider;

        return $this;
    }

    public function getInvitedPropertyProvider(): int
    {
        return $this->invitedPropertyProvider;
    }

    public function setInvitedPropertyProvider(int $invitedPropertyProvider): self
    {
        $this->invitedPropertyProvider = $invitedPropertyProvider;

        return $this;
    }

    public function getActivePropertyProvider(): int
    {
        return $this->activePropertyProvider;
    }

    public function setActivePropertyProvider(int $activePropertyProvider): self
    {
        $this->activePropertyProvider = $activePropertyProvider;

        return $this;
    }

    public function getRegisteredPropertySeeker(): int
    {
        return $this->registeredPropertySeeker;
    }

    public function setRegisteredPropertySeeker(int $registeredPropertySeeker): self
    {
        $this->registeredPropertySeeker = $registeredPropertySeeker;

        return $this;
    }

    public function getInvitedPropertySeeker(): int
    {
        return $this->invitedPropertySeeker;
    }

    public function setInvitedPropertySeeker(int $invitedPropertySeeker): self
    {
        $this->invitedPropertySeeker = $invitedPropertySeeker;

        return $this;
    }

    public function getActivePropertySeeker(): int
    {
        return $this->activePropertySeeker;
    }

    public function setActivePropertySeeker(int $activePropertySeeker): self
    {
        $this->activePropertySeeker = $activePropertySeeker;

        return $this;
    }

    public function getActiveInternalAccountUser(): int
    {
        return $this->activeInternalAccountUser;
    }

    public function setActiveInternalAccountUser(int $activeInternalAccountUser): self
    {
        $this->activeInternalAccountUser = $activeInternalAccountUser;

        return $this;
    }

    public function getBlockedInternalAccountUser(): int
    {
        return $this->blockedInternalAccountUser;
    }

    public function setBlockedInternalAccountUser(int $blockedInternalAccountUser): self
    {
        $this->blockedInternalAccountUser = $blockedInternalAccountUser;

        return $this;
    }

    public function getTotal(): int
    {
        $total = 0;

        $total += $this->activeInternalAccountUser;
        $total += $this->blockedInternalAccountUser;
        $total += $this->activePropertyProvider;
        $total += $this->invitedPropertyProvider;
        $total += $this->registeredPropertyProvider;
        $total += $this->activePropertySeeker;
        $total += $this->invitedPropertySeeker;
        $total += $this->registeredPropertySeeker;

        return $total;
    }
}
