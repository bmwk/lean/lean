<?php

declare(strict_types=1);

namespace App\Domain\Entity\Statistics;

class SettlementStatistics
{
    private int $activeLookingForPropertyRequest = 0;
    private int $inactiveLookingForPropertyRequest = 0;
    private int $archivedLookingForPropertyRequest = 0;
    private int $matchingTask = 0;
    private int $averageNumberOfResultsPerMatching = 0;
    private int $lookingForPropertyRequestExposeForwarding = 0;
    private int $settlementConceptExposeForwarding = 0;
    private int $successfulSettlement = 0;

    public function getActiveLookingForPropertyRequest(): int
    {
        return $this->activeLookingForPropertyRequest;
    }

    public function setActiveLookingForPropertyRequest(int $activeLookingForPropertyRequest): self
    {
        $this->activeLookingForPropertyRequest = $activeLookingForPropertyRequest;

        return $this;
    }

    /**
     * @return int
     */
    public function getInactiveLookingForPropertyRequest(): int
    {
        return $this->inactiveLookingForPropertyRequest;
    }

    public function setInactiveLookingForPropertyRequest(int $inactiveLookingForPropertyRequest): self
    {
        $this->inactiveLookingForPropertyRequest = $inactiveLookingForPropertyRequest;

        return $this;
    }

    public function getArchivedLookingForPropertyRequest(): int
    {
        return $this->archivedLookingForPropertyRequest;
    }

    public function setArchivedLookingForPropertyRequest(int $archivedLookingForPropertyRequest): self
    {
        $this->archivedLookingForPropertyRequest = $archivedLookingForPropertyRequest;

        return $this;
    }

    public function getMatchingTask(): int
    {
        return $this->matchingTask;
    }

    public function setMatchingTask(int $matchingTask): self
    {
        $this->matchingTask = $matchingTask;

        return $this;
    }

    public function getAverageNumberOfResultsPerMatching(): int
    {
        return $this->averageNumberOfResultsPerMatching;
    }

    public function setAverageNumberOfResultsPerMatching(int $averageNumberOfResultsPerMatching): self
    {
        $this->averageNumberOfResultsPerMatching = $averageNumberOfResultsPerMatching;

        return $this;
    }

    public function getLookingForPropertyRequestExposeForwarding(): int
    {
        return $this->lookingForPropertyRequestExposeForwarding;
    }

    public function setLookingForPropertyRequestExposeForwarding(int $lookingForPropertyRequestExposeForwarding): self
    {
        $this->lookingForPropertyRequestExposeForwarding = $lookingForPropertyRequestExposeForwarding;

        return $this;
    }

    public function getSettlementConceptExposeForwarding(): int
    {
        return $this->settlementConceptExposeForwarding;
    }

    public function setSettlementConceptExposeForwarding(int $settlementConceptExposeForwarding): self
    {
        $this->settlementConceptExposeForwarding = $settlementConceptExposeForwarding;

        return $this;
    }

    public function getSuccessfulSettlement(): int
    {
        return $this->successfulSettlement;
    }

    public function setSuccessfulSettlement(int $successfulSettlement): self
    {
        $this->successfulSettlement = $successfulSettlement;

        return $this;
    }
}
