<?php

declare(strict_types=1);

namespace App\Domain\Entity;

use App\Repository\PlaceRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\InverseJoinColumn;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\OrderBy;
use Doctrine\ORM\Mapping\Table;
use Symfony\Component\Uid\Uuid;

#[Entity(repositoryClass: PlaceRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[HasLifecycleCallbacks]
class Place
{
    use CreatedAtTrait;
    use UpdatedAtTrait;

    #[Id]
    #[GeneratedValue]
    #[Column(type: 'integer', nullable: false, options: ['unsigned' => true])]
    private ?int $id = null;

    #[Column(type: 'uuid', unique: true, nullable: false)]
    private Uuid $uuid;

    #[Column(type: 'string', length: 190, nullable: false)]
    private string $placeName;

    #[Column(type: 'string', length: 190, nullable: true)]
    private ?string $placeShortName = null;

    #[Column(type: 'integer', nullable: false, enumType: PlaceType::class, options: ['unsigned' => true])]
    private PlaceType $placeType;

    #[OneToOne]
    #[JoinColumn(nullable: true)]
    private ?GeolocationPoint $geolocationPoint = null;

    #[ManyToMany(targetEntity: Place::class, mappedBy: 'childrenPlaces')]
    #[OrderBy(["placeName" => "ASC"])]
    private Collection|array $parentPlaces;

    #[ManyToMany(targetEntity: Place::class, inversedBy: 'parentPlaces')]
    #[OrderBy(["placeName" => "ASC"])]
    #[JoinTable('parent_place_children_place')]
    #[JoinColumn(name: 'parent_place_id', referencedColumnName: 'id')]
    #[InverseJoinColumn(name: 'child_place_id', referencedColumnName: 'id')]
    private Collection|array $childrenPlaces;

    public function __construct()
    {
        $this->parentPlaces = new ArrayCollection();
        $this->childrenPlaces = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getUuid(): Uuid
    {
        return $this->uuid;
    }

    public function setUuid(Uuid $uuid): self
    {
        $this->uuid = $uuid;

        return $this;
    }

    public function getPlaceName(): string
    {
        return $this->placeName;
    }

    public function setPlaceName(string $placeName): self
    {
        $this->placeName = $placeName;

        return $this;
    }

    public function getPlaceShortName(): ?string
    {
        return $this->placeShortName;
    }

    public function setPlaceShortName(?string $placeShortName): self
    {
        $this->placeShortName = $placeShortName;

        return $this;
    }

    public function getPlaceType(): PlaceType
    {
        return $this->placeType;
    }

    public function setPlaceType(PlaceType $placeType): self
    {
        $this->placeType = $placeType;

        return $this;
    }

    public function getGeolocationPoint(): ?GeolocationPoint
    {
        return $this->geolocationPoint;
    }

    public function setGeolocationPoint(?GeolocationPoint $geolocationPoint): self
    {
        $this->geolocationPoint = $geolocationPoint;

        return $this;
    }

    public function getParentPlaces(): Collection|array
    {
        return $this->parentPlaces;
    }

    public function setParentPlaces(Collection|array $parentPlaces): self
    {
        $this->parentPlaces = $parentPlaces;

        return $this;
    }

    public function getChildrenPlaces(): Collection|array
    {
        return $this->childrenPlaces;
    }

    public function setChildrenPlaces(Collection|array $childrenPlaces): self
    {
        $this->childrenPlaces = $childrenPlaces;

        return $this;
    }
}
