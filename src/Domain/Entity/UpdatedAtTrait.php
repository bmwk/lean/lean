<?php

declare(strict_types=1);

namespace App\Domain\Entity;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\PreUpdate;

trait UpdatedAtTrait
{
    #[Column(type: 'datetime', nullable: true)]
    protected ?\DateTime $updatedAt = null;

    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    #[PreUpdate]
    public function setUpdatedAtOnPreUpdate(): void
    {
        $this->updatedAt = new \DateTime();
    }
}
