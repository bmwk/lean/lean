<?php

declare(strict_types=1);

namespace App\Domain\Entity;

enum PriceSegmentType: int
{
    case LOW = 0;
    case LOW_MID = 1;
    case MID = 2;
    case MID_HIGH = 3;
    case HIGH = 4;

    public function getName(): string
    {
        return match ($this) {
            self::LOW => 'Niedrig',
            self::LOW_MID => 'Niedrig-Mittel',
            self::MID => 'Mittel',
            self::MID_HIGH => 'Mittel-Hoch',
            self::HIGH => 'Hoch',
        };
    }
}
