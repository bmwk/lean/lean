<?php

declare(strict_types=1);

namespace App\Domain\Entity\Notification;

use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\CreatedAtTrait;
use App\Domain\Entity\LookingForPropertyRequest\LookingForPropertyRequest;
use App\Domain\Entity\Notification\Message\AbstractMessage;
use App\Domain\Entity\Notification\Message\NoMatchFoundForLookingForPropertyRequest;
use App\Domain\Entity\Notification\Message\OccurrenceResubmissionDue;
use App\Domain\Entity\Notification\Message\PropertyBecomeEmpty;
use App\Domain\Entity\Notification\Message\PropertyNoMatchFound;
use App\Domain\Entity\Notification\Message\PropertyNoMatchingExecuted;
use App\Domain\Entity\Notification\Message\PropertyNotUpdated;
use App\Domain\Entity\Notification\Message\PropertyRentalAgreementExpired;
use App\Domain\Entity\Notification\Message\PropertyUsageChange;
use App\Domain\Entity\Person\Occurrence;
use App\Domain\Entity\Property\BuildingUnit;
use App\Domain\Entity\UpdatedAtTrait;
use App\Repository\NotificationRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\Table;

#[Entity(repositoryClass: NotificationRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[HasLifecycleCallbacks]
class Notification
{
    use CreatedAtTrait;
    use UpdatedAtTrait;

    #[Id]
    #[GeneratedValue]
    #[Column(type: Types::INTEGER, nullable: false, options: ['unsigned' => true])]
    private ?int $id = null;

    #[ManyToOne]
    #[JoinColumn(nullable: false)]
    private AccountUser $accountUser;

    #[Column(type: Types::SMALLINT, nullable: false, enumType: NotificationType::class, options: ['unsigned' => true])]
    private NotificationType $notificationType;

    #[Column(type: Types::BOOLEAN, nullable: false)]
    private bool $beenRead;

    #[ManyToOne]
    #[JoinColumn(nullable: true)]
    private ?BuildingUnit $buildingUnit = null;

    #[ManyToOne]
    #[JoinColumn(nullable: true)]
    private ?Occurrence $occurrence = null;

    #[ManyToOne]
    #[JoinColumn(nullable: true)]
    private ?LookingForPropertyRequest $lookingForPropertyRequest = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getAccountUser(): AccountUser
    {
        return $this->accountUser;
    }

    public function setAccountUser(AccountUser $accountUser): self
    {
        $this->accountUser = $accountUser;

        return $this;
    }

    public function getNotificationType(): NotificationType
    {
        return $this->notificationType;
    }

    public function setNotificationType(NotificationType $notificationType): self
    {
        $this->notificationType = $notificationType;

        return $this;
    }

    public function isBeenRead(): bool
    {
        return $this->beenRead;
    }

    public function setBeenRead(bool $beenRead): self
    {
        $this->beenRead = $beenRead;

        return $this;
    }

    public function getBuildingUnit(): ?BuildingUnit
    {
        return $this->buildingUnit;
    }

    public function setBuildingUnit(?BuildingUnit $buildingUnit): self
    {
        $this->buildingUnit = $buildingUnit;

        return $this;
    }

    public function getOccurrence(): ?Occurrence
    {
        return $this->occurrence;
    }

    public function setOccurrence(?Occurrence $occurrence): self
    {
        $this->occurrence = $occurrence;

        return $this;
    }

    public function getLookingForPropertyRequest(): ?LookingForPropertyRequest
    {
        return $this->lookingForPropertyRequest;
    }

    public function setLookingForPropertyRequest(?LookingForPropertyRequest $lookingForPropertyRequest): self
    {
        $this->lookingForPropertyRequest = $lookingForPropertyRequest;

        return $this;
    }

    public function getMessage(): AbstractMessage
    {
        return match ($this->getNotificationType()) {
            NotificationType::PROPERTY_BECAME_EMPTY => new PropertyBecomeEmpty(buildingUnit: $this->buildingUnit),
            NotificationType::PROPERTY_USAGE_CHANGE => new PropertyUsageChange(buildingUnit: $this->buildingUnit),
            NotificationType::PROPERTY_NO_MATCHING_EXECUTED => new PropertyNoMatchingExecuted(buildingUnit: $this->buildingUnit),
            NotificationType::OCCURRENCE_RESUBMISSION_DUE => new OccurrenceResubmissionDue(occurrence: $this->occurrence),
            NotificationType::PROPERTY_RENTAL_AGREEMENT_EXPIRED => new PropertyRentalAgreementExpired(buildingUnit: $this->buildingUnit),
            NotificationType::PROPERTY_NO_MATCH_FOUND => new PropertyNoMatchFound(buildingUnit: $this->buildingUnit),
            NotificationType::NO_MATCH_FOUND_FOR_LOOKING_FOR_PROPERTY_REQUEST => new NoMatchFoundForLookingForPropertyRequest(lookingForPropertyRequest: $this->lookingForPropertyRequest),
            NotificationType::PROPERTY_NOT_UPDATED => new PropertyNotUpdated(buildingUnit: $this->buildingUnit)
        };
    }
}
