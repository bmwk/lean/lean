<?php

declare(strict_types=1);

namespace App\Domain\Entity\Notification;

enum NotificationType: int
{
    case PROPERTY_BECAME_EMPTY = 0;
    case PROPERTY_USAGE_CHANGE = 1;
    case PROPERTY_NO_MATCHING_EXECUTED = 2;
    case OCCURRENCE_RESUBMISSION_DUE = 3;
    case PROPERTY_RENTAL_AGREEMENT_EXPIRED = 4;
    case PROPERTY_NO_MATCH_FOUND = 5;
    case NO_MATCH_FOUND_FOR_LOOKING_FOR_PROPERTY_REQUEST = 6;
    case PROPERTY_NOT_UPDATED = 7;

    public function getName(): string
    {
        return match ($this) {
            self::PROPERTY_BECAME_EMPTY => 'Objekt leer gefallen',
            self::PROPERTY_USAGE_CHANGE => 'Nutzungsart geändert',
            self::PROPERTY_NO_MATCHING_EXECUTED => 'Kein Objekt-Matching durchgeführt',
            self::OCCURRENCE_RESUBMISSION_DUE => 'Wiedervorlage ist fällig',
            self::PROPERTY_RENTAL_AGREEMENT_EXPIRED => 'Mietvertag abgelaufen',
            self::PROPERTY_NO_MATCH_FOUND => 'Kein Gesuch zum Objekt gefunden',
            self::NO_MATCH_FOUND_FOR_LOOKING_FOR_PROPERTY_REQUEST => 'Kein Objekt zum Gesuch gefunden',
            self::PROPERTY_NOT_UPDATED => 'Objekt nicht aktualisiert'
        };
    }
}
