<?php

declare(strict_types=1);

namespace App\Domain\Entity\Notification;

class NotificationFilter
{
    /**
     * @var NotificationType[]|null
     */
    private ?array $notificationTypes = null;

    private ?array $readStatus = null;

    /**
     * @return NotificationType[]|null
     */
    public function getNotificationTypes(): ?array
    {
        return $this->notificationTypes;
    }

    /**
     * @param NotificationType[]|null $notificationTypes
     */
    public function setNotificationTypes(?array $notificationTypes): self
    {
        $this->notificationTypes = $notificationTypes;

        return $this;
    }

    public function getReadStatus(): ?array
    {
        return $this->readStatus;
    }

    public function setReadStatus(?array $readStatus): self
    {
        $this->readStatus = $readStatus;

        return $this;
    }

    public function isFilterActive(): bool
    {
        if (empty($this->notificationTypes) === true && empty($this->readStatus) === true) {
            return false;
        }

        return true;
    }
}
