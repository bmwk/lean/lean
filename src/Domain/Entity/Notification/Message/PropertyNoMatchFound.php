<?php

declare(strict_types=1);

namespace App\Domain\Entity\Notification\Message;

use App\Domain\Entity\Notification\NotificationType;
use App\Domain\Entity\Property\BuildingUnit;

class PropertyNoMatchFound extends AbstractMessage
{
    public function __construct(
        private readonly BuildingUnit $buildingUnit
    ) {
    }

    public function getNotificationType(): NotificationType
    {
        return NotificationType::PROPERTY_NO_MATCH_FOUND;
    }

    public function getObjectDescription(): string
    {
        return $this->buildingUnit->getAddress()->getStreetName() . ' ' . $this->buildingUnit->getAddress()->getHouseNumber();
    }

    public function getMessage(): string
    {
        return 'Es wurde seit längerem kein passendes Ansiedlungsgesuch bzw. -konzept gefunden. Ggf. sollte ein Matching ausgeführt werden.';
    }

    public function getRelatedObjectName(): string
    {
        return 'Matching';
    }

    public function getRouteName(): string
    {
        return 'company_location_management_matching_object_detail';
    }

    public function getRouteParameters(): array
    {
        return ['id' => $this->buildingUnit->getId()];
    }
}
