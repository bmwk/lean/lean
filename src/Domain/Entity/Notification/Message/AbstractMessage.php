<?php

declare(strict_types=1);

namespace App\Domain\Entity\Notification\Message;

use App\Domain\Entity\Notification\NotificationType;

abstract class AbstractMessage
{
    public function getTitle(): string
    {
        return $this->getNotificationType()->getName();
    }

    abstract public function getNotificationType(): NotificationType;

    abstract public function getObjectDescription(): string;

    abstract public function getMessage(): string;

    abstract public function getRelatedObjectName(): string;

    abstract public function getRouteName(): string;

    abstract public function getRouteParameters(): array;
}
