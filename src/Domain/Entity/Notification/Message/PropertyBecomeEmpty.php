<?php

declare(strict_types=1);

namespace App\Domain\Entity\Notification\Message;

use App\Domain\Entity\Notification\NotificationType;
use App\Domain\Entity\Property\BuildingUnit;

class PropertyBecomeEmpty extends AbstractMessage
{
    public function __construct(
        private readonly BuildingUnit $buildingUnit
    ) {
    }

    public function getNotificationType(): NotificationType
    {
        return NotificationType::PROPERTY_BECAME_EMPTY;
    }

    public function getObjectDescription(): string
    {
        return $this->buildingUnit->getAddress()->getStreetName() . ' ' . $this->buildingUnit->getAddress()->getHouseNumber();
    }

    public function getMessage(): string
    {
        return 'Beim Objekt ist hinterlegt, dass es zum heutigen Datum leer fällt. Der Objektstatus sollte überprüft werden.';
    }

    public function getRelatedObjectName(): string
    {
        return 'Objekt';
    }

    public function getRouteName(): string
    {
        return 'property_vacancy_management_building_unit_basic_data';
    }

    public function getRouteParameters(): array
    {
        return ['buildingUnitId' => $this->buildingUnit->getId()];
    }
}
