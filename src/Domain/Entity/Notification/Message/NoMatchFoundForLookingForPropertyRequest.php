<?php

declare(strict_types=1);

namespace App\Domain\Entity\Notification\Message;

use App\Domain\Entity\LookingForPropertyRequest\LookingForPropertyRequest;
use App\Domain\Entity\Notification\NotificationType;

class NoMatchFoundForLookingForPropertyRequest extends AbstractMessage
{
    public function __construct(
        private readonly LookingForPropertyRequest $lookingForPropertyRequest
    ) {
    }

    public function getNotificationType(): NotificationType
    {
        return NotificationType::NO_MATCH_FOUND_FOR_LOOKING_FOR_PROPERTY_REQUEST;
    }

    public function getObjectDescription(): string
    {
        return '';
    }

    public function getMessage(): string
    {
        return 'Es wurde seit längerem kein passendes Objekt zum Ansiedlungsgesuch gefunden. Ggf. sollten die Suchparameter angepasst werden.';
    }

    public function getRelatedObjectName(): string
    {
        return 'Gesuch';
    }

    public function getRouteName(): string
    {
        return 'company_location_management_looking_for_property_request_edit';
    }

    public function getRouteParameters(): array
    {
        return ['id' => $this->lookingForPropertyRequest->getId()];
    }
}
