<?php

declare(strict_types=1);

namespace App\Domain\Entity\Notification\Message;

use App\Domain\Entity\Notification\NotificationType;
use App\Domain\Entity\Person\Occurrence;

class OccurrenceResubmissionDue extends AbstractMessage
{
    public function __construct(
        private readonly Occurrence $occurrence
    ) {
    }

    public function getNotificationType(): NotificationType
    {
        return NotificationType::OCCURRENCE_RESUBMISSION_DUE;
    }

    public function getObjectDescription(): string
    {
        return $this->occurrence->getPerson()->buildShortDisplayName();
    }

    public function getMessage(): string
    {
        return 'Eine gesetzte Wiedervorlage ist fällig.';
    }

    public function getRelatedObjectName(): string
    {
        return 'Kontakt';
    }

    public function getRouteName(): string
    {
        return 'person_management_occurrence_overview';
    }

    public function getRouteParameters(): array
    {
        return ['personId' => $this->occurrence->getPerson()->getId()];
    }
}
