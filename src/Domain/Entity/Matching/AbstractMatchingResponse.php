<?php

declare(strict_types=1);

namespace App\Domain\Entity\Matching;

use App\Domain\Entity\CreatedAtTrait;
use App\Domain\Entity\CreatedByAccountUserTrait;
use App\Domain\Entity\Property\BuildingUnit;
use App\Domain\Entity\UpdatedAtTrait;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;

abstract class AbstractMatchingResponse
{
    use CreatedByAccountUserTrait;
    use CreatedAtTrait;
    use UpdatedAtTrait;

    #[Id]
    #[GeneratedValue]
    #[Column(type: Types::INTEGER, nullable: false, options: ['unsigned' => true])]
    protected ?int $id = null;

    #[ManyToOne]
    #[JoinColumn(nullable: false)]
    protected BuildingUnit $buildingUnit;

    #[Column(type: Types::SMALLINT, nullable: true, enumType: Response::class, options: ['unsigned' => true])]
    protected Response $response;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getBuildingUnit(): BuildingUnit
    {
        return $this->buildingUnit;
    }

    public function setBuildingUnit(BuildingUnit $buildingUnit): self
    {
        $this->buildingUnit = $buildingUnit;

        return $this;
    }

    public function getResponse(): Response
    {
        return $this->response;
    }

    public function setResponse(Response $response): self
    {
        $this->response = $response;

        return $this;
    }
}
