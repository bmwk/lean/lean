<?php

declare(strict_types=1);

namespace App\Domain\Entity\Matching;

enum Response: int
{
    case DISMISS = 0;

    public function getName(): string
    {
        return match ($this) {
            self::DISMISS => 'irrelevant',
        };
    }
}
