<?php

declare(strict_types=1);

namespace App\Domain\Entity\Matching;

use App\Domain\Entity\AbstractExposeForwarding;
use App\Domain\Entity\AccountTrait;
use App\Domain\Entity\CreatedAtTrait;
use App\Domain\Entity\Property\BuildingUnit;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;

abstract class AbstractMatchingResult
{
    use AccountTrait;
    use CreatedAtTrait;

    #[Id]
    #[GeneratedValue]
    #[Column(type: Types::INTEGER, nullable: false, options: ['unsigned' => true])]
    protected ?int $id = null;

    #[ManyToOne]
    #[JoinColumn(nullable: false)]
    protected BuildingUnit $buildingUnit;

    #[Column(type: Types::INTEGER, nullable: true, options: ['unsigned' => true])]
    protected ?int $score = null;

    #[Column(type: Types::INTEGER, nullable: true, options: ['unsigned' => true])]
    protected ?int $possibleMaxScore = null;

    #[Column(type: Types::BOOLEAN, nullable: false)]
    protected bool $eliminated;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getBuildingUnit(): BuildingUnit
    {
        return $this->buildingUnit;
    }

    public function setBuildingUnit(BuildingUnit $buildingUnit): self
    {
        $this->buildingUnit = $buildingUnit;

        return $this;
    }

    public function getScore(): ?int
    {
        return $this->score;
    }

    public function setScore(?int $score): self
    {
        $this->score = $score;

        return $this;
    }

    public function getPossibleMaxScore(): ?int
    {
        return $this->possibleMaxScore;
    }

    public function setPossibleMaxScore(?int $possibleMaxScore): self
    {
        $this->possibleMaxScore = $possibleMaxScore;

        return $this;
    }

    public function isEliminated(): bool
    {
        return $this->eliminated;
    }

    public function setEliminated(bool $eliminated): self
    {
        $this->eliminated = $eliminated;

        return $this;
    }

    abstract public function fetchExposeForwarding(): ?AbstractExposeForwarding;

    abstract public function fetchMatchingResponse(): ?AbstractMatchingResponse;
}
