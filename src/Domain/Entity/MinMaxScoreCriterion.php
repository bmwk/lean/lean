<?php

declare(strict_types=1);

namespace App\Domain\Entity;

use App\Repository\MinMaxScoreCriterionRepository;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\Table;

#[Entity(repositoryClass: MinMaxScoreCriterionRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
class MinMaxScoreCriterion implements ScoreCriterionInterface
{
    #[Id]
    #[GeneratedValue]
    #[Column(type: 'integer', nullable: false, options: ['unsigned' => true])]
    private ?int $id = null;

    #[OneToOne(fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private ScoreCriterion $minScoreCriterion;

    #[OneToOne(fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private ScoreCriterion $maxScoreCriterion;

    #[Column(type: 'string', nullable: false)]
    private string $name;

    #[Column(type: 'integer', nullable: true, options: ['unsigned' => true])]
    private ?int $penaltyPoints = null;

    public function isSet(): bool
    {
        if ($this->minScoreCriterion->isSet() === false && $this->maxScoreCriterion->isSet() === false) {
            return false;
        }

        return true;
    }

    public function isEvaluationResult(): bool
    {
        $minIsSetAndNotEvaluated = $this->minScoreCriterion->isSet() && !$this->minScoreCriterion->isEvaluationResult();
        $minIsSetAndEvaluated = $this->minScoreCriterion->isSet() && $this->minScoreCriterion->isEvaluationResult();
        $maxIsSetAndNotEvaluated = $this->maxScoreCriterion->isSet() && !$this->maxScoreCriterion->isEvaluationResult();
        $maxIsSetAndEvaluated = $this->maxScoreCriterion->isSet() && $this->maxScoreCriterion->isEvaluationResult();


        if ($maxIsSetAndNotEvaluated && $this->minScoreCriterion->isSet()) {
            return false;
        }

        if ($minIsSetAndNotEvaluated && $this->maxScoreCriterion->isSet()) {
            return false;
        }

        if (!$this->minScoreCriterion->isSet() && !$this->maxScoreCriterion->isSet()) {
            return false;
        }

        if ($maxIsSetAndNotEvaluated && $minIsSetAndNotEvaluated) {
            return false;
        }

        if ($minIsSetAndNotEvaluated && !$maxIsSetAndEvaluated) {
            return false;
        }

        if ($maxIsSetAndNotEvaluated && !$minIsSetAndEvaluated) {
            return false;
        }

        return true;
    }

    public static function createNewMinMaxScoreCriterion(string $name): self
    {
        $minMaxScoreCriterion = new self();

        $minScoreCriterion = new ScoreCriterion();
        $minScoreCriterion
            ->setName('minimum')
            ->setPoints(1);
        $maxScoreCriterion = new ScoreCriterion();
        $maxScoreCriterion
            ->setName('maximum')
            ->setPoints(1);

        $minMaxScoreCriterion
            ->setName($name)
            ->setMinScoreCriterion($minScoreCriterion)
            ->setMaxScoreCriterion($maxScoreCriterion);

        return $minMaxScoreCriterion;
    }

    public function getMinScoreCriterion(): ScoreCriterion
    {
        return $this->minScoreCriterion;
    }

    public function setMinScoreCriterion(ScoreCriterion $minScoreCriterion): self
    {
        $this->minScoreCriterion = $minScoreCriterion;
        return $this;
    }

    public function getMaxScoreCriterion(): ScoreCriterion
    {
        return $this->maxScoreCriterion;
    }

    public function setMaxScoreCriterion(ScoreCriterion $maxScoreCriterion): self
    {
        $this->maxScoreCriterion = $maxScoreCriterion;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getPoints(): int
    {
        $points = 0;

        if ($this->minScoreCriterion->isSet() && $this->minScoreCriterion->isEvaluationResult()) {
            $points += $this->minScoreCriterion->getPoints();
        }

        if ($this->maxScoreCriterion->isSet() && $this->maxScoreCriterion->isEvaluationResult()) {
            $points += $this->maxScoreCriterion->getPoints();
        }

        return $points;
    }

    public function getMaxPoints(): int
    {
        $points = 0;

        $points += $this->minScoreCriterion->getPoints();
        $points += $this->maxScoreCriterion->getPoints();

        return $points;
    }

    public function getPenaltyPoints(): ?int
    {
        return $this->penaltyPoints;
    }

    public function setPenaltyPoints(?int $penaltyPoints): self
    {
        $this->penaltyPoints = $penaltyPoints;
        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;
        return $this;
    }
}
