<?php

declare(strict_types=1);

namespace App\Domain\Entity\HallOfInspiration;

enum CompanyValue: int
{
    case SUSTAINABILITY = 0;
    case SPECIALIZED = 1;
    case INNOVATIVE = 2;
    case NETWORKING = 3;
    case RELEXATION_RECREATION = 4;
    case CREATIVITY = 5;
    case EXPERIENCE_ENTERTAINMENT = 6;
    case LOCALITY_REGIONALITY = 7;
    case SERVICE_ORIENTED = 8;
    case VITYLITY_VIBRANCY = 9;

    public function getLabel(): string
    {
        return match ($this) {
            self::SUSTAINABILITY => 'Nachhaltigkeit',
            self::SPECIALIZED => 'spezialisiert',
            self::INNOVATIVE => 'innovativ',
            self::NETWORKING => 'Vernetzung',
            self::RELEXATION_RECREATION => 'Entspannung & Erholung',
            self::CREATIVITY => 'Kreativität',
            self::EXPERIENCE_ENTERTAINMENT => 'Erlebnis & Unterhaltung',
            self::LOCALITY_REGIONALITY => 'Lokalität & Regionalität',
            self::SERVICE_ORIENTED => 'serviceorientiert',
            self::VITYLITY_VIBRANCY => 'Vitalität & Lebendigkeit',
        };
    }
}
