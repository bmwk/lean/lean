<?php

declare(strict_types=1);

namespace App\Domain\Entity\HallOfInspiration;

enum CompanyLocation: int
{
    case COMPANY_SEAT = 0;
    case HOME_TOWN = 1;
    case REGIONAL = 2;
    case SUPRA_REGIONAL = 3;
    case NATIONAL = 4;
    case INTERNATIONAL = 5;

    public function getName(): string
    {
        return match ($this) {
            self::COMPANY_SEAT => 'am Firmensitz',
            self::HOME_TOWN => 'in eigener Stadt',
            self::REGIONAL => 'regional',
            self::SUPRA_REGIONAL => 'überregional',
            self::NATIONAL => 'national',
            self::INTERNATIONAL => 'international',
        };
    }
}
