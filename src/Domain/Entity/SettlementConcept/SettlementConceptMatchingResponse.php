<?php

declare(strict_types=1);

namespace App\Domain\Entity\SettlementConcept;

use App\Domain\Entity\Matching\AbstractMatchingResponse;
use App\Domain\Entity\Property\BuildingUnit;
use App\Repository\SettlementConcept\SettlementConceptMatchingResponseRepository;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\UniqueConstraint;

#[Entity(repositoryClass: SettlementConceptMatchingResponseRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[UniqueConstraint(name: 'settlement_concept_building_unit', columns: ['settlement_concept_id', 'building_unit_id'])]
#[HasLifecycleCallbacks]
class SettlementConceptMatchingResponse extends AbstractMatchingResponse
{
    #[ManyToOne(inversedBy: 'settlementConceptMatchingResponses')]
    #[JoinColumn(nullable: false)]
    protected BuildingUnit $buildingUnit;

    #[ManyToOne(inversedBy: 'settlementConceptMatchingResponses')]
    #[JoinColumn(nullable: false)]
    private SettlementConcept $settlementConcept;

    public function getSettlementConcept(): SettlementConcept
    {
        return $this->settlementConcept;
    }

    public function setSettlementConcept(SettlementConcept $settlementConcept): self
    {
        $this->settlementConcept = $settlementConcept;

        return $this;
    }
}
