<?php

declare(strict_types=1);

namespace App\Domain\Entity\SettlementConcept;

use App\Domain\Entity\MinMaxScoreCriterion;
use App\Domain\Entity\ScoreCriterion;
use App\Repository\SettlementConcept\SettlementConceptScoreCriteriaRepository;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\Table;

#[Entity(repositoryClass: SettlementConceptScoreCriteriaRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
class SettlementConceptScoreCriteria
{
    #[Id]
    #[GeneratedValue]
    #[Column(type: 'integer', nullable: false, options: ['unsigned' => true])]
    private ?int $id = null;

    #[OneToOne(fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private MinMaxScoreCriterion $retailSpace;

    #[OneToOne(fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private ScoreCriterion $secondarySpace;

    #[OneToOne(fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private ScoreCriterion $outdoorSalesArea;

    #[OneToOne(fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private ScoreCriterion $storeWidth;

    #[OneToOne(fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private ScoreCriterion $shopWindowLength;

    #[OneToOne(fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private ScoreCriterion $locationFactors;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getRetailSpace(): MinMaxScoreCriterion
    {
        return $this->retailSpace;
    }

    public function setRetailSpace(MinMaxScoreCriterion $retailSpace): self
    {
        $this->retailSpace = $retailSpace;

        return $this;
    }

    public function getSecondarySpace(): ScoreCriterion
    {
        return $this->secondarySpace;
    }

    public function setSecondarySpace(ScoreCriterion $secondarySpace): self
    {
        $this->secondarySpace = $secondarySpace;
        return $this;
    }

    public function getOutdoorSalesArea(): ScoreCriterion
    {
        return $this->outdoorSalesArea;
    }

    public function setOutdoorSalesArea(ScoreCriterion $outdoorSalesArea): self
    {
        $this->outdoorSalesArea = $outdoorSalesArea;
        return $this;
    }

    public function getStoreWidth(): ScoreCriterion
    {
        return $this->storeWidth;
    }

    public function setStoreWidth(ScoreCriterion $storeWidth): self
    {
        $this->storeWidth = $storeWidth;
        return $this;
    }

    public function getShopWindowLength(): ScoreCriterion
    {
        return $this->shopWindowLength;
    }

    public function setShopWindowLength(ScoreCriterion $shopWindowLength): self
    {
        $this->shopWindowLength = $shopWindowLength;
        return $this;
    }

    public function getLocationFactors(): ScoreCriterion
    {
        return $this->locationFactors;
    }

    public function setLocationFactors(ScoreCriterion $locationFactors): self
    {
        $this->locationFactors = $locationFactors;
        return $this;
    }

    public function calculatePoints(): int
    {
        $pointSum = 0;

        if ($this->retailSpace->isSet() && $this->retailSpace->isEvaluationResult()) {
            $pointSum += $this->retailSpace->getPoints();
        }

        if ($this->secondarySpace->isSet() === true && $this->secondarySpace->isEvaluationResult() === true) {
            $pointSum += $this->secondarySpace->getPoints();
        }

        if ($this->outdoorSalesArea->isSet() === true && $this->outdoorSalesArea->isEvaluationResult() === true) {
            $pointSum += $this->outdoorSalesArea->getPoints();
        }

        if ($this->storeWidth->isSet() === true && $this->storeWidth->isEvaluationResult() === true) {
            $pointSum += $this->storeWidth->getPoints();
        }

        if ($this->shopWindowLength->isSet() === true && $this->shopWindowLength->isEvaluationResult() === true) {
            $pointSum += $this->shopWindowLength->getPoints();
        }

        if ($this->locationFactors->isSet() === true && $this->locationFactors->isEvaluationResult() === true) {
            if ($this->locationFactors->getPointPenalty() !== null) {
                $intermediateSum = $this->locationFactors->getPoints() - $this->locationFactors->getPointPenalty();

                if ($intermediateSum > 0) {
                    $pointSum += $intermediateSum;
                }

            }else {
                $pointSum += $this->locationFactors->getPoints();
            }
        }

        return $pointSum;
    }

    public function calculateMaxPoints(): int
    {
        $maxPointSum = 0;

        if ($this->retailSpace->isSet()) {
            $maxPointSum += $this->retailSpace->getMaxPoints();
        }

        if ($this->secondarySpace->isSet() === true) {
            $maxPointSum += $this->secondarySpace->getPoints();
        }

        if ($this->outdoorSalesArea->isSet() === true) {
            $maxPointSum += $this->outdoorSalesArea->getPoints();
        }

        if ($this->storeWidth->isSet() === true) {
            $maxPointSum += $this->storeWidth->getPoints();
        }

        if ($this->shopWindowLength->isSet() === true) {
            $maxPointSum += $this->shopWindowLength->getPoints();
        }

        if ($this->locationFactors->isSet() === true) {
            $maxPointSum += $this->locationFactors->getPoints();
        }

        return $maxPointSum;
    }
}
