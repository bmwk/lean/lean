<?php

declare(strict_types=1);

namespace App\Domain\Entity\SettlementConcept;

use App\Domain\Entity\CreatedAtTrait;
use App\Repository\SettlementConcept\SettlementConceptAuthenticationTokenGenerationRequestRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\Table;
use Symfony\Component\Uid\Uuid;

#[Entity(repositoryClass: SettlementConceptAuthenticationTokenGenerationRequestRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[HasLifecycleCallbacks]
class SettlementConceptAuthenticationTokenGenerationRequest
{
    use CreatedAtTrait;

    #[Id]
    #[GeneratedValue]
    #[Column(type: Types::INTEGER, nullable: false, options: ['unsigned' => true])]
    private ?int $id = null;

    #[Column(type: 'uuid', unique: true, nullable: false)]
    private Uuid $uuid;

    #[OneToOne]
    #[JoinColumn(nullable: false)]
    private SettlementConceptAccountUser $settlementConceptAccountUser;

    #[Column(type: Types::DATETIME_IMMUTABLE, nullable: false)]
    private \DateTimeImmutable $validTill;

    public static function createFromSettlementConceptAccountUser(SettlementConceptAccountUser $settlementConceptAccountUser): self
    {
        $settlementConceptAuthenticationTokenGenerationRequest = new self();

        $settlementConceptAuthenticationTokenGenerationRequest
            ->setUuid(Uuid::v6())
            ->setSettlementConceptAccountUser($settlementConceptAccountUser);

        return $settlementConceptAuthenticationTokenGenerationRequest;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getUuid(): Uuid
    {
        return $this->uuid;
    }

    public function setUuid(Uuid $uuid): self
    {
        $this->uuid = $uuid;

        return $this;
    }

    public function getSettlementConceptAccountUser(): SettlementConceptAccountUser
    {
        return $this->settlementConceptAccountUser;
    }

    public function setSettlementConceptAccountUser(SettlementConceptAccountUser $settlementConceptAccountUser): self
    {
        $this->settlementConceptAccountUser = $settlementConceptAccountUser;

        return $this;
    }

    public function getValidTill(): \DateTimeImmutable
    {
        return $this->validTill;
    }

    public function setValidTill(\DateTimeImmutable $validTill): self
    {
        $this->validTill = $validTill;

        return $this;
    }
}
