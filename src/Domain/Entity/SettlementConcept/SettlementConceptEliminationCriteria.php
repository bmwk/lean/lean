<?php

declare(strict_types=1);

namespace App\Domain\Entity\SettlementConcept;

use App\Domain\Entity\EliminationCriterion;
use App\Repository\SettlementConcept\SettlementConceptEliminationCriteriaRepository;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\Table;

#[Entity(repositoryClass: SettlementConceptEliminationCriteriaRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
class SettlementConceptEliminationCriteria
{
    #[Id]
    #[GeneratedValue]
    #[Column(type: 'integer', nullable: false, options: ['unsigned' => true])]
    private ?int $id = null;

    #[OneToOne(fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private EliminationCriterion $propertyOfferTypes;

    #[OneToOne(fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private EliminationCriterion $minSpace;

    #[OneToOne(fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private EliminationCriterion $maxSpace;

    #[OneToOne(fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private EliminationCriterion $outdoorSpaceRequired;

    #[OneToOne(fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private EliminationCriterion $groundLevelSalesAreaRequired;

    #[OneToOne(fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private EliminationCriterion $barrierFreeAccessRequired;

    #[OneToOne(fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private EliminationCriterion $locationCategories;

    #[OneToOne(fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private EliminationCriterion $parkingLotsRequired;

    #[OneToOne(fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private EliminationCriterion $ageStructures;

    #[OneToOne(fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private EliminationCriterion $industryClassification;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getPropertyOfferTypes(): EliminationCriterion
    {
        return $this->propertyOfferTypes;
    }

    public function setPropertyOfferTypes(EliminationCriterion $propertyOfferTypes): self
    {
        $this->propertyOfferTypes = $propertyOfferTypes;
        return $this;
    }

    public function getMinSpace(): EliminationCriterion
    {
        return $this->minSpace;
    }

    public function setMinSpace(EliminationCriterion $minSpace): self
    {
        $this->minSpace = $minSpace;
        return $this;
    }

    public function getMaxSpace(): EliminationCriterion
    {
        return $this->maxSpace;
    }

    public function setMaxSpace(EliminationCriterion $maxSpace): self
    {
        $this->maxSpace = $maxSpace;
        return $this;
    }

    public function getOutdoorSpaceRequired(): EliminationCriterion
    {
        return $this->outdoorSpaceRequired;
    }

    public function setOutdoorSpaceRequired(EliminationCriterion $outdoorSpaceRequired): self
    {
        $this->outdoorSpaceRequired = $outdoorSpaceRequired;
        return $this;
    }

    public function getGroundLevelSalesAreaRequired(): EliminationCriterion
    {
        return $this->groundLevelSalesAreaRequired;
    }

    public function setGroundLevelSalesAreaRequired(EliminationCriterion $groundLevelSalesAreaRequired): self
    {
        $this->groundLevelSalesAreaRequired = $groundLevelSalesAreaRequired;
        return $this;
    }

    public function getBarrierFreeAccessRequired(): EliminationCriterion
    {
        return $this->barrierFreeAccessRequired;
    }

    public function setBarrierFreeAccessRequired(EliminationCriterion $barrierFreeAccessRequired): self
    {
        $this->barrierFreeAccessRequired = $barrierFreeAccessRequired;
        return $this;
    }

    public function getLocationCategories(): EliminationCriterion
    {
        return $this->locationCategories;
    }

    public function setLocationCategories(EliminationCriterion $locationCategories): self
    {
        $this->locationCategories = $locationCategories;
        return $this;
    }

    public function getParkingLotsRequired(): EliminationCriterion
    {
        return $this->parkingLotsRequired;
    }

    public function setParkingLotsRequired(EliminationCriterion $parkingLotsRequired): self
    {
        $this->parkingLotsRequired = $parkingLotsRequired;
        return $this;
    }

    public function getAgeStructures(): EliminationCriterion
    {
        return $this->ageStructures;
    }

    public function setAgeStructures(EliminationCriterion $ageStructures): self
    {
        $this->ageStructures = $ageStructures;
        return $this;
    }

    public function getIndustryClassification(): EliminationCriterion
    {
        return $this->industryClassification;
    }

    public function setIndustryClassification(EliminationCriterion $industryClassification): self
    {
        $this->industryClassification = $industryClassification;
        return $this;
    }

    public function isEliminated(): bool
    {
        if ($this->propertyOfferTypes->isSet() === true && $this->propertyOfferTypes->isEvaluationResult() === false) {
            return true;
        }

        if ($this->minSpace->isSet() === true && $this->minSpace->isEvaluationResult() === false) {
            return true;
        }

        if ($this->maxSpace->isSet() === true && $this->maxSpace->isEvaluationResult() === false) {
            return true;
        }

        if ($this->outdoorSpaceRequired->isSet() === true && $this->outdoorSpaceRequired->isEvaluationResult() === false) {
            return true;
        }

        if ($this->groundLevelSalesAreaRequired->isSet() === true && $this->groundLevelSalesAreaRequired->isEvaluationResult() === false) {
            return true;
        }

        if ($this->barrierFreeAccessRequired->isSet() === true && $this->barrierFreeAccessRequired->isEvaluationResult() === false) {
            return true;
        }

        if ($this->locationCategories->isSet() === true && $this->locationCategories->isEvaluationResult() === false) {
            return true;
        }

        if ($this->parkingLotsRequired->isSet() === true && $this->parkingLotsRequired->isEvaluationResult() === false) {
            return true;
        }

        if ($this->ageStructures->isSet() === true && $this->ageStructures->isEvaluationResult() === false) {
            return true;
        }

        if ($this->industryClassification->isSet() === true && $this->industryClassification->isEvaluationResult() === false) {
            return true;
        }

        return false;
    }
}
