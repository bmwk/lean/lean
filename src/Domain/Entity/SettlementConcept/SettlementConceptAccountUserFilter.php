<?php

declare(strict_types=1);

namespace App\Domain\Entity\SettlementConcept;

class SettlementConceptAccountUserFilter
{
    private ?array $enabledStates = null;

    public function getEnabledStates(): ?array
    {
        return $this->enabledStates;
    }

    public function setEnabledStates(?array $enabledState): self
    {
        $this->enabledStates = $enabledState;

        return $this;
    }

    public function isFilterActive(): bool
    {
        if (empty($this->enabledStates) === true) {
            return false;
        }

        return true;
    }
}
