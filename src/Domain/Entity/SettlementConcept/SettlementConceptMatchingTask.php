<?php

declare(strict_types=1);

namespace App\Domain\Entity\SettlementConcept;

use App\Domain\Entity\AbstractTask;
use App\Domain\Entity\Property\BuildingUnit;
use App\Repository\SettlementConcept\SettlementConceptMatchingTaskRepository;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\Table;

#[Entity(repositoryClass: SettlementConceptMatchingTaskRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[HasLifecycleCallbacks]
class SettlementConceptMatchingTask extends AbstractTask
{
    #[OneToOne(inversedBy: 'settlementConceptMatchingTask')]
    #[JoinColumn(nullable: false)]
    private BuildingUnit $buildingUnit;

    public function getBuildingUnit(): BuildingUnit
    {
        return $this->buildingUnit;
    }

    public function setBuildingUnit(BuildingUnit $buildingUnit): self
    {
        $this->buildingUnit = $buildingUnit;

        return $this;
    }
}
