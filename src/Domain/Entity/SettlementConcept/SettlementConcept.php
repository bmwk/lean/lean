<?php

declare(strict_types=1);

namespace App\Domain\Entity\SettlementConcept;

use App\Domain\Entity\AccountTrait;
use App\Domain\Entity\AgeStructure;
use App\Domain\Entity\Classification\IndustryClassification;
use App\Domain\Entity\Classification\NaceClassification;
use App\Domain\Entity\CreatedAtTrait;
use App\Domain\Entity\PriceSegmentType;
use App\Domain\Entity\PropertyOfferType;
use App\Domain\Entity\UpdatedAtTrait;
use App\Repository\SettlementConcept\SettlementConceptRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\Table;

#[Entity(repositoryClass: SettlementConceptRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[HasLifecycleCallbacks]
class SettlementConcept
{
    use AccountTrait;
    use CreatedAtTrait;
    use UpdatedAtTrait;

    #[Id]
    #[GeneratedValue]
    #[Column(type: 'integer', nullable: false, options: ['unsigned' => true])]
    private ?int $id = null;

    #[ManyToOne]
    #[JoinColumn(nullable: false)]
    private SettlementConceptAccountUser $settlementConceptAccountUser;

    #[Column(type: 'json', nullable: true, enumType: PropertyOfferType::class)]
    private ?array $propertyOfferTypes = null;

    #[OneToOne(fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private SettlementConceptPropertyRequirement $settlementConceptPropertyRequirement;

    #[Column(type: 'json', nullable: true, enumType: AgeStructure::class)]
    private ?array $ageStructures = null;

    #[Column(type: 'json', nullable: true, enumType: PriceSegmentType::class)]
    private ?array $priceSegments = null;

    #[ManyToOne(fetch: 'EAGER')]
    #[JoinColumn(nullable: true)]
    private ?NaceClassification $naceClassification = null;

    #[ManyToOne(fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private IndustryClassification $industryClassification;

    #[Column(type: 'text', nullable: true)]
    private ?string $description = null;

    #[Column(type: 'string', length: 256, nullable: true)]
    private ?string $externalReferenceNumber = null;

    /**
     * @var Collection<int, SettlementConceptExposeForwarding >|SettlementConceptExposeForwarding[]
     */
    #[OneToMany(mappedBy: 'settlementConcept', targetEntity: SettlementConceptExposeForwarding::class)]
    private Collection|array $settlementConceptExposeForwardings;

    /**
     * @var Collection<int, SettlementConceptMatchingResponse>|SettlementConceptMatchingResponse[]
     */
    #[OneToMany(mappedBy: 'settlementConcept', targetEntity: SettlementConceptMatchingResponse::class)]
    private Collection|array $settlementConceptMatchingResponses;

    public function __construct()
    {
        $this->settlementConceptExposeForwardings = new ArrayCollection();
        $this->settlementConceptMatchingResponses = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getPropertyOfferTypes(): ?array
    {
        return $this->propertyOfferTypes;
    }

    public function setPropertyOfferTypes(?array $propertyOfferTypes): self
    {
        $this->propertyOfferTypes = $propertyOfferTypes;

        return $this;
    }

    public function getSettlementConceptPropertyRequirement(): SettlementConceptPropertyRequirement
    {
        return $this->settlementConceptPropertyRequirement;
    }

    public function setSettlementConceptPropertyRequirement(SettlementConceptPropertyRequirement $settlementConceptPropertyRequirement): self
    {
        $this->settlementConceptPropertyRequirement = $settlementConceptPropertyRequirement;

        return $this;
    }

    public function getAgeStructures(): ?array
    {
        return $this->ageStructures;
    }

    public function setAgeStructures(?array $ageStructures): self
    {
        $this->ageStructures = $ageStructures;

        return $this;
    }

    public function getPriceSegments(): ?array
    {
        return $this->priceSegments;
    }

    public function setPriceSegments(?array $priceSegments): self
    {
        $this->priceSegments = $priceSegments;

        return $this;
    }

    public function getNaceClassification(): ?NaceClassification
    {
        return $this->naceClassification;
    }

    public function setNaceClassification(?NaceClassification $naceClassification): self
    {
        $this->naceClassification = $naceClassification;

        return $this;
    }

    public function getIndustryClassification(): IndustryClassification
    {
        return $this->industryClassification;
    }

    public function setIndustryClassification(IndustryClassification $industryClassification): self
    {
        $this->industryClassification = $industryClassification;

        return $this;
    }

    public function getSettlementConceptAccountUser(): SettlementConceptAccountUser
    {
        return $this->settlementConceptAccountUser;
    }

    public function setSettlementConceptAccountUser(SettlementConceptAccountUser $settlementConceptAccountUser): self
    {
        $this->settlementConceptAccountUser = $settlementConceptAccountUser;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getExternalReferenceNumber(): ?string
    {
        return $this->externalReferenceNumber;
    }

    public function setExternalReferenceNumber(?string $externalReferenceNumber): self
    {
        $this->externalReferenceNumber = $externalReferenceNumber;

        return $this;
    }

    /**
     * @return  Collection<int, SettlementConceptExposeForwarding >|SettlementConceptExposeForwarding[]
     */
    public function getSettlementConceptExposeForwardings(): Collection|array
    {
        return $this->settlementConceptExposeForwardings;
    }

    /**
     * @param Collection<int, SettlementConceptExposeForwarding >|SettlementConceptExposeForwarding[] $settlementConceptExposeForwardings
     */
    public function setSettlementConceptExposeForwardings(Collection|array $settlementConceptExposeForwardings): self
    {
        $this->settlementConceptExposeForwardings = $settlementConceptExposeForwardings;

        return $this;
    }

    /**
     * @return  Collection<int, SettlementConceptMatchingResponse>|SettlementConceptMatchingResponse[]
     */
    public function getSettlementConceptMatchingResponses(): Collection|array
    {
        return $this->settlementConceptMatchingResponses;
    }

    /**
     * @param Collection<int, SettlementConceptMatchingResponse>|SettlementConceptMatchingResponse[] $settlementConceptMatchingResponses
     */
    public function setSettlementConceptMatchingResponses(Collection|array $settlementConceptMatchingResponses): self
    {
        $this->settlementConceptMatchingResponses = $settlementConceptMatchingResponses;

        return $this;
    }
}
