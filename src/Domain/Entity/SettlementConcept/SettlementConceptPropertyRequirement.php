<?php

declare(strict_types=1);

namespace App\Domain\Entity\SettlementConcept;

use App\Domain\Entity\ParkingLotRequirementType;
use App\Domain\Entity\LocationCategory;
use App\Domain\Entity\LocationFactor;
use App\Domain\Entity\ValueRequirement;
use App\Repository\SettlementConcept\SettlementConceptPropertyRequirementRepository;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\Table;

#[Entity(repositoryClass: SettlementConceptPropertyRequirementRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
class SettlementConceptPropertyRequirement
{
    #[Id]
    #[GeneratedValue]
    #[Column(type: 'integer', nullable: false, options: ['unsigned' => true])]
    private ?int $id = null;

    #[OneToOne(fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private ValueRequirement $space;

    #[OneToOne(fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private ValueRequirement $secondarySpace;

    #[Column(type: 'boolean', nullable: false)]
    private bool $outdoorSalesAreaRequired = false;

    #[OneToOne(fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private ValueRequirement $outdoorSalesArea;

    #[OneToOne(fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private ValueRequirement $storeWidth;

    #[OneToOne(fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private ValueRequirement $shopWindowLength;

    #[Column(type: 'boolean', nullable: false)]
    private bool $groundLevelSalesAreaRequired = false;

    #[Column(type: 'boolean', nullable: false)]
    private bool $barrierFreeAccessRequired = false;

    #[Column(type: 'smallint', nullable: true, enumType: ParkingLotRequirementType::class, options: ['unsigned' => true])]
    private ?ParkingLotRequirementType $parkingLotsRequired = null;

    #[Column(type: 'json', nullable: true, enumType: LocationCategory::class)]
    private ?array $locationCategories = null;

    #[Column(type: 'json', nullable: true, enumType: LocationFactor::class)]
    private ?array $locationFactors = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getSpace(): ValueRequirement
    {
        return $this->space;
    }

    public function setSpace(ValueRequirement $space): self
    {
        $this->space = $space;
        return $this;
    }

    public function getSecondarySpace(): ValueRequirement
    {
        return $this->secondarySpace;
    }

    public function setSecondarySpace(ValueRequirement $secondarySpace): self
    {
        $this->secondarySpace = $secondarySpace;
        return $this;
    }

    public function isOutdoorSalesAreaRequired(): bool
    {
        return $this->outdoorSalesAreaRequired;
    }

    public function setOutdoorSalesAreaRequired(bool $outdoorSalesAreaRequired): self
    {
        $this->outdoorSalesAreaRequired = $outdoorSalesAreaRequired;
        return $this;
    }

    public function getOutdoorSalesArea(): ValueRequirement
    {
        return $this->outdoorSalesArea;
    }

    public function setOutdoorSalesArea(ValueRequirement $outdoorSalesArea): self
    {
        $this->outdoorSalesArea = $outdoorSalesArea;
        return $this;
    }

    public function getStoreWidth(): ValueRequirement
    {
        return $this->storeWidth;
    }

    public function setStoreWidth(ValueRequirement $storeWidth): self
    {
        $this->storeWidth = $storeWidth;
        return $this;
    }

    public function getShopWindowLength(): ValueRequirement
    {
        return $this->shopWindowLength;
    }

    public function setShopWindowLength(ValueRequirement $shopWindowLength): self
    {
        $this->shopWindowLength = $shopWindowLength;
        return $this;
    }

    public function isGroundLevelSalesAreaRequired(): bool
    {
        return $this->groundLevelSalesAreaRequired;
    }

    public function setGroundLevelSalesAreaRequired(bool $groundLevelSalesAreaRequired): self
    {
        $this->groundLevelSalesAreaRequired = $groundLevelSalesAreaRequired;
        return $this;
    }

    public function isBarrierFreeAccessRequired(): bool
    {
        return $this->barrierFreeAccessRequired;
    }

    public function setBarrierFreeAccessRequired(bool $barrierFreeAccessRequired): self
    {
        $this->barrierFreeAccessRequired = $barrierFreeAccessRequired;
        return $this;
    }

    public function getParkingLotsRequired(): ?ParkingLotRequirementType
    {
        return $this->parkingLotsRequired;
    }

    public function setParkingLotsRequired(?ParkingLotRequirementType $parkingLotsRequired): self
    {
        $this->parkingLotsRequired = $parkingLotsRequired;
        return $this;
    }

    public function getLocationCategories(): ?array
    {
        return $this->locationCategories;
    }

    public function setLocationCategories(?array $locationCategories): self
    {
        $this->locationCategories = $locationCategories;
        return $this;
    }

    public function getLocationFactors(): ?array
    {
        return $this->locationFactors;
    }

    public function setLocationFactors(?array $locationFactors): self
    {
        $this->locationFactors = $locationFactors;
        return $this;
    }
}
