<?php

declare(strict_types=1);

namespace App\Domain\Entity\SettlementConcept;

use App\Domain\Entity\AbstractExposeForwarding;
use App\Domain\Entity\Matching\AbstractMatchingResponse;
use App\Domain\Entity\Matching\AbstractMatchingResult;
use App\Domain\Entity\Property\BuildingUnit;
use App\Repository\SettlementConcept\SettlementConceptMatchingResultRepository;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\Table;

#[Entity(repositoryClass: SettlementConceptMatchingResultRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[HasLifecycleCallbacks]
class SettlementConceptMatchingResult extends AbstractMatchingResult
{
    #[ManyToOne(inversedBy: 'settlementConceptMatchingResults')]
    #[JoinColumn(nullable: false)]
    protected BuildingUnit $buildingUnit;

    #[ManyToOne]
    #[JoinColumn(nullable: false)]
    private SettlementConcept $settlementConcept;

    #[OneToOne(fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private SettlementConceptEliminationCriteria $settlementConceptEliminationCriteria;

    #[OneToOne(fetch: 'EAGER')]
    #[JoinColumn(nullable: false)]
    private SettlementConceptScoreCriteria $settlementConceptScoreCriteria;

    public function getSettlementConcept(): SettlementConcept
    {
        return $this->settlementConcept;
    }

    public function setSettlementConcept(SettlementConcept $settlementConcept): self
    {
        $this->settlementConcept = $settlementConcept;

        return $this;
    }

    public function getSettlementConceptEliminationCriteria(): SettlementConceptEliminationCriteria
    {
        return $this->settlementConceptEliminationCriteria;
    }

    public function setSettlementConceptEliminationCriteria(SettlementConceptEliminationCriteria $settlementConceptEliminationCriteria): self
    {
        $this->settlementConceptEliminationCriteria = $settlementConceptEliminationCriteria;

        return $this;
    }

    public function getSettlementConceptScoreCriteria(): SettlementConceptScoreCriteria
    {
        return $this->settlementConceptScoreCriteria;
    }

    public function setSettlementConceptScoreCriteria(SettlementConceptScoreCriteria $settlementConceptScoreCriteria): self
    {
        $this->settlementConceptScoreCriteria = $settlementConceptScoreCriteria;

        return $this;
    }

    public function fetchExposeForwarding(): ?AbstractExposeForwarding
    {
        return $this->settlementConcept->getSettlementConceptExposeForwardings()->findFirst(
            p: function (int $key, SettlementConceptExposeForwarding $settlementConceptExposeForwarding): bool {
                return $settlementConceptExposeForwarding->getBuildingUnit() === $this->getBuildingUnit();
            }
        );
    }

    public function fetchMatchingResponse(): ?AbstractMatchingResponse
    {
        return $this->settlementConcept->getSettlementConceptMatchingResponses()->findFirst(
            p: function (int $key, SettlementConceptMatchingResponse $settlementConceptMatchingResponse): bool {
                return $settlementConceptMatchingResponse->getBuildingUnit() === $this->getBuildingUnit();
            }
        );
    }
}
