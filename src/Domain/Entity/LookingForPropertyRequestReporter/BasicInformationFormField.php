<?php

declare(strict_types=1);

namespace App\Domain\Entity\LookingForPropertyRequestReporter;

enum BasicInformationFormField: int
{
    case TITLE = 0;
    case REQUEST_REASON = 1;
    case INDUSTRY_CLASSIFICATION = 2;
    case INDUSTRY_SECTOR = 3;
    case INDUSTRY_DETAILS = 4;
    case CONCEPT_DESCRIPTION = 5;
    case REQUEST_AVAILABLE_TILL = 6;

    public function getName(): string
    {
        return match ($this) {
            self::TITLE => 'Titel ihres Gesuchs',
            self::REQUEST_REASON => 'Gesuchsgrund',
            self::INDUSTRY_CLASSIFICATION => 'Vorgesehene Nutzung',
            self::INDUSTRY_SECTOR => 'Branchenbereich',
            self::INDUSTRY_DETAILS => 'Branchendetails',
            self::CONCEPT_DESCRIPTION => 'Beschreibung des Konzepts',
            self::REQUEST_AVAILABLE_TILL => 'Gesuch soll automatisch enden',
        };
    }
}
