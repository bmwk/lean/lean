<?php

declare(strict_types=1);

namespace App\Domain\Entity\LookingForPropertyRequestReporter;

enum ReporterInformationFormField: int
{
    case SALUTATION = 0;
    case NAME = 1;
    case FIRST_NAME = 2;
    case EMAIL = 3;
    case PHONE_NUMBER = 4;
    case PRIVACY_POLICY_ACCEPT = 5;

    public function getName(): string
    {
        return match ($this) {
            self::SALUTATION => 'Anrede',
            self::NAME => 'Nachname',
            self::FIRST_NAME => 'Vorname',
            self::EMAIL => 'E-Mail Adresse',
            self::PHONE_NUMBER => 'Telefon',
            self::PRIVACY_POLICY_ACCEPT => 'Datenschutz akzeptieren',
        };
    }
}
