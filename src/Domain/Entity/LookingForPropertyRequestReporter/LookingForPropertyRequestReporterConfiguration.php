<?php

declare(strict_types=1);

namespace App\Domain\Entity\LookingForPropertyRequestReporter;

use App\Domain\Entity\Account;
use App\Domain\Entity\Classification\IndustryClassification;
use App\Domain\Entity\CreatedAtTrait;
use App\Domain\Entity\UpdatedAtTrait;
use App\Repository\LookingForPropertyRequestReporterConfigurationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\Table;
use Symfony\Component\Uid\Uuid;

#[Entity(repositoryClass: LookingForPropertyRequestReporterConfigurationRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
#[HasLifecycleCallbacks]
class LookingForPropertyRequestReporterConfiguration
{
    use CreatedAtTrait;
    use UpdatedAtTrait;

    #[Id]
    #[GeneratedValue]
    #[Column(type: Types::INTEGER, nullable: false, options: ['unsigned' => true])]
    private ?int $id = null;

    #[OneToOne]
    #[JoinColumn(nullable: false)]
    private Account $account;

    #[Column(type: 'uuid', unique: true, nullable: false)]
    private Uuid $accountKey;

    /**
     * @var Collection<int, IndustryClassification>|IndustryClassification[]
     */
    #[ManyToMany(targetEntity: IndustryClassification::class)]
    #[JoinTable(name: 'lfpr_reporter_configuration_industry_classification')]
    private Collection|array $industryClassifications;

    /**
     * @var BasicInformationFormField[]
     */
    #[Column(type: Types::JSON, nullable: false, enumType: BasicInformationFormField::class)]
    private array $requiredBasicInformationFormFields;

    /**
     * @var LocationInformationFormField[]
     */
    #[Column(type: Types::JSON, nullable: false, enumType: LocationInformationFormField::class)]
    private array $requiredLocationFormFields;

    /**
     * @var PropertyInformationFormField[]
     */
    #[Column(type: Types::JSON, nullable: false, enumType: PropertyInformationFormField::class)]
    private array $requiredPropertyInformationFormFields;

    /**
     * @var ReporterInformationFormField[]
     */
    #[Column(type: Types::JSON, nullable: false, enumType: ReporterInformationFormField::class)]
    private array $requiredReporterFormFields;

    #[Column(type: Types::STRING, length: 12, nullable: false)]
    private string $firstColor;

    #[Column(type: Types::STRING, length: 12, nullable: false)]
    private string $firstFontColor;

    #[Column(type: Types::STRING, length: 12, nullable: false)]
    private string $secondaryColor;

    #[Column(type: Types::STRING, length: 12, nullable: false)]
    private string $secondaryFontColor;

    #[Column(type: Types::TEXT, nullable: false)]
    private string $privacyPolicy;

    #[Column(type: Types::STRING, length: 190, nullable: true)]
    private ?string $title = null;

    #[Column(type: Types::TEXT, nullable: true)]
    private ?string $text = null;

    #[Column(type: Types::STRING, length: 190, nullable: false)]
    private string $reportNotificationEmail;

    public function __construct()
    {
        $this->industryClassifications = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getAccount(): Account
    {
        return $this->account;
    }

    public function setAccount(Account $account): self
    {
        $this->account = $account;

        return $this;
    }

    public function getAccountKey(): Uuid
    {
        return $this->accountKey;
    }

    public function setAccountKey(Uuid $accountKey): self
    {
        $this->accountKey = $accountKey;

        return $this;
    }

    public function getIndustryClassifications(): Collection|array
    {
        return $this->industryClassifications;
    }

    public function setIndustryClassifications(Collection|array $industryClassifications): self
    {
        $this->industryClassifications = $industryClassifications;

        return $this;
    }

    public function getRequiredBasicInformationFormFields(): array
    {
        return $this->requiredBasicInformationFormFields;
    }

    public function setRequiredBasicInformationFormFields(array $requiredBasicInformationFormFields): self
    {
        $this->requiredBasicInformationFormFields = $requiredBasicInformationFormFields;

        return $this;
    }

    public function addRequiredBasicInformationFormField(BasicInformationFormField $basicInformationFormField): self
    {
        $this->requiredBasicInformationFormFields[] = $basicInformationFormField;

        return $this;
    }

    public function getRequiredLocationFormFields(): array
    {
        return $this->requiredLocationFormFields;
    }

    public function setRequiredLocationFormFields(array $requiredLocationFormFields): self
    {
        $this->requiredLocationFormFields = $requiredLocationFormFields;

        return $this;
    }

    public function addRequiredLocationInformationFormField(LocationInformationFormField $locationInformationFormField): self
    {
        $this->requiredLocationFormFields[] = $locationInformationFormField;

        return $this;
    }

    public function getRequiredPropertyInformationFormFields(): array
    {
        return $this->requiredPropertyInformationFormFields;
    }

    public function setRequiredPropertyInformationFormFields(array $requiredPropertyInformationFormFields): self
    {
        $this->requiredPropertyInformationFormFields = $requiredPropertyInformationFormFields;

        return $this;
    }

    public function addRequiredPropertyInformationFormField(PropertyInformationFormField $propertyInformationFormField): self
    {
        $this->requiredPropertyInformationFormFields[] = $propertyInformationFormField;

        return $this;
    }

    public function getRequiredReporterFormFields(): array
    {
        return $this->requiredReporterFormFields;
    }

    public function setRequiredReporterFormFields(array $requiredReporterFormFields): self
    {
        $this->requiredReporterFormFields = $requiredReporterFormFields;

        return $this;
    }

    public function addRequiredReporterInformationFormField(ReporterInformationFormField $reporterInformationFormField): self
    {
        $this->requiredReporterFormFields[] = $reporterInformationFormField;

        return $this;
    }

    public function getFirstColor(): string
    {
        return $this->firstColor;
    }

    public function setFirstColor(string $firstColor): self
    {
        $this->firstColor = $firstColor;

        return $this;
    }

    public function getFirstFontColor(): string
    {
        return $this->firstFontColor;
    }

    public function setFirstFontColor(string $firstFontColor): self
    {
        $this->firstFontColor = $firstFontColor;

        return $this;
    }

    public function getSecondaryColor(): string
    {
        return $this->secondaryColor;
    }

    public function setSecondaryColor(string $secondaryColor): self
    {
        $this->secondaryColor = $secondaryColor;

        return $this;
    }

    public function getSecondaryFontColor(): string
    {
        return $this->secondaryFontColor;
    }

    public function setSecondaryFontColor(string $secondaryFontColor): self
    {
        $this->secondaryFontColor = $secondaryFontColor;

        return $this;
    }

    public function getPrivacyPolicy(): string
    {
        return $this->privacyPolicy;
    }

    public function setPrivacyPolicy(string $privacyPolicy): self
    {
        $this->privacyPolicy = $privacyPolicy;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(?string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getReportNotificationEmail(): string
    {
        return $this->reportNotificationEmail;
    }

    public function setReportNotificationEmail(string $reportNotificationEmail): self
    {
        $this->reportNotificationEmail = $reportNotificationEmail;

        return $this;
    }
}
