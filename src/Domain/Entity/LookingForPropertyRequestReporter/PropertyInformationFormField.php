<?php

declare(strict_types=1);

namespace App\Domain\Entity\LookingForPropertyRequestReporter;

enum PropertyInformationFormField: int
{
    case TOTAL_SPACE = 0;
    case RETAIL_SPACE = 1;
    case OUTDOOR_SPACE = 2;
    case SUBSIDIARY_SPACE = 3;
    case USABLE_SPACE = 4;
    case STORAGE_SPACE = 5;
    case NUMBER_OF_PARKING_LOTS = 6;
    case ROOM_COUNT = 7;
    case SHOP_WINDOW_LENGTH = 8;
    case SHOP_WIDTH = 9;
    case PURCHASE_PRICE_NET = 10;
    case PURCHASE_PRICE_GROSS = 11;
    case PURCHASE_PRICE_PER_SQUARE_METER = 12;
    case NET_COLD_RENT = 13;
    case COLD_RENT = 14;
    case RENTAL_PRICE_PER_SQUARE_METER = 15;
    case LEASE = 16;
    case EARLIEST_AVAILABLE_FROM = 17;
    case LATEST_AVAILABLE_FROM = 18;

    public function getName(): string
    {
        return match ($this) {
            self::TOTAL_SPACE => 'Gesamtflächenbedarf',
            self::RETAIL_SPACE => 'Verkaufsflächenbedarf',
            self::OUTDOOR_SPACE => 'Außenflächebedarf',
            self::SUBSIDIARY_SPACE => 'Nebenflächenbedarf',
            self::USABLE_SPACE => 'Nutzflächenbedarf',
            self::STORAGE_SPACE => 'Lagerflächenbedarf',
            self::NUMBER_OF_PARKING_LOTS => 'Anzahl benötiger Parkplätze',
            self::ROOM_COUNT => 'Anzahl Räume',
            self::SHOP_WINDOW_LENGTH => 'Schaufensterbreite',
            self::SHOP_WIDTH => 'Ladenbreite',
            self::PURCHASE_PRICE_NET => 'Nettokaufpreis',
            self::PURCHASE_PRICE_GROSS => 'Bruttokaufpreis',
            self::PURCHASE_PRICE_PER_SQUARE_METER => 'Kaufpreis pro m²',
            self::NET_COLD_RENT => 'Nettokaltmiete',
            self::COLD_RENT => 'Kaltmiete',
            self::RENTAL_PRICE_PER_SQUARE_METER => 'Mietpreis pro m²',
            self::LEASE => 'Pacht',
            self::EARLIEST_AVAILABLE_FROM => 'frühester Bezugsdatum',
            self::LATEST_AVAILABLE_FROM => 'spätester Bezugsdatum',
        };
    }
}
