<?php

declare(strict_types=1);

namespace App\Domain\Entity\LookingForPropertyRequestReporter;

enum LocationInformationFormField: int
{
    case QUARTERS = 0;
    case LOCATION_CATEGORIES = 1;
    case LOCATION_FACTORS = 3;

    public function getName(): string
    {
        return match ($this) {
            self::QUARTERS => 'Stadtteile',
            self::LOCATION_CATEGORIES => 'Einzelhandelslagen',
            self::LOCATION_FACTORS => 'Standortfaktoren'
        };
    }
}
