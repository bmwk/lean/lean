<?php

declare(strict_types=1);

namespace App\Domain\LookingForPropertyRequestReport;

use App\Domain\Classification\ClassificationService;
use App\Domain\Entity\Account;
use App\Domain\Entity\LookingForPropertyRequest\PropertyRequirement;
use App\Domain\Entity\LookingForPropertyRequestReport\LookingForPropertyRequestReport;
use App\Domain\Entity\LookingForPropertyRequestReport\LookingForPropertyRequestReportFilter;
use App\Domain\Entity\LookingForPropertyRequestReport\LookingForPropertyRequestReportStatistics;
use App\Domain\Entity\LookingForPropertyRequestReport\LookingForPropertyRequestReportNote;
use App\Domain\Entity\Place;
use App\Domain\Entity\PriceRequirement;
use App\Domain\Entity\PropertyMandatoryRequirement;
use App\Domain\Entity\PropertyValueRangeRequirement;
use App\Domain\Location\LocationService;
use App\Domain\LookingForPropertyRequestReporter\Api\Entity\Request\ReportCreateRequest;
use App\Repository\LookingForPropertyRequestReport\LookingForPropertyRequestReportNoteRepository;
use App\Repository\LookingForPropertyRequestReport\LookingForPropertyRequestReportRepository;
use Symfony\Component\Uid\Uuid;

class LookingForPropertyRequestReportService
{
    public function __construct(
        private readonly LocationService $locationService,
        private readonly ClassificationService $classificationService,
        private readonly LookingForPropertyRequestReportRepository $lookingForPropertyRequestReportRepository,
        private readonly LookingForPropertyRequestReportNoteRepository $lookingForPropertyRequestReportNoteRepository
    ) {
    }

    public function fetchLookingForPropertyRequestReportByUuid(Uuid $uuid): ?LookingForPropertyRequestReport
    {
        return $this->lookingForPropertyRequestReportRepository->findOneBy(criteria: [
            'uuid'    => $uuid,
            'deleted' => false,
        ]);
    }

    public function  fetchLookingForPropertyRequestReportNoteById(int $id): ?LookingForPropertyRequestReportNote
    {
        return $this->lookingForPropertyRequestReportNoteRepository->find(id: $id);
    }

    public function fetchLookingForPropertyRequestReportByIdAndAccount(int $id, Account $account): ?LookingForPropertyRequestReport
    {
        return $this->lookingForPropertyRequestReportRepository->findOneBy(criteria: [
            'id'             => $id,
            'account'        => $account,
            'emailConfirmed' => true,
            'deleted'        => false,
        ]);
    }

    /**
     * @return LookingForPropertyRequestReport[]
     */
    public function fetchUnprocessedLookingForPropertyRequestReportsByAccount(
        Account $account,
        ?bool $emailConfirmed = null,
        ?LookingForPropertyRequestReportFilter $lookingForPropertyRequestReportFilter = null
    ): array {
        return $this->lookingForPropertyRequestReportRepository->findByAccount(
            account: $account,
            processed: false,
            emailConfirmed: $emailConfirmed,
            lookingForPropertyRequestReportFilter: $lookingForPropertyRequestReportFilter
        );
    }

    /**
     * @return LookingForPropertyRequestReport[]
     */
    public function fetchProcessedLookingForPropertyRequestReportsByAccount(
        Account $account,
        ?bool $emailConfirmed = null,
        ?LookingForPropertyRequestReportFilter $lookingForPropertyRequestReportFilter = null
    ): array {
        return $this->lookingForPropertyRequestReportRepository->findByAccount(
            account: $account,
            processed: true,
            emailConfirmed: $emailConfirmed,
            lookingForPropertyRequestReportFilter: $lookingForPropertyRequestReportFilter
        );
    }

    public function createLookingForPropertyRequestReportFromReportCreateRequest(ReportCreateRequest $reportCreateRequest): LookingForPropertyRequestReport
    {
        $lookingForPropertyRequestReport = new LookingForPropertyRequestReport();

        $basicInformation = $reportCreateRequest->getBasicInformation();
        $locationRequirement = $reportCreateRequest->getLocationRequirement();
        $propertyRequirementInformation = $reportCreateRequest->getPropertyRequirement();
        $reporter = $reportCreateRequest->getReporter();

        $inPlaces = array_map(
            callback: function (Uuid $placeUuid): Place {
                return $this->locationService->fetchPlaceByUuid(uuid: $placeUuid);
            },
            array: $locationRequirement->getPlaceUuids()
        );

        $inQuarters = [];

        if ($locationRequirement->getQuarterPlaceUuids() !== null) {
            $inQuarters = array_map(
                callback: function (Uuid $quarterPlaceUuid): Place {
                    return $this->locationService->fetchPlaceByUuid(uuid: $quarterPlaceUuid);
                },
                array: $locationRequirement->getQuarterPlaceUuids()
            );
        }

        $industryClassification = $this->classificationService->fetchIndustryClassificationById($basicInformation->getIndustryClassificationId());

        $propertyMandatoryRequirement = new PropertyMandatoryRequirement();

        $propertyMandatoryRequirement
            ->setRoofingRequired($propertyRequirementInformation->isRoofingRequired())
            ->setOutdoorSpaceRequired($propertyRequirementInformation->isOutdoorSpaceRequired())
            ->setShowroomRequired($propertyRequirementInformation->isShowroomRequired())
            ->setShopWindowRequired($propertyRequirementInformation->isShopWindowRequired())
            ->setGroundLevelSalesAreaRequired($propertyRequirementInformation->isGroundLevelSalesAreaRequired())
            ->setBarrierFreeAccessRequired($propertyRequirementInformation->isBarrierFreeAccessRequired());

        $propertyValueRangeRequirement = new PropertyValueRangeRequirement();

        $propertyValueRangeRequirement
            ->setTotalSpace($propertyRequirementInformation->getTotalSpace())
            ->setRetailSpace($propertyRequirementInformation->getRetailSpace())
            ->setOutdoorSpace($propertyRequirementInformation->getOutdoorSpace())
            ->setSubsidiarySpace($propertyRequirementInformation->getSubsidarySpace())
            ->setUsableSpace($propertyRequirementInformation->getUsableSpace())
            ->setStorageSpace($propertyRequirementInformation->getStorageSpace())
            ->setShopWindowLength($propertyRequirementInformation->getShopWindowLength())
            ->setShopWidth($propertyRequirementInformation->getShopWidth());

        $propertyRequirement = new PropertyRequirement();

        $propertyRequirement
            ->setIndustryClassification($industryClassification)
            ->setNumberOfParkingLots($propertyRequirementInformation->getNumberOfParkingLots())
            ->setRoomCount($propertyRequirementInformation->getRoomCount())
            ->setLocationCategories($locationRequirement->getLocationCategories())
            ->setEarliestAvailableFrom($propertyRequirementInformation->getEarliestAvailableFrom())
            ->setLatestAvailableFrom($propertyRequirementInformation->getLatestAvailableFrom())
            ->setInQuarters($inQuarters)
            ->setInPlaces($inPlaces)
            ->setPropertyMandatoryRequirement($propertyMandatoryRequirement)
            ->setPropertyValueRangeRequirement($propertyValueRangeRequirement);

        $priceRequirement = new PriceRequirement();

        $priceRequirement
            ->setPropertyOfferTypes($propertyRequirementInformation->getPropertyOfferTypes())
            ->setPurchasePriceNet($propertyRequirementInformation->getPurchasePriceNet())
            ->setPurchasePriceGross($propertyRequirementInformation->getPurchasePriceGross())
            ->setPurchasePricePerSquareMeter($propertyRequirementInformation->getPurchasePriceGross())
            ->setNetColdRent($propertyRequirementInformation->getNetColdRent())
            ->setColdRent($propertyRequirementInformation->getColdRent())
            ->setRentalPricePerSquareMeter($propertyRequirementInformation->getRentalPricePerSquareMeter())
            ->setLease($propertyRequirementInformation->getLease())
            ->setNonCommissionable($propertyRequirementInformation->isOnlyNonCommissionable());

        $lookingForPropertyRequestReport
            ->setUuid(Uuid::v6())
            ->setReporterCompanyName($reporter->getCompanyName())
            ->setReporterSalutation($reporter->getSalutation())
            ->setReporterFirstName($reporter->getFirstName())
            ->setReporterLastName($reporter->getName())
            ->setReporterPhoneNumber($reporter->getPhoneNumber())
            ->setReporterEmail($reporter->getEmail())
            ->setReportedAt(new \DateTimeImmutable())
            ->setProcessed(false)
            ->setAdmitted(false)
            ->setRejected(false)
            ->setDeleted(false)
            ->setAnonymized(false)
            ->setConfirmationToken(Uuid::v6())
            ->setEmailConfirmed(false)
            ->setTitle($basicInformation->getTitle())
            ->setConceptDescription($basicInformation->getConceptDescription())
            ->setRequestReason($basicInformation->getRequestReason())
            ->setRequestAvailableTill($basicInformation->getRequestAvailableTill())
            ->setAutomaticEnding($basicInformation->isAutomaticEnding())
            ->setLocationFactors($locationRequirement->getLocationFactors())
            ->setPropertyRequirement($propertyRequirement)
            ->setPriceRequirement($priceRequirement);

        return $lookingForPropertyRequestReport;
    }

    public function fetchLookingForPropertyRequestReportStatisticsByAccount(Account $account): LookingForPropertyRequestReportStatistics
    {
        $lookingForPropertyRequestReportStatistics = new LookingForPropertyRequestReportStatistics();

        $lookingForPropertyRequestReportStatistics
            ->setUnprocessed(
                $this->lookingForPropertyRequestReportRepository->count(criteria: [
                    'account'        => $account,
                    'deleted'        => false,
                    'processed'      => false,
                    'emailConfirmed' => true,
                ])
            )
            ->setProcessed(
                $this->lookingForPropertyRequestReportRepository->count(criteria: [
                    'account'        => $account,
                    'deleted'        => false,
                    'processed'      => true,
                    'emailConfirmed' => true,
                ])
            );

        return $lookingForPropertyRequestReportStatistics;
    }
}
