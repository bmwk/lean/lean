<?php

declare(strict_types=1);

namespace App\Domain\LookingForPropertyRequestReport;

use App\Domain\Entity\Account;
use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\DeletionType;
use App\Domain\Entity\LookingForPropertyRequestReport\LookingForPropertyRequestReport;
use App\Domain\Entity\LookingForPropertyRequestReport\LookingForPropertyRequestReportNote;
use App\Repository\LookingForPropertyRequestReport\LookingForPropertyRequestReportRepository;
use Doctrine\ORM\EntityManagerInterface;

class LookingForPropertyRequestReportDeletionService
{
    public function __construct(
        private readonly LookingForPropertyRequestReportRepository $lookingForPropertyRequestReportRepository,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function deleteLookingForPropertyRequestReport(
        LookingForPropertyRequestReport $lookingForPropertyRequestReport,
        DeletionType $deletionType = DeletionType::SOFT,
        ?AccountUser $deletedByAccountUser = null
    ): void {
        switch ($deletionType) {
            case DeletionType::SOFT:
                $this->softDeleteLookingForPropertyRequestReport(lookingForPropertyRequestReport: $lookingForPropertyRequestReport, deletedByAccountUser: $deletedByAccountUser);
                break;
            case DeletionType::HARD:
                $this->hardDeleteLookingForPropertyRequestReport(lookingForPropertyRequestReport: $lookingForPropertyRequestReport);
                break;
            default:
                throw new \RuntimeException(message: 'Unsupported DeletionType');
        }
    }

    public function deleteLookingForPropertyRequestsByAccount(
        Account $account,
        DeletionType $deletionType = DeletionType::SOFT,
        ?AccountUser $deletedByAccountUser = null
    ): void {
        switch ($deletionType) {
            case DeletionType::SOFT:
                $this->softDeleteLookingForPropertyRequestReportsByAccount(account: $account, deletedByAccountUser: $deletedByAccountUser);
                break;
            case DeletionType::HARD:
                $this->hardDeleteLookingForPropertyRequestReportsByAccount(account: $account);
                break;
            default:
                throw new \RuntimeException(message: 'Unsupported DeletionType');
        }
    }

    public function deleteLookingForPropertyRequestReportNote(LookingForPropertyRequestReportNote $lookingForPropertyRequestReportNote): void
    {
         $this->hardDeleteLookingForPropertyRequestReportNote(lookingForPropertyRequestReportNote: $lookingForPropertyRequestReportNote);
     }

    private function softDeleteLookingForPropertyRequestReport(LookingForPropertyRequestReport $lookingForPropertyRequestReport, AccountUser $deletedByAccountUser): void
    {
        $lookingForPropertyRequestReport
            ->setDeleted(true)
            ->setDeletedAt(new \DateTimeImmutable())
            ->setDeletedByAccountUser($deletedByAccountUser);

        $this->entityManager->flush();
    }

    private function hardDeleteLookingForPropertyRequestReport(LookingForPropertyRequestReport $lookingForPropertyRequestReport): void
    {
        foreach ($lookingForPropertyRequestReport->getLookingForPropertyRequestReportNotes() as $lookingForPropertyRequestReportNote) {
            $this->deleteLookingForPropertyRequestReportNote(lookingForPropertyRequestReportNote: $lookingForPropertyRequestReportNote);
        }

        $this->entityManager->remove(entity: $lookingForPropertyRequestReport);
        $this->entityManager->flush();
    }

    private function hardDeleteLookingForPropertyRequestReportNote(LookingForPropertyRequestReportNote $lookingForPropertyRequestReportNote): void
    {
        $this->entityManager->remove(entity: $lookingForPropertyRequestReportNote);
        $this->entityManager->flush();
    }

    private function softDeleteLookingForPropertyRequestReportsByAccount(Account $account, AccountUser $deletedByAccountUser): void
    {
        $lookingForPropertyRequestReports = $this->lookingForPropertyRequestReportRepository->findBy(criteria: ['account' => $account]);

        foreach ($lookingForPropertyRequestReports as $lookingForPropertyRequestReport) {
            $this->softDeleteLookingForPropertyRequestReport(
                lookingForPropertyRequestReport: $lookingForPropertyRequestReport,
                deletedByAccountUser: $deletedByAccountUser
            );
        }
    }

    private function hardDeleteLookingForPropertyRequestReportsByAccount(Account $account): void
    {
        $lookingForPropertyRequestReports = $this->lookingForPropertyRequestReportRepository->findBy(criteria: ['account' => $account]);

        foreach ($lookingForPropertyRequestReports as $lookingForPropertyRequestReport) {
            $this->hardDeleteLookingForPropertyRequestReport(lookingForPropertyRequestReport: $lookingForPropertyRequestReport);
        }
    }
}
