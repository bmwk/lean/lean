<?php

declare(strict_types=1);

namespace App\Domain\LookingForPropertyRequestReport;

use App\Domain\Entity\Account;
use App\Domain\Entity\Statistics\AmountPerMonth;
use App\Repository\LookingForPropertyRequestReport\LookingForPropertyRequestReportRepository;

class LookingForPropertyRequestReportStatisticsService
{
    public function __construct(
        private readonly LookingForPropertyRequestReportRepository $lookingForPropertyRequestReportRepository
    ) {
    }

    public function buildAmountLookingForPropertyRequestReportsByMonth(Account $account, int $year, int $month): AmountPerMonth
    {
        $dateTimeStart = \DateTime::createFromFormat(format: 'Y-n-j H:i:s', datetime: $year . '-' . $month . '-' . '1' . ' 00:00:00');
        $dateTimeEnd = \DateTime::createFromFormat(format: 'Y-n-j H:i:s', datetime: $year . '-' . $month . '-' . '1' . ' 23:59:59')->modify(modifier: 'last day of');

        return new AmountPerMonth(
            year: (int) $dateTimeStart->format(format: 'Y'),
            month: (int) $dateTimeStart->format(format: 'n'),
            amount: $this->lookingForPropertyRequestReportRepository->countByCreatedAtRange(account: $account, dateTimeStart: $dateTimeStart, dateTimeEnd: $dateTimeEnd)
        );
    }

    /**
     * @return AmountPerMonth[]
     */
    public function buildAmountLookingForPropertyRequestReportsByMonthFromYear(Account $account, int $year): array
    {
        $amountLookingForPropertyRequestReportsByMonthFromYear = [];

        foreach (range(start: 1, end: 12) as $month) {
            $amountLookingForPropertyRequestReportsByMonthFromYear[] = $this->buildAmountLookingForPropertyRequestReportsByMonth(
                account: $account,
                year: $year,
                month: $month
            );
        }

        return $amountLookingForPropertyRequestReportsByMonthFromYear;
    }
}
