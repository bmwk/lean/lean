<?php

declare(strict_types=1);

namespace App\Domain\LookingForPropertyRequestReport;

use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\AccountUser\AccountUserRole;
use App\Domain\Entity\LookingForPropertyRequestReport\LookingForPropertyRequestReport;
use Symfony\Bundle\SecurityBundle\Security;

class LookingForPropertyRequestReportSecurityService
{
    public function __construct(
        private readonly Security $security
    ) {
    }

    public function canViewLookingForPropertyRequestReport(
        LookingForPropertyRequestReport $lookingForPropertyRequestReport,
        AccountUser $accountUser
    ): bool {
        $account = $accountUser->getAccount();

        if (
            $lookingForPropertyRequestReport->getAccount() === $account
            && (
                $this->security->isGranted(attributes: AccountUserRole::ROLE_USER->value) === true
                || $this->security->isGranted(attributes: AccountUserRole::ROLE_LOOKING_FOR_PROPERTY_REQUEST_REPORT_MANAGER->value) === true
                || $this->security->isGranted(attributes: AccountUserRole::ROLE_VIEWER->value) === true
                || $this->security->isGranted(attributes: AccountUserRole::ROLE_RESTRICTED_VIEWER->value) === true
            )
        ) {
            return true;
        }

        return false;
    }

    public function canEditLookingForPropertyRequestReport(
        LookingForPropertyRequestReport $lookingForPropertyRequestReport,
        AccountUser $accountUser
    ): bool {
        $account = $accountUser->getAccount();

        if (
            $lookingForPropertyRequestReport->getAccount() === $account
            && (
                $this->security->isGranted(attributes: AccountUserRole::ROLE_USER->value) === true
                || $this->security->isGranted(attributes: AccountUserRole::ROLE_LOOKING_FOR_PROPERTY_REQUEST_REPORT_MANAGER->value) === true
            )
        ) {
            return true;
        }

        return false;
    }

    public function canDeleteLookingForPropertyRequestReport(
        LookingForPropertyRequestReport $lookingForPropertyRequestReport,
        AccountUser $accountUser
    ): bool {
        return $this->canEditLookingForPropertyRequestReport(
            lookingForPropertyRequestReport: $lookingForPropertyRequestReport,
            accountUser: $accountUser
        );
    }
}
