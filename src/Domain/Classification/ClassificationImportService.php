<?php

declare(strict_types=1);

namespace App\Domain\Classification;

use App\Domain\Entity\Classification\ClassificationExample;
use App\Domain\Entity\Classification\IndustryClassification;
use App\Domain\Entity\Classification\NaceClassification;
use App\Domain\Entity\Classification\NaceClassificationLevel;
use Doctrine\ORM\EntityManagerInterface;

class ClassificationImportService
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly ClassificationService $classificationService
    ) {
    }

    /**
     * @return NaceClassification[]
     */
    public function importNaceClassificationsFromCsvToDatabase(array $naceClassificationCsvLines): array
    {
        $naceClassifications = [];
        $lastNaceClassification = null;
        $this->entityManager->beginTransaction();

        foreach ($naceClassificationCsvLines as $naceClassificationCsvLine) {
            $naceClassification = $this->createNaceClassificationFromCsvData($naceClassificationCsvLine);

            if ($lastNaceClassification !== null && $naceClassification->getLevel() !== NaceClassificationLevel::LEVEL_ONE) {
                $this->setNaceClassificationParent(
                    naceClassification: $naceClassification,
                    lastNaceClassification: $lastNaceClassification
                );
            }

            $existingNaceClassification = $this->classificationService->fetchNaceClassificationByCode(
                code: $naceClassification->getCode()
            );

            if ($existingNaceClassification === null) {
                $this->entityManager->persist($naceClassification);
            } else {
                $this->updateNacsClassification(
                    existingNaceClassification: $existingNaceClassification,
                    naceClassification: $naceClassification
                );
            }

            $this->entityManager->flush();
            $naceClassifications[] = $naceClassification;
            $lastNaceClassification = $naceClassification;
        }

        $this->entityManager->commit();
        return $naceClassifications;
    }

    private function createNaceClassificationFromCsvData(
        array $naceClassificationLine,
    ): NaceClassification {
        $naceClassification = new NaceClassification();
        $naceClassification
            ->setName($naceClassificationLine['Titel'])
            ->setCode($naceClassificationLine['Schlüssel WZ 2008'])
            ->setLevel(NaceClassificationLevel::from((int)$naceClassificationLine['Ebene']));

        return $naceClassification;
    }

    private function setNaceClassificationParent(
        NaceClassification $naceClassification,
        NaceClassification $lastNaceClassification
    ): void {
        if ($naceClassification->getLevel() === $lastNaceClassification->getLevel()) {
            $naceClassification->setParent($lastNaceClassification->getParent());

            return;
        }

        if ($naceClassification->getLevel()->value === $lastNaceClassification->getLevel()->value + 1) {
            $naceClassification->setParent($lastNaceClassification);

            return;
        }

        if ($naceClassification->getLevel()->value < $lastNaceClassification->getLevel()->value + 1) {
            $parentNaceClassification = $lastNaceClassification->getParent();

            while($parentNaceClassification->getLevel()->value >= $naceClassification->getLevel()->value) {
                $parentNaceClassification = $parentNaceClassification->getParent();
            }

            $naceClassification->setParent($parentNaceClassification);

            return;
        }

        throw new \RuntimeException('Unexpected Classification Level');
    }

    private function updateNacsClassification(
        NaceClassification $existingNaceClassification,
        NaceClassification $naceClassification
    ): void {
        $existingNaceClassification
            ->setLevel($naceClassification->getLevel())
            ->setName($naceClassification->getName());

        if ($naceClassification->getParent() === null) {
            return;
        }

        $newParent = $this->classificationService->fetchNaceClassificationByCode(
            code: $naceClassification->getParent()?->getCode()
        );

        if ($newParent !== null) {
            $existingNaceClassification
                ->setParent($newParent);
        }
    }

    /**
     * @return IndustryClassification[]
     */
    public function importIndustryClassificationsFromCsvIntoDatabase(array $industryClassificationCsvLines): array
    {
        $lastIndustryClassification = null;
        $industryClassifications = [];
        $this->entityManager->beginTransaction();

        foreach ($industryClassificationCsvLines as $industryClassificationCsvLine) {
            $industryClassification = $this->createIndustryClassificationFromCsvData($industryClassificationCsvLine);
            $industryClassifications[] = $industryClassification;
            $this->setIndustryClassificationParent(
                industryClassification: $industryClassification,
                lastIndustryClassification: $lastIndustryClassification
            );

            $existingIndustryClassification = $this->classificationService->fetchIndustryClassification(
                levelOne: $industryClassification->getLevelOne(),
                levelTwo: $industryClassification->getLevelTwo(),
                levelThree: $industryClassification->getLevelThree()
            );

            if ($existingIndustryClassification === null) {
                $this->entityManager->persist($industryClassification);
            } else {
                $this->updateIndustryClassification(
                    existingIndustryClassification: $existingIndustryClassification,
                    industryClassification: $industryClassification
                );
            }

            $lastIndustryClassification = $industryClassification;
            $this->entityManager->flush();
        }

        $this->entityManager->commit();

        return $industryClassifications;
    }

    private function createIndustryClassificationFromCsvData(
        array $industryClassificationCsvLine
    ): IndustryClassification {
        $industryClassification = new IndustryClassification();
        $industryClassification
            ->setExampleSubSector($industryClassificationCsvLine['Beispiele-Teilbranchen']);

        $industryClassification
            ->setLevelOne((int)$industryClassificationCsvLine['Nutzungsart-ID'])
            ->setName($industryClassificationCsvLine['Nutzungsart']);

        if ($industryClassificationCsvLine['Branchenbereich-ID'] !== null && $industryClassificationCsvLine['Branchenbereich-ID'] !== '') {
            $industryClassification
                ->setLevelTwo((int)$industryClassificationCsvLine['Branchenbereich-ID'])
                ->setName($industryClassificationCsvLine['Branchenbereich']);
        }

        if ($industryClassificationCsvLine['Branchendetail-ID'] !== null && $industryClassificationCsvLine['Branchendetail-ID'] !== '') {
            $industryClassification
                ->setLevelThree((int)$industryClassificationCsvLine['Branchendetail-ID'])
                ->setName($industryClassificationCsvLine['Branchendetail']);
        }

        return $industryClassification;
    }

    private function updateIndustryClassification(
        IndustryClassification $existingIndustryClassification,
        IndustryClassification $industryClassification
    ): void {
        $existingIndustryClassification
            ->setName($industryClassification->getName());
        $industryClassificationParent = $industryClassification->getParent();

        if ($industryClassificationParent === null) {
            return;
        }

        $newParent = $this->classificationService->fetchIndustryClassification(
            levelOne: $industryClassificationParent->getLevelOne(),
            levelTwo: $industryClassificationParent->getLevelTwo(),
            levelThree: $industryClassificationParent->getLevelThree()
        );

        if ($newParent !== null) {
            $existingIndustryClassification
                ->setParent($newParent);
        }
    }

    private function setIndustryClassificationParent(
        IndustryClassification  $industryClassification,
        ?IndustryClassification $lastIndustryClassification
    ): void {
        if ($lastIndustryClassification === null) {
            return;
        }

        $industryClassificationLevel = $this->getIndustryClassificationLevel($industryClassification);
        $lastIndustryClassificationLevel = $this->getIndustryClassificationLevel($lastIndustryClassification);


        if ($industryClassificationLevel === 1) {
            return;
        }

        if ($lastIndustryClassificationLevel + 1 === $industryClassificationLevel) {
            $industryClassification->setParent($lastIndustryClassification);

            return;
        }

        if ($lastIndustryClassificationLevel === $industryClassificationLevel) {
            $industryClassification->setParent($lastIndustryClassification->getParent());

            return;
        }

        if ($lastIndustryClassificationLevel === $industryClassificationLevel + 1) {
            $industryClassification->setParent(
                $lastIndustryClassification->getParent()->getParent()
            );

            return;
        }

        throw new \RuntimeException('Uncovered case');
    }

    private function getIndustryClassificationLevel(IndustryClassification $industryClassification): int
    {
        if ($industryClassification->getLevelThree() !== null) {
            return 3;
        }

        if ($industryClassification->getLevelTwo() !== null) {
            return 2;
        }

        return 1;
    }

    /**
     * @return ClassificationExample[]
     */
    public function importClassificationExamplesFromCsvIntoDatabase(
        array $industryClassificationCsvLines
    ): array {
        $importedClassificationExamples = [];
        $this->entityManager->beginTransaction();

        foreach ($industryClassificationCsvLines as $industryClassificationCsvLine) {

            $brandName = $industryClassificationCsvLine['Beispiele-Marken'];
            $naceClassifications = $this->getNaceCodesFromCsvLine($industryClassificationCsvLine);
            $industryClassification = $this->getIndustryClassificationFromCsvLine($industryClassificationCsvLine);

            if (array_key_exists($brandName, $importedClassificationExamples) === true) {
                $classificationExample = $importedClassificationExamples[$brandName];
            } else {
                $classificationExample = $this->classificationService->fetchClassificationExampleByBrandName($brandName);
                $importedClassificationExamples[$brandName] = $classificationExample;
            }

            if ($classificationExample === null) {
                $classificationExample = $this->createClassificationWithBrandName($brandName);
                $importedClassificationExamples[$brandName] = $classificationExample;
                $this->entityManager->persist($classificationExample);
            }

            foreach ($naceClassifications as $naceClassification) {

                if ($classificationExample->getNaceClassifications()->contains($naceClassification) === false) {
                    $classificationExample->getNaceClassifications()->add($naceClassification);
                }
            }

            if ($classificationExample->getIndustryClassifications()->contains($industryClassification) === false) {
                $classificationExample->getIndustryClassifications()->add($industryClassification);
            }

            $this->entityManager->flush();
        }

        $this->entityManager->commit();

        return array_values($importedClassificationExamples);
    }

    /**
     * @return NaceClassification[]
     */
    private function getNaceCodesFromCsvLine(array $industryClassificationCsvLine): array
    {
        $naceCodeStrings = explode(
            separator: ',',
            string: $industryClassificationCsvLine['WZ-Codes']
        );

        foreach ($naceCodeStrings as $naceCodeString) {
            $arrayKey = array_search($naceCodeString, $naceCodeStrings);

            if ($arrayKey === false) {
                continue;
            }

            $naceCodeStrings[$arrayKey] = trim($naceCodeString);
        }

        return $this->classificationService->fetchNaceClassificationsByCodes($naceCodeStrings);
    }

    private function getIndustryClassificationFromCsvLine(array $industryClassificationCsvLine): ?IndustryClassification
    {
        $industryClassification = $this->createIndustryClassificationFromCsvData($industryClassificationCsvLine);

        return  $this->classificationService->fetchIndustryClassificationByLevels(
            levelOne: $industryClassification->getLevelOne(),
            levelTwo: $industryClassification->getLevelTwo(),
            levelThree: $industryClassification->getLevelThree()
        );
    }

    private function createClassificationWithBrandName(string $brandName): ClassificationExample
    {
        $classificationExample = new ClassificationExample();
        $classificationExample->setBrandName($brandName);

        return $classificationExample;
    }

    public function normalizeIndustryClassificationCsvLinesAfterBrandName(array $industryClassificationCsvLines): array
    {
        $normalizedCsvLines = [];

        foreach ($industryClassificationCsvLines as $industryClassificationCsvLine) {
            $brandValues = $industryClassificationCsvLine['Beispiele-Marken'];

            if ($brandValues === null || $brandValues === '') {
                continue;
            }

            $brands = $this->explodeAndCleanUpBrands($brandValues);

            $newNormalizedLinesForClassification = $this->createNormalizedCsvLinesForGivenCsvLineAndBrands(
                industryClassificationCsvLine: $industryClassificationCsvLine,
                brands: $brands
            );

            $normalizedCsvLines = array_merge(
                $normalizedCsvLines,
                $newNormalizedLinesForClassification
            );
        }

        return $normalizedCsvLines;
    }

    private function explodeAndCleanUpBrands(string $brandNames): array
    {
        $listOfBrands = [];

        $brands = explode(',', $brandNames);

        foreach ($brands as $brand) {
            $cleanName = trim($brand);

            if ($cleanName !== null && $cleanName !== '') {
                $listOfBrands[] = $cleanName;
            }
        }

        return $listOfBrands;
    }

    private function createNormalizedCsvLinesForGivenCsvLineAndBrands(
        array $industryClassificationCsvLine,
        array $brands
    ): array {
        $returnArray = [];

        foreach ($brands as $brand) {
            $line = $industryClassificationCsvLine;
            $line['Beispiele-Marken'] = $brand;

            $returnArray[] = $line;
        }

        return $returnArray;
    }

    public function importIndustryClassificationNaceClassificationLinks(array $industryClassificationCsvLines): array
    {
        $this->entityManager->beginTransaction();
        $updatedIndustryClassifications = [];

        foreach ($industryClassificationCsvLines as $industryClassificationCsvLine) {

            if ($industryClassificationCsvLine['WZ-Codes'] === null || $industryClassificationCsvLine['WZ-Codes'] === '') {
                continue;
            }

            $levelOne = (int)$industryClassificationCsvLine['Nutzungsart-ID'];
            $levelTwo = $industryClassificationCsvLine['Branchenbereich-ID'] !== ''
                ? (int)$industryClassificationCsvLine['Branchenbereich-ID']
                : null;
            $levelThree = $industryClassificationCsvLine['Branchendetail-ID'] !== ''
                ? (int)$industryClassificationCsvLine['Branchendetail-ID']
                : null;

            $industryClassification = $this->classificationService->fetchOneIndustryClassificationByLevels(
                levelOne: $levelOne,
                levelTwo: $levelTwo,
                levelThree: $levelThree
            );

            $brands = $this->explodeAndCleanUpBrands($industryClassificationCsvLine['WZ-Codes']);
            $naceClassificationCodes = $this->classificationService->fetchNaceClassificationsByCodes($brands);

            foreach ($naceClassificationCodes as $naceClassificationCode) {
                $industryClassification
                    ->getNaceClassifications()
                    ->add($naceClassificationCode);
            }

            $this->entityManager->flush();
            $updatedIndustryClassifications[] = $industryClassification;
        }

        $this->entityManager->commit();


        return $updatedIndustryClassifications;
    }
}
