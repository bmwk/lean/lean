<?php

declare(strict_types=1);

namespace App\Domain\Classification;

use App\Domain\Entity\Classification\ClassificationExample;
use App\Domain\Entity\Classification\IndustryClassification;
use App\Domain\Entity\Classification\NaceClassification;
use App\Domain\Entity\Classification\NaceClassificationLevel;
use App\Repository\Classification\ClassificationExampleRepository;
use App\Repository\Classification\IndustryClassificationRepository;
use App\Repository\Classification\NaceClassificationRepository;
use Symfony\Component\Form\FormInterface;

class ClassificationService
{
    public function __construct(
        private readonly NaceClassificationRepository $naceClassificationRepository,
        private readonly IndustryClassificationRepository $industryClassificationRepository,
        private readonly ClassificationExampleRepository $classificationExampleRepository
    ) {
    }

    public function fetchNaceClassificationByCode(string $code): ?NaceClassification
    {
        return $this->naceClassificationRepository->findOneBy(criteria: ['code' => $code]);
    }

    /**
     * @return NaceClassification[]
     */
    public function fetchNaceClassificationsWithLevel(?NaceClassificationLevel $level): array
    {
        if ($level === null) {
            return $this->naceClassificationRepository->findAll();
        }

        return $this->naceClassificationRepository->findBy(criteria: ['level' => $level]);
    }

    public function fetchNaceClassificaionById(int $naceClassificationId): ?NaceClassification
    {
        return $this->naceClassificationRepository->findOneBy(criteria: ['id' => $naceClassificationId]);
    }

    /**
     * @return NaceClassification[]
     */
    public function fetchNaceClassificationChildrens(?NaceClassification $parentNaceClassification): array
    {
        return $this->naceClassificationRepository->findBy(criteria: ['parent' => $parentNaceClassification]);
    }

    public function fetchIndustryClassification(int $levelOne, ?int $levelTwo, ?int $levelThree): ?IndustryClassification
    {
        return $this->industryClassificationRepository->findOneBy(criteria: [
            'levelOne'   => $levelOne,
            'levelTwo'   => $levelTwo,
            'levelThree' => $levelThree,
        ]);
    }

    /**
     * @return NaceClassification[]
     */
    public function fetchNaceClassificationsByCodes(array $naceCodes): array
    {
        return $this->naceClassificationRepository->findBy(criteria: ['code' => $naceCodes]);
    }

    public function fetchIndustryClassificationByLevels(int $levelOne, ?int $levelTwo, ?int $levelThree): ?IndustryClassification
    {
        return $this->fetchIndustryClassification(
            levelOne: $levelOne,
            levelTwo: $levelTwo,
            levelThree: $levelThree
        );
    }

    public function fetchClassificationExampleByBrandName(string $brandName): ?ClassificationExample
    {
        return $this->classificationExampleRepository->findOneBy(criteria: ['brandName' => $brandName]);
    }

    /**
     * @return IndustryClassification[]
     */
    public function fetchAllIndustryClassifications(): array
    {
        return $this->industryClassificationRepository->findAll();
    }

    public function fetchIndustryClassificationById(int $id): ?IndustryClassification
    {
        return $this->industryClassificationRepository->findOneBy(criteria: ['id' => $id]);
    }

    /**
     * @return IndustryClassification[]
     */
    public function fetchIndustryClassificationChildren(IndustryClassification $parentIndustryClassification): array
    {
        return $this->industryClassificationRepository->findBy(criteria: ['parent' => $parentIndustryClassification]);
    }

    public function getTopMostParentFromIndustryClassification(IndustryClassification $industryClassification): IndustryClassification
    {
        $parent = $industryClassification->getParent();

        if ($parent === null) {
            return $industryClassification;
        }

        return $this->getTopMostParentFromIndustryClassification(industryClassification: $parent);
    }

    public function fetchNaceClassificationById(int $naceClassificationId): ?NaceClassification
    {
        return $this->naceClassificationRepository->findOneBy(criteria: ['id' => $naceClassificationId]);
    }

    /**
     * @return IndustryClassification[]
     */
    public function fetchTopLevelIndustryClassifications(): array
    {
        return $this->industryClassificationRepository->findBy(criteria: [
            'levelTwo'   => null,
            'levelThree' => null,
        ]);
    }

    /**
     * @return IndustryClassification[]
     */
    public function fetchTopLevelIndustryClassificationsWithLevelTwo(): array
    {
        $topLevelIndustryClassifications = $this->fetchTopLevelIndustryClassifications();

        $industryClassificationList = [];

        foreach ($topLevelIndustryClassifications as $topLevelIndustryClassification) {
            $industryClassificationList[$topLevelIndustryClassification->getId()] = $this->industryClassificationRepository->findSecondLevelIndustryClassificationsByParent($topLevelIndustryClassification);
        }

        return $industryClassificationList;
    }

    /**
     * @return IndustryClassification[]
     */
    public function fetchSecondLevelIndustryClassifications(): array
    {
        return $this->industryClassificationRepository->findSecondLevelIndustryClassifications();
    }

    /**
     * @return IndustryClassification[]
     */
    public function fetchThirdLevelIndustryClassifications(): array
    {
        return $this->industryClassificationRepository->findThirdLevelIndustryClassifications();
    }

    public function fetchOneIndustryClassificationByLevels(int $levelOne, ?int $levelTwo, ?int $levelThree): ?IndustryClassification
    {
        return $this->industryClassificationRepository->findOneBy(criteria: [
            'levelOne'   => $levelOne,
            'levelTwo'   => $levelTwo,
            'levelThree' => $levelThree,
        ]);
    }

    /**
     * @return IndustryClassification[]
     */
    public function fetchOneIndustryClassificationByLevelsOneTwoAndThree(int $levelOne, ?int $levelTwo, ?int $levelThree): array
    {
        return $this->industryClassificationRepository->findBy(criteria: [
            'levelOne'   => $levelOne,
            'levelTwo'   => $levelTwo,
            'levelThree' => $levelThree,
        ]);
    }

    /**
     * @return IndustryClassification[]
     */
    public function fetchOneIndustryClassificationByLevelsOneAndTwo(int $levelOne, ?int $levelTwo): array
    {
        return $this->industryClassificationRepository->findBy(criteria: [
            'levelOne' => $levelOne,
            'levelTwo' => $levelTwo,
        ]);
    }

    /**
     * @return IndustryClassification[]
     */
    public function fetchOneIndustryClassificationByLevelOne(int $levelOne): array
    {
        return $this->industryClassificationRepository->findBy(criteria: ['levelOne' => $levelOne]);
    }

    /**
     * @return IndustryClassification[]
     */
    public function fetchIndustryClassificationWhereSearchTermInName(string $searchTerm): array
    {
        $queryBuilder = $this->industryClassificationRepository->createQueryBuilder('industryClassification');

        return $queryBuilder
            ->where(predicates: 'LOWER(industryClassification.name) LIKE :name')
            ->setParameter(key: 'name', value: '%' . strtolower($searchTerm) . '%')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return NaceClassification[]
     */
    public function fetchNaceClassificationWhereSearchTermInCode(string $code): array
    {
        $queryBuilder = $this->naceClassificationRepository->createQueryBuilder('naceClassification');

        return $queryBuilder
            ->where(predicates: 'LOWER(naceClassification.code) LIKE :code')
            ->setParameter(key: 'code', value: strtolower($code) . '%')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return NaceClassification[]
     */
    public function fetchNaceClassificationWhereSearchTermInName(string $name): array
    {
        $queryBuilder = $this->naceClassificationRepository->createQueryBuilder('naceClassification');

        return $queryBuilder
            ->where(predicates: 'LOWER(naceClassification.name) LIKE :name')
            ->setParameter(key: 'name', value: '%' . strtolower($name) . '%')
            ->getQuery()
            ->getResult();
    }

    public function fetchIndustryClassificationByIndustryNaceClassificationForm(FormInterface $industryNaceClassificationForm): ?IndustryClassification
    {
        $industryClassificationId = $industryNaceClassificationForm->get('industryClassificationId')->getData();

        if ($industryClassificationId !== null && $industryClassificationId !== '') {
            return $this->fetchIndustryClassificationById((int)$industryClassificationId);
        }

        return null;
    }

    public function fetchNaceClassificationByIndustryNaceClassificationForm(FormInterface $industryNaceClassificationForm): ?NaceClassification
    {
        $naceClassificationId = $industryNaceClassificationForm->get('naceClassificationId')->getData();

        if ($naceClassificationId !== null && $naceClassificationId !== '') {
            return $this->fetchNaceClassificationById((int)$naceClassificationId);
        }

        return null;
    }
}
