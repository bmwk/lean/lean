<?php

declare(strict_types=1);

namespace App\Domain\LookingForPropertyRequest;

use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\AccountUser\AccountUserRole;
use App\Domain\Entity\LookingForPropertyRequest\LookingForPropertyRequestExposeForwarding;
use Symfony\Bundle\SecurityBundle\Security;

class LookingForPropertyRequestExposeForwardingSecurityService
{
    public function __construct(
        private readonly Security $security
    ) {
    }

    public function canViewLookingForPropertyRequestExposeForwarding(
        LookingForPropertyRequestExposeForwarding $lookingForPropertyRequestExposeForwarding,
        AccountUSer $accountUser
    ): bool {
        $account = $accountUser->getAccount();

        $lookingForPropertyRequest = $lookingForPropertyRequestExposeForwarding->getLookingForPropertyRequest();

        if (
            $this->security->isGranted(attributes: AccountUserRole::ROLE_USER->value) === true
            && (
                $lookingForPropertyRequest->getAccount() === $account
                || $account->getSubAccounts()->contains(element: $lookingForPropertyRequest->getAccount()) === true
            )
        ) {
            return true;
        }

        if (
            $this->security->isGranted(attributes: AccountUserRole::ROLE_PROPERTY_SEEKER->value) === true
            && $accountUser->getPerson() !== null
            && $lookingForPropertyRequest->getAccount() === $account
            && $lookingForPropertyRequest->getPerson() === $accountUser->getPerson()
        ) {
            return true;
        }

        return false;
    }

    public function canEditLookingForPropertyRequestExposeForwarding(
        LookingForPropertyRequestExposeForwarding $lookingForPropertyRequestExposeForwarding,
        AccountUser $accountUser
    ): bool {
        $account = $accountUser->getAccount();

        $lookingForPropertyRequest = $lookingForPropertyRequestExposeForwarding->getLookingForPropertyRequest();

        if (
            $this->security->isGranted(attributes: AccountUserRole::ROLE_USER->value) === true
            && (
                $lookingForPropertyRequest->getAccount() === $account
                || $account->getSubAccounts()->contains(element: $lookingForPropertyRequest->getAccount()) === true
            )
        ) {
            return true;
        }

        if (
            $this->security->isGranted(attributes: AccountUserRole::ROLE_PROPERTY_SEEKER->value) === true
            && $accountUser->getPerson() !== null
            && $lookingForPropertyRequest->getAccount() === $account
            && $lookingForPropertyRequest->getPerson() === $accountUser->getPerson()
        ) {
            return true;
        }

        return false;
    }

    public function canDeleteLookingForPropertyRequestExposeForwarding(
        LookingForPropertyRequestExposeForwarding $lookingForPropertyRequestExposeForwarding,
        AccountUser $accountUser
    ): bool {
        return $this->canEditLookingForPropertyRequestExposeForwarding(
            lookingForPropertyRequestExposeForwarding: $lookingForPropertyRequestExposeForwarding,
            accountUser: $accountUser
        );
    }

    public function canRejectLookingForPropertyRequestExposeForwarding(
        LookingForPropertyRequestExposeForwarding $lookingForPropertyRequestExposeForwarding,
        AccountUser $accountUser
    ): bool {
        return $this->canEditLookingForPropertyRequestExposeForwarding(
            lookingForPropertyRequestExposeForwarding: $lookingForPropertyRequestExposeForwarding,
            accountUser: $accountUser
        );
    }

    public function canSetForwardingResponseLookingForPropertyRequestExposeForwarding(
        LookingForPropertyRequestExposeForwarding $lookingForPropertyRequestExposeForwarding,
        AccountUser $accountUser
    ): bool {
        return $this->canEditLookingForPropertyRequestExposeForwarding(
            lookingForPropertyRequestExposeForwarding: $lookingForPropertyRequestExposeForwarding,
            accountUser: $accountUser
        );
    }
}
