<?php

declare(strict_types=1);

namespace App\Domain\LookingForPropertyRequest;

use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\AccountUser\AccountUserRole;
use App\Domain\Entity\LookingForPropertyRequest\LookingForPropertyRequest;
use Symfony\Bundle\SecurityBundle\Security;

class LookingForPropertyRequestSecurityService
{
    public function __construct(
        private readonly Security $security
    ) {
    }

    public function canCreateLookingForPropertyRequest(): bool
    {
        if (
            $this->security->isGranted(attributes: AccountUserRole::ROLE_USER->value) === true
            || $this->security->isGranted(attributes: AccountUserRole::ROLE_LOOKING_FOR_PROPERTY_REQUEST_FEEDER->value) === true
            || $this->security->isGranted(attributes: AccountUserRole::ROLE_PROPERTY_SEEKER->value) === true
        ) {
            return true;
        }

        return false;
    }

    public function canViewLookingForPropertyRequest(LookingForPropertyRequest $lookingForPropertyRequest, AccountUser $accountUser): bool
    {
        $account = $accountUser->getAccount();

        if (
            $account->getParentAccount() === null
            && (
                $lookingForPropertyRequest->getAccount() === $account
                || $account->getSubAccounts()->contains(element: $lookingForPropertyRequest->getAccount()) === true
            )
            && (
                $this->security->isGranted(attributes: AccountUserRole::ROLE_USER->value) === true
                || $this->security->isGranted(attributes: AccountUserRole::ROLE_LOOKING_FOR_PROPERTY_REQUEST_FEEDER->value) === true
                || $this->security->isGranted(attributes: AccountUserRole::ROLE_VIEWER->value) === true
                || $this->security->isGranted(attributes: AccountUserRole::ROLE_RESTRICTED_VIEWER->value) === true
            )
        ) {
            return true;
        }

        if (
            $this->security->isGranted(attributes: AccountUserRole::ROLE_PROPERTY_SEEKER->value) === true
            && $accountUser->getPerson() !== null
            && $lookingForPropertyRequest->getPerson() === $accountUser->getPerson()
            && $lookingForPropertyRequest->getAccount() === $account
        ) {
            return true;
        }

        return false;
    }

    public function canEditLookingForPropertyRequest(LookingForPropertyRequest $lookingForPropertyRequest, AccountUser $accountUser): bool
    {
        $account = $accountUser->getAccount();

        if (
            $account->getParentAccount() === null
            && (
                $lookingForPropertyRequest->getAccount() === $account
                || $account->getSubAccounts()->contains(element: $lookingForPropertyRequest->getAccount()) === true
            )
        ) {
            if ($this->security->isGranted(attributes: AccountUserRole::ROLE_USER->value) === true) {
                return true;
            }

            if (
                $this->security->isGranted(attributes: AccountUserRole::ROLE_LOOKING_FOR_PROPERTY_REQUEST_FEEDER->value) === true
                && (
                    $lookingForPropertyRequest->getAccount() === $account
                    || $account->getSubAccounts()->contains(element: $lookingForPropertyRequest->getAccount()) === true
                )
                && (
                    $lookingForPropertyRequest->getCreatedByAccountUser() === $accountUser
                    || $lookingForPropertyRequest->getAssignedToAccountUser() === $accountUser
                )
            ) {
                return true;
            }
        }

        if (
            $this->security->isGranted(attributes: AccountUserRole::ROLE_PROPERTY_SEEKER->value) === true
            && $accountUser->getPerson() !== null
            && $lookingForPropertyRequest->getPerson() === $accountUser->getPerson()
            && $lookingForPropertyRequest->getAccount() === $account
        ) {
            return true;
        }

        return false;
    }

    public function canDeleteLookingForPropertyRequest(LookingForPropertyRequest $lookingForPropertyRequest, AccountUser $accountUser): bool
    {
        $account = $accountUser->getAccount();

        if (
            $account->getParentAccount() === null
            && (
                $lookingForPropertyRequest->getAccount() === $account
                || $account->getSubAccounts()->contains(element: $lookingForPropertyRequest->getAccount()) === true
            )
        ) {
            if ($this->security->isGranted(attributes: AccountUserRole::ROLE_USER->value) === true) {
                return true;
            }

            if (
                $this->security->isGranted(attributes: AccountUserRole::ROLE_LOOKING_FOR_PROPERTY_REQUEST_FEEDER->value) === true
                && (
                    $lookingForPropertyRequest->getAccount() === $account
                    || $account->getSubAccounts()->contains(element: $lookingForPropertyRequest->getAccount()) === true
                )
                && $lookingForPropertyRequest->getCreatedByAccountUser() === $accountUser
            ) {
                return true;
            }
        }

        if (
            $this->security->isGranted(attributes: AccountUserRole::ROLE_PROPERTY_SEEKER->value) === true
            && $accountUser->getPerson() !== null
            && $lookingForPropertyRequest->getPerson() === $accountUser->getPerson()
            && $lookingForPropertyRequest->getAccount() === $account
        ) {
            return true;
        }

        return false;
    }

    public function canAssignToAccountUserLookingForPropertyRequest(LookingForPropertyRequest $lookingForPropertyRequest, AccountUser $accountUser): bool
    {
        $account = $accountUser->getAccount();

        if (
            $account->getParentAccount() === null
            && $this->security->isGranted(attributes: AccountUserRole::ROLE_USER->value) === true
            && (
                $lookingForPropertyRequest->getAccount() === $account
                || $account->getSubAccounts()->contains(element: $lookingForPropertyRequest->getAccount()) === true
            )
        ) {
            return true;
        }

        if (
            $account->getParentAccount() === null
            && $this->security->isGranted(attributes: AccountUserRole::ROLE_LOOKING_FOR_PROPERTY_REQUEST_FEEDER->value) === true
            && (
                $lookingForPropertyRequest->getAccount() === $account
                || $account->getSubAccounts()->contains(element: $lookingForPropertyRequest->getAccount()) === true
            )
            && $lookingForPropertyRequest->getCreatedByAccountUser() === $accountUser
        ) {
            return true;
        }

        return false;
    }
}
