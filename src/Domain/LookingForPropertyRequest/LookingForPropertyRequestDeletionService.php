<?php

declare(strict_types=1);

namespace App\Domain\LookingForPropertyRequest;

use App\Domain\Entity\Account;
use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\DeletionType;
use App\Domain\Entity\LookingForPropertyRequest\LookingForPropertyRequest;
use App\Domain\Entity\LookingForPropertyRequest\LookingForPropertyRequestExposeForwarding;
use App\Domain\Entity\LookingForPropertyRequest\LookingForPropertyRequestMatchingResponse;
use App\Domain\LookingForPropertyRequest\Matching\MatchingDeletionService;
use App\Domain\LookingForPropertyRequestReport\LookingForPropertyRequestReportDeletionService;
use App\Domain\Notification\NotificationDeletionService;
use App\Repository\LookingForPropertyRequest\LookingForPropertyRequestRepository;
use Doctrine\ORM\EntityManagerInterface;

class LookingForPropertyRequestDeletionService
{
    public function __construct(
        private readonly MatchingDeletionService $matchingDeletionService,
        private readonly LookingForPropertyRequestReportDeletionService $lookingForPropertyRequestReportDeletionService,
        private readonly PriceRequirementDeletionService $priceRequirementDeletionService,
        private readonly PropertyRequirementDeletionService $propertyRequirementDeletionService,
        private readonly NotificationDeletionService $notificationDeletionService,
        private readonly LookingForPropertyRequestRepository $lookingForPropertyRequestRepository,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function deleteLookingForPropertyRequest(
        LookingForPropertyRequest $lookingForPropertyRequest,
        DeletionType $deletionType = DeletionType::SOFT,
        ?AccountUser $deletedByAccountUser = null
    ): void {
        switch ($deletionType) {
            case DeletionType::SOFT:
                $this->softDeleteLookingForPropertyRequest(lookingForPropertyRequest: $lookingForPropertyRequest, deletedByAccountUser: $deletedByAccountUser);
                break;
            case DeletionType::HARD:
                $this->hardDeleteLookingForPropertyRequest(lookingForPropertyRequest: $lookingForPropertyRequest);
                break;
            default:
                throw new \RuntimeException(message: 'Unsupported DeletionType');
        }
    }

    public function deleteLookingForPropertyRequestsByAccount(
        Account $account,
        DeletionType $deletionType = DeletionType::SOFT,
        ?AccountUser $deletedByAccountUser = null
    ): void {
        switch ($deletionType) {
            case DeletionType::SOFT:
                $this->softDeleteLookingForPropertyRequestsByAccount(account: $account, deletedByAccountUser: $deletedByAccountUser);
                break;
            case DeletionType::HARD:
                $this->hardDeleteLookingForPropertyRequestsByAccount(account: $account);
                break;
            default:
                throw new \RuntimeException(message: 'Unsupported DeletionType');
        }
    }

    public function deleteLookingForPropertyRequestExposeForwarding(LookingForPropertyRequestExposeForwarding $lookingForPropertyRequestExposeForwarding): void
    {
        $this->hardDeleteLookingForPropertyRequestExposeForwarding(lookingForPropertyRequestExposeForwarding: $lookingForPropertyRequestExposeForwarding);
    }

    public function deleteLookingForPropertyRequestMatchingResponse(LookingForPropertyRequestMatchingResponse $lookingForPropertyRequestMatchingResponse): void
    {
        $this->hardDeleteLookingForPropertyRequestMatchingResponse(lookingForPropertyRequestMatchingResponse: $lookingForPropertyRequestMatchingResponse);
    }

    private function softDeleteLookingForPropertyRequest(LookingForPropertyRequest $lookingForPropertyRequest, AccountUser $deletedByAccountUser): void
    {
        $lookingForPropertyRequest
            ->setDeleted(deleted: true)
            ->setDeletedAt(deletedAt: new \DateTimeImmutable())
            ->setDeletedByAccountUser(deletedByAccountUser: $deletedByAccountUser);

        $this->entityManager->flush();
    }

    private function hardDeleteLookingForPropertyRequest(LookingForPropertyRequest $lookingForPropertyRequest): void
    {
        foreach ($lookingForPropertyRequest->getMatchingResults() as $matchingResult) {
            $this->matchingDeletionService->deleteMatchingResult(matchingResult: $matchingResult);
        }

        foreach ($lookingForPropertyRequest->getLookingForPropertyRequestExposeForwardings() as $lookingForPropertyRequestExposeForwarding) {
            $this->hardDeleteLookingForPropertyRequestExposeForwarding(lookingForPropertyRequestExposeForwarding: $lookingForPropertyRequestExposeForwarding);
        }

        foreach ($lookingForPropertyRequest->getLookingForPropertyRequestMatchingResponses() as $lookingForPropertyRequestMatchingResponse) {
            $this->hardDeleteLookingForPropertyRequestMatchingResponse(lookingForPropertyRequestMatchingResponse: $lookingForPropertyRequestMatchingResponse);
        }

        $this->notificationDeletionService->deleteNotificationsByLookingForPropertyRequest(lookingForPropertyRequest: $lookingForPropertyRequest);

        if ($lookingForPropertyRequest->getLookingForPropertyRequestReport() !== null) {
            $this->lookingForPropertyRequestReportDeletionService->deleteLookingForPropertyRequestReport(
                lookingForPropertyRequestReport: $lookingForPropertyRequest->getLookingForPropertyRequestReport(),
                deletionType: DeletionType::HARD
            );
        }

        $this->entityManager->remove(entity: $lookingForPropertyRequest);
        $this->entityManager->flush();

        $this->propertyRequirementDeletionService->deletePropertyRequirement(propertyRequirement: $lookingForPropertyRequest->getPropertyRequirement());
        $this->priceRequirementDeletionService->deletePriceRequirement(priceRequirement: $lookingForPropertyRequest->getPriceRequirement());
    }

    private function softDeleteLookingForPropertyRequestsByAccount(Account $account, AccountUser $deletedByAccountUser): void
    {
        $lookingForPropertyRequests = $this->lookingForPropertyRequestRepository->findBy(criteria: ['account' => $account]);

        foreach ($lookingForPropertyRequests as $lookingForPropertyRequest) {
            $this->softDeleteLookingForPropertyRequest(lookingForPropertyRequest: $lookingForPropertyRequest, deletedByAccountUser: $deletedByAccountUser);
        }
    }

    private function hardDeleteLookingForPropertyRequestsByAccount(Account $account): void
    {
        $lookingForPropertyRequests = $this->lookingForPropertyRequestRepository->findBy(criteria: ['account' => $account]);

        foreach ($lookingForPropertyRequests as $lookingForPropertyRequest) {
            $this->hardDeleteLookingForPropertyRequest(lookingForPropertyRequest: $lookingForPropertyRequest);
        }
    }

    private function hardDeleteLookingForPropertyRequestExposeForwarding(LookingForPropertyRequestExposeForwarding $lookingForPropertyRequestExposeForwarding): void
    {
        $this->entityManager->remove(entity: $lookingForPropertyRequestExposeForwarding);
        $this->entityManager->flush();
    }

    private function hardDeleteLookingForPropertyRequestMatchingResponse(LookingForPropertyRequestMatchingResponse $lookingForPropertyRequestMatchingResponse): void
    {
        $this->entityManager->remove(entity: $lookingForPropertyRequestMatchingResponse);
        $this->entityManager->flush();
    }
}
