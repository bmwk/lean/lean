<?php

declare(strict_types=1);

namespace App\Domain\LookingForPropertyRequest\Matching;

use App\Domain\Entity\Account;
use App\Domain\Entity\LookingForPropertyRequest\MatchingTask;
use App\Repository\LookingForPropertyRequest\MatchingTaskRepository;
use Doctrine\ORM\EntityManagerInterface;

class MatchingTaskDeletionService
{
    public function __construct(
        private readonly MatchingTaskRepository $matchingTaskRepository,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function deleteMatchingTask(MatchingTask $matchingTask): void
    {
        $this->hardDeleteMatchingTask(matchingTask: $matchingTask);
    }

    public function deleteMatchingTasksByAccount(Account $account): void
    {
        $this->hardDeleteMatchingTasksByAccount(account: $account);
    }

    private function hardDeleteMatchingTask(MatchingTask $matchingTask): void
    {
        $this->entityManager->remove($matchingTask);
        $this->entityManager->flush();
    }

    private function hardDeleteMatchingTasksByAccount(Account $account): void
    {
        $matchingTasks = $this->matchingTaskRepository->findBy(criteria: ['account' => $account]);

        foreach ($matchingTasks as $matchingTask) {
            $this->hardDeleteMatchingTask(matchingTask: $matchingTask);
        }
    }
}
