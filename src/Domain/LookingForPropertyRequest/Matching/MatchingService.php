<?php

declare(strict_types=1);

namespace App\Domain\LookingForPropertyRequest\Matching;

use App\Domain\Classification\ClassificationService;
use App\Domain\Entity\Account;
use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\LookingForPropertyRequest\MatchingResult;
use App\Domain\Entity\LookingForPropertyRequest\MatchingTask;
use App\Domain\Entity\Property\BuildingUnit;
use App\Domain\Entity\TaskStatus;
use App\Domain\LookingForPropertyRequest\LookingForPropertyRequestService;
use App\Domain\LookingForPropertyRequest\Matching\Exception\MatchingTaskNotFoundException;
use App\Domain\Property\BuildingUnitService;
use App\Repository\LookingForPropertyRequest\MatchingResultRepository;
use App\Repository\LookingForPropertyRequest\MatchingTaskRepository;
use Doctrine\ORM\EntityManagerInterface;

class MatchingService
{
    private readonly PointMatchingStrategy $pointMatchingStrategy;

    public function __construct(
        private readonly MatchingResultRepository $matchingResultRepository,
        private readonly MatchingTaskRepository $matchingTaskRepository,
        private readonly MatchingDeletionService $matchingDeletionService,
        private readonly LookingForPropertyRequestService $lookingForPropertyRequestService,
        private readonly BuildingUnitService $buildingUnitService,
        private readonly ClassificationService $classificationService,
        private readonly EntityManagerInterface $entityManager
    ) {
        $this->pointMatchingStrategy = new PointMatchingStrategy(
            buildingUnitService: $this->buildingUnitService,
            classificationService: $this->classificationService
        );
    }

    public function fetchMatchingResultById(Account $account, int $id): ?MatchingResult
    {
        return $this->matchingResultRepository->findOneBy(criteria: [
            'account' => $account,
            'id'      => $id,
        ]);
    }

    public function countMatchingTasksByAccountAndTaskStatus(Account $account, TaskStatus $taskStatus): int
    {
        return $this->matchingTaskRepository->count(criteria: ['account' => $account, 'taskStatus' => $taskStatus]);
    }

    public function countMatchingResultsGroupedByBuildingUnitAndEliminated(Account $account): array
    {
        return $this->matchingResultRepository->countGroupedByBuildingUnitAndEliminated(account: $account);
    }

    /**
     * @return MatchingResult[]
     */
    public function executeMatching(BuildingUnit $buildingUnit, Account $account): array
    {
        $lookingForPropertyRequests = $this->lookingForPropertyRequestService->fetchLookingForPropertyRequestsByAccount(
            account: $account,
            withFromSubAccounts: false,
            archived: false,
            active: true
        );

        $this->deleteAllMatchingResultsForBuildingUnit(buildingUnit: $buildingUnit);

        $matchingResults = $this->pointMatchingStrategy->doMatching(requests: $lookingForPropertyRequests, buildingUnit: $buildingUnit);

        foreach ($matchingResults as $matchingResult) {
            $matchingResult->setAccount($account);
        }

        $this->saveMatchingResults(matchingResults: $matchingResults);

        return $matchingResults;
    }

    /**
     * @param MatchingResult[] $matchingResults
     */
    public function saveMatchingResults(array $matchingResults): void
    {
        foreach ($matchingResults as $matchingResult) {
            $this->entityManager->persist($matchingResult->getLookingForPropertyRequest());

            $this->entityManager->persist($matchingResult->getLookingForPropertyRequest()->getPropertyRequirement());

            $eliminationCriteria = $matchingResult->getEliminationCriteria();

            $this->entityManager->persist($eliminationCriteria);
            $this->entityManager->persist($eliminationCriteria->getInPlace());
            $this->entityManager->persist($eliminationCriteria->getInQuarter());
            $this->entityManager->persist($eliminationCriteria->getCommissionable());
            $this->entityManager->persist($eliminationCriteria->getPropertyOfferTypes());
            $this->entityManager->persist($eliminationCriteria->getMaximumPrice());
            $this->entityManager->persist($eliminationCriteria->getMinimumPrice());
            $this->entityManager->persist($eliminationCriteria->getRoofing());
            $this->entityManager->persist($eliminationCriteria->getOutdoorSpace());
            $this->entityManager->persist($eliminationCriteria->getShowroom());
            $this->entityManager->persist($eliminationCriteria->getShopWindow());
            $this->entityManager->persist($eliminationCriteria->getGroundLevelSalesArea());
            $this->entityManager->persist($eliminationCriteria->getBarrierFreeAccess());
            $this->entityManager->persist($eliminationCriteria->getMinimumSpace());
            $this->entityManager->persist($eliminationCriteria->getMaximumSpace());
            $this->entityManager->persist($eliminationCriteria->getIndustryClassification());

            $scoreCriteria = $matchingResult->getScoreCriteria();

            $this->entityManager->persist($scoreCriteria);
            $this->entityManager->persist($scoreCriteria->getSpace());
            $this->entityManager->persist($scoreCriteria->getSpace()->getMinScoreCriterion());
            $this->entityManager->persist($scoreCriteria->getSpace()->getMaxScoreCriterion());
            $this->entityManager->persist($scoreCriteria->getRoomCount());
            $this->entityManager->persist($scoreCriteria->getNumberOfParkingLots());
            $this->entityManager->persist($scoreCriteria->getAvailableFrom());
            $this->entityManager->persist($scoreCriteria->getSubsidiarySpace());
            $this->entityManager->persist($scoreCriteria->getSubsidiarySpace()->getMinScoreCriterion());
            $this->entityManager->persist($scoreCriteria->getSubsidiarySpace()->getMaxScoreCriterion());
            $this->entityManager->persist($scoreCriteria->getRetailSpace());
            $this->entityManager->persist($scoreCriteria->getRetailSpace()->getMinScoreCriterion());
            $this->entityManager->persist($scoreCriteria->getRetailSpace()->getMaxScoreCriterion());
            $this->entityManager->persist($scoreCriteria->getOutdoorSpace());
            $this->entityManager->persist($scoreCriteria->getOutdoorSpace()->getMinScoreCriterion());
            $this->entityManager->persist($scoreCriteria->getOutdoorSpace()->getMaxScoreCriterion());
            $this->entityManager->persist($scoreCriteria->getStorageSpace());
            $this->entityManager->persist($scoreCriteria->getStorageSpace()->getMinScoreCriterion());
            $this->entityManager->persist($scoreCriteria->getStorageSpace()->getMaxScoreCriterion());
            $this->entityManager->persist($scoreCriteria->getShopWindowLength());
            $this->entityManager->persist($scoreCriteria->getShopWindowLength()->getMinScoreCriterion());
            $this->entityManager->persist($scoreCriteria->getShopWindowLength()->getMaxScoreCriterion());
            $this->entityManager->persist($scoreCriteria->getShopWidth());
            $this->entityManager->persist($scoreCriteria->getShopWidth()->getMinScoreCriterion());
            $this->entityManager->persist($scoreCriteria->getShopWidth()->getMaxScoreCriterion());
            $this->entityManager->persist($scoreCriteria->getLocationCategory());
            $this->entityManager->persist($scoreCriteria->getMaximumPrice());
            $this->entityManager->persist($scoreCriteria->getMinimumPrice());

            $this->entityManager->persist($scoreCriteria->getIndustryDensity());

            $this->entityManager->persist($matchingResult);
        }

        $this->entityManager->flush();
    }

    public function createAndSaveMatchingTask(BuildingUnit $buildingUnit, Account $account, AccountUser $accountUser): MatchingTask
    {
        $matchingTask = new MatchingTask();

        $matchingTask
            ->setAccount($account)
            ->setBuildingUnit($buildingUnit)
            ->setCreatedByAccountUser($accountUser)
            ->setTaskStatus(TaskStatus::CREATED);

        $this->entityManager->persist(entity: $matchingTask);

        $this->entityManager->flush();

        return $matchingTask;
    }

    public function updateTaskStatusFromMatchingTask(MatchingTask $matchingTask, TaskStatus $taskStatus): void
    {
        $matchingTask->setTaskStatus($taskStatus);
        $this->entityManager->flush();
    }

    public function executeMatchingTaskById(int $matchingTaskId, bool $dryRun = false): void
    {
        $matchingTask = $this->matchingTaskRepository->find(id: $matchingTaskId);

        if ($matchingTask === null) {
            throw new MatchingTaskNotFoundException();
        }

        $this->executeMatchingTask(matchingTask: $matchingTask, dryRun: $dryRun);
    }

    public function executeMatchingTask(MatchingTask $matchingTask, bool $dryRun): void
    {
        if ($matchingTask->getTaskStatus() !== TaskStatus::CREATED) {
            throw new \RuntimeException(message: 'Task is already process or in progress');
        }

        if ($dryRun === false) {
            $this->updateTaskStatusFromMatchingTask(matchingTask: $matchingTask, taskStatus: TaskStatus::IN_PROGRESS);
        }

        $this->executeMatching(buildingUnit: $matchingTask->getBuildingUnit(), account: $matchingTask->getAccount());

        if ($dryRun === false) {
            $this->updateTaskStatusFromMatchingTask(matchingTask: $matchingTask, taskStatus: TaskStatus::DONE);
        }
    }

    public function executeMatchingTasks(bool $dryRun = false): void
    {
        $matchingTasks = $this->matchingTaskRepository->findBy(criteria: ['taskStatus' => TaskStatus::CREATED]);

        foreach ($matchingTasks as $matchingTask) {
            $this->executeMatchingTask(matchingTask: $matchingTask, dryRun: $dryRun);
        }
    }

    public function fetchMatchingTaskByBuildingUnit(BuildingUnit $buildingUnit): ?MatchingTask
    {
        return $this->matchingTaskRepository->findOneBy(criteria: ['buildingUnit' => $buildingUnit]);
    }

    public function deleteAllMatchingResultsForBuildingUnit(BuildingUnit $buildingUnit): void
    {
        foreach ($buildingUnit->getMatchingResults() as $matchingResult) {
            $this->matchingDeletionService->deleteMatchingResult(matchingResult: $matchingResult);
        }
    }

    public function isMatchingTaskRunning(): bool
    {
        if (empty($this->matchingTaskRepository->findBy(criteria: ['taskStatus' => TaskStatus::CREATED])) === false) {
            return true;
        }

        if (empty($this->matchingTaskRepository->findBy(criteria: ['taskStatus' => TaskStatus::IN_PROGRESS])) === false) {
            return true;
        }

        return false;
    }
}
