<?php

declare(strict_types=1);

namespace App\Domain\LookingForPropertyRequest\Matching;

use App\Domain\Entity\EliminationCriteria;
use App\Domain\Entity\EliminationCriterion;
use App\Domain\Entity\LookingForPropertyRequest\MatchingResult;
use App\Domain\Entity\LookingForPropertyRequest\ScoreCriteria;
use App\Domain\Entity\ScoreCriterionInterface;
use Doctrine\ORM\EntityManagerInterface;

class MatchingDeletionService
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function deleteMatchingResult(MatchingResult $matchingResult): void
    {
        $this->hardDeleteMatchingResult(matchingResult: $matchingResult);
    }

    private function hardDeleteMatchingResult(MatchingResult $matchingResult): void
    {
        $eliminationCriteria = $matchingResult->getEliminationCriteria();
        $scoreCriteria = $matchingResult->getScoreCriteria();

        $this->entityManager->remove(entity: $matchingResult);
        $this->entityManager->flush();

        $this->hardDeleteEliminationCriteria(eliminationCriteria: $eliminationCriteria);
        $this->hardScoreCriteria(scoreCriteria: $scoreCriteria);
    }

    private function hardDeleteEliminationCriteria(EliminationCriteria $eliminationCriteria): void
    {
        $eliminationCriterionToDelete = [
            $eliminationCriteria->getInPlace(),
            $eliminationCriteria->getInQuarter(),
            $eliminationCriteria->getCommissionable(),
            $eliminationCriteria->getPropertyOfferTypes(),
            $eliminationCriteria->getMaximumPrice(),
            $eliminationCriteria->getMinimumPrice(),
            $eliminationCriteria->getRoofing(),
            $eliminationCriteria->getOutdoorSpace(),
            $eliminationCriteria->getShowroom(),
            $eliminationCriteria->getShopWindow(),
            $eliminationCriteria->getGroundLevelSalesArea(),
            $eliminationCriteria->getBarrierFreeAccess(),
            $eliminationCriteria->getMinimumSpace(),
            $eliminationCriteria->getMaximumSpace(),
        ];

        if ($eliminationCriteria->getIndustryClassification() !== null) {
            $eliminationCriterionToDelete[] = $eliminationCriteria->getIndustryClassification();
        }

        $this->entityManager->remove(entity: $eliminationCriteria);
        $this->entityManager->flush();

        foreach ($eliminationCriterionToDelete as $eliminationCriterion) {
            $this->hardDeleteEliminationCriterion(eliminationCriterion: $eliminationCriterion);
        }
    }

    private function hardDeleteEliminationCriterion(EliminationCriterion $eliminationCriterion): void
    {
        $this->entityManager->remove(entity: $eliminationCriterion);
        $this->entityManager->flush();
    }

    private function hardScoreCriteria(ScoreCriteria $scoreCriteria): void
    {
        $scoreCriterionToDelete = [
            $scoreCriteria->getSpace(),
            $scoreCriteria->getRoomCount(),
            $scoreCriteria->getNumberOfParkingLots(),
            $scoreCriteria->getAvailableFrom(),
            $scoreCriteria->getSubsidiarySpace(),
            $scoreCriteria->getRetailSpace(),
            $scoreCriteria->getOutdoorSpace(),
            $scoreCriteria->getStorageSpace(),
            $scoreCriteria->getShopWindowLength(),
            $scoreCriteria->getShopWidth(),
            $scoreCriteria->getLocationCategory(),
            $scoreCriteria->getMaximumPrice(),
            $scoreCriteria->getMinimumPrice(),
            $scoreCriteria->getIndustryDensity(),
        ];

        $this->entityManager->remove(entity: $scoreCriteria);
        $this->entityManager->flush();

        foreach ($scoreCriterionToDelete as $scoreCriterion) {
            $this->hardDeleteScoreCriterion(scoreCriterion: $scoreCriterion);
        }
    }

    private function hardDeleteScoreCriterion(ScoreCriterionInterface $scoreCriterion): void
    {
        $this->entityManager->remove(entity: $scoreCriterion);
        $this->entityManager->flush();
    }
}
