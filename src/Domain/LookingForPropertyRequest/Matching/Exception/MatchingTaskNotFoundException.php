<?php

declare(strict_types=1);

namespace App\Domain\LookingForPropertyRequest\Matching\Exception;

class MatchingTaskNotFoundException extends \RuntimeException
{

}