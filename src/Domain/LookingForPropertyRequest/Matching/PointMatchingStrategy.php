<?php

declare(strict_types=1);

namespace App\Domain\LookingForPropertyRequest\Matching;

use App\Domain\Classification\ClassificationService;
use App\Domain\Entity\EliminationCriteria;
use App\Domain\Entity\EliminationCriterion;
use App\Domain\Entity\LocationCategory;
use App\Domain\Entity\LookingForPropertyRequest\LookingForPropertyRequest;
use App\Domain\Entity\LookingForPropertyRequest\MatchingResult;
use App\Domain\Entity\LookingForPropertyRequest\ScoreCriteria;
use App\Domain\Entity\MinMaxScoreCriterion;
use App\Domain\Entity\Property\BarrierFreeAccess;
use App\Domain\Entity\Property\BuildingUnit;
use App\Domain\Entity\Property\PropertyUser;
use App\Domain\Entity\PropertyOfferType;
use App\Domain\Entity\ScoreCriterion;
use App\Domain\Entity\ValueRequirement;
use App\Domain\Matching\MatchingStrategyInterface;
use App\Domain\Property\BuildingUnitService;

class PointMatchingStrategy implements MatchingStrategyInterface
{
    private const INDUSTRY_DIVERSITY_THRESHOLD = 5;
    private const PRICE_SCORE_POINTS = 0;

    public function __construct(
        private readonly BuildingUnitService $buildingUnitService,
        private readonly ClassificationService $classificationService
    ) {
    }

    /**
     * @inheritDoc
     */
    public function doMatching(array $requests, BuildingUnit $buildingUnit): array
    {
        $results = [];

        foreach ($requests as $request) {
            $matchingResult = new MatchingResult();
            $eliminationCriteria = $this->setupEliminationCriteria();
            $scoreCriteria = $this->setupScoreCriteria();

            $matchingResult
                ->setEliminationCriteria($eliminationCriteria)
                ->setScoreCriteria($scoreCriteria)
                ->setBuildingUnit($buildingUnit)
                ->setLookingForPropertyRequest($request);

            $results[] = $matchingResult;

            $this->checkEliminationCriteria(
                $eliminationCriteria,
                $request,
                $buildingUnit
            );

            $matchingResult->setEliminated($eliminationCriteria->isEliminated());

            if ($eliminationCriteria->isEliminated()) {
                $matchingResult->setScore(0);
                continue;
            }

            $this->checkScoreCriteria($scoreCriteria, $request, $buildingUnit);

            $matchingResult->setScore($scoreCriteria->calculatePoints());
            $matchingResult->setPossibleMaxScore($scoreCriteria->calculateMaxPoints());
        }

        usort($results, function (MatchingResult $itemA, MatchingResult $itemB) {
            return $itemA->getScore() <=> $itemB->getScore();
        });

        return array_reverse($results, false);
    }

    private function setupEliminationCriteria(): EliminationCriteria
    {
        $eliminationCriteria = new EliminationCriteria();

        $inPlace = new EliminationCriterion();
        $inPlace->setName('inPlace');

        $inQuarter = new EliminationCriterion();
        $inQuarter->setName('inQuarter');

        $commissionable = new EliminationCriterion();
        $commissionable->setName('commissionable');

        $propertyOfferType = new EliminationCriterion();
        $propertyOfferType->setName('propertyOfferType');

        $maximumPrice = new EliminationCriterion();
        $maximumPrice->setName('maximumPrice');

        $minimumPrice = new EliminationCriterion();
        $minimumPrice->setName('minimumPrice');

        $roofing = new EliminationCriterion();
        $roofing->setName('roofing');

        $outdoorSpace = new EliminationCriterion();
        $outdoorSpace->setName('outdoorSpace');

        $showroom = new EliminationCriterion();
        $showroom->setName('showRoom');

        $shopWindow = new EliminationCriterion();
        $shopWindow->setName('shopWindow');

        $groundLevelSalesArea = new EliminationCriterion();
        $groundLevelSalesArea->setName('groundLevelSalesArea');

        $barrierFreeAccess = new EliminationCriterion();
        $barrierFreeAccess->setName('barrierFreeAccess');

        $minimumSpace = new EliminationCriterion();
        $minimumSpace->setName('minimumSpace');

        $maximumSpace = new EliminationCriterion();
        $maximumSpace->setName('maximumSpace');

        $industryClassification = new EliminationCriterion();
        $industryClassification->setName('industryClassification');

        $eliminationCriteria
            ->setInPlace($inPlace)
            ->setInQuarter($inQuarter)
            ->setCommissionable($commissionable)
            ->setPropertyOfferTypes($propertyOfferType)
            ->setMaximumPrice($maximumPrice)
            ->setMinimumPrice($minimumPrice)
            ->setRoofing($roofing)
            ->setOutdoorSpace($outdoorSpace)
            ->setShowroom($showroom)
            ->setShopWindow($shopWindow)
            ->setGroundLevelSalesArea($groundLevelSalesArea)
            ->setBarrierFreeAccess($barrierFreeAccess)
            ->setMinimumSpace($minimumSpace)
            ->setMaximumSpace($maximumSpace)
            ->setIndustryClassification($industryClassification);

        return $eliminationCriteria;
    }

    private function setupScoreCriteria(): ScoreCriteria
    {
        $scoreCriteria = new ScoreCriteria();

        $minimumSpace = new ScoreCriterion();
        $minimumSpace
            ->setName('minimumSpace')
            ->setPoints(8);

        $maximumSpace = new ScoreCriterion();
        $maximumSpace
            ->setName('maximumSpace')
            ->setPoints(8);

        $minMaxSpace = new MinMaxScoreCriterion();
        $minMaxSpace
            ->setName('minMaxSpace')
            ->setMinScoreCriterion($minimumSpace)
            ->setMaxScoreCriterion($maximumSpace);

        $roomCount = new ScoreCriterion();
        $roomCount
            ->setName('roomCount')
            ->setPoints(6);

        $numberOfParkingLots = new ScoreCriterion();
        $numberOfParkingLots
            ->setName('numberOfParkingLots')
            ->setPoints(6);

        $availableFrom = new ScoreCriterion();
        $availableFrom
            ->setName('availableFrom')
            ->setPoints(6);

        $minSubsidiarySpace = new ScoreCriterion();
        $minSubsidiarySpace
            ->setName('minSubsidiarySpace')
            ->setPoints(3);

        $maxSubsidiarySpace = new ScoreCriterion();
        $maxSubsidiarySpace
            ->setName('maxSubsidiarySpace')
            ->setPoints(3);

        $subsidiarySpace = new MinMaxScoreCriterion();
        $subsidiarySpace
            ->setName('subsidarySpace')
            ->setMinScoreCriterion($minSubsidiarySpace)
            ->setMaxScoreCriterion($maxSubsidiarySpace);

        $minRetailSpace = new ScoreCriterion();
        $minRetailSpace
            ->setName('minRetailSpace')
            ->setPoints(8);

        $maxRetailSpace = new ScoreCriterion();
        $maxRetailSpace
            ->setName('maxRetailSpace')
            ->setPoints(8);

        $retailSpace = new MinMaxScoreCriterion();
        $retailSpace
            ->setName('retailSpace')
            ->setMinScoreCriterion($minRetailSpace)
            ->setMaxScoreCriterion($maxRetailSpace);

        $minOutdoorSpace = new ScoreCriterion();
        $minOutdoorSpace
            ->setName('minOutdoorSpace')
            ->setPoints(4);

        $maxOutdoorSpace = new ScoreCriterion();
        $maxOutdoorSpace
            ->setName('maxOutdoorSpace')
            ->setPoints(4);

        $outdoorSpace = new MinMaxScoreCriterion();
        $outdoorSpace
            ->setName('outdoorSpace')
            ->setMinScoreCriterion($minOutdoorSpace)
            ->setMaxScoreCriterion($maxOutdoorSpace);

        $minStorageSpace = new ScoreCriterion();
        $minStorageSpace
            ->setName('minStorageSpace')
            ->setPoints(3);

        $maxStorageSpace = new ScoreCriterion();
        $maxStorageSpace
            ->setName('maxStorageSpace')
            ->setPoints(3);

        $storageSpace = new MinMaxScoreCriterion();
        $storageSpace
            ->setName('storageSpace')
            ->setMinScoreCriterion($minStorageSpace)
            ->setMaxScoreCriterion($maxStorageSpace);

        $minShopWindowLength = new ScoreCriterion();
        $minShopWindowLength
            ->setName('minShopWindowLength')
            ->setPoints(3);

        $maxShopWindowLength = new ScoreCriterion();
        $maxShopWindowLength
            ->setName('maxShopWindowLength')
            ->setPoints(3);

        $shopWindowLength = new MinMaxScoreCriterion();
        $shopWindowLength
            ->setName('shopWindowLength')
            ->setMinScoreCriterion($minShopWindowLength)
            ->setMaxScoreCriterion($maxShopWindowLength);

        $minShopWidth = new ScoreCriterion();
        $minShopWidth
            ->setName('minShopWidth')
            ->setPoints(3);

        $maxShopWidth = new ScoreCriterion();
        $maxShopWidth
            ->setName('maxShopWidth')
            ->setPoints(3);

        $shopWidth = new MinMaxScoreCriterion();
        $shopWidth
            ->setName('shopWidth')
            ->setMinScoreCriterion($minShopWidth)
            ->setMaxScoreCriterion($maxShopWidth);

        $locationCategory = new ScoreCriterion();
        $locationCategory
            ->setName('locationCategory')
            ->setPoints(6);

        $maximumPrice = new ScoreCriterion();
        $maximumPrice
            ->setName('maximumPrice')
            ->setPoints(self::PRICE_SCORE_POINTS);

        $minimumPrice = new ScoreCriterion();
        $minimumPrice
            ->setName('minimumPrice')
            ->setPoints(self::PRICE_SCORE_POINTS);

        $industryDensity = new ScoreCriterion();
        $industryDensity
            ->setName('industryDensity')
            ->setPoints(10);

        $scoreCriteria
            ->setSpace($minMaxSpace)
            ->setRoomCount($roomCount)
            ->setNumberOfParkingLots($numberOfParkingLots)
            ->setAvailableFrom($availableFrom)
            ->setSubsidiarySpace($subsidiarySpace)
            ->setRetailSpace($retailSpace)
            ->setOutdoorSpace($outdoorSpace)
            ->setStorageSpace($storageSpace)
            ->setShopWindowLength($shopWindowLength)
            ->setShopWidth($shopWidth)
            ->setLocationCategory($locationCategory)
            ->setMaximumPrice($maximumPrice)
            ->setMinimumPrice($minimumPrice)
            ->setIndustryDensity($industryDensity);

        return $scoreCriteria;
    }

    private function checkEliminationCriteria(
        EliminationCriteria $eliminationCriteria,
        LookingForPropertyRequest $lookingForPropertyRequest,
        BuildingUnit $buildingUnit
    ): void {
        $this->evaluateInQuarter($eliminationCriteria, $lookingForPropertyRequest, $buildingUnit);
        $this->evaluateInPlace($eliminationCriteria, $lookingForPropertyRequest, $buildingUnit);
        $this->evaluateCommission($eliminationCriteria, $lookingForPropertyRequest, $buildingUnit);
        $this->evaluatePropertyOfferTypes($eliminationCriteria, $lookingForPropertyRequest, $buildingUnit);
        $this->evaluateMaximumPrice($eliminationCriteria, $lookingForPropertyRequest, $buildingUnit);
        $this->evaluateMinimumPrice($eliminationCriteria, $lookingForPropertyRequest, $buildingUnit);
        $this->evaluateRoofing($eliminationCriteria, $lookingForPropertyRequest, $buildingUnit);
        $this->evaluateOutdoorSpaceElimination($eliminationCriteria, $lookingForPropertyRequest, $buildingUnit);
        $this->evaluateShowroom($eliminationCriteria, $lookingForPropertyRequest, $buildingUnit);
        $this->evaluateShopWindow($eliminationCriteria, $lookingForPropertyRequest, $buildingUnit);
        $this->evaluateGroundLevelSalesArea($eliminationCriteria, $lookingForPropertyRequest, $buildingUnit);
        $this->evaluateBarrierFreeAccess($eliminationCriteria, $lookingForPropertyRequest, $buildingUnit);
        $this->evaluateMinimumSpaceElimination($eliminationCriteria, $lookingForPropertyRequest, $buildingUnit);
        $this->evaluateMaximumSpaceElimination($eliminationCriteria, $lookingForPropertyRequest, $buildingUnit);
        $this->evaluateIndustryClassification(
            $eliminationCriteria->getIndustryClassification(),
            $lookingForPropertyRequest,
            $buildingUnit
        );
    }

    private function checkScoreCriteria(
        ScoreCriteria $scoreCriteria,
        LookingForPropertyRequest $lookingForPropertyRequest,
        BuildingUnit $buildingUnit
    ): void {
        $this->evaluateMinMaxCriterion(
            $scoreCriteria->getSpace(),
            $lookingForPropertyRequest->getPropertyRequirement()->getPropertyValueRangeRequirement()->getTotalSpace(),
            $buildingUnit->getAreaSize()
        );

        $this->evaluateRoomCount($scoreCriteria, $lookingForPropertyRequest, $buildingUnit);
        $this->evaluateNumberOfParkingLots($scoreCriteria, $lookingForPropertyRequest, $buildingUnit);
        $this->evaluateAvailableFrom($scoreCriteria, $lookingForPropertyRequest, $buildingUnit);

        $this->evaluateMinMaxCriterion(
            $scoreCriteria->getSubsidiarySpace(),
            $lookingForPropertyRequest->getPropertyRequirement()->getPropertyValueRangeRequirement()->getSubsidiarySpace(),
            $buildingUnit->getPropertySpacesData()?->getSubsidiarySpace()
        );

        $this->evaluateMinMaxCriterion(
            $scoreCriteria->getRetailSpace(),
            $lookingForPropertyRequest->getPropertyRequirement()->getPropertyValueRangeRequirement()->getRetailSpace(),
            $buildingUnit->getPropertySpacesData()?->getRetailSpace()
        );

        $this->evaluateMinMaxCriterion(
            $scoreCriteria->getOutdoorSpace(),
            $lookingForPropertyRequest->getPropertyRequirement()->getPropertyValueRangeRequirement()->getOutdoorSpace(),
            $buildingUnit->getPropertySpacesData()?->getTotalOutdoorArea()
        );

        $this->evaluateMinMaxCriterion(
            $scoreCriteria->getStorageSpace(),
            $lookingForPropertyRequest->getPropertyRequirement()->getPropertyValueRangeRequirement()->getStorageSpace(),
            $buildingUnit->getPropertySpacesData()?->getStorageSpace()
        );

        $this->evaluateMinMaxCriterion(
            $scoreCriteria->getShopWindowLength(),
            $lookingForPropertyRequest->getPropertyRequirement()->getPropertyValueRangeRequirement()->getShopWindowLength(),
            $buildingUnit->getShopWindowFrontWidth()
        );

        $this->evaluateMinMaxCriterion(
            $scoreCriteria->getShopWidth(),
            $lookingForPropertyRequest->getPropertyRequirement()->getPropertyValueRangeRequirement()->getShopWidth(),
            $buildingUnit->getShopWidth()
        );

        $this->evaluateLocationCategory(
            locationCategoryScoreCriterion: $scoreCriteria->getLocationCategory(),
            requestedLocationCategories: $lookingForPropertyRequest->getPropertyRequirement()->getLocationCategories(),
            buildingLocationCategory: $buildingUnit->getBuilding()->getLocationCategory()
        );

        $this->evaluateMaximumPriceScore(
            $scoreCriteria->getMaximumPrice(),
            $lookingForPropertyRequest,
            $buildingUnit
        );

        $this->evaluateMinimumPriceScore(
            $scoreCriteria->getMinimumPrice(),
            $lookingForPropertyRequest,
            $buildingUnit
        );

        $this->evaluateIndustryDensity(
            $scoreCriteria->getIndustryDensity(),
            $lookingForPropertyRequest,
            $buildingUnit
        );
    }

    private function evaluateInQuarter(
        EliminationCriteria $eliminationCriteria,
        LookingForPropertyRequest $lookingForPropertyRequest,
        BuildingUnit $buildingUnit
    ): void {
        $inQuarterCriterion = $eliminationCriteria->getInQuarter();
        $quarterArray = $lookingForPropertyRequest->getPropertyRequirement()->getInQuarters();

        if ($quarterArray->isEmpty()) {
            $inQuarterCriterion->setSet(false);

            return;
        }

        $inQuarterCriterion->setSet(true);
        $buildingUnitQuarter = $buildingUnit->getAddress()->getQuarterPlace();

        if ($quarterArray->contains($buildingUnitQuarter)) {
            $inQuarterCriterion->setEvaluationResult(true);
        }
    }

    private function evaluateInPlace(
        EliminationCriteria $eliminationCriteria,
        LookingForPropertyRequest $lookingForPropertyRequest,
        BuildingUnit $buildingUnit
    ): void {
        $inPlaceCriterion = $eliminationCriteria->getInPlace();
        $inPlaces = $lookingForPropertyRequest->getPropertyRequirement()->getInPlaces();

        if ($eliminationCriteria->getInQuarter()->isSet()) {
            $inPlaceCriterion->setSet(false);

            return;
        }

        if ($inPlaces->isEmpty()) {
            $inPlaceCriterion->setSet(false);

            return;
        }

        $inPlaceCriterion->setSet(true);
        $buildingUnitPlace = $buildingUnit->getAddress()->getPlace();

        if ($inPlaces->contains($buildingUnitPlace)) {
            $inPlaceCriterion->setEvaluationResult(true);
        }
    }

    private function evaluateCommission(
        EliminationCriteria $eliminationCriteria,
        LookingForPropertyRequest $lookingForPropertyRequest,
        BuildingUnit $buildingUnit
    ): void {
        $commissionCriterion = $eliminationCriteria->getCommissionable();
        $marketingInformationCommission = $buildingUnit
            ->getPropertyMarketingInformation()
            ?->getPropertyPricingInformation()
            ?->isCommissionable();

        $nonCommissionable = $lookingForPropertyRequest->getPriceRequirement()->getNonCommissionable();

        if ($marketingInformationCommission === null) {
            return;
        }

        if ($marketingInformationCommission === true && $nonCommissionable === true) {
            $commissionCriterion
                ->setSet(true)
                ->setEvaluationResult(false);

            return;
        }

        $commissionCriterion
            ->setSet(true)
            ->setEvaluationResult(true);
    }

    private function evaluatePropertyOfferTypes(
        EliminationCriteria $eliminationCriteria,
        LookingForPropertyRequest $lookingForPropertyRequest,
        BuildingUnit $buildingUnit
    ): void {
        $priceTypeCriteria = $eliminationCriteria->getPropertyOfferTypes();
        $propertyOfferTypes = $lookingForPropertyRequest->getPriceRequirement()->getPropertyOfferTypes();
        $propertyOfferType = $buildingUnit->getPropertyMarketingInformation()?->getPropertyOfferType();

        if (empty($propertyOfferTypes) === true) {
            $priceTypeCriteria->setSet(false);

            return;
        }

        $priceTypeCriteria->setSet(true);

        if ($propertyOfferType !== null && in_array(needle: $propertyOfferType, haystack: $propertyOfferTypes)) {
            $priceTypeCriteria->setEvaluationResult(true);
        }
    }

    private function evaluateMaximumPrice(
        EliminationCriteria $eliminationCriteria,
        LookingForPropertyRequest $lookingForPropertyRequest,
        BuildingUnit $buildingUnit
    ): void {
        $maximumPriceCriterion = $eliminationCriteria->getMaximumPrice();
        $priceRequirement = $lookingForPropertyRequest->getPriceRequirement();

        $maxPurchasePriceNet = $priceRequirement->getPurchasePriceNet()->getMaximumValue();
        $maxPurchasePriceGross = $priceRequirement->getPurchasePriceGross()->getMaximumValue();
        $maxPurchasePricePerSquareMeter = $priceRequirement->getPurchasePricePerSquareMeter()->getMaximumValue();
        $maxNetColdRent = $priceRequirement->getNetColdRent()->getMaximumValue();
        $maxColdRent = $priceRequirement->getColdRent()->getMaximumValue();
        $maxRentalPricePerSquareMeter = $priceRequirement->getRentalPricePerSquareMeter()->getMaximumValue();

        if ($maxPurchasePriceNet !== null) {
            $maximumPriceCriterion->setSet(true);
        }

        if ($maxPurchasePriceGross !== null) {
            $maximumPriceCriterion->setSet(true);
        }

        if ($maxPurchasePricePerSquareMeter !== null) {
            $maximumPriceCriterion->setSet(true);
        }

        if ($maxNetColdRent !== null) {
            $maximumPriceCriterion->setSet(true);
        }

        if ($maxColdRent !== null) {
            $maximumPriceCriterion->setSet(true);
        }

        if ($maxRentalPricePerSquareMeter !== null) {
            $maximumPriceCriterion->setSet(true);
        }

        if ($maximumPriceCriterion->isSet() === false) {
            return;
        }

        $propertyOfferTypes = $lookingForPropertyRequest->getPriceRequirement()->getPropertyOfferTypes();
        $pricingInformation = $buildingUnit->getPropertyMarketingInformation()?->getPropertyPricingInformation();

        if ($pricingInformation === null) {
            return;
        }

        if (in_array(needle: PropertyOfferType::PURCHASE, haystack: $propertyOfferTypes)) {
            if (
                $maxPurchasePriceNet !== null
                && $maxPurchasePriceNet * 1.2 < $pricingInformation->getPurchasePriceNet()
            ) {
                return;
            }

            if (
                $maxPurchasePriceGross !== null
                && $maxPurchasePriceGross * 1.2 < $pricingInformation->getPurchasePriceGross()
            ) {
                return;
            }

            if (
                $maxPurchasePricePerSquareMeter !== null
                && $maxPurchasePricePerSquareMeter * 1.2 < $pricingInformation->getPurchasePricePerSquareMeter()
            ) {
                return;
            }
        } else {
            if (
                $maxNetColdRent !== null
                && $maxNetColdRent * 1.2 < $pricingInformation->getNetColdRent()
            ) {
                return;
            }

            if (
                $maxColdRent !== null
                && $maxColdRent * 1.2 < $pricingInformation->getColdRent()
            ) {
                return;
            }

            if (
                $maxRentalPricePerSquareMeter !== null
                && $maxRentalPricePerSquareMeter * 1.2 < $pricingInformation->getRentalPricePerSquareMeter()
            ) {
                return;
            }
        }

        $maximumPriceCriterion->setEvaluationResult(true);
    }

    private function evaluateMinimumPrice(
        EliminationCriteria $eliminationCriteria,
        LookingForPropertyRequest $lookingForPropertyRequest,
        BuildingUnit $buildingUnit
    ): void {
         $minimumPriceCriterion = $eliminationCriteria->getMinimumPrice();
         $priceRequirement = $lookingForPropertyRequest->getPriceRequirement();

         $minPurchasePriceNet = $priceRequirement->getPurchasePriceNet()->getMinimumValue();
         $minPurchasePriceGross = $priceRequirement->getPurchasePriceGross()->getMinimumValue();
         $minPurchasePricePerSquareMeter = $priceRequirement->getPurchasePricePerSquareMeter()->getMinimumValue();
         $minNetColdRent = $priceRequirement->getNetColdRent()->getMinimumValue();
         $minColdRent = $priceRequirement->getColdRent()->getMinimumValue();
         $minRentalPricePerSquareMeter = $priceRequirement->getRentalPricePerSquareMeter()->getMinimumValue();

        if ($minPurchasePriceNet !== null) {
            $minimumPriceCriterion->setSet(true);
        }

        if ($minPurchasePriceGross !== null) {
            $minimumPriceCriterion->setSet(true);
        }

        if ($minPurchasePricePerSquareMeter !== null) {
            $minimumPriceCriterion->setSet(true);
        }

        if ($minNetColdRent !== null) {
            $minimumPriceCriterion->setSet(true);
        }

        if ($minColdRent !== null) {
            $minimumPriceCriterion->setSet(true);
        }

        if ($minRentalPricePerSquareMeter !== null) {
            $minimumPriceCriterion->setSet(true);
        }

        if ($minimumPriceCriterion->isSet() === false) {
            return;
        }

        $propertyOfferTypes = $lookingForPropertyRequest->getPriceRequirement()->getPropertyOfferTypes();
        $pricingInformation = $buildingUnit->getPropertyMarketingInformation()?->getPropertyPricingInformation();

        if ($pricingInformation === null) {
            return;
        }

        if (in_array(needle: PropertyOfferType::PURCHASE, haystack: $propertyOfferTypes)) {
            if (
                $minPurchasePriceNet !== null
                && $minPurchasePriceNet * 0.8 > $pricingInformation->getPurchasePriceNet()
            ) {
                return;
            }

            if (
                $minPurchasePriceGross !== null
                && $minPurchasePriceGross * 0.8 > $pricingInformation->getPurchasePriceGross()
            ) {
                return;
            }

            if (
                $minPurchasePricePerSquareMeter !== null
                && $minPurchasePricePerSquareMeter * 0.8 > $pricingInformation->getPurchasePricePerSquareMeter()
            ) {
                return;
            }
        } else {
            if (
                $minNetColdRent !== null
                && $minNetColdRent * 0.8 > $pricingInformation->getNetColdRent()
            ) {
                return;
            }

            if (
                $minColdRent !== null
                && $minColdRent * 0.8 > $pricingInformation->getColdRent()
            ) {
                return;
            }

            if (
                $minRentalPricePerSquareMeter !== null
                && $minRentalPricePerSquareMeter * 0.8 > $pricingInformation->getRentalPricePerSquareMeter()
            ) {
                return;
            }
        }

        $minimumPriceCriterion->setEvaluationResult(true);
    }

    private function evaluateRoomCount(
        ScoreCriteria $scoreCriteria,
        LookingForPropertyRequest $lookingForPropertyRequest,
        BuildingUnit $buildingUnit
    ): void {
        $roomCountCriterion = $scoreCriteria->getRoomCount();
        $rooms = $buildingUnit->getPropertySpacesData()?->getRoomCount();
        $requestedNumberOfRooms = $lookingForPropertyRequest->getPropertyRequirement()->getRoomCount();

        if ($requestedNumberOfRooms === null) {
            $roomCountCriterion->setSet(false);

            return;
        }

        $roomCountCriterion->setSet(true);

        if ($rooms === null) {
            return;
        }

        if ($requestedNumberOfRooms === (int)$rooms) {
            $roomCountCriterion->setEvaluationResult(true);
            $roomCountCriterion->setPointPenalty(0);

            return;
        }

        if (0.8 * $requestedNumberOfRooms <= $rooms && $rooms <= 1.2 * $requestedNumberOfRooms) {
            $roomCountCriterion->setEvaluationResult(true);
            $roomCountCriterion->setPointPenalty(1);

            return;
        }

        $roomCountCriterion->setEvaluationResult(false);
    }

    private function evaluateNumberOfParkingLots(
        ScoreCriteria $scoreCriteria,
        LookingForPropertyRequest $lookingForPropertyRequest,
        BuildingUnit $buildingUnit
    ): void {
        $numberOfParkingLotsCriterion = $scoreCriteria->getNumberOfParkingLots();
        $parkingLots = $buildingUnit->getNumberOfParkingLots();
        $requestedNumberOfParkingLots = $lookingForPropertyRequest->getPropertyRequirement()->getNumberOfParkingLots();

        if ($requestedNumberOfParkingLots === null) {
            $numberOfParkingLotsCriterion->setSet(false);

            return;
        }

        $numberOfParkingLotsCriterion->setSet(true);

        if ($requestedNumberOfParkingLots === $parkingLots) {
            $numberOfParkingLotsCriterion
                ->setEvaluationResult(true)
                ->setPointPenalty(0);

            return;
        }

        $numberOfParkingLotsCriterion->setEvaluationResult(false);
    }

    private function evaluateAvailableFrom(
        ScoreCriteria $scoreCriteria,
        LookingForPropertyRequest $lookingForPropertyRequest,
        BuildingUnit $buildingUnit
    ): void {
        $availableFromCriterion = $scoreCriteria->getAvailableFrom();
        $availableFrom = $buildingUnit->getPropertyMarketingInformation()?->getAvailableFrom();
        $earliestRequestedAvailableFrom = $lookingForPropertyRequest->getPropertyRequirement()->getEarliestAvailableFrom();

        if ($earliestRequestedAvailableFrom === null) {
            $availableFromCriterion->setSet(false);

            return;
        }

        $availableFromCriterion->setSet(true);

        if ($availableFrom === null) {
            return;
        }

        if ($availableFrom < $earliestRequestedAvailableFrom) {
            $availableFromCriterion->setEvaluationResult(false);

            return;
        }

        $availableFromCriterion->setEvaluationResult(true);
    }

    private function evaluateRetailSpace(
        ScoreCriteria $scoreCriteria,
        LookingForPropertyRequest $lookingForPropertyRequest,
        BuildingUnit $buildingUnit
    ): void {
        $retailSpaceCriterion = $scoreCriteria->getRetailSpace();
        $retailSpace = $buildingUnit->getPropertySpacesData()?->getRetailSpace();
        $requestedRetailSpace = $lookingForPropertyRequest
            ->getPropertyRequirement()
            ->getPropertyValueRangeRequirement()
            ->getRetailSpace();

        if ($requestedRetailSpace->getMaximumValue() === null && $requestedRetailSpace->getMinimumValue() === null) {
            $retailSpaceCriterion->setSet(false);

            return;
        }

        $retailSpaceCriterion->setSet(true);

        if ($retailSpace === null) {
            return;
        }

        if (
            $requestedRetailSpace->getMinimumValue() !== null
            && $requestedRetailSpace->getMinimumValue() > $retailSpace
        ) {
            $retailSpaceCriterion->setEvaluationResult(false);

            return;
        }

        if (
            $requestedRetailSpace->getMaximumValue() !== null
            && $retailSpace > $requestedRetailSpace->getMaximumValue()
        ) {
            $retailSpaceCriterion->setEvaluationResult(false);

            return;
        }

        $retailSpaceCriterion->setEvaluationResult(true);
    }

    private function evaluateRoofing(
        EliminationCriteria $eliminationCriteria,
        LookingForPropertyRequest $lookingForPropertyRequest,
        BuildingUnit $buildingUnit
    ): void {
        $roofingCriterion = $eliminationCriteria->getRoofing();
        $isRoofingRequired = $lookingForPropertyRequest
            ->getPropertyRequirement()
            ->getPropertyMandatoryRequirement()
            ->isRoofingRequired();

        if ($isRoofingRequired === false) {
            $roofingCriterion->setSet(false);

            return;
        }

        $roofingCriterion->setSet(true);
        $buildingUnitRoof = $buildingUnit->getRoofingPresent();

        if ($buildingUnitRoof === true) {
            $roofingCriterion->setEvaluationResult(true);
        }
    }

    private function evaluateOutdoorSpaceElimination(
        EliminationCriteria $eliminationCriteria,
        LookingForPropertyRequest $lookingForPropertyRequest,
        BuildingUnit $buildingUnit
    ): void {
        $outdoorSpaceCriterion = $eliminationCriteria->getOutdoorSpace();
        $outdoorSpaceRequired = $lookingForPropertyRequest
            ->getPropertyRequirement()
            ->getPropertyMandatoryRequirement()
            ->isOutdoorSpaceRequired();

        if ($outdoorSpaceRequired === false) {
            $outdoorSpaceCriterion->setSet(false);

            return;
        }

        $outdoorSpaceCriterion->setSet(true);
        $outdoorSellingArea = $buildingUnit->getPropertySpacesData()?->getTotalOutdoorArea();

        if ($outdoorSellingArea !== null) {
            $outdoorSpaceCriterion->setEvaluationResult(true);
        }
    }

    private function evaluateShowroom(
        EliminationCriteria $eliminationCriteria,
        LookingForPropertyRequest $lookingForPropertyRequest,
        BuildingUnit $buildingUnit
    ): void {
        $showroomCriterion = $eliminationCriteria->getShowroom();
        $isShowroomRequired = $lookingForPropertyRequest
            ->getPropertyRequirement()
            ->getPropertyMandatoryRequirement()
            ->isShowroomRequired();

        if ($isShowroomRequired === false) {
            $showroomCriterion->setSet(false);

            return;
        }

        $showroomCriterion->setSet(true);
        $showroomAvailable = $buildingUnit->isShowroomAvailable();

        if ($showroomAvailable === true) {
            $showroomCriterion->setEvaluationResult(true);
        }
    }

    private function evaluateShopWindow(
        EliminationCriteria $eliminationCriteria,
        LookingForPropertyRequest $lookingForPropertyRequest,
        BuildingUnit $buildingUnit
    ): void {
        $shopWindowCriterion = $eliminationCriteria->getShopWindow();
        $isShopWindowRequired = $lookingForPropertyRequest
            ->getPropertyRequirement()
            ->getPropertyMandatoryRequirement()
            ->isShopWindowRequired();

        if ($isShopWindowRequired === false) {
            $shopWindowCriterion->setSet(false);

            return;
        }

        $shopWindowCriterion->setSet(true);
        $shopWindowFrontWidth = $buildingUnit->getShopWindowFrontWidth();

        if ($shopWindowFrontWidth !== null) {
            $shopWindowCriterion->setEvaluationResult(true);
        }
    }

    private function evaluateGroundLevelSalesArea(
        EliminationCriteria $eliminationCriteria,
        LookingForPropertyRequest $lookingForPropertyRequest,
        BuildingUnit $buildingUnit
    ): void {
        $groundSalesLevelAreaCriterion = $eliminationCriteria->getGroundLevelSalesArea();
        $isGroundLevelSalesAreaRequired = $lookingForPropertyRequest
            ->getPropertyRequirement()
            ->getPropertyMandatoryRequirement()
            ->isGroundLevelSalesAreaRequired();

        if ($isGroundLevelSalesAreaRequired === false) {
            $groundSalesLevelAreaCriterion->setSet(false);

            return;
        }

        $groundSalesLevelAreaCriterion->setSet(true);
        $groundLevelSalesArea = $buildingUnit->isGroundLevelSalesArea();

        if ($groundLevelSalesArea === true) {
            $groundSalesLevelAreaCriterion->setEvaluationResult(true);
        }
    }

    private function evaluateBarrierFreeAccess(
        EliminationCriteria $eliminationCriteria,
        LookingForPropertyRequest $lookingForPropertyRequest,
        BuildingUnit $buildingUnit
    ): void {
        $barrierFreeAccessCriterion = $eliminationCriteria->getBarrierFreeAccess();
        $isBarrierFreeAccessRequired = $lookingForPropertyRequest
            ->getPropertyRequirement()
            ->getPropertyMandatoryRequirement()
            ->isBarrierFreeAccessRequired();

        if ($isBarrierFreeAccessRequired === false) {
            $barrierFreeAccessCriterion->setSet(false);

            return;
        }

        $barrierFreeAccessCriterion->setSet(true);
        $barrierFreeAccess = $buildingUnit->getBarrierFreeAccess();

        if (
            $barrierFreeAccess === BarrierFreeAccess::IS_AVAILABLE
            || $barrierFreeAccess === BarrierFreeAccess::CAN_BE_GUARANTEED
        ) {
            $barrierFreeAccessCriterion->setEvaluationResult(true);
        }
    }

    private function evaluateMinMaxCriterion(
        MinMaxScoreCriterion $minMaxScoreCriterion,
        ValueRequirement $valueRequirement,
        ?float $value
    ): void {
        $minScoreCriterion = $minMaxScoreCriterion->getMinScoreCriterion();
        $maxScoreCriterion = $minMaxScoreCriterion->getMaxScoreCriterion();
        $minScoreCriterion->setSet($valueRequirement->getMinimumValue() !== null);
        $maxScoreCriterion->setSet($valueRequirement->getMaximumValue() !== null);

        if (!$minScoreCriterion->isSet() && !$maxScoreCriterion->isSet()) {
            return;
        }

        if ($value === null) {
            return;
        }

        if (
            $minScoreCriterion->isSet()
            && $value >= $valueRequirement->getMinimumValue()
        ) {
            $minScoreCriterion->setEvaluationResult(true);
        }

        if (
            $maxScoreCriterion->isSet()
            && $value <=$valueRequirement->getMaximumValue()
        ) {
            $maxScoreCriterion->setEvaluationResult(true);
        }
    }

    private function evaluateMinimumSpaceElimination(
        EliminationCriteria $eliminationCriteria,
        LookingForPropertyRequest $lookingForPropertyRequest,
        BuildingUnit $buildingUnit
    ): void {
        $minSpaceEliminationCriterion = $eliminationCriteria->getMinimumSpace();
        $space = $buildingUnit->getAreaSize();
        $requiredSpace = $lookingForPropertyRequest
            ->getPropertyRequirement()
            ->getPropertyValueRangeRequirement()
            ->getTotalSpace();

        if ($requiredSpace->getMinimumValue() === null) {
            $minSpaceEliminationCriterion->setSet(false);

            return;
        }

        $minSpaceEliminationCriterion->setSet(true);

        if ($space === null) {
            return;
        }

        if (0.8 * $requiredSpace->getMinimumValue() <= $space) {
            $minSpaceEliminationCriterion->setEvaluationResult(true);
        }
    }

    private function evaluateMaximumSpaceElimination(
        EliminationCriteria $eliminationCriteria,
        LookingForPropertyRequest $lookingForPropertyRequest,
        BuildingUnit $buildingUnit
    ): void {
        $maximumSpaceEliminationCriterion = $eliminationCriteria->getMaximumSpace();
        $space = $buildingUnit->getAreaSize();
        $requiredSpace = $lookingForPropertyRequest
            ->getPropertyRequirement()
            ->getPropertyValueRangeRequirement()
            ->getTotalSpace();

        if ($requiredSpace->getMaximumValue() === null) {
            $maximumSpaceEliminationCriterion->setSet(false);

            return;
        }

        $maximumSpaceEliminationCriterion->setSet(true);

        if ($space === null) {
            return;
        }

        if ($space <= 1.2 * $requiredSpace->getMaximumValue()) {
            $maximumSpaceEliminationCriterion->setEvaluationResult(true);
        }
    }

    private function evaluateLocationCategory(
        ScoreCriterion $locationCategoryScoreCriterion,
        ?array $requestedLocationCategories,
        ?LocationCategory $buildingLocationCategory
    ): void {
        if (empty($requestedLocationCategories) === true) {
            $locationCategoryScoreCriterion->setSet(false);

            return;
        }

        $locationCategoryScoreCriterion->setSet(true);

        if (in_array($buildingLocationCategory, $requestedLocationCategories)) {
            $locationCategoryScoreCriterion->setEvaluationResult(true);
        }
    }

    private function evaluateMaximumPriceScore(
        ScoreCriterion $maximumPriceCriterion,
        LookingForPropertyRequest $lookingForPropertyRequest,
        BuildingUnit $buildingUnit
    ): void {
        $priceRequirement = $lookingForPropertyRequest->getPriceRequirement();
        $maxPurchasePriceNet = $priceRequirement->getPurchasePriceNet()->getMaximumValue();
        $maxPurchasePriceGross = $priceRequirement->getPurchasePriceGross()->getMaximumValue();
        $maxPurchasePricePerSquareMeter = $priceRequirement->getPurchasePricePerSquareMeter()->getMaximumValue();
        $maxNetColdRent = $priceRequirement->getNetColdRent()->getMaximumValue();
        $maxColdRent = $priceRequirement->getColdRent()->getMaximumValue();
        $maxRentalPricePerSquareMeter = $priceRequirement->getRentalPricePerSquareMeter()->getMaximumValue();

        if ($maxPurchasePriceNet !== null) {
            $maximumPriceCriterion->setSet(true);
        }

        if ($maxPurchasePriceGross !== null) {
            $maximumPriceCriterion->setSet(true);
        }

        if ($maxPurchasePricePerSquareMeter !== null) {
            $maximumPriceCriterion->setSet(true);
        }

        if ($maxNetColdRent !== null) {
            $maximumPriceCriterion->setSet(true);
        }

        if ($maxColdRent !== null) {
            $maximumPriceCriterion->setSet(true);
        }

        if ($maxRentalPricePerSquareMeter !== null) {
            $maximumPriceCriterion->setSet(true);
        }

        if ($maximumPriceCriterion->isSet() === false) {
            return;
        }

        $propertyOfferTypes = $lookingForPropertyRequest->getPriceRequirement()->getPropertyOfferTypes();
        $pricingInformation = $buildingUnit->getPropertyMarketingInformation()?->getPropertyPricingInformation();

        if ($pricingInformation === null) {
            return;
        }

        if (in_array(needle: PropertyOfferType::PURCHASE, haystack: $propertyOfferTypes)) {
            if (
                $maxPurchasePriceNet !== null
                && $maxPurchasePriceNet < $pricingInformation->getPurchasePriceNet()
            ) {
                return;
            }

            if (
                $maxPurchasePriceGross !== null
                && $maxPurchasePriceGross < $pricingInformation->getPurchasePriceGross()
            ) {
                return;
            }

            if (
                $maxPurchasePricePerSquareMeter !== null
                && $maxPurchasePricePerSquareMeter < $pricingInformation->getPurchasePricePerSquareMeter()
            ) {
                return;
            }
        } else {
            if (
                $maxNetColdRent !== null
                && $maxNetColdRent < $pricingInformation->getNetColdRent()
            ) {
                return;
            }

            if (
                $maxColdRent !== null
                && $maxColdRent < $pricingInformation->getColdRent()
            ) {
                return;
            }

            if (
                $maxRentalPricePerSquareMeter !== null
                && $maxRentalPricePerSquareMeter  < $pricingInformation->getRentalPricePerSquareMeter()
            ) {
                return;
            }
        }

        $maximumPriceCriterion->setEvaluationResult(true);
    }

    private function evaluateMinimumPriceScore(
        ScoreCriterion $minimumPriceCriterion,
        LookingForPropertyRequest $lookingForPropertyRequest,
        BuildingUnit $buildingUnit
    ): void {
        $priceRequirement = $lookingForPropertyRequest->getPriceRequirement();

        $minPurchasePriceNet = $priceRequirement->getPurchasePriceNet()->getMinimumValue();
        $minPurchasePriceGross = $priceRequirement->getPurchasePriceGross()->getMinimumValue();
        $minPurchasePricePerSquareMeter = $priceRequirement->getPurchasePricePerSquareMeter()->getMinimumValue();
        $minNetColdRent = $priceRequirement->getNetColdRent()->getMinimumValue();
        $minColdRent = $priceRequirement->getColdRent()->getMinimumValue();
        $minRentalPricePerSquareMeter = $priceRequirement->getRentalPricePerSquareMeter()->getMinimumValue();

        if ($minPurchasePriceNet !== null) {
            $minimumPriceCriterion->setSet(true);
        }

        if ($minPurchasePriceGross !== null) {
            $minimumPriceCriterion->setSet(true);
        }

        if ($minPurchasePricePerSquareMeter !== null) {
            $minimumPriceCriterion->setSet(true);
        }

        if ($minNetColdRent !== null) {
            $minimumPriceCriterion->setSet(true);
        }

        if ($minColdRent !== null) {
            $minimumPriceCriterion->setSet(true);
        }

        if ($minRentalPricePerSquareMeter !== null) {
            $minimumPriceCriterion->setSet(true);
        }

        if ($minimumPriceCriterion->isSet() === false) {
            return;
        }

        $propertyOfferTypes = $lookingForPropertyRequest->getPriceRequirement()->getPropertyOfferTypes();
        $pricingInformation = $buildingUnit->getPropertyMarketingInformation()?->getPropertyPricingInformation();

        if ($pricingInformation === null) {
            return;
        }

        if (in_array(needle: PropertyOfferType::PURCHASE, haystack: $propertyOfferTypes)) {
            if (
                $minPurchasePriceNet !== null
                && $minPurchasePriceNet > $pricingInformation->getPurchasePriceNet()
            ) {
                return;
            }

            if (
                $minPurchasePriceGross !== null
                && $minPurchasePriceGross > $pricingInformation->getPurchasePriceGross()
            ) {
                return;
            }

            if (
                $minPurchasePricePerSquareMeter !== null
                && $minPurchasePricePerSquareMeter > $pricingInformation->getPurchasePricePerSquareMeter()
            ) {
                return;
            }
        } else {
            if (
                $minNetColdRent !== null
                && $minNetColdRent > $pricingInformation->getNetColdRent()
            ) {
                return;
            }

            if (
                $minColdRent !== null
                && $minColdRent > $pricingInformation->getColdRent()
            ) {
                return;
            }

            if (
                $minRentalPricePerSquareMeter !== null
                && $minRentalPricePerSquareMeter > $pricingInformation->getRentalPricePerSquareMeter()
            ) {
                return;
            }
        }

        $minimumPriceCriterion->setEvaluationResult(true);
    }

    private function evaluateIndustryDensity(
        ScoreCriterion $branchDensityCriterion,
        LookingForPropertyRequest $lookingForPropertyRequest,
        BuildingUnit $buildingUnit
    ): void {
        if ($buildingUnit->getGeolocationPoint() === null) {
            $branchDensityCriterion->setSet(set: false);

            return;
        }

        $branchDensityCriterion->setSet(set: true);

        $buildingUnits = $this->buildingUnitService->fetchBuildingUnitsWithCurrentUsageInBoundingBox(
            account: $buildingUnit->getAccount(),
            withFromSubAccounts: true,
            archived: false,
            buildingUnit: $buildingUnit,
            industryClassification: $lookingForPropertyRequest->getPropertyRequirement()->getIndustryClassification()
        );

        $branchDensityCriterion->setEvaluationResult(
            evaluationResult: count($buildingUnits) < self::INDUSTRY_DIVERSITY_THRESHOLD
        );
    }

    private function evaluateIndustryClassification(
        EliminationCriterion $industryClassification,
        LookingForPropertyRequest $lookingForPropertyRequest,
        BuildingUnit $buildingUnit
    ): void {
        if ($lookingForPropertyRequest->getPropertyRequirement()->getIndustryClassification() === null) {
            $industryClassification->setSet(false);

            return;
        }

        $industryClassification->setSet(true);

        $propertyRequirementIndustryClassificationParent = $this->classificationService->getTopMostParentFromIndustryClassification(
            $lookingForPropertyRequest->getPropertyRequirement()->getIndustryClassification()
        );

        /** @var PropertyUser $propertyUser */
        foreach ($buildingUnit->getPropertyUsers() as $propertyUser) {
            if ($propertyUser->getIndustryClassification() === null) {
                continue;
            }
            $buildingUnitIndustryClassificationParent = $this->classificationService->getTopMostParentFromIndustryClassification(
                $propertyUser->getIndustryClassification()
            );

            if ($buildingUnitIndustryClassificationParent === $propertyRequirementIndustryClassificationParent) {
                $industryClassification->setEvaluationResult(true);

                return;
            }
        }

        $possibleUsageIndustryClassifications = $buildingUnit
            ->getPropertyMarketingInformation()
            ?->getPossibleUsageIndustryClassifications();

        if ($possibleUsageIndustryClassifications === null) {
            return;
        }

        foreach ($possibleUsageIndustryClassifications as $possibleUsageIndustryClassification) {
            $possibleUsageIndustryClassificationParent = $this->classificationService->getTopMostParentFromIndustryClassification(
                $possibleUsageIndustryClassification
            );

            if ($propertyRequirementIndustryClassificationParent === $possibleUsageIndustryClassificationParent) {
                $industryClassification->setEvaluationResult(true);

                return;
            }
        }
    }
}
