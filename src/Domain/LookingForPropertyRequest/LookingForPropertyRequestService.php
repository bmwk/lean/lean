<?php

declare(strict_types=1);

namespace App\Domain\LookingForPropertyRequest;

use App\Domain\Entity\Account;
use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\ForwardingResponse;
use App\Domain\Entity\LookingForPropertyRequest\LookingForPropertyRequest;
use App\Domain\Entity\LookingForPropertyRequest\LookingForPropertyRequestExposeForwarding;
use App\Domain\Entity\LookingForPropertyRequest\LookingForPropertyRequestFilter;
use App\Domain\Entity\LookingForPropertyRequest\LookingForPropertyRequestMatchingResponse;
use App\Domain\Entity\LookingForPropertyRequest\LookingForPropertyRequestSearch;
use App\Domain\Entity\LookingForPropertyRequest\LookingForPropertyRequestsStatistics;
use App\Domain\Entity\LookingForPropertyRequest\PropertyRequirement;
use App\Domain\Entity\LookingForPropertyRequestReport\LookingForPropertyRequestReport;
use App\Domain\Entity\Notification\NotificationType;
use App\Domain\Entity\Person\Person;
use App\Domain\Entity\PriceRequirement;
use App\Domain\Entity\Property\BuildingUnit;
use App\Domain\Entity\SortingOption\SortingOption;
use App\Domain\Notification\NotificationService;
use App\Repository\LookingForPropertyRequest\LookingForPropertyRequestExposeForwardingRepository;
use App\Repository\LookingForPropertyRequest\LookingForPropertyRequestMatchingResponseRepository;
use App\Repository\LookingForPropertyRequest\LookingForPropertyRequestRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;

class LookingForPropertyRequestService
{
    public function __construct(
        private readonly LookingForPropertyRequestRepository $lookingForPropertyRequestRepository,
        private readonly LookingForPropertyRequestExposeForwardingRepository $lookingForPropertyRequestExposeForwardingRepository,
        private readonly LookingForPropertyRequestMatchingResponseRepository $lookingForPropertyRequestMatchingResponseRepository,
        private readonly NotificationService $notificationService,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    /**
     * @return LookingForPropertyRequest[]
     */
    public function fetchLookingForPropertyRequestsByAccount(
        Account $account,
        bool $withFromSubAccounts,
        ?bool $archived = null,
        ?bool $active = null
    ): array {
        return $this->lookingForPropertyRequestRepository->findByAccount(
            account: $account,
            withFromSubAccounts: $withFromSubAccounts,
            archived: $archived,
            active: $active
        );
    }

    public function fetchLookingForPropertyRequestsPaginatedByAccount(
        Account $account,
        bool $withFromSubAccounts,
        int $firstResult,
        int $maxResults,
        ?bool $archived = null,
        ?bool $active = null,
        ?LookingForPropertyRequestFilter $lookingForPropertyRequestFilter = null,
        ?LookingForPropertyRequestSearch $lookingForPropertyRequestSearch = null,
        ?SortingOption $sortingOption = null
    ): Paginator {
        return $this->lookingForPropertyRequestRepository->findPaginatedByAccount(
            account: $account,
            withFromSubAccounts: $withFromSubAccounts,
            firstResult: $firstResult,
            maxResults: $maxResults,
            archived: $archived,
            active: $active,
            lookingForPropertyRequestFilter: $lookingForPropertyRequestFilter,
            lookingForPropertyRequestSearch: $lookingForPropertyRequestSearch,
            sortingOption: $sortingOption,
        );
    }

    /**
     * @param int[] $ids
     * @return LookingForPropertyRequest[]
     */
    public function fetchLookingForPropertyRequestsByIds(Account $account, bool $withFromSubAccounts, array $ids, ?bool $archived = null): array
    {
        return $this->lookingForPropertyRequestRepository->findByIds(
            account: $account,
            withFromSubAccounts: $withFromSubAccounts,
            ids: $ids,
            archived: $archived
        );
    }

    /**
     * @return LookingForPropertyRequest[]
     */
    public function fetchLookingForPropertyRequestsByPerson(
        Account $account, bool $withFromSubAccounts,
        Person $person,
        ?bool $archived = null,
        ?bool $active = null
    ): array {
        return $this->lookingForPropertyRequestRepository->findByPerson(
            account: $account,
            withFromSubAccounts: $withFromSubAccounts,
            person: $person,
            archived: $archived,
            active: $active
        );
    }

    public function fetchLookingForPropertyRequestsPaginatedByPerson(
        Account $account,
        bool $withFromSubAccounts,
        Person $person,
        int $firstResult,
        int $maxResults,
        ?bool $archived = null,
        ?bool $active = null,
        ?SortingOption $sortingOption = null
    ): Paginator {
        return $this->lookingForPropertyRequestRepository->findPaginatedByPerson(
            account: $account,
            withFromSubAccounts: $withFromSubAccounts,
            person: $person,
            firstResult: $firstResult,
            maxResults: $maxResults,
            archived: $archived,
            active: $active,
            sortingOption: $sortingOption
        );
    }

    /**
     * @param int[] $ids
     * @return LookingForPropertyRequest[]
     */
    public function fetchLookingForPropertyRequestsByPersonAndIds(
        Account $account,
        bool $withFromSubAccounts,
        Person $person,
        array $ids,
        ?bool $archived = null
    ): array {
        return $this->lookingForPropertyRequestRepository->findByPersonAndIds(
            account: $account,
            withFromSubAccounts: $withFromSubAccounts,
            person: $person,
            ids: $ids,
            archived: $archived
        );
    }

    public function fetchLookingForPropertyRequestById(Account $account, bool $withFromSubAccounts, int $id): ?LookingForPropertyRequest
    {
        return $this->lookingForPropertyRequestRepository->findOneById(
            account: $account,
            withFromSubAccounts: $withFromSubAccounts,
            id: $id
        );
    }

    public function fetchLookingForPropertyRequestByPersonAndId(Account $account, bool $withFromSubAccounts, Person $person, int $id): ?LookingForPropertyRequest
    {
        return $this->lookingForPropertyRequestRepository->findOneByPersonAndId(
            account: $account,
            withFromSubAccounts: $withFromSubAccounts,
            person: $person,
            id: $id
        );
    }

    /**
     * @return LookingForPropertyRequest[]
     */
    public function fetchLookingForPropertyRequestsWithoutMatchFound(Account $account, bool $withFromSubAccounts, int $noMatchFoundInDays, int $intervalLength): array
    {
        return $this->lookingForPropertyRequestRepository->findWithoutMatchFound(
            account: $account,
            withFromSubAccounts: $withFromSubAccounts,
            noMatchFoundInDays: $noMatchFoundInDays,
            intervalLength: $intervalLength
        );
    }

    public function fetchLookingForPropertyRequestsStatisticsByPerson(Account $account, Person $person): LookingForPropertyRequestsStatistics
    {
        $lookingForPropertyRequestsStatistics = new LookingForPropertyRequestsStatistics();

        $lookingForPropertyRequestsStatistics
            ->setActive($this->lookingForPropertyRequestRepository->countByPerson(account: $account, withFromSubAccounts: false, person: $person, archived: false, active: true))
            ->setInactive($this->lookingForPropertyRequestRepository->countByPerson(account: $account, withFromSubAccounts: false, person: $person, archived: false, active: false))
            ->setArchived($this->lookingForPropertyRequestRepository->countByPerson(account: $account, withFromSubAccounts: false, person: $person, archived: true, active: null));

        return $lookingForPropertyRequestsStatistics;
    }

    public function fetchLookingForPropertyRequestExposeForwardingByLookingForPropertyRequestAndBuildingUnit(
        Account $account,
        LookingForPropertyRequest $lookingForPropertyRequest,
        BuildingUnit $buildingUnit,
    ): ?LookingForPropertyRequestExposeForwarding {
        return $this->lookingForPropertyRequestExposeForwardingRepository->findOneByLookingForPropertyRequestAndBuildingUnit(
            account: $account,
            lookingForPropertyRequest: $lookingForPropertyRequest,
            buildingUnit: $buildingUnit
        );
    }

    public function fetchLookingForPropertyRequestMatchingResponseByLookingForPropertyRequestAndBuildingUnit(
        Account $account,
        LookingForPropertyRequest $lookingForPropertyRequest,
        BuildingUnit $buildingUnit,
    ): ?LookingForPropertyRequestMatchingResponse {
        return $this->lookingForPropertyRequestMatchingResponseRepository->findOneByLookingForPropertyRequestAndBuildingUnit(
            account: $account,
            lookingForPropertyRequest: $lookingForPropertyRequest,
            buildingUnit: $buildingUnit
        );
    }

    /**
     * @return LookingForPropertyRequestExposeForwarding[]
     */
    public function fetchLookingForPropertyRequestExposeForwardingByPersonAndLookingForPropertyRequest(
        Account $account,
        Person $person,
        LookingForPropertyRequest $lookingForPropertyRequest
    ): array {
        return $this->lookingForPropertyRequestExposeForwardingRepository->findByPersonAndLookingForPropertyRequest(
            account: $account,
            person: $person,
            lookingForPropertyRequest: $lookingForPropertyRequest,
        );
    }

    public function fetchLookingForPropertyRequestExposeForwardingByPersonAndId(Account $account, Person $person, int $id): ?LookingForPropertyRequestExposeForwarding
    {
        return $this->lookingForPropertyRequestExposeForwardingRepository->findOneByPersonAndId(account: $account,person: $person, id: $id);
    }

    public function countLookingForPropertyRequestsByAccount(
        Account $account,
        bool $withFromSubAccounts,
        ?bool $archived = null,
        ?bool $active = null
    ): int {
        return $this->lookingForPropertyRequestRepository->countByAccount(
            account: $account,
            withFromSubAccounts: $withFromSubAccounts,
            archived: $archived,
            active: $active
        );
    }

    public function countLookingForPropertyRequestExposeForwardingsByAccount(Account $account): int
    {
        return $this->lookingForPropertyRequestExposeForwardingRepository->countByAccount(account: $account);
    }

    public function countLookingForPropertyRequestExposeForwardingsByAccountAndForwardingResponse(
        Account $account,
        ForwardingResponse $forwardingResponse
    ): int {
        return $this->lookingForPropertyRequestExposeForwardingRepository->countByAccountAndForwardingResponse(
            account: $account,
            forwardingResponse: $forwardingResponse
        );
    }

    public function countLookingForPropertyRequestsByAssignedAccountUser(Account $account, AccountUser $accountUser): int
    {
        return $this->lookingForPropertyRequestRepository->count(criteria: [
            'account'               => $account,
            'assignedToAccountUser' => $accountUser,
            'deleted'               => false,
        ]);
    }

    public function countLookingForPropertyRequestsByCreatedByAccountUser(Account $account, AccountUser $accountUser): int
    {
        return $this->lookingForPropertyRequestRepository->count(criteria: [
            'account'              => $account,
            'createdByAccountUser' => $accountUser,
            'deleted'              => false,
        ]);
    }

    public function buildLookingForPropertyRequestsStatisticsByAccount(Account $account): LookingForPropertyRequestsStatistics
    {
        $lookingForPropertyRequestsStatistics = new LookingForPropertyRequestsStatistics();

        $lookingForPropertyRequestsStatistics
            ->setActive($this->lookingForPropertyRequestRepository->countByAccount(account: $account, withFromSubAccounts: true, archived: false, active: true))
            ->setInactive($this->lookingForPropertyRequestRepository->countByAccount(account: $account, withFromSubAccounts: true, archived: false, active: false))
            ->setArchived($this->lookingForPropertyRequestRepository->countByAccount(account: $account, withFromSubAccounts: true, archived: true, active: null));

        return $lookingForPropertyRequestsStatistics;
    }


    public function persistLookingForPropertyRequest(LookingForPropertyRequest $lookingForPropertyRequest): void
    {
        $this->persistPropertyRequirement(propertyRequirement: $lookingForPropertyRequest->getPropertyRequirement());
        $this->persistPriceRequirement(priceRequirement: $lookingForPropertyRequest->getPriceRequirement());

        $this->entityManager->persist(entity: $lookingForPropertyRequest);
    }

    public function persistPropertyRequirement(PropertyRequirement $propertyRequirement): void
    {
        $propertyValueRangeRequirement = $propertyRequirement->getPropertyValueRangeRequirement();

        $this->entityManager->persist(entity: $propertyValueRangeRequirement->getTotalSpace());
        $this->entityManager->persist(entity: $propertyValueRangeRequirement->getRetailSpace());
        $this->entityManager->persist(entity: $propertyValueRangeRequirement->getOutdoorSpace());
        $this->entityManager->persist(entity: $propertyValueRangeRequirement->getUsableSpace());
        $this->entityManager->persist(entity: $propertyValueRangeRequirement->getStorageSpace());
        $this->entityManager->persist(entity: $propertyValueRangeRequirement->getShopWindowLength());
        $this->entityManager->persist(entity: $propertyValueRangeRequirement->getShopWidth());
        $this->entityManager->persist(entity: $propertyValueRangeRequirement->getSubsidiarySpace());

        $this->entityManager->persist(entity: $propertyValueRangeRequirement);

        $this->entityManager->persist(entity: $propertyRequirement->getPropertyMandatoryRequirement());

        $this->entityManager->persist(entity: $propertyRequirement);
    }

    public function persistPriceRequirement(PriceRequirement $priceRequirement): void
    {
        $this->entityManager->persist(entity: $priceRequirement->getPurchasePriceNet());
        $this->entityManager->persist(entity: $priceRequirement->getPurchasePriceGross());
        $this->entityManager->persist(entity: $priceRequirement->getPurchasePricePerSquareMeter());
        $this->entityManager->persist(entity: $priceRequirement->getNetColdRent());
        $this->entityManager->persist(entity: $priceRequirement->getColdRent());
        $this->entityManager->persist(entity: $priceRequirement->getRentalPricePerSquareMeter());
        $this->entityManager->persist(entity: $priceRequirement->getLease());

        $this->entityManager->persist(entity: $priceRequirement);
    }

    public static function createLookingForPropertyRequestFromLookingForPropertyRequestReport(
        LookingForPropertyRequestReport $lookingForPropertyRequestReport,
        AccountUser $accountUser
    ): LookingForPropertyRequest {
        $account = $lookingForPropertyRequestReport->getAccount();

        $lookingForPropertyRequest = LookingForPropertyRequest::createFromLookingForPropertyRequestReport(lookingForPropertyRequestReport: $lookingForPropertyRequestReport);

        $lookingForPropertyRequest
            ->setAccount($account)
            ->setCreatedByAccountUser($accountUser);

        return $lookingForPropertyRequest;
    }

    public function createNoMatchFoundNotificationsByAccount(Account $account, int $noMatchFoundInDays, int $intervalLength): void
    {
        $notificationType = NotificationType::NO_MATCH_FOUND_FOR_LOOKING_FOR_PROPERTY_REQUEST;

        $lookingForPropertyRequests = $this->fetchLookingForPropertyRequestsWithoutMatchFound(
            account: $account,
            withFromSubAccounts: false,
            noMatchFoundInDays: $noMatchFoundInDays,
            intervalLength: $intervalLength
        );

        foreach ($lookingForPropertyRequests as $lookingForPropertyRequest) {
            $notification = $this->notificationService->fetchNotificationByNotificationTypeAndObject(
                accountUser: $lookingForPropertyRequest->getCreatedByAccountUser(),
                notificationType: $notificationType,
                object: $lookingForPropertyRequest
            );

            if ($notification === null) {
                $this->notificationService->createAndSaveNotification(
                    accountUser: $lookingForPropertyRequest->getCreatedByAccountUser(),
                    notificationType: $notificationType,
                    beenRead: false,
                    lookingForPropertyRequest: $lookingForPropertyRequest
                );
            }
        }
    }
}
