<?php

declare(strict_types=1);

namespace App\Domain\LookingForPropertyRequest;

use App\Domain\Entity\PriceRequirement;
use Doctrine\ORM\EntityManagerInterface;

class PriceRequirementDeletionService
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function deletePriceRequirement(PriceRequirement $priceRequirement): void
    {
        $this->hardDeletePriceRequirement(priceRequirement: $priceRequirement);
    }

    private function hardDeletePriceRequirement(PriceRequirement $priceRequirement): void
    {
        $this->entityManager->remove(entity: $priceRequirement);
        $this->entityManager->remove(entity: $priceRequirement->getPurchasePriceNet());
        $this->entityManager->remove(entity: $priceRequirement->getPurchasePriceGross());
        $this->entityManager->remove(entity: $priceRequirement->getPurchasePricePerSquareMeter());
        $this->entityManager->remove(entity: $priceRequirement->getNetColdRent());
        $this->entityManager->remove(entity: $priceRequirement->getColdRent());
        $this->entityManager->remove(entity: $priceRequirement->getRentalPricePerSquareMeter());
        $this->entityManager->remove(entity: $priceRequirement->getLease());

        $this->entityManager->flush();
    }
}
