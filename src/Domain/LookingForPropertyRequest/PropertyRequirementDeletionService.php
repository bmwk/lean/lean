<?php

declare(strict_types=1);

namespace App\Domain\LookingForPropertyRequest;

use App\Domain\Entity\LookingForPropertyRequest\PropertyRequirement;
use App\Domain\Entity\PropertyMandatoryRequirement;
use App\Domain\Entity\PropertyValueRangeRequirement;
use Doctrine\ORM\EntityManagerInterface;

class PropertyRequirementDeletionService
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function deletePropertyRequirement(PropertyRequirement $propertyRequirement): void
    {
        $this->hardDeletePropertyRequirement(propertyRequirement: $propertyRequirement);
    }

    private function hardDeletePropertyRequirement(PropertyRequirement $propertyRequirement): void
    {
        $this->entityManager->remove(entity: $propertyRequirement);

        $this->hardDeletePropertyMandatoryRequirement(
            propertyMandatoryRequirement: $propertyRequirement->getPropertyMandatoryRequirement()
        );

        $this->hardDeletePropertyValueRangeRequirement(
            propertyValueRangeRequirement: $propertyRequirement->getPropertyValueRangeRequirement()
        );

        $this->entityManager->flush();
    }

    private function hardDeletePropertyMandatoryRequirement(PropertyMandatoryRequirement $propertyMandatoryRequirement): void
    {
        $this->entityManager->remove($propertyMandatoryRequirement);
        $this->entityManager->flush();
    }

    private function hardDeletePropertyValueRangeRequirement(PropertyValueRangeRequirement $propertyValueRangeRequirement): void
    {
        $this->entityManager->remove(entity: $propertyValueRangeRequirement);
        $this->entityManager->remove(entity: $propertyValueRangeRequirement->getTotalSpace());
        $this->entityManager->remove(entity: $propertyValueRangeRequirement->getRetailSpace());
        $this->entityManager->remove(entity: $propertyValueRangeRequirement->getOutdoorSpace());
        $this->entityManager->remove(entity: $propertyValueRangeRequirement->getUsableSpace());
        $this->entityManager->remove(entity: $propertyValueRangeRequirement->getStorageSpace());
        $this->entityManager->remove(entity: $propertyValueRangeRequirement->getShopWindowLength());
        $this->entityManager->remove(entity: $propertyValueRangeRequirement->getShopWidth());
        $this->entityManager->remove(entity: $propertyValueRangeRequirement->getSubsidiarySpace());

        $this->entityManager->flush();
    }
}
