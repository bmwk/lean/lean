<?php

declare(strict_types=1);

namespace App\Domain\SettlementConcept\Matching;

use App\Domain\Classification\ClassificationService;
use App\Domain\Entity\Classification\IndustryClassification;
use App\Domain\Entity\EliminationCriterion;
use App\Domain\Entity\LocationCategory;
use App\Domain\Entity\MinMaxScoreCriterion;
use App\Domain\Entity\ParkingLotRequirementType;
use App\Domain\Entity\Property\BarrierFreeAccess;
use App\Domain\Entity\Property\BuildingUnit;
use App\Domain\Entity\PropertyOfferType;
use App\Domain\Entity\ScoreCriterion;
use App\Domain\Entity\SettlementConcept\SettlementConcept;
use App\Domain\Entity\SettlementConcept\SettlementConceptEliminationCriteria;
use App\Domain\Entity\SettlementConcept\SettlementConceptMatchingResult;
use App\Domain\Entity\SettlementConcept\SettlementConceptScoreCriteria;
use App\Domain\Entity\ValueRequirement;
use App\Domain\Matching\MatchingStrategyInterface;
use Doctrine\Common\Collections\Collection;

class SettlementConceptPointMatchingStrategy implements MatchingStrategyInterface
{
    public const RETAIL_SPACE_POINTS = 8;
    public const SECONDARY_SPACE_POINTS = 8;
    public const OUTDOOR_SALES_AREA = 4;
    public const STORE_WIDTH = 3;
    public const SHOP_WINDOW_LENGTH = 3;
    public const LOCATION_FACTORS_POINTS = 10;
    public const LOCATION_FACTORS_PENALTY_POINTS = 2;

    public function __construct(
        private readonly ClassificationService $classificationService
    ) {
    }

    /**
     * @param SettlementConcept[] $requests
     * @return SettlementConceptMatchingResult[]
     */
    public function doMatching(array $requests, BuildingUnit $buildingUnit): array
    {
        $results = [];

        foreach ($requests as $request) {
            $settlementConceptMatchingResult = new SettlementConceptMatchingResult();

            $results[] = $settlementConceptMatchingResult;

            $eliminationCriteria = $this->setupEliminationCriteria();

            $scoreCriteria = $this->setupScoreCriteria();

            $settlementConceptMatchingResult
                ->setSettlementConcept($request)
                ->setBuildingUnit($buildingUnit)
                ->setSettlementConceptEliminationCriteria($eliminationCriteria)
                ->setSettlementConceptScoreCriteria($scoreCriteria);

            $this->checkEliminationCriteria(
                eliminationCriteria: $eliminationCriteria,
                request: $request,
                buildingUnit: $buildingUnit
            );

            $settlementConceptMatchingResult->setEliminated($eliminationCriteria->isEliminated());

            if ($settlementConceptMatchingResult->isEliminated() === true) {
                continue;
            }

            $this->checkScoreCriteria(
                settlementConceptScoreCriteria: $scoreCriteria,
                request: $request,
                buildingUnit: $buildingUnit
            );

            $settlementConceptMatchingResult
                ->setPossibleMaxScore($scoreCriteria->calculateMaxPoints())
                ->setScore($scoreCriteria->calculatePoints());
        }

        return $results;
    }

    private function checkEliminationCriteria(
        SettlementConceptEliminationCriteria $eliminationCriteria,
        SettlementConcept $request,
        BuildingUnit $buildingUnit
    ): void {
        $this->evaluatePropertyOfferTypes(
            propertyOfferTypeCriterion: $eliminationCriteria->getPropertyOfferTypes(),
            settlementConceptPropertyOfferTypes: $request->getPropertyOfferTypes(),
            buildingUnitPropertyOfferType: $buildingUnit->getPropertyMarketingInformation()?->getPropertyOfferType()
        );

        if ($buildingUnit->getPropertySpacesData()->getRetailSpace() !== null) {
            $areaSize = $buildingUnit->getPropertySpacesData()->getRetailSpace();
        } else {
            $areaSize = $buildingUnit->getAreaSize();
        }

        $this->evaluateMinSpace(
            spaceCriterion: $eliminationCriteria->getMinSpace(),
            spaceValueRequirement: $request->getSettlementConceptPropertyRequirement()->getSpace(),
            areaSize: $areaSize
        );

        $this->evaluateMaxSpace(
            spaceCriterion: $eliminationCriteria->getMaxSpace(),
            spaceValueRequirement: $request->getSettlementConceptPropertyRequirement()->getSpace(),
            areaSize: $areaSize
        );

        $this->evaluateOutdoorSpaceRequired(
            outdoorSpaceRequiredEliminationCriterion: $eliminationCriteria->getOutdoorSpaceRequired(),
            outdoorSpaceRequired: $request->getSettlementConceptPropertyRequirement()->isOutdoorSalesAreaRequired(),
            outdoorSpace: $buildingUnit->getPropertySpacesData()?->getOutdoorSalesArea()
        );

        $this->evaluateGroundLevelSalesAreaRequired(
            groundLevelSalesAreaRequiredEliminationCriterion: $eliminationCriteria->getGroundLevelSalesAreaRequired(),
            groundLevelSalesAreaRequired: $request->getSettlementConceptPropertyRequirement()->isGroundLevelSalesAreaRequired(),
            buildingUnitGroundLevelSalesArea: $buildingUnit->isGroundLevelSalesArea()
        );

        $this->evaluateBarrierFreeAccessRequired(
            barrierFreeAccessRequiredEliminationCriterion: $eliminationCriteria->getBarrierFreeAccessRequired(),
            barrierFreeAccessRequired: $request->getSettlementConceptPropertyRequirement()->isBarrierFreeAccessRequired(),
            buildingUnitBarrierFreeAccess: $buildingUnit->getBarrierFreeAccess()
        );

        $this->evaluateLocationCategories(
            locationCategoriesEliminationCriterion: $eliminationCriteria->getLocationCategories(),
            locationCategories: $request->getSettlementConceptPropertyRequirement()->getLocationCategories(),
            buildingUnitLocationCategory: $buildingUnit->getBuilding()->getLocationCategory()
        );

        $this->evaluateParkingLotsRequired(
            parkingLotsRequiredEliminationCriterion: $eliminationCriteria->getParkingLotsRequired(),
            parkingLotsRequired: $request->getSettlementConceptPropertyRequirement()->getParkingLotsRequired(),
            numberOfParkingLots: $buildingUnit->getNumberOfParkingLots()
        );

        $this->evaluateAgeStructures(
            ageStructuresEliminationCriterion: $eliminationCriteria->getAgeStructures(),
            requestAgeStructures: $request->getAgeStructures(),
            buildingUnit: $buildingUnit
        );

        $this->evaluateIndustryClassification(
            industryClassificationEliminationCriterion: $eliminationCriteria->getIndustryClassification(),
            requestIndustryClassification: $request->getIndustryClassification(),
            possibleIndustryClassification: $buildingUnit->getPropertyMarketingInformation()?->getPossibleUsageIndustryClassifications()
        );
    }

    private function checkScoreCriteria(
        SettlementConceptScoreCriteria $settlementConceptScoreCriteria,
        SettlementConcept $request,
        BuildingUnit $buildingUnit
    ): void {
        if ($buildingUnit->getPropertySpacesData()->getRetailSpace() !== null) {
            $space = $buildingUnit->getPropertySpacesData()->getRetailSpace();
        } else {
            $space = $buildingUnit->getAreaSize();
        }

        $this->evaluateMinMaxCriterion(
            minMaxScoreCriterion: $settlementConceptScoreCriteria->getRetailSpace(),
            valueRequirement: $request->getSettlementConceptPropertyRequirement()->getSpace(),
            value: $space
        );

        $this->evaluateValueRequirement(
            scoreCriterion: $settlementConceptScoreCriteria->getSecondarySpace(),
            valueRequirement: $request->getSettlementConceptPropertyRequirement()->getSecondarySpace(),
            comparisonValue: $buildingUnit->getPropertySpacesData()->getSubsidiarySpace()
        );

        $this->evaluateValueRequirement(
            scoreCriterion: $settlementConceptScoreCriteria->getOutdoorSalesArea(),
            valueRequirement: $request->getSettlementConceptPropertyRequirement()->getOutdoorSalesArea(),
            comparisonValue: $buildingUnit->getPropertySpacesData()->getOutdoorSalesArea()
        );

        $this->evaluateValueRequirement(
            scoreCriterion: $settlementConceptScoreCriteria->getStoreWidth(),
            valueRequirement: $request->getSettlementConceptPropertyRequirement()->getStoreWidth(),
            comparisonValue: $buildingUnit->getShopWidth()
        );

        $this->evaluateValueRequirement(
            scoreCriterion: $settlementConceptScoreCriteria->getShopWindowLength(),
            valueRequirement: $request->getSettlementConceptPropertyRequirement()->getShopWindowLength(),
            comparisonValue: $buildingUnit->getShopWindowFrontWidth()
        );

        $this->evaluateLocationFactors(
            locationFactorsScoreCriterion: $settlementConceptScoreCriteria->getLocationFactors(),
            settlementConceptLocationFactors: $request->getSettlementConceptPropertyRequirement()->getLocationFactors(),
            locationFactorsBuildingUnit: $buildingUnit->getLocationFactors(),
            locationFactorsBuilding: $buildingUnit->getBuilding()->getLocationFactors()
        );
    }

    private function setupEliminationCriteria(): SettlementConceptEliminationCriteria
    {
        $eliminationCriteria = new SettlementConceptEliminationCriteria();

        $propertyOfferTypes = $this->createEliminationCriterion(name: 'propertyOfferTypes');
        $minSpace = $this->createEliminationCriterion(name: 'minSpace');
        $maxSpace = $this->createEliminationCriterion(name: 'maxSpace');
        $outdoorSpaceRequired = $this->createEliminationCriterion(name: 'outdoorSpaceRequired');
        $groundLevelSalesAreaRequired = $this->createEliminationCriterion(name: 'groundLevelSalesAreaRequired');
        $barrierFreeAccessRequired = $this->createEliminationCriterion(name: 'barrierFreeAccessRequired');
        $locationCategories = $this->createEliminationCriterion(name: 'locationCategories');
        $parkingLotsRequired = $this->createEliminationCriterion(name: 'parkingLotsRequired');
        $ageStructures = $this->createEliminationCriterion(name: 'ageStructures');
        $industryClassification = $this->createEliminationCriterion(name: 'industryClassification');

        $eliminationCriteria
            ->setPropertyOfferTypes($propertyOfferTypes)
            ->setMinSpace($minSpace)
            ->setMaxSpace($maxSpace)
            ->setOutdoorSpaceRequired($outdoorSpaceRequired)
            ->setGroundLevelSalesAreaRequired($groundLevelSalesAreaRequired)
            ->setBarrierFreeAccessRequired($barrierFreeAccessRequired)
            ->setLocationCategories($locationCategories)
            ->setParkingLotsRequired($parkingLotsRequired)
            ->setAgeStructures($ageStructures)
            ->setIndustryClassification($industryClassification);

        return $eliminationCriteria;
    }

    private function setupScoreCriteria(): SettlementConceptScoreCriteria
    {
        $scoreCriteria = new SettlementConceptScoreCriteria();

        $minRetailSpace = new ScoreCriterion();

        $minRetailSpace
            ->setName('minRetailSpace')
            ->setPoints(self::RETAIL_SPACE_POINTS);

        $maxRetailSpace = new ScoreCriterion();

        $maxRetailSpace
            ->setName('maxRetailSpace')
            ->setPoints(self::RETAIL_SPACE_POINTS);

        $retailSpace = new MinMaxScoreCriterion();

        $retailSpace
            ->setName('retailSpace')
            ->setMinScoreCriterion($minRetailSpace)
            ->setMaxScoreCriterion($maxRetailSpace);

        $secondarySpace = $this->createScoreCriterion(name: 'secondarySpace', points: self::SECONDARY_SPACE_POINTS);

        $outdoorSalesArea = $this->createScoreCriterion(name: 'outdoorSalesArea', points: self::OUTDOOR_SALES_AREA);

        $storeWidth = $this->createScoreCriterion(name: 'storeWidth', points: self::STORE_WIDTH);

        $shopWindowLength = $this->createScoreCriterion(name: 'shopWindowLength', points: self::SHOP_WINDOW_LENGTH);

        $locationFactors = $this->createScoreCriterion(name: 'locationFactors', points: self::LOCATION_FACTORS_POINTS);

        $scoreCriteria
            ->setRetailSpace($retailSpace)
            ->setSecondarySpace($secondarySpace)
            ->setOutdoorSalesArea($outdoorSalesArea)
            ->setStoreWidth($storeWidth)
            ->setShopWindowLength($shopWindowLength)
            ->setLocationFactors($locationFactors);

        return $scoreCriteria;
    }

    private function evaluatePropertyOfferTypes(
        EliminationCriterion $propertyOfferTypeCriterion,
        ?array $settlementConceptPropertyOfferTypes,
        ?PropertyOfferType $buildingUnitPropertyOfferType
    ): void {
        if ($settlementConceptPropertyOfferTypes === null) {
            $propertyOfferTypeCriterion->setSet(false);

            return;
        }

        $propertyOfferTypeCriterion->setSet(true);

        if ($buildingUnitPropertyOfferType === null) {
            return;
        }

        if (in_array($buildingUnitPropertyOfferType, $settlementConceptPropertyOfferTypes) === true) {
            $propertyOfferTypeCriterion->setEvaluationResult(true);
        }
    }

    private function evaluateMinSpace(
        EliminationCriterion $spaceCriterion,
        ValueRequirement $spaceValueRequirement,
        ?float $areaSize
    ): void {
        if ($spaceValueRequirement->getMinimumValue() === null) {
            $spaceCriterion->setSet(false);

            return;
        }

        $spaceCriterion->setSet(true);

        if ($areaSize === null) {
            return;
        }

        if (0.8 * $spaceValueRequirement->getMinimumValue() <= $areaSize) {
            $spaceCriterion->setEvaluationResult(true);
        }
    }

    private function evaluateMaxSpace(
        EliminationCriterion $spaceCriterion,
        ValueRequirement $spaceValueRequirement,
        ?float $areaSize
    ): void {
        if ($spaceValueRequirement->getMaximumValue() === null) {
            $spaceCriterion->setSet(false);

            return;
        }

        $spaceCriterion->setSet(true);

        if ($areaSize === null) {
            return;
        }

        if (1.2 * $spaceValueRequirement->getMaximumValue() >= $areaSize) {
            $spaceCriterion->setEvaluationResult(true);
        }
    }

    private function evaluateOutdoorSpaceRequired(
        EliminationCriterion $outdoorSpaceRequiredEliminationCriterion,
        bool $outdoorSpaceRequired,
        ?float $outdoorSpace
    ): void {
        if ($outdoorSpaceRequired === false) {
            $outdoorSpaceRequiredEliminationCriterion->setSet(false);

            return;
        }

        $outdoorSpaceRequiredEliminationCriterion->setSet(true);

        if ($outdoorSpace !== null) {
            $outdoorSpaceRequiredEliminationCriterion->setEvaluationResult(true);
        }
    }

    private function evaluateGroundLevelSalesAreaRequired(
        EliminationCriterion $groundLevelSalesAreaRequiredEliminationCriterion,
        bool $groundLevelSalesAreaRequired,
        ?bool $buildingUnitGroundLevelSalesArea
    ): void {
        if ($groundLevelSalesAreaRequired === false) {
            $groundLevelSalesAreaRequiredEliminationCriterion->setSet(false);

            return;
        }

        $groundLevelSalesAreaRequiredEliminationCriterion->setSet(true);

        if ($buildingUnitGroundLevelSalesArea === true) {
            $groundLevelSalesAreaRequiredEliminationCriterion->setEvaluationResult(true);
        }
    }

    private function evaluateBarrierFreeAccessRequired(
        EliminationCriterion $barrierFreeAccessRequiredEliminationCriterion,
        bool $barrierFreeAccessRequired,
        ?BarrierFreeAccess $buildingUnitBarrierFreeAccess
    ): void {
        if ($barrierFreeAccessRequired === false) {
            $barrierFreeAccessRequiredEliminationCriterion->setSet(false);

            return;
        }

        $barrierFreeAccessRequiredEliminationCriterion->setSet(true);

        if ($buildingUnitBarrierFreeAccess === null) {
            return;
        }

        if (
            $buildingUnitBarrierFreeAccess === BarrierFreeAccess::IS_AVAILABLE
            || $buildingUnitBarrierFreeAccess === BarrierFreeAccess::CAN_BE_GUARANTEED
        ) {
            $barrierFreeAccessRequiredEliminationCriterion->setEvaluationResult(true);
        }
    }

    private function evaluateLocationCategories(
        EliminationCriterion $locationCategoriesEliminationCriterion,
        ?array $locationCategories,
        ?LocationCategory $buildingUnitLocationCategory
    ): void {
        if ($locationCategories === [] || $locationCategories === null) {
            $locationCategoriesEliminationCriterion->setSet(false);

            return;
        }

        $locationCategoriesEliminationCriterion->setSet(true);

        if ($buildingUnitLocationCategory === null) {
            return;
        }

        if (in_array(needle: $buildingUnitLocationCategory, haystack: $locationCategories) === true) {
            $locationCategoriesEliminationCriterion->setEvaluationResult(true);
        }
    }

    private function evaluateParkingLotsRequired(
        EliminationCriterion $parkingLotsRequiredEliminationCriterion,
        ?ParkingLotRequirementType $parkingLotsRequired,
        ?int $numberOfParkingLots
    ): void {
        if ($parkingLotsRequired === null) {
            $parkingLotsRequiredEliminationCriterion->setSet(false);

            return;
        }

        $parkingLotsRequiredEliminationCriterion->setSet(true);

        if ($parkingLotsRequired === ParkingLotRequirementType::MANDATORY && $numberOfParkingLots < 1) {
            $parkingLotsRequiredEliminationCriterion->setEvaluationResult(evaluationResult: false);

            return;
        }

        $parkingLotsRequiredEliminationCriterion->setEvaluationResult(evaluationResult: true);
    }

    private function evaluateAgeStructures(
        EliminationCriterion $ageStructuresEliminationCriterion,
        ?array $requestAgeStructures,
        BuildingUnit $buildingUnit
    ): void {
        if ($requestAgeStructures === null || $requestAgeStructures ===  []) {
            $ageStructuresEliminationCriterion->setSet(false);

            return;
        }

        $ageStructuresEliminationCriterion
            ->setSet(true)
            ->setEvaluationResult(true);
    }

    private function evaluateIndustryClassification(
        EliminationCriterion $industryClassificationEliminationCriterion,
        ?IndustryClassification $requestIndustryClassification,
        ?Collection $possibleIndustryClassification
    ): void {
        if ($requestIndustryClassification === null) {
            $industryClassificationEliminationCriterion->setSet(false);

            return;
        }

        $industryClassificationEliminationCriterion->setSet(true);

        if ($possibleIndustryClassification === null) {
            return;
        }

        if ($possibleIndustryClassification->isEmpty() === true) {
            return;
        }

        $parentRequestIndustryClassification = $this->classificationService->getTopMostParentFromIndustryClassification(
            industryClassification: $requestIndustryClassification
        );

        if ($possibleIndustryClassification->contains($parentRequestIndustryClassification)) {
            $industryClassificationEliminationCriterion->setEvaluationResult(true);
        }
    }

    private function evaluateValueRequirement(
        ScoreCriterion $scoreCriterion,
        ValueRequirement $valueRequirement,
        ?float $comparisonValue
    ): void {
        if ($valueRequirement->getMaximumValue() === null && $valueRequirement->getMinimumValue() === null) {
            $scoreCriterion->setSet(false);

            return;
        }

        $scoreCriterion->setSet(true);

        if ($valueRequirement->getMinimumValue() !== null && $valueRequirement->getMinimumValue() > $comparisonValue) {
            $scoreCriterion->setEvaluationResult(false);

            return;
        }

        if ($valueRequirement->getMaximumValue() !== null && $valueRequirement->getMaximumValue() < $comparisonValue) {
            $scoreCriterion->setEvaluationResult(false);

            return;
        }

        $scoreCriterion->setEvaluationResult(true);
    }

    private function evaluateMinMaxCriterion(
        MinMaxScoreCriterion $minMaxScoreCriterion,
        ValueRequirement $valueRequirement,
        ?float $value
    ): void {
        $minScoreCriterion = $minMaxScoreCriterion->getMinScoreCriterion();
        $maxScoreCriterion = $minMaxScoreCriterion->getMaxScoreCriterion();
        $minScoreCriterion->setSet($valueRequirement->getMinimumValue() !== null);
        $maxScoreCriterion->setSet($valueRequirement->getMaximumValue() !== null);

        if ($minScoreCriterion->isSet() === false && $maxScoreCriterion->isSet() === false) {
            return;
        }

        if ($value === null) {
            return;
        }

        if ($minScoreCriterion->isSet() && $value >= $valueRequirement->getMinimumValue()) {
            $minScoreCriterion->setEvaluationResult(evaluationResult: true);
        }

        if ($maxScoreCriterion->isSet() && $value <= $valueRequirement->getMaximumValue()) {
            $maxScoreCriterion->setEvaluationResult(evaluationResult: true);
        }
    }

    private function evaluateLocationFactors(
        ScoreCriterion $locationFactorsScoreCriterion,
        ?array $settlementConceptLocationFactors,
        ?array $locationFactorsBuildingUnit,
        ?array $locationFactorsBuilding
    ): void {
        if ($settlementConceptLocationFactors === null || $settlementConceptLocationFactors === []) {
            $locationFactorsScoreCriterion->setSet(false);

            return;
        }

        $locationFactorsScoreCriterion->setSet(true);
        $locationFactors = [];

        if ($locationFactorsBuildingUnit !== null && $locationFactorsBuilding === null) {
            $locationFactors = $locationFactorsBuildingUnit;
        }

        if ($locationFactorsBuildingUnit === null && $locationFactorsBuilding !== null) {
            $locationFactors = $locationFactorsBuilding;
        }

        if ($locationFactorsBuildingUnit !== null && $locationFactorsBuilding !== null) {
            $locationFactors = $locationFactorsBuildingUnit + $locationFactorsBuilding;
        }

        $intersection = [];

        foreach ($settlementConceptLocationFactors as $settlementConceptLocationFactor) {
            if (in_array($settlementConceptLocationFactor, $locationFactors)) {
                $intersection[] = $settlementConceptLocationFactor;
            }
        }

        if (count($intersection) !== 0) {
            $penaltyPoints = (count($settlementConceptLocationFactors) - count($intersection)) * self::LOCATION_FACTORS_PENALTY_POINTS;
            $locationFactorsScoreCriterion
                ->setEvaluationResult(true)
                ->setPointPenalty($penaltyPoints);
        }
    }

    private function createEliminationCriterion(string $name): EliminationCriterion
    {
        $eliminationCriterion = new EliminationCriterion();

        $eliminationCriterion->setName($name);

        return $eliminationCriterion;
    }

    private function createScoreCriterion(string $name, int $points = 1): ScoreCriterion
    {
        $scoreCriterion = new ScoreCriterion();

        $scoreCriterion
            ->setName($name)
            ->setPoints($points);

        return $scoreCriterion;
    }
}
