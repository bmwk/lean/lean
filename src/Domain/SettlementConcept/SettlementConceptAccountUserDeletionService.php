<?php

declare(strict_types=1);

namespace App\Domain\SettlementConcept;

use App\Domain\Entity\Account;
use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\DeletionType;
use App\Domain\Entity\SettlementConcept\SettlementConceptAccountUser;
use App\Domain\Entity\SettlementConcept\SettlementConceptAuthenticationTokenGenerationRequest;
use App\Repository\SettlementConcept\SettlementConceptAccountUserRepository;
use App\Repository\SettlementConcept\SettlementConceptAuthenticationTokenGenerationRequestRepository;
use Doctrine\ORM\EntityManagerInterface;

class SettlementConceptAccountUserDeletionService
{
    public function __construct(
        private readonly SettlementConceptAuthenticationTokenGenerationRequestRepository $settlementConceptAuthenticationTokenGenerationRequestRepository,
        private readonly SettlementConceptAccountUserRepository $settlementConceptAccountUserRepository,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function deleteSettlementConceptAccountUser(
        SettlementConceptAccountUser $settlementConceptAccountUser,
        DeletionType $deletionType = DeletionType::SOFT,
        ?AccountUser $deletedByAccountUser = null
    ): void {
        switch ($deletionType) {
            case DeletionType::SOFT:
                $this->softDeleteSettlementConceptAccountUser(
                    settlementConceptAccountUser: $settlementConceptAccountUser,
                    deletedByAccountUser: $deletedByAccountUser
                );
                break;
            case DeletionType::HARD:
                $this->hardDeleteSettlementConceptAccountUser(settlementConceptAccountUser: $settlementConceptAccountUser);
                break;
            default:
                throw new \RuntimeException(message: 'Unsupported deletion type');
        }
    }

    public function deleteSettlementConceptAccountUsersByAccount(
        Account $account,
        DeletionType $deletionType = DeletionType::SOFT,
        ?AccountUser $deletedByAccountUser = null
    ): void {
        switch ($deletionType) {
            case DeletionType::SOFT:
                $this->softDeleteSettlementConceptAccountUsersByAccount(account: $account, deletedByAccountUser: $deletedByAccountUser);
                break;
            case DeletionType::HARD:
                $this->hardDeleteSettlementConceptAccountUsersByAccount(account: $account);
                break;
            default:
                throw new \RuntimeException(message: 'Unsupported deletion type');
        }
    }

    public function deleteSettlementConceptAuthenticationTokenGenerationRequest(
        SettlementConceptAuthenticationTokenGenerationRequest $settlementConceptAuthenticationTokenGenerationRequest
    ): void {
        $this->hardDeleteSettlementConceptAuthenticationTokenGenerationRequest(
            settlementConceptAuthenticationTokenGenerationRequest: $settlementConceptAuthenticationTokenGenerationRequest
        );
    }

    private function softDeleteSettlementConceptAccountUser(
        SettlementConceptAccountUser $settlementConceptAccountUser,
        AccountUser $deletedByAccountUser
    ): void {
        $settlementConceptAccountUser
            ->setDeleted(deleted: true)
            ->setDeletedByAccountUser(deletedByAccountUser: $deletedByAccountUser)
            ->setDeletedAt(deletedAt: new \DateTimeImmutable());

        $this->entityManager->flush();
    }

    private function hardDeleteSettlementConceptAccountUser(SettlementConceptAccountUser $settlementConceptAccountUser): void
    {
        $settlementConceptAuthenticationTokenGenerationRequests = $this->settlementConceptAuthenticationTokenGenerationRequestRepository->findBy(
            criteria: ['settlementConceptAccountUser' => $settlementConceptAccountUser]
        );

        foreach ($settlementConceptAuthenticationTokenGenerationRequests as $settlementConceptAuthenticationTokenGenerationRequest) {
            $this->deleteSettlementConceptAuthenticationTokenGenerationRequest(
                settlementConceptAuthenticationTokenGenerationRequest: $settlementConceptAuthenticationTokenGenerationRequest
            );
        }

        $this->entityManager->remove(entity: $settlementConceptAccountUser);
        $this->entityManager->flush();
    }

    private function softDeleteSettlementConceptAccountUsersByAccount(Account $account, AccountUser $deletedByAccountUser): void
    {
        $settlementConceptAccountUsers = $this->settlementConceptAccountUserRepository->findBy(criteria: ['account' => $account]);

        foreach ($settlementConceptAccountUsers as $settlementConceptAccountUser) {
            $this->softDeleteSettlementConceptAccountUser(
                settlementConceptAccountUser: $settlementConceptAccountUser,
                deletedByAccountUser: $deletedByAccountUser
            );
        }
    }

    private function hardDeleteSettlementConceptAccountUsersByAccount(Account $account): void
    {
        $settlementConceptAccountUsers = $this->settlementConceptAccountUserRepository->findBy(criteria: ['account' => $account]);

        foreach ($settlementConceptAccountUsers as $settlementConceptAccountUser) {
            $this->hardDeleteSettlementConceptAccountUser(settlementConceptAccountUser: $settlementConceptAccountUser);
        }
    }

    private function hardDeleteSettlementConceptAuthenticationTokenGenerationRequest(
        SettlementConceptAuthenticationTokenGenerationRequest $settlementConceptAuthenticationTokenGenerationRequest
    ): void {
        $this->entityManager->remove($settlementConceptAuthenticationTokenGenerationRequest);
        $this->entityManager->flush();
    }
}
