<?php

declare(strict_types=1);

namespace App\Domain\SettlementConcept;

use App\Domain\Entity\SettlementConcept\SettlementConcept;
use App\Domain\Entity\SettlementConcept\SettlementConceptExposeForwarding;
use App\Repository\SettlementConcept\SettlementConceptExposeForwardingRepository;
use Doctrine\ORM\EntityManagerInterface;

class SettlementConceptExposeForwardingDeletionService
{
    public function __construct(
        private readonly SettlementConceptExposeForwardingRepository $settlementConceptExposeForwardingRepository,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function deleteSettlementConceptExposeForwarding(SettlementConceptExposeForwarding $settlementConceptExposeForwarding): void
    {
        $this->hardDeleteSettlementConceptExposeForwarding(settlementConceptExposeForwarding: $settlementConceptExposeForwarding);
    }

    public function deleteSettlementConceptExposeForwardingsBySettlementConcept(SettlementConcept $settlementConcept): void
    {
        $this->hardDeleteSettlementConceptExposeForwardingsBySettlementConcept(settlementConcept: $settlementConcept);
    }

    private function hardDeleteSettlementConceptExposeForwarding(SettlementConceptExposeForwarding $settlementConceptExposeForwarding): void
    {
        $this->entityManager->remove(entity: $settlementConceptExposeForwarding);
        $this->entityManager->flush();
    }

    private function hardDeleteSettlementConceptExposeForwardingsBySettlementConcept(SettlementConcept $settlementConcept): void
    {
        $settlementConceptExposeForwardings = $this->settlementConceptExposeForwardingRepository->findBy(
            criteria: ['settlementConcept' => $settlementConcept]
        );

        foreach ($settlementConceptExposeForwardings as $settlementConceptExposeForwarding) {
            $this->hardDeleteSettlementConceptExposeForwarding(settlementConceptExposeForwarding: $settlementConceptExposeForwarding);
        }
    }
}
