<?php

declare(strict_types=1);

namespace App\Domain\SettlementConcept;

use App\Domain\Entity\Account;
use App\Domain\Entity\SettlementConcept\SettlementConcept;
use App\Domain\Entity\SettlementConcept\SettlementConceptPropertyRequirement;
use App\Repository\SettlementConcept\SettlementConceptRepository;
use Doctrine\ORM\EntityManagerInterface;

class SettlementConceptDeletionService
{
    public function __construct(
        private readonly SettlementConceptExposeForwardingDeletionService $settlementConceptExposeForwardingDeletionService,
        private readonly SettlementConceptMatchingResponseDeletionService $settlementConceptMatchingResponseDeletionService,
        private readonly SettlementConceptMatchingResultDeletionService $settlementConceptMatchingResultDeletionService,
        private readonly SettlementConceptRepository $settlementConceptRepository,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function deleteSettlementConcept(SettlementConcept $settlementConcept): void
    {
        $this->hardDeleteSettlementConcept(settlementConcept: $settlementConcept);
    }

    public function deleteSettlementConceptByAccounts(Account $account): void
    {
        $this->hardDeleteSettlementConceptsByAccounts(account: $account);
    }

    private function hardDeleteSettlementConcept(SettlementConcept $settlementConcept): void
    {
        $this->settlementConceptExposeForwardingDeletionService->deleteSettlementConceptExposeForwardingsBySettlementConcept(
            settlementConcept: $settlementConcept
        );

        $this->settlementConceptMatchingResponseDeletionService->deleteSettlementConceptMatchingResponsesBySettlementConcept(
            settlementConcept: $settlementConcept
        );

        $this->settlementConceptMatchingResultDeletionService->deleteSettlementConceptMatchingResultsBySettlementConcept(
            settlementConcept: $settlementConcept
        );

        $this->entityManager->remove(entity: $settlementConcept);

        $this->hardDeleteSettlementConceptPropertyRequirement(
            settlementConceptPropertyRequirement: $settlementConcept->getSettlementConceptPropertyRequirement()
        );

        $this->entityManager->flush();
    }

    private function hardDeleteSettlementConceptsByAccounts(Account $account): void
    {
        $settlementConcepts = $this->settlementConceptRepository->findBy(criteria: ['account' => $account]);

        foreach ($settlementConcepts as $settlementConcept) {
            $this->hardDeleteSettlementConcept(settlementConcept: $settlementConcept);
        }
    }

    private function hardDeleteSettlementConceptPropertyRequirement(
        SettlementConceptPropertyRequirement $settlementConceptPropertyRequirement
    ): void {
        $this->entityManager->remove(entity: $settlementConceptPropertyRequirement->getSpace());
        $this->entityManager->remove(entity: $settlementConceptPropertyRequirement->getSecondarySpace());
        $this->entityManager->remove(entity: $settlementConceptPropertyRequirement->getOutdoorSalesArea());
        $this->entityManager->remove(entity: $settlementConceptPropertyRequirement->getStoreWidth());
        $this->entityManager->remove(entity: $settlementConceptPropertyRequirement->getShopWindowLength());

        $this->entityManager->remove(entity: $settlementConceptPropertyRequirement);
        $this->entityManager->flush();
    }

}
