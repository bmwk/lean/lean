<?php

declare(strict_types=1);

namespace App\Domain\SettlementConcept;

use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\AccountUser\AccountUserRole;
use App\Domain\Entity\SettlementConcept\SettlementConceptAccountUser;
use Symfony\Bundle\SecurityBundle\Security;

class SettlementConceptAccountUserSecurityService
{
    public function __construct(
        protected readonly Security $security
    ) {
    }

    public function canViewSettlementConceptAccountUser(
        SettlementConceptAccountUser $settlementConceptAccountUser,
        AccountUser $accountUser
    ): bool {
        $account = $accountUser->getAccount();

        if (
            $account->getParentAccount() === null
            && $settlementConceptAccountUser->getAccount() === $account
            && (
                $this->security->isGranted(attributes: AccountUserRole::ROLE_ACCOUNT_ADMIN->value) === true
                || $this->security->isGranted(attributes: AccountUserRole::ROLE_VIEWER->value) === true
            )
        ) {
            return true;
        }

        return false;
    }

    public function canEditSettlementConceptAccountUser(
        SettlementConceptAccountUser $settlementConceptAccountUser,
        AccountUser $accountUser
    ): bool {
        $account = $accountUser->getAccount();

        if (
            $account->getParentAccount() === null
            && $settlementConceptAccountUser->getAccount() === $account
            && $this->security->isGranted(attributes: AccountUserRole::ROLE_ACCOUNT_ADMIN->value) === true
        ) {
            return true;
        }

        return false;
    }

    public function canDeleteSettlementConceptAccountUser(
        SettlementConceptAccountUser $settlementConceptAccountUser,
        AccountUser $accountUser
    ): bool {
        return $this->canEditSettlementConceptAccountUser(
            settlementConceptAccountUser: $settlementConceptAccountUser,
            accountUser: $accountUser
        );
    }
}
