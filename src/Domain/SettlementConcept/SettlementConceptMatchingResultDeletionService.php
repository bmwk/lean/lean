<?php

declare(strict_types=1);

namespace App\Domain\SettlementConcept;

use App\Domain\Entity\Property\BuildingUnit;
use App\Domain\Entity\SettlementConcept\SettlementConcept;
use App\Domain\Entity\SettlementConcept\SettlementConceptEliminationCriteria;
use App\Domain\Entity\SettlementConcept\SettlementConceptMatchingResult;
use App\Domain\Entity\SettlementConcept\SettlementConceptScoreCriteria;
use App\Repository\SettlementConcept\SettlementConceptMatchingResultRepository;
use Doctrine\ORM\EntityManagerInterface;

class SettlementConceptMatchingResultDeletionService
{
    public function __construct(
        private readonly SettlementConceptMatchingResultRepository $settlementConceptMatchingResultRepository,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function deleteSettlementConceptMatchingResult(SettlementConceptMatchingResult $settlementConceptMatchingResult): void
    {
        $this->hardDeleteSettlementConceptMatchingResult(settlementConceptMatchingResult: $settlementConceptMatchingResult);
    }

    public function deleteSettlementConceptMatchingResultsByBuildingUnit(BuildingUnit $buildingUnit): void
    {
        $this->hardDeleteSettlementConceptMatchingResultsByBuildingUnit(buildingUnit: $buildingUnit);
    }

    public function deleteSettlementConceptMatchingResultsBySettlementConcept(SettlementConcept $settlementConcept): void
    {
        $this->hardDeleteSettlementConceptMatchingResultsBySettlementConcept(settlementConcept: $settlementConcept);
    }

    private function hardDeleteSettlementConceptMatchingResult(SettlementConceptMatchingResult $settlementConceptMatchingResult): void
    {
        $settlementConceptEliminationCriteria = $settlementConceptMatchingResult->getSettlementConceptEliminationCriteria();
        $settlementConceptScoreCriteria = $settlementConceptMatchingResult->getSettlementConceptScoreCriteria();

        $this->entityManager->remove(entity: $settlementConceptMatchingResult);
        $this->entityManager->flush();

        $this->hardDeleteSettlementConceptEliminationCriteria(settlementConceptEliminationCriteria: $settlementConceptEliminationCriteria);
        $this->hardDeleteSettlementConceptScoreCriteria(settlementConceptScoreCriteria: $settlementConceptScoreCriteria);
    }

    private function hardDeleteSettlementConceptMatchingResultsByBuildingUnit(BuildingUnit $buildingUnit): void
    {
        $settlementConceptMatchingResults = $this->settlementConceptMatchingResultRepository->findBy(
            criteria: ['buildingUnit' => $buildingUnit]
        );

        foreach ($settlementConceptMatchingResults as $settlementConceptMatchingResult) {
            $this->hardDeleteSettlementConceptMatchingResult(settlementConceptMatchingResult: $settlementConceptMatchingResult);
        }
    }

    private function hardDeleteSettlementConceptMatchingResultsBySettlementConcept(SettlementConcept $settlementConcept): void
    {
        $settlementConceptMatchingResults = $this->settlementConceptMatchingResultRepository->findBy(
            criteria: ['settlementConcept' => $settlementConcept]
        );

        foreach ($settlementConceptMatchingResults as $settlementConceptMatchingResult) {
            $this->hardDeleteSettlementConceptMatchingResult(settlementConceptMatchingResult: $settlementConceptMatchingResult);
        }
    }

    private function hardDeleteSettlementConceptEliminationCriteria(
        SettlementConceptEliminationCriteria $settlementConceptEliminationCriteria
    ): void {
        $eliminationCriteriaToDelete = [
            $settlementConceptEliminationCriteria->getPropertyOfferTypes(),
            $settlementConceptEliminationCriteria->getMinSpace(),
            $settlementConceptEliminationCriteria->getMaxSpace(),
            $settlementConceptEliminationCriteria->getOutdoorSpaceRequired(),
            $settlementConceptEliminationCriteria->getGroundLevelSalesAreaRequired(),
            $settlementConceptEliminationCriteria->getBarrierFreeAccessRequired(),
            $settlementConceptEliminationCriteria->getLocationCategories(),
            $settlementConceptEliminationCriteria->getParkingLotsRequired(),
            $settlementConceptEliminationCriteria->getAgeStructures(),
            $settlementConceptEliminationCriteria->getIndustryClassification()
        ];

        $this->entityManager->remove(entity: $settlementConceptEliminationCriteria);
        $this->entityManager->flush();

        foreach ($eliminationCriteriaToDelete as $eliminationCriterionToDelete) {
            $this->entityManager->remove(entity: $eliminationCriterionToDelete);
        }

        $this->entityManager->flush();
    }

    private function hardDeleteSettlementConceptScoreCriteria(SettlementConceptScoreCriteria $settlementConceptScoreCriteria): void
    {
        $scoreCriteriaToDelete = [
            $settlementConceptScoreCriteria->getSecondarySpace(),
            $settlementConceptScoreCriteria->getOutdoorSalesArea(),
            $settlementConceptScoreCriteria->getStoreWidth(),
            $settlementConceptScoreCriteria->getShopWindowLength(),
            $settlementConceptScoreCriteria->getLocationFactors(),
            $settlementConceptScoreCriteria->getRetailSpace()
        ];

        $this->entityManager->remove(entity: $settlementConceptScoreCriteria);
        $this->entityManager->flush();

        foreach ($scoreCriteriaToDelete as $scoreCriterionToDelete) {
            $this->entityManager->remove(entity: $scoreCriterionToDelete);
        }

        $this->entityManager->flush();
    }
}
