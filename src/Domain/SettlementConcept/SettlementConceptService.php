<?php

declare(strict_types=1);

namespace App\Domain\SettlementConcept;

use App\Domain\Classification\ClassificationService;
use App\Domain\Entity\Account;
use App\Domain\Entity\AgeStructure;
use App\Domain\Entity\ForwardingResponse;
use App\Domain\Entity\LocationCategory;
use App\Domain\Entity\LocationFactor;
use App\Domain\Entity\PriceSegmentType;
use App\Domain\Entity\Property\BuildingUnit;
use App\Domain\Entity\PropertyOfferType;
use App\Domain\Entity\SettlementConcept\SettlementConcept;
use App\Domain\Entity\SettlementConcept\SettlementConceptAccountUser;
use App\Domain\Entity\SettlementConcept\SettlementConceptExposeForwarding;
use App\Domain\Entity\SettlementConcept\SettlementConceptPropertyRequirement;
use App\Domain\Entity\ValueRequirement;
use App\Domain\SettlementConcept\Api\Entity\Request\SettlementConceptPropertyRequirementRequest;
use App\Domain\SettlementConcept\Api\Entity\Request\SettlementConceptRequest;
use App\Domain\SettlementConcept\Api\Entity\Request\ValueRequirementPostRequest;
use App\Repository\SettlementConcept\SettlementConceptExposeForwardingRepository;
use App\Repository\SettlementConcept\SettlementConceptRepository;
use Doctrine\ORM\EntityManagerInterface;

class SettlementConceptService
{
    public function __construct(
        private readonly ClassificationService $classificationService,
        private readonly SettlementConceptRepository $settlementConceptRepository,
        private readonly SettlementConceptExposeForwardingRepository $settlementConceptExposeForwardingRepository,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    /**
     * @return SettlementConcept[]
     */
    public function fetchSettlementConceptsByAccountUser(SettlementConceptAccountUser $settlementConceptAccountUser): array
    {
        return $this->settlementConceptRepository->findBy(criteria: [
            'settlementConceptAccountUser' => $settlementConceptAccountUser,
        ]);
    }

    public function fetchSettlementConceptByIdAndSettlementConceptAccountUser(
        int $id,
        SettlementConceptAccountUser $settlementConceptAccountUser
    ): ?SettlementConcept {
        return $this->settlementConceptRepository->findOneBy(criteria: [
            'id' => $id,
            'settlementConceptAccountUser' => $settlementConceptAccountUser,
        ]);
    }

    /**
     * @return SettlementConcept[]
     */
    public function fetchSettlementConcepts(
        Account $account,
        bool $fromDeletedSettlementConceptAccountUser,
        bool $fromDisabledSettlementConceptAccountUser,
        ?SettlementConceptAccountUser $settlementConceptAccountUser = null
    ): array {
        return $this->settlementConceptRepository->findByAccount(
            account: $account,
            fromDeletedSettlementConceptAccountUser: $fromDeletedSettlementConceptAccountUser,
            fromDisabledSettlementConceptAccountUser: $fromDisabledSettlementConceptAccountUser,
            settlementConceptAccountUser: $settlementConceptAccountUser
        );
    }

    public function fetchSettlementConceptById(
        Account $account,
        int $id,
        bool $fromDeletedSettlementConceptAccountUser,
        bool $fromDisabledSettlementConceptAccountUser,
        ?SettlementConceptAccountUser $settlementConceptAccountUser = null
    ): ?SettlementConcept {
        return $this->settlementConceptRepository->findOneById(
            account: $account,
            id: $id,
            fromDeletedSettlementConceptAccountUser: $fromDeletedSettlementConceptAccountUser,
            fromDisabledSettlementConceptAccountUser: $fromDisabledSettlementConceptAccountUser,
            settlementConceptAccountUser: $settlementConceptAccountUser
        );
    }

    /**
     * @return SettlementConceptExposeForwarding[]
     */
    public function fetchSettlementConceptExposeForwardingsBySettlementConcept(SettlementConcept $settlementConcept): array
    {
        return $this->settlementConceptExposeForwardingRepository->findBy(criteria: ['settlementConcept' => $settlementConcept]);
    }

    public function fetchSettlementConceptExposeForwardingByIdAndSettlementConceptAccountUser(
        int $id,
        SettlementConceptAccountUser $settlementConceptAccountUser
    ): ?SettlementConceptExposeForwarding {
        return $this->settlementConceptExposeForwardingRepository->findOneByIdAndSettlementConceptAccountUser(
            id: $id,
            settlementConceptAccountUser: $settlementConceptAccountUser
        );
    }

    public function fetchSettlementConceptExposeForwardingBySettlementConceptAndBuildingUnit(
        Account $account,
        SettlementConcept $settlementConcept,
        BuildingUnit $buildingUnit
    ): ?SettlementConceptExposeForwarding {
        return $this->settlementConceptExposeForwardingRepository->findBySettlementConceptAndBuildingUnit(
            account: $account,
            settlementConcept: $settlementConcept,
            buildingUnit: $buildingUnit
        );
    }

    public function createSettlementConceptFromPostRequest(
        SettlementConceptRequest $settlementConceptPostRequest,
        SettlementConceptAccountUser $settlementConceptAccountUser
    ): SettlementConcept {
        $settlementConcept = new SettlementConcept();
        $ageStructures = [];
        $propertyOfferTypes = [];

        foreach ($settlementConceptPostRequest->getAgeStructures() as $ageStructure) {
            $ageStructures[] = AgeStructure::from($ageStructure);
        }

        foreach ($settlementConceptPostRequest->getPropertyOfferTypes() as $propertyOfferType) {
            $propertyOfferTypes[] = PropertyOfferType::from($propertyOfferType);
        }

        if ($settlementConceptPostRequest->getNaceClassificationId() !== null) {
            $naceClassification = $this->classificationService->fetchNaceClassificationById(
                naceClassificationId: $settlementConceptPostRequest->getNaceClassificationId()
            );

            $settlementConcept->setNaceClassification($naceClassification);
        }

        $industryClassification = $this->classificationService->fetchIndustryClassificationById(
            id: $settlementConceptPostRequest->getIndustryClassificationId()
        );

        $settlementConceptPropertyRequirement = self::createSettlementConceptPropertyRequirementFromSettlementConceptPropertyRequirementRequest(
            settlementConceptPropertyRequirementPostRequest: $settlementConceptPostRequest->getSettlementConceptPropertyRequirementRequest()
        );

        $priceSegments = [];

        foreach ($settlementConceptPostRequest->getPriceSegments() as $priceSegment) {
            $priceSegments[] = PriceSegmentType::from($priceSegment);
        }

        $settlementConcept
            ->setSettlementConceptPropertyRequirement($settlementConceptPropertyRequirement)
            ->setAccount($settlementConceptAccountUser->getAccount())
            ->setAgeStructures($ageStructures)
            ->setPriceSegments($priceSegments)
            ->setPropertyOfferTypes($propertyOfferTypes)
            ->setIndustryClassification($industryClassification)
            ->setDescription($settlementConceptPostRequest->getDescription())
            ->setExternalReferenceNumber($settlementConceptPostRequest->getExternalReferenceNumber())
            ->setSettlementConceptAccountUser($settlementConceptAccountUser);

        return $settlementConcept;
    }

    public function countSettlementConceptExposeForwardingsByAccount(Account $account): int
    {
        return $this->settlementConceptExposeForwardingRepository->countByAccount(account: $account);
    }

    public function countSettlementConceptExposeForwardingsByAccountAndForwardingResponse(
        Account $account,
        ForwardingResponse $forwardingResponse
    ): int {
        return $this->settlementConceptExposeForwardingRepository->countByAccountAndForwardingResponse(
            account: $account,
            forwardingResponse: $forwardingResponse
        );
    }

    public function updateSettlementConceptWithSettlementConceptRequest(
        SettlementConcept $settlementConcept,
        SettlementConceptRequest $settlementConceptRequest
    ): void {
        $industryClassification = $this->classificationService->fetchIndustryClassificationById(
            id: $settlementConceptRequest->getIndustryClassificationId()
        );

        $naceClassification = null;

        if ($settlementConceptRequest->getNaceClassificationId() !== null) {
            $naceClassification = $this->classificationService->fetchNaceClassificationById(
                naceClassificationId: $settlementConceptRequest->getNaceClassificationId()
            );
        }

        $propertyOfferTypes = [];

        foreach ($settlementConceptRequest->getPropertyOfferTypes() as $propertyOfferType) {
            $propertyOfferTypes[] = PropertyOfferType::from($propertyOfferType);
        }

        $ageStructures = [];

        if ($settlementConceptRequest->getAgeStructures() !== null) {
            foreach ($settlementConceptRequest->getAgeStructures() as $ageStructure) {
                $ageStructures[] = AgeStructure::from($ageStructure);
            }
        }

        $priceSegments = [];

        if ($settlementConceptRequest->getPriceSegments() !== null) {
            foreach ($settlementConceptRequest->getPriceSegments() as $priceSegment) {
                $priceSegments[] = PriceSegmentType::from($priceSegment);
            }
        }

        $settlementConceptPropertyRequirement = $this->updateSettlementConceptPropertyRequirementFromSettlementConceptPropertyRequirementRequest(
            settlementConceptPropertyRequirement: $settlementConcept->getSettlementConceptPropertyRequirement(),
            settlementConceptPropertyRequirementRequest: $settlementConceptRequest->getSettlementConceptPropertyRequirementRequest()
        );

        $settlementConcept
            ->setIndustryClassification($industryClassification)
            ->setNaceClassification($naceClassification)
            ->setPropertyOfferTypes($propertyOfferTypes)
            ->setAgeStructures($ageStructures)
            ->setPriceSegments($priceSegments)
            ->setDescription($settlementConceptRequest->getDescription())
            ->setExternalReferenceNumber($settlementConceptRequest->getExternalReferenceNumber())
            ->setSettlementConceptPropertyRequirement($settlementConceptPropertyRequirement);
    }

    public function updateSettlementConceptPropertyRequirementFromSettlementConceptPropertyRequirementRequest(
        SettlementConceptPropertyRequirement $settlementConceptPropertyRequirement,
        SettlementConceptPropertyRequirementRequest $settlementConceptPropertyRequirementRequest
    ): SettlementConceptPropertyRequirement {
        self::updateValueRequirement(
            valueRequirement: $settlementConceptPropertyRequirement->getShopWindowLength(),
            minValue: $settlementConceptPropertyRequirementRequest->getShopWindowLength()->getMinimumValue(),
            maxValue: $settlementConceptPropertyRequirementRequest->getShopWindowLength()->getMaximumValue()
        );

        self::updateValueRequirement(
            valueRequirement: $settlementConceptPropertyRequirement->getOutdoorSalesArea(),
            minValue: $settlementConceptPropertyRequirementRequest->getOutdoorSalesArea()->getMinimumValue(),
            maxValue: $settlementConceptPropertyRequirementRequest->getOutdoorSalesArea()->getMaximumValue()
        );

        self::updateValueRequirement(
            valueRequirement: $settlementConceptPropertyRequirement->getStoreWidth(),
            minValue: $settlementConceptPropertyRequirementRequest->getStoreWidth()->getMinimumValue(),
            maxValue: $settlementConceptPropertyRequirementRequest->getStoreWidth()->getMaximumValue()
        );

        self::updateValueRequirement(
            valueRequirement: $settlementConceptPropertyRequirement->getSpace(),
            minValue: $settlementConceptPropertyRequirementRequest->getSpace()->getMinimumValue(),
            maxValue: $settlementConceptPropertyRequirementRequest->getSpace()->getMaximumValue()
        );

        self::updateValueRequirement(
            valueRequirement: $settlementConceptPropertyRequirement->getSecondarySpace(),
            minValue: $settlementConceptPropertyRequirementRequest->getSpace()->getMinimumValue(),
            maxValue: $settlementConceptPropertyRequirementRequest->getSpace()->getMaximumValue()
        );

        $locationFactors = null;

        if ($settlementConceptPropertyRequirementRequest->getLocationFactors() !== null) {
            $locationFactors = [];

            foreach ($settlementConceptPropertyRequirementRequest->getLocationFactors() as $locationFactor) {
                $locationFactors[] = LocationFactor::from($locationFactor);
            }
        }

        $locationCategories = null;

        if ($settlementConceptPropertyRequirementRequest->getLocationCategories() !== null) {
            $locationCategories = [];

            foreach ($settlementConceptPropertyRequirementRequest->getLocationCategories() as $locationCategory) {
                $locationCategories[] = LocationCategory::from($locationCategory);
            }
        }

        $settlementConceptPropertyRequirement
            ->setLocationFactors($locationFactors)
            ->setLocationCategories($locationCategories)
            ->setOutdoorSalesAreaRequired($settlementConceptPropertyRequirement->isOutdoorSalesAreaRequired())
            ->setGroundLevelSalesAreaRequired($settlementConceptPropertyRequirementRequest->isGroundLevelSalesAreaRequired())
            ->setBarrierFreeAccessRequired($settlementConceptPropertyRequirementRequest->isBarrierFreeAccessRequired())
            ->setParkingLotsRequired($settlementConceptPropertyRequirementRequest->getParkingLotsRequired());

        return $settlementConceptPropertyRequirement;
    }

    public function persistSettlementConcept(SettlementConcept $settlementConcept): void
    {
        $this->entityManager->persist($settlementConcept->getSettlementConceptPropertyRequirement());
        $this->entityManager->persist($settlementConcept->getSettlementConceptPropertyRequirement()->getSpace());
        $this->entityManager->persist($settlementConcept->getSettlementConceptPropertyRequirement()->getSecondarySpace());
        $this->entityManager->persist($settlementConcept->getSettlementConceptPropertyRequirement()->getOutdoorSalesArea());
        $this->entityManager->persist($settlementConcept->getSettlementConceptPropertyRequirement()->getStoreWidth());
        $this->entityManager->persist($settlementConcept->getSettlementConceptPropertyRequirement()->getShopWindowLength());
        $this->entityManager->persist($settlementConcept);
    }

    private static function createSettlementConceptPropertyRequirementFromSettlementConceptPropertyRequirementRequest(
        SettlementConceptPropertyRequirementRequest $settlementConceptPropertyRequirementPostRequest
    ): SettlementConceptPropertyRequirement {
        $settlementConceptPropertyRequirement = new SettlementConceptPropertyRequirement();

        $space = self::createValueRequirementFromValueRequirementPostRequest(
            valueRequirementPostRequest: $settlementConceptPropertyRequirementPostRequest->getSpace(),
            name: 'space'
        );

        $secondarySpace = self::createValueRequirementFromValueRequirementPostRequest(
            valueRequirementPostRequest: $settlementConceptPropertyRequirementPostRequest->getSecondarySpace(),
            name: 'secondary space'
        );

        $outdoorSalesArea = self::createValueRequirementFromValueRequirementPostRequest(
            valueRequirementPostRequest: $settlementConceptPropertyRequirementPostRequest->getOutdoorSalesArea(),
            name: 'outdoor sales area required'
        );

        $storeWidth = self::createValueRequirementFromValueRequirementPostRequest(
            valueRequirementPostRequest: $settlementConceptPropertyRequirementPostRequest->getStoreWidth(),
            name: 'store width'
        );

        $shopWindowLength = self::createValueRequirementFromValueRequirementPostRequest(
            valueRequirementPostRequest: $settlementConceptPropertyRequirementPostRequest->getShopWindowLength(),
            name: 'shop window length'
        );

        $locationCategories = [];

        foreach ($settlementConceptPropertyRequirementPostRequest->getLocationCategories() as $locationCategory) {
            $locationCategories[] = LocationCategory::from($locationCategory);
        }

        $locationFactors = [];

        foreach ($settlementConceptPropertyRequirementPostRequest->getLocationFactors() as $locationFactor) {
            $locationFactors[] = LocationFactor::from($locationFactor);
        }

        $settlementConceptPropertyRequirement->setSpace($space)
            ->setSecondarySpace($secondarySpace)
            ->setOutdoorSalesArea($outdoorSalesArea)
            ->setStoreWidth($storeWidth)
            ->setShopWindowLength($shopWindowLength)
            ->setOutdoorSalesAreaRequired($settlementConceptPropertyRequirementPostRequest->isOutdoorSalesAreaRequired())
            ->setGroundLevelSalesAreaRequired($settlementConceptPropertyRequirementPostRequest->isGroundLevelSalesAreaRequired())
            ->setBarrierFreeAccessRequired($settlementConceptPropertyRequirementPostRequest->isBarrierFreeAccessRequired())
            ->setParkingLotsRequired($settlementConceptPropertyRequirementPostRequest->getParkingLotsRequired())
            ->setLocationCategories($locationCategories)
            ->setLocationFactors($locationFactors);

        return $settlementConceptPropertyRequirement;
    }

    private static function createValueRequirementFromValueRequirementPostRequest(ValueRequirementPostRequest $valueRequirementPostRequest, string $name = null): ValueRequirement
    {
        $valueRequirement = new ValueRequirement();

        $valueRequirement
            ->setMinimumValue($valueRequirementPostRequest->getMinimumValue())
            ->setMaximumValue($valueRequirementPostRequest->getMaximumValue())
            ->setName($name);

        return $valueRequirement;
    }

    private static function updateValueRequirement(ValueRequirement $valueRequirement, ?float $minValue, ?float $maxValue): void
    {
        $valueRequirement
            ->setMaximumValue($maxValue)
            ->setMinimumValue($minValue);
    }
}
