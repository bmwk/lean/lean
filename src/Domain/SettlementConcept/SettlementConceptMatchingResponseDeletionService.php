<?php

declare(strict_types=1);

namespace App\Domain\SettlementConcept;

use App\Domain\Entity\SettlementConcept\SettlementConcept;
use App\Domain\Entity\SettlementConcept\SettlementConceptMatchingResponse;
use App\Repository\SettlementConcept\SettlementConceptMatchingResponseRepository;
use Doctrine\ORM\EntityManagerInterface;

class SettlementConceptMatchingResponseDeletionService
{
    public function __construct(
        private readonly SettlementConceptMatchingResponseRepository $settlementConceptMatchingResponseRepository,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function deleteSettlementConceptMatchingResponse(SettlementConceptMatchingResponse $settlementConceptMatchingResponse): void
    {
        $this->hardDeleteSettlementConceptMatchingResponse(settlementConceptMatchingResponse: $settlementConceptMatchingResponse);
    }

    public function deleteSettlementConceptMatchingResponsesBySettlementConcept(SettlementConcept $settlementConcept): void
    {
        $this->hardDeleteSettlementConceptMatchingResponsesBySettlementConcept(settlementConcept: $settlementConcept);
    }

    private function hardDeleteSettlementConceptMatchingResponse(SettlementConceptMatchingResponse $settlementConceptMatchingResponse): void
    {
        $this->entityManager->remove(entity: $settlementConceptMatchingResponse);
        $this->entityManager->flush();
    }

    private function hardDeleteSettlementConceptMatchingResponsesBySettlementConcept(SettlementConcept $settlementConcept): void
    {
        $settlementConceptMatchingResponses = $this->settlementConceptMatchingResponseRepository->findBy(
            criteria: ['settlementConcept' => $settlementConcept]
        );

        foreach ($settlementConceptMatchingResponses as $settlementConceptMatchingResponse) {
            $this->hardDeleteSettlementConceptMatchingResponse(settlementConceptMatchingResponse: $settlementConceptMatchingResponse);
        }
    }
}
