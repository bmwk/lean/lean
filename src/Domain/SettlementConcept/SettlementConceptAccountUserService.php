<?php

declare(strict_types=1);

namespace App\Domain\SettlementConcept;

use App\Domain\Entity\Account;
use App\Domain\Entity\SettlementConcept\SettlementConceptAccountUser;
use App\Domain\Entity\SettlementConcept\SettlementConceptAccountUserFilter;
use App\Domain\Entity\SettlementConcept\SettlementConceptAccountUserSearch;
use App\Domain\Entity\SettlementConcept\SettlementConceptAuthenticationTokenGenerationRequest;
use App\Domain\Entity\SortingOption\SortingOption;
use App\Repository\SettlementConcept\SettlementConceptAccountUserRepository;
use App\Repository\SettlementConcept\SettlementConceptAuthenticationTokenGenerationRequestRepository;
use App\Security\JsonWebTokenService;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Uid\Uuid;

class SettlementConceptAccountUserService
{
    public function __construct(
        private readonly string $noReplyEmailAddress,
        private readonly SettlementConceptAuthenticationTokenGenerationRequestRepository $settlementConceptAuthenticationTokenGenerationRequestRepository,
        private readonly SettlementConceptAccountUserRepository $settlementConceptAccountUserRepository,
        private readonly JsonWebTokenService $jsonWebTokenService,
        private readonly MailerInterface $mailer
    ) {
    }

    public function fetchSettlementConceptAccountUsersPaginatedByAccount(
        Account $account,
        int $firstResult,
        int $maxResults,
        ?SettlementConceptAccountUserFilter $settlementConceptAccountUserFilter = null,
        ?SettlementConceptAccountUserSearch $settlementConceptAccountUserSearch = null,
        ?SortingOption $sortingOption = null
    ): Paginator {
        return $this->settlementConceptAccountUserRepository->findPaginatedByAccount(
            account: $account,
            firstResult: $firstResult,
            maxResults: $maxResults,
            settlementConceptAccountUserFilter: $settlementConceptAccountUserFilter,
            settlementConceptAccountUserSearch: $settlementConceptAccountUserSearch,
            sortingOption: $sortingOption
        );
    }

    public function fetchSettlementConceptAccountUserById(int $id, Account $account): ?SettlementConceptAccountUser
    {
        return $this->settlementConceptAccountUserRepository->findOneBy(criteria: [
            'id'      => $id,
            'account' => $account,
            'deleted' => false,
        ]);
    }

    public function fetchSettlementConceptAuthenticationTokenGenerationRequestByUuid(Uuid $uuid): ?SettlementConceptAuthenticationTokenGenerationRequest
    {
        return $this->settlementConceptAuthenticationTokenGenerationRequestRepository->findOneBy(criteria: ['uuid' => $uuid]);
    }

    public function fetchSettlementConceptAuthenticationTokenGenerationRequestBySettlementConceptAccountUser(
        SettlementConceptAccountUser $settlementConceptAccountUser
    ): ?SettlementConceptAuthenticationTokenGenerationRequest {
        return $this->settlementConceptAuthenticationTokenGenerationRequestRepository->findOneBy(criteria: [
            'settlementConceptAccountUser' => $settlementConceptAccountUser,
        ]);
    }

    /**
     * @return SettlementConceptAccountUser[]
     */
    public function fetchToBeDeletedSettlementConceptAccountUserFromAllAccounts(?int $timeTillHardDelete): array
    {
        return $this->settlementConceptAccountUserRepository->findToBeDeletedSettlementConceptAccountUserFromAllAccounts(
            timeTillHardDelete: $timeTillHardDelete
        );
    }

    public function generateJsonWebToken(SettlementConceptAccountUser $settlementConceptAccountUser): string
    {
        return $this->jsonWebTokenService->createToken(payloadData: ['identifier' => $settlementConceptAccountUser->getIdentifier()]);
    }

    public function sendEmailWithLinkToAuthenticationTokenGeneration(
        SettlementConceptAuthenticationTokenGenerationRequest $settlementConceptAuthenticationTokenGenerationRequest
    ): void {
        $settlementConceptAccountUser = $settlementConceptAuthenticationTokenGenerationRequest->getSettlementConceptAccountUser();

        $email = (new TemplatedEmail())
            ->from(addresses: $this->noReplyEmailAddress)
            ->to($settlementConceptAccountUser->getEmail())
            ->subject(subject: 'Link zur Ihrem LeAn® API Zugriffstoken')
            ->htmlTemplate(template: 'external_user/email/api_user/settlement_concept_account_user/link_authentication_token_generation.html.twig')
            ->textTemplate(template: 'external_user/email/api_user/settlement_concept_account_user/link_authentication_token_generation.txt.twig')
            ->context(context: [
                'settlementConceptAccountUser' => $settlementConceptAccountUser,
                'identifier'                   => $settlementConceptAuthenticationTokenGenerationRequest->getUuid()->toRfc4122(),
            ]);

        $this->mailer->send($email);
    }
}
