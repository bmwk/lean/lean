<?php

declare(strict_types=1);

namespace App\Domain\SettlementConcept;

use App\Domain\Entity\Account;
use App\Domain\Entity\SettlementConcept\SettlementConceptMatchingTask;
use App\Repository\SettlementConcept\SettlementConceptMatchingTaskRepository;
use Doctrine\ORM\EntityManagerInterface;

class SettlementConceptMatchingTaskDeletionService
{
    public function __construct(
        private readonly SettlementConceptMatchingTaskRepository $settlementConceptMatchingTaskRepository,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function deleteSettlementConceptMatchingTask(SettlementConceptMatchingTask $settlementConceptMatchingTask): void
    {
        $this->hardDeleteSettlementConceptMatchingTask(settlementConceptMatchingTask: $settlementConceptMatchingTask);
    }

    public function deleteSettlementConceptMatchingTasksByAccount(Account $account): void
    {
        $this->hardDeleteSettlementConceptMatchingTasksByAccount(account: $account);
    }

    private function hardDeleteSettlementConceptMatchingTask(SettlementConceptMatchingTask $settlementConceptMatchingTask): void
    {
        $this->entityManager->remove(entity: $settlementConceptMatchingTask);
        $this->entityManager->flush();
    }

    private function hardDeleteSettlementConceptMatchingTasksByAccount(Account $account): void
    {
        $settlementConceptMatchingTasks = $this->settlementConceptMatchingTaskRepository->findBy(criteria: ['account' => $account]);

        foreach ($settlementConceptMatchingTasks as $settlementConceptMatchingTask) {
            $this->hardDeleteSettlementConceptMatchingTask(settlementConceptMatchingTask: $settlementConceptMatchingTask);
        }
    }
}
