<?php

declare(strict_types=1);

namespace App\Domain\SettlementConcept;

use App\Domain\Classification\ClassificationService;
use App\Domain\Entity\Account;
use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\Property\BuildingUnit;
use App\Domain\Entity\SettlementConcept\SettlementConcept;
use App\Domain\Entity\SettlementConcept\SettlementConceptMatchingResponse;
use App\Domain\Entity\SettlementConcept\SettlementConceptMatchingResult;
use App\Domain\Entity\SettlementConcept\SettlementConceptMatchingTask;
use App\Domain\Entity\TaskStatus;
use App\Domain\LookingForPropertyRequest\Matching\Exception\MatchingTaskNotFoundException;
use App\Domain\Matching\MatchingStrategyInterface;
use App\Domain\SettlementConcept\Matching\SettlementConceptPointMatchingStrategy;
use App\Repository\SettlementConcept\SettlementConceptMatchingResponseRepository;
use App\Repository\SettlementConcept\SettlementConceptMatchingResultRepository;
use App\Repository\SettlementConcept\SettlementConceptMatchingTaskRepository;
use Doctrine\ORM\EntityManagerInterface;

class SettlementConceptMatchingService
{
    private readonly MatchingStrategyInterface $matchingStrategy;

    public function __construct(
        private readonly SettlementConceptMatchingResultDeletionService $settlementConceptMatchingResultDeletionService,
        private readonly SettlementConceptService $settlementConceptService,
        private readonly ClassificationService $classificationService,
        private readonly SettlementConceptMatchingResponseRepository $settlementConceptMatchingResponseRepository,
        private readonly SettlementConceptMatchingResultRepository $settlementConceptMatchingResultRepository,
        private readonly SettlementConceptMatchingTaskRepository $settlementConceptMatchingTaskRepository,
        private readonly EntityManagerInterface $entityManager
    ) {
        $this->matchingStrategy = new SettlementConceptPointMatchingStrategy(classificationService: $this->classificationService);
    }

    public function fetchSettlementConceptMatchingResultById(Account $account, int $id): ?SettlementConceptMatchingResult
    {
        return $this->settlementConceptMatchingResultRepository->findOneBy(criteria: [
            'account' => $account,
            'id'      => $id,
        ]);
    }

    public function fetchSettlementConceptMatchingTaskByBuildingUnit(BuildingUnit $buildingUnit): ?SettlementConceptMatchingTask
    {
        return $this->settlementConceptMatchingTaskRepository->findOneBy(criteria: ['buildingUnit' => $buildingUnit]);
    }

    public function fetchSettlementConceptMatchingResponseBySettlementConceptAndBuildingUnit(
        Account $account,
        SettlementConcept $settlementConcept,
        BuildingUnit $buildingUnit,
    ): ?SettlementConceptMatchingResponse {
        return $this->settlementConceptMatchingResponseRepository->findOneBySettlementConceptAndBuildingUnit(
            account: $account,
            settlementConcept: $settlementConcept,
            buildingUnit: $buildingUnit
        );
    }

    public function countSettlementConceptMatchingResultsGroupedByBuildingUnitAndEliminated(Account $account): array
    {
        return $this->settlementConceptMatchingResultRepository->countGroupedByBuildingUnitAndEliminated(account: $account);
    }

    public function updateSettlementConceptTaskStatus(SettlementConceptMatchingTask $settlementConceptMatchingTask, TaskStatus $taskStatus): void
    {
        $settlementConceptMatchingTask->setTaskStatus($taskStatus);

        $this->entityManager->flush();
    }

    public function createSettlementConceptMatchingTask(
        BuildingUnit $buildingUnit,
        Account $account,
        AccountUser $accountUser
    ): SettlementConceptMatchingTask {
        $settlementConceptMatchingTask = new SettlementConceptMatchingTask();

        $settlementConceptMatchingTask
            ->setAccount($account)
            ->setBuildingUnit($buildingUnit)
            ->setCreatedByAccountUser($accountUser)
            ->setTaskStatus(TaskStatus::CREATED);

        return $settlementConceptMatchingTask;
    }

    public function createAndSaveSettlementConceptMatchingTask(
        BuildingUnit $buildingUnit,
        Account $account,
        AccountUser $accountUser
    ): SettlementConceptMatchingTask {
        $settlementConceptMatchingTask = $this->createSettlementConceptMatchingTask(buildingUnit: $buildingUnit, account: $account, accountUser: $accountUser);

        $this->entityManager->persist(entity: $settlementConceptMatchingTask);

        $this->entityManager->flush();

        return $settlementConceptMatchingTask;
    }

    public function executeSettlementConceptMatchingTaskById(int $id, bool $dryRun = false): void
    {
        $settlementConceptMatchingTask = $this->settlementConceptMatchingTaskRepository->findOneBy(criteria: ['id' => $id]);

        if ($settlementConceptMatchingTask === null) {
            throw new MatchingTaskNotFoundException();
        }

        $this->executeSettlementConceptMatchingTask(settlementConceptMatchingTask: $settlementConceptMatchingTask, dryRun: $dryRun);
    }

    public function executeSettlementConceptMatchingTask(SettlementConceptMatchingTask $settlementConceptMatchingTask, bool $dryRun = false): void
    {
        if ($settlementConceptMatchingTask->getTaskStatus() !== TaskStatus::CREATED) {
            throw new \RuntimeException(message: 'Task is already process or in progress');
        }

        if ($dryRun === false) {
            $this->updateSettlementConceptTaskStatus(settlementConceptMatchingTask: $settlementConceptMatchingTask, taskStatus: TaskStatus::IN_PROGRESS);
        }

        $this->executeMatching(buildingUnit: $settlementConceptMatchingTask->getBuildingUnit(), account: $settlementConceptMatchingTask->getAccount());

        if ($dryRun === false) {
            $this->updateSettlementConceptTaskStatus(settlementConceptMatchingTask: $settlementConceptMatchingTask, taskStatus: TaskStatus::DONE);
        }
    }

    public function executeSettlementConceptMatchingTasks(bool $dryRun = false): void
    {
        $settlementConceptMatchingTasks = $this->settlementConceptMatchingTaskRepository->findBy(criteria: ['taskStatus' => TaskStatus::CREATED]);

        foreach ($settlementConceptMatchingTasks as $settlementConceptMatchingTask) {
            $this->executeSettlementConceptMatchingTask(settlementConceptMatchingTask: $settlementConceptMatchingTask, dryRun: $dryRun);
        }
    }

    /**
     * @return SettlementConceptMatchingResult[]
     */
    private function executeMatching(BuildingUnit $buildingUnit, Account $account): array
    {
        $settlementConcepts = $this->settlementConceptService->fetchSettlementConcepts(
            account: $account,
            fromDeletedSettlementConceptAccountUser: false,
            fromDisabledSettlementConceptAccountUser: false
        );

        $this->settlementConceptMatchingResultDeletionService->deleteSettlementConceptMatchingResultsByBuildingUnit(buildingUnit: $buildingUnit);

        $results = $this->matchingStrategy->doMatching(requests: $settlementConcepts, buildingUnit: $buildingUnit);

        foreach ($results as $result) {
            $result->setAccount($account);
        }

        $this->saveMatchingResults(results: $results);

        return $results;
    }

    /**
     * @param SettlementConceptMatchingResult[] $results
     */
    private function saveMatchingResults(array $results): void
    {
        $this->entityManager->beginTransaction();

        foreach ($results as $result) {
            $this->entityManager->persist(entity: $result);

            $settlementConceptEliminationCriteria = $result->getSettlementConceptEliminationCriteria();

            $this->entityManager->persist($settlementConceptEliminationCriteria);
            $this->entityManager->persist($settlementConceptEliminationCriteria->getPropertyOfferTypes());
            $this->entityManager->persist($settlementConceptEliminationCriteria->getMinSpace());
            $this->entityManager->persist($settlementConceptEliminationCriteria->getMaxSpace());
            $this->entityManager->persist($settlementConceptEliminationCriteria->getOutdoorSpaceRequired());
            $this->entityManager->persist($settlementConceptEliminationCriteria->getGroundLevelSalesAreaRequired());
            $this->entityManager->persist($settlementConceptEliminationCriteria->getBarrierFreeAccessRequired());
            $this->entityManager->persist($settlementConceptEliminationCriteria->getLocationCategories());
            $this->entityManager->persist($settlementConceptEliminationCriteria->getParkingLotsRequired());
            $this->entityManager->persist($settlementConceptEliminationCriteria->getAgeStructures());
            $this->entityManager->persist($settlementConceptEliminationCriteria->getIndustryClassification());

            $settlementConceptScoreCriteria = $result->getSettlementConceptScoreCriteria();

            $this->entityManager->persist($settlementConceptScoreCriteria);
            $this->entityManager->persist($settlementConceptScoreCriteria->getRetailSpace());
            $this->entityManager->persist($settlementConceptScoreCriteria->getRetailSpace()->getMinScoreCriterion());
            $this->entityManager->persist($settlementConceptScoreCriteria->getRetailSpace()->getMaxScoreCriterion());
            $this->entityManager->persist($settlementConceptScoreCriteria->getSecondarySpace());
            $this->entityManager->persist($settlementConceptScoreCriteria->getOutdoorSalesArea());
            $this->entityManager->persist($settlementConceptScoreCriteria->getStoreWidth());
            $this->entityManager->persist($settlementConceptScoreCriteria->getShopWindowLength());
            $this->entityManager->persist($settlementConceptScoreCriteria->getLocationFactors());

            $this->entityManager->flush();
        }

        $this->entityManager->commit();
    }
}
