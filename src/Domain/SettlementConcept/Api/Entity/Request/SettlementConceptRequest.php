<?php

declare(strict_types=1);

namespace App\Domain\SettlementConcept\Api\Entity\Request;

class SettlementConceptRequest
{
    private array $propertyOfferTypes;
    private SettlementConceptPropertyRequirementRequest $settlementConceptPropertyRequirementRequest;
    private ?array $ageStructures;
    private ?array $priceSegments;
    private ?int $naceClassificationId;
    private int $industryClassificationId;
    private ?string $description = null;
    private ?string $externalReferenceNumber = null;

    public static function createFromJsonContent(object $jsonContent): self
    {
        $settlementConceptPropertyRequirementRequest = SettlementConceptPropertyRequirementRequest::createFromJsonContent(
           jsonContent: $jsonContent->settlementConceptPropertyRequirement
        );

        $settlementConceptRequest = new self();

        $settlementConceptRequest->propertyOfferTypes = $jsonContent->propertyOfferTypes;
        $settlementConceptRequest->settlementConceptPropertyRequirementRequest = $settlementConceptPropertyRequirementRequest;
        $settlementConceptRequest->ageStructures = $jsonContent->ageStructures ?? null;
        $settlementConceptRequest->priceSegments = $jsonContent->priceSegments ?? null;
        $settlementConceptRequest->naceClassificationId = $jsonContent->naceClassificationId ?? null;
        $settlementConceptRequest->industryClassificationId = $jsonContent->industryClassificationId;
        $settlementConceptRequest->description = $jsonContent->description ?? null;
        $settlementConceptRequest->externalReferenceNumber = $jsonContent->externalReferenceNumber ?? null;

        return $settlementConceptRequest;
    }

    public function getPropertyOfferTypes(): array
    {
        return $this->propertyOfferTypes;
    }

    public function getSettlementConceptPropertyRequirementRequest(): SettlementConceptPropertyRequirementRequest
    {
        return $this->settlementConceptPropertyRequirementRequest;
    }

    public function getAgeStructures(): ?array
    {
        return $this->ageStructures;
    }

    public function getPriceSegments(): ?array
    {
        return $this->priceSegments;
    }

    public function getNaceClassificationId(): ?int
    {
        return $this->naceClassificationId;
    }

    public function getIndustryClassificationId(): int
    {
        return $this->industryClassificationId;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getExternalReferenceNumber(): ?string
    {
        return $this->externalReferenceNumber;
    }
}
