<?php

declare(strict_types=1);

namespace App\Domain\SettlementConcept\Api\Entity\Request;

class ValueRequirementPostRequest
{
    private ?float $minimumValue;
    private ?float $maximumValue;

    public static function createFromJsonContent(object $jsonContent): self
    {
        $valueRequirementPostRequest = new self();
        $valueRequirementPostRequest->maximumValue = $jsonContent->maximumValue;
        $valueRequirementPostRequest->minimumValue = $jsonContent->minimumValue;

        return $valueRequirementPostRequest;
    }

    public function getMinimumValue(): ?float
    {
        return $this->minimumValue;
    }

    public function getMaximumValue(): ?float
    {
        return $this->maximumValue;
    }
}
