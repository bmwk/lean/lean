<?php

declare(strict_types=1);

namespace App\Domain\SettlementConcept\Api\Entity\Request;

use App\Domain\Entity\ParkingLotRequirementType;

class SettlementConceptPropertyRequirementRequest
{
    private ?ValueRequirementPostRequest $space;
    private ?ValueRequirementPostRequest $secondarySpace;
    private bool $outdoorSalesAreaRequired;
    private ?ValueRequirementPostRequest $outdoorSalesArea;
    private ?ValueRequirementPostRequest $storeWidth;
    private ?ValueRequirementPostRequest $shopWindowLength;
    private bool $groundLevelSalesAreaRequired;
    private bool $barrierFreeAccessRequired;
    private ?ParkingLotRequirementType $parkingLotsRequired;
    private ?array $locationCategories = null;
    private ?array $locationFactors = null;

    public static function createFromJsonContent(object $jsonContent): self
    {
        $spacePostRequest = ValueRequirementPostRequest::createFromJsonContent($jsonContent->space);
        $secondarySpacePostRequest = ValueRequirementPostRequest::createFromJsonContent($jsonContent->secondarySpace);
        $outdoorSalesAreaPostRequest = ValueRequirementPostRequest::createFromJsonContent($jsonContent->outdoorSalesArea);
        $storeWidthPostRequest = ValueRequirementPostRequest::createFromJsonContent($jsonContent->storeWidth);
        $shopWindowLengthPostRequest = ValueRequirementPostRequest::createFromJsonContent($jsonContent->shopWindowLength);

        $settlementConceptPropertyRequirementPostRequest = new self();

        $settlementConceptPropertyRequirementPostRequest->outdoorSalesAreaRequired = $jsonContent->outdoorSalesAreaRequired;
        $settlementConceptPropertyRequirementPostRequest->groundLevelSalesAreaRequired = $jsonContent->groundLevelSalesAreaRequired;
        $settlementConceptPropertyRequirementPostRequest->barrierFreeAccessRequired = $jsonContent->barrierFreeAccessRequired;
        $settlementConceptPropertyRequirementPostRequest->parkingLotsRequired = $jsonContent->parkingLotsRequired !== null
            ? ParkingLotRequirementType::from($jsonContent->parkingLotsRequired)
            : null;
        $settlementConceptPropertyRequirementPostRequest->locationCategories = $jsonContent->locationCategories ?? null;
        $settlementConceptPropertyRequirementPostRequest->locationFactors = $jsonContent->locationFactors ?? null;
        $settlementConceptPropertyRequirementPostRequest->space = $spacePostRequest;
        $settlementConceptPropertyRequirementPostRequest->secondarySpace = $secondarySpacePostRequest;
        $settlementConceptPropertyRequirementPostRequest->outdoorSalesArea = $outdoorSalesAreaPostRequest;
        $settlementConceptPropertyRequirementPostRequest->storeWidth = $storeWidthPostRequest;
        $settlementConceptPropertyRequirementPostRequest->shopWindowLength = $shopWindowLengthPostRequest;

        return $settlementConceptPropertyRequirementPostRequest;
    }

    public function getSpace(): ValueRequirementPostRequest
    {
        return $this->space;
    }

    public function getSecondarySpace(): ValueRequirementPostRequest
    {
        return $this->secondarySpace;
    }

    public function isOutdoorSalesAreaRequired(): bool
    {
        return $this->outdoorSalesAreaRequired;
    }

    public function getOutdoorSalesArea(): ValueRequirementPostRequest
    {
        return $this->outdoorSalesArea;
    }

    public function getStoreWidth(): ValueRequirementPostRequest
    {
        return $this->storeWidth;
    }

    public function getShopWindowLength(): ValueRequirementPostRequest
    {
        return $this->shopWindowLength;
    }

    public function setShopWindowLength(ValueRequirementPostRequest $shopWindowLength): self
    {
        $this->shopWindowLength = $shopWindowLength;
        return $this;
    }

    public function isGroundLevelSalesAreaRequired(): bool
    {
        return $this->groundLevelSalesAreaRequired;
    }

    public function isBarrierFreeAccessRequired(): bool
    {
        return $this->barrierFreeAccessRequired;
    }

    public function getParkingLotsRequired(): ?ParkingLotRequirementType
    {
        return $this->parkingLotsRequired;
    }

    public function getLocationCategories(): ?array
    {
        return $this->locationCategories;
    }

    public function getLocationFactors(): ?array
    {
        return $this->locationFactors;
    }
}
