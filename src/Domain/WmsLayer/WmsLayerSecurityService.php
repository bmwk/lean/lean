<?php

declare(strict_types=1);

namespace App\Domain\WmsLayer;

use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\AccountUser\AccountUserRole;
use App\Domain\Entity\WmsLayer;
use Symfony\Bundle\SecurityBundle\Security;

class WmsLayerSecurityService
{
    public function __construct(
        private readonly Security $security
    ) {
    }
     public function canViewWmsLayer(WmsLayer $wmsLayer, AccountUser $accountUser): bool
    {
        $account = $accountUser->getAccount();

        if ($accountUser->getWmsLayers()->contains(element: $wmsLayer) === true) {
            return true;
        }

        if ($account->getWmsLayers()->contains(element: $wmsLayer) === true) {
            return true;
        }

        return false;
    }

    public function canEditWmsLayer(WmsLayer $wmsLayer, AccountUser $accountUser): bool
    {
        $account = $accountUser->getAccount();

        if ($accountUser->getWmsLayers()->contains(element: $wmsLayer) === true) {
            return true;
        }

        if (
            $account->getWmsLayers()->contains(element: $wmsLayer) === true
            && (
               $this->security->isGranted(attributes: AccountUserRole::ROLE_ACCOUNT_ADMIN->value) === true
               || $this->security->isGranted(attributes: AccountUserRole::ROLE_CONFIGURATOR->value) === true
            )
        ) {
            return true;
        }

        return false;
    }

    public function canDeleteWmsLayer(WmsLayer $wmsLayer, AccountUser $accountUser): bool
    {
        $account = $accountUser->getAccount();

        if ($accountUser->getWmsLayers()->contains(element: $wmsLayer) === true) {
            return true;
        }

        if (
            $account->getWmsLayers()->contains(element: $wmsLayer) === true
            && (
                $this->security->isGranted(attributes: AccountUserRole::ROLE_ACCOUNT_ADMIN->value) === true
                || $this->security->isGranted(attributes: AccountUserRole::ROLE_CONFIGURATOR->value) === true
            )
        ) {
            return true;
        }

        return false;
    }
}
