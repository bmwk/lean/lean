<?php

declare(strict_types=1);

namespace App\Domain\Property;

use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\AccountUser\AccountUserRole;
use App\Domain\Entity\Property\PropertyUser;
use Symfony\Bundle\SecurityBundle\Security;

class PropertyUserSecurityService
{
    public function __construct(
        private readonly Security $security
    ) {
    }

    public function canViewPropertyUser(PropertyUser $propertyUser, AccountUser $accountUser): bool
    {
        $account = $accountUser->getAccount();

        if (
            $account->getParentAccount() === null
            && (
                $this->security->isGranted(attributes: AccountUserRole::ROLE_USER->value) === true
                || $this->security->isGranted(attributes: AccountUserRole::ROLE_PROPERTY_FEEDER->value) === true
                || $this->security->isGranted(attributes: AccountUserRole::ROLE_VIEWER->value) === true
                || $this->security->isGranted(attributes: AccountUserRole::ROLE_RESTRICTED_VIEWER->value) === true
            )
            && (
                $propertyUser->getAccount() === $account
                || $account->getSubAccounts()->contains(element: $propertyUser->getAccount()) === true
            )
        ) {
            return true;
        }

        return false;
    }

    public function canEditPropertyUser(PropertyUser $propertyUser, AccountUser $accountUser): bool
    {
        $account = $accountUser->getAccount();

        if (
            $account->getParentAccount() === null
            && (
                $this->security->isGranted(attributes: AccountUserRole::ROLE_USER->value) === true
                || $this->security->isGranted(attributes: AccountUserRole::ROLE_PROPERTY_FEEDER->value) === true
            )
            && (
                $propertyUser->getAccount() === $account
                || $account->getSubAccounts()->contains(element: $propertyUser->getAccount()) === true
            )
        ) {
            return true;
        }

        return false;
    }

    public function canDeletePropertyUser(PropertyUser $propertyUser, AccountUser $accountUser): bool
    {
        return $this->canEditPropertyUser(propertyUser: $propertyUser, accountUser: $accountUser);
    }
}
