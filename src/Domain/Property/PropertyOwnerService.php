<?php

declare(strict_types=1);

namespace App\Domain\Property;

use App\Domain\Entity\Account;
use App\Domain\Entity\Person\Person;
use App\Domain\Entity\Property\BuildingUnit;
use App\Domain\Entity\Property\PropertyOwner;
use App\Repository\Property\PropertyOwnerRepository;

class PropertyOwnerService
{
    public function __construct(
        private readonly PropertyOwnerRepository $propertyOwnerRepository
    ) {
    }

    public function fetchPropertyOwnerByIdAndBuildingUnit(
        Account $account,
        bool $withFromSubAccounts,
        int $id,
        BuildingUnit $buildingUnit
    ): ?PropertyOwner {
        return $this->propertyOwnerRepository->findOneByIdAndBuildingUnit(
            account: $account,
            withFromSubAccounts: $withFromSubAccounts,
            id: $id,
            buildingUnit: $buildingUnit
        );
    }

    public function fetchPropertyOwnerByBuildingUnitAndPerson(
        Account $account,
        bool $withFromSubAccounts,
        BuildingUnit $buildingUnit,
        Person $person
    ): ?PropertyOwner {
        return $this->propertyOwnerRepository->findOneByBuildingUnitAndPerson(
            account: $account,
            withFromSubAccounts: $withFromSubAccounts,
            buildingUnit: $buildingUnit,
            person: $person
        );
    }
}
