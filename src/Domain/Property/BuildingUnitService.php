<?php

declare(strict_types=1);

namespace App\Domain\Property;

use App\Domain\Document\DocumentService;
use App\Domain\EarlyWarningSystem\JsonRepresentation\BuildingUnitFactory;
use App\Domain\Entity\Account;
use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\Classification\IndustryClassification;
use App\Domain\Entity\LookingForPropertyRequest\MatchingFilter;
use App\Domain\Entity\LookingForPropertyRequest\MatchingSearch;
use App\Domain\Entity\Person\Person;
use App\Domain\Entity\Property\BuildingUnit;
use App\Domain\Entity\Property\BuildingUnitFilter;
use App\Domain\Entity\Property\BuildingUnitSearch;
use App\Domain\Entity\SortingOption\SortingOption;
use App\Domain\Image\ImageService;
use App\Domain\Location\LocationService;
use App\GoogleMaps\GoogleMapsService;
use App\Repository\Property\BuildingUnitRepository;
use App\TaodEarlyWarningSystem\TaodEarlyWarningSystemService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;

class BuildingUnitService extends AbstractPropertyService
{
    public function __construct(
        ImageService $imageService,
        DocumentService $documentService,
        LocationService $locationService,
        GoogleMapsService $googleMapsService,
        EntityManagerInterface $entityManager,
        private readonly BuildingUnitRepository $buildingUnitRepository,
        private readonly TaodEarlyWarningSystemService $taodEarlyWarningSystemService
    ) {
        parent::__construct(
            imageService: $imageService,
            documentService: $documentService,
            locationService: $locationService,
            googleMapsService: $googleMapsService,
            entityManager: $entityManager
        );
    }

    /**
     * @return BuildingUnit[]
     */
    public function fetchBuildingUnitsByAccount(
        Account $account,
        bool $withFromSubAccounts,
        bool $archived,
        ?BuildingUnitFilter $buildingUnitFilter = null,
        ?BuildingUnitSearch $buildingUnitSearch = null,
        ?SortingOption $sortingOption = null
    ): array {
        return $this->buildingUnitRepository->findByAccount(
            account: $account,
            withFromSubAccounts: $withFromSubAccounts,
            archived: $archived,
            buildingUnitFilter: $buildingUnitFilter,
            buildingUnitSearch: $buildingUnitSearch,
            sortingOption: $sortingOption
        );
    }

    public function fetchBuildingUnitsPaginatedByAccount(
        Account $account,
        bool $withFromSubAccounts,
        bool $archived,
        int $firstResult,
        int $maxResults,
        ?BuildingUnitFilter $buildingUnitFilter = null,
        ?BuildingUnitSearch $buildingUnitSearch = null,
        ?SortingOption $sortingOption = null
    ): Paginator {
        return $this->buildingUnitRepository->findPaginatedByAccount(
            account: $account,
            withFromSubAccounts: $withFromSubAccounts,
            archived: $archived,
            firstResult: $firstResult,
            maxResults: $maxResults,
            buildingUnitFilter: $buildingUnitFilter,
            buildingUnitSearch: $buildingUnitSearch,
            sortingOption: $sortingOption
        );
    }

    /**
     * @param int[] $ids
     * @return BuildingUnit[]
     */
    public function fetchBuildingUnitsByIds(
        Account $account,
        bool $withFromSubAccounts,
        array $ids,
        bool $archived,
        ?AccountUser $createdByAccountUser = null,
        ?BuildingUnitFilter $buildingUnitFilter = null,
        ?BuildingUnitSearch $buildingUnitSearch = null,
        ?SortingOption $sortingOption = null
    ): array {
        return $this->buildingUnitRepository->findByIds(
            account: $account,
            withFromSubAccounts: $withFromSubAccounts,
            ids: $ids,
            archived: $archived,
            createdByAccountUser: $createdByAccountUser,
            buildingUnitFilter: $buildingUnitFilter,
            buildingUnitSearch: $buildingUnitSearch,
            sortingOption: $sortingOption
        );
    }

    public function fetchBuildingUnitByIdWithoutAccount(int $id): ?BuildingUnit
    {
        return $this->buildingUnitRepository->find(id: $id);
    }

    public function fetchBuildingUnitById(Account $account, bool $withFromSubAccounts, int $id, ?bool $archived = null): ?BuildingUnit
    {
        return $this->buildingUnitRepository->findOneById(
            account: $account,
            withFromSubAccounts: $withFromSubAccounts,
            id: $id,
            archived: $archived
        );
    }

    /**
     * @return BuildingUnit[]
     */
    public function fetchBuildingUnitsByConnectedPerson(
        Account $account,
        bool $withFromSubAccounts,
        Person $person,
        bool $archived
    ): array {
        return $this->buildingUnitRepository->findByConnectedPerson(
            account: $account,
            withFromSubAccounts: $withFromSubAccounts,
            person: $person,
            archived: $archived
        );
    }

    public function fetchBuildingUnitWithConnectedImagesById(Account $account, bool $withFromSubAccounts, int $id): ?BuildingUnit
    {
        return $this->buildingUnitRepository->findOneWithConnectedImagesById(account: $account, withFromSubAccounts: $withFromSubAccounts, id: $id);
    }

    public function fetchBuildingUnitWithConnectedDocumentsById(Account $account, bool $withFromSubAccounts, int $id): ?BuildingUnit
    {
        return $this->buildingUnitRepository->findOneWithConnectedDocumentsById(account: $account, withFromSubAccounts: $withFromSubAccounts, id: $id);
    }

    public function fetchBuildingUnitsWithMatchingResultsPaginatedByAccount(
        Account $account,
        bool $withFromSubAccounts,
        bool $archived,
        bool $includeEliminatedMatchingResults,
        int $firstResult,
        int $maxResults,
        ?MatchingFilter $matchingFilter = null,
        ?MatchingSearch $matchingSearch = null
    ): Paginator {
        return $this->buildingUnitRepository->findPaginatedWithMatchingResults(
            account: $account,
            withFromSubAccounts: $withFromSubAccounts,
            archived: $archived,
            includeEliminatedMatchingResults: $includeEliminatedMatchingResults,
            firstResult: $firstResult,
            maxResults: $maxResults,
            matchingFilter: $matchingFilter,
            matchingSearch: $matchingSearch
        );
    }

    public function fetchBuildingUnitWithMatchingResultsById(
        Account $account,
        bool $withFromSubAccounts,
        int $id,
        bool $archived,
        bool $includeEliminatedMatchingResults
    ): ?BuildingUnit {
        return $this->buildingUnitRepository->findOneWithMatchingResultsById(
            account: $account,
            withFromSubAccounts: $withFromSubAccounts,
            id: $id,
            archived: $archived,
            includeEliminatedMatchingResults: $includeEliminatedMatchingResults
        );
    }

    public function fetchBuildingUnitsWithCurrentUsageInBoundingBox(
        Account $account,
        bool $withFromSubAccounts,
        bool $archived,
        BuildingUnit $buildingUnit,
        IndustryClassification $industryClassification,
        float $searchRadius = 1.5
    ): array {
        return $this->buildingUnitRepository->findWithCurrentUsageInBoundingBoxByIndustryClassification(
            account: $account,
            withFromSubAccounts: $withFromSubAccounts,
            archived: $archived,
            buildingUnit: $buildingUnit,
            industryClassification: $industryClassification,
            searchRadius: $searchRadius
        );
    }

    /**
     * @return BuildingUnit[]
     */
    public function fetchBuildingUnitsByBecameEmptyToday(Account $account, bool $withFromSubAccounts): array
    {
        return $this->buildingUnitRepository->findByBecameEmptyToday(account: $account, withFromSubAccounts: $withFromSubAccounts);
    }

    /**
     * @return BuildingUnit[]
     */
    public function fetchBuildingUnitsByRentalAgreementExpiredToday(Account $account, bool $withFromSubAccounts): array
    {
        return $this->buildingUnitRepository->findByRentalAgreementExpiredToday(account: $account, withFromSubAccounts: $withFromSubAccounts);
    }

    /**
     * @return BuildingUnit[]
     */
    public function fetchBuildingUnitsByNotUpdatedInDays(Account $account, bool $withFromSubAccounts, int $notUpdatedInDays): array
    {
        return $this->buildingUnitRepository->findByNotUpdatedInDays(account: $account, withFromSubAccounts: $withFromSubAccounts, notUpdatedInDays: $notUpdatedInDays);
    }

    /**
     * @return BuildingUnit[]
     */
    public function fetchBuildingUnitsWithoutFoundMatching(Account $account, bool $withFromSubAccounts, int $noMatchFoundInDays, int $intervalLength): array
    {
        return $this->buildingUnitRepository->findWithoutFoundMatching(
            account: $account,
            withFromSubAccounts: $withFromSubAccounts,
            noMatchFoundInDays:$noMatchFoundInDays,
            intervalLength: $intervalLength
        );
    }

    /**
     * @return BuildingUnit[]
     */
    public function fetchBuildingUnitsWithoutMatchingExecuted(Account $account, bool $withFromSubAccounts, int $noMatchingExecutedInDays): array
    {
        return $this->buildingUnitRepository->findWithoutMatchingExecuted(
            account: $account,
            withFromSubAccounts: $withFromSubAccounts,
            notMatchingExecutedInDays: $noMatchingExecutedInDays
        );
    }

    /**
     * @param Account[] $accounts
     */
    public function fetchBuildingUnitByInternalNumber(string $internalNumber, array $accounts): ?BuildingUnit
    {
        return $this->buildingUnitRepository->findOneBy(criteria: [
            'internalNumber' => $internalNumber,
            'account'        => $accounts,
            'deleted'        => false,
        ]);
    }

    public function countBuildingUnitsByAssignedAccountUser(Account $account, AccountUser $accountUser, bool $archived): int
    {
        return $this->buildingUnitRepository->count(criteria: [
            'account'               => $account,
            'assignedToAccountUser' => $accountUser,
            'archived'              => $archived,
            'deleted'               => false,
        ]);
    }

    public function countBuildingUnitsByCreatedByAccountUser(Account $account, AccountUser $accountUser, bool $archived): int
    {
        return $this->buildingUnitRepository->count(criteria: [
            'account'              => $account,
            'createdByAccountUser' => $accountUser,
            'archived'             => $archived,
            'deleted'              => false,
        ]);
    }

    /**
     * @return BuildingUnit[]
     */
    public function fetchToBeDeletedBuildingUnitsFromAllAccounts(?int $timeTillHardDelete = null): array
    {
        return $this->buildingUnitRepository->findToBeDeletedFromAllAccounts(timeTillHardDelete: $timeTillHardDelete);
    }

    public function pushAllBuildingUnitsIntoEarlyWarningSystem(): void
    {
        $this->taodEarlyWarningSystemService->truncateBuildingUnitTable();

        $buildingUnitJsonRepresentations = [];

        foreach ($this->buildingUnitRepository->findBy(criteria: ['deleted' => false, 'archived' => false]) as $buildingUnit) {
            $buildingUnitJsonRepresentations[] = BuildingUnitFactory::createFromBuildingUnit(buildingUnit: $buildingUnit);
        }

        $this->taodEarlyWarningSystemService->createAndSaveBuildingUnitsFromBuildingUnitJsonRepresentations(buildingUnitJsonRepresentations: $buildingUnitJsonRepresentations);
    }
}
