<?php

declare(strict_types=1);

namespace App\Domain\Property;

use App\Domain\Account\AccountUserService;
use App\Domain\Entity\Account;
use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\AccountUser\AccountUserRole;
use App\Domain\Entity\AccountUser\AccountUserType;
use App\Domain\Entity\Notification\NotificationType;
use App\Domain\Entity\Property\BuildingUnit;
use App\Domain\Notification\NotificationService;

class BuildingUnitNotificationService
{
    public function __construct(
        private readonly BuildingUnitService $buildingUnitService,
        private readonly AccountUserService $accountUserService,
        private readonly NotificationService $notificationService
    ) {
    }

    public function createBuildingUnitBecameEmptyNotificationsByAccount(Account $account): void
    {
        $accountUsers = $this->fetchAccountUsers(account: $account);

        $notificationType = NotificationType::PROPERTY_BECAME_EMPTY;

        foreach ($this->buildingUnitService->fetchBuildingUnitsByBecameEmptyToday(account: $account, withFromSubAccounts: true) as $buildingUnit) {
            foreach ($accountUsers as $accountUser) {
                $notification = $this->notificationService->fetchNotificationByNotificationTypeAndObject(
                    accountUser: $accountUser,
                    notificationType: $notificationType,
                    object: $buildingUnit
                );

                if ($notification === null) {
                    $this->notificationService->createAndSaveNotification(
                        accountUser: $accountUser,
                        notificationType: $notificationType,
                        beenRead: false,
                        buildingUnit: $buildingUnit
                    );
                }
            }
        }
    }

    public function createBuildingUnitRentalAgreementExpiredNotificationsByAccount(Account $account): void
    {
        $accountUsers = $this->fetchAccountUsers(account: $account);

        $notificationType = NotificationType::PROPERTY_RENTAL_AGREEMENT_EXPIRED;

        foreach ($this->buildingUnitService->fetchBuildingUnitsByRentalAgreementExpiredToday(account: $account, withFromSubAccounts: true) as $buildingUnit) {
            foreach ($accountUsers as $accountUser) {
                $notification = $this->notificationService->fetchNotificationByNotificationTypeAndObject(
                    accountUser: $accountUser,
                    notificationType: $notificationType,
                    object: $buildingUnit
                );

                if ($notification === null) {
                    $this->notificationService->createAndSaveNotification(
                        accountUser: $accountUser,
                        notificationType: $notificationType,
                        beenRead: false,
                        buildingUnit: $buildingUnit
                    );
                }
            }
        }
    }

    public function createBuildingUnitNotUpdatedNotificationsByAccount(Account $account, int $notUpdatedInDays): void
    {
        $accountUsers = $this->fetchAccountUsers(account: $account);

        $notificationType = NotificationType::PROPERTY_NOT_UPDATED;

        foreach ($this->buildingUnitService->fetchBuildingUnitsByNotUpdatedInDays(account: $account, withFromSubAccounts: true, notUpdatedInDays: $notUpdatedInDays) as $buildingUnit) {
            foreach ($accountUsers as $accountUser) {
                $notification = $this->notificationService->fetchNotificationByNotificationTypeAndObject(
                    accountUser: $accountUser,
                    notificationType: $notificationType,
                    object: $buildingUnit
                );

                if ($notification === null) {
                    $this->notificationService->createAndSaveNotification(
                        accountUser: $accountUser,
                        notificationType: $notificationType,
                        beenRead: false,
                        buildingUnit: $buildingUnit
                    );
                }
            }
        }
    }

    public function createBuildingUnitNoMatchFoundNotificationsByAccount(Account $account, int $noMatchFoundInDays, int $intervalLength): void
    {
        $accountUsers = $this->fetchAccountUsers(account: $account);

        $notificationType = NotificationType::PROPERTY_NO_MATCH_FOUND;

        $buildingUnits = $this->buildingUnitService->fetchBuildingUnitsWithoutFoundMatching(
            account: $account,
            withFromSubAccounts: true,
            noMatchFoundInDays: $noMatchFoundInDays,
            intervalLength: $intervalLength
        );

        foreach ($buildingUnits as $buildingUnit) {
            foreach ($accountUsers as $accountUser) {
                $notification = $this->notificationService->fetchNotificationByNotificationTypeAndObject(
                    accountUser: $accountUser,
                    notificationType: $notificationType,
                    object: $buildingUnit
                );

                if ($notification === null) {
                    $this->notificationService->createAndSaveNotification(
                        accountUser: $accountUser,
                        notificationType: $notificationType,
                        beenRead: false,
                        buildingUnit: $buildingUnit
                    );
                }
            }
        }
    }

    public function createBuildingUnitNoMatchingExecutedNotificationsByAccount(Account $account, int $noMatchingExecutedInDays): void
    {
        $accountUsers = $this->fetchAccountUsers(account: $account);

        $notificationType = NotificationType::PROPERTY_NO_MATCHING_EXECUTED;

        $buildingUnits = $this->buildingUnitService->fetchBuildingUnitsWithoutMatchingExecuted(
            account: $account,
            withFromSubAccounts: true,
            noMatchingExecutedInDays: $noMatchingExecutedInDays
        );

        foreach ($buildingUnits as $buildingUnit) {
            foreach ($accountUsers as $accountUser) {
                $notification = $this->notificationService->fetchNotificationByNotificationTypeAndObject(
                    accountUser: $accountUser,
                    notificationType: $notificationType,
                    object: $buildingUnit
                );

                if ($notification === null) {
                    $this->notificationService->createAndSaveNotification(
                        accountUser: $accountUser,
                        notificationType: $notificationType,
                        beenRead: false,
                        buildingUnit: $buildingUnit
                    );
                }
            }
        }
    }

    public function createBuildingUnitUsageChangeNotifications(BuildingUnit $buildingUnit, AccountUser $changedByAccountUser): void
    {
        $account = $buildingUnit->getAccount();

        if ($account->getParentAccount() !== null) {
            $account = $account->getParentAccount();
        }

        $accountUsers = $this->fetchAccountUsers(account: $account);

        foreach ($accountUsers as $accountUser) {
            if ($accountUser === $changedByAccountUser) {
                continue;
            }

            $this->notificationService->createAndSaveNotification(
                accountUser: $accountUser,
                notificationType: NotificationType::PROPERTY_USAGE_CHANGE,
                beenRead: false,
                buildingUnit: $buildingUnit
            );
        }
    }

    /**
     * @return AccountUser[]
     */
    private function fetchAccountUsers(Account $account): array
    {
        return $this->accountUserService->fetchAccountUsersByAccount(
            account: $account,
            withFromSubAccounts: false,
            accountUserTypes: [AccountUserType::INTERNAL],
            accountUserRoles: [
                AccountUserRole::ROLE_ACCOUNT_ADMIN,
                AccountUserRole::ROLE_USER,
                AccountUserRole::ROLE_PROPERTY_FEEDER,
                AccountUserRole::ROLE_VIEWER,
                AccountUserRole::ROLE_RESTRICTED_VIEWER,
            ]
        );
    }
}
