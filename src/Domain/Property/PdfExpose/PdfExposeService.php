<?php

declare(strict_types=1);

namespace App\Domain\Property\PdfExpose;

use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\Property\BuildingUnit;
use App\Domain\OneTimeToken\OneTimeTokenService;
use Symfony\Component\Process\Process;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class PdfExposeService
{
    private readonly string $pdfExposeGeneratorScriptDirectory;
    private readonly string $pdfExposeGeneratorScript;

    public function __construct(
        private readonly string $appDefaultUri,
        private readonly string $defaultUriForPdfGenerator,
        private readonly string $projectDir,
        private readonly OneTimeTokenService $oneTimeTokenService,
        private readonly UrlGeneratorInterface $router
    ) {
        $this->pdfExposeGeneratorScriptDirectory = $this->projectDir . '/assets/lean/js/property_vacancy_management/building_unit/pdf_expose';
        $this->pdfExposeGeneratorScript = 'pdf_expose.js';
    }

    public function generateBuildingUnitPdfExpose(BuildingUnit $buildingUnit, AccountUser $accountUser): string
    {
        $oneTimeTokenData = [
            'identifier'     => 'buildingUnitPdfExpose',
            'buildingUnitId' => $buildingUnit->getId(),
            'accountUserId'  => $accountUser->getId(),
        ];

        $pdfHeaderOneTimeToken = $this->oneTimeTokenService->createAndSaveOneTimeToken(data: $oneTimeTokenData);
        $pdfContentOneTimeToken = $this->oneTimeTokenService->createAndSaveOneTimeToken(data: $oneTimeTokenData);

        $pdfHeaderUrlPath = $this->router->generate(
            name: 'property_vacancy_management_building_unit_pdf_expose_header',
            parameters: ['token' => $pdfHeaderOneTimeToken->getToken()->toRfc4122()]
        );

        $pdfContentUrlPath = $this->router->generate(
            name: 'property_vacancy_management_building_unit_pdf_expose_content',
            parameters: ['token' => $pdfContentOneTimeToken->getToken()->toRfc4122()]
        );

        if (empty($this->defaultUriForPdfGenerator) === false) {
            $schemeAndHostname = $this->defaultUriForPdfGenerator;
        } else {
            $schemeAndHostname = $this->appDefaultUri;
        }

        $process = new Process(
            command: ['node', $this->pdfExposeGeneratorScript, $schemeAndHostname, $pdfHeaderUrlPath, $pdfContentUrlPath],
            cwd: $this->pdfExposeGeneratorScriptDirectory
        );

        $process->run();

        if ($process->isSuccessful() === false) {
            throw new \RuntimeException($process->getErrorOutput());
        }

        return $process->getOutput();
    }
}
