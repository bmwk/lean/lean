<?php

declare(strict_types=1);

namespace App\Domain\Property;

use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\AccountUser\AccountUserRole;
use App\Domain\Entity\Property\PropertyOwner;
use Symfony\Bundle\SecurityBundle\Security;

class PropertyOwnerSecurityService
{
    public function __construct(
        private readonly Security $security
    ) {
    }

    public function canViewPropertyOwner(PropertyOwner $propertyOwner, AccountUser $accountUser): bool
    {
        $account = $accountUser->getAccount();

        if (
            $account->getParentAccount() === null
            && (
                $this->security->isGranted(attributes: AccountUserRole::ROLE_USER->value) === true
                || $this->security->isGranted(attributes: AccountUserRole::ROLE_PROPERTY_FEEDER->value) === true
                || $this->security->isGranted(attributes: AccountUserRole::ROLE_VIEWER->value) === true
                || $this->security->isGranted(attributes: AccountUserRole::ROLE_RESTRICTED_VIEWER->value) === true
            )
            && (
                $propertyOwner->getAccount() === $account
                || $account->getSubAccounts()->contains(element: $propertyOwner->getAccount()) === true
            )
        ) {
            return true;
        }

        return false;
    }

    public function canEditPropertyOwner(PropertyOwner $propertyOwner, AccountUser $accountUser): bool
    {
        $account = $accountUser->getAccount();

        if (
            $account->getParentAccount() === null
            && (
                $this->security->isGranted(attributes: AccountUserRole::ROLE_USER->value) === true
                || $this->security->isGranted(attributes: AccountUserRole::ROLE_PROPERTY_FEEDER->value) === true
            )
            && (
                $propertyOwner->getAccount() === $account
                || $account->getSubAccounts()->contains(element: $propertyOwner->getAccount()) === true
            )
        ) {
            return true;
        }

        return false;
    }

    public function canDeletePropertyOwner(PropertyOwner $propertyOwner, AccountUser $accountUser): bool
    {
        return $this->canEditPropertyOwner(propertyOwner: $propertyOwner, accountUser: $accountUser);
    }
}
