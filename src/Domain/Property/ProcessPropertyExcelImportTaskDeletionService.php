<?php

declare(strict_types=1);

namespace App\Domain\Property;

use App\Domain\Entity\Account;
use App\Domain\Entity\ProcessPropertyExcelImportTask;
use App\Repository\ProcessPropertyExcelImportTaskRepository;
use Doctrine\ORM\EntityManagerInterface;

class ProcessPropertyExcelImportTaskDeletionService
{
    public function __construct(
        private readonly ProcessPropertyExcelImportTaskRepository $processPropertyExcelImportTaskRepository,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function deleteProcessPropertyExcelImportTask(ProcessPropertyExcelImportTask $processPropertyExcelImportTask): void
    {
        $this->hardDeleteProcessPropertyExcelImportTask(processPropertyExcelImportTask: $processPropertyExcelImportTask);
    }

    public function deleteProcessPropertyExcelImportTasksByAccount(Account $account): void
    {
        $this->hardDeleteProcessPropertyExcelImportTasksByAccount(account: $account);
    }

    private function hardDeleteProcessPropertyExcelImportTask(ProcessPropertyExcelImportTask $processPropertyExcelImportTask): void
    {
        $this->entityManager->remove($processPropertyExcelImportTask);
        $this->entityManager->flush();
    }

    private function hardDeleteProcessPropertyExcelImportTasksByAccount(Account $account): void
    {
        $processPropertyExcelImportTasks = $this->processPropertyExcelImportTaskRepository->findBy(criteria: ['account' => $account]);

        foreach ($processPropertyExcelImportTasks as $processPropertyExcelImportTask) {
            $this->hardDeleteProcessPropertyExcelImportTask(processPropertyExcelImportTask: $processPropertyExcelImportTask);
        }
    }
}
