<?php

declare(strict_types=1);

namespace App\Domain\Property;

use App\Domain\Classification\ClassificationService;
use App\Domain\Entity\Account;
use App\Domain\Entity\Statistics\AmountByIndustryClassification;
use App\Repository\Property\BuildingUnitRepository;

class BuildingUnitStatisticsService
{
    public function __construct(
        private readonly BuildingUnitRepository $buildingUnitRepository,
        private readonly ClassificationService $classificationService
    ) {
    }

    /**
     * @return AmountByIndustryClassification[]
     */
    public function buildAmountBuildingUnitsByCurrentUsage(Account $account, bool $withFromSubAccounts): array
    {
        $amountByIndustryClassifications = [];

        $amountByIndustryClassifications[] = new AmountByIndustryClassification(
            amount: $this->buildingUnitRepository->countByCurrentUsage(
                account: $account,
                withFromSubAccounts: $withFromSubAccounts,
                industryClassification: null
            ),
            industryClassification: null
        );

        foreach ($this->classificationService->fetchTopLevelIndustryClassifications() as $industryClassification) {
            $amountByIndustryClassifications[] = new AmountByIndustryClassification(
                amount: $this->buildingUnitRepository->countByCurrentUsage(
                    account: $account,
                    withFromSubAccounts: $withFromSubAccounts,
                    industryClassification: $industryClassification
                ),
                industryClassification: $industryClassification
            );
        }

        return $amountByIndustryClassifications;
    }
}
