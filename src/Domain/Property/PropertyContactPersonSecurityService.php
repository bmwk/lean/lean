<?php

declare(strict_types=1);

namespace App\Domain\Property;

use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\AccountUser\AccountUserRole;
use App\Domain\Entity\Property\PropertyContactPerson;
use Symfony\Bundle\SecurityBundle\Security;

class PropertyContactPersonSecurityService
{
    public function __construct(
        private readonly Security $security
    ) {
    }

    public function canViewPropertyContactPerson(PropertyContactPerson $propertyContactPerson, AccountUser $accountUser): bool
    {
        $account = $accountUser->getAccount();

        if (
            $account->getParentAccount() === null
            && (
                $this->security->isGranted(attributes: AccountUserRole::ROLE_USER->value) === true
                || $this->security->isGranted(attributes: AccountUserRole::ROLE_PROPERTY_FEEDER->value) === true
                || $this->security->isGranted(attributes: AccountUserRole::ROLE_VIEWER->value) === true
                || $this->security->isGranted(attributes: AccountUserRole::ROLE_RESTRICTED_VIEWER->value) === true
            )
            && (
                $propertyContactPerson->getAccount() === $account
                || $account->getSubAccounts()->contains(element: $propertyContactPerson->getAccount()) === true
            )
        ) {
            return true;
        }

        return false;
    }

    public function canEditPropertyContactPerson(PropertyContactPerson $propertyContactPerson, AccountUser $accountUser): bool
    {
        $account = $accountUser->getAccount();

        if (
            $account->getParentAccount() === null
            && (
                $this->security->isGranted(attributes: AccountUserRole::ROLE_USER->value) === true
                || $this->security->isGranted(attributes: AccountUserRole::ROLE_PROPERTY_FEEDER->value) === true
            )
            && (
                $propertyContactPerson->getAccount() === $account
                || $account->getSubAccounts()->contains(element: $propertyContactPerson->getAccount()) === true
            )
        ) {
            return true;
        }

        return false;
    }

    public function canDeletePropertyContactPerson(PropertyContactPerson $propertyContactPerson, AccountUser $accountUser): bool
    {
        return $this->canEditPropertyContactPerson(propertyContactPerson: $propertyContactPerson, accountUser: $accountUser);
    }
}
