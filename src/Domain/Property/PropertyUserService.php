<?php

declare(strict_types=1);

namespace App\Domain\Property;

use App\Domain\Classification\ClassificationService;
use App\Domain\Entity\Account;
use App\Domain\Entity\Person\Person;
use App\Domain\Entity\Property\BuildingUnit;
use App\Domain\Entity\Property\PropertyUser;
use App\Repository\Property\PropertyUserRepository;
use Symfony\Component\Form\FormInterface;

class PropertyUserService
{
    public function __construct(
        private readonly PropertyUserRepository $propertyUserRepository,
        private readonly ClassificationService $classificationService
    ) {
    }

    public function fetchPropertyUserByIdAndBuildingUnit(
        Account $account,
        bool $withFromSubAccounts,
        int $id,
        BuildingUnit $buildingUnit
    ): ?PropertyUser {
        return $this->propertyUserRepository->findOneByIdAndBuildingUnit(
            account: $account,
            withFromSubAccounts: $withFromSubAccounts,
            id: $id,
            buildingUnit: $buildingUnit
        );
    }

    public function fetchPropertyUserByBuildingUnitAndPerson(
        Account $account,
        bool $withFromSubAccounts,
        BuildingUnit $buildingUnit,
        Person $person
    ): ?PropertyUser {
        return $this->propertyUserRepository->findOneByBuildingUnitAndPerson(
            account: $account,
            withFromSubAccounts: $withFromSubAccounts,
            buildingUnit: $buildingUnit,
            person: $person
        );
    }

    public function createPropertyUserFromPropertyUserForm(FormInterface $propertyUserForm): PropertyUser
    {
        $propertyUser = $propertyUserForm->getData();

        $industryNaceClassificationForm = $propertyUserForm->get('industryNaceClassification');

        $industryClassification = $this->classificationService->fetchIndustryClassificationByIndustryNaceClassificationForm(
            industryNaceClassificationForm: $industryNaceClassificationForm
        );

        $naceClassification = $this->classificationService->fetchNaceClassificationByIndustryNaceClassificationForm(
            industryNaceClassificationForm: $industryNaceClassificationForm
        );

        $propertyUser
            ->setIndustryClassification($industryClassification)
            ->setNaceClassification($naceClassification);

        return $propertyUser;
    }
}
