<?php

declare(strict_types=1);

namespace App\Domain\Property;

use App\Domain\Entity\Account;
use App\Domain\Entity\Property\PropertyOwner;
use App\Repository\Property\PropertyOwnerRepository;
use Doctrine\ORM\EntityManagerInterface;

class PropertyOwnerDeletionService
{
    public function __construct(
        private readonly PropertyOwnerRepository $propertyOwnerRepository,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function deletePropertyOwner(PropertyOwner $propertyOwner): void
    {
        $this->hardDeletePropertyOwner(propertyOwner: $propertyOwner);
    }

    public function deletePropertyOwnersByAccount(Account $account): void
    {
        $this->hardDeletePropertyOwnersByAccount(account: $account);
    }

    private function hardDeletePropertyOwner(PropertyOwner $propertyOwner): void
    {
        $this->entityManager->remove(entity: $propertyOwner);

        $this->entityManager->flush();
    }

    private function hardDeletePropertyOwnersByAccount(Account $account): void
    {
        $propertyOwners = $this->propertyOwnerRepository->findBy(criteria: ['account' => $account]);

        foreach ($propertyOwners as $propertyOwner) {
            $this->hardDeletePropertyOwner(propertyOwner: $propertyOwner);
        }
    }
}
