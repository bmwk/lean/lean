<?php

declare(strict_types=1);

namespace App\Domain\Property;

use App\Domain\Document\DocumentDeletionService;
use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\DeletionType;
use App\Domain\Entity\Property\Building;
use App\Domain\Image\ImageDeletionService;
use App\Domain\Location\LocationDeletionService;
use Doctrine\ORM\EntityManagerInterface;

class BuildingDeletionService extends AbstractPropertyDeletionService
{
    public function __construct(
        DocumentDeletionService $documentDeletionService,
        ImageDeletionService $imageDeletionService,
        PropertyUserDeletionService $propertyUserDeletionService,
        PropertyOwnerDeletionService $propertyOwnerDeletionService,
        LocationDeletionService $locationDeletionService,
        EntityManagerInterface $entityManager
    ) {
        parent::__construct(
            documentDeletionService: $documentDeletionService,
            imageDeletionService: $imageDeletionService,
            propertyUserDeletionService: $propertyUserDeletionService,
            propertyOwnerDeletionService: $propertyOwnerDeletionService,
            locationDeletionService: $locationDeletionService,
            entityManager: $entityManager
        );
    }

    public function deleteBuilding(
        Building $building,
        DeletionType $deletionType = DeletionType::SOFT,
        ?AccountUser $deletedByAccountUser = null
    ): void {
        switch ($deletionType) {
            case DeletionType::SOFT:
                $this->softDeleteBuilding(building: $building, deletedByAccountUser: $deletedByAccountUser);
                break;
            case DeletionType::HARD:
                $this->hardDeleteBuilding(building: $building);
                break;
            default:
                throw new \RuntimeException(message: 'Unsupported DeletionType');
        }
    }

    private function softDeleteBuilding(Building $building, AccountUser $deletedByAccountUser): void
    {
        $building
            ->setDeleted(deleted: true)
            ->setDeletedAt(deletedAt: new \DateTimeImmutable())
            ->setDeletedByAccountUser(deletedByAccountUser: $deletedByAccountUser);

        $this->entityManager->flush();
    }

    private function hardDeleteBuilding(Building $building): void
    {
        if ($building->getMainImage() !== null) {
            $this->deleteMainImage(property: $building);
        }

        if ($building->getGeolocationPoint() !== null) {
            $this->deleteGeolocationPoint(property: $building);
        }

        if ($building->getGeolocationPolygon() !== null) {
            $this->deleteGeolocationPolygon(property: $building);
        }

        if ($building->getGeolocationMultiPolygon() !== null) {
            $this->deleteGeolocationMultiPolygon(property: $building);
        }

        $this->deleteAllImages(property: $building);

        $this->deleteAllDocuments(property: $building);

        $this->deleteAllPropertyUsers(property: $building);

        $this->deleteAllPropertyOwners(property: $building);

        $address = $building->getAddress();

        $this->entityManager->remove(entity: $building);

        $this->entityManager->flush();

        $this->deleteAddress(address: $address);

        $this->deletePropertySpacesData(property: $building);
    }
}
