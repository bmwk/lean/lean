<?php

declare(strict_types=1);

namespace App\Domain\Property;

use App\Domain\Document\DocumentDeletionService;
use App\Domain\Entity\Document;
use App\Domain\Entity\Image;
use App\Domain\Entity\Property\AbstractProperty;
use App\Domain\Entity\Property\Address;
use App\Domain\Entity\Property\PropertyOwner;
use App\Domain\Entity\Property\PropertyUser;
use App\Domain\Image\ImageDeletionService;
use App\Domain\Location\LocationDeletionService;
use Doctrine\ORM\EntityManagerInterface;

abstract class AbstractPropertyDeletionService
{
    public function __construct(
        protected readonly DocumentDeletionService $documentDeletionService,
        protected readonly ImageDeletionService $imageDeletionService,
        protected readonly PropertyUserDeletionService $propertyUserDeletionService,
        protected readonly PropertyOwnerDeletionService $propertyOwnerDeletionService,
        protected readonly LocationDeletionService $locationDeletionService,
        protected readonly EntityManagerInterface $entityManager
    ) {
    }

    public function deleteAddress(Address $address): void
    {
        $this->entityManager->remove(entity: $address);

        $this->entityManager->flush();
    }

    public function deletePropertySpacesData(AbstractProperty $property): void
    {
        $propertySpacesData = $property->getPropertySpacesData();

        $this->entityManager->remove(entity: $propertySpacesData);

        $this->entityManager->flush();
    }

    public function deleteMainImage(AbstractProperty $property): void
    {
        $mainImage = $property->getMainImage();

        if ($mainImage === null) {
            return;
        }

        $property->setMainImage(mainImage: null);

        $this->entityManager->flush();

        $this->imageDeletionService->deleteImage(image: $mainImage);
    }

    public function deleteGeolocationPoint(AbstractProperty $property): void
    {
        $geolocationPoint = $property->getGeolocationPoint();

        if ($geolocationPoint === null) {
            return;
        }

        $property->setGeolocationPoint(geolocationPoint: null);

        $this->entityManager->flush();

        $this->locationDeletionService->deleteGeolocationPoint(geolocationPoint: $geolocationPoint);
    }

    public function deleteGeolocationPolygon(AbstractProperty $property): void
    {
        $geolocationPolygon = $property->getGeolocationPolygon();

        if ($geolocationPolygon === null) {
            return;
        }

        $property->setGeolocationPolygon(geolocationPolygon: null);

        $this->entityManager->flush();

        $this->locationDeletionService->deleteGeolocationPolygon(geolocationPolygon: $geolocationPolygon);
    }

    public function deleteGeolocationMultiPolygon(AbstractProperty $property): void
    {
        $geolocationMultiPolygon = $property->getGeolocationMultiPolygon();

        if ($geolocationMultiPolygon === null) {
            return;
        }

        $property->setGeolocationMultiPolygon(geolocationMultiPolygon: null);

        $this->entityManager->flush();

        $this->locationDeletionService->deleteGeolocationMultiPolygon(geolocationMultiPolygon: $geolocationMultiPolygon);
    }

    public function deleteImage(Image $image, AbstractProperty $property): void
    {
        $this->hardDeleteImage(image: $image, property: $property);
    }

    public function deleteAllImages(AbstractProperty $property): void
    {
        $this->hardDeleteAllImages(property: $property);
    }

    public function deleteDocument(Document $document, AbstractProperty $property): void
    {
        $this->hardDeleteDocument(document: $document, property: $property);
    }

    public function deleteAllDocuments(AbstractProperty $property): void
    {
        $this->hardDeleteAllDocuments(property: $property);
    }

    public function deletePropertyUser(PropertyUser $propertyUser, AbstractProperty $property): void
    {
        $this->hardDeletePropertyUser(propertyUser: $propertyUser, property: $property);
    }

    public function deleteAllPropertyUsers(AbstractProperty $property): void
    {
        $this->hardDeleteAllPropertyUsers(property: $property);
    }

    public function deletePropertyOwner(PropertyOwner $propertyOwner, AbstractProperty $property): void
    {
        $this->hardDeletePropertyOwner(propertyOwner: $propertyOwner, property: $property);
    }

    public function deleteAllPropertyOwners(AbstractProperty $property): void
    {
        $this->hardDeleteAllPropertyOwners(property: $property);
    }

    private function hardDeleteImage(Image $image, AbstractProperty $property): void
    {
        if ($property->getMainImage() === $image) {
            $property->setMainImage(mainImage: null);
        }

        if ($property->getImages()->contains($image)) {
            $property->getImages()->removeElement($image);
        }

        $this->entityManager->flush();

        $this->imageDeletionService->deleteImage(image: $image);
    }

    private function hardDeleteAllImages(AbstractProperty $property): void
    {
        foreach ($property->getImages() as $image) {
            $this->hardDeleteImage(image: $image, property: $property);
        }
    }

    private function hardDeleteDocument(Document $document, AbstractProperty $property): void
    {
        if ($property->getDocuments()->contains(element: $document)) {
            $property->getDocuments()->removeElement(element: $document);
        }

        $this->entityManager->flush();

        $this->documentDeletionService->deleteDocument(document: $document);
    }

    private function hardDeleteAllDocuments(AbstractProperty $property): void
    {
        foreach ($property->getDocuments() as $document) {
            $this->hardDeleteDocument(document: $document, property: $property);
        }
    }

    private function hardDeletePropertyUser(PropertyUser $propertyUser, AbstractProperty $property): void
    {
        if ($property->getPropertyUsers()->contains(element: $propertyUser)) {
            $property->getPropertyUsers()->removeElement(element: $propertyUser);
        }

        $this->entityManager->flush();

        $this->propertyUserDeletionService->deletePropertyUser(propertyUser: $propertyUser);
    }

    private function hardDeleteAllPropertyUsers(AbstractProperty $property): void
    {
        foreach ($property->getPropertyUsers() as $propertyUser) {
            $this->hardDeletePropertyUser(propertyUser: $propertyUser, property: $property);
        }
    }

    private function hardDeletePropertyOwner(PropertyOwner $propertyOwner, AbstractProperty $property): void
    {
        if ($property->getPropertyOwners()->contains(element: $propertyOwner)) {
            $property->getPropertyOwners()->removeElement(element: $propertyOwner);
        }

        $this->entityManager->flush();

        $this->propertyOwnerDeletionService->deletePropertyOwner(propertyOwner: $propertyOwner);
    }

    private function hardDeleteAllPropertyOwners(AbstractProperty $property): void
    {
        foreach ($property->getPropertyOwners() as $propertyOwner) {
            $this->hardDeletePropertyOwner(propertyOwner: $propertyOwner, property: $property);
        }
    }
}
