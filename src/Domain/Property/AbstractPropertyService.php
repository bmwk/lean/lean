<?php

declare(strict_types=1);

namespace App\Domain\Property;

use App\Domain\Document\DocumentService;
use App\Domain\Entity\Property\Address;
use App\Domain\Image\ImageService;
use App\Domain\Location\LocationService;
use App\GoogleMaps\GoogleMapsService;
use Doctrine\ORM\EntityManagerInterface;

abstract class AbstractPropertyService
{
    protected function __construct(
        protected readonly ImageService $imageService,
        protected readonly DocumentService $documentService,
        protected readonly LocationService $locationService,
        protected readonly GoogleMapsService $googleMapsService,
        protected readonly EntityManagerInterface $entityManager
    ) {
    }

    public function queryGoogleMapsByLatitudeAndLongitudeToFillAddress(float $latitude, float $longitude, Address $address): void
    {
        if ($this->googleMapsService->isServiceAvailable() === false) {
            return;
        }

        $addressComponents = GoogleMapsService::fetchAddressComponentsFromApiResult(
            apiResult: $this->googleMapsService->fetchAddressByLatitudeAndLongitude(
                latitude: $latitude,
                longitude: $longitude
            )
        );

        if ($addressComponents === null) {
            return;
        }

        $streetName = GoogleMapsService::fetchStreetNameFromAddressComponents(addressComponents: $addressComponents);
        $streetNumber = GoogleMapsService::fetchStreetNumberFromAddressComponents(addressComponents: $addressComponents);
        $postalCode = GoogleMapsService::fetchPostalCodeFromAddressComponents(addressComponents: $addressComponents);

        if ($streetName !== null) {
            $address->setStreetName($streetName);
        }

        if ($streetNumber !== null) {
            $address->setHouseNumber($streetNumber);
        }

        if ($postalCode !== null) {
            $address->setPostalCode($postalCode);
        }
    }
}
