<?php

declare(strict_types=1);

namespace App\Domain\Property;

use App\Domain\Entity\Account;
use App\Domain\Entity\Property\PropertyUser;
use App\Repository\Property\PropertyUserRepository;
use Doctrine\ORM\EntityManagerInterface;

class PropertyUserDeletionService
{
    public function __construct(
        private readonly PropertyUserRepository $propertyUserRepository,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function deletePropertyUser(PropertyUser $propertyUser): void
    {
        $this->hardDeletePropertyUser(propertyUser: $propertyUser);
    }

    public function deletePropertyUsersByAccount(Account $account): void
    {
        $this->hardDeletePropertyUsersByAccount(account: $account);
    }

    private function hardDeletePropertyUser(PropertyUser $propertyUser): void
    {
        $this->entityManager->remove(entity: $propertyUser);

        $this->entityManager->flush();
    }

    private function hardDeletePropertyUsersByAccount(Account $account): void
    {
        $propertyUsers = $this->propertyUserRepository->findBy(criteria: ['account' => $account]);

        foreach ($propertyUsers as $propertyUser) {
            $this->hardDeletePropertyUser(propertyUser: $propertyUser);
        }
    }
}
