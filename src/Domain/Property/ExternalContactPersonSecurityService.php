<?php

declare(strict_types=1);

namespace App\Domain\Property;

use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\AccountUser\AccountUserRole;
use App\Domain\Entity\Property\ExternalContactPerson;
use Symfony\Bundle\SecurityBundle\Security;

class ExternalContactPersonSecurityService
{
    public function __construct(
        private readonly Security $security
    ) {
    }

    public function canViewExternalContactPerson(ExternalContactPerson $externalContactPerson, AccountUser $accountUser): bool
    {
        $account = $accountUser->getAccount();

        $buildingUnit = $externalContactPerson->getBuildingUnit();

        if (
            $account->getParentAccount() === null
            && (
                $this->security->isGranted(attributes: AccountUserRole::ROLE_USER->value) === true
                || $this->security->isGranted(attributes: AccountUserRole::ROLE_PROPERTY_FEEDER->value) === true
                || $this->security->isGranted(attributes: AccountUserRole::ROLE_VIEWER->value) === true
            )
            && (
                $buildingUnit->getAccount() === $account
                || $account->getSubAccounts()->contains(element: $buildingUnit->getAccount()) === true
            )
        ) {
            return true;
        }

        if (
            $account->getParentAccount() !== null
            && $this->security->isGranted(attributes: AccountUserRole::ROLE_PROPERTY_PROVIDER->value) === true
            && $buildingUnit->getAccount() === $account
        ) {
            return true;
        }

        return false;
    }

    public function canEditExternalContactPerson(ExternalContactPerson $externalContactPerson, AccountUser $accountUser): bool
    {
        $account = $accountUser->getAccount();

        $buildingUnit = $externalContactPerson->getBuildingUnit();

        if (
            $account->getParentAccount() !== null
            && $this->security->isGranted(attributes: AccountUserRole::ROLE_PROPERTY_PROVIDER->value) === true
            && $buildingUnit->getAccount() === $account
            && $buildingUnit->getOpenImmoAdditionalInformation() === null
        ) {
            return true;
        }

        return false;
    }

    public function canDeleteExternalContactPerson(ExternalContactPerson $externalContactPerson, AccountUser $accountUser): bool
    {
        return $this->canEditExternalContactPerson(externalContactPerson: $externalContactPerson, accountUser: $accountUser);
    }
}
