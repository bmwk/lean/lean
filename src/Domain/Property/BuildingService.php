<?php

declare(strict_types=1);

namespace App\Domain\Property;

use App\Domain\Document\DocumentService;
use App\Domain\Image\ImageService;
use App\Domain\Location\LocationService;
use App\GoogleMaps\GoogleMapsService;
use Doctrine\ORM\EntityManagerInterface;

class BuildingService extends AbstractPropertyService
{
    public function __construct(
        ImageService $imageService,
        DocumentService $documentService,
        LocationService $locationService,
        GoogleMapsService $googleMapsService,
        EntityManagerInterface $entityManager
    ) {
        parent::__construct(
            imageService: $imageService,
            documentService: $documentService,
            locationService: $locationService,
            googleMapsService: $googleMapsService,
            entityManager: $entityManager
        );
    }
}
