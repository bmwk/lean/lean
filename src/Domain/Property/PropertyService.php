<?php

declare(strict_types=1);

namespace App\Domain\Property;

use App\Domain\Classification\ClassificationService;
use App\Domain\Entity\Account;
use App\Domain\Entity\PropertyVacancyManagementStatistics;
use App\Repository\Property\BuildingUnitRepository;

class PropertyService
{
    public function __construct(
        private readonly BuildingUnitRepository $buildingUnitRepository,
        private readonly ClassificationService $classificationService,
    ) {
    }

    public function fetchPropertyVacancyManagementStatisticsByAccount(Account $account, bool $archived): PropertyVacancyManagementStatistics
    {
        $propertyVacancyManagementStatistics = new PropertyVacancyManagementStatistics();

        $retailIndustryClassification = $this->classificationService->fetchIndustryClassificationByLevels(
            levelOne: 1,
            levelTwo: null,
            levelThree: null
        );

        $serviceIndustryClassification = $this->classificationService->fetchIndustryClassificationByLevels(
            levelOne: 2,
            levelTwo: null,
            levelThree: null
        );

        $gastronomyIndustryClassification = $this->classificationService->fetchIndustryClassificationByLevels(
            levelOne: 3,
            levelTwo: null,
            levelThree: null
        );

        $artCultureIndustryClassification = $this->classificationService->fetchIndustryClassificationByLevels(
            levelOne: 4,
            levelTwo: null,
            levelThree: null
        );

        $propertyVacancyManagementStatistics
            ->setEmpty(
                $this->buildingUnitRepository->count(criteria: [
                    'account'       => $account,
                    'archived'      => $archived,
                    'deleted'       => false,
                    'objectIsEmpty' => true,
                ])
            )
            ->setBecomesEmpty(
                $this->buildingUnitRepository->count(criteria: [
                    'account'            => $account,
                    'archived'           => $archived,
                    'deleted'            => false,
                    'objectBecomesEmpty' => true,
                ])
            )
            ->setReuseAgreed(
                $this->buildingUnitRepository->count(criteria: [
                    'account'     => $account,
                    'archived'    => $archived,
                    'deleted'     => false,
                    'reuseAgreed' => true,
                ])
            )
            ->setInUse(
                $this->buildingUnitRepository->count(criteria: [
                    'account'            => $account,
                    'archived'           => $archived,
                    'deleted'            => false,
                    'objectIsEmpty'      => false,
                    'objectBecomesEmpty' => false,
                    'reuseAgreed'        => false,
                ])
            )
            ->setRetail(
                $this->buildingUnitRepository->count(criteria: [
                    'account'                            => $account,
                    'archived'                           => $archived,
                    'deleted'                            => false,
                    'objectIsEmpty'                      => false,
                    'reuseAgreed'                        => false,
                    'currentUsageIndustryClassification' => $retailIndustryClassification,
                ])
            )
            ->setService(
                $this->buildingUnitRepository->count(criteria: [
                    'account'                            => $account,
                    'archived'                           => $archived,
                    'deleted'                            => false,
                    'objectIsEmpty'                      => false,
                    'reuseAgreed'                        => false,
                    'currentUsageIndustryClassification' => $serviceIndustryClassification,
                ])
            )->setGastronomy(
                $this->buildingUnitRepository->count(criteria: [
                    'account'                            => $account,
                    'archived'                           => $archived,
                    'deleted'                            => false,
                    'objectIsEmpty'                      => false,
                    'reuseAgreed'                        => false,
                    'currentUsageIndustryClassification' => $gastronomyIndustryClassification,
                ])
            )->setArtCulture(
                $this->buildingUnitRepository->count(criteria: [
                    'account'                            => $account,
                    'archived'                           => $archived,
                    'deleted'                            => false,
                    'objectIsEmpty'                      => false,
                    'reuseAgreed'                        => false,
                    'currentUsageIndustryClassification' => $artCultureIndustryClassification,
                ])
            )
            ->setOther(
                $propertyVacancyManagementStatistics->getInUse()
                + $propertyVacancyManagementStatistics->getBecomesEmpty()
                - $propertyVacancyManagementStatistics->getRetail()
                - $propertyVacancyManagementStatistics->getService()
                - $propertyVacancyManagementStatistics->getGastronomy()
                - $propertyVacancyManagementStatistics->getArtCulture()
            );

        return $propertyVacancyManagementStatistics;
    }
}
