<?php

declare(strict_types=1);

namespace App\Domain\Property;

use App\Domain\Document\DocumentDeletionService;
use App\Domain\Entity\Account;
use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\BuildingUnitFeedback;
use App\Domain\Entity\DeletionType;
use App\Domain\Entity\Image;
use App\Domain\Entity\Property\AbstractProperty;
use App\Domain\Entity\Property\BuildingUnit;
use App\Domain\Entity\Property\PropertyContactPerson;
use App\Domain\Image\ImageDeletionService;
use App\Domain\Location\LocationDeletionService;
use App\Domain\LookingForPropertyRequest\LookingForPropertyRequestDeletionService;
use App\Domain\LookingForPropertyRequest\Matching\MatchingDeletionService;
use App\Domain\LookingForPropertyRequest\Matching\MatchingTaskDeletionService;
use App\Domain\Notification\NotificationDeletionService;
use App\Domain\OpenImmoExporter\OpenImmoExportDeletionService;
use App\Domain\OpenImmoFtpTransfer\OpenImmoTransferLogDeletionService;
use App\Domain\SettlementConcept\SettlementConceptExposeForwardingDeletionService;
use App\Domain\SettlementConcept\SettlementConceptMatchingResponseDeletionService;
use App\Domain\SettlementConcept\SettlementConceptMatchingResultDeletionService;
use App\Domain\SettlementConcept\SettlementConceptMatchingTaskDeletionService;
use App\Repository\BuildingUnitFeedbackRepository;
use App\Repository\Property\BuildingUnitRepository;
use Doctrine\ORM\EntityManagerInterface;

class BuildingUnitDeletionService extends AbstractPropertyDeletionService
{
    public function __construct(
        DocumentDeletionService $documentDeletionService,
        ImageDeletionService $imageDeletionService,
        PropertyUserDeletionService $propertyUserDeletionService,
        PropertyOwnerDeletionService $propertyOwnerDeletionService,
        LocationDeletionService $locationDeletionService,
        EntityManagerInterface $entityManager,
        private readonly OpenImmoExportDeletionService $openImmoExportDeletionService,
        private readonly BuildingDeletionService $buildingDeletionService,
        private readonly PropertyContactPersonDeletionService $propertyContactPersonDeletionService,
        private readonly LookingForPropertyRequestDeletionService $lookingForPropertyRequestDeletionService,
        private readonly MatchingDeletionService $matchingDeletionService,
        private readonly MatchingTaskDeletionService $matchingTaskDeletionService,
        private readonly SettlementConceptExposeForwardingDeletionService $settlementConceptExposeForwardingDeletionService,
        private readonly SettlementConceptMatchingResponseDeletionService $settlementConceptMatchingResponseDeletionService,
        private readonly SettlementConceptMatchingResultDeletionService $settlementConceptMatchingResultDeletionService,
        private readonly SettlementConceptMatchingTaskDeletionService $settlementConceptMatchingTaskDeletionService,
        private readonly NotificationDeletionService $notificationDeletionService,
        private readonly OpenImmoTransferLogDeletionService $openImmoTransferLogDeletionService,
        private readonly BuildingUnitFeedbackRepository $buildingUnitFeedbackRepository,
        private readonly BuildingUnitRepository $buildingUnitRepository
    ) {
        parent::__construct(
            documentDeletionService: $documentDeletionService,
            imageDeletionService: $imageDeletionService,
            propertyUserDeletionService: $propertyUserDeletionService,
            propertyOwnerDeletionService: $propertyOwnerDeletionService,
            locationDeletionService: $locationDeletionService,
            entityManager: $entityManager
        );
    }

    public function deleteBuildingUnit(
        BuildingUnit $buildingUnit,
        DeletionType $deletionType = DeletionType::SOFT,
        ?AccountUser $deletedByAccountUser = null
    ): void {
        switch ($deletionType) {
            case DeletionType::SOFT:
                $this->softDeleteBuildingUnit(buildingUnit: $buildingUnit, deletedByAccountUser: $deletedByAccountUser);
                break;
            case DeletionType::HARD:
                $this->hardDeleteBuildingUnit(buildingUnit: $buildingUnit);
                break;
            default:
                throw new \RuntimeException(message: 'Unsupported DeletionType');
        }
    }

    public function deleteBuildingUnitsByAccount(
        Account $account,
        DeletionType $deletionType = DeletionType::SOFT,
        ?AccountUser $deletedByAccountUser = null
    ): void {
        switch ($deletionType) {
            case DeletionType::SOFT:
                $this->softDeleteBuildingUnitsByAccount(account: $account, deletedByAccountUser: $deletedByAccountUser);
                break;
            case DeletionType::HARD:
                $this->hardDeleteBuildingUnitsByAccount(account: $account);
                break;
            default:
                throw new \RuntimeException(message: 'Unsupported DeletionType');
        }
    }

    public function deleteImage(Image $image, AbstractProperty $property): void
    {
        $property->setMainImage(null);

        parent::deleteImage(image: $image, property: $property);
    }

    public function deleteEnergyCertificate(BuildingUnit $buildingUnit): void
    {
        $energyCertificate = $buildingUnit->getEnergyCertificate();

        if ($energyCertificate === null) {
            return;
        }

        $buildingUnit->setEnergyCertificate(null);

        $this->entityManager->flush();

        $this->entityManager->remove(entity: $energyCertificate);

        $this->entityManager->flush();
    }

    public function deletePropertyMarketingInformation(BuildingUnit $buildingUnit): void
    {
        $propertyMarketingInformation = $buildingUnit->getPropertyMarketingInformation();
        $propertyPricingInformation = $propertyMarketingInformation->getPropertyPricingInformation();

        $this->entityManager->flush();

        $this->entityManager->remove(entity: $propertyMarketingInformation);

        $this->entityManager->flush();

        $this->entityManager->remove(entity: $propertyPricingInformation);
    }

    public function deletePropertyContactPerson(PropertyContactPerson $propertyContactPerson, BuildingUnit $buildingUnit): void
    {
        $this->hardDeletePropertyContactPerson(propertyContactPerson: $propertyContactPerson, buildingUnit: $buildingUnit);
    }

    public function deleteAllPropertyContactPersons(BuildingUnit $buildingUnit): void
    {
        $this->hardDeleteAllPropertyContactPersons(buildingUnit: $buildingUnit);
    }

    public function deleteAllOpenImmoExports(BuildingUnit $buildingUnit): void
    {
        $this->hardDeleteAllOpenImmoExports(buildingUnit: $buildingUnit);
    }

    public function deleteAllOpenImmoTransferLogs(BuildingUnit $buildingUnit): void
    {
        $this->hardDeleteAllOpenImmoTransferLogs(buildingUnit: $buildingUnit);
    }

    public function deleteAllMatchingResults(BuildingUnit $buildingUnit): void
    {
        $this->hardDeleteAllMatchingResults(buildingUnit: $buildingUnit);
    }

    public function deleteAllSettlementConceptMatchingResults(BuildingUnit $buildingUnit): void
    {
        $this->hardDeleteAllSettlementConceptMatchingResults(buildingUnit: $buildingUnit);
    }

    public function deleteAllLookingForPropertyRequestExposeForwardings(BuildingUnit $buildingUnit): void
    {
        $this->hardDeleteAllLookingForPropertyRequestExposeForwardings(buildingUnit: $buildingUnit);
    }

    public function deleteAllSettlementConceptExposeForwardings(BuildingUnit $buildingUnit): void
    {
        $this->hardDeleteAllSettlementConceptExposeForwardings(buildingUnit: $buildingUnit);
    }

    public function deleteAllLookingForPropertyRequestMatchingResponses(BuildingUnit $buildingUnit): void
    {
        $this->hardDeleteAllLookingForPropertyRequestMatchingResponses(buildingUnit: $buildingUnit);
    }

    public function deleteAllSettlementConceptMatchingResponses(BuildingUnit $buildingUnit): void
    {
        $this->hardDeleteAllSettlementConceptMatchingResponses(buildingUnit: $buildingUnit);
    }

    private function softDeleteBuildingUnit(BuildingUnit $buildingUnit, AccountUser $deletedByAccountUser): void
    {
        $buildingUnit
            ->setDeleted(deleted: true)
            ->setDeletedAt(deletedAt: new \DateTimeImmutable())
            ->setDeletedByAccountUser(deletedByAccountUser: $deletedByAccountUser);

        $this->entityManager->flush();
    }

    private function hardDeleteBuildingUnit(BuildingUnit $buildingUnit): void
    {
        if ($buildingUnit->getMainImage() !== null) {
            $this->deleteMainImage(property: $buildingUnit);
        }

        if ($buildingUnit->getGeolocationPoint() !== null) {
            $this->deleteGeolocationPoint(property: $buildingUnit);
        }

        if ($buildingUnit->getGeolocationPolygon() !== null) {
            $this->deleteGeolocationPolygon(property: $buildingUnit);
        }

        if ($buildingUnit->getGeolocationMultiPolygon() !== null) {
            $this->deleteGeolocationMultiPolygon(property: $buildingUnit);
        }

        $this->deleteAllImages(property: $buildingUnit);

        $this->deleteAllDocuments(property: $buildingUnit);

        $this->deleteAllPropertyUsers(property: $buildingUnit);

        $this->deleteAllPropertyOwners(property: $buildingUnit);

        $this->deleteAllPropertyContactPersons(buildingUnit: $buildingUnit);

        $this->deleteAllOpenImmoExports(buildingUnit: $buildingUnit);

        $this->deleteAllOpenImmoTransferLogs(buildingUnit: $buildingUnit);

        $this->deleteAllMatchingResults(buildingUnit: $buildingUnit);

        $this->deleteAllSettlementConceptMatchingResults(buildingUnit: $buildingUnit);

        $this->deleteAllLookingForPropertyRequestExposeForwardings(buildingUnit: $buildingUnit);

        $this->deleteAllSettlementConceptExposeForwardings(buildingUnit: $buildingUnit);

        $this->deleteAllLookingForPropertyRequestMatchingResponses(buildingUnit: $buildingUnit);

        $this->deleteAllSettlementConceptMatchingResponses(buildingUnit: $buildingUnit);

        $this->notificationDeletionService->deleteNotificationsByBuildingUnit(buildingUnit: $buildingUnit);

        if ($buildingUnit->getEnergyCertificate() !== null) {
            $this->deleteEnergyCertificate(buildingUnit: $buildingUnit);
        }

        if ($buildingUnit->getOpenImmoAdditionalInformation() !== null) {
            $this->hardDeleteOpenImmoAdditionalInformation(buildingUnit: $buildingUnit);
        }

        if ($buildingUnit->getExternalContactPerson() !== null) {
            $this->hardDeleteExternalContactPerson(buildingUnit: $buildingUnit);
        }

        if ($buildingUnit->getMatchingTask() !== null) {
            $this->matchingTaskDeletionService->deleteMatchingTask(matchingTask: $buildingUnit->getMatchingTask());

            $buildingUnit->setMatchingTask(null);
        }

        if ($buildingUnit->getSettlementConceptMatchingTask() !== null) {
            $this->settlementConceptMatchingTaskDeletionService->deleteSettlementConceptMatchingTask(
                settlementConceptMatchingTask: $buildingUnit->getSettlementConceptMatchingTask()
            );

            $buildingUnit->setSettlementConceptMatchingTask(null);
        }

        if ($buildingUnit->getPropertyVacancyReport() !== null) {
            $buildingUnit->getPropertyVacancyReport()->setBuildingUnit(null);
            $this->entityManager->flush();
        }

        $this->hardDeleteAllBuildingUnitFeedbacks(buildingUnit: $buildingUnit);

        $building = $buildingUnit->getBuilding();
        $address = $buildingUnit->getAddress();

        $this->entityManager->remove(entity: $buildingUnit);

        $this->entityManager->flush();

        $this->deletePropertySpacesData(property: $buildingUnit);

        $this->deletePropertyMarketingInformation(buildingUnit: $buildingUnit);

        $this->buildingDeletionService->deleteBuilding(building: $building, deletionType: DeletionType::HARD);

        $this->deleteAddress(address: $address);
    }

    private function softDeleteBuildingUnitsByAccount(Account $account, ?AccountUser $deletedByAccountUser): void
    {
        $buildingUnits = $this->buildingUnitRepository->findBy(criteria: ['account' => $account]);

        foreach ($buildingUnits as $buildingUnit) {
            $this->softDeleteBuildingUnit(buildingUnit: $buildingUnit, deletedByAccountUser: $deletedByAccountUser);
        }
    }

    private function hardDeleteBuildingUnitsByAccount(Account $account): void
    {
        $buildingUnits = $this->buildingUnitRepository->findBy(criteria: ['account' => $account]);

        foreach ($buildingUnits as $buildingUnit) {
            $this->hardDeleteBuildingUnit(buildingUnit: $buildingUnit);
        }
    }

    private function hardDeleteOpenImmoAdditionalInformation(BuildingUnit $buildingUnit): void
    {
        $openImmoAdditionalInformation = $buildingUnit->getOpenImmoAdditionalInformation();

        if ($openImmoAdditionalInformation === null) {
            return;
        }

        $buildingUnit->setOpenImmoAdditionalInformation(null);

        $this->entityManager->remove(entity: $openImmoAdditionalInformation);

        $this->entityManager->flush();
    }

    private function hardDeleteExternalContactPerson(BuildingUnit $buildingUnit): void
    {
        $externalContactPerson = $buildingUnit->getExternalContactPerson();

        if ($externalContactPerson === null) {
            return;
        }

        $this->entityManager->remove(entity: $externalContactPerson);
        $this->entityManager->flush();
    }

    private function hardDeletePropertyContactPerson(PropertyContactPerson $propertyContactPerson, BuildingUnit $buildingUnit): void
    {
        if ($buildingUnit->getPropertyContactPersons()->contains(element: $propertyContactPerson)) {
            $buildingUnit->getPropertyContactPersons()->removeElement(element: $propertyContactPerson);
        }

        $this->entityManager->flush();

        $this->propertyContactPersonDeletionService->deletePropertyContactPerson(propertyContactPerson: $propertyContactPerson);
    }

    private function hardDeleteAllPropertyContactPersons(BuildingUnit $buildingUnit): void
    {
        foreach ($buildingUnit->getPropertyContactPersons() as $propertyContactPerson) {
            $this->hardDeletePropertyContactPerson(propertyContactPerson: $propertyContactPerson, buildingUnit: $buildingUnit);
        }
    }

    private function hardDeleteAllOpenImmoExports(BuildingUnit $buildingUnit): void
    {
        foreach ($buildingUnit->getOpenImmoExports() as $openImmoExport) {
            $this->openImmoExportDeletionService->deleteOpenImmoExport(openImmoExport: $openImmoExport);
        }
    }

    private function hardDeleteAllOpenImmoTransferLogs(BuildingUnit $buildingUnit): void
    {
        foreach ($buildingUnit->getOpenImmoTransferLogs() as $openImmoTransferLog) {
            $this->openImmoTransferLogDeletionService->deleteOpenImmoTransferLog(openImmoTransferLog: $openImmoTransferLog);
        }
    }
    private function hardDeleteAllMatchingResults(BuildingUnit $buildingUnit): void
    {
        foreach ($buildingUnit->getMatchingResults() as $matchingResult) {
            $this->matchingDeletionService->deleteMatchingResult(matchingResult: $matchingResult);
        }
    }

    private function hardDeleteAllSettlementConceptMatchingResults(BuildingUnit $buildingUnit): void
    {
        foreach ($buildingUnit->getSettlementConceptMatchingResults() as $settlementConceptMatchingResult) {
            $this->settlementConceptMatchingResultDeletionService->deleteSettlementConceptMatchingResult(
                settlementConceptMatchingResult: $settlementConceptMatchingResult
            );
        }
    }

    private function hardDeleteAllLookingForPropertyRequestExposeForwardings(BuildingUnit $buildingUnit): void
    {
        foreach ($buildingUnit->getLookingForPropertyRequestExposeForwardings() as $lookingForPropertyRequestExposeForwarding) {
            $this->lookingForPropertyRequestDeletionService->deleteLookingForPropertyRequestExposeForwarding(
                lookingForPropertyRequestExposeForwarding: $lookingForPropertyRequestExposeForwarding
            );
        }
    }

    private function hardDeleteAllSettlementConceptExposeForwardings(BuildingUnit $buildingUnit): void
    {
        foreach ($buildingUnit->getSettlementConceptExposeForwardings() as $settlementConceptExposeForwarding) {
            $this->settlementConceptExposeForwardingDeletionService->deleteSettlementConceptExposeForwarding(
                settlementConceptExposeForwarding: $settlementConceptExposeForwarding
            );
        }
    }

    private function hardDeleteAllLookingForPropertyRequestMatchingResponses(BuildingUnit $buildingUnit): void
    {
        foreach ($buildingUnit->getLookingForPropertyRequestMatchingResponses() as $lookingForPropertyRequestMatchingResponse) {
            $this->lookingForPropertyRequestDeletionService->deleteLookingForPropertyRequestMatchingResponse(
                lookingForPropertyRequestMatchingResponse: $lookingForPropertyRequestMatchingResponse
            );
        }
    }

    private function hardDeleteAllSettlementConceptMatchingResponses(BuildingUnit $buildingUnit): void
    {
        foreach ($buildingUnit->getSettlementConceptMatchingResponses() as $settlementConceptMatchingResponse) {
            $this->settlementConceptMatchingResponseDeletionService->deleteSettlementConceptMatchingResponse(
                settlementConceptMatchingResponse: $settlementConceptMatchingResponse
            );
        }
    }

    private function hardDeleteBuildingUnitFeedback(BuildingUnitFeedback $buildingUnitFeedback, BuildingUnit $buildingUnit): void
    {
        if ($buildingUnitFeedback->getBuildingUnit() !== $buildingUnit) {
            return;
        }

        $this->entityManager->remove($buildingUnitFeedback);

        $this->entityManager->flush();
    }

    private function hardDeleteAllBuildingUnitFeedbacks(BuildingUnit $buildingUnit): void
    {
        $buildingUnitFeedbacks = $this->buildingUnitFeedbackRepository->findByBuildingUnit(buildingUnit: $buildingUnit);

        foreach ($buildingUnitFeedbacks as $buildingUnitFeedback) {
            $this->hardDeleteBuildingUnitFeedback(buildingUnitFeedback: $buildingUnitFeedback, buildingUnit: $buildingUnit);
        }
    }
}
