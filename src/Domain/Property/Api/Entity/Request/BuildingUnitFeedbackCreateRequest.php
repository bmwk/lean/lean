<?php

declare(strict_types=1);

namespace App\Domain\Property\Api\Entity\Request;

class BuildingUnitFeedbackCreateRequest
{
    private int $feedbackId;

    public function getFeedbackId(): int
    {
        return $this->feedbackId;
    }

    public function setFeedbackId(int $feedbackId): self
    {
        $this->feedbackId = $feedbackId;

        return $this;
    }
}
