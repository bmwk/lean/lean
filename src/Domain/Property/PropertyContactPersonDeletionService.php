<?php

declare(strict_types=1);

namespace App\Domain\Property;

use App\Domain\Entity\Account;
use App\Domain\Entity\Property\PropertyContactPerson;
use App\Repository\Property\PropertyContactPersonRepository;
use Doctrine\ORM\EntityManagerInterface;

class PropertyContactPersonDeletionService
{
    public function __construct(
        private readonly PropertyContactPersonRepository $propertyContactPersonRepository,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function deletePropertyContactPerson(PropertyContactPerson $propertyContactPerson): void
    {
        $this->hardDeletePropertyContactPerson(propertyContactPerson: $propertyContactPerson);
    }

    private function hardDeletePropertyContactPerson(PropertyContactPerson $propertyContactPerson): void
    {
        $this->entityManager->remove(entity: $propertyContactPerson);

        $this->entityManager->flush();
    }

    public function deletePropertyContactPersonsByAccount(Account $account): void
    {
        $this->hardDeletePropertyContactPersonsByAccount(account: $account);
    }

    private function hardDeletePropertyContactPersonsByAccount(Account $account): void
    {
        $propertyContactPersons = $this->propertyContactPersonRepository->findBy(criteria: ['account' => $account]);

        foreach ($propertyContactPersons as $propertyContactPerson) {
            $this->hardDeletePropertyContactPerson(propertyContactPerson: $propertyContactPerson);
        }
    }
}
