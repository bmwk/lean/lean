<?php

declare(strict_types=1);

namespace App\Domain\Property;

use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\AccountUser\AccountUserRole;
use App\Domain\Entity\Property\BuildingUnit;
use Symfony\Bundle\SecurityBundle\Security;

class BuildingUnitSecurityService
{
    public function __construct(
        private readonly Security $security
    ) {
    }

    public function canCreateBuildingUnit(): bool
    {
        return (
            $this->security->isGranted(attributes: AccountUserRole::ROLE_USER->value) === true
            || $this->security->isGranted(attributes: AccountUserRole::ROLE_PROPERTY_FEEDER->value) === true
            || $this->security->isGranted(attributes: AccountUserRole::ROLE_PROPERTY_PROVIDER->value) === true
        );
    }

    public function canViewBuildingUnit(BuildingUnit $buildingUnit, AccountUser $accountUser): bool
    {
        $account = $accountUser->getAccount();

        if (
            $account->getParentAccount() === null
            && (
                $this->security->isGranted(attributes: AccountUserRole::ROLE_USER->value) === true
                || $this->security->isGranted(attributes: AccountUserRole::ROLE_PROPERTY_FEEDER->value) === true
                || $this->security->isGranted(attributes: AccountUserRole::ROLE_VIEWER->value) === true
                || $this->security->isGranted(attributes: AccountUserRole::ROLE_RESTRICTED_VIEWER->value) === true
            )
            && (
                $buildingUnit->getAccount() === $account
                || $account->getSubAccounts()->contains(element: $buildingUnit->getAccount()) === true
            )
        ) {
            return true;
        }

        if (
            $account->getParentAccount() !== null
            && $this->security->isGranted(attributes: AccountUserRole::ROLE_PROPERTY_PROVIDER->value) === true
            && $buildingUnit->getAccount() === $account
        ) {
            return true;
        }

        return false;
    }

    public function canEditBuildingUnit(BuildingUnit $buildingUnit, AccountUser $accountUser): bool
    {
        if ($buildingUnit->isArchived() === true) {
            return false;
        }

        $account = $accountUser->getAccount();

        if (
            $account->getParentAccount() === null
            && $this->security->isGranted(attributes: AccountUserRole::ROLE_USER->value) === true
            && $buildingUnit->getAccount() === $account
        ) {
            return true;
        }

        if (
            $account->getParentAccount() === null
            && $this->security->isGranted(attributes: AccountUserRole::ROLE_PROPERTY_FEEDER->value) === true
            && $buildingUnit->getAccount() === $account
            && (
                $buildingUnit->getCreatedByAccountUser() === $accountUser
                || $buildingUnit->getAssignedToAccountUser() === $accountUser
            )
        ) {
            return true;
        }

        if (
            $account->getParentAccount() !== null
            && $this->security->isGranted(attributes: AccountUserRole::ROLE_PROPERTY_PROVIDER->value) === true
            && $buildingUnit->getAccount() === $account
        ) {
            return true;
        }

        return false;
    }

    public function canArchiveBuildingUnit(BuildingUnit $buildingUnit, AccountUser $accountUser): bool
    {
        $account = $accountUser->getAccount();

        if (
            $account->getParentAccount() === null
            && $this->security->isGranted(attributes: AccountUserRole::ROLE_USER->value) === true
            && $buildingUnit->getAccount() === $account
        ) {
            return true;
        }

        if (
            $account->getParentAccount() === null
            && $this->security->isGranted(attributes: AccountUserRole::ROLE_PROPERTY_FEEDER->value) === true
            && $buildingUnit->getAccount() === $account
            && (
                $buildingUnit->getCreatedByAccountUser() === $accountUser
                || $buildingUnit->getAssignedToAccountUser() === $accountUser
            )
        ) {
            return true;
        }

        if (
            $account->getParentAccount() !== null
            && $this->security->isGranted(attributes: AccountUserRole::ROLE_PROPERTY_PROVIDER->value) === true
            && $buildingUnit->getAccount() === $account
        ) {
            return true;
        }

        return false;
    }

    public function canDeleteBuildingUnit(BuildingUnit $buildingUnit, AccountUser $accountUser): bool
    {
        $account = $accountUser->getAccount();

        if (
            $account->getParentAccount() === null
            && $this->security->isGranted(attributes: AccountUserRole::ROLE_ACCOUNT_ADMIN->value) === true
            && (
                $buildingUnit->getAccount() === $account
                || $account->getSubAccounts()->contains(element: $buildingUnit->getAccount()) === true
            )
        ) {
            return true;
        }

        if (
            $account->getParentAccount() === null
            && $this->security->isGranted(attributes: AccountUserRole::ROLE_USER->value) === true
            && $buildingUnit->getAccount() === $account
        ) {
            return true;
        }

        if (
            $account->getParentAccount() === null
            && $this->security->isGranted(attributes: AccountUserRole::ROLE_PROPERTY_FEEDER->value) === true
            && $buildingUnit->getAccount() === $account
            && $buildingUnit->getCreatedByAccountUser() === $accountUser
        ) {
            return true;
        }

        if (
            $account->getParentAccount() !== null
            && $this->security->isGranted(attributes: AccountUserRole::ROLE_PROPERTY_PROVIDER->value) === true
            && $buildingUnit->getAccount() === $account
        ) {
            return true;
        }

        return false;
    }

    public function canAssignToAccountUserBuildingUnit(BuildingUnit $buildingUnit, AccountUser $accountUser): bool
    {
        $account = $accountUser->getAccount();

        if (
            $account->getParentAccount() === null
            && $this->security->isGranted(attributes: AccountUserRole::ROLE_USER->value) === true
            && $buildingUnit->getAccount() === $account
        ) {
            return true;
        }

        if (
            $account->getParentAccount() === null
            && $this->security->isGranted(attributes: AccountUserRole::ROLE_PROPERTY_FEEDER->value) === true
            && $buildingUnit->getAccount() === $account
            && $buildingUnit->getCreatedByAccountUser() === $accountUser
        ) {
            return true;
        }

        return false;
    }

    public function canOpenImmoExportBuildingUnit(BuildingUnit $buildingUnit, AccountUser $accountUser): bool
    {
        if ($buildingUnit->isArchived() === true) {
            return false;
        }

        $account = $accountUser->getAccount();

        if (
            $account->getParentAccount() === null
            && (
                $this->security->isGranted(attributes: AccountUserRole::ROLE_USER->value) === true
                || $this->security->isGranted(attributes: AccountUserRole::ROLE_PROPERTY_FEEDER->value) === true
            )
            && $buildingUnit->getAccount() === $account
        ) {
            return true;
        }

        return false;
    }
}
