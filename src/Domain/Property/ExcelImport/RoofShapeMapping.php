<?php

declare(strict_types=1);

namespace App\Domain\Property\ExcelImport;

use App\Domain\Entity\Property\RoofShape;

class RoofShapeMapping
{
    public static function fetchRoofShapeByName(string $roofShapeName): ?RoofShape
    {
        return match (strtolower($roofShapeName)) {
            strtolower(RoofShape::FLAT_ROOF->getName()) => RoofShape::FLAT_ROOF,
            strtolower(RoofShape::PENT_ROOF->getName()) => RoofShape::PENT_ROOF,
            strtolower(RoofShape::PITCHED_ROOF->getName()) => RoofShape::PITCHED_ROOF,
            strtolower(RoofShape::SHED_ROOF->getName()) => RoofShape::SHED_ROOF,
            strtolower(RoofShape::SADDLE_OR_GABLE_ROOF->getName()) => RoofShape::SADDLE_OR_GABLE_ROOF,
            strtolower(RoofShape::BUTTERFLY_ROOF->getName()) => RoofShape::BUTTERFLY_ROOF,
            strtolower(RoofShape::TRENCH_ROOF->getName()) => RoofShape::TRENCH_ROOF,
            strtolower(RoofShape::HIP_ROOF->getName()) => RoofShape::HIP_ROOF,
            strtolower(RoofShape::CRESTED_HIP_OR_CREST_HIP_ROOF->getName()) => RoofShape::CRESTED_HIP_OR_CREST_HIP_ROOF,
            strtolower(RoofShape::TENT_ROOF->getName()) => RoofShape::TENT_ROOF,
            strtolower(RoofShape::FOOT_HIP_ROOF->getName()) => RoofShape::FOOT_HIP_ROOF,
            strtolower(RoofShape::MANSARD_ROOF->getName()) => RoofShape::MANSARD_ROOF,
            strtolower(RoofShape::ZOLLINGER_ROOF->getName()) => RoofShape::ZOLLINGER_ROOF,
            strtolower(RoofShape::BARREL_ROOF->getName()) => RoofShape::BARREL_ROOF,
            strtolower(RoofShape::ARCHED_ROOF->getName()) => RoofShape::ARCHED_ROOF,
            strtolower(RoofShape::RHOMBIC_ROOF->getName()) => RoofShape::RHOMBIC_ROOF,
            strtolower(RoofShape::FOLDED_ROOF->getName()) => RoofShape::FOLDED_ROOF,
            strtolower(RoofShape::WELSH_HOOD->getName()) => RoofShape::WELSH_HOOD,
            strtolower(RoofShape::CONICAL_ROOF->getName()) => RoofShape::CONICAL_ROOF,
            strtolower(RoofShape::HELMET->getName()) => RoofShape::HELMET,
            strtolower(RoofShape::ONION_HELMET->getName()) => RoofShape::ONION_HELMET,
            strtolower(RoofShape::PAGODA_ROOF->getName()) => RoofShape::PAGODA_ROOF,
            default => null,
        };
    }
}
