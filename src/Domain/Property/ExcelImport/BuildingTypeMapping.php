<?php

declare(strict_types=1);

namespace App\Domain\Property\ExcelImport;

use App\Domain\Entity\Property\BuildingType;

class BuildingTypeMapping
{
    public static function fetchBuildingTypeByShortName(string $name): ?BuildingType
    {
        return match (strtolower($name)) {
            strtolower('EFH') => BuildingType::SINGLE_FAMILY_HOUSE,
            strtolower('MFH') => BuildingType::APARTMENT_HOUSE,
            strtolower('WuG') => BuildingType::RESIDENTIAL_AND_COMMERCIAL_BUILDING_MIXED_USE,
            strtolower('VWG') => BuildingType::ADMINISTRATION_BUILDING,
            strtolower('SPZ') => BuildingType::SPECIAL_BUILDING,
            strtolower('SI') => BuildingType::OTHER_REAL_ESTATE,
            strtolower('BAU') => BuildingType::FARMHOUSE,
            strtolower('DHH') => BuildingType::SEMI_DETACHED_HOUSE,
            strtolower('VIL') => BuildingType::VILLA,
            strtolower('REI') => BuildingType::TERRACED_HOUSE,
            strtolower('BGW') => BuildingType::BUNGALOW,
            strtolower('BRH') => BuildingType::OFFICE_BUILDING,
            strtolower('GSH') => BuildingType::BUSINESS_HOUSE,
            default => null
        };
    }
}
