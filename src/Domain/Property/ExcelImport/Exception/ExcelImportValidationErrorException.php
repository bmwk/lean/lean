<?php

declare(strict_types=1);

namespace App\Domain\Property\ExcelImport\Exception;

class ExcelImportValidationErrorException extends \RuntimeException implements ExcelImportErrorExceptionInterface
{
}
