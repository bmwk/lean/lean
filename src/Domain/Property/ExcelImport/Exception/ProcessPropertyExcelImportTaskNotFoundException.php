<?php

declare(strict_types=1);

namespace App\Domain\Property\ExcelImport\Exception;

class ProcessPropertyExcelImportTaskNotFoundException extends \RuntimeException
{
}
