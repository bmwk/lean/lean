<?php

declare(strict_types=1);

namespace App\Domain\Property\ExcelImport;

class ExcelImportColIndex
{
    const STREET_NAME = 1;
    const HOUSE_NUMBER = 2;
    const POSTAL_CODE = 3;
    const PLACE_NAME = 4;
    const QUARTER_NAME = 5;
    const IN_FLOORS = 6;
    const PLACE_DESCRIPTION = 7;
    const PROPERTY_USAGE = 8;
    const COMPANY_NAME = 9;
    const SALUTATION = 10;
    const PERSON_TITLE = 11;
    const FIRST_NAME = 12;
    const NAME = 13;
    const POSITION = 14;
    const PHONE_NUMBER = 15;
    const EMAIL = 16;
    const BRANCH = 17;
    const FAMILYBUSINESS = 18;
    const INDUSTRY = 19;
    const BUSINESS_FORM = 20;
    const NACE_CODE = 21;
    const PAST_PROPERTY_USAGE = 22;
    const PAST_PROPERTY_USERS = 23;
    const SHOP_WINDOW_FRONT_WIDTH = 24;
    const SHOWROOM_AVAILABLE = 25;
    const PROPERTY_AREA = 26;
    const RETAIL_SPACE = 27;
    const SUBSIDIARY_SPACE = 28;
    const GROUND_LEVEL_SALES_AREA = 29;
    const USABLE_OUTDOOR_AREA = 30;
    const BARRIER_FREE_ACCESS = 31;
    const ENTRANCE_DOOR_WIDTH = 32;
    const BUILDING_TYPE = 33;
    const CONSTRUCTION_YEAR = 34;
    const NUMBER_OF_FLOORS = 35;
    const ROOF_SHAPE = 36;
    const BUILDING_CONDITION = 37;
    const NUMBER_OF_PARKING_LOTS = 38;
    const REMARK = 39;
    const INTERNAL_NUMBER = 40;
    const GIS_BUILDING_ID = 41;
    const LONGITUDE = 42;
    const LATITUDE = 43;
    const PICTURES_AVAILABLE = 44;
    const DATASET_COLLECT_DATE = 45;
    const DATASET_CAPTURED_BY = 46;
    const PO_SALUTATION = 47;
    const PO_PERSON_TITLE = 48;
    const PO_FIRST_NAME = 49;
    const PO_NAME = 50;
    const PO_POSITION = 51;
    const PO_COMPANY_NAME = 52;
    const PO_STREET_NAME = 53;
    const PO_HOUSE_NUMBER = 54;
    const PO_POSTAL_CODE = 55;
    const PO_PLACE_NAME = 56;
    const PO_PHONE_NUMBER = 57;
    const PO_EMAIL = 58;

    public static function getHeaderColumns(): array
    {
        return [
            self::STREET_NAME             => 'Straße',
            self::HOUSE_NUMBER            => 'HsNr',
            self::POSTAL_CODE             => 'PLZ',
            self::PLACE_NAME              => 'Ort',
            self::QUARTER_NAME            => 'Orts-/Stadtteil',
            self::IN_FLOORS               => 'In Etage(n)',
            self::PLACE_DESCRIPTION       => 'Lagebezeichnung',
            self::PROPERTY_USAGE          => 'Nutzung',
            self::COMPANY_NAME            => 'Firmenname',
            self::SALUTATION              => 'Anrede',
            self::PERSON_TITLE            => 'Titel',
            self::FIRST_NAME              => 'Vorname',
            self::NAME                    => 'Nachname',
            self::POSITION                => 'Position',
            self::PHONE_NUMBER            => 'Telefon',
            self::EMAIL                   => 'E-Mail',
            self::BRANCH                  => 'Filiale',
            self::FAMILYBUSINESS          => 'Familienbetrieb',
            self::INDUSTRY                => 'Branche',
            self::BUSINESS_FORM           => 'Betriebsform',
            self::NACE_CODE               => 'NACE-Code',
            self::PAST_PROPERTY_USAGE     => 'ehemalige Nutzung',
            self::PAST_PROPERTY_USERS     => 'ehemalige Nutzer',
            self::SHOP_WINDOW_FRONT_WIDTH => 'Gesamtbreite Schaufenster in m',
            self::SHOWROOM_AVAILABLE      => 'Showroom vorhanden',
            self::PROPERTY_AREA           => 'Gesamtfläche in qm',
            self::RETAIL_SPACE            => 'Verkaufsfläche in qm',
            self::SUBSIDIARY_SPACE        => 'Nebenfläche in qm',
            self::GROUND_LEVEL_SALES_AREA => 'Ebenerdige Verkaufsfläche',
            self::USABLE_OUTDOOR_AREA     => 'Nutzbare Außenfläche in qm',
            self::BARRIER_FREE_ACCESS     => 'Barrierefreier Zugang',
            self::ENTRANCE_DOOR_WIDTH     => 'Breite Eingangstür in m',
            self::BUILDING_TYPE           => 'Gebäudeart',
            self::CONSTRUCTION_YEAR       => 'Baujahr',
            self::NUMBER_OF_FLOORS        => 'Anzahl Geschosse',
            self::ROOF_SHAPE              => 'Dachform',
            self::BUILDING_CONDITION      => 'Gebäudezustand',
            self::NUMBER_OF_PARKING_LOTS  => 'Anzahl Stellplätze',
            self::REMARK                  => 'Bemerkungen',
            self::INTERNAL_NUMBER         => 'Interne Ref.Nr.',
            self::GIS_BUILDING_ID         => 'GIS-Gebäude-ID',
            self::LONGITUDE               => 'X-Koordinate',
            self::LATITUDE                => 'Y-Koordinate',
            self::PICTURES_AVAILABLE      => 'Bilder vorhanden',
            self::DATASET_COLLECT_DATE    => 'Erhebungsdatum',
            self::DATASET_CAPTURED_BY     => 'Erfasser:in',
            self::PO_SALUTATION           => 'ET_Anrede',
            self::PO_PERSON_TITLE         => 'ET_Titel',
            self::PO_FIRST_NAME           => 'ET_Vorname',
            self::PO_NAME                 => 'ET_Name',
            self::PO_POSITION             => 'ET_Position',
            self::PO_COMPANY_NAME         => 'ET_Firma',
            self::PO_STREET_NAME          => 'ET_Strasse',
            self::PO_HOUSE_NUMBER         => 'ET_HsNr',
            self::PO_POSTAL_CODE          => 'ET_PLZ',
            self::PO_PLACE_NAME           => 'ET_Ort',
            self::PO_PHONE_NUMBER         => 'ET_Telefon',
            self::PO_EMAIL                => 'ET_E-Mail',
        ];
    }

    public static function getColumns(): array
    {
        return [
            self::STREET_NAME,
            self::HOUSE_NUMBER,
            self::POSTAL_CODE,
            self::PLACE_NAME,
            self::QUARTER_NAME,
            self::IN_FLOORS,
            self::PLACE_DESCRIPTION,
            self::PROPERTY_USAGE,
            self::COMPANY_NAME,
            self::SALUTATION,
            self::PERSON_TITLE,
            self::FIRST_NAME,
            self::NAME,
            self::POSITION,
            self::PHONE_NUMBER,
            self::EMAIL,
            self::BRANCH,
            self::FAMILYBUSINESS,
            self::INDUSTRY,
            self::BUSINESS_FORM,
            self::NACE_CODE,
            self::PAST_PROPERTY_USAGE,
            self::PAST_PROPERTY_USERS,
            self::SHOP_WINDOW_FRONT_WIDTH,
            self::SHOWROOM_AVAILABLE,
            self::PROPERTY_AREA,
            self::RETAIL_SPACE,
            self::SUBSIDIARY_SPACE,
            self::GROUND_LEVEL_SALES_AREA,
            self::USABLE_OUTDOOR_AREA,
            self::BARRIER_FREE_ACCESS,
            self::ENTRANCE_DOOR_WIDTH,
            self::BUILDING_TYPE,
            self::CONSTRUCTION_YEAR,
            self::NUMBER_OF_FLOORS,
            self::ROOF_SHAPE,
            self::BUILDING_CONDITION,
            self::NUMBER_OF_PARKING_LOTS,
            self::REMARK,
            self::INTERNAL_NUMBER,
            self::GIS_BUILDING_ID,
            self::LONGITUDE,
            self::LATITUDE,
            self::PICTURES_AVAILABLE,
            self::DATASET_COLLECT_DATE,
            self::DATASET_CAPTURED_BY,
            self::PO_SALUTATION,
            self::PO_PERSON_TITLE,
            self::PO_FIRST_NAME,
            self::PO_NAME,
            self::PO_POSITION,
            self::PO_COMPANY_NAME,
            self::PO_STREET_NAME,
            self::PO_HOUSE_NUMBER,
            self::PO_POSTAL_CODE,
            self::PO_PLACE_NAME,
            self::PO_PHONE_NUMBER,
            self::PO_EMAIL,
        ];
    }
}
