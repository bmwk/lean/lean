<?php

declare(strict_types=1);

namespace App\Domain\Property\ExcelImport;

use App\Domain\Entity\Person\PersonTitle;

class PersonTitleMapping
{
    public static function fetchPersonTitleByName(string $name): ?PersonTitle
    {
        return match (strtolower($name)) {
            strtolower(PersonTitle::DR->getName()) => PersonTitle::DR,
            strtolower(PersonTitle::PROF->getName()) => PersonTitle::PROF,
            strtolower(PersonTitle::PROF_DR->getName()) => PersonTitle::PROF_DR,
            default => null
        };
    }
}
