<?php

declare(strict_types=1);

namespace App\Domain\Property\ExcelImport;

use App\Domain\Entity\Property\BarrierFreeAccess;

class BarrierFreeAccessMapping
{
    public static function fetchBarrierFreeAccessByName(string $name): ?BarrierFreeAccess
    {
        return match (strtolower($name)) {
            strtolower(BarrierFreeAccess::IS_AVAILABLE->getName()) => BarrierFreeAccess::IS_AVAILABLE,
            strtolower(BarrierFreeAccess::CAN_BE_GUARANTEED->getName()) => BarrierFreeAccess::CAN_BE_GUARANTEED,
            strtolower(BarrierFreeAccess::IS_CURRENTLY_NOT_AVAILABLE->getName()) => BarrierFreeAccess::IS_CURRENTLY_NOT_AVAILABLE,
            strtolower(BarrierFreeAccess::IS_NOT_POSSIBLE->getName()) => BarrierFreeAccess::IS_NOT_POSSIBLE,
            default => null
        };
    }
}
