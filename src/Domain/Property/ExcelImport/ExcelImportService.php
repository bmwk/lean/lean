<?php

declare(strict_types=1);

namespace App\Domain\Property\ExcelImport;

use App\Domain\Classification\ClassificationService;
use App\Domain\Entity\Account;
use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\GeolocationPoint;
use App\Domain\Entity\Person\Contact;
use App\Domain\Entity\Person\Person;
use App\Domain\Entity\Person\PersonType;
use App\Domain\Entity\Place;
use App\Domain\Entity\ProcessPropertyExcelImportTask;
use App\Domain\Entity\Property\Address;
use App\Domain\Entity\Property\BranchType;
use App\Domain\Entity\Property\Building;
use App\Domain\Entity\Property\BuildingUnit;
use App\Domain\Entity\Property\OwnershipType;
use App\Domain\Entity\Property\PropertyMarketingInformation;
use App\Domain\Entity\Property\PropertyOwner;
use App\Domain\Entity\Property\PropertyOwnerStatus;
use App\Domain\Entity\Property\PropertyPricingInformation;
use App\Domain\Entity\Property\PropertyRelationship;
use App\Domain\Entity\Property\PropertySpacesData;
use App\Domain\Entity\Property\PropertyUser;
use App\Domain\Entity\Property\PropertyUserStatus;
use App\Domain\Entity\Property\UsableOutdoorAreaPossibility;
use App\Domain\Entity\PropertyExcelImport;
use App\Domain\Entity\SortingOption\SortingOption;
use App\Domain\Entity\TaskStatus;
use App\Domain\File\FileService;
use App\Domain\Location\LocationService;
use App\Domain\Person\PersonService;
use App\Domain\Property\BuildingUnitService;
use App\Domain\Property\ExcelImport\Exception\ExcelImportErrorExceptionInterface;
use App\Domain\Property\ExcelImport\Exception\ExcelImportProcessingErrorException;
use App\Domain\Property\ExcelImport\Exception\ExcelImportValidationErrorException;
use App\Domain\Property\ExcelImport\Exception\ProcessPropertyExcelImportTaskNotFoundException;
use App\Domain\Property\PropertyOwnerService;
use App\Domain\Property\PropertyUserService;
use App\GoogleMaps\Exception\GoogleMapsException;
use App\GoogleMaps\GoogleMapsService;
use App\Repository\ProcessPropertyExcelImportTaskRepository;
use App\Repository\PropertyExcelImportRepository;
use CrEOF\Spatial\PHP\Types\Geometry\Point;
use Doctrine\DBAL\Driver\Exception;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Exception\ORMException;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\ORM\UnitOfWork;
use Doctrine\Persistence\ManagerRegistry;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ExcelImportService
{
    private const FIRST_ROW = 6;

    private array $cachedPersons = [];

    public function __construct(
        private readonly PropertyExcelImportRepository $propertyExcelImportRepository,
        private readonly ProcessPropertyExcelImportTaskRepository $processPropertyExcelImportTaskRepository,
        private readonly ExcelImportValidator $excelImportValidator,
        private readonly BuildingUnitService $buildingUnitService,
        private readonly PropertyOwnerService $propertyOwnerService,
        private readonly PropertyUserService $propertyUserService,
        private readonly PersonService $personService,
        private readonly ClassificationService $classificationService,
        private readonly FileService $fileService,
        private readonly LocationService $locationService,
        private readonly GoogleMapsService $googleMapsService,
        private readonly EntityManagerInterface $entityManager,
        private readonly ManagerRegistry $registry
    ) {
    }

    /**
     * @param Place[] $places
     */
    public static function findPlaceFromPlacesByName(string $placeName, array $places): ?Place
    {
        $places = array_filter(
            array: $places,
            callback: function (Place $place) use ($placeName): bool {
                return (preg_match('/^' . $placeName . '/i', $place->getPlaceName()) === 1);
            }
        );

        if (count($places) > 0) {
            return array_values($places)[0];
        }

        return null;
    }

    /**
     * @param Place[] $quarters
     */
    public static function findQuarterFromQuartersByName(string $quarterName, array $quarters): ?Place
    {
        $quarters = array_filter(
            array: $quarters,
            callback: function (Place $quarter) use ($quarterName): bool {
                return (preg_match('/^' . $quarter->getPlaceName() . '/i', $quarterName) === 1);
            }
        );

        if (count($quarters) > 0) {
            return array_values($quarters)[0];
        }

        return null;
    }

    /**
     * @param Contact[] $contacts
     */
    public static function findContactFromContactsByName(string $contactName, array $contacts): ?Contact
    {
        $contacts = array_filter(
            array: $contacts,
            callback: function (Contact $contact) use ($contactName): bool {
                return (preg_match('/^' . $contact->getName() . '/i', $contactName) === 1);
            }
        );

        if (count($contacts) > 0) {
            return array_values($contacts)[0];
        }

        return null;
    }

    public function createAndSavePropertyExcelImportFromUploadedFile(
        UploadedFile $uploadedFile,
        AccountUser $accountUser
    ): PropertyExcelImport {
        $this->entityManager->beginTransaction();

        $file = $this->fileService->saveFileFromUploadedFile(
            uploadedFile: $uploadedFile,
            account: $accountUser->getAccount(),
            public: false,
            accountUser: $accountUser
        );

        $propertyExcelImport = new PropertyExcelImport();

        $propertyExcelImport
            ->setAccount($accountUser->getAccount())
            ->setCreatedByAccountUser($accountUser)
            ->setFile($file);

        $this->entityManager->persist($propertyExcelImport);
        $this->entityManager->flush();

        $this->entityManager->commit();

        return $propertyExcelImport;
    }

    public function createAndSaveProcessPropertyExcelImportTask(
        PropertyExcelImport $propertyExcelImport,
        AccountUser $accountUser
    ): ProcessPropertyExcelImportTask {
        $processPropertyExcelImportTask = new ProcessPropertyExcelImportTask();

        $processPropertyExcelImportTask
            ->setAccount($accountUser->getAccount())
            ->setCreatedByAccountUser($accountUser)
            ->setTaskStatus(TaskStatus::CREATED)
            ->setPropertyExcelImport($propertyExcelImport);

        $this->entityManager->persist($processPropertyExcelImportTask);
        $this->entityManager->flush();

        return $processPropertyExcelImportTask;
    }

    /**
     * @return PropertyExcelImport[]
     */
    public function fetchPropertyExcelImportsByAccount(Account $account, ?SortingOption $sortingOption = null): array
    {
        return $this->propertyExcelImportRepository->findByAccount(account: $account, sortingOption: $sortingOption);
    }

    public function fetchPropertyExcelImportsPaginatedByAccount(
        Account $account,
        int $firstResult,
        int $maxResults,
        ?SortingOption $sortingOption = null
    ): Paginator {
        return $this->propertyExcelImportRepository->findPaginatedByAccount(
            account: $account,
            firstResult: $firstResult,
            maxResults: $maxResults,
            sortingOption: $sortingOption,
        );
    }

    public function executeProcessPropertyExcelImportTaskById(int $id, bool $dryRun = false): void
    {
        $processPropertyExcelImportTask = $this->processPropertyExcelImportTaskRepository->find(id: $id);

        if ($processPropertyExcelImportTask === null) {
            throw new ProcessPropertyExcelImportTaskNotFoundException();
        }

        $this->executeProcessPropertyExcelImportTask(processPropertyExcelImportTask: $processPropertyExcelImportTask, dryRun: $dryRun);
    }

    public function executeProcessPropertyExcelImportTask(
        ProcessPropertyExcelImportTask $processPropertyExcelImportTask,
        bool $dryRun = false
    ): void {
        if ($processPropertyExcelImportTask->getTaskStatus() !== TaskStatus::CREATED) {
            throw new \RuntimeException('Task already processed or in progress');
        }

        if ($dryRun === false) {
            $processPropertyExcelImportTask->setTaskStatus(TaskStatus::IN_PROGRESS);

            $this->entityManager->flush();
        }

        try {
            $this->processExcelImport(
                propertyExcelImport: $processPropertyExcelImportTask->getPropertyExcelImport(),
                dryRun: $dryRun
            );
        } catch (ExcelImportErrorExceptionInterface $excelImportErrorException) {
            if ($dryRun === false) {
                $entityManager = $this->entityManager;

                if ($entityManager->isOpen() === false) {
                    $this->registry->resetManager();
                    $entityManager = $this->registry->getManagerForClass(ProcessPropertyExcelImportTask::class);

                    $processPropertyExcelImportTask = $this->processPropertyExcelImportTaskRepository->find($processPropertyExcelImportTask->getId());
                }

                $processPropertyExcelImportTask->setTaskStatus(TaskStatus::FAILED);

                $entityManager->flush();
            }

            throw $excelImportErrorException;
        }

        if ($dryRun === false) {
            $processPropertyExcelImportTask->setTaskStatus(TaskStatus::DONE);
            $this->entityManager->flush();
        }
    }

    public function executeProcessPropertyExcelImportTasks(int $amountOfTasksToProcess, bool $dryRun = false): void
    {
        foreach ($this->processPropertyExcelImportTaskRepository->findBy(['taskStatus' => TaskStatus::CREATED], null, $amountOfTasksToProcess) as $processPropertyExcelImportTask) {
            $this->executeProcessPropertyExcelImportTask(
                processPropertyExcelImportTask: $processPropertyExcelImportTask,
                dryRun: $dryRun
            );
        }
    }

    public function processExcelImport(PropertyExcelImport $propertyExcelImport, bool $dryRun = false): void
    {
        $this->cachedPersons = [];

        $file = $propertyExcelImport->getFile();
        $spreadsheet = IOFactory::load($this->fileService->getRealPathFromFile($file));
        $worksheet = $spreadsheet->getSheetByName('Erhebungsbogen');
        $errors = [];

        if ($worksheet === null) {
            $errors[] = ['row' => null, 'col' => null, 'error' => 'Ungültige Excel-Datei'];
            $propertyExcelImport->setValidationErrors($errors);
            $this->entityManager->flush();

            throw new ExcelImportValidationErrorException();
        }

        if ($this->excelImportValidator->isHeaderValid($worksheet) !== true) {
            if ($dryRun === false) {
                $errors[] = ['row' => null, 'col' => null, 'error' => 'Ungültige Excel-Datei'];
                $propertyExcelImport->setValidationErrors($errors);
                $this->entityManager->flush();
            }

            throw new ExcelImportValidationErrorException();
        }

        $highestRow = $worksheet->getHighestDataRow();
        $account = $propertyExcelImport->getAccount();
        $accountUser = $propertyExcelImport->getCreatedByAccountUser();
        $account->getAssignedPlace();
        $places = [$account->getAssignedPlace()];

        for ($row = self::FIRST_ROW; $row <= $highestRow; ++$row) {
            if ($this->isRowEmpty($worksheet, $row) === true) {
                continue;
            }

            $rowErrors = $this->excelImportValidator->validateRow(
                worksheet: $worksheet,
                row: $row,
                places: $places
            );

            if (count($rowErrors) > 0) {
                $errors = array_merge($errors, $rowErrors);
            }
        }

        if (count($errors) > 0) {
            if ($dryRun === false) {
                $propertyExcelImport->setValidationErrors($errors);
                $this->entityManager->flush();
            }

            throw new ExcelImportValidationErrorException();
        }

        if ($dryRun === false) {
            $this->entityManager->beginTransaction();
        }

        try {
            for ($row = self::FIRST_ROW; $row <= $highestRow; ++$row) {
                if ($this->isRowEmpty(worksheet: $worksheet, row: $row) === true) {
                    continue;
                }

                $internalNumber = $this->fetchInternalNumberFromRow(worksheet: $worksheet, row: $row);

                $buildingUnit = null;

                if ($internalNumber !== null) {
                    $buildingUnit = $this->buildingUnitService->fetchBuildingUnitByInternalNumber(internalNumber: $internalNumber, accounts: [$account]);
                }

                if ($buildingUnit === null) {
                    $buildingUnit = new BuildingUnit();

                    $propertyMarketingInformation = new PropertyMarketingInformation();
                    $propertySpacesData = new PropertySpacesData();
                    $propertyPricingInformation = new PropertyPricingInformation();
                    $propertyMarketingInformation->setPropertyPricingInformation($propertyPricingInformation);

                    $buildingUnit
                        ->setAccount($account)
                        ->setCreatedByAccountUser($accountUser)
                        ->setAddress(new Address())
                        ->setBuilding(new Building())
                        ->setArchived(false)
                        ->setDeleted(false)
                        ->setPropertyMarketingInformation($propertyMarketingInformation)
                        ->setPropertySpacesData($propertySpacesData);

                    $this->entityManager->persist($propertySpacesData);
                    $this->entityManager->persist($propertyMarketingInformation);
                    $this->entityManager->persist($propertyPricingInformation);
                }

                if ($worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::PROPERTY_USAGE, row: $row)->getValue() === 'L') {
                    $buildingUnit->setObjectIsEmpty(true);
                } else {
                    $buildingUnit->setObjectIsEmpty(false);
                }

                $this->mapColumns(
                    account: $account,
                    worksheet: $worksheet,
                    row: $row,
                    places: $places,
                    buildingUnit: $buildingUnit
                );

                $unitOfWork = $this->entityManager->getUnitOfWork();

                $getGeolocationPoint = $buildingUnit->getGeolocationPoint();
                $buildingUnitAddress = $buildingUnit->getAddress();
                $building = $buildingUnit->getBuilding();
                $propertySpacesData = $buildingUnit->getPropertySpacesData();

                if (
                    $getGeolocationPoint !== null
                    && $unitOfWork->getEntityState($getGeolocationPoint) === UnitOfWork::STATE_NEW
                ) {
                    $this->entityManager->persist($buildingUnit->getGeolocationPoint());
                }

                if ($unitOfWork->getEntityState($buildingUnitAddress) === UnitOfWork::STATE_NEW) {
                    $this->entityManager->persist($buildingUnitAddress);
                }

                if ($unitOfWork->getEntityState($building) === UnitOfWork::STATE_NEW) {
                    $buildingAddress = clone $buildingUnitAddress;
                    $buildingAddress->setId(null);

                    $this->entityManager->persist($buildingAddress);

                    $buildingPropertySpacesData = new PropertySpacesData();
                    $building->setPropertySpacesData($buildingPropertySpacesData);

                    $this->entityManager->persist($buildingPropertySpacesData);

                    $building
                        ->setAccount($account)
                        ->setCreatedByAccountUser($accountUser)
                        ->setDeleted(false)
                        ->setName($buildingAddress->getStreetName() . ' ' . $buildingAddress->getHouseNumber())
                        ->setAddress($buildingAddress)
                        ->setBarrierFreeAccess($buildingUnit->getBarrierFreeAccess())
                        ->setWheelchairAccessible(false)
                        ->setRampPresent(false)
                        ->setObjectForeclosed(false)
                        ->setPropertySpacesData($buildingPropertySpacesData);

                    $this->entityManager->persist($building);
                }

                if ($unitOfWork->getEntityState($propertySpacesData) === UnitOfWork::STATE_NEW) {
                    $this->entityManager->persist($propertySpacesData);
                }

                foreach ($buildingUnit->getPropertyUsers() as $propertyUser) {
                    if ($unitOfWork->getEntityState($propertyUser->getPerson()) === UnitOfWork::STATE_NEW) {
                        $propertyUser->getPerson()
                            ->setAccount($account)
                            ->setCreatedByAccountUser($accountUser)
                            ->setDeleted(false)
                            ->setAnonymized(false);

                        $this->entityManager->persist($propertyUser->getPerson());
                    }

                    foreach ($propertyUser->getPerson()->getContacts() as $contact) {
                        if ($unitOfWork->getEntityState($contact) === UnitOfWork::STATE_NEW) {
                            $contact
                                ->setAccount($account)
                                ->setCreatedByAccountUser($accountUser)
                                ->setDeleted(false)
                                ->setAnonymized(false);

                            $this->entityManager->persist($contact);
                        }
                    }

                    if ($unitOfWork->getEntityState($propertyUser) === UnitOfWork::STATE_NEW) {
                        $propertyUser
                            ->setAccount($account)
                            ->setCreatedByAccountUser($accountUser)
                            ->setPropertyRelationship(PropertyRelationship::TENANT)
                            ->setReceiveNewsletter(false);

                        $this->entityManager->persist($propertyUser);
                    }
                }

                foreach ($buildingUnit->getPropertyOwners() as $propertyOwner) {
                    if ($unitOfWork->getEntityState($propertyOwner->getPerson()) === UnitOfWork::STATE_NEW) {
                        $propertyOwner->getPerson()
                            ->setAccount($account)
                            ->setCreatedByAccountUser($accountUser)
                            ->setDeleted(false)
                            ->setAnonymized(false);

                        $this->entityManager->persist($propertyOwner->getPerson());
                    }

                    foreach ($propertyOwner->getPerson()->getContacts() as $contact) {
                        if ($unitOfWork->getEntityState($contact) === UnitOfWork::STATE_NEW) {
                            $contact
                                ->setAccount($account)
                                ->setCreatedByAccountUser($accountUser)
                                ->setDeleted(false)
                                ->setAnonymized(false);

                            $this->entityManager->persist($contact);
                        }
                    }

                    if ($unitOfWork->getEntityState($propertyOwner) === UnitOfWork::STATE_NEW) {
                        $propertyOwner
                            ->setAccount($account)
                            ->setCreatedByAccountUser($accountUser)
                            ->setPropertyOwnerStatus(PropertyOwnerStatus::CURRENT_OWNER)
                            ->setOwnershipType(OwnershipType::PROPERTY);

                        $this->entityManager->persist($propertyOwner);
                    }
                }

                if ($unitOfWork->getEntityState($buildingUnit) === UnitOfWork::STATE_NEW) {
                    $buildingUnit
                        ->setName($buildingUnitAddress->getStreetName() . ' ' . $buildingUnitAddress->getHouseNumber())
                        ->setObjectBecomesEmpty(false)
                        ->setReuseAgreed(false)
                        ->setWheelchairAccessible(false)
                        ->setRampPresent(false)
                        ->setObjectForeclosed(false);

                    $this->entityManager->persist($buildingUnit);

                    if ($dryRun === false) {
                        $this->entityManager->flush();
                    }
                }
            }

            if ($dryRun === false) {
                $this->entityManager->flush();
                $this->entityManager->commit();
            }
        } catch (ORMException $ormException) {
            $this->entityManager->rollback();
            throw new ExcelImportProcessingErrorException();
        } catch (Exception $exception) {
            $this->entityManager->rollback();
            throw new ExcelImportProcessingErrorException();
        }
    }

    public function mapColumns(
        Account $account,
        Worksheet $worksheet,
        int $row,
        array $places,
        BuildingUnit $buildingUnit
    ): void {
        $this
            ->fetchAndMapAddress(worksheet: $worksheet, row: $row, buildingUnit: $buildingUnit, places: $places)
            ->fetchAndMapInFloors(worksheet: $worksheet, row: $row, buildingUnit: $buildingUnit)
            ->fetchAndMapPlaceDescription(worksheet: $worksheet, row: $row, buildingUnit: $buildingUnit)
            ->fetchAndMapPropertyUsage(worksheet: $worksheet, row: $row, buildingUnit: $buildingUnit)
            ->fetchAndMapPropertyUser(worksheet: $worksheet, row: $row, buildingUnit: $buildingUnit, account: $account)
            ->fetchAndMapBusinessForm(worksheet: $worksheet, row: $row, buildingUnit: $buildingUnit)
            ->fetchAndMapPastPropertyUser(worksheet: $worksheet, row: $row, buildingUnit: $buildingUnit, account: $account)
            ->fetchAndMapShopWindowFrontWidth(worksheet: $worksheet, row: $row, buildingUnit: $buildingUnit)
            ->fetchAndMapShowroomAvailable(worksheet: $worksheet, row: $row, buildingUnit: $buildingUnit)
            ->fetchAndMapPropertyArea(worksheet: $worksheet, row: $row, buildingUnit: $buildingUnit)
            ->fetchAndMapPropertySpacesData(worksheet: $worksheet, row: $row, buildingUnit: $buildingUnit)
            ->fetchAndMapGroundLevelSalesArea(worksheet: $worksheet, row: $row, buildingUnit: $buildingUnit)
            ->fetchAndMapBarrierFreeAccess(worksheet: $worksheet, row: $row, buildingUnit: $buildingUnit)
            ->fetchAndMapEntranceDoorWidth(worksheet: $worksheet, row: $row, buildingUnit: $buildingUnit)
            ->fetchAndMapBuildingType(worksheet: $worksheet, row: $row, buildingUnit: $buildingUnit)
            ->fetchAndMapConstructionYear(worksheet: $worksheet, row: $row, buildingUnit: $buildingUnit)
            ->fetchAndMapNumberOfFloors(worksheet: $worksheet, row: $row, buildingUnit: $buildingUnit)
            ->fetchAndMapRoofShape(worksheet: $worksheet, row: $row, buildingUnit: $buildingUnit)
            ->fetchAndMapBuildingCondition(worksheet: $worksheet, row: $row, buildingUnit: $buildingUnit)
            ->fetchAndMapNumberOfParkingLots(worksheet: $worksheet, row: $row, buildingUnit: $buildingUnit)
            ->fetchAndMapRemark(worksheet: $worksheet, row: $row, buildingUnit: $buildingUnit)
            ->fetchAndMapInternalNumber(worksheet: $worksheet, row: $row, buildingUnit: $buildingUnit)
            ->fetchAndMapGisBuildingId(worksheet: $worksheet, row: $row, buildingUnit: $buildingUnit)
            ->fetchAndMapLongitudeAndLatitude(worksheet: $worksheet, row: $row, buildingUnit: $buildingUnit)
            ->fetchAndMapPicturesAvailable(worksheet: $worksheet, row: $row)
            ->fetchAndMapDatasetCollectDate(worksheet: $worksheet, row: $row, buildingUnit: $buildingUnit)
            ->fetchAndMapPropertyOwner(worksheet: $worksheet, row: $row, buildingUnit: $buildingUnit, account: $account);
    }

    private function isRowEmpty(Worksheet $worksheet, int $row): bool
    {
        foreach (ExcelImportColIndex::getColumns() as $columnIndex) {
            $value =  $worksheet->getCellByColumnAndRow($columnIndex, $row)->getValue();

            if ($value !== null && $value !== '') {
                return false;
            }
        }

        return true;
    }

    private function fetchInternalNumberFromRow(Worksheet $worksheet, int $row): ?string
    {
        $internalNumber = $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::INTERNAL_NUMBER, row: $row)->getValue();

        if ($internalNumber !== null && $internalNumber !== '') {
            return (string)$internalNumber;
        }

        return null;
    }

    private function fetchAndMapAddress(Worksheet $worksheet, int $row, BuildingUnit $buildingUnit, array $places): self
    {
        $placeName = trim($worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::PLACE_NAME, row: $row)->getValue());
        $streetName = trim($worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::STREET_NAME, row: $row)->getValue());
        $houseNumber = trim((string)$worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::HOUSE_NUMBER, row: $row)->getValue());
        $postalCode = trim((string)$worksheet->getCellByColumnAndRow(columnIndex:ExcelImportColIndex::POSTAL_CODE, row: $row)->getValue());
        $quarterName = $worksheet->getCellByColumnAndRow(columnIndex:ExcelImportColIndex::QUARTER_NAME, row: $row)->getValue();

        $place = self::findPlaceFromPlacesByName(placeName: $placeName, places: $places);
        $quarters = $this->locationService->fetchCityQuartersFromPlace(place: $place);

        $address = $buildingUnit->getAddress();

        $address
            ->setPlace($place)
            ->setStreetName($streetName)
            ->setHouseNumber($houseNumber)
            ->setPostalCode($postalCode);

        if ($quarterName !== null && $quarterName !== '') {
            $address->setQuarterPlace(self::findQuarterFromQuartersByName(quarterName: trim($quarterName), quarters: $quarters));
        }

        return $this;
    }

    private function fetchAndMapInFloors(Worksheet $worksheet, int $row, BuildingUnit $buildingUnit): self
    {
        $buildingUnit->setInFloors([
            $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::IN_FLOORS, row: $row)->getValue(),
        ]);

        return $this;
    }

    private function fetchAndMapPlaceDescription(Worksheet $worksheet, int $row, BuildingUnit $buildingUnit): self
    {
        $buildingUnit->setPlaceDescription($worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::PLACE_DESCRIPTION, row: $row)->getValue());

        return $this;
    }

    private function fetchAndMapPropertyUsage(Worksheet $worksheet, int $row, BuildingUnit $buildingUnit): self
    {
        $propertyUsageShortName = $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::PROPERTY_USAGE, row: $row)->getValue();

        if ($propertyUsageShortName === null || $propertyUsageShortName === '') {
            return $this;
        }

        $industryClassificationLevelOne = IndustryClassificationMapping::fetchIndustryClassificationLevelOneByShortName(trim($propertyUsageShortName));

        if ($industryClassificationLevelOne === null) {
            return $this;
        }

        $industryClassification = $this->classificationService->fetchIndustryClassificationByLevels(
            levelOne: $industryClassificationLevelOne,
            levelTwo: null,
            levelThree: null
        );

        if ($industryClassification === null) {
            return $this;
        }

        $buildingUnit->setCurrentUsageIndustryClassification($industryClassification);

        return $this;
    }

    private function fetchAndMapPropertyUser(Worksheet $worksheet, int $row, BuildingUnit $buildingUnit, Account $account): self
    {
        if ($worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::PROPERTY_USAGE, row: $row)->getValue() === 'L') {
            return $this;
        }

        $companyName = $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::COMPANY_NAME, row: $row)->getValue();
        $lastName = $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::NAME, row: $row)->getValue();

        if (
            ($companyName === null || $companyName === '')
            && ($lastName === null || $lastName === '')
        ) {
            return $this;
        }

        $personFromExcelData = self::createPerson(
            companyName: $companyName,
            salutation: $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::SALUTATION, row: $row)->getValue(),
            personTitle: $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::PERSON_TITLE, row: $row)->getValue(),
            firstName: $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::FIRST_NAME, row: $row)->getValue(),
            lastName: $lastName,
            phoneNumber: (string)$worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::PHONE_NUMBER, row: $row)->getValue(),
            email: $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::EMAIL, row: $row)->getValue()
        );

        if ($personFromExcelData === null) {
            return $this;
        }

        $existingPersonInDatabase = $this->personService->fetchPersonByPerson(accounts: [$account], person: $personFromExcelData);

        if ($existingPersonInDatabase !== null && $buildingUnit->getId() !== null) {
            $propertyUserInDatabase = $this->propertyUserService->fetchPropertyUserByBuildingUnitAndPerson(
                account: $account,
                withFromSubAccounts: false,
                buildingUnit: $buildingUnit,
                person: $existingPersonInDatabase
            );

            if ($propertyUserInDatabase !== null) {
                $propertyUserInDatabase->setPropertyUserStatus(PropertyUserStatus::CURRENT_PROPERTY_USER);

                $this
                    ->fetchAndMapPropertyUsageForPropertyUser(worksheet: $worksheet, row: $row, propertyUser: $propertyUserInDatabase)
                    ->fetchAndMapFamilybusiness(worksheet: $worksheet, row: $row, propertyUser: $propertyUserInDatabase)
                    ->fetchAndMapBranch(worksheet: $worksheet, row: $row, propertyUser: $propertyUserInDatabase)
                    ->fetchAndMapNaceCode(worksheet: $worksheet, row: $row, propertyUser: $propertyUserInDatabase);

                return $this;
            }
        }

        $propertyUser = new PropertyUser();

        $propertyUser->setPropertyUserStatus(PropertyUserStatus::CURRENT_PROPERTY_USER);

        $this
            ->fetchAndMapPropertyUsageForPropertyUser(worksheet: $worksheet, row: $row, propertyUser: $propertyUser)
            ->fetchAndMapFamilybusiness(worksheet: $worksheet, row: $row, propertyUser: $propertyUser)
            ->fetchAndMapBranch(worksheet: $worksheet, row: $row, propertyUser: $propertyUser)
            ->fetchAndMapNaceCode(worksheet: $worksheet, row: $row, propertyUser: $propertyUser);

        if ($existingPersonInDatabase !== null) {
            $propertyUser->setPerson($existingPersonInDatabase);
        } else {
            $existingPersonInCache = $this->fetchPersonFromCache($personFromExcelData);

            if ($existingPersonInCache !== null) {
                $propertyUser->setPerson($existingPersonInCache);
            } else {
                $propertyUser->setPerson($personFromExcelData);
                $this->addPersonToCache($personFromExcelData);
            }
        }

        $buildingUnit->getPropertyUsers()->add($propertyUser);

        $person = $propertyUser->getPerson();

        if (
            ($lastName === null || $lastName === '')
            || (
                $person->getPersonType() !== PersonType::COMPANY
                && $person->getPersonType() !== PersonType::COMMUNE
            )
        ) {
            return $this;
        }

        $contactFromExcelData = self::createContact(
            lastName: $lastName,
            salutation: $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::SALUTATION, row: $row)->getValue(),
            personTitle: $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::PERSON_TITLE, row: $row)->getValue(),
            firstName: $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::FIRST_NAME, row: $row)->getValue(),
            phoneNumber: (string)$worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::PHONE_NUMBER, row: $row)->getValue(),
            email: $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::EMAIL, row: $row)->getValue(),
            position: $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::POSITION, row: $row)->getValue()
        );

        if ($contactFromExcelData === null) {
            return $this;
        }

        if ($person->getContacts()->count() > 0) {
            $existingContact = self::findContactFromContactsByName(
                contactName: $contactFromExcelData->getName(),
                contacts: $person->getContacts()->toArray()
            );

            if ($existingContact !== null) {
                return $this;
            }
        }

        $contactFromExcelData->setPerson($person);
        $person->getContacts()->add($contactFromExcelData);

        return $this;
    }

    private function fetchAndMapPropertyUsageForPropertyUser(Worksheet $worksheet, int $row, PropertyUser $propertyUser): self
    {
        $propertyUsageShortName = $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::PROPERTY_USAGE, row: $row)->getValue();

        if ($propertyUsageShortName === null || $propertyUsageShortName === '') {
            return $this;
        }

        $industryClassificationLevelOne = IndustryClassificationMapping::fetchIndustryClassificationLevelOneByShortName(trim($propertyUsageShortName));

        if ($industryClassificationLevelOne === null) {
            return $this;
        }

        $industryClassification = $this->classificationService->fetchIndustryClassificationByLevels(
            levelOne: $industryClassificationLevelOne,
            levelTwo: null,
            levelThree: null
        );

        if ($industryClassification === null) {
            return $this;
        }

        $propertyUser->setIndustryClassification($industryClassification);

        return $this;
    }

    private function fetchAndMapFamilybusiness(Worksheet $worksheet, int $row, PropertyUser $propertyUser): self
    {
        $familybusiness = $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::FAMILYBUSINESS, row: $row)->getValue();

        if ($familybusiness !== null && $familybusiness !== '') {
            $familybusiness = strtolower(trim($familybusiness));
        }

        $propertyUser->setFamilybusiness($familybusiness === 'ja');

        return $this;
    }

    private function fetchAndMapBranch(Worksheet $worksheet, int $row, PropertyUser $propertyUser): self
    {
        $branch = $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::BRANCH, row: $row)->getValue();
        $branchType = null;

        if ($branch !== null && $branch !== '') {
            $branch = strtolower($branch);

            if ($branch === 'regional') {
                $branchType = BranchType::REGIONAL;
            } elseif ($branch === 'international') {
                $branchType = BranchType::INTERNATIONAL;
            } elseif ($branch === 'national') {
                $branchType = BranchType::NATIONAL;
            }
        }

        if ($branchType === null) {
            $propertyUser->setBranch(false);

            return $this;
        }

        $propertyUser
            ->setBranch(true)
            ->setBranchType($branchType);

        return $this;
    }

    private function fetchAndMapBusinessForm(Worksheet $worksheet, int $row, BuildingUnit $buildingUnit): self
    {
        $businessForm = $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::BUSINESS_FORM, row: $row)->getValue();

        if ($businessForm === null || $businessForm === '') {
            return $this;
        }

        $buildingUnit->setCurrentPropertyUsageBusinessType(PropertyUsageBusinessTypeMapping::fetchPropertyUsageBusinessTypeByShortName(trim($businessForm)));

        return $this;
    }

    private function fetchAndMapNaceCode(Worksheet $worksheet, int $row, PropertyUser $propertyUser): self
    {
        $naceCode = $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::NACE_CODE, row: $row)->getValue();

        if ($naceCode === null || $naceCode === '') {
            return $this;
        }

        if (preg_match('/^\d{5}$/', (string) $naceCode) === 1) {
            $naceCode = mb_substr(chunk_split((string) $naceCode, 2, '.'), 0, -1);
        }

        $naceClassification = $this->classificationService->fetchNaceClassificationByCode(code: $naceCode);

        if ($naceClassification === null) {
            return $this;
        }

        $propertyUser->setNaceClassification($naceClassification);

        return $this;
    }

    private function fetchAndMapPastPropertyUser(Worksheet $worksheet, int $row, BuildingUnit $buildingUnit, Account $account): self
    {
        $companyName = $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::PAST_PROPERTY_USERS, row: $row)->getValue();

        if ($companyName === null || $companyName === '') {
            return $this;
        }

        $personFromExcelData = self::createPerson(companyName: $companyName);

        if ($personFromExcelData === null) {
            return $this;
        }

        $existingPersonInDatabase = $this->personService->fetchPersonByPerson(accounts: [$account], person: $personFromExcelData);

        if ($existingPersonInDatabase !== null && $buildingUnit->getId() !== null) {
            $propertyUserInDatabase = $this->propertyUserService->fetchPropertyUserByBuildingUnitAndPerson(
                account: $account,
                withFromSubAccounts: false,
                buildingUnit: $buildingUnit,
                person: $existingPersonInDatabase
            );

            if ($propertyUserInDatabase !== null) {
                $propertyUserInDatabase
                    ->setPropertyUserStatus(PropertyUserStatus::FORMER_PROPERTY_USER)
                    ->setBranch(false)
                    ->setFamilybusiness(false);

                $this->fetchAndMapPastPropertyUsageForPropertyUser(worksheet: $worksheet, row: $row, propertyUser: $propertyUserInDatabase);

                if ($worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::PROPERTY_USAGE, row: $row)->getValue() === 'L') {
                    $this->fetchAndMapNaceCode(worksheet: $worksheet, row: $row, propertyUser: $propertyUserInDatabase);
                }

                return $this;
            }
        }

        $propertyUser = new PropertyUser();

        $propertyUser
            ->setPropertyUserStatus(PropertyUserStatus::FORMER_PROPERTY_USER)
            ->setBranch(false)
            ->setFamilybusiness(false);

        $this->fetchAndMapPastPropertyUsageForPropertyUser(worksheet: $worksheet, row: $row, propertyUser: $propertyUser);

        if ($worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::PROPERTY_USAGE, row: $row)->getValue() === 'L') {
            $this->fetchAndMapNaceCode(worksheet: $worksheet, row: $row, propertyUser: $propertyUser);
        }

        if ($existingPersonInDatabase !== null) {
            $propertyUser->setPerson($existingPersonInDatabase);
        } else {
            $existingPersonInCache = $this->fetchPersonFromCache($personFromExcelData);

            if ($existingPersonInCache !== null) {
                $propertyUser->setPerson($existingPersonInCache);
            } else {
                $propertyUser->setPerson($personFromExcelData);
                $this->addPersonToCache($personFromExcelData);
            }
        }

        $buildingUnit->getPropertyUsers()->add($propertyUser);

        return $this;
    }

    private function fetchAndMapPastPropertyUsageForPropertyUser(Worksheet $worksheet, int $row, PropertyUser $propertyUser): self
    {
        $pastPropertyUsageShortName = $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::PAST_PROPERTY_USAGE, row: $row)->getValue();

        if ($pastPropertyUsageShortName === null || $pastPropertyUsageShortName === '') {
            return $this;
        }

        $industryClassificationLevelOne = IndustryClassificationMapping::fetchIndustryClassificationLevelOneByShortName(trim($pastPropertyUsageShortName));

        if ($industryClassificationLevelOne === null) {
            return $this;
        }

        $industryClassification = $this->classificationService->fetchIndustryClassificationByLevels(
            levelOne: $industryClassificationLevelOne,
            levelTwo: null,
            levelThree: null
        );

        if ($industryClassification === null) {
            return $this;
        }

        $propertyUser->setIndustryClassification($industryClassification);

        return $this;
    }

    private function fetchAndMapShopWindowFrontWidth(Worksheet $worksheet, int $row, BuildingUnit $buildingUnit): self
    {
        $shopWindowFrontWidth = $worksheet->getCellByColumnAndRow(ExcelImportColIndex::SHOP_WINDOW_FRONT_WIDTH, $row)->getValue();

        if ($shopWindowFrontWidth === null || $shopWindowFrontWidth === '') {
            return $this;
        }

        $buildingUnit->setShopWindowFrontWidth(floatval(str_replace(',', '.', (string)$shopWindowFrontWidth)));
        $buildingUnit->setShopWindowAvailable(true);

        return $this;
    }

    private function fetchAndMapShowroomAvailable(Worksheet $worksheet, int $row, BuildingUnit $buildingUnit): self
    {
        $showroomAvailable = $worksheet->getCellByColumnAndRow(ExcelImportColIndex::SHOWROOM_AVAILABLE, $row)->getValue();

        if ($showroomAvailable === null || $showroomAvailable === '') {
            return $this;
        }

        $showroomAvailable = strtolower(trim($showroomAvailable));

        if ($showroomAvailable === 'ja') {
            $buildingUnit->setShowroomAvailable(true);
        }

        if ($showroomAvailable === 'nein') {
            $buildingUnit->setShowroomAvailable(false);
        }

        return $this;
    }

    private function fetchAndMapPropertyArea(Worksheet $worksheet, int $row, BuildingUnit $buildingUnit): self
    {
        $propertyArea = $worksheet->getCellByColumnAndRow(ExcelImportColIndex::PROPERTY_AREA, $row)->getValue();

        if ($propertyArea === null || $propertyArea === '') {
            return $this;
        }

        $buildingUnit->setAreaSize(floatval(str_replace(',', '.', (string)$propertyArea)));

        return $this;
    }

    private function fetchAndMapPropertySpacesData(Worksheet $worksheet, int $row, BuildingUnit $buildingUnit): self
    {
        if ($buildingUnit->getPropertySpacesData() === null) {
            $propertySpaceData = new PropertySpacesData();
            $buildingUnit->setPropertySpacesData($propertySpaceData);
        }

        $this
            ->fetchAndMapRetailSpace(worksheet: $worksheet, row: $row, propertySpacesData: $buildingUnit->getPropertySpacesData())
            ->fetchAndMapSubsidiarySpace(worksheet: $worksheet, row: $row, propertySpacesData: $buildingUnit->getPropertySpacesData())
            ->fetchAndMapUsableOutdoorArea(worksheet: $worksheet, row: $row, propertySpacesData: $buildingUnit->getPropertySpacesData());

        return $this;
    }

    private function fetchAndMapRetailSpace(Worksheet $worksheet, int $row, PropertySpacesData $propertySpacesData): self
    {
        $openSellingArea = $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::RETAIL_SPACE, row: $row)->getValue();

        if ($openSellingArea === null || $openSellingArea === '') {
            return $this;
        }

        $propertySpacesData->setRetailSpace(floatval(str_replace(',', '.', (string)$openSellingArea)));

        return $this;
    }

    private function fetchAndMapSubsidiarySpace(Worksheet $worksheet, int $row, PropertySpacesData $propertySpacesData): self
    {
        $subsidiaryArea = $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::SUBSIDIARY_SPACE, row: $row)->getValue();

        if ($subsidiaryArea === null || $subsidiaryArea === '') {
            return $this;
        }

        $propertySpacesData->setSubsidiarySpace(floatval(str_replace(',', '.', (string)$subsidiaryArea)));

        return $this;
    }

    private function fetchAndMapGroundLevelSalesArea(Worksheet $worksheet, int $row, BuildingUnit $buildingUnit): self
    {
        $groundLevelSalesArea = $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::GROUND_LEVEL_SALES_AREA, row: $row)->getValue();

        if ($groundLevelSalesArea !== null && $groundLevelSalesArea !== '') {
            $groundLevelSalesArea = strtolower(trim($groundLevelSalesArea));
        }

        $buildingUnit->setGroundLevelSalesArea($groundLevelSalesArea === 'ja');

        return $this;
    }

    private function fetchAndMapUsableOutdoorArea(Worksheet $worksheet, int $row, PropertySpacesData $propertySpacesData): self
    {
        $usableOutdoorArea = $worksheet->getCellByColumnAndRow(ExcelImportColIndex::USABLE_OUTDOOR_AREA, $row)->getValue();

        if ($usableOutdoorArea === null || $usableOutdoorArea === '') {
            $propertySpacesData->setUsableOutdoorAreaPossibility(null);

            return $this;
        }

        $propertySpacesData
            ->setUsableOutdoorAreaPossibility(UsableOutdoorAreaPossibility::IS_AVAILABLE)
            ->setUsableOutdoorArea(floatval(str_replace(',', '.', (string)$usableOutdoorArea)));

        return $this;
    }

    private function fetchAndMapBarrierFreeAccess(Worksheet $worksheet, int $row, BuildingUnit $buildingUnit): self
    {
        $barrierFreeAccess = $worksheet->getCellByColumnAndRow(ExcelImportColIndex::BARRIER_FREE_ACCESS, $row)->getValue();

        if ($barrierFreeAccess === null || $barrierFreeAccess === '') {
            return $this;
        }

        $buildingUnit->setBarrierFreeAccess(BarrierFreeAccessMapping::fetchBarrierFreeAccessByName(trim($barrierFreeAccess)));

        return $this;
    }

    private function fetchAndMapEntranceDoorWidth(Worksheet $worksheet, int $row, BuildingUnit $buildingUnit): self
    {
        $entranceDoorWidth = $worksheet->getCellByColumnAndRow(ExcelImportColIndex::ENTRANCE_DOOR_WIDTH, $row)->getValue();

        if ($entranceDoorWidth === null || $entranceDoorWidth === '') {
            return $this;
        }

        $buildingUnit->setEntranceDoorWidth(floatval(str_replace(',', '.', (string)$entranceDoorWidth)));

        return $this;
    }

    private function fetchAndMapBuildingType(Worksheet $worksheet, int $row, BuildingUnit $buildingUnit): self
    {
        $buildingTypeName = $worksheet->getCellByColumnAndRow(ExcelImportColIndex::BUILDING_TYPE, $row)->getValue();

        if ($buildingTypeName === null || $buildingTypeName === '') {
            return $this;
        }

        $buildingUnit->getBuilding()->setBuildingType(BuildingTypeMapping::fetchBuildingTypeByShortName(trim($buildingTypeName)));

        return $this;
    }

    private function fetchAndMapConstructionYear(Worksheet $worksheet, int $row, BuildingUnit $buildingUnit): self
    {
        $constructionYear = $worksheet->getCellByColumnAndRow(ExcelImportColIndex::CONSTRUCTION_YEAR, $row)->getValue();

        if ($constructionYear === null || $constructionYear === '') {
            return $this;
        }

        $buildingUnit->getBuilding()->setConstructionYear(intval($constructionYear));

        return $this;
    }

    private function fetchAndMapNumberOfFloors(Worksheet $worksheet, int $row, BuildingUnit $buildingUnit): self
    {
        $numberOfFloors = $worksheet->getCellByColumnAndRow(ExcelImportColIndex::NUMBER_OF_FLOORS, $row)->getValue();

        if ($numberOfFloors === null || $numberOfFloors === '') {
            return $this;
        }

        $buildingUnit->getBuilding()->setNumberOfFloors(intval($numberOfFloors));

        return $this;
    }

    private function fetchAndMapRoofShape(Worksheet $worksheet, int $row, BuildingUnit $buildingUnit): self
    {
        $roofShapeName = $worksheet->getCellByColumnAndRow(ExcelImportColIndex::ROOF_SHAPE, $row)->getValue();

        if ($roofShapeName === null || $roofShapeName === '') {
            return $this;
        }

        $buildingUnit->getBuilding()->setRoofShape(RoofShapeMapping::fetchRoofShapeByName(trim($roofShapeName)));

        return $this;
    }

    private function fetchAndMapBuildingCondition(Worksheet $worksheet, int $row, BuildingUnit $buildingUnit): self
    {
        $buildingCondition = $worksheet->getCellByColumnAndRow(ExcelImportColIndex::BUILDING_CONDITION, $row)->getValue();

        if ($buildingCondition === null || $buildingCondition === '') {
            return $this;
        }

        $buildingUnit->getBuilding()->setBuildingCondition(BuildingConditionMapping::fetchBuildingConditionByName(trim($buildingCondition)));

        return $this;
    }

    private function fetchAndMapNumberOfParkingLots(Worksheet $worksheet, int $row, BuildingUnit $buildingUnit): self
    {
        $numberOfParkingLots = $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::NUMBER_OF_PARKING_LOTS, row: $row)->getValue();

        if ($numberOfParkingLots === null || $numberOfParkingLots === '') {
            return $this;
        }

        $buildingUnit->setNumberOfParkingLots(intval($numberOfParkingLots));

        return $this;
    }

    private function fetchAndMapRemark(Worksheet $worksheet, int $row, BuildingUnit $buildingUnit): self
    {
        $buildingUnit->setInternalNote($worksheet->getCellByColumnAndRow(columnIndex:ExcelImportColIndex::REMARK, row: $row)->getValue());

        return $this;
    }

    private function fetchAndMapInternalNumber(Worksheet $worksheet, int $row, BuildingUnit $buildingUnit): self
    {
        $buildingUnit->setInternalNumber($this->fetchInternalNumberFromRow(worksheet: $worksheet, row: $row));

        return $this;
    }

    private function fetchAndMapGisBuildingId(Worksheet $worksheet, int $row, BuildingUnit $buildingUnit): self
    {
        $buildingUnit->getBuilding()->setGisBuildingId(
            $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::GIS_BUILDING_ID, row: $row)->getValue()
        );

        return $this;
    }

    private function fetchAndMapLongitudeAndLatitude(Worksheet $worksheet, int $row, BuildingUnit $buildingUnit): self
    {
        $longitude = $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::LONGITUDE, row: $row)->getFormattedValue();
        $latitude = $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::LATITUDE, row: $row)->getFormattedValue();

        if (
            $longitude === null
            || $longitude === ''
            || $latitude === null
            || $latitude === ''
        ) {
            if ($buildingUnit->getGeolocationPoint() !== null) {
                return $this;
            }

            if ($this->googleMapsService->isServiceAvailable() !== true) {
                return $this;
            }

            $address = $buildingUnit->getAddress();

            try {
                $googleApiResponse = $this->googleMapsService->fetchGeoDataByAddress(
                    streetName: $address->getStreetName(),
                    houseNumber: $address->getHouseNumber(),
                    postalCode: $address->getPostalCode(),
                    placeName: $address->getPlace()->getPlaceName()
                );

                if (
                    count($googleApiResponse->results) > 0
                    && isset($googleApiResponse->results[0]->geometry)
                    && isset($googleApiResponse->results[0]->geometry->location)
                ) {
                    $geolocationPoint = GeolocationPoint::createFromLatitudeAndLongitude(
                        latitude: $googleApiResponse->results[0]->geometry->location->lat,
                        longitude: $googleApiResponse->results[0]->geometry->location->lng
                    );

                    $buildingUnit->setGeolocationPoint($geolocationPoint);
                }
            } catch (GoogleMapsException $googleMapsException) {
            }
        } else {
            if ($buildingUnit->getGeolocationPoint() === null) {
                $geolocationPoint = GeolocationPoint::createFromLatitudeAndLongitude(
                    latitude: floatval($latitude),
                    longitude: floatval($longitude)
                );

                $buildingUnit->setGeolocationPoint($geolocationPoint);
            } else {
                $buildingUnit->getGeolocationPoint()->setPoint(new Point($longitude, $latitude));
            }
        }

        return $this;
    }

    private function fetchAndMapPicturesAvailable(Worksheet $worksheet, int $row): self
    {
        $picturesAvailable = $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::PICTURES_AVAILABLE, row: $row)->getValue();

        return $this;
    }

    private function fetchAndMapDatasetCollectDate(Worksheet $worksheet, int $row, BuildingUnit $buildingUnit): self
    {
        $datasetCollectDate = $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::DATASET_COLLECT_DATE, row: $row)->getFormattedValue();

        $dateParts = explode('/', $datasetCollectDate);
        $month = sprintf("%02d", $dateParts[0]);
        $day = sprintf("%02d", $dateParts[1]);
        $year = $dateParts[2];

        $buildingUnit->setCreatedAt(new \DateTimeImmutable($year . '-' . $month . '-' . $day));

        return $this;
    }

    private function fetchAndMapPropertyOwner(Worksheet $worksheet, int $row, BuildingUnit $buildingUnit, Account $account): self
    {
        $companyName = $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::PO_COMPANY_NAME, row: $row)->getValue();
        $lastName = $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::PO_NAME, row: $row)->getValue();

        if (
            ($companyName === null || $companyName === '')
            && ($lastName === null || $lastName === '')
        ) {
            return $this;
        }

        $personFromExcelData = self::createPerson(
            companyName: $companyName,
            salutation: $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::PO_SALUTATION, row: $row)->getValue(),
            personTitle: $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::PO_PERSON_TITLE, row: $row)->getValue(),
            firstName: $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::PO_FIRST_NAME, row: $row)->getValue(),
            lastName: $lastName,
            phoneNumber: (string)$worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::PO_PHONE_NUMBER, row: $row)->getValue(),
            email: $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::PO_EMAIL, row: $row)->getValue(),
            streetName: $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::PO_STREET_NAME, row: $row)->getValue(),
            houseNumber: (string)$worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::PO_HOUSE_NUMBER, row: $row)->getValue(),
            postalCode: (string)$worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::PO_POSTAL_CODE, row: $row)->getValue(),
            placeName:  $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::PO_PLACE_NAME, row: $row)->getValue()
        );

        if ($personFromExcelData === null) {
            return $this;
        }

        $existingPersonInDatabase = $this->personService->fetchPersonByPerson(accounts: [$account], person: $personFromExcelData);

        if ($existingPersonInDatabase !== null && $buildingUnit->getId() !== null) {
            $propertyOwnerInDatabase = $this->propertyOwnerService->fetchPropertyOwnerByBuildingUnitAndPerson(
                account: $account,
                withFromSubAccounts: false,
                buildingUnit: $buildingUnit,
                person: $existingPersonInDatabase
            );

            if ($propertyOwnerInDatabase !== null) {
                return $this;
            }
        }

        $propertyOwner = new PropertyOwner();

        if ($existingPersonInDatabase !== null) {
            $propertyOwner->setPerson($existingPersonInDatabase);
        } else {
            $existingPersonInCache = $this->fetchPersonFromCache($personFromExcelData);

            if ($existingPersonInCache !== null) {
                $propertyOwner->setPerson($existingPersonInCache);
            } else {
                $propertyOwner->setPerson($personFromExcelData);
                $this->addPersonToCache($personFromExcelData);
            }
        }

        $buildingUnit->getPropertyOwners()->add($propertyOwner);

        $person = $propertyOwner->getPerson();

        if (
            ($lastName === null || $lastName === '')
            || (
                $person->getPersonType() !== PersonType::COMPANY
                && $person->getPersonType() !== PersonType::COMMUNE
            )
        ) {
            return $this;
        }

        $contactFromExcelData = self::createContact(
            lastName: $lastName,
            salutation: $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::PO_SALUTATION, row: $row)->getValue(),
            personTitle: $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::PO_PERSON_TITLE, row: $row)->getValue(),
            firstName: $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::PO_FIRST_NAME, row: $row)->getValue(),
            phoneNumber: (string)$worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::PO_PHONE_NUMBER, row: $row)->getValue(),
            email: $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::PO_EMAIL, row: $row)->getValue(),
            position: $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::PO_POSITION, row: $row)->getValue()
        );

        if ($contactFromExcelData === null) {
            return $this;
        }

        if ($person->getContacts()->count() > 0) {
            $existingContact = self::findContactFromContactsByName(
                contactName: $contactFromExcelData->getName(),
                contacts: $person->getContacts()->toArray()
            );

            if ($existingContact !== null) {
                return $this;
            }
        }

        $contactFromExcelData->setPerson($person);
        $person->getContacts()->add($contactFromExcelData);

        return $this;
    }

    private static function createPerson(
        ?string $companyName = null,
        ?string $salutation = null,
        ?string $personTitle = null,
        ?string $firstName = null,
        ?string $lastName = null,
        ?string $phoneNumber = null,
        ?string $email = null,
        ?string $streetName = null,
        ?string $houseNumber = null,
        ?string $postalCode = null,
        ?string $placeName = null
    ): ?Person {
        if ($companyName !== null && $companyName !== '') {
            $personType = PersonType::COMPANY;
        } elseif (
            ($companyName === null || $companyName === '')
            && ($lastName !== null && $lastName !== '')
        ) {
            $personType = PersonType::NATURAL_PERSON;
        } else {
            return null;
        }

        $person = new Person();

        $person->setPersonType($personType);

        if ($personType === PersonType::COMPANY) {
            $person->setName($companyName);
        } elseif ($personType === PersonType::NATURAL_PERSON) {
            $person->setName($lastName);

            if ($salutation !== null && $salutation !== '') {
                $person->setSalutation(SalutationMapping::fetchSalutationByName($salutation));
            }

            if ($personTitle !== null && $personTitle !== '') {
                $person->setPersonTitle(PersonTitleMapping::fetchPersonTitleByName($personTitle));
            }

            if ($firstName !== '') {
                $person->setFirstName($firstName);
            }
        }

        if ($phoneNumber !== '') {
            $person->setPhoneNumber($phoneNumber);
        }

        if ($email !== '') {
            $person->setEmail($email);
        }

        if ($streetName !== '') {
            $person->setStreetName($streetName);
        }

        if ($houseNumber !== '') {
            $person->setHouseNumber($houseNumber);
        }

        if ($postalCode !== '') {
            $person->setPostalCode($postalCode);
        }

        if ($placeName !== '') {
            $person->setPlaceName($placeName);
        }

        return $person;
    }

    private static function createContact(
        string $lastName,
        ?string $salutation = null,
        ?string $personTitle = null,
        ?string $firstName = null,
        ?string $phoneNumber = null,
        ?string $email = null,
        ?string $position = null
    ): ?Contact {
        if ($lastName === '') {
            return null;
        }

        $contact = new Contact();

        if ($salutation !== null && $salutation !== '') {
            $contact->setSalutation(SalutationMapping::fetchSalutationByName($salutation));
        }

        if ($personTitle !== null && $personTitle !== '') {
            $contact->setPersonTitle(PersonTitleMapping::fetchPersonTitleByName($personTitle));
        }

        if ($firstName !== null && $firstName !== '') {
            $contact->setName($firstName . ' ' . $lastName);
        } else {
            $contact->setName($lastName);
        }

        if ($phoneNumber !== '') {
            $contact->setPhoneNumber($phoneNumber);
        }

        if ($email !== '') {
            $contact->setEmail($email);
        }

        if ($position !== '') {
            $contact->setPosition($position);
        }

        return $contact;
    }

    private function fetchPersonFromCache(Person $person): ?Person
    {
        $checksum = $person->createChecksum();

        if (isset($this->cachedPersons[$checksum])) {
            return $this->cachedPersons[$checksum];
        } else {
            return null;
        }
    }

    private function addPersonToCache(Person $person): void
    {
        $this->cachedPersons[$person->createChecksum()] = $person;
    }
}
