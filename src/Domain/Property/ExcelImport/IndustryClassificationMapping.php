<?php

declare(strict_types=1);

namespace App\Domain\Property\ExcelImport;

class IndustryClassificationMapping
{
    public static function fetchIndustryClassificationLevelOneByShortName(string $name): ?int
    {
        return match (strtolower($name)) {
            strtolower('E') => 1,
            strtolower('D') => 2,
            strtolower('G') => 3,
            strtolower('KK') => 4,
            strtolower('PA') => 5,
            strtolower('BL') => 6,
            strtolower('GW') => 7,
            strtolower('SW') => 8,
            strtolower('V') => 9,
            strtolower('IV') => 10,
            strtolower('PG') => 11,
            strtolower('KW') => 12,
            strtolower('GH') => 13,
            strtolower('AG') => 14,
            strtolower('W') => 15,
            strtolower('NG') => 16,
            default => null
        };
    }
}
