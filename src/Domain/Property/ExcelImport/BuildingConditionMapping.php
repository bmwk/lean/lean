<?php

declare(strict_types=1);

namespace App\Domain\Property\ExcelImport;

use App\Domain\Entity\Property\BuildingCondition;

class BuildingConditionMapping
{
    public static function fetchBuildingConditionByName(string $name): ?BuildingCondition
    {
        return match (strtolower($name)) {
            strtolower(BuildingCondition::LIKE_NEW->getName()) => BuildingCondition::LIKE_NEW,
            strtolower(BuildingCondition::SANITIZED->getName()) => BuildingCondition::SANITIZED,
            strtolower(BuildingCondition::PARTIALLY_RENOVATED->getName()) => BuildingCondition::PARTIALLY_RENOVATED,
            strtolower(BuildingCondition::MODERNIZED->getName()) => BuildingCondition::MODERNIZED,
            strtolower(BuildingCondition::UNRENOVATED->getName()) => BuildingCondition::UNRENOVATED,
            strtolower(BuildingCondition::RENOVATION_NEEDED->getName()) => BuildingCondition::RENOVATION_NEEDED,
            strtolower(BuildingCondition::DEMOLITION_OBJECT->getName()) => BuildingCondition::DEMOLITION_OBJECT,
            strtolower(BuildingCondition::DILAPIDATED->getName()) => BuildingCondition::DILAPIDATED,
            strtolower(BuildingCondition::GROOMED->getName()) => BuildingCondition::GROOMED,
            strtolower(BuildingCondition::GUTTED->getName()) => BuildingCondition::GUTTED,
            strtolower(BuildingCondition::SHELL->getName()) => BuildingCondition::SHELL,
            strtolower(BuildingCondition::NEW_BUILDING->getName()) => BuildingCondition::NEW_BUILDING,
            default => null
        };
    }
}
