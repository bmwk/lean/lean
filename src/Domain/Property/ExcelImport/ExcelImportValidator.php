<?php

declare(strict_types=1);

namespace App\Domain\Property\ExcelImport;

use App\Domain\Entity\Place;
use App\Domain\Entity\PlaceType;
use App\Domain\Location\LocationService;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class ExcelImportValidator
{
    private const HEADER_ROW_NUMBER = 5;

    /**
     * @var string[]
     */
    private array $headerColumns;

    public function __construct(
        private readonly LocationService $locationService
    ) {
        $this->headerColumns = ExcelImportColIndex::getHeaderColumns();
    }

    public function isHeaderValid(Worksheet $worksheet): bool
    {
        for ($col = 1; $col <= count($this->headerColumns); ++$col) {
            $value = $worksheet->getCellByColumnAndRow(columnIndex: $col, row: self::HEADER_ROW_NUMBER)->getValue();

            if ($value !== $this->headerColumns[$col]) {
                return false;
            }
        }

        return true;
    }

    public function validateRow(Worksheet $worksheet, int $row, array $places): array
    {
        $errors = [];

        $validatePlaceNameResult = $this->validatePlaceName(worksheet: $worksheet, row: $row, places: $places);

        if ($validatePlaceNameResult !== null) {
            $errors[] = $validatePlaceNameResult;
        }

        $placeName = $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::PLACE_NAME, row: $row)->getValue();

        if ($placeName !== null) {
            $place = ExcelImportService::findPlaceFromPlacesByName(placeName: trim($placeName), places: $places);
        } else {
            $place = null;
        }

        $validateStreetNameResult = $this->validateStreetName(worksheet: $worksheet, row: $row);

        if ($validateStreetNameResult !== null) {
            $errors[] = $validateStreetNameResult;
        }

        $validateHouseNumberResult = $this->validateHouseNumber(worksheet: $worksheet, row: $row);

        if ($validateHouseNumberResult !== null) {
            $errors[] = $validateHouseNumberResult;
        }

        if ($place !== null) {
            $validatePostalCodeResult = $this->validatePostalCode(worksheet: $worksheet, row: $row, place: $place);

            if ($validatePostalCodeResult !== null) {
                $errors[] = $validatePostalCodeResult;
            }

            $validateQuarterNameResult = $this->validateQuarterName(worksheet: $worksheet, row: $row, place: $place);

            if ($validateQuarterNameResult !== null) {
                $errors[] = $validateQuarterNameResult;
            }
        }

        $validateInFloorsResult = $this->validateInFloors(worksheet: $worksheet, row: $row);

        if ($validateInFloorsResult !== null) {
            $errors[] = $validateInFloorsResult;
        }

        $validatePlaceDescriptionResult = $this->validatePlaceDescription(worksheet: $worksheet, row: $row);

        if ($validatePlaceDescriptionResult !== null) {
            $errors[] = $validatePlaceDescriptionResult;
        }

        $validateUsageResult = $this->validateUsage(worksheet: $worksheet, row: $row);

        if ($validateUsageResult !== null) {
            $errors[] = $validateUsageResult;
        }

        $validateCompanyNameResult = $this->validateCompanyName(worksheet: $worksheet, row: $row);

        if ($validateCompanyNameResult !== null) {
            $errors[] = $validateCompanyNameResult;
        }

        $validateBranchResult = $this->validateBranch(worksheet: $worksheet, row: $row);

        if ($validateBranchResult !== null) {
            $errors[] = $validateBranchResult;
        }

        $validateFamilybusinessResult = $this->validateFamilybusiness(worksheet: $worksheet, row: $row);

        if ($validateFamilybusinessResult !== null) {
            $errors[] = $validateFamilybusinessResult;
        }

        $validateIndustryResult = $this->validateIndustry(worksheet: $worksheet, row: $row);

        if ($validateIndustryResult !== null) {
            $errors[] = $validateIndustryResult;
        }

        $validateBusinessFormResult = $this->validateBusinessForm(worksheet: $worksheet, row: $row);

        if ($validateBusinessFormResult !== null) {
            $errors[] = $validateBusinessFormResult;
        }

        $validateNaceCodeResult = $this->validateNaceCode(worksheet: $worksheet, row: $row);

        if ($validateNaceCodeResult !== null) {
            $errors[] = $validateNaceCodeResult;
        }

        $validatePastUsageResult = $this->validatePastUsage(worksheet: $worksheet, row: $row);

        if ($validatePastUsageResult !== null) {
            $errors[] = $validatePastUsageResult;
        }

        $validatePastPropertyUsersResult = $this->validatePastPropertyUsers(worksheet: $worksheet, row: $row);

        if ($validatePastPropertyUsersResult !== null) {
            $errors[] = $validatePastPropertyUsersResult;
        }

        $validateShopWindowFrontWidthResult = $this->validateShopWindowFrontWidth(worksheet: $worksheet, row: $row);

        if ($validateShopWindowFrontWidthResult !== null) {
            $errors[] = $validateShopWindowFrontWidthResult;
        }

        $validateShowroomAvailableResult = $this->validateShowroomAvailable(worksheet: $worksheet, row: $row);

        if ($validateShowroomAvailableResult !== null) {
            $errors[] = $validateShowroomAvailableResult;
        }

        $validatePropertyAreaResult = $this->validatePropertyArea(worksheet: $worksheet, row: $row);

        if ($validatePropertyAreaResult !== null) {
            $errors[] = $validatePropertyAreaResult;
        }

        $validateRetailSpaceResult = $this->validateRetailSpace(worksheet: $worksheet, row: $row);

        if ($validateRetailSpaceResult !== null) {
            $errors[] = $validateRetailSpaceResult;
        }

        $validateSubsidiarySpaceResult = $this->validateSubsidiarySpace(worksheet: $worksheet, row: $row);

        if ($validateSubsidiarySpaceResult !== null) {
            $errors[] = $validateSubsidiarySpaceResult;
        }

        $validateGroundLevelSalesAreaResult = $this->validateGroundLevelSalesArea(worksheet: $worksheet, row: $row);

        if ($validateGroundLevelSalesAreaResult !== null) {
            $errors[] = $validateGroundLevelSalesAreaResult;
        }

        $validateUsableOutdoorAreaResult = $this->validateUsableOutdoorArea(worksheet: $worksheet, row: $row);

        if ($validateUsableOutdoorAreaResult !== null) {
            $errors[] = $validateUsableOutdoorAreaResult;
        }

        $validateBarrierFreeAccessResult = $this->validateBarrierFreeAccess(worksheet: $worksheet, row: $row);

        if ($validateBarrierFreeAccessResult !== null) {
            $errors[] = $validateBarrierFreeAccessResult;
        }

        $validateEntranceDoorWidthResult = $this->validateEntranceDoorWidth(worksheet: $worksheet, row: $row);

        if ($validateEntranceDoorWidthResult !== null) {
            $errors[] = $validateEntranceDoorWidthResult;
        }

        $validateBuildingTypeResult = $this->validateBuildingType(worksheet: $worksheet, row: $row);

        if ($validateBuildingTypeResult !== null) {
            $errors[] = $validateBuildingTypeResult;
        }

        $validateConstructionYearResult = $this->validateConstructionYear(worksheet: $worksheet, row: $row);

        if ($validateConstructionYearResult !== null) {
            $errors[] = $validateConstructionYearResult;
        }

        $validateNumberOfFloorsResult = $this->validateNumberOfFloors(worksheet: $worksheet, row: $row);

        if ($validateNumberOfFloorsResult !== null) {
            $errors[] = $validateNumberOfFloorsResult;
        }

        $validateRoofShapeResult = $this->validateRoofShape(worksheet: $worksheet, row: $row);

        if ($validateRoofShapeResult !== null) {
            $errors[] = $validateRoofShapeResult;
        }

        $validateBuildingConditionResult = $this->validateBuildingCondition(worksheet: $worksheet, row: $row);

        if ($validateBuildingConditionResult !== null) {
            $errors[] = $validateBuildingConditionResult;
        }

        $validateNumberOfParkingLotsResult = $this->validateNumberOfParkingLots(worksheet: $worksheet, row: $row);

        if ($validateNumberOfParkingLotsResult !== null) {
            $errors[] = $validateNumberOfParkingLotsResult;
        }

        $validateRemarkResult = $this->validateRemark(worksheet: $worksheet, row: $row);

        if ($validateRemarkResult !== null) {
            $errors[] = $validateRemarkResult;
        }

        $validateInternalNumberResult = $this->validateInternalNumber(worksheet: $worksheet, row: $row);

        if ($validateInternalNumberResult !== null) {
            $errors[] = $validateInternalNumberResult;
        }

        $validateGisBuildingIdResult = $this->validateGisBuildingId(worksheet: $worksheet, row: $row);

        if ($validateGisBuildingIdResult !== null) {
            $errors[] = $validateGisBuildingIdResult;
        }

        $validateLongitudeAndLatitudeResult = $this->validateLongitudeAndLatitude(worksheet: $worksheet, row: $row);

        if ($validateLongitudeAndLatitudeResult !== null) {
            $errors[] = $validateLongitudeAndLatitudeResult;
        }

        $validatePicturesAvailableResult = $this->validatePicturesAvailable(worksheet: $worksheet, row: $row);

        if ($validatePicturesAvailableResult !== null) {
            $errors[] = $validatePicturesAvailableResult;
        }

        $validateDatasetCollectDateResult = $this->validateDatasetCollectDate(worksheet: $worksheet, row: $row);

        if ($validateDatasetCollectDateResult !== null) {
            $errors[] = $validateDatasetCollectDateResult;
        }

        $validateDatasetCapturedByResult = $this->validateDatasetCapturedBy(worksheet: $worksheet, row: $row);

        if ($validateDatasetCapturedByResult !== null) {
            $errors[] = $validateDatasetCapturedByResult;
        }

        return $errors;
    }

    private function validatePlaceName(Worksheet $worksheet, int $row, array $places): ?array
    {
        $cell = $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::PLACE_NAME, row: $row);

        if ($cell->getValue() === null) {
            return ['row' => $row, 'col' => ExcelImportColIndex::PLACE_NAME, 'error' => 'Der Ort darf nicht leer sein'];
        }

        if ($cell->getDataType() !== DataType::TYPE_STRING && $cell->getDataType() !== DataType::TYPE_STRING2) {
            return ['row' => $row, 'col' => ExcelImportColIndex::PLACE_NAME, 'error' => 'ungültiger Datentyp'];
        }

        $place = ExcelImportService::findPlaceFromPlacesByName(trim($cell->getValue()), $places);

        if ($place === null) {
            return ['row' => $row, 'col' => ExcelImportColIndex::PLACE_NAME, 'error' => 'Der Ort ist ungültig'];
        }

        return null;
    }

    private function validateStreetName(Worksheet $worksheet, int $row): ?array
    {
        $cell = $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::STREET_NAME, row: $row);

        if ($cell->getValue() === null) {
            return ['row' => $row, 'col' => ExcelImportColIndex::STREET_NAME, 'error' => 'Die Straße darf nicht leer sein'];
        }

        if ($cell->getDataType() !== DataType::TYPE_STRING && $cell->getDataType() !== DataType::TYPE_STRING2) {
            return ['row' => $row, 'col' => ExcelImportColIndex::STREET_NAME, 'error' => 'ungültiger Datentyp'];
        }

        if (strlen((string)$cell->getValue()) > 190) {
            return ['row' => $row, 'col' => ExcelImportColIndex::STREET_NAME, 'error' => 'Wert zu lang'];
        }

        return null;
    }

    private function validateHouseNumber(Worksheet $worksheet, int $row): ?array
    {
        $cell = $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::HOUSE_NUMBER, row: $row);

        if ($cell->getValue() === null) {
            return ['row' => $row, 'col' => ExcelImportColIndex::HOUSE_NUMBER, 'error' => 'Die Haus-Nummer darf nicht leer sein'];
        }

        if ($cell->getDataType() !== DataType::TYPE_NUMERIC && $cell->getDataType() !== DataType::TYPE_STRING && $cell->getDataType() !== DataType::TYPE_STRING2) {
            return ['row' => $row, 'col' => ExcelImportColIndex::HOUSE_NUMBER, 'error' => 'ungültiger Datentyp'];
        }

        if (strlen((string)$cell->getValue()) > 20) {
            return ['row' => $row, 'col' => ExcelImportColIndex::HOUSE_NUMBER, 'error' => 'Wert zu lang'];
        }

        return null;
    }

    private function validatePostalCode(Worksheet $worksheet, int $row, Place $place): ?array
    {
        $cell = $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::POSTAL_CODE, row: $row);
        $postalCodes = [];

        foreach ($place->getChildrenPlaces() as $childPlace) {
            if ($childPlace->getPlaceType() === PlaceType::POSTAL_CODE) {
                $postalCodes[] = $childPlace->getPlaceName();
            }
        }

        if ($cell->getValue() === null) {
            return ['row' => $row, 'col' => ExcelImportColIndex::POSTAL_CODE, 'error' => 'Die PLZ darf nicht leer sein'];
        }

        if (in_array((string)$cell->getValue(), $postalCodes, true) === false) {
            return ['row' => $row, 'col' => ExcelImportColIndex::POSTAL_CODE, 'error' => 'Die PLZ passt nicht zum Ort'];
        }

        return null;
    }

    private function validateQuarterName(Worksheet $worksheet, int $row, Place $place): ?array
    {
        $quarters = $this->locationService->fetchCityQuartersFromPlace(place: $place);

        $cell = $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::QUARTER_NAME, row: $row);

        if ($cell->getValue() === null) {
            return null;
        }

        if ($cell->getDataType() !== DataType::TYPE_STRING && $cell->getDataType() !== DataType::TYPE_STRING2) {
            return ['row' => $row, 'col' => ExcelImportColIndex::QUARTER_NAME, 'error' => 'ungültiger Datentyp'];
        }

        if (ExcelImportService::findQuarterFromQuartersByName(quarterName: $cell->getValue(), quarters: $quarters) === null) {
            return ['row' => $row, 'col' => ExcelImportColIndex::QUARTER_NAME, 'error' => 'Der Orts-/Stadtteil passt nicht zum Ort'];
        }

        return null;
    }

    private function validateInFloors(Worksheet $worksheet, int $row): ?array
    {
        $inFloors = $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::IN_FLOORS, row: $row)->getValue();

        return null;
    }

    private function validatePlaceDescription(Worksheet $worksheet, int $row): ?array
    {
        $placeDescription = $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::PLACE_DESCRIPTION, row: $row)->getValue();

        return null;
    }

    private function validateUsage(Worksheet $worksheet, int $row): ?array
    {
        $cell = $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::PROPERTY_USAGE, row: $row);

        if ($cell->getValue() === null) {
            return ['row' => $row, 'col' => ExcelImportColIndex::PROPERTY_USAGE, 'error' => 'Die Nutzung darf nicht leer sein'];
        }

        if ($cell->getDataType() !== DataType::TYPE_STRING && $cell->getDataType() !== DataType::TYPE_STRING2) {
            return ['row' => $row, 'col' => ExcelImportColIndex::PROPERTY_USAGE, 'error' => 'ungültiger Datentyp'];
        }

        if (strtolower(trim($cell->getValue())) === 'l') {
            return null;
        }

        if (IndustryClassificationMapping::fetchIndustryClassificationLevelOneByShortName(trim($cell->getValue())) === null) {
            return ['row' => $row, 'col' => ExcelImportColIndex::PROPERTY_USAGE, 'error' => 'ungültiger Wert'];
        }

        return null;
    }

    private function validateCompanyName(Worksheet $worksheet, int $row): ?array
    {
        $companyName = $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::COMPANY_NAME, row: $row)->getValue();

        if (strlen((string)$companyName) > 190) {
            return ['row' => $row, 'col' => ExcelImportColIndex::COMPANY_NAME, 'error' => 'Wert zu lang'];
        }

        return null;
    }

    private function validateBranch(Worksheet $worksheet, int $row): ?array
    {
        $cell = $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::BRANCH, row: $row);

        if ($cell->getValue() === null) {
            return null;
        }

        if ($cell->getDataType() !== DataType::TYPE_STRING && $cell->getDataType() !== DataType::TYPE_STRING2) {
            return ['row' => $row, 'col' => ExcelImportColIndex::BRANCH, 'error' => 'ungültiger Datentyp'];
        }

        if (in_array($cell->getValue(), ['keine Filiale', 'regional', 'international', 'national'], true) === false) {
            return ['row' => $row, 'col' => ExcelImportColIndex::BRANCH, 'error' => 'ungültiger Wert'];
        }

        return null;
    }

    private function validateFamilybusiness(Worksheet $worksheet, int $row): ?array
    {
        $cell = $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::FAMILYBUSINESS, row: $row);

        if ($cell->getValue() === null) {
            return null;
        }

        if ($cell->getDataType() !== DataType::TYPE_STRING && $cell->getDataType() !== DataType::TYPE_STRING2) {
            return ['row' => $row, 'col' => ExcelImportColIndex::FAMILYBUSINESS, 'error' => 'ungültiger Datentyp'];
        }

        if (in_array(strtolower(trim($cell->getValue())), ['ja', 'nein'], true) === false) {
            return ['row' => $row, 'col' => ExcelImportColIndex::FAMILYBUSINESS, 'error' => 'ungültiger Wert'];
        }

        return null;
    }

    private function validateIndustry(Worksheet $worksheet, int $row): ?array
    {
        $industryName = $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::INDUSTRY, row: $row)->getValue();

        if (strlen((string)$industryName) > 190) {
            return ['row' => $row, 'col' => ExcelImportColIndex::INDUSTRY, 'error' => 'Wert zu lang'];
        }

        return null;
    }

    private function validateBusinessForm(Worksheet $worksheet, int $row): ?array
    {
        $cell = $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::BUSINESS_FORM, row: $row);

        if ($cell->getValue() === null) {
            return null;
        }

        if ($cell->getDataType() !== DataType::TYPE_STRING && $cell->getDataType() !== DataType::TYPE_STRING2) {
            return ['row' => $row, 'col' => ExcelImportColIndex::BUSINESS_FORM, 'error' => 'ungültiger Datentyp'];
        }

        if (PropertyUsageBusinessTypeMapping::fetchPropertyUsageBusinessTypeByShortName(trim($cell->getValue())) === null) {
            return ['row' => $row, 'col' => ExcelImportColIndex::BUSINESS_FORM, 'error' => 'ungültiger Wert'];
        }

        return null;
    }

    private function validateNaceCode(Worksheet $worksheet, int $row): ?array
    {
        $cell = $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::NACE_CODE, row: $row);

        if ($cell->getValue() === null) {
            return null;
        }

        if ($cell->getDataType() === DataType::TYPE_NUMERIC) {
            if (preg_match('/^\d{5}$/', (string) $cell->getValue()) === 0) {
                return ['row' => $row, 'col' => ExcelImportColIndex::NACE_CODE, 'error' => 'ungültiger Wert'];
            }
        }

        if ($cell->getDataType() === DataType::TYPE_STRING && $cell->getDataType() === DataType::TYPE_STRING2) {
            if (
                preg_match('/^\d{5}$/', (string) $cell->getValue()) === 0
                && preg_match('/^\d{2}\.\d{2}\.\d{1}$/', $cell->getValue()) === 0
            ) {
                return ['row' => $row, 'col' => ExcelImportColIndex::NACE_CODE, 'error' => 'ungültiger Wert'];
            }
        }

        return null;
    }

    private function validatePastUsage(Worksheet $worksheet, int $row): ?array
    {
        $cell = $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::PAST_PROPERTY_USAGE, row: $row);

        if ($cell->getValue() === null) {
            return null;
        }

        if ($cell->getDataType() !== DataType::TYPE_STRING && $cell->getDataType() !== DataType::TYPE_STRING2) {
            return ['row' => $row, 'col' => ExcelImportColIndex::PAST_PROPERTY_USAGE, 'error' => 'ungültiger Datentyp'];
        }

        if (strtolower(trim($cell->getValue())) === 'l') {
            return null;
        }

        if (IndustryClassificationMapping::fetchIndustryClassificationLevelOneByShortName(trim($cell->getValue())) === null) {
            return ['row' => $row, 'col' => ExcelImportColIndex::PAST_PROPERTY_USAGE, 'error' => 'ungültiger Wert'];
        }

        return null;
    }

    private function validatePastPropertyUsers(Worksheet $worksheet, int $row): ?array
    {
        $pastPropertyUsers = $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::PAST_PROPERTY_USERS, row: $row)->getValue();

        return null;
    }

    private function validateShopWindowFrontWidth(Worksheet $worksheet, int $row): ?array
    {
        $cell = $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::SHOP_WINDOW_FRONT_WIDTH, row: $row);

        if ($cell->getValue() === null) {
            return null;
        }

        if ($cell->getDataType() !== DataType::TYPE_NUMERIC) {
            return ['row' => $row, 'col' => ExcelImportColIndex::SHOP_WINDOW_FRONT_WIDTH, 'error' => 'ungültiger Datentyp'];
        }

        if (!is_float($cell->getValue()) && !is_int($cell->getValue())) {
            return ['row' => $row, 'col' => ExcelImportColIndex::SHOP_WINDOW_FRONT_WIDTH, 'error' => 'ungültiger Wert'];
        }

        return null;
    }

    private function validateShowroomAvailable(Worksheet $worksheet, int $row): ?array
    {
        $cell = $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::SHOWROOM_AVAILABLE, row: $row);

        if ($cell->getValue() === null) {
            return null;
        }

        if ($cell->getDataType() !== DataType::TYPE_STRING && $cell->getDataType() !== DataType::TYPE_STRING2) {
            return ['row' => $row, 'col' => ExcelImportColIndex::SHOWROOM_AVAILABLE, 'error' => 'ungültiger Datentyp'];
        }

        if (in_array(strtolower(trim($cell->getValue())), ['ja', 'nein'], true) === false) {
            return ['row' => $row, 'col' => ExcelImportColIndex::SHOWROOM_AVAILABLE, 'error' => 'ungültiger Wert'];
        }

        return null;
    }

    private function validatePropertyArea(Worksheet $worksheet, int $row): ?array
    {
        $cell = $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::PROPERTY_AREA, row: $row);

        if ($cell->getValue() === null) {
            return null;
        }

        if ($cell->getDataType() !== DataType::TYPE_NUMERIC) {
            return ['row' => $row, 'col' => ExcelImportColIndex::PROPERTY_AREA, 'error' => 'ungültiger Datentyp'];
        }

        if (!is_float($cell->getValue()) && !is_int($cell->getValue())) {
            return ['row' => $row, 'col' => ExcelImportColIndex::PROPERTY_AREA, 'error' => 'ungültiger Wert'];
        }

        return null;
    }

    private function validateRetailSpace(Worksheet $worksheet, int $row): ?array
    {
        $cell = $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::RETAIL_SPACE, row: $row);

        if ($cell->getValue() === null) {
            return null;
        }

        if ($cell->getDataType() !== DataType::TYPE_NUMERIC) {
            return ['row' => $row, 'col' => ExcelImportColIndex::RETAIL_SPACE, 'error' => 'ungültiger Datentyp'];
        }

        if (!is_float($cell->getValue()) && !is_int($cell->getValue())) {
            return ['row' => $row, 'col' => ExcelImportColIndex::RETAIL_SPACE, 'error' => 'ungültiger Wert'];
        }

        return null;
    }

    private function validateSubsidiarySpace(Worksheet $worksheet, int $row): ?array
    {
        $cell = $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::SUBSIDIARY_SPACE, row: $row);

        if ($cell->getValue() === null) {
            return null;
        }

        if ($cell->getDataType() !== DataType::TYPE_NUMERIC) {
            return ['row' => $row, 'col' => ExcelImportColIndex::SUBSIDIARY_SPACE, 'error' => 'ungültiger Datentyp'];
        }

        if (!is_float($cell->getValue()) && !is_int($cell->getValue())) {
            return ['row' => $row, 'col' => ExcelImportColIndex::SUBSIDIARY_SPACE, 'error' => 'ungültiger Wert'];
        }

        return null;
    }

    private function validateGroundLevelSalesArea(Worksheet $worksheet, int $row): ?array
    {
        $cell = $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::GROUND_LEVEL_SALES_AREA, row: $row);

        if ($cell->getValue() === null) {
            return null;
        }

        if ($cell->getDataType() !== DataType::TYPE_STRING && $cell->getDataType() !== DataType::TYPE_STRING2) {
            return ['row' => $row, 'col' => ExcelImportColIndex::GROUND_LEVEL_SALES_AREA, 'error' => 'ungültiger Datentyp'];
        }

        if (in_array(strtolower(trim($cell->getValue())), ['ja', 'nein'], true) === false) {
            return ['row' => $row, 'col' => ExcelImportColIndex::GROUND_LEVEL_SALES_AREA, 'error' => 'ungültiger Wert'];
        }

        return null;
    }

    private function validateUsableOutdoorArea(Worksheet $worksheet, int $row): ?array
    {
        $cell = $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::USABLE_OUTDOOR_AREA, row: $row);

        if ($cell->getValue() === null) {
            return null;
        }

        if ($cell->getDataType() !== DataType::TYPE_NUMERIC) {
            return ['row' => $row, 'col' => ExcelImportColIndex::USABLE_OUTDOOR_AREA, 'error' => 'ungültiger Datentyp'];
        }

        if (!is_float($cell->getValue()) && !is_int($cell->getValue())) {
            return ['row' => $row, 'col' => ExcelImportColIndex::USABLE_OUTDOOR_AREA, 'error' => 'ungültiger Wert'];
        }

        return null;
    }

    private function validateBarrierFreeAccess(Worksheet $worksheet, int $row): ?array
    {
        $cell = $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::BARRIER_FREE_ACCESS, row: $row);

        if ($cell->getValue() === null) {
            return null;
        }

        if ($cell->getDataType() !== DataType::TYPE_STRING && $cell->getDataType() !== DataType::TYPE_STRING2) {
            return ['row' => $row, 'col' => ExcelImportColIndex::BARRIER_FREE_ACCESS, 'error' => 'ungültiger Datentyp'];
        }

        if (BarrierFreeAccessMapping::fetchBarrierFreeAccessByName(trim($cell->getValue())) === null) {
            return ['row' => $row, 'col' => ExcelImportColIndex::BARRIER_FREE_ACCESS, 'error' => 'ungültiger Wert'];
        }

        return null;
    }

    private function validateEntranceDoorWidth(Worksheet $worksheet, int $row): ?array
    {
        $cell = $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::ENTRANCE_DOOR_WIDTH, row: $row);

        if ($cell->getValue() === null) {
            return null;
        }

        if ($cell->getDataType() !== DataType::TYPE_NUMERIC) {
            return ['row' => $row, 'col' => ExcelImportColIndex::ENTRANCE_DOOR_WIDTH, 'error' => 'ungültiger Datentyp'];
        }

        if (!is_float($cell->getValue()) && !is_int($cell->getValue())) {
            return ['row' => $row, 'col' => ExcelImportColIndex::ENTRANCE_DOOR_WIDTH, 'error' => 'ungültiger Wert'];
        }

        return null;
    }

    private function validateBuildingType(Worksheet $worksheet, int $row): ?array
    {
        $cell = $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::BUILDING_TYPE, row: $row);

        if ($cell->getValue() === null) {
            return null;
        }

        if ($cell->getDataType() !== DataType::TYPE_STRING && $cell->getDataType() !== DataType::TYPE_STRING2) {
            return ['row' => $row, 'col' => ExcelImportColIndex::BUILDING_TYPE, 'error' => 'ungültiger Datentyp'];
        }

        if (BuildingTypeMapping::fetchBuildingTypeByShortName(trim($cell->getValue())) === null) {
            return ['row' => $row, 'col' => ExcelImportColIndex::BUILDING_TYPE, 'error' => 'ungültiger Wert'];
        }

        return null;
    }

    private function validateConstructionYear(Worksheet $worksheet, int $row): ?array
    {
        $cell = $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::CONSTRUCTION_YEAR, row: $row);

        if ($cell->getValue() === null) {
            return null;
        }

        if ($cell->getDataType() !== DataType::TYPE_NUMERIC) {
            return ['row' => $row, 'col' => ExcelImportColIndex::CONSTRUCTION_YEAR, 'error' => 'ungültiger Datentyp'];
        }

        if (preg_match('/^[1-9][0-9][0-9][0-9]$/', (string) $cell->getValue()) === 0) {
            return ['row' => $row, 'col' => ExcelImportColIndex::CONSTRUCTION_YEAR, 'error' => 'ungültiger Wert'];
        }

        return null;
    }

    private function validateNumberOfFloors(Worksheet $worksheet, int $row): ?array
    {
        $cell = $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::NUMBER_OF_FLOORS, row: $row);

        if ($cell->getValue() === null) {
            return null;
        }

        if ($cell->getDataType() !== DataType::TYPE_NUMERIC) {
            return ['row' => $row, 'col' => ExcelImportColIndex::NUMBER_OF_FLOORS, 'error' => 'ungültiger Datentyp'];
        }

        if ($this->isValidInteger((string) $cell->getValue()) === false) {
            return ['row' => $row, 'col' => ExcelImportColIndex::NUMBER_OF_FLOORS, 'error' => 'ungültiger Wert'];
        }

        return null;
    }

    private function validateRoofShape(Worksheet $worksheet, int $row): ?array
    {
        $cell = $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::ROOF_SHAPE, row: $row);

        if ($cell->getValue() === null) {
            return null;
        }

        if ($cell->getDataType() !== DataType::TYPE_STRING && $cell->getDataType() !== DataType::TYPE_STRING2) {
            return ['row' => $row, 'col' => ExcelImportColIndex::ROOF_SHAPE, 'error' => 'ungültiger Datentyp'];
        }

        if (RoofShapeMapping::fetchRoofShapeByName(trim($cell->getValue())) === null) {
            return ['row' => $row, 'col' => ExcelImportColIndex::ROOF_SHAPE, 'error' => 'ungültiger Wert'];
        }

        return null;
    }

    private function validateBuildingCondition(Worksheet $worksheet, int $row): ?array
    {
        $cell = $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::BUILDING_CONDITION, row: $row);

        if ($cell->getValue() === null) {
            return null;
        }

        if ($cell->getDataType() !== DataType::TYPE_STRING && $cell->getDataType() !== DataType::TYPE_STRING2) {
            return ['row' => $row, 'col' => ExcelImportColIndex::BUILDING_CONDITION, 'error' => 'ungültiger Datentyp'];
        }

        if (BuildingConditionMapping::fetchBuildingConditionByName(trim($cell->getValue())) === null) {
            return ['row' => $row, 'col' => ExcelImportColIndex::BUILDING_CONDITION, 'error' => 'ungültiger Wert'];
        }

        return null;
    }

    private function validateNumberOfParkingLots(Worksheet $worksheet, int $row): ?array
    {
        $cell = $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::NUMBER_OF_PARKING_LOTS, row: $row);

        if ($cell->getValue() === null) {
            return null;
        }

        if ($cell->getDataType() !== DataType::TYPE_NUMERIC) {
            return ['row' => $row, 'col' => ExcelImportColIndex::NUMBER_OF_PARKING_LOTS, 'error' => 'ungültiger Datentyp'];
        }

        if ($this->isValidInteger((string) $cell->getValue()) === false) {
            return ['row' => $row, 'col' => ExcelImportColIndex::NUMBER_OF_PARKING_LOTS, 'error' => 'ungültiger Wert'];
        }

        return null;
    }

    private function validateRemark(Worksheet $worksheet, int $row): ?array
    {
        $remark = $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::REMARK, row: $row)->getValue();

        return null;
    }

    private function validateInternalNumber(Worksheet $worksheet, int $row): ?array
    {
        $internalNumber = $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::INTERNAL_NUMBER, row: $row)->getValue();

        if (strlen((string)$internalNumber) > 60) {
            return ['row' => $row, 'col' => ExcelImportColIndex::INTERNAL_NUMBER, 'error' => 'Wert zu lang'];
        }

        return null;
    }

    private function validateGisBuildingId(Worksheet $worksheet, int $row): ?array
    {
        $gisBuildingId = $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::GIS_BUILDING_ID, row: $row)->getValue();

        if (strlen((string)$gisBuildingId) > 190) {
            return ['row' => $row, 'col' => ExcelImportColIndex::GIS_BUILDING_ID, 'error' => 'Wert zu lang'];
        }

        return null;
    }

    private function validateLongitudeAndLatitude(Worksheet $worksheet, int $row): ?array
    {
        $longitudeCell = $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::LONGITUDE, row: $row);
        $latitudeCell = $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::LATITUDE, row: $row);

        if ($longitudeCell->getValue() === null && $latitudeCell->getValue() === null) {
            return null;
        }

        return null;
    }

    private function validatePicturesAvailable(Worksheet $worksheet, int $row): ?array
    {
        $picturesAvailable = $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::PICTURES_AVAILABLE, row: $row)->getValue();

        return null;
    }

    private function validateDatasetCollectDate(Worksheet $worksheet, int $row): ?array
    {
        $cell = $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::DATASET_COLLECT_DATE, row: $row);

        if ($cell->getValue() === null) {
            return ['row' => $row, 'col' => ExcelImportColIndex::DATASET_COLLECT_DATE, 'error' => 'Das Erhebungsdatum darf nicht leer sein'];
        }

        if ($cell->getDataType() !== DataType::TYPE_NUMERIC) {
            return ['row' => $row, 'col' => ExcelImportColIndex::DATASET_COLLECT_DATE, 'error' => 'ungültiger Datentyp'];
        }

        if ($this->isValidDate($cell->getFormattedValue()) === false) {
            return ['row' => $row, 'col' => ExcelImportColIndex::DATASET_COLLECT_DATE, 'error' => 'ungültiger Wert'];
        }

        return null;
    }

    private function validateDatasetCapturedBy(Worksheet $worksheet, int $row): ?array
    {
        $datasetCapturedBy = $worksheet->getCellByColumnAndRow(columnIndex: ExcelImportColIndex::DATASET_CAPTURED_BY, row: $row)->getValue();

        return null;
    }

    private function isValidInteger(string $value): bool
    {
        if (preg_match('/^(0|[1-9][0-9]{0,})$/', $value) === 1) {
            return true;
        }

        return false;
    }

    private function isValidDate(string $value): bool
    {
        if (preg_match('/^([1-9]|[1-9][0-2])\/([1-9]|[0-3][0-9])\/[1-9][0-9][0-9][0-9]$/', $value) === 0) {
            return false;
        }

        $dateParts = explode('/', $value);

        return checkdate(intval($dateParts[0]), intval($dateParts[1]), intval($dateParts[2]));
    }
}
