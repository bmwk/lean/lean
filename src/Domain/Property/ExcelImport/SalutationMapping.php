<?php

declare(strict_types=1);

namespace App\Domain\Property\ExcelImport;

use App\Domain\Entity\Person\Salutation;

class SalutationMapping
{
    public static function fetchSalutationByName(string $name): ?Salutation
    {
        return match (strtolower($name)) {
            strtolower(Salutation::HERR->getName()) => Salutation::HERR,
            strtolower(Salutation::FRAU->getName()) => Salutation::FRAU,
            strtolower(Salutation::DIVERS->getName()) => Salutation::DIVERS,
            default => null
        };
    }
}
