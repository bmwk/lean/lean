<?php

declare(strict_types=1);

namespace App\Domain\Property;

use App\Domain\Entity\Account;
use App\Domain\Entity\Person\Person;
use App\Domain\Entity\Property\BuildingUnit;
use App\Domain\Entity\Property\PropertyContactPerson;
use App\Repository\Property\PropertyContactPersonRepository;

class PropertyContactPersonService
{
    public function __construct(
        private readonly PropertyContactPersonRepository $propertyContactPersonRepository
    ) {
    }

    public function fetchPropertyContactPersonByIdAndBuildingUnit(
        Account $account,
        bool $withFromSubAccounts,
        int $id,
        BuildingUnit $buildingUnit
    ): ?PropertyContactPerson {
        return $this->propertyContactPersonRepository->findOneByIdAndBuildingUnit(
            account: $account,
            withFromSubAccounts: $withFromSubAccounts,
            id: $id,
            buildingUnit: $buildingUnit
        );
    }

    public function fetchPropertyContactPersonByBuildingUnitAndPerson(
        Account $account,
        bool $withFromSubAccounts,
        BuildingUnit $buildingUnit,
        Person $person
    ): ?PropertyContactPerson {
        return $this->propertyContactPersonRepository->findOneByBuildingUnitAndPerson(
            account: $account,
            withFromSubAccounts: $withFromSubAccounts,
            buildingUnit: $buildingUnit,
            person: $person
        );
    }
}
