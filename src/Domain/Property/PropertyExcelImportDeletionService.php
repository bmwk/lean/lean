<?php

declare(strict_types=1);

namespace App\Domain\Property;

use App\Domain\Entity\Account;
use App\Domain\Entity\PropertyExcelImport;
use App\Domain\File\FileDeletionService;
use App\Repository\PropertyExcelImportRepository;
use Doctrine\ORM\EntityManagerInterface;

class PropertyExcelImportDeletionService
{
    public function __construct(
        private readonly FileDeletionService $fileDeletionService,
        private readonly PropertyExcelImportRepository $propertyExcelImportRepository,
        private readonly EntityManagerInterface $entityManager,
    ) {
    }

    public function deletePropertyExcelImport(PropertyExcelImport $propertyExcelImport): void
    {
        $this->hardDeletePropertyExcelImport(propertyExcelImport: $propertyExcelImport);
    }

    public function deletePropertyExcelImportsByAccount(Account $account): void
    {
        $this->hardDeletePropertyExcelImportsByAccount(account: $account);
    }

    private function hardDeletePropertyExcelImport(PropertyExcelImport $propertyExcelImport): void
    {
        $this->entityManager->remove($propertyExcelImport);
        $this->entityManager->flush();

        $this->fileDeletionService->deleteFile(file: $propertyExcelImport->getFile());
    }

    private function hardDeletePropertyExcelImportsByAccount(Account $account): void
    {
        $propertyExcelImports = $this->propertyExcelImportRepository->findBy(criteria: ['account' => $account]);

        foreach ($propertyExcelImports as $propertyExcelImport) {
            $this->hardDeletePropertyExcelImport(propertyExcelImport: $propertyExcelImport);
        }
    }
}
