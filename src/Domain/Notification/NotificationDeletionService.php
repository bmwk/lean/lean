<?php

declare(strict_types=1);

namespace App\Domain\Notification;

use App\Domain\Entity\LookingForPropertyRequest\LookingForPropertyRequest;
use App\Domain\Entity\Notification\Notification;
use App\Domain\Entity\Person\Occurrence;
use App\Domain\Entity\Property\BuildingUnit;
use App\Repository\NotificationRepository;
use Doctrine\ORM\EntityManagerInterface;

class NotificationDeletionService
{
    public function __construct(
        private readonly NotificationRepository $notificationRepository,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function deleteNotification(Notification $notification): void
    {
        $this->hardDeleteNotification(notification: $notification);
    }

    public function deleteNotificationsByBuildingUnit(BuildingUnit $buildingUnit): void
    {
        $this->hardDeleteNotificationsByBuildingUnit(buildingUnit: $buildingUnit);
    }

    public function deleteNotificationsByOccurrence(Occurrence $occurrence): void
    {
        $this->hardDeleteNotificationsByOccurrence(occurrence: $occurrence);
    }

    public function deleteNotificationsByLookingForPropertyRequest(LookingForPropertyRequest $lookingForPropertyRequest): void
    {
        $this->hardDeleteNotificationsByLookingForPropertyRequest(lookingForPropertyRequest: $lookingForPropertyRequest);
    }

    private function hardDeleteNotification(Notification $notification): void
    {
        $this->entityManager->remove(entity: $notification);

        $this->entityManager->flush();
    }

    private function hardDeleteNotificationsByBuildingUnit(BuildingUnit $buildingUnit): void
    {
        $notifications = $this->notificationRepository->findBy(criteria: ['buildingUnit' => $buildingUnit]);

        foreach ($notifications as $notification) {
            $this->hardDeleteNotification(notification: $notification);
        }
    }

    private function hardDeleteNotificationsByOccurrence(Occurrence $occurrence): void
    {
        $notifications = $this->notificationRepository->findBy(criteria: ['occurrence' => $occurrence]);

        foreach ($notifications as $notification) {
            $this->hardDeleteNotification(notification: $notification);
        }
    }

    private function hardDeleteNotificationsByLookingForPropertyRequest(LookingForPropertyRequest $lookingForPropertyRequest): void
    {
        $notifications = $this->notificationRepository->findBy(criteria: ['lookingForPropertyRequest' => $lookingForPropertyRequest]);

        foreach ($notifications as $notification) {
            $this->hardDeleteNotification(notification: $notification);
        }
    }
}
