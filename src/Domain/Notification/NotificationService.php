<?php

declare(strict_types=1);

namespace App\Domain\Notification;

use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\LookingForPropertyRequest\LookingForPropertyRequest;
use App\Domain\Entity\Notification\Notification;
use App\Domain\Entity\Notification\NotificationFilter;
use App\Domain\Entity\Notification\NotificationType;
use App\Domain\Entity\Person\Occurrence;
use App\Domain\Entity\Property\BuildingUnit;
use App\Domain\Entity\SortingOption\SortingOption;
use App\Repository\NotificationRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;

class NotificationService
{
    public function __construct(
        private readonly NotificationRepository $notificationRepository,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function createAndSaveNotification(
        AccountUser $accountUser,
        NotificationType $notificationType,
        bool $beenRead,
        ?BuildingUnit $buildingUnit = null,
        ?Occurrence $occurrence = null,
        ?LookingForPropertyRequest $lookingForPropertyRequest = null
    ): Notification {
        $notification = $this->createNotification(
            accountUser: $accountUser,
            notificationType: $notificationType,
            beenRead: $beenRead,
            buildingUnit: $buildingUnit,
            occurrence: $occurrence,
            lookingForPropertyRequest: $lookingForPropertyRequest
        );

        $this->entityManager->persist(entity: $notification);
        $this->entityManager->flush();

        return $notification;
    }

    public function createNotification(
        AccountUser $accountUser,
        NotificationType $notificationType,
        bool $beenRead,
        ?BuildingUnit $buildingUnit = null,
        ?Occurrence $occurrence = null,
        ?LookingForPropertyRequest $lookingForPropertyRequest = null
    ): Notification {
        $notification = new Notification();

        $notification
            ->setAccountUser($accountUser)
            ->setBeenRead($beenRead)
            ->setNotificationType($notificationType);

        if ($buildingUnit !== null) {
            $notification->setBuildingUnit($buildingUnit);
        }

        if ($occurrence !== null) {
            $notification->setOccurrence($occurrence);
        }

        if ($lookingForPropertyRequest !== null) {
            $notification->setLookingForPropertyRequest($lookingForPropertyRequest);
        }

        return $notification;
    }

    public function fetchNotificationByNotificationTypeAndObject(
        AccountUser $accountUser,
        NotificationType $notificationType,
        BuildingUnit|Occurrence|LookingForPropertyRequest $object
    ): ?Notification {
        return $this->notificationRepository->findOneByNotificationTypeAndObject(
            accountUser: $accountUser,
            notificationType: $notificationType,
            object: $object
        );
    }

    public function fetchNotificationsPaginatedByAccountUser(
        AccountUser $accountUser,
        int $firstResult,
        int $maxResults,
        ?NotificationFilter $notificationFilter = null,
        ?SortingOption $sortingOption = null
    ): Paginator {
        return $this->notificationRepository->findPaginatedByAccountUser(
            accountUser: $accountUser,
            firstResult: $firstResult,
            maxResults: $maxResults,
            notificationFilter: $notificationFilter,
            sortingOption: $sortingOption
        );
    }

    /**
     * @return Notification[]
     */
    public function fetchNotificationsByAccountUser(AccountUser $accountUser, ?NotificationFilter $notificationFilter = null, ?int $limit = null): array
    {
        return $this->notificationRepository->findByAccountUser(accountUser: $accountUser, notificationFilter: $notificationFilter, limit: $limit);
    }

    /**
     * @param int[] $ids
     * @return Notification[]
     */
    public function fetchNotificationsByIds(AccountUser $accountUser, array $ids, ?NotificationFilter $notificationFilter = null): array
    {
        return $this->notificationRepository->findByIds(accountUser: $accountUser, ids: $ids, notificationFilter: $notificationFilter);
    }

    public function fetchNotificationById(AccountUser $accountUser, int $id, ?NotificationFilter $notificationFilter = null): ?Notification
    {
        return $this->notificationRepository->findOneById(accountUser: $accountUser, id: $id, notificationFilter: $notificationFilter);
    }

    public function countNotificationsByAccountUser(AccountUser $accountUser, ?NotificationFilter $notificationFilter = null): int
    {
        return $this->notificationRepository->countByAccountUser(accountUser: $accountUser, notificationFilter: $notificationFilter);
    }
}
