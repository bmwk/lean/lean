<?php

declare(strict_types=1);

namespace App\Domain\LookingForPropertyRequestReporter\Api\Entity;

use App\Domain\Entity\LocationCategory;
use App\Domain\Entity\LocationFactor;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

class LocationRequirement
{
    /**
     * @var Uuid[]
     */
    #[Assert\NotBlank]
    private array $placeUuids;

    /**
     * @var Uuid[]|null
     */
    private ?array $quarterPlaceUuids = null;

    /**
     * @var LocationCategory[]|null
     */
    private ?array $locationCategories = null;

    /**
     * @var LocationFactor[]|null
     */
    private ?array $locationFactors = null;

    private ?string $otherRequirementsForTheLocation = null;

    public static function createFormApiRequestJsonContent(object $jsonContent): self
    {
        $locationRequirement = new self();

        $locationRequirement->placeUuids = array_map(
            callback: function (string $uuid): Uuid {
                return Uuid::fromString($uuid);
            },
            array: $jsonContent->placeUuids
        );

        if ($jsonContent->quarterPlaceUuids !== null) {
            $locationRequirement->quarterPlaceUuids = array_map(
                callback: function (string $uuid): Uuid {
                    return Uuid::fromString($uuid);
                },
                array: $jsonContent->quarterPlaceUuids
            );
        }

        if ($jsonContent->locationCategories !== null) {
            $locationRequirement->locationCategories = array_map(
                callback: function (int $locationCategoryValue): LocationCategory {
                    return LocationCategory::from($locationCategoryValue);
                },
                array: $jsonContent->locationCategories
            );
        }

        if ($jsonContent->locationFactors !== null) {
            $locationRequirement->locationFactors = array_map(
                callback: function (int $locationFactorValue): LocationFactor {
                    return LocationFactor::from($locationFactorValue);
                },
                array: $jsonContent->locationFactors
            );
        }

        $locationRequirement->otherRequirementsForTheLocation = isset($jsonContent->otherRequirementsForTheLocation)  && !empty($jsonContent->otherRequirementsForTheLocation) ? $jsonContent->otherRequirementsForTheLocation : null;

        return $locationRequirement;
    }

    /**
     * @return Uuid[]
     */
    public function getPlaceUuids(): array
    {
        return $this->placeUuids;
    }

    /**
     * @param Uuid[] $placeUuids
     */
    public function setPlaceUuids(array $placeUuids): self
    {
        $this->placeUuids = $placeUuids;

        return $this;
    }

    /**
     * @return Uuid[]|null
     */
    public function getQuarterPlaceUuids(): ?array
    {
        return $this->quarterPlaceUuids;
    }

    /**
     * @param Uuid[]|null $quarterPlaceUuids
     */
    public function setQuarterPlaceUuids(?array $quarterPlaceUuids): self
    {
        $this->quarterPlaceUuids = $quarterPlaceUuids;

        return $this;
    }

    /**
     * @return LocationCategory[]|null
     */
    public function getLocationCategories(): ?array
    {
        return $this->locationCategories;
    }

    /**
     * @param LocationCategory[]|null $locationCategories
     */
    public function setLocationCategories(?array $locationCategories): self
    {
        $this->locationCategories = $locationCategories;

        return $this;
    }

    /**
     * @return LocationFactor[]|null
     */
    public function getLocationFactors(): ?array
    {
        return $this->locationFactors;
    }

    /**
     * @param LocationFactor[]|null $locationFactors
     */
    public function setLocationFactors(?array $locationFactors): self
    {
        $this->locationFactors = $locationFactors;

        return $this;
    }

    public function getOtherRequirementsForTheLocation(): ?string
    {
        return $this->otherRequirementsForTheLocation;
    }

    public function setOtherRequirementsForTheLocation(?string $otherRequirementsForTheLocation): self
    {
        $this->otherRequirementsForTheLocation = $otherRequirementsForTheLocation;

        return $this;
    }
}
