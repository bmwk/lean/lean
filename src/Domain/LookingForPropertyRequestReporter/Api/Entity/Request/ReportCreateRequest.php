<?php

declare(strict_types=1);

namespace App\Domain\LookingForPropertyRequestReporter\Api\Entity\Request;

use App\Domain\LookingForPropertyRequestReporter\Api\Entity\BasicInformation;
use App\Domain\LookingForPropertyRequestReporter\Api\Entity\LocationRequirement;
use App\Domain\LookingForPropertyRequestReporter\Api\Entity\PropertyRequirement;
use App\Domain\LookingForPropertyRequestReporter\Api\Entity\Reporter;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

class ReportCreateRequest
{
    #[Assert\NotBlank]
    private Uuid $accountKey;

    #[Assert\Valid]
    private BasicInformation $basicInformation;

    #[Assert\Valid]
    private LocationRequirement $locationRequirement;

    #[Assert\Valid]
    private PropertyRequirement $propertyRequirement;

    #[Assert\Valid]
    private Reporter $reporter;

    public static function createFormApiRequestJsonContent(object $jsonContent): self
    {
        $reportCreateRequest = new self();

        $reportCreateRequest->accountKey = Uuid::fromString($jsonContent->accountKey);
        $reportCreateRequest->basicInformation = BasicInformation::createFormApiRequestJsonContent(jsonContent: $jsonContent->basicInformation);
        $reportCreateRequest->locationRequirement = LocationRequirement::createFormApiRequestJsonContent(jsonContent: $jsonContent->locationRequirement);
        $reportCreateRequest->propertyRequirement = PropertyRequirement::createFormApiRequestJsonContent(jsonContent: $jsonContent->propertyRequirement);
        $reportCreateRequest->reporter = Reporter::createFormApiRequestJsonContent(jsonContent: $jsonContent->reporter);

        return $reportCreateRequest;
    }

    public function getAccountKey(): Uuid
    {
        return $this->accountKey;
    }

    public function setAccountKey(Uuid $accountKey): self
    {
        $this->accountKey = $accountKey;

        return $this;
    }

    public function getBasicInformation(): BasicInformation
    {
        return $this->basicInformation;
    }

    public function setBasicInformation(BasicInformation $basicInformation): self
    {
        $this->basicInformation = $basicInformation;

        return $this;
    }

    public function getLocationRequirement(): LocationRequirement
    {
        return $this->locationRequirement;
    }

    public function setLocationRequirement(LocationRequirement $locationRequirement): self
    {
        $this->locationRequirement = $locationRequirement;

        return $this;
    }

    public function getPropertyRequirement(): PropertyRequirement
    {
        return $this->propertyRequirement;
    }

    public function setPropertyRequirement(PropertyRequirement $propertyRequirement): self
    {
        $this->propertyRequirement = $propertyRequirement;

        return $this;
    }

    public function getReporter(): Reporter
    {
        return $this->reporter;
    }

    public function setReporter(Reporter $reporter): self
    {
        $this->reporter = $reporter;

        return $this;
    }
}
