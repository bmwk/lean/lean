<?php

declare(strict_types=1);

namespace App\Domain\LookingForPropertyRequestReporter\Api\Entity;

use App\Domain\Entity\Person\Salutation;
use Symfony\Component\Validator\Constraints as Assert;

class Reporter
{
    private ?string $companyName = null;

    private ?Salutation $salutation = null;

    private ?string $firstName = null;

    #[Assert\NotBlank]
    private string $name;

    private ?string $phoneNumber = null;

    #[Assert\NotBlank]
    #[Assert\Email]
    private string $email;

    public static function createFormApiRequestJsonContent(object $jsonContent): self
    {
        $reporter = new self();

        $reporter->companyName = isset($jsonContent->companyName)  && !empty($jsonContent->companyName) ?  $jsonContent->companyName : null;
        $reporter->salutation = $jsonContent->salutation !== null ? Salutation::from($jsonContent->salutation) : null;
        $reporter->firstName = isset($jsonContent->firstName)  && !empty($jsonContent->firstName) ? $jsonContent->firstName : null;
        $reporter->name = $jsonContent->name;
        $reporter->phoneNumber = isset($jsonContent->phoneNumber)  && !empty($jsonContent->phoneNumber) ? $jsonContent->phoneNumber : null;
        $reporter->email = $jsonContent->email;

        return $reporter;
    }

    public function getCompanyName(): ?string
    {
        return $this->companyName;
    }

    public function setCompanyName(?string $companyName): self
    {
        $this->companyName = $companyName;

        return $this;
    }

    public function getSalutation(): ?Salutation
    {
        return $this->salutation;
    }

    public function setSalutation(?Salutation $salutation): self
    {
        $this->salutation = $salutation;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }


    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(?string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }
}
