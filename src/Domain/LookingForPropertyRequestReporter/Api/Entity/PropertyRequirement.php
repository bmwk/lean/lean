<?php

declare(strict_types=1);

namespace App\Domain\LookingForPropertyRequestReporter\Api\Entity;

use App\Domain\Entity\PropertyOfferType;
use App\Domain\Entity\ValueRequirement;
use Symfony\Component\Validator\Constraints as Assert;

class PropertyRequirement
{
    /**
     * @var PropertyOfferType[]
     */
    #[Assert\NotBlank]
    private array $propertyOfferTypes;

    #[Assert\NotNull]
    private bool $onlyNonCommissionable;

    private ValueRequirement $totalSpace;

    private ValueRequirement $retailSpace;

    private ValueRequirement $outdoorSpace;

    private ValueRequirement $subsidarySpace;

    private ValueRequirement $usableSpace;

    private ValueRequirement $storageSpace;

    private ValueRequirement $shopWindowLength;

    private ValueRequirement $shopWidth;

    private ValueRequirement $purchasePriceNet;

    private ValueRequirement $purchasePriceGross;

    private ValueRequirement $purchasePricePerSquareMeter;

    private ValueRequirement $netColdRent;

    private ValueRequirement $coldRent;

    private ValueRequirement $rentalPricePerSquareMeter;

    private ValueRequirement $lease;

    private bool $roofingRequired;

    private bool $outdoorSpaceRequired;

    private bool $showroomRequired;

    private bool $shopWindowRequired;

    private bool $groundLevelSalesAreaRequired;

    private bool $barrierFreeAccessRequired;

    private ?int $numberOfParkingLots = null;

    private ?int $roomCount = null;

    private ?\DateTime $earliestAvailableFrom = null;

    private ?\DateTime $latestAvailableFrom = null;

    public static function createFormApiRequestJsonContent(object $jsonContent): self
    {
        $propertyRequirement = new self();

        $propertyRequirement->propertyOfferTypes = array_map(
            callback: function (int $propertyOfferTypeValue): PropertyOfferType {
                return PropertyOfferType::from($propertyOfferTypeValue);
            },
            array: $jsonContent->propertyOfferTypes
        );

        $propertyRequirement->onlyNonCommissionable = $jsonContent->onlyNonCommissionable;

        $propertyRequirement->totalSpace = ValueRequirement::create(name: 'totalSpace', minimumValue: (float)$jsonContent->totalSpaceMin, maximumValue: (float)$jsonContent->totalSpaceMax);
        $propertyRequirement->retailSpace = ValueRequirement::create(name: 'retailSpace', minimumValue: (float)$jsonContent->retailSpaceMin, maximumValue: (float)$jsonContent->retailSpaceMax);
        $propertyRequirement->outdoorSpace = ValueRequirement::create(name: 'outdoorSpace', minimumValue: (float)$jsonContent->outdoorSpaceMin, maximumValue: (float)$jsonContent->outdoorSpaceMax);
        $propertyRequirement->subsidarySpace = ValueRequirement::create(name: 'subsidarySpace', minimumValue: (float)$jsonContent->subsidarySpaceMin, maximumValue: (float)$jsonContent->subsidarySpaceMax);
        $propertyRequirement->usableSpace = ValueRequirement::create(name: 'usableSpace', minimumValue: (float)$jsonContent->usableSpaceMin, maximumValue: (float)$jsonContent->usableSpaceMax);
        $propertyRequirement->storageSpace = ValueRequirement::create(name: 'storageSpace', minimumValue: (float)$jsonContent->storageSpaceMin, maximumValue: (float)$jsonContent->storageSpaceMax);
        $propertyRequirement->shopWindowLength = ValueRequirement::create(name: 'shopWindowLength', minimumValue: (float)$jsonContent->shopWindowLengthMin, maximumValue: (float)$jsonContent->shopWindowLengthMax);
        $propertyRequirement->shopWidth = ValueRequirement::create(name: 'shopWidth', minimumValue: (float)$jsonContent->shopWidthMin, maximumValue: (float)$jsonContent->shopWidthMax);
        $propertyRequirement->purchasePriceNet = ValueRequirement::create(name: 'purchasePriceNet', minimumValue: (float)$jsonContent->purchasePriceNetMin, maximumValue: (float)$jsonContent->purchasePriceNetMax);
        $propertyRequirement->purchasePriceGross = ValueRequirement::create(name: 'purchasePriceGross', minimumValue: (float)$jsonContent->purchasePriceGrossMin, maximumValue: (float)$jsonContent->purchasePriceGrossMax);
        $propertyRequirement->purchasePricePerSquareMeter = ValueRequirement::create(name: 'purchasePricePerSquareMeter', minimumValue: (float)$jsonContent->purchasePricePerSquareMeterMin, maximumValue: (float)$jsonContent->purchasePricePerSquareMeterMax);
        $propertyRequirement->netColdRent = ValueRequirement::create(name: 'netColdRent', minimumValue: (float)$jsonContent->netColdRentMin, maximumValue: (float)$jsonContent->netColdRentMax);
        $propertyRequirement->coldRent = ValueRequirement::create(name: 'coldRent', minimumValue: (float)$jsonContent->coldRentMin, maximumValue: (float)$jsonContent->coldRentMax);
        $propertyRequirement->rentalPricePerSquareMeter = ValueRequirement::create(name: 'rentalPricePerSquareMeter', minimumValue: (float)$jsonContent->rentalPricePerSquareMeterMin, maximumValue: (float)$jsonContent->rentalPricePerSquareMeterMax);
        $propertyRequirement->lease = ValueRequirement::create(name: 'lease', minimumValue: (float)$jsonContent->leaseMin, maximumValue: (float)$jsonContent->leaseMax);

        $propertyRequirement->barrierFreeAccessRequired = $jsonContent->barrierFreeAccessRequired;
        $propertyRequirement->roofingRequired = $jsonContent->roofingRequired;
        $propertyRequirement->outdoorSpaceRequired = $jsonContent->outdoorSpaceRequired;
        $propertyRequirement->showroomRequired = $jsonContent->showroomRequired;
        $propertyRequirement->shopWindowRequired = $jsonContent->shopWindowRequired;
        $propertyRequirement->groundLevelSalesAreaRequired = $jsonContent->groundLevelSalesAreaRequired;

        $propertyRequirement->numberOfParkingLots = isset($jsonContent->numberOfParkingLots) && !empty($jsonContent->numberOfParkingLots) ? (int) $jsonContent->numberOfParkingLots : null;
        $propertyRequirement->roomCount = isset($jsonContent->roomCount) && !empty($jsonContent->roomCount) ? (int) $jsonContent->roomCount : null;
        $propertyRequirement->earliestAvailableFrom = isset($jsonContent->earliestAvailableFrom) && !empty($jsonContent->earliestAvailableFrom) ? new \DateTime($jsonContent->earliestAvailableFrom) : null;
        $propertyRequirement->latestAvailableFrom = isset($jsonContent->latestAvailableFrom) && !empty($jsonContent->latestAvailableFrom) ? new \DateTime($jsonContent->latestAvailableFrom) : null;

        return $propertyRequirement;
    }

    /**
     * @return PropertyOfferType[]
     */
    public function getPropertyOfferTypes(): array
    {
        return $this->propertyOfferTypes;
    }

    /**
     * @param PropertyOfferType[] $propertyOfferTypes
     */
    public function setPropertyOfferTypes(array $propertyOfferTypes): self
    {
        $this->propertyOfferTypes = $propertyOfferTypes;

        return $this;
    }

    public function isOnlyNonCommissionable(): bool
    {
        return $this->onlyNonCommissionable;
    }

    public function setOnlyNonCommissionable(bool $onlyNonCommissionable): self
    {
        $this->onlyNonCommissionable = $onlyNonCommissionable;

        return $this;
    }

    public function getTotalSpace(): ValueRequirement
    {
        return $this->totalSpace;
    }

    public function setTotalSpace(ValueRequirement $totalSpace): self
    {
        $this->totalSpace = $totalSpace;
        return $this;
    }

    public function getRetailSpace(): ValueRequirement
    {
        return $this->retailSpace;
    }

    public function setRetailSpace(ValueRequirement $retailSpace): self
    {
        $this->retailSpace = $retailSpace;
        return $this;
    }

    public function getOutdoorSpace(): ValueRequirement
    {
        return $this->outdoorSpace;
    }

    public function setOutdoorSpace(ValueRequirement $outdoorSpace): self
    {
        $this->outdoorSpace = $outdoorSpace;
        return $this;
    }

    public function getSubsidarySpace(): ValueRequirement
    {
        return $this->subsidarySpace;
    }

    public function setSubsidarySpace(ValueRequirement $subsidarySpace): self
    {
        $this->subsidarySpace = $subsidarySpace;
        return $this;
    }

    public function getUsableSpace(): ValueRequirement
    {
        return $this->usableSpace;
    }

    public function setUsableSpace(ValueRequirement $usableSpace): self
    {
        $this->usableSpace = $usableSpace;
        return $this;
    }

    public function getStorageSpace(): ValueRequirement
    {
        return $this->storageSpace;
    }

    public function setStorageSpace(ValueRequirement $storageSpace): self
    {
        $this->storageSpace = $storageSpace;
        return $this;
    }

    public function getShopWindowLength(): ValueRequirement
    {
        return $this->shopWindowLength;
    }

    public function setShopWindowLength(ValueRequirement $shopWindowLength): self
    {
        $this->shopWindowLength = $shopWindowLength;
        return $this;
    }

    public function getShopWidth(): ValueRequirement
    {
        return $this->shopWidth;
    }

    public function setShopWidth(ValueRequirement $shopWidth): self
    {
        $this->shopWidth = $shopWidth;
        return $this;
    }

    public function getPurchasePriceNet(): ValueRequirement
    {
        return $this->purchasePriceNet;
    }

    public function setPurchasePriceNet(ValueRequirement $purchasePriceNet): self
    {
        $this->purchasePriceNet = $purchasePriceNet;
        return $this;
    }

    public function getPurchasePriceGross(): ValueRequirement
    {
        return $this->purchasePriceGross;
    }

    public function setPurchasePriceGross(ValueRequirement $purchasePriceGross): self
    {
        $this->purchasePriceGross = $purchasePriceGross;
        return $this;
    }

    public function getPurchasePricePerSquareMeter(): ValueRequirement
    {
        return $this->purchasePricePerSquareMeter;
    }

    public function setPurchasePricePerSquareMeter(ValueRequirement $purchasePricePerSquareMeter): self
    {
        $this->purchasePricePerSquareMeter = $purchasePricePerSquareMeter;
        return $this;
    }

    public function getNetColdRent(): ValueRequirement
    {
        return $this->netColdRent;
    }

    public function setNetColdRent(ValueRequirement $netColdRent): self
    {
        $this->netColdRent = $netColdRent;
        return $this;
    }

    public function getColdRent(): ValueRequirement
    {
        return $this->coldRent;
    }

    public function setColdRent(ValueRequirement $coldRent): self
    {
        $this->coldRent = $coldRent;
        return $this;
    }

    public function getRentalPricePerSquareMeter(): ValueRequirement
    {
        return $this->rentalPricePerSquareMeter;
    }

    public function setRentalPricePerSquareMeter(ValueRequirement $rentalPricePerSquareMeter): self
    {
        $this->rentalPricePerSquareMeter = $rentalPricePerSquareMeter;
        return $this;
    }

    public function getLease(): ValueRequirement
    {
        return $this->lease;
    }

    public function setLease(ValueRequirement $lease): self
    {
        $this->lease = $lease;
        return $this;
    }

    public function isRoofingRequired(): bool
    {
        return $this->roofingRequired;
    }

    public function setRoofingRequired(bool $roofingRequired): self
    {
        $this->roofingRequired = $roofingRequired;
        return $this;
    }

    public function isOutdoorSpaceRequired(): bool
    {
        return $this->outdoorSpaceRequired;
    }

    public function setOutdoorSpaceRequired(bool $outdoorSpaceRequired): self
    {
        $this->outdoorSpaceRequired = $outdoorSpaceRequired;
        return $this;
    }

    public function isShowroomRequired(): bool
    {
        return $this->showroomRequired;
    }

    public function setShowroomRequired(bool $showroomRequired): self
    {
        $this->showroomRequired = $showroomRequired;
        return $this;
    }

    public function isShopWindowRequired(): bool
    {
        return $this->shopWindowRequired;
    }

    public function setShopWindowRequired(bool $shopWindowRequired): self
    {
        $this->shopWindowRequired = $shopWindowRequired;
        return $this;
    }

    public function isGroundLevelSalesAreaRequired(): bool
    {
        return $this->groundLevelSalesAreaRequired;
    }

    public function setGroundLevelSalesAreaRequired(bool $groundLevelSalesAreaRequired): self
    {
        $this->groundLevelSalesAreaRequired = $groundLevelSalesAreaRequired;
        return $this;
    }

    public function isBarrierFreeAccessRequired(): bool
    {
        return $this->barrierFreeAccessRequired;
    }

    public function setBarrierFreeAccessRequired(bool $barrierFreeAccessRequired): self
    {
        $this->barrierFreeAccessRequired = $barrierFreeAccessRequired;
        return $this;
    }

    public function getNumberOfParkingLots(): ?int
    {
        return $this->numberOfParkingLots;
    }

    public function setNumberOfParkingLots(?int $numberOfParkingLots): self
    {
        $this->numberOfParkingLots = $numberOfParkingLots;
        return $this;
    }

    public function getRoomCount(): ?int
    {
        return $this->roomCount;
    }

    public function setRoomCount(?int $roomCount): self
    {
        $this->roomCount = $roomCount;
        return $this;
    }

    public function getEarliestAvailableFrom(): ?\DateTime
    {
        return $this->earliestAvailableFrom;
    }

    public function setEarliestAvailableFrom(?\DateTime $earliestAvailableFrom): self
    {
        $this->earliestAvailableFrom = $earliestAvailableFrom;
        return $this;
    }

    public function getLatestAvailableFrom(): ?\DateTime
    {
        return $this->latestAvailableFrom;
    }

    public function setLatestAvailableFrom(?\DateTime $latestAvailableFrom): self
    {
        $this->latestAvailableFrom = $latestAvailableFrom;
        return $this;
    }


}
