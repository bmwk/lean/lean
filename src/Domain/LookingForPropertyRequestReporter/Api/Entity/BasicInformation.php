<?php

declare(strict_types=1);

namespace App\Domain\LookingForPropertyRequestReporter\Api\Entity;

use App\Domain\Entity\RequestReason;
use Symfony\Component\Validator\Constraints as Assert;

class BasicInformation
{
    #[Assert\NotBlank]
    private string $title;

    #[Assert\NotBlank]
    private RequestReason $requestReason;

    #[Assert\NotBlank]
    private int $industryClassificationId;

    private ?int $industrySectorIndustryClassificationId = null;

    private ?int $industryDetailsIndustryClassificationId = null;

    private ?string $conceptDescription = null;

    private ?\DateTime $requestAvailableTill = null;

    private bool $automaticEnding;

    public static function createFormApiRequestJsonContent(object $jsonContent): self
    {
        $basicInformation = new self();

        $basicInformation->title = $jsonContent->title;
        $basicInformation->requestReason = RequestReason::from((int) $jsonContent->requestReason);
        $basicInformation->industryClassificationId = $jsonContent->industryClassificationId;

        $basicInformation->conceptDescription = isset($jsonContent->conceptDescription) && !empty($jsonContent->conceptDescription) ? $jsonContent->conceptDescription : null;
        $basicInformation->requestAvailableTill = isset($jsonContent->requestAvailableTill) && !empty($jsonContent->requestAvailableTill) ? new \DateTime($jsonContent->requestAvailableTill) : null;
        $basicInformation->automaticEnding = $jsonContent->automaticEnding;

        return $basicInformation;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getRequestReason(): RequestReason
    {
        return $this->requestReason;
    }

    public function setRequestReason(RequestReason $requestReason): self
    {
        $this->requestReason = $requestReason;

        return $this;
    }

    public function getIndustryClassificationId(): int
    {
        return $this->industryClassificationId;
    }

    public function setIndustryClassificationId(int $industryClassificationId): self
    {
        $this->industryClassificationId = $industryClassificationId;

        return $this;
    }

    public function getIndustrySectorIndustryClassificationId(): ?int
    {
        return $this->industrySectorIndustryClassificationId;
    }

    public function setIndustrySectorIndustryClassificationId(?int $industrySectorIndustryClassificationId): self
    {
        $this->industrySectorIndustryClassificationId = $industrySectorIndustryClassificationId;

        return $this;
    }

    public function getIndustryDetailsIndustryClassificationId(): ?int
    {
        return $this->industryDetailsIndustryClassificationId;
    }

    public function setIndustryDetailsIndustryClassificationId(?int $industryDetailsIndustryClassificationId): self
    {
        $this->industryDetailsIndustryClassificationId = $industryDetailsIndustryClassificationId;
        return $this;
    }

    public function getConceptDescription(): ?string
    {
        return $this->conceptDescription;
    }

    public function setConceptDescription(?string $conceptDescription): self
    {
        $this->conceptDescription = $conceptDescription;

        return $this;
    }

    public function getRequestAvailableTill(): ?\DateTime
    {
        return $this->requestAvailableTill;
    }

    public function setRequestAvailableTill(?\DateTime $requestAvailableTill): self
    {
        $this->requestAvailableTill = $requestAvailableTill;

        return $this;
    }

    public function isAutomaticEnding(): bool
    {
        return $this->automaticEnding;
    }

    public function setAutomaticEnding(bool $automaticEnding): self
    {
        $this->automaticEnding = $automaticEnding;

        return $this;
    }
}
