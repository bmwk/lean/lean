<?php

declare(strict_types=1);

namespace App\Domain\LookingForPropertyRequestReporter;

use App\Domain\Entity\Account;
use App\Domain\Entity\LookingForPropertyRequestReporter\LookingForPropertyRequestReporterConfiguration;
use App\Domain\Entity\LookingForPropertyRequestReporter\ReporterInformationFormField;
use App\Repository\LookingForPropertyRequestReporterConfigurationRepository;
use Symfony\Component\Uid\Uuid;

class LookingForPropertyRequestReporterConfigurationService
{
    public function __construct(
        private readonly LookingForPropertyRequestReporterConfigurationRepository $lookingForPropertyRequestReporterConfigurationRepository,
        private readonly string $projectDir
    ) {
    }

    public function fetchLookingForPropertyRequestReporterConfigurationByAccountKey(Uuid $accountKey): ?LookingForPropertyRequestReporterConfiguration
    {
        return $this->lookingForPropertyRequestReporterConfigurationRepository->findOneBy(criteria: ['accountKey' => $accountKey]);
    }

    public function fetchLookingForPropertyRequestReporterConfigurationByAccount(Account $account): ?LookingForPropertyRequestReporterConfiguration
    {
        return $this->lookingForPropertyRequestReporterConfigurationRepository->findOneByAccount(account: $account);
    }

    public function buildDefaultLookingForPropertyRequestReporterConfigurationByAccount(Account $account): LookingForPropertyRequestReporterConfiguration
    {
        $lookingForPropertyRequestReporterConfiguration = new LookingForPropertyRequestReporterConfiguration();

        $lookingForPropertyRequestReporterConfiguration
            ->setAccount($account)
            ->setReportNotificationEmail($account->getEmail())
            ->setAccountKey(Uuid::v6())
            ->setFirstColor('#0d5aa7')
            ->setFirstFontColor('#f0f0f0')
            ->setSecondaryColor('#ff6347')
            ->setSecondaryFontColor('#f0f0f0')
            ->setTitle('Gesuchsmelder')
            ->setText($this->defaultText())
            ->setRequiredReporterFormFields([
                ReporterInformationFormField::FIRST_NAME,
                ReporterInformationFormField::NAME,
            ])
            ->setRequiredPropertyInformationFormFields([])
            ->setRequiredLocationFormFields([])
            ->setRequiredBasicInformationFormFields([])
            ->setPrivacyPolicy(base64_encode($this->defaultPrivacyPolicyText()));

        return $lookingForPropertyRequestReporterConfiguration;
    }

    public function calculateIdealHexFontColor(string $hexBackgroundColor): string
    {
        $R1 = hexdec(substr($hexBackgroundColor, 1, 2));
        $G1 = hexdec(substr($hexBackgroundColor, 3, 2));
        $B1 = hexdec(substr($hexBackgroundColor, 5, 2));

        $blackColor = '#000000';
        $R2BlackColor = hexdec(substr($blackColor, 1, 2));
        $G2BlackColor = hexdec(substr($blackColor, 3, 2));
        $B2BlackColor = hexdec(substr($blackColor, 5, 2));

        $L1 = 0.2126 * pow($R1 / 255, 2.2)
            + 0.7152 * pow($G1 / 255, 2.2)
            + 0.0722 * pow($B1 / 255, 2.2);

        $L2 = 0.2126 * pow($R2BlackColor / 255, 2.2)
            + 0.7152 * pow($G2BlackColor / 255, 2.2)
            + 0.0722 * pow($B2BlackColor / 255, 2.2);

        if ($L1 > $L2) {
            $contrastRatio = (int) (($L1 + 0.05) / ($L2 + 0.05));
        } else {
            $contrastRatio = (int) (($L2 + 0.05) / ($L1 + 0.05));
        }

        if ($contrastRatio > 5) {
            return '#0f0f0f';
        } else {
            return '#f0f0f0';
        }
    }

    private function defaultText(): string
    {
        return file_get_contents(filename: $this->projectDir . '/assets/files/text_templates/looking_for_property_request_reporter_default_description.txt');
    }

    private function defaultPrivacyPolicyText(): string
    {
        return file_get_contents(filename: $this->projectDir . '/assets/files/text_templates/looking_for_property_request_reporter_default_privacy_policy.html');
    }
}
