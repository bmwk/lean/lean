<?php

declare(strict_types=1);

namespace App\Domain\LookingForPropertyRequestReporter;

use App\Domain\Entity\LookingForPropertyRequestReport\LookingForPropertyRequestReport;
use App\Domain\Entity\LookingForPropertyRequestReporter\LookingForPropertyRequestReporterConfiguration;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class LookingForPropertyRequestReporterEmailService
{
    public function __construct(
        private readonly string $noReplyEmailAddress,
        private readonly MailerInterface $mailer,
        private readonly UrlGeneratorInterface $urlGenerator
    ) {
    }

    public function sendLookingForPropertyRequestReportCreatedConfirmationEmail(LookingForPropertyRequestReport $lookingForPropertyRequestReport): void
    {
        $name = $lookingForPropertyRequestReport->getReporterFirstName() . ' ' . $lookingForPropertyRequestReport->getReporterLastName();
        $name = trim(string: $name);

        $account = $lookingForPropertyRequestReport->getAccount();

        $confirmationUrl = $this->urlGenerator->generate(
            name: 'looking_for_property_request_reporter_report_confirmation',
            parameters: [
                'reportUuid'        => $lookingForPropertyRequestReport->getUuid()->toRfc4122(),
                'confirmationToken' => $lookingForPropertyRequestReport->getConfirmationToken()->toRfc4122(),
            ],
            referenceType: UrlGeneratorInterface::ABSOLUTE_URL
        );

        $email = (new TemplatedEmail())
            ->from(addresses: $this->noReplyEmailAddress)
            ->to(new Address(address: $lookingForPropertyRequestReport->getReporterEmail(), name: $name))
            ->subject(subject: 'Ihre Gesuchsmeldung')
            ->htmlTemplate(template: 'company_location_management/email/looking_for_property_request_report/confirmation_email.html.twig')
            ->textTemplate(template: 'company_location_management/email/looking_for_property_request_report/confirmation_email.txt.twig')
            ->context(context: [
                'confirmationUrl' => $confirmationUrl,
                'placeName'       => $account->getAssignedPlace()->getPlaceName(),
                'account'         => $account,
            ]);

        $this->mailer->send($email);
    }

    public function sendNewLookingForPropertyRequestReportNotificationEmail(
        LookingForPropertyRequestReport $lookingForPropertyRequestReport,
        LookingForPropertyRequestReporterConfiguration $lookingForPropertyRequestReporterConfiguration
    ): void {
        $account = $lookingForPropertyRequestReport->getAccount();

        $reportUrl = $this->urlGenerator->generate(
            name: 'company_location_management_looking_for_property_request_report_report',
            parameters: [
                'id' => $lookingForPropertyRequestReport->getId(),
            ],
            referenceType: UrlGeneratorInterface::ABSOLUTE_URL
        );

        $email = (new TemplatedEmail())
            ->from(addresses: $this->noReplyEmailAddress)
            ->to(new Address(address: $lookingForPropertyRequestReporterConfiguration->getReportNotificationEmail()))
            ->subject(subject: 'Eine neue Gesuchsmeldung ist eingegangen')
            ->htmlTemplate(template: 'company_location_management/email/looking_for_property_request_report/notification_email.html.twig')
            ->textTemplate(template: 'company_location_management/email/looking_for_property_request_report/notification_email.txt.twig')
            ->context(context: [
                'reportUrl'                       => $reportUrl,
                'lookingForPropertyRequestReport' => $lookingForPropertyRequestReport,
                'account'                         => $account,
            ]);

        $this->mailer->send($email);
    }
}
