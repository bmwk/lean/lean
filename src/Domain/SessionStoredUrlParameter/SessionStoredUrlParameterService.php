<?php

declare(strict_types=1);

namespace App\Domain\SessionStoredUrlParameter;

use App\Domain\Entity\UrlParameterInterface;
use App\SessionStorage\SessionStorageService;

class SessionStoredUrlParameterService
{
    public function __construct(
        private readonly SessionStorageService $sessionStorageService
    ) {
    }

    /**
     * @param string[] $urlParameterObjectClasses
     */
    public function fetchMergedUrlParameters(array $urlParameterObjectClasses, string $sessionKeyName): array
    {
        $urlParameters = [];

        foreach ($urlParameterObjectClasses as $urlParameterObjectClass) {
            $urlParameterObject = $this->sessionStorageService->fetchObject(dataClassName: $urlParameterObjectClass, sessionKeyName: $sessionKeyName);

            if (($urlParameterObject instanceof UrlParameterInterface) === true) {
                $urlParameters = array_merge($urlParameters, $urlParameterObject->toUrlParameters());
            }
        }

        return $urlParameters;
    }
}
