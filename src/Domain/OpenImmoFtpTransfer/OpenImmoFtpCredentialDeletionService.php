<?php

declare(strict_types=1);

namespace App\Domain\OpenImmoFtpTransfer;

use App\Domain\Entity\Account;
use App\Domain\Entity\OpenImmoFtpTransfer\OpenImmoFtpCredential;
use App\Repository\OpenImmoFtpTransfer\OpenImmoFtpCredentialRepository;
use Doctrine\ORM\EntityManagerInterface;

class OpenImmoFtpCredentialDeletionService
{
    public function __construct(
        private readonly OpenImmoFtpCredentialRepository $openImmoFtpCredentialRepository,
        private readonly OpenImmoFtpTransferTaskDeletionService $openImmoFtpTransferTaskDeletionService,
        private readonly OpenImmoTransferLogDeletionService $openImmoTransferLogDeletionService,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function deleteOpenImmoFtpCredential(OpenImmoFtpCredential $openImmoFtpCredential): void
    {
        $this->hardDeleteOpenImmoFtpCredential(openImmoFtpCredential: $openImmoFtpCredential);
    }

    public function deleteOpenImmoFtpCredentialsByAccount(Account $account): void
    {
        $this->hardDeleteOpenImmoFtpCredentialsByAccount(account: $account);
    }

    private function hardDeleteOpenImmoFtpCredential(OpenImmoFtpCredential $openImmoFtpCredential): void
    {
        foreach ($openImmoFtpCredential->getOpenImmoFtpTransferTasks() as $openImmoFtpTransferTask) {
            $this->openImmoFtpTransferTaskDeletionService->deleteOpenImmoFtpTransferTask(openImmoFtpTransferTask: $openImmoFtpTransferTask);
        }

        foreach ($openImmoFtpCredential->getOpenImmoTransferLogs() as $openImmoTransferLog) {
            $this->openImmoTransferLogDeletionService->deleteOpenImmoTransferLog(openImmoTransferLog: $openImmoTransferLog);
        }

        $this->entityManager->remove(entity: $openImmoFtpCredential);
        $this->entityManager->flush();
    }

    private function hardDeleteOpenImmoFtpCredentialsByAccount(Account $account): void
    {
        $openImmoFtpCredentials = $this->openImmoFtpCredentialRepository->findBy(criteria: ['account' => $account]);

        foreach ($openImmoFtpCredentials as $openImmoFtpCredential) {
            $this->hardDeleteOpenImmoFtpCredential(openImmoFtpCredential: $openImmoFtpCredential);
        }
    }
}
