<?php

declare(strict_types=1);

namespace App\Domain\OpenImmoFtpTransfer;

use App\Domain\Entity\OpenImmoFtpTransfer\OpenImmoTransferLog;
use Doctrine\ORM\EntityManagerInterface;

class OpenImmoTransferLogDeletionService
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function deleteOpenImmoTransferLog(OpenImmoTransferLog $openImmoTransferLog): void
    {
        $this->hardDeleteOpenImmoTransferLog(openImmoTransferLog: $openImmoTransferLog);
    }

    private function hardDeleteOpenImmoTransferLog(OpenImmoTransferLog $openImmoTransferLog): void
    {
        $this->entityManager->remove(entity: $openImmoTransferLog);
        $this->entityManager->flush();
    }
}
