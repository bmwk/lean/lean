<?php

declare(strict_types=1);

namespace App\Domain\OpenImmoFtpTransfer;

use App\Domain\Entity\OpenImmoExporter\ExportStatus;
use App\Domain\Entity\OpenImmoExporter\OpenImmoExport;
use App\Domain\Entity\OpenImmoFtpTransfer\FtpProtocol;
use App\Domain\Entity\OpenImmoFtpTransfer\OpenImmoFtpCredential;
use App\Domain\File\FileService;
use App\Domain\OpenImmoExporter\Exception\ExportNotYetCompletedException;
use App\Domain\OpenImmoFtpTransfer\Exception\FtpConnectionFailedException;
use App\Domain\OpenImmoFtpTransfer\Exception\UnsupportedFtpProtocolException;

class OpenImmoFtpService
{
    public function __construct(
        private readonly FileService $fileService,
        private readonly OpenImmoFtpCredentialService $openImmoFtpCredentialService
    ) {
    }

    public function openImmoFileTransfer(OpenImmoFtpCredential $openImmoFtpCredential, OpenImmoExport $openImmoExport): void
    {
        if ($openImmoExport->getExportStatus() !== ExportStatus::DONE) {
            throw new ExportNotYetCompletedException();
        }

        if ($openImmoFtpCredential->getFtpProtocol() === FtpProtocol::FTP_FTPS) {
            $this->openImmoFileTransferViaFtp(openImmoFtpCredential: $openImmoFtpCredential, openImmoExport: $openImmoExport);
        } elseif ($openImmoFtpCredential->getFtpProtocol() === FtpProtocol::SFTP) {
            $this->openImmoFileTransferViaSftp(openImmoFtpCredential: $openImmoFtpCredential, openImmoExport: $openImmoExport);
        } else {
            throw new UnsupportedFtpProtocolException();
        }
    }

    private function ftpConnect(OpenImmoFtpCredential $openImmoFtpCredential): \FTP\Connection
    {
        $port = ($openImmoFtpCredential->getPort() !== null) ? $openImmoFtpCredential->getPort() : 21;

        try {
            $ftpConnection = ftp_ssl_connect(hostname: $openImmoFtpCredential->getHostname(), port: $port);
        } catch (\ErrorException $errorException) {
            throw new FtpConnectionFailedException();
        }

        if ($ftpConnection === false) {
            throw new FtpConnectionFailedException();
        }

        $plainPassword = $this->openImmoFtpCredentialService->openSslDecrypt(encryptedPassword: $openImmoFtpCredential->getPassword());

        $ftpLoginSuccessful = ftp_login(
            ftp: $ftpConnection,
            username: $openImmoFtpCredential->getUsername(),
            password: $plainPassword
        );

        if ($ftpLoginSuccessful === false) {
            ftp_close(ftp: $ftpConnection);

            try {
                $ftpConnection = ftp_connect(hostname: $openImmoFtpCredential->getHostname(), port: $port);
            } catch (\ErrorException $errorException) {
                throw new FtpConnectionFailedException();
            }

            if ($ftpConnection === false) {
                throw new FtpConnectionFailedException();
            }

            $ftpLoginSuccessful = ftp_login(
                ftp: $ftpConnection,
                username: $openImmoFtpCredential->getUsername(),
                password: $plainPassword
            );

            if ($ftpLoginSuccessful === false) {
                throw new FtpConnectionFailedException();
            }
        }

        ftp_pasv(ftp: $ftpConnection, enable: true);

        return $ftpConnection;
    }

    private function openImmoFileTransferViaFtp(OpenImmoFtpCredential $openImmoFtpCredential, OpenImmoExport $openImmoExport): void
    {
        if ($openImmoExport->getExportStatus() !== ExportStatus::DONE) {
            throw new ExportNotYetCompletedException();
        }

        $ftpConnection = $this->ftpConnect(openImmoFtpCredential: $openImmoFtpCredential);
        $remoteFilename = $openImmoExport->getBuildingUnit()->getId() . '_' . $openImmoExport->getFile()->getFileName();
        $localFilepath = $this->fileService->getRealPathFromFile(file: $openImmoExport->getFile());

        ftp_put(
            ftp: $ftpConnection,
            remote_filename: $remoteFilename,
            local_filename: $localFilepath,
            mode: FTP_BINARY
        );

        ftp_close($ftpConnection);
    }

    private function openImmoFileTransferViaSftp(OpenImmoFtpCredential $openImmoFtpCredential, OpenImmoExport $openImmoExport): void
    {
        if ($openImmoExport->getExportStatus() !== ExportStatus::DONE) {
            throw new ExportNotYetCompletedException();
        }

        $port = ($openImmoFtpCredential->getPort() !== null) ? $openImmoFtpCredential->getPort() : 22;

        try {
            $sshConnection = ssh2_connect(host: $openImmoFtpCredential->getHostname(), port: $port);
        } catch (\ErrorException $errorException) {
            throw new FtpConnectionFailedException();
        }

        if ($sshConnection === false) {
            throw new FtpConnectionFailedException();
        }

        $plainPassword = $this->openImmoFtpCredentialService->openSslDecrypt(encryptedPassword: $openImmoFtpCredential->getPassword());

        if (
            ssh2_auth_password(
                session: $sshConnection,
                username: $openImmoFtpCredential->getUsername(),
                password: $plainPassword
            ) === false
        ) {
            throw new FtpConnectionFailedException();
        }

        $remoteFilename = $openImmoExport->getBuildingUnit()->getId() . '_' . $openImmoExport->getFile()->getFileName();
        $localFilepath = $this->fileService->getRealPathFromFile(file: $openImmoExport->getFile());

        ssh2_scp_send(session: $sshConnection, local_file: $localFilepath, remote_file: $remoteFilename, create_mode: 0644);

        ssh2_disconnect(session: $sshConnection);
    }
}
