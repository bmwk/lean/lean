<?php

declare(strict_types=1);

namespace App\Domain\OpenImmoFtpTransfer\Exception;

class UnsupportedFtpProtocolException extends \RuntimeException implements OpenImmoFtpExceptionInterface
{
}
