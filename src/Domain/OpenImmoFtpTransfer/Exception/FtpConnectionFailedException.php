<?php

declare(strict_types=1);

namespace App\Domain\OpenImmoFtpTransfer\Exception;

class FtpConnectionFailedException extends \RuntimeException implements OpenImmoFtpExceptionInterface
{
}
