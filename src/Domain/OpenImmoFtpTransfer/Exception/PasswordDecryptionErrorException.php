<?php

declare(strict_types=1);

namespace App\Domain\OpenImmoFtpTransfer\Exception;

class PasswordDecryptionErrorException extends \RuntimeException implements OpenImmoFtpExceptionInterface
{
}
