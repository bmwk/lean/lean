<?php

declare(strict_types=1);

namespace App\Domain\OpenImmoFtpTransfer\Exception;

class PasswordEncryptionErrorException extends \RuntimeException implements OpenImmoFtpExceptionInterface
{
}
