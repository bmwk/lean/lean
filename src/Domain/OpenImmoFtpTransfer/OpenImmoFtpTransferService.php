<?php

declare(strict_types=1);

namespace App\Domain\OpenImmoFtpTransfer;

use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\OpenImmoExporter\ExportActionType;
use App\Domain\Entity\OpenImmoExporter\ExportStatus;
use App\Domain\Entity\OpenImmoExporter\OpenImmoExport;
use App\Domain\Entity\OpenImmoFtpTransfer\OpenImmoFtpCredential;
use App\Domain\Entity\OpenImmoFtpTransfer\OpenImmoFtpTransferTask;
use App\Domain\Entity\OpenImmoFtpTransfer\OpenImmoTransferLog;
use App\Domain\Entity\OpenImmoFtpTransfer\OpenImmoTransferStatus;
use App\Domain\Entity\Property\BuildingUnit;
use App\Domain\Entity\TaskStatus;
use App\Domain\OpenImmoExporter\Exception\ExportNotYetCompletedException;
use App\Domain\OpenImmoExporter\OpenImmoExportDeletionService;
use App\Domain\OpenImmoFtpTransfer\Exception\OpenImmoFtpExceptionInterface;
use App\Repository\OpenImmoFtpTransfer\OpenImmoFtpTransferTaskRepository;
use App\Repository\OpenImmoFtpTransfer\OpenImmoTransferLogRepository;
use Doctrine\ORM\EntityManagerInterface;

class OpenImmoFtpTransferService
{
    public function __construct(
        private readonly OpenImmoFtpTransferTaskRepository $openImmoFtpTransferTaskRepository,
        private readonly OpenImmoTransferLogRepository $openImmoTransferLogRepository,
        private readonly OpenImmoFtpService $openImmoFtpService,
        private readonly OpenImmoExportDeletionService $openImmoExportDeletionService,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    /**
     * @return OpenImmoFtpTransferTask[]
     */
    public function fetchOpenImmoFtpTransferTasksByTaskStatus(TaskStatus $taskStatus, int $limit): array
    {
        return $this->openImmoFtpTransferTaskRepository->findByTasksStatus(taskStatus: $taskStatus, limit: $limit);
    }

    /**
     * @param OpenImmoFtpTransferTask[] $openImmoFtpTransferTasks
     * @return BuildingUnit[]
     */
    public function fetchExportedAndNotDeletedBuildingUnitsByOpenImmoFtpCredential(OpenImmoFtpCredential $openImmoFtpCredential): array
    {
        $buildingUnits = [];

        foreach ($openImmoFtpCredential->getOpenImmoTransferLogs() as $openImmoTransferLog) {
            if ($openImmoTransferLog->getOpenImmoTransferStatus() !== OpenImmoTransferStatus::DONE) {
                continue;
            }

            if ($openImmoTransferLog->getExportActionType() === ExportActionType::CHANGE) {
                if (in_array(needle: $openImmoTransferLog->getBuildingUnit(), haystack: $buildingUnits) === false) {
                    $buildingUnits[] = $openImmoTransferLog->getBuildingUnit();
                }
            } elseif ($openImmoTransferLog->getExportActionType() === ExportActionType::DELETE) {
                $buildingUnits = array_diff($buildingUnits, [$openImmoTransferLog->getBuildingUnit()]);
            }
        }

        return $buildingUnits;
    }

    /**
     * @return OpenImmoTransferLog[]
     */
    public function fetchOpenImmoTransferLogByBuildingUnit(BuildingUnit $buildingUnit): array
    {
        return $this->openImmoTransferLogRepository->findByBuildingUnit(buildingUnit: $buildingUnit);
    }

    public function createAndSaveOpenImmoFtpTransferTask(
        OpenImmoExport $openImmoExport,
        OpenImmoFtpCredential $openImmoFtpCredential,
        AccountUser $accountUser
    ): OpenImmoFtpTransferTask {
        $openImmoFtpTransferTask = new OpenImmoFtpTransferTask();

        $openImmoFtpTransferTask
            ->setOpenImmoFtpCredential($openImmoFtpCredential)
            ->setOpenImmoExport($openImmoExport)
            ->setTaskStatus(TaskStatus::CREATED)
            ->setAccount($accountUser->getAccount())
            ->setCreatedByAccountUser($accountUser);

        $this->entityManager->persist($openImmoFtpTransferTask);
        $this->entityManager->flush();

        return $openImmoFtpTransferTask;
    }

    public function createAndSaveOpenImmoTransferLog(
        OpenImmoTransferStatus $openImmoTransferStatus,
        \DateTimeImmutable $transferredAt,
        ExportActionType $exportActionType,
        OpenImmoFtpCredential $openImmoFtpCredential,
        BuildingUnit $buildingUnit
    ): OpenImmoTransferLog {
        $openImmoTransferLog = new OpenImmoTransferLog();

        $openImmoTransferLog
            ->setOpenImmoTransferStatus($openImmoTransferStatus)
            ->setTransferredAt($transferredAt)
            ->setExportActionType($exportActionType)
            ->setOpenImmoFtpCredential($openImmoFtpCredential)
            ->setBuildingUnit($buildingUnit);

        $this->entityManager->persist($openImmoTransferLog);
        $this->entityManager->flush();

        return $openImmoTransferLog;
    }

    public function executeOpenImmoFtpTransferTasks(): void
    {
        $openImmoFtpTransferTasks = $this->fetchOpenImmoFtpTransferTasksByTaskStatus(taskStatus: TaskStatus::CREATED, limit: 5);

        foreach ($openImmoFtpTransferTasks as $openImmoFtpTransferTask) {
            $this->executeOpenImmoFtpTransferTask(openImmoFtpTransferTask: $openImmoFtpTransferTask);
        }
    }

    public function executeOpenImmoFtpTransferTask(OpenImmoFtpTransferTask $openImmoFtpTransferTask): void
    {
        $openImmoFtpTransferTask->setTaskStatus(TaskStatus::IN_PROGRESS);

        $this->entityManager->flush();

        try {
            $this->doOpenImmoFtpTransfer(
                openImmoFtpCredential: $openImmoFtpTransferTask->getOpenImmoFtpCredential(),
                openImmoExport: $openImmoFtpTransferTask->getOpenImmoExport()
            );
        } catch (ExportNotYetCompletedException $exportNotYetCompletedException) {
            $openImmoFtpTransferTask->setTaskStatus(TaskStatus::CREATED);

            $this->entityManager->flush();
        } catch (OpenImmoFtpExceptionInterface $openImmoFtpException) {
        }
    }

    public function doOpenImmoFtpTransfer(OpenImmoFtpCredential $openImmoFtpCredential, OpenImmoExport $openImmoExport): void
    {
        if ($openImmoExport->getExportStatus() !== ExportStatus::DONE) {
            throw new ExportNotYetCompletedException();
        }

        try {
            $this->openImmoFtpService->openImmoFileTransfer(openImmoFtpCredential: $openImmoFtpCredential, openImmoExport: $openImmoExport);
        } catch (OpenImmoFtpExceptionInterface $openImmoFtpException) {
            $this->createAndSaveOpenImmoTransferLog(
                openImmoTransferStatus: OpenImmoTransferStatus::FAILED,
                transferredAt: new \DateTimeImmutable(),
                exportActionType: $openImmoExport->getExportActionType(),
                openImmoFtpCredential: $openImmoFtpCredential,
                buildingUnit: $openImmoExport->getBuildingUnit()
            );

            $this->openImmoExportDeletionService->deleteOpenImmoExport(openImmoExport: $openImmoExport);

            throw $openImmoFtpException;
        }

        $this->createAndSaveOpenImmoTransferLog(
            openImmoTransferStatus: OpenImmoTransferStatus::DONE,
            transferredAt: new \DateTimeImmutable(),
            exportActionType: $openImmoExport->getExportActionType(),
            openImmoFtpCredential: $openImmoFtpCredential,
            buildingUnit: $openImmoExport->getBuildingUnit()
        );

        $this->openImmoExportDeletionService->deleteOpenImmoExport(openImmoExport: $openImmoExport);
    }
}
