<?php

declare(strict_types=1);

namespace App\Domain\OpenImmoFtpTransfer;

use App\Domain\Entity\Account;
use App\Domain\Entity\OpenImmoFtpTransfer\OpenImmoFtpTransferTask;
use App\Repository\OpenImmoFtpTransfer\OpenImmoFtpTransferTaskRepository;
use Doctrine\ORM\EntityManagerInterface;

class OpenImmoFtpTransferTaskDeletionService
{
    public function __construct(
        private readonly OpenImmoFtpTransferTaskRepository $openImmoFtpTransferTaskRepository,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function deleteOpenImmoFtpTransferTask(OpenImmoFtpTransferTask $openImmoFtpTransferTask): void
    {
        $this->hardDeleteOpenImmoFtpTransferTask(openImmoFtpTransferTask: $openImmoFtpTransferTask);
    }

    public function deleteOpenImmoFtpTransferTasksByAccount(Account $account): void
    {
        $this->hardDeleteOpenImmoFtpTransferTasksByAccount(account: $account);
    }

    private function hardDeleteOpenImmoFtpTransferTask(OpenImmoFtpTransferTask $openImmoFtpTransferTask): void
    {
        $this->entityManager->remove(entity: $openImmoFtpTransferTask);
        $this->entityManager->flush();
    }

    private function hardDeleteOpenImmoFtpTransferTasksByAccount(Account $account): void
    {
        $openImmoFtpTransferTasks = $this->openImmoFtpTransferTaskRepository->findBy(criteria: ['account' => $account]);

        foreach ($openImmoFtpTransferTasks as $openImmoFtpTransferTask) {
            $this->deleteOpenImmoFtpTransferTask(openImmoFtpTransferTask: $openImmoFtpTransferTask);
        }
    }
}
