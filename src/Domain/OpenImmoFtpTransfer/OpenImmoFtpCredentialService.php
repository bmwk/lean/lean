<?php

declare(strict_types=1);

namespace App\Domain\OpenImmoFtpTransfer;

use App\Domain\Entity\Account;
use App\Domain\Entity\OpenImmoFtpTransfer\OpenImmoFtpCredential;
use App\Domain\OpenImmoFtpTransfer\Exception\PasswordDecryptionErrorException;
use App\Domain\OpenImmoFtpTransfer\Exception\PasswordEncryptionErrorException;
use App\Repository\OpenImmoFtpTransfer\OpenImmoFtpCredentialRepository;

class OpenImmoFtpCredentialService
{
    private const OPENSSL_ENCRYPT_CIPHER_ALGO = 'aria-256-cbc';
    private const OPENSSL_ENCRYPT_OPTIONS = 0;
    private const OPENSSL_ENCRYPT_IV = '1234567891011121';

    public function __construct(
        private readonly string $secret,
        private readonly OpenImmoFtpCredentialRepository $openImmoFtpCredentialRepository
    ) {
    }

    public function fetchOpenImmoFtpCredentialById(int $id, Account $account): ?OpenImmoFtpCredential
    {
        return $this->openImmoFtpCredentialRepository->findOneBy(criteria: ['id' => $id, 'account' => $account]);
    }

    /**
     * @return OpenImmoFtpCredential[]
     */
    public function fetchOpenImmoFtpCredentials(Account $account): array
    {
        return $this->openImmoFtpCredentialRepository->findBy(criteria: ['account' => $account]);
    }

    public function checkIfOpenImmoFtpCredentialsExists(Account $account): bool
    {
        return $this->openImmoFtpCredentialRepository->findOneBy(criteria: ['account' => $account]) !== null;
    }

    public function openSslEncrypt(string $plainPassword): string
    {
        $encryptedPassword = openssl_encrypt(
            data: $plainPassword,
            cipher_algo: self::OPENSSL_ENCRYPT_CIPHER_ALGO,
            passphrase: $this->secret,
            options: self::OPENSSL_ENCRYPT_OPTIONS,
            iv: self::OPENSSL_ENCRYPT_IV
        );

        if ($encryptedPassword === false) {
            throw new PasswordEncryptionErrorException();
        }

        return $encryptedPassword;
    }

    public function openSslDecrypt(string $encryptedPassword): string
    {
        $plainPassword = openssl_decrypt(
            data: $encryptedPassword,
            cipher_algo: self::OPENSSL_ENCRYPT_CIPHER_ALGO,
            passphrase: $this->secret,
            options: self::OPENSSL_ENCRYPT_OPTIONS,
            iv: self::OPENSSL_ENCRYPT_IV
        );

        if ($plainPassword === false) {
            throw new PasswordDecryptionErrorException();
        }

        return $plainPassword;
    }
}
