<?php

declare(strict_types=1);

namespace App\Domain\LocationIndicators;

use App\Domain\Entity\Account;
use App\Domain\Entity\Place;
use App\Domain\Entity\WegweiserKommunePlaceMapping;
use App\Repository\WegweiserKommunePlaceMappingRepository;

class LocationIndicatorsService
{
    public function __construct(
        private readonly WegweiserKommunePlaceMappingRepository $wegweiserKommunePlaceMappingRepository
    ) {
    }

    public function fetchWegweiserKommunePlaceMappingByPlace(Account $account, Place $place): ?WegweiserKommunePlaceMapping
    {
        return $this->wegweiserKommunePlaceMappingRepository->findOneByPlace(account: $account, place: $place);
    }
}
