<?php

declare(strict_types=1);

namespace App\Domain\LocationIndicators;

use App\Domain\Entity\Account;
use App\Domain\Entity\WegweiserKommunePlaceMapping;
use App\Repository\WegweiserKommunePlaceMappingRepository;
use Doctrine\ORM\EntityManagerInterface;

class WegweiserKommunePlaceMappingDeletionService
{
    public function __construct(
        private readonly WegweiserKommunePlaceMappingRepository $wegweiserKommunePlaceMappingRepository,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    private function deleteWegweiserKommunePlaceMapping(WegweiserKommunePlaceMapping $wegweiserKommunePlaceMapping): void
    {
        $this->hardDeleteWegweiserKommunePlaceMapping(wegweiserKommunePlaceMapping: $wegweiserKommunePlaceMapping);
    }

    public function deleteWegweiserKommunePlaceMappingsByAccount(Account $account): void
    {
        $this->hardDeleteWegweiserKommunePlaceMappingsByAccount(account: $account);
    }

    private function hardDeleteWegweiserKommunePlaceMapping(WegweiserKommunePlaceMapping $wegweiserKommunePlaceMapping): void
    {
        $this->entityManager->remove($wegweiserKommunePlaceMapping);
        $this->entityManager->clear();
    }

    private function hardDeleteWegweiserKommunePlaceMappingsByAccount(Account $account): void
    {
        $wegweiserKommunePlaceMappings = $this->wegweiserKommunePlaceMappingRepository->findBy(criteria: ['account' => $account]);

        foreach ($wegweiserKommunePlaceMappings as $wegweiserKommunePlaceMapping) {
            $this->hardDeleteWegweiserKommunePlaceMapping(wegweiserKommunePlaceMapping: $wegweiserKommunePlaceMapping);
        }
    }
}
