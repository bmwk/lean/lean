<?php

declare(strict_types=1);

namespace App\Domain\Document;

use App\Domain\Entity\Document;
use App\Domain\File\FileDeletionService;
use Doctrine\ORM\EntityManagerInterface;

class DocumentDeletionService
{
    public function __construct(
        private readonly FileDeletionService $fileDeletionService,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function deleteDocument(Document $document): void
    {
        $this->hardDeleteDocument(document: $document);
    }

    private function hardDeleteDocument(Document $document): void
    {
        $file = $document->getFile();

        $this->entityManager->remove(entity: $document);

        $this->entityManager->flush();

        $this->fileDeletionService->deleteFile(file: $file);
    }
}
