<?php

declare(strict_types=1);

namespace App\Domain\Document;

use App\Domain\Entity\Account;
use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\Document;
use App\Domain\Entity\Property\BuildingUnit;
use App\Domain\File\FileService;
use App\Repository\DocumentRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class DocumentService
{
    public function __construct(
        private readonly DocumentRepository $documentRepository,
        private readonly FileService $fileService,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function fetchDocumentByIdAndBuildingUnit(
        Account $account,
        bool $withFromSubAccounts,
        int $id,
        BuildingUnit $buildingUnit
    ): ?Document {
        return $this->documentRepository->findOneByIdAndBuildingUnit(
            account: $account,
            withFromSubAccounts: $withFromSubAccounts,
            id: $id,
            buildingUnit: $buildingUnit
        );
    }

    public function createAndSaveDocumentFromFileContent(
        string $fileContent,
        string $filename,
        string $fileExtension,
        bool $public,
        AccountUser $accountUser,
        ?string $title = null
    ): Document {
        $document = new Document();

        $document
            ->setAccount($accountUser->getAccount())
            ->setCreatedByAccountUser($accountUser)
            ->setPublic($public)
            ->setTitle($title)
            ->setFile($this->fileService->saveFileFromFileContent(
                fileContent: $fileContent,
                filename: $filename,
                fileExtension: $fileExtension,
                account: $accountUser->getAccount(),
                public: $public
            ));

        $this->entityManager->persist(entity: $document);

        $this->entityManager->flush();

        return $document;
    }

    public function createAndSaveDocumentFromUploadedFile(
        UploadedFile $uploadedFile,
        bool $public,
        AccountUser $accountUser,
        ?string $title = null
    ): Document {
        return $this->createAndSaveDocumentFromFileContent(
            fileContent: $uploadedFile->getContent(),
            filename: $uploadedFile->getClientOriginalName(),
            fileExtension: $uploadedFile->getExtension(),
            public: $public,accountUser: $accountUser,
            title: $title
        );
    }
}
