<?php

declare(strict_types=1);

namespace App\Domain\SurveyResult;

use App\Domain\Document\DocumentDeletionService;
use App\Domain\Entity\SurveyResult\SurveyResult;
use App\Domain\Entity\SurveyResult\SurveyResultChapter;
use App\Domain\Image\ImageDeletionService;
use Doctrine\ORM\EntityManagerInterface;

class SurveyResultDeletionService
{
    public function __construct(
        private readonly ImageDeletionService $imageDeletionService,
        private readonly DocumentDeletionService $documentDeletionService,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function deleteSurveyResult(SurveyResult $surveyResult): void
    {
        $this->hardDeleteSurveyResult(surveyResult: $surveyResult);
    }

    public function deleteSurveyResultChapter(SurveyResultChapter $surveyResultChapter): void
    {
        $this->hardDeleteSurveyChapterResult(surveyResultChapter: $surveyResultChapter);
    }

    private function hardDeleteSurveyResult(SurveyResult $surveyResult): void
    {
        $image = $surveyResult->getImage();
        $document = $surveyResult->getDocument();

        foreach ($surveyResult->getSurveyResultChapters() as $surveyResultChapter) {
            $this->deleteSurveyResultChapter(surveyResultChapter: $surveyResultChapter);
        }

        $this->entityManager->remove(entity: $surveyResult);
        $this->entityManager->flush();

        if ($image !== null) {
            $this->imageDeletionService->deleteImage(image: $image);
        }

        if ($document !== null) {
            $this->documentDeletionService->deleteDocument(document: $document);
        }
    }

    private function hardDeleteSurveyChapterResult(SurveyResultChapter $surveyResultChapter): void
    {
        $image = $surveyResultChapter->getImage();
        $document = $surveyResultChapter->getDocument();

        $this->entityManager->remove(entity: $surveyResultChapter);
        $this->entityManager->flush();

        if ($image !== null) {
            $this->imageDeletionService->deleteImage(image: $image);
        }

        if ($document !== null) {
            $this->documentDeletionService->deleteDocument(document: $document);
        }
    }
}
