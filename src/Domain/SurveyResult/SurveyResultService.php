<?php

declare(strict_types=1);

namespace App\Domain\SurveyResult;

use App\Domain\Entity\Account;
use App\Domain\Entity\SurveyResult\SurveyResult;
use App\Domain\Entity\SurveyResult\SurveyResultChapter;
use App\Repository\SurveyResult\SurveyResultRepository;
use App\Repository\SurveyResult\SurveyResultChapterRepository;

class SurveyResultService
{
    public function __construct(
        private readonly SurveyResultRepository $surveyResultRepository,
        private readonly SurveyResultChapterRepository $surveyResultChapterRepository
    ) {
    }

    public function fetchSurveyResultByIdAndAccount(int $id, Account $account): ?SurveyResult
    {
        return $this->surveyResultRepository->findOneBy(criteria: [
            'id'      => $id,
            'account' => $account,
        ]);
    }

    public function fetchSurveyResultChapterByIdAndSurveyResult(int $id, SurveyResult $surveyResult): ?SurveyResultChapter
    {
        return $this->surveyResultChapterRepository->findOneBy(criteria: [
            'id'           => $id,
            'surveyResult' => $surveyResult,
        ]);
    }

    /**
     * @return SurveyResult[]
     */
    public function fetchSurveyResultsByAccount(Account $account): array
    {
        return $this->surveyResultRepository->findBy(criteria: ['account' => $account]);
    }
}
