<?php

declare(strict_types=1);

namespace App\Domain\InterfaceConfiguration;

use App\Domain\Entity\Account;
use App\Domain\Entity\HallOfInspiration\HallOfInspirationConfiguration;
use App\Domain\Entity\HystreetConfiguration;
use App\Domain\Entity\Place;
use App\Domain\Entity\WegweiserKommunePlaceMapping;
use App\Domain\Entity\ZielbildcheckConfiguration;
use App\Repository\HallOfInspirationConfigurationRepository;
use App\Repository\HystreetConfigurationRepository;
use App\Repository\WegweiserKommunePlaceMappingRepository;
use App\Repository\ZielbildcheckConfigurationRepository;

class InterfaceConfigurationService
{
    public function __construct(
        private readonly HallOfInspirationConfigurationRepository $hallOfInspirationConfigurationRepository,
        private readonly HystreetConfigurationRepository $hystreetConfigurationRepository,
        private readonly ZielbildcheckConfigurationRepository $zielbildcheckConfigurationRepository,
        private readonly WegweiserKommunePlaceMappingRepository $wegweiserKommunePlaceMappingRepository
    ) {
    }

    public function fetchHystreetConfigurationByAccount(Account $account): ?HystreetConfiguration
    {
        return $this->hystreetConfigurationRepository->findOneByAccount(account: $account);
    }

    public function fetchHallOfInspirationConfigurationByAccount(Account $account): ?HallOfInspirationConfiguration
    {
        return $this->hallOfInspirationConfigurationRepository->findOneByAccount(account: $account);
    }

    public function fetchZielbildcheckConfigurationByAccount(Account $account): ?ZielbildcheckConfiguration
    {
        return $this->zielbildcheckConfigurationRepository->findOneByAccount(account: $account);
    }

    public function fetchWegweiserKommunePlaceMappingByPlace(Account $account, Place $place): ?WegweiserKommunePlaceMapping
    {
        return $this->wegweiserKommunePlaceMappingRepository->findOneByPlace(account: $account, place: $place);
    }

    public function hasHystreetConfiguration(Account $account): bool
    {
        $hystreetConfiguration = $this->fetchHystreetConfigurationByAccount(account: $account);

        if ($hystreetConfiguration === null) {
            return false;
        }

        if (
            $hystreetConfiguration->getAccessToken() !== null
            && empty($hystreetConfiguration->getAccessToken()) !== true
        ) {
            return true;
        }

        return false;
    }

    public function hasHallOfInspirationConfiguration(Account $account): bool
    {
        $hallOfInspirationConfiguration = $this->fetchHallOfInspirationConfigurationByAccount(account: $account);

        if ($hallOfInspirationConfiguration === null) {
            return false;
        }

        if (
            $hallOfInspirationConfiguration->getAccessToken() !== null
            && empty($hallOfInspirationConfiguration->getAccessToken()) !== true
        ) {
            return true;
        }

        return false;
    }

    public function hasZielbildcheckConfiguration(Account $account): bool
    {
        $zielbildcheckConfiguration = $this->fetchZielbildcheckConfigurationByAccount(account: $account);

        if ($zielbildcheckConfiguration === null) {
            return false;
        }

        if (
            $zielbildcheckConfiguration->getAccessToken() !== null
            && empty($zielbildcheckConfiguration->getAccessToken()) !== true
        ) {
            return true;
        }

        return false;
    }

    public function hasWegweiserKommunePlaceMapping(Account $account, Place $place): bool
    {
        $wegweiserKommunePlaceMapping = $this->fetchWegweiserKommunePlaceMappingByPlace(account: $account, place: $place);

        if ($wegweiserKommunePlaceMapping === null) {
            return false;
        }

        if (
            $wegweiserKommunePlaceMapping->getWegweiserKommuneRegionFriendlyUrl() !== null
            && empty($wegweiserKommunePlaceMapping->getWegweiserKommuneRegionFriendlyUrl()) !== true
        ) {
            return true;
        }

        return false;
    }
}
