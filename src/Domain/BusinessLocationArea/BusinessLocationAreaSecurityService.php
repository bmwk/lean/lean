<?php

declare(strict_types=1);

namespace App\Domain\BusinessLocationArea;

use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\AccountUser\AccountUserRole;
use App\Domain\Entity\AccountUser\AccountUserType;
use App\Domain\Entity\BusinessLocationArea;
use Symfony\Bundle\SecurityBundle\Security;

class BusinessLocationAreaSecurityService
{
    public function __construct(
        private readonly Security $security
    ) {
    }

    public function canViewBusinessLocationArea(BusinessLocationArea $businessLocationArea, AccountUSer $accountUser): bool
    {
        $account = $accountUser->getAccount();

        if (
            $businessLocationArea->getAccount() === $account
            && $accountUser->getAccountUserType() === AccountUserType::INTERNAL
        ) {
            return true;
        }

        return false;
    }

    public function canEditBusinessLocationArea(BusinessLocationArea $businessLocationArea, AccountUser $accountUser): bool
    {
        $account = $accountUser->getAccount();

        if (
            $businessLocationArea->getAccount() === $account
            && $this->security->isGranted(attributes: AccountUserRole::ROLE_ACCOUNT_ADMIN->value) === true
        ) {
            return true;
        }

        return false;
    }

    public function canDeleteBusinessLocationArea(BusinessLocationArea $businessLocationArea, AccountUser $accountUser): bool
    {
        $account = $accountUser->getAccount();

        if (
            $businessLocationArea->getAccount() === $account
            && $this->security->isGranted(attributes: AccountUserRole::ROLE_ACCOUNT_ADMIN->value) === true
        ) {
            return true;
        }

        return false;
    }
}
