<?php

declare(strict_types=1);

namespace App\Domain\BusinessLocationArea;

use App\Domain\EarlyWarningSystem\JsonRepresentation\BusinessLocationAreaFactory;
use App\Domain\Entity\Account;
use App\Domain\Entity\BusinessLocationArea;
use App\Domain\Entity\GeolocationPoint;
use App\Domain\Entity\LocationCategory;
use App\TaodEarlyWarningSystem\TaodEarlyWarningSystemService;
use App\Repository\BusinessLocationAreaRepository;

class BusinessLocationAreaService
{
    public function __construct(
        private readonly TaodEarlyWarningSystemService $taodEarlyWarningSystemService,
        private readonly BusinessLocationAreaRepository $businessLocationAreaRepository
    ) {
    }

    /**
     * @return BusinessLocationArea[]
     */
    public function fetchBusinessLocationAreasByAccount(Account $account): array
    {
        return $this->businessLocationAreaRepository->findBy(criteria: ['account' => $account]);
    }

    public function fetchBusinessLocationAreaById(Account $account, int $id): ?BusinessLocationArea
    {
        return $this->businessLocationAreaRepository->findOneBy(criteria: [
            'account' => $account,
            'id'      => $id,
        ]);
    }

    public function calculateLocationCategoryByBusinessLocationArea(Account $account, GeolocationPoint $geolocationPoint): ?LocationCategory
    {
        $businessLocationArea = $this->businessLocationAreaRepository->findOneByPolygonContainingPoint(account: $account, geolocationPoint: $geolocationPoint);

        return $businessLocationArea?->getLocationCategory();
    }

    public function pushAllBusinessLocationAreasIntoEarlyWarningSystem(): void
    {
        $this->taodEarlyWarningSystemService->truncateBusinessLocationAreaTable();

        $businessLocationAreaJsonRepresentations = [];

        foreach ($this->businessLocationAreaRepository->findAll() as $businessLocationArea) {
            $businessLocationAreaJsonRepresentations[] = BusinessLocationAreaFactory::createFromBusinessLocationArea(businessLocationArea: $businessLocationArea);
        }

        $this->taodEarlyWarningSystemService->createAndSaveBusinessLocationAreasFromBusinessLocationAreaJsonRepresentations(
            businessLocationAreaJsonRepresentations: $businessLocationAreaJsonRepresentations
        );
    }
}
