<?php

declare(strict_types=1);

namespace App\Domain\BusinessLocationArea;

use App\Domain\Entity\Account;
use App\Domain\Entity\BusinessLocationArea;
use App\Domain\Location\LocationDeletionService;
use App\Repository\BusinessLocationAreaRepository;
use Doctrine\ORM\EntityManagerInterface;

class BusinessLocationAreaDeletionService
{
    public function __construct(
        private readonly BusinessLocationAreaRepository $businessLocationAreaRepository,
        private readonly LocationDeletionService $locationDeletionService,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function deleteBusinessLocationArea(BusinessLocationArea $businessLocationArea): void
    {
        $this->hardDeleteBusinessLocationArea(businessLocationArea: $businessLocationArea);
    }

    public function deleteBusinessLocationAreasByAccount(Account $account): void
    {
        $this->hardDeleteBusinessLocationAreasByAccount(account: $account);
    }

    private function hardDeleteBusinessLocationArea(BusinessLocationArea $businessLocationArea): void
    {
        $this->entityManager->remove(entity: $businessLocationArea);
        $this->entityManager->flush();

        $this->locationDeletionService->deleteGeolocationPolygon(geolocationPolygon: $businessLocationArea->getGeolocationPolygon());
    }

    private function hardDeleteBusinessLocationAreasByAccount(Account $account): void
    {
        $businessLocationAreas = $this->businessLocationAreaRepository->findBy(criteria: ['account' => $account]);

        foreach ($businessLocationAreas as $businessLocationArea) {
            $this->hardDeleteBusinessLocationArea(businessLocationArea: $businessLocationArea);
        }
    }
}
