<?php

declare(strict_types=1);

namespace App\Domain\Filesystem;

use App\Domain\Filesystem\Exception\IsNotDirectoryException;
use App\Domain\Filesystem\Exception\IsNotReadableException;
use App\Domain\Filesystem\Exception\IsNotWritableException;
use Symfony\Component\Filesystem\Filesystem;

class FilesystemService
{
    public function __construct(
        private readonly Filesystem $filesystem
    ) {
    }

    public function checkIsDirectoryAndPermissions(\SplFileInfo $targetDirectory): void
    {
        if ($targetDirectory->isDir() !== true) {
            throw new IsNotDirectoryException(message: 'The target directory: \'' . $targetDirectory->getRealPath() . '\' is not a directory');
        }

        if ($targetDirectory->isReadable() !== true) {
            throw new IsNotReadableException(message: 'The target directory: \'' . $targetDirectory->getRealPath() . '\' is not readable');
        }

        if ($targetDirectory->isWritable() !== true) {
            throw new IsNotWritableException(message: 'The target directory: \'' . $targetDirectory->getRealPath() . '\' is not writable');
        }
    }

    public function makeDirectory(string $directoryName, \SplFileInfo $targetDirectory): \SplFileInfo
    {
        $this->checkIsDirectoryAndPermissions(targetDirectory: $targetDirectory);

        $directoryPath = $targetDirectory->getRealPath() . '/' . $directoryName;

        if ($this->filesystem->exists(files: $directoryPath) === false) {
            $this->filesystem->mkdir(dirs: $directoryPath);
        }

        return new \SplFileInfo(filename: $directoryPath);
    }

    public function copy(string $originFile, string $targetFile, bool $overwriteNewerFiles = false): void
    {
        $this->filesystem->copy(originFile: $originFile, targetFile: $targetFile, overwriteNewerFiles: $overwriteNewerFiles);
    }

    public function mirror(string $originDir, string $targetDir, \Traversable $iterator = null, array $options = []): void
    {
        $this->filesystem->mirror(originDir: $originDir, targetDir: $targetDir, iterator: $iterator, options: $options);
    }

    public function remove(string|iterable $files): void
    {
        $this->filesystem->remove(files: $files);
    }
}
