<?php

declare(strict_types = 1);

namespace App\Domain\Filesystem\Exception;

class IsNotFileException extends \RuntimeException
{

}
