<?php

declare(strict_types=1);

namespace App\FtpUser;

use Doctrine\DBAL\Connection;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class FtpUserService
{
    public function __construct(
        private readonly Connection $proftpdConnection,
        private readonly int $ftpUserUid,
        private readonly int $ftpUserGid,
        private readonly string $ftpUserShell,
        private readonly string $ftpUserHomedirBasePath
    ) {
    }

    public function createFtpUser(string $userid, string $plainPassword, string $homedirPath): void
    {
        $this->proftpdConnection->insert(
            table: 'users',
            data: [
                'userid'  => $userid,
                'passwd'  => self::createOpenSslPassword(plainPassword: $plainPassword),
                'uid'     => $this->ftpUserUid,
                'gid'     => $this->ftpUserGid,
                'homedir' => $this->ftpUserHomedirBasePath . '/' . $homedirPath,
                'shell'   => $this->ftpUserShell,
            ]
        );
    }

    public function deleteFtpUser(string $userid): void
    {
        $this->proftpdConnection->delete(
            table: 'users',
            criteria: [
                'userid' => $userid,
            ]
        );
    }

    public function changeFtpUserPassword(string $userid, string $plainPassword): void
    {
        $this->proftpdConnection->update(
            table: 'users',
            data: [
                'passwd' => self::createOpenSslPassword(plainPassword: $plainPassword),
            ],
            criteria: [
                'userid' => $userid,
            ]
        );
    }

    public function checkFtpUserExists(string $userid): bool
    {
        return $this->proftpdConnection->fetchOne(query: 'SELECT userid FROM users WHERE userid = ?', params: [$userid]) !== false;
    }

    private static function createOpenSslPassword(string $plainPassword): string
    {
        $process = new Process(command: ['openssl', 'passwd', '-1', $plainPassword]);

        $process->run();

        if ($process->isSuccessful() === false) {
            throw new ProcessFailedException(process: $process);
        }

        return rtrim(string: $process->getOutput());
    }
}
