<?php

declare(strict_types=1);

namespace App\SessionStorage;

use Symfony\Component\HttpFoundation\RequestStack;

class SessionStorageService
{
    public function __construct(
        private readonly RequestStack $requestStack
    ) {
    }

    public function fetchObject(string $dataClassName, string $sessionKeyName): ?object
    {
        $session = $this->requestStack->getSession();

        $namespaceSessionKeyName = $dataClassName . '.' . $sessionKeyName;

        if (
            $session->has(name: $namespaceSessionKeyName) === true
            && ($session->get(name: $namespaceSessionKeyName) instanceof $dataClassName) === true
        ) {
            return $session->get(name: $namespaceSessionKeyName);
        }

        return null;
    }

    public function storeObject(object $object, string $sessionKeyName): void
    {
        $session = $this->requestStack->getSession();

        $namespaceSessionKeyName = get_class(object: $object) . '.' . $sessionKeyName;

        $session->set(name: $namespaceSessionKeyName, value: $object);
    }

    public function removeObject(string $dataClassName, string $sessionKeyName): void
    {
        $session = $this->requestStack->getSession();

        $session->remove(name: $dataClassName . '.' . $sessionKeyName);
    }
}
