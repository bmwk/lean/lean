<?php

declare(strict_types=1);

namespace App\Command\Location;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'app:location:import')]
class LocationImportCommand extends Command
{
    public function __construct(
        private readonly string $placeDumpLocation,
        private readonly string $parentPlaceChildPlaceDumpLocation,
        private readonly string $geolocationPointDumpLocation,
        private readonly EntityManagerInterface $entityManager
    ) {
        parent::__construct();
    }

    public function run(InputInterface $input, OutputInterface $output): int
    {
        ini_set(option: 'memory_limit', value: '512M');

        $geolocationPointContent = file_get_contents(filename: $this->geolocationPointDumpLocation);
        $placeDump = file_get_contents(filename: $this->placeDumpLocation);
        $parentPlaceChildPlace = file_get_contents(filename: $this->parentPlaceChildPlaceDumpLocation);

        if ($geolocationPointContent === false) {
            throw new \RuntimeException(message: 'A import file is missing or no read rights');
        }

        if ($placeDump === false) {
            throw new \RuntimeException(message: 'A import file is missing or no read rights');
        }

        if ($parentPlaceChildPlace === false) {
            throw new \RuntimeException(message: 'A import file is missing or no read rights');
        }

        $this->entityManager->beginTransaction();
        $this->executeFileContent(fileContent: $geolocationPointContent);
        $this->executeFileContent(fileContent: $placeDump);
        $this->executeFileContent(fileContent: $parentPlaceChildPlace);
        $this->entityManager->commit();

        return Command::SUCCESS;
    }

    private function executeFileContent(string $fileContent): void
    {
        if (empty($fileContent) === true) {
            throw new \RuntimeException(message: 'A import file has no content');
        }

        $this->entityManager->getConnection()->executeStatement($fileContent);
    }
}
