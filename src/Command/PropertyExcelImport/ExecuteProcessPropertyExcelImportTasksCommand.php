<?php

declare(strict_types=1);

namespace App\Command\PropertyExcelImport;

use App\Domain\Property\ExcelImport\ExcelImportService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'app:process-property-excel-import-tasks:execute')]
class ExecuteProcessPropertyExcelImportTasksCommand extends Command
{
    public function __construct(
        private readonly ExcelImportService $excelImportService
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->addArgument(name: 'amountOfTasksToProcess', mode: InputArgument::REQUIRED);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $amountOfTasksToProcess = (int)$input->getArgument('amountOfTasksToProcess');

        $this->excelImportService->executeProcessPropertyExcelImportTasks(
            amountOfTasksToProcess: $amountOfTasksToProcess,
            dryRun: false
        );

        return Command::SUCCESS;
    }
}
