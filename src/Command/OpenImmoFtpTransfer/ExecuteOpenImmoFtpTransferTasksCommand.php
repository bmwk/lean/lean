<?php

declare(strict_types=1);

namespace App\Command\OpenImmoFtpTransfer;

use App\Domain\OpenImmoFtpTransfer\OpenImmoFtpTransferService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'app:open-immo-ftp-transfer-tasks:execute')]
class ExecuteOpenImmoFtpTransferTasksCommand extends Command
{
    public function __construct(
        private readonly OpenImmoFtpTransferService $openImmoFtpTransferService
    ) {
        parent::__construct();
    }

    public function run(InputInterface $input, OutputInterface $output): int
    {
        $this->openImmoFtpTransferService->executeOpenImmoFtpTransferTasks();

        return Command::SUCCESS;
    }
}
