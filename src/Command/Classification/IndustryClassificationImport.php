<?php

declare(strict_types=1);

namespace App\Command\Classification;

use App\Domain\Classification\ClassificationImportService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\SerializerInterface;

#[AsCommand(name: 'app:classification:import:industry-classification')]
class IndustryClassificationImport extends Command
{
    public function __construct(
        private readonly ClassificationImportService $classificationImportService,
        private readonly string $industryClassificationFilePath,
        private readonly SerializerInterface $serializer
    ) {
        parent::__construct();
    }

    public function run(InputInterface $input, OutputInterface $output): int
    {
        $industryClassificationCsvLines = $this->serializer->decode(
            data: file_get_contents(filename: $this->industryClassificationFilePath),
            format: 'csv',
            context: [
                CsvEncoder::ENCLOSURE_KEY => '"',
                CsvEncoder::DELIMITER_KEY => ';',
            ]
        );

        $this->classificationImportService->importIndustryClassificationsFromCsvIntoDatabase(industryClassificationCsvLines: $industryClassificationCsvLines);

        return Command::SUCCESS;
    }
}
