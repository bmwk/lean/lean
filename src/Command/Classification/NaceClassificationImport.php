<?php

declare(strict_types=1);

namespace App\Command\Classification;

use App\Domain\Classification\ClassificationImportService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\SerializerInterface;

#[AsCommand(name: 'app:classification:import:nace')]
class NaceClassificationImport extends Command
{
    public function __construct(
        private readonly ClassificationImportService $classificationImportService,
        private readonly string $naceClassificationFilePath,
        private readonly SerializerInterface $serializer

    ) {
        parent::__construct();
    }

    public function run(InputInterface $input, OutputInterface $output): int
    {
        $naceClassificationCsvLines = $this->serializer->decode(
            data: file_get_contents(filename: $this->naceClassificationFilePath),
            format: 'csv',
            context: [
                CsvEncoder::ENCLOSURE_KEY => '"',
                CsvEncoder::DELIMITER_KEY => ';',
            ]
        );

        $this->classificationImportService->importNaceClassificationsFromCsvToDatabase(naceClassificationCsvLines: $naceClassificationCsvLines);

        return Command::SUCCESS;
    }
}
