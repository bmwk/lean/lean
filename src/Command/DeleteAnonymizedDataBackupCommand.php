<?php

declare(strict_types=1);

namespace App\Command;

use App\Repository\AnonymizedDataBackupRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'app:anonymized-data-backup:delete')]
class DeleteAnonymizedDataBackupCommand extends Command
{
    public function __construct(
        private readonly int $anonymizeBackupTimeTillDelete,
        private readonly AnonymizedDataBackupRepository $anonymizedDataBackupRepository,
        private readonly EntityManagerInterface $entityManager
    ) {
        parent::__construct();
    }

    public function run(InputInterface $input, OutputInterface $output): int
    {
        $anonymizedDataBackups = $this->anonymizedDataBackupRepository->findByCreatedAtOlderThanTimeTillDelete(
            timeTillDelete: $this->anonymizeBackupTimeTillDelete
        );

        $this->entityManager->beginTransaction();

        foreach ($anonymizedDataBackups as $anonymizedDataBackup) {
            $this->entityManager->remove($anonymizedDataBackup);
            $this->entityManager->flush();
        }
        $this->entityManager->commit();

        return Command::SUCCESS;
    }
}
