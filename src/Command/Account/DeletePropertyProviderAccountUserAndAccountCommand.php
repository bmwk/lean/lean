<?php

declare(strict_types=1);

namespace App\Command\Account;

use App\Domain\Account\AccountDeletionService;
use App\Domain\Account\AccountUserService;
use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\AccountUser\AccountUserType;
use App\Domain\Entity\DeletionType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'app:account:account-user:providers:delete')]
class DeletePropertyProviderAccountUserAndAccountCommand extends Command
{
    public function __construct(
        private readonly AccountUserService $accountUserService,
        private readonly AccountDeletionService $accountDeletionService,
        private readonly int $timeTillHardDelete,
        private readonly EntityManagerInterface $entityManager,
    ) {
        parent::__construct();
    }

    public function run(InputInterface $input, OutputInterface $output): int
    {
        $propertyProviderAccountUsersToDelete = $this->accountUserService->fetchToBeDeletedAccountUsersByAccountUserTypesFromAllAccounts(
            accountUserTypes: [AccountUserType::PROPERTY_PROVIDER],
            timeTillHardDelete: $this->timeTillHardDelete
        );

        if (empty($propertyProviderAccountUsersToDelete) === true) {
            return Command::SUCCESS;
        }

        $this->entityManager->beginTransaction();

        foreach ($propertyProviderAccountUsersToDelete as $propertyProviderAccountUser) {
            $this->deleteProviderAccount(propertyProviderAccountUser: $propertyProviderAccountUser);
        }

        $this->entityManager->commit();

        return Command::SUCCESS;
    }

    private function deleteProviderAccount(AccountUser $propertyProviderAccountUser): void
    {
        $this->accountDeletionService->deleteAccount(
            account: $propertyProviderAccountUser->getAccount(),
            deletionType: DeletionType::HARD
        );
    }
}
