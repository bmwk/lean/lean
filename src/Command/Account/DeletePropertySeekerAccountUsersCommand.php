<?php

declare(strict_types=1);

namespace App\Command\Account;

use App\Domain\Account\AccountUserDeletionService;
use App\Domain\Account\AccountUserService;
use App\Domain\Entity\AccountUser\AccountUserType;
use App\Domain\Entity\DeletionType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'app:account:account-user:seekers:delete')]
class DeletePropertySeekerAccountUsersCommand extends Command
{
    public function __construct(
        private readonly AccountUserDeletionService $accountUserDeletionService,
        private readonly AccountUserService $accountUserService,
        private readonly int $timeTillHardDelete,
        private readonly EntityManagerInterface $entityManager
    ) {
        parent::__construct();
    }

    public function run(InputInterface $input, OutputInterface $output): int
    {
        $accountUsersToDelete = $this->accountUserService->fetchToBeDeletedAccountUsersByAccountUserTypesFromAllAccounts(
            accountUserTypes: [AccountUserType::PROPERTY_SEEKER],
            timeTillHardDelete: $this->timeTillHardDelete
        );

        if (empty($accountUsersToDelete) === true) {
            return Command::SUCCESS;
        }

        $this->entityManager->beginTransaction();

        foreach ($accountUsersToDelete as $accountUser) {
            $this->accountUserDeletionService->deleteAccountUser(accountUser: $accountUser, deletionType: DeletionType::HARD);
        }

        $this->entityManager->commit();

        return Command::SUCCESS;
    }
}
