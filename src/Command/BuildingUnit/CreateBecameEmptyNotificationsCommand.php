<?php

declare(strict_types=1);

namespace App\Command\BuildingUnit;

use App\Domain\Account\AccountService;
use App\Domain\Property\BuildingUnitNotificationService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'app:building-unit:create-became-empty-notifications')]
class CreateBecameEmptyNotificationsCommand extends Command
{
    public function __construct(
        private readonly AccountService $accountService,
        private readonly BuildingUnitNotificationService $buildingUnitNotificationService
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        foreach ($this->accountService->fetchAllMainAccounts() as $account) {
            $this->buildingUnitNotificationService->createBuildingUnitBecameEmptyNotificationsByAccount(account: $account);
        }

        return Command::SUCCESS;
    }
}
