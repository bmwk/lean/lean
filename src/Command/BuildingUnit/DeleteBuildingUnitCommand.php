<?php

declare(strict_types=1);

namespace App\Command\BuildingUnit;

use App\Domain\Entity\DeletionType;
use App\Domain\Property\BuildingUnitDeletionService;
use App\Domain\Property\BuildingUnitService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'app:building-unit:building-units:delete')]
class DeleteBuildingUnitCommand extends Command
{
    public function __construct(
        private readonly BuildingUnitDeletionService $buildingUnitDeletionService,
        private readonly BuildingUnitService $buildingUnitService,
        private readonly int $timeTillHardDelete,
        private readonly EntityManagerInterface $entityManager
    ) {
        parent::__construct();
    }

    public function run(InputInterface $input, OutputInterface $output): int
    {
        $buildingUnits = $this->buildingUnitService->fetchToBeDeletedBuildingUnitsFromAllAccounts(
            timeTillHardDelete: $this->timeTillHardDelete
        );

        $this->entityManager->beginTransaction();

        foreach ($buildingUnits as $buildingUnit) {
            $this->buildingUnitDeletionService->deleteBuildingUnit(buildingUnit: $buildingUnit, deletionType: DeletionType::HARD);
        }

        $this->entityManager->commit();

        return Command::SUCCESS;
    }
}
