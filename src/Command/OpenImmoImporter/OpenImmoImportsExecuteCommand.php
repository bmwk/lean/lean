<?php

declare(strict_types=1);

namespace App\Command\OpenImmoImporter;

use App\Domain\OpenImmoImporter\OpenImmoImportService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'app:open-immo-importer:open-immo-imports:execute')]
class OpenImmoImportsExecuteCommand extends Command
{
    public function __construct(
        private readonly OpenImmoImportService $openImmoImporterService
    ) {
        parent::__construct();
    }

    public function run(InputInterface $input, OutputInterface $output): int
    {
        $this->openImmoImporterService->executeOpenImmoImports();

        return Command::SUCCESS;
    }
}
