<?php

declare(strict_types=1);

namespace App\Command\OpenImmoImporter;

use App\Domain\OpenImmoImporter\OpenImmoImportService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'app:open-immo-importer:open-immo-import:execute')]
class OpenImmoImportExecuteCommand extends Command
{
    public function __construct(
        private readonly OpenImmoImportService $openImmoImporterService
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->addArgument(name: 'openImmoImportId', mode: InputArgument::REQUIRED);
    }

    public function run(InputInterface $input, OutputInterface $output): int
    {
        $openImmoImportId = (int) $input->getArgument(name: 'openImmoImportId');

        $openImmoImport =  $this->openImmoImporterService->fetchOpenImmoImportById(id: $openImmoImportId);

        if ($openImmoImport === null) {
            return Command::FAILURE;
        }

        $this->openImmoImporterService->executeOpenImmoImport(openImmoImport: $openImmoImport);

        return Command::SUCCESS;
    }
}
