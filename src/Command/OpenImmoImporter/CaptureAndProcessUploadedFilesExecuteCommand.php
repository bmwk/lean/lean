<?php

declare(strict_types=1);

namespace App\Command\OpenImmoImporter;

use App\Domain\OpenImmoImporter\OpenImmoUploadCaptureService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'app:open-immo-importer:capture-and-process-uploaded-files:execute')]
class CaptureAndProcessUploadedFilesExecuteCommand extends Command
{
    public function __construct(
        private readonly OpenImmoUploadCaptureService $openImmoUploadCaptureService
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->openImmoUploadCaptureService->captureAndProcessUploadedFilesFromAllAccounts();

        return Command::SUCCESS;
    }
}
