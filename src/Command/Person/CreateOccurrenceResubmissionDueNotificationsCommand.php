<?php

declare(strict_types=1);

namespace App\Command\Person;

use App\Domain\Account\AccountService;
use App\Domain\Person\PersonService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'app:occurrence:create-occurrence-resubmission-due-notifications')]
class CreateOccurrenceResubmissionDueNotificationsCommand extends Command
{
    public function __construct(
        private readonly AccountService $accountService,
        private readonly PersonService $personService
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        foreach ($this->accountService->fetchAllMainAccounts() as $account) {
            $this->personService->createOccurrenceResubmissionDueNotificationsByAccount(account: $account);
        }

        return Command::SUCCESS;
    }
}
