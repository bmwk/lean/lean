<?php

declare(strict_types=1);

namespace App\Command;

use App\Domain\Entity\Account;
use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\AccountUser\AccountUserRole;
use App\Domain\Entity\AccountUser\AccountUserType;
use App\Repository\AccountRepository;
use App\Repository\AccountUser\AccountUserRepository;
use App\Repository\PlaceRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

#[AsCommand(name: 'app:account:create')]
class AccountCreateCommand extends Command
{
    public function __construct(
        private readonly AccountRepository $accountRepository,
        private readonly AccountUserRepository $accountUserRepository,
        private readonly PlaceRepository $placeRepository,
        private readonly EntityManagerInterface $entityManager,
        private readonly UserPasswordHasherInterface $passwordHasher
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        if ($this->anyAccountExist() === true) {
            $output->writeLn(messages: '<error>Es ist bereits ein Account eingerichtet.</error>');

            return Command::FAILURE;
        }

        try {
            $this->entityManager->beginTransaction();

            $account = $this->createAccount(input: $input, output: $output);
            $accountUser = $this->createAccountUser(input: $input, output: $output);

            $accountUser->setAccount($account);

            $this->entityManager->persist($account);
            $this->entityManager->persist($accountUser);
            $this->entityManager->flush();

            $this->entityManager->commit();
            $this->entityManager->clear();

            $output->writeln(messages: '<info>Account und AccountUser wurden angelegt.</info>');
        } catch (\Exception $e) {
            $this->entityManager->rollback();
            $this->entityManager->clear();

            $output->writeLn(sprintf('<error>%s</error>', $e->getMessage()));

            return Command::FAILURE;
        }

        return Command::SUCCESS;
    }

    private function createAccount(InputInterface $input, OutputInterface $output): Account
    {
        $output->writeln(messages: '<comment>Account</comment>');

        $helper = $this->getHelper('question');

        $accountName = $helper->ask(
            input: $input,
            output: $output,
            question: new Question(question: '<question>Name des Accounts:</question> ')
        );

        $accountEmail = $helper->ask(
            input: $input,
            output: $output,
            question: new Question(question: '<question>Email des accounts:</question> ')
        );

        if (filter_var(value: $accountEmail, filter: FILTER_VALIDATE_EMAIL) === false) {
            throw new \Exception(sprintf(format: '%s ist keine valide E-Mail-Adresse.', values: $accountEmail));
        }

        $appHostname = $helper->ask(
            input: $input,
            output: $output,
            question: new Question(question: '<question>App-Hostname:</question> ')
        );

        $placeId = (int)$helper->ask(
            input: $input,
            output: $output,
            question: new Question(question: '<question>PlaceId:</question> ')
        );

        $place = $this->placeRepository->find(id: $placeId);

        if ($place === null) {
            throw new \Exception(sprintf(format: 'Place mit Id %s existiert nicht.', values: $placeId));
        }

        $account = new Account();

        $account
            ->setName($accountName)
            ->setEmail($accountEmail)
            ->setAppHostname($appHostname)
            ->setAssignedPlace($place)
            ->setEnabled(true)
            ->setDeleted(false)
            ->setAnonymized(false);

        return $account;
    }

    private function createAccountUser(InputInterface $input, OutputInterface $output): AccountUser
    {
        $output->writeln(messages: '<comment>Account User</comment> ');

        $helper = $this->getHelper(name: 'question');

        $accountUserFullName = $helper->ask(
            input: $input,
            output: $output,
            question: new Question(question: '<question>Vollständiger Name des Users:</question> ')
        );

        $accountUserEmail = $helper->ask(
            input: $input,
            output: $output,
            question: new Question(question: '<question>Email des Users:</question> ')
        );

        if (filter_var(value: $accountUserEmail, filter: FILTER_VALIDATE_EMAIL) === false) {
            throw new \Exception(sprintf(format: '%s ist keine valide E-Mail-Adresse.', values: $accountUserEmail));
        }

        $accountUserUsername = $helper->ask(
            input: $input,
            output: $output,
            question: new Question(question: '<question>Benutzername:</question> ')
        );

        if ($this->accountUserByUsernameExist(username: $accountUserUsername) === true) {
            throw new \Exception(sprintf(format: 'Nutzer mit Nutzername %s existiert bereits.', values: $accountUserUsername));
        }

        $accountUserPassword = $helper->ask(
            input: $input,
            output: $output,
            question: new Question(question: '<question>Passwort:</question> ')
        );

        $accountUser = new AccountUser();

        $accountUser
            ->setFullName($accountUserFullName)
            ->setEmail($accountUserEmail)
            ->setUsername($accountUserUsername)
            ->setAccountUserType(AccountUserType::INTERNAL)
            ->setAnonymized(false)
            ->setDeleted(false)
            ->setEnabled(true)
            ->setRoles([AccountUserRole::ROLE_ACCOUNT_ADMIN->value])
            ->setPassword($this->passwordHasher->hashPassword(user: $accountUser, plainPassword: $accountUserPassword));

        return $accountUser;
    }

    private function accountUserByUsernameExist(string $username): bool
    {
        return $this->accountUserRepository->findOneBy(criteria: ['username' => $username]) !== null;
    }

    private function anyAccountExist(): bool
    {
        return $this->accountRepository->count(criteria: []) > 0;
    }
}
