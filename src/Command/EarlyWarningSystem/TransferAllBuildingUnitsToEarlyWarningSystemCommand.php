<?php

declare(strict_types=1);

namespace App\Command\EarlyWarningSystem;

use App\Domain\Property\BuildingUnitService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'app:building-units-to-early-warning-system:transfer-all')]
class TransferAllBuildingUnitsToEarlyWarningSystemCommand extends Command
{
    public function __construct(
        private readonly BuildingUnitService $buildingUnitService
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->buildingUnitService->pushAllBuildingUnitsIntoEarlyWarningSystem();

        return Command::SUCCESS;
    }
}
