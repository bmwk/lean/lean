<?php

declare(strict_types=1);

namespace App\Command\EarlyWarningSystem;

use App\Domain\BusinessLocationArea\BusinessLocationAreaService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'app:business-location-areas-to-early-warning-system:transfer-all')]
class TransferAllBusinessLocationAreasToEarlyWarningSystemCommand extends Command
{
    public function __construct(
        private readonly BusinessLocationAreaService $businessLocationAreaService
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->businessLocationAreaService->pushAllBusinessLocationAreasIntoEarlyWarningSystem();

        return Command::SUCCESS;
    }
}
