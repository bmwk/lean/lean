<?php

declare(strict_types=1);

namespace App\Command\LookingForPropertyRequest;

use App\Domain\Account\AccountService;
use App\Domain\LookingForPropertyRequest\LookingForPropertyRequestService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'app:looking-for-property-request:create-no-match-found-notifications')]
class CreateNoMatchFoundNotificationsCommand extends Command
{
    private const NO_MATCH_FOUND_IN_DAYS = 30;
    private const INTERVAL_LENGTH = 30;

    public function __construct(
        private readonly AccountService $accountService,
        private readonly LookingForPropertyRequestService $lookingForPropertyRequestService
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        foreach ($this->accountService->fetchAllMainAccounts() as $account) {
            $this->lookingForPropertyRequestService->createNoMatchFoundNotificationsByAccount(
                account: $account,
                noMatchFoundInDays: self::NO_MATCH_FOUND_IN_DAYS,
                intervalLength: self::INTERVAL_LENGTH
            );
        }

        return Command::SUCCESS;
    }
}
