<?php

declare(strict_types=1);

namespace App\Command\LookingForPropertyRequest;

use App\Domain\LookingForPropertyRequest\Matching\MatchingService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'app:matching-task:execute')]
class ExecuteMatchingTaskCommand extends Command
{
    public function __construct(
        private readonly MatchingService $matchingService
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->addArgument(name: 'matchingTaskId', mode: InputArgument::REQUIRED, description: 'Id of the matching task');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $matchingTaskId = (int)$input->getArgument('matchingTaskId');

        $this->matchingService->executeMatchingTaskById(
            matchingTaskId: $matchingTaskId,
            dryRun: false
        );

        return Command::SUCCESS;
    }
}
