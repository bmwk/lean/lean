<?php

declare(strict_types=1);

namespace App\Command\LookingForPropertyRequest;

use App\Domain\LookingForPropertyRequest\Matching\MatchingService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'app:matching-tasks:execute')]
class ExecuteMatchingTasksCommand extends Command
{
    public function __construct(
        private readonly MatchingService $matchingService
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->matchingService->executeMatchingTasks();

        return Command::SUCCESS;
    }
}
