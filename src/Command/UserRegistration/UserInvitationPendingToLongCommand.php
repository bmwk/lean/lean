<?php

declare(strict_types=1);

namespace App\Command\UserRegistration;

use App\Domain\UserRegistration\UserInvitationEmailService;
use App\Domain\UserRegistration\UserInvitationService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'app:user-invitation-pending-to-long')]
class UserInvitationPendingToLongCommand extends Command
{
    public function __construct(
        private readonly UserInvitationService $userInvitationService,
        private readonly UserInvitationEmailService $userInvitationEmailService
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        foreach ($this->userInvitationService->fetchOverdueUserInvitations() as $userInvitation) {
            $this->userInvitationEmailService->sendUserInvitationOverdueEmail(userInvitation: $userInvitation);
        }

        return Command::SUCCESS;
    }
}
