<?php

declare(strict_types=1);

namespace App\Command\UserRegistration;

use App\Domain\UserRegistration\UserRegistrationDeletionService;
use App\Domain\UserRegistration\UserRegistrationEmailService;
use App\Domain\UserRegistration\UserRegistrationService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'app:user-registration-remove-overdue')]
class UserRegistrationRemoveOverdueCommand extends Command
{
    public function __construct(
        private readonly UserRegistrationService $userRegistrationService,
        private readonly UserRegistrationDeletionService $userRegistrationDeletionService,
        private readonly UserRegistrationEmailService $userRegistrationEmailService
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        foreach ($this->userRegistrationService->fetchExpiredInactiveUserRegistrationsFromAllAccounts() as $userRegistration) {
            $this->userRegistrationDeletionService->deleteUserRegistration(userRegistration: $userRegistration);

            $this->userRegistrationEmailService->sendUserRegistrationDeletedEmail(userRegistration: $userRegistration);
        }

        return Command::SUCCESS;
    }
}
