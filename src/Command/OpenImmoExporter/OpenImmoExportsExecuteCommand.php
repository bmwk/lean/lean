<?php

declare(strict_types=1);

namespace App\Command\OpenImmoExporter;

use App\Domain\OpenImmoExporter\OpenImmoExportService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'app:open-immo-exporter:open-immo-exports:execute')]
class OpenImmoExportsExecuteCommand extends Command
{
    public function __construct(
        private readonly OpenImmoExportService $openImmoExporterService,
    ) {
        parent::__construct();
    }

    public function run(InputInterface $input, OutputInterface $output): int
    {
        $this->openImmoExporterService->executeOpenImmoExports();

        return Command::SUCCESS;
    }
}
