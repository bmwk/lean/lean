<?php

declare(strict_types=1);

namespace App\Command\OpenImmoExporter;

use App\Domain\OpenImmoExporter\OpenImmoExportService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'app:open-immo-exporter:open-immo-export:execute')]
class OpenImmoExportExecuteCommand extends Command
{
    public function __construct(
        private readonly OpenImmoExportService $openImmoExporterService
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->addArgument(name: 'openImmoExportId', mode: InputArgument::REQUIRED);
    }

    public function run(InputInterface $input, OutputInterface $output): int
    {
        $openImmoExportId = (int) $input->getArgument(name: 'openImmoExportId');

        $openImmoExport = $this->openImmoExporterService->fetchOpenImmoExportById(id: $openImmoExportId);

        $this->openImmoExporterService->executeOpenImmoExport(openImmoExport: $openImmoExport);

        return Command::SUCCESS;
    }
}
