<?php

declare(strict_types=1);

namespace App\Command\SettlementConcept;

use App\Domain\Entity\DeletionType;
use App\Domain\SettlementConcept\SettlementConceptAccountUserDeletionService;
use App\Domain\SettlementConcept\SettlementConceptAccountUserService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'app:settlement-concepts:settlement-concept-account-users:delete')]
class DeleteSettlementConceptAccountUserCommand extends Command
{
    public function __construct(
        private readonly SettlementConceptAccountUserDeletionService $settlementConceptAccountUserDeletionService,
        private readonly SettlementConceptAccountUserService $settlementConceptAccountUserService,
        private readonly ?int $timeTillHardDelete
    ) {
        parent::__construct();
    }

    public function run(InputInterface $input, OutputInterface $output): int
    {
        $settlementConceptAccountUsers = $this->settlementConceptAccountUserService->fetchToBeDeletedSettlementConceptAccountUserFromAllAccounts(
            timeTillHardDelete: $this->timeTillHardDelete
        );

        foreach ($settlementConceptAccountUsers as $settlementConceptAccountUser) {
            $this->settlementConceptAccountUserDeletionService->deleteSettlementConceptAccountUser(
                settlementConceptAccountUser: $settlementConceptAccountUser,
                deletionType: DeletionType::HARD
            );
        }

        return Command::SUCCESS;
    }
}
