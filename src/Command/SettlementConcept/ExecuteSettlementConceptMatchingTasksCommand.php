<?php

declare(strict_types=1);

namespace App\Command\SettlementConcept;

use App\Domain\SettlementConcept\SettlementConceptMatchingService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'app:settlement-concept:matching-tasks:execute')]
class ExecuteSettlementConceptMatchingTasksCommand extends Command
{
    public function __construct(
        private readonly SettlementConceptMatchingService $settlementConceptMatchingService
    ) {
        parent::__construct();
    }

    public function run(InputInterface $input, OutputInterface $output): int
    {
        $this->settlementConceptMatchingService->executeSettlementConceptMatchingTasks();

        return Command::SUCCESS;
    }
}
