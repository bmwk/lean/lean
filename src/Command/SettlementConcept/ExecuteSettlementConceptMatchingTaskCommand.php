<?php

declare(strict_types=1);

namespace App\Command\SettlementConcept;

use App\Domain\SettlementConcept\SettlementConceptMatchingService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'app:settlement-concept:matching-task:execute')]
class ExecuteSettlementConceptMatchingTaskCommand extends Command
{
    public function __construct(
        private readonly SettlementConceptMatchingService $settlementConceptMatchingService
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->addArgument(name: 'taskId', mode: InputArgument::REQUIRED);
    }

    public function run(InputInterface $input, OutputInterface $output): int
    {
        $taskId = (int)$input->getArgument(name: 'taskId');
        $this->settlementConceptMatchingService->executeSettlementConceptMatchingTaskById(id: $taskId);

        return Command::SUCCESS;
    }
}
