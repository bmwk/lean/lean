<?php

declare(strict_types=1);

namespace App\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\Form\Exception\UnexpectedTypeException;

class UsToEuDecimalTransformer implements DataTransformerInterface
{
    public function transform($value): string
    {
        if ($value === null) {
            return '';
        }

        if (is_numeric($value) === false) {
            throw new UnexpectedTypeException($value, 'numeric');
        }

        return str_replace('.', ',', (string)number_format($value,2,null, ''));
    }

    public function reverseTransform($value): ?float
    {
        if ($value === '' || $value === null) {
            return null;
        }

        if (!is_string($value)) {
            throw new UnexpectedTypeException($value, 'string');
        }

        $value = str_replace(',', '.', $value);

        if (is_numeric($value) === false) {
            throw new TransformationFailedException('not a number');
        }

        return (float)$value;
    }
}
