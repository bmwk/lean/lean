<?php

declare(strict_types=1);

namespace App\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;

class Base64ToStringTransformer implements DataTransformerInterface
{
    public function transform($valueAsBase64): ?string
    {
        if ($valueAsBase64 === null) {
            return null;
        }

        return base64_decode($valueAsBase64);
    }


    public function reverseTransform($valueAsString): ?string
    {
        if ($valueAsString === null) {
            return null;
        }

        return base64_encode($valueAsString);
    }
}