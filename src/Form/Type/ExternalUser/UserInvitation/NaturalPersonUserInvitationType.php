<?php

declare(strict_types=1);

namespace App\Form\Type\ExternalUser\UserInvitation;

use Symfony\Component\Form\FormBuilderInterface;

class NaturalPersonUserInvitationType extends AbstractUserInvitationType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        parent::buildForm(builder: $builder, options: $options);
    }
}
