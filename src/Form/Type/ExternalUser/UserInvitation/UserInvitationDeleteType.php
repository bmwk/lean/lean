<?php

declare(strict_types=1);

namespace App\Form\Type\ExternalUser\UserInvitation;

use App\Form\Type\AbstractDeleteType;

class UserInvitationDeleteType extends AbstractDeleteType
{
}
