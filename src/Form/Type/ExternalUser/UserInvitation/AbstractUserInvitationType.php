<?php

declare(strict_types=1);

namespace App\Form\Type\ExternalUser\UserInvitation;

use App\Domain\Entity\Person\Salutation;
use App\Domain\Entity\UserRegistration\UserInvitation;
use App\Domain\Entity\UserRegistration\UserRegistrationType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EnumType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

abstract class AbstractUserInvitationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $disabled = $options['canEdit'] === false;

        $builder
            ->add(child: 'salutation', type: EnumType::class, options: [
                'label'        => 'Anrede',
                'required'     => false,
                'disabled'     => $disabled,
                'class'        => Salutation::class,
                'choice_label' => function (Salutation $salutation): string {
                    return $salutation->getName();
                },
            ])
            ->add(child: 'lastName', type: TextType::class, options: ['label' => 'Nachname', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'firstName', type: TextType::class, options: ['label' => 'Vorname', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'email', type: TextType::class, options: ['label' => 'E-Mail-Adresse', 'required' => true, 'disabled' => $disabled])
            ->add(child: 'userRegistrationType', type: EnumType::class, options: [
                'label'        => 'Einladen als',
                'required'     => true,
                'disabled'     => $disabled,
                'class'        => UserRegistrationType::class,
                'choices'      => $options['userRegistrationTypes'],
                'choice_label' => function (UserRegistrationType $userRegistrationType): string {
                    return $userRegistrationType->getName();
                },
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefaults(['data_class' => UserInvitation::class])
            ->setRequired(['userRegistrationTypes', 'canEdit'])
            ->setAllowedTypes(option: 'userRegistrationTypes', allowedTypes: ['array'])
            ->setAllowedTypes(option: 'canEdit', allowedTypes: ['bool']);
    }
}
