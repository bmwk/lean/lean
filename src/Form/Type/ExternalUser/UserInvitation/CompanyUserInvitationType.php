<?php

declare(strict_types=1);

namespace App\Form\Type\ExternalUser\UserInvitation;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class CompanyUserInvitationType extends AbstractUserInvitationType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        parent::buildForm(builder: $builder, options: $options);

        $disabled = $options['canEdit'] === false;

        $builder->add(child: 'companyName', type: TextType::class, options: ['label' => 'Firmenname', 'required' => true, 'disabled' => $disabled]);
    }
}
