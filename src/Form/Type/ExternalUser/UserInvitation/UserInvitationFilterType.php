<?php

declare(strict_types=1);

namespace App\Form\Type\ExternalUser\UserInvitation;

use App\Domain\Entity\Person\PersonType;
use App\Domain\Entity\UserRegistration\UserInvitationFilter;
use App\Form\Type\AbstractFilterType;
use App\SessionStorage\SessionStorageService;
use Symfony\Component\Form\Extension\Core\Type\EnumType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class UserInvitationFilterType extends AbstractFilterType
{
    public function __construct(
        SessionStorageService $sessionStorageService,
        RequestStack $requestStack,
        UrlGeneratorInterface $router
    ) {
        parent::__construct(
            sessionStorageService: $sessionStorageService,
            requestStack: $requestStack,
            router: $router
        );
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(child: 'personTypes', type: EnumType::class, options: [
                'required' => false,
                'class'    => PersonType::class,
                'choices'  => [
                    PersonType::NATURAL_PERSON,
                    PersonType::COMPANY,
                ],
                'choice_label' => function (PersonType $personType): string {
                    if ($personType === PersonType::COMPANY) {
                        return 'Unternehmen';
                    }

                    return $personType->getName();
                },
                'expanded' => true,
                'multiple' => true,
            ]);

        parent::buildForm(builder: $builder, options: $options);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => UserInvitationFilter::class]);

        parent::configureOptions(resolver: $resolver);
    }
}
