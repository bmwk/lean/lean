<?php

declare(strict_types=1);

namespace App\Form\Type\ExternalUser\UserInvitation;

use App\Form\Type\AbstractMassOperationType;

class UserInvitationMassOperationSendInvitationType extends AbstractMassOperationType
{
}
