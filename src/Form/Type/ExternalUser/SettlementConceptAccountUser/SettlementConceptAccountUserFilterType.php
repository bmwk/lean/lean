<?php

declare(strict_types=1);

namespace App\Form\Type\ExternalUser\SettlementConceptAccountUser;

use App\Domain\Entity\SettlementConcept\SettlementConceptAccountUserFilter;
use App\Form\Type\AbstractFilterType;
use App\SessionStorage\SessionStorageService;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class SettlementConceptAccountUserFilterType extends AbstractFilterType
{
    public function __construct(
        SessionStorageService $sessionStorageService,
        RequestStack $requestStack,
        UrlGeneratorInterface $router
    ) {
        parent::__construct(
            sessionStorageService: $sessionStorageService,
            requestStack: $requestStack,
            router: $router
        );
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add(child: 'enabledStates', type: ChoiceType::class, options: [
            'choices' => [
                'freigeschaltet'       => true,
                'nicht freigeschaltet' => false,
            ],
            'expanded' => true,
            'multiple' => true,
        ]);

        parent::buildForm(builder: $builder, options: $options);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => SettlementConceptAccountUserFilter::class]);

        parent::configureOptions(resolver: $resolver);
    }
}
