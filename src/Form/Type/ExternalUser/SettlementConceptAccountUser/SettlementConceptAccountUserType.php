<?php

declare(strict_types=1);

namespace App\Form\Type\ExternalUser\SettlementConceptAccountUser;

use App\Domain\Entity\SettlementConcept\SettlementConceptAccountUser;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SettlementConceptAccountUserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $disabled = $options['canEdit'] === false;

        $builder
            ->add(child: 'fullName', type: TextType::class, options: ['label' => 'Name', 'required' => true, 'disabled' => $disabled])
            ->add(child: 'email', type: EmailType::class, options: ['label' => 'E-Mail-Adresse', 'required' => true, 'disabled' => $disabled]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefaults(['data_class' => SettlementConceptAccountUser::class])
            ->setRequired(['canEdit'])
            ->setAllowedTypes(option: 'canEdit', allowedTypes: ['bool']);
    }
}
