<?php

declare(strict_types=1);

namespace App\Form\Type\ExternalUser\SettlementConceptAccountUser;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class SettlementConceptAccountUserResendApiTokenType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
    }
}
