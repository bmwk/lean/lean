<?php

declare(strict_types=1);

namespace App\Form\Type\ExternalUser\SettlementConceptAccountUser;

use App\Form\Type\AbstractDeleteType;

class SettlementConceptAccountUserDeleteType extends AbstractDeleteType
{
}
