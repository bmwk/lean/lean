<?php

declare(strict_types=1);

namespace App\Form\Type\ExternalUser\AccountUser;

use App\Domain\Entity\AccountUser\AccountUser;
use App\Form\Type\PersonManagement\Person\PersonEditType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AccountUserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $disabled = $options['canEdit'] === false;

        $builder
            ->add(child: 'fullName', type: TextType::class, options: ['label' => 'Name', 'required' => true, 'disabled' => $disabled])
            ->add(child: 'nameAbbreviation', type: TextType::class, options: ['label' => 'Namenskürzel', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'email', type: EmailType::class, options: ['label' => 'E-Mail-Adresse', 'required' => true, 'disabled' => $disabled])
            ->add(child: 'username', type: TextType::class, options: ['label' => 'Benutzername', 'required' => true, 'disabled' => $disabled]);

        $person = $builder->getData()->getPerson();

        if ($person !== null && $person->isAnonymized() === false) {
            $builder
                ->add(child: 'person', type: PersonEditType::class, options: ['data' => $person, 'canEdit' => $options['canEdit']])
                ->add(child: 'personType', type: HiddenType::class, options: ['data' => $person->getPersonType()->name, 'mapped' => false]);
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefaults(['data_class' => AccountUser::class])
            ->setRequired(['canEdit'])
            ->setAllowedTypes(option: 'canEdit', allowedTypes: ['bool']);
    }
}
