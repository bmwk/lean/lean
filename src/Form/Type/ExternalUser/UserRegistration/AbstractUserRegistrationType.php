<?php

declare(strict_types=1);

namespace App\Form\Type\ExternalUser\UserRegistration;

use App\Domain\Entity\Person\Salutation;
use App\Domain\Entity\UserRegistration\UserRegistration;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EnumType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

abstract class AbstractUserRegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $disabled = $options['canEdit'] === false;

        $builder
            ->add(child: 'salutation', type: EnumType::class, options: [
                'label'        => 'Anrede',
                'required'     => false,
                'disabled'     => $disabled,
                'class'        => Salutation::class,
                'choice_label' => function (Salutation $salutation): string {
                    return $salutation->getName();
                },
            ])
            ->add(child: 'lastName', type: TextType::class, options: ['label' => 'Nachname', 'disabled' => $disabled])
            ->add(child: 'firstName', type: TextType::class, options: ['label' => 'Vorname', 'disabled' => $disabled])
            ->add(child: 'streetName', type: TextType::class, options: ['label' => 'Straße', 'disabled' => $disabled])
            ->add(child: 'houseNumber', type: TextType::class, options: ['label' => 'Hausnummer', 'disabled' => $disabled])
            ->add(child: 'postalCode', type: TextType::class, options: ['label' => 'PLZ', 'disabled' => $disabled])
            ->add(child: 'placeName', type: TextType::class, options: ['label' => 'Ort', 'disabled' => $disabled])
            ->add(child: 'email', type: TextType::class, options: ['label' => 'E-Mail-Adresse', 'disabled' => $disabled])
            ->add(child: 'phoneNumber', type: TextType::class, options: ['label' => 'Telefonnummer', 'disabled' => $disabled])
            ->add(child: 'gtcPrivacy', type: CheckboxType::class, options: ['required' => true, 'label'=> 'AGB / Datenschutz', 'disabled' => $disabled]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefaults(['data_class' => UserRegistration::class])
            ->setRequired(['canEdit'])
            ->setAllowedTypes(option: 'canEdit', allowedTypes: ['bool']);
    }
}
