<?php

declare(strict_types=1);

namespace App\Form\Type\ExternalUser\UserRegistration;

use Symfony\Component\Form\FormBuilderInterface;

class NaturalPersonUserRegistrationType extends AbstractUserRegistrationType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        parent::buildForm(builder: $builder, options: $options);
    }
}
