<?php

declare(strict_types=1);

namespace App\Form\Type\ExternalUser\UserRegistration;

use App\Form\Type\AbstractMassOperationType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class UserRegistrationMassOperationDeleteType extends AbstractMassOperationType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        parent::buildForm(builder: $builder, options: $options);

        $builder->add(child: 'verificationCode', type: TextType::class, options: ['label' => 'Bestätigungscode', 'required' => true]);
    }
}
