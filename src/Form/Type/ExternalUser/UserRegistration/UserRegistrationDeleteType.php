<?php

declare(strict_types=1);

namespace App\Form\Type\ExternalUser\UserRegistration;

use App\Form\Type\AbstractDeleteType;

class UserRegistrationDeleteType extends AbstractDeleteType
{
}
