<?php

declare(strict_types=1);

namespace App\Form\Type\ExternalUser\UserRegistration;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class CompanyUserRegistrationType extends AbstractUserRegistrationType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $disabled = $options['canEdit'] === false;

        parent::buildForm(builder: $builder, options: $options);

        $builder->add(child: 'companyName', type: TextType::class, options: ['label' => 'Firmenname', 'disabled' => $disabled]);
    }
}
