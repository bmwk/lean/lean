<?php

declare(strict_types=1);

namespace App\Form\Type;

use App\SessionStorage\SessionStorageService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

abstract class AbstractSessionStoredType extends AbstractType
{
    protected function __construct(
        private readonly SessionStorageService $sessionStorageService
    ) {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $dataClassName = $options['dataClassName'];
        $sessionKeyName = $options['sessionKeyName'];

        $builder
            ->addEventListener(eventName: FormEvents::PRE_SET_DATA, listener: function (FormEvent $event) use ($dataClassName, $sessionKeyName): void {
                $this->setDataFromSession(dataClassName: $dataClassName, sessionKeyName: $sessionKeyName, formEvent: $event);
            })
            ->addEventListener(eventName: FormEvents::POST_SUBMIT, listener: function (FormEvent $event) use ($dataClassName, $sessionKeyName): void {
                $data = $event->getData();
                $form = $event->getForm();

                if ($form->isValid() === false) {
                    return;
                }

                $this->sessionStorageService->storeObject(object: $data, sessionKeyName: $sessionKeyName);
            });
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setRequired(['dataClassName', 'sessionKeyName'])
            ->setAllowedTypes(option: 'dataClassName', allowedTypes: ['string'])
            ->setAllowedTypes(option: 'sessionKeyName', allowedTypes: ['string']);
    }

    protected static function checkIfSessionStorageParametersAreAvailable(array $options): bool
    {
        if (
            isset($options['dataClassName']) === true
            && isset($options['sessionKeyName']) === true
            && empty($options['dataClassName']) === false
            && empty($options['sessionKeyName']) === false
        ) {
            return true;
        }

        return false;
    }

    private function setDataFromSession(string $dataClassName, string $sessionKeyName, FormEvent $formEvent): void
    {
        $object = $this->sessionStorageService->fetchObject(dataClassName: $dataClassName, sessionKeyName: $sessionKeyName);

        if ($object === null && $formEvent->getData() !== null) {
            return;
        }

        if ($object === null) {
            $object = new $dataClassName();
        }

        $formEvent->setData($object);
    }
}
