<?php

declare(strict_types=1);

namespace App\Form\Type\Configuration\PropertyVacancyReporter;

use App\Domain\Entity\PropertyVacancyReporter\PropertyVacancyReporterConfiguration;
use App\Domain\Entity\PropertyVacancyReporter\PropertyInformationFormField;
use App\Domain\Entity\PropertyVacancyReporter\PropertyLocationFormField;
use App\Domain\Entity\PropertyVacancyReporter\ReporterFormField;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EnumType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MandatoryFieldsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(child: 'requiredPropertyInformationFormFields', type: EnumType::class, options: [
                'label'        => 'Objektdetails',
                'class'        => PropertyInformationFormField::class,
                'choice_label' => 'name',
                'setter'       => function (PropertyVacancyReporterConfiguration $propertyVacancyReporterConfiguration, array $requiredPropertyInformationFormFields, FormInterface $form): void {
                    $propertyVacancyReporterConfiguration->setRequiredPropertyInformationFormFields(array_values(array: $requiredPropertyInformationFormFields));
                },
                'choice_filter' => function (PropertyInformationFormField $propertyInformationFormField): bool {
                    $excludeFields = [
                        PropertyInformationFormField::PROPERTY_USAGE_TYPE,
                    ];

                    if (in_array(needle: $propertyInformationFormField, haystack: $excludeFields, strict: true)) {
                        return false;
                    }

                    return true;
                },
                'multiple' => true,
                'expanded' => true,
            ])
            ->add(child: 'requiredPropertyLocationFormFields', type: EnumType::class, options: [
                'label'        => 'Lagebeschreibung',
                'class'        => PropertyLocationFormField::class,
                'choice_label' => 'name',
                'setter'       => function (PropertyVacancyReporterConfiguration $propertyVacancyReporterConfiguration, array $requiredPropertyLocationFormFields, FormInterface $form): void {
                    $propertyVacancyReporterConfiguration->setRequiredPropertyLocationFormFields(array_values(array: $requiredPropertyLocationFormFields));
                },
                'choice_filter' => function (PropertyLocationFormField $propertyLocationFormField): bool {
                    $excludeFields = [
                        PropertyLocationFormField::STREET_NAME,
                        PropertyLocationFormField::HOUSE_NUMBER,
                        PropertyLocationFormField::POSTAL_CODE,
                        PropertyLocationFormField::PLACE,
                    ];

                    if (in_array(needle: $propertyLocationFormField, haystack: $excludeFields, strict: true)) {
                        return false;
                    }

                    return true;
                },
                'multiple' => true,
                'expanded' => true,
            ])
            ->add(child: 'requiredReporterFormFields', type: EnumType::class, options: [
                'label'        => 'Persönliche Angaben',
                'class'        => ReporterFormField::class,
                'choice_label' => 'name',
                'setter'       => function (PropertyVacancyReporterConfiguration $propertyVacancyReporterConfiguration, array $requiredReporterFormFields, FormInterface $form): void {
                    $propertyVacancyReporterConfiguration->setRequiredReporterFormFields(array_values(array: $requiredReporterFormFields));
                },
                'choice_filter' => function (ReporterFormField $reporterFormField): bool {
                    $excludeFields = [
                        ReporterFormField::EMAIL,
                        ReporterFormField::PRIVACY_POLICY_ACCEPT,
                        ReporterFormField::NAME,
                    ];

                    if (in_array(needle: $reporterFormField, haystack: $excludeFields, strict: true)) {
                        return false;
                    }

                    return true;
                },
                'multiple' => true,
                'expanded' => true,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => PropertyVacancyReporterConfiguration::class]);
    }
}
