<?php

declare(strict_types=1);

namespace App\Form\Type\Configuration\PropertyVacancyReporter;

use App\Domain\Entity\PropertyVacancyReporter\PropertyVacancyReporterConfiguration;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

class GeneralType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(child: 'reportNotificationEmail', type: TextType::class, options: [
                'label'       => 'E-Mail für die Benachrichtigung bei neuen Leerstandsmeldungen',
                'required'    => true,
                'constraints' => [
                    new NotBlank(['message' => 'Bitte geben Sie eine E-Mail an']),
                    new Email(['message' => 'Bitte geben Sie eine valide E-Mail an']),
                ],
            ])
            ->add(child: 'title', type: TextType::class, options: ['label' => 'Überschrift für den Leerstandsmelder', 'required' => false])
            ->add(child: 'text', type: TextareaType::class, options: ['label' => 'Einführungstext für den Leerstandsmelder', 'required' => false]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => PropertyVacancyReporterConfiguration::class]);
    }
}
