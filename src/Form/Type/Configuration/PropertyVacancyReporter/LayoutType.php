<?php

declare(strict_types=1);

namespace App\Form\Type\Configuration\PropertyVacancyReporter;

use App\Domain\Entity\PropertyVacancyReporter\PropertyVacancyReporterConfiguration;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LayoutType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(child: 'firstColor', type: TextType::class, options: ['label' => 'Primärfarbe'])
            ->add(child: 'secondaryColor', type: TextType::class, options: ['label' => 'Sekundärfarbe']);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => PropertyVacancyReporterConfiguration::class]);
    }
}
