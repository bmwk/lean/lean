<?php

declare(strict_types=1);

namespace App\Form\Type\Configuration\BasicSettings;

use App\Domain\Entity\AccountConfiguration;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProcessesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(child: 'allowProviderRegistration', type: HiddenType::class, options: ['label' => 'Öffentliche Registrierung von Anbietenden (Eigentümer:innen, Makler:innen, etc.) erlauben', 'required' => false])
            ->add(child: 'allowSeekerRegistration', type: HiddenType::class, options: ['label' => 'Öffentliche Registrierung von Suchenden erlauben', 'required' => false]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => AccountConfiguration::class]);
    }
}
