<?php

declare(strict_types=1);

namespace App\Form\Type\Configuration\BasicSettings;

use App\Domain\Entity\AccountLegalTextsConfiguration;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PrivacyPolicyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(child: 'privacyPolicyProvider', type: TextareaType::class, options: ['label' => 'Erklärung zum Datenschutz', 'required' => true])
            ->add(child: 'privacyPolicySeeker', type: TextareaType::class, options: ['label' => 'Erklärung zum Datenschutz', 'required' => true])
            ->add(child: 'pdfExposeDisclaimer', type: TextareaType::class, options: ['label' => 'Disclaimer für das PDF-Exposé', 'required' => true]);

        $builder->get('privacyPolicyProvider')
            ->addModelTransformer(new CallbackTransformer(
                function (?string $privacyPolicyAsBase64): ?string {
                    if ($privacyPolicyAsBase64 === null) {
                        return null;
                    }

                    return base64_decode($privacyPolicyAsBase64);
                },
                function (?string $privacyPolicyAsString): ?string {
                    if ($privacyPolicyAsString === null) {
                        return null;
                    }

                    return base64_encode($privacyPolicyAsString);
                }
            ));

        $builder->get('privacyPolicySeeker')
            ->addModelTransformer(new CallbackTransformer(
                function (?string $privacyPolicyAsBase64): ?string {
                    if ($privacyPolicyAsBase64 === null) {
                        return null;
                    }

                    return base64_decode($privacyPolicyAsBase64);
                },
                function (?string $privacyPolicyAsString): ?string {
                    if ($privacyPolicyAsString === null) {
                        return null;
                    }

                    return base64_encode($privacyPolicyAsString);
                }
            ));

    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => AccountLegalTextsConfiguration::class]);
    }
}
