<?php

declare(strict_types=1);

namespace App\Form\Type\Configuration\BasicSettings\Wms;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class WmsLayerAddType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add(child: 'capabilitiesUrl', type: TextType::class, options: ['label' => 'Capabilities-URL', 'required' => true]);
    }
}
