<?php

declare(strict_types=1);

namespace App\Form\Type\Configuration\BasicSettings\Wms;

use App\Domain\Entity\WmsLayer;
use App\Domain\Map\MapService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ChooseLayersType extends AbstractType
{
    public function __construct(
        private readonly MapService $mapService
    ) {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(child: 'capabilitiesUrl', type: HiddenType::class, options: ['label' => 'Capabilities-URL', 'required' => true])
            ->addEventListener(eventName: FormEvents::PRE_SET_DATA, listener: function (FormEvent $event) use ($options): void {
                $form = $event->getForm();

                if ($options['capabilitiesUrl'] !== null) {
                    $wmsLayers = $this->mapService->parserWmsCapabilitiesFromUrl(url: $options['capabilitiesUrl']);

                    $form->add(child: 'capabilitiesUrl', type: HiddenType::class, options: [
                        'label'    => 'Capabilities-URL',
                        'required' => true,
                        'data'     => $options['capabilitiesUrl'],
                    ]);

                    $this->addWmsLayersChoiceType(form: $form, wmsLayers: $wmsLayers);
                }
            })
            ->addEventListener(eventName: FormEvents::PRE_SUBMIT, listener: function (FormEvent $event) use ($options): void {
                $data = $event->getData();
                $form = $event->getForm();

                $wmsLayers = $this->mapService->parserWmsCapabilitiesFromUrl(url: $data['capabilitiesUrl']);

                $this->addWmsLayersChoiceType(form: $form, wmsLayers: $wmsLayers);
            });
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setRequired(['capabilitiesUrl']);
    }

    /**
     * @param WmsLayer[] $wmsLayers
     */
    private function addWmsLayersChoiceType(FormInterface $form, array $wmsLayers): void
    {
        $form->add(child: 'wmsLayers', type: ChoiceType::class, options: [
            'choices'      => $wmsLayers,
            'choice_label' => 'title',
            'choice_value' => 'title',
            'multiple'     => true,
            'expanded'     => true,
        ]);
    }
}
