<?php

declare(strict_types=1);

namespace App\Form\Type\Configuration\BasicSettings\Wms;

use App\Form\Type\AbstractDeleteType;

class WmsLayerDeleteType extends AbstractDeleteType
{
}
