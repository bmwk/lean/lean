<?php

declare(strict_types=1);

namespace App\Form\Type\Configuration\BasicSettings\Wms;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class WmsCapabilitiesUrlType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(child: 'capabilitiesUrlVersionOneDotOne', type: TextType::class, options: [
                'label'    => 'WMS-Version 1.1.1',
                'data'     => $options['wmsCapabilitiesUrl'] . '&VERSION=1.1.1',
                'required' => false,
                'attr'     => ['readonly' => true],
            ])
            ->add(child: 'capabilitiesUrlVersionOneDotThree', type: TextType::class, options: [
                'label'    => 'WMS-Version 1.3.0',
                'data'     => $options['wmsCapabilitiesUrl'] . '&VERSION=1.3.0',
                'required' => false,
                'attr'     => ['readonly' => true],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setRequired(['wmsCapabilitiesUrl'])
            ->setAllowedTypes(option: 'wmsCapabilitiesUrl', allowedTypes: ['string']);
    }
}
