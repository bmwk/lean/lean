<?php

declare(strict_types=1);

namespace App\Form\Type\Configuration\BasicSettings;

use App\Domain\Entity\AccountLegalTextsConfiguration;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GtcType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(child: 'generalTermsAndConditionsProvider', type: TextareaType::class, options: ['label' => 'AGB für Anbietende', 'required' => true])
            ->add(child: 'generalTermsAndConditionsSeeker', type: TextareaType::class, options: ['label' => 'AGB für Suchende', 'required' => true]);

        $builder->get('generalTermsAndConditionsProvider')
            ->addModelTransformer(new CallbackTransformer(
                function (?string $gtcProviderAsBase64): ?string {
                    if ($gtcProviderAsBase64 === null) {
                        return null;
                    }

                    return base64_decode($gtcProviderAsBase64);
                },
                function (?string $gtcProviderAsString): ?string {
                    if ($gtcProviderAsString === null) {
                        return null;
                    }

                    return base64_encode($gtcProviderAsString);
                }
            ));

        $builder->get('generalTermsAndConditionsSeeker')
            ->addModelTransformer(new CallbackTransformer(
                function (?string $gtcSeekerAsBase64): ?string {
                    if ($gtcSeekerAsBase64 === null) {
                        return null;
                    }

                    return base64_decode($gtcSeekerAsBase64);
                },
                function (?string $gtcSeekerAsString): ?string {
                    if ($gtcSeekerAsString === null) {
                        return null;
                    }

                    return base64_encode($gtcSeekerAsString);
                }
            ));

    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => AccountLegalTextsConfiguration::class]);
    }
}
