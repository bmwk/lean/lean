<?php

declare(strict_types=1);

namespace App\Form\Type\Configuration\BasicSettings;

use App\Domain\Entity\AccountLegalTextsConfiguration;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ImprintType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add(child: 'imprint', type: TextareaType::class, options: ['label' => 'Impressum', 'required' => true]);

        $builder->get('imprint')
            ->addModelTransformer(new CallbackTransformer(
                function (?string $imprintAsBase64): ?string {
                    if ($imprintAsBase64 === null) {
                        return null;
                    }

                    return base64_decode($imprintAsBase64);
                },
                function (?string $imprintAsString): ?string {
                    if ($imprintAsString === null) {
                        return null;
                    }

                    return base64_encode($imprintAsString);
                }
            ));
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => AccountLegalTextsConfiguration::class]);
    }
}
