<?php

declare(strict_types=1);

namespace App\Form\Type\Configuration\BasicSettings;

use App\Domain\Entity\Account;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AccountDataType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(child: 'name', type: TextType::class, options: ['label' => 'Kommune / Kommunale Gesellschaft', 'required' => true])
            ->add(child: 'streetName', type: TextType::class, options: ['label' => 'Straße', 'required' => false])
            ->add(child: 'houseNumber', type: TextType::class, options: ['label' => 'Hausnr.', 'required' => false])
            ->add(child: 'postalCode', type: TextType::class, options: ['label' => 'PLZ', 'required' => false])
            ->add(child: 'placeName', type: TextType::class, options: ['label' => 'Ort', 'required' => false])
            ->add(child: 'phoneNumber', type: TextType::class, options: ['label' => 'Telefon', 'required' => false])
            ->add(child: 'email', type: EmailType::class, options: ['label' => 'E-Mail-Adresse', 'required' => true])
            ->add(child: 'website', type: TextType::class, options: ['label' => 'Webseite',  'required' => false]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => Account::class]);
    }
}
