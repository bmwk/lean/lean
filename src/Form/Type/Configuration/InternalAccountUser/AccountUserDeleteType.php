<?php

declare(strict_types=1);

namespace App\Form\Type\Configuration\InternalAccountUser;

use App\Form\Type\AbstractDeleteType;

class AccountUserDeleteType extends AbstractDeleteType
{
}
