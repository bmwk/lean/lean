<?php

declare(strict_types=1);

namespace App\Form\Type\Configuration\InternalAccountUser;

use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\AccountUser\AccountUserRole;
use App\Domain\Entity\AccountUser\AccountUserType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\EnumType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class InternalAccountUserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(child: 'fullName', type: TextType::class, options: ['label' => 'Name', 'required' => true])
            ->add(child: 'nameAbbreviation', type: TextType::class, options: ['label' => 'Namenskürzel', 'required' => false])
            ->add(child: 'phoneNumber', type: TextType::class, options: ['label' => 'Telefonnummer', 'required' => false])
            ->add(child: 'email', type: EmailType::class, options: ['label' => 'E-Mail-Adresse', 'required' => true])
            ->add(child: 'username', type: TextType::class, options: ['label' => 'Benutzername', 'required' => true])
            ->add(child: 'roles', type: EnumType::class, options: [
                'label'        => 'User Roles',
                'required'     => true,
                'class'        => AccountUserRole::class,
                'choices'      => AccountUserType::INTERNAL->getPossibleAccountUserRoles(),
                'choice_label' => 'name',
                'getter'       => function (AccountUser $accountUser, FormInterface $form): array {
                    return array_map(
                        callback: function (string $roleName): AccountUserRole {
                            return AccountUserRole::from($roleName);
                        },
                        array: $accountUser->getRoles());
                },
                'setter' => function (AccountUser $accountUser, array $roles, FormInterface $form): void {
                    $accountUser->setRoles(array_values(array: $roles));
                },
                'multiple' => true,
                'expanded' => true,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => AccountUser::class]);
    }
}
