<?php

declare(strict_types=1);

namespace App\Form\Type\Configuration\InternalAccountUser;

use App\Form\Type\AbstractMassOperationType;

class AccountUserMassOperationLockType extends AbstractMassOperationType
{
}
