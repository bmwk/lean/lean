<?php

declare(strict_types=1);

namespace App\Form\Type\Configuration\InternalAccountUser;

use App\Domain\Entity\AccountUser\AccountUserFilter;
use App\Form\Type\AbstractFilterType;
use App\SessionStorage\SessionStorageService;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class InternalAccountUserFilterType extends AbstractFilterType
{
    public function __construct(
        SessionStorageService $sessionStorageService,
        RequestStack $requestStack,
        UrlGeneratorInterface $router
    ) {
        parent::__construct(
            sessionStorageService: $sessionStorageService,
            requestStack: $requestStack,
            router: $router
        );
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        parent::buildForm(builder: $builder, options: $options);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => AccountUserFilter::class]);

        parent::configureOptions(resolver: $resolver);
    }
}
