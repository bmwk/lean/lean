<?php

declare(strict_types=1);

namespace App\Form\Type\Configuration\LookingForPropertyRequestReporter;

use App\Domain\Entity\LookingForPropertyRequestReporter\LookingForPropertyRequestReporterConfiguration;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

class GeneralType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(child: 'reportNotificationEmail', type: EmailType::class, options: [
                'label'       => 'E-Mail für die Benachrichtigung bei neuen Gesuchsmeldungen',
                'required'    => true,
                'constraints' => [
                    new NotBlank(['message' => 'Bitte geben Sie eine E-Mail an']),
                    new Email(['message' => 'Bitte geben Sie eine valide E-Mail an']),
                ],
            ])
            ->add(child: 'title', type: TextType::class, options: ['label' => 'Überschrift für den Gesuchsmelder', 'required' => false])
            ->add(child: 'text', type: TextareaType::class, options: ['label' => 'Einführungstext für den Gesuchsmelder', 'required' => false]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => LookingForPropertyRequestReporterConfiguration::class]);
    }
}
