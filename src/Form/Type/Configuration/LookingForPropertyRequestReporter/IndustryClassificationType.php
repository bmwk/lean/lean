<?php

declare(strict_types=1);

namespace App\Form\Type\Configuration\LookingForPropertyRequestReporter;

use App\Domain\Entity\Classification\IndustryClassification;
use App\Domain\Entity\LookingForPropertyRequestReporter\LookingForPropertyRequestReporterConfiguration;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class IndustryClassificationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add(child: 'industryClassifications', type: EntityType::class, options: [
            'required'      => false,
            'class'         => IndustryClassification::class,
            'query_builder' => function (EntityRepository $entityRepository) {
                $queryBuilder = $entityRepository->createQueryBuilder('ic');
                return $queryBuilder->where($queryBuilder->expr()->isNull('ic.levelTwo'))
                    ->andWhere($queryBuilder->expr()->isNull('ic.levelThree'));
            },
            'choice_label' => 'name',
            'multiple'     => true,
            'expanded'     => true,
        ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => LookingForPropertyRequestReporterConfiguration::class]);
    }
}
