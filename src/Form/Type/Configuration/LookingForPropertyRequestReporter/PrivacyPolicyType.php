<?php

declare(strict_types=1);

namespace App\Form\Type\Configuration\LookingForPropertyRequestReporter;

use App\Domain\Entity\LookingForPropertyRequestReporter\LookingForPropertyRequestReporterConfiguration;
use App\Form\DataTransformer\Base64ToStringTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class PrivacyPolicyType extends AbstractType
{
    public function __construct(
        private readonly Base64ToStringTransformer $base64ToStringTransformer
    ) {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add(child: 'privacyPolicy', type: TextareaType::class, options: [
            'label'       => 'Erklärung zum Datenschutz',
            'constraints' => [
                new NotBlank(['message' => 'Der Text zur Datenschutzerklärung darf nicht leer sein']),
            ],
        ]);

        $builder->get('privacyPolicy')->addModelTransformer($this->base64ToStringTransformer);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => LookingForPropertyRequestReporterConfiguration::class]);
    }
}
