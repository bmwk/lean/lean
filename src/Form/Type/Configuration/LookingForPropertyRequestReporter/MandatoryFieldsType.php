<?php

declare(strict_types=1);

namespace App\Form\Type\Configuration\LookingForPropertyRequestReporter;

use App\Domain\Entity\LookingForPropertyRequestReporter\BasicInformationFormField;
use App\Domain\Entity\LookingForPropertyRequestReporter\LookingForPropertyRequestReporterConfiguration;
use App\Domain\Entity\LookingForPropertyRequestReporter\LocationInformationFormField;
use App\Domain\Entity\LookingForPropertyRequestReporter\PropertyInformationFormField;
use App\Domain\Entity\LookingForPropertyRequestReporter\ReporterInformationFormField;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EnumType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MandatoryFieldsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(child: 'requiredBasicInformationFormFields', type: EnumType::class, options: [
                'label'        => 'Grundinformationen',
                'class'        => BasicInformationFormField::class,
                'choice_label' => 'name',
                'setter'       => function (LookingForPropertyRequestReporterConfiguration $lookingForPropertyRequestReporterConfiguration, array $requiredBasicInformationFormFields, FormInterface $form): void {
                    $lookingForPropertyRequestReporterConfiguration->setRequiredBasicInformationFormFields(array_values(array: $requiredBasicInformationFormFields));
                },
                'choice_filter' => function (BasicInformationFormField $basicInformationFormField): bool {
                    $excludeFields = [
                        BasicInformationFormField::TITLE,
                        BasicInformationFormField::REQUEST_REASON,
                        BasicInformationFormField::INDUSTRY_CLASSIFICATION,
                    ];

                    if (in_array(needle: $basicInformationFormField, haystack: $excludeFields, strict: true)) {
                        return false;
                    }

                    return true;
                },
                'multiple' => true,
                'expanded' => true,
            ])
            ->add(child: 'requiredLocationFormFields', type: EnumType::class, options: [
                'label'        => 'Lageanforderung',
                'class'        => LocationInformationFormField::class,
                'choice_label' => 'name',
                'setter'       => function (LookingForPropertyRequestReporterConfiguration $lookingForPropertyRequestReporterConfiguration, array $requiredLocationFormFields, FormInterface $form): void {
                    $lookingForPropertyRequestReporterConfiguration->setRequiredLocationFormFields(array_values(array: $requiredLocationFormFields));
                },
                'choice_filter' => function (LocationInformationFormField $locationInformationFormField): bool {
                    $excludeFields = [];

                    if (in_array(needle: $locationInformationFormField, haystack: $excludeFields, strict: true)) {
                        return false;
                    }

                    return true;
                },
                'multiple' => true,
                'expanded' => true,
            ])
            ->add(child: 'requiredPropertyInformationFormFields', type: EnumType::class, options: [
                'label'        => 'Objektanforderungen',
                'class'        => PropertyInformationFormField::class,
                'choice_label' => 'name',
                'setter'       => function (LookingForPropertyRequestReporterConfiguration $lookingForPropertyRequestReporterConfiguration, array $requiredPropertyInformationFormFields, FormInterface $form): void {
                    $lookingForPropertyRequestReporterConfiguration->setRequiredPropertyInformationFormFields(array_values(array: $requiredPropertyInformationFormFields));
                },
                'choice_filter' => function (PropertyInformationFormField $propertyInformationFormField): bool {
                    $excludeFields = [
                        PropertyInformationFormField::TOTAL_SPACE,
                    ];

                    if (in_array(needle: $propertyInformationFormField, haystack: $excludeFields, strict: true)) {
                        return false;
                    }

                    return true;
                },
                'multiple' => true,
                'expanded' => true,
            ])
            ->add(child: 'requiredReporterFormFields', type: EnumType::class, options: [
                'label'        => 'Persönliche Angaben',
                'class'        => ReporterInformationFormField::class,
                'choice_label' => 'name',
                'setter'       => function (LookingForPropertyRequestReporterConfiguration $lookingForPropertyRequestReporterConfiguration, array $requiredReporterFormFields, FormInterface $form): void {
                    $lookingForPropertyRequestReporterConfiguration->setRequiredReporterFormFields(array_values(array: $requiredReporterFormFields));
                },
                'choice_filter' => function (ReporterInformationFormField $reporterInformationFormField): bool {
                    $excludeFields = [
                        ReporterInformationFormField::EMAIL,
                        ReporterInformationFormField::PRIVACY_POLICY_ACCEPT,
                        ReporterInformationFormField::NAME,
                    ];

                    if (in_array(needle: $reporterInformationFormField, haystack: $excludeFields, strict: true)) {
                        return false;
                    }

                    return true;
                },
                'multiple' => true,
                'expanded' => true,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => LookingForPropertyRequestReporterConfiguration::class]);
    }
}
