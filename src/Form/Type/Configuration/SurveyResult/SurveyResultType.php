<?php

declare(strict_types=1);

namespace App\Form\Type\Configuration\SurveyResult;

use App\Domain\Entity\SurveyResult\SurveyResult;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SurveyResultType extends AbstractSurveyResultType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        parent::buildForm(builder: $builder, options: $options);

        $builder->add(child: 'shortDescription', type: TextareaType::class, options: ['label' => 'Kurzbeschreibung', 'required' => false]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => SurveyResult::class]);
    }
}
