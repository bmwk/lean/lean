<?php

declare(strict_types=1);

namespace App\Form\Type\Configuration\SurveyResult;

use App\Domain\Entity\SurveyResult\SurveyResult;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

abstract class AbstractSurveyResultType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(child: 'title', type: TextType::class, options: ['label' => 'Titel', 'required' => true])
            ->add(child: 'description', type: TextareaType::class, options: ['label' => 'ausführliche Beschreibung', 'required' => false]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => SurveyResult::class]);
    }
}
