<?php

declare(strict_types=1);

namespace App\Form\Type\Configuration\SurveyResult;

use App\Domain\Entity\SurveyResult\SurveyResultChapter;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SurveyResultChapterType extends AbstractSurveyResultType
{
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => SurveyResultChapter::class]);
    }
}
