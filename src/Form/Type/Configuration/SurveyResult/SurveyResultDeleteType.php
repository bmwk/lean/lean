<?php

declare(strict_types=1);

namespace App\Form\Type\Configuration\SurveyResult;

use App\Form\Type\AbstractDeleteType;

class SurveyResultDeleteType extends AbstractDeleteType
{
}
