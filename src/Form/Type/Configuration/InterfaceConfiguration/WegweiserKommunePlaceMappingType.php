<?php

declare(strict_types=1);

namespace App\Form\Type\Configuration\InterfaceConfiguration;

use App\Domain\Entity\WegweiserKommunePlaceMapping;
use App\Form\Type\AbstractDeleteType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class WegweiserKommunePlaceMappingType extends AbstractDeleteType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(child: 'wegweiserKommuneRegionId', type: TextType::class, options: ['label' => 'Region-ID', 'required' => false])
            ->add(child: 'wegweiserKommuneRegionFriendlyUrl', type: TextType::class, options: ['label' => 'Region-URL', 'required' => false]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => WegweiserKommunePlaceMapping::class]);
    }
}
