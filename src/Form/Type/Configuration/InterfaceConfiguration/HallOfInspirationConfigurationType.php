<?php

declare(strict_types=1);

namespace App\Form\Type\Configuration\InterfaceConfiguration;

use App\Domain\Entity\HallOfInspiration\HallOfInspirationConfiguration;
use App\Form\Type\AbstractDeleteType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class HallOfInspirationConfigurationType extends AbstractDeleteType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add(child: 'accessToken', type: TextareaType::class, options: ['label' => 'Access-Token', 'required' => false]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => HallOfInspirationConfiguration::class]);
    }
}
