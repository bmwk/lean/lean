<?php

declare(strict_types=1);

namespace App\Form\Type\Configuration\OpenImmoInterface;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class FtpUserDataType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(child: 'ftpHostname', type: TextType::class, options: [
                'label'    => 'Host',
                'required' => false,
                'attr'     => ['readonly' => true],
            ])
            ->add(child: 'ftpUsername', type: TextType::class, options: [
                'label'    => 'Benutzername',
                'required' => false,
                'attr'     => ['readonly' => true],
            ])
            ->add(child: 'ftpPassword', type: TextType::class, options: [
                'label'    => 'Passwort',
                'required' => false,
                'attr'     => ['readonly' => true],
            ]);
    }
}
