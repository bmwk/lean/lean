<?php

declare(strict_types=1);

namespace App\Form\Type\Configuration\OpenImmoFtpCredential;

use App\Domain\Entity\OpenImmoFtpTransfer\FtpProtocol;
use App\Domain\Entity\OpenImmoFtpTransfer\OpenImmoFtpCredential;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EnumType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OpenImmoFtpCredentialType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(child: 'portalName', type: TextType::class, options: ['label' => 'Portalname', 'required' => true])
            ->add(child: 'portalProviderNumber', type: TextType::class, options: ['label' => 'Portal-Anbieternr.', 'required' => false])
            ->add(child: 'hostname', type: TextType::class, options: ['label' => 'FTP-Hostname', 'required' => true])
            ->add(child: 'port', type: IntegerType::class, options: ['label' => 'FTP-Port', 'required' => false])
            ->add(child: 'username', type: TextType::class, options: ['label' => 'FTP-Benutzername', 'required' => true])
            ->add(child: 'password', type: HiddenType::class, options: ['label' => 'FTP-Passwort', 'required' => true])
            ->add(child: 'plainPassword', type: PasswordType::class, options: ['label' => 'FTP-Passwort', 'required' => false, 'mapped' => false])
            ->add(child: 'ftpProtocol', type: EnumType::class, options: [
                'label'        => 'FTP-Protokoll',
                'required'     => false,
                'class'        => FtpProtocol::class,
                'choice_label' => 'name',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => OpenImmoFtpCredential::class]);
    }
}
