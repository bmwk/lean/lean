<?php

declare(strict_types=1);

namespace App\Form\Type\Configuration\OpenImmoFtpCredential;

use App\Form\Type\AbstractDeleteType;

class OpenImmoFtpCredentialDeleteType extends AbstractDeleteType
{
}
