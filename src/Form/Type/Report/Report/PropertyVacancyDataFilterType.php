<?php

declare(strict_types=1);

namespace App\Form\Type\Report\Report;

use App\Domain\Entity\LocationCategory;
use App\Domain\Entity\Place;
use App\Domain\Entity\Property\VacancyReason;
use App\Domain\Entity\Report\PropertyVacancy\PropertyVacancyDataFilter;
use App\Domain\Location\LocationService;
use App\Form\Type\Classification\IndustryClassificationType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EnumType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PropertyVacancyDataFilterType extends AbstractType
{
    public function __construct(
        private readonly LocationService $locationService
    ) {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(child: 'industryClassification', type: IndustryClassificationType::class, options: ['mapped' => false, 'required' => false])
            ->add(child: 'quarterPlace', type: EntityType::class, options: [
                'label'        => 'Orts-/Stadtteil',
                'required'     => false,
                'class'        => Place::class,
                'choices'      => $this->locationService->fetchCityQuartersFromPlace(place: $options['places'][0]),
                'choice_label' => 'placeName',
            ])
            ->add(child: 'locationCategories', type: EnumType::class, options: [
                'label'        => 'Lagekategorie',
                'required'     => false,
                'class'        => LocationCategory::class,
                'choices'  => [
                    LocationCategory::ONE_A_LOCATION,
                    LocationCategory::ONE_B_LOCATION,
                    LocationCategory::ONE_C_LOCATION,
                    LocationCategory::TWO_A_LOCATION,
                    LocationCategory::TWO_B_LOCATION,
                    LocationCategory::TWO_C_LOCATION,
                    LocationCategory::OTHER,
                ],
                'choice_label' => function (LocationCategory $locationCategory): string {
                    return $locationCategory->getName();
                 },
                'multiple' => true,
                'expanded' => true,
            ])
            ->add(child: 'vacancyReasons', type: EnumType::class, options: [
                'label'        => 'Leerstandsgrund',
                'required'     => false,
                'class'        => VacancyReason::class,
                'choice_label' => function (VacancyReason $vacancyReason): string {
                    return $vacancyReason->getName();
                },
                'multiple' => true,
                'expanded' => true,
            ])
            ->add(child: 'onlyGroundFloor', type: CheckboxType::class, options: ['label' => 'Nur Erdgeschoss-Lagen']);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefaults(['data_class' => PropertyVacancyDataFilter::class])
            ->setRequired(['places']);
    }
}
