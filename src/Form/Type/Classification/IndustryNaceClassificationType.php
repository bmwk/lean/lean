<?php

declare(strict_types=1);

namespace App\Form\Type\Classification;

use App\Domain\Entity\Classification\IndustryClassification;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class IndustryNaceClassificationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $disabled = false;

        $builder
            ->add(child: 'industryUsage', type: EntityType::class, options: [
                'label'         => 'Nutzung',
                'disabled'      => $disabled,
                'class'         => IndustryClassification::class,
                'query_builder' => function (EntityRepository $entityRepository): QueryBuilder {
                    $queryBuilder = $entityRepository->createQueryBuilder(alias: 'ic');

                    return $queryBuilder
                        ->where($queryBuilder->expr()->isNull('ic.levelTwo'))
                        ->andWhere($queryBuilder->expr()->isNull('ic.levelThree'));
                },
                'choice_label'  => function (IndustryClassification $industryClassification): string {
                    return $industryClassification->getLevelOne() . '!' . $industryClassification->getName();
                },
            ])
            ->add(child: 'industryNaceSearchQuery', type: TextType::class, options: ['label' => 'Suche', 'attr' => ['placeholder' => 'Suchbegriff eingeben', 'disabled' => $disabled]])
            ->add(child: 'industryArea', type: ChoiceType::class, options: ['label' => 'Branchenbereich', 'disabled' => $disabled])
            ->add(child: 'industryDetails', type: ChoiceType::class, options: ['label' => 'Branchendetails', 'disabled' => $disabled])
            ->add(child: 'naceSection', type: ChoiceType::class, options: ['label' => 'Branchenklassifizierung - Abschnitt', 'disabled' => $disabled])
            ->add(child: 'naceDepartment', type: ChoiceType::class, options: ['label' => 'Branchenklassifizierung - Abteilung', 'disabled' => $disabled])
            ->add(child: 'naceGroup', type: ChoiceType::class, options: ['label' => 'Branchenklassifizierung - Gruppe', 'disabled' => $disabled])
            ->add(child: 'naceClass', type: ChoiceType::class, options: ['label' => 'Branchenklassifizierung - Klasse', 'disabled' => $disabled])
            ->add(child: 'naceSubclass', type: ChoiceType::class, options: ['label' => 'Branchenklassifizierung - Unterklasse', 'disabled' => $disabled])
            ->add(child: 'industryClassificationId', type: HiddenType::class)
            ->add(child: 'naceClassificationId', type: HiddenType::class);

        $builder->addEventListener(
            eventName: FormEvents::PRE_SUBMIT,
            listener: function (FormEvent $event) use ($options): void {
                $data = $event->getData();

                $data['industryArea'] = null;
                $data['industryDetails'] = null;
                $data['naceSection'] = null;
                $data['naceDepartment'] = null;
                $data['naceGroup'] = null;
                $data['naceClass'] = null;
                $data['naceSubclass'] = null;

                $event->setData($data);
            }
        );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setRequired(['canEdit'])
            ->setAllowedTypes(option: 'canEdit', allowedTypes: ['bool']);
    }
}
