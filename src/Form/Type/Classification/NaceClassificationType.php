<?php

declare(strict_types=1);

namespace App\Form\Type\Classification;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class NaceClassificationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(child: 'naceSection', type: ChoiceType::class, options: ['label' => 'Branchenklassifizierung - Abschnitt'])
            ->add(child: 'naceDepartment', type: ChoiceType::class, options: ['label' => 'Branchenklassifizierung - Abteilung'])
            ->add(child: 'naceGroup', type: ChoiceType::class, options: ['label' => 'Branchenklassifizierung - Gruppe'])
            ->add(child: 'naceClass', type: ChoiceType::class, options: ['label' => 'Branchenklassifizierung - Klasse'])
            ->add(child: 'naceSubclass', type: ChoiceType::class, options: ['label' => 'Branchenklassifizierung - Unterklasse']);

        $builder->addEventListener(
            eventName: FormEvents::PRE_SUBMIT,
            listener: function (FormEvent $event) use ($options): void {
                $data = $event->getData();

                $data['naceSection'] = null;
                $data['naceDepartment'] = null;
                $data['naceGroup'] = null;
                $data['naceClass'] = null;
                $data['naceSubclass'] = null;

                $event->setData($data);
            }
        );
    }
}
