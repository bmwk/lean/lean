<?php

declare(strict_types=1);

namespace App\Form\Type\CompanyLocationManagement\LookingForPropertyRequestReport;

use App\Form\Type\AbstractDeleteType;

class ReportDeleteType extends AbstractDeleteType
{
}
