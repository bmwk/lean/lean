<?php

declare(strict_types=1);

namespace App\Form\Type\CompanyLocationManagement\LookingForPropertyRequestReport;

use App\Domain\Entity\LocationFactor;
use App\Domain\Entity\LookingForPropertyRequest\LookingForPropertyRequest;
use App\Domain\Entity\RequestReason;
use App\Form\Type\Classification\IndustryClassificationType;
use App\Form\Type\CompanyLocationManagement\LookingForPropertyRequest\PriceRequirementType;
use App\Form\Type\CompanyLocationManagement\LookingForPropertyRequest\PropertyRequirementType;
use App\Form\Type\PersonManagement\Person\PersonCreateType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EnumType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class ReportToLookingForPropertyRequestType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(child: 'industryClassification', type: IndustryClassificationType::class, options: ['mapped' => false, 'required' => false])
            ->add(child: 'personCreate', type: PersonCreateType::class, options: ['mapped'  => false])
            ->add(child: 'locationFactors', type: EnumType::class, options: [
                'label'        => 'Standortfaktoren',
                'required'     => false,
                'class'        => LocationFactor::class,
                'choice_label' => function (LocationFactor $locationFactor): string {
                    return $locationFactor->getName();
                },
                'multiple' => true,
                'expanded' => true,
            ])
            ->add(child: 'title', type: TextType::class, options: [
                'label'       => 'Titel des Gesuchs',
                'required'    => false,
                'constraints' => [new NotBlank()],
            ])
            ->add(child: 'conceptDescription', type: TextareaType::class, options: ['label' => 'Beschreibung des Konzepts', 'required' => false])
            ->add(child: 'requestAvailableTill', type: DateType::class, options: [
                'label'    => 'Gesuch aktiv bis',
                'required' => false,
                'widget'   => 'single_text',
                'format'   => 'dd.MM.yyyy',
                'html5'    => false,
            ])
            ->add(child: 'automaticEnding', type: CheckboxType::class, options: ['label' => 'Gesuch soll automatisch enden', 'required' => false])
            ->add(child: 'priceRequirement', type: PriceRequirementType::class,options: ['canEdit' => true])
            ->add(child: 'propertyRequirement', type: PropertyRequirementType::class, options: [
                'places'  => $options['places'],
                'canEdit' => true,
            ])
            ->add(child: 'requestReason', type: EnumType::class, options: [
                'label'        => 'Gesuchsgrund',
                'required'     => false,
                'class'        => RequestReason::class,
                'choice_label' => function (RequestReason $requestReason): string {
                    return $requestReason->getName();
                },
            ])
            ->addEventListener(eventName: FormEvents::POST_SUBMIT, listener: function (FormEvent $event) use ($options): void {
                $data = $event->getData();
                $form = $event->getForm();

                $person = $form->get('personCreate')->getData();

                if ($person !== null) {
                    $data->setPerson($person);
                }
            });
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefaults(['data_class' => LookingForPropertyRequest::class])
            ->setRequired(['places']);
    }
}
