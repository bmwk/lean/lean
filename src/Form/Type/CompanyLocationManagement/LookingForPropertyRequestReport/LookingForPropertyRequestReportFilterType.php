<?php

declare(strict_types=1);

namespace App\Form\Type\CompanyLocationManagement\LookingForPropertyRequestReport;

use App\Domain\Classification\ClassificationService;
use App\Domain\Entity\LookingForPropertyRequestReport\LookingForPropertyRequestReportFilter;
use App\Form\Type\AbstractFilterType;
use App\SessionStorage\SessionStorageService;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class LookingForPropertyRequestReportFilterType extends AbstractFilterType
{
    public function __construct(
        SessionStorageService $sessionStorageService,
        RequestStack $requestStack,
        UrlGeneratorInterface $router,
        private readonly ClassificationService $classificationService
    ) {
        parent::__construct(
            sessionStorageService: $sessionStorageService,
            requestStack: $requestStack,
            router: $router
        );
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add(child: 'industryClassifications', type: ChoiceType::class, options: [
            'choices'      => $this->classificationService->fetchTopLevelIndustryClassifications(),
            'choice_label' => 'name',
            'choice_value' => 'id',
            'expanded'     => true,
            'multiple'     => true,
        ]);

        parent::buildForm(builder: $builder, options: $options);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => LookingForPropertyRequestReportFilter::class]);

        parent::configureOptions(resolver: $resolver);
    }
}
