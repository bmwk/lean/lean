<?php

declare(strict_types=1);

namespace App\Form\Type\CompanyLocationManagement\Matching;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class LookingForPropertyRequestExposeForwardingType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
    }
}
