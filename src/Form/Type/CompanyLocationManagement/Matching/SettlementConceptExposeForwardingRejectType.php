<?php

declare(strict_types=1);

namespace App\Form\Type\CompanyLocationManagement\Matching;

use App\Domain\Entity\SettlementConcept\SettlementConceptExposeForwarding;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SettlementConceptExposeForwardingRejectType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add(child: 'rejectReason', type: TextareaType::class, options: ['label' => 'Ablehnungsgrund', 'required' => true]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => SettlementConceptExposeForwarding::class]);
    }
}
