<?php

declare(strict_types=1);

namespace App\Form\Type\CompanyLocationManagement\Matching;

use App\Domain\Account\AccountUserService;
use App\Domain\Classification\ClassificationService;
use App\Domain\Entity\Account;
use App\Domain\Entity\AccountUser\AccountUserRole;
use App\Domain\Entity\AccountUser\AccountUserType;
use App\Domain\Entity\LookingForPropertyRequest\MatchingFilter;
use App\Domain\Entity\Property\BarrierFreeAccess;
use App\Domain\Entity\PropertyOfferType;
use App\Form\Type\AbstractFilterType;
use App\Form\Type\CompanyLocationManagement\LookingForPropertyRequest\ValueRequirementType;
use App\Form\Type\EuropeanDecimalType;
use App\SessionStorage\SessionStorageService;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EnumType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class MatchingFilterType extends AbstractFilterType
{
    private static array $propertyStatusChoices = [
        'Leerstand'                        => 0,
        'drohender Leerstand'              => 1,
        'Ankerobjekt / Schlüsselimmobilie' => 2,
    ];

    private static array $matchingStatusChoices = [
        'mit Treffern'             => 0,
        'ohne Treffer'             => 1,
        'kein Matching ausgeführt' => 2,
    ];

    public function __construct(
        SessionStorageService $sessionStorageService,
        RequestStack $requestStack,
        UrlGeneratorInterface $router,
        private readonly AccountUserService $accountUserService,
        private readonly ClassificationService $classificationService
    ) {
        parent::__construct(
            sessionStorageService: $sessionStorageService,
            requestStack: $requestStack,
            router: $router
        );
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(child: 'propertyStatus', type: ChoiceType::class, options: [
                'choices'  => self::$propertyStatusChoices,
                'expanded' => true,
                'multiple' => true,
            ])
            ->add(child: 'industryClassifications', type: ChoiceType::class, options: [
                'choices'      => $this->classificationService->fetchTopLevelIndustryClassifications(),
                'choice_label' => 'name',
                'choice_value' => 'id',
                'expanded'     => true,
                'multiple'     => true,
            ])
            ->add(child: 'propertyOfferTypes', type: EnumType::class, options: [
                'label'        => 'Angebotsart',
                'class'        => PropertyOfferType::class,
                'choice_label' => function (PropertyOfferType $propertyOfferType): string {
                    return $propertyOfferType->getName();
                },
                'expanded' => true,
                'multiple' => true,
            ])
            ->add(child: 'areaSizeMinimum', type: EuropeanDecimalType::class, options: ['label' => 'Gesamtgröße von'])
            ->add(child: 'areaSizeMaximum', type: EuropeanDecimalType::class, options: ['label' => 'Gesamtgröße bis'])
            ->add(child: 'shopWindowFrontWidthMinimum', type: EuropeanDecimalType::class, options: ['label' => 'Breite von'])
            ->add(child: 'shopWindowFrontWidthMaximum', type: EuropeanDecimalType::class, options: ['label' => 'Breite bis',])
            ->add(child: 'lastMatchingFrom', type: DateType::class, options: [
                'label'  => 'von',
                'widget' => 'single_text',
                'format' => 'dd.MM.yyyy',
                'html5'  => false,
            ])
            ->add(child: 'lastMatchingTill', type: DateType::class, options: [
                'label'  => 'bis',
                'widget' => 'single_text',
                'format' => 'dd.MM.yyyy',
                'html5'  => false,
            ])
            ->add(child: 'matchingStatus', type: ChoiceType::class, options: [
                'choices'  => self::$matchingStatusChoices,
                'expanded' => true,
                'multiple' => true,
            ])
            ->add(child: 'barrierFreeAccess', type: EnumType::class, options: [
                'label'        => 'Barierfreier Zugang',
                'class'        => BarrierFreeAccess::class,
                'choice_label' => function (BarrierFreeAccess $barrierFreeAccess): string {
                    return $barrierFreeAccess->getName();
                },
            ])
            ->add(child: 'assignedToAccountUser', type: ChoiceType::class, options: [
                'label'    => 'Sachbearbeiter:in',
                'required' => false,
                'choices'  => $this->accountUserService->fetchAccountUsersByAccount(
                    account: $options['account'],
                    withFromSubAccounts: false,
                    accountUserTypes: [AccountUserType::INTERNAL],
                    accountUserRoles: [
                        AccountUserRole::ROLE_ACCOUNT_ADMIN,
                        AccountUserRole::ROLE_USER,
                        AccountUserRole::ROLE_PROPERTY_VACANCY_REPORT_MANAGER,
                    ]
                ),
                'choice_label' => 'fullName',
            ])
            ->add(child: 'purchasePriceGross', type: ValueRequirementType::class, options: ['label' => 'Kaufpreis', 'canEdit' => true])
            ->add(child: 'purchasePricePerSquareMeter', type: ValueRequirementType::class, options: ['label' => 'Kaufpreis pro m²', 'canEdit' => true])
            ->add(child: 'coldRent', type: ValueRequirementType::class, options: ['label' => 'Kaltmiete', 'canEdit' => true])
            ->add(child: 'rentalPricePerSquareMeter', type: ValueRequirementType::class, options: ['label' => 'Mietpreis pro m²', 'canEdit' => true])
            ->add(child: 'lease', type: ValueRequirementType::class, options: ['label' => 'Pacht', 'canEdit' => true]);

        parent::buildForm(builder: $builder, options: $options);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefaults(['data_class' => MatchingFilter::class])
            ->setRequired(['account'])
            ->setAllowedTypes(option: 'account', allowedTypes: [Account::class]);

        parent::configureOptions(resolver: $resolver);
    }
}
