<?php

declare(strict_types=1);

namespace App\Form\Type\CompanyLocationManagement\LookingForPropertyRequest;

use App\Domain\Entity\ValueRequirement;
use App\Form\Type\EuropeanDecimalType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\PositiveOrZero;

class ValueRequirementType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $disabled = $options['canEdit'] === false;

        $builder
            ->add(child: 'minimumValue', type: EuropeanDecimalType::class, options: [
                'label'       => isset($options['label']) ? $options['label'] . ' min' : 'min',
                'required'    => false,
                'disabled'    => $disabled,
                'constraints' => [
                    new PositiveOrZero(),
                ],
            ])
            ->add(child: 'maximumValue', type: EuropeanDecimalType::class, options: [
                'label'       => isset($options['label']) ? $options['label'] . ' max' : 'max',
                'required'    => false,
                'disabled'    => $disabled,
                'constraints' => [
                    new PositiveOrZero(),
                ],
            ]);

        $builder->addEventListener(eventName: FormEvents::POST_SUBMIT, listener: function (FormEvent $event): void {
            $event->getForm()->getData()->setName($event->getForm()->getName());
        });
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefaults(['data_class' => ValueRequirement::class])
            ->setRequired(['canEdit'])
            ->setAllowedTypes(option: 'canEdit', allowedTypes: ['bool']);
    }
}
