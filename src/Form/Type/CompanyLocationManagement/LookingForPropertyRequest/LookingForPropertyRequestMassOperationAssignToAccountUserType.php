<?php

declare(strict_types=1);

namespace App\Form\Type\CompanyLocationManagement\LookingForPropertyRequest;

use App\Domain\Account\AccountUserService;
use App\Domain\Entity\AccountUser\AccountUserRole;
use App\Domain\Entity\AccountUser\AccountUserType;
use App\Form\Type\AbstractMassOperationType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LookingForPropertyRequestMassOperationAssignToAccountUserType extends AbstractMassOperationType
{
    public function __construct(
        private readonly AccountUserService $accountUserService
    ) {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        parent::buildForm(builder: $builder, options: $options);

        $builder->add(child: 'assignedToAccountUser', type: ChoiceType::class, options: [
            'label'    => 'Sachbearbeiter:in',
            'required' => false,
            'choices'  => $this->accountUserService->fetchAccountUsersByAccount(
                account: $options['account'],
                withFromSubAccounts: false,
                accountUserTypes: [AccountUserType::INTERNAL],
                accountUserRoles: [
                    AccountUserRole::ROLE_ACCOUNT_ADMIN,
                    AccountUserRole::ROLE_USER,
                    AccountUserRole::ROLE_LOOKING_FOR_PROPERTY_REQUEST_FEEDER,
                ]
            ),
            'choice_label' => 'fullName',
        ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setRequired(['account']);
    }
}
