<?php

declare(strict_types=1);

namespace App\Form\Type\CompanyLocationManagement\LookingForPropertyRequest;

use App\Domain\Entity\LocationCategory;
use App\Domain\Entity\LookingForPropertyRequest\PropertyRequirement;
use App\Domain\Entity\Place;
use App\Domain\Location\LocationService;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EnumType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PropertyRequirementType extends AbstractType
{
    public function __construct(
        private readonly LocationService $locationService
    ) {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $disabled = $options['canEdit'] === false;

        if (count($options['places']) === 1) {
            $builder->add(child: 'inQuarters', type: EntityType::class, options: [
                'label'        => 'Stadtteile',
                'required'     => false,
                'disabled'     => $disabled,
                'placeholder'  => 'alle Stadtteile',
                'class'        => Place::class,
                'choices'      => $this->locationService->fetchCityQuartersFromPlace(place: $options['places'][0]),
                'choice_label' => function (Place $place): string {
                    return $place->getPlaceName();
                 },
                'expanded' => true,
                'multiple' => true,
            ]);
        } else {
            $builder->add(child: 'inPlaces', type: EntityType::class, options: [
                'label'        => 'Ort',
                'required'     => false,
                'disabled'     => $disabled,
                'class'        => Place::class,
                'choices'      => $options['places'],
                'choice_label' => function (Place $place): string {
                    return $place->getPlaceName();
                },
                'expanded' => true,
                'multiple' => true,
            ]);
        }

        $builder
            ->add(child: 'numberOfParkingLots', type: NumberType::class, options: ['label' => 'Anzahl benötigter Parkplätze', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'roomCount', type: NumberType::class, options: ['label' => 'Anzahl Räume', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'earliestAvailableFrom', type: DateType::class, options: [
                'label'    => 'frühestes Bezugsdatum',
                'required' => false,
                'disabled' => $disabled,
                'widget'   => 'single_text',
                'format'   => 'dd.MM.yyyy',
                'html5'    => false,
            ])
            ->add(child: 'latestAvailableFrom', type: DateType::class, options: [
                'label'    => 'spätestes Bezugsdatum',
                'required' => false,
                'disabled' => $disabled,
                'widget'   => 'single_text',
                'format'   => 'dd.MM.yyyy',
                'html5'    => false,
            ])
            ->add(child: 'propertyMandatoryRequirement', type: PropertyMandatoryRequirementType::class, options: ['canEdit' => $options['canEdit']])
            ->add(child: 'propertyValueRangeRequirement', type: PropertyValueRangeRequirementType::class, options: ['canEdit' => $options['canEdit']])
            ->add(child: 'locationCategories', type: EnumType::class, options: [
                'label'    => 'Einzelhandelslage',
                'required' => false,
                'disabled' => $disabled,
                'class'    => LocationCategory::class,
                'choices'  => [
                    LocationCategory::ONE_A_LOCATION,
                    LocationCategory::ONE_B_LOCATION,
                    LocationCategory::ONE_C_LOCATION,
                    LocationCategory::TWO_A_LOCATION,
                    LocationCategory::TWO_B_LOCATION,
                    LocationCategory::TWO_C_LOCATION,
                    LocationCategory::OTHER,
                ],
                'choice_label' => function (LocationCategory $locationCategory): string {
                    return $locationCategory->getName();
                },
                'expanded' => true,
                'multiple' => true,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefaults(['data_class' => PropertyRequirement::class])
            ->setRequired(['places', 'canEdit'])
            ->setAllowedTypes(option: 'places', allowedTypes: 'array')
            ->setAllowedTypes(option: 'canEdit', allowedTypes: ['bool']);
    }
}
