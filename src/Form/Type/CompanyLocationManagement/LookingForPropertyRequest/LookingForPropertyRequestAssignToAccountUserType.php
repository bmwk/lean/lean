<?php

declare(strict_types=1);

namespace App\Form\Type\CompanyLocationManagement\LookingForPropertyRequest;

use App\Domain\Account\AccountUserService;
use App\Domain\Entity\Account;
use App\Domain\Entity\AccountUser\AccountUserRole;
use App\Domain\Entity\AccountUser\AccountUserType;
use App\Domain\Entity\LookingForPropertyRequest\LookingForPropertyRequest;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LookingForPropertyRequestAssignToAccountUserType extends AbstractType
{
    public function __construct(
        private readonly AccountUserService $accountUserService
    ) {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add(child: 'assignedToAccountUser', type: ChoiceType::class, options: [
            'label'    => 'Sachbearbeiter:in',
            'required' => false,
            'choices'  => $this->accountUserService->fetchAccountUsersByAccount(
                account: $options['account'],
                withFromSubAccounts: false,
                accountUserTypes: [AccountUserType::INTERNAL],
                accountUserRoles: [
                    AccountUserRole::ROLE_ACCOUNT_ADMIN,
                    AccountUserRole::ROLE_USER,
                    AccountUserRole::ROLE_LOOKING_FOR_PROPERTY_REQUEST_FEEDER,
                ]
            ),
            'choice_label' => 'fullName',
        ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefaults(['data_class' => LookingForPropertyRequest::class])
            ->setRequired(['account'])
            ->setAllowedTypes(option: 'account', allowedTypes: [Account::class]);
    }
}
