<?php

declare(strict_types=1);

namespace App\Form\Type\CompanyLocationManagement\LookingForPropertyRequest;

use App\Domain\Entity\Account;
use App\Domain\Entity\AccountUser\AccountUserType;
use App\Domain\Entity\LocationFactor;
use App\Domain\Entity\LookingForPropertyRequest\LookingForPropertyRequest;
use App\Domain\Entity\Person\Person;
use App\Domain\Entity\RequestReason;
use App\Form\Type\Classification\IndustryClassificationType;
use App\Form\Type\PersonManagement\Person\PersonSelectOrPersonCreateType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EnumType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class LookingForPropertyRequestType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $disabled = $options['canEdit'] === false;

        $accountUserType = $options['accountUserType'];
        $canViewPerson = $options['canViewPerson'];

        if ($accountUserType === AccountUserType::INTERNAL) {
            if ($canViewPerson === true) {
                $builder->add(child: 'personSelectOrPersonCreate', type: PersonSelectOrPersonCreateType::class, options: [
                    'mapped'         => false,
                    'account'        => $options['account'],
                    'selectedPerson' => $options['selectedPerson'],
                    'canEdit'        => $options['canEdit'],
                ]);
            }
        }

        $builder
            ->add(child: 'industryClassification', type: IndustryClassificationType::class, options: ['mapped' => false, 'required' => false, 'disabled' => $disabled])
            ->add(child: 'locationFactors', type: EnumType::class, options: [
                'label'        => 'Standortfaktoren',
                'required'     => false,
                'disabled'     => $disabled,
                'class'        => LocationFactor::class,
                'choice_label' => function (LocationFactor $locationFactor): string {
                    return $locationFactor->getName();
                 },
                'multiple' => true,
                'expanded' => true,
            ])
            ->add(child: 'title', type: TextType::class, options: [
                'label'       => 'Titel des Gesuchs',
                'required'    => false,
                'disabled'    => $disabled,
                'constraints' => [new NotBlank()],
            ])
            ->add(child: 'conceptDescription', type: TextareaType::class, options: ['label' => 'Beschreibung des Konzepts', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'requestAvailableTill', type: DateType::class, options: [
                'label'    => 'Gesuch aktiv bis',
                'required' => false,
                'disabled' => $disabled,
                'widget'   => 'single_text',
                'format'   => 'dd.MM.yyyy',
                'html5'    => false,
            ])
            ->add(child: 'automaticEnding', type: CheckboxType::class, options: ['label' => 'Gesuch soll automatisch enden', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'priceRequirement', type: PriceRequirementType::class, options: ['canEdit' => $options['canEdit']])
            ->add(child: 'propertyRequirement', type: PropertyRequirementType::class, options: [
                'places'  => $options['places'],
                'canEdit' => $options['canEdit'],
            ])
            ->add(child: 'requestReason', type: EnumType::class, options: [
                'label'        => 'Gesuchsgrund',
                'required'     => false,
                'disabled'     => $disabled,
                'class'        => RequestReason::class,
                'choice_label' => function (RequestReason $requestReason): string {
                    return $requestReason->getName();
                },
            ])
            ->addEventListener(eventName: FormEvents::POST_SUBMIT, listener: function (FormEvent $event) use ($accountUserType, $canViewPerson): void {
                $data = $event->getData();
                $form = $event->getForm();

                if ($accountUserType === AccountUserType::INTERNAL) {
                    if ($canViewPerson === true) {
                        $data->setPerson($form->get('personSelectOrPersonCreate')->getData());
                    }
                }
            });
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefaults(['data_class' => LookingForPropertyRequest::class])
            ->setRequired(['places', 'accountUserType', 'account', 'selectedPerson', 'canEdit', 'canViewPerson'])
            ->setAllowedTypes(option: 'places', allowedTypes: 'array')
            ->setAllowedTypes(option: 'accountUserType', allowedTypes: AccountUserType::class)
            ->setAllowedTypes(option: 'account', allowedTypes: Account::class)
            ->setAllowedTypes(option: 'selectedPerson', allowedTypes: [Person::class, 'null'])
            ->setAllowedTypes(option: 'canEdit', allowedTypes: ['bool'])
            ->setAllowedTypes(option: 'canViewPerson', allowedTypes: ['bool']);

    }
}
