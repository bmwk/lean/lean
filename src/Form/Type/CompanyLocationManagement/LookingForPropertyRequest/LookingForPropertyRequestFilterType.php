<?php

declare(strict_types=1);

namespace App\Form\Type\CompanyLocationManagement\LookingForPropertyRequest;

use App\Domain\Account\AccountUserService;
use App\Domain\Classification\ClassificationService;
use App\Domain\Entity\Account;
use App\Domain\Entity\AccountUser\AccountUserRole;
use App\Domain\Entity\LookingForPropertyRequest\LookingForPropertyRequestFilter;
use App\Domain\Entity\RequestReason;
use App\Form\Type\AbstractFilterType;
use App\Form\Type\EuropeanDecimalType;
use App\SessionStorage\SessionStorageService;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EnumType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class LookingForPropertyRequestFilterType extends AbstractFilterType
{
    public function __construct(
        SessionStorageService $sessionStorageService,
        RequestStack $requestStack,
        UrlGeneratorInterface $router,
        private readonly AccountUserService $accountUserService,
        private readonly ClassificationService $classificationService
    ) {
        parent::__construct(
            sessionStorageService: $sessionStorageService,
            requestStack: $requestStack,
            router: $router
        );
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(child: 'requestReasons', type: EnumType::class, options: [
                'label'        => 'Ansiedlungsgrund',
                'class'        => RequestReason::class,
                'required'     => false,
                'choice_label' => function (RequestReason $requestReason): string {
                    return $requestReason->getName();
                },
                'expanded' => true,
                'multiple' => true,
            ])
            ->add(child: 'industryClassifications', type: ChoiceType::class, options: [
                'choices'      => $this->classificationService->fetchTopLevelIndustryClassifications(),
                'choice_label' => 'name',
                'choice_value' => 'id',
                'expanded'     => true,
                'multiple'     => true,
            ])
            ->add(child: 'areaSizeMinimum', type: EuropeanDecimalType::class, options: ['label' => 'von'])
            ->add(child: 'areaSizeMaximum', type: EuropeanDecimalType::class, options: ['label' => 'bis'])
            ->add(child: 'lastUpdatedFrom', type: DateType::class, options: [
                'label'  => 'von',
                'widget' => 'single_text',
                'format' => 'dd.MM.yyyy',
                'html5'  => false,
            ])->add(child: 'lastUpdatedTill', type: DateType::class, options: [
                'label'  => 'bis',
                'widget' => 'single_text',
                'format' => 'dd.MM.yyyy',
                'html5'  => false,
            ])
            ->add(child: 'assignedToAccountUser', type: ChoiceType::class, options: [
                'label'    => 'Sachbearbeiter:in',
                'required' => false,
                'choices'  => $this->accountUserService->fetchAccountUsersByAccount(
                    account: $options['account'],
                    withFromSubAccounts: false,
                    accountUserRoles: [
                        AccountUserRole::ROLE_ACCOUNT_ADMIN,
                        AccountUserRole::ROLE_USER,
                        AccountUserRole::ROLE_LOOKING_FOR_PROPERTY_REQUEST_FEEDER,
                    ]
                ),
                'choice_label' => 'fullName',
            ]);

        parent::buildForm(builder: $builder, options: $options);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefaults(['data_class' => LookingForPropertyRequestFilter::class])
            ->setRequired(['account'])
            ->setAllowedTypes(option: 'account', allowedTypes: [Account::class]);

        parent::configureOptions(resolver: $resolver);
    }
}
