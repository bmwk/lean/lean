<?php

declare(strict_types=1);

namespace App\Form\Type\CompanyLocationManagement\LookingForPropertyRequest;

use App\Domain\Entity\PropertyValueRangeRequirement;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PropertyValueRangeRequirementType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(child: 'totalSpace', type: ValueRequirementType::class, options: ['label' => 'Gesamtflächenbedarf', 'canEdit' => $options['canEdit']])
            ->add(child: 'retailSpace', type: ValueRequirementType::class, options: ['label' => 'Verkaufsflächenbedarf', 'canEdit' => $options['canEdit']])
            ->add(child: 'outdoorSpace', type: ValueRequirementType::class, options: ['label' => 'Außenflächebedarf', 'canEdit' => $options['canEdit']])
            ->add(child: 'usableSpace', type: ValueRequirementType::class, options: ['label' => 'Nutzflächenbedarf', 'canEdit' => $options['canEdit']])
            ->add(child: 'storageSpace', type: ValueRequirementType::class, options: ['label' => 'Lagerflächenbedarf', 'canEdit' => $options['canEdit']])
            ->add(child: 'shopWindowLength', type: ValueRequirementType::class, options: ['label'  => 'Schaufensterbreite', 'canEdit' => $options['canEdit']])
            ->add(child: 'shopWidth', type: ValueRequirementType::class, options: ['label'  => 'Ladenbreite', 'canEdit' => $options['canEdit']])
            ->add(child: 'subsidiarySpace', type: ValueRequirementType::class, options: ['label' => 'Nebenfläche', 'canEdit' => $options['canEdit']]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefaults(['data_class' => PropertyValueRangeRequirement::class])
            ->setRequired(['canEdit'])
            ->setAllowedTypes(option: 'canEdit', allowedTypes: ['bool']);
    }
}
