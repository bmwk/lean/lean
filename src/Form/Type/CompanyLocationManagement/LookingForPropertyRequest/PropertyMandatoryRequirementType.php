<?php

declare(strict_types=1);

namespace App\Form\Type\CompanyLocationManagement\LookingForPropertyRequest;

use App\Domain\Entity\PropertyMandatoryRequirement;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PropertyMandatoryRequirementType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $disabled = $options['canEdit'] === false;

        $builder
            ->add(child: 'roofingRequired', type: CheckboxType::class, options: ['label' => 'Überdachung benötigt', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'outdoorSpaceRequired', type: CheckboxType::class, options: ['label' => 'Außenfläche benötigt', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'showroomRequired', type: CheckboxType::class, options: ['label' => 'Showroom benötigt', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'shopWindowRequired', type: CheckboxType::class, options: ['label' => 'Schaufenster benötigt', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'groundLevelSalesAreaRequired', type: CheckboxType::class, options: ['label' => 'Ebenerdige Verkaufsfläche benötigt', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'barrierFreeAccessRequired', type: CheckboxType::class, options: ['label' => 'Barrierefreier Zugang benötigt', 'required' => false, 'disabled' => $disabled]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefaults(['data_class' => PropertyMandatoryRequirement::class])
            ->setRequired(['canEdit'])
            ->setAllowedTypes(option: 'canEdit', allowedTypes: ['bool']);
    }
}
