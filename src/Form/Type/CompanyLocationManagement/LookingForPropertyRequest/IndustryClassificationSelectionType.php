<?php

declare(strict_types=1);

namespace App\Form\Type\CompanyLocationManagement\LookingForPropertyRequest;

use App\Domain\Entity\Classification\IndustryClassification;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class IndustryClassificationSelectionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(child: 'industryUsage', type: EntityType::class, options: [
                'label'         => 'Vorgesehene Nutzung',
                'class'         => IndustryClassification::class,
                'query_builder' => function (EntityRepository $entityRepository) {
                    $queryBuilder = $entityRepository->createQueryBuilder(alias: 'industryClassification');

                    return $queryBuilder
                        ->where(predicates: 'industryClassification.levelTwo IS NULL')
                        ->andWhere('industryClassification.levelThree IS NULL');
                },
                'choice_label' => function (IndustryClassification $industryClassification): string {
                    return $industryClassification->getName();
                },
                'required' => false,
            ])
            ->add(child: 'industryArea', type: ChoiceType::class, options: [
                'label'    => 'Branchenbereich',
                'required' => false,
            ])
            ->add(child: 'industryDetails', type: ChoiceType::class, options: [
                'label'    => 'Branchendetails',
                'required' => false,
            ])
            ->add(child: 'industryClassificationId', type: HiddenType::class, options: ['required' => false]);

        $builder->addEventListener(
            eventName: FormEvents::PRE_SUBMIT,
            listener: function (FormEvent $event): void {
                $data = $event->getData();
                $form = $event->getForm();

                $industryUsage = $data['industryUsage'];
                $industryArea = $data['industryArea'];
                $industryDetails = $data['industryDetails'];

                if ($industryUsage !== null) {
                    $form->add(child: 'industryArea', type: EntityType::class, options: [
                        'label'         => 'Branchenbereich',
                        'class'         => IndustryClassification::class,
                        'query_builder' => function (EntityRepository $entityRepository) use ($industryUsage, $industryArea) {
                            $queryBuilder = $entityRepository->createQueryBuilder(alias: 'industryClassification');

                            return $queryBuilder
                                ->where(predicates: 'industryClassification.levelOne IS NOT NULL')
                                ->andWhere('industryClassification.levelTwo IS NOT NULL')
                                ->andWhere('industryClassification.levelThree IS NULL');
                        },
                        'choice_label' => function (IndustryClassification $industryClassification): string {
                            return $industryClassification->getName();
                        },
                        'required' => false,
                    ]);
                }

                if ($industryUsage !== null && $industryArea !== null && $industryDetails !==  null) {
                    $form->add(child: 'industryDetails', type: EntityType::class, options: [
                        'label'         => 'Branchenbereich',
                        'class'         => IndustryClassification::class,
                        'query_builder' => function (EntityRepository $entityRepository) use ($industryUsage, $industryArea, $industryDetails) {
                            $queryBuilder = $entityRepository->createQueryBuilder('industryClassification');

                            return $queryBuilder
                                ->where(predicates: 'industryClassification.levelOne IS NOT NULL')
                                ->andWhere('industryClassification.levelTwo IS NOT NULL')
                                ->andWhere('industryClassification.levelThree IS NOT NULL');
                        },
                        'choice_label' => function (IndustryClassification $industryClassification): string {
                            return $industryClassification->getName();
                        },
                        'required' => false,
                    ]);
                }
            }
        );
    }
}
