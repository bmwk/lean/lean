<?php

declare(strict_types=1);

namespace App\Form\Type\CompanyLocationManagement\LookingForPropertyRequest;

use App\Form\Type\AbstractDeleteType;

class LookingForPropertyRequestDeleteType extends AbstractDeleteType
{
}
