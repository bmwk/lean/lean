<?php

declare(strict_types=1);

namespace App\Form\Type\CompanyLocationManagement\LookingForPropertyRequest;

use App\Domain\Entity\PriceRequirement;
use App\Domain\Entity\PropertyOfferType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EnumType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PriceRequirementType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $disabled = $options['canEdit'] === false;

        $builder
            ->add(child: 'nonCommissionable', type: CheckboxType::class, options: ['label' => 'Nur provisionsfreie Angebote', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'propertyOfferTypes', type: EnumType::class, options: [
                'label'        => 'Angebotstypen',
                'required'     => false,
                'disabled'     => $disabled,
                'class'        => PropertyOfferType::class,
                'choice_label' => function (PropertyOfferType $propertyOfferType): string {
                    return $propertyOfferType->getName();
                },
                'expanded' => true,
                'multiple' => true,
            ])
            ->add(child: 'purchasePriceNet', type: ValueRequirementType::class, options: ['label' => 'Nettokaufpreis', 'canEdit' => $options['canEdit']])
            ->add(child: 'purchasePriceGross', type: ValueRequirementType::class, options: ['label' => 'Bruttokaufpreis', 'canEdit' => $options['canEdit']])
            ->add(child: 'purchasePricePerSquareMeter', type: ValueRequirementType::class, options: ['label' => 'Kaufpreis pro m²', 'canEdit' => $options['canEdit']])
            ->add(child: 'netColdRent', type: ValueRequirementType::class, options: ['label' => 'Nettokaltmiete', 'canEdit' => $options['canEdit']])
            ->add(child: 'coldRent', type: ValueRequirementType::class, options: ['label' => 'Kaltmiete', 'canEdit' => $options['canEdit']])
            ->add(child: 'rentalPricePerSquareMeter', type: ValueRequirementType::class, options:  ['label' => 'Mietpreis pro m²', 'canEdit' => $options['canEdit']])
            ->add(child: 'lease', type: ValueRequirementType::class, options: ['label' => 'Pacht', 'canEdit' => $options['canEdit']]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefaults(['data_class' => PriceRequirement::class])
            ->setRequired(['canEdit'])
            ->setAllowedTypes(option: 'canEdit', allowedTypes: ['bool']);
    }
}
