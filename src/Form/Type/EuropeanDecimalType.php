<?php

declare(strict_types=1);

namespace App\Form\Type;

use App\Form\DataTransformer\UsToEuDecimalTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class EuropeanDecimalType extends AbstractType
{
    public function __construct(
        private readonly UsToEuDecimalTransformer $usToEuDecimalTransformer
    ) {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->addModelTransformer($this->usToEuDecimalTransformer);
    }

    public function getParent(): string
    {
        return TextType::class;
    }
}
