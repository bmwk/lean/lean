<?php

declare(strict_types=1);

namespace App\Form\Type\PropertyVacancyManagement;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;

class PropertyExcelImportType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add(child: 'excelFile', type: FileType::class, options: ['label' => 'Excel-Datei']);
    }
}
