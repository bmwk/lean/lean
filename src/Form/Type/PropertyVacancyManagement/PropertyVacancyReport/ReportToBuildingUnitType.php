<?php

declare(strict_types=1);

namespace App\Form\Type\PropertyVacancyManagement\PropertyVacancyReport;

use App\Domain\Entity\Property\BarrierFreeAccess;
use App\Domain\Entity\Property\BuildingUnit;
use App\Form\Type\EuropeanDecimalType;
use App\Form\Type\PropertyVacancyManagement\Property\BuildingDetailType;
use App\Form\Type\PropertyVacancyManagement\Property\BuildingUnitCreateType;
use App\Form\Type\PropertyVacancyManagement\Property\PropertyMarketingInformationType;
use App\Form\Type\PropertyVacancyManagement\Property\PropertyOwnerType;
use App\Form\Type\PropertyVacancyManagement\Property\PropertySpacesDataType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EnumType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;

class ReportToBuildingUnitType extends BuildingUnitCreateType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        parent::buildForm(builder: $builder, options: $options);

        $builder
            ->add(child: 'propertyOwnerCreate', type: HiddenType::class, options: ['mapped' => false])
            ->add(child: 'propertyOwner', type: PropertyOwnerType::class, options: [
                'mapped'         => false,
                'account'        => $options['account'],
                'selectedPerson' => null,
                'canEdit'        => true,
                'canViewPerson'  => true,
            ])
            ->add(child: 'propertySpacesData', type: PropertySpacesDataType::class, options: ['canEdit' => true])
            ->add(child: 'building', type: BuildingDetailType::class, options: ['canEdit' => true])
            ->add(child: 'propertyMarketingInformation', type: PropertyMarketingInformationType::class, options: [
                'canEdit' => true
            ])
            ->add(child: 'numberOfParkingLots', type: NumberType::class, options: ['label' => 'Anzahl Stellplätze', 'required' => false])
            ->add(child: 'barrierFreeAccess', type: EnumType::class, options: [
                'label'        => 'Barrierefreier Zugang',
                'required'     => false,
                'class'        => BarrierFreeAccess::class,
                'choice_label' => function (BarrierFreeAccess $barrierFreeAccess): string {
                    return $barrierFreeAccess->getName();
                },
                'placeholder' => 'keine Angabe',
            ])
            ->add(child: 'groundLevelSalesArea', type: CheckboxType::class, options: [
                'label'    => 'ebenerdige Verkaufsfläche',
                'required' => false,
                'getter'   => function (BuildingUnit $buildingUnit, FormInterface $form): ?bool {
                    $reflectionClass = new \ReflectionClass($buildingUnit);

                    if ($reflectionClass->getProperty('groundLevelSalesArea')->isInitialized($buildingUnit) === false) {
                        return null;
                    }

                    return $buildingUnit->isGroundLevelSalesArea();
                },
                'setter' => function (BuildingUnit $buildingUnit, bool $groundLevelSalesArea, FormInterface $form): void {
                    $buildingUnit->setGroundLevelSalesArea($groundLevelSalesArea);
                },
            ])
            ->add(child: 'shopWindowFrontWidth', type: EuropeanDecimalType::class, options: ['label' => 'Gesamtbreite Schaufenster', 'required' => false])
            ->add(child: 'shopWidth', type: EuropeanDecimalType::class, options: ['label' => 'Ladenbreite', 'required' => false]);
    }
}
