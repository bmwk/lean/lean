<?php

declare(strict_types=1);

namespace App\Form\Type\PropertyVacancyManagement\PropertyVacancyReport;

use App\Form\Type\AbstractDeleteType;

class PropertyVacancyReportDeleteType extends AbstractDeleteType
{
}
