<?php

declare(strict_types=1);

namespace App\Form\Type\PropertyVacancyManagement\PropertyVacancyReport;

use App\Domain\Entity\PropertyVacancyReport\PropertyVacancyReportNote;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PropertyVacancyReportNoteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add(child: 'note', type: TextareaType::class, options: ['label' => 'Notiz', 'required' => true]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => PropertyVacancyReportNote::class]);
    }
}
