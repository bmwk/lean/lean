<?php

declare(strict_types=1);

namespace App\Form\Type\PropertyVacancyManagement\Property;

use App\Domain\Entity\Property\Currency;
use App\Domain\Entity\Property\PropertyPricingInformation;
use App\Form\Type\EuropeanDecimalType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EnumType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PropertyPricingInformationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $disabled = $options['canEdit'] === false;

        $builder
            ->add(child: 'currency', type: EnumType::class, options: [
                'label'        => 'Währung',
                'required'     => false,
                'disabled'     => $disabled,
                'class'        => Currency::class,
                'choice_label' => function (Currency $currency): string {
                    return $currency->getName();
                }
            ])
            ->add(child: 'purchasePriceNet', type: EuropeanDecimalType::class, options: ['label' => 'Kaufpreis Netto', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'purchasePriceGross', type: EuropeanDecimalType::class, options: ['label' => 'Kaufpreis Brutto', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'purchasePricePerSquareMeter', type: EuropeanDecimalType::class, options: ['label' => 'Kaufpreis pro m²', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'netColdRent', type: EuropeanDecimalType::class, options: ['label' => 'Nettokaltmiete', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'coldRent', type: EuropeanDecimalType::class, options: ['label' => 'Kaltmiete', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'rentIncludingHeating', type: EuropeanDecimalType::class, options: ['label' => 'Warmmiete', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'additionalCosts', type: EuropeanDecimalType::class, options: ['label' => 'monatliche Betriebskosten/Nebenkosten, ohne Heizkosten', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'heatingCostsIncluded', type: CheckboxType::class, options: ['label' => 'Heizkosten in der Warmmiete enthalten', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'rentalPricePerSquareMeter', type: EuropeanDecimalType::class, options: ['label' => 'Mietpreis pro m²', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'lease', type: EuropeanDecimalType::class, options: ['label' => 'Pacht', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'leasehold', type: EuropeanDecimalType::class, options: ['label' => 'Erbpacht', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'houseMoney', type: EuropeanDecimalType::class, options: ['label' => 'Hausgeld', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'commissionable', type: CheckboxType::class, options: ['label' => 'provisionspflichtig', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'insideCommission', type: TextType::class, options: ['label' => 'Innencourtage', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'outsideCommission', type: TextType::class, options: ['label' => 'Aussencourtage', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'commissionNet', type: EuropeanDecimalType::class, options: ['label' => 'Provision (netto)', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'commissionGross', type: EuropeanDecimalType::class, options: ['label' => 'Provision (brutto)', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'freeTextPrice', type: TextType::class, options: ['label' => 'Freitext Preis', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'deposit', type: EuropeanDecimalType::class, options: ['label' => 'Kaution', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'depositText', type: TextType::class, options: ['label' => 'Freitext Kaution', 'required' => false, 'disabled' => $disabled]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefaults(['data_class' => PropertyPricingInformation::class])
            ->setRequired(['canEdit'])
            ->setAllowedTypes(option: 'canEdit', allowedTypes: ['bool']);
    }
}
