<?php

declare(strict_types=1);

namespace App\Form\Type\PropertyVacancyManagement\Property;

use App\Domain\Entity\Property\PropertySpacesData;
use App\Domain\Entity\Property\UsableOutdoorAreaPossibility;
use App\Form\Type\EuropeanDecimalType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EnumType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PropertySpacesDataType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $disabled = $options['canEdit'] === false;

        $builder
            ->add(child: 'roomCount', type: NumberType::class, options: ['label' => 'Anzahl Räume', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'livingSpace', type: EuropeanDecimalType::class, options: ['label' => 'Wohnfläche', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'usableSpace', type: EuropeanDecimalType::class, options: ['label' => 'Nutzfläche', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'subsidiarySpace', type: EuropeanDecimalType::class, options: ['label' => 'Nebenfläche', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'storeSpace', type: EuropeanDecimalType::class, options: ['label' => 'Ladenfläche', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'storageSpace', type: EuropeanDecimalType::class, options: ['label' => 'Lagerfläche', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'retailSpace', type: EuropeanDecimalType::class, options: ['label' => 'Verkaufsfläche', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'openSpace', type: EuropeanDecimalType::class, options: ['label' => 'Freifläche', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'officeSpace', type: EuropeanDecimalType::class, options: ['label' => 'Bürofläche', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'officePartSpace', type: EuropeanDecimalType::class, options: ['label' => 'Büroteilfläche', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'administrationSpace', type: EuropeanDecimalType::class, options: ['label' => 'Verwaltungsfläche', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'gastronomySpace', type: EuropeanDecimalType::class, options: ['label' => 'Gastrofläche', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'productionSpace', type: EuropeanDecimalType::class, options: ['label' => 'Produktionsfläche', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'plotSize', type: EuropeanDecimalType::class, options: ['label' => 'Grundstücksfläche', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'otherSpace', type: EuropeanDecimalType::class, options: ['label' => 'Sonstige Fläche', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'balconyTurfSpace', type: EuropeanDecimalType::class, options: ['label' => 'Balkon/Terrasse Fläche', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'gardenSpace', type: EuropeanDecimalType::class, options: ['label' => 'Gartenfläche', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'cellarSpace', type: EuropeanDecimalType::class, options: ['label' => 'Kellerfläche', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'atticSpace', type: EuropeanDecimalType::class, options: ['label' => 'Dachbodenfläche', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'heatableSurface', type: EuropeanDecimalType::class, options: ['label' => 'beheizbare Fläche', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'rentableArea', type: EuropeanDecimalType::class, options: ['label' => 'vermietbare Fläche', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'minimumSpaceForSeparation', type: EuropeanDecimalType::class, options: ['label' => 'Objekt teilbar ab', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'usableOutdoorAreaPossibility', type: EnumType::class, options: [
                'label'        => 'Nutzbarer Außenbereich',
                'required'     => false,
                'disabled'     => $disabled,
                'class'        => UsableOutdoorAreaPossibility::class,
                'choice_label' => function (UsableOutdoorAreaPossibility $usableOutdoorAreaPossibility): string {
                    return $usableOutdoorAreaPossibility->getName();
                },
            ])
            ->add(child: 'totalOutdoorArea', type: EuropeanDecimalType::class, options: ['label' => 'Gesamtfläche Außenbereich', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'usableOutdoorArea', type: EuropeanDecimalType::class, options: ['label' => 'Nutzbare Fläche Außenbereich', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'outdoorSalesArea', type: EuropeanDecimalType::class, options: ['label' => 'Verkaufsfläche Außenbereich', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'roofingPresent', type: HiddenType::class, options: ['mapped' => false]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefaults(['data_class' => PropertySpacesData::class])
            ->setRequired(['canEdit'])
            ->setAllowedTypes(option: 'canEdit', allowedTypes: ['bool']);
    }
}
