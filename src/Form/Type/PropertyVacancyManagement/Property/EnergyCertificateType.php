<?php

declare(strict_types=1);

namespace App\Form\Type\PropertyVacancyManagement\Property;

use App\Domain\Entity\Property\EnergyCertificate;
use App\Domain\Entity\Property\EnergyCertificateHeatingType;
use App\Domain\Entity\Property\EnergyEfficiencyClass;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EnumType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EnergyCertificateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $disabled = $options['canEdit'] === false;

        $builder
            ->add(child: 'energyCertificateType', type: EnumType::class, options: [
                'label'        => 'Art des Energieausweises',
                'required'     => false,
                'disabled'     => $disabled,
                'class'        => \App\Domain\Entity\Property\EnergyCertificateType::class,
                'choice_label' => function (\App\Domain\Entity\Property\EnergyCertificateType $energyCertificateType): string {
                    return $energyCertificateType->getName();
                },
            ])
            ->add(child: 'issueDate', type: DateType::class, options: [
                'label'    => 'Ausstellungsdatum',
                'required' => false,
                'disabled' => $disabled,
                'widget'   => 'single_text',
                'format'   => 'dd.MM.yyyy',
                'html5'    => false,
            ])
            ->add(child: 'expirationDate', type: DateType::class, options: [
                'label'    => 'Gültig bis',
                'required' => false,
                'disabled' => $disabled,
                'widget'   => 'single_text',
                'format'   => 'dd.MM.yyyy',
                'html5'    => false,
            ])
            ->add(child: 'buildingConstructionYear', type: TextType::class, options: ['label' => 'Baujahr laut Energieausweis', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'energyEfficiencyClass', type: EnumType::class, options: [
                'label'        => 'Energieeffizienzklasse',
                'required'     => false,
                'disabled'     => $disabled,
                'class'        => EnergyEfficiencyClass::class,
                'choice_label' => function (EnergyEfficiencyClass $energyEfficiencyClass): string {
                    return $energyEfficiencyClass->getName();
                },
            ])
            ->add(child: 'energyCertificateHeatingTypes', type: EnumType::class, options: [
                'label'        => '',
                'required'     => false,
                'disabled'     => $disabled,
                'class'        => EnergyCertificateHeatingType::class,
                'choice_label' => function (EnergyCertificateHeatingType $energyCertificateHeatingType): string {
                    return $energyCertificateHeatingType->getName();
                },
                'expanded' => true,
                'multiple' => true,
            ])
            ->add(child: 'withHotWater', type: ChoiceType::class, options: [
                'label'    => 'incl. Warmwasser',
                'required' => false,
                'disabled' => $disabled,
                'choices'  => [
                    'Nein' => false,
                    'Ja'   => true,
                ],
            ])
            ->add(child: 'energyDemand', type: TextType::class, options: ['label' => 'Endenergiebedarf', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'electricityDemand', type: TextType::class, options: ['label' => 'Endenergiebedarf Strom', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'heatingEnergyDemand', type: TextType::class, options: ['label' => 'Endenergiebedarf Wärme', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'energyConsumption', type: TextType::class, options: ['label' => 'Energieverbrauchkennwert', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'electricityConsumption', type: TextType::class, options: ['label' => 'Stromverbrauchskennwert', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'heatingEnergyConsumption', type: TextType::class, options: ['label' => ' Heizenergieverbrauchskennwert', 'required' => false, 'disabled' => $disabled]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefaults(['data_class' => EnergyCertificate::class])
            ->setRequired(['canEdit'])
            ->setAllowedTypes(option: 'canEdit', allowedTypes: ['bool']);
    }
}
