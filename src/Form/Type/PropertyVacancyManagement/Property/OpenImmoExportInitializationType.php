<?php

declare(strict_types=1);

namespace App\Form\Type\PropertyVacancyManagement\Property;

use App\Domain\Entity\OpenImmoExporter\ExportActionType;
use App\Domain\Entity\OpenImmoFtpTransfer\OpenImmoExportInitialization;
use App\Domain\Entity\OpenImmoFtpTransfer\OpenImmoFtpCredential;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EnumType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OpenImmoExportInitializationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(child: 'openImmoFtpCredentials', type: EntityType::class, options: [
                'class'        => OpenImmoFtpCredential::class,
                'choice_label' => function (OpenImmoFtpCredential $openImmoFtpCredential): string {
                    return $openImmoFtpCredential->getPortalName();
                },
                'expanded' => true,
                'multiple' => true,
            ])
            ->add(child: 'exportActionType', type: EnumType::class, options: [
                'label'    => 'Aktion',
                'required' => true,
                'class'    => ExportActionType::class,
                'choices'  => [
                    ExportActionType::CHANGE,
                    ExportActionType::DELETE,
                ],
                'choice_label' => function (ExportActionType $exportActionType): string {
                    return $exportActionType->getDescription();
                },
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => OpenImmoExportInitialization::class]);
    }
}
