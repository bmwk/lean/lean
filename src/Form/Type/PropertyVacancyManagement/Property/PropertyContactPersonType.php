<?php

declare(strict_types=1);

namespace App\Form\Type\PropertyVacancyManagement\Property;

use App\Domain\Entity\Account;
use App\Domain\Entity\Person\Person;
use App\Domain\Entity\Property\PropertyContactPerson;
use App\Domain\Entity\Property\PropertyContactResponsibility;
use App\Domain\Entity\Property\VisibleContactInformation;
use App\Form\Type\PersonManagement\Person\PersonSelectOrPersonCreateType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EnumType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PropertyContactPersonType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $disabled = $options['canEdit'] === false;

        if ($options['canViewPerson'] === true) {
            $builder->add(child: 'personSelectOrPersonCreate', type: PersonSelectOrPersonCreateType::class, options: [
                'mapped'         => false,
                'account'        => $options['account'],
                'selectedPerson' => $options['selectedPerson'],
                'canEdit'        => $options['canEdit'],
            ]);
        }

        $builder
            ->add(child: 'propertyContactResponsibility', type: EnumType::class, options: [
                'label'        => 'Ansprechperson für',
                'required'     => false,
                'disabled'     => $disabled,
                'class'        => PropertyContactResponsibility::class,
                'choice_label' => function (PropertyContactResponsibility $propertyContactResponsibility): string {
                    return $propertyContactResponsibility->getName();
                },
            ])
            ->add(child: 'visibleContactInformations', type: EnumType::class, options: [
                'label'        => 'Öffentliche Daten',
                'required'     => false,
                'disabled'     => $disabled,
                'class'        => VisibleContactInformation::class,
                'choice_label' => function (VisibleContactInformation $visibleContactInformation): string {
                    return $visibleContactInformation->getName();
                },
                'expanded' => true,
                'multiple' => true,
            ])
            ->addEventListener(eventName: FormEvents::POST_SUBMIT, listener: function (FormEvent $event) use ($options): void {
                $data = $event->getData();
                $form = $event->getForm();

                $person = $form->get('personSelectOrPersonCreate')->getData();

                if ($person !== null) {
                    $data->setPerson($person);
                }
            });
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefaults(['data_class' => PropertyContactPerson::class])
            ->setRequired(['account', 'selectedPerson', 'canEdit', 'canViewPerson'])
            ->setAllowedTypes(option: 'account', allowedTypes: Account::class)
            ->setAllowedTypes(option: 'selectedPerson', allowedTypes: [Person::class, 'null'])
            ->setAllowedTypes(option: 'canEdit', allowedTypes: ['bool'])
            ->setAllowedTypes(option: 'canViewPerson', allowedTypes: ['bool']);
    }
}
