<?php

declare(strict_types=1);

namespace App\Form\Type\PropertyVacancyManagement\Property;

use App\Domain\Entity\Account;
use App\Domain\Entity\Person\Person;
use App\Domain\Entity\Property\BranchType;
use App\Domain\Entity\Property\PropertyRelationship;
use App\Domain\Entity\Property\PropertyUser;
use App\Domain\Entity\Property\PropertyUserStatus;
use App\Form\Type\Classification\IndustryNaceClassificationType;
use App\Form\Type\PersonManagement\Person\PersonSelectOrPersonCreateType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EnumType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PropertyUserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $disabled = $options['canEdit'] === false;

        if ($options['canViewPerson'] === true) {
            $builder->add(child: 'personSelectOrPersonCreate', type: PersonSelectOrPersonCreateType::class, options: [
                'mapped'         => false,
                'account'        => $options['account'],
                'selectedPerson' => $options['selectedPerson'],
                'canEdit'        => $options['canEdit'],
            ]);
        }

        $builder
            ->add(child: 'propertyUserStatus', type: EnumType::class, options: [
                'label'        => 'Status des Nutzungsverhältnisses',
                'required'     => true,
                'disabled'     => $disabled,
                'class'        => PropertyUserStatus::class,
                'choice_label' => function (PropertyUserStatus $propertyUserStatus): string {
                    return $propertyUserStatus->getName();
                },
            ])
            ->add(child: 'isSubtenant', type: CheckboxType::class, options: ['label' => 'ist Untermieter', 'disabled' => $disabled])
            ->add(child: 'propertyRelationship', type: EnumType::class, options: [
                'label'        => 'Verhältnis zum Eigentum',
                'required'     => true,
                'disabled'     => $disabled,
                'class'        => PropertyRelationship::class,
                'choice_label' => function (PropertyRelationship $propertyRelationship): string {
                    return $propertyRelationship->getName();
                },
            ])
            ->add(child: 'endOfRental', type: DateType::class, options: [
                'label'    => 'Ende des Miet-/Pachtvertrages',
                'required' => false,
                'disabled' => $disabled,
                'widget'   => 'single_text',
                'format'   => 'dd.MM.yyyy',
                'html5'    => false,
            ])
            ->add(child: 'section', type: TextType::class, options: ['label' => 'Sparte', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'industryName', type: TextType::class, options: ['label' => 'Branche', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'branch', type: CheckboxType::class, options: ['label' => 'Filiale', 'disabled' => $disabled])
            ->add(child: 'branchType', type: EnumType::class, options: [
                'label'        => 'Art der Filiale',
                'required'     => false,
                'disabled'     => $disabled,
                'class'        => BranchType::class,
                'choice_label' => function (BranchType $branchType): string {
                    return $branchType->getName();
                },
            ])
            ->add(child: 'mainAssortment', type: TextType::class, options: ['label' => 'Hauptsortiment', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'sideAssortment', type: TextType::class, options: ['label' => 'Nebensortiment', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'openingHours', type: TextareaType::class, options: ['label' => 'Öffnungszeiten', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'familybusiness', type: CheckboxType::class, options: ['label' => 'Familienbetrieb', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'deliveryService', type: TextType::class, options: ['label' => 'Lieferservice / Click & Collect', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'rating', type: TextType::class, options: ['label' => 'Bewertung', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'website', type: TextType::class, options: ['label' => 'Webseite', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'googleBusiness', type: TextType::class, options: ['label' => 'Google Business', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'facebook', type: TextType::class, options: ['label' => 'Facebook', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'instagram', type: TextType::class, options: ['label' => 'Instagram', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'receiveNewsletter', type: CheckboxType::class, options: ['label' => 'ist Newsletter-Empfänger', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'industryNaceClassification', type: IndustryNaceClassificationType::class, options: ['mapped' => false, 'canEdit' => $options['canEdit']])
            ->addEventListener(eventName: FormEvents::POST_SUBMIT, listener: function (FormEvent $event) use ($options): void {
                $data = $event->getData();
                $form = $event->getForm();

                $person = $form->get('personSelectOrPersonCreate')->getData();

                if ($person !== null) {
                    $data->setPerson($person);
                }
            });
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefaults(['data_class' => PropertyUser::class])
            ->setRequired(['account', 'selectedPerson', 'canEdit', 'canViewPerson'])
            ->setAllowedTypes(option: 'account', allowedTypes: Account::class)
            ->setAllowedTypes(option: 'selectedPerson', allowedTypes: [Person::class, 'null'])
            ->setAllowedTypes(option: 'canEdit', allowedTypes: ['bool'])
            ->setAllowedTypes(option: 'canViewPerson', allowedTypes: ['bool']);
    }
}
