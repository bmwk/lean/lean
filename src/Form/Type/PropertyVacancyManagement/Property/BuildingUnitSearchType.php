<?php

declare(strict_types=1);

namespace App\Form\Type\PropertyVacancyManagement\Property;

use App\Domain\Entity\Property\BuildingUnitSearch;
use App\Form\Type\AbstractSearchType;
use App\SessionStorage\SessionStorageService;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class BuildingUnitSearchType extends AbstractSearchType
{
    public function __construct(
        SessionStorageService $sessionStorageService,
        RequestStack $requestStack,
        UrlGeneratorInterface $router
    ) {
        parent::__construct(
            sessionStorageService: $sessionStorageService,
            requestStack: $requestStack,
            router: $router
        );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => BuildingUnitSearch::class]);

        parent::configureOptions(resolver: $resolver);
    }
}
