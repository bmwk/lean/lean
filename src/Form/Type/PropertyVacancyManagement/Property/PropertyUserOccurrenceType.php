<?php

declare(strict_types=1);

namespace App\Form\Type\PropertyVacancyManagement\Property;

use App\Domain\Entity\Person\Occurrence;
use App\Domain\Entity\Person\Person;
use App\Domain\Entity\Property\PropertyUser;
use App\Form\Type\PersonManagement\Occurrence\AbstractOccurrenceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PropertyUserOccurrenceType extends AbstractOccurrenceType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $disabled = $options['canEdit'] === false;

        parent::buildForm(builder: $builder, options: $options);

        $builder->add(child: 'propertyUser', type: EntityType::class, options: [
            'label'        => 'Nutzer',
            'required'     => true,
            'disabled'     => $disabled,
            'class'        => PropertyUser::class,
            'choices'      => self::filterPropertyUsersWithDeletedPerson(occurrence: $builder->getData(), propertyUsers: $options['propertyUsers']),
            'data'         => self::fetchSelectedPropertyUser(occurrence: $builder->getData(), propertyUsers: $options['propertyUsers']),
            'choice_label' => function (PropertyUser $propertyUser) {
                return $propertyUser->getPerson()->buildShortDisplayName();
            },
            'mapped' => false,
        ]);

        $builder->addEventListener(
            eventName: FormEvents::SUBMIT,
            listener: function (FormEvent $event): void {
                /** @var Occurrence $data */
                $data = $event->getData();

                $form = $event->getForm();

                $data->setPerson($form->get('propertyUser')->getData()->getPerson());

                $data->getPropertyUsers()->clear();

                $data->getPropertyUsers()->add($form->get('propertyUser')->getData());
            }
        );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        parent::configureOptions(resolver: $resolver);

        $resolver
            ->setRequired(['propertyUsers'])
            ->setAllowedTypes(option: 'propertyUsers', allowedTypes: ['array']);
    }

    private static function fetchSelectedPropertyUser(?Occurrence $occurrence, array $propertyUsers): ?PropertyUser
    {
        if ($occurrence === null) {
            return null;
        }

        if ($occurrence->getPropertyUsers()->count() === 1) {
            return $occurrence->getPropertyUsers()->first();
        }

        return self::findPropertyUserByPerson(person: $occurrence->getPerson(), propertyUsers: $propertyUsers);
    }

    /**
     * @param PropertyUser[] $propertyUsers
     */
    public static function findPropertyUserByPerson(Person $person, array $propertyUsers): ?PropertyUser
    {
        $propertyUsers = array_filter(
            array: $propertyUsers,
            callback: function (PropertyUser $propertyUser) use ($person): bool {
                return ($propertyUser->getPerson() === $person);
            }
        );

        if (count($propertyUsers) > 0) {
            return array_values($propertyUsers)[0];
        }

        return null;
    }

    /**
     * @param PropertyUser[] $propertyUsers
     * @return PropertyUser[]
     */
    public static function filterPropertyUsersWithDeletedPerson(?Occurrence $occurrence, array $propertyUsers): array
    {
        $propertyUser = self::fetchSelectedPropertyUser(occurrence: $occurrence, propertyUsers: $propertyUsers);

        $selectedPerson = $propertyUser?->getPerson();

        return array_filter(
            array: $propertyUsers,
            callback: function (PropertyUser $propertyUser) use ($selectedPerson): bool {
                if ($propertyUser->getPerson()->isDeleted() === false) {
                    return true;
                }

                if ($selectedPerson !== null && $propertyUser->getPerson() === $selectedPerson) {
                    return true;
                }

                return false;
            }
        );
    }
}
