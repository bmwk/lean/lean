<?php

declare(strict_types=1);

namespace App\Form\Type\PropertyVacancyManagement\Property;

use App\Domain\Entity\Property\PropertyRestriction;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EnumType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PropertyRestrictionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $disabled = $options['canEdit'] === false;

        $builder
            ->add(child: 'propertyRestrictionType', type: EnumType::class, options: [
                'label'        => 'Restriktion',
                'required'     => true,
                'disabled'     => $disabled,
                'class'        => \App\Domain\Entity\Property\PropertyRestrictionType::class,
                'choice_label' => function (\App\Domain\Entity\Property\PropertyRestrictionType $propertyRestrictionType): string {
                    return $propertyRestrictionType->getName();
                },
            ])
            ->add(child: 'description', type: TextareaType::class, options: ['label'  => 'Beschreibung', 'required' => true, 'disabled' => $disabled]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefaults(['data_class' => PropertyRestriction::class])
            ->setRequired(['canEdit'])
            ->setAllowedTypes(option: 'canEdit', allowedTypes: ['bool']);
    }
}
