<?php

declare(strict_types=1);

namespace App\Form\Type\PropertyVacancyManagement\Property;

use App\Domain\Entity\Account;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BuildingUnitCreateType extends AbstractBuildingUnitType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        parent::buildForm(builder: $builder, options: $options);

        $builder
            ->add(child: 'propertyUserCreate', type: HiddenType::class, options: ['mapped' => false])
            ->add(child: 'propertyUser', type: PropertyUserType::class, options: [
                'mapped'         => false,
                'account'        => $options['account'],
                'selectedPerson' => null,
                'canEdit'        => true,
                'canViewPerson'  => true,
            ])
            ->add(child: 'longitude', type: HiddenType::class, options: ['mapped' => false])
            ->add(child: 'latitude', type: HiddenType::class, options: ['mapped' => false]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        parent::configureOptions(resolver: $resolver);

        $resolver
            ->setRequired(['account'])
            ->setAllowedTypes(option: 'account', allowedTypes: Account::class);
    }
}
