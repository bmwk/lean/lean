<?php

declare(strict_types=1);

namespace App\Form\Type\PropertyVacancyManagement\Property;

use App\Domain\Entity\Classification\IndustryClassification;
use App\Domain\Entity\Property\DevelopmentPotential;
use App\Domain\Entity\Property\PropertyMarketingInformation;
use App\Domain\Entity\Property\PropertyMarketingStatus;
use App\Domain\Entity\Property\PropertyPriceType;
use App\Domain\Entity\PropertyOfferType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EnumType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PropertyMarketingInformationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $disabled = $options['canEdit'] === false;

        if ($options['canEdit'] === true) {
            $builder->add(child: 'propertyRestrictionAddButton', type: ButtonType::class);
        }

        $builder
            ->add(child: 'propertyPricingInformation', type: PropertyPricingInformationType::class, options: [
                'canEdit' => $options['canEdit'],
            ])
            ->add(child: 'propertyMarketingStatus', type: EnumType::class, options: [
                'label'    => 'Status',
                'required' => false,
                'disabled' => $disabled,
                'class'    => PropertyMarketingStatus::class,
                'choices'  => [
                    PropertyMarketingStatus::ACQUISITION_OPEN,
                    PropertyMarketingStatus::ACQUISITION_COMPLETED,
                    PropertyMarketingStatus::DIALOG_WITH_OWNER,
                    PropertyMarketingStatus::RESUBMISSION,
                    PropertyMarketingStatus::MARKETING_INTERNAL,
                    PropertyMarketingStatus::MARKETING_PUBLIC,
                    PropertyMarketingStatus::SUCCESSFULLY_MARKETED,
                ],
                'choice_label' => function (PropertyMarketingStatus $propertyMarketingStatus): string {
                    return $propertyMarketingStatus->getName();
                }
            ])
            ->add(child: 'propertyOfferType', type: EnumType::class, options: [
                'label'        => 'Angebotsart',
                'required'     => false,
                'disabled'     => $disabled,
                'class'        => PropertyOfferType::class,
                'choice_label' => function (PropertyOfferType $propertyOfferType): string {
                    return $propertyOfferType->getName();
                }
            ])
            ->add(child: 'propertyPriceType', type: EnumType::class, options: [
                'label'        => 'Preisart',
                'required'     => false,
                'disabled'     => $disabled,
                'class'        => PropertyPriceType::class,
                'choice_label' => function (PropertyPriceType $propertyPriceType): string {
                    return $propertyPriceType->getName();
                }
            ])
            ->add(child: 'availableFrom', type: DateType::class, options: [
                'label'    => 'Bezugsfrei ab',
                'required' => false,
                'disabled' => $disabled,
                'widget'   => 'single_text',
                'format'   => 'dd.MM.yyyy',
                'html5'    => false,
            ])
            ->add(child: 'developmentPotential', type: EnumType::class, options: [
                'label'        => 'Entwicklungspotenzial',
                'required'     => false,
                'disabled'     => $disabled,
                'class'        => DevelopmentPotential::class,
                'choice_label' => function (DevelopmentPotential $developmentPotential): string {
                    return $developmentPotential->getName();
                }
            ])
            ->add(child: 'potentialDescription', type: TextareaType::class, options: ['label' => 'Beschreibung', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'possibleUsageIndustryClassifications', type: EntityType::class, options: [
                'label'         => 'Mögliche Nutzung',
                'required'      => false,
                'disabled'      => $disabled,
                'class'         => IndustryClassification::class,
                'query_builder' => function (EntityRepository $entityRepository) {
                    $queryBuilder = $entityRepository->createQueryBuilder('ic');
                    return $queryBuilder->where($queryBuilder->expr()->isNull('ic.levelTwo'))
                        ->andWhere($queryBuilder->expr()->isNull('ic.levelThree'));
                },
                'choice_label' => 'name',
                'expanded'     => true,
                'multiple'     => true,
            ])
            ->add(child: 'environmentDescription', type: TextareaType::class, options: ['label' => 'Beschreibung', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'propertyRestrictions', type: CollectionType::class, options: [
                'entry_type'    => PropertyRestrictionType::class,
                'entry_options' => ['label' => false, 'canEdit' => $options['canEdit']],
                'allow_add'     => true,
                'allow_delete'  => true,
                'prototype'     => true,
            ])
            ->add(child: 'groundReferenceValue', type: NumberType::class, options: ['label' => 'Bodenrichtwert', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'groundValue', type: NumberType::class, options: ['label' => 'Bodenwert', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'marketValue', type: NumberType::class, options: ['label' => 'Verkehrswert', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'valueAffectingCircumstances', type: TextType::class, options: ['label' => 'wertbeeinflussende Umstände', 'required' => false, 'disabled' => $disabled]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefaults(['data_class' => PropertyMarketingInformation::class])
            ->setRequired(['canEdit'])
            ->setAllowedTypes(option: 'canEdit', allowedTypes: ['bool']);
    }
}
