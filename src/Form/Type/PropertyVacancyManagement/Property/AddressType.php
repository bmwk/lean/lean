<?php

declare(strict_types=1);

namespace App\Form\Type\PropertyVacancyManagement\Property;

use App\Domain\Entity\Place;
use App\Domain\Entity\PlaceType;
use App\Domain\Entity\Property\Address;
use App\Domain\Location\LocationService;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddressType extends AbstractType
{
    public function __construct(
        private readonly LocationService $locationService
    ) {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $disabled = $options['canEdit'] === false;

        if (count($options['places']) === 1) {
            $builder->add(child: 'place', type: EntityType::class, options: [
                'label'        => 'Ort',
                'required'     => false,
                'disabled'     => true,
                'class'        => Place::class,
                'choices'      => $options['places'],
                'choice_label' => 'placeName',
            ]);
        } else {
            $builder->add(child: 'place', type: EntityType::class, options: [
                'label'        => 'Ort',
                'required'     => true,
                'disabled'     => $disabled,
                'class'        => Place::class,
                'choices'      => $options['places'],
                'choice_label' => 'placeName',
            ]);
        }

        $builder
            ->add(child: 'postalCode', type: ChoiceType::class, options: [
                'label'        => 'PLZ',
                'required'     => true,
                'disabled'     => $disabled,
                'choice_label' => function (string $postalCode): string {
                    return $postalCode;
                },
                'choices' => array_map(
                    callback: function (Place $place): string {
                        return $place->getPlaceName();
                    },
                    array: array_filter(
                        array: $options['places'][0]->getChildrenPlaces()->getValues(),
                        callback: function (Place $place): bool {
                            if ($place->getPlaceType() === PlaceType::POSTAL_CODE) {
                                return true;
                            }

                            return false;
                        }
                    )
                ),
            ])
            ->add(child: 'quarterPlace', type: EntityType::class, options: [
                'label'        => 'Orts-/Stadtteil',
                'required'     => false,
                'disabled'     => $disabled,
                'class'        => Place::class,
                'choices'      => $this->locationService->fetchCityQuartersFromPlace(place: $options['places'][0]),
                'choice_label' => 'placeName',
            ])
            ->add(child: 'streetName', type: TextType::class, options: ['label' => 'Straße', 'required' => true, 'disabled' => $disabled])
            ->add(child: 'houseNumber', type: TextType::class, options: ['label' => 'Hausnummer', 'required' => true, 'disabled' => $disabled])
            ->add(child: 'additionalAddressInformation', type: TextareaType::class, options: ['label' => 'Adresszusatz', 'required' => false, 'disabled' => $disabled]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefaults(['data_class' => Address::class])
            ->setRequired(['places', 'canEdit'])
            ->setAllowedTypes(option: 'places', allowedTypes: ['array'])
            ->setAllowedTypes(option: 'canEdit', allowedTypes: ['bool']);
    }
}
