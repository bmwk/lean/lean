<?php

declare(strict_types=1);

namespace App\Form\Type\PropertyVacancyManagement\Property;

use App\Form\Type\AbstractMassOperationType;

class BuildingUnitMassOperationUnarchiveType extends AbstractMassOperationType
{
}
