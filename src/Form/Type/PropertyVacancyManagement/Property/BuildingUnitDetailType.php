<?php

declare(strict_types=1);

namespace App\Form\Type\PropertyVacancyManagement\Property;

use App\Domain\Entity\Property\BarrierFreeAccess;
use App\Domain\Entity\Property\BuildingUnit;
use App\Domain\Entity\Property\HeatingMethod;
use App\Domain\Entity\Property\InterimUseType;
use App\Form\Type\EuropeanDecimalType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EnumType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BuildingUnitDetailType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $disabled = $options['canEdit'] === false;

        $builder
            ->add(child: 'heatingMethod', type: EnumType::class, options: [
                'label'        => 'Heizungsart',
                'required'     => false,
                'disabled'     => $disabled,
                'class'        => HeatingMethod::class,
                'choice_label' => function (HeatingMethod $heatingMethod): string {
                    return $heatingMethod->getName();
                },
                'placeholder' => 'keine Angabe',
            ])
            ->add(child: 'numberOfParkingLots', type: NumberType::class, options: ['label' => 'Anzahl Stellplätze', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'barrierFreeAccess', type: EnumType::class, options: [
                'label'        => 'Barrierefreier Zugang',
                'required'     => false,
                'disabled'     => $disabled,
                'class'        => BarrierFreeAccess::class,
                'choice_label' => function (BarrierFreeAccess $barrierFreeAccess): string {
                    return $barrierFreeAccess->getName();
                },
                'placeholder' => 'keine Angabe',
            ])
            ->add(child: 'wheelchairAccessible', type: CheckboxType::class, options: ['label' => 'Rollstuhlgerecht', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'rampPresent', type: CheckboxType::class, options: ['label' => 'Rampe vorhanden', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'entranceDoorWidth', type: EuropeanDecimalType::class, options: ['label' => 'Breite der Eingangstür', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'shopWindowFrontWidth', type: EuropeanDecimalType::class, options: ['label' => 'Gesamtbreite Schaufenster', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'shopWidth', type: EuropeanDecimalType::class, options: ['label' => 'Ladenbreite', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'shopWindowAvailable', type: CheckboxType::class, options: ['label' => 'Schaufenster vorhanden', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'groundLevelSalesArea', type: CheckboxType::class, options: ['label' => 'ebenerdige Verkaufsfläche', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'showroomAvailable', type: CheckboxType::class, options: ['label' => 'Showroom vorhanden', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'airConditioned', type: CheckboxType::class, options: ['label' => 'klimatisierte Räume', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'particularities', type: TextareaType::class, options: ['label' => 'Besonderheiten', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'inFloors', type: TextType::class, options: [
                'label'    => 'Objekt in Etage',
                'required' => false,
                'disabled' => $disabled,
                'getter'   => function (BuildingUnit $buildingUnit, FormInterface $form): ?string {
                    if (empty($buildingUnit->getInFloors()) === false) {
                        return $buildingUnit->getInFloors()[0];
                    } else {
                        return null;
                    }
                },
                'setter' => function (BuildingUnit $buildingUnit, ?string $inFloor, FormInterface $form): void {
                    if ($inFloor === null) {
                        $buildingUnit->setInFloors([]);
                    } else {
                        $buildingUnit->setInFloors([$inFloor]);
                    }
                },
            ])
            ->add(child: 'neighbourBusiness', type: TextType::class, options: ['label' => 'Nachbargeschäfte', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'passantFrequencyGenerator', type: TextType::class, options: ['label' => 'Große Frequenzbringer in der Umgebung', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'building', type: BuildingDetailType::class, options: ['canEdit' => $options['canEdit']])
            ->add(child: 'propertySpacesData', type: PropertySpacesDataType::class, options: ['canEdit' => $options['canEdit']])
            ->add(child: 'interimUsePossible', type: HiddenType::class, options: ['label' => 'Zwischennutzung des Objekts möglich', 'required' => false])
            ->add(child: 'interimUseInformation', type: TextareaType::class, options: ['label' => 'Informationen zur Zwischennutzung', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'interimUseTypes', type: EnumType::class, options: [
                'label'        => 'Objekt geeignet für/als',
                'required'     => false,
                'disabled'     => $disabled,
                'class'        => InterimUseType::class,
                'choice_label' => function (InterimUseType $interimUseType): string {
                    return $interimUseType->getName();
                },
                'expanded' => true,
                'multiple' => true,
            ])
            ->add(child: 'energyCertificateIsAvailable', type: HiddenType::class, options: ['mapped' => false])
            ->add(child: 'energyCertificate', type: EnergyCertificateType::class, options: ['canEdit' => $options['canEdit']])
            ->add(child: 'objectForeclosed', type: HiddenType::class, options: [
                'label'      => 'Objekt wird zwangsversteigert',
                'required'   => false,
                'empty_data' => false,
            ])
            ->add(child: 'fileNumberForeclosure', type: TextType::class, options: ['label' => 'Aktenzeichen Zwangsversteigerung', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'auctionDate', type: DateType::class, options: [
                'label'    => 'Versteigerungstermin',
                'required' => false,
                'disabled' => $disabled,
                'widget'   => 'single_text',
                'format'   => 'dd.MM.yyyy',
                'html5'    => false,
            ])
            ->add(child: 'responsibleLocalCourt', type: TextType::class, options: ['label' => 'zuständiges Amtsgericht', 'required' => false, 'disabled' => $disabled]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefaults(['data_class' => BuildingUnit::class])
            ->setRequired(['canEdit'])
            ->setAllowedTypes(option: 'canEdit', allowedTypes: ['bool']);
    }
}
