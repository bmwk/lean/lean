<?php

declare(strict_types=1);

namespace App\Form\Type\PropertyVacancyManagement\Property;

use App\Domain\Entity\Classification\IndustryClassification;
use App\Domain\Entity\Property\BuildingUnit;
use App\Domain\Entity\Property\PropertyUsageBusinessType;
use App\Domain\Entity\Property\VacancyReason;
use App\Form\Type\EuropeanDecimalType;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EnumType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

abstract class AbstractBuildingUnitType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $disabled = $options['canEdit'] === false;

        $builder
            ->add(child: 'name', type: TextType::class, options: ['label' => 'Bezeichnung', 'required' => true, 'disabled' => $disabled])
            ->add(child: 'internalNumber', type: TextType::class, options: ['label' => 'interne Referenz-Nummer', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'areaSize', type: EuropeanDecimalType::class, options: ['label' => 'Gesamtfläche', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'description', type: TextareaType::class, options: ['label' => 'Beschreibung für das Exposé', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'objectIsEmpty', type: CheckboxType::class, options: ['label' => 'Objekt steht leer', 'disabled' => $disabled])
            ->add(child: 'objectIsEmptySince', type: DateType::class, options: [
                'label'    => 'Leerstand seit',
                'required' => false,
                'disabled' => $disabled,
                'widget'   => 'single_text',
                'format'   => 'dd.MM.yyyy',
                'html5'    => false,
            ])
            ->add(child: 'objectBecomesEmpty', type: CheckboxType::class, options: ['label' => 'Objekt fällt leer', 'disabled' => $disabled])
            ->add(child: 'objectBecomesEmptyFrom', type: DateType::class, options: [
                'label'    => 'Leerstand ab',
                'required' => false,
                'disabled' => $disabled,
                'widget'   => 'single_text',
                'format'   => 'dd.MM.yyyy',
                'html5'    => false,
            ])
            ->add(child: 'reuseAgreed', type: CheckboxType::class, options: ['label' => 'Nachnutzung vereinbart', 'disabled' => $disabled])
            ->add(child: 'reuseFrom', type: DateType::class, options: [
                'label'    => 'Nachnutzung ab',
                'required' => false,
                'disabled' => $disabled,
                'widget'   => 'single_text',
                'format'   => 'dd.MM.yyyy',
                'html5'    => false,
            ])
            ->add(child: 'currentUsageIndustryClassification', type: EntityType::class, options: [
                'label'         => 'Aktuelle Nutzung',
                'disabled'      => $disabled,
                'class'         => IndustryClassification::class,
                'choice_label'  => 'name',
                'query_builder' => function (EntityRepository $entityRepository): QueryBuilder {
                    $queryBuilder = $entityRepository->createQueryBuilder(alias: 'ic');

                    return $queryBuilder
                        ->where($queryBuilder->expr()->isNull('ic.levelTwo'))
                        ->andWhere($queryBuilder->expr()->isNull('ic.levelThree'));
                },
            ])
            ->add(child: 'pastUsageIndustryClassification', type: EntityType::class, options: [
                'label'         => 'Ehemalige Nutzung',
                'disabled'      => $disabled,
                'class'         => IndustryClassification::class,
                'choice_label'  => 'name',
                'query_builder' => function (EntityRepository $entityRepository): QueryBuilder {
                    $queryBuilder = $entityRepository->createQueryBuilder(alias: 'ic');

                    return $queryBuilder
                        ->where($queryBuilder->expr()->isNull('ic.levelTwo'))
                        ->andWhere($queryBuilder->expr()->isNull('ic.levelThree'));
                },
            ])
            ->add(child: 'futureUsageIndustryClassification', type: EntityType::class, options: [
                'label'         => 'Zukünftige Nutzung',
                'disabled'      => $disabled,
                'class'         => IndustryClassification::class,
                'choice_label'  => 'name',
                'query_builder' => function (EntityRepository $entityRepository): QueryBuilder {
                    $queryBuilder = $entityRepository->createQueryBuilder(alias: 'ic');

                    return $queryBuilder
                        ->where($queryBuilder->expr()->isNull('ic.levelTwo'))
                        ->andWhere($queryBuilder->expr()->isNull('ic.levelThree'));
                },
            ])
            ->add(child: 'pastPropertyUsageBusinessType', type: EnumType::class, options: [
                'label'        => 'Ehemalige Betriebsform',
                'disabled'     => $disabled,
                'class'        => PropertyUsageBusinessType::class,
                'choice_label' => function (PropertyUsageBusinessType $propertyUsageBusinessType): string {
                    return $propertyUsageBusinessType->getName();
                },
            ])
            ->add(child: 'currentPropertyUsageBusinessType', type: EnumType::class, options: [
                'label'        => 'Aktuelle Betriebsform',
                'disabled'     => $disabled,
                'class'        => PropertyUsageBusinessType::class,
                'choice_label' => function (PropertyUsageBusinessType $propertyUsageBusinessType): string {
                    return $propertyUsageBusinessType->getName();
                },
            ])
            ->add(child: 'futurePropertyUsageBusinessType', type: EnumType::class, options: [
                'label'        => 'Zukünftige Betriebsform',
                'disabled'     => $disabled,
                'class'        => PropertyUsageBusinessType::class,
                'choice_label' => function (PropertyUsageBusinessType $propertyUsageBusinessType): string {
                    return $propertyUsageBusinessType->getName();
                },
            ])
            ->add(child: 'pastUsageDescription', type: TextareaType::class, options: ['label' => 'Weitere Informationen zur ehemaligen Nutzung', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'currentUsageDescription', type: TextareaType::class, options: ['label' => 'Weitere Informationen zur aktuellen Nutzung', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'futureUsageDescription', type: TextareaType::class, options: ['label' => 'Weitere Informationen zur zukünftigen Nutzung', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'vacancyReason', type: EnumType::class, options: [
                'label'        => 'Leerstandsgrund',
                'disabled'     => $disabled,
                'class'        => VacancyReason::class,
                'choices'      => [
                    VacancyReason::BUSINESS_CLOSURE,
                    VacancyReason::FUTURE_CONTRACT,
                    VacancyReason::INTENTIONAL,
                    VacancyReason::UNECONOMIC,
                    VacancyReason::STRUCTURAL_REASON,
                    VacancyReason::DEMOLITION,
                    VacancyReason::RECONSTRUCTION,
                    VacancyReason::RENOVATION,
                    VacancyReason::OTHER,
                ],
                'choice_label' => function (VacancyReason $vacancyReason): string {
                    return $vacancyReason->getName();
                },
            ])
            ->add(child: 'keyProperty', type: CheckboxType::class, options: ['label' => 'Objekt hat eine Ankerfunktion bzw. ist eine Schlüsselimmobilie', 'disabled' => $disabled])
            ->add(child: 'address', type: AddressType::class, options: [
                'places'  => $options['places'],
                'canEdit' => $options['canEdit'],
            ])
            ->add(child: 'placeDescription', type: TextareaType::class, options: ['label' => 'Lagebeschreibung für das Exposé', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'internalNote', type: TextareaType::class, options: ['label' => 'Notizen', 'required' => false, 'disabled' => $disabled]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefaults(['data_class' => BuildingUnit::class])
            ->setRequired(['places', 'canEdit'])
            ->setAllowedTypes(option: 'places', allowedTypes: ['array'])
            ->setAllowedTypes(option: 'canEdit', allowedTypes: ['bool']);
    }
}
