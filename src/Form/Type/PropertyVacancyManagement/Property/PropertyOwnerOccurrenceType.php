<?php

declare(strict_types=1);

namespace App\Form\Type\PropertyVacancyManagement\Property;

use App\Domain\Entity\Person\Occurrence;
use App\Domain\Entity\Person\Person;
use App\Domain\Entity\Property\PropertyOwner;
use App\Form\Type\PersonManagement\Occurrence\AbstractOccurrenceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PropertyOwnerOccurrenceType extends AbstractOccurrenceType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        parent::buildForm(builder: $builder, options: $options);

        $disabled = $options['canEdit'] === false;

        $builder->add(child: 'propertyOwner', type: EntityType::class, options: [
            'label'        => 'Eigentümer',
            'required'     => true,
            'disabled'     => $disabled,
            'class'        => PropertyOwner::class,
            'choices'      => self::filterPropertyOwnersWithDeletedPerson(occurrence: $builder->getData(), propertyOwners: $options['propertyOwners']),
            'data'         => self::fetchSelectedPropertyOwner(occurrence: $builder->getData(), propertyOwners: $options['propertyOwners']),
            'choice_label' => function (PropertyOwner $propertyOwner) {
                return $propertyOwner->getPerson()->buildShortDisplayName();
            },
            'mapped' => false,
        ]);

        $builder->addEventListener(
            eventName: FormEvents::SUBMIT,
            listener: function (FormEvent $event): void {
                /** @var Occurrence $data */
                $data = $event->getData();

                $form = $event->getForm();

                $data->setPerson($form->get('propertyOwner')->getData()->getPerson());

                $data->getPropertyOwners()->clear();

                $data->getPropertyOwners()->add($form->get('propertyOwner')->getData());
            }
        );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        parent::configureOptions(resolver: $resolver);

        $resolver
            ->setRequired(['propertyOwners'])
            ->setAllowedTypes(option: 'propertyOwners', allowedTypes: 'array');
    }

    private static function fetchSelectedPropertyOwner(?Occurrence $occurrence, array $propertyOwners): ?PropertyOwner
    {
        if ($occurrence === null) {
            return null;
        }

        if ($occurrence->getPropertyOwners()->count() === 1) {
            return $occurrence->getPropertyOwners()->first();
        }

        return self::findPropertyOwnerByPerson(person: $occurrence->getPerson(), propertyOwners: $propertyOwners);
    }

    /**
     * @param PropertyOwner[] $propertyOwners
     */
    public static function findPropertyOwnerByPerson(Person $person, array $propertyOwners): ?PropertyOwner
    {
        $propertyOwners = array_filter(
            array: $propertyOwners,
            callback: function (PropertyOwner $propertyOwner) use ($person): bool {
                return ($propertyOwner->getPerson() === $person);
            }
        );

        if (count($propertyOwners) > 0) {
            return array_values($propertyOwners)[0];
        }

        return null;
    }

    /**
     * @param PropertyOwner[] $propertyOwners
     * @return PropertyOwner[]
     */
    public static function filterPropertyOwnersWithDeletedPerson(?Occurrence $occurrence, array $propertyOwners): array
    {
        $propertyOwner = self::fetchSelectedPropertyOwner(occurrence: $occurrence, propertyOwners: $propertyOwners);

        $selectedPerson = $propertyOwner?->getPerson();

        return array_filter(
            array: $propertyOwners,
            callback: function (PropertyOwner $propertyOwner) use ($selectedPerson): bool {
                if ($propertyOwner->getPerson()->isDeleted() === false) {
                    return true;
                }

                if ($selectedPerson !== null && $propertyOwner->getPerson() === $selectedPerson) {
                    return true;
                }

                return false;
            }
        );
    }
}
