<?php

declare(strict_types=1);

namespace App\Form\Type\PropertyVacancyManagement\Property;

use App\Domain\Entity\Account;
use App\Domain\Entity\Person\Person;
use App\Domain\Entity\Property\OwnershipType;
use App\Domain\Entity\Property\PropertyOwner;
use App\Domain\Entity\Property\PropertyOwnerStatus;
use App\Form\Type\PersonManagement\Person\PersonSelectOrPersonCreateType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EnumType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PropertyOwnerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $disabled = $options['canEdit'] === false;

        if ($options['canViewPerson'] === true) {
            $builder->add(child: 'personSelectOrPersonCreate', type: PersonSelectOrPersonCreateType::class, options: [
                'mapped'         => false,
                'account'        => $options['account'],
                'selectedPerson' => $options['selectedPerson'],
                'canEdit'        => $options['canEdit'],
            ]);
        }

        $builder
            ->add(child: 'propertyOwnerStatus', type: EnumType::class, options: [
                'label'        => 'Status des Eigentumsverhältnisses',
                'required'     => true,
                'disabled'     => $disabled,
                'class'        => PropertyOwnerStatus::class,
                'choice_label' => function (PropertyOwnerStatus $propertyOwnerStatus): string {
                    return $propertyOwnerStatus->getName();
                },
            ])
            ->add(child: 'ownershipType', type: EnumType::class, options: [
                'label'        => 'Eigentumsart',
                'required'     => true,
                'disabled'     => $disabled,
                'class'        => OwnershipType::class,
                'choice_label' => function (OwnershipType $ownershipType): string {
                    return $ownershipType->getName();
                },
            ])
            ->add(child: 'ownershipShare', type: TextType::class, options: ['label' => 'Eigentumsanteil', 'disabled' => $disabled])
            ->addEventListener(eventName: FormEvents::POST_SUBMIT, listener: function (FormEvent $event) use ($options): void {
                $data = $event->getData();
                $form = $event->getForm();

                $person = $form->get('personSelectOrPersonCreate')->getData();

                if ($person !== null) {
                    $data->setPerson($person);
                }
            });
   }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefaults(['data_class' => PropertyOwner::class])
            ->setRequired(['account', 'selectedPerson', 'canEdit', 'canViewPerson'])
            ->setAllowedTypes(option: 'account', allowedTypes: Account::class)
            ->setAllowedTypes(option: 'selectedPerson', allowedTypes: [Person::class, 'null'])
            ->setAllowedTypes(option: 'canEdit', allowedTypes: ['bool'])
            ->setAllowedTypes(option: 'canViewPerson', allowedTypes: ['bool']);
    }
}
