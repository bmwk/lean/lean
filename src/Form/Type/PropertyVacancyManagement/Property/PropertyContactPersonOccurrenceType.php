<?php

declare(strict_types=1);

namespace App\Form\Type\PropertyVacancyManagement\Property;

use App\Domain\Entity\Person\Occurrence;
use App\Domain\Entity\Person\Person;
use App\Domain\Entity\Property\PropertyContactPerson;
use App\Form\Type\PersonManagement\Occurrence\AbstractOccurrenceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PropertyContactPersonOccurrenceType extends AbstractOccurrenceType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        parent::buildForm(builder: $builder, options: $options);

        $disabled = $options['canEdit'] === false;

        $builder->add(child: 'propertyContactPerson', type: EntityType::class, options: [
            'label'        => 'Ansprechperson',
            'required'     => true,
            'disabled'     => $disabled,
            'class'        => PropertyContactPerson::class,
            'choices'      => self::filterPropertyContactPersonsWithDeletedPerson(occurrence: $builder->getData(), propertyContactPersons: $options['propertyContactPersons']),
            'data'         => self::fetchSelectedPropertyContactPerson(occurrence: $builder->getData(), propertyContactPersons: $options['propertyContactPersons']),
            'choice_label' => function (PropertyContactPerson $propertyContactPerson) {
                return $propertyContactPerson->getPerson()->buildShortDisplayName();
            },
            'mapped' => false,
        ]);

        $builder->addEventListener(
            eventName: FormEvents::SUBMIT,
            listener: function (FormEvent $event): void {
                /** @var Occurrence $data */
                $data = $event->getData();

                $form = $event->getForm();

                $data->setPerson($form->get('propertyContactPerson')->getData()->getPerson());

                $data->getPropertyContactPersons()->clear();

                $data->getPropertyContactPersons()->add($form->get('propertyContactPerson')->getData());
            }
        );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        parent::configureOptions(resolver: $resolver);

        $resolver
            ->setRequired(['propertyContactPersons'])
            ->setAllowedTypes(option: 'propertyContactPersons', allowedTypes: 'array');
    }

    private static function fetchSelectedPropertyContactPerson(?Occurrence $occurrence, array $propertyContactPersons): ?PropertyContactPerson
    {
        if ($occurrence === null) {
            return null;
        }

        if ($occurrence->getPropertyContactPersons()->count() === 1) {
            return $occurrence->getPropertyContactPersons()->first();
        }

        return self::findPropertyContactPersonByPerson(person: $occurrence->getPerson(), propertyContactPersons: $propertyContactPersons);
    }

    /**
     * @param PropertyContactPerson[] $propertyContactPersons
     */
    public static function findPropertyContactPersonByPerson(Person $person, array $propertyContactPersons): ?PropertyContactPerson
    {
        $propertyContactPersons = array_filter(
            array: $propertyContactPersons,
            callback: function (PropertyContactPerson $propertyContactPerson) use ($person): bool {
                return ($propertyContactPerson->getPerson() === $person);
            }
        );

        if (count($propertyContactPersons) > 0) {
            return array_values($propertyContactPersons)[0];
        }

        return null;
    }

    /**
     * @param PropertyContactPerson[] $propertyContactPersons
     * @return PropertyContactPerson[]
     */
    public static function filterPropertyContactPersonsWithDeletedPerson(?Occurrence $occurrence, array $propertyContactPersons): array
    {
        $propertyContactPerson = self::fetchSelectedPropertyContactPerson(occurrence: $occurrence, propertyContactPersons: $propertyContactPersons);

        $selectedPerson = $propertyContactPerson?->getPerson();

        return array_filter(
            array: $propertyContactPersons,
            callback: function (PropertyContactPerson $propertyContactPerson) use ($selectedPerson): bool {
                if ($propertyContactPerson->getPerson()->isDeleted() === false) {
                    return true;
                }

                if ($selectedPerson !== null &&  $propertyContactPerson->getPerson() === $selectedPerson) {
                    return true;
                }

                return false;
            }
        );
    }
}
