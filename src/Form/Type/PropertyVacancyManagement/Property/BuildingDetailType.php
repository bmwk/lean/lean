<?php

declare(strict_types=1);

namespace App\Form\Type\PropertyVacancyManagement\Property;

use App\Domain\Entity\LocationCategory;
use App\Domain\Entity\LocationFactor;
use App\Domain\Entity\LocationType;
use App\Domain\Entity\Property\Building;
use App\Domain\Entity\Property\BuildingCondition;
use App\Domain\Entity\Property\BuildingStandard;
use App\Domain\Entity\Property\BuildingType;
use App\Domain\Entity\Property\EsgCertificate;
use App\Domain\Entity\Property\InternetConnectionType;
use App\Domain\Entity\Property\LiftType;
use App\Domain\Entity\Property\MonumentProtectionType;
use App\Domain\Entity\Property\RoofShape;
use App\Domain\Entity\Property\WindowType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EnumType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BuildingDetailType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $disabled = $options['canEdit'] === false;

        $builder
            ->add(child: 'locationType', type: EnumType::class, options: [
                'label'        => 'Standort-Typ',
                'required'     => false,
                'disabled'     => $disabled,
                'class'        => LocationType::class,
                'choice_label' => function (LocationType $locationType): string {
                    return $locationType->getName();
                },
            ])
            ->add(child: 'locationCategory', type: EnumType::class, options: [
                'label'    => 'Lagekategorie',
                'required' => false,
                'disabled' => $disabled,
                'class'    => LocationCategory::class,
                'choices'  => [
                    LocationCategory::ONE_A_LOCATION,
                    LocationCategory::ONE_B_LOCATION,
                    LocationCategory::ONE_C_LOCATION,
                    LocationCategory::TWO_A_LOCATION,
                    LocationCategory::TWO_B_LOCATION,
                    LocationCategory::TWO_C_LOCATION,
                    LocationCategory::OTHER,
                ],
                'choice_label' => function (LocationCategory $locationCategory): string {
                    return $locationCategory->getName();
                },
            ])
            ->add(child: 'locationFactors', type: EnumType::class, options: [
                'label'        => 'Standortfaktoren',
                'required'     => false,
                'disabled'     => $disabled,
                'class'        => LocationFactor::class,
                'choice_label' => function (LocationFactor $locationFactor): string {
                    return $locationFactor->getName();
                },
                'multiple' => true,
                'expanded' => true,
            ])
            ->add(child: 'buildingCondition', type: EnumType::class, options: [
                'label'        => 'Gebäudezustand',
                'required'     => false,
                'disabled'     => $disabled,
                'class'        => BuildingCondition::class,
                'choice_label' => function (BuildingCondition $buildingCondition): string {
                    return $buildingCondition->getName();
                },
            ])
            ->add(child: 'buildingStandard', type: EnumType::class, options: [
                'label'        => 'Gebäude-Standard',
                'required'     => false,
                'disabled'     => $disabled,
                'class'        => BuildingStandard::class,
                'choice_label' => function (BuildingStandard $buildingStandard): string {
                    return $buildingStandard->getName();
                },
            ])
            ->add(child: 'buildingType', type: EnumType::class, options: [
                'label'    => 'Gebäudetyp',
                'required' => false,
                'disabled' => $disabled,
                'class'    => BuildingType::class,
                'choices'  => [
                    BuildingType::BUSINESS_HOUSE,
                    BuildingType::RESIDENTIAL_AND_COMMERCIAL_BUILDING_MIXED_USE,
                    BuildingType::OFFICE_BUILDING,
                    BuildingType::ADMINISTRATION_BUILDING,
                    BuildingType::SPECIAL_BUILDING,
                    BuildingType::APARTMENT_HOUSE,
                    BuildingType::SINGLE_FAMILY_HOUSE,
                    BuildingType::FARMHOUSE,
                    BuildingType::SEMI_DETACHED_HOUSE,
                    BuildingType::VILLA,
                    BuildingType::TERRACED_HOUSE,
                    BuildingType::BUNGALOW,
                    BuildingType::OTHER_REAL_ESTATE,
                ],
                'choice_label' => function (BuildingType $buildingType): string {
                    return $buildingType->getName();
                },
            ])
            ->add(child: 'roofShape', type: EnumType::class, options: [
                'label'        => 'Dachform',
                'required'     => false,
                'disabled'     => $disabled,
                'class'        => RoofShape::class,
                'choice_label' => function (RoofShape $roofShape): string {
                    return $roofShape->getName();
                },
            ])
            ->add(child: 'constructionYear', type: NumberType::class, options: ['label' => 'Baujahr', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'buildingBlockCode', type: TextType::class, options: ['label' => 'Baublockkennziffer', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'monumentProtectionTypes', type: EnumType::class, options: [
                'label'        => 'Denkmalschutz',
                'required'     => false,
                'disabled'     => $disabled,
                'class'        => MonumentProtectionType::class,
                'choice_label' => function (MonumentProtectionType $monumentProtectionType): string {
                    return $monumentProtectionType->getName();
                },
                'expanded' => true,
                'multiple' => true,
            ])
            ->add(child: 'windowTypes', type: EnumType::class, options: [
                'label'        => 'Schaufenster',
                'required'     => false,
                'disabled'     => $disabled,
                'class'        => WindowType::class,
                'choice_label' => function (WindowType $windowType): string {
                    return $windowType->getName();
                },
                'expanded' => true,
                'multiple' => true,
            ])
            ->add(child: 'liftTypes', type: EnumType::class, options: [
                'label'        => 'Aufzug',
                'required'     => false,
                'disabled'     => $disabled,
                'class'        => LiftType::class,
                'choice_label' => function (LiftType $liftType): string {
                    return $liftType->getName();
                },
                'expanded' => true,
                'multiple' => true,
            ])
            ->add(child: 'internetConnectionTypes', type: EnumType::class, options: [
                'label'        => 'Internetanschluss',
                'required'     => false,
                'disabled'     => $disabled,
                'class'        => InternetConnectionType::class,
                'choice_label' => function (InternetConnectionType $internetConnectionType): string {
                    return $internetConnectionType->getName();
                },
                'expanded' => true,
                'multiple' => true,
            ])
            ->add(child: 'numberOfFloors', type: NumberType::class, options: ['label' => 'Anzahl Etagen', 'required' => false,'disabled' => $disabled])
            ->add(child: 'esgCertificateAvailable', type: CheckboxType::class, options: ['label' => 'ESG-Zertifikat vorhanden', 'disabled' => $disabled])
            ->add(child: 'esgCertificates', type: EnumType::class, options: [
                'label'        => 'ESG-Zertifikate',
                'required'     => false,
                'disabled'     => $disabled,
                'placeholder'  => 'Bitte wählen',
                'class'        => EsgCertificate::class,
                'choice_label' => function (EsgCertificate $esgCertificate): string {
                    return $esgCertificate->getName();
                },
                'expanded' => true,
                'multiple' => true,
            ])
            ->add(child: 'esgCertificateDetailInformation', type: TextareaType::class, options: ['label' => 'Erläuterung zum Zertifikat', 'required' => false, 'disabled' => $disabled]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefaults(['data_class' => Building::class])
            ->setRequired(['canEdit'])
            ->setAllowedTypes(option: 'canEdit', allowedTypes: ['bool']);
    }
}
