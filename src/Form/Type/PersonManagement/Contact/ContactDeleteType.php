<?php

declare(strict_types=1);

namespace App\Form\Type\PersonManagement\Contact;

use App\Form\Type\AbstractDeleteType;

class ContactDeleteType extends AbstractDeleteType
{
}
