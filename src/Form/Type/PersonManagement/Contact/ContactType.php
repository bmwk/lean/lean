<?php

declare(strict_types=1);

namespace App\Form\Type\PersonManagement\Contact;

use App\Domain\Entity\Person\Contact;
use App\Domain\Entity\Person\PersonTitle;
use App\Domain\Entity\Person\Salutation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EnumType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $disabled = $options['canEdit'] === false;

        $builder
            ->add(child: 'salutation', type: EnumType::class, options: [
                'label'        => 'Anrede',
                'required'     => false,
                'disabled'     => $disabled,
                'class'        => Salutation::class,
                'choice_label' => function (Salutation $salutation): string {
                    return $salutation->getName();
                },
            ])
            ->add(child: 'personTitle', type: EnumType::class, options: [
                'label'        => 'Titel',
                'required'     => false,
                'disabled'     => $disabled,
                'class'        => PersonTitle::class,
                'choice_label' => function (PersonTitle $personTitle): string {
                    return $personTitle->getName();
                },
            ])
            ->add(child: 'firstName', type: TextType::class, options: ['label' => 'Vorname', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'name', type: TextType::class, options: ['label' => 'Name', 'required' => true, 'disabled' => $disabled])
            ->add(child: 'email', type: TextType::class, options: ['label' => 'E-Mail', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'mobilePhoneNumber', type: TextType::class, options: ['label' => 'Handynummer', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'phoneNumber', type: TextType::class, options: ['label' => 'Telefonnummer', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'faxNumber', type: TextType::class, options: ['label' => 'Faxnummer', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'position', type: TextType::class, options: ['label' => 'Position', 'required' => false, 'disabled' => $disabled]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefaults(['data_class' => Contact::class])
            ->setRequired(['canEdit'])
            ->setAllowedTypes(option: 'canEdit', allowedTypes: ['bool']);
    }
}
