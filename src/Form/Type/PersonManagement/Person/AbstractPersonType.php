<?php

declare(strict_types=1);

namespace App\Form\Type\PersonManagement\Person;

use App\Domain\Entity\Person\Person;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

abstract class AbstractPersonType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $disabled = $options['canEdit'] === false;

        $builder
            ->add(child: 'streetName', type: TextType::class, options: ['label' => 'Straße', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'houseNumber', type: TextType::class, options: ['label' => 'Hausnummer', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'postalCode', type: TextType::class, options: ['label' => 'PLZ', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'placeName', type: TextType::class, options: ['label' => 'Ort', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'additionalAddressInformation', type: TextareaType::class, options: ['label' => 'Adresszusatz', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'email', type: EmailType::class, options: ['label' => 'E-Mail', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'mobilePhoneNumber', type: TextType::class, options: ['label' => 'Handynummer', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'phoneNumber', type: TextType::class, options: ['label' => 'Telefonnummer', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'faxNumber', type: TextType::class, options: ['label' => 'Faxnummer', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'website', type: TextType::class, options: ['label' => 'Internet', 'required' => false, 'disabled' => $disabled]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefaults(['data_class' => Person::class])
            ->setRequired(['canEdit'])
            ->setAllowedTypes(option: 'canEdit', allowedTypes: ['bool']);
    }
}
