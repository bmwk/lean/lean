<?php

declare(strict_types=1);

namespace App\Form\Type\PersonManagement\Person;

use App\Domain\Entity\Person\PersonTitle;
use App\Domain\Entity\Person\PersonType;
use App\Domain\Entity\Person\Salutation;
use Symfony\Component\Form\Extension\Core\Type\EnumType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class NaturalPersonType extends AbstractPersonType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $disabled = $options['canEdit'] === false;

        $builder
            ->add(child: 'firstName', type: TextType::class,  options: ['label' => 'Vorname', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'name', type: TextType::class, options: ['label' => 'Nachname', 'required' => true, 'disabled' => $disabled])
            ->add(child: 'birthName', type: TextType::class, options: ['label' => 'Geburtsname', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'salutation', type: EnumType::class, options: [
                'label'        => 'Anrede',
                'required'     => false,
                'disabled'     => $disabled,
                'class'        => Salutation::class,
                'choice_label' => function (Salutation $salutation): string {
                    return $salutation->getName();
                },
            ])
            ->add(child: 'personTitle', type: EnumType::class, options: [
                'label'        => 'Titel',
                'required'     => false,
                'disabled'     => $disabled,
                'class'        => PersonTitle::class,
                'choice_label' => function (PersonTitle $personTitle): string {
                    return $personTitle->getName();
                },
            ])
            ->add(child: 'personTypeName', type: HiddenType::class, options: [
                'data'   => PersonType::NATURAL_PERSON->name,
                'mapped' => false,
            ]);

        parent::buildForm(builder: $builder, options: $options);
    }
}
