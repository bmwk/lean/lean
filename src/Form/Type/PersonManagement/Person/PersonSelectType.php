<?php

declare(strict_types=1);

namespace App\Form\Type\PersonManagement\Person;

use App\Domain\Entity\Account;
use App\Domain\Entity\Person\Person;
use App\Domain\Person\PersonService;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\DataMapperInterface;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PersonSelectType extends AbstractType implements DataMapperInterface
{
    public function __construct(
        private readonly PersonService $personService
    ) {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $disabled = $options['canEdit'] === false;

        $builder
            ->add(child: 'personId', type: HiddenType::class)
            ->add(child: 'personSearch', type: TextType::class, options: ['label' => 'angelegte Person/Firma/Kommune', 'disabled' => $disabled])
            ->add(child: 'person', type: EntityType::class, options: [
                'class'        => Person::class,
                'choices'      => $this->personService->fetchPersonsByAccount(account: $options['account']),
                'choice_label' => function (Person $person): string {
                    return $person->buildDisplayName();
                },
                'required' => false,
            ])
            ->addEventListener(eventName: FormEvents::PRE_SET_DATA, listener: function (FormEvent $event) use ($options): void {
                if ($options['selectedPerson'] !== null) {
                    $event->setData($options['selectedPerson']);
                }
            })
            ->addEventListener(eventName: FormEvents::PRE_SUBMIT, listener: function (FormEvent $event) use ($options): void {
                $data = $event->getData();

                $person = $this->personService->fetchPersonById(account: $options['account'], id: (int) $data['personId']);

                $data['person'] = $person?->getId();

                $event->setData($data);
            })
            ->setDataMapper($this);
    }

    /**
     * @param Person|null $viewData
     */
    public function mapDataToForms(mixed $viewData, \Traversable $forms): void
    {
        if ($viewData === null) {
            return;
        }

        if (($viewData instanceof Person) === false) {
            throw new UnexpectedTypeException(value: $viewData, expectedType: Person::class);
        }

        $forms = iterator_to_array($forms);

        $forms['personId']->setData($viewData->getId());
        $forms['personSearch']->setData($viewData->buildDisplayName());
        $forms['person']->setData($viewData);
    }

    /**
     * @param Person|null $viewData
     */
    public function mapFormsToData(\Traversable $forms, mixed &$viewData): void
    {
        $forms = iterator_to_array(iterator: $forms);

        $viewData = $forms['person']->getData();
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefault(option: 'empty_data', value: null)
            ->setRequired(['account', 'selectedPerson', 'canEdit'])
            ->setAllowedTypes(option: 'account', allowedTypes: Account::class)
            ->setAllowedTypes(option: 'selectedPerson', allowedTypes: [Person::class, 'null'])
            ->setAllowedTypes(option: 'canEdit', allowedTypes: ['bool']);
    }
}
