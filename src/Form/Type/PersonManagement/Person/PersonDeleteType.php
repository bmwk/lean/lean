<?php

declare(strict_types=1);

namespace App\Form\Type\PersonManagement\Person;

use App\Form\Type\AbstractDeleteType;

class PersonDeleteType extends AbstractDeleteType
{
}
