<?php

declare(strict_types=1);

namespace App\Form\Type\PersonManagement\Person;

use App\Domain\Entity\Person\PersonFilter;
use App\Domain\Entity\Person\PersonType;
use App\Form\Type\AbstractFilterType;
use App\SessionStorage\SessionStorageService;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EnumType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class PersonFilterType extends AbstractFilterType
{
    public function __construct(
        SessionStorageService $sessionStorageService,
        RequestStack $requestStack,
        UrlGeneratorInterface $router
    ) {
        parent::__construct(
            sessionStorageService: $sessionStorageService,
            requestStack: $requestStack,
            router: $router
        );
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(child: 'personTypes', type: EnumType::class, options: [
                'class'        => PersonType::class,
                'choice_label' => function (PersonType $personType): string {
                    if ($personType === PersonType::COMPANY) {
                        return 'Unternehmen';
                    }

                    return $personType->getName();
                },
                'expanded' => true,
                'multiple' => true,
            ])
            ->add(child: 'usageTypes', type: ChoiceType::class, options: [
                'choices'  => [
                    'Eigentümer'                 => 0,
                    'Nutzer'                     => 1,
                    'Suchender'                  => 4,
                    'Ansprechperson Objekt'      => 2,
                    'Ansprechperson Vermarktung' => 3,
                ],
                'expanded' => true,
                'multiple' => true,
           ]);

        parent::buildForm(builder: $builder, options: $options);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => PersonFilter::class]);

        parent::configureOptions(resolver: $resolver);
    }
}
