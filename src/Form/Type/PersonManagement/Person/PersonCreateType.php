<?php

declare(strict_types=1);

namespace App\Form\Type\PersonManagement\Person;

use App\Domain\Entity\Person\Person;
use App\Domain\Entity\Person\PersonType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\DataMapperInterface;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PersonCreateType extends AbstractType implements DataMapperInterface
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(child: 'selectedPersonType', type: HiddenType::class, options: ['required' => true])
            ->add(child: 'naturalPerson', type: NaturalPersonType::class, options: ['canEdit' => true])
            ->add(child: 'company', type: CompanyType::class, options: ['canEdit' => true])
            ->add(child: 'commune', type: CommuneType::class, options: ['canEdit' => true])
            ->addEventListener(eventName: FormEvents::POST_SUBMIT, listener: function (FormEvent $event) use ($options): void {
                $data = $event->getData();
                $form = $event->getForm();

                $data->setPersonType(PersonType::from((int)$form->get('selectedPersonType')->getData()));
            })
            ->setDataMapper($this);
    }

    /**
     * @param Person|null $viewData
     */
    public function mapDataToForms(mixed $viewData, \Traversable $forms): void
    {
        if ($viewData === null) {
            return;
        }

        if (($viewData instanceof Person) === false) {
            throw new UnexpectedTypeException(value: $viewData, expectedType: Person::class);
        }
    }

    /**
     * @param Person|null $viewData
     */
    public function mapFormsToData(\Traversable $forms, mixed &$viewData): void
    {
        $forms = iterator_to_array(iterator: $forms);

        $personType = PersonType::from((int)$forms['selectedPersonType']->getData());

        $viewData = match ($personType) {
            PersonType::NATURAL_PERSON => $forms['naturalPerson']->getData(),
            PersonType::COMPANY => $forms['company']->getData(),
            PersonType::COMMUNE => $forms['commune']->getData(),
            default => throw new \RuntimeException()
        };
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefault(option: 'empty_data', value: null);
    }
}
