<?php

declare(strict_types=1);

namespace App\Form\Type\PersonManagement\Person;

use App\Domain\Entity\Person\Person;
use App\Domain\Entity\Person\PersonType;
use App\Form\Type\PersonManagement\Occurrence\AbstractOccurrenceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PersonOccurrenceType extends AbstractOccurrenceType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        parent::buildForm(builder: $builder, options: $options);

        $disabled = $options['canEdit'] === false;

        $builder->add(child: 'person', type: EntityType::class, options: [
            'label'        => 'Person / Firma / Kommune',
            'required'     => true,
            'disabled'     => $disabled,
            'class'        => Person::class,
            'choices'      => $options['persons'],
            'choice_label' => function (Person $person) {
                if ($person->getPersonType() === PersonType::NATURAL_PERSON) {
                    return $person->getFirstName() . ' ' . $person->getName();
                }

                return $person->getName();
            },
        ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        parent::configureOptions(resolver: $resolver);

        $resolver
            ->setRequired(['persons'])
            ->setAllowedTypes(option: 'persons', allowedTypes: 'array');
    }
}
