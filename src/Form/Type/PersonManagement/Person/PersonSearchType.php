<?php

declare(strict_types=1);

namespace App\Form\Type\PersonManagement\Person;

use App\Domain\Entity\Person\PersonSearch;
use App\Form\Type\AbstractSearchType;
use App\SessionStorage\SessionStorageService;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class PersonSearchType extends AbstractSearchType
{
    public function __construct(
        SessionStorageService $sessionStorageService,
        RequestStack $requestStack,
        UrlGeneratorInterface $router
    ) {
        parent::__construct(
            sessionStorageService: $sessionStorageService,
            requestStack: $requestStack,
            router: $router
        );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => PersonSearch::class]);

        parent::configureOptions(resolver: $resolver);
    }
}
