<?php

declare(strict_types=1);

namespace App\Form\Type\PersonManagement\Person;

use App\Domain\Entity\Person\LegalEntityType;
use App\Domain\Entity\Person\PersonType;
use Symfony\Component\Form\Extension\Core\Type\EnumType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class CompanyType extends AbstractPersonType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $disabled = $options['canEdit'] === false;

        $builder
            ->add(child: 'name', type: TextType::class, options: ['label' => 'Firmenname', 'required' => true, 'disabled' => $disabled])
            ->add(child: 'legalEntityType', type: EnumType::class, options: [
                'label'        => 'Rechtsform',
                'required'     => false,
                'disabled'     => $disabled,
                'class'        => LegalEntityType::class,
                'choice_label' => function (LegalEntityType $legalEntityType): string {
                    return $legalEntityType->getName();
                },
            ])
            ->add(child: 'personTypeName', type: HiddenType::class, options: [
                'data'   => PersonType::COMPANY->name,
                'mapped' => false,
            ]);

        parent::buildForm(builder: $builder, options: $options);
    }
}
