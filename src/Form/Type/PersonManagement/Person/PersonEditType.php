<?php

declare(strict_types=1);

namespace App\Form\Type\PersonManagement\Person;

use App\Domain\Entity\Person\Person;
use App\Domain\Entity\Person\PersonType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PersonEditType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $personType = $builder->getData()->getPersonType();

        $builder->add(child: 'personType', type: HiddenType::class, options: ['data' => $personType->name, 'mapped' => false]);

        $personType = match ($personType) {
            PersonType::NATURAL_PERSON => new NaturalPersonType(),
            PersonType::COMPANY => new CompanyType(),
            PersonType::COMMUNE => new CommuneType(),
            default => throw new \RuntimeException(message: 'invalid personType')
        };

        $personType->buildForm(builder: $builder, options: $options);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefaults(['data_class' => Person::class])
            ->setRequired(['canEdit'])
            ->setAllowedTypes(option: 'canEdit', allowedTypes: ['bool']);
    }
}
