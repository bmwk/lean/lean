<?php

declare(strict_types=1);

namespace App\Form\Type\PersonManagement\Person;

use App\Domain\Entity\Person\PersonType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class CommuneType extends AbstractPersonType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $disabled = $options['canEdit'] === false;

        $builder
            ->add(child: 'name', type: TextType::class, options: ['label' => 'Bezeichnung', 'required' => true, 'disabled' => $disabled])
            ->add(child: 'personTypeName', type: HiddenType::class, options: [
                'data'   => PersonType::COMMUNE->name,
                'mapped' => false,
            ]);

        parent::buildForm(builder: $builder, options: $options);
    }
}
