<?php

declare(strict_types=1);

namespace App\Form\Type\PersonManagement\Person;

use App\Domain\Entity\Account;
use App\Domain\Entity\Person\Person;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\DataMapperInterface;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PersonSelectOrPersonCreateType extends AbstractType implements DataMapperInterface
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        if ($options['canEdit'] === true) {
            $builder
                ->add(child: 'personCreateButton', type: ButtonType::class)
                ->add(child: 'personCreate', type: PersonCreateType::class);
        }

        $builder
            ->add(child: 'selectOrCreate', type: HiddenType::class)
            ->add(child: 'personSelect', type: PersonSelectType::class, options: [
                'account'        => $options['account'],
                'selectedPerson' => $options['selectedPerson'],
                'canEdit'        => $options['canEdit'],
            ])
            ->setDataMapper($this);
    }

    /**
     * @param Person|null $viewData
     */
    public function mapDataToForms(mixed $viewData, \Traversable $forms): void
    {
        if ($viewData === null) {
            return;
        }

        if (($viewData instanceof Person) === false) {
            throw new UnexpectedTypeException(value: $viewData, expectedType: Person::class);
        }
    }

    /**
     * @param Person|null $viewData
     */
    public function mapFormsToData(\Traversable $forms, mixed &$viewData): void
    {
        $forms = iterator_to_array(iterator: $forms);

        if ($forms['selectOrCreate']->getData() === 'create') {
            $viewData = $forms['personCreate']->getData();
        } elseif ($forms['selectOrCreate']->getData() === 'select') {
            $viewData = $forms['personSelect']->getData();
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefault(option: 'empty_data', value: null)
            ->setRequired(['account', 'selectedPerson', 'canEdit'])
            ->setAllowedTypes(option: 'account', allowedTypes: Account::class)
            ->setAllowedTypes(option: 'selectedPerson', allowedTypes: [Person::class, 'null'])
            ->setAllowedTypes(option: 'canEdit', allowedTypes: ['bool']);
    }
}
