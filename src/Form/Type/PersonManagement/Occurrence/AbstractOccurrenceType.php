<?php

declare(strict_types=1);

namespace App\Form\Type\PersonManagement\Occurrence;

use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\Person\Occurrence;
use App\Domain\Entity\Person\OccurrenceType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EnumType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

abstract class AbstractOccurrenceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $disabled = $options['canEdit'] === false;

        $builder
            ->add(child: 'occurrenceType', type: EnumType::class, options: [
                'label'    => 'Vorgangsart',
                'required' => true,
                'disabled' => $disabled,
                'class'    => OccurrenceType::class,
                'choices'  => [
                    OccurrenceType::MEMO,
                    OccurrenceType::PHONE_CALL_MADE,
                    OccurrenceType::EMAIL_CORRESPONDENT,
                    OccurrenceType::LETTER_SEND,
                    OccurrenceType::QUESTIONNAIRE_SENT,
                    OccurrenceType::RECEIVED_FEEDBACK,
                    OccurrenceType::OTHER,
                ],
                'choice_label' => function (OccurrenceType $occurrenceType): string {
                    return $occurrenceType->getName();
                },
            ])
            ->add(child: 'note', type: TextareaType::class, options: ['label' => 'Notiz', 'required' => true, 'disabled' => $disabled])
            ->add(child: 'occurrenceAt', type: DateType::class, options: [
                'label'    => 'Erfasst am',
                'required' => true,
                'disabled' => $disabled,
                'widget'   => 'single_text',
                'format'   => 'dd.MM.yyyy',
                'html5'    => false,
            ])
            ->add(child: 'clerk', type: TextType::class, options: [
                'label'    => 'Sachbearbeiter',
                'required' => false,
                'disabled' => true,
                'data'     => $options['accountUser']->getFullName(),
                'mapped'   => false,
            ])
            ->add(child: 'followUp', type: CheckboxType::class, options: ['label' => 'Wiedervorlage', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'followUpDone', type: CheckboxType::class, options: ['label' => 'Wiedervorlage erledigt', 'required' => false, 'disabled' => $disabled])
            ->add(child: 'followUpAt', type: DateType::class, options: [
                'label'    => 'Wiedervorlage am',
                'required' => false,
                'disabled' => $disabled,
                'widget'   => 'single_text',
                'format'   => 'dd.MM.yyyy',
                'html5'    => false,
        ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefaults(['data_class' => Occurrence::class])
            ->setRequired(['accountUser', 'canEdit'])
            ->setAllowedTypes(option: 'accountUser', allowedTypes: AccountUser::class)
            ->setAllowedTypes(option: 'canEdit', allowedTypes: ['bool']);;
    }
}
