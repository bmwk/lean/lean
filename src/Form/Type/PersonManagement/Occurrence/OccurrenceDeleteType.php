<?php

declare(strict_types=1);

namespace App\Form\Type\PersonManagement\Occurrence;

use App\Form\Type\AbstractDeleteType;

class OccurrenceDeleteType extends AbstractDeleteType
{
}
