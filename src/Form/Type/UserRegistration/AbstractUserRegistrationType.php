<?php

declare(strict_types=1);

namespace App\Form\Type\UserRegistration;

use App\Domain\Entity\Person\Salutation;
use App\Domain\Entity\UserRegistration\UserRegistration;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EnumType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

abstract class AbstractUserRegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(child: 'salutation', type: EnumType::class, options: [
                'label'        => 'Anrede',
                'required'     => false,
                'class'        => Salutation::class,
                'choice_label' => function (Salutation $salutation): string {
                    return $salutation->getName();
                },
            ])
            ->add(child: 'lastName', type: TextType::class, options: ['label' => 'Nachname'])
            ->add(child: 'firstName', type: TextType::class, options: ['label' => 'Vorname'])
            ->add(child: 'streetName', type: TextType::class, options: ['label' => 'Straße'])
            ->add(child: 'houseNumber', type: TextType::class, options: ['label' => 'Hausnummer'])
            ->add(child: 'postalCode', type: TextType::class, options: ['label' => 'PLZ'])
            ->add(child: 'placeName', type: TextType::class, options: ['label' => 'Ort'])
            ->add(child: 'email', type: TextType::class, options: ['label' => 'E-Mail-Adresse'])
            ->add(child: 'phoneNumber', type: TextType::class, options: ['label' => 'Telefonnummer'])
            ->add(child: 'gtcPrivacy', type: CheckboxType::class, options: ['required' => true, 'label'=> '']);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => UserRegistration::class]);
    }
}
