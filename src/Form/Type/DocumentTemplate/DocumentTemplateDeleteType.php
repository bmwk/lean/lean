<?php

declare(strict_types=1);

namespace App\Form\Type\DocumentTemplate;

use App\Form\Type\AbstractDeleteType;

class DocumentTemplateDeleteType extends AbstractDeleteType
{
}
