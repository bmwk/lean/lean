<?php

declare(strict_types=1);

namespace App\Form\Type\AccountUser;

use App\Domain\Entity\AccountUser\AccountUser;
use App\Form\Type\PersonManagement\Person\PersonEditType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

class BaseDataType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(child: 'fullName', type: TextType::class, options: [
                'label'       => 'Name',
                'required'    => true,
                'constraints' => [new NotBlank()],
            ])
            ->add(child: 'nameAbbreviation', type: TextType::class, options: ['label' => 'Namenskürzel', 'required' => false])
            ->add(child: 'phoneNumber', type: TextType::class, options: ['label' => 'Telefonnummer', 'required' => false])
            ->add(child: 'email', type: EmailType::class, options: [
                'label'       => 'E-Mail-Adresse',
                'required'    => true,
                'constraints' => [
                    new NotBlank(),
                    new Email(message: 'Dieser Wert ist keine gültige E-Mail-Adresse.'),
                ],
            ])
            ->add(child: 'username', type: TextType::class, options: [
                'label'       => 'Benutzername',
                'required'    => true,
                'constraints' => [new NotBlank()],
            ])
            ->add(child: 'currentPassword', type: PasswordType::class, options: ['label' => 'Bisheriges Kennwort', 'required' => false, 'mapped' => false])
            ->add(child: 'password', type: RepeatedType::class, options: [
                'type'            => PasswordType::class,
                'required'        => false,
                'mapped'          => false,
                'invalid_message' => 'Die neu eingegebenen Kennwörter stimmen nicht überein.',
                'first_options'   => ['label' => 'Kennwort'],
                'second_options'  => ['label' => 'Neues Kennwort wiederholen'],
            ]);

        $person = $builder->getData()->getPerson();

        if ($person !== null && $person->isAnonymized() === false) {
            $builder
                ->add(child: 'person', type: PersonEditType::class, options: ['data' => $person, 'canEdit' => true])
                ->add(child: 'personType', type: HiddenType::class, options: ['data' => $person->getPersonType()->name, 'mapped' => false]);
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => AccountUser::class]);
    }
}
