<?php

declare(strict_types=1);

namespace App\Form\Type\AccountUser;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;

class DeleteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add(child: 'currentPasswordForDeletion', type: PasswordType::class, options: ['label' => 'Ihr Kennwort', 'required' => false]);
    }
}
