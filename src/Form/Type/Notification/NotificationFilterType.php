<?php

declare(strict_types=1);

namespace App\Form\Type\Notification;

use App\Domain\Entity\Notification\NotificationFilter;
use App\Domain\Entity\Notification\NotificationType;
use App\Form\Type\AbstractFilterType;
use App\SessionStorage\SessionStorageService;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EnumType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class NotificationFilterType extends AbstractFilterType
{
    public function __construct(
        SessionStorageService $sessionStorageService,
        RequestStack $requestStack,
        UrlGeneratorInterface $router
    ) {
        parent::__construct(
            sessionStorageService: $sessionStorageService,
            requestStack: $requestStack,
            router: $router
        );
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(child: 'readStatus', type: ChoiceType::class, options: [
                'choices' => [
                    'ungelesen' => false,
                    'gelesen'   => true,
                ],
                'expanded' => true,
                'multiple' => true,
            ])
            ->add(child: 'notificationTypes', type: EnumType::class, options: [
                'required' => false,
                'class'    => NotificationType::class,
                'choices'  => [
                    NotificationType::PROPERTY_BECAME_EMPTY,
                    NotificationType::PROPERTY_NO_MATCH_FOUND,
                    NotificationType::PROPERTY_NO_MATCHING_EXECUTED,
                    NotificationType::PROPERTY_RENTAL_AGREEMENT_EXPIRED,
                    NotificationType::PROPERTY_USAGE_CHANGE,
                    NotificationType::OCCURRENCE_RESUBMISSION_DUE,
                ],
                'choice_label' => function (NotificationType $notificationType): string {
                    return $notificationType->getName();
                },
                'expanded' => true,
                'multiple' => true,
            ]);

        parent::buildForm(builder: $builder, options: $options);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => NotificationFilter::class]);

        parent::configureOptions(resolver: $resolver);
    }
}
