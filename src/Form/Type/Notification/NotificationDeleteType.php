<?php

declare(strict_types=1);

namespace App\Form\Type\Notification;

use App\Form\Type\AbstractDeleteType;

class NotificationDeleteType extends AbstractDeleteType
{
}
