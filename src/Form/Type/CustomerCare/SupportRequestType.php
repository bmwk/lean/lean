<?php

declare(strict_types=1);

namespace App\Form\Type\CustomerCare;

use App\Domain\Entity\CustomerCare\SupportRequest;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SupportRequestType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(child: 'fullName', type: TextType::class, options: ['label' => 'Name', 'required' => true])
            ->add(child: 'email', type: EmailType::class, options: ['label' => 'E-Mail-Adresse', 'required' => true])
            ->add(child: 'subject', type: TextType::class, options: ['label' => 'Titel', 'required' => true])
            ->add(child: 'message', type: TextareaType::class, options: ['label' => 'Nachricht', 'required' => true]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => SupportRequest::class]);
    }
}
