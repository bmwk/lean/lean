<?php

declare(strict_types=1);

namespace App\Form\Type\Frontend;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class PasswordResetType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add(child: 'password', type: RepeatedType::class, options: [
            'type'           => PasswordType::class,
            'required'       => true,
            'first_options'  => ['label' => 'Passwort'],
            'second_options' => ['label' => 'Passwort wiederholen'],
            'constraints'    => [new NotBlank()],
        ]);
    }
}
