<?php

declare(strict_types=1);

namespace App\Form\Type\Frontend;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Email;

class PasswordForgottenType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        parent::buildForm(builder: $builder, options: $options);

        $builder->add(child: 'email', type: EmailType::class, options: [
            'label'       => 'E-Mail-Adresse',
            'required'    => true,
            'constraints' => [
                new NotBlank(),
                new Email(
                    message: 'Dieser Wert ist keine gültige E-Mail-Adresse.'
                ),
            ],
        ]);
    }
}
