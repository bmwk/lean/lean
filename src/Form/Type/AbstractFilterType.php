<?php

declare(strict_types=1);

namespace App\Form\Type;

use App\SessionStorage\SessionStorageService;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

abstract class AbstractFilterType extends AbstractSessionStoredType
{
    protected function __construct(
        SessionStorageService $sessionStorageService,
        private readonly RequestStack $requestStack,
        private readonly UrlGeneratorInterface $router
    ) {
        parent::__construct(sessionStorageService: $sessionStorageService);
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $request = $this->requestStack->getCurrentRequest();

        $queryParameters = $request->query->all();

        $routeParameters =  $request->get(key: '_route_params');

        if (
            isset($queryParameters['seite']) === true
            && isset($queryParameters['anzeigen']) === true
            && (int) $queryParameters['seite'] > 1
        ) {
            $queryParameters['seite'] = 1;

            $builder->setAction(
                action: $this->router->generate(name: $request->get(key: '_route'), parameters: array_merge($routeParameters, $queryParameters))
            );
        }

        $builder
            ->add(child: 'reset', type: SubmitType::class, options: ['label' => 'Filter zurücksetzen'])
            ->addEventListener(eventName: FormEvents::PRE_SUBMIT, listener: function (FormEvent $event): void {
                $data = $event->getData();

                   if (isset($data['reset']) === true) {
                       $event->setData(null);
                   }
            });

        if (self::checkIfSessionStorageParametersAreAvailable(options: $options) === true) {
            parent::buildForm(builder: $builder, options: $options);
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setRequired(['dataClassName', 'sessionKeyName'])
            ->setAllowedTypes(option: 'dataClassName', allowedTypes: ['string', 'null'])
            ->setAllowedTypes(option: 'sessionKeyName', allowedTypes: ['string', 'null']);
    }
}
