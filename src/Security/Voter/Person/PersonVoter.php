<?php

declare(strict_types=1);

namespace App\Security\Voter\Person;

use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\Person\Person;
use App\Domain\Person\PersonSecurityService;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class PersonVoter extends Voter
{
    const VIEW = 'view';
    const EDIT = 'edit';
    const DELETE = 'delete';

    public function __construct(
        private readonly PersonSecurityService $personSecurityService
    ) {
    }

    protected function supports(string $attribute, mixed $subject): bool
    {
        if (in_array(needle: $attribute, haystack: [self::VIEW, self::EDIT, self::DELETE]) === false) {
            return false;
        }

        if (!$subject instanceof Person) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        return match ($attribute) {
            self::VIEW => $this->canView(person: $subject, accountUser: $user),
            self::EDIT => $this->canEdit(person: $subject, accountUser: $user),
            self::DELETE => $this->canDelete(person: $subject, accountUser: $user),
            default => throw new \LogicException(message: 'This code should not be reached!')
        };
    }

    private function canView(Person $person, AccountUser $accountUser): bool
    {
        return $this->personSecurityService->canViewPerson(person: $person, accountUser: $accountUser);
    }

    private function canEdit(Person $person, AccountUser $accountUser): bool
    {
        return $this->personSecurityService->canEditPerson(person: $person, accountUser: $accountUser);
    }

    private function canDelete(Person $person, AccountUser $accountUser): bool
    {
        return $this->personSecurityService->canDeletePerson(person: $person, accountUser: $accountUser);
    }
}
