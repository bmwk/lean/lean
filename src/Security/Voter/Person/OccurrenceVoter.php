<?php

declare(strict_types=1);

namespace App\Security\Voter\Person;

use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\Person\Occurrence;
use App\Domain\Person\OccurrenceSecurityService;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class OccurrenceVoter extends Voter
{
    const VIEW = 'view';
    const EDIT = 'edit';
    const DELETE = 'delete';

    public function __construct(
        private readonly OccurrenceSecurityService $occurrenceSecurityService
    ) {
    }

    protected function supports(string $attribute, mixed $subject): bool
    {
        if (in_array(needle: $attribute, haystack: [self::VIEW, self::EDIT, self::DELETE]) === false) {
            return false;
        }

        if (!$subject instanceof Occurrence) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        return match ($attribute) {
            self::VIEW => $this->canView(occurrence: $subject, accountUser: $user),
            self::EDIT => $this->canEdit(occurrence: $subject, accountUser: $user),
            self::DELETE => $this->canDelete(occurrence: $subject, accountUser: $user),
            default => throw new \LogicException(message: 'This code should not be reached!')
        };
    }

    private function canView(Occurrence $occurrence, AccountUser $accountUser): bool
    {
        return $this->occurrenceSecurityService->canViewOccurrence(occurrence: $occurrence, accountUser: $accountUser);
    }

    private function canEdit(Occurrence $occurrence, AccountUser $accountUser): bool
    {
        return $this->occurrenceSecurityService->canEditOccurrence(occurrence: $occurrence, accountUser: $accountUser);
    }

    private function canDelete(Occurrence $occurrence, AccountUser $accountUser): bool
    {
        return $this->occurrenceSecurityService->canDeleteOccurrence(occurrence: $occurrence, accountUser: $accountUser);
    }
}
