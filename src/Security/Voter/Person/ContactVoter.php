<?php

declare(strict_types=1);

namespace App\Security\Voter\Person;

use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\Person\Contact;
use App\Domain\Person\ContactSecurityService;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class ContactVoter extends Voter
{
    private const VIEW = 'view';
    private const EDIT = 'edit';
    private const DELETE = 'delete';

    public function __construct(
        private readonly ContactSecurityService $contactSecurityService
    ) {
    }

    protected function supports(string $attribute, mixed $subject): bool
    {
        if (in_array(needle: $attribute, haystack: [self::VIEW, self::EDIT, self::DELETE]) === false) {
            return false;
        }

        if (!$subject instanceof Contact) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        return match ($attribute) {
            self::VIEW => $this->canView(contact: $subject, accountUser: $user),
            self::EDIT => $this->canEdit(contact: $subject, accountUser: $user),
            self::DELETE => $this->canDelete(contact: $subject, accountUser: $user),
            default => throw new \LogicException(message: 'This code should not be reached!')
        };
    }

    private function canView(Contact $contact, AccountUser $accountUser): bool
    {
        return $this->contactSecurityService->canViewContact(contact: $contact, accountUser: $accountUser);
    }

    private function canEdit(Contact $contact, AccountUser $accountUser): bool
    {
        return $this->contactSecurityService->canEditContact(contact: $contact, accountUser: $accountUser);
    }

    private function canDelete(Contact $contact, AccountUser $accountUser): bool
    {
        return $this->contactSecurityService->canDeleteContact(contact: $contact, accountUser: $accountUser);
    }
}
