<?php

declare(strict_types=1);

namespace App\Security\Voter;

use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\UserRegistration\UserRegistration;
use App\Domain\UserRegistration\UserRegistrationSecurityService;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class UserRegistrationVoter extends Voter
{
    const VIEW = 'view';
    const EDIT = 'edit';
    const DELETE = 'delete';

    public function __construct(
        private readonly UserRegistrationSecurityService $userRegistrationSecurityService,
    ) {
    }

    protected function supports(string $attribute, mixed $subject): bool
    {
        if (in_array(needle: $attribute, haystack: [self::VIEW, self::EDIT, self::DELETE]) === false) {
            return false;
        }

        if (!$subject instanceof UserRegistration) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        return match ($attribute) {
            self::VIEW => $this->canView(userRegistration: $subject, accountUser: $user),
            self::EDIT => $this->canEdit(userRegistration: $subject, accountUser: $user),
            self::DELETE => $this->canDelete(userRegistration: $subject, accountUser: $user),
            default => throw new \LogicException(message: 'This code should not be reached!')
        };
    }

    private function canView(UserRegistration $userRegistration, AccountUser $accountUser): bool
    {
        return $this->userRegistrationSecurityService->canViewUserRegistration(userRegistration: $userRegistration, accountUser: $accountUser);
    }

    private function canEdit(UserRegistration $userRegistration, AccountUser $accountUser): bool
    {
        return $this->userRegistrationSecurityService->canEditUserRegistration(userRegistration: $userRegistration, accountUser: $accountUser);
    }

    private function canDelete(UserRegistration $userRegistration, AccountUser $accountUser): bool
    {
        return $this->userRegistrationSecurityService->canDeleteUserRegistration(userRegistration: $userRegistration, accountUser: $accountUser);
    }
}
