<?php

declare(strict_types=1);

namespace App\Security\Voter\LookingForPropertyRequest;

use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\LookingForPropertyRequest\LookingForPropertyRequestExposeForwarding;
use App\Domain\LookingForPropertyRequest\LookingForPropertyRequestExposeForwardingSecurityService;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class LookingForPropertyRequestExposeForwardingVoter extends Voter
{
    const VIEW = 'view';
    const EDIT = 'edit';
    const DELETE = 'delete';
    const REJECT = 'reject';
    const SET_FORWARDING_RESPONSE = 'setForwardingResponse';

    public function __construct(
        private readonly LookingForPropertyRequestExposeForwardingSecurityService $lookingForPropertyRequestExposeForwardingSecurityService
    ) {
    }

    protected function supports(string $attribute, mixed $subject): bool
    {
        $supportedAttributes = [
            self::VIEW,
            self::EDIT,
            self::DELETE,
            self::REJECT,
            self::SET_FORWARDING_RESPONSE,
        ];

        if (in_array(needle: $attribute, haystack: $supportedAttributes) === false) {
            return false;
        }

        if (!$subject instanceof LookingForPropertyRequestExposeForwarding) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        return match ($attribute) {
            self::VIEW => $this->canView(lookingForPropertyRequestExposeForwarding: $subject, accountUser: $user),
            self::EDIT => $this->canEdit(lookingForPropertyRequestExposeForwarding: $subject, accountUser: $user),
            self::DELETE => $this->canDelete(lookingForPropertyRequestExposeForwarding: $subject, accountUser: $user),
            self::REJECT => $this->canReject(lookingForPropertyRequestExposeForwarding: $subject, accountUser: $user),
            self::SET_FORWARDING_RESPONSE => $this->canSetForwardingResponse(lookingForPropertyRequestExposeForwarding: $subject, accountUser: $user),
            default => throw new \LogicException(message: 'This code should not be reached!')
        };
    }

    private function canView(
        LookingForPropertyRequestExposeForwarding $lookingForPropertyRequestExposeForwarding,
        AccountUSer $accountUser
    ): bool {
        return $this->lookingForPropertyRequestExposeForwardingSecurityService->canViewLookingForPropertyRequestExposeForwarding(
            lookingForPropertyRequestExposeForwarding: $lookingForPropertyRequestExposeForwarding,
            accountUser: $accountUser
        );
    }

    private function canEdit(
        LookingForPropertyRequestExposeForwarding $lookingForPropertyRequestExposeForwarding,
        AccountUser $accountUser
    ): bool {
        return $this->lookingForPropertyRequestExposeForwardingSecurityService->canEditLookingForPropertyRequestExposeForwarding(
            lookingForPropertyRequestExposeForwarding: $lookingForPropertyRequestExposeForwarding,
            accountUser: $accountUser
        );
    }

    private function canDelete(
        LookingForPropertyRequestExposeForwarding $lookingForPropertyRequestExposeForwarding,
        AccountUser $accountUser
    ): bool {
        return $this->lookingForPropertyRequestExposeForwardingSecurityService->canDeleteLookingForPropertyRequestExposeForwarding(
            lookingForPropertyRequestExposeForwarding: $lookingForPropertyRequestExposeForwarding,
            accountUser: $accountUser
        );
    }

    private function canReject(
        LookingForPropertyRequestExposeForwarding $lookingForPropertyRequestExposeForwarding,
        AccountUser $accountUser
    ): bool {
        return $this->lookingForPropertyRequestExposeForwardingSecurityService->canRejectLookingForPropertyRequestExposeForwarding(
            lookingForPropertyRequestExposeForwarding: $lookingForPropertyRequestExposeForwarding,
            accountUser: $accountUser
        );
    }

    private function canSetForwardingResponse(
        LookingForPropertyRequestExposeForwarding $lookingForPropertyRequestExposeForwarding,
        AccountUser $accountUser
    ): bool {
        return $this->lookingForPropertyRequestExposeForwardingSecurityService->canSetForwardingResponseLookingForPropertyRequestExposeForwarding(
            lookingForPropertyRequestExposeForwarding: $lookingForPropertyRequestExposeForwarding,
            accountUser: $accountUser
        );
    }
}
