<?php

declare(strict_types=1);

namespace App\Security\Voter\LookingForPropertyRequest;

use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\LookingForPropertyRequest\LookingForPropertyRequest;
use App\Domain\LookingForPropertyRequest\LookingForPropertyRequestSecurityService;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class LookingForPropertyRequestVoter extends Voter
{
    private const VIEW = 'view';
    private const EDIT = 'edit';
    private const DELETE = 'delete';
    private const ASSIGN_TO_ACCOUNT_USER = 'assignToAccountUser';

    public function __construct(
        private readonly LookingForPropertyRequestSecurityService $lookingForPropertyRequestSecurityService
    ) {
    }

    protected function supports(string $attribute, mixed $subject): bool
    {
        if (in_array(needle: $attribute, haystack: [self::VIEW, self::EDIT, self::DELETE, self::ASSIGN_TO_ACCOUNT_USER]) === false) {
            return false;
        }

        if (!$subject instanceof LookingForPropertyRequest) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        return match ($attribute) {
            self::VIEW => $this->canView(lookingForPropertyRequest: $subject, accountUser: $user),
            self::EDIT => $this->canEdit(lookingForPropertyRequest: $subject, accountUser: $user),
            self::DELETE => $this->canDelete(lookingForPropertyRequest: $subject, accountUser: $user),
            self::ASSIGN_TO_ACCOUNT_USER => $this->canAssignToAccountUser(lookingForPropertyRequest: $subject, accountUser: $user),
            default => throw new \LogicException(message: 'This code should not be reached!')
        };
    }

    public function canView(LookingForPropertyRequest $lookingForPropertyRequest, AccountUser $accountUser): bool
    {
        return $this->lookingForPropertyRequestSecurityService->canViewLookingForPropertyRequest(
            lookingForPropertyRequest: $lookingForPropertyRequest,
            accountUser: $accountUser
        );
    }

    private function canEdit(LookingForPropertyRequest $lookingForPropertyRequest, AccountUser $accountUser): bool
    {
        return $this->lookingForPropertyRequestSecurityService->canEditLookingForPropertyRequest(
            lookingForPropertyRequest: $lookingForPropertyRequest,
            accountUser: $accountUser
        );
    }

    private function canDelete(LookingForPropertyRequest $lookingForPropertyRequest, AccountUser $accountUser): bool
    {
        return $this->lookingForPropertyRequestSecurityService->canDeleteLookingForPropertyRequest(
            lookingForPropertyRequest: $lookingForPropertyRequest,
            accountUser: $accountUser
        );
    }

    private function canAssignToAccountUser(LookingForPropertyRequest $lookingForPropertyRequest, AccountUser $accountUser): bool
    {
        return $this->lookingForPropertyRequestSecurityService->canAssignToAccountUserLookingForPropertyRequest(
            lookingForPropertyRequest: $lookingForPropertyRequest,
            accountUser: $accountUser
        );
    }
}
