<?php

declare(strict_types=1);

namespace App\Security\Voter\PropertyVacancyReport;

use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\PropertyVacancyReport\PropertyVacancyReport;
use App\Domain\PropertyVacancyReport\PropertyVacancyReportSecurityService;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class PropertyVacancyReportVoter extends Voter
{
    const VIEW = 'view';
    const EDIT = 'edit';
    const DELETE = 'delete';

    public function __construct(
        private readonly PropertyVacancyReportSecurityService $propertyVacancyReportSecurityService
    ) {
    }

    protected function supports(string $attribute, mixed $subject): bool
    {
        if (in_array(needle: $attribute, haystack: [self::VIEW, self::EDIT, self::DELETE]) === false) {
            return false;
        }

        if (!$subject instanceof PropertyVacancyReport) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        return match ($attribute) {
            self::VIEW => $this->canView(propertyVacancyReport: $subject, accountUser: $user),
            self::EDIT => $this->canEdit(propertyVacancyReport: $subject, accountUser: $user),
            self::DELETE => $this->canDelete(propertyVacancyReport: $subject, accountUser: $user),
            default => throw new \LogicException(message: 'This code should not be reached!')
        };
    }

    private function canView(PropertyVacancyReport $propertyVacancyReport, AccountUSer $accountUser): bool
    {
        return $this->propertyVacancyReportSecurityService->canViewPropertyVacancyReport(
            propertyVacancyReport: $propertyVacancyReport,
            accountUser: $accountUser
        );
    }

    private function canEdit(PropertyVacancyReport $propertyVacancyReport, AccountUser $accountUser): bool
    {
        return $this->propertyVacancyReportSecurityService->canEditPropertyVacancyReport(
            propertyVacancyReport: $propertyVacancyReport,
            accountUser: $accountUser
        );
    }

    private function canDelete(PropertyVacancyReport $propertyVacancyReport, AccountUser $accountUser): bool
    {
        return $this->propertyVacancyReportSecurityService->canDeletePropertyVacancyReport(
            propertyVacancyReport: $propertyVacancyReport,
            accountUser: $accountUser
        );
    }
}
