<?php

declare(strict_types=1);

namespace App\Security\Voter\Account;

use App\Domain\Account\AccountUserSecurityService;
use App\Domain\Entity\AccountUser\AccountUser;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class AccountUserVoter extends Voter
{
    private const VIEW = 'view';
    private const EDIT = 'edit';
    private const DELETE = 'delete';

    public function __construct(
        private readonly AccountUserSecurityService $accountUserSecurityService
    ) {
    }

    protected function supports(string $attribute, mixed $subject): bool
    {
        if (in_array(needle: $attribute, haystack: [self::VIEW, self::EDIT, self::DELETE]) === false) {
            return false;
        }

        if (!$subject instanceof AccountUser) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        return match ($attribute) {
            self::VIEW => $this->canView(accountUser: $subject, user: $user),
            self::EDIT => $this->canEdit(accountUser: $subject, user: $user),
            self::DELETE => $this->canDelete(accountUser: $subject, user: $user),
            default => throw new \LogicException(message: 'This code should not be reached!')
        };
    }

    private function canView(AccountUser $accountUser, AccountUser $user): bool
    {
        return $this->accountUserSecurityService->canViewAccountUser(accountUser: $accountUser, user: $user);
    }

    private function canEdit(AccountUser $accountUser, AccountUser $user): bool
    {
        return $this->accountUserSecurityService->canEditAccountUser(accountUser: $accountUser, user: $user);
    }

    private function canDelete(AccountUser $accountUser, AccountUser $user): bool
    {
        return $this->accountUserSecurityService->canDeleteAccountUser(accountUser: $accountUser, user: $user);
    }
}
