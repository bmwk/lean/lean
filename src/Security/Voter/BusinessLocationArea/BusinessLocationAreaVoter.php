<?php

declare(strict_types=1);

namespace App\Security\Voter\BusinessLocationArea;

use App\Domain\BusinessLocationArea\BusinessLocationAreaSecurityService;
use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\BusinessLocationArea;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class BusinessLocationAreaVoter extends Voter
{
    private const VIEW = 'view';
    private const EDIT = 'edit';
    private const DELETE = 'delete';

    public function __construct(
        private readonly BusinessLocationAreaSecurityService $businessLocationAreaSecurityService
    ) {
    }

    protected function supports(string $attribute, mixed $subject): bool
    {
        if (in_array(needle: $attribute, haystack: [self::VIEW, self::EDIT, self::DELETE]) === false) {
            return false;
        }

        if (!$subject instanceof BusinessLocationArea) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        return match ($attribute) {
            self::VIEW => $this->canView(businessLocationArea: $subject, accountUser: $user),
            self::EDIT => $this->canEdit(businessLocationArea: $subject, accountUser: $user),
            self::DELETE => $this->canDelete(businessLocationArea: $subject, accountUser: $user),
            default => throw new \LogicException(message: 'This code should not be reached!')
        };
    }

    private function canView(BusinessLocationArea $businessLocationArea, AccountUSer $accountUser): bool
    {
        return $this->businessLocationAreaSecurityService->canViewBusinessLocationArea(
            businessLocationArea: $businessLocationArea,
            accountUser: $accountUser
        );
    }

    private function canEdit(BusinessLocationArea $businessLocationArea, AccountUser $accountUser): bool
    {
        return $this->businessLocationAreaSecurityService->canEditBusinessLocationArea(
            businessLocationArea: $businessLocationArea,
            accountUser: $accountUser
        );
    }

    private function canDelete(BusinessLocationArea $businessLocationArea, AccountUser $accountUser): bool
    {
        return $this->businessLocationAreaSecurityService->canDeleteBusinessLocationArea(
            businessLocationArea: $businessLocationArea,
            accountUser: $accountUser
        );
    }
}
