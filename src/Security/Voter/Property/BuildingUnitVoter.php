<?php

declare(strict_types=1);

namespace App\Security\Voter\Property;

use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\Property\BuildingUnit;
use App\Domain\Property\BuildingUnitSecurityService;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class BuildingUnitVoter extends Voter
{
    private const VIEW = 'view';
    private const EDIT = 'edit';
    private const DELETE = 'delete';
    private const ARCHIVE = 'archive';
    private const ASSIGN_TO_ACCOUNT_USER = 'assignToAccountUser';
    private const OPEN_IMMO_EXPORT = 'openImmoExport';

    public function __construct(
        private readonly BuildingUnitSecurityService $buildingUnitSecurityService
    ) {
    }

    protected function supports(string $attribute, mixed $subject): bool
    {
        $supportedAttributes = [
            self::VIEW,
            self::EDIT,
            self::DELETE,
            self::ARCHIVE,
            self::ASSIGN_TO_ACCOUNT_USER,
            self::OPEN_IMMO_EXPORT
        ];

        if (in_array(needle: $attribute, haystack: $supportedAttributes) === false) {
            return false;
        }

        if (!$subject instanceof BuildingUnit) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        return match ($attribute) {
            self::VIEW => $this->canView(buildingUnit: $subject, accountUser: $user),
            self::EDIT => $this->canEdit(buildingUnit: $subject, accountUser: $user),
            self::DELETE => $this->canDelete(buildingUnit: $subject, accountUser: $user),
            self::ARCHIVE => $this->canArchive(buildingUnit: $subject, accountUser: $user),
            self::ASSIGN_TO_ACCOUNT_USER => $this->canAssignToAccountUser(buildingUnit: $subject, accountUser: $user),
            self::OPEN_IMMO_EXPORT => $this->canOpenImmoExport(buildingUnit: $subject, accountUser: $user),
            default => throw new \LogicException(message: 'This code should not be reached!')
        };
    }

    private function canView(BuildingUnit $buildingUnit, AccountUser $accountUser): bool
    {
        return $this->buildingUnitSecurityService->canViewBuildingUnit(buildingUnit: $buildingUnit, accountUser: $accountUser);
    }

    private function canEdit(BuildingUnit $buildingUnit, AccountUser $accountUser): bool
    {
        return $this->buildingUnitSecurityService->canEditBuildingUnit(buildingUnit: $buildingUnit, accountUser: $accountUser);
    }

    private function canDelete(BuildingUnit $buildingUnit, AccountUser $accountUser): bool
    {
        return $this->buildingUnitSecurityService->canDeleteBuildingUnit(buildingUnit: $buildingUnit, accountUser: $accountUser);
    }

    private function canArchive(BuildingUnit $buildingUnit, AccountUser $accountUser): bool
    {
        return $this->buildingUnitSecurityService->canArchiveBuildingUnit(buildingUnit: $buildingUnit, accountUser: $accountUser);
    }

    private function canAssignToAccountUser(BuildingUnit $buildingUnit, AccountUser $accountUser): bool
    {
        return $this->buildingUnitSecurityService->canAssignToAccountUserBuildingUnit(buildingUnit: $buildingUnit, accountUser: $accountUser);
    }

    private function canOpenImmoExport(BuildingUnit $buildingUnit, AccountUser $accountUser): bool
    {
        return $this->buildingUnitSecurityService->canOpenImmoExportBuildingUnit(buildingUnit: $buildingUnit, accountUser: $accountUser);
    }
}
