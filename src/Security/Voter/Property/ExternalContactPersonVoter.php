<?php

declare(strict_types=1);

namespace App\Security\Voter\Property;

use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\Property\ExternalContactPerson;
use App\Domain\Property\ExternalContactPersonSecurityService;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class ExternalContactPersonVoter extends Voter
{
    const VIEW = 'view';
    const EDIT = 'edit';
    const DELETE = 'delete';

    public function __construct(
        private readonly ExternalContactPersonSecurityService $externalContactPersonSecurityService
    ) {
    }

    protected function supports(string $attribute, mixed $subject): bool
    {
        if (in_array(needle: $attribute, haystack: [self::VIEW, self::EDIT, self::DELETE]) === false) {
            return false;
        }

        if (!$subject instanceof ExternalContactPerson) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        return match ($attribute) {
            self::VIEW => $this->canView(externalContactPerson: $subject, accountUser: $user),
            self::EDIT => $this->canEdit(externalContactPerson: $subject, accountUser: $user),
            self::DELETE => $this->canDelete(externalContactPerson: $subject, accountUser: $user),
            default => throw new \LogicException(message: 'This code should not be reached!')
        };
    }

    private function canView(ExternalContactPerson $externalContactPerson, AccountUser $accountUser): bool
    {
        return $this->externalContactPersonSecurityService->canViewExternalContactPerson(
            externalContactPerson: $externalContactPerson,
            accountUser: $accountUser
        );
    }

    private function canEdit(ExternalContactPerson $externalContactPerson, AccountUser $accountUser): bool
    {
        return $this->externalContactPersonSecurityService->canEditExternalContactPerson(
            externalContactPerson: $externalContactPerson,
            accountUser: $accountUser
        );
    }

    private function canDelete(ExternalContactPerson $externalContactPerson, AccountUser $accountUser): bool
    {
        return $this->externalContactPersonSecurityService->canDeleteExternalContactPerson(
            externalContactPerson: $externalContactPerson,
            accountUser: $accountUser
        );
    }
}
