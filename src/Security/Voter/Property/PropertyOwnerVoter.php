<?php

declare(strict_types=1);

namespace App\Security\Voter\Property;

use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\Property\PropertyOwner;
use App\Domain\Property\PropertyOwnerSecurityService;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class PropertyOwnerVoter extends Voter
{
    const VIEW = 'view';
    const EDIT = 'edit';
    const DELETE = 'delete';

    public function __construct(
        private readonly PropertyOwnerSecurityService $propertyOwnerSecurityService
    ) {
    }

    protected function supports(string $attribute, mixed $subject): bool
    {
        if (in_array(needle: $attribute, haystack: [self::VIEW, self::EDIT, self::DELETE]) === false) {
            return false;
        }

        if (!$subject instanceof PropertyOwner) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        return match ($attribute) {
            self::VIEW => $this->canView(propertyOwner: $subject, accountUser: $user),
            self::EDIT => $this->canEdit(propertyOwner: $subject, accountUser: $user),
            self::DELETE => $this->canDelete(propertyOwner: $subject, accountUser: $user),
            default => throw new \LogicException(message: 'This code should not be reached!')
        };
    }

    private function canView(PropertyOwner $propertyOwner, AccountUser $accountUser): bool
    {
        return $this->propertyOwnerSecurityService->canViewPropertyOwner(propertyOwner: $propertyOwner, accountUser: $accountUser);
    }

    private function canEdit(PropertyOwner $propertyOwner, AccountUser $accountUser): bool
    {
        return $this->propertyOwnerSecurityService->canEditPropertyOwner(propertyOwner: $propertyOwner, accountUser: $accountUser);
    }

    private function canDelete(PropertyOwner $propertyOwner, AccountUser $accountUser): bool
    {
        return $this->propertyOwnerSecurityService->canDeletePropertyOwner(propertyOwner: $propertyOwner, accountUser: $accountUser);
    }
}
