<?php

declare(strict_types=1);

namespace App\Security\Voter\Property;

use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\Property\PropertyContactPerson;
use App\Domain\Property\PropertyContactPersonSecurityService;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class PropertyContactPersonVoter extends Voter
{
    const VIEW = 'view';
    const EDIT = 'edit';
    const DELETE = 'delete';

    public function __construct(
        private readonly PropertyContactPersonSecurityService $propertyContactPersonSecurityService
    ) {
    }

    protected function supports(string $attribute, mixed $subject): bool
    {
        if (in_array(needle: $attribute, haystack: [self::VIEW, self::EDIT, self::DELETE]) === false) {
            return false;
        }

        if (!$subject instanceof PropertyContactPerson) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        return match ($attribute) {
            self::VIEW => $this->canView(propertyContactPerson: $subject, accountUser: $user),
            self::EDIT => $this->canEdit(propertyContactPerson: $subject, accountUser: $user),
            self::DELETE => $this->canDelete(propertyContactPerson: $subject, accountUser: $user),
            default => throw new \LogicException(message: 'This code should not be reached!')
        };
    }

    private function canView(PropertyContactPerson $propertyContactPerson, AccountUser $accountUser): bool
    {
        return $this->propertyContactPersonSecurityService->canViewPropertyContactPerson(
            propertyContactPerson: $propertyContactPerson,
            accountUser: $accountUser
        );
    }

    private function canEdit(PropertyContactPerson $propertyContactPerson, AccountUser $accountUser): bool
    {
        return $this->propertyContactPersonSecurityService->canEditPropertyContactPerson(
            propertyContactPerson: $propertyContactPerson,
            accountUser: $accountUser
        );
    }

    private function canDelete(PropertyContactPerson $propertyContactPerson, AccountUser $accountUser): bool
    {
        return $this->propertyContactPersonSecurityService->canDeletePropertyContactPerson(
            propertyContactPerson: $propertyContactPerson,
            accountUser: $accountUser
        );
    }
}
