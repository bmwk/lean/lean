<?php

declare(strict_types=1);

namespace App\Security\Voter\Property;

use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\Property\PropertyUser;
use App\Domain\Property\PropertyUserSecurityService;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class PropertyUserVoter extends Voter
{
    const VIEW = 'view';
    const EDIT = 'edit';
    const DELETE = 'delete';

    public function __construct(
        private readonly PropertyUserSecurityService $propertyUserSecurityService
    ) {
    }

    protected function supports(string $attribute, mixed $subject): bool
    {
        if (in_array(needle: $attribute, haystack: [self::VIEW, self::EDIT, self::DELETE]) === false) {
            return false;
        }

        if (!$subject instanceof PropertyUser) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        return match ($attribute) {
            self::VIEW => $this->canView(propertyUser: $subject, accountUser: $user),
            self::EDIT => $this->canEdit(propertyUser: $subject, accountUser: $user),
            self::DELETE => $this->canDelete(propertyUser: $subject, accountUser: $user),
            default => throw new \LogicException(message: 'This code should not be reached!')
        };
    }

    private function canView(PropertyUser $propertyUser, AccountUser $accountUser): bool
    {
        return $this->propertyUserSecurityService->canViewPropertyUser(propertyUser: $propertyUser, accountUser: $accountUser);
    }

    private function canEdit(PropertyUser $propertyUser, AccountUser $accountUser): bool
    {
        return $this->propertyUserSecurityService->canEditPropertyUser(propertyUser: $propertyUser, accountUser: $accountUser);
    }

    private function canDelete(PropertyUser $propertyUser, AccountUser $accountUser): bool
    {
        return $this->propertyUserSecurityService->canDeletePropertyUser(propertyUser: $propertyUser, accountUser: $accountUser);
    }
}
