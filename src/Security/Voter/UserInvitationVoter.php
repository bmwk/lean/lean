<?php

declare(strict_types=1);

namespace App\Security\Voter;

use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\UserRegistration\UserInvitation;
use App\Domain\UserRegistration\UserInvitationSecurityService;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class UserInvitationVoter extends Voter
{
    const VIEW = 'view';
    const EDIT = 'edit';
    const DELETE = 'delete';

    public function __construct(
        private readonly UserInvitationSecurityService $userInvitationSecurityService
    ) {
    }

    protected function supports(string $attribute, mixed $subject): bool
    {
        if (in_array(needle: $attribute, haystack: [self::VIEW, self::EDIT, self::DELETE]) === false) {
            return false;
        }

        if (!$subject instanceof UserInvitation) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        return match ($attribute) {
            self::VIEW => $this->canView(userInvitation: $subject, accountUser: $user),
            self::EDIT => $this->canEdit(userInvitation: $subject, accountUser: $user),
            self::DELETE => $this->canDelete(userInvitation: $subject, accountUser: $user),
            default => throw new \LogicException(message: 'This code should not be reached!')
        };
    }

    private function canView(UserInvitation $userInvitation, AccountUSer $accountUser): bool
    {
        return $this->userInvitationSecurityService->canViewUserInvitation(userInvitation: $userInvitation, accountUser: $accountUser);
    }

    private function canEdit(UserInvitation $userInvitation, AccountUser $accountUser): bool
    {
        return $this->userInvitationSecurityService->canEditUserInvitation(userInvitation: $userInvitation, accountUser: $accountUser);
    }

    private function canDelete(UserInvitation $userInvitation, AccountUser $accountUser): bool
    {
        return $this->userInvitationSecurityService->canDeleteUserInvitation(userInvitation: $userInvitation, accountUser: $accountUser);
    }
}
