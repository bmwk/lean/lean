<?php

declare(strict_types=1);

namespace App\Security\Voter;

use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\SettlementConcept\SettlementConceptAccountUser;
use App\Domain\SettlementConcept\SettlementConceptAccountUserSecurityService;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class SettlementConceptAccountUserVoter extends Voter
{
    private const VIEW = 'view';
    private const EDIT = 'edit';
    private const DELETE = 'delete';

    public function __construct(
        private readonly SettlementConceptAccountUserSecurityService $settlementConceptAccountUserSecurityService
    ) {
    }

    protected function supports(string $attribute, mixed $subject): bool
    {
        if (in_array(needle: $attribute, haystack: [self::VIEW, self::EDIT, self::DELETE]) === false) {
            return false;
        }

        if (!$subject instanceof SettlementConceptAccountUser) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        return match ($attribute) {
            self::VIEW => $this->canView(settlementConceptAccountUser: $subject, user: $user),
            self::EDIT => $this->canEdit(settlementConceptAccountUser: $subject, user: $user),
            self::DELETE => $this->canDelete(settlementConceptAccountUser: $subject, user: $user),
            default => throw new \LogicException(message: 'This code should not be reached!')
        };
    }

    private function canView(SettlementConceptAccountUser $settlementConceptAccountUser, AccountUser $user): bool
    {
        return $this->settlementConceptAccountUserSecurityService->canViewSettlementConceptAccountUser(
            settlementConceptAccountUser: $settlementConceptAccountUser,
            accountUser: $user
        );
    }

    private function canEdit(SettlementConceptAccountUser $settlementConceptAccountUser, AccountUser $user): bool
    {
        return $this->settlementConceptAccountUserSecurityService->canEditSettlementConceptAccountUser(
            settlementConceptAccountUser: $settlementConceptAccountUser,
            accountUser: $user
        );
    }

    private function canDelete(SettlementConceptAccountUser $settlementConceptAccountUser, AccountUser $user): bool
    {
        return $this->settlementConceptAccountUserSecurityService->canDeleteSettlementConceptAccountUser(
            settlementConceptAccountUser: $settlementConceptAccountUser,
            accountUser: $user
        );
    }
}
