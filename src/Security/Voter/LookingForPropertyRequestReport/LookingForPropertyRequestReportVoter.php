<?php

declare(strict_types=1);

namespace App\Security\Voter\LookingForPropertyRequestReport;

use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\LookingForPropertyRequestReport\LookingForPropertyRequestReport;
use App\Domain\LookingForPropertyRequestReport\LookingForPropertyRequestReportSecurityService;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class LookingForPropertyRequestReportVoter extends Voter
{
    const VIEW = 'view';
    const EDIT = 'edit';
    const DELETE = 'delete';

    public function __construct(
        private readonly LookingForPropertyRequestReportSecurityService $lookingForPropertyRequestReportSecurityService
    ) {
    }

    protected function supports(string $attribute, mixed $subject): bool
    {
        if (in_array(needle: $attribute, haystack: [self::VIEW, self::EDIT, self::DELETE]) === false) {
            return false;
        }

        if (!$subject instanceof LookingForPropertyRequestReport) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        return match ($attribute) {
            self::VIEW => $this->canView(lookingForPropertyRequestReport: $subject, accountUser: $user),
            self::EDIT => $this->canEdit(lookingForPropertyRequestReport: $subject, accountUser: $user),
            self::DELETE => $this->canDelete(lookingForPropertyRequestReport: $subject, accountUser: $user),
            default => throw new \LogicException(message: 'This code should not be reached!')
        };
    }

    private function canView(LookingForPropertyRequestReport $lookingForPropertyRequestReport, AccountUSer $accountUser): bool
    {
        return $this->lookingForPropertyRequestReportSecurityService->canViewLookingForPropertyRequestReport(
            lookingForPropertyRequestReport: $lookingForPropertyRequestReport,
            accountUser: $accountUser
        );
    }

    private function canEdit(LookingForPropertyRequestReport $lookingForPropertyRequestReport, AccountUser $accountUser): bool
    {
        return $this->lookingForPropertyRequestReportSecurityService->canEditLookingForPropertyRequestReport(
            lookingForPropertyRequestReport: $lookingForPropertyRequestReport,
            accountUser: $accountUser
        );
    }

    private function canDelete(LookingForPropertyRequestReport $lookingForPropertyRequestReport, AccountUser $accountUser): bool
    {
        return $this->lookingForPropertyRequestReportSecurityService->canDeleteLookingForPropertyRequestReport(
            lookingForPropertyRequestReport: $lookingForPropertyRequestReport,
            accountUser: $accountUser
        );
    }
}
