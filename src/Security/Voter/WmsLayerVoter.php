<?php

declare(strict_types=1);

namespace App\Security\Voter;

use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\WmsLayer;
use App\Domain\WmsLayer\WmsLayerSecurityService;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class WmsLayerVoter extends Voter
{
    const VIEW = 'view';
    const EDIT = 'edit';
    const DELETE = 'delete';

    public function __construct(
        private readonly WmsLayerSecurityService $wmsLayerSecurityService
    ) {
    }

    protected function supports(string $attribute, mixed $subject): bool
    {
        if (in_array(needle: $attribute, haystack: [self::VIEW, self::EDIT, self::DELETE]) === false) {
            return false;
        }

        if (!$subject instanceof WmsLayer) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        return match ($attribute) {
            self::VIEW => $this->canView(wmsLayer: $subject, accountUser: $user),
            self::EDIT => $this->canEdit(wmsLayer: $subject, accountUser: $user),
            self::DELETE => $this->canDelete(wmsLayer: $subject, accountUser: $user),
            default => throw new \LogicException(message: 'This code should not be reached!')
        };
    }

    private function canView(WmsLayer $wmsLayer, AccountUSer $accountUser): bool
    {
        return $this->wmsLayerSecurityService->canViewWmsLayer(wmsLayer: $wmsLayer, accountUser: $accountUser);
    }

    private function canEdit(WmsLayer $wmsLayer, AccountUser $accountUser): bool
    {
        return $this->wmsLayerSecurityService->canEditWmsLayer(wmsLayer: $wmsLayer, accountUser: $accountUser);
    }

    private function canDelete(WmsLayer $wmsLayer, AccountUser $accountUser): bool
    {
        return $this->wmsLayerSecurityService->canDeleteWmsLayer(wmsLayer: $wmsLayer, accountUser: $accountUser);
    }
}
