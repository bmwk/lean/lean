<?php

declare(strict_types=1);

namespace App\Security;

class InvalidJsonWebTokenException extends \RuntimeException
{
}
