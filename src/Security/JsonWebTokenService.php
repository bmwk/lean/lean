<?php

declare(strict_types=1);

namespace App\Security;

final class JsonWebTokenService
{
    private readonly array $header;

    public function __construct(
        private readonly string $secret
    ) {
        $this->header = [
            'alg' => 'HS512',
            'typ' => 'JWT',
        ];
    }

    public function createToken(array $payloadData): string
    {
        $header = self::base64UrlEncode(value: json_encode($this->header));
        $payload = self::base64UrlEncode(value: json_encode($payloadData));
        $signature = $this->createTokenSignature(header: $header, payload: $payload);

        return $header . '.' . $payload . '.' . $signature;
    }

    public function isTokenValid(string $token): bool
    {
        if (count(explode('.', $token)) !== 3) {
            throw new InvalidJsonWebTokenException();
        }

        [$header, $payload, $signature] = explode('.', $token);

        if (empty($header) === true) {
            throw new InvalidJsonWebTokenException();
        }

        if (empty($payload) === true) {
            throw new InvalidJsonWebTokenException();
        }

        if (empty($signature) === true) {
            throw new InvalidJsonWebTokenException();
        }

        if ($signature === $this->createTokenSignature(header: $header, payload: $payload)) {
            return true;
        }

        return false;
    }

    public function fetchPayloadDataFromToken(string $token): \stdClass
    {
        if ($this->isTokenValid(token: $token) !== true) {
            throw new InvalidJsonWebTokenException();
        }

        [$header, $payload, $signature] = explode('.', $token);

        return json_decode($this->base64UrlDecode(value: $payload));
    }

    private function createTokenSignature(string $header, string $payload): string
    {
        return hash_hmac(algo: 'sha512', data: $header . '.' . $payload, key: $this->secret);
    }

    private static function base64UrlEncode(string $value): string
    {
        return str_replace(['+','/','='], ['-','_',''], base64_encode($value));
    }

    private static function base64UrlDecode(string $value): string
    {
        return base64_decode(str_replace(['-','_'], ['+','/'], $value));
    }
}
