<?php

declare(strict_types=1);

namespace App\Security;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Http\Authenticator\AbstractAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\Authenticator\Passport\SelfValidatingPassport;

class SettlementConceptJsonWebTokenAuthenticator extends AbstractAuthenticator
{
    public function __construct(
        private readonly JsonWebTokenService $jsonWebTokenService
    ) {
    }

    public function supports(Request $request): bool
    {
        return $request->headers->has(key: 'Authorization');
    }

    public function authenticate(Request $request): Passport
    {
        $headerValue = $request->headers->get(key: 'Authorization');
        $apiToken = explode(' ', $headerValue)[1];

        if ($apiToken === null) {
            throw new CustomUserMessageAuthenticationException(message: 'No API token was provided');
        }

        try {
            if ($this->jsonWebTokenService->isTokenValid(token: $apiToken) !== true) {
                throw new CustomUserMessageAuthenticationException(message:'Invalid API token provided');
            }

            $payloadData = $this->jsonWebTokenService->fetchPayloadDataFromToken(token: $apiToken);

            if (empty($payloadData->identifier) === true) {
                throw new CustomUserMessageAuthenticationException(message:'Invalid API token provided');
            }
        } catch (InvalidJsonWebTokenException $invalidJsonWebTokenException) {
            throw new CustomUserMessageAuthenticationException(message:'Invalid API token provided');
        }

        return new SelfValidatingPassport(userBadge: new UserBadge(userIdentifier: $payloadData->identifier));
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        return null;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {
        return new JsonResponse(
            data: ['message' => strtr($exception->getMessageKey(), $exception->getMessageData())],
            status: Response::HTTP_UNAUTHORIZED
        );
    }
}
