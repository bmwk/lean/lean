<?php

declare(strict_types=1);

namespace App\Security;

use App\Domain\Entity\SettlementConcept\SettlementConceptAccountUser;
use Symfony\Component\Security\Core\Exception\UserNotFoundException;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class SettlementConceptAccountUserChecker implements UserCheckerInterface
{
    public function checkPreAuth(UserInterface $user): void
    {
        if (!$user instanceof SettlementConceptAccountUser) {
            return;
        }

        if ($user->getAccount()->isEnabled() !== true) {
            throw new UserNotFoundException();
        }

        if ($user->getAccount()->isDeleted() === true) {
            throw new UserNotFoundException();
        }

        if ($user->getAccount()->isAnonymized() === true) {
            throw new UserNotFoundException();
        }

        if ($user->isEnabled() !== true) {
            throw new UserNotFoundException();
        }

        if ($user->isDeleted() === true) {
            throw new UserNotFoundException();
        }
    }

    public function checkPostAuth(UserInterface $user): void
    {
        if (!$user instanceof SettlementConceptAccountUser) {
            return;
        }
    }
}
