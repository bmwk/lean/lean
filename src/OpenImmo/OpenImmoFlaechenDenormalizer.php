<?php

declare(strict_types=1);

namespace App\OpenImmo;

use App\OpenImmo\Entity\Flaechen;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use Symfony\Component\PropertyInfo\PropertyTypeExtractorInterface;
use Symfony\Component\Serializer\Mapping\ClassDiscriminatorResolverInterface;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactoryInterface;
use Symfony\Component\Serializer\NameConverter\NameConverterInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class OpenImmoFlaechenDenormalizer implements DenormalizerInterface
{
    private readonly ObjectNormalizer $objectNormalizer;

    public function __construct(
        ClassMetadataFactoryInterface $classMetadataFactory = null,
        NameConverterInterface $nameConverter = null,
        PropertyAccessorInterface  $propertyAccessor = null,
        PropertyTypeExtractorInterface $propertyTypeExtractor = null,
        ClassDiscriminatorResolverInterface $classDiscriminatorResolver = null,
        callable $objectClassResolver = null,
        array $defaultContext = []
    ) {
        $this->objectNormalizer = new ObjectNormalizer(
            classMetadataFactory: $classMetadataFactory,
            nameConverter: $nameConverter,
            propertyAccessor: $propertyAccessor,
            propertyTypeExtractor: $propertyTypeExtractor,
            classDiscriminatorResolver: $classDiscriminatorResolver,
            objectClassResolver: $objectClassResolver,
            defaultContext: $defaultContext
        );
    }

    public function supportsDenormalization(mixed $data, string $type, string $format = null): bool
    {
        return $type === Flaechen::class && $format === 'xml' && is_array(value: $data) === true;
    }

    public function denormalize(mixed $data, string $type, string $format = null, array $context = [])
    {
        if (isset($data['anzahl_stellplaetze']) === true) {
            $data['anzahl_stellplaetze'] = intval(value: $data['anzahl_stellplaetze']);
        }

        return $this->objectNormalizer->denormalize(data: $data, type: $type, format: $format, context: $context);
    }
}
