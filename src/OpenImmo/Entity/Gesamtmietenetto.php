<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class Gesamtmietenetto
{
    #[SerializedName('@gesamtmieteust')]
    private ?float $gesamtmieteust = null;

    #[SerializedName('#')]
    private ?float $value = null;

    public function getGesamtmieteust(): ?float
    {
        return $this->gesamtmieteust;
    }

    public function setGesamtmieteust(?float $gesamtmieteust): self
    {
        $this->gesamtmieteust = $gesamtmieteust;
        return $this;
    }

    public function getValue(): ?float
    {
        return $this->value;
    }

    public function setValue(?float $value): self
    {
        $this->value = $value;
        return $this;
    }
}
