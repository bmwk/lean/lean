<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class Parken
{
    #[SerializedName('@parken_typ')]
    protected string $parkenTyp;

    public function getParkenTyp(): ?string
    {
        return $this->parkenTyp;
    }

    public function setParkenTyp(?string $parkenTyp): self
    {
        $this->parkenTyp = $parkenTyp;
        return $this;
    }
}
