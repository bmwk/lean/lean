<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

class Objektart
{
    private ?array $zimmer = [];

    private ?array $wohnung = [];

    private ?array $haus = [];

    private ?array $grundstueck = [];

    private ?array $bueroPraxen = [];

    private ?array $einzelhandel = [];

    private ?array $gastgewerbe = [];

    private ?array $hallenLagerProd = [];

    private ?array $landUndForstwirtschaft = [];

    private ?array $parken = [];

    private ?array $sonstige = [];

    private ?array $freizeitimmobilieGewerblich = [];

    private ?array $zinshausRenditeobjekt = [];

    private ?array $objektartZusatz = [];

    public function getZimmer(): ?array
    {
        return $this->zimmer ?? [];
    }

    public function addZimmer(Zimmer $zimmer): self
    {
        $this->zimmer[] = $zimmer;
        return $this;
    }

    public function removeZimmer(Zimmer $zimmer): self
    {
        return $this;
    }

    public function getWohnung(): ?array
    {
        return $this->wohnung ?? [];
    }

    public function addWohnung(Wohnung $wohnung): self
    {
        $this->wohnung[] = $wohnung;
        return $this;
    }

    public function removeWohnung(Wohnung $wohnung): self
    {
        return $this;
    }

    public function getHaus(): ?array
    {
        return $this->haus ?? [];
    }

    public function addHaus(Haus $haus): self
    {
        $this->haus[] = $haus;
        return $this;
    }

    public function removeHaus(Haus $haus): self
    {
        return $this;
    }

    public function getGrundstueck(): ?array
    {
        return $this->grundstueck ?? [];
    }

    public function addGrundstueck(Grundstueck $grundstueck): self
    {
        $this->grundstueck[] = $grundstueck;
        return $this;
    }

    public function removeGrundstueck(Grundstueck $grundstueck): self
    {
        return $this;
    }

    public function getBueroPraxen(): ?array
    {
        return $this->bueroPraxen ?? [];
    }

    public function addBueroPraxen(BueroPraxen $bueroPraxen): self
    {
        $this->bueroPraxen[] = $bueroPraxen;
        return $this;
    }

    public function removeBueroPraxen(BueroPraxen $bueroPraxen): self
    {
        return $this;
    }

    public function getEinzelhandel(): ?array
    {
        return $this->einzelhandel ?? [];
    }

    public function addEinzelhandel(Einzelhandel $einzelhandel): self
    {
        $this->einzelhandel[] = $einzelhandel;
        return $this;
    }

    public function removeEinzelhandel(Einzelhandel $einzelhandel): self
    {
        return $this;
    }

    public function getGastgewerbe(): ?array
    {
        return $this->gastgewerbe ?? [];
    }

    public function addGastgewerbe(Gastgewerbe $gastgewerbe): self
    {
        $this->gastgewerbe[] = $gastgewerbe;
        return $this;
    }

    public function removeGastgewerbe(Gastgewerbe $gastgewerbe): self
    {
        return $this;
    }

    public function getHallenLagerProd(): ?array
    {
        return $this->hallenLagerProd ?? [];
    }

    public function addHallenLagerProd(HallenLagerProd $hallenLagerProd): self
    {
        $this->hallenLagerProd[] = $hallenLagerProd;
        return $this;
    }

    public function removeHallenLagerProd(HallenLagerProd $hallenLagerProd): self
    {
        return $this;
    }

    public function getLandUndForstwirtschaft(): ?array
    {
        return $this->landUndForstwirtschaft ?? [];
    }

    public function addLandUndForstwirtschaft(LandUndForstwirtschaft $landUndForstwirtschaft): self
    {
        $this->landUndForstwirtschaft[] = $landUndForstwirtschaft;
        return $this;
    }

    public function removeLandUndForstwirtschaft(LandUndForstwirtschaft $landUndForstwirtschaft): self
    {
        return $this;
    }

    public function getParken(): ?array
    {
        return $this->parken ?? [];
    }

    public function addParken(Parken $parken): self
    {
        $this->parken[] = $parken;
        return $this;
    }

    public function removeParken(Parken $parken): self
    {
        return $this;
    }

    public function getSonstige(): ?array
    {
        return $this->sonstige ?? [];
    }

    public function addSonstige(Sonstige $sonstige): self
    {
        $this->sonstige = $sonstige;
        return $this;
    }

    public function removeSonstige(Sonstige $sonstige): self
    {
        return $this;
    }

    public function getFreizeitimmobilieGewerblich(): ?array
    {
        return $this->freizeitimmobilieGewerblich ?? [];
    }

    public function addFreizeitimmobilieGewerblich(FreizeitimmobilieGewerblich $freizeitimmobilieGewerblich): self
    {
        $this->freizeitimmobilieGewerblich[] = $freizeitimmobilieGewerblich;
        return $this;
    }

    public function removeFreizeitimmobilieGewerblich(FreizeitimmobilieGewerblich $freizeitimmobilieGewerblich): self
    {
        return $this;
    }

    public function getZinshausRenditeobjekt(): ?array
    {
        return $this->zinshausRenditeobjekt ?? [];
    }

    public function addZinshausRenditeobjekt(ZinshausRenditeobjekt $zinshausRenditeobjekt): self
    {
        $this->zinshausRenditeobjekt[] = $zinshausRenditeobjekt;
        return $this;
    }

    public function removeZinshausRenditeobjekt(ZinshausRenditeobjekt $zinshausRenditeobjekt): self
    {
        return $this;
    }

    public function getObjektartZusatz(): ?array
    {
        return $this->objektartZusatz ?? [];
    }

    public function addObjektartZusatz(string $objektartZusatz): self
    {
        $this->objektartZusatz[] = $objektartZusatz;
        return $this;
    }

    public function removeObjektartZusatz(string $objektartZusatz): self
    {
        return $this;
    }
}
