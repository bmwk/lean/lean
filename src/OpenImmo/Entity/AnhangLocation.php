<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

enum AnhangLocation: string
{
    case EXTERN = 'EXTERN';
    case INTERN = 'INTERN';
    case REMOTE = 'REMOTE';
}
