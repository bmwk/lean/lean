<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class Stellplatzart
{
    #[SerializedName('@GARAGE')]
    private ?bool $garage = null;

    #[SerializedName('@TIEFGARAGE')]
    private ?bool $tiefgarage = null;

    #[SerializedName('@CARPORT')]
    private ?bool $carport = null;

    #[SerializedName('@FREIPLATZ')]
    private ?bool $freiplatz = null;

    #[SerializedName('@PARKHAUS')]
    private ?bool $parkhaus = null;

    #[SerializedName('@DUPLEX')]
    private ?bool $duplex = null;

    public function getGarage(): ?bool
    {
        return $this->garage;
    }

    public function setGarage(?bool $garage): self
    {
        $this->garage = $garage;
        return $this;
    }

    public function getTiefgarage(): ?bool
    {
        return $this->tiefgarage;
    }

    public function setTiefgarage(?bool $tiefgarage): self
    {
        $this->tiefgarage = $tiefgarage;
        return $this;
    }

    public function getCarport(): ?bool
    {
        return $this->carport;
    }

    public function setCarport(?bool $carport): self
    {
        $this->carport = $carport;
        return $this;
    }

    public function getFreiplatz(): ?bool
    {
        return $this->freiplatz;
    }

    public function setFreiplatz(?bool $freiplatz): self
    {
        $this->freiplatz = $freiplatz;
        return $this;
    }

    public function getParkhaus(): ?bool
    {
        return $this->parkhaus;
    }

    public function setParkhaus(?bool $parkhaus): self
    {
        $this->parkhaus = $parkhaus;
        return $this;
    }

    public function getDuplex(): ?bool
    {
        return $this->duplex;
    }

    public function setDuplex(?bool $duplex): self
    {
        $this->duplex = $duplex;
        return $this;
    }
}
