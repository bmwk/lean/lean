<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class Geschlecht
{
    #[SerializedName('@geschl_attr')]
    private ?string $geschlAttr = null;

    public function getGeschlAttr(): ?string
    {
        return $this->geschlAttr;
    }

    public function setGeschlAttr(?string $geschlAttr): self
    {
        $this->geschlAttr = $geschlAttr;
        return $this;
    }
}
