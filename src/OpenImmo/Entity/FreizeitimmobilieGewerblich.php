<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class FreizeitimmobilieGewerblich
{
    #[SerializedName('@freizeit_typ')]
    private ?string $freizeitTyp = null;

    public function getFreizeitTyp(): ?string
    {
        return $this->freizeitTyp;
    }

    public function setFreizeitTyp(?string $freizeitTyp): self
    {
        $this->freizeitTyp = $freizeitTyp;
        return $this;
    }
}
