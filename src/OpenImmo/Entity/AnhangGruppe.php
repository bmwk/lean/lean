<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

enum AnhangGruppe: string
{
    case MAIN_IMAGE = 'TITELBILD';
    case INSIDE_VIEW = 'INNENANSICHT';
    case OUTSIDE_VIEW = 'AUSSENANSICHT';
    case FLOOR_PLAN = 'GRUNDRISS';
    case SIDE_PLAN = "KARTEN_LAGEPLAN";
    case PROVIDERLOGO = 'ANBIETERLOGO';
    case IMAGE = 'BILD';
    case DOCUMENT = 'DOKUMENT';
    case LINKS = 'LINKS';
    case PANORAMA = 'PANORAMA';
    case QRCODE = 'QRCODE';
    case MOVIE = 'FILM';
    case MOVIE_LINK = 'FILMLINK';
    case SCALE_OF_EPASSPORT = 'EPASS_SKALA';
    case PROVIDER_URL = 'ANBOBJURL';
}
