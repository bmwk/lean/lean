<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class InnenCourtage
{
    #[SerializedName('@mit_mwst')]
    private ?bool $mitMwst = null;

    #[SerializedName('#')]
    private ?string $value = null;

    public function getMitMwst(): ?bool
    {
        return $this->mitMwst;
    }

    public function setMitMwst(?bool $mitMwst): self
    {
        $this->mitMwst = $mitMwst;
        return $this;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(?string $value): self
    {
        $this->value = $value;
        return $this;
    }
}
