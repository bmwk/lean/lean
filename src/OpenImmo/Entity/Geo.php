<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class Geo
{
    private ?string $plz = null;

    private ?string $ort = null;

    private ?Geokoordinaten $geokoordinaten = null;

    private ?string $strasse = null;

    private ?string $hausnummer = null;

    private ?string $bundesland = null;

    private ?Land $land = null;

    private ?string $gemeindecode = null;

    private ?string $flur = null;

    private ?string $flurstueck = null;

    private ?string $gemarkung = null;

    private ?int $etage = null;

    private ?int $anzahlEtagen = null;

    #[SerializedName('@lage_im_bau')]
    private ?LageImBau $lageImBau = null;

    private ?string $wohnungsnr = null;

    private ?LageGebiet $lageGebiet = null;

    private ?string $regionalerZusatz = null;

    private ?bool $kartenMakro = null;

    private ?bool $kartenMikro = null;

    private ?bool $virtuelletour = null;

    private ?bool $luftbildern = null;

    public function getPlz(): ?string
    {
        return $this->plz;
    }

    public function setPlz(?string $plz): self
    {
        $this->plz = $plz;
        return $this;
    }

    public function getOrt(): ?string
    {
        return $this->ort;
    }

    public function setOrt(?string $ort): self
    {
        $this->ort = $ort;
        return $this;
    }

    public function getGeokoordinaten(): ?Geokoordinaten
    {
        return $this->geokoordinaten;
    }

    public function setGeokoordinaten(?Geokoordinaten $geokoordinaten): self
    {
        $this->geokoordinaten = $geokoordinaten;
        return $this;
    }

    public function getStrasse(): ?string
    {
        return $this->strasse;
    }

    public function setStrasse(?string $strasse): self
    {
        $this->strasse = $strasse;
        return $this;
    }

    public function getHausnummer(): ?string
    {
        return $this->hausnummer;
    }

    public function setHausnummer(?string $hausnummer): self
    {
        $this->hausnummer = $hausnummer;
        return $this;
    }

    public function getBundesland(): ?string
    {
        return $this->bundesland;
    }

    public function setBundesland(?string $bundesland): self
    {
        $this->bundesland = $bundesland;
        return $this;
    }

    public function getLand(): ?Land
    {
        return $this->land;
    }

    public function setLand(?Land $land): self
    {
        $this->land = $land;
        return $this;
    }

    public function getGemeindecode(): ?string
    {
        return $this->gemeindecode;
    }

    public function setGemeindecode(?string $gemeindecode): self
    {
        $this->gemeindecode = $gemeindecode;
        return $this;
    }

    public function getFlur(): ?string
    {
        return $this->flur;
    }

    public function setFlur(?string $flur): self
    {
        $this->flur = $flur;
        return $this;
    }

    public function getFlurstueck(): ?string
    {
        return $this->flurstueck;
    }

    public function setFlurstueck(?string $flurstueck): self
    {
        $this->flurstueck = $flurstueck;
        return $this;
    }

    public function getGemarkung(): ?string
    {
        return $this->gemarkung;
    }

    public function setGemarkung(?string $gemarkung): self
    {
        $this->gemarkung = $gemarkung;
        return $this;
    }

    public function getEtage(): ?int
    {
        return $this->etage;
    }

    public function setEtage(?int $etage): self
    {
        $this->etage = $etage;
        return $this;
    }

    public function getAnzahlEtagen(): ?int
    {
        return $this->anzahlEtagen;
    }

    public function setAnzahlEtagen(?int $anzahlEtagen): self
    {
        $this->anzahlEtagen = $anzahlEtagen;
        return $this;
    }

    public function getLageImBau(): ?LageImBau
    {
        return $this->lageImBau;
    }

    public function setLageImBau(?LageImBau $lageImBau): self
    {
        $this->lageImBau = $lageImBau;
        return $this;
    }

    public function getWohnungsnr(): ?string
    {
        return $this->wohnungsnr;
    }

    public function setWohnungsnr(?string $wohnungsnr): self
    {
        $this->wohnungsnr = $wohnungsnr;
        return $this;
    }

    public function getLageGebiet(): ?LageGebiet
    {
        return $this->lageGebiet;
    }

    public function setLageGebiet(?LageGebiet $lageGebiet): self
    {
        $this->lageGebiet = $lageGebiet;
        return $this;
    }

    public function getRegionalerZusatz(): ?string
    {
        return $this->regionalerZusatz;
    }

    public function setRegionalerZusatz(?string $regionalerZusatz): self
    {
        $this->regionalerZusatz = $regionalerZusatz;
        return $this;
    }

    public function getKartenMakro(): ?bool
    {
        return $this->kartenMakro;
    }

    public function setKartenMakro(?bool $kartenMakro): self
    {
        $this->kartenMakro = $kartenMakro;
        return $this;
    }

    public function getKartenMikro(): ?bool
    {
        return $this->kartenMikro;
    }

    public function setKartenMikro(?bool $kartenMikro): self
    {
        $this->kartenMikro = $kartenMikro;
        return $this;
    }

    public function getVirtuelletour(): ?bool
    {
        return $this->virtuelletour;
    }

    public function setVirtuelletour(?bool $virtuelletour): self
    {
        $this->virtuelletour = $virtuelletour;
        return $this;
    }

    public function getLuftbildern(): ?bool
    {
        return $this->luftbildern;
    }

    public function setLuftbildern(?bool $luftbildern): self
    {
        $this->luftbildern = $luftbildern;
        return $this;
    }
}
