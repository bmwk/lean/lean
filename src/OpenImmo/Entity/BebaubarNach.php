<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

class BebaubarNach
{
    private ?string $bebaubarAttr = null;

    public function getBebaubarAttr(): ?string
    {
        return $this->bebaubarAttr;
    }

    public function setBebaubarAttr(?string $bebaubarAttr): self
    {
        $this->bebaubarAttr = $bebaubarAttr;
        return $this;
    }
}
