<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class Anhaenge
{
    private Collection|array $anhang;

    private ?array $userDefinedSimplefield = [];

    private ?array $userDefinedAnyfield = [];

    private ?array $userDefinedExtend = [];

    public function __construct()
    {
        $this->anhang = new ArrayCollection();
    }

    public function getAnhang(): Collection|array
    {
        return $this->anhang;
    }

    public function addAnhang(Anhang $anhang): self
    {
        $this->anhang->add($anhang);
        return $this;
    }

    public function removeAnhang(Anhang $anhang): self
    {
        $this->anhang->removeElement($anhang);
        return $this;
    }

    public function getUserDefinedSimplefield(): ?array
    {
        return $this->userDefinedSimplefield ?? [];
    }

    public function setUserDefinedSimplefield(?array $userDefinedSimplefield): Anhaenge
    {
        $this->userDefinedSimplefield = $userDefinedSimplefield;
        return $this;
    }

    public function getUserDefinedAnyfield(): ?array
    {
        return $this->userDefinedAnyfield ?? [];
    }

    public function setUserDefinedAnyfield(?array $userDefinedAnyfield): Anhaenge
    {
        $this->userDefinedAnyfield = $userDefinedAnyfield;
        return $this;
    }

    public function getUserDefinedExtend(): ?array
    {
        return $this->userDefinedExtend ?? [];
    }

    public function setUserDefinedExtend(?array $userDefinedExtend): Anhaenge
    {
        $this->userDefinedExtend = $userDefinedExtend;
        return $this;
    }
}
