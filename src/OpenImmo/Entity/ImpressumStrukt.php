<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class ImpressumStrukt
{
    #[SerializedName('@firmenname')]
    private ?string $firmenname ;

    #[SerializedName('@firmenanschrift')]
    private ?string $firmenanschrift ;

    #[SerializedName('@telefon')]
    private ?string $telefon ;

    #[SerializedName('@vertretungsberechtigter')]
    private ?string $vertretungsberechtigter ;

    #[SerializedName('@berufsaufsichtsbehoerde')]
    private ?string $berufsaufsichtsbehoerde ;

    #[SerializedName('@handelsregister')]
    private ?string $handelsregister ;

    #[SerializedName('@handelsregister_nr')]
    private ?string $handelsregisterNr ;

    #[SerializedName('@umsst-id')]
    private ?string $umsstId ;

    #[SerializedName('@steuernummer')]
    private ?string $steuernummer ;

    #[SerializedName('@weiteres')]
    private ?string $weiteres ;

    public function getFirmenname(): ?string
    {
        return $this->firmenname;
    }

    public function setFirmenname(?string $firmenname): self
    {
        $this->firmenname = $firmenname;
        return $this;
    }

    public function getFirmenanschrift(): ?string
    {
        return $this->firmenanschrift;
    }

    public function setFirmenanschrift(?string $firmenanschrift): self
    {
        $this->firmenanschrift = $firmenanschrift;
        return $this;
    }

    public function getTelefon(): ?string
    {
        return $this->telefon;
    }

    public function setTelefon(?string $telefon): self
    {
        $this->telefon = $telefon;
        return $this;
    }

    public function getVertretungsberechtigter(): ?string
    {
        return $this->vertretungsberechtigter;
    }

    public function setVertretungsberechtigter(?string $vertretungsberechtigter): self
    {
        $this->vertretungsberechtigter = $vertretungsberechtigter;
        return $this;
    }

    public function getBerufsaufsichtsbehoerde(): ?string
    {
        return $this->berufsaufsichtsbehoerde;
    }

    public function setBerufsaufsichtsbehoerde(?string $berufsaufsichtsbehoerde): self
    {
        $this->berufsaufsichtsbehoerde = $berufsaufsichtsbehoerde;
        return $this;
    }

    public function getHandelsregister(): ?string
    {
        return $this->handelsregister;
    }

    public function setHandelsregister(?string $handelsregister): self
    {
        $this->handelsregister = $handelsregister;
        return $this;
    }

    public function getHandelsregisterNr(): ?string
    {
        return $this->handelsregisterNr;
    }

    public function setHandelsregisterNr(?string $handelsregisterNr): self
    {
        $this->handelsregisterNr = $handelsregisterNr;
        return $this;
    }

    public function getUmsstId(): ?string
    {
        return $this->umsstId;
    }

    public function setUmsstId(?string $umsstId): self
    {
        $this->umsstId = $umsstId;
        return $this;
    }

    public function getSteuernummer(): ?string
    {
        return $this->steuernummer;
    }

    public function setSteuernummer(?string $steuernummer): self
    {
        $this->steuernummer = $steuernummer;
        return $this;
    }

    public function getWeiteres(): ?string
    {
        return $this->weiteres;
    }

    public function setWeiteres(?string $weiteres): self
    {
        $this->weiteres = $weiteres;
        return $this;
    }
}
