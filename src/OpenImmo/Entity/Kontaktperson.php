<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

class Kontaktperson
{
    private ?string $emailZentrale = null;

    private ?string $emailDirekt = null;

    private ?string $telZentrale = null;

    private ?string $telDurchw = null;

    private ?string $telFax = null;

    private ?string $telHandy = null;

    private ?string $name = null;

    private ?string $vorname = null;

    private ?string $titel = null;

    private ?string $anrede = null;

    private ?string $position = null;

    private ?string $anredeBrief = null;

    private ?string $firma = null;

    private ?string $zusatzfeld = null;

    private ?string $strasse = null;

    private ?string $hausnummer = null;

    private ?string $plz = null;

    private ?string $ort = null;

    private ?string $postfach = null;

    private ?string $postfPlz = null;

    private ?string $postfOrt = null;

    private ?Land $land = null;

    private ?string $emailPrivat = null;

    private ?array $emailSonstige = [];

    private ?string $emailFeedback = null;

    private ?string $telPrivat = null;

    private ?array $telSonstige = [];

    private ?string $url = null;

    private ?bool $adressfreigabe = null;

    private ?string $personennummer = null;

    private ?string $immobilientreuhaenderid = null;

    private ?Foto $foto = null;

    private ?string $referenzId = null;

    private ?string $freitextfeld = null;

    public function getEmailZentrale(): ?string
    {
        return $this->emailZentrale;
    }

    public function setEmailZentrale(?string $emailZentrale): self
    {
        $this->emailZentrale = $emailZentrale;
        return $this;
    }

    public function getEmailDirekt(): ?string
    {
        return $this->emailDirekt;
    }

    public function setEmailDirekt(?string $emailDirekt): self
    {
        $this->emailDirekt = $emailDirekt;
        return $this;
    }

    public function getTelZentrale(): ?string
    {
        return $this->telZentrale;
    }

    public function setTelZentrale(?string $telZentrale): self
    {
        $this->telZentrale = $telZentrale;
        return $this;
    }

    public function getTelDurchw(): ?string
    {
        return $this->telDurchw;
    }

    public function setTelDurchw(?string $telDurchw): self
    {
        $this->telDurchw = $telDurchw;
        return $this;
    }

    public function getTelFax(): ?string
    {
        return $this->telFax;
    }

    public function setTelFax(?string $telFax): self
    {
        $this->telFax = $telFax;
        return $this;
    }

    public function getTelHandy(): ?string
    {
        return $this->telHandy;
    }

    public function setTelHandy(?string $telHandy): self
    {
        $this->telHandy = $telHandy;
        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getVorname(): ?string
    {
        return $this->vorname;
    }

    public function setVorname(?string $vorname): self
    {
        $this->vorname = $vorname;
        return $this;
    }

    public function getTitel(): ?string
    {
        return $this->titel;
    }

    public function setTitel(?string $titel): self
    {
        $this->titel = $titel;
        return $this;
    }

    public function getAnrede(): ?string
    {
        return $this->anrede;
    }

    public function setAnrede(?string $anrede): self
    {
        $this->anrede = $anrede;
        return $this;
    }

    public function getPosition(): ?string
    {
        return $this->position;
    }

    public function setPosition(?string $position): self
    {
        $this->position = $position;
        return $this;
    }

    public function getAnredeBrief(): ?string
    {
        return $this->anredeBrief;
    }

    public function setAnredeBrief(?string $anredeBrief): self
    {
        $this->anredeBrief = $anredeBrief;
        return $this;
    }

    public function getFirma(): ?string
    {
        return $this->firma;
    }

    public function setFirma(?string $firma): self
    {
        $this->firma = $firma;
        return $this;
    }

    public function getZusatzfeld(): ?string
    {
        return $this->zusatzfeld;
    }

    public function setZusatzfeld(?string $zusatzfeld): self
    {
        $this->zusatzfeld = $zusatzfeld;
        return $this;
    }

    public function getStrasse(): ?string
    {
        return $this->strasse;
    }

    public function setStrasse(?string $strasse): self
    {
        $this->strasse = $strasse;
        return $this;
    }

    public function getHausnummer(): ?string
    {
        return $this->hausnummer;
    }

    public function setHausnummer(?string $hausnummer): self
    {
        $this->hausnummer = $hausnummer;
        return $this;
    }

    public function getPlz(): ?string
    {
        return $this->plz;
    }

    public function setPlz(?string $plz): self
    {
        $this->plz = $plz;
        return $this;
    }

    public function getOrt(): ?string
    {
        return $this->ort;
    }

    public function setOrt(?string $ort): self
    {
        $this->ort = $ort;
        return $this;
    }

    public function getPostfach(): ?string
    {
        return $this->postfach;
    }

    public function setPostfach(?string $postfach): self
    {
        $this->postfach = $postfach;
        return $this;
    }

    public function getPostfPlz(): ?string
    {
        return $this->postfPlz;
    }

    public function setPostfPlz(?string $postfPlz): self
    {
        $this->postfPlz = $postfPlz;
        return $this;
    }

    public function getPostfOrt(): ?string
    {
        return $this->postfOrt;
    }

    public function setPostfOrt(?string $postfOrt): self
    {
        $this->postfOrt = $postfOrt;
        return $this;
    }

    public function getLand(): ?Land
    {
        return $this->land;
    }

    public function setLand(?Land $land): self
    {
        $this->land = $land;
        return $this;
    }

    public function getEmailPrivat(): ?string
    {
        return $this->emailPrivat;
    }

    public function setEmailPrivat(?string $emailPrivat): self
    {
        $this->emailPrivat = $emailPrivat;
        return $this;
    }

    /**
     * Returns array of array
     */
    public function getEmailSonstige(): ?array
    {
        return $this->emailSonstige ?? [];
    }

    public function addEmailSonstige(EmailSonstige $emailSonstige): self
    {
        $this->emailSonstige[] = $emailSonstige;
        return $this;
    }

    public function removeEmailSonstige(EmailSonstige $emailSonstige): self
    {
        return $this;
    }

    public function getEmailFeedback(): ?string
    {
        return $this->emailFeedback;
    }

    public function setEmailFeedback(?string $emailFeedback): self
    {
        $this->emailFeedback = $emailFeedback;
        return $this;
    }

    public function getTelPrivat(): ?string
    {
        return $this->telPrivat;
    }

    public function setTelPrivat(?string $telPrivat): self
    {
        $this->telPrivat = $telPrivat;
        return $this;
    }

    public function getTelSonstige(): ?array
    {
        return $this->telSonstige ?? [];
    }

    public function addTelSonstige(TelSonstige $telSonstige): self
    {
        $this->telSonstige[] = $telSonstige;
        return $this;
    }

    public function removeTelSonstige(TelSonstige $telSonstige): self
    {
        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url): self
    {
        $this->url = $url;
        return $this;
    }

    public function getAdressfreigabe(): ?bool
    {
        return $this->adressfreigabe;
    }

    public function setAdressfreigabe(?bool $adressfreigabe): self
    {
        $this->adressfreigabe = $adressfreigabe;
        return $this;
    }

    public function getPersonennummer(): ?string
    {
        return $this->personennummer;
    }

    public function setPersonennummer(?string $personennummer): self
    {
        $this->personennummer = $personennummer;
        return $this;
    }

    public function getImmobilientreuhaenderid(): ?string
    {
        return $this->immobilientreuhaenderid;
    }

    public function setImmobilientreuhaenderid(?string $immobilientreuhaenderid): self
    {
        $this->immobilientreuhaenderid = $immobilientreuhaenderid;
        return $this;
    }

    public function getFoto(): ?Foto
    {
        return $this->foto;
    }

    public function setFoto(?Foto $foto): self
    {
        $this->foto = $foto;
        return $this;
    }

    public function getReferenzId(): ?string
    {
        return $this->referenzId;
    }

    public function setReferenzId(?string $referenzId): self
    {
        $this->referenzId = $referenzId;
        return $this;
    }

    public function getFreitextfeld(): ?string
    {
        return $this->freitextfeld;
    }

    public function setFreitextfeld(?string $freitextfeld): self
    {
        $this->freitextfeld = $freitextfeld;
        return $this;
    }
}
