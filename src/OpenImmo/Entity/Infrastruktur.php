<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class Infrastruktur
{
    private ?bool $zulieferung = null;

    private ?Ausblick $ausblick = null;

    private Collection|array $distanzen;

    private Collection|array $distanzenSport;

    private ?array $userDefinedSimplefield = [];

    private ?array $userDefinedAnyfield = [];

    private ?array $userDefinedExtend = [];

    public function __construct()
    {
        $this->distanzen = new ArrayCollection();
        $this->distanzenSport = new ArrayCollection();
    }

    public function getZulieferung(): ?bool
    {
        return $this->zulieferung;
    }

    public function setZulieferung(?bool $zulieferung): self
    {
        $this->zulieferung = $zulieferung;
        return $this;
    }

    public function getAusblick(): ?Ausblick
    {
        return $this->ausblick;
    }

    public function setAusblick(?Ausblick $ausblick): self
    {
        $this->ausblick = $ausblick;
        return $this;
    }

    public function getDistanzen(): Collection|array
    {
        return $this->distanzen;
    }

    public function setDistanzen(Distanzen $distanzen): self
    {
        $this->distanzen->add($distanzen);
        return $this;
    }

    public function addDistanzen(Distanzen $distanzen): self
    {
        $this->distanzen->add($distanzen);
        return $this;
    }

    public function removeDistanzen(Distanzen $distanzen): self
    {
        $this->distanzen->removeElement($distanzen);
        return $this;
    }

    public function getDistanzenSport(): Collection|array
    {
        return $this->distanzenSport;
    }

    public function setDistanzenSport(DistanzenSport $distanzenSport): self
    {
        $this->distanzenSport->add($distanzenSport);
        return $this;
    }

    public function addDistanzenSport(DistanzenSport $distanzenSport): self
    {
        $this->distanzenSport->add($distanzenSport);
        return $this;
    }

    public function removeDistanzenSport(DistanzenSport $distanzenSport): self
    {
        $this->distanzenSport->removeElement($distanzenSport);
        return $this;
    }

    public function getUserDefinedSimplefield(): ?array
    {
        return $this->userDefinedSimplefield ?? [];
    }

    public function addUserDefinedSimplefield(UserDefinedSimplefield $userDefinedSimplefield): self
    {
        $this->userDefinedSimplefield[] = $userDefinedSimplefield;
        return $this;
    }

    public function removeUserDefinedSimplefield(UserDefinedSimplefield $userDefinedSimplefield): self
    {
        return $this;
    }

    public function getUserDefinedAnyfield(): ?array
    {
        return $this->userDefinedAnyfield ?? [];
    }

    public function addUserDefinedAnyfield(UserDefinedAnyfield $userDefinedAnyfield): self
    {
        $this->userDefinedAnyfield[] = $userDefinedAnyfield;
        return $this;
    }

    public function removeUserDefinedAnyfield(UserDefinedAnyfield $userDefinedAnyfield): self
    {
        return $this;
    }

    public function getUserDefinedExtend(): ?array
    {
        return $this->userDefinedExtend ?? [];
    }

    public function addUserDefinedExtend(UserDefinedExtend $userDefinedExtend): self
    {
        $this->userDefinedExtend[] = $userDefinedExtend;
        return $this;
    }

    public function removeUserDefinedExtend(UserDefinedExtend $userDefinedExtend): self
    {
        return $this;
    }
}
