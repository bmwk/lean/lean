<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class Ruecklagenetto
{
    #[SerializedName('@ruecklageust')]
    private ?float $ruecklageust = null;

    #[SerializedName('#')]
    private ?float $value = null;

    public function getRuecklageust(): ?float
    {
        return $this->ruecklageust;
    }

    public function setRuecklageust(?float $ruecklageust): self
    {
        $this->ruecklageust = $ruecklageust;
        return $this;
    }

    public function getValue(): ?float
    {
        return $this->value;
    }

    public function setValue(?float $value): self
    {
        $this->value = $value;
        return $this;
    }
}
