<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use App\Domain\Property\OpenImmoImporter\OpenImmo\SerializedName;

class Sonstigemietenetto
{
    #[SerializedName('@sonstigemieteust')]
    private ?float $sonstigemieteust = null;

    #[SerializedName('#')]
    private ?float $value = null;

    public function getSonstigemieteust(): ?float
    {
        return $this->sonstigemieteust;
    }

    public function setSonstigemieteust(?float $sonstigemieteust): self
    {
        $this->sonstigemieteust = $sonstigemieteust;
        return $this;
    }

    public function getValue(): ?float
    {
        return $this->value;
    }

    public function setValue(?float $value): self
    {
        $this->value = $value;
        return $this;
    }
}
