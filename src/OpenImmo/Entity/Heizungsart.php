<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class Heizungsart
{
    #[SerializedName('@OFEN')]
    private ?bool $ofen = null;

    #[SerializedName('@ETAGE')]
    private ?bool $etage = null;

    #[SerializedName('@ZENTRAL')]
    private ?bool $zentral = null;

    #[SerializedName('@FERN')]
    private ?bool $fern = null;

    #[SerializedName('@FUSSBODEN')]
    private ?bool $fussboden = null;

    public function getOfen(): ?bool
    {
        return $this->ofen;
    }

    public function setOfen(?bool $ofen): self
    {
        $this->ofen = $ofen;
        return $this;
    }

    public function getEtage(): ?bool
    {
        return $this->etage;
    }

    public function setEtage(?bool $etage): self
    {
        $this->etage = $etage;
        return $this;
    }

    public function getZentral(): ?bool
    {
        return $this->zentral;
    }

    public function setZentral(?bool $zentral): self
    {
        $this->zentral = $zentral;
        return $this;
    }

    public function getFern(): ?bool
    {
        return $this->fern;
    }

    public function setFern(?bool $fern): self
    {
        $this->fern = $fern;
        return $this;
    }

    public function getFussboden(): ?bool
    {
        return $this->fussboden;
    }

    public function setFussboden(?bool $fussboden): self
    {
        $this->fussboden = $fussboden;
        return $this;
    }
}
