<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class Moebliert
{
    #[SerializedName('@moeb')]
    private ?string $moeb = null;

    public function getMoeb(): ?string
    {
        return $this->moeb;
    }

    public function setMoeb(?string $moeb): self
    {
        $this->moeb = $moeb;
        return $this;
    }
}
