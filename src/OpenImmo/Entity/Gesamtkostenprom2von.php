<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class Gesamtkostenprom2von
{
    #[SerializedName('@gesamtkostenprom2bis')]
    private ?float $gesamtkostenprom2bis;

    #[SerializedName('#')]
    private ?float $value = null;

    public function getGesamtkostenprom2bis(): ?float
    {
        return $this->gesamtkostenprom2bis;
    }

    public function setGesamtkostenprom2bis(?float $gesamtkostenprom2bis): self
    {
        $this->gesamtkostenprom2bis = $gesamtkostenprom2bis;
        return $this;
    }

    public function getValue(): ?float
    {
        return $this->value;
    }

    public function setValue(?float $value): self
    {
        $this->value = $value;
        return $this;
    }
}
