<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class Summemietenetto
{
    #[SerializedName('@summemieteust')]
    private ?float $summemieteust = null;

    #[SerializedName('#')]
    private ?float $value = null;

    public function getSummemieteust(): ?float
    {
        return $this->summemieteust;
    }

    public function setSummemieteust(?float $summemieteust): self
    {
        $this->summemieteust = $summemieteust;
        return $this;
    }

    public function getValue(): ?float
    {
        return $this->value;
    }

    public function setValue(?float $value): self
    {
        $this->value = $value;
        return $this;
    }
}
