<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class Nutzungsart
{
    #[SerializedName('@WOHNEN')]
    private ?bool $wohnen = null;

    #[SerializedName('@GEWERBE')]
    private ?bool $gewerbe = null;

    #[SerializedName('@ANLAGE')]
    protected bool $anlage;

    #[SerializedName('@WAZ')]
    protected bool $waz;

    public function getWohnen(): ?bool
    {
        return $this->wohnen;
    }

    public function setWohnen(?bool $wohnen): self
    {
        $this->wohnen = $wohnen;
        return $this;
    }

    public function getGewerbe(): ?bool
    {
        return $this->gewerbe;
    }

    public function setGewerbe(?bool $gewerbe): self
    {
        $this->gewerbe = $gewerbe;
        return $this;
    }

    public function getAnlage(): ?bool
    {
        return $this->anlage;
    }

    public function setAnlage(?bool $anlage): self
    {
        $this->anlage = $anlage;
        return $this;
    }

    public function getWaz(): ?bool
    {
        return $this->waz;
    }

    public function setWaz(?bool $waz): self
    {
        $this->waz = $waz;
        return $this;
    }
}
