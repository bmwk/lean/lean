<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class Bad
{
    #[SerializedName('@DUSCHE')]
    private ?bool $dusche = null;

    #[SerializedName('@WANNE')]
    private ?bool $wanne = null;

    #[SerializedName('@FENSTER')]
    private ?bool $fenster = null;

    #[SerializedName('@BIDET')]
    private ?bool $bidet = null;

    #[SerializedName('@PISSOIR')]
    private ?bool $pissoir = null;

    public function getDusche(): ?bool
    {
        return $this->dusche;
    }

    public function setDusche(?bool $dusche): self
    {
        $this->dusche = $dusche;
        return $this;
    }

    public function getWanne(): ?bool
    {
        return $this->wanne;
    }

    public function setWanne(?bool $wanne): self
    {
        $this->wanne = $wanne;
        return $this;
    }

    public function getFenster(): ?bool
    {
        return $this->fenster;
    }

    public function setFenster(?bool $fenster): self
    {
        $this->fenster = $fenster;
        return $this;
    }

    public function getBidet(): ?bool
    {
        return $this->bidet;
    }

    public function setBidet(?bool $bidet): self
    {
        $this->bidet = $bidet;
        return $this;
    }

    public function getPissoir(): ?bool
    {
        return $this->pissoir;
    }

    public function setPissoir(?bool $pissoir): self
    {
        $this->pissoir = $pissoir;
        return $this;
    }
}
