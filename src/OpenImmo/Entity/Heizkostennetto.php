<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class Heizkostennetto
{
    #[SerializedName('@heizkostenust')]
    private ?float $heizkostenust = null;

    #[SerializedName('#')]
    private ?float $value = null;

    public function getHeizkostenust(): ?float
    {
        return $this->heizkostenust;
    }

    public function setHeizkostenust(?float $heizkostenust): self
    {
        $this->heizkostenust = $heizkostenust;
        return $this;
    }

    public function getValue(): ?float
    {
        return $this->value;
    }

    public function setValue(?float $value): self
    {
        $this->value = $value;
        return $this;
    }
}
