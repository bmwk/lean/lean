<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

class Ausblick
{
    private ?string $blick = null;

    public function getBlick(): ?string
    {
        return $this->blick;
    }

    public function setBlick(?string $blick): self
    {
        $this->blick = $blick;
        return $this;
    }
}
