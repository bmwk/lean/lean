<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class EmailSonstige
{
    #[SerializedName('@emailart')]
    private ?string $emailart = null;

    #[SerializedName('@bemerkung')]
    private ?string $bemerkung = null;

    #[SerializedName('#')]
    private ?string $value = null;

    public function getEmailart(): ?string
    {
        return $this->emailart;
    }

    public function setEmailart(?string $emailart): self
    {
        $this->emailart = $emailart;
        return $this;
    }

    public function getBemerkung(): ?string
    {
        return $this->bemerkung;
    }

    public function setBemerkung(?string $bemerkung): self
    {
        $this->bemerkung = $bemerkung;
        return $this;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(?string $value): self
    {
        $this->value = $value;
        return $this;
    }
}
