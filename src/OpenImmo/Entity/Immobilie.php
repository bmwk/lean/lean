<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

class Immobilie
{
    private ?Objektkategorie $objektkategorie = null;

    private ?Geo $geo = null;

    private ?Kontaktperson $kontaktperson = null;

    private ?array $weitereAdresse = [];

    private ?Preise $preise = null;

    private ?Bieterverfahren $bieterverfahren = null;

    private ?Versteigerung $versteigerung = null;

    private ?Flaechen $flaechen = null;

    private ?Ausstattung $ausstattung = null;

    private ?ZustandAngaben $zustandAngaben = null;

    private ?Bewertung $bewertung = null;

    private ?Infrastruktur $infrastruktur = null;

    private ?Freitexte $freitexte = null;

    private ?Anhaenge $anhaenge = null;

    private ?VerwaltungObjekt $verwaltungObjekt = null;

    private ?VerwaltungTechn $verwaltungTechn = null;

    public function getObjektkategorie(): ?Objektkategorie
    {
        return $this->objektkategorie;
    }

    public function setObjektkategorie(?Objektkategorie $objektkategorie): self
    {
        $this->objektkategorie = $objektkategorie;
        return $this;
    }

    public function getGeo(): ?Geo
    {
        return $this->geo;
    }

    public function setGeo(?Geo $geo): self
    {
        $this->geo = $geo;
        return $this;
    }

    public function getKontaktperson(): ?Kontaktperson
    {
        return $this->kontaktperson;
    }

    public function setKontaktperson(?Kontaktperson $kontaktperson): self
    {
        $this->kontaktperson = $kontaktperson;
        return $this;
    }

    public function getWeitereAdresse(): ?array
    {
        return $this->weitereAdresse ?? [];
    }

    public function addWeitereAdresse(WeitereAdresse $weitereAdresse): self
    {
        $this->weitereAdresse[] = $weitereAdresse;
        return $this;
    }

    public function removeWeitereAdresse(WeitereAdresse $weitereAdresse): self
    {
        return $this;
    }

    public function getPreise(): ?Preise
    {
        return $this->preise;
    }

    public function setPreise(?Preise $preise): self
    {
        $this->preise = $preise;
        return $this;
    }

    public function getBieterverfahren(): ?Bieterverfahren
    {
        return $this->bieterverfahren;
    }

    public function setBieterverfahren(?Bieterverfahren $bieterverfahren): self
    {
        $this->bieterverfahren = $bieterverfahren;
        return $this;
    }

    public function getVersteigerung(): ?Versteigerung
    {
        return $this->versteigerung;
    }

    public function setVersteigerung(?Versteigerung $versteigerung): self
    {
        $this->versteigerung = $versteigerung;
        return $this;
    }

    public function getFlaechen(): ?Flaechen
    {
        return $this->flaechen;
    }

    public function setFlaechen(?Flaechen $flaechen): self
    {
        $this->flaechen = $flaechen;
        return $this;
    }

    public function getAusstattung(): ?Ausstattung
    {
        return $this->ausstattung;
    }

    public function setAusstattung(?Ausstattung $ausstattung): self
    {
        $this->ausstattung = $ausstattung;
        return $this;
    }

    public function getZustandAngaben(): ?ZustandAngaben
    {
        return $this->zustandAngaben;
    }

    public function setZustandAngaben(?ZustandAngaben $zustandAngaben): self
    {
        $this->zustandAngaben = $zustandAngaben;
        return $this;
    }

    public function getBewertung(): ?Bewertung
    {
        return $this->bewertung;
    }

    public function setBewertung(?Bewertung $bewertung): self
    {
        $this->bewertung = $bewertung;
        return $this;
    }

    public function getInfrastruktur(): ?Infrastruktur
    {
        return $this->infrastruktur;
    }

    public function setInfrastruktur(?Infrastruktur $infrastruktur): self
    {
        $this->infrastruktur = $infrastruktur;
        return $this;
    }

    public function getFreitexte(): ?Freitexte
    {
        return $this->freitexte;
    }

    public function setFreitexte(?Freitexte $freitexte): self
    {
        $this->freitexte = $freitexte;
        return $this;
    }

    public function getAnhaenge(): ?Anhaenge
    {
        return $this->anhaenge;
    }

    public function setAnhaenge(?Anhaenge $anhaenge): self
    {
        $this->anhaenge = $anhaenge;
        return $this;
    }

    public function getVerwaltungObjekt(): ?VerwaltungObjekt
    {
        return $this->verwaltungObjekt;
    }

    public function setVerwaltungObjekt(?VerwaltungObjekt $verwaltungObjekt): self
    {
        $this->verwaltungObjekt = $verwaltungObjekt;
        return $this;
    }

    public function getVerwaltungTechn(): ?VerwaltungTechn
    {
        return $this->verwaltungTechn;
    }

    public function setVerwaltungTechn(?VerwaltungTechn $verwaltungTechn): self
    {
        $this->verwaltungTechn = $verwaltungTechn;
        return $this;
    }
}
