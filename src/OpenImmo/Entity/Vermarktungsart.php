<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class Vermarktungsart
{
    #[SerializedName('@KAUF')]
    private ?bool $kauf = null;

    #[SerializedName('@MIETE_PACHT')]
    private ?bool $mietePacht = null;

    #[SerializedName('@ERBPACHT')]
    private ?bool $erbpacht = null;

    #[SerializedName('@LEASING')]
    private ?bool $leasing = null;

    public function getKauf(): ?bool
    {
        return $this->kauf;
    }

    public function setKauf(?bool $kauf): self
    {
        $this->kauf = $kauf;
        return $this;
    }

    public function getMietePacht(): ?bool
    {
        return $this->mietePacht;
    }

    public function setMietePacht(?bool $mietePacht): self
    {
        $this->mietePacht = $mietePacht;
        return $this;
    }

    public function getErbpacht(): ?bool
    {
        return $this->erbpacht;
    }

    public function setErbpacht(?bool $erbpacht): self
    {
        $this->erbpacht = $erbpacht;
        return $this;
    }

    public function getLeasing(): ?bool
    {
        return $this->leasing;
    }

    public function setLeasing(?bool $leasing): self
    {
        $this->leasing = $leasing;
        return $this;
    }
}
