<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class Wohnung
{
    #[SerializedName('@wohnungtyp')]
    private ?string $wohnungtyp = null;

    public function getWohnungtyp(): ?string
    {
        return $this->wohnungtyp;
    }

    public function setWohnungtyp(?string $wohnungtyp): self
    {
        $this->wohnungtyp = $wohnungtyp;
        return $this;
    }
}
