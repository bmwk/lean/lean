<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class Befeuerung
{
    #[SerializedName('@OEL')]
    private ?bool $oel = null;

    #[SerializedName('@GAS')]
    private ?bool $gas = null;

    #[SerializedName('@ELEKTRO')]
    private ?bool $elektro = null;

    #[SerializedName('@ALTERNATIV')]
    private ?bool $alternativ = null;

    #[SerializedName('@SOLAR')]
    private ?bool $solar = null;

    #[SerializedName('@ERDWAERME')]
    private ?bool $erdwaerme = null;

    #[SerializedName('@LUFTWP')]
    private ?bool $luftwp = null;

    #[SerializedName('@FERN')]
    private ?bool $fern = null;

    #[SerializedName('@BLOCK')]
    private ?bool $block = null;

    #[SerializedName('@WASSER-ELEKTRO')]
    private ?bool $wasserElektro = null;

    #[SerializedName('@PELLET')]
    private ?bool $pellet = null;

    #[SerializedName('@KOHLE')]
    private ?bool $kohle = null;

    #[SerializedName('@HOLZ')]
    private ?bool $holz = null;

    #[SerializedName('@FLUESSIGGAS')]
    private ?bool $fluessiggas = null;

    public function getOel(): ?bool
    {
        return $this->oel;
    }

    public function setOel(?bool $oel): self
    {
        $this->oel = $oel;
        return $this;
    }

    public function getGas(): ?bool
    {
        return $this->gas;
    }

    public function setGas(?bool $gas): self
    {
        $this->gas = $gas;
        return $this;
    }

    public function getElektro(): ?bool
    {
        return $this->elektro;
    }

    public function setElektro(?bool $elektro): self
    {
        $this->elektro = $elektro;
        return $this;
    }

    public function getAlternativ(): ?bool
    {
        return $this->alternativ;
    }

    public function setAlternativ(?bool $alternativ): self
    {
        $this->alternativ = $alternativ;
        return $this;
    }

    public function getSolar(): ?bool
    {
        return $this->solar;
    }

    public function setSolar(?bool $solar): self
    {
        $this->solar = $solar;
        return $this;
    }

    public function getErdwaerme(): ?bool
    {
        return $this->erdwaerme;
    }

    public function setErdwaerme(?bool $erdwaerme): self
    {
        $this->erdwaerme = $erdwaerme;
        return $this;
    }

    public function getLuftwp(): ?bool
    {
        return $this->luftwp;
    }

    public function setLuftwp(?bool $luftwp): self
    {
        $this->luftwp = $luftwp;
        return $this;
    }

    public function getFern(): ?bool
    {
        return $this->fern;
    }

    public function setFern(?bool $fern): self
    {
        $this->fern = $fern;
        return $this;
    }

    public function getBlock(): ?bool
    {
        return $this->block;
    }

    public function setBlock(?bool $block): self
    {
        $this->block = $block;
        return $this;
    }

    public function getWasserElektro(): ?bool
    {
        return $this->wasserElektro;
    }

    public function setWasserElektro(?bool $wasserElektro): self
    {
        $this->wasserElektro = $wasserElektro;
        return $this;
    }

    public function getPellet(): ?bool
    {
        return $this->pellet;
    }

    public function setPellet(?bool $pellet): self
    {
        $this->pellet = $pellet;
        return $this;
    }

    public function getKohle(): ?bool
    {
        return $this->kohle;
    }

    public function setKohle(?bool $kohle): self
    {
        $this->kohle = $kohle;
        return $this;
    }

    public function getHolz(): ?bool
    {
        return $this->holz;
    }

    public function setHolz(?bool $holz): self
    {
        $this->holz = $holz;
        return $this;
    }

    public function getFluessiggas(): ?bool
    {
        return $this->fluessiggas;
    }

    public function setFluessiggas(?bool $fluessiggas): self
    {
        $this->fluessiggas = $fluessiggas;
        return $this;
    }
}
