<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class Uebertragung
{
    #[SerializedName('@art')]
    private ?string $art = null;

    #[SerializedName('@umfang')]
    private ?string $umfang = null;

    #[SerializedName('@modus')]
    private ?string $modus = null;

    #[SerializedName('@version')]
    private ?string $version = null;

    #[SerializedName('@sendersoftware')]
    private ?string $sendersoftware = null;

    #[SerializedName('@senderversion')]
    private ?string $senderversion = null;

    #[SerializedName('@techn_email')]
    private ?string $technEmail = null;

    #[SerializedName('@regi_id')]
    private ?string $regiId = null;

    #[SerializedName('@timestamp')]
    private ?\DateTime $timestamp = null;

    #[SerializedName('#')]
    private ?string $value = null;

    public function getArt(): ?string
    {
        return $this->art;
    }

    public function setArt(?string $art): self
    {
        $this->art = $art;
        return $this;
    }

    public function getUmfang(): ?string
    {
        return $this->umfang;
    }

    public function setUmfang(?string $umfang): self
    {
        $this->umfang = $umfang;
        return $this;
    }

    public function getModus(): ?string
    {
        return $this->modus;
    }

    public function setModus(?string $modus): self
    {
        $this->modus = $modus;
        return $this;
    }

    public function getVersion(): ?string
    {
        return $this->version;
    }

    public function setVersion(?string $version): self
    {
        $this->version = $version;
        return $this;
    }

    public function getSendersoftware(): ?string
    {
        return $this->sendersoftware;
    }

    public function setSendersoftware(?string $sendersoftware): self
    {
        $this->sendersoftware = $sendersoftware;
        return $this;
    }

    public function getSenderversion(): ?string
    {
        return $this->senderversion;
    }

    public function setSenderversion(?string $senderversion): self
    {
        $this->senderversion = $senderversion;
        return $this;
    }

    public function getTechnEmail(): ?string
    {
        return $this->technEmail;
    }

    public function setTechnEmail(?string $technEmail): self
    {
        $this->technEmail = $technEmail;
        return $this;
    }

    public function getRegiId(): ?string
    {
        return $this->regiId;
    }

    public function setRegiId(?string $regiId): self
    {
        $this->regiId = $regiId;
        return $this;
    }

    public function getTimestamp(): ?\DateTime
    {
        return $this->timestamp;
    }

    public function setTimestamp(?string $timestamp): self
    {
        $this->timestamp = new \DateTime($timestamp);
        return $this;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(?string $value): self
    {
        $this->value = $value;
        return $this;
    }
}
