<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class Anbieter
{
    private ?string $anbieternr = null;

    private ?string $firma = null;

    private ?string $openimmoAnid = null;

    private ?string $lizenzkennung = null;

    private ?Anhang $anhang = null;

    private Collection|array $immobilie;

    private ?string $impressum = null;

    private ?ImpressumStrukt $impressumStrukt = null;

    public function __construct()
    {
        $this->immobilie = new ArrayCollection();
    }


    public function getAnbieternr(): ?string
    {
        return $this->anbieternr;
    }

    public function setAnbieternr(?string $anbieternr): self
    {
        $this->anbieternr = $anbieternr;
        return $this;
    }

    public function getFirma(): ?string
    {
        return $this->firma;
    }

    public function setFirma(?string $firma): self
    {
        $this->firma = $firma;
        return $this;
    }

    public function getOpenimmoAnid(): ?string
    {
        return $this->openimmoAnid;
    }

    public function setOpenimmoAnid(?string $openimmoAnid): self
    {
        $this->openimmoAnid = $openimmoAnid;
        return $this;
    }

    public function getLizenzkennung(): ?string
    {
        return $this->lizenzkennung;
    }

    public function setLizenzkennung(?string $lizenzkennung): self
    {
        $this->lizenzkennung = $lizenzkennung;
        return $this;
    }

    public function getAnhang(): ?Anhang
    {
        return $this->anhang;
    }

    public function setAnhang(?Anhang $anhang): self
    {
        $this->anhang = $anhang;
        return $this;
    }

    /**
     * @return Collection|Immobilie[]
     */
    public function getImmobilie(): Collection|array
    {
        return $this->immobilie;
    }

    public function addImmobilie(Immobilie $immobilie): self
    {
        $this->immobilie->add($immobilie);
        return $this;
    }

    public function removeImmobilie(Immobilie $immobilie): self
    {
        $this->immobilie->removeElement($immobilie);
        return $this;
    }

    public function getImpressum(): ?string
    {
        return $this->impressum;
    }

    public function setImpressum(?string $impressum): self
    {
        $this->impressum = $impressum;
        return $this;
    }

    public function getImpressumStrukt(): ?ImpressumStrukt
    {
        return $this->impressumStrukt;
    }

    public function setImpressumStrukt(?ImpressumStrukt $impressumStrukt): self
    {
        $this->impressumStrukt = $impressumStrukt;
        return $this;
    }
}
