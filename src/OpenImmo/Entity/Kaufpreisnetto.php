<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class Kaufpreisnetto
{
    #[SerializedName('@kaufpreisust')]
    private ?float $kaufpreisust = null;

    #[SerializedName('#')]
    private ?float $value = null;

    public function getKaufpreisust(): ?float
    {
        return $this->kaufpreisust;
    }

    public function setKaufpreisust(?float $kaufpreisust): self
    {
        $this->kaufpreisust = $kaufpreisust;
        return $this;
    }

    public function getValue(): ?float
    {
        return $this->value;
    }

    public function setValue(?float $value): self
    {
        $this->value = $value;
        return $this;
    }
}
