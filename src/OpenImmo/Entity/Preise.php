<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

class Preise
{
    private ?Kaufpreis $kaufpreis = null;

    private ?Kaufpreisnetto $kaufpreisnetto = null;

    private ?float $kaufpreisbrutto = null;

    private ?float $nettokaltmiete = null;

    private ?float $kaltmiete = null;

    private ?float $warmmiete = null;

    private ?float $nebenkosten = null;

    private ?bool $heizkostenEnthalten = null;

    private ?float $heizkosten = null;

    private ?bool $zzgMehrwertsteuer = null;

    private ?float $mietzuschlaege = null;

    private ?Hauptmietzinsnetto $hauptmietzinsnetto = null;

    private ?float $pauschalmiete = null;

    private ?Betriebskostennetto $betriebskostennetto = null;

    private ?Evbnetto $evbnetto = null;

    private ?Gesamtmietenetto $gesamtmietenetto = null;

    private ?float $gesamtmietebrutto = null;

    private ?Gesamtbelastungnetto $gesamtbelastungnetto = null;

    private ?float $gesamtbelastungbrutto = null;

    private ?Gesamtkostenprom2von $gesamtkostenprom2von;

    private ?Heizkostennetto $heizkostennetto = null;

    private ?Monatlichekostennetto $monatlichekostennetto = null;

    private ?float $monatlichekostenbrutto = null;

    private ?Nebenkostenprom2von $nebenkostenprom2von;

    private ?Ruecklagenetto $ruecklagenetto = null;

    private ?Sonstigekostennetto $sonstigekostennetto = null;

    private ?Sonstigemietenetto $sonstigemietenetto = null;

    private ?Summemietenetto $summemietenetto = null;

    private ?Nettomieteprom2von $nettomieteprom2von;

    private ?float $pacht = null;

    private ?float $erbpacht = null;

    private ?float $hausgeld = null;

    private ?float $abstand = null;

    private ?\DateTime $preisZeitraumVon = null;

    private ?\DateTime $preisZeitraumBis = null;

    private ?PreisZeiteinheit $preisZeiteinheit = null;

    private ?float $mietpreisProQm = null;

    private ?float $kaufpreisProQm = null;

    private ?bool $provisionspflichtig = null;

    private ?ProvisionTeilen $provisionTeilen = null;

    private ?InnenCourtage $innenCourtage = null;

    private ?AussenCourtage $aussenCourtage = null;

    private ?string $courtageHinweis = null;

    private ?Provisionnetto $provisionnetto = null;

    private ?float $provisionbrutto = null;

    private ?Waehrung $waehrung = null;

    private ?float $mwstSatz = null;

    private ?float $mwstGesamt = null;

    private ?string $freitextPreis = null;

    private ?string $xFache = null;

    private ?float $nettorendite = null;

    private ?float $nettorenditeSoll = null;

    private ?float $nettorenditeIst = null;

    private ?MieteinnahmenIst $mieteinnahmenIst = null;

    private ?MieteinnahmenSoll $mieteinnahmenSoll = null;

    private ?float $erschliessungskosten = null;

    private ?float $kaution = null;

    private ?string $kautionText = null;

    private ?float $geschaeftsguthaben = null;

    private ?StpCarport $stpCarport = null;

    private ?StpDuplex $stpDuplex = null;

    private ?StpFreiplatz $stpFreiplatz = null;

    private ?StpGarage $stpGarage = null;

    private ?StpParkhaus $stpParkhaus = null;

    private ?StpTiefgarage $stpTiefgarage = null;

    protected array $stpSonstige = [];

    private ?float $richtpreis = null;

    private ?float $richtpreisprom2;

    public function getKaufpreis(): ?Kaufpreis
    {
        return $this->kaufpreis;
    }

    public function setKaufpreis(?Kaufpreis $kaufpreis): self
    {
        $this->kaufpreis = $kaufpreis;
        return $this;
    }

    public function getKaufpreisnetto(): ?Kaufpreisnetto
    {
        return $this->kaufpreisnetto;
    }

    public function setKaufpreisnetto(?Kaufpreisnetto $kaufpreisnetto): self
    {
        $this->kaufpreisnetto = $kaufpreisnetto;
        return $this;
    }

    public function getKaufpreisbrutto(): ?float
    {
        return $this->kaufpreisbrutto;
    }

    public function setKaufpreisbrutto(?float $kaufpreisbrutto): self
    {
        $this->kaufpreisbrutto = $kaufpreisbrutto;
        return $this;
    }

    public function getNettokaltmiete(): ?float
    {
        return $this->nettokaltmiete;
    }

    public function setNettokaltmiete(?float $nettokaltmiete): self
    {
        $this->nettokaltmiete = $nettokaltmiete;
        return $this;
    }

    public function getKaltmiete(): ?float
    {
        return $this->kaltmiete;
    }

    public function setKaltmiete(?float $kaltmiete): self
    {
        $this->kaltmiete = $kaltmiete;
        return $this;
    }

    public function getWarmmiete(): ?float
    {
        return $this->warmmiete;
    }

    public function setWarmmiete(?float $warmmiete): self
    {
        $this->warmmiete = $warmmiete;
        return $this;
    }

    public function getNebenkosten(): ?float
    {
        return $this->nebenkosten;
    }

    public function setNebenkosten(?float $nebenkosten): self
    {
        $this->nebenkosten = $nebenkosten;
        return $this;
    }

    public function getHeizkostenEnthalten(): ?bool
    {
        return $this->heizkostenEnthalten;
    }

    public function setHeizkostenEnthalten(?bool $heizkostenEnthalten): self
    {
        $this->heizkostenEnthalten = $heizkostenEnthalten;
        return $this;
    }

    public function getHeizkosten(): ?float
    {
        return $this->heizkosten;
    }

    public function setHeizkosten(?float $heizkosten): self
    {
        $this->heizkosten = $heizkosten;
        return $this;
    }

    public function getZzgMehrwertsteuer(): ?bool
    {
        return $this->zzgMehrwertsteuer;
    }

    public function setZzgMehrwertsteuer(?bool $zzgMehrwertsteuer): self
    {
        $this->zzgMehrwertsteuer = $zzgMehrwertsteuer;
        return $this;
    }

    public function getMietzuschlaege(): ?float
    {
        return $this->mietzuschlaege;
    }

    public function setMietzuschlaege(?float $mietzuschlaege): self
    {
        $this->mietzuschlaege = $mietzuschlaege;
        return $this;
    }

    public function getHauptmietzinsnetto(): ?Hauptmietzinsnetto
    {
        return $this->hauptmietzinsnetto;
    }

    public function setHauptmietzinsnetto(?Hauptmietzinsnetto $hauptmietzinsnetto): self
    {
        $this->hauptmietzinsnetto = $hauptmietzinsnetto;
        return $this;
    }

    public function getPauschalmiete(): ?float
    {
        return $this->pauschalmiete;
    }

    public function setPauschalmiete(?float $pauschalmiete): self
    {
        $this->pauschalmiete = $pauschalmiete;
        return $this;
    }

    public function getBetriebskostennetto(): ?Betriebskostennetto
    {
        return $this->betriebskostennetto;
    }

    public function setBetriebskostennetto(?Betriebskostennetto $betriebskostennetto): self
    {
        $this->betriebskostennetto = $betriebskostennetto;
        return $this;
    }

    public function getEvbnetto(): ?Evbnetto
    {
        return $this->evbnetto;
    }

    public function setEvbnetto(?Evbnetto $evbnetto): self
    {
        $this->evbnetto = $evbnetto;
        return $this;
    }

    public function getGesamtmietenetto(): ?Gesamtmietenetto
    {
        return $this->gesamtmietenetto;
    }

    public function setGesamtmietenetto(?Gesamtmietenetto $gesamtmietenetto): self
    {
        $this->gesamtmietenetto = $gesamtmietenetto;
        return $this;
    }

    public function getGesamtmietebrutto(): ?float
    {
        return $this->gesamtmietebrutto;
    }

    public function setGesamtmietebrutto(?float $gesamtmietebrutto): self
    {
        $this->gesamtmietebrutto = $gesamtmietebrutto;
        return $this;
    }

    public function getGesamtbelastungnetto(): ?Gesamtbelastungnetto
    {
        return $this->gesamtbelastungnetto;
    }

    public function setGesamtbelastungnetto(?Gesamtbelastungnetto $gesamtbelastungnetto): self
    {
        $this->gesamtbelastungnetto = $gesamtbelastungnetto;
        return $this;
    }

    public function getGesamtbelastungbrutto(): ?float
    {
        return $this->gesamtbelastungbrutto;
    }

    public function setGesamtbelastungbrutto(?float $gesamtbelastungbrutto): self
    {
        $this->gesamtbelastungbrutto = $gesamtbelastungbrutto;
        return $this;
    }

    public function getGesamtkostenprom2von(): ?Gesamtkostenprom2von
    {
        return $this->gesamtkostenprom2von;
    }

    public function setGesamtkostenprom2von(?Gesamtkostenprom2von $gesamtkostenprom2von): self
    {
        $this->gesamtkostenprom2von = $gesamtkostenprom2von;
        return $this;
    }

    public function getHeizkostennetto(): ?Heizkostennetto
    {
        return $this->heizkostennetto;
    }

    public function setHeizkostennetto(?Heizkostennetto $heizkostennetto): self
    {
        $this->heizkostennetto = $heizkostennetto;
        return $this;
    }

    public function getMonatlichekostennetto(): ?Monatlichekostennetto
    {
        return $this->monatlichekostennetto;
    }

    public function setMonatlichekostennetto(?Monatlichekostennetto $monatlichekostennetto): self
    {
        $this->monatlichekostennetto = $monatlichekostennetto;
        return $this;
    }

    public function getMonatlichekostenbrutto(): ?float
    {
        return $this->monatlichekostenbrutto;
    }

    public function setMonatlichekostenbrutto(?float $monatlichekostenbrutto): self
    {
        $this->monatlichekostenbrutto = $monatlichekostenbrutto;
        return $this;
    }

    public function getNebenkostenprom2von(): ?Nebenkostenprom2von
    {
        return $this->nebenkostenprom2von;
    }

    public function setNebenkostenprom2von(?Nebenkostenprom2von $nebenkostenprom2von): self
    {
        $this->nebenkostenprom2von = $nebenkostenprom2von;
        return $this;
    }

    public function getRuecklagenetto(): ?Ruecklagenetto
    {
        return $this->ruecklagenetto;
    }

    public function setRuecklagenetto(?Ruecklagenetto $ruecklagenetto): self
    {
        $this->ruecklagenetto = $ruecklagenetto;
        return $this;
    }

    public function getSonstigekostennetto(): ?Sonstigekostennetto
    {
        return $this->sonstigekostennetto;
    }

    public function setSonstigekostennetto(?Sonstigekostennetto $sonstigekostennetto): self
    {
        $this->sonstigekostennetto = $sonstigekostennetto;
        return $this;
    }

    public function getSonstigemietenetto(): ?Sonstigemietenetto
    {
        return $this->sonstigemietenetto;
    }

    public function setSonstigemietenetto(?Sonstigemietenetto $sonstigemietenetto): self
    {
        $this->sonstigemietenetto = $sonstigemietenetto;
        return $this;
    }

    public function getSummemietenetto(): ?Summemietenetto
    {
        return $this->summemietenetto;
    }

    public function setSummemietenetto(?Summemietenetto $summemietenetto): self
    {
        $this->summemietenetto = $summemietenetto;
        return $this;
    }

    public function getNettomieteprom2von(): ?Nettomieteprom2von
    {
        return $this->nettomieteprom2von;
    }

    public function setNettomieteprom2von(?Nettomieteprom2von $nettomieteprom2von): self
    {
        $this->nettomieteprom2von = $nettomieteprom2von;
        return $this;
    }

    public function getPacht(): ?float
    {
        return $this->pacht;
    }

    public function setPacht(?float $pacht): self
    {
        $this->pacht = $pacht;
        return $this;
    }

    public function getErbpacht(): ?float
    {
        return $this->erbpacht;
    }

    public function setErbpacht(?float $erbpacht): self
    {
        $this->erbpacht = $erbpacht;
        return $this;
    }

    public function getHausgeld(): ?float
    {
        return $this->hausgeld;
    }

    public function setHausgeld(?float $hausgeld): self
    {
        $this->hausgeld = $hausgeld;
        return $this;
    }

    public function getAbstand(): ?float
    {
        return $this->abstand;
    }

    public function setAbstand(?float $abstand): self
    {
        $this->abstand = $abstand;
        return $this;
    }

    public function getPreisZeitraumVon(): ?\DateTime
    {
        return $this->preisZeitraumVon;
    }

    public function setPreisZeitraumVon(?\DateTime $preisZeitraumVon): self
    {
        $this->preisZeitraumVon = $preisZeitraumVon;
        return $this;
    }

    public function getPreisZeitraumBis(): ?\DateTime
    {
        return $this->preisZeitraumBis;
    }

    public function setPreisZeitraumBis(?\DateTime $preisZeitraumBis): self
    {
        $this->preisZeitraumBis = $preisZeitraumBis;
        return $this;
    }

    public function getPreisZeiteinheit(): ?PreisZeiteinheit
    {
        return $this->preisZeiteinheit;
    }

    public function setPreisZeiteinheit(?PreisZeiteinheit $preisZeiteinheit): self
    {
        $this->preisZeiteinheit = $preisZeiteinheit;
        return $this;
    }

    public function getMietpreisProQm(): ?float
    {
        return $this->mietpreisProQm;
    }

    public function setMietpreisProQm(?float $mietpreisProQm): self
    {
        $this->mietpreisProQm = $mietpreisProQm;
        return $this;
    }

    public function getKaufpreisProQm(): ?float
    {
        return $this->kaufpreisProQm;
    }

    public function setKaufpreisProQm(?float $kaufpreisProQm): self
    {
        $this->kaufpreisProQm = $kaufpreisProQm;
        return $this;
    }

    public function getProvisionspflichtig(): ?bool
    {
        return $this->provisionspflichtig;
    }

    public function setProvisionspflichtig(?bool $provisionspflichtig): self
    {
        $this->provisionspflichtig = $provisionspflichtig;
        return $this;
    }

    public function getProvisionTeilen(): ?ProvisionTeilen
    {
        return $this->provisionTeilen;
    }

    public function setProvisionTeilen(?ProvisionTeilen $provisionTeilen): self
    {
        $this->provisionTeilen = $provisionTeilen;
        return $this;
    }

    public function getInnenCourtage(): ?InnenCourtage
    {
        return $this->innenCourtage;
    }

    public function setInnenCourtage(?InnenCourtage $innenCourtage): self
    {
        $this->innenCourtage = $innenCourtage;
        return $this;
    }

    public function getAussenCourtage(): ?AussenCourtage
    {
        return $this->aussenCourtage;
    }

    public function setAussenCourtage(?AussenCourtage $aussenCourtage): self
    {
        $this->aussenCourtage = $aussenCourtage;
        return $this;
    }

    public function getCourtageHinweis(): ?string
    {
        return $this->courtageHinweis;
    }

    public function setCourtageHinweis(?string $courtageHinweis): self
    {
        $this->courtageHinweis = $courtageHinweis;
        return $this;
    }

    public function getProvisionnetto(): ?Provisionnetto
    {
        return $this->provisionnetto;
    }

    public function setProvisionnetto(?Provisionnetto $provisionnetto): self
    {
        $this->provisionnetto = $provisionnetto;
        return $this;
    }

    public function getProvisionbrutto(): ?float
    {
        return $this->provisionbrutto;
    }

    public function setProvisionbrutto(?float $provisionbrutto): self
    {
        $this->provisionbrutto = $provisionbrutto;
        return $this;
    }

    public function getWaehrung(): ?Waehrung
    {
        return $this->waehrung;
    }

    public function setWaehrung(?Waehrung $waehrung): self
    {
        $this->waehrung = $waehrung;
        return $this;
    }

    public function getMwstSatz(): ?float
    {
        return $this->mwstSatz;
    }

    public function setMwstSatz(?float $mwstSatz): self
    {
        $this->mwstSatz = $mwstSatz;
        return $this;
    }

    public function getMwstGesamt(): ?float
    {
        return $this->mwstGesamt;
    }

    public function setMwstGesamt(?float $mwstGesamt): self
    {
        $this->mwstGesamt = $mwstGesamt;
        return $this;
    }

    public function getFreitextPreis(): ?string
    {
        return $this->freitextPreis;
    }

    public function setFreitextPreis(?string $freitextPreis): self
    {
        $this->freitextPreis = $freitextPreis;
        return $this;
    }

    public function getXFache(): ?string
    {
        return $this->xFache;
    }

    public function setXFache(?string $xFache): self
    {
        $this->xFache = $xFache;
        return $this;
    }

    public function getNettorendite(): ?float
    {
        return $this->nettorendite;
    }

    public function setNettorendite(?float $nettorendite): self
    {
        $this->nettorendite = $nettorendite;
        return $this;
    }

    public function getNettorenditeSoll(): ?float
    {
        return $this->nettorenditeSoll;
    }

    public function setNettorenditeSoll(?float $nettorenditeSoll): self
    {
        $this->nettorenditeSoll = $nettorenditeSoll;
        return $this;
    }

    public function getNettorenditeIst(): ?float
    {
        return $this->nettorenditeIst;
    }

    public function setNettorenditeIst(?float $nettorenditeIst): self
    {
        $this->nettorenditeIst = $nettorenditeIst;
        return $this;
    }

    public function getMieteinnahmenIst(): ?MieteinnahmenIst
    {
        return $this->mieteinnahmenIst;
    }

    public function setMieteinnahmenIst(?MieteinnahmenIst $mieteinnahmenIst): self
    {
        $this->mieteinnahmenIst = $mieteinnahmenIst;
        return $this;
    }

    public function getMieteinnahmenSoll(): ?MieteinnahmenSoll
    {
        return $this->mieteinnahmenSoll;
    }

    public function setMieteinnahmenSoll(?MieteinnahmenSoll $mieteinnahmenSoll): self
    {
        $this->mieteinnahmenSoll = $mieteinnahmenSoll;
        return $this;
    }

    public function getErschliessungskosten(): ?float
    {
        return $this->erschliessungskosten;
    }

    public function setErschliessungskosten(?float $erschliessungskosten): self
    {
        $this->erschliessungskosten = $erschliessungskosten;
        return $this;
    }

    public function getKaution(): ?float
    {
        return $this->kaution;
    }

    public function setKaution(?float $kaution): self
    {
        $this->kaution = $kaution;
        return $this;
    }

    public function getKautionText(): ?string
    {
        return $this->kautionText;
    }

    public function setKautionText(?string $kautionText): self
    {
        $this->kautionText = $kautionText;
        return $this;
    }

    public function getGeschaeftsguthaben(): ?float
    {
        return $this->geschaeftsguthaben;
    }

    public function setGeschaeftsguthaben(?float $geschaeftsguthaben): self
    {
        $this->geschaeftsguthaben = $geschaeftsguthaben;
        return $this;
    }

    public function getStpCarport(): ?StpCarport
    {
        return $this->stpCarport;
    }

    public function setStpCarport(?StpCarport $stpCarport): self
    {
        $this->stpCarport = $stpCarport;
        return $this;
    }

    public function getStpDuplex(): ?StpDuplex
    {
        return $this->stpDuplex;
    }

    public function setStpDuplex(?StpDuplex $stpDuplex): self
    {
        $this->stpDuplex = $stpDuplex;
        return $this;
    }

    public function getStpFreiplatz(): ?StpFreiplatz
    {
        return $this->stpFreiplatz;
    }

    public function setStpFreiplatz(?StpFreiplatz $stpFreiplatz): self
    {
        $this->stpFreiplatz = $stpFreiplatz;
        return $this;
    }

    public function getStpGarage(): ?StpGarage
    {
        return $this->stpGarage;
    }

    public function setStpGarage(?StpGarage $stpGarage): self
    {
        $this->stpGarage = $stpGarage;
        return $this;
    }

    public function getStpParkhaus(): ?StpParkhaus
    {
        return $this->stpParkhaus;
    }

    public function setStpParkhaus(?StpParkhaus $stpParkhaus): self
    {
        $this->stpParkhaus = $stpParkhaus;
        return $this;
    }

    public function getStpTiefgarage(): ?StpTiefgarage
    {
        return $this->stpTiefgarage;
    }

    public function setStpTiefgarage(?StpTiefgarage $stpTiefgarage): self
    {
        $this->stpTiefgarage = $stpTiefgarage;
        return $this;
    }

    /**
     * Returns array of array
     */
    public function getStpSonstige(): ?array
    {
        return $this->stpSonstige ?? [];
    }

    public function setStpSonstige(?array $stpSonstige): self
    {
        $this->stpSonstige = $stpSonstige;
        return $this;
    }

    public function getRichtpreis(): ?float
    {
        return $this->richtpreis;
    }

    public function setRichtpreis(?float $richtpreis): self
    {
        $this->richtpreis = $richtpreis;
        return $this;
    }

    public function getRichtpreisprom2(): ?float
    {
        return $this->richtpreisprom2;
    }

    public function setRichtpreisprom2(?float $richtpreisprom2): self
    {
        $this->richtpreisprom2 = $richtpreisprom2;
        return $this;
    }
}
