<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class Ausstattung
{
    private ?string $ausstattKategorie = null;

    private ?bool $wgGeeignet = null;

    private ?bool $raeumeVeraenderbar = null;

    private ?Bad $bad = null;

    private ?Kueche $kueche = null;

    private ?Boden $boden = null;

    private ?bool $kamin = null;

    private ?Heizungsart $heizungsart = null;

    private ?Befeuerung $befeuerung = null;

    private ?bool $klimatisiert = null;

    private ?Fahrstuhl $fahrstuhl = null;

    private Collection|array $stellplatzart;

    private ?bool $gartennutzung = null;

    private ?AusrichtBalkonTerrasse $ausrichtBalkonTerrasse = null;

    private ?Moebliert $moebliert = null;

    private ?bool $rollstuhlgerecht = null;

    private ?bool $kabelSatTv = null;

    private ?bool $dvbt = null;

    private ?bool $barrierefrei = null;

    private ?bool $sauna = null;

    private ?bool $swimmingpool = null;

    private ?bool $waschTrockenraum = null;

    private ?bool $wintergarten = null;

    private ?bool $dvVerkabelung = null;

    private ?bool $rampe = null;

    private ?bool $hebebuehne = null;

    private ?bool $kran = null;

    private ?bool $gastterrasse = null;

    private ?string $stromanschlusswert = null;

    private ?bool $kantineCafeteria = null;

    private ?bool $teekueche = null;

    private ?float $hallenhoehe = null;

    private ?AngeschlGastronomie $angeschlGastronomie = null;

    private ?bool $brauereibindung = null;

    private ?bool $sporteinrichtungen = null;

    private ?bool $wellnessbereich = null;

    private Collection|array $serviceleistungen;

    private ?bool $telefonFerienimmobilie = null;

    private ?BreitbandZugang $breitbandZugang = null;

    private ?bool $umtsEmpfang = null;

    private ?Sicherheitstechnik $sicherheitstechnik = null;

    private ?Unterkellert $unterkellert = null;

    private ?bool $abstellraum = null;

    private ?bool $fahrradraum = null;

    private ?bool $rolladen = null;

    private ?Dachform $dachform = null;

    private ?Bauweise $bauweise = null;

    private ?Ausbaustufe $ausbaustufe = null;

    private ?Energietyp $energietyp = null;

    private ?bool $bibliothek = null;

    private ?bool $dachboden = null;

    private ?bool $gaestewc = null;

    private ?bool $kabelkanaele = null;

    private ?bool $seniorengerecht = null;

    public function __construct()
    {
        $this->stellplatzart = new ArrayCollection();
        $this->serviceleistungen = new ArrayCollection();
    }


    public function getAusstattKategorie(): ?string
    {
        return $this->ausstattKategorie;
    }

    public function setAusstattKategorie(?string $ausstattKategorie): self
    {
        $this->ausstattKategorie = $ausstattKategorie;
        return $this;
    }

    public function getWgGeeignet(): ?bool
    {
        return $this->wgGeeignet;
    }

    public function setWgGeeignet(?bool $wgGeeignet): self
    {
        $this->wgGeeignet = $wgGeeignet;
        return $this;
    }

    public function getRaeumeVeraenderbar(): ?bool
    {
        return $this->raeumeVeraenderbar;
    }

    public function setRaeumeVeraenderbar(?bool $raeumeVeraenderbar): self
    {
        $this->raeumeVeraenderbar = $raeumeVeraenderbar;
        return $this;
    }

    public function getBad(): ?Bad
    {
        return $this->bad;
    }

    public function setBad(?Bad $bad): self
    {
        $this->bad = $bad;
        return $this;
    }

    public function getKueche(): ?Kueche
    {
        return $this->kueche;
    }

    public function setKueche(?Kueche $kueche): self
    {
        $this->kueche = $kueche;
        return $this;
    }

    public function getBoden(): ?Boden
    {
        return $this->boden;
    }

    public function setBoden(?Boden $boden): self
    {
        $this->boden = $boden;
        return $this;
    }

    public function getKamin(): ?bool
    {
        return $this->kamin;
    }

    public function setKamin(?bool $kamin): self
    {
        $this->kamin = $kamin;
        return $this;
    }

    public function getHeizungsart(): ?Heizungsart
    {
        return $this->heizungsart;
    }

    public function setHeizungsart(?Heizungsart $heizungsart): self
    {
        $this->heizungsart = $heizungsart;
        return $this;
    }

    public function getBefeuerung(): ?Befeuerung
    {
        return $this->befeuerung;
    }

    public function setBefeuerung(?Befeuerung $befeuerung): self
    {
        $this->befeuerung = $befeuerung;
        return $this;
    }

    public function getKlimatisiert(): ?bool
    {
        return $this->klimatisiert;
    }

    public function setKlimatisiert(?bool $klimatisiert): self
    {
        $this->klimatisiert = $klimatisiert;
        return $this;
    }

    public function getFahrstuhl(): ?Fahrstuhl
    {
        return $this->fahrstuhl;
    }

    public function setFahrstuhl(?Fahrstuhl $fahrstuhl): self
    {
        $this->fahrstuhl = $fahrstuhl;
        return $this;
    }

    public function getStellplatzart(): Collection|array
    {
        return $this->stellplatzart;
    }

    public function setStellplatzart(Stellplatzart $stellplatzart): self
    {
        $this->stellplatzart->add($stellplatzart);
        return $this;
    }

    public function addStellplatzart(Stellplatzart $stellplatzart): self
    {
        $this->stellplatzart->add($stellplatzart);
        return $this;
    }

    public function removeStellplatzart(Stellplatzart $stellplatzart): self
    {
        $this->stellplatzart->removeElement($stellplatzart);
        return $this;
    }

    public function getGartennutzung(): ?bool
    {
        return $this->gartennutzung;
    }

    public function setGartennutzung(?bool $gartennutzung): self
    {
        $this->gartennutzung = $gartennutzung;
        return $this;
    }

    public function getAusrichtBalkonTerrasse(): ?AusrichtBalkonTerrasse
    {
        return $this->ausrichtBalkonTerrasse;
    }

    public function setAusrichtBalkonTerrasse(?AusrichtBalkonTerrasse $ausrichtBalkonTerrasse): self
    {
        $this->ausrichtBalkonTerrasse = $ausrichtBalkonTerrasse;
        return $this;
    }

    public function getMoebliert(): ?Moebliert
    {
        return $this->moebliert;
    }

    public function setMoebliert(?Moebliert $moebliert): self
    {
        $this->moebliert = $moebliert;
        return $this;
    }

    public function getRollstuhlgerecht(): ?bool
    {
        return $this->rollstuhlgerecht;
    }

    public function setRollstuhlgerecht(?bool $rollstuhlgerecht): self
    {
        $this->rollstuhlgerecht = $rollstuhlgerecht;
        return $this;
    }

    public function getKabelSatTv(): ?bool
    {
        return $this->kabelSatTv;
    }

    public function setKabelSatTv(?bool $kabelSatTv): self
    {
        $this->kabelSatTv = $kabelSatTv;
        return $this;
    }

    public function getDvbt(): ?bool
    {
        return $this->dvbt;
    }

    public function setDvbt(?bool $dvbt): self
    {
        $this->dvbt = $dvbt;
        return $this;
    }

    public function getBarrierefrei(): ?bool
    {
        return $this->barrierefrei;
    }

    public function setBarrierefrei(?bool $barrierefrei): self
    {
        $this->barrierefrei = $barrierefrei;
        return $this;
    }

    public function getSauna(): ?bool
    {
        return $this->sauna;
    }

    public function setSauna(?bool $sauna): self
    {
        $this->sauna = $sauna;
        return $this;
    }

    public function getSwimmingpool(): ?bool
    {
        return $this->swimmingpool;
    }

    public function setSwimmingpool(?bool $swimmingpool): self
    {
        $this->swimmingpool = $swimmingpool;
        return $this;
    }

    public function getWaschTrockenraum(): ?bool
    {
        return $this->waschTrockenraum;
    }

    public function setWaschTrockenraum(?bool $waschTrockenraum): self
    {
        $this->waschTrockenraum = $waschTrockenraum;
        return $this;
    }

    public function getWintergarten(): ?bool
    {
        return $this->wintergarten;
    }

    public function setWintergarten(?bool $wintergarten): self
    {
        $this->wintergarten = $wintergarten;
        return $this;
    }

    public function getDvVerkabelung(): ?bool
    {
        return $this->dvVerkabelung;
    }

    public function setDvVerkabelung(?bool $dvVerkabelung): self
    {
        $this->dvVerkabelung = $dvVerkabelung;
        return $this;
    }

    public function getRampe(): ?bool
    {
        return $this->rampe;
    }

    public function setRampe(?bool $rampe): self
    {
        $this->rampe = $rampe;
        return $this;
    }

    public function getHebebuehne(): ?bool
    {
        return $this->hebebuehne;
    }

    public function setHebebuehne(?bool $hebebuehne): self
    {
        $this->hebebuehne = $hebebuehne;
        return $this;
    }

    public function getKran(): ?bool
    {
        return $this->kran;
    }

    public function setKran(?bool $kran): self
    {
        $this->kran = $kran;
        return $this;
    }

    public function getGastterrasse(): ?bool
    {
        return $this->gastterrasse;
    }

    public function setGastterrasse(?bool $gastterrasse): self
    {
        $this->gastterrasse = $gastterrasse;

        return $this;
    }

    public function getStromanschlusswert():string
    {
        return $this->stromanschlusswert;
    }

    public function setStromanschlusswert(?string $stromanschlusswert): self
    {
        $this->stromanschlusswert = $stromanschlusswert;
        return $this;
    }

    public function getKantineCafeteria(): ?bool
    {
        return $this->kantineCafeteria;
    }

    public function setKantineCafeteria(?bool $kantineCafeteria): self
    {
        $this->kantineCafeteria = $kantineCafeteria;
        return $this;
    }

    public function getTeekueche(): ?bool
    {
        return $this->teekueche;
    }

    public function setTeekueche(?bool $teekueche): self
    {
        $this->teekueche = $teekueche;
        return $this;
    }

    public function getHallenhoehe(): ?float
    {
        return $this->hallenhoehe;
    }

    public function setHallenhoehe(?float $hallenhoehe): self
    {
        $this->hallenhoehe = $hallenhoehe;
        return $this;
    }

    public function getAngeschlGastronomie(): ?AngeschlGastronomie
    {
        return $this->angeschlGastronomie;
    }

    public function setAngeschlGastronomie(?AngeschlGastronomie $angeschlGastronomie): self
    {
        $this->angeschlGastronomie = $angeschlGastronomie;
        return $this;
    }

    public function getBrauereibindung(): ?bool
    {
        return $this->brauereibindung;
    }

    public function setBrauereibindung(?bool $brauereibindung): self
    {
        $this->brauereibindung = $brauereibindung;
        return $this;
    }

    public function getSporteinrichtungen(): ?bool
    {
        return $this->sporteinrichtungen;
    }

    public function setSporteinrichtungen(?bool $sporteinrichtungen): self
    {
        $this->sporteinrichtungen = $sporteinrichtungen;
        return $this;
    }

    public function getWellnessbereich(): ?bool
    {
        return $this->wellnessbereich;
    }

    public function setWellnessbereich(?bool $wellnessbereich): self
    {
        $this->wellnessbereich = $wellnessbereich;
        return $this;
    }

    public function getServiceleistungen(): Collection|array
    {
        return $this->serviceleistungen;
    }

    public function setServiceleistungen(Serviceleistungen $serviceleistungen): self
    {
        $this->serviceleistungen->add($serviceleistungen);
        return $this;
    }

    public function addServiceleistungen(Serviceleistungen $serviceleistungen): self
    {
        $this->serviceleistungen->add($serviceleistungen);
        return $this;
    }

    public function removeServiceleistungen(Serviceleistungen $serviceleistungen): self
    {
        $this->serviceleistungen->remove($serviceleistungen);
        return $this;
    }

    public function getTelefonFerienimmobilie(): ?bool
    {
        return $this->telefonFerienimmobilie;
    }

    public function setTelefonFerienimmobilie(?bool $telefonFerienimmobilie): self
    {
        $this->telefonFerienimmobilie = $telefonFerienimmobilie;
        return $this;
    }

    public function getBreitbandZugang(): ?breitbandZugang
    {
        return $this->breitbandZugang;
    }

    public function setBreitbandZugang(?BreitbandZugang $breitbandZugang): self
    {
        $this->breitbandZugang = $breitbandZugang;
        return $this;
    }

    public function getUmtsEmpfang(): ?bool
    {
        return $this->umtsEmpfang;
    }

    public function setUmtsEmpfang(?bool $umtsEmpfang): self
    {
        $this->umtsEmpfang = $umtsEmpfang;
        return $this;
    }

    public function getSicherheitstechnik(): ?Sicherheitstechnik
    {
        return $this->sicherheitstechnik;
    }

    public function setSicherheitstechnik(?Sicherheitstechnik $sicherheitstechnik): self
    {
        $this->sicherheitstechnik = $sicherheitstechnik;
        return $this;
    }

    public function getUnterkellert(): ?Unterkellert
    {
        return $this->unterkellert;
    }

    public function setUnterkellert(?Unterkellert $unterkellert): self
    {
        $this->unterkellert = $unterkellert;
        return $this;
    }

    public function getAbstellraum(): ?bool
    {
        return $this->abstellraum;
    }

    public function setAbstellraum(?bool $abstellraum): self
    {
        $this->abstellraum = $abstellraum;
        return $this;
    }

    public function getFahrradraum(): ?bool
    {
        return $this->fahrradraum;
    }

    public function setFahrradraum(?bool $fahrradraum): self
    {
        $this->fahrradraum = $fahrradraum;
        return $this;
    }

    public function getRolladen(): ?bool
    {
        return $this->rolladen;
    }

    public function setRolladen(?bool $rolladen): self
    {
        $this->rolladen = $rolladen;
        return $this;
    }

    public function getDachform(): ?Dachform
    {
        return $this->dachform;
    }

    public function setDachform(?Dachform $dachform): self
    {
        $this->dachform = $dachform;
        return $this;
    }

    public function getBauweise(): ?Bauweise
    {
        return $this->bauweise;
    }

    public function setBauweise(?Bauweise $bauweise): self
    {
        $this->bauweise = $bauweise;
        return $this;
    }

    public function getAusbaustufe(): ?Ausbaustufe
    {
        return $this->ausbaustufe;
    }

    public function setAusbaustufe(?Ausbaustufe $ausbaustufe): self
    {
        $this->ausbaustufe = $ausbaustufe;
        return $this;
    }

    public function getEnergietyp(): ?Energietyp
    {
        return $this->energietyp;
    }

    public function setEnergietyp(?Energietyp $energietyp): self
    {
        $this->energietyp = $energietyp;
        return $this;
    }

    public function getBibliothek(): ?bool
    {
        return $this->bibliothek;
    }

    public function setBibliothek(?bool $bibliothek): self
    {
        $this->bibliothek = $bibliothek;
        return $this;
    }

    public function getDachboden(): ?bool
    {
        return $this->dachboden;
    }

    public function setDachboden(?bool $dachboden): self
    {
        $this->dachboden = $dachboden;
        return $this;
    }

    public function getGaestewc(): ?bool
    {
        return $this->gaestewc;
    }

    public function setGaestewc(?bool $gaestewc): self
    {
        $this->gaestewc = $gaestewc;
        return $this;
    }

    public function getKabelkanaele(): ?bool
    {
        return $this->kabelkanaele;
    }

    public function setKabelkanaele(?bool $kabelkanaele): self
    {
        $this->kabelkanaele = $kabelkanaele;
        return $this;
    }

    public function getSeniorengerecht(): ?bool
    {
        return $this->seniorengerecht;
    }

    public function setSeniorengerecht(?bool $seniorengerecht): self
    {
        $this->seniorengerecht = $seniorengerecht;
        return $this;
    }
}
