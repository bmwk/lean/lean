<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class MaxMietdauer
{
    #[SerializedName('@max_dauer')]
    private ?string $maxDauer = null;

    #[SerializedName('#')]
    private ?string $value = null;

    public function getMaxDauer(): ?string
    {
        return $this->maxDauer;
    }

    public function setMaxDauer(?string $maxDauer): self
    {
        $this->maxDauer = $maxDauer;
        return $this;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(?string $value): self
    {
        $this->value = $value;
        return $this;
    }
}
