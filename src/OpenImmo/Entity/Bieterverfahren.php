<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

class Bieterverfahren
{
    private ?\DateTime $beginnAngebotsphase = null;

    private ?\DateTime $besichtigungstermin = null;

    private ?\DateTime $besichtigungstermin2;

    private ?\DateTime $beginnBietzeit = null;

    private ?\DateTime $endeBietzeit = null;

    private ?bool $hoechstgebotZeigen = null;

    private ?float $mindestpreis = null;

    public function getBeginnAngebotsphase(): ?\DateTime
    {
        return $this->beginnAngebotsphase;
    }

    public function setBeginnAngebotsphase(?\DateTime $beginnAngebotsphase): self
    {
        $this->beginnAngebotsphase = $beginnAngebotsphase;
        return $this;
    }

    public function getBesichtigungstermin(): ?\DateTime
    {
        return $this->besichtigungstermin;
    }

    public function setBesichtigungstermin(?\DateTime $besichtigungstermin): self
    {
        $this->besichtigungstermin = $besichtigungstermin;
        return $this;
    }

    public function getBesichtigungstermin2(): ?\DateTime
    {
        return $this->besichtigungstermin2;
    }

    public function setBesichtigungstermin2(?\DateTime $besichtigungstermin2): self
    {
        $this->besichtigungstermin2 = $besichtigungstermin2;
        return $this;
    }

    public function getBeginnBietzeit(): ?\DateTime
    {
        return $this->beginnBietzeit;
    }

    public function setBeginnBietzeit(?\DateTime $beginnBietzeit): self
    {
        $this->beginnBietzeit = $beginnBietzeit;
        return $this;
    }

    public function getEndeBietzeit(): ?\DateTime
    {
        return $this->endeBietzeit;
    }

    public function setEndeBietzeit(?\DateTime $endeBietzeit): self
    {
        $this->endeBietzeit = $endeBietzeit;
        return $this;
    }

    public function getHoechstgebotZeigen(): ?bool
    {
        return $this->hoechstgebotZeigen;
    }

    public function setHoechstgebotZeigen(?bool $hoechstgebotZeigen): self
    {
        $this->hoechstgebotZeigen = $hoechstgebotZeigen;
        return $this;
    }

    public function getMindestpreis(): ?float
    {
        return $this->mindestpreis;
    }

    public function setMindestpreis(?float $mindestpreis): self
    {
        $this->mindestpreis = $mindestpreis;
        return $this;
    }
}
