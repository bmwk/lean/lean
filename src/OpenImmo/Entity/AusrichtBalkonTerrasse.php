<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class AusrichtBalkonTerrasse
{
    #[SerializedName('@NORD')]
    private ?bool $nord = null;

    #[SerializedName('@OST')]
    private ?bool $ost = null;

    #[SerializedName('@SUED')]
    private ?bool $sued = null;

    #[SerializedName('@WEST')]
    private ?bool $west = null;

    #[SerializedName('@NORDOST')]
    private ?bool $nordost = null;

    #[SerializedName('@NORDWEST')]
    private ?bool $nordwest = null;

    #[SerializedName('@SUEDOST')]
    private ?bool $suedost = null;

    #[SerializedName('@SUEDWEST')]
    private ?bool $suedwest = null;

    public function getNord(): ?bool
    {
        return $this->nord;
    }

    public function setNord(?bool $nord): self
    {
        $this->nord = $nord;
        return $this;
    }

    public function getOst(): ?bool
    {
        return $this->ost;
    }

    public function setOst(?bool $ost): self
    {
        $this->ost = $ost;
        return $this;
    }

    public function getSued(): ?bool
    {
        return $this->sued;
    }

    public function setSued(?bool $sued): self
    {
        $this->sued = $sued;
        return $this;
    }

    public function getWest(): ?bool
    {
        return $this->west;
    }

    public function setWest(?bool $west): self
    {
        $this->west = $west;
        return $this;
    }

    public function getNordost(): ?bool
    {
        return $this->nordost;
    }

    public function setNordost(?bool $nordost): self
    {
        $this->nordost = $nordost;
        return $this;
    }

    public function getNordwest(): ?bool
    {
        return $this->nordwest;
    }

    public function setNordwest(?bool $nordwest): self
    {
        $this->nordwest = $nordwest;
        return $this;
    }

    public function getSuedost(): ?bool
    {
        return $this->suedost;
    }

    public function setSuedost(?bool $suedost): self
    {
        $this->suedost = $suedost;
        return $this;
    }

    public function getSuedwest(): ?bool
    {
        return $this->suedwest;
    }

    public function setSuedwest(?bool $suedwest): self
    {
        $this->suedwest = $suedwest;
        return $this;
    }
}
