<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class Gesamtbelastungnetto
{
    #[SerializedName('@gesamtbelastungust')]
    private ?float $gesamtbelastungust = null;

    #[SerializedName('#')]
    private ?float $value = null;

    public function getGesamtbelastungust(): ?float
    {
        return $this->gesamtbelastungust;
    }

    public function setGesamtbelastungust(?float $gesamtbelastungust): self
    {
        $this->gesamtbelastungust = $gesamtbelastungust;
        return $this;
    }

    public function getValue(): ?float
    {
        return $this->value;
    }

    public function setValue(?float $value): self
    {
        $this->value = $value;
        return $this;
    }
}
