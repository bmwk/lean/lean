<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class Check
{
    #[SerializedName('@ctype')]
    private ?string $ctype = null;

    private ?\DateTime $value = null;

    public function getCtype(): ?string
    {
        return $this->ctype;
    }

    public function setCtype(?string $ctype): self
    {
        $this->ctype = $ctype;
        return $this;
    }

    public function getValue(): ?\DateTime
    {
        return $this->value;
    }

    public function setValue(?\DateTime $value): self
    {
        $this->value = $value;
        return $this;
    }
}
