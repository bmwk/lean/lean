<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

class ZustandAngaben
{
    private ?string $baujahr = null;

    private ?string $letztemodernisierung = null;

    private ?Zustand $zustand = null;

    private ?Alter $alter = null;

    private ?BebaubarNach $bebaubarNach = null;

    private ?Erschliessung $erschliessung = null;

    private ?ErschliessungUmfang $erschliessungUmfang = null;

    private ?string $bauzone = null;

    private ?string $altlasten = null;

    private ?Energiepass $energiepass = null;

    private ?Verkaufstatus $verkaufstatus = null;

    public function getBaujahr(): ?string
    {
        return $this->baujahr;
    }

    public function setBaujahr(?string $baujahr): self
    {
        $this->baujahr = $baujahr;
        return $this;
    }

    public function getLetztemodernisierung(): ?string
    {
        return $this->letztemodernisierung;
    }

    public function setLetztemodernisierung(?string $letztemodernisierung): self
    {
        $this->letztemodernisierung = $letztemodernisierung;
        return $this;
    }

    public function getZustand(): ?Zustand
    {
        return $this->zustand;
    }

    public function setZustand(?Zustand $zustand): self
    {
        $this->zustand = $zustand;
        return $this;
    }

    public function getAlter(): ?Alter
    {
        return $this->alter;
    }

    public function setAlter(?Alter $alter): self
    {
        $this->alter = $alter;
        return $this;
    }

    public function getBebaubarNach(): ?BebaubarNach
    {
        return $this->bebaubarNach;
    }

    public function setBebaubarNach(?BebaubarNach $bebaubarNach): self
    {
        $this->bebaubarNach = $bebaubarNach;
        return $this;
    }

    public function getErschliessung(): ?Erschliessung
    {
        return $this->erschliessung;
    }

    public function setErschliessung(?Erschliessung $erschliessung): self
    {
        $this->erschliessung = $erschliessung;
        return $this;
    }

    public function getErschliessungUmfang(): ?ErschliessungUmfang
    {
        return $this->erschliessungUmfang;
    }

    public function setErschliessungUmfang(?ErschliessungUmfang $erschliessungUmfang): self
    {
        $this->erschliessungUmfang = $erschliessungUmfang;
        return $this;
    }

    public function getBauzone(): ?string
    {
        return $this->bauzone;
    }

    public function setBauzone(?string $bauzone): self
    {
        $this->bauzone = $bauzone;
        return $this;
    }

    public function getAltlasten(): ?string
    {
        return $this->altlasten;
    }

    public function setAltlasten(?string $altlasten): self
    {
        $this->altlasten = $altlasten;
        return $this;
    }

    public function getEnergiepass(): ?Energiepass
    {
        return $this->energiepass;
    }

    public function setEnergiepass(Energiepass $energiepass): self
    {
        $this->energiepass = $energiepass;
        return $this;
    }

    public function getVerkaufstatus(): ?Verkaufstatus
    {
        return $this->verkaufstatus;
    }

    public function setVerkaufstatus(?Verkaufstatus $verkaufstatus): self
    {
        $this->verkaufstatus = $verkaufstatus;
        return $this;
    }
}
