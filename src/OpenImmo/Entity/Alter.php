<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

class Alter
{
    private ?string $alterAttr = null;

    public function getAlterAttr(): ?string
    {
        return $this->alterAttr;
    }

    public function setAlterAttr(?string $alterAttr): self
    {
        $this->alterAttr = $alterAttr;
        return $this;
    }
}
