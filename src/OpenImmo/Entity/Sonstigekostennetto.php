<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class Sonstigekostennetto
{
    #[SerializedName('@sonstigekostenust')]
    private ?float $sonstigekostenust = null;

    #[SerializedName('#')]
    private ?float $value = null;

    public function getSonstigekostenust(): ?float
    {
        return $this->sonstigekostenust;
    }

    public function setSonstigekostenust(?float $sonstigekostenust): self
    {
        $this->sonstigekostenust = $sonstigekostenust;
        return $this;
    }

    public function getValue(): ?float
    {
        return $this->value;
    }

    public function setValue(?float $value): self
    {
        $this->value = $value;
        return $this;
    }
}
