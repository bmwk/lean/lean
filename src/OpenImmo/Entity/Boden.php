<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class Boden
{
    #[SerializedName('@FLIESEN')]
    private ?bool $fliesen = null;

    #[SerializedName('@STEIN')]
    private ?bool $stein = null;

    #[SerializedName('@TEPPICH')]
    private ?bool $teppich = null;

    #[SerializedName('@PARKETT')]
    private ?bool $parkett = null;

    #[SerializedName('@FERTIGPARKETT')]
    private ?bool $fertigparkett = null;

    #[SerializedName('@LAMINAT')]
    private ?bool $laminat = null;

    #[SerializedName('@DIELEN')]
    private ?bool $dielen = null;

    #[SerializedName('@KUNSTSTOFF')]
    private ?bool $kunststoff = null;

    #[SerializedName('@ESTRICH')]
    private ?bool $estrich = null;

    #[SerializedName('@DOPPELBODEN')]
    private ?bool $doppelboden = null;

    #[SerializedName('@LINOLEUM')]
    private ?bool $linoleum = null;

    #[SerializedName('@MARMOR')]
    private ?bool $marmor = null;

    #[SerializedName('@TERRAKOTTA')]
    private ?bool $terrakotta = null;

    #[SerializedName('@GRANIT')]
    private ?bool $granit = null;

    public function getFliesen(): ?bool
    {
        return $this->fliesen;
    }

    public function setFliesen(?bool $fliesen): self
    {
        $this->fliesen = $fliesen;
        return $this;
    }

    public function getStein(): ?bool
    {
        return $this->stein;
    }

    public function setStein(?bool $stein): self
    {
        $this->stein = $stein;
        return $this;
    }

    public function getTeppich(): ?bool
    {
        return $this->teppich;
    }

    public function setTeppich(?bool $teppich): self
    {
        $this->teppich = $teppich;
        return $this;
    }

    public function getParkett(): ?bool
    {
        return $this->parkett;
    }

    public function setParkett(?bool $parkett): self
    {
        $this->parkett = $parkett;
        return $this;
    }

    public function getFertigparkett(): ?bool
    {
        return $this->fertigparkett;
    }

    public function setFertigparkett(?bool $fertigparkett): self
    {
        $this->fertigparkett = $fertigparkett;
        return $this;
    }

    public function getLaminat(): ?bool
    {
        return $this->laminat;
    }

    public function setLaminat(?bool $laminat): self
    {
        $this->laminat = $laminat;
        return $this;
    }

    public function getDielen(): ?bool
    {
        return $this->dielen;
    }

    public function setDielen(?bool $dielen): self
    {
        $this->dielen = $dielen;
        return $this;
    }

    public function getKunststoff(): ?bool
    {
        return $this->kunststoff;
    }

    public function setKunststoff(?bool $kunststoff): self
    {
        $this->kunststoff = $kunststoff;
        return $this;
    }

    public function getEstrich(): ?bool
    {
        return $this->estrich;
    }

    public function setEstrich(?bool $estrich): self
    {
        $this->estrich = $estrich;
        return $this;
    }

    public function getDoppelboden(): ?bool
    {
        return $this->doppelboden;
    }

    public function setDoppelboden(?bool $doppelboden): self
    {
        $this->doppelboden = $doppelboden;
        return $this;
    }

    public function getLinoleum(): ?bool
    {
        return $this->linoleum;
    }

    public function setLinoleum(?bool $linoleum): self
    {
        $this->linoleum = $linoleum;
        return $this;
    }

    public function getMarmor(): ?bool
    {
        return $this->marmor;
    }

    public function setMarmor(?bool $marmor): self
    {
        $this->marmor = $marmor;
        return $this;
    }

    public function getTerrakotta(): ?bool
    {
        return $this->terrakotta;
    }

    public function setTerrakotta(?bool $terrakotta): self
    {
        $this->terrakotta = $terrakotta;
        return $this;
    }

    public function getGranit(): ?bool
    {
        return $this->granit;
    }

    public function setGranit(?bool $granit): self
    {
        $this->granit = $granit;
        return $this;
    }
}
