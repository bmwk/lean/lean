<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use App\Domain\Property\OpenImmoImporter\OpenImmo\SerializedName;

class BueroPraxen
{
    #[SerializedName('@BUERO_TYP')]
    protected string $bueroTyp;

    public function getBueroTyp(): ?string
    {
        return $this->bueroTyp;
    }

    public function setBueroTyp(?string $bueroTyp): self
    {
        $this->bueroTyp = $bueroTyp;
        return $this;
    }
}
