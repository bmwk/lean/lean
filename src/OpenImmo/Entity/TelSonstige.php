<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class TelSonstige
{
    #[SerializedName('@telefonart')]
    private ?string $telefonart = null;

    #[SerializedName('@bemerkung')]
    private ?string $bemerkung = null;

    #[SerializedName('#')]
    private ?string $value = null;

    public function getTelefonart(): ?string
    {
        return $this->telefonart;
    }

    public function setTelefonart(?string $telefonart): self
    {
        $this->telefonart = $telefonart;
        return $this;
    }

    public function getBemerkung(): ?string
    {
        return $this->bemerkung;
    }

    public function setBemerkung(?string $bemerkung): self
    {
        $this->bemerkung = $bemerkung;
        return $this;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(?string $value): self
    {
        $this->value = $value;
        return $this;
    }
}
