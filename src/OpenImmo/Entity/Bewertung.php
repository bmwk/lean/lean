<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

class Bewertung
{
    private ?array $feld = [];

    public function getFeld(): ?array
    {
        return $this->feld ?? [];
    }

    public function addFeld(Feld $feld): self
    {
        $this->feld[] = $feld;
        return $this;
    }

    public function removeFeld(Feld $feld): self
    {
        return $this;
    }
}
