<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class LandUndForstwirtschaft
{
    #[SerializedName('@land_typ')]
    private ?string $landTyp = null;

    public function getLandTyp(): ?string
    {
        return $this->landTyp;
    }

    public function setLandTyp(?string $landTyp): self
    {
        $this->landTyp = $landTyp;
        return $this;
    }
}
