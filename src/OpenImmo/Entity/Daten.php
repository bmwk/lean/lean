<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

class Daten
{
    private ?string $pfad = null;

    private ?string $anhanginhalt = null;

    public function getPfad(): ?string
    {
        return $this->pfad;
    }

    public function setPfad(?string $pfad): self
    {
        $this->pfad = $pfad;
        return $this;
    }

    public function getAnhanginhalt(): ?string
    {
        return $this->anhanginhalt;
    }

    public function setAnhanginhalt(?string $anhanginhalt): self
    {
        $this->anhanginhalt = $anhanginhalt;
        return $this;
    }
}
