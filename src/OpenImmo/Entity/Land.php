<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class Land
{
    #[SerializedName('@iso_land')]
    private ?string $isoLand = null;

    public function getIsoLand(): ?string
    {
        return $this->isoLand;
    }

    public function setIsoLand(?string $isoLand): self
    {
        $this->isoLand = $isoLand;
        return $this;
    }
}
