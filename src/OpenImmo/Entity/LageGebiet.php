<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class LageGebiet
{
    #[SerializedName('@gebiete')]
    private ?string $gebiete = null;

    #[SerializedName('#')]
    private ?string $value = null;

    public function getGebiete(): ?string
    {
        return $this->gebiete;
    }

    public function setGebiete(?string $gebiete): self
    {
        $this->gebiete = $gebiete;
        return $this;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(?string $value): self
    {
        $this->value = $value;
        return $this;
    }
}
