<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class Bauweise
{
    #[SerializedName('@MASSIV')]
    private ?bool $massiv = null;

    #[SerializedName('@FERTIGTEILE')]
    private ?bool $fertigteile = null;

    #[SerializedName('@HOLZ')]
    private ?bool $holz = null;

    public function getMassiv(): ?bool
    {
        return $this->massiv;
    }

    public function setMassiv(?bool $massiv): self
    {
        $this->massiv = $massiv;
        return $this;
    }

    public function getFertigteile(): ?bool
    {
        return $this->fertigteile;
    }

    public function setFertigteile(?bool $fertigteile): self
    {
        $this->fertigteile = $fertigteile;
        return $this;
    }

    public function getHolz(): ?bool
    {
        return $this->holz;
    }

    public function setHolz(?bool $holz): self
    {
        $this->holz = $holz;
        return $this;
    }
}
