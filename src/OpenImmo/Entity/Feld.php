<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

class Feld
{
    private ?string $name = null;

    private ?string $wert = null;

    private ?array $typ = [];

    private ?array $modus = [];

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getWert(): ?string
    {
        return $this->wert;
    }

    public function setWert(?string $wert): self
    {
        $this->wert = $wert;
        return $this;
    }

    public function getTyp(): ?array
    {
        return $this->typ ?? [];
    }

    public function addTyp(string $typ): self
    {
        $this->typ[] = $typ;
        return $this;
    }

    public function removeType(string $typ): self
    {
        return $this;
    }

    public function getModus(): ?array
    {
        return $this->modus ?? [];
    }

    public function addModus(string $modus): self
    {
        $this->modus[] = $modus;
        return $this;
    }

    public function removeModus(string $modus): self
    {
        return $this;
    }
}
