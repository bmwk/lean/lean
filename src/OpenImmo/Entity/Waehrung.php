<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class Waehrung
{
    #[SerializedName('@iso_waehrung')]
    private ?string $isoWaehrung = null;

    public function getIsoWaehrung(): ?string
    {
        return $this->isoWaehrung;
    }

    public function setIsoWaehrung(?string $isoWaehrung): self
    {
        $this->isoWaehrung = $isoWaehrung;
        return $this;
    }
}
