<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class Monatlichekostennetto
{
    #[SerializedName('@monatlichekostenust')]
    private ?float $monatlichekostenust = null;

    #[SerializedName('#')]
    private ?float $value = null;

    public function getMonatlichekostenust(): ?float
    {
        return $this->monatlichekostenust;
    }

    public function setMonatlichekostenust(?float $monatlichekostenust): self
    {
        $this->monatlichekostenust = $monatlichekostenust;
        return $this;
    }

    public function getValue(): ?float
    {
        return $this->value;
    }

    public function setValue(?float $value): self
    {
        $this->value = $value;
        return $this;
    }
}
