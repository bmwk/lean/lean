<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use App\Domain\Property\OpenImmoImporter\OpenImmo\SerializedName;

class Nettomieteprom2von
{
    #[SerializedName('@nettomieteprom2bis')]
    private ?float $nettomieteprom2bis;

    #[SerializedName('#')]
    private ?float $value = null;

    public function getNettomieteprom2bis(): ?float
    {
        return $this->nettomieteprom2bis;
    }

    public function setNettomieteprom2bis(?float $nettomieteprom2bis): self
    {
        $this->nettomieteprom2bis = $nettomieteprom2bis;
        return $this;
    }

    public function getValue(): ?float
    {
        return $this->value;
    }

    public function setValue(?float $value): self
    {
        $this->value = $value;
        return $this;
    }
}
