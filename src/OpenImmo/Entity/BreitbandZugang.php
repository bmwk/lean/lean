<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

class BreitbandZugang
{
    private ?string $art = null;

    private ?float $speed = null;

    public function getArt(): ?string
    {
        return $this->art;
    }

    public function setArt(?string $art): self
    {
        $this->art = $art;
        return $this;
    }

    public function getSpeed(): ?float
    {
        return $this->speed;
    }

    public function setSpeed(?float $speed): self
    {
        $this->speed = $speed;
        return $this;
    }
}
