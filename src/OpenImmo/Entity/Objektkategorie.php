<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

class Objektkategorie
{
    private ?Nutzungsart $nutzungsart = null;

    private ?Vermarktungsart $vermarktungsart = null;

    private ?Objektart $objektart = null;


    public function getNutzungsart(): ?Nutzungsart
    {
        return $this->nutzungsart;
    }

    public function setNutzungsart(?Nutzungsart $nutzungsart): self
    {
        $this->nutzungsart = $nutzungsart;
        return $this;
    }

    public function getVermarktungsart(): ?Vermarktungsart
    {
        return $this->vermarktungsart;
    }

    public function setVermarktungsart(?Vermarktungsart $vermarktungsart): self
    {
        $this->vermarktungsart = $vermarktungsart;
        return $this;
    }

    public function getObjektart(): ?Objektart
    {
        return $this->objektart;
    }

    public function setObjektart(?Objektart $objektart): self
    {
        $this->objektart = $objektart;
        return $this;
    }
}
