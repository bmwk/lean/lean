<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class LageImBau
{
    #[SerializedName('@LINKS')]
    private ?bool $links = null;

    #[SerializedName('@RECHTS')]
    private ?bool $rechts = null;

    #[SerializedName('@VORNE')]
    private ?bool $vorne = null;

    #[SerializedName('@HINTEN')]
    private ?bool $hinten = null;

    public function getLinks(): ?bool
    {
        return $this->links;
    }

    public function setLinks(?bool $links): self
    {
        $this->links = $links;
        return $this;
    }

    public function getRechts(): ?bool
    {
        return $this->rechts;
    }

    public function setRechts(?bool $rechts): self
    {
        $this->rechts = $rechts;
        return $this;
    }

    public function getVorne(): ?bool
    {
        return $this->vorne;
    }

    public function setVorne(?bool $vorne): self
    {
        $this->vorne = $vorne;
        return $this;
    }

    public function getHinten(): ?bool
    {
        return $this->hinten;
    }

    public function setHinten(?bool $hinten): self
    {
        $this->hinten = $hinten;
        return $this;
    }
}
