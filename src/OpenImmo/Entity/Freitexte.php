<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

class Freitexte
{
    private ?string $objekttitel = null;

    private ?string $dreizeiler = null;

    private ?string $lage = null;

    private ?string $ausstattBeschr = null;

    private ?string $objektbeschreibung = null;

    private ?string $sonstigeAngaben = null;

    private ?ObjektText $objektText = null;

    public function getObjekttitel(): ?string
    {
        return $this->objekttitel;
    }

    public function setObjekttitel(?string $objekttitel): self
    {
        $this->objekttitel = $objekttitel;
        return $this;
    }

    public function getDreizeiler(): ?string
    {
        return $this->dreizeiler;
    }

    public function setDreizeiler(?string $dreizeiler): self
    {
        $this->dreizeiler = $dreizeiler;
        return $this;
    }

    public function getLage(): ?string
    {
        return $this->lage;
    }

    public function setLage(?string $lage): self
    {
        $this->lage = $lage;
        return $this;
    }

    public function getAusstattBeschr(): ?string
    {
        return $this->ausstattBeschr;
    }

    public function setAusstattBeschr(?string $ausstattBeschr): self
    {
        $this->ausstattBeschr = $ausstattBeschr;
        return $this;
    }

    public function getObjektbeschreibung(): ?string
    {
        return $this->objektbeschreibung;
    }

    public function setObjektbeschreibung(?string $objektbeschreibung): self
    {
        $this->objektbeschreibung = $objektbeschreibung;
        return $this;
    }

    public function getSonstigeAngaben(): ?string
    {
        return $this->sonstigeAngaben;
    }

    public function setSonstigeAngaben(?string $sonstigeAngaben): self
    {
        $this->sonstigeAngaben = $sonstigeAngaben;
        return $this;
    }

    public function getObjektText(): ?ObjektText
    {
        return $this->objektText;
    }

    public function setObjektText(?ObjektText $objektText): self
    {
        $this->objektText = $objektText;
        return $this;
    }
}
