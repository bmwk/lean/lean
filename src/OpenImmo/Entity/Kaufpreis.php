<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class Kaufpreis
{
    #[SerializedName('@auf_anfrage')]
    private ?bool $aufAnfrage = null;

    #[SerializedName('#')]
    private ?float $value = null;

    public function getAufAnfrage(): ?bool
    {
        return $this->aufAnfrage;
    }

    public function setAufAnfrage(?bool $aufAnfrage): self
    {
        $this->aufAnfrage = $aufAnfrage;
        return $this;
    }

    public function getValue(): ?float
    {
        return $this->value;
    }

    public function setValue(?float $value): self
    {
        $this->value = $value;
        return $this;
    }
}
