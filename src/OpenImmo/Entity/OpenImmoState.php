<?php

namespace App\OpenImmo\Entity;

use App\Domain\Entity\Property\BuildingUnit;

class OpenImmoState
{
    private $buildingUnitsToDelete = [];

    public function addBuildingUnitToDelete(BuildingUnit $buildingUnit): self
    {
        $this->buildingUnitsToDelete[$buildingUnit->getId()] = $buildingUnit;

        return $this;
    }

    public function setBuildingUnitsToDelete(array $buildingUnits): self
    {
        foreach ($buildingUnits as $buildingUnit) {
            $this->addBuildingUnitToDelete($buildingUnit);
        }

        return $this;
    }

    public function getBuildingUnitsToDelete(): array
    {
        return $this->buildingUnitsToDelete;
    }

    public function removeBuildingUnitToDelete(BuildingUnit $buildingUnit): self
    {
        if (array_key_exists($buildingUnit->getId(), $this->buildingUnitsToDelete)) {
            unset($this->buildingUnitsToDelete[$buildingUnit->getId()]);
        }

        return $this;
    }
}
