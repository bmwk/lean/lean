<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use App\OpenImmo\OpenImmoFlaechenDenormalizer;
use App\OpenImmo\OpenImmoXmlEncoder;
use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Component\PropertyAccess\PropertyAccessor;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
use Symfony\Component\Serializer\NameConverter\MetadataAwareNameConverter;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class OpenImmo
{
    private ?Uebertragung $uebertragung = null;

    private ?array $anbieter = [];

    public static function createFromOpenImmoXml(\SplFileInfo $importFile): self
    {
        $xml = file_get_contents(filename: $importFile->getRealPath());

        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));

        $metadataAwareNameConverter = new MetadataAwareNameConverter(
            metadataFactory: $classMetadataFactory,
            fallbackNameConverter: new CamelCaseToSnakeCaseNameConverter()
        );

        $encoders = [new XmlEncoder()];
        $normalizers = [
            new ArrayDenormalizer(),
            new OpenImmoFlaechenDenormalizer(
                classMetadataFactory: $classMetadataFactory,
                nameConverter: $metadataAwareNameConverter,
                propertyAccessor: new PropertyAccessor(),
                propertyTypeExtractor: new ReflectionExtractor()
            ),
            new ObjectNormalizer(
                classMetadataFactory: $classMetadataFactory,
                nameConverter: $metadataAwareNameConverter,
                propertyAccessor: new PropertyAccessor(),
                propertyTypeExtractor: new ReflectionExtractor()
            ),
        ];

        $serializer = new Serializer(normalizers: $normalizers, encoders: $encoders);

        return $serializer->deserialize(
            data: $xml,
            type: OpenImmo::class,
            format: 'xml',
            context: ['disable_type_enforcement'=> 'true']
        );
    }

    public function toXml(): string
    {
        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));

        $metadataAwareNameConverter = new MetadataAwareNameConverter(
            metadataFactory: $classMetadataFactory,
            fallbackNameConverter: new CamelCaseToSnakeCaseNameConverter()
        );

        $encoders = [new XmlEncoder()];
        $normalizers = [
            new ArrayDenormalizer(),
            new DateTimeNormalizer(),
            new ObjectNormalizer(
                classMetadataFactory: $classMetadataFactory,
                nameConverter: $metadataAwareNameConverter,
                propertyAccessor: new PropertyAccessor(),
                propertyTypeExtractor: new ReflectionExtractor()
            ),
        ];

        $serializer = new Serializer(normalizers: $normalizers, encoders: $encoders);
        return $serializer->serialize(
            data: $this,
            format: 'xml',
            context: [
                'disable_type_enforcement'=> 'true',
                AbstractObjectNormalizer::SKIP_NULL_VALUES => true
                ]
        );
    }

    public function getUebertragung(): ?Uebertragung
    {
        return $this->uebertragung;
    }

    public function setUebertragung(?Uebertragung $uebertragung): self
    {
        $this->uebertragung = $uebertragung;

        return $this;
    }

    /**
     * @return Anbieter[]|null
     */
    public function getAnbieter(): ?array
    {
        return $this->anbieter;
    }

    public function addAnbieter(Anbieter $anbieter): self
    {
        $this->anbieter[] = $anbieter;

        return $this;
    }

    public function removeAnbieter(Anbieter $anbieter): self
    {
        return $this;
    }
}
