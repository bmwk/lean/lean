<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class Sicherheitstechnik
{
    #[SerializedName('@ALARMANLAGE')]
    private ?bool $alarmanlage = null;

    #[SerializedName('@KAMERA')]
    private ?bool $kamera = null;

    #[SerializedName('@POLIZEIRUF')]
    private ?bool $polizeiruf = null;

    public function getAlarmanlage(): ?bool
    {
        return $this->alarmanlage;
    }

    public function setAlarmanlage(?bool $alarmanlage): self
    {
        $this->alarmanlage = $alarmanlage;
        return $this;
    }

    public function getKamera(): ?bool
    {
        return $this->kamera;
    }

    public function setKamera(?bool $kamera): self
    {
        $this->kamera = $kamera;
        return $this;
    }

    public function getPolizeiruf(): ?bool
    {
        return $this->polizeiruf;
    }

    public function setPolizeiruf(?bool $polizeiruf): self
    {
        $this->polizeiruf = $polizeiruf;
        return $this;
    }
}
