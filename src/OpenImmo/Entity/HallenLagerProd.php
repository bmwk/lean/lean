<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use App\Domain\Property\OpenImmoImporter\OpenImmo\SerializedName;

class HallenLagerProd
{
    #[SerializedName('@hallen_typ')]
    private ?string $hallenTyp = null;

    public function getHallenTyp(): ?string
    {
        return $this->hallenTyp;
    }

    public function setHallenTyp(?string $hallenTyp): self
    {
        $this->hallenTyp = $hallenTyp;
        return $this;
    }
}
