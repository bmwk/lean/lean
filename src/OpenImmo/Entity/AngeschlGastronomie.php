<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

class AngeschlGastronomie
{
    private ?bool $hotelrestaurant = null;

    private ?bool $bar = null;

    public function getHotelrestaurant(): ?bool
    {
        return $this->hotelrestaurant;
    }

    public function setHotelrestaurant(?bool $hotelrestaurant): self
    {
        $this->hotelrestaurant = $hotelrestaurant;
        return $this;
    }

    public function getBar(): ?bool
    {
        return $this->bar;
    }

    public function setBar(?bool $bar): self
    {
        $this->bar = $bar;
        return $this;
    }
}
