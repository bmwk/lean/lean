<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class Zimmer
{
    #[SerializedName('@zimmertyp')]
    private ?string $zimmertyp = null;

    public function getZimmertyp(): ?string
    {
        return $this->zimmertyp;
    }

    public function setZimmertyp(?string $zimmertyp): self
    {
        $this->zimmertyp = $zimmertyp;
        return $this;
    }
}
