<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class ProvisionTeilen
{
    #[SerializedName('@wert')]
    private ?string $wert = null;

    #[SerializedName('#')]
    private ?string $value = null;

    public function getWert(): ?string
    {
        return $this->wert;
    }

    public function setWert(?string $wert): self
    {
        $this->wert = $wert;
        return $this;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(?string $value): self
    {
        $this->value = $value;
        return $this;
    }
}
