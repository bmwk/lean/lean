<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

class Energiepass
{
    private ?string $epart = null;

    private ?string $gueltigBis = null;

    private ?string $energieverbrauchkennwert = null;

    private ?bool $mitwarmwasser = null;

    private ?string $endenergiebedarf = null;

    private ?string $primaerenergietraeger = null;

    private ?string $stromwert = null;

    private ?string $waermewert = null;

    private ?string $wertklasse = null;

    private ?string $baujahr = null;

    private ?\DateTime $ausstelldatum = null;

    private ?string $jahrgang = null;

    private ?string $gebaeudeart = null;

    private ?string $epasstext = null;

    private ?string $geg2018;

    private ?string $hwbwert = null;

    private ?string $hwbklasse = null;

    private ?string $fgeewert = null;

    private ?string $fgeeklasse = null;

    public function getEpart(): ?string
    {
        return $this->epart;
    }

    public function setEpart(?string $epart): self
    {
        $this->epart = $epart;
        return $this;
    }

    public function getGueltigBis(): ?string
    {
        return $this->gueltigBis;
    }

    public function setGueltigBis(?string $gueltigBis): self
    {
        $this->gueltigBis = $gueltigBis;
        return $this;
    }

    public function getEnergieverbrauchkennwert(): ?string
    {
        return $this->energieverbrauchkennwert;
    }

    public function setEnergieverbrauchkennwert(?string $energieverbrauchkennwert): self
    {
        $this->energieverbrauchkennwert = $energieverbrauchkennwert;
        return $this;
    }

    public function getMitwarmwasser(): ?bool
    {
        return $this->mitwarmwasser;
    }

    public function setMitwarmwasser(?bool $mitwarmwasser): self
    {
        $this->mitwarmwasser = $mitwarmwasser;
        return $this;
    }

    public function getEndenergiebedarf(): ?string
    {
        return $this->endenergiebedarf;
    }

    public function setEndenergiebedarf(?string $endenergiebedarf): self
    {
        $this->endenergiebedarf = $endenergiebedarf;
        return $this;
    }

    public function getPrimaerenergietraeger(): ?string
    {
        return $this->primaerenergietraeger;
    }

    public function setPrimaerenergietraeger(?string $primaerenergietraeger): self
    {
        $this->primaerenergietraeger = $primaerenergietraeger;
        return $this;
    }

    public function getStromwert(): ?string
    {
        return $this->stromwert;
    }

    public function setStromwert(?string $stromwert): self
    {
        $this->stromwert = $stromwert;
        return $this;
    }

    public function getWaermewert(): ?string
    {
        return $this->waermewert;
    }

    public function setWaermewert(?string $waermewert): self
    {
        $this->waermewert = $waermewert;
        return $this;
    }

    public function getWertklasse(): ?string
    {
        return $this->wertklasse;
    }

    public function setWertklasse(?string $wertklasse): self
    {
        $this->wertklasse = $wertklasse;
        return $this;
    }

    public function getBaujahr(): ?string
    {
        return $this->baujahr;
    }

    public function setBaujahr(?string $baujahr): self
    {
        $this->baujahr = $baujahr;
        return $this;
    }

    public function getAusstelldatum(): ?\DateTime
    {
        return $this->ausstelldatum;
    }

    public function setAusstelldatum(?\DateTime $ausstelldatum): self
    {
        $this->ausstelldatum = $ausstelldatum;
        return $this;
    }

    public function getJahrgang(): ?string
    {
        return $this->jahrgang;
    }

    public function setJahrgang(?string $jahrgang): self
    {
        $this->jahrgang = $jahrgang;
        return $this;
    }

    public function getGebaeudeart(): ?string
    {
        return $this->gebaeudeart;
    }

    public function setGebaeudeart(?string $gebaeudeart): self
    {
        $this->gebaeudeart = $gebaeudeart;
        return $this;
    }

    public function getEpasstext(): ?string
    {
        return $this->epasstext;
    }

    public function setEpasstext(?string $epasstext): self
    {
        $this->epasstext = $epasstext;
        return $this;
    }

    public function getGeg2018(): ?string
    {
        return $this->geg2018;
    }

    public function setGeg2018(?string $geg2018): self
    {
        $this->geg2018 = $geg2018;
        return $this;
    }

    public function getHwbwert(): ?string
    {
        return $this->hwbwert;
    }

    public function setHwbwert(?string $hwbwert): self
    {
        $this->hwbwert = $hwbwert;
        return $this;
    }

    public function getHwbklasse(): ?string
    {
        return $this->hwbklasse;
    }

    public function setHwbklasse(?string $hwbklasse): self
    {
        $this->hwbklasse = $hwbklasse;
        return $this;
    }

    public function getFgeewert(): ?string
    {
        return $this->fgeewert;
    }

    public function setFgeewert(?string $fgeewert): self
    {
        $this->fgeewert = $fgeewert;
        return $this;
    }

    public function getFgeeklasse(): ?string
    {
        return $this->fgeeklasse;
    }

    public function setFgeeklasse(?string $fgeeklasse): self
    {
        $this->fgeeklasse = $fgeeklasse;
        return $this;
    }
}
