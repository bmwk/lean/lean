<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class Hauptmietzinsnetto
{
    #[SerializedName('@hauptmietzinsust')]
    private ?float $hauptmietzinsust = null;

    #[SerializedName('#')]
    private ?float $value = null;

    public function getHauptmietzinsust(): ?float
    {
        return $this->hauptmietzinsust;
    }

    public function setHauptmietzinsust(?float $hauptmietzinsust): self
    {
        $this->hauptmietzinsust = $hauptmietzinsust;
        return $this;
    }

    public function getValue(): ?float
    {
        return $this->value;
    }

    public function setValue(?float $value): self
    {
        $this->value = $value;
        return $this;
    }
}
