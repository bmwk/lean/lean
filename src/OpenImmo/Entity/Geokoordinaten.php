<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class Geokoordinaten
{
    #[SerializedName('@breitengrad')]
    private ?float $breitengrad = null;

    #[SerializedName('@laengengrad')]
    private ?float $laengengrad = null;

    public function getBreitengrad(): ?float
    {
        return $this->breitengrad;
    }

    public function setBreitengrad(?float $breitengrad): self
    {
        $this->breitengrad = $breitengrad;
        return $this;
    }

    public function getLaengengrad(): ?float
    {
        return $this->laengengrad;
    }

    public function setLaengengrad(?float $laengengrad): self
    {
        $this->laengengrad = $laengengrad;
        return $this;
    }
}
