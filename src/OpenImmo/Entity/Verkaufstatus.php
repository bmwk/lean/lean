<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class Verkaufstatus
{
    #[SerializedName('@stand')]
    private ?string $stand = null;

    public function getStand(): ?string
    {
        return $this->stand;
    }

    public function setStand(?string $stand): self
    {
        $this->stand = $stand;
        return $this;
    }
}
