<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class Energietyp
{
    #[SerializedName('@PASSIVHAUS')]
    private ?bool $passivhaus = null;

    #[SerializedName('@NIEDRIGENERGIE')]
    private ?bool $niedrigenergie = null;

    #[SerializedName('@NEUBAUSTANDARD')]
    private ?bool $neubaustandard = null;

    #[SerializedName('@KFW40')]
    private ?bool $kfw40;

    #[SerializedName('@KFW60')]
    private ?bool $kfw60;

    #[SerializedName('@KFW55')]
    private ?bool $kfw55;

    #[SerializedName('@KFW70')]
    private ?bool $kfw70;

    #[SerializedName('@MINERGIEBAUWEISE')]
    private ?bool $minergiebauweise = null;

    #[SerializedName('@MINERGIE_ZERTIFIZIERT')]
    private ?bool $minergieZertifiziert = null;

    public function getPassivhaus(): ?bool
    {
        return $this->passivhaus;
    }

    public function setPassivhaus(?bool $passivhaus): self
    {
        $this->passivhaus = $passivhaus;
        return $this;
    }

    public function getNiedrigenergie(): ?bool
    {
        return $this->niedrigenergie;
    }

    public function setNiedrigenergie(?bool $niedrigenergie): self
    {
        $this->niedrigenergie = $niedrigenergie;
        return $this;
    }

    public function getNeubaustandard(): ?bool
    {
        return $this->neubaustandard;
    }

    public function setNeubaustandard(?bool $neubaustandard): self
    {
        $this->neubaustandard = $neubaustandard;
        return $this;
    }

    public function getKfw40(): ?bool
    {
        return $this->kfw40;
    }

    public function setKfw40(?bool $kfw40): self
    {
        $this->kfw40 = $kfw40;
        return $this;
    }

    public function getKfw60(): ?bool
    {
        return $this->kfw60;
    }

    public function setKfw60(?bool $kfw60): self
    {
        $this->kfw60 = $kfw60;
        return $this;
    }

    public function getKfw55(): ?bool
    {
        return $this->kfw55;
    }

    public function setKfw55(?bool $kfw55): self
    {
        $this->kfw55 = $kfw55;
        return $this;
    }

    public function getKfw70(): ?bool
    {
        return $this->kfw70;
    }

    public function setKfw70(?bool $kfw70): self
    {
        $this->kfw70 = $kfw70;
        return $this;
    }

    public function getMinergiebauweise(): ?bool
    {
        return $this->minergiebauweise;
    }

    public function setMinergiebauweise(?bool $minergiebauweise): self
    {
        $this->minergiebauweise = $minergiebauweise;
        return $this;
    }

    public function getMinergieZertifiziert(): ?bool
    {
        return $this->minergieZertifiziert;
    }

    public function setMinergieZertifiziert(?bool $minergieZertifiziert): self
    {
        $this->minergieZertifiziert = $minergieZertifiziert;
        return $this;
    }
}
