<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

class Flaechen
{
    private ?float $wohnflaeche = null;

    private ?float $nutzflaeche = null;

    private ?float $gesamtflaeche = null;

    private ?float $ladenflaeche = null;

    private ?float $lagerflaeche = null;

    private ?float $verkaufsflaeche = null;

    private ?float $freiflaeche = null;

    private ?float $bueroflaeche = null;

    private ?float $bueroteilflaeche = null;

    private ?float $fensterfront = null;

    private ?float $verwaltungsflaeche = null;

    private ?float $gastroflaeche = null;

    private ?string $grz = null;

    private ?string $gfz = null;

    private ?string $bmz = null;

    private ?string $bgf = null;

    private ?float $grundstuecksflaeche = null;

    private ?float $sonstflaeche = null;

    private ?float $anzahlZimmer = null;

    private ?float $anzahlSchlafzimmer = null;

    private ?float $anzahlBadezimmer = null;

    private ?float $anzahlSepWc = null;

    private ?float $anzahlBalkone = null;

    private ?float $anzahlTerrassen = null;

    private ?float $anzahlLogia = null;

    private ?float $balkonTerrasseFlaeche = null;

    private ?float $anzahlWohnSchlafzimmer = null;

    private ?float $gartenflaeche = null;

    private ?float $kellerflaeche = null;

    private ?float $fensterfrontQm = null;

    private ?float $grundstuecksfront = null;

    private ?float $dachbodenflaeche = null;

    private ?float $teilbarAb = null;

    private ?float $beheizbareFlaeche = null;

    private ?int $anzahlStellplaetze = null;

    private ?float $plaetzeGastraum = null;

    private ?float $anzahlBetten = null;

    private ?float $anzahlTagungsraeume = null;

    private ?float $vermietbareFlaeche = null;

    private ?float $anzahlWohneinheiten = null;

    private ?float $anzahlGewerbeeinheiten = null;

    private ?bool $einliegerwohnung = null;

    private ?float $kubatur = null;

    private ?float $ausnuetzungsziffer = null;

    private ?float $flaechevon = null;

    private ?float $flaechebis = null;


    public function getWohnflaeche(): ?float
    {
        return $this->wohnflaeche;
    }

    public function setWohnflaeche(?float $wohnflaeche): self
    {
        $this->wohnflaeche = $wohnflaeche;
        return $this;
    }

    public function getNutzflaeche(): ?float
    {
        return $this->nutzflaeche;
    }

    public function setNutzflaeche(?float $nutzflaeche): self
    {
        $this->nutzflaeche = $nutzflaeche;
        return $this;
    }

    public function getGesamtflaeche(): ?float
    {
        return $this->gesamtflaeche;
    }

    public function setGesamtflaeche(?float $gesamtflaeche): self
    {
        $this->gesamtflaeche = $gesamtflaeche;
        return $this;
    }

    public function getLadenflaeche(): ?float
    {
        return $this->ladenflaeche;
    }

    public function setLadenflaeche(?float $ladenflaeche): self
    {
        $this->ladenflaeche = $ladenflaeche;
        return $this;
    }

    public function getLagerflaeche(): ?float
    {
        return $this->lagerflaeche;
    }

    public function setLagerflaeche(?float $lagerflaeche): self
    {
        $this->lagerflaeche = $lagerflaeche;
        return $this;
    }

    public function getVerkaufsflaeche(): ?float
    {
        return $this->verkaufsflaeche;
    }

    public function setVerkaufsflaeche(?float $verkaufsflaeche): self
    {
        $this->verkaufsflaeche = $verkaufsflaeche;
        return $this;
    }

    public function getFreiflaeche(): ?float
    {
        return $this->freiflaeche;
    }

    public function setFreiflaeche(?float $freiflaeche): self
    {
        $this->freiflaeche = $freiflaeche;
        return $this;
    }

    public function getBueroflaeche(): ?float
    {
        return $this->bueroflaeche;
    }

    public function setBueroflaeche(?float $bueroflaeche): self
    {
        $this->bueroflaeche = $bueroflaeche;
        return $this;
    }

    public function getBueroteilflaeche(): ?float
    {
        return $this->bueroteilflaeche;
    }

    public function setBueroteilflaeche(?float $bueroteilflaeche): self
    {
        $this->bueroteilflaeche = $bueroteilflaeche;
        return $this;
    }

    public function getFensterfront(): ?float
    {
        return $this->fensterfront;
    }

    public function setFensterfront(?float $fensterfront): self
    {
        $this->fensterfront = $fensterfront;
        return $this;
    }

    public function getVerwaltungsflaeche(): ?float
    {
        return $this->verwaltungsflaeche;
    }

    public function setVerwaltungsflaeche(?float $verwaltungsflaeche): self
    {
        $this->verwaltungsflaeche = $verwaltungsflaeche;
        return $this;
    }

    public function getGastroflaeche(): ?float
    {
        return $this->gastroflaeche;
    }

    public function setGastroflaeche(?float $gastroflaeche): self
    {
        $this->gastroflaeche = $gastroflaeche;
        return $this;
    }

    public function getGrz(): ?string
    {
        return $this->grz;
    }

    public function setGrz(?string $grz): self
    {
        $this->grz = $grz;
        return $this;
    }

    public function getGfz(): ?string
    {
        return $this->gfz;
    }

    public function setGfz(?string $gfz): self
    {
        $this->gfz = $gfz;
        return $this;
    }

    public function getBmz(): ?string
    {
        return $this->bmz;
    }

    public function setBmz(?string $bmz): self
    {
        $this->bmz = $bmz;
        return $this;
    }

    public function getBgf(): ?string
    {
        return $this->bgf;
    }

    public function setBgf(?string $bgf): self
    {
        $this->bgf = $bgf;
        return $this;
    }

    public function getGrundstuecksflaeche(): ?float
    {
        return $this->grundstuecksflaeche;
    }

    public function setGrundstuecksflaeche(?float $grundstuecksflaeche): self
    {
        $this->grundstuecksflaeche = $grundstuecksflaeche;
        return $this;
    }

    public function getSonstflaeche(): ?float
    {
        return $this->sonstflaeche;
    }

    public function setSonstflaeche(?float $sonstflaeche): self
    {
        $this->sonstflaeche = $sonstflaeche;
        return $this;
    }

    public function getAnzahlZimmer(): ?float
    {
        return $this->anzahlZimmer;
    }

    public function setAnzahlZimmer(?float $anzahlZimmer): self
    {
        $this->anzahlZimmer = $anzahlZimmer;
        return $this;
    }

    public function getAnzahlSchlafzimmer(): ?float
    {
        return $this->anzahlSchlafzimmer;
    }

    public function setAnzahlSchlafzimmer(?float $anzahlSchlafzimmer): self
    {
        $this->anzahlSchlafzimmer = $anzahlSchlafzimmer;
        return $this;
    }

    public function getAnzahlBadezimmer(): ?float
    {
        return $this->anzahlBadezimmer;
    }

    public function setAnzahlBadezimmer(?float $anzahlBadezimmer): self
    {
        $this->anzahlBadezimmer = $anzahlBadezimmer;
        return $this;
    }

    public function getAnzahlSepWc(): ?float
    {
        return $this->anzahlSepWc;
    }

    public function setAnzahlSepWc(?float $anzahlSepWc): self
    {
        $this->anzahlSepWc = $anzahlSepWc;
        return $this;
    }

    public function getAnzahlBalkone(): ?float
    {
        return $this->anzahlBalkone;
    }

    public function setAnzahlBalkone(?float $anzahlBalkone): self
    {
        $this->anzahlBalkone = $anzahlBalkone;
        return $this;
    }

    public function getAnzahlTerrassen(): ?float
    {
        return $this->anzahlTerrassen;
    }

    public function setAnzahlTerrassen(?float $anzahlTerrassen): self
    {
        $this->anzahlTerrassen = $anzahlTerrassen;
        return $this;
    }

    public function getAnzahlLogia(): ?float
    {
        return $this->anzahlLogia;
    }

    public function setAnzahlLogia(?float $anzahlLogia): self
    {
        $this->anzahlLogia = $anzahlLogia;
        return $this;
    }

    public function getBalkonTerrasseFlaeche(): ?float
    {
        return $this->balkonTerrasseFlaeche;
    }

    public function setBalkonTerrasseFlaeche(?float $balkonTerrasseFlaeche): self
    {
        $this->balkonTerrasseFlaeche = $balkonTerrasseFlaeche;
        return $this;
    }

    public function getAnzahlWohnSchlafzimmer(): ?float
    {
        return $this->anzahlWohnSchlafzimmer;
    }

    public function setAnzahlWohnSchlafzimmer(?float $anzahlWohnSchlafzimmer): self
    {
        $this->anzahlWohnSchlafzimmer = $anzahlWohnSchlafzimmer;
        return $this;
    }

    public function getGartenflaeche(): ?float
    {
        return $this->gartenflaeche;
    }

    public function setGartenflaeche(?float $gartenflaeche): self
    {
        $this->gartenflaeche = $gartenflaeche;
        return $this;
    }

    public function getKellerflaeche(): ?float
    {
        return $this->kellerflaeche;
    }

    public function setKellerflaeche(?float $kellerflaeche): self
    {
        $this->kellerflaeche = $kellerflaeche;
        return $this;
    }

    public function getFensterfrontQm(): ?float
    {
        return $this->fensterfrontQm;
    }

    public function setFensterfrontQm(?float $fensterfrontQm): self
    {
        $this->fensterfrontQm = $fensterfrontQm;
        return $this;
    }

    public function getGrundstuecksfront(): ?float
    {
        return $this->grundstuecksfront;
    }

    public function setGrundstuecksfront(?float $grundstuecksfront): self
    {
        $this->grundstuecksfront = $grundstuecksfront;
        return $this;
    }

    public function getDachbodenflaeche(): ?float
    {
        return $this->dachbodenflaeche;
    }

    public function setDachbodenflaeche(?float $dachbodenflaeche): self
    {
        $this->dachbodenflaeche = $dachbodenflaeche;
        return $this;
    }

    public function getTeilbarAb(): ?float
    {
        return $this->teilbarAb;
    }

    public function setTeilbarAb(?float $teilbarAb): self
    {
        $this->teilbarAb = $teilbarAb;
        return $this;
    }

    public function getBeheizbareFlaeche(): ?float
    {
        return $this->beheizbareFlaeche;
    }

    public function setBeheizbareFlaeche(?float $beheizbareFlaeche): self
    {
        $this->beheizbareFlaeche = $beheizbareFlaeche;
        return $this;
    }

    public function getAnzahlStellplaetze(): ?int
    {
        return $this->anzahlStellplaetze;
    }

    public function setAnzahlStellplaetze(?int $anzahlStellplaetze): self
    {
        $this->anzahlStellplaetze = $anzahlStellplaetze;
        return $this;
    }

    public function getPlaetzeGastraum(): ?float
    {
        return $this->plaetzeGastraum;
    }

    public function setPlaetzeGastraum(?float $plaetzeGastraum): self
    {
        $this->plaetzeGastraum = $plaetzeGastraum;
        return $this;
    }

    public function getAnzahlBetten(): ?float
    {
        return $this->anzahlBetten;
    }

    public function setAnzahlBetten(?float $anzahlBetten): self
    {
        $this->anzahlBetten = $anzahlBetten;
        return $this;
    }

    public function getAnzahlTagungsraeume(): ?float
    {
        return $this->anzahlTagungsraeume;
    }

    public function setAnzahlTagungsraeume(?float $anzahlTagungsraeume): self
    {
        $this->anzahlTagungsraeume = $anzahlTagungsraeume;
        return $this;
    }

    public function getVermietbareFlaeche(): ?float
    {
        return $this->vermietbareFlaeche;
    }

    public function setVermietbareFlaeche(?float $vermietbareFlaeche): self
    {
        $this->vermietbareFlaeche = $vermietbareFlaeche;
        return $this;
    }

    public function getAnzahlWohneinheiten(): ?float
    {
        return $this->anzahlWohneinheiten;
    }

    public function setAnzahlWohneinheiten(?float $anzahlWohneinheiten): self
    {
        $this->anzahlWohneinheiten = $anzahlWohneinheiten;
        return $this;
    }

    public function getAnzahlGewerbeeinheiten(): ?float
    {
        return $this->anzahlGewerbeeinheiten;
    }

    public function setAnzahlGewerbeeinheiten(?float $anzahlGewerbeeinheiten): self
    {
        $this->anzahlGewerbeeinheiten = $anzahlGewerbeeinheiten;
        return $this;
    }

    public function getEinliegerwohnung(): ?bool
    {
        return $this->einliegerwohnung;
    }

    public function setEinliegerwohnung(?bool $einliegerwohnung): self
    {
        $this->einliegerwohnung = $einliegerwohnung;
        return $this;
    }

    public function getKubatur(): ?float
    {
        return $this->kubatur;
    }

    public function setKubatur(?float $kubatur): self
    {
        $this->kubatur = $kubatur;
        return $this;
    }

    public function getAusnuetzungsziffer(): ?float
    {
        return $this->ausnuetzungsziffer;
    }

    public function setAusnuetzungsziffer(?float $ausnuetzungsziffer): self
    {
        $this->ausnuetzungsziffer = $ausnuetzungsziffer;
        return $this;
    }

    public function getFlaechevon(): ?float
    {
        return $this->flaechevon;
    }

    public function setFlaechevon(?float $flaechevon): self
    {
        $this->flaechevon = $flaechevon;
        return $this;
    }

    public function getFlaechebis(): ?float
    {
        return $this->flaechebis;
    }

    public function setFlaechebis(?float $flaechebis): self
    {
        $this->flaechebis = $flaechebis;
        return $this;
    }
}
