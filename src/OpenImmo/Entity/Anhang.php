<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class Anhang
{
    #[SerializedName('@location')]
    private ?string $location = null;

    #[SerializedName('@gruppe')]
    private ?string $gruppe = null;

    private ?string $anhangtitel = null;

    private ?string $format = null;

    private ?Check $check = null;

    private ?Daten $daten = null;

    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function setLocation(?string $location): self
    {
        $this->location = $location;
        return $this;
    }

    public function getGruppe(): ?string
    {
        return $this->gruppe;
    }

    public function setGruppe(?string $gruppe): self
    {
        $this->gruppe = $gruppe;
        return $this;
    }

    public function getAnhangtitel(): ?string
    {
        return $this->anhangtitel;
    }

    public function setAnhangtitel(?string $anhangtitel): self
    {
        $this->anhangtitel = $anhangtitel;
        return $this;
    }

    public function getFormat(): ?string
    {
        return $this->format;
    }

    public function setFormat(?string $format): Anhang
    {
        $this->format = $format;
        return $this;
    }

    public function getCheck(): ?Check
    {
        return $this->check;
    }

    public function setCheck(?Check $check): Anhang
    {
        $this->check = $check;
        return $this;
    }

    public function getDaten(): ?Daten
    {
        return $this->daten;
    }

    public function setDaten(?Daten $daten): Anhang
    {
        $this->daten = $daten;
        return $this;
    }
}
