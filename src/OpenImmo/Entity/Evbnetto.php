<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class Evbnetto
{
    /**
     * @SerializedName("@evbust)
     * @var float
     */
    private ?float $evbust = null;

    #[SerializedName('#')]
    private ?float $value = null;

    public function getEvbust(): ?float
    {
        return $this->evbust;
    }

    public function setEvbust(?float $evbust): self
    {
        $this->evbust = $evbust;
        return $this;
    }

    public function getValue(): ?float
    {
        return $this->value;
    }

    public function setValue(?float $value): self
    {
        $this->value = $value;
        return $this;
    }
}
