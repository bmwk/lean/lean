<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class Serviceleistungen
{
    #[SerializedName('@BETREUTES_WOHNEN')]
    private ?bool $betreutesWohnen = null;

    #[SerializedName('@CATERING')]
    private ?bool $catering = null;

    #[SerializedName('@REINIGUNG')]
    private ?bool $reinigung = null;

    #[SerializedName('@EINKAUF')]
    private ?bool $einkauf = null;

    #[SerializedName('@WACHDIENST')]
    private ?bool $wachdienst = null;

    public function getBetreutesWohnen(): ?bool
    {
        return $this->betreutesWohnen;
    }

    public function setBetreutesWohnen(?bool $betreutesWohnen): self
    {
        $this->betreutesWohnen = $betreutesWohnen;
        return $this;
    }

    public function getCatering(): ?bool
    {
        return $this->catering;
    }

    public function setCatering(?bool $catering): self
    {
        $this->catering = $catering;
        return $this;
    }

    public function getReinigung(): ?bool
    {
        return $this->reinigung;
    }

    public function setReinigung(?bool $reinigung): self
    {
        $this->reinigung = $reinigung;
        return $this;
    }

    public function getEinkauf(): ?bool
    {
        return $this->einkauf;
    }

    public function setEinkauf(?bool $einkauf): self
    {
        $this->einkauf = $einkauf;
        return $this;
    }

    public function getWachdienst(): ?bool
    {
        return $this->wachdienst;
    }

    public function setWachdienst(?bool $wachdienst): self
    {
        $this->wachdienst = $wachdienst;
        return $this;
    }
}
