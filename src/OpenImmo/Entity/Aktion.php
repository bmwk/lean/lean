<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class Aktion
{
    #[SerializedName('@aktionart')]
    private ?string $aktionart = null;

    public function getAktionart(): ?string
    {
        return $this->aktionart;
    }

    public function setAktionart(?string $aktionart): self
    {
        $this->aktionart = $aktionart;
        return $this;
    }
}
