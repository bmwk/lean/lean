<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

class VerwaltungTechn
{
    private ?string $objektnrIntern = null;
    
    private ?string $objektnrExtern = null;
    
    private ?Aktion $aktion = null;
    
    private ?\DateTime $aktivVon = null;
    
    private ?\DateTime $aktivBis = null;
    
    protected string $openimmoObid;
    
    private ?string $kennungUrsprung = null;
    
    private ?\DateTime $standVom = null;
    
    private ?bool $weitergabeGenerell = null;
    
    private ?string $weitergabePositiv = null;

    private ?string $weitergabeNegativ = null;
    
    private ?string $gruppenKennung = null;
    
    private ?Master $master = null;
    
    private ?string $sprache = null;


    public function getObjektnrIntern(): ?string
    {
        return $this->objektnrIntern;
    }

    public function setObjektnrIntern(?string $objektnrIntern): self
    {
        $this->objektnrIntern = $objektnrIntern;
        return $this;
    }

    public function getObjektnrExtern(): ?string
    {
        return $this->objektnrExtern;
    }

    public function setObjektnrExtern(?string $objektnrExtern): self
    {
        $this->objektnrExtern = $objektnrExtern;
        return $this;
    }

    public function getAktion(): ?Aktion
    {
        return $this->aktion;
    }

    public function setAktion(?Aktion $aktion): self
    {
        $this->aktion = $aktion;
        return $this;
    }

    public function getAktivVon(): ?\DateTime
    {
        return $this->aktivVon;
    }

    public function setAktivVon(?\DateTime $aktivVon): self
    {
        $this->aktivVon = $aktivVon;
        return $this;
    }

    public function getAktivBis(): ?\DateTime
    {
        return $this->aktivBis;
    }

    public function setAktivBis(?\DateTime $aktivBis): self
    {
        $this->aktivBis = $aktivBis;
        return $this;
    }

    public function getOpenimmoObid(): ?string
    {
        return $this->openimmoObid;
    }

    public function setOpenimmoObid(?string $openimmoObid): self
    {
        $this->openimmoObid = $openimmoObid;
        return $this;
    }

    public function getKennungUrsprung(): ?string
    {
        return $this->kennungUrsprung;
    }

    public function setKennungUrsprung(?string $kennungUrsprung): self
    {
        $this->kennungUrsprung = $kennungUrsprung;
        return $this;
    }

    public function getStandVom(): ?\DateTime
    {
        return $this->standVom;
    }

    public function setStandVom(?\DateTime $standVom): self
    {
        $this->standVom = $standVom;
        return $this;
    }

    public function getWeitergabeGenerell(): ?bool
    {
        return $this->weitergabeGenerell;
    }

    public function setWeitergabeGenerell(?bool $weitergabeGenerell): self
    {
        $this->weitergabeGenerell = $weitergabeGenerell;
        return $this;
    }

    public function getWeitergabePositiv(): ?string
    {
        return $this->weitergabePositiv;
    }

    public function setWeitergabePositiv(?string $weitergabePositiv): self
    {
        $this->weitergabePositiv = $weitergabePositiv;
        return $this;
    }

    public function getWeitergabeNegativ(): ?string
    {
        return $this->weitergabeNegativ;
    }

    public function setWeitergabeNegativ(?string $weitergabeNegativ): self
    {
        $this->weitergabeNegativ = $weitergabeNegativ;
        return $this;
    }

    public function getGruppenKennung(): ?string
    {
        return $this->gruppenKennung;
    }

    public function setGruppenKennung(?string $gruppenKennung): self
    {
        $this->gruppenKennung = $gruppenKennung;
        return $this;
    }

    public function getMaster(): ?Master
    {
        return $this->master;
    }

    public function setMaster(?Master $master): self
    {
        $this->master = $master;
        return $this;
    }

    public function getSprache(): ?string
    {
        return $this->sprache;
    }

    public function setSprache(?string $sprache): self
    {
        $this->sprache = $sprache;
        return $this;
    }
}
