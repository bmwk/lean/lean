<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class Fahrstuhl
{
    #[SerializedName('@PERSONEN')]
    private ?bool $personen = null;

    #[SerializedName('@LASTEN')]
    private ?bool $lasten = null;

    public function getPersonen(): ?bool
    {
        return $this->personen;
    }

    public function setPersonen(?bool $personen): self
    {
        $this->personen = $personen;
        return $this;
    }

    public function getLasten(): ?bool
    {
        return $this->lasten;
    }

    public function setLasten(?bool $lasten): self
    {
        $this->lasten = $lasten;
        return $this;
    }
}
