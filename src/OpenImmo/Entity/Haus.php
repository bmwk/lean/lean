<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class Haus
{
    #[SerializedName('@haustyp')]
    private ?string $haustyp = null;

    public function getHaustyp(): ?string
    {
        return $this->haustyp;
    }

    public function setHaustyp(?string $haustyp): self
    {
        $this->haustyp = $haustyp;
        return $this;
    }
}
