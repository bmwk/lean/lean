<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class MinMietdauer
{
    #[SerializedName('@min_dauer')]
    private ?string $minDauer = null;

    #[SerializedName('#')]
    private ?string $value = null;

    public function getMinDauer(): ?string
    {
        return $this->minDauer;
    }

    public function setMinDauer(?string $minDauer): self
    {
        $this->minDauer = $minDauer;
        return $this;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(?string $value): self
    {
        $this->value = $value;
        return $this;
    }
}
