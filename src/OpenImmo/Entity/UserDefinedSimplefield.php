<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class UserDefinedSimplefield
{
    #[SerializedName('@feldname')]
    private ?string $feldname = null;

    #[SerializedName('#')]
    private ?string $value = null;

    public function getFeldname(): ?string
    {
        return $this->feldname;
    }

    public function setFeldname(?string $feldname): self
    {
        $this->feldname = $feldname;
        return $this;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(?string $value): self
    {
        $this->value = $value;
        return $this;
    }
}
