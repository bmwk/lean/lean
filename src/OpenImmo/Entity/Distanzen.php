<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class Distanzen
{
    #[SerializedName('@distanz_zu')]
    private ?string $distanzZu = null;

    #[SerializedName('#')]
    private ?string $value = null;

    public function getDistanzZu(): ?string
    {
        return $this->distanzZu;
    }

    public function setDistanzZu(?string $distanzZu): self
    {
        $this->distanzZu = $distanzZu;
        return $this;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(?string $value): self
    {
        $this->value = $value;
        return $this;
    }
}
