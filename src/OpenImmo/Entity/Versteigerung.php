<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

class Versteigerung
{
    private ?bool $zwangsversteigerung = null;

    private ?string $aktenzeichen = null;

    private ?\DateTime $zvtermin = null;

    private ?\DateTime $zusatztermin = null;

    private ?string $amtsgericht = null;

    private ?float $verkehrswert = null;

    public function getZwangsversteigerung(): ?bool
    {
        return $this->zwangsversteigerung;
    }

    public function setZwangsversteigerung(?bool $zwangsversteigerung): self
    {
        $this->zwangsversteigerung = $zwangsversteigerung;
        return $this;
    }

    public function getAktenzeichen(): ?string
    {
        return $this->aktenzeichen;
    }

    public function setAktenzeichen(?string $aktenzeichen): self
    {
        $this->aktenzeichen = $aktenzeichen;
        return $this;
    }

    public function getZvtermin(): ?\DateTime
    {
        return $this->zvtermin;
    }

    public function setZvtermin(?\DateTime $zvtermin): self
    {
        $this->zvtermin = $zvtermin;
        return $this;
    }

    public function getZusatztermin(): ?\DateTime
    {
        return $this->zusatztermin;
    }

    public function setZusatztermin(?\DateTime $zusatztermin): self
    {
        $this->zusatztermin = $zusatztermin;
        return $this;
    }

    public function getAmtsgericht(): ?string
    {
        return $this->amtsgericht;
    }

    public function setAmtsgericht(?string $amtsgericht): self
    {
        $this->amtsgericht = $amtsgericht;
        return $this;
    }

    public function getVerkehrswert(): ?float
    {
        return $this->verkehrswert;
    }

    public function setVerkehrswert(?float $verkehrswert): self
    {
        $this->verkehrswert = $verkehrswert;
        return $this;
    }
}
