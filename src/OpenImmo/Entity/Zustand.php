<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class Zustand
{
    #[SerializedName('@zustand_art')]
    private ?string $zustandArt = null;

    public function getZustandArt(): ?string
    {
        return $this->zustandArt;
    }

    public function setZustandArt(?string $zustandArt): self
    {
        $this->zustandArt = $zustandArt;
        return $this;
    }
}
