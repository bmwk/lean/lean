<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class Sonstige
{
    #[SerializedName('@sonstige_typ')]
    private ?string $sonstigeTyp = null;

    public function getSonstigeTyp(): ?string
    {
        return $this->sonstigeTyp;
    }

    public function setSonstigeTyp(?string $sonstigeTyp): self
    {
        $this->sonstigeTyp = $sonstigeTyp;
        return $this;
    }
}
