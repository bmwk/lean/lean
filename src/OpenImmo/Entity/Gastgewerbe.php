<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class Gastgewerbe
{
    #[SerializedName('@gastgew_typ')]
    private ?string $gastgewTyp = null;

    public function getGastgewTyp(): ?string
    {
        return $this->gastgewTyp;
    }

    public function setGastgewTyp(?string $gastgewTyp): self
    {
        $this->gastgewTyp = $gastgewTyp;
        return $this;
    }
}
