<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class Dachform
{
    #[SerializedName('@KRUEPPELWALMDACH')]
    private ?bool $krueppelwalmdach = null;

    #[SerializedName('@MANSARDDACH')]
    private ?bool $mansarddach = null;

    #[SerializedName('@PULTDACH')]
    private ?bool $pultdach = null;

    #[SerializedName('@SATTELDACH')]
    private ?bool $satteldach = null;

    #[SerializedName('@WALMDACH')]
    private ?bool $walmdach = null;

    #[SerializedName('@FLACHDACH')]
    private ?bool $flachdach = null;

    #[SerializedName('@PYRAMIDENDACH')]
    private ?bool $pyramidendach = null;

    public function getKrueppelwalmdach(): ?bool
    {
        return $this->krueppelwalmdach;
    }

    public function setKrueppelwalmdach(?bool $krueppelwalmdach): self
    {
        $this->krueppelwalmdach = $krueppelwalmdach;
        return $this;
    }

    public function getMansarddach(): ?bool
    {
        return $this->mansarddach;
    }

    public function setMansarddach(?bool $mansarddach): self
    {
        $this->mansarddach = $mansarddach;
        return $this;
    }

    public function getPultdach(): ?bool
    {
        return $this->pultdach;
    }

    public function setPultdach(?bool $pultdach): self
    {
        $this->pultdach = $pultdach;
        return $this;
    }

    public function getSatteldach(): ?bool
    {
        return $this->satteldach;
    }

    public function setSatteldach(?bool $satteldach): self
    {
        $this->satteldach = $satteldach;
        return $this;
    }

    public function getWalmdach(): ?bool
    {
        return $this->walmdach;
    }

    public function setWalmdach(?bool $walmdach): self
    {
        $this->walmdach = $walmdach;
        return $this;
    }

    public function getFlachdach(): ?bool
    {
        return $this->flachdach;
    }

    public function setFlachdach(?bool $flachdach): self
    {
        $this->flachdach = $flachdach;
        return $this;
    }

    public function getPyramidendach(): ?bool
    {
        return $this->pyramidendach;
    }

    public function setPyramidendach(?bool $pyramidendach): self
    {
        $this->pyramidendach = $pyramidendach;
        return $this;
    }
}
