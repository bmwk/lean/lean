<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class Einzelhandel
{
    #[SerializedName('@handel_typ')]
    private ?string $handelTyp = null;

    public function getHandelTyp(): ?string
    {
        return $this->handelTyp;
    }

    public function setHandelTyp(?string $handelTyp): self
    {
        $this->handelTyp = $handelTyp;
        return $this;
    }
}
