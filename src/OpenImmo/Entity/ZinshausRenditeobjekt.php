<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class ZinshausRenditeobjekt
{
    #[SerializedName('@zins_typ')]
    private ?string $zinsTyp = null;

    public function getZinsTyp(): ?string
    {
        return $this->zinsTyp;
    }

    public function setZinsTyp(?string $zinsTyp): self
    {
        $this->zinsTyp = $zinsTyp;
        return $this;
    }
}
