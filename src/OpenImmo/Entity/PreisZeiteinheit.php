<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class PreisZeiteinheit
{
    #[SerializedName('@zeiteinheit')]
    private ?string $zeiteinheit = null;

    public function getZeiteinheit(): ?string
    {
        return $this->zeiteinheit;
    }

    public function setZeiteinheit(?string $zeiteinheit): self
    {
        $this->zeiteinheit = $zeiteinheit;
        return $this;
    }
}
