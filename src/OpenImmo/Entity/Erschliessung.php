<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class Erschliessung
{
    #[SerializedName('@erschl_attr')]
    protected string $erschlAttr;

    public function getErschlAttr(): ?string
    {
        return $this->erschlAttr;
    }

    public function setErschlAttr(?string $erschlAttr): self
    {
        $this->erschlAttr = $erschlAttr;
        return $this;
    }
}
