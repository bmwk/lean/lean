<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

class VerwaltungObjekt
{
    private ?bool $objektadresseFreigeben = null;
    
    private ?string $verfuegbarAb = null;
    
    private ?\DateTime $abdatum = null;
    
    private ?\DateTime $bisdatum = null;
    
    private ?MinMietdauer $minMietdauer = null;
    
    private ?MaxMietdauer $maxMietdauer = null;
    
    private ?\DateTime $versteigerungstermin = null;
    
    private ?bool $wbsSozialwohnung = null;
    
    private ?bool $vermietet = null;
    
    private ?string $gruppennummer = null;
    
    private ?string $zugang = null;
    
    private ?float $laufzeit = null;
    
    private ?int $maxPersonen = null;
    
    private ?bool $nichtraucher = null;
    
    private ?bool $haustiere = null;
    
    private ?Geschlecht $geschlecht = null;

    private ?bool $denkmalgeschuetzt = null;
    
    private ?bool $alsFerien = null;
    
    private ?bool $gewerblicheNutzung = null;
    
    private ?string $branchen = null;
    
    private ?bool $hochhaus = null;

    public function getObjektadresseFreigeben(): ?bool
    {
        return $this->objektadresseFreigeben;
    }

    public function setObjektadresseFreigeben(?bool $objektadresseFreigeben): self
    {
        $this->objektadresseFreigeben = $objektadresseFreigeben;
        return $this;
    }

    public function getVerfuegbarAb(): ?string
    {
        return $this->verfuegbarAb;
    }

    public function setVerfuegbarAb(?string $verfuegbarAb): self
    {
        $this->verfuegbarAb = $verfuegbarAb;
        return $this;
    }

    public function getAbdatum(): ?\DateTime
    {
        return $this->abdatum;
    }

    public function setAbdatum(?\DateTime $abdatum): self
    {
        $this->abdatum = $abdatum;
        return $this;
    }

    public function getBisdatum(): ?\DateTime
    {
        return $this->bisdatum;
    }

    public function setBisdatum(?\DateTime $bisdatum): self
    {
        $this->bisdatum = $bisdatum;
        return $this;
    }

    public function getMinMietdauer(): ?MinMietdauer
    {
        return $this->minMietdauer;
    }

    public function setMinMietdauer(?MinMietdauer $minMietdauer): self
    {
        $this->minMietdauer = $minMietdauer;
        return $this;
    }

    public function getMaxMietdauer(): ?MaxMietdauer
    {
        return $this->maxMietdauer;
    }

    public function setMaxMietdauer(?MaxMietdauer $maxMietdauer): self
    {
        $this->maxMietdauer = $maxMietdauer;
        return $this;
    }

    public function getVersteigerungstermin(): ?\DateTime
    {
        return $this->versteigerungstermin;
    }

    public function setVersteigerungstermin(?\DateTime $versteigerungstermin): self
    {
        $this->versteigerungstermin = $versteigerungstermin;
        return $this;
    }

    public function getWbsSozialwohnung(): ?bool
    {
        return $this->wbsSozialwohnung;
    }

    public function setWbsSozialwohnung(?bool $wbsSozialwohnung): self
    {
        $this->wbsSozialwohnung = $wbsSozialwohnung;
        return $this;
    }

    public function getVermietet(): ?bool
    {
        return $this->vermietet;
    }

    public function setVermietet(?bool $vermietet): self
    {
        $this->vermietet = $vermietet;
        return $this;
    }

    public function getGruppennummer(): ?string
    {
        return $this->gruppennummer;
    }

    public function setGruppennummer(?string $gruppennummer): self
    {
        $this->gruppennummer = $gruppennummer;
        return $this;
    }

    public function getZugang(): ?string
    {
        return $this->zugang;
    }

    public function setZugang(?string $zugang): self
    {
        $this->zugang = $zugang;
        return $this;
    }

    public function getLaufzeit(): ?float
    {
        return $this->laufzeit;
    }

    public function setLaufzeit(?float $laufzeit): self
    {
        $this->laufzeit = $laufzeit;
        return $this;
    }

    public function getMaxPersonen(): ?int
    {
        return $this->maxPersonen;
    }

    public function setMaxPersonen(?int $maxPersonen): self
    {
        $this->maxPersonen = $maxPersonen;
        return $this;
    }

    public function getNichtraucher(): ?bool
    {
        return $this->nichtraucher;
    }

    public function setNichtraucher(?bool $nichtraucher): self
    {
        $this->nichtraucher = $nichtraucher;
        return $this;
    }

    public function getHaustiere(): ?bool
    {
        return $this->haustiere;
    }

    public function setHaustiere(?bool $haustiere): self
    {
        $this->haustiere = $haustiere;
        return $this;
    }

    public function getGeschlecht(): ?Geschlecht
    {
        return $this->geschlecht;
    }

    public function setGeschlecht(?Geschlecht $geschlecht): self
    {
        $this->geschlecht = $geschlecht;
        return $this;
    }

    public function getDenkmalgeschuetzt(): ?bool
    {
        return $this->denkmalgeschuetzt;
    }

    public function setDenkmalgeschuetzt(?bool $denkmalgeschuetzt): self
    {
        $this->denkmalgeschuetzt = $denkmalgeschuetzt;
        return $this;
    }

    public function getAlsFerien(): ?bool
    {
        return $this->alsFerien;
    }

    public function setAlsFerien(?bool $alsFerien): self
    {
        $this->alsFerien = $alsFerien;
        return $this;
    }

    public function getGewerblicheNutzung(): ?bool
    {
        return $this->gewerblicheNutzung;
    }

    public function setGewerblicheNutzung(?bool $gewerblicheNutzung): self
    {
        $this->gewerblicheNutzung = $gewerblicheNutzung;
        return $this;
    }

    public function getBranchen(): ?string
    {
        return $this->branchen;
    }

    public function setBranchen(?string $branchen): self
    {
        $this->branchen = $branchen;
        return $this;
    }

    public function getHochhaus(): ?bool
    {
        return $this->hochhaus;
    }

    public function setHochhaus(?bool $hochhaus): self
    {
        $this->hochhaus = $hochhaus;
        return $this;
    }
}
