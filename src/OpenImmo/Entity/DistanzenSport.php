<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class DistanzenSport
{
    #[SerializedName('@distanz_zu_sport')]
    private ?string $distanzZuSport = null;

    #[SerializedName('#')]
    private ?float $value = null;

    public function getDistanzZuSport(): ?string
    {
        return $this->distanzZuSport;
    }

    public function setDistanzZuSport(?string $distanzZuSport): self
    {
        $this->distanzZuSport = $distanzZuSport;
        return $this;
    }

    public function getValue(): ?float
    {
        return $this->value;
    }

    public function setValue(?float $value): self
    {
        $this->value = $value;
        return $this;
    }
}
