<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class Unterkellert
{
    #[SerializedName('@keller')]
    private ?string $keller = null;

    public function getKeller(): ?string
    {
        return $this->keller;
    }

    public function setKeller(?string $keller): self
    {
        $this->keller = $keller;
        return $this;
    }
}
