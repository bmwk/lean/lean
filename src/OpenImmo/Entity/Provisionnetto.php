<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class Provisionnetto
{
    #[SerializedName('@provisionust')]
    private ?float $provisionust = null;

    #[SerializedName('#')]
    private ?float $value = null;

    public function getProvisionust(): ?float
    {
        return $this->provisionust;
    }

    public function setProvisionust(?float $provisionust): self
    {
        $this->provisionust = $provisionust;
        return $this;
    }

    public function getValue(): ?float
    {
        return $this->value;
    }

    public function setValue(?float $value): self
    {
        $this->value = $value;
        return $this;
    }
}
