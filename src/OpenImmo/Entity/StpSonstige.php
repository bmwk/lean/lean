<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class StpSonstige
{
    #[SerializedName('@platzart')]
    private ?string $platzart = null;

    #[SerializedName('@bemerkung')]
    private ?string $bemerkung = null;

    public function getPlatzart(): ?string
    {
        return $this->platzart;
    }

    public function setPlatzart(?string $platzart): self
    {
        $this->platzart = $platzart;
        return $this;
    }

    public function getBemerkung(): ?string
    {
        return $this->bemerkung;
    }

    public function setBemerkung(?string $bemerkung): self
    {
        $this->bemerkung = $bemerkung;
        return $this;
    }
}
