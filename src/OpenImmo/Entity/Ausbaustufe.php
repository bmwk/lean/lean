<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

class Ausbaustufe
{
    private ?bool $bausatzhaus = null;

    private ?bool $ausbauhaus = null;

    private ?bool $schluesselfertigmitkeller = null;

    private ?bool $schluesselfertigohnebodenplatte = null;

    private ?bool $schluesselfertigmitbodenplatte = null;

    public function getBausatzhaus(): ?bool
    {
        return $this->bausatzhaus;
    }

    public function setBausatzhaus(?bool $bausatzhaus): self
    {
        $this->bausatzhaus = $bausatzhaus;
        return $this;
    }

    public function getAusbauhaus(): ?bool
    {
        return $this->ausbauhaus;
    }

    public function setAusbauhaus(?bool $ausbauhaus): self
    {
        $this->ausbauhaus = $ausbauhaus;
        return $this;
    }

    public function getSchluesselfertigmitkeller(): ?bool
    {
        return $this->schluesselfertigmitkeller;
    }

    public function setSchluesselfertigmitkeller(?bool $schluesselfertigmitkeller): self
    {
        $this->schluesselfertigmitkeller = $schluesselfertigmitkeller;
        return $this;
    }

    public function getSchluesselfertigohnebodenplatte(): ?bool
    {
        return $this->schluesselfertigohnebodenplatte;
    }

    public function setSchluesselfertigohnebodenplatte(?bool $schluesselfertigohnebodenplatte): self
    {
        $this->schluesselfertigohnebodenplatte = $schluesselfertigohnebodenplatte;
        return $this;
    }

    public function getSchluesselfertigmitbodenplatte(): ?bool
    {
        return $this->schluesselfertigmitbodenplatte;
    }

    public function setSchluesselfertigmitbodenplatte(?bool $schluesselfertigmitbodenplatte): self
    {
        $this->schluesselfertigmitbodenplatte = $schluesselfertigmitbodenplatte;
        return $this;
    }
}
