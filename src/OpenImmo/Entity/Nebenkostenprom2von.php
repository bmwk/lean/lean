<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class Nebenkostenprom2von
{
    #[SerializedName('@nebenkostenprom2bis')]
    private ?float $nebenkostenprom2bis;

    #[SerializedName('#')]
    protected float $value;

    public function getNebenkostenprom2bis(): ?float
    {
        return $this->nebenkostenprom2bis;
    }

    public function setNebenkostenprom2bis(?float $nebenkostenprom2bis): self
    {
        $this->nebenkostenprom2bis = $nebenkostenprom2bis;
        return $this;
    }

    public function getValue(): ?float
    {
        return $this->value;
    }

    public function setValue(?float $value): self
    {
        $this->value = $value;
        return $this;
    }
}
