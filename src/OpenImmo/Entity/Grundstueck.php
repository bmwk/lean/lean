<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class Grundstueck
{
    #[SerializedName('@grundst_typ')]
    private ?string $grundstTyp = null;

    public function getGrundstTyp(): ?string
    {
        return $this->grundstTyp;
    }

    public function setGrundstTyp(?string $grundstTyp): self
    {
        $this->grundstTyp = $grundstTyp;
        return $this;
    }
}
