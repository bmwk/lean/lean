<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class Betriebskostennetto
{
    #[SerializedName('@betriebskostenust')]
    private ?float $betriebskostenust = null;


    #[SerializedName('#')]
    private ?float $value = null;

    public function getBetriebskostenust(): ?float
    {
        return $this->betriebskostenust;
    }

    public function setBetriebskostenust(?float $betriebskostenust): self
    {
        $this->betriebskostenust = $betriebskostenust;
        return $this;
    }

    public function getValue(): ?float
    {
        return $this->value;
    }

    public function setValue(?float $value): self
    {
        $this->value = $value;
        return $this;
    }
}
