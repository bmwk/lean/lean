<?php

declare(strict_types=1);

namespace App\OpenImmo\Entity;

use Symfony\Component\Serializer\Annotation\SerializedName;

class Kueche
{
    #[SerializedName('@EBK')]
    private ?bool $ebk = null;

    #[SerializedName('@OFFEN')]
    private ?bool $offen = null;

    #[SerializedName('@PANTRY')]
    private ?bool $pantry = null;

    public function getEbk(): ?bool
    {
        return $this->ebk;
    }

    public function setEbk(?bool $ebk): self
    {
        $this->ebk = $ebk;
        return $this;
    }

    public function getOffen(): ?bool
    {
        return $this->offen;
    }

    public function setOffen(?bool $offen): self
    {
        $this->offen = $offen;
        return $this;
    }

    public function getPantry(): ?bool
    {
        return $this->pantry;
    }

    public function setPantry(?bool $pantry): self
    {
        $this->pantry = $pantry;
        return $this;
    }
}
