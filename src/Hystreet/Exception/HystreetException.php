<?php

declare(strict_types=1);

namespace App\Hystreet\Exception;

class HystreetException extends \Exception
{
}
