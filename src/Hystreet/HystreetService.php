<?php

declare(strict_types=1);

namespace App\Hystreet;

use App\Hystreet\Exception\HystreetException;
use App\Hystreet\Response\Location;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use App\Hystreet\Response\LocationDetails;
use Symfony\Contracts\HttpClient\ResponseInterface;

class HystreetService
{
    public function __construct(
        private readonly HttpClientInterface $httpClient,
        private readonly string $hystreetApiUrl,
        private readonly string $hystreetContentType
    ) {
    }

    /**
     * @return Location[]
     */
    public function getLocations(string $apiAccessToken): array
    {
        $locationsResponse = $this->doGetRequest(apiAccessToken: $apiAccessToken, url: $this->hystreetApiUrl . '/locations');

        $locations = [];

        foreach (json_decode($locationsResponse->getContent()) as $location) {
            $locations[] = Location::createFromJsonObject($location);
        }

        return $locations;
    }

    public function getLocation(
        string $apiAccessToken,
        int $id,
        ?\DateTime $from = null,
        ?\DateTime $to = null,
        string $resolution = 'hour'
    ): LocationDetails {
        $queryParams = ['resolution=' . urlencode($resolution)];

        if ($to !== null && $from === null) {
            throw new HystreetException('require from date when to date is given');
        } elseif ($from > $to) {
            throw new HystreetException('to date is in the past');
        }

        if ($from !== null) {
            $queryParams[] = 'from=' . urlencode($from->format('Y-m-d H:i:sP'));
        }

        if ($to !== null) {
            $queryParams[] = 'to=' . urlencode($to->format('Y-m-d H:i:sP'));
        }

        $url = $this->hystreetApiUrl . '/locations/' . $id . ((count($queryParams) > 0) ? '?' . implode('&', $queryParams) : '');

        $response = $this->doGetRequest(apiAccessToken: $apiAccessToken, url: $url);

        return LocationDetails::createFromJsonObject(json_decode($response->getContent()));
    }

    private function doGetRequest(string $apiAccessToken, string $url): ResponseInterface
    {
        try {
            $response = $this->httpClient->request( 
                method: 'GET',
                url: $url,
                options: [
                    'headers' => [
                        'Content-Type: ' . $this->hystreetContentType,
                        'X-API-Token: ' . $apiAccessToken,
                    ],
                ]
            );

            if ($response->getStatusCode() !== 200) {
                throw new HystreetException('Http status code is ' . $response->getStatusCode() . ' for ' . $url);
            }
        } catch (TransportExceptionInterface $transportException) {
            throw new HystreetException('Transport exception ' . $transportException->getMessage());
        }

        return $response;
    }
}
