<?php

declare(strict_types=1);

namespace App\Hystreet\Response\Location;

class Statistics
{
    public int $todayCount;

    public static function createFromJsonObject(object $jsonObject): self
    {
        $statistics = new self();

        $statistics->todayCount = $jsonObject->today_count;

        return $statistics;
    }

    public function getTodayCount(): int
    {
        return $this->todayCount;
    }
}
