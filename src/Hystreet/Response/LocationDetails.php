<?php

declare(strict_types=1);

namespace App\Hystreet\Response;

use App\Hystreet\Response\LocationDetails\Incident;
use App\Hystreet\Response\LocationDetails\Measurement;
use App\Hystreet\Response\LocationDetails\Metadata;
use App\Hystreet\Response\LocationDetails\Statistics;

class LocationDetails
{
    private int $id;

    private string $name;

    private string $city;

    private ?int $population = null;

    private ?string $state = null;

    private Statistics $statistics;

    private Metadata $metadata;

    /**
     * @var Measurement[]
     */
    private array $measurements;

    /**
     * @var Incident[]
     */
    private array $incidents;

    public static function createFromJsonObject(object $jsonObject): self
    {
        $locationDetail = new self();

        $locationDetail->id = $jsonObject->id;
        $locationDetail->name = $jsonObject->name;
        $locationDetail->city = $jsonObject->city;

        if (isset($jsonObject->populatio)) {
            $locationDetail->population = $jsonObject->population;
        }

        if (isset($jsonObject->state)) {
            $locationDetail->state = $jsonObject->state;
        }

        $locationDetail->statistics = Statistics::createFromJsonObject($jsonObject->statistics);
        $locationDetail->metadata = Metadata::createFromJsonObject($jsonObject->metadata);

        foreach ($jsonObject->measurements as $measurement) {
            $locationDetail->measurements[] = Measurement::createFromJsonObject($measurement);
        }

        foreach ($jsonObject->incidents as $incident) {
            $locationDetail->incidents[] = Incident::createFromJsonObject($incident);
        }

        return $locationDetail;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function getPopulation(): ?int
    {
        return $this->population;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function getStatistics(): Statistics
    {
        return $this->statistics;
    }

    public function getMetadata(): Metadata
    {
        return $this->metadata;
    }

    public function getMeasurements(): array
    {
        return $this->measurements;
    }

    public function getIncidents(): array
    {
        return $this->incidents;
    }
}
