<?php

declare(strict_types=1);

namespace App\Hystreet\Response\LocationDetails;

use App\Hystreet\Response\LocationDetails\Measurement\Detail;

class Measurement
{
    private \DateTime $timestamp;

    private ?string $weatherCondition = null;

    private ?float $temperature = null;

    private ?float $minTemperature = null;

    private int $pedestriansCount;

    private bool $unverified;

    private ?Detail $detail = null;

    public static function createFromJsonObject(object $jsonObject): self
    {
        $measurement = new self();

        $measurement->timestamp = new \DateTime($jsonObject->timestamp);
        $measurement->weatherCondition = $jsonObject->weather_condition;
        $measurement->temperature = $jsonObject->temperature;
        $measurement->minTemperature = $jsonObject->min_temperature;
        $measurement->pedestriansCount = $jsonObject->pedestrians_count;
        $measurement->unverified = $jsonObject->unverified;

        if (isset($jsonObject->details)) {
            $measurement->detail = Detail::createFromJsonObject($jsonObject->details);
        }

        return $measurement;
    }

    public function getTimestamp(): \DateTime
    {
        return $this->timestamp;
    }

    public function getWeatherCondition(): ?string
    {
        return $this->weatherCondition;
    }

    public function getTemperature(): ?float
    {
        return $this->temperature;
    }

    public function getMinTemperature(): ?float
    {
        return $this->minTemperature;
    }

    public function getPedestriansCount(): int
    {
        return $this->pedestriansCount;
    }

    public function isUnverified(): bool
    {
        return $this->unverified;
    }

    public function getDetail(): ?Detail
    {
        return $this->detail;
    }
}
