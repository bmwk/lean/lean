<?php

declare(strict_types=1);

namespace App\Hystreet\Response\LocationDetails;

class Metadata
{
    private \DateTime $measuredFrom;

    private \DateTime $measuredTo;

    private \DateTime $earliestMeasurementAt;

    private \DateTime $latestMeasurementAt;

    private ?string $ltrLabel = null;

    private ?string $rtlLabel = null;

    private array $zones;

    private string $weatherCondition;

    private float $temperature;

    private float $minTemperature;

    public static function createFromJsonObject(object $jsonObject): self
    {
        $metadata = new self();

        $metadata->measuredFrom = new \DateTime($jsonObject->measured_from);
        $metadata->measuredTo = new \DateTime($jsonObject->measured_to);
        $metadata->earliestMeasurementAt = new \DateTime($jsonObject->earliest_measurement_at);
        $metadata->latestMeasurementAt = new \DateTime($jsonObject->latest_measurement_at);
        $metadata->ltrLabel = $jsonObject->ltr_label;
        $metadata->rtlLabel = $jsonObject->rtl_label;
        $metadata->zones = $jsonObject->zones;

        if (isset($jsonObject->weather_condition)) {
            $metadata->weatherCondition = $jsonObject->weather_condition;
        }

        if (isset($jsonObject->temperature)) {
            $metadata->temperature = $jsonObject->temperature;
        }

        if (isset($jsonObject->min_temperature)) {
            $metadata->minTemperature = $jsonObject->min_temperature;
        }

        return $metadata;
    }

    public function getMeasuredFrom(): \DateTime
    {
        return $this->measuredFrom;
    }

    public function getMeasuredTo(): \DateTime
    {
        return $this->measuredTo;
    }

    public function getEarliestMeasurementAt(): \DateTime
    {
        return $this->earliestMeasurementAt;
    }

    public function getLatestMeasurementAt(): \DateTime
    {
        return $this->latestMeasurementAt;
    }

    public function getLtrLabel(): ?string
    {
        return $this->ltrLabel;
    }

    public function getRtlLabels(): ?string
    {
        return $this->rtlLabel;
    }

    public function getZones(): array
    {
        return $this->zones;
    }

    public function getWeatherCondition(): string
    {
        return $this->weatherCondition;
    }

    public function getTemperature(): float
    {
        return $this->temperature;
    }

    public function getMinTemperature(): float
    {
        return $this->minTemperature;
    }
}
