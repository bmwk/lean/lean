<?php

declare(strict_types=1);

namespace App\Hystreet\Response\LocationDetails;

class Statistics
{
    public int $todayCount;

    public int $timerangeCount;

    public static function createFromJsonObject(object $jsonObject): self
    {
        $statistics = new self();

        $statistics->todayCount = $jsonObject->today_count;
        $statistics->timerangeCount = $jsonObject->timerange_count;

        return $statistics;
    }

    public function getTodayCount(): int
    {
        return $this->todayCount;
    }

    public function getTimerangeCount(): int
    {
        return $this->timerangeCount;
    }
}
