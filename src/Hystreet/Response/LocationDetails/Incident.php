<?php

declare(strict_types=1);

namespace App\Hystreet\Response\LocationDetails;

class Incident
{
    private int $id;

    private string $name;

    private ?string $icon = null;

    private string $description;

    private \DateTime $activeFrom;

    private \DateTime $activeTo;

    public static function createFromJsonObject(object $jsonObject): self
    {
        $incident = new self();

        $incident->id = $jsonObject->id;
        $incident->name = $jsonObject->name;

        if (isset($jsonObject->icon)) {
            $incident->icon = $jsonObject->icon;
        }

        $incident->description = $jsonObject->description;
        $incident->activeFrom = new \DateTime($jsonObject->active_from);
        $incident->activeTo = new \DateTime($jsonObject->active_to);

        return $incident;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getIcon(): ?string
    {
        return $this->icon;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getActiveFrom(): \DateTime
    {
        return $this->activeFrom;
    }

    public function getActiveTo(): \DateTime
    {
        return $this->activeTo;
    }
}
