<?php

declare(strict_types=1);

namespace App\Hystreet\Response\LocationDetails\Measurement\Detail;

class Zone
{
    private int $id;

    private ?int $pedestriansCount = null;

    private ?int $ltrPedestriansCount = null;

    private ?int $rtlPedestriansCount = null;

    public static function createFromJsonObject(object $jsonObject): self
    {
        $zone = new self();

        $zone->id = $jsonObject->id;

        if (isset( $jsonObject->pedestrians_count)) {
            $zone->pedestriansCount = $jsonObject->pedestrians_count;
        }

        if (isset( $jsonObject->ltr_pedestrains_count)) {
            $zone->ltrPedestriansCount = $jsonObject->ltr_pedestrains_count;
        }

        if (isset( $jsonObject->rtl_pedestrains_count)) {
            $zone->rtlPedestriansCount = $jsonObject->rtl_pedestrains_count;
        }

        return $zone;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getPedestriansCount(): ?int
    {
        return $this->pedestriansCount;
    }

    public function getLtrPedestriansCount(): ?int
    {
        return $this->ltrPedestriansCount;
    }

    public function getRtlPedestriansCount(): ?int
    {
        return $this->rtlPedestriansCount;
    }
}
