<?php

declare(strict_types=1);

namespace App\Hystreet\Response\LocationDetails\Measurement;

use App\Hystreet\Response\LocationDetails\Measurement\Detail\Zone;

class Detail
{
    private ?int $ltrPedestriansCount = null;

    private ?int $rtlPedestriansCount = null;

    private ?int $adultPedestriansCount = null;

    private ?int $childPedestriansCount = null;

    private ?int $adultLtrPedestriansCount = null;

    private ?int $adultRtlPedestriansCount = null;

    private ?int $childLtrPedestriansCount = null;

    private ?int $childRtlPedestriansCount = null;

    /**
     * @var Zone[]
     */
    private ?array $zones = null;

    public static function createFromJsonObject(object $jsonObject): self
    {
        $detail = new self();

        if (isset($jsonObject->ltr_pedestrians_count)) {
            $detail->ltrPedestriansCount = $jsonObject->ltr_pedestrians_count;
        }

        if (isset($jsonObject->rtl_pedestrians_count)) {
            $detail->rtlPedestriansCount = $jsonObject->rtl_pedestrians_count;
        }

        if (isset($jsonObject->adult_pedestrians_count)) {
            $detail->adultPedestriansCount = $jsonObject->adult_pedestrians_count;
        }

        if (isset($jsonObject->child_pedestrians_count)) {
            $detail->childPedestriansCount = $jsonObject->child_pedestrians_count;
        }

        if (isset($jsonObject->adult_ltr_pedestrians_count)) {
            $detail->adultLtrPedestriansCount = $jsonObject->adult_ltr_pedestrians_count;
        }

        if (isset($jsonObject->adult_rtl_pedestrians_count)) {
            $detail->adultRtlPedestriansCount = $jsonObject->adult_rtl_pedestrians_count;
        }

        if (isset($jsonObject->child_ltr_pedestrians_count)) {
            $detail->childLtrPedestriansCount = $jsonObject->child_ltr_pedestrians_count;
        }

        if (isset($jsonObject->child_rtl_pedestrians_count)) {
            $detail->childRtlPedestriansCount = $jsonObject->child_rtl_pedestrians_count;
        }

        if (isset($jsonObject->zones)) {
            $detail->zones = [];

            foreach ($jsonObject->zones as $zone) {
                $detail->zones[] = Zone::createFromJsonObject($zone);
            }
        }

        return $detail;
    }

    public function getLtrPedestriansCount(): ?int
    {
        return $this->ltrPedestriansCount;
    }

    public function getRtlPedestriansCount(): ?int
    {
        return $this->rtlPedestriansCount;
    }

    public function getAdultPedestriansCount(): ?int
    {
        return $this->adultPedestriansCount;
    }

    public function getChildPedestriansCount(): ?int
    {
        return $this->childPedestriansCount;
    }

    public function getAdultLtrPedestriansCount(): ?int
    {
        return $this->adultLtrPedestriansCount;
    }

    public function getAdultRtlPedestriansCount(): ?int
    {
        return $this->adultRtlPedestriansCount;
    }

    public function getChildLtrPedestriansCount(): ?int
    {
        return $this->childLtrPedestriansCount;
    }

    public function getChildRtlPedestriansCount(): ?int
    {
        return $this->childRtlPedestriansCount;
    }

    public function getZones(): ?array
    {
        return $this->zones;
    }
}
