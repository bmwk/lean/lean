<?php

declare(strict_types=1);

namespace App\Hystreet\Response;

use App\Hystreet\Response\Location\Statistics;

class Location
{
    private int $id;

    private string $name;

    private string $city;

    private Statistics $statistics;

    public static function createFromJsonObject(object $jsonObject): self
    {
        $location = new self();

        $location->id = $jsonObject->id;
        $location->name = $jsonObject->name;
        $location->city = $jsonObject->city;
        $location->statistics = Statistics::createFromJsonObject($jsonObject->statistics);

        return $location;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function getStatistics(): Statistics
    {
        return $this->statistics;
    }
}
