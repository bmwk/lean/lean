<?php

declare(strict_types=1);

namespace App\DoctrineExtensions\Query\MySql;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\AST\Node;
use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\SqlWalker;
use Doctrine\ORM\Query\Parser;

class JsonContains extends FunctionNode
{
    private Node $target;
    private Node $candidate;
    private Node $path;

    public function parse(Parser $parser): void
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);

        $this->target = $parser->StringPrimary();

        $parser->match(Lexer::T_COMMA);

        $this->candidate = $parser->StringPrimary();

        $parser->match(Lexer::T_COMMA);
        $this->path = $parser->StringPrimary();

        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }

    public function getSql(SqlWalker $sqlWalker): string
    {
        return 'JSON_CONTAINS(' . $this->target->dispatch($sqlWalker) . ', ' . $this->candidate->dispatch($sqlWalker) . ', ' . $this->path->dispatch($sqlWalker) . ')';
    }
}
