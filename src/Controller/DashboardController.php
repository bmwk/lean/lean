<?php

declare(strict_types=1);

namespace App\Controller;

use App\Domain\Entity\AccountUser\AccountUserRole;
use App\Domain\InterfaceConfiguration\InterfaceConfigurationService;
use App\Domain\LocationIndicators\LocationIndicatorsService;
use App\Domain\LookingForPropertyRequest\LookingForPropertyRequestService;
use App\Domain\LookingForPropertyRequestReport\LookingForPropertyRequestReportService;
use App\Domain\Person\OccurrenceSecurityService;
use App\Domain\Person\PersonService;
use App\Domain\Property\BuildingUnitService;
use App\Domain\Property\PropertyService;
use App\Domain\PropertyVacancyReport\PropertyVacancyReportService;
use App\Domain\Report\SettlementInformationStatistics\SettlementInformationStatisticsService;
use App\HallOfInspiration\HallOfInspirationService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[IsGranted('IS_AUTHENTICATED')]
class DashboardController extends AbstractController
{
    private const ACTIVE_NAV_ITEM = 'dashboard';

    public function __construct(
        private readonly PropertyService $propertyService,
        private readonly BuildingUnitService $buildingUnitService,
        private readonly PropertyVacancyReportService $propertyVacancyReportService,
        private readonly PersonService $personService,
        private readonly OccurrenceSecurityService $occurrenceSecurityService,
        private readonly LookingForPropertyRequestService $lookingForPropertyRequestService,
        private readonly LocationIndicatorsService $locationIndicatorsService,
        private readonly LookingForPropertyRequestReportService $lookingForPropertyRequestReportService,
        private readonly SettlementInformationStatisticsService $settlementInformationStatisticsService,
        private readonly InterfaceConfigurationService $interfaceConfigurationService,
        private readonly HallOfInspirationService $hallOfInspirationService,
    ) {
    }

    #[Route('/', name: 'dashboard', methods: ['GET'])]
    public function dashboard(UserInterface $user): Response
    {
        $account = $user->getAccount();

        $templateParameters = [
            'pageTitle'     => 'Leerstands- und Ansiedlungsmanagement',
            'activeNavItem' => self::ACTIVE_NAV_ITEM,
        ];

        if (
            $this->isGranted(attribute: AccountUserRole::ROLE_USER->value) === true
            || $this->isGranted(attribute: AccountUserRole::ROLE_PROPERTY_VACANCY_REPORT_MANAGER->value) === true
            || $this->isGranted(attribute: AccountUserRole::ROLE_VIEWER->value) === true
            || $this->isGranted(attribute: AccountUserRole::ROLE_RESTRICTED_VIEWER->value) === true
        ) {
            $templateParameters['propertyVacancyReportStatistics'] = $this->propertyVacancyReportService->fetchPropertyVacancyReportStatisticsByAccount(
                account: $account
            );
        }

        if (
            $this->isGranted(attribute: AccountUserRole::ROLE_USER->value) === true
            || $this->isGranted(attribute: AccountUserRole::ROLE_PROPERTY_FEEDER->value) === true
            || $this->isGranted(attribute: AccountUserRole::ROLE_VIEWER->value) === true
            || $this->isGranted(attribute: AccountUserRole::ROLE_RESTRICTED_VIEWER->value) === true
            || $this->isGranted(attribute: AccountUserRole::ROLE_PROPERTY_PROVIDER->value) === true
        ) {
            $propertyVacancyManagementStatistics = $this->propertyService->fetchPropertyVacancyManagementStatisticsByAccount(
                account: $account,
                archived: false
            );

            if (
                $this->isGranted(attribute: AccountUserRole::ROLE_USER->value) === true
                || $this->isGranted(attribute: AccountUserRole::ROLE_PROPERTY_FEEDER->value) === true
            ) {
                $propertyVacancyManagementStatistics->setAssignedToCurrentUser(
                    $this->buildingUnitService->countBuildingUnitsByAssignedAccountUser(account: $account, accountUser: $user, archived: false)
                );

                $propertyVacancyManagementStatistics->setCreatedByCurrentUser(
                    $this->buildingUnitService->countBuildingUnitsByCreatedByAccountUser(account: $account, accountUser: $user, archived: false)
                );
            }

            $templateParameters['propertyVacancyManagementStatistics'] = $propertyVacancyManagementStatistics;
        }

        if (
            $this->isGranted(attribute: AccountUserRole::ROLE_USER->value) === true
            || $this->isGranted(attribute: AccountUserRole::ROLE_LOOKING_FOR_PROPERTY_REQUEST_REPORT_MANAGER->value) === true
            || $this->isGranted(attribute: AccountUserRole::ROLE_LOOKING_FOR_PROPERTY_REQUEST_FEEDER->value) === true
            || $this->isGranted(attribute: AccountUserRole::ROLE_VIEWER->value) === true
            || $this->isGranted(attribute: AccountUserRole::ROLE_RESTRICTED_VIEWER->value) === true
        ) {
            $templateParameters['lookingForPropertyRequestReportStatistics'] = $this->lookingForPropertyRequestReportService->fetchLookingForPropertyRequestReportStatisticsByAccount(
                account: $account
            );
        }

        if (
            $this->isGranted(attribute: AccountUserRole::ROLE_USER->value) === true
            || $this->isGranted(attribute: AccountUserRole::ROLE_LOOKING_FOR_PROPERTY_REQUEST_FEEDER->value) === true
            || $this->isGranted(attribute: AccountUserRole::ROLE_VIEWER->value) === true
            || $this->isGranted(attribute: AccountUserRole::ROLE_RESTRICTED_VIEWER->value) === true
        ) {
            $lookingForPropertyRequestStatistics = $this->lookingForPropertyRequestService->buildLookingForPropertyRequestsStatisticsByAccount(
                account: $account
            );

            if (
                $this->isGranted(attribute: AccountUserRole::ROLE_USER->value) === true
                || $this->isGranted(attribute: AccountUserRole::ROLE_LOOKING_FOR_PROPERTY_REQUEST_FEEDER->value) === true
            ) {
                $lookingForPropertyRequestStatistics->setAssignedToCurrentUser(
                    $this->lookingForPropertyRequestService->countLookingForPropertyRequestsByAssignedAccountUser(
                        account: $account,
                        accountUser: $user
                    )
                );

                $lookingForPropertyRequestStatistics->setCreatedByCurrentUser(
                    $this->lookingForPropertyRequestService->countLookingForPropertyRequestsByCreatedByAccountUser(
                        account: $account,
                        accountUser: $user
                    )
                );
            }

            $templateParameters['lookingForPropertyRequestStatistics'] = $lookingForPropertyRequestStatistics;
        }

        if (
            $this->isGranted(attribute: AccountUserRole::ROLE_USER->value) === true
            || $this->isGranted(attribute: AccountUserRole::ROLE_VIEWER->value) === true
            || $this->isGranted(attribute: AccountUserRole::ROLE_RESTRICTED_VIEWER->value) === true
        ) {
            $templateParameters['matchingStatistics'] = $this->settlementInformationStatisticsService->buildMatchingStatisticsByAccount(
                account: $account
            );
        }

        if (
            $this->interfaceConfigurationService->hasHallOfInspirationConfiguration(account: $user->getAccount()) === true
            && (
                $this->isGranted(attribute: AccountUserRole::ROLE_USER->value) === true
                || $this->isGranted(attribute: AccountUserRole::ROLE_LOOKING_FOR_PROPERTY_REQUEST_REPORT_MANAGER->value) === true
                || $this->isGranted(attribute: AccountUserRole::ROLE_VIEWER->value) === true
                || $this->isGranted(attribute: AccountUserRole::ROLE_RESTRICTED_VIEWER->value) === true
            )
        ) {
            $hallOfInspirationConfiguration = $this->interfaceConfigurationService->fetchHallOfInspirationConfigurationByAccount(account: $user->getAccount());

            if ($hallOfInspirationConfiguration !== null && empty($hallOfInspirationConfiguration->getAccessToken()) === false) {
                $templateParameters['hallOfInspirationConcept'] = $this->hallOfInspirationService->getConceptRandomized(apiAccessToken: $hallOfInspirationConfiguration->getAccessToken());
            } else {
                $templateParameters['hallOfInspirationConcept'] = null;
            }
        }

        if (
            $this->isGranted(attribute: AccountUserRole::ROLE_USER->value) === true
            || $this->isGranted(attribute: AccountUserRole::ROLE_VIEWER->value) === true
            || $this->isGranted(attribute: AccountUserRole::ROLE_PROPERTY_FEEDER->value) === true
            || $this->isGranted(attribute: AccountUserRole::ROLE_LOOKING_FOR_PROPERTY_REQUEST_FEEDER->value) === true
        ) {
            if ($this->occurrenceSecurityService->canViewOnlyOwnOccurrences() === true) {
                $performedWithAccountUser = $user;
            } else {
                $performedWithAccountUser = null;
            }

            $templateParameters['occurrences'] = $this->personService->fetchOccurrencesWithUndoneFollowUpsByAccount(
                account: $account,
                withFromSubAccounts: false,
                performedWithAccountUser: $performedWithAccountUser,
                followUpAt: new \DateTime()
            );
        }

        if ($this->isGranted(attribute: AccountUserRole::ROLE_PROPERTY_SEEKER->value) === true) {
            $templateParameters['lookingForPropertyRequestsStatistics'] = $this->lookingForPropertyRequestService->fetchLookingForPropertyRequestsStatisticsByPerson(
                account: $account,
                person: $user->getPerson()
            );
        }

        if (
            $this->isGranted(attribute: AccountUserRole::ROLE_USER->value) === true
            || $this->isGranted(attribute: AccountUserRole::ROLE_VIEWER->value) === true
            || $this->isGranted(attribute: AccountUserRole::ROLE_RESTRICTED_VIEWER->value) === true
        ) {
            $templateParameters['wegweiserKommunePlaceMapping'] = $this->locationIndicatorsService->fetchWegweiserKommunePlaceMappingByPlace(
                account: $account,
                place: $account->getAssignedPlace()
            );
        }

        return $this->render(view: 'dashboard/dashboard.html.twig', parameters: $templateParameters);
    }
}
