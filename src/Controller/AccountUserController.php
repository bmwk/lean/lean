<?php

declare(strict_types=1);

namespace App\Controller;

use App\Domain\Account\AccountDeletionService;
use App\Domain\Account\AccountUserDeletionService;
use App\Domain\Account\AccountUserService;
use App\Domain\Entity\AccountUser\AccountUserRole;
use App\Domain\Entity\DeletionType;
use App\Domain\LookingForPropertyRequest\LookingForPropertyRequestDeletionService;
use App\Domain\LookingForPropertyRequest\LookingForPropertyRequestService;
use App\Form\Type\AccountUser\BaseDataType;
use App\Form\Type\AccountUser\DeleteType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[IsGranted('IS_AUTHENTICATED')]
#[Route('/benutzer', name: 'account_user_')]
class AccountUserController extends AbstractController
{
    private const ACTIVE_NAV_GROUP = 'accountUser';

    public function __construct(
        private readonly AccountDeletionService $accountDeletionService,
        private readonly AccountUserDeletionService $accountUserDeletionService,
        private readonly AccountUserService $accountUserService,
        private readonly LookingForPropertyRequestDeletionService $lookingForPropertyRequestDeletionService,
        private readonly LookingForPropertyRequestService $lookingForPropertyRequestService,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    #[Route('/stammdaten-verwaltung', name: 'base_data', methods: ['GET', 'POST'])]
    public function baseData(Request $request, UserInterface $user): Response
    {
        $this->denyAccessUnlessGranted(attribute: 'view', subject: $user);

        $currentUserIdentifier = $user->getUserIdentifier();

        $baseDataForm = $this->createForm(type: BaseDataType::class, data: $user);

        $baseDataForm->add(child: 'save', type: SubmitType::class);

        $baseDataForm->handleRequest(request: $request);

        $formErrors = [];

        if (count($baseDataForm->get('password')->getErrors(deep: true)) > 0) {
            foreach ($baseDataForm->get('password')->getErrors(deep: true, flatten: true) as $formError) {
                $formErrors[] = $formError->getMessage();
            }
        }

        if ($baseDataForm->isSubmitted() && $baseDataForm->isValid()) {
            $this->denyAccessUnlessGranted(attribute: 'edit', subject: $user);

            if (
                $user->getUserIdentifier() !== $currentUserIdentifier
                && $this->accountUserService->isUsernameExistentInDatabase(userIdentifier: $user->getUserIdentifier()) === true
            ) {
                $user->setUsername($currentUserIdentifier);

                $formErrors[] = 'Benutzername ist ungültig';
            }

            $currentPassword = $baseDataForm->get('currentPassword')->getData();
            $plainPassword = $baseDataForm->get('password')->getData();

            if ($plainPassword !== null) {
                if ($this->accountUserService->isPasswordEqual(accountUser: $user, plainPassword: $currentPassword) !== true) {
                    $formErrors[] = 'Das bisherige Kennwort ist falsch.';
                }

                if ($this->accountUserService->isThePasswordSecure(plainPassword: $plainPassword) === false) {
                    $formErrors[] = 'Das Kennwort muss mindestens 8 Zeichen lang sein, mindestens je einen Großbuchstaben, einen Kleinbuchstaben und eine Zahl enthalten. Idealerweise auch ein Sonderzeichen.';
                }
            }

            if (empty($formErrors) === true) {
                if ($plainPassword !== null) {
                    $hashedPassword = $this->accountUserService->hashPassword(accountUser: $user, plainPassword: $plainPassword);

                    $user->setPassword($hashedPassword);
                }

                $this->entityManager->flush();

                $this->addFlash(type: 'dataSaved', message: 'Die Daten wurden gespeichert.');

                return $this->redirectToRoute(route: 'account_user_base_data');
            }
        }

        if (empty($formErrors) === false) {
            $this->addFlash(type: 'submitResponse', message: [
                'success' => false,
                'message' => 'Daten konnten nicht gespeichert werden',
                'errors'  => $formErrors,
                'data'    => [],
            ]);
        }

        return $this->render(view: 'account_user/base_data.html.twig', parameters: [
            'baseDataForm'   => $baseDataForm,
            'submitResponse' => $request->getSession()->getFlashBag()->get(type: 'submitResponse'),
            'pageTitle'      => 'Stammdaten verwalten',
            'activeNavGroup' => self::ACTIVE_NAV_GROUP,
        ]);
    }

    #[Security("is_granted('ROLE_PROPERTY_PROVIDER') or is_granted('ROLE_PROPERTY_SEEKER')")]
    #[Route('/loeschen', name: 'delete', methods: ['GET', 'POST'])]
    public function delete(Request $request, UserInterface $user): Response
    {
        $this->denyAccessUnlessGranted(attribute: 'delete', subject: $user);

        $account = $user->getAccount();

        $deleteForm = $this->createForm(type: DeleteType::class);

        $deleteForm->add(child: 'delete', type: SubmitType::class);

        $deleteForm->handleRequest(request: $request);

        if ($deleteForm->isSubmitted() && $deleteForm->isValid()) {
            if ($this->accountUserService->isPasswordEqual(accountUser: $user, plainPassword: $deleteForm->get('currentPasswordForDeletion')->getData()) === false) {
                $this->addFlash(type: 'dataError', message: 'Das angegebene Kennwort ist falsch.');

                return $this->redirectToRoute(route: 'account_user_delete');
            }

            $this->entityManager->beginTransaction();

            $lookingForPropertyRequests = $this->lookingForPropertyRequestService->fetchLookingForPropertyRequestsByPerson(
                account: $account,
                withFromSubAccounts: false,
                person: $user->getPerson(),
                archived: false
            );

            foreach ($lookingForPropertyRequests as $lookingForPropertyRequest) {
                $this->lookingForPropertyRequestDeletionService->deleteLookingForPropertyRequest(
                    lookingForPropertyRequest: $lookingForPropertyRequest,
                    deletionType: DeletionType::SOFT,
                    deletedByAccountUser: $user
                );
            }

            $lookingForPropertyRequests = $this->lookingForPropertyRequestService->fetchLookingForPropertyRequestsByPerson(
                account: $account,
                withFromSubAccounts: false,
                person: $user->getPerson(),
                archived: true
            );

            foreach ($lookingForPropertyRequests as $lookingForPropertyRequest) {
                $this->lookingForPropertyRequestDeletionService->deleteLookingForPropertyRequest(
                    lookingForPropertyRequest: $lookingForPropertyRequest,
                    deletionType: DeletionType::SOFT,
                    deletedByAccountUser: $user
                );
            }

            if (
                $this->isGranted(attribute: AccountUserRole::ROLE_PROPERTY_PROVIDER->value) === true
                && $account->getParentAccount() !== null
            ) {
                $accountUsers = $this->accountUserService->fetchAccountUsersByAccount(account: $account, withFromSubAccounts: false);

                foreach ($accountUsers as $accountUser) {
                    $this->accountUserDeletionService->deleteAccountUser(
                        accountUser: $accountUser,
                        deletionType: DeletionType::ANONYMIZATION,
                        deletedByAccountUser: $user
                    );
                }

                $this->accountDeletionService->deleteAccount(account: $account, deletionType: DeletionType::SOFT, deletedByAccountUser: $user);
            }

            $this->accountUserDeletionService->deleteAccountUser(accountUser: $user, deletionType: DeletionType::ANONYMIZATION, deletedByAccountUser: $user);

            $this->entityManager->commit();

            return $this->redirectToRoute(route: 'logout');
        }

        return $this->render(view: 'account_user/delete.html.twig', parameters: [
            'baseDataForm'   => $this->createForm(type: BaseDataType::class, data: $user),
            'deleteForm'     => $deleteForm,
            'pageTitle'      => 'Account löschen',
            'activeNavGroup' => self::ACTIVE_NAV_GROUP,
        ]);
    }
}
