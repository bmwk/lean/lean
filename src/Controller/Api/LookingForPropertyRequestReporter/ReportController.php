<?php

declare(strict_types=1);

namespace App\Controller\Api\LookingForPropertyRequestReporter;

use App\Domain\LookingForPropertyRequestReport\LookingForPropertyRequestReportService;
use App\Domain\LookingForPropertyRequestReporter\Api\Entity\Request\ReportCreateRequest;
use App\Domain\LookingForPropertyRequestReporter\Api\Entity\Response\ReportCreateResponse;
use App\Domain\LookingForPropertyRequestReporter\LookingForPropertyRequestReporterConfigurationService;
use App\Domain\LookingForPropertyRequestReporter\LookingForPropertyRequestReporterEmailService;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[IsGranted('PUBLIC_ACCESS')]
#[Route('/api/looking-for-property-request-reporter', name: 'api_looking_for_property_request_reporter_')]
class ReportController extends AbstractController
{
    public function __construct(
        private readonly LookingForPropertyRequestReportService $lookingForPropertyRequestReportService,
        private readonly LookingForPropertyRequestReporterConfigurationService $lookingForPropertyRequestReporterConfigurationService,
        private readonly LookingForPropertyRequestReporterEmailService $lookingForPropertyRequestReporterEmailService,
        private readonly ValidatorInterface $validator,
        private readonly SerializerInterface $serializer,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    #[Route('/reports',  name: 'options_reports', methods: ['OPTIONS'], format: 'json')]
    public function optionsReports(): JsonResponse
    {
        return new JsonResponse(
            data: null,
            status: Response::HTTP_NO_CONTENT,
            headers: [
                'Allow' => 'POST,OPTIONS',
                'Access-Control-Allow-Methods' => join(separator: ',', array: [Request::METHOD_POST, Request::METHOD_OPTIONS]),
                'Access-Control-Allow-Headers' => 'Content-Type',
                'Access-Control-Allow-Origin'  => '*',
            ]
        );
    }

    #[Route('/reports',  name: 'report_create', methods: ['POST'], format: 'json')]
    public function reportCreate(Request $request): JsonResponse
    {
        $jsonContent = json_decode(json: $request->getContent(), associative: false);

        if ($jsonContent === null) {
            throw new BadRequestException();
        }

        $reportCreateRequest = ReportCreateRequest::createFormApiRequestJsonContent(jsonContent: $jsonContent);

        $errors = $this->validator->validate(value: $reportCreateRequest);

        if (empty($errors->count()) === false) {
            throw new BadRequestException();
        }

        $lookingForPropertyRequestReport = $this->lookingForPropertyRequestReportService->createLookingForPropertyRequestReportFromReportCreateRequest(
            reportCreateRequest: $reportCreateRequest
        );

        $lookingForPropertyRequestReporterConfiguration = $this->lookingForPropertyRequestReporterConfigurationService
            ->fetchLookingForPropertyRequestReporterConfigurationByAccountKey(accountKey: $reportCreateRequest->getAccountKey());

        if ($lookingForPropertyRequestReporterConfiguration === null) {
            throw new BadRequestException();
        }

        $account = $lookingForPropertyRequestReporterConfiguration->getAccount();

        $lookingForPropertyRequestReport->setAccount($account);

        $this->entityManager->beginTransaction();

        $this->entityManager->persist(entity: $lookingForPropertyRequestReport->getPropertyRequirement());
        $this->entityManager->persist(entity: $lookingForPropertyRequestReport->getPropertyRequirement()->getPropertyMandatoryRequirement());
        $this->entityManager->persist(entity: $lookingForPropertyRequestReport->getPropertyRequirement()->getPropertyValueRangeRequirement());
        $this->entityManager->persist(entity: $lookingForPropertyRequestReport->getPropertyRequirement()->getPropertyValueRangeRequirement()->getTotalSpace());
        $this->entityManager->persist(entity: $lookingForPropertyRequestReport->getPropertyRequirement()->getPropertyValueRangeRequirement()->getRetailSpace());
        $this->entityManager->persist(entity: $lookingForPropertyRequestReport->getPropertyRequirement()->getPropertyValueRangeRequirement()->getOutdoorSpace());
        $this->entityManager->persist(entity: $lookingForPropertyRequestReport->getPropertyRequirement()->getPropertyValueRangeRequirement()->getSubsidiarySpace());
        $this->entityManager->persist(entity: $lookingForPropertyRequestReport->getPropertyRequirement()->getPropertyValueRangeRequirement()->getUsableSpace());
        $this->entityManager->persist(entity: $lookingForPropertyRequestReport->getPropertyRequirement()->getPropertyValueRangeRequirement()->getStorageSpace());
        $this->entityManager->persist(entity: $lookingForPropertyRequestReport->getPropertyRequirement()->getPropertyValueRangeRequirement()->getShopWindowLength());
        $this->entityManager->persist(entity: $lookingForPropertyRequestReport->getPropertyRequirement()->getPropertyValueRangeRequirement()->getShopWidth());
        $this->entityManager->persist(entity: $lookingForPropertyRequestReport->getPriceRequirement());
        $this->entityManager->persist(entity: $lookingForPropertyRequestReport->getPriceRequirement()->getPurchasePriceNet());
        $this->entityManager->persist(entity: $lookingForPropertyRequestReport->getPriceRequirement()->getPurchasePriceGross());
        $this->entityManager->persist(entity: $lookingForPropertyRequestReport->getPriceRequirement()->getPurchasePricePerSquareMeter());
        $this->entityManager->persist(entity: $lookingForPropertyRequestReport->getPriceRequirement()->getNetColdRent());
        $this->entityManager->persist(entity: $lookingForPropertyRequestReport->getPriceRequirement()->getColdRent());
        $this->entityManager->persist(entity: $lookingForPropertyRequestReport->getPriceRequirement()->getRentalPricePerSquareMeter());
        $this->entityManager->persist(entity: $lookingForPropertyRequestReport->getPriceRequirement()->getLease());
        $this->entityManager->persist(entity: $lookingForPropertyRequestReport);

        $this->entityManager->flush();

        $this->lookingForPropertyRequestReporterEmailService->sendLookingForPropertyRequestReportCreatedConfirmationEmail(lookingForPropertyRequestReport: $lookingForPropertyRequestReport);

        $this->entityManager->commit();

        return new JsonResponse(
            data: $this->serializer->serialize(
                data: new ReportCreateResponse($lookingForPropertyRequestReport->getUuid()),
                format: 'json'
            ),
            headers: ['Access-Control-Allow-Origin' => '*'],
            json: true
        );
    }
}
