<?php

declare(strict_types=1);

namespace App\Controller\Api\LookingForPropertyRequestReporter;

use App\Domain\Entity\LookingForPropertyRequestReporter\BasicInformationFormField;
use App\Domain\Entity\LookingForPropertyRequestReporter\PropertyInformationFormField;
use App\Domain\Entity\LookingForPropertyRequestReporter\ReporterInformationFormField;
use App\Domain\Entity\PlaceRelationType;
use App\Domain\Location\LocationService;
use App\Domain\LookingForPropertyRequestReporter\LookingForPropertyRequestReporterConfigurationService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Uid\Uuid;

#[IsGranted('PUBLIC_ACCESS')]
#[Route('/api/looking-for-property-request-reporter', name: 'api_looking_for_property_request_reporter_')]
class ConfigurationController extends AbstractController
{
    public function __construct(
        private readonly LookingForPropertyRequestReporterConfigurationService $lookingForPropertyRequestReporterConfigurationService,
        private readonly LocationService $locationService,
        private readonly SerializerInterface $serializer
    ) {
    }

    #[Route('/configurations/{uuid<[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}>}', name: 'get_configuration', methods: ['GET'], format: 'json')]
    public function getConfiguration(string $uuid): JsonResponse
    {
        $lookingForPropertyRequestReporterConfiguration = $this->lookingForPropertyRequestReporterConfigurationService
            ->fetchLookingForPropertyRequestReporterConfigurationByAccountKey(Uuid::fromString(uuid: $uuid));

        if ($lookingForPropertyRequestReporterConfiguration === null) {
            throw new NotFoundHttpException();
        }

        $places = $this->locationService->fetchRecursivePlacesByPlacesAndPlaceRelationTypeAndPlaceType(
            places: [$lookingForPropertyRequestReporterConfiguration->getAccount()->getAssignedPlace()],
            placeTypes: $this->locationService->getCommunePlaceTypes(),
            placeRelationType: PlaceRelationType::CHILD
        );

        $lookingForPropertyRequestReporterConfiguration
            ->setPrivacyPolicy(base64_decode($lookingForPropertyRequestReporterConfiguration->getPrivacyPolicy()))
            ->addRequiredBasicInformationFormField(BasicInformationFormField::TITLE)
            ->addRequiredBasicInformationFormField(BasicInformationFormField::REQUEST_REASON)
            ->addRequiredBasicInformationFormField(BasicInformationFormField::INDUSTRY_CLASSIFICATION)
            ->addRequiredPropertyInformationFormField(PropertyInformationFormField::TOTAL_SPACE)
            ->addRequiredReporterInformationFormField(ReporterInformationFormField::EMAIL)
            ->addRequiredReporterInformationFormField(ReporterInformationFormField::PRIVACY_POLICY_ACCEPT)
            ->addRequiredReporterInformationFormField(ReporterInformationFormField::NAME);

        $attributes = [
            'accountKey',
            'industryClassifications' => [
                'id',
                'name',
                'levelOne',
                'levelTwo',
                'levelThree',
            ],
            'requiredBasicInformationFormFields',
            'requiredLocationFormFields',
            'requiredPropertyInformationFormFields',
            'requiredReporterFormFields',
            'firstColor',
            'firstFontColor',
            'secondaryColor',
            'secondaryFontColor',
            'privacyPolicy',
            'title',
            'text',
            'uuid',
            'placeName',
            'placeShortName',
            'placeType',
        ];

        return new JsonResponse(
            data: $this->serializer->serialize(
                data: [
                    'configuration' => $lookingForPropertyRequestReporterConfiguration,
                    'places'        => $places,
                ],
                format: 'json',
                context: [AbstractNormalizer::ATTRIBUTES => $attributes]
            ),
            headers: ['Access-Control-Allow-Origin' => '*'],
            json: true
        );
    }
}
