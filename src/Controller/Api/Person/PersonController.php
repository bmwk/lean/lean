<?php

declare(strict_types=1);

namespace App\Controller\Api\Person;

use App\Domain\Entity\Person\PersonSearch;
use App\Domain\Person\PersonService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

#[Security("is_granted('ROLE_USER') or is_granted('ROLE_PROPERTY_FEEDER') or is_granted('ROLE_LOOKING_FOR_PROPERTY_REQUEST_FEEDER') or is_granted('ROLE_PROPERTY_VACANCY_REPORT_MANAGER') or is_granted('ROLE_LOOKING_FOR_PROPERTY_REQUEST_REPORT_MANAGER') or is_granted('ROLE_VIEWER')")]
#[Route('/api/person-management', name: 'api_person_management_')]
class PersonController extends AbstractController
{
    public function __construct(
        private readonly PersonService $personService
    ) {
    }

    #[Route('/persons', name: 'get_persons', methods: ['GET'], format: 'json')]
    public function getPersons(Request $request, UserInterface $user): JsonResponse
    {
        $account = $user->getAccount();

        $personSearch = null;

        if ($request->get('search') !== null && empty($request->get('search') === false)) {
            $personSearch = new PersonSearch();
            $personSearch->setText($request->get('search'));
        }

        $persons = $this->personService->fetchPersonsByAccount(account: $account, anonymized: false, personSearch: $personSearch);

        $attributes = [
            'id',
            'personType',
            'name',
            'firstName',
            'displayName',
        ];

        return $this->json(
            data: $persons,
            context: [AbstractNormalizer::ATTRIBUTES => $attributes]
        );
    }
}
