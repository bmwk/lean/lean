<?php

declare(strict_types=1);

namespace App\Controller\Api\Location;

use App\Domain\Location\Api\Entity\ResourceNamePlaceTypeMapping;
use App\Domain\Location\LocationService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Uid\Uuid;

#[IsGranted('PUBLIC_ACCESS')]
#[Route('/api/location', name: 'api_location_')]
class LocationController extends AbstractController
{
    public function __construct(
        private readonly LocationService $locationService,
        private readonly SerializerInterface $serializer
    ) {
    }

    #[Route('/places/{placeUuid<[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}>}', name: 'place', methods: ['GET'], format: 'json')]
    public function place(string $placeUuid): JsonResponse
    {
        $place = $this->locationService->fetchPlaceByUuid(uuid: Uuid::fromString($placeUuid));

        if ($place === null) {
            throw new NotFoundHttpException();
        }

        $placeAttributes = [
            'uuid',
            'placeName',
            'placeShortName',
            'placeType',
            'geolocationPoint' => [
                'point',
            ],
        ];

        $placeAttributes['parentPlaces'] = $placeAttributes;
        $placeAttributes['parentPlaces']['parentPlaces'] = $placeAttributes;

        return new JsonResponse(
            data: $this->serializer->serialize(
                data: $place,
                format: 'json',
                context: [AbstractNormalizer::ATTRIBUTES => $placeAttributes]
            ),
            headers: ['Access-Control-Allow-Origin' => '*'],
            json: true
        );
    }

    #[Route('/places/{placeUuid<[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}>}/places', name: 'child_places_from_place', methods: ['GET'], format: 'json')]
    public function childPlacesFromPlace(string $placeUuid): JsonResponse
    {
        $place =  $this->locationService->fetchPlaceByUuid(uuid: Uuid::fromString($placeUuid));

        if ($place === null) {
            throw new NotFoundHttpException();
        }

        $placeAttributes = [
            'uuid',
            'placeName',
            'placeShortName',
            'placeType',
            'geolocationPoint' => [
                'point',
            ],
        ];

        return new JsonResponse(
            data: $this->serializer->serialize(
                data: $place->getChildrenPlaces(),
                format: 'json',
                context: [AbstractNormalizer::ATTRIBUTES => $placeAttributes]
            ),
            headers: ['Access-Control-Allow-Origin' => '*'],
            json: true
        );
    }

    #[Route('/places/{placeUuid<[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}>}/{placeTypeName}', name: 'child_places_from_place_by_place_type', methods: ['GET'], format: 'json')]
    public function childPlacesFromPlaceByPlaceType(string $placeUuid, string $placeTypeName): JsonResponse
    {
        $placeType = ResourceNamePlaceTypeMapping::getPlaceType($placeTypeName);

        if ($placeType === null) {
            throw new NotFoundHttpException();
        }

        $place = $this->locationService->fetchPlaceByUuid(uuid: Uuid::fromString($placeUuid));

        if ($place === null) {
            throw new NotFoundHttpException();
        }

        $places = $this->locationService->fetchChildPlacesByPlaceType(place: $place, placeTypes: [$placeType]);

        $placeAttributes = [
            'uuid',
            'placeName',
            'placeShortName',
            'placeType',
            'geolocationPoint' => [
                'point',
            ],
        ];

        return new JsonResponse(
            data: $this->serializer->serialize(
                data: $places,
                format: 'json',
                context: [AbstractNormalizer::ATTRIBUTES => $placeAttributes]
            ),
            headers: ['Access-Control-Allow-Origin' => '*'],
            json: true
        );
    }

    #[Route('/{placeTypeName}', name: 'places_by_type', methods: ['GET'], format: 'json')]
    public function placesByPlaceType(string $placeTypeName): JsonResponse
    {
        $placeType = ResourceNamePlaceTypeMapping::getPlaceType($placeTypeName);

        if ($placeType === null) {
            throw new NotFoundHttpException();
        }

        $placeAttributes = [
            'uuid',
            'placeName',
            'placeShortName',
            'placeType',
            'geolocationPoint' => [
                'point',
            ],
        ];

        $places = $this->locationService->fetchPlacesByPlaceType(placeType: $placeType);

        return new JsonResponse(
            data: $this->serializer->serialize(
                data: $places,
                format: 'json',
                context: [AbstractNormalizer::ATTRIBUTES => $placeAttributes]
            ),
            headers: ['Access-Control-Allow-Origin' => '*'],
            json: true
        );
    }
}
