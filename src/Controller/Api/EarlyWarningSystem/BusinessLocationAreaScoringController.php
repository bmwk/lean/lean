<?php

declare(strict_types=1);

namespace App\Controller\Api\EarlyWarningSystem;

use App\Domain\EarlyWarningSystem\EarlyWarningSystemService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

#[Security("is_granted('ROLE_ACCOUNT_ADMIN') or is_granted('ROLE_EARLY_WARNING_SYSTEM_VIEWER')")]
#[Route('/api/early-warning-system', name: 'api_early_warning_system_')]
class BusinessLocationAreaScoringController extends AbstractController
{
    private const IGNORED_ATTRIBUTES = [
        '__initializer__',
        '__cloner__',
        '__isInitialized__',
    ];

    public function __construct(
        private readonly EarlyWarningSystemService $earlyWarningSystemService,
        private readonly SerializerInterface $serializer
    ) {
    }

    #[Route('/business-location-area-scorings', name: 'get_business_location_area_scorings', methods: ['GET'])]
    public function getBusinessLocationAreaScorings(UserInterface $user): JsonResponse
    {
        $businessLocationAreaScorings = $this->earlyWarningSystemService->fetchBusinessLocationAreaScorings(account: $user->getAccount());

        return new JsonResponse(
            data: $this->serializer->serialize(
                data: $businessLocationAreaScorings,
                format: 'json',
                context: [AbstractNormalizer::IGNORED_ATTRIBUTES => self::IGNORED_ATTRIBUTES]
            ),
            json: true
        );
    }

    #[Route('/business-location-area-scorings/{businessLocationAreaId<\d{1,10}>}', name: 'get_business_location_area_scoring', methods: ['GET'])]
    public function getBusinessLocationAreaScoring(int $businessLocationAreaId, UserInterface $user): JsonResponse
    {
        $businessLocationAreaScoring = $this->earlyWarningSystemService->fetchBusinessLocationAreaScoringByBusinessLocationAreaId(
            businessLocationAreaId: $businessLocationAreaId,
            account: $user->getAccount()
        );

        if ($businessLocationAreaScoring === null) {
            throw new NotFoundHttpException();
        }

        return new JsonResponse(
            data: $this->serializer->serialize(
                data: $businessLocationAreaScoring,
                format: 'json',
                context: [AbstractNormalizer::IGNORED_ATTRIBUTES => self::IGNORED_ATTRIBUTES]
            ),
            json: true
        );
    }
}
