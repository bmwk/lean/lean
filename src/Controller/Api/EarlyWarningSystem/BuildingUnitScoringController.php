<?php

declare(strict_types=1);

namespace App\Controller\Api\EarlyWarningSystem;

use App\Domain\EarlyWarningSystem\EarlyWarningSystemService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

#[Security("is_granted('ROLE_ACCOUNT_ADMIN') or is_granted('ROLE_EARLY_WARNING_SYSTEM_VIEWER')")]
#[Route('/api/early-warning-system', name: 'api_early_warning_system_')]
class BuildingUnitScoringController extends AbstractController
{
    private const IGNORED_ATTRIBUTES = [
        '__initializer__',
        '__cloner__',
        '__isInitialized__',
    ];

    public function __construct(
        private readonly EarlyWarningSystemService $earlyWarningSystemService,
        private readonly SerializerInterface $serializer
    ) {
    }

    #[Route('/building-unit-scorings', name: 'get_building_unit_scorings')]
    public function getBuildingUnitScorings(UserInterface $user): JsonResponse
    {
        return new JsonResponse(
            data: $this->serializer->serialize(
                data: $this->earlyWarningSystemService->fetchBuildingUnitScorings(account: $user->getAccount()),
                format: 'json',
                context: [AbstractNormalizer::IGNORED_ATTRIBUTES => self::IGNORED_ATTRIBUTES]
            ),
            json: true
        );
    }

    #[Route('/building-unit-scorings/{taodBuildingUnitId<\d{1,10}>}', name: 'get_building_unit_scoring')]
    public function getBuildingUnitScoring(int $taodBuildingUnitId, UserInterface $user): JsonResponse
    {
        $buildingUnitScoring = $this->earlyWarningSystemService->fetchBuildingUnitScoringByTaodBuildingUnitId(
            taodBuildingUnitId: $taodBuildingUnitId,
            account: $user->getAccount()
        );

        if ($buildingUnitScoring === null) {
            throw new NotFoundHttpException();
        }

        return new JsonResponse(
            data: $this->serializer->serialize(
                data: $buildingUnitScoring,
                format: 'json',
                context: [AbstractNormalizer::IGNORED_ATTRIBUTES => self::IGNORED_ATTRIBUTES]
            ),
            json: true
        );
    }
}
