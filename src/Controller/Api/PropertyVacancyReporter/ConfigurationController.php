<?php

declare(strict_types=1);

namespace App\Controller\Api\PropertyVacancyReporter;

use App\Domain\Entity\PlaceRelationType;
use App\Domain\Entity\PropertyVacancyReporter\PropertyInformationFormField;
use App\Domain\Entity\PropertyVacancyReporter\PropertyLocationFormField;
use App\Domain\Entity\PropertyVacancyReporter\ReporterFormField;
use App\Domain\Location\LocationService;
use App\Domain\PropertyVacancyReporter\PropertyVacancyReporterConfigurationService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Uid\Uuid;

#[IsGranted('PUBLIC_ACCESS')]
#[Route('/api/property-vacancy-reporter', name: 'api_property_vacancy_reporter_')]
class ConfigurationController extends AbstractController
{
    public function __construct(
        private readonly PropertyVacancyReporterConfigurationService $propertyVacancyReporterConfigurationService,
        private readonly LocationService $locationService,
        private readonly SerializerInterface $serializer
    ) {
    }

    #[Route('/configurations/{uuid<[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}>}', name: 'get_configuration', methods: ['GET'], format: 'json')]
    public function getConfiguration(string $uuid): JsonResponse
    {
        $accountKey = Uuid::fromString(uuid: $uuid);

        $propertyVacancyReporterConfiguration = $this->propertyVacancyReporterConfigurationService
            ->fetchPropertyVacancyReporterConfigurationByAccountKey(accountKey: $accountKey);

        if ($propertyVacancyReporterConfiguration === null) {
            throw new NotFoundHttpException();
        }

        $places = $this->locationService->fetchRecursivePlacesByPlacesAndPlaceRelationTypeAndPlaceType(
            places: [$propertyVacancyReporterConfiguration->getAccount()->getAssignedPlace()],
            placeTypes: $this->locationService->getCommunePlaceTypes(),
            placeRelationType: PlaceRelationType::CHILD
        );

        $propertyVacancyReporterConfiguration
            ->setPrivacyPolicy(base64_decode($propertyVacancyReporterConfiguration->getPrivacyPolicy()))
            ->addRequiredPropertyInformationFormField(PropertyInformationFormField::PROPERTY_USAGE_TYPE)
            ->addRequiredPropertyLocationFormField(PropertyLocationFormField::STREET_NAME)
            ->addRequiredPropertyLocationFormField(PropertyLocationFormField::HOUSE_NUMBER)
            ->addRequiredPropertyLocationFormField(PropertyLocationFormField::POSTAL_CODE)
            ->addRequiredPropertyLocationFormField(PropertyLocationFormField::PLACE)
            ->addRequiredReporterFormField(ReporterFormField::EMAIL)
            ->addRequiredReporterFormField(ReporterFormField::PRIVACY_POLICY_ACCEPT)
            ->addRequiredReporterFormField(ReporterFormField::NAME);

        $attributes = [
            'accountKey',
            'industryClassifications' => [
                'id',
                'name',
                'levelOne',
                'levelTwo',
                'levelThree',
            ],
            'requiredPropertyInformationFormFields',
            'requiredPropertyLocationFormFields',
            'requiredReporterFormFields',
            'firstColor',
            'firstFontColor',
            'secondaryColor',
            'secondaryFontColor',
            'privacyPolicy',
            'title',
            'text',
            'uuid',
            'placeName',
            'placeShortName',
            'placeType',
        ];

        return new JsonResponse(
            data: $this->serializer->serialize(
                data: [
                    'configuration' => $propertyVacancyReporterConfiguration,
                    'places'        => $places,
                ],
                format: 'json',
                context: [AbstractNormalizer::ATTRIBUTES => $attributes]
            ),
            headers: ['Access-Control-Allow-Origin' => '*'],
            json: true
        );
    }
}
