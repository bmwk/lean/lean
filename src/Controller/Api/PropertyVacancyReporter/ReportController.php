<?php

declare(strict_types=1);

namespace App\Controller\Api\PropertyVacancyReporter;

use App\Domain\PropertyVacancyReport\PropertyVacancyReportService;
use App\Domain\PropertyVacancyReporter\Api\Entity\Request\ReportCreateRequest;
use App\Domain\PropertyVacancyReporter\Api\Entity\Response\ReportCreateResponse;
use App\Domain\PropertyVacancyReporter\PropertyVacancyReporterConfigurationService;
use App\Domain\PropertyVacancyReporter\PropertyVacancyReporterEmailService;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[IsGranted('PUBLIC_ACCESS')]
#[Route('/api/property-vacancy-reporter', name: 'api_property_vacancy_reporter_')]
class ReportController extends AbstractController
{
    public function __construct(
        private readonly PropertyVacancyReporterEmailService $propertyVacancyReporterEmailService,
        private readonly PropertyVacancyReportService $propertyVacancyReportService,
        private readonly PropertyVacancyReporterConfigurationService $propertyVacancyReporterConfigurationService,
        private readonly ValidatorInterface $validator,
        private readonly SerializerInterface $serializer,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    #[Route('/reports',  name: 'options_reports', methods: ['OPTIONS'], format: 'json')]
    public function optionsReports(): JsonResponse
    {
        return new JsonResponse(
            data: null,
            status: Response::HTTP_NO_CONTENT,
            headers: [
                'Allow' => 'POST,OPTIONS',
                'Access-Control-Allow-Methods' => join(separator: ',', array: [Request::METHOD_POST, Request::METHOD_OPTIONS]),
                'Access-Control-Allow-Headers' => 'Content-Type',
                'Access-Control-Allow-Origin'  => '*',
            ]
        );
    }

    #[Route('/reports',  name: 'report_create', methods: ['POST'], format: 'json')]
    public function reportCreate(Request $request): JsonResponse
    {
        $jsonContent = json_decode(json: $request->getContent(), associative: false);

        if ($jsonContent === null) {
            throw new BadRequestException();
        }

        $reportCreateRequest = ReportCreateRequest::createFormApiRequestJsonContent(jsonContent: $jsonContent);

        $errors = $this->validator->validate(value: $reportCreateRequest);

        if (empty($errors->count()) === false) {
            throw new BadRequestException();
        }

        $propertyVacancyReport = $this->propertyVacancyReportService->createPropertyVacancyReportFromReportCreateRequest(reportCreateRequest: $reportCreateRequest);

        $propertyVacancyReporterConfiguration = $this->propertyVacancyReporterConfigurationService
            ->fetchPropertyVacancyReporterConfigurationByAccountKey(accountKey: $reportCreateRequest->getAccountKey());

        if ($propertyVacancyReporterConfiguration === null) {
            throw new BadRequestException();
        }

        $account = $propertyVacancyReporterConfiguration->getAccount();

        $propertyVacancyReport->setAccount($account);

        $this->entityManager->persist(entity: $propertyVacancyReport);

        $this->entityManager->flush();

        $this->propertyVacancyReporterEmailService->sendPropertyVacancyReportCreatedConfirmationEmail(propertyVacancyReport: $propertyVacancyReport);

        return new JsonResponse(
            data: $this->serializer->serialize(
                data: new ReportCreateResponse($propertyVacancyReport->getUuid()),
                format: 'json'
            ),
            headers: ['Access-Control-Allow-Origin' => '*'],
            json: true
        );
    }

    #[Route('/reports/{reportUuid<[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}>}/photo/upload', name: 'options_photo_upload', methods: ['OPTIONS'], format: 'json')]
    public function optionsPhotoUpload(): JsonResponse
    {
        return new JsonResponse(
            data: null,
            status: Response::HTTP_NO_CONTENT,
            headers: [
                'Allow' => 'POST,OPTIONS',
                'Access-Control-Allow-Methods' => join(separator: ',', array: [Request::METHOD_POST, Request::METHOD_OPTIONS]),
                'Access-Control-Allow-Headers' => 'Content-Type,Cache-Control,X-Requested-With',
                'Access-Control-Allow-Origin'  => '*',
            ]
        );
    }

    #[Route('/reports/{reportUuid<[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}>}/photo/upload', name: 'photo_upload', methods: ['POST'], format: 'json')]
    public function photoUpload(string $reportUuid, Request $request): JsonResponse
    {
        $propertyVacancyReport = $this->propertyVacancyReportService->fetchPropertyVacancyReportByUuid(uuid: Uuid::fromString(uuid: $reportUuid));

        if ($propertyVacancyReport === null) {
            throw new NotFoundHttpException();
        }

        $imageFile = $request->files->get(key: 'imageFile');

        if ($imageFile === null) {
            throw new BadRequestHttpException();
        }

        $allowedFileMimeTypes = [
            'image/bmp',
            'image/cis-cod',
            'image/gif',
            'image/ief',
            'image/jpeg',
            'image/png',
            'image/pipeg',
            'image/svg+xml',
            'image/tiff',
            'image/x-cmu-raster',
            'image/x-cmx',
            'image/x-icon',
            'image/x-portable-anymap',
            'image/x-portable-bitmap',
            'image/x-portable-graymap',
            'image/x-portable-pixmap',
            'image/x-rgb',
            'image/x-xbitmap',
            'image/x-xpixmap',
            'image/x-xwindowdump',
        ];

        if (in_array(needle: $imageFile->getMimeType(), haystack: $allowedFileMimeTypes) !== true) {
            throw new BadRequestHttpException();
        }

        $propertyVacancyReportImage = $this->propertyVacancyReportService->createAndSavePropertyVacancyReportImageFromFileContent(
            fileContent: $imageFile->getContent(),
            fileName: $imageFile->getClientOriginalName(),
            fileExtension: $imageFile->guessExtension(),
            propertyVacancyReport: $propertyVacancyReport
        );

        return new JsonResponse(data: ['success' => true], headers: ['Access-Control-Allow-Origin' => '*']);
    }
}
