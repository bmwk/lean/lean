<?php

declare(strict_types=1);

namespace App\Controller\Api\PropertyVacancyManagement;

use App\Domain\Account\AccountUserService;
use App\Domain\Entity\AccountUser\AccountUserRole;
use App\Domain\Entity\AccountUser\AccountUserType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

#[Security("is_granted('ROLE_USER') or is_granted('ROLE_PROPERTY_FEEDER') or is_granted('ROLE_VIEWER') or is_granted('ROLE_RESTRICTED_VIEWER')")]
#[Route('/api/property-vacancy-management', name: 'api_property_vacancy_management_')]
class AccountUserController extends AbstractController
{
    public function __construct(
        private readonly AccountUserService $accountUserService,
        private readonly SerializerInterface $serializer
    ) {
    }

    #[Route('/account-users', name: 'get_account_users', methods: ['GET'], format: 'json')]
    public function getAccountUsers(UserInterface $user): JsonResponse
    {
        $attributes = [
            'id',
            'fullName',
        ];

        $accountUsers = $this->accountUserService->fetchAccountUsersByAccount(
            account: $user->getAccount(),
            withFromSubAccounts: false,
            accountUserTypes: [AccountUserType::INTERNAL],
            accountUserRoles: [
                AccountUserRole::ROLE_ACCOUNT_ADMIN,
                AccountUserRole::ROLE_USER,
                AccountUserRole::ROLE_PROPERTY_FEEDER,
            ]
        );

        return new JsonResponse(
            data: $this->serializer->serialize(
                data: $accountUsers,
                format: 'json',
                context: [AbstractNormalizer::ATTRIBUTES => $attributes]
            ),
            json: true
        );
    }
}
