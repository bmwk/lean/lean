<?php

declare(strict_types=1);

namespace App\Controller\Api\PropertyVacancyManagement;

use App\Domain\Entity\GeolocationPoint;
use App\Domain\Entity\GeolocationPolygon;
use App\Domain\Property\BuildingUnitDeletionService;
use App\Domain\Property\BuildingUnitService;
use CrEOF\Spatial\PHP\Types\Geometry\Point;
use CrEOF\Spatial\PHP\Types\Geometry\Polygon;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

#[Security("is_granted('ROLE_USER') or is_granted('ROLE_PROPERTY_FEEDER') or is_granted('ROLE_PROPERTY_PROVIDER') or is_granted('ROLE_VIEWER') or is_granted('ROLE_RESTRICTED_VIEWER')")]
#[Route('/api/property-vacancy-management', name: 'api_property_vacancy_management_')]
class BuildingUnitController extends AbstractController
{
    public function __construct(
        private readonly BuildingUnitService $buildingUnitService,
        private readonly BuildingUnitDeletionService $buildingUnitDeletionService,
        private readonly SerializerInterface $serializer,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    #[Route('/building-units', name: 'get_building_units', methods: ['GET'], format: 'json')]
    public function getBuildingUnits(Request $request, UserInterface $user): JsonResponse
    {
        $archived = false;

        if ($request->get(key: 'archived') !== null && $request->get(key: 'archived') === 'true') {
            $archived = true;
        }

        $buildingUnits = $this->buildingUnitService->fetchBuildingUnitsByAccount(
            account: $user->getAccount(),
            withFromSubAccounts: true,
            archived: $archived
        );

        $attributes = [
            'id',
            'internalNumber',
            'name',
            'currentUsageIndustryClassification' => [
                'name',
                'levelOne',
                'levelTwo',
                'levelThree',
            ],
            'pastUsageIndustryClassification' => [
                'name',
                'levelOne',
                'levelTwo',
                'levelThree',
            ],
            'futureUsageIndustryClassification' => [
                'name',
                'levelOne',
                'levelTwo',
                'levelThree',
            ],
            'objectIsEmpty',
            'objectBecomesEmpty',
            'reuseAgreed',
            'objectIsEmptySince',
            'objectBecomesEmptyFrom',
            'reuseFrom',
            'keyProperty',
            'areaSize',
            'shopWindowFrontWidth',
            'barrierFreeAccess',
            'assignedToAccountUser' => [
                'id',
                'fullName',
            ],
            'inFloors',
            'propertyMarketingInformation' => [
                'propertyOfferType',
                'possibleUsageIndustryClassifications' => [
                    'name',
                    'levelOne',
                    'levelTwo',
                    'levelThree',
                ],
            ],
            'address' => [
                'streetName',
                'houseNumber',
                'place' => [
                    'placeName',
                ],
                'quarterPlace' => [
                    'placeName',
                ],
            ],
            'geolocationPoint' => [
                'point',
            ],
            'geolocationPolygon' => [
                'polygon',
            ],
            'geolocationMultiPolygon' => [
                'multiPolygon',
            ],
            'createdAt',
            'updatedAt',
        ];

        return new JsonResponse(
            data: $this->serializer->serialize(
                data: $buildingUnits,
                format: 'json',
                context: [AbstractNormalizer::ATTRIBUTES => $attributes]
            ),
            json: true
        );
    }

    #[Route('/building-units/{buildingUnitId<\d{1,10}>}', name: 'get_building_unit', methods: ['GET'], format: 'json')]
    public function getBuildingUnit(int $buildingUnitId, UserInterface $user): JsonResponse
    {
        $buildingUnit = $this->buildingUnitService->fetchBuildingUnitById(
            account: $user->getAccount(),
            withFromSubAccounts: true,
            id: $buildingUnitId
        );

        if ($buildingUnit === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'view', subject: $buildingUnit);

        $attributes = [
            'id',
            'name',
            'currentUsageIndustryClassification' => [
                'name',
                'levelOne',
                'levelTwo',
                'levelThree',
            ],
            'pastUsageIndustryClassification' => [
                'name',
                'levelOne',
                'levelTwo',
                'levelThree',
            ],
            'futureUsageIndustryClassification' => [
                'name',
                'levelOne',
                'levelTwo',
                'levelThree',
            ],
            'objectIsEmpty',
            'objectBecomesEmpty',
            'reuseAgreed',
            'propertyMarketingInformation' => [],
            'address' => [
                'streetName',
                'place' => [
                    'placeName',
                ],
            ],
            'geolocationPoint' => [
                'id',
                'point',
            ],
            'geolocationPolygon' => [
                'id',
                'polygon',
            ],
            'geolocationMultiPolygon' => [
                'id',
                'multiPolygon',
            ],
        ];

        return new JsonResponse(
            data: $this->serializer->serialize(
                data: $buildingUnit,
                format: 'json',
                context: [AbstractNormalizer::ATTRIBUTES => $attributes]
            ),
            json: true
        );
    }

    #[Security("is_granted('ROLE_USER') or is_granted('ROLE_PROPERTY_FEEDER') or is_granted('ROLE_PROPERTY_PROVIDER')")]
    #[Route('/building-units/{buildingUnitId<\d{1,10}>}', name: 'put_building_unit', methods: ['PUT'], format: 'json')]
    public function putBuildingUnit(int $buildingUnitId, Request $request, UserInterface $user): JsonResponse
    {
        $buildingUnit = $this->buildingUnitService->fetchBuildingUnitById(
            account: $user->getAccount(),
            withFromSubAccounts: true,
            id: $buildingUnitId
        );

        if ($buildingUnit === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'edit', subject: $buildingUnit);

        $jsonContent = json_decode($request->getContent(), false);

        if (
            (isset($jsonContent->longitude) === false && isset($jsonContent->latitude) === false)
            && isset($jsonContent->polygonCoordinates) === false
        ) {
            throw new BadRequestException();
        }

        if (isset($jsonContent->longitude) === true && isset($jsonContent->latitude) === true) {
            $this->entityManager->beginTransaction();

            if ($buildingUnit->getGeolocationPoint() === null) {
                $geolocationPoint = GeolocationPoint::createFromLatitudeAndLongitude(
                    latitude: $jsonContent->latitude,
                    longitude: $jsonContent->longitude
                );

                $this->entityManager->persist(entity: $geolocationPoint);

                $buildingUnit->setGeolocationPoint($geolocationPoint);
            } else {
                $buildingUnit->getGeolocationPoint()->setPoint(new Point($jsonContent->longitude, $jsonContent->latitude));
            }

            $this->entityManager->flush();
            $this->entityManager->commit();
        }

        if (isset($jsonContent->polygonCoordinates) === true) {
            $polygon = new Polygon($jsonContent->polygonCoordinates);

            $this->entityManager->beginTransaction();

            if ($buildingUnit->getGeolocationPolygon() === null) {
                $geolocationPolygon = new GeolocationPolygon();

                $geolocationPolygon->setPolygon($polygon);

                $this->entityManager->persist(entity: $geolocationPolygon);

                $buildingUnit->setGeolocationPolygon($geolocationPolygon);
            } else {
                $buildingUnit->getGeolocationPolygon()->setPolygon($polygon);
            }

            $this->entityManager->flush();
            $this->entityManager->commit();
        }

        return new JsonResponse(data: [], status: Response::HTTP_OK);
    }

    #[Security("is_granted('ROLE_USER') or is_granted('ROLE_PROPERTY_FEEDER') or is_granted('ROLE_PROPERTY_PROVIDER')")]
    #[Route('/building-units/{buildingUnitId<\d{1,10}>}/geolocation-polygons/{geolocationPolygonId<\d{1,10}>}', name: 'delete_building_unit_geolocation_polygon', methods: ['DELETE'], format: 'json')]
    public function deleteBuildingUnitGeolocationPolygon(int $buildingUnitId, int $geolocationPolygonId, UserInterface $user): JsonResponse
    {
        $buildingUnit = $this->buildingUnitService->fetchBuildingUnitById(
            account: $user->getAccount(),
            withFromSubAccounts: true,
            id: $buildingUnitId
        );

        if ($buildingUnit === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'edit', subject: $buildingUnit);

        $geolocationPolygon = $buildingUnit->getGeolocationPolygon();

        if ($geolocationPolygon->getId() !== $geolocationPolygonId) {
            throw new NotFoundHttpException();
        }

        $this->entityManager->beginTransaction();

        $this->buildingUnitDeletionService->deleteGeolocationPolygon(property: $buildingUnit);

        $this->entityManager->commit();

        return new JsonResponse(data: [], status: Response::HTTP_OK);
    }
}
