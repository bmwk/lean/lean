<?php

declare(strict_types=1);

namespace App\Controller\Api\PropertyVacancyManagement;

use App\Domain\Entity\BuildingUnitFeedback;
use App\Domain\Feedback\FeedbackService;
use App\Domain\Property\Api\Entity\Request\BuildingUnitFeedbackCreateRequest;
use App\Domain\Property\BuildingUnitService;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\SerializerInterface;

#[Security("is_granted('ROLE_USER') or is_granted('ROLE_PROPERTY_FEEDER')")]
#[Route('/api/property-vacancy-management', name: 'api_property_vacancy_management_')]
class BuildingUnitFeedbackController extends AbstractController
{
    public function __construct(
        private readonly BuildingUnitService $buildingUnitService,
        private readonly FeedbackService $feedbackService,
        private readonly SerializerInterface $serializer,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    #[Route('/building-units/{buildingUnitId<\d{1,10}>}/feedbacks', name: 'post_building_unit_feedback', methods: ['POST'], format: 'json')]
    public function postBuildingUnitFeedback(int $buildingUnitId, Request $request, UserInterface $user): JsonResponse
    {
        $buildingUnit = $this->buildingUnitService->fetchBuildingUnitById(
            account: $user->getAccount(),
            withFromSubAccounts: true,
            id: $buildingUnitId
        );

        if ($buildingUnit === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'edit', subject: $buildingUnit);

        $buildingUnitFeedbackFromRequest = $this->serializer->deserialize(
            data: $request->getContent(),
            type: BuildingUnitFeedbackCreateRequest::class,
            format: 'json'
        );

        $feedback = $this->feedbackService->fetchFeedbackById(id: $buildingUnitFeedbackFromRequest->getFeedbackId());

        if ($feedback === null) {
            throw new NotFoundHttpException();
        }

        $buildingUnitFeedback = new BuildingUnitFeedback();

        $buildingUnitFeedback
            ->setBuildingUnit($buildingUnit)
            ->setFeedback($feedback);

        $this->entityManager->persist(entity: $buildingUnitFeedback);
        $this->entityManager->flush();

        return new JsonResponse(data: [], status: Response::HTTP_CREATED);
    }

    #[Route('/building-units/{buildingUnitId<\d{1,10}>}/feedbacks/{feedbackId<\d{1,10}>}', name: 'delete_building_unit_feedback', methods: ['DELETE'], format: 'json')]
    public function deleteBuildingUnitFeedback(int $buildingUnitId, int $feedbackId, UserInterface $user): JsonResponse
    {
        $buildingUnit = $this->buildingUnitService->fetchBuildingUnitById(
            account: $user->getAccount(),
            withFromSubAccounts: true,
            id: $buildingUnitId
        );

        if ($buildingUnit === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'edit', subject: $buildingUnit);

        $feedback = $this->feedbackService->fetchFeedbackById(id: $feedbackId);

        if ($feedback === null) {
            throw new NotFoundHttpException();
        }

        $buildingUnitFeedback = $this->feedbackService->fetchBuildingUnitFeedbackByBuildingUnitAndFeedback(
            buildingUnit: $buildingUnit,
            feedback: $feedback
        );

        if ($buildingUnitFeedback === null) {
            throw new NotFoundHttpException();
        }

        $this->entityManager->remove(entity: $buildingUnitFeedback);
        $this->entityManager->flush();

        return new JsonResponse();
    }
}
