<?php

declare(strict_types=1);

namespace App\Controller\Api\Statistics;

use App\Domain\LookingForPropertyRequestReport\LookingForPropertyRequestReportStatisticsService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\SerializerInterface;

#[Security("is_granted('ROLE_USER') or is_granted('ROLE_VIEWER') or is_granted('ROLE_RESTRICTED_VIEWER')")]
#[Route('/api/statistics/looking-for-property-request-report', name: 'api_statistics_looking_for_property_request_report_')]
class LookingForPropertyRequestReportController extends AbstractController
{
    public function __construct(
        private readonly LookingForPropertyRequestReportStatisticsService $lookingForPropertyRequestReportStatisticsService,
        private readonly SerializerInterface $serializer
    ) {
    }

    #[Route('/amount-looking-for-property-request-reports', name: 'amount_looking_for_property_request_reports', methods: ['GET'], format: 'json')]
    public function amountLookingForPropertyRequestReports(Request $request, UserInterface $user): JsonResponse
    {
        $year = $request->get(key: 'year');

        if (empty($year) === true) {
            throw new BadRequestHttpException();
        }

        if (preg_match(pattern: '/^[1-9][0-9]{3}$/', subject: $year) === 0) {
            throw new BadRequestHttpException();
        }

        return new JsonResponse(
            data: $this->serializer->serialize(
                data: $this->lookingForPropertyRequestReportStatisticsService->buildAmountLookingForPropertyRequestReportsByMonthFromYear(
                    account: $user->getAccount(),
                    year: (int) $year,
                ),
                format: 'json'
            ),
            json: true
        );
    }
}
