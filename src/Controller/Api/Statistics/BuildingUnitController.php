<?php

declare(strict_types=1);

namespace App\Controller\Api\Statistics;

use App\Domain\Property\BuildingUnitStatisticsService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

#[Security("is_granted('ROLE_USER') or is_granted('ROLE_VIEWER') or is_granted('ROLE_RESTRICTED_VIEWER')")]
#[Route('/api/statistics/building-unit', name: 'api_statistics_building_unit_')]
class BuildingUnitController extends AbstractController
{
    public function __construct(
        private readonly BuildingUnitStatisticsService $buildingUnitStatisticsService,
        private readonly SerializerInterface $serializer
    ) {
    }

    #[Route('/amount-building-units-by-current-usage', name: 'amount_building_units_by_current_usage', methods: ['GET'], format: 'json')]
    public function amountBuildingUnitsByCurrentUsage(UserInterface $user): JsonResponse
    {
        $industryClassificationAttributes = [
            'id',
            'name',
            'levelOne',
            'levelTwo',
            'levelThree',
        ];

        $attributes = [
           'amount',
           'industryClassification' => $industryClassificationAttributes,
        ];

        return new JsonResponse(
            data: $this->serializer->serialize(
                data: $this->buildingUnitStatisticsService->buildAmountBuildingUnitsByCurrentUsage(
                    account: $user->getAccount(),
                    withFromSubAccounts: true
                ),
                format: 'json',
                context: [AbstractNormalizer::ATTRIBUTES => $attributes]
            ),
            json: true
        );
    }
}
