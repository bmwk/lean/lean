<?php

declare(strict_types=1);

namespace App\Controller\Api\Statistics;

use App\Domain\PropertyVacancyReport\PropertyVacancyReportStatisticsService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\SerializerInterface;

#[Security("is_granted('ROLE_USER') or is_granted('ROLE_VIEWER') or is_granted('ROLE_RESTRICTED_VIEWER')")]
#[Route('/api/statistics/property-vacancy-report', name: 'api_statistics_property_vacancy_report_')]
class PropertyVacancyReportController extends AbstractController
{
    public function __construct(
        private readonly PropertyVacancyReportStatisticsService $propertyVacancyReportStatisticsService,
        private readonly SerializerInterface $serializer
    ) {
    }

    #[Route('/amount-property-vacancy-reports', name: 'amount_property_vacancy_reports', methods: ['GET'], format: 'json')]
    public function amountPropertyVacancyReports(Request $request, UserInterface $user): JsonResponse
    {
        $year = $request->get(key: 'year');

        if (empty($year) === true) {
            throw new BadRequestHttpException();
        }

        if (preg_match(pattern: '/^[1-9][0-9]{3}$/', subject: $year) === 0) {
            throw new BadRequestHttpException();
        }

        return new JsonResponse(
            data: $this->serializer->serialize(
                data: $this->propertyVacancyReportStatisticsService->buildAmountPropertyVacancyReportsByMonthFromYear(
                    account: $user->getAccount(),
                    year: (int) $year,
                ),
                format: 'json'
            ),
            json: true
        );
    }
}
