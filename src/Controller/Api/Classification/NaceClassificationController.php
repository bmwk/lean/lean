<?php

declare(strict_types=1);

namespace App\Controller\Api\Classification;

use App\Domain\Classification\ClassificationService;
use App\Domain\Entity\Classification\NaceClassificationLevel;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

#[IsGranted('PUBLIC_ACCESS')]
#[Route('/api/classifications', name: 'api_classifications_')]
class NaceClassificationController extends AbstractController
{
    public function __construct(
        private readonly ClassificationService $classificationService,
        private readonly SerializerInterface $serializer
    ) {
    }

    #[Route('/nace-classifications', name: 'get_nace_classifications', methods: ['GET'], format: 'json')]
    public function getNaceClassifications(Request $request): JsonResponse
    {
        $levelId = $request->query->get('level');
        $name = $request->query->has('name') ? $request->query->get('name') : null;
        $code = $request->query->get('code') ? $request->query->get('code') : null;

        $level = null;

        if ($levelId !== null) {
            $level = NaceClassificationLevel::from((int)$levelId);
        }

        $naceClassifications = $this->classificationService->fetchNaceClassificationsWithLevel(level: $level);

        if ($code !== null) {
            $naceClassifications = $this->classificationService->fetchNaceClassificationWhereSearchTermInCode(code: $code);
        }

        if ($name !== null) {
            $naceClassifications = $this->classificationService->fetchNaceClassificationWhereSearchTermInName(name: $name);
        }

        $naceClassificationAttributes = [
            'id',
            'name',
            'level',
            'code',
        ];

        $ignoredAttributes = [
            'children',
            '__initializer__',
            '__cloner__',
            '__isInitialized__',
            'naceClassifications',
        ];

        return new JsonResponse(
            data: $this->serializer->serialize(
                data: $naceClassifications,
                format: 'json',
                context: [
                    AbstractNormalizer::ATTRIBUTES         => $naceClassificationAttributes,
                    AbstractNormalizer::IGNORED_ATTRIBUTES => $ignoredAttributes,
                ]
            ),
            headers: ['Access-Control-Allow-Origin' => '*'],
            json: true
        );
    }

    #[Route('/nace-classifications/{naceClassificationId<\d{1,10}>}', name: 'get_nace_classification', methods: ['GET'], format: 'json')]
    public function getNaceClassification(int $naceClassificationId): JsonResponse
    {
        $naceClassification = $this->classificationService->fetchNaceClassificaionById(naceClassificationId: $naceClassificationId);

        if ($naceClassification === null) {
            throw new NotFoundHttpException();
        }

        $naceClassification->getIndustryClassifications();

        $naceClassificationAttributes = [
            'id',
            'name',
            'level',
            'code',
            'parent',
            'industryClassifications',
        ];

        $ignoredAttributes = [
            'children',
            '__initializer__',
            '__cloner__',
            '__isInitialized__',
            'naceClassifications',
        ];

        return new JsonResponse(
            data: $this->serializer->serialize(
                data: $naceClassification,
                format: 'json',
                context: [
                    AbstractNormalizer::ATTRIBUTES         => $naceClassificationAttributes,
                    AbstractNormalizer::IGNORED_ATTRIBUTES => $ignoredAttributes,
                ]
            ),
            headers: ['Access-Control-Allow-Origin' => '*'],
            json: true
        );
    }

    #[Route('/nace-classifications/{naceClassificationId<\d{1,10}>}/nace-classifications', name: 'get_children_nace_classifications_from_nace_classification', methods: ['GET'], format: 'json')]
    public function getChildrenNaceClassificationsFromNaceClassification(int $naceClassificationId): JsonResponse
    {
        $parentNaceClassification = $this->classificationService->fetchNaceClassificaionById(naceClassificationId: $naceClassificationId);

        if ($parentNaceClassification === null) {
            throw new NotFoundHttpException();
        }

        $naceClassifications = $this->classificationService->fetchNaceClassificationChildrens(parentNaceClassification: $parentNaceClassification);

        $naceClassificationAttributes = [
            'id',
            'name',
            'level',
            'code',
        ];

        return new JsonResponse(
            data: $this->serializer->serialize(
                data: $naceClassifications,
                format: 'json',
                context: [AbstractNormalizer::ATTRIBUTES => $naceClassificationAttributes]
            ),
            headers: ['Access-Control-Allow-Origin' => '*'],
            json: true
        );
    }
}
