<?php

declare(strict_types=1);

namespace App\Controller\Api\Classification;

use App\Domain\Classification\ClassificationService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

#[IsGranted('PUBLIC_ACCESS')]
#[Route('/api/classifications', name: 'api_classifications_')]
class IndustryClassificationController extends AbstractController
{
    public function __construct(
        private readonly ClassificationService $classificationService,
        private readonly SerializerInterface $serializer
    ) {
    }

    #[Route('/industry-classifications', name: 'get_industry_classifications', methods: ['GET'], format: 'json')]
    public function getIndustryClassifications(Request $request): JsonResponse
    {
        $level = $request->query->get('level') !== null ? (int)$request->query->get('level') : null;
        $levelOne = $request->query->has('levelOne') ? (int)$request->query->get('levelOne') : null;
        $levelTwo = $request->query->has('levelTwo') ? (int)$request->query->get('levelTwo') : null;
        $levelThree = $request->query->has('levelThree') ? (int)$request->query->get('levelThree') : null;
        $name = $request->query->has('name') ? $request->query->get('name') : null;

        $industryClassificationAttributes = [
            'id',
            'name',
            'levelOne',
            'levelTwo',
            'levelThree',
        ];

        $industryClassifications = match ($level) {
            1 => $this->classificationService->fetchTopLevelIndustryClassifications(),
            2 => $this->classificationService->fetchSecondLevelIndustryClassifications(),
            3 => $this->classificationService->fetchThirdLevelIndustryClassifications(),
            default => $this->classificationService->fetchAllIndustryClassifications()
        };

        if ($levelThree !== null && $levelTwo !== null && $levelOne !== null) {
            $industryClassifications = $this->classificationService->fetchOneIndustryClassificationByLevelsOneTwoAndThree(
                levelOne: $levelOne,
                levelTwo: $levelTwo,
                levelThree: $levelThree
            );
        }

        if ($levelTwo !== null && $levelOne !== null && $levelThree === null) {
            $industryClassifications = $this->classificationService->fetchOneIndustryClassificationByLevelsOneAndTwo(
                levelOne: $levelOne,
                levelTwo: $levelTwo
            );
        }

        if ($levelOne !== null && $levelTwo === null && $levelThree === null) {
            $industryClassifications = $this->classificationService->fetchOneIndustryClassificationByLevelOne(levelOne: $levelOne);
        }

        if ($name !== null) {
            $industryClassifications = $this->classificationService->fetchIndustryClassificationWhereSearchTermInName(searchTerm: $name);
        }

        return new JsonResponse(
            data: $this->serializer->serialize(
                data: $industryClassifications,
                format: 'json',
                context: [AbstractNormalizer::ATTRIBUTES => $industryClassificationAttributes]
            ),
            headers: ['Access-Control-Allow-Origin' => '*'],
            json: true
        );
    }

    #[Route('/industry-classifications/{industryClassificationId<\d{1,10}>}', name: 'get_industry_classification', methods: ['GET'], format: 'json')]
    public function getIndustryClassification(int $industryClassificationId): JsonResponse
    {
        $industryClassification = $this->classificationService->fetchIndustryClassificationById(id: $industryClassificationId);

        if ($industryClassification === null) {
            throw new NotFoundHttpException();
        }

        $industryClassification->getNaceClassifications();

        $industryClassificationAttributes = [
            'id',
            'name',
            'levelOne',
            'levelTwo',
            'levelThree',
            'parent',
            'naceClassifications',
        ];

        $ignoredAttributes = [
            'children',
            '__initializer__',
            '__cloner__',
            '__isInitialized__',
            'industryClassifications',
        ];

        return new JsonResponse(
            data: $this->serializer->serialize(
                data: $industryClassification,
                format: 'json',
                context: [
                    AbstractNormalizer::ATTRIBUTES         => $industryClassificationAttributes,
                    AbstractNormalizer::IGNORED_ATTRIBUTES => $ignoredAttributes,
                ]
            ),
            headers: ['Access-Control-Allow-Origin' => '*'],
            json: true
        );
    }

    #[Route('/industry-classifications/{industryClassificationId<\d{1,10}>}/industry-classifications', name: 'get_children_industry_classification_from_industry_classification', methods: ['GET'], format: 'json')]
    public function getChildrenIndustryClassificationFromIndustryClassification(int $industryClassificationId): JsonResponse
    {
        $parentIndustryClassification = $this->classificationService->fetchIndustryClassificationById(id: $industryClassificationId);

        if ($parentIndustryClassification === null) {
            throw new NotFoundHttpException();
        }

        $industryClassifications = $this->classificationService->fetchIndustryClassificationChildren(
            parentIndustryClassification: $parentIndustryClassification
        );

        $industryClassificationAttributes = [
            'id',
            'name',
            'levelOne',
            'levelTwo',
            'levelThree',
        ];

        return new JsonResponse(
            data: $this->serializer->serialize(
                data: $industryClassifications,
                format: 'json',
                context: [AbstractNormalizer::ATTRIBUTES => $industryClassificationAttributes]
            ),
            headers: ['Access-Control-Allow-Origin' => '*'],
            json: true
        );
    }
}
