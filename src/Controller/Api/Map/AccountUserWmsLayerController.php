<?php

declare(strict_types=1);

namespace App\Controller\Api\Map;

use App\Domain\Account\AccountUserDeletionService;
use App\Domain\Entity\WmsLayer;
use App\Domain\Map\MapService;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

#[Security("is_granted('ROLE_USER') or is_granted('ROLE_PROPERTY_FEEDER') or is_granted('ROLE_PROPERTY_PROVIDER') or is_granted('ROLE_VIEWER') or is_granted('ROLE_RESTRICTED_VIEWER')")]
#[Route('/api/map/account-user', name: 'api_map_account_user_')]
class AccountUserWmsLayerController extends AbstractController
{
    public function __construct(
        private readonly MapService $mapService,
        private readonly AccountUserDeletionService $accountUserDeletionService,
        private readonly SerializerInterface $serializer,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    #[Route('/wms-layers', name: 'get_wms_layers', methods: ['GET'], format: 'json')]
    public function getWmsLayers(UserInterface $user): JsonResponse
    {
        return new JsonResponse(
            data: $this->serializer->serialize(
                data: $user->getWmsLayers(),
                format: 'json'
            ),
            json: true
        );
    }

    #[Route('/wms-layers/{wmsLayerId<\d{1,10}>}', name: 'get_wms_layer', methods: ['GET'], format: 'json')]
    public function getWmsLayer(int $wmsLayerId, UserInterface $user): JsonResponse
    {
        $wmsLayer = $this->mapService->fetchWmsLayerById(id: $wmsLayerId);

        if ($wmsLayer === null) {
            throw new NotFoundHttpException();
        }

        if ($user->getWmsLayers()->contains(element: $wmsLayer) === false) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'view', subject: $wmsLayer);

        return new JsonResponse(
            data: $this->serializer->serialize(
                data: $wmsLayer,
                format: 'json'
            ),
            json: true
        );
    }

    #[Route('/wms-layers', name: 'post_wms_layer', methods: ['POST'], format: 'json')]
    public function postWmsLayer(Request $request, UserInterface $user): JsonResponse
    {
        $wmsLayer = $this->serializer->deserialize(
            data: $request->getContent(),
            type: WmsLayer::class,
            format: 'json',
            context: [AbstractNormalizer::IGNORED_ATTRIBUTES => ['id', 'createdAt', 'updatedAt']]
        );

        $this->entityManager->beginTransaction();

        $this->entityManager->persist(entity: $wmsLayer);

        $user->getWmsLayers()->add($wmsLayer);

        $this->entityManager->flush();
        $this->entityManager->commit();

        return new JsonResponse(
            data: $this->serializer->serialize(
                data: $wmsLayer,
                format: 'json'
            ),
            status: Response::HTTP_CREATED,
            json: true
        );
    }

    #[Route('/wms-layers/{wmsLayerId<\d{1,10}>}', name: 'put_wms_layer', methods: ['PUT'], format: 'json')]
    public function putWmsLayer(int $wmsLayerId, Request $request, UserInterface $user): JsonResponse
    {
        $wmsLayer = $this->mapService->fetchWmsLayerById(id: $wmsLayerId);

        if ($wmsLayer === null) {
            throw new NotFoundHttpException();
        }

        if ($user->getWmsLayers()->contains(element: $wmsLayer) === false) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'edit', subject: $wmsLayer);

        $wmsLayerFromRequest = $this->serializer->deserialize(
            data: $request->getContent(),
            type: WmsLayer::class,
            format: 'json',
            context: [AbstractNormalizer::IGNORED_ATTRIBUTES => ['id', 'createdAt', 'updatedAt']]
        );

        $wmsLayer->setTitle($wmsLayerFromRequest->getTitle());

        $this->entityManager->flush();

        return new JsonResponse(data: []);
    }

    #[Route('/wms-layers/{wmsLayerId<\d{1,10}>}', name: 'delete_wms_layer', methods: ['DELETE'], format: 'json')]
    public function deleteWmsLayer(int $wmsLayerId, UserInterface $user): JsonResponse
    {
        $wmsLayer = $this->mapService->fetchWmsLayerById(id: $wmsLayerId);

        if ($wmsLayer === null) {
            throw new NotFoundHttpException();
        }

        if ($user->getWmsLayers()->contains(element: $wmsLayer) === false) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'delete', subject: $wmsLayer);

        $this->accountUserDeletionService->deleteWmsLayer(accountUser: $user, wmsLayer: $wmsLayer);

        return new JsonResponse(data: []);
    }
}
