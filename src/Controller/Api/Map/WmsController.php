<?php

declare(strict_types=1);

namespace App\Controller\Api\Map;

use App\Domain\Map\MapService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

#[Security("is_granted('ROLE_USER') or is_granted('ROLE_PROPERTY_FEEDER') or is_granted('ROLE_PROPERTY_PROVIDER') or is_granted('ROLE_VIEWER') or is_granted('ROLE_RESTRICTED_VIEWER')")]
#[Route('/api/map', name: 'api_map_')]
class WmsController extends AbstractController
{
    public function __construct(
        private readonly MapService $mapService,
        private readonly SerializerInterface $serializer
    ) {
    }

    #[Route('/parser/wms-capabilities', name: 'parser_wms_capabilities', methods: ['POST', 'GET'], format: 'json')]
    public function parserWmsCapabilities(Request $request): JsonResponse
    {
        $requestJson = json_decode(json: $request->getContent());

        $wmsLayers = $this->mapService->parserWmsCapabilitiesFromUrl(url: $requestJson->url);

        return new JsonResponse(
            data: $this->serializer->serialize(data: $wmsLayers, format: 'json'),
            json: true
        );
    }
}
