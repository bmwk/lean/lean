<?php

declare(strict_types=1);

namespace App\Controller\Api\Map;

use App\Domain\Map\MapService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\SerializerInterface;

#[Security("is_granted('ROLE_USER') or is_granted('ROLE_PROPERTY_FEEDER') or is_granted('ROLE_VIEWER') or is_granted('ROLE_RESTRICTED_VIEWER')")]
#[Route('/api/map/account', name: 'api_map_account_')]
class AccountWmsLayerController extends AbstractController
{
    public function __construct(
        private readonly MapService $mapService,
        private readonly SerializerInterface $serializer
    ) {
    }

    #[Route('/wms-layers', name: 'get_wms_layers', methods: ['GET'], format: 'json')]
    public function getWmsLayers(UserInterface $user): JsonResponse
    {
        $account = $user->getAccount();

        return new JsonResponse(
            data: $this->serializer->serialize(
                data: $account->getWmsLayers(),
                format: 'json'
            ),
            json: true
        );
    }

    #[Route('/wms-layers/{wmsLayerId<\d{1,10}>}', name: 'get_wms_layer', methods: ['GET'], format: 'json')]
    public function getWmsLayer(int $wmsLayerId, UserInterface $user): JsonResponse
    {
        $account = $user->getAccount();

        $wmsLayer = $this->mapService->fetchWmsLayerById(id: $wmsLayerId);

        if ($wmsLayer === null) {
            throw new NotFoundHttpException();
        }

        if ($account->getWmsLayers()->contains(element: $wmsLayer) === false) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'view', subject: $wmsLayer);

        return new JsonResponse(
            data: $this->serializer->serialize(
                data: $wmsLayer,
                format: 'json'
            ),
            json: true
        );
    }
}
