<?php

declare(strict_types=1);

namespace App\Controller\Api\BusinessLocationArea;

use App\Domain\BusinessLocationArea\BusinessLocationAreaDeletionService;
use App\Domain\BusinessLocationArea\BusinessLocationAreaService;
use App\Domain\Entity\BusinessLocationArea;
use App\Domain\Entity\GeolocationPolygon;
use App\Domain\Entity\LocationCategory;
use CrEOF\Spatial\PHP\Types\Geometry\Polygon;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

#[Security("is_granted('ROLE_USER') or is_granted('ROLE_PROPERTY_FEEDER') or is_granted('ROLE_VIEWER') or is_granted('ROLE_RESTRICTED_VIEWER') or is_granted('ROLE_EARLY_WARNING_SYSTEM_VIEWER')")]
#[Route('/api/business-location-area', name: 'api_business_location_area_')]
class BusinessLocationAreaController extends AbstractController
{
    public function __construct(
        private readonly BusinessLocationAreaService $businessLocationAreaService,
        private readonly  BusinessLocationAreaDeletionService $businessLocationAreaDeletionService,
        private readonly SerializerInterface $serializer,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    #[Route('/business-location-areas', name: 'get_business_location_areas', methods: ['GET'], format: 'json')]
    public function getBusinessLocationAreas(UserInterface $user): JsonResponse
    {
        $account = $user->getAccount();

        $businessLocationAreas = $this->businessLocationAreaService->fetchBusinessLocationAreasByAccount(account: $account);

        $ignoredAttributes = [
            'account',
            'parentPlaces',
            'childrenPlaces',
            '__initializer__',
            '__cloner__',
            '__isInitialized__',
        ];

        return new JsonResponse(
            data: $this->serializer->serialize(
                data: $businessLocationAreas,
                format: 'json',
                context: [
                    AbstractNormalizer::IGNORED_ATTRIBUTES => $ignoredAttributes,
                ]
            ),
            json: true
        );
    }

    #[Route('/business-location-areas/{businessLocationAreaId<\d{1,10}>}', name: 'get_business_location_area', methods: ['GET'], format: 'json')]
    public function getBusinessLocationArea(int $businessLocationAreaId, UserInterface $user): JsonResponse
    {
        $account = $user->getAccount();

        $businessLocationArea = $this->businessLocationAreaService->fetchBusinessLocationAreaById(account: $account, id: $businessLocationAreaId);

        if ($businessLocationArea === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'view', subject: $businessLocationArea);

        $ignoredAttributes = [
            'account',
            'parentPlaces',
            'childrenPlaces',
            '__initializer__',
            '__cloner__',
            '__isInitialized__',
        ];

        return new JsonResponse(
            data: $this->serializer->serialize(
                data: $businessLocationArea,
                format: 'json',
                context: [
                    AbstractNormalizer::IGNORED_ATTRIBUTES => $ignoredAttributes,
                ]
            ),
            json: true
        );
    }

    #[Security("is_granted('ROLE_ACCOUNT_ADMIN')")]
    #[Route('/business-location-areas', name: 'post_business_location_area', methods: ['POST'], format: 'json')]
    public function postBusinessLocationArea(Request $request, UserInterface $user): JsonResponse
    {
        $account = $user->getAccount();

        $jsonContent = json_decode(json: $request->getContent(), associative: false);

        if (isset($jsonContent->locationCategory) === false || isset($jsonContent->polygonCoordinates) === false) {
            throw new BadRequestHttpException();
        }

        $locationCategory = LocationCategory::tryFrom((int)$jsonContent->locationCategory);

        if ($locationCategory === null) {
            throw new BadRequestHttpException();
        }

        $this->entityManager->beginTransaction();

        $businessLocationAreas = new BusinessLocationArea();

        $polygon = new Polygon($jsonContent->polygonCoordinates);

        $geolocationPolygon = new GeolocationPolygon();

        $geolocationPolygon->setPolygon($polygon);

        $this->entityManager->persist(entity: $geolocationPolygon);

        $businessLocationAreas
            ->setAccount($account)
            ->setLocationCategory($locationCategory)
            ->setGeolocationPolygon($geolocationPolygon);

        $this->entityManager->persist(entity: $businessLocationAreas);

        $this->entityManager->flush();
        $this->entityManager->commit();

        return new JsonResponse(data: [], status: Response::HTTP_CREATED);
    }

    #[Security("is_granted('ROLE_ACCOUNT_ADMIN')")]
    #[Route('/business-location-areas/{businessLocationAreaId<\d{1,10}>}', name: 'put_business_location_area', methods: ['PUT'], format: 'json')]
    public function putBusinessLocationArea(int $businessLocationAreaId, Request $request, UserInterface $user): JsonResponse
    {
        $account = $user->getAccount();

        $businessLocationArea = $this->businessLocationAreaService->fetchBusinessLocationAreaById(account: $account, id: $businessLocationAreaId);

        if ($businessLocationArea === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'edit', subject: $businessLocationArea);

        $jsonContent = json_decode(json: $request->getContent(), associative: false);

        if (isset($jsonContent->locationCategory) === false || isset($jsonContent->polygonCoordinates) === false) {
            throw new BadRequestHttpException();
        }

        $locationCategory = LocationCategory::tryFrom((int)$jsonContent->locationCategory);

        if ($locationCategory === null) {
            throw new BadRequestHttpException();
        }

        $this->entityManager->beginTransaction();

        $polygon = new Polygon($jsonContent->polygonCoordinates);

        $geolocationPolygon = $businessLocationArea->getGeolocationPolygon();

        $geolocationPolygon->setPolygon($polygon);

        $businessLocationArea->setLocationCategory($locationCategory);

        $this->entityManager->flush();
        $this->entityManager->commit();

        return new JsonResponse(data: [], status: Response::HTTP_OK);
    }

    #[Security("is_granted('ROLE_ACCOUNT_ADMIN')")]
    #[Route('/business-location-areas/{businessLocationAreaId<\d{1,10}>}', name: 'delete_business_location_area', methods: ['DELETE'], format: 'json')]
    public function deleteBusinessLocationArea(int $businessLocationAreaId, UserInterface $user): JsonResponse
    {
        $account = $user->getAccount();

        $businessLocationArea = $this->businessLocationAreaService->fetchBusinessLocationAreaById(account: $account, id: $businessLocationAreaId);

        if ($businessLocationArea === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'delete', subject: $businessLocationArea);

        $this->entityManager->beginTransaction();

        $this->businessLocationAreaDeletionService->deleteBusinessLocationArea(businessLocationArea: $businessLocationArea);

        $this->entityManager->commit();

        return new JsonResponse(data: [], status: Response::HTTP_OK);
    }
}
