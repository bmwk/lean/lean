<?php

declare(strict_types=1);

namespace App\Controller\Documentation\Api;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[IsGranted('PUBLIC_ACCESS')]
#[Route('/dokumentation/api', name: 'documentation_api_')]
class SettlementConceptController extends AbstractController
{
    #[Route('/settlement-concept', name: 'settlement_concept', methods: ['GET'])]
    public function settlementConcept(): Response
    {
        return $this->render(view: 'documentation/api/settlement_concept.html.twig');
    }

    #[Route('/settlement-concept.yaml', name: 'settlement_concept_api_documentation_yaml', methods: ['GET'])]
    public function settlementConceptApiDocumentationYaml(string $settlementConceptApiDocumentationFilePath): BinaryFileResponse
    {
        return new BinaryFileResponse(file: $settlementConceptApiDocumentationFilePath);
    }
}
