<?php

declare(strict_types=1);

namespace App\Controller\Help;

use App\Domain\Entity\CustomerCare\SupportRequest;
use App\Form\Type\CustomerCare\SupportRequestType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

#[IsGranted('IS_AUTHENTICATED')]
#[Route('/hilfe/kundenservice', name: 'help_customer_care_')]
class CustomerCareController extends AbstractController
{
    private const ACTIVE_NAV_ITEM = 'help';

    public function __construct(
        private readonly string $supportSystem,
        private readonly string $supportEmailAddress,
        private readonly string $supportPublicEmailAddress,
        private readonly string $supportPublicPhoneNumber,
        private readonly string $osTicketApiUrl,
        private readonly string $osTicketApiToken,
        private readonly string $noReplyEmailAddress,
        private readonly HttpClientInterface $httpClient,
        private readonly MailerInterface $mailer,
        private readonly SerializerInterface $serializer
    ) {
    }

    #[Route('/supportanfrage', name: 'support_request', methods: ['GET', 'POST'])]
    public function supportRequest(Request $request, UserInterface $user): Response
    {
        $supportRequest = new SupportRequest();

        $supportRequest
            ->setFullName($user->getFullName())
            ->setEmail($user->getEmail())
            ->setAccount($user->getAccount())
            ->setAccountUser($user);

        $supportRequestForm = $this->createForm(type: SupportRequestType::class, data: $supportRequest);

        $supportRequestForm->add(child: 'save', type: SubmitType::class);

        $supportRequestForm->handleRequest(request: $request);

        if ($supportRequestForm->isSubmitted() && $supportRequestForm->isValid()) {
            $supportRequest->setMessage('LeAn ' . $supportRequest->getAccount()->getName() . PHP_EOL . 'Account-ID: ' . $supportRequest->getAccount()->getId() . PHP_EOL . 'User-ID: ' . $supportRequest->getAccountUser()->getId() . PHP_EOL . PHP_EOL . '---' . PHP_EOL . $supportRequest->getSubject() . PHP_EOL . '---' . PHP_EOL . PHP_EOL . $supportRequest->getMessage());

            if ($this->supportSystem === 'os_ticket') {
                $attributes = [
                    'fullName',
                    'email',
                    'account' => [
                        'id',
                    ],
                    'accountUser' => [
                        'id',
                    ],
                    'subject',
                    'message',
                ];

                try {
                    $response = $this->httpClient->request(
                        method: 'POST',
                        url: $this->osTicketApiUrl,
                        options: [
                            'headers' => [
                                'X-API-Key: ' . $this->osTicketApiToken,
                                'Content-Type: application/json; charset=utf-8',
                            ],
                            'body' => $this->serializer->serialize(
                                data: $supportRequest,
                                format: 'json',
                                context: [AbstractNormalizer::ATTRIBUTES => $attributes]
                            ),
                        ]
                    );

                    if ($response->getStatusCode() === Response::HTTP_OK || $response->getStatusCode() === Response::HTTP_CREATED) {
                        $this->addFlash(type: 'dataSaved', message: 'Ihre Anfrage wurde erfolgreich gesendet.');

                        return $this->redirectToRoute(route: 'help_customer_care_support_request_messages_send');
                    } else {
                        $this->addFlash(type: 'dataError', message: 'Die Anfrage konnte nicht gesendet werden, bitte probieren Sie es erneut.');
                    }
                } catch (TransportExceptionInterface $transportException) {
                    $this->addFlash(type: 'dataError', message: 'Die Anfrage konnte nicht gesendet werden, bitte probieren Sie es erneut.');
                }
            } elseif ($this->supportSystem === 'email') {
                $email = (new TemplatedEmail())
                    ->from(addresses: $this->noReplyEmailAddress)
                    ->to(
                        addresses: new Address(
                            address: $this->supportEmailAddress,
                            name: 'LeAn-Service-Team'
                        )
                    )
                    ->subject(subject: 'LeAn ' . $supportRequest->getAccount()->getName() . ': Support-Anfrage von '. $supportRequest->getFullName())
                    ->htmlTemplate(template: 'help/email/customer_care/support_request_email.html.twig')
                    ->textTemplate(template: 'help/email/customer_care/support_request_email.txt.twig')
                    ->context(context: [
                        'supportRequest' => $supportRequest,
                        'account'        => $user->getAccount(),
                    ]);

                $this->mailer->send(message: $email);

                $this->addFlash(type: 'dataSaved', message: 'Ihre Anfrage wurde erfolgreich gesendet.');

                return $this->redirectToRoute(route: 'help_customer_care_support_request_messages_send');
            }
        }

        return $this->render(view: 'help/customer_care/support_request.html.twig', parameters: [
            'supportRequestForm'        => $supportRequestForm,
            'supportPublicEmailAddress' => $this->supportPublicEmailAddress,
            'supportPublicPhoneNumber'  => $this->supportPublicPhoneNumber,
            'pageTitle'                 => 'Kundenservice',
            'activeNavItem'             => self::ACTIVE_NAV_ITEM,
        ]);
    }

    #[Route('/kundenservice/nachricht-versandt', name: 'support_request_messages_send', methods: ['GET', 'POST'])]
    public function supportRequestMessageSend(): Response
    {
        return $this->render(view: 'help/customer_care/support_request_messages_send.html.twig', parameters: [
            'supportPublicEmailAddress' => $this->supportPublicEmailAddress,
            'supportPublicPhoneNumber'  => $this->supportPublicPhoneNumber,
            'pageTitle'                 => 'Nachricht gesendet',
            'activeNavItem'             => self::ACTIVE_NAV_ITEM,
        ]);
    }
}
