<?php

declare(strict_types=1);

namespace App\Controller\Help;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[IsGranted('IS_AUTHENTICATED')]
#[Route('/hilfe/faq', name: 'help_faq_')]
class FaqController extends AbstractController
{
    private const ACTIVE_NAV_ITEM = 'help';

    #[IsGranted('ROLE_PROPERTY_PROVIDER')]
    #[Route('/anbietende', name: 'property_provider', methods: ['GET'])]
    public function propertyProvider(): Response
    {
        return $this->render(view: 'help/faq/property_provider.html.twig', parameters: [
            'pageTitle'     => 'FAQ für Anbietende',
            'activeNavItem' => self::ACTIVE_NAV_ITEM,
        ]);
    }

    #[IsGranted('ROLE_PROPERTY_SEEKER')]
    #[Route('/suchende', name: 'property_seeker', methods: ['GET'])]
    public function propertySeeker(): Response
    {
        return $this->render(view: 'help/faq/property_seeker.html.twig', parameters: [
            'pageTitle'     => 'FAQ für Suchende',
            'activeNavItem' => self::ACTIVE_NAV_ITEM,
        ]);
    }
}
