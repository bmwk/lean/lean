<?php

declare(strict_types=1);

namespace App\Controller;

use App\Domain\SessionStoredUrlParameter\SessionStoredUrlParameterService;
use App\SessionStorage\SessionStorageService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

abstract class AbstractLeAnController extends AbstractController
{
    public function __construct(
        private readonly SessionStorageService $sessionStorageService,
        private readonly SessionStoredUrlParameterService $sessionStoredUrlParameterService
    ) {
    }

    protected function fetchObjectFromSession(string $dataClassName, string $sessionKeyName): ?object
    {
        return $this->sessionStorageService->fetchObject(dataClassName: $dataClassName, sessionKeyName: $sessionKeyName);
    }

    protected function storeObjectInSession(object $object, string $sessionKeyName): void
    {
        $this->sessionStorageService->storeObject(object: $object, sessionKeyName: $sessionKeyName);
    }

    protected function removeObjectFromSession(string $dataClassName, string $sessionKeyName): void
    {
        $this->sessionStorageService->removeObject(dataClassName: $dataClassName, sessionKeyName: $sessionKeyName);
    }

    protected function fetchMergedUrlParametersFromSession(array $urlParameterObjectClasses, string $sessionKeyName): array
    {
        return $this->sessionStoredUrlParameterService->fetchMergedUrlParameters(
            urlParameterObjectClasses: $urlParameterObjectClasses,
            sessionKeyName: $sessionKeyName
        );
    }
}
