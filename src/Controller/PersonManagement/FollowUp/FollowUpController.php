<?php

declare(strict_types=1);

namespace App\Controller\PersonManagement\FollowUp;

use App\Controller\AbstractLeAnController;
use App\Domain\Entity\PaginationParameter;
use App\Domain\Entity\SortingOption\SortingOption;
use App\Domain\Person\OccurrenceSecurityService;
use App\Domain\Person\PersonService;
use App\Domain\SessionStoredUrlParameter\SessionStoredUrlParameterService;
use App\SessionStorage\SessionStorageService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Security("is_granted('ROLE_USER') or is_granted('ROLE_PROPERTY_FEEDER') or is_granted('ROLE_LOOKING_FOR_PROPERTY_REQUEST_FEEDER') or is_granted('ROLE_VIEWER')")]
#[Route('/kontaktverwaltung/wiedervorlagen', name: 'person_management_follow_up_')]
class FollowUpController extends AbstractLeAnController
{
    private const ACTIVE_NAV_GROUP = 'person_management';
    private const ACTIVE_NAV_ITEM = self::ACTIVE_NAV_GROUP . '_follow_up';

    private const OVERVIEW_SESSION_KEY_NAME = 'personManagementFollowUpOverview';

    public function __construct(
        SessionStorageService $sessionStorageService,
        SessionStoredUrlParameterService $sessionStoredUrlParameterService,
        private readonly PersonService $personService,
        private readonly OccurrenceSecurityService $occurrenceSecurityService
    ) {
        parent::__construct(
            sessionStorageService: $sessionStorageService,
            sessionStoredUrlParameterService: $sessionStoredUrlParameterService
        );
    }

    #[Route('/uebersicht', name: 'overview', methods: ['POST', 'GET'])]
    public function followUpOverview(Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $paginationParameter = PaginationParameter::createFromRequest(request: $request);

        $this->storeObjectInSession(object: $paginationParameter, sessionKeyName: self::OVERVIEW_SESSION_KEY_NAME);

        $sortingOption = null;

        if ($request->query->get(key: 'sortieren') !== null) {
            $sortingOption = SortingOption::createFromRequest(request: $request);

            $this->storeObjectInSession(object: $sortingOption, sessionKeyName: self::OVERVIEW_SESSION_KEY_NAME);
        }

        if ($this->occurrenceSecurityService->canViewOnlyOwnOccurrences() === true) {
            $performedWithAccountUser = $user;
        } else {
            $performedWithAccountUser = null;
        }

        $occurrences = $this->personService->fetchOccurrencesPaginatedByAccount(
            account: $account,
            withFromSubAccounts: false,
            firstResult: $paginationParameter->getFirstResult(),
            maxResults: $paginationParameter->getLimit(),
            performedWithAccountUser: $performedWithAccountUser,
            followUp: true,
            followUpDone: false,
            sortingOption: $sortingOption
        );

        return $this->render(view: 'person_management/follow_up/overview.html.twig', parameters: [
            'occurrences'    => $occurrences,
            'pageTitle'      => 'Kontaktverwaltung - Wiedervorlagen',
            'activeNavGroup' => self::ACTIVE_NAV_GROUP,
            'activeNavItem'  => self::ACTIVE_NAV_ITEM,
        ]);
    }
}
