<?php

declare(strict_types=1);

namespace App\Controller\PersonManagement\Person;

use App\Domain\Person\PersonService;
use App\Domain\Property\BuildingUnitService;
use App\Domain\SessionStoredUrlParameter\SessionStoredUrlParameterService;
use App\SessionStorage\SessionStorageService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Security("is_granted('ROLE_USER') or is_granted('ROLE_PROPERTY_FEEDER') or is_granted('ROLE_VIEWER')")]
#[Route('/kontaktverwaltung', name: 'person_management_property_')]
class PropertyController extends AbstractPersonController
{
    private const ACTIVE_NAV_GROUP = 'person_management';
    private const ACTIVE_NAV_ITEM = self::ACTIVE_NAV_GROUP . '_person';

    public function __construct(
        SessionStorageService $sessionStorageService,
        SessionStoredUrlParameterService $sessionStoredUrlParameterService,
        private readonly PersonService $personService,
        private readonly BuildingUnitService $buildingUnitService
    ) {
        parent::__construct(
            sessionStorageService: $sessionStorageService,
            sessionStoredUrlParameterService: $sessionStoredUrlParameterService
        );
    }

    #[Route('/kontakte/{personId<\d{1,10}>}/objekte', name: 'overview', methods: ['GET'])]
    public function overview(int $personId, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $person = $this->personService->fetchPersonById(account: $account, id: $personId);

        if ($person === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'view', subject: $person);

        $buildingUnits = $this->buildingUnitService->fetchBuildingUnitsByConnectedPerson(
            account: $account,
            withFromSubAccounts: true,
            person: $person,
            archived: false
        );

        $archivedBuildingUnits = $this->buildingUnitService->fetchBuildingUnitsByConnectedPerson(
            account: $account,
            withFromSubAccounts: true,
            person: $person,
            archived: true
        );

        return $this->render(view: 'person_management/person/property/overview.html.twig', parameters: [
            'buildingUnits'         => $buildingUnits,
            'archivedBuildingUnits' => $archivedBuildingUnits,
            'person'                => $person,
            'pageTitle'             => 'Kontaktverwaltung - zugeordnete Objekte',
            'activeNavGroup'        => self::ACTIVE_NAV_GROUP,
            'activeNavItem'         => self::ACTIVE_NAV_ITEM,
        ]);
    }
}
