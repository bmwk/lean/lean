<?php

declare(strict_types=1);

namespace App\Controller\PersonManagement\Person;

use App\Domain\LookingForPropertyRequest\LookingForPropertyRequestService;
use App\Domain\Person\PersonService;
use App\Domain\SessionStoredUrlParameter\SessionStoredUrlParameterService;
use App\SessionStorage\SessionStorageService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Security("is_granted('ROLE_USER') or is_granted('ROLE_LOOKING_FOR_PROPERTY_REQUEST_FEEDER') or is_granted('ROLE_VIEWER')")]
#[Route('/kontaktverwaltung', name: 'person_management_looking_for_property_request_')]
class LookingForPropertyRequestController extends AbstractPersonController
{
    private const ACTIVE_NAV_GROUP = 'person_management';
    private const ACTIVE_NAV_ITEM = self::ACTIVE_NAV_GROUP . '_person';

    public function __construct(
        SessionStorageService $sessionStorageService,
        SessionStoredUrlParameterService $sessionStoredUrlParameterService,
        private readonly PersonService $personService,
        private readonly LookingForPropertyRequestService $lookingForPropertyRequestService
    ) {
        parent::__construct(
            sessionStorageService: $sessionStorageService,
            sessionStoredUrlParameterService: $sessionStoredUrlParameterService
        );
    }

    #[Route('/kontakte/{personId<\d{1,10}>}/gesuche', name: 'overview', methods: ['GET'])]
    public function overview(int $personId, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $person = $this->personService->fetchPersonById(account: $account, id: $personId);

        if ($person === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'view', subject: $person);

        $lookingForPropertyRequests = $this->lookingForPropertyRequestService->fetchLookingForPropertyRequestsByPerson(
            account: $account,
            withFromSubAccounts: false,
            person: $person,
            archived: false
        );

        return $this->render(view: 'person_management/person/looking_for_property_request/overview.html.twig', parameters: [
            'lookingForPropertyRequests' => $lookingForPropertyRequests,
            'person'                     => $person,
            'pageTitle'                  => 'Kontaktverwaltung - zugeordnete Gesuche',
            'activeNavGroup'             => self::ACTIVE_NAV_GROUP,
            'activeNavItem'              => self::ACTIVE_NAV_ITEM,
        ]);
    }
}
