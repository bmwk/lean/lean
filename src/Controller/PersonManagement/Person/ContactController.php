<?php

declare(strict_types=1);

namespace App\Controller\PersonManagement\Person;

use App\Domain\Entity\DeletionType;
use App\Domain\Entity\PaginationParameter;
use App\Domain\Entity\Person\Contact;
use App\Domain\Entity\SortingOption\SortingOption;
use App\Domain\Person\PersonDeletionService;
use App\Domain\Person\PersonService;
use App\Domain\SessionStoredUrlParameter\SessionStoredUrlParameterService;
use App\Form\Type\PersonManagement\Contact\ContactDeleteType;
use App\Form\Type\PersonManagement\Contact\ContactType;
use App\SessionStorage\SessionStorageService;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Security("is_granted('ROLE_USER')
or is_granted('ROLE_PROPERTY_FEEDER')
or is_granted('ROLE_LOOKING_FOR_PROPERTY_REQUEST_FEEDER')
or is_granted('ROLE_VIEWER')")]
#[Route('/kontaktverwaltung', name: 'person_management_contact_')]
class ContactController extends AbstractPersonController
{
    private const ACTIVE_NAV_GROUP = 'person_management';
    private const ACTIVE_NAV_ITEM = self::ACTIVE_NAV_GROUP . '_person';

    private const OVERVIEW_ROUTE_NAME = 'person_management_contact_overview';

    private const OVERVIEW_SESSION_KEY_NAME = 'personManagementContactOverview';

    private const URL_PARAMETER_OBJECT_CLASSES = [
        PaginationParameter::class,
        SortingOption::class,
    ];

    public function __construct(
        SessionStorageService $sessionStorageService,
        SessionStoredUrlParameterService $sessionStoredUrlParameterService,
        private readonly PersonService $personService,
        private readonly PersonDeletionService $personDeletionService,
        private readonly EntityManagerInterface $entityManager
    ) {
        parent::__construct(
            sessionStorageService: $sessionStorageService,
            sessionStoredUrlParameterService: $sessionStoredUrlParameterService
        );
    }

    #[Route('/kontakte/{personId<\d{1,10}>}/ansprechpersonen', name: 'overview', methods: ['GET'])]
    public function overview(int $personId, Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $person = $this->personService->fetchPersonById(account: $account, id: $personId);

        if ($person === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'view', subject: $person);

        $paginationParameter = PaginationParameter::createFromRequest(request: $request);

        $this->storeObjectInSession(object: $paginationParameter, sessionKeyName: self::OVERVIEW_SESSION_KEY_NAME);

        $sortingOption = null;

        if ($request->query->get(key: 'sortieren') !== null) {
            $sortingOption = SortingOption::createFromRequest(request: $request);

            $this->storeObjectInSession(object: $sortingOption, sessionKeyName: self::OVERVIEW_SESSION_KEY_NAME);
        }

        $contacts = $this->personService->fetchPaginatedContactsByPerson(
            person: $person,
            firstResult: $paginationParameter->getFirstResult(),
            maxResults: $paginationParameter->getLimit(),
            sortingOption: $sortingOption
        );

        return $this->render(view: 'person_management/person/contact/overview.html.twig', parameters: [
            'person'         => $person,
            'contacts'       => $contacts,
            'pageTitle'      => 'Kontaktverwaltung - Ansprechpersonen',
            'activeNavGroup' => self::ACTIVE_NAV_GROUP,
            'activeNavItem'  => self::ACTIVE_NAV_ITEM,
        ]);
    }

    #[Security("is_granted('ROLE_USER') or is_granted('ROLE_PROPERTY_FEEDER') or is_granted('ROLE_LOOKING_FOR_PROPERTY_REQUEST_FEEDER')")]
    #[Route('/kontakte/{personId<\d{1,10}>}/ansprechperson/anlegen', name: 'create', methods: ['GET', 'POST'])]
    public function create(int $personId, Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $person = $this->personService->fetchPersonById(account: $account, id: $personId);

        if ($person === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'edit', subject: $person);

        $contact = new Contact();

        $contactForm = $this->createForm(type: ContactType::class, data: $contact, options: [
            'canEdit' => true,
        ]);

        $contactForm->add(child: 'save', type: SubmitType::class);

        $contactForm->handleRequest(request: $request);

        if ($contactForm->isSubmitted() && $contactForm->isValid()) {
            $contact
                ->setPerson($person)
                ->setAccount($account)
                ->setCreatedByAccountUser($user)
                ->setDeleted(false)
                ->setAnonymized(false);

            $this->entityManager->persist(entity: $contact);

            $this->entityManager->flush();

            $this->addFlash(type: 'dataSaved', message: 'Die Ansprechperson wurden angelegt.');

            return $this->redirectToRoute(route: 'person_management_contact_overview', parameters: ['personId' => $person->getId()]);
        }

        return $this->render(view: 'person_management/person/contact/create.html.twig', parameters: [
            'contactForm'    => $contactForm,
            'person'         => $person,
            'pageTitle'      => 'Kontaktverwaltung - Ansprechperson anlegen',
            'activeNavGroup' => self::ACTIVE_NAV_GROUP,
            'activeNavItem'  => self::ACTIVE_NAV_ITEM,
        ]);
    }

    #[Route('/kontakte/{personId<\d{1,10}>}/ansprechpersonen/{contactId<\d{1,10}>}', name: 'edit', methods: ['GET', 'POST'])]
    public function edit(int $personId, int $contactId, Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $person = $this->personService->fetchPersonById(account: $account, id: $personId);

        if ($person === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'view', subject: $person);

        $contact = $this->personService->fetchContactByIdAndPerson(id: $contactId, person: $person, accounts: [$account]);

        if ($contact === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'view', subject: $contact);

        $contactForm = $this->createForm(type: ContactType::class, data: $contact, options: [
            'canEdit' => $this->isGranted(attribute: 'edit', subject: $contact),
        ]);

        $contactForm->add(child: 'save', type: SubmitType::class);

        $contactForm->handleRequest(request: $request);

        if ($contactForm->isSubmitted() && $contactForm->isValid()) {
            $this->denyAccessUnlessGranted(attribute: 'edit', subject: $person);
            $this->denyAccessUnlessGranted(attribute: 'edit', subject: $contact);

            $this->entityManager->flush();

            $this->addFlash(type: 'dataSaved', message: 'Die Daten wurden gespeichert.');

            return $this->redirectToRoute(route: 'person_management_contact_overview', parameters: ['personId' => $person->getId()]);
        }

        return $this->render(view: 'person_management/person/contact/edit.html.twig', parameters: [
            'contactForm'    => $contactForm,
            'contact'        => $contact,
            'person'         => $person,
            'pageTitle'      => 'Kontaktverwaltung - Ansprechperson editieren',
            'activeNavGroup' => self::ACTIVE_NAV_GROUP,
            'activeNavItem'  => self::ACTIVE_NAV_ITEM,
        ]);
    }

    #[Security("is_granted('ROLE_USER') or is_granted('ROLE_PROPERTY_FEEDER') or is_granted('ROLE_LOOKING_FOR_PROPERTY_REQUEST_FEEDER')")]
    #[Route('/kontakte/{personId<\d{1,10}>}/ansprechperson/{contactId<\d{1,10}>}/loeschen', name: 'delete', methods: ['GET', 'POST'])]
    public function delete(int $personId, int $contactId, Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $person = $this->personService->fetchPersonById(account: $account, id: $personId);

        if ($person === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'edit', subject: $person);

        $contact = $this->personService->fetchContactByIdAndPerson(id: $contactId, person: $person, accounts: [$account]);

        if ($contact === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'delete', subject: $contact);

        $contactForm = $this->createForm(type: ContactType::class, data: $contact, options: [
            'canEdit' => false,
        ]);

        $contactDeleteForm = $this->createForm(type: ContactDeleteType::class);

        $contactDeleteForm->add(child: 'delete', type: SubmitType::class);

        $contactDeleteForm->handleRequest(request: $request);

        if (
            $contactDeleteForm->isSubmitted()
            && $contactDeleteForm->isValid()
            && $contactDeleteForm->get('verificationCode')->getData() === 'ansprechperson_' . $contact->getId()
        ) {
            $this->denyAccessUnlessGranted(attribute: 'delete', subject: $contact);

            $this->personDeletionService->deleteContact(contact: $contact, deletionType: DeletionType::ANONYMIZATION, deletedByAccountUser: $user);

            $this->addFlash(type: 'dataDeleted', message: 'Die Ansprechperson wurden gelöscht.');

            return $this->redirectToRoute(route: 'person_management_contact_overview', parameters: ['personId' => $person->getId()]);
        }

        return $this->render(view: 'person_management/person/contact/delete.html.twig', parameters: [
            'contactDeleteForm' => $contactDeleteForm,
            'contactForm'       => $contactForm,
            'contact'           => $contact,
            'person'            => $person,
            'pageTitle'         => 'Kontaktverwaltung - Ansprechperson löschen',
            'activeNavGroup'    => self::ACTIVE_NAV_GROUP,
            'activeNavItem'     => self::ACTIVE_NAV_ITEM,
        ]);
    }
}
