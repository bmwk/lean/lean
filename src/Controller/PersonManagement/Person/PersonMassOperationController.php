<?php

declare(strict_types=1);

namespace App\Controller\PersonManagement\Person;

use App\Domain\Entity\DeletionType;
use App\Domain\Entity\Person\Person;
use App\Domain\Person\PersonDeletionService;
use App\Domain\Person\PersonService;
use App\Domain\SessionStoredUrlParameter\SessionStoredUrlParameterService;
use App\Form\Type\PersonManagement\Person\PersonMassOperationDeleteType;
use App\SessionStorage\SessionStorageService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Security("is_granted('ROLE_USER') or is_granted('ROLE_PROPERTY_FEEDER') or is_granted('ROLE_LOOKING_FOR_PROPERTY_REQUEST_FEEDER')")]
#[Route('/kontaktverwaltung', name: 'person_management_person_mass_operation_')]
class PersonMassOperationController extends AbstractPersonController
{
    private const ACTIVE_NAV_GROUP = 'person_management';
    private const ACTIVE_NAV_ITEM = self::ACTIVE_NAV_GROUP . '_person';

    public function __construct(
        SessionStorageService $sessionStorageService,
        SessionStoredUrlParameterService $sessionStoredUrlParameterService,
        private readonly PersonService $personService,
        private readonly PersonDeletionService $personDeletionService
    ) {
        parent::__construct(
            sessionStorageService: $sessionStorageService,
            sessionStoredUrlParameterService: $sessionStoredUrlParameterService
        );
    }

    #[Route('/kontakte/massenoperation/loeschen', name: 'delete', methods: ['POST'])]
    public function delete(Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $personMassOperationDeleteForm = $this->createForm(type: PersonMassOperationDeleteType::class);

        $personMassOperationDeleteForm->add(child: 'delete', type: SubmitType::class);

        $personMassOperationDeleteForm->handleRequest(request: $request);

        if ($request->get('selectedRowIds') !== null) {
            $personMassOperationDeleteForm->get('selectedRowIds')->setData($request->get('selectedRowIds'));
        }

        $persons = $this->personService->fetchPersonsByIds(
            account: $account,
            ids: json_decode($personMassOperationDeleteForm->get('selectedRowIds')->getData())
        );

        $persons = array_filter(
            array: $persons,
            callback: function (Person $person): bool {
                return $this->isGranted(attribute: 'delete', subject: $person);
            }
        );

        if (
            $personMassOperationDeleteForm->isSubmitted()
            && $personMassOperationDeleteForm->isValid()
            && $personMassOperationDeleteForm->get('verificationCode')->getData() === 'kontakte_' . count($persons)
        ) {
            foreach ($persons as $person) {
                if ($this->isGranted(attribute: 'delete', subject: $person) === false) {
                    continue;
                }

                $this->personDeletionService->deletePerson(person: $person, deletionType: DeletionType::ANONYMIZATION, deletedByAccountUser: $user);
            }

            $this->addFlash(type: 'dataDeleted', message: 'Die Kontakte wurden gelöscht.');

            return $this->redirectToRoute(route: 'person_management_person_overview');
        }

        return $this->render(view: 'person_management/person/mass_operation/delete.html.twig', parameters: [
            'personMassOperationDeleteForm' => $personMassOperationDeleteForm,
            'persons'                       => $persons,
            'pageTitle'                     => 'Kontaktverwaltung - Kontakte löschen',
            'activeNavGroup'                => self::ACTIVE_NAV_GROUP,
            'activeNavItem'                 => self::ACTIVE_NAV_ITEM,
        ]);
    }
}
