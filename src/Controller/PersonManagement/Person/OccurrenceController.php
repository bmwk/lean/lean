<?php

declare(strict_types=1);

namespace App\Controller\PersonManagement\Person;

use App\Domain\Entity\DeletionType;
use App\Domain\Entity\PaginationParameter;
use App\Domain\Entity\Person\Occurrence;
use App\Domain\Entity\SortingOption\SortingOption;
use App\Domain\Person\OccurrenceDeletionService;
use App\Domain\Person\OccurrenceSecurityService;
use App\Domain\Person\PersonService;
use App\Domain\SessionStoredUrlParameter\SessionStoredUrlParameterService;
use App\Form\Type\PersonManagement\Occurrence\OccurrenceDeleteType;
use App\Form\Type\PersonManagement\Person\PersonOccurrenceType;
use App\SessionStorage\SessionStorageService;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Security("is_granted('ROLE_USER') or is_granted('ROLE_PROPERTY_FEEDER') or is_granted('ROLE_LOOKING_FOR_PROPERTY_REQUEST_FEEDER') or is_granted('ROLE_VIEWER')")]
#[Route('/kontaktverwaltung', name: 'person_management_occurrence_')]
class OccurrenceController extends AbstractPersonController
{
    private const ACTIVE_NAV_GROUP = 'person_management';
    private const ACTIVE_NAV_ITEM = self::ACTIVE_NAV_GROUP . '_person';

    private const OVERVIEW_ROUTE_NAME = 'person_management_occurrence_overview';

    private const OVERVIEW_SESSION_KEY_NAME = 'personManagementOccurrenceOverview';

    private const URL_PARAMETER_OBJECT_CLASSES = [
        PaginationParameter::class,
        SortingOption::class,
    ];

    public function __construct(
        SessionStorageService $sessionStorageService,
        SessionStoredUrlParameterService $sessionStoredUrlParameterService,
        private readonly PersonService $personService,
        private readonly OccurrenceDeletionService $occurrenceDeletionService,
        private readonly OccurrenceSecurityService $occurrenceSecurityService,
        private readonly EntityManagerInterface $entityManager
    ) {
        parent::__construct(
            sessionStorageService: $sessionStorageService,
            sessionStoredUrlParameterService: $sessionStoredUrlParameterService
        );
    }

    #[Route('/kontakte/{personId<\d{1,10}>}/vorgaenge', name: 'overview', methods: ['GET'])]
    public function overview(int $personId, Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $person = $this->personService->fetchPersonById(account: $account, id: $personId);

        if ($person === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'view', subject: $person);

        $paginationParameter = PaginationParameter::createFromRequest(request: $request);

        $this->storeObjectInSession(object: $paginationParameter, sessionKeyName: self::OVERVIEW_SESSION_KEY_NAME);

        $sortingOption = null;

        if ($request->query->get(key: 'sortieren') !== null) {
            $sortingOption = SortingOption::createFromRequest(request: $request);

            $this->storeObjectInSession(object: $sortingOption, sessionKeyName: self::OVERVIEW_SESSION_KEY_NAME);
        }

        if ($this->occurrenceSecurityService->canViewOnlyOwnOccurrences() === true) {
            $performedWithAccountUser = $user;
        } else {
            $performedWithAccountUser = null;
        }

        $occurrences = $this->personService->fetchPaginatedOccurrencesByPerson(
            account: $account,
            withFromSubAccounts: false,
            person: $person,
            firstResult: $paginationParameter->getFirstResult(),
            maxResults: $paginationParameter->getLimit(),
            performedWithAccountUser: $performedWithAccountUser,
            sortingOption: $sortingOption
        );

        return $this->render(view: 'person_management/person/occurrence/overview.html.twig', parameters: [
            'person'         => $person,
            'occurrences'    => $occurrences,
            'pageTitle'      => 'Kontaktverwaltung - Vorgänge',
            'activeNavGroup' => self::ACTIVE_NAV_GROUP,
            'activeNavItem'  => self::ACTIVE_NAV_ITEM,
        ]);
    }

    #[Security("is_granted('ROLE_USER') or is_granted('ROLE_PROPERTY_FEEDER') or is_granted('ROLE_LOOKING_FOR_PROPERTY_REQUEST_FEEDER')")]
    #[Route('/kontakte/{personId<\d{1,10}>}/vorgaenge/anlegen', name: 'create', methods: ['POST', 'GET'])]
    public function create(int $personId, Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $person = $this->personService->fetchPersonById(account: $account, id: $personId);

        if ($person === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'edit', subject: $person);

        $occurrence = new Occurrence();

        $occurrence->setPerson($person);

        $occurrenceForm = $this->createForm(type: PersonOccurrenceType::class, data: $occurrence, options: [
            'persons'     => [$person],
            'accountUser' => $user,
            'canEdit'     => true,
        ]);

        $occurrenceForm->add(child: 'save', type: SubmitType::class);

        $occurrenceForm->handleRequest(request: $request);

        if ($occurrenceForm->isSubmitted() && $occurrenceForm->isValid()) {
            $this->denyAccessUnlessGranted(attribute: 'edit', subject: $person);

            $occurrence
                ->setAccount($account)
                ->setCreatedByAccountUser($user)
                ->setPerformedWithAccountUser($user)
                ->setDeleted(false)
                ->setAnonymized(false);

            $this->entityManager->persist(entity: $occurrence);
            $this->entityManager->flush();

            $this->addFlash(type: 'dataSaved', message: 'Der Vorgang wurden eingetragen.');

            $urlParameters = $this->fetchMergedUrlParametersFromSession(
                urlParameterObjectClasses: self::URL_PARAMETER_OBJECT_CLASSES,
                sessionKeyName: self::OVERVIEW_SESSION_KEY_NAME
            );

            return $this->redirectToRoute(
                route: self::OVERVIEW_ROUTE_NAME,
                parameters: array_merge(['personId' => $person->getId()], $urlParameters)
            );
        }

        return $this->render(view: 'person_management/person/occurrence/create.html.twig', parameters: [
            'occurrenceForm' => $occurrenceForm,
            'person'         => $person,
            'pageTitle'      => 'Kontaktverwaltung - Vorgang eintragen',
            'activeNavGroup' => self::ACTIVE_NAV_GROUP,
            'activeNavItem'  => self::ACTIVE_NAV_ITEM,
        ]);
    }

    #[Route('/kontakte/{personId<\d{1,10}>}/vorgaenge/{occurrenceId<\d{1,10}>}', name: 'edit', methods: ['POST', 'GET'])]
    public function edit(int $personId, int $occurrenceId, Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $person = $this->personService->fetchPersonById(account: $account, id: $personId);

        if ($person === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'view', subject: $person);

        $occurrence = $this->personService->fetchOccurrenceByIdAndPerson(
            account: $account,
            withFromSubAccounts: false,
            id: $occurrenceId,
            person: $person
        );

        if ($occurrence === null) {
            throw new NotFoundHttpException();
        }

        $occurrenceForm = $this->createForm(type: PersonOccurrenceType::class, data: $occurrence, options: [
            'persons'     => [$person],
            'accountUser' => $occurrence->getPerformedWithAccountUser(),
            'canEdit'     => $this->isGranted(attribute: 'edit', subject: $occurrence),
        ]);

        $this->denyAccessUnlessGranted(attribute: 'view', subject: $occurrence);

        $occurrenceForm->add(child: 'save', type: SubmitType::class);

        $occurrenceForm->handleRequest(request: $request);

        if ($occurrenceForm->isSubmitted() && $occurrenceForm->isValid()) {
            $this->denyAccessUnlessGranted(attribute: 'edit', subject: $person);
            $this->denyAccessUnlessGranted(attribute: 'edit', subject: $occurrence);

            $this->entityManager->flush();

            $this->addFlash(type: 'dataSaved', message: 'Die Änderungen wurden gespeichert.');

            $urlParameters = $this->fetchMergedUrlParametersFromSession(
                urlParameterObjectClasses: self::URL_PARAMETER_OBJECT_CLASSES,
                sessionKeyName: self::OVERVIEW_SESSION_KEY_NAME
            );

            return $this->redirectToRoute(
                route: self::OVERVIEW_ROUTE_NAME,
                parameters: array_merge(['personId' => $person->getId()], $urlParameters)
            );
        }

        return $this->render(view: 'person_management/person/occurrence/edit.html.twig', parameters: [
            'occurrenceForm' => $occurrenceForm,
            'occurrence'     => $occurrence,
            'person'         => $person,
            'pageTitle'      => 'Kontaktverwaltung - Vorgang bearbeiten',
            'activeNavGroup' => self::ACTIVE_NAV_GROUP,
            'activeNavItem'  => self::ACTIVE_NAV_ITEM,
        ]);
    }

    #[Route('/kontakte/{personId<\d{1,10}>}/vorgaenge/{occurrenceId<\d{1,10}>}/loeschen', name: 'delete', methods: ['POST', 'GET'])]
    public function delete(int $personId, int $occurrenceId, Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $person = $this->personService->fetchPersonById(account: $account, id: $personId);

        if ($person === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'view', subject: $person);

        $occurrence = $this->personService->fetchOccurrenceByIdAndPerson(
            account: $account,
            withFromSubAccounts: false,
            id: $occurrenceId,
            person: $person
        );

        if ($occurrence === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'delete', subject: $occurrence);

        $occurrenceDeleteForm = $this->createForm(type: OccurrenceDeleteType::class);

        $occurrenceDeleteForm->add(child: 'delete', type: SubmitType::class);

        $occurrenceDeleteForm->handleRequest(request: $request);

        if ($occurrenceDeleteForm->isSubmitted() && $occurrenceDeleteForm->isValid()) {
            $formData = $occurrenceDeleteForm->getData();

            if ($formData['verificationCode'] === 'vorgang_' . $occurrence->getId()) {
                $this->denyAccessUnlessGranted(attribute: 'edit', subject: $person);
                $this->denyAccessUnlessGranted(attribute: 'delete', subject: $occurrence);

                $this->occurrenceDeletionService->deleteOccurrence(occurrence: $occurrence, deletionType: DeletionType::SOFT, deletedByAccountUser: $user);

                $this->addFlash(type: 'dataDeleted', message: 'Die Notiz wurde gelöscht.');

                $urlParameters = $this->fetchMergedUrlParametersFromSession(
                    urlParameterObjectClasses: self::URL_PARAMETER_OBJECT_CLASSES,
                    sessionKeyName: self::OVERVIEW_SESSION_KEY_NAME
                );

                return $this->redirectToRoute(route: self::OVERVIEW_ROUTE_NAME, parameters: array_merge(['personId' => $person->getId()], $urlParameters));
            } else {
                $this->addFlash(type: 'dataError', message: 'Der Verifizierungscode war falsch.');
            }
        }

        $occurrenceForm = $this->createForm(type: PersonOccurrenceType::class, data: $occurrence, options: [
            'persons'     => [$person],
            'accountUser' => $occurrence->getPerformedWithAccountUser(),
            'canEdit'     => false,
        ]);

        return $this->render(view: 'person_management/person/occurrence/delete.html.twig', parameters: [
            'occurrenceDeleteForm' => $occurrenceDeleteForm,
            'occurrenceForm'       => $occurrenceForm,
            'occurrence'           => $occurrence,
            'person'               => $person,
            'pageTitle'            => 'Kontaktverwaltung - Vorgang bearbeiten',
            'activeNavGroup'       => self::ACTIVE_NAV_GROUP,
            'activeNavItem'        => self::ACTIVE_NAV_ITEM,
        ]);
    }

    #[Route('/vorgaenge/{occurrenceId<\d{1,10}>}/anhaenge/upload', name: 'attachment_upload', methods: ['POST'])]
    public function attachmentUpload(int $occurrenceId, Request $request, UserInterface $user): JsonResponse
    {
        $account = $user->getAccount();

        $occurrence = $this->personService->fetchOccurrenceById(account: $account, withFromSubAccounts: false, id: $occurrenceId);

        if ($occurrence === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'edit', subject: $occurrence->getPerson());

        $occurrenceAttachmentFile = $request->files->get('occurrenceAttachmentFile');

        if ($occurrenceAttachmentFile === null) {
            throw new BadRequestHttpException();
        }

        $allowedFileMimeTypes = [
            'application/pdf',
            'application/msword',
            'application/msexcel',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        ];

        if (in_array($occurrenceAttachmentFile->getMimeType(), $allowedFileMimeTypes) !== true) {
            throw new BadRequestHttpException();
        }

        $occurrenceAttachment = $this->personService->createAndSaveOccurrenceAttachmentFromUploadedFile(
            uploadedFile: $occurrenceAttachmentFile,
            accountUser: $user,
            occurrence: $occurrence
        );

        $flashBag = $request->getSession()->getFlashBag();

        if ($flashBag->has(type: 'dataSaved') === false) {
            $this->addFlash(type: 'dataSaved', message: 'Die Anlage(n) wurde(n) hochgeladen.');
        }

        return new JsonResponse(data: ['success' => true]);
    }
}
