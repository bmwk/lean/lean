<?php

declare(strict_types=1);

namespace App\Controller\PersonManagement\Person;

use App\Domain\Entity\DeletionType;
use App\Domain\Entity\PaginationParameter;
use App\Domain\Entity\Person\PersonFilter;
use App\Domain\Entity\Person\PersonSearch;
use App\Domain\Entity\Person\PersonType;
use App\Domain\Entity\SortingOption\SortingOption;
use App\Domain\Person\PersonDeletionService;
use App\Domain\Person\PersonSecurityService;
use App\Domain\Person\PersonService;
use App\Domain\SessionStoredUrlParameter\SessionStoredUrlParameterService;
use App\Form\Type\PersonManagement\Person\CommuneType;
use App\Form\Type\PersonManagement\Person\CompanyType;
use App\Form\Type\PersonManagement\Person\NaturalPersonType;
use App\Form\Type\PersonManagement\Person\PersonCreateType;
use App\Form\Type\PersonManagement\Person\PersonDeleteType;
use App\Form\Type\PersonManagement\Person\PersonFilterType;
use App\Form\Type\PersonManagement\Person\PersonSearchType;
use App\SessionStorage\SessionStorageService;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Security("is_granted('ROLE_USER')
or is_granted('ROLE_PROPERTY_FEEDER')
or is_granted('ROLE_LOOKING_FOR_PROPERTY_REQUEST_FEEDER')
or is_granted('ROLE_VIEWER')")]
#[Route('/kontaktverwaltung', name: 'person_management_person_')]
class PersonController extends AbstractPersonController
{
    private const ACTIVE_NAV_GROUP = 'person_management';
    private const ACTIVE_NAV_ITEM = self::ACTIVE_NAV_GROUP . '_person';

    private const OVERVIEW_ROUTE_NAME = 'person_management_person_overview';

    private const OVERVIEW_SESSION_KEY_NAME = 'personOverview';

    private const URL_PARAMETER_OBJECT_CLASSES = [
        PaginationParameter::class,
        SortingOption::class,
    ];

    public function __construct(
        SessionStorageService $sessionStorageService,
        SessionStoredUrlParameterService $sessionStoredUrlParameterService,
        private readonly PersonDeletionService $personDeletionService,
        private readonly PersonSecurityService $personSecurityService,
        private readonly PersonService $personService,
        private readonly EntityManagerInterface $entityManager
    ) {
        parent::__construct(
            sessionStorageService: $sessionStorageService,
            sessionStoredUrlParameterService: $sessionStoredUrlParameterService
        );
    }

    #[Route('/kontakte/uebersicht', name: 'overview', methods: ['GET', 'POST'])]
    public function overview(Request $request, UserInterface $user): Response
    {
        $paginationParameter = PaginationParameter::createFromRequest(request: $request);

        $this->storeObjectInSession(object: $paginationParameter, sessionKeyName: self::OVERVIEW_SESSION_KEY_NAME);

        $sortingOption = null;

        if ($request->query->get(key: 'sortieren') !== null) {
            $sortingOption = SortingOption::createFromRequest(request: $request);

            $this->storeObjectInSession(object: $sortingOption, sessionKeyName: self::OVERVIEW_SESSION_KEY_NAME);
        }

        $personFilterForm = $this->createForm(type: PersonFilterType::class, options: [
            'dataClassName'  => PersonFilter::class,
            'sessionKeyName' => self::OVERVIEW_SESSION_KEY_NAME,
        ]);

        $personFilterForm->add(child: 'apply', type: SubmitType::class);

        $personFilterForm->handleRequest(request: $request);

        $personSearchForm = $this->createForm(type: PersonSearchType::class, options: [
            'dataClassName'  => PersonSearch::class,
            'sessionKeyName' => self::OVERVIEW_SESSION_KEY_NAME,
        ]);

        $personSearchForm->add(child: 'search', type: SubmitType::class);

        $personSearchForm->handleRequest(request: $request);

        $account = $user->getAccount();

        if ($this->personSecurityService->canViewOnlyOwnPersons() === true) {
            $createdByAccountUser = $user;
        } else {
            $createdByAccountUser = null;
        }

        $persons = $this->personService->fetchPersonsPaginatedByAccount(
            account: $account,
            firstResult: $paginationParameter->getFirstResult(),
            maxResults: $paginationParameter->getLimit(),
            anonymized: false,
            createdByAccountUser: $createdByAccountUser,
            personFilter: $personFilterForm->getData(),
            personSearch: $personSearchForm->getData(),
            sortingOption: $sortingOption
        );

        return $this->render(view: 'person_management/person/overview.html.twig', parameters: [
            'personFilterForm' => $personFilterForm,
            'personSearchForm' => $personSearchForm,
            'canCreatePerson'  => $this->personSecurityService->canCreatePerson(),
            'canViewOnly'      => $this->personSecurityService->canViewOnly(),
            'persons'          => $persons,
            'personFilter'     => $personFilterForm->getData(),
            'pageTitle'        => 'Kontaktverwaltung',
            'activeNavGroup'   => self::ACTIVE_NAV_GROUP,
            'activeNavItem'    => self::ACTIVE_NAV_ITEM,
        ]);
    }

    #[Security("is_granted('ROLE_USER') or is_granted('ROLE_PROPERTY_FEEDER') or is_granted('ROLE_LOOKING_FOR_PROPERTY_REQUEST_FEEDER')")]
    #[Route('/kontakte/anlegen', name: 'create', methods: ['GET', 'POST'])]
    public function create(Request $request, UserInterface $user): Response
    {
        if ($this->personSecurityService->canCreatePerson() === false) {
            throw new AccessDeniedHttpException();
        }

        $personCreateForm = $this->createForm(type: PersonCreateType::class);

        $personCreateForm->add(child: 'save', type: SubmitType::class);

        $personCreateForm->handleRequest(request: $request);

        if ($personCreateForm->isSubmitted() && $personCreateForm->isValid()) {
            $person = $personCreateForm->getData();

            $person
                ->setAccount($user->getAccount())
                ->setCreatedByAccountUser($user)
                ->setDeleted(false)
                ->setAnonymized(false);

            $this->entityManager->persist(entity: $person);

            $this->entityManager->flush();

            $this->addFlash(type: 'dataSaved', message: 'Der Kontakt wurde angelegt.');

            $urlParameters = $this->fetchMergedUrlParametersFromSession(
                urlParameterObjectClasses: self::URL_PARAMETER_OBJECT_CLASSES,
                sessionKeyName: self::OVERVIEW_SESSION_KEY_NAME
            );

            return $this->redirectToRoute(route: self::OVERVIEW_ROUTE_NAME, parameters: $urlParameters);
        }

        return $this->render(view: 'person_management/person/create.html.twig', parameters: [
            'personCreateForm' => $personCreateForm,
            'pageTitle'        => 'Kontaktverwaltung - Kontakt anlegen',
            'activeNavGroup'   => self::ACTIVE_NAV_GROUP,
            'activeNavItem'    => self::ACTIVE_NAV_ITEM,
        ]);
    }

    #[Route('/kontakte/{personId<\d{1,10}>}', name: 'base_data', methods: ['GET', 'POST'])]
    public function baseData(int $personId, Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $person = $this->personService->fetchPersonById(account: $account, id: $personId);

        if ($person === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'view', subject: $person);

        switch ($person->getPersonType()) {
            case PersonType::NATURAL_PERSON:
                $personForm = $this->createForm(type: NaturalPersonType::class, data: $person, options: [
                    'canEdit' => $this->isGranted(attribute: 'edit', subject: $person),
                ]);

                $pageTitle = 'Kontaktverwaltung - Privatperson editieren';
                break;
            case PersonType::COMPANY:
                $personForm = $this->createForm(type: CompanyType::class, data: $person, options: [
                    'canEdit' => $this->isGranted(attribute: 'edit', subject: $person),
                ]);

                $pageTitle = 'Personenverwaltung - Firma editieren';
                break;
            case PersonType::COMMUNE:
                $personForm = $this->createForm(type: CommuneType::class, data: $person, options: [
                    'canEdit' => $this->isGranted(attribute: 'edit', subject: $person),
                ]);

                $pageTitle = 'Personenverwaltung - Kommune editieren';
                break;
        }

        $personForm->add(child: 'save', type: SubmitType::class);

        $personForm->handleRequest(request: $request);

        if ($personForm->isSubmitted() && $personForm->isValid()) {
            if ($person->isAnonymized() === true) {
                throw new AccessDeniedHttpException();
            }

            $this->denyAccessUnlessGranted(attribute: 'edit', subject: $person);

            $this->entityManager->flush();

            $this->addFlash(type: 'dataSaved', message: 'Die Daten wurden gespeichert.');

            return $this->redirectToRoute(route: 'person_management_person_base_data', parameters: ['personId' => $person->getId()]);
        }

        return $this->render(view: 'person_management/person/base_data.html.twig', parameters: [
            'personForm'     => $personForm,
            'person'         => $person,
            'pageTitle'      => $pageTitle,
            'activeNavGroup' => self::ACTIVE_NAV_GROUP,
            'activeNavItem'  => self::ACTIVE_NAV_ITEM,
        ]);
    }

    #[Security("is_granted('ROLE_USER') or is_granted('ROLE_PROPERTY_FEEDER') or is_granted('ROLE_LOOKING_FOR_PROPERTY_REQUEST_FEEDER')")]
    #[Route('/kontakte/{personId<\d{1,10}>}/loeschen', name: 'delete', methods: ['GET', 'POST'])]
    public function delete(int $personId, Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $person = $this->personService->fetchPersonById(account: $account, id: $personId);

        if ($person === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'delete', subject: $person);

        switch ($person->getPersonType()) {
            case PersonType::NATURAL_PERSON:
                $personForm = $this->createForm(type: NaturalPersonType::class, data: $person, options: ['canEdit' => false]);
                $pageTitle = 'Kontaktverwaltung - Privatperson editieren';
                break;
            case PersonType::COMPANY:
                $personForm = $this->createForm(type: CompanyType::class, data: $person, options: ['canEdit' => false]);
                $pageTitle = 'Personenverwaltung - Firma editieren';
                break;
            case PersonType::COMMUNE:
                $personForm = $this->createForm(type: CommuneType::class, data: $person, options: ['canEdit' => false]);
                $pageTitle = 'Personenverwaltung - Kommune editieren';
                break;
        }

        $personDeleteForm = $this->createForm(type: PersonDeleteType::class);

        $personDeleteForm->add(child: 'delete', type: SubmitType::class);

        $personDeleteForm->handleRequest(request: $request);

        if (
            $personDeleteForm->isSubmitted()
            && $personDeleteForm->isValid()
            && $personDeleteForm->get('verificationCode')->getData() === 'kontakt_' . $person->getId()
        ) {
            $this->personDeletionService->deletePerson(person: $person, deletionType: DeletionType::ANONYMIZATION, deletedByAccountUser: $user);

            $this->addFlash(type: 'dataDeleted', message: 'Der Kontakt wurde gelöscht.');

            $urlParameters = $this->fetchMergedUrlParametersFromSession(
                urlParameterObjectClasses: self::URL_PARAMETER_OBJECT_CLASSES,
                sessionKeyName: self::OVERVIEW_SESSION_KEY_NAME
            );

            return $this->redirectToRoute(route: self::OVERVIEW_ROUTE_NAME, parameters: $urlParameters);
        }

        return $this->render(view: 'person_management/person/delete.html.twig', parameters: [
            'personDeleteForm' => $personDeleteForm,
            'personForm'       => $personForm,
            'person'           => $person,
            'pageTitle'        => $pageTitle,
            'activeNavGroup'   => self::ACTIVE_NAV_GROUP,
            'activeNavItem'    => self::ACTIVE_NAV_ITEM,
        ]);
    }
}
