<?php

declare(strict_types=1);

namespace App\Controller\Configuration;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Security("is_granted('ROLE_ACCOUNT_ADMIN')")]
#[Route('/konfiguration/geschaeftslagen', name: 'configuration_business_location_area_')]
class BusinessLocationAreaController extends AbstractController
{
    private const ACTIVE_NAV_GROUP = 'configuration';

    #[Route('/karte', name: 'map', methods: ['GET', 'POST'])]
    public function map(UserInterface $user): Response
    {
        return $this->render(view: 'configuration/business_location_area/map.html.twig', parameters: [
            'geolocationPoint' => $user->getAccount()->getAssignedPlace()->getGeolocationPoint(),
            'pageTitle'        => 'Konfiguration - Geschäftslagen',
            'activeNavGroup'   => self::ACTIVE_NAV_GROUP,
        ]);
    }
}
