<?php

declare(strict_types=1);

namespace App\Controller\Configuration;

use App\Domain\LookingForPropertyRequestReporter\LookingForPropertyRequestReporterConfigurationService;
use App\Form\Type\Configuration\LookingForPropertyRequestReporter\GeneralType;
use App\Form\Type\Configuration\LookingForPropertyRequestReporter\LayoutType;
use App\Form\Type\Configuration\LookingForPropertyRequestReporter\MandatoryFieldsType;
use App\Form\Type\Configuration\LookingForPropertyRequestReporter\PrivacyPolicyType;
use App\Form\Type\Configuration\LookingForPropertyRequestReporter\IndustryClassificationType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Security("is_granted('ROLE_ACCOUNT_ADMIN') or is_granted('ROLE_CONFIGURATOR')")]
#[Route('/konfiguration/gesuchsmelder', name: 'configuration_looking_for_property_request_reporter_')]
class LookingForPropertyRequestReporterController extends AbstractController
{
    private const ACTIVE_NAV_GROUP = 'configuration';

    public function __construct(
        private readonly LookingForPropertyRequestReporterConfigurationService $lookingForPropertyRequestReporterConfigurationService,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    #[Route('/allgemein', name: 'general', methods: ['GET', 'POST'])]
    public function general(Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $lookingForPropertyRequestReporterConfiguration = $this->lookingForPropertyRequestReporterConfigurationService
            ->fetchLookingForPropertyRequestReporterConfigurationByAccount(account: $account);

        if ($lookingForPropertyRequestReporterConfiguration === null) {
            $lookingForPropertyRequestReporterConfiguration = $this->lookingForPropertyRequestReporterConfigurationService->buildDefaultLookingForPropertyRequestReporterConfigurationByAccount(account: $account);
            $this->entityManager->persist(entity: $lookingForPropertyRequestReporterConfiguration);
            $this->entityManager->flush();
        }

        $generalForm = $this->createForm(type: GeneralType::class, data: $lookingForPropertyRequestReporterConfiguration);

        $generalForm->add(child: 'save', type: SubmitType::class);

        $generalForm->handleRequest(request: $request);

        if ($generalForm->isSubmitted() && $generalForm->isValid()) {
            $this->entityManager->flush();

            $this->addFlash(type: 'dataSaved', message: 'Die Allgemeinen Einstellungen wurden gespeichert.');

            return $this->redirectToRoute(route: 'configuration_looking_for_property_request_reporter_general');
        }

        return $this->render(view: 'configuration/looking_for_property_request_reporter/general.html.twig', parameters: [
            'generalForm'    => $generalForm,
            'pageTitle'      => 'Konfiguration - Gesuchsmelder',
            'activeNavGroup' => self::ACTIVE_NAV_GROUP,
        ]);
    }

    #[Route('/nutzungsarten', name: 'property_usage', methods: ['GET', 'POST'])]
    public function propertyUsage(Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $lookingForPropertyRequestReporterConfiguration = $this->lookingForPropertyRequestReporterConfigurationService
            ->fetchLookingForPropertyRequestReporterConfigurationByAccount(account: $account);

        $propertyUsageForm = $this->createForm(type: IndustryClassificationType::class, data: $lookingForPropertyRequestReporterConfiguration);

        $propertyUsageForm->add(child: 'save', type: SubmitType::class);

        $propertyUsageForm->handleRequest(request: $request);

        if ($propertyUsageForm->isSubmitted() && $propertyUsageForm->isValid()) {
            $this->entityManager->flush();

            $this->addFlash(type: 'dataSaved', message: 'Die Einstellungen wurden gespeichert.');

            return $this->redirectToRoute(route: 'configuration_looking_for_property_request_reporter_property_usage');
        }

        return $this->render(view: 'configuration/looking_for_property_request_reporter/property_usage.html.twig', parameters: [
            'propertyUsageForm' => $propertyUsageForm,
            'pageTitle'         => 'Konfiguration - Gesuchsmelder',
            'activeNavGroup'    => self::ACTIVE_NAV_GROUP,
        ]);
    }

    #[Route('/pflichtfelder', name: 'mandatory_fields', methods: ['GET', 'POST'])]
    public function mandatoryFields(Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $lookingForPropertyRequestReporterConfiguration = $this->lookingForPropertyRequestReporterConfigurationService
            ->fetchLookingForPropertyRequestReporterConfigurationByAccount(account: $account);

        $mandatoryFieldsForm = $this->createForm(type: MandatoryFieldsType::class, data: $lookingForPropertyRequestReporterConfiguration);

        $mandatoryFieldsForm->add(child: 'save', type: SubmitType::class);

        $mandatoryFieldsForm->handleRequest(request: $request);

        if ($mandatoryFieldsForm->isSubmitted() && $mandatoryFieldsForm->isValid()) {
            $this->entityManager->flush();

            $this->addFlash(type: 'dataSaved', message: 'Die Einstellungen wurden gespeichert.');

            return $this->redirectToRoute(route: 'configuration_looking_for_property_request_reporter_mandatory_fields');
        }

        return $this->render(view: 'configuration/looking_for_property_request_reporter/mandatory_fields.html.twig', parameters: [
            'mandatoryFieldsForm' => $mandatoryFieldsForm,
            'pageTitle'           => 'Konfiguration - Gesuchsmelder',
            'activeNavGroup'      => self::ACTIVE_NAV_GROUP,
        ]);
    }

    #[Route('/datenschutz', name: 'privacy_policy', methods: ['GET', 'POST'])]
    public function privacyPolicy(Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $lookingForPropertyRequestReporterConfiguration = $this->lookingForPropertyRequestReporterConfigurationService
            ->fetchLookingForPropertyRequestReporterConfigurationByAccount(account: $account);

        $privacyPolicyForm = $this->createForm(type: PrivacyPolicyType::class, data: $lookingForPropertyRequestReporterConfiguration);

        $privacyPolicyForm->add(child: 'save', type: SubmitType::class);

        $privacyPolicyForm->handleRequest(request: $request);

        if ($privacyPolicyForm->isSubmitted() && $privacyPolicyForm->isValid()) {
            $this->entityManager->flush();

            $this->addFlash(type: 'dataSaved', message: 'Die Einstellungen wurden gespeichert.');

            return $this->redirectToRoute(route: 'configuration_looking_for_property_request_reporter_privacy_policy');
        }

        return $this->render(view: 'configuration/looking_for_property_request_reporter/privacy_policy.html.twig', parameters: [
            'privacyPolicyForm' => $privacyPolicyForm,
            'pageTitle'         => 'Konfiguration - Gesuchsmelder',
            'activeNavGroup'    => self::ACTIVE_NAV_GROUP,
        ]);
    }

    #[Route('/layout', name: 'layout', methods: ['GET', 'POST'])]
    public function layout(Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $lookingForPropertyRequestReporterConfiguration = $this->lookingForPropertyRequestReporterConfigurationService
            ->fetchLookingForPropertyRequestReporterConfigurationByAccount(account: $account);

        $layoutForm = $this->createForm(type: LayoutType ::class, data: $lookingForPropertyRequestReporterConfiguration);

        $layoutForm->add(child: 'save', type: SubmitType::class);

        $layoutForm->handleRequest(request: $request);

        if ($layoutForm->isSubmitted() && $layoutForm->isValid()) {
            $firstFontColor = $this->lookingForPropertyRequestReporterConfigurationService->calculateIdealHexFontColor(
                hexBackgroundColor: $layoutForm->getData()->getFirstColor()
            );

            $secondaryFontColor = $this->lookingForPropertyRequestReporterConfigurationService->calculateIdealHexFontColor(
                hexBackgroundColor: $layoutForm->getData()->getSecondaryColor()
            );

            $layoutForm->getData()->setFirstFontColor($firstFontColor);
            $layoutForm->getData()->setSecondaryFontColor($secondaryFontColor);

            $this->entityManager->flush();

            $this->addFlash(type: 'dataSaved', message: 'Die Einstellungen wurden gespeichert.');

            return $this->redirectToRoute(route: 'configuration_looking_for_property_request_reporter_layout');
        }

        return $this->render(view: 'configuration/looking_for_property_request_reporter/layout.html.twig', parameters: [
            'layoutForm'     => $layoutForm,
            'pageTitle'      => 'Konfiguration - Gesuchsmelder',
            'activeNavGroup' => self::ACTIVE_NAV_GROUP,
        ]);
    }

    #[Route('/vorschau', name: 'preview', methods: ['GET'])]
    public function preview(UserInterface $user): Response
    {
        $account = $user->getAccount();

        $lookingForPropertyRequestReporterConfiguration = $this->lookingForPropertyRequestReporterConfigurationService
            ->fetchLookingForPropertyRequestReporterConfigurationByAccount(account: $account);

        return $this->render(view: 'configuration/looking_for_property_request_reporter/preview.html.twig', parameters: [
            'accountKey'     => $lookingForPropertyRequestReporterConfiguration->getAccountKey()->toRfc4122(),
            'pageTitle'      => 'Konfiguration - Gesuchsmelder',
            'activeNavGroup' => self::ACTIVE_NAV_GROUP,
        ]);
    }

    #[Route('/einbettungscode', name: 'embed_code', methods: ['GET'])]
    public function embedCode(Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $lookingForPropertyRequestReporterConfiguration = $this->lookingForPropertyRequestReporterConfigurationService
            ->fetchLookingForPropertyRequestReporterConfigurationByAccount(account: $account);

        return $this->render(view: 'configuration/looking_for_property_request_reporter/embed_code.html.twig', parameters: [
            'accountKey'     => $lookingForPropertyRequestReporterConfiguration->getAccountKey()->toRfc4122(),
            'hostname'       => 'https://' . $request->getHttpHost(),
            'pageTitle'      => 'Konfiguration - Gesuchsmelder',
            'activeNavGroup' => self::ACTIVE_NAV_GROUP,
        ]);
    }

    #[Route('/faq', name: 'faq', methods: ['GET'])]
    public function faq(): Response
    {
        return $this->render(view: 'configuration/looking_for_property_request_reporter/faq.html.twig', parameters: [
            'pageTitle'      => 'Konfiguration - Gesuchsmelder',
            'activeNavGroup' => self::ACTIVE_NAV_GROUP,
        ]);
    }
}
