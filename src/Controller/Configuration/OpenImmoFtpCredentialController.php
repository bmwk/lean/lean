<?php

declare(strict_types=1);

namespace App\Controller\Configuration;

use App\Domain\Entity\OpenImmoFtpTransfer\OpenImmoFtpCredential;
use App\Domain\Entity\TaskStatus;
use App\Domain\OpenImmoFtpTransfer\OpenImmoFtpCredentialDeletionService;
use App\Domain\OpenImmoFtpTransfer\OpenImmoFtpCredentialService;
use App\Domain\OpenImmoFtpTransfer\OpenImmoFtpTransferService;
use App\Form\Type\Configuration\OpenImmoFtpCredential\OpenImmoFtpCredentialDeleteType;
use App\Form\Type\Configuration\OpenImmoFtpCredential\OpenImmoFtpCredentialType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Security("is_granted('ROLE_ACCOUNT_ADMIN') or is_granted('ROLE_CONFIGURATOR')")]
#[Route('/konfiguration/open-immo-export', name: 'configuration_open_immo_ftp_credentials_')]
class OpenImmoFtpCredentialController extends AbstractController
{
    private const ACTIVE_NAV_GROUP = 'configuration';

    public function __construct(
        private readonly OpenImmoFtpCredentialService $openImmoFtpCredentialService,
        private readonly OpenImmoFtpCredentialDeletionService $openImmoFtpDeletionService,
        private readonly OpenImmoFtpTransferService $openImmoFtpTransferService,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    #[Route('/uebersicht', name: 'overview', methods: ['GET', 'POST'])]
    public function overview(UserInterface $user): Response
    {
        $account = $user->getAccount();

        $openImmoFtpCredentials = $this->openImmoFtpCredentialService->fetchOpenImmoFtpCredentials(account: $account);

        return $this->render(view: 'configuration/open_immo_ftp_credential/overview.html.twig', parameters: [
            'openImmoFtpCredentials' => $openImmoFtpCredentials,
            'pageTitle'              => 'Konfiguration - OpenImmo-Export',
            'activeNavGroup'         => self::ACTIVE_NAV_GROUP,
        ]);
    }

    #[Route('/anlegen', name: 'create', methods: ['GET', 'POST'])]
    public function create(Request $request, UserInterface $user): Response
    {
        $openImmoFtpCredential = new OpenImmoFtpCredential();

        $account = $user->getAccount();

        $openImmoFtpCredentialForm = $this->createForm(type: OpenImmoFtpCredentialType::class, data: $openImmoFtpCredential);

        $openImmoFtpCredentialForm->add(child: 'save', type: SubmitType::class);

        $openImmoFtpCredentialForm->handleRequest(request: $request);

        if ($openImmoFtpCredentialForm->isSubmitted() && $openImmoFtpCredentialForm->isValid()) {
            $plainPassword = $openImmoFtpCredentialForm->get('plainPassword')->getData();

            $openImmoFtpCredential
                ->setPassword($this->openImmoFtpCredentialService->openSslEncrypt(plainPassword: $plainPassword))
                ->setCreatedByAccountUser($user)
                ->setAccount($account);

            $this->entityManager->persist($openImmoFtpCredential);
            $this->entityManager->flush();

            $this->addFlash(type: 'dataSaved', message: 'Das externe Portal wurde erfolgreich eingerichtet.');

            return $this->redirectToRoute(route: 'configuration_open_immo_ftp_credentials_overview');
        }

        return $this->render(view: 'configuration/open_immo_ftp_credential/create.html.twig', parameters: [
            'openImmoFtpCredentialForm' => $openImmoFtpCredentialForm,
            'pageTitle'                 => 'Konfiguration - OpenImmo-Export',
            'activeNavGroup'            => self::ACTIVE_NAV_GROUP,
        ]);
    }

    #[Route('/{id<\d{1,10}>}', name: 'edit', methods: ['GET', 'POST'])]
    public function edit(int $id, Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $openImmoFtpCredential = $this->openImmoFtpCredentialService->fetchOpenImmoFtpCredentialById(id: $id, account: $account);

        if ($openImmoFtpCredential === null) {
            throw new NotFoundHttpException();
        }

        $openImmoFtpCredentialForm = $this->createForm(type: OpenImmoFtpCredentialType::class, data: $openImmoFtpCredential);

        $openImmoFtpCredentialForm->add(child: 'save', type: SubmitType::class);

        $openImmoFtpCredentialForm->handleRequest(request: $request);

        if ($openImmoFtpCredentialForm->isSubmitted() && $openImmoFtpCredentialForm->isValid()) {
            $plainPassword = $openImmoFtpCredentialForm->get('plainPassword')->getData();

            if ($plainPassword !== null) {
                $openImmoFtpCredential->setPassword($this->openImmoFtpCredentialService->openSslEncrypt(plainPassword: $plainPassword));
            }

            $this->entityManager->flush();

            $this->addFlash(type: 'dataSaved', message: 'Die Parameter für das externe Portal wurde erfolgreich aktualisiert.');

            return $this->redirectToRoute(route: 'configuration_open_immo_ftp_credentials_overview');
        }

        return $this->render(view: 'configuration/open_immo_ftp_credential/edit.html.twig', parameters: [
            'openImmoFtpCredentialForm' => $openImmoFtpCredentialForm,
            'openImmoFtpCredential'     => $openImmoFtpCredential,
            'pageTitle'                 => 'Konfiguration - OpenImmo-Export',
            'activeNavGroup'            => self::ACTIVE_NAV_GROUP,
        ]);
    }

    #[Route('/{id<\d{1,10}>}/loeschen', name: 'delete', methods: ['GET', 'POST'])]
    public function delete(int $id, Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $openImmoFtpCredential = $this->openImmoFtpCredentialService->fetchOpenImmoFtpCredentialById(id: $id, account: $account);

        if ($openImmoFtpCredential === null) {
            throw new NotFoundHttpException();
        }

        $exportedBuildingUnits = $this->openImmoFtpTransferService->fetchExportedAndNotDeletedBuildingUnitsByOpenImmoFtpCredential(openImmoFtpCredential: $openImmoFtpCredential);

        $openImmoFtpCredentialDeleteForm = $this->createForm(type: OpenImmoFtpCredentialDeleteType::class);

        $openImmoFtpCredentialDeleteForm->add(child: 'delete', type: SubmitType::class);

        $openImmoFtpCredentialDeleteForm->handleRequest(request: $request);

        if ($openImmoFtpCredentialDeleteForm->isSubmitted() && $openImmoFtpCredentialDeleteForm->isValid()) {
            if ($openImmoFtpCredentialDeleteForm->get('verificationCode')->getData() === 'portal_' . $openImmoFtpCredential->getId()) {
                $this->openImmoFtpDeletionService->deleteOpenImmoFtpCredential(openImmoFtpCredential: $openImmoFtpCredential);

                $this->addFlash(type: 'dataDeleted', message: 'Die Parameter für das externe Portal wurden gelöscht.');

                return $this->redirectToRoute(route: 'configuration_open_immo_ftp_credentials_overview');
            } else {
                $this->addFlash(type: 'dataError', message: 'Der eingegebene Bestätigungscode war falsch.');
            }
        }

        return $this->render(view: 'configuration/open_immo_ftp_credential/delete.html.twig', parameters: [
            'openImmoFtpCredentialDeleteForm' => $openImmoFtpCredentialDeleteForm,
            'openImmoFtpCredential'           => $openImmoFtpCredential,
            'exportedBuildingUnits'           => $exportedBuildingUnits,
            'pageTitle'                       => 'Konfiguration - OpenImmo-Export',
            'activeNavGroup'                  => self::ACTIVE_NAV_GROUP,
        ]);
    }
}
