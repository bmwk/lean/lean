<?php

declare(strict_types=1);

namespace App\Controller\Configuration\BasicSettings;

use App\Domain\Account\AccountService;
use App\Domain\Entity\AccountConfiguration;
use App\Domain\Entity\AccountLegalTextsConfiguration;
use App\Domain\Entity\HallOfInspiration\HallOfInspirationConfiguration;
use App\Domain\Entity\HystreetConfiguration;
use App\Domain\Entity\WegweiserKommunePlaceMapping;
use App\Domain\Entity\ZielbildcheckConfiguration;
use App\Domain\Image\ImageDeletionService;
use App\Domain\InterfaceConfiguration\InterfaceConfigurationService;
use App\Form\Type\Configuration\BasicSettings\AccountDataType;
use App\Form\Type\Configuration\BasicSettings\GtcType;
use App\Form\Type\Configuration\BasicSettings\ImprintType;
use App\Form\Type\Configuration\BasicSettings\LayoutType;
use App\Form\Type\Configuration\BasicSettings\LogoDeleteType;
use App\Form\Type\Configuration\BasicSettings\PrivacyPolicyType;
use App\Form\Type\Configuration\BasicSettings\ProcessesType;
use App\Form\Type\Configuration\InterfaceConfiguration\HallOfInspirationConfigurationType;
use App\Form\Type\Configuration\InterfaceConfiguration\HystreetConfigurationType;
use App\Form\Type\Configuration\InterfaceConfiguration\WegweiserKommunePlaceMappingType;
use App\Form\Type\Configuration\InterfaceConfiguration\ZielbildcheckConfigurationType;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\UnitOfWork;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Security("is_granted('ROLE_ACCOUNT_ADMIN') or is_granted('ROLE_CONFIGURATOR')")]
#[Route('/konfiguration/grundeinstellungen', name: 'configuration_basic_settings_')]
class BasicSettingsController extends AbstractController
{
    private const ACTIVE_NAV_GROUP = 'configuration';

    public function __construct(
        private readonly AccountService $accountService,
        private readonly ImageDeletionService $imageDeletionService,
        private readonly InterfaceConfigurationService $interfaceConfigurationService,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    #[Route('/betreiberdaten', name: 'account_data', methods: ['GET', 'POST'])]
    public function accountData(Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $accountDataForm = $this->createForm(type: AccountDataType::class, data: $account);

        $accountDataForm->add(child: 'save', type: SubmitType::class);

        $accountDataForm->handleRequest(request: $request);

        if ($accountDataForm->isSubmitted() && $accountDataForm->isValid()) {
            $this->entityManager->flush();

            $this->addFlash(type: 'dataSaved', message: 'Die Betreiberdaten wurden gespeichert.');

            return $this->redirectToRoute(route: 'configuration_basic_settings_account_data');
        }

        return $this->render(view: 'configuration/basic_settings/account_data.html.twig', parameters: [
            'accountDataForm' => $accountDataForm,
            'pageTitle'       => 'Konfiguration - Grundeinstelungen – Betreiberdaten',
            'activeNavGroup'  => self::ACTIVE_NAV_GROUP,
        ]);
    }

    #[Route('/impressum', name: 'imprint', methods: ['GET', 'POST'])]
    public function imprint(Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $accountLegalTextsConfiguration = $this->accountService->fetchAccountLegalTextsConfigurationByAccount(account: $account);

        if ($accountLegalTextsConfiguration === null) {
            $accountLegalTextsConfiguration = new AccountLegalTextsConfiguration();

            $accountLegalTextsConfiguration->setAccount($account);

            $this->entityManager->persist($accountLegalTextsConfiguration);
        }

        $imprintForm = $this->createForm(type: ImprintType::class, data: $accountLegalTextsConfiguration);

        $imprintForm->add(child: 'save', type: SubmitType::class);

        $imprintForm->handleRequest(request: $request);

        if ($imprintForm->isSubmitted() && $imprintForm->isValid()) {
            $this->entityManager->flush();

            $this->addFlash(type: 'dataSaved', message: 'Das Impressum wurde gespeichert.');

            return $this->redirectToRoute(route: 'configuration_basic_settings_imprint');
        }

        return $this->render(view: 'configuration/basic_settings/imprint.html.twig', parameters: [
            'imprintForm'    => $imprintForm,
            'pageTitle'      => 'Konfiguration - Grundeinstelungen – Impressum',
            'activeNavGroup' => self::ACTIVE_NAV_GROUP,
        ]);
    }

    #[Route('/datenschutz', name: 'privacy_policy', methods: ['GET', 'POST'])]
    public function privacyPolicy(Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $accountLegalTextsConfiguration = $this->accountService->fetchAccountLegalTextsConfigurationByAccount(account: $account);

        if ($accountLegalTextsConfiguration === null) {
            $accountLegalTextsConfiguration = new AccountLegalTextsConfiguration();

            $accountLegalTextsConfiguration->setAccount($account);

            $this->entityManager->persist($accountLegalTextsConfiguration);
        }

        if ($request->query->get('lade-vorlage') !== null && $request->query->get('lade-vorlage') === 'suchende') {
            $accountLegalTextsConfiguration->setPrivacyPolicySeeker(base64_encode($this->accountService->defaultPrivacyPolicySeekerText()));
            $this->addFlash(type: 'dataSaved', message: 'Die Datenschutzerklärung für Suchende wurden aus der Vorlage geladen.');
        } elseif ($request->query->get('lade-vorlage') !== null && $request->query->get('lade-vorlage') === 'anbietende') {
            $accountLegalTextsConfiguration->setPrivacyPolicyProvider(base64_encode($this->accountService->defaultPrivacyPolicyProviderText()));
            $this->addFlash(type: 'dataSaved', message: 'Die Datenschutzerklärung für Anbietende wurden aus der Vorlage geladen.');
        }

        if ($accountLegalTextsConfiguration->getPdfExposeDisclaimer() === null) {
            $accountLegalTextsConfiguration->setPdfExposeDisclaimer($this->accountService->defaultPdfExposeDisclaimerText(account: $account));
        }

        $privacyPolicyForm = $this->createForm(type: PrivacyPolicyType::class, data: $accountLegalTextsConfiguration);

        $privacyPolicyForm->add(child: 'save', type: SubmitType::class);

        $privacyPolicyForm->handleRequest(request: $request);

        if ($privacyPolicyForm->isSubmitted() && $privacyPolicyForm->isValid()) {
            $this->entityManager->flush();

            $this->addFlash(type: 'dataSaved', message: 'Die Datenschutzerklärung wurde gespeichert.');

            return $this->redirectToRoute(route: 'configuration_basic_settings_privacy_policy');
        }

        return $this->render(view: 'configuration/basic_settings/privacy_policy.html.twig', parameters: [
            'privacyPolicyForm' => $privacyPolicyForm,
            'pageTitle'         => 'Konfiguration - Grundeinstellungen – Datenschutz',
            'activeNavGroup'    => self::ACTIVE_NAV_GROUP,
        ]);
    }

    #[Route('/agb', name: 'gtc', methods: ['GET', 'POST'])]
    public function gtc(Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $accountLegalTextsConfiguration = $this->accountService->fetchAccountLegalTextsConfigurationByAccount(account: $account);

        if ($accountLegalTextsConfiguration === null) {
            $accountLegalTextsConfiguration = new AccountLegalTextsConfiguration();

            $accountLegalTextsConfiguration->setAccount($account);

            $this->entityManager->persist($accountLegalTextsConfiguration);
        }

        if ($request->query->get('lade-vorlage') !== null && $request->query->get('lade-vorlage') === 'suchende') {
            $accountLegalTextsConfiguration->setGeneralTermsAndConditionsSeeker(base64_encode($this->accountService->defaultGtcSeekerText()));
            $this->addFlash(type: 'dataSaved', message: 'Die Nutzungsbedingungen für Suchende wurden aus der Vorlage geladen.');
        } elseif ($request->query->get('lade-vorlage') !== null && $request->query->get('lade-vorlage') === 'anbietende') {
            $accountLegalTextsConfiguration->setGeneralTermsAndConditionsProvider(base64_encode($this->accountService->defaultGtcProviderText()));
            $this->addFlash(type: 'dataSaved', message: 'Die Nutzungsbedingungen für Anbietende wurden aus der Vorlage geladen.');
        }

        $gtcForm = $this->createForm(type: GtcType::class, data: $accountLegalTextsConfiguration);

        $gtcForm->add(child: 'save', type: SubmitType::class);

        $gtcForm->handleRequest(request: $request);

        if ($gtcForm->isSubmitted() && $gtcForm->isValid()) {
            $this->entityManager->flush();

            $this->addFlash(type: 'dataSaved', message: 'Die Nutzungsbedingungen wurden gespeichert.');

            return $this->redirectToRoute(route: 'configuration_basic_settings_gtc');
        }

        return $this->render(view: 'configuration/basic_settings/gtc.html.twig', parameters: [
            'gtcForm'        => $gtcForm,
            'pageTitle'      => 'Konfiguration - Grundeinstellungen – AGB',
            'activeNavGroup' => self::ACTIVE_NAV_GROUP,
        ]);
    }

    #[Route('/layout', name: 'layout', methods: ['GET', 'POST'])]
    public function layout(Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $accountConfiguration = $this->accountService->fetchAccountConfigurationByAccount(account: $account);

        if ($accountConfiguration === null) {
            $accountConfiguration = new AccountConfiguration();

            $accountConfiguration->setAccount($account);

            $this->entityManager->persist($accountConfiguration);
        }

        $layoutForm = $this->createForm(type: LayoutType::class, data: $accountConfiguration);

        $layoutForm->add(child: 'save', type: SubmitType::class);

        $layoutForm->handleRequest(request: $request);

        if ($layoutForm->isSubmitted() && $layoutForm->isValid()) {
            $this->entityManager->flush();

            $this->addFlash(type: 'dataSaved', message: 'Die Einstellungen wurden gespeichert.');

            return $this->redirectToRoute(route: 'configuration_basic_settings_layout');
        }

        return $this->render(view: 'configuration/basic_settings/layout.html.twig', parameters: [
            'account'        => $account,
            'layoutForm'     => $layoutForm,
            'pageTitle'      => 'Konfiguration - Grundeinstellungen – Layout',
            'activeNavGroup' => self::ACTIVE_NAV_GROUP,
        ]);
    }

    #[Route('/layout/logo-image-upload', name: 'layout_logo_image_upload', methods: ['POST'])]
    public function layoutLogoImageUpload(Request $request, UserInterface $user): JsonResponse
    {
        $layoutLogoFile = $request->files->get('logoImageFile');

        $account = $user->getAccount();

        $accountConfiguration = $this->accountService->fetchAccountConfigurationByAccount(account: $account);

        if ($accountConfiguration === null) {
            $accountConfiguration = new AccountConfiguration();

            $accountConfiguration->setAccount($account);

            $this->entityManager->persist($accountConfiguration);
        }

        if ($layoutLogoFile === null) {
            throw new BadRequestHttpException();
        }

        $allowedFileMimeTypes = [
            'image/jpeg',
            'image/png',
            'image/svg+xml',
        ];

        if (in_array(needle: $layoutLogoFile->getMimeType(), haystack: $allowedFileMimeTypes) !== true) {
            throw new BadRequestHttpException();
        }

        $this->entityManager->beginTransaction();

        $currentLogoImage = $accountConfiguration->getLogoImage();

        $logoImage = $this->accountService->createAndSaveLogoImageFromUploadedFile(uploadedFile: $layoutLogoFile, accountUser: $user);

        $logoImage->setTitle('Logo ' . $account->getName());

        $accountConfiguration->setLogoImage($logoImage);

        $this->entityManager->flush();

        $this->entityManager->commit();

        if ($currentLogoImage !== null) {
            $this->imageDeletionService->deleteImage(image: $currentLogoImage);
        }

        $this->addFlash(type: 'dataSaved', message: 'Das Logo wurde hochgeladen.');

        return new JsonResponse(data: ['success' => true], status: Response::HTTP_CREATED);
    }

    #[Route('/logo-entfernen', name: 'logo_delete', methods: ['GET', 'POST'])]
    public function logoDelete(Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $accountConfiguration = $account->getAccountConfiguration();

        $logoImage = $accountConfiguration->getLogoImage();

        if ($logoImage === null) {
            throw new NotFoundHttpException();
        }

        $logoDeleteForm = $this->createForm(type: LogoDeleteType::class);

        $logoDeleteForm->add(child: 'delete', type: SubmitType::class);

        $logoDeleteForm->handleRequest(request: $request);

        if ($logoDeleteForm->isSubmitted() && $logoDeleteForm->isValid()) {
            if ($logoDeleteForm->get('verificationCode')->getData() === 'logo_' . $account->getId()) {
                $this->entityManager->beginTransaction();

                $accountConfiguration->setLogoImage(null);

                $this->entityManager->flush();

                $this->imageDeletionService->deleteImage(image: $logoImage);

                $this->entityManager->commit();

                $this->addFlash(type: 'dataDeleted', message: 'Das Logo wurde entfernt.');

                return $this->redirectToRoute(route: 'configuration_basic_settings_layout');
            } else {
                $this->addFlash(type: 'dataError', message: 'Der Verifizierungscode ist falsch.');
            }
        }

        return $this->render(view: 'configuration/basic_settings/logo_delete.html.twig', parameters: [
            'account'        => $account,
            'logoDeleteForm' => $logoDeleteForm,
            'pageTitle'      => 'Konfiguration - Grundeinstellungen – Logo entfernen',
            'activeNavGroup' => self::ACTIVE_NAV_GROUP,
        ]);
    }

    #[Route('/prozesse', name: 'processes', methods: ['GET', 'POST'])]
    public function processes(Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $accountConfiguration = $this->accountService->fetchAccountConfigurationByAccount(account: $account);

        if ($accountConfiguration === null) {
            $accountConfiguration = new AccountConfiguration();

            $accountConfiguration->setAccount($account);

            $this->entityManager->persist($accountConfiguration);
        }

        $processesForm = $this->createForm(type: ProcessesType::class, data: $accountConfiguration);

        $processesForm->add(child: 'save', type: SubmitType::class);

        $processesForm->handleRequest(request: $request);

        if ($processesForm->isSubmitted() && $processesForm->isValid()) {
            $this->entityManager->flush();

            $this->addFlash(type: 'dataSaved', message: 'Die Einstellungen wurden gespeichert.');

            return $this->redirectToRoute(route: 'configuration_basic_settings_processes');
        }

        return $this->render(view: 'configuration/basic_settings/processes.html.twig', parameters: [
            'account'        => $account,
            'processesForm'  => $processesForm,
            'pageTitle'      => 'Konfiguration - Grundeinstellungen – Prozesse',
            'activeNavGroup' => self::ACTIVE_NAV_GROUP,
        ]);
    }

    #[Route('/schnittstellen', name: 'interfaces', methods: ['GET', 'POST'])]
    public function interfaces(Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $hystreetConfiguration = $this->interfaceConfigurationService->fetchHystreetConfigurationByAccount(account: $account);

        if ($hystreetConfiguration === null) {
            $hystreetConfiguration = new HystreetConfiguration();
        }

        $hallOfInspirationConfiguration = $this->interfaceConfigurationService->fetchHallOfInspirationConfigurationByAccount(account: $account);

        if ($hallOfInspirationConfiguration === null) {
            $hallOfInspirationConfiguration = new HallOfInspirationConfiguration();
        }

        $zielbildcheckConfiguration = $this->interfaceConfigurationService->fetchZielbildcheckConfigurationByAccount(account: $account);

        if ($zielbildcheckConfiguration === null) {
            $zielbildcheckConfiguration = new ZielbildcheckConfiguration();
        }

        $wegweiserKommunePlaceMapping = $this->interfaceConfigurationService->fetchWegweiserKommunePlaceMappingByPlace(account: $account, place: $account->getAssignedPlace());

        if ($wegweiserKommunePlaceMapping === null) {
            $wegweiserKommunePlaceMapping = new WegweiserKommunePlaceMapping();
        }

        $hystreetConfigurationForm = $this->createForm(type: HystreetConfigurationType::class, data: $hystreetConfiguration);
        $hallOfInspirationConfigurationForm = $this->createForm(type: HallOfInspirationConfigurationType::class, data: $hallOfInspirationConfiguration);
        $zielbildcheckConfigurationForm = $this->createForm(type: ZielbildcheckConfigurationType::class, data: $zielbildcheckConfiguration);
        $wegweiserKommunePlaceMappingForm = $this->createForm(type: WegweiserKommunePlaceMappingType::class, data: $wegweiserKommunePlaceMapping);

        $hystreetConfigurationForm->add(child: 'save', type: SubmitType::class);
        $hallOfInspirationConfigurationForm->add(child: 'save', type: SubmitType::class);
        $zielbildcheckConfigurationForm->add(child: 'save', type: SubmitType::class);
        $wegweiserKommunePlaceMappingForm->add(child: 'save', type: SubmitType::class);

        $hystreetConfigurationForm->handleRequest(request: $request);
        $hallOfInspirationConfigurationForm->handleRequest(request: $request);
        $zielbildcheckConfigurationForm->handleRequest(request: $request);
        $wegweiserKommunePlaceMappingForm->handleRequest(request: $request);

        if ($hystreetConfigurationForm->isSubmitted() && $hystreetConfigurationForm->isValid()) {
            $unitOfWork = $this->entityManager->getUnitOfWork();

            if ($unitOfWork->getEntityState($hystreetConfiguration) === UnitOfWork::STATE_NEW) {
                $hystreetConfiguration->setAccount($account);
                $this->entityManager->persist(entity: $hystreetConfiguration);
            }

            $this->entityManager->flush();

            $this->addFlash(type: 'dataSaved', message: 'Der Hystreet-Access-Token wurde gespeichert.');

            return $this->redirectToRoute(route: 'configuration_basic_settings_interfaces');
        }

        if ($hallOfInspirationConfigurationForm->isSubmitted() && $hallOfInspirationConfigurationForm->isValid()) {
            $unitOfWork = $this->entityManager->getUnitOfWork();

            if ($unitOfWork->getEntityState($hallOfInspirationConfiguration) === UnitOfWork::STATE_NEW) {
                $hallOfInspirationConfiguration->setAccount($account);
                $this->entityManager->persist(entity: $hallOfInspirationConfiguration);
            }

            $this->entityManager->flush();

            $this->addFlash(type: 'dataSaved', message: 'Der Hall of Inspiration-Access-Token wurde gespeichert.');

            return $this->redirectToRoute(route: 'configuration_basic_settings_interfaces');
        }

        if ($zielbildcheckConfigurationForm->isSubmitted() && $zielbildcheckConfigurationForm->isValid()) {
            $unitOfWork = $this->entityManager->getUnitOfWork();

            if ($unitOfWork->getEntityState($zielbildcheckConfiguration) === UnitOfWork::STATE_NEW) {
                $zielbildcheckConfiguration->setAccount($account);
                $this->entityManager->persist(entity: $zielbildcheckConfiguration);
            }

            $this->entityManager->flush();

            $this->addFlash(type: 'dataSaved', message: 'Der Zielbildcheck-Access-Token wurde gespeichert.');

            return $this->redirectToRoute(route: 'configuration_basic_settings_interfaces');
        }

        if ($wegweiserKommunePlaceMappingForm->isSubmitted() && $wegweiserKommunePlaceMappingForm->isValid()) {
            $unitOfWork = $this->entityManager->getUnitOfWork();

            if ($unitOfWork->getEntityState($wegweiserKommunePlaceMapping) === UnitOfWork::STATE_NEW) {
                $wegweiserKommunePlaceMapping
                    ->setAccount($account)
                    ->setPlace($account->getAssignedPlace());

                $this->entityManager->persist(entity: $wegweiserKommunePlaceMapping);
            }

            $this->entityManager->flush();

            $this->addFlash(type: 'dataSaved', message: 'Die Wegweiser-Kommune-Konfiguration wurde gespeichert.');

            return $this->redirectToRoute(route: 'configuration_basic_settings_interfaces');
        }

        return $this->render(view: 'configuration/basic_settings/interfaces.html.twig', parameters: [
            'hystreetConfigurationForm'          => $hystreetConfigurationForm,
            'hallOfInspirationConfigurationForm' => $hallOfInspirationConfigurationForm,
            'zielbildcheckConfigurationForm'     => $zielbildcheckConfigurationForm,
            'wegweiserKommunePlaceMappingForm'   => $wegweiserKommunePlaceMappingForm,
            'pageTitle'                          => 'Konfiguration - Grundeinstellungen – Schnittstellen',
            'activeNavGroup'                     => self::ACTIVE_NAV_GROUP,
        ]);
    }
}
