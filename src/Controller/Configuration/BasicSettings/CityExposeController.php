<?php

declare(strict_types=1);

namespace App\Controller\Configuration\BasicSettings;

use App\Domain\Account\AccountService;
use App\Domain\Document\DocumentDeletionService;
use App\Domain\Entity\AccountConfiguration;
use App\Form\Type\Configuration\BasicSettings\CityExpose\CityExposeDeleteType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Security("is_granted('ROLE_ACCOUNT_ADMIN') or is_granted('ROLE_CONFIGURATOR')")]
#[Route('/konfiguration/grundeinstellungen/stadt-expose', name: 'configuration_basic_settings_city_expose_')]
class CityExposeController extends AbstractController
{
    private const ACTIVE_NAV_GROUP = 'configuration';

    public function __construct(
        private readonly AccountService $accountService,
        private readonly DocumentDeletionService $documentDeletionService,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    #[Route('/uebersicht', name: 'overview', methods: ['GET'])]
    public function overview(UserInterface $user): Response
    {
        $account = $user->getAccount();

        return $this->render(view: 'configuration/basic_settings/city_expose/overview.html.twig', parameters: [
            'account'        => $account,
            'pageTitle'      => 'Konfiguration - Grundeinstellungen – Stadt-Exposé',
            'activeNavGroup' => self::ACTIVE_NAV_GROUP,
        ]);
    }

    #[Route('/pdf-upload', name: 'pdf_upload', methods: ['POST'])]
    public function pdfUpload(Request $request, UserInterface $user): JsonResponse
    {
        $cityExposePdfFile = $request->files->get(key: 'cityExposePdfFile');

        $account = $user->getAccount();

        $accountConfiguration = $this->accountService->fetchAccountConfigurationByAccount(account: $account);

        if ($accountConfiguration === null) {
            $accountConfiguration = new AccountConfiguration();

            $accountConfiguration->setAccount($account);

            $this->entityManager->persist($accountConfiguration);
        }

        if ($cityExposePdfFile === null) {
            throw new BadRequestHttpException();
        }

        $allowedFileMimeTypes = [
            'application/pdf',
        ];

        if (in_array(needle: $cityExposePdfFile->getMimeType(), haystack: $allowedFileMimeTypes) !== true) {
            throw new BadRequestHttpException();
        }

        $this->entityManager->beginTransaction();

        $currentCityExpose = $accountConfiguration->getCityExpose();

        $cityExpose = $this->accountService->createAndSaveCityExposeFromUploadedFile(uploadedFile: $cityExposePdfFile, accountUser: $user);

        $cityExpose->setTitle('Stadt-Exposé ' . $account->getName());

        $accountConfiguration->setCityExpose($cityExpose);

        $this->entityManager->flush();

        $this->entityManager->commit();

        if ($currentCityExpose !== null) {
            $this->documentDeletionService->deleteDocument(document: $currentCityExpose);
        }

        $this->addFlash(type: 'dataSaved', message: 'Das Stadt-Exposé wurde hochgeladen.');

        return new JsonResponse(data: ['success' => true], status: Response::HTTP_CREATED);
    }

    #[Route('/entfernen', name: 'delete', methods: ['GET', 'POST'])]
    public function delete(Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $accountConfiguration = $account->getAccountConfiguration();

        $cityExpose = $accountConfiguration->getCityExpose();

        if ($cityExpose === null) {
            throw new NotFoundHttpException();
        }

        $cityExposeDeleteForm = $this->createForm(type: CityExposeDeleteType::class);

        $cityExposeDeleteForm->add(child: 'delete', type: SubmitType::class);

        $cityExposeDeleteForm->handleRequest(request: $request);

        if ($cityExposeDeleteForm->isSubmitted() && $cityExposeDeleteForm->isValid()) {
            if ($cityExposeDeleteForm->get('verificationCode')->getData() === 'expose_' . $account->getId()) {
                $this->entityManager->beginTransaction();

                $accountConfiguration->setCityExpose(null);

                $this->entityManager->flush();

                $this->documentDeletionService->deleteDocument(document: $cityExpose);

                $this->entityManager->commit();

                $this->addFlash(type: 'dataDeleted', message: 'Das Stad-Exposé wurde entfernt.');

                return $this->redirectToRoute(route: 'configuration_basic_settings_city_expose_overview');
            } else {
                $this->addFlash(type: 'dataError', message: 'Der Verifizierungscode ist falsch.');
            }
        }

        return $this->render(view: 'configuration/basic_settings/city_expose/delete.html.twig', parameters: [
            'account'              => $account,
            'cityExposeDeleteForm' => $cityExposeDeleteForm,
            'pageTitle'            => 'Konfiguration - Grundeinstellungen – Stadt-Exposé entfernen',
            'activeNavGroup'       => self::ACTIVE_NAV_GROUP,
        ]);
    }
}
