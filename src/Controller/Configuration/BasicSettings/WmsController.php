<?php

declare(strict_types=1);

namespace App\Controller\Configuration\BasicSettings;

use App\Domain\Account\AccountDeletionService;
use App\Domain\Map\MapService;
use App\Form\Type\Configuration\BasicSettings\Wms\ChooseLayersType;
use App\Form\Type\Configuration\BasicSettings\Wms\WmsCapabilitiesUrlType;
use App\Form\Type\Configuration\BasicSettings\Wms\WmsLayerAddType;
use App\Form\Type\Configuration\BasicSettings\Wms\WmsLayerDeleteType;
use App\Form\Type\Configuration\BasicSettings\Wms\WmsLayerType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Security("is_granted('ROLE_ACCOUNT_ADMIN') or is_granted('ROLE_CONFIGURATOR')")]
#[Route('/konfiguration/grundeinstellungen/wms', name: 'configuration_basic_settings_wms_')]
class WmsController extends AbstractController
{
    private const ACTIVE_NAV_GROUP = 'configuration';

    public function __construct(
        private readonly string $appDefaultUri,
        private readonly MapService $mapService,
        private readonly AccountDeletionService $accountDeletionService,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    #[Route('/uebersicht', name: 'overview', methods: ['GET', 'POST'])]
    public function overview(Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $wmsLayerAddForm = $this->createForm(type: WmsLayerAddType::class, options: [
            'action' => $this->generateUrl(route: 'configuration_basic_settings_wms_choose_layers'),
            'method' => Request::METHOD_POST,
        ]);

        $wmsLayerAddForm->add(child: 'add', type: SubmitType::class);

        $wmsLayerAddForm->handleRequest(request: $request);

        $wmsCapabilitiesUrl = $this->appDefaultUri . '/mapserv?map=WMS&REQUEST=GetCapabilities&SERVICE=WMS';

        $wmsCapabilitiesUrlForm = $this->createForm(type: WmsCapabilitiesUrlType::class, options: [
            'wmsCapabilitiesUrl' => $wmsCapabilitiesUrl,
        ]);

        return $this->render(view: 'configuration/basic_settings/wms/overview.html.twig', parameters: [
            'wmsLayerAddForm'        => $wmsLayerAddForm,
            'wmsCapabilitiesUrlForm' => $wmsCapabilitiesUrlForm,
            'wmsLayers'              => $account->getWmsLayers(),
            'pageTitle'              => 'Konfiguration - Grundeinstelungen – WebMapServices',
            'activeNavGroup'         => self::ACTIVE_NAV_GROUP,
        ]);
    }

    #[Route('/ebenen-auswaehlen', name: 'choose_layers', methods: ['POST'])]
    public function chooseLayers(Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $wmsLayerAddForm = $this->createForm(type: WmsLayerAddType::class);

        $wmsLayerAddForm->add(child: 'add', type: SubmitType::class);

        $wmsLayerAddForm->handleRequest(request: $request);

        $chooseLayersForm = $this->createForm(type: ChooseLayersType::class, options: [
            'capabilitiesUrl' => null,
        ]);

        if ($wmsLayerAddForm->isSubmitted() && $wmsLayerAddForm->isValid()) {
            $capabilitiesUrl = $wmsLayerAddForm->get('capabilitiesUrl')->getData();

            $chooseLayersForm = $this->createForm(type: ChooseLayersType::class, options: [
                'capabilitiesUrl' => $capabilitiesUrl,
            ]);
        }

        $chooseLayersForm->add(child: 'save', type: SubmitType::class);

        $chooseLayersForm->handleRequest(request: $request);

        if ($chooseLayersForm->isSubmitted() && $chooseLayersForm->isValid()) {

            foreach ($chooseLayersForm->get('wmsLayers')->getData() as $wmsLayer) {
                $this->entityManager->persist(entity: $wmsLayer);

                $account->getWmsLayers()->add($wmsLayer);
            }

            $this->entityManager->flush();

            return $this->redirectToRoute(route: 'configuration_basic_settings_wms_overview');
        }

        return $this->render(view: 'configuration/basic_settings/wms/choose_layers.html.twig', parameters: [
            'chooseLayersForm' => $chooseLayersForm,
            'pageTitle'        => 'Konfiguration - Grundeinstelungen – WebMapServices',
            'activeNavGroup'   => self::ACTIVE_NAV_GROUP,
        ]);
    }

    #[Route('/{id<\d{1,10}>}', name: 'edit', methods: ['GET', 'POST'])]
    public function edit(int $id, Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $wmsLayer = $this->mapService->fetchWmsLayerById(id: $id);

        if ($wmsLayer === null) {
            throw new NotFoundHttpException();
        }

        if ($account->getWmsLayers()->contains(element: $wmsLayer) === false) {
            throw new NotFoundHttpException();
        }

        $wmsLayerForm = $this->createForm(type: WmsLayerType::class, data: $wmsLayer);

        $wmsLayerForm->add(child: 'save', type: SubmitType::class);

        $wmsLayerForm->handleRequest(request: $request);

        if ($wmsLayerForm->isSubmitted() && $wmsLayerForm->isValid()) {
            $this->entityManager->flush();

            $this->addFlash(type: 'dataSaved', message: 'Der WMS-Layer wurde bearbeitet.');

            return $this->redirectToRoute(route: 'configuration_basic_settings_wms_overview');
        }

        return $this->render(view: 'configuration/basic_settings/wms/edit.html.twig', parameters: [
            'wmsLayerForm'   => $wmsLayerForm,
            'wmsLayer'       => $wmsLayer,
            'pageTitle'      => 'Konfiguration - Grundeinstelungen – WebMapServices',
            'activeNavGroup' => self::ACTIVE_NAV_GROUP,
        ]);
    }

    #[Route('/{id<\d{1,10}>}/loeschen', name: 'delete', methods: ['GET', 'POST'])]
    public function delete(int $id, Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $wmsLayer = $this->mapService->fetchWmsLayerById(id: $id);

        if ($wmsLayer === null) {
            throw new NotFoundHttpException();
        }

        if ($account->getWmsLayers()->contains(element: $wmsLayer) === false) {
            throw new NotFoundHttpException();
        }

        $wmsLayerDeleteForm = $this->createForm(type: WmsLayerDeleteType::class);

        $wmsLayerDeleteForm->add(child: 'delete', type: SubmitType::class);

        $wmsLayerDeleteForm->handleRequest(request: $request);

        if (
            $wmsLayerDeleteForm->isSubmitted()
            && $wmsLayerDeleteForm->isValid()
            && $wmsLayerDeleteForm->get('verificationCode')->getData() === 'layer_' . $wmsLayer->getId()
        ) {
            $this->accountDeletionService->deleteWmsLayer(account: $account, wmsLayer: $wmsLayer);

            $this->addFlash(type: 'dataDeleted', message: 'Der WMS-Layer wurde gelöscht.');

            return $this->redirectToRoute(route: 'configuration_basic_settings_wms_overview');
        }

        return $this->render(view: 'configuration/basic_settings/wms/delete.html.twig', parameters: [
            'wmsLayerDeleteForm' => $wmsLayerDeleteForm,
            'wmsLayer'           => $wmsLayer,
            'pageTitle'          => 'Konfiguration - Grundeinstelungen – WebMapServices',
            'activeNavGroup'     => self::ACTIVE_NAV_GROUP,
        ]);
    }
}
