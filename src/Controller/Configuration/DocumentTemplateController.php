<?php

declare(strict_types=1);

namespace App\Controller\Configuration;

use App\Domain\DocumentTemplate\DocumentTemplateDeletionService;
use App\Domain\DocumentTemplate\DocumentTemplateService;
use App\Form\Type\DocumentTemplate\DocumentTemplateDeleteType;
use App\Form\Type\DocumentTemplate\DocumentTemplateType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Security("is_granted('ROLE_ACCOUNT_ADMIN')")]
#[Route('/konfiguration/dokumentenvorlagen', name: 'configuration_document_template_')]
class DocumentTemplateController extends AbstractController
{
    private const ACTIVE_NAV_GROUP = 'configuration';

    public function __construct(
        private readonly DocumentTemplateDeletionService $documentTemplateDeletionService,
        private readonly DocumentTemplateService $documentTemplateService,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    #[Route('/uebersicht', name: 'overview', methods: ['GET'])]
    public function overview(UserInterface $user): Response
    {
        return $this->render(view: 'configuration/document_template/overview.html.twig', parameters: [
            'documentTemplates' => $this->documentTemplateService->fetchDocumentTemplatesByAccount(account: $user->getAccount()),
            'pageTitle'         => 'Konfiguration - Dokumentenvorlagen',
            'activeNavGroup'    => self::ACTIVE_NAV_GROUP,
        ]);
    }

    #[Route('/upload', name: 'upload', methods: ['POST'])]
    public function upload(Request $request, UserInterface $user): JsonResponse
    {
        $documentTemplateFile = $request->files->get(key: 'documentTemplateFile');

        if ($documentTemplateFile === null) {
            throw new BadRequestHttpException();
        }

        $allowedFileMimeTypes = [
            'application/pdf',
            'application/msword',
            'application/msexcel',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        ];

        if (in_array(needle: $documentTemplateFile->getMimeType(), haystack: $allowedFileMimeTypes) !== true) {
            throw new BadRequestHttpException();
        }

        $this->entityManager->beginTransaction();

        $documentTemplate = $this->documentTemplateService->createAndSaveDocumentTemplateFromUploadedFile(
            uploadedFile: $documentTemplateFile,
            accountUser: $user
        );

        $documentTemplate->setName($documentTemplate->getFile()->getFileName());

        $this->entityManager->flush();
        $this->entityManager->commit();

        $flashBag = $request->getSession()->getFlashBag();

        if ($flashBag->has(type: 'dataSaved') === false) {
            $this->addFlash(type: 'dataSaved', message: 'Vorlage(n) wurde(n) hochgeladen.');
        }

        return new JsonResponse(['success' => true]);
    }

    #[Route('/{documentTemplateId<\d{1,10}>}', name: 'edit', methods: ['GET', 'POST'])]
    public function edit(int $documentTemplateId, Request $request, UserInterface $user): Response
    {
        $documentTemplate = $this->documentTemplateService->fetchDocumentTemplateById(account: $user->getAccount(), id: $documentTemplateId);

        if ($documentTemplate === null) {
            throw new NotFoundHttpException();
        }

        $documentTemplateForm = $this->createForm(type: DocumentTemplateType::class, data: $documentTemplate);

        $documentTemplateForm->add(child: 'save', type: SubmitType::class);

        $documentTemplateForm->handleRequest(request: $request);

        if ($documentTemplateForm->isSubmitted() && $documentTemplateForm->isValid()) {
            $this->entityManager->flush();

            $this->addFlash(type: 'dataSaved', message: 'Die Änderungen wurden gespeichert.');

            return $this->redirectToRoute(route: 'configuration_document_template_overview');
        }

        return $this->render(view: 'configuration/document_template/edit.html.twig', parameters: [
            'documentTemplate'     => $documentTemplate,
            'documentTemplateForm' => $documentTemplateForm,
            'pageTitle'            => 'Konfiguration - Dokumentenvorlage bearbeiten',
            'activeNavGroup'       => self::ACTIVE_NAV_GROUP,
        ]);
    }

    #[Route('/{documentTemplateId<\d{1,10}>}/loeschen', name: 'delete', methods: ['GET', 'POST'])]
    public function delete(int $documentTemplateId, Request $request, UserInterface $user): Response
    {
        $documentTemplate = $this->documentTemplateService->fetchDocumentTemplateById(account: $user->getAccount(), id: $documentTemplateId);

        if ($documentTemplate === null) {
            throw new NotFoundHttpException();
        }

        $documentTemplateDeleteForm = $this->createForm(type: DocumentTemplateDeleteType::class);

        $documentTemplateDeleteForm->add(child: 'delete', type: SubmitType::class);

        $documentTemplateDeleteForm->handleRequest(request: $request);

        if (
            $documentTemplateDeleteForm->isSubmitted()
            && $documentTemplateDeleteForm->isValid()
            && $documentTemplateDeleteForm->get('verificationCode')->getData() === 'vorlage_' . $documentTemplate->getId()
        ) {
            $this->documentTemplateDeletionService->deleteDocumentTemplate(documentTemplate: $documentTemplate);

            $this->addFlash(type: 'dataDeleted', message: 'Das Dokument wurde gelöscht.');

            return $this->redirectToRoute(route: 'configuration_document_template_overview');
        }

        return $this->render(view: 'configuration/document_template/delete.html.twig', parameters: [
            'documentTemplate'           => $documentTemplate,
            'documentTemplateDeleteForm' => $documentTemplateDeleteForm,
            'documentTemplateForm'       => $this->createForm(type: DocumentTemplateType::class, data: $documentTemplate),
            'pageTitle'                  => 'Konfiguration - Dokumentenvorlage bearbeiten',
            'activeNavGroup'             => self::ACTIVE_NAV_GROUP,
        ]);
    }
}
