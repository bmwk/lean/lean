<?php

declare(strict_types=1);

namespace App\Controller\Configuration\SurveyResult;

use App\Domain\Document\DocumentDeletionService;
use App\Domain\Document\DocumentService;
use App\Domain\Entity\SurveyResult\SurveyResult;
use App\Domain\Image\ImageDeletionService;
use App\Domain\Image\ImageService;
use App\Domain\SurveyResult\SurveyResultDeletionService;
use App\Domain\SurveyResult\SurveyResultService;
use App\Form\Type\Configuration\SurveyResult\SurveyResultDeleteType;
use App\Form\Type\Configuration\SurveyResult\SurveyResultType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Security("is_granted('ROLE_ACCOUNT_ADMIN')")]
#[Route('/konfiguration/umfrageergebnisse', name: 'configuration_survey_result_')]
class SurveyResultController extends AbstractController
{
    private const ACTIVE_NAV_GROUP = 'configuration';

    public function __construct(
        private readonly SurveyResultService $surveyResultService,
        private readonly ImageService $imageService,
        private readonly DocumentService $documentService,
        private readonly SurveyResultDeletionService $surveyResultDeletionService,
        private readonly ImageDeletionService $imageDeletionService,
        private readonly DocumentDeletionService $documentDeletionService,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    #[Route('/uebersicht', name: 'overview', methods: ['GET'])]
    public function overview(UserInterface $user): Response
    {
        return $this->render(view: 'configuration/survey_result/overview.html.twig', parameters: [
            'surveyResults'  => $this->surveyResultService->fetchSurveyResultsByAccount(account: $user->getAccount()),
            'pageTitle'      => 'Konfiguration - Umfrageergebnisse',
            'activeNavGroup' => self::ACTIVE_NAV_GROUP,
        ]);
    }

    #[Route('/einstellen', name: 'create', methods: ['GET', 'POST'])]
    public function create(Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $surveyResult = new SurveyResult();

        $surveyResultForm = $this->createForm(type: SurveyResultType::class, data: $surveyResult);

        $surveyResultForm->add(child:'save', type: SubmitType::class);

        $surveyResultForm->handleRequest(request: $request);

        if ($surveyResultForm->isSubmitted() && $surveyResultForm->isValid()) {
            $surveyResult = $surveyResultForm->getData();

            $surveyResult
                ->setAccount($account)
                ->setCreatedByAccountUser($user);

            $this->entityManager->persist(entity: $surveyResult);

            $this->entityManager->flush();

            $this->addFlash(type: 'dataSaved', message: 'Die Umfrage wurde erfolgreich angelegt.');

            return $this->redirectToRoute(route: 'configuration_survey_result_edit', parameters: ['surveyResultId' => $surveyResult->getId()]);
        }

        return $this->render(view: 'configuration/survey_result/create.html.twig', parameters: [
            'surveyResultForm' => $surveyResultForm,
            'pageTitle'        => 'Konfiguration - Umfrageergebnisse',
            'activeNavGroup'   => self::ACTIVE_NAV_GROUP,
        ]);
    }

    #[Route('/{surveyResultId<\d{1,10}>}', name: 'edit', methods: ['GET', 'POST'])]
    public function edit(int $surveyResultId, Request $request,  UserInterface $user): Response
    {
        $surveyResult = $this->surveyResultService->fetchSurveyResultByIdAndAccount(id: $surveyResultId, account: $user->getAccount());

        if ($surveyResult === null) {
            throw new NotFoundHttpException();
        }

        $surveyResultForm = $this->createForm(type: SurveyResultType::class, data: $surveyResult);

        $surveyResultForm->add(child:'save', type: SubmitType::class);

        $surveyResultForm->handleRequest(request: $request);

        if ($surveyResultForm->isSubmitted() && $surveyResultForm->isValid()) {
            $this->entityManager->flush();

            $this->addFlash(type: 'dataSaved', message: 'Die Umfrage wurde erfolgreich geändert.');
        }

        return $this->render(view: 'configuration/survey_result/edit.html.twig', parameters: [
            'surveyResultForm' => $surveyResultForm,
            'surveyResult'     => $surveyResult,
            'pageTitle'        => 'Konfiguration - Umfrageergebnisse',
            'activeNavGroup'   => self::ACTIVE_NAV_GROUP,
        ]);
    }

    #[Route('/{surveyResultId<\d{1,10}>}/loeschen', name: 'delete', methods: ['GET', 'POST'])]
    public function delete(int $surveyResultId, Request $request, UserInterface $user): Response
    {
        $surveyResult = $this->surveyResultService->fetchSurveyResultByIdAndAccount(id: $surveyResultId, account: $user->getAccount());

        if ($surveyResult === null) {
            throw new NotFoundHttpException();
        }

        $surveyResultDeleteForm = $this->createForm(type: SurveyResultDeleteType::class);

        $surveyResultDeleteForm->add(child: 'delete', type: SubmitType::class);

        $surveyResultDeleteForm->handleRequest(request: $request);

        if ($surveyResultDeleteForm->isSubmitted() && $surveyResultDeleteForm->isValid()) {
            if ($surveyResultDeleteForm->get('verificationCode')->getData() !== 'umfrage_' . $surveyResult->getId()) {
                $this->addFlash(type: 'dataError', message: 'Der eingegebene Verifizierungscode ist falsch.');

                return $this->redirectToRoute(route: 'configuration_survey_result_delete', parameters: ['surveyResultId' => $surveyResultId]);
            }

            $this->surveyResultDeletionService->deleteSurveyResult(surveyResult: $surveyResult);

            $this->addFlash(type: 'dataDeleted', message: 'Das Umfrageergebnis wurden gelöscht.');

            return $this->redirectToRoute(route: 'configuration_survey_result_overview');
        }

        return $this->render(view: 'configuration/survey_result/delete.html.twig', parameters: [
            'surveyResult'           => $surveyResult,
            'surveyResultDeleteForm' => $surveyResultDeleteForm,
            'pageTitle'              => 'Konfiguration - Umfrageergebnisse',
            'activeNavGroup'         => self::ACTIVE_NAV_GROUP,
        ]);
    }

    #[Route('/{surveyResultId<\d{1,10}>}/bild-upload', name: 'image_upload', methods: ['POST'])]
    public function imageUpload(int $surveyResultId, Request $request, UserInterface $user): Response
    {
        $surveyResult = $this->surveyResultService->fetchSurveyResultByIdAndAccount(id: $surveyResultId, account: $user->getAccount());

        if ($surveyResult === null) {
            throw new NotFoundHttpException();
        }

        $imageUploadedFile = $request->files->get(key: 'imageFile');

        if ($imageUploadedFile === null) {
            throw new BadRequestHttpException();
        }

        $allowedFileMimeTypes = [
            'image/jpeg',
            'image/png',
        ];

        if (in_array(needle: $imageUploadedFile->getMimeType(), haystack: $allowedFileMimeTypes) !== true) {
            throw new BadRequestHttpException();
        }

        $image = $this->imageService->createAndSaveImageFromUploadedFile(
            uploadedFile: $imageUploadedFile,
            public: false,
            resized: false,
            withThumbnail: true,
            accountUser: $user,
            title: 'Bild zur Umfrage ' . $surveyResult->getTitle()
        );

        $currentImage = $surveyResult->getImage();

        $surveyResult->setImage($image);

        $this->entityManager->flush();

        if ($currentImage !== null) {
            $this->imageDeletionService->deleteImage(image: $currentImage);
        }

        $this->addFlash(type: 'dataSaved', message: 'Das Bild wurde hochgeladen.');

        return new JsonResponse(data: ['success' => true]);
    }

    #[Route('/{surveyResultId<\d{1,10}>}/dokument-upload', name: 'document_upload', methods: ['POST'])]
    public function documentUpload(int $surveyResultId, Request $request, UserInterface $user): Response
    {
        $surveyResult = $this->surveyResultService->fetchSurveyResultByIdAndAccount(id: $surveyResultId, account: $user->getAccount());

        if ($surveyResult === null) {
            throw new NotFoundHttpException();
        }

        $documentUploadedFile = $request->files->get(key: 'documentFile');

        if ($documentUploadedFile === null) {
            throw new BadRequestHttpException();
        }

        $allowedFileMimeTypes = [
            'application/pdf',
        ];

        if (in_array(needle: $documentUploadedFile->getMimeType(), haystack: $allowedFileMimeTypes) !== true) {
            throw new BadRequestHttpException();
        }

        $document = $this->documentService->createAndSaveDocumentFromUploadedFile(
            uploadedFile: $documentUploadedFile,
            public: false,
            accountUser: $user,
            title: 'Umfrage ' . $surveyResult->getTitle()
        );

        $currentDocument = $surveyResult->getDocument();

        $surveyResult->setDocument($document);

        $this->entityManager->flush();

        if ($currentDocument !== null) {
            $this->documentDeletionService->deleteDocument(document: $currentDocument);
        }

        $this->addFlash(type: 'dataSaved', message: 'Das Dokument wurde hochgeladen.');

        return new JsonResponse(data: ['success' => true]);
    }
}
