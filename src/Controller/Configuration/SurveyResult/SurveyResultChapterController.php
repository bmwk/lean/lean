<?php

declare(strict_types=1);

namespace App\Controller\Configuration\SurveyResult;

use App\Domain\Document\DocumentDeletionService;
use App\Domain\Document\DocumentService;
use App\Domain\Entity\SurveyResult\SurveyResultChapter;
use App\Domain\Image\ImageDeletionService;
use App\Domain\Image\ImageService;
use App\Domain\SurveyResult\SurveyResultDeletionService;
use App\Domain\SurveyResult\SurveyResultService;
use App\Form\Type\Configuration\SurveyResult\SurveyResultChapterDeleteType;
use App\Form\Type\Configuration\SurveyResult\SurveyResultChapterType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Security("is_granted('ROLE_ACCOUNT_ADMIN')")]
#[Route('/konfiguration/umfrageergebnisse/{surveyResultId<\d{1,10}>}/kapitel', name: 'configuration_survey_result_survey_result_chapter_')]
class SurveyResultChapterController extends AbstractController
{
    private const ACTIVE_NAV_GROUP = 'configuration';

    public function __construct(
        private readonly SurveyResultService $surveyResultService,
        private readonly ImageService $imageService,
        private readonly DocumentService $documentService,
        private readonly SurveyResultDeletionService $surveyResultDeletionService,
        private readonly ImageDeletionService $imageDeletionService,
        private readonly DocumentDeletionService $documentDeletionService,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    #[Route('/einstellen', name: 'create', methods: ['GET', 'POST'])]
    public function create(int $surveyResultId, Request $request, UserInterface $user): Response
    {
        $surveyResult = $this->surveyResultService->fetchSurveyResultByIdAndAccount(id: $surveyResultId, account: $user->getAccount());

        if ($surveyResult === null) {
            throw new NotFoundHttpException();
        }

        $surveyResultChapter = new SurveyResultChapter();

        $surveyResultChapterForm = $this->createForm(type: SurveyResultChapterType::class, data: $surveyResultChapter);

        $surveyResultChapterForm->add(child:'save', type: SubmitType::class);

        $surveyResultChapterForm->handleRequest(request: $request);

        if ($surveyResultChapterForm->isSubmitted() && $surveyResultChapterForm->isValid()) {
            $surveyResultChapter = $surveyResultChapterForm->getData();

            $surveyResultChapter
                ->setSurveyResult($surveyResult)
                ->setCreatedByAccountUser($user);

            $this->entityManager->persist(entity: $surveyResultChapter);

            $this->entityManager->flush();

            $this->addFlash(type: 'dataSaved', message: 'Das Kapitel wurde erfolgreich angelegt.');

            return $this->redirectToRoute(route: 'configuration_survey_result_survey_result_chapter_edit', parameters: [
                'surveyResultId'        => $surveyResult->getId(),
                'surveyResultChapterId' => $surveyResultChapter->getId()
            ]);
        }

        return $this->render(view: 'configuration/survey_result/survey_result_chapter/create.html.twig', parameters: [
            'surveyResultChapterForm' => $surveyResultChapterForm,
            'surveyResult'            => $surveyResult,
            'pageTitle'               => 'Konfiguration - Umfrageergebnisse',
            'activeNavGroup'          => self::ACTIVE_NAV_GROUP,
        ]);
    }

    #[Route('/{surveyResultChapterId<\d{1,10}>}', name: 'edit', methods: ['GET', 'POST'])]
    public function edit(int $surveyResultId, int $surveyResultChapterId, Request $request, UserInterface $user): Response
    {
        $surveyResult = $this->surveyResultService->fetchSurveyResultByIdAndAccount(id: $surveyResultId, account: $user->getAccount());

        if ($surveyResult === null) {
            throw new NotFoundHttpException();
        }

        $surveyResultChapter = $this->surveyResultService->fetchSurveyResultChapterByIdAndSurveyResult(id: $surveyResultChapterId, surveyResult: $surveyResult);

        if ($surveyResultChapter === null) {
            throw new NotFoundHttpException();
        }

        $surveyResultChapterForm = $this->createForm(type: SurveyResultChapterType::class, data: $surveyResultChapter);

        $surveyResultChapterForm->add(child:'save', type: SubmitType::class);

        $surveyResultChapterForm->handleRequest(request: $request);

        if ($surveyResultChapterForm->isSubmitted() && $surveyResultChapterForm->isValid()) {
            $this->entityManager->flush();

            $this->addFlash(type: 'dataSaved', message: 'Das Kapitel wurde erfolgreich geändert.');
        }

        return $this->render(view: 'configuration/survey_result/survey_result_chapter/edit.html.twig', parameters: [
            'surveyResultChapterForm' => $surveyResultChapterForm,
            'surveyResultChapter'     => $surveyResultChapter,
            'pageTitle'               => 'Konfiguration - Umfrageergebnisse',
            'activeNavGroup'          => self::ACTIVE_NAV_GROUP,
        ]);
    }

    #[Route('{surveyResultChapterId<\d{1,10}>}/loeschen', name: 'delete', methods: ['GET', 'POST'])]
    public function delete(int $surveyResultId, int $surveyResultChapterId, Request $request, UserInterface $user): Response
    {
        $surveyResult = $this->surveyResultService->fetchSurveyResultByIdAndAccount(id: $surveyResultId, account: $user->getAccount());

        if ($surveyResult === null) {
            throw new NotFoundHttpException();
        }

        $surveyResultChapter = $this->surveyResultService->fetchSurveyResultChapterByIdAndSurveyResult(id: $surveyResultChapterId, surveyResult: $surveyResult);

        if ($surveyResultChapter === null) {
            throw new NotFoundHttpException();
        }

        $surveyResultChapterDeleteForm = $this->createForm(type: SurveyResultChapterDeleteType::class);

        $surveyResultChapterDeleteForm->add(child: 'delete', type: SubmitType::class);

        $surveyResultChapterDeleteForm->handleRequest(request: $request);

        if ($surveyResultChapterDeleteForm->isSubmitted() && $surveyResultChapterDeleteForm->isValid()) {
            if ($surveyResultChapterDeleteForm->get('verificationCode')->getData() !== 'kapitel_' . $surveyResultChapter->getId()) {
                $this->addFlash(type: 'dataError', message: 'Der eingegebene Verifizierungscode ist falsch.');

                return $this->redirectToRoute(route: 'configuration_survey_result_survey_result_chapter_delete', parameters: [
                    'surveyResultId'        => $surveyResult->getId(),
                    'surveyResultChapterId' => $surveyResultChapter->getId(),
                ]);
            }

            $this->surveyResultDeletionService->deleteSurveyResultChapter(surveyResultChapter: $surveyResultChapter);

            $this->addFlash(type: 'dataDeleted', message: 'Das Kapitel wurde gelöscht.');

            return $this->redirectToRoute(route: 'configuration_survey_result_edit', parameters: ['surveyResultId' => $surveyResult->getId()]);
        }

        return $this->render(view: 'configuration/survey_result/survey_result_chapter/delete.html.twig', parameters: [
            'surveyResultChapter'           => $surveyResultChapter,
            'surveyResultChapterDeleteForm' => $surveyResultChapterDeleteForm,
            'pageTitle'                     => 'Konfiguration - Umfrageergebnisse',
            'activeNavGroup'                => self::ACTIVE_NAV_GROUP,
        ]);
    }

    #[Route('/{surveyResultChapterId<\d{1,10}>}/bild-upload', name: 'image_upload', methods: ['POST'])]
    public function imageUpload(int $surveyResultId, int $surveyResultChapterId, Request $request, UserInterface $user): Response
    {
        $surveyResult = $this->surveyResultService->fetchSurveyResultByIdAndAccount(id: $surveyResultId, account: $user->getAccount());

        if ($surveyResult === null) {
            throw new NotFoundHttpException();
        }

        $surveyResultChapter = $this->surveyResultService->fetchSurveyResultChapterByIdAndSurveyResult(id: $surveyResultChapterId, surveyResult: $surveyResult);

        if ($surveyResultChapter === null) {
            throw new NotFoundHttpException();
        }

        $imageUploadedFile = $request->files->get(key: 'chapterImageFile');

        if ($imageUploadedFile === null) {
            throw new BadRequestHttpException();
        }

        $allowedFileMimeTypes = [
            'image/jpeg',
            'image/png',
        ];

        if (in_array(needle: $imageUploadedFile->getMimeType(), haystack: $allowedFileMimeTypes) !== true) {
            throw new BadRequestHttpException();
        }

        $image = $this->imageService->createAndSaveImageFromUploadedFile(
            uploadedFile: $imageUploadedFile,
            public: false,
            resized: false,
            withThumbnail: true,
            accountUser: $user,
            title: 'Bild zum Kapitel ' . $surveyResultChapter->getTitle()
        );

        $currentImage = $surveyResult->getImage();

        $surveyResultChapter->setImage($image);

        $this->entityManager->flush();

        if ($currentImage !== null) {
            $this->imageDeletionService->deleteImage(image: $currentImage);
        }

        $this->addFlash(type: 'dataSaved', message: 'Das Bild wurde hochgeladen.');

        return new JsonResponse(data: ['success' => true]);
    }

    #[Route('{surveyResultChapterId<\d{1,10}>}/dokument-upload', name: 'document_upload', methods: ['POST'])]
    public function documentUpload(int $surveyResultId, int $surveyResultChapterId, Request $request, UserInterface $user): Response
    {
        $surveyResult = $this->surveyResultService->fetchSurveyResultByIdAndAccount(id: $surveyResultId, account: $user->getAccount());

        if ($surveyResult === null) {
            throw new NotFoundHttpException();
        }

        $surveyResultChapter = $this->surveyResultService->fetchSurveyResultChapterByIdAndSurveyResult(id: $surveyResultChapterId, surveyResult: $surveyResult);

        if ($surveyResultChapter === null) {
            throw new NotFoundHttpException();
        }

        $documentUploadedFile = $request->files->get('chapterDocumentFile');

        if ($documentUploadedFile === null) {
            throw new BadRequestHttpException();
        }

        $allowedFileMimeTypes = [
            'application/pdf',
        ];

        if (in_array(needle: $documentUploadedFile->getMimeType(), haystack: $allowedFileMimeTypes) !== true) {
            throw new BadRequestHttpException();
        }

        $document = $this->documentService->createAndSaveDocumentFromUploadedFile(
            uploadedFile: $documentUploadedFile,
            public: false,
            accountUser: $user,
            title: 'Umfrage ' . $surveyResult->getTitle()
        );

        $currentDocument = $surveyResult->getDocument();

        $surveyResultChapter->setDocument($document);

        $this->entityManager->flush();

        if ($currentDocument !== null) {
            $this->documentDeletionService->deleteDocument(document: $currentDocument);
        }

        $this->addFlash(type: 'dataSaved', message: 'Das Dokument wurde hochgeladen.');

        return new JsonResponse(data: ['success' => true]);
    }
}
