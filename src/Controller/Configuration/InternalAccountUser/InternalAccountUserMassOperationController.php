<?php

declare(strict_types=1);

namespace App\Controller\Configuration\InternalAccountUser;

use App\Domain\Account\AccountUserDeletionService;
use App\Domain\Account\AccountUserService;
use App\Domain\Entity\AccountUser\AccountUserType;
use App\Domain\Entity\DeletionType;
use App\Form\Type\Configuration\InternalAccountUser\AccountUserMassOperationDeleteType;
use App\Form\Type\Configuration\InternalAccountUser\AccountUserMassOperationLockType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Security("is_granted('ROLE_ACCOUNT_ADMIN')")]
#[Route('/konfiguration/benutzer/massenoperation', name: 'configuration_internal_account_user_mass_operation_')]
class InternalAccountUserMassOperationController extends AbstractController
{
    private const ACTIVE_NAV_GROUP = 'configuration';

    public function __construct(
        private readonly AccountUserDeletionService $accountUserDeletionService,
        private readonly AccountUserService $accountUserService,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    #[Security("is_granted('ROLE_ACCOUNT_ADMIN')")]
    #[Route('/loeschen', name: 'delete', methods: ['POST'])]
    public function delete(Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $accountUserMassOperationDeleteForm = $this->createForm(type: AccountUserMassOperationDeleteType::class);

        $accountUserMassOperationDeleteForm->add(child: 'delete', type: SubmitType::class);

        $accountUserMassOperationDeleteForm->handleRequest(request: $request);

        if ($request->get('selectedRowIds') !== null) {
            $accountUserMassOperationDeleteForm->get('selectedRowIds')->setData($request->get('selectedRowIds'));
        }

        $accountUsers = $this->accountUserService->fetchAccountUsersByIds(
            account: $account,
            withFromSubAccounts: false,
            ids: json_decode($accountUserMassOperationDeleteForm->get('selectedRowIds')->getData())
        );

        if (
            $accountUserMassOperationDeleteForm->isSubmitted()
            && $accountUserMassOperationDeleteForm->isValid()
            && $accountUserMassOperationDeleteForm->get('verificationCode')->getData() === 'benutzer_' . count($accountUsers)
        ) {
            foreach ($accountUsers as $accountUser) {
                if ($accountUser->getAccountUserType() !== AccountUserType::INTERNAL) {
                    continue;
                }

                if ($this->isGranted(attribute: 'delete', subject: $accountUser) === false) {
                    continue;
                }

                $this->accountUserDeletionService->deleteAccountUser(
                    accountUser: $accountUser,
                    deletionType: DeletionType::ANONYMIZATION,
                    deletedByAccountUser: $user
                );
            }

            $this->addFlash(type: 'dataDeleted', message: 'Die Sachbearbeiter:innen wurden gelöscht');

            return $this->redirectToRoute(route: 'configuration_internal_account_user_overview');
        }

        return $this->render(view: 'configuration/internal_account_user/mass_operation/delete.html.twig', parameters: [
            'accountUserMassOperationDeleteForm' => $accountUserMassOperationDeleteForm,
            'accountUsers'                       => $accountUsers,
            'pageTitle'                          => 'Nutzermanagement | Sachbearbeiter:innen löschen',
            'activeNavGroup'                     => self::ACTIVE_NAV_GROUP,
        ]);
    }

    #[Security("is_granted('ROLE_ACCOUNT_ADMIN')")]
    #[Route('/sperren', name: 'lock', methods: ['POST'])]
    public function lock(Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $accountUserMassOperationLockForm = $this->createForm(type: AccountUserMassOperationLockType::class);

        $accountUserMassOperationLockForm->add(child: 'lock', type: SubmitType::class);

        $accountUserMassOperationLockForm->handleRequest(request: $request);

        if ($request->get('selectedRowIds') !== null) {
            $accountUserMassOperationLockForm->get('selectedRowIds')->setData($request->get('selectedRowIds'));
        }

        $accountUsers = $this->accountUserService->fetchAccountUsersByIds(
            account: $account,
            withFromSubAccounts: false,
            ids: json_decode($accountUserMassOperationLockForm->get('selectedRowIds')->getData())
        );

        if ($accountUserMassOperationLockForm->isSubmitted() && $accountUserMassOperationLockForm->isValid()) {
            foreach ($accountUsers as $accountUser) {
                if ($accountUser->getAccountUserType() !== AccountUserType::INTERNAL) {
                    continue;
                }

                if ($this->isGranted(attribute: 'edit', subject: $accountUser) === false) {
                    continue;
                }

               $accountUser->setEnabled(false);
            }

            $this->entityManager->flush();

            $this->addFlash(type: 'dataDeleted', message: 'Die Sachbearbeiter:innen wurden gesperrt');

            return $this->redirectToRoute(route: 'configuration_internal_account_user_overview');
        }

        return $this->render(view: 'configuration/internal_account_user/mass_operation/lock.html.twig', parameters: [
            'accountUserMassOperationLockForm' => $accountUserMassOperationLockForm,
            'accountUsers'                     => $accountUsers,
            'pageTitle'                        => 'Nutzermanagement | Sachbearbeiter:innen sperren',
            'activeNavGroup'                   => self::ACTIVE_NAV_GROUP,
        ]);
    }
}
