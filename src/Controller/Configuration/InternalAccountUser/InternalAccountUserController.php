<?php

declare(strict_types=1);

namespace App\Controller\Configuration\InternalAccountUser;

use App\Controller\AbstractLeAnController;
use App\Domain\Account\AccountUserDeletionService;
use App\Domain\Account\AccountUserEmailService;
use App\Domain\Account\AccountUserPasswordResetService;
use App\Domain\Account\AccountUserService;
use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\AccountUser\AccountUserFilter;
use App\Domain\Entity\AccountUser\AccountUserRole;
use App\Domain\Entity\AccountUser\AccountUserSearch;
use App\Domain\Entity\AccountUser\AccountUserType;
use App\Domain\Entity\DeletionType;
use App\Domain\Entity\PaginationParameter;
use App\Domain\Entity\SortingOption\SortingOption;
use App\Domain\SessionStoredUrlParameter\SessionStoredUrlParameterService;
use App\Form\Type\Configuration\InternalAccountUser\AccountUserDeleteType;
use App\Form\Type\Configuration\InternalAccountUser\InternalAccountUserFilterType;
use App\Form\Type\Configuration\InternalAccountUser\InternalAccountUserSearchType;
use App\Form\Type\Configuration\InternalAccountUser\InternalAccountUserType;
use App\SessionStorage\SessionStorageService;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Security("is_granted('ROLE_ACCOUNT_ADMIN')")]
#[Route('/konfiguration/benutzer', name: 'configuration_internal_account_user_')]
class InternalAccountUserController extends AbstractLeAnController
{
    private const ACTIVE_NAV_GROUP = 'configuration';

    private const OVERVIEW_ROUTE_NAME = 'configuration_internal_account_user_overview';

    private const OVERVIEW_SESSION_KEY_NAME = 'internalAccountUserOverview';

    private const URL_PARAMETER_OBJECT_CLASSES = [
        PaginationParameter::class,
        SortingOption::class,
    ];

    public function __construct(
        SessionStorageService $sessionStorageService,
        SessionStoredUrlParameterService $sessionStoredUrlParameterService,
        private readonly AccountUserDeletionService $accountUserDeletionService,
        private readonly AccountUserEmailService $accountUserEmailService,
        private readonly AccountUserService $accountUserService,
        private readonly AccountUserPasswordResetService $accountUserPasswordResetService,
        private readonly string $projectDir,
        private readonly UserPasswordHasherInterface $passwordHasher,
        private readonly EntityManagerInterface $entityManager
    ) {
        parent::__construct(
            sessionStorageService: $sessionStorageService,
            sessionStoredUrlParameterService: $sessionStoredUrlParameterService
        );
    }

    #[Security("is_granted('ROLE_ACCOUNT_ADMIN')")]
    #[Route('/uebersicht', name: 'overview', methods: ['GET', 'POST'])]
    public function overview(Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $paginationParameter = PaginationParameter::createFromRequest(request: $request);

        $this->storeObjectInSession(object: $paginationParameter, sessionKeyName: self::OVERVIEW_SESSION_KEY_NAME);

        $sortingOption = null;

        if ($request->query->get(key: 'sortieren') !== null) {
            $sortingOption = SortingOption::createFromRequest(request: $request);

            $this->storeObjectInSession(object: $sortingOption, sessionKeyName: self::OVERVIEW_SESSION_KEY_NAME);
        }

        $internalAccountUserFilterForm = $this->createForm(type: InternalAccountUserFilterType::class, options: [
            'dataClassName'  => AccountUserFilter::class,
            'sessionKeyName' => self::OVERVIEW_SESSION_KEY_NAME,
        ]);

        $internalAccountUserFilterForm->add(child: 'apply', type: SubmitType::class);

        $internalAccountUserFilterForm->handleRequest(request: $request);

        $internalAccountUserSearchForm = $this->createForm(type: InternalAccountUserSearchType::class, options: [
            'dataClassName'  => AccountUserSearch::class,
            'sessionKeyName' => self::OVERVIEW_SESSION_KEY_NAME,
        ]);

        $internalAccountUserSearchForm->add(child: 'search', type: SubmitType::class);

        $internalAccountUserSearchForm->handleRequest(request: $request);

        $accountUsers = $this->accountUserService->fetchAccountUsersPaginatedByAccount(
            account: $account,
            withFromSubAccounts: false,
            firstResult: $paginationParameter->getFirstResult(),
            maxResults: $paginationParameter->getLimit(),
            accountUserTypes: [AccountUserType::INTERNAL],
            accountUserFilter: $internalAccountUserFilterForm->getData(),
            accountUserSearch: $internalAccountUserSearchForm->getData(),
            sortingOption: $sortingOption
        );

        return $this->render(view: 'configuration/internal_account_user/overview.html.twig', parameters: [
            'internalAccountUserFilterForm' => $internalAccountUserFilterForm,
            'internalAccountUserSearchForm' => $internalAccountUserSearchForm,
            'accountUsers'                  => $accountUsers,
            'activeNavGroup'                => self::ACTIVE_NAV_GROUP,
        ]);
    }

    #[Security("is_granted('ROLE_ACCOUNT_ADMIN')")]
    #[Route('/anlegen', name: 'create', methods: ['GET', 'POST'])]
    public function create(Request $request, UserInterface $user): Response
    {
        $accountUser = new AccountUser();

        $accountUser->setRoles([AccountUserRole::ROLE_USER->value]);

        $internalAccountUserForm = $this->createForm(type: InternalAccountUserType::class, data: $accountUser);

        $internalAccountUserForm->add(child: 'save', type: SubmitType::class);

        $internalAccountUserForm->handleRequest(request: $request);

        if ($internalAccountUserForm->isSubmitted() && $internalAccountUserForm->isValid()) {
            $this->entityManager->beginTransaction();

            $accountUser
                ->setAccount($user->getAccount())
                ->setAccountUserType(AccountUserType::INTERNAL)
                ->setAnonymized(false)
                ->setDeleted(false)
                ->setEnabled(true)
                ->setPassword($this->passwordHasher->hashPassword(user: $accountUser, plainPassword: bin2hex(string: random_bytes(length: 10))));

            $this->entityManager->persist(entity: $accountUser);

            $this->entityManager->flush();

            $accountUserPasswordReset = $this->accountUserPasswordResetService->createAccountUserPasswordReset(
                accountUser: $accountUser,
                createdByAccountUser: $user,
                daysUntilExpired: 7
            );

            $this->entityManager->persist(entity: $accountUserPasswordReset);

            $this->entityManager->flush();

            $this->entityManager->commit();

            $this->accountUserEmailService->sendAccountUserCreatedMail(accountUserPasswordReset: $accountUserPasswordReset);

            $this->addFlash(type: 'dataSaved', message: $accountUser->getFullName() . ' wurde als Nutzer:in angelegt und hat eine E-Mail zum Setzen eines Passworts erhalten.');

            $urlParameters = $this->fetchMergedUrlParametersFromSession(
                urlParameterObjectClasses: self::URL_PARAMETER_OBJECT_CLASSES,
                sessionKeyName: self::OVERVIEW_SESSION_KEY_NAME
            );

            return $this->redirectToRoute(route: self::OVERVIEW_ROUTE_NAME, parameters: $urlParameters);
        }

        return $this->render(view: 'configuration/internal_account_user/create.html.twig', parameters: [
            'internalAccountUserForm' => $internalAccountUserForm,
            'submitResponse'          => $request->getSession()->getFlashBag()->get(type: 'submitResponse'),
            'pageTitle'               => 'Nutzer:in anlegen',
            'activeNavGroup'          => self::ACTIVE_NAV_GROUP,
        ]);
    }

    #[Security("is_granted('ROLE_ACCOUNT_ADMIN')")]
    #[Route('/{accountUserId<\d{1,10}>}', name: 'edit', methods: ['GET', 'POST'])]
    public function edit(int $accountUserId, Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $accountUser = $this->accountUserService->fetchAccountUserById(account: $account, withFromSubAccounts: false, id: $accountUserId);

        if ($accountUser === null) {
            throw new NotFoundHttpException();
        }

        if ($accountUser->getAccountUserType() !== AccountUserType::INTERNAL) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'view', subject: $accountUser);

        $internalAccountUserForm = $this->createForm(type: InternalAccountUserType::class, data: $accountUser);

        $internalAccountUserForm->add(child: 'save', type: SubmitType::class);

        $internalAccountUserForm->handleRequest(request: $request);

        if ($internalAccountUserForm->isSubmitted() && $internalAccountUserForm->isValid()) {
            $this->denyAccessUnlessGranted(attribute: 'edit', subject: $accountUser);

            $this->entityManager->flush();

            $this->addFlash(type: 'dataSaved', message: 'Die Änderungen wurden gespeichert.');

            $urlParameters = $this->fetchMergedUrlParametersFromSession(
                urlParameterObjectClasses: self::URL_PARAMETER_OBJECT_CLASSES,
                sessionKeyName: self::OVERVIEW_SESSION_KEY_NAME
            );

            return $this->redirectToRoute(route: self::OVERVIEW_ROUTE_NAME, parameters: $urlParameters);
        }

        return $this->render(view: 'configuration/internal_account_user/edit.html.twig', parameters: [
            'internalAccountUserForm' => $internalAccountUserForm,
            'accountUser'             => $accountUser,
            'submitResponse'          => $request->getSession()->getFlashBag()->get(type: 'submitResponse'),
            'pageTitle'               => 'Stammdaten verwalten',
            'activeNavGroup'          => self::ACTIVE_NAV_GROUP,
        ]);
    }

    #[Security("is_granted('ROLE_ACCOUNT_ADMIN')")]
    #[Route('/{accountUserId<\d{1,10}>}/loeschen', name: 'delete', methods: ['GET', 'POST'])]
    public function delete(int $accountUserId, Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $accountUser = $this->accountUserService->fetchAccountUserById(account: $account, withFromSubAccounts: false, id: $accountUserId);

        if ($accountUser === null) {
            throw new NotFoundHttpException();
        }

        if ($accountUser->getAccountUserType() !== AccountUserType::INTERNAL) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'delete', subject: $accountUser);

        $accountUserDeleteForm = $this->createForm(type: AccountUserDeleteType::class);

        $accountUserDeleteForm->add(child: 'delete', type: SubmitType::class);

        $accountUserDeleteForm->handleRequest(request: $request);

        if (
            $accountUserDeleteForm->isSubmitted()
            && $accountUserDeleteForm->isValid()
            && $accountUserDeleteForm->get('verificationCode')->getData() === 'benutzer_' . $accountUser->getId()
        ) {
            $this->accountUserDeletionService->deleteAccountUser(
                accountUser: $accountUser,
                deletionType: DeletionType::ANONYMIZATION,
                deletedByAccountUser: $user
            );

            $this->addFlash(type: 'dataDeleted', message: 'Der Account wurde gelöscht.');

            $urlParameters = $this->fetchMergedUrlParametersFromSession(
                urlParameterObjectClasses: self::URL_PARAMETER_OBJECT_CLASSES,
                sessionKeyName: self::OVERVIEW_SESSION_KEY_NAME
            );

            return $this->redirectToRoute(route: self::OVERVIEW_ROUTE_NAME, parameters: $urlParameters);
        }

        $internalAccountUserForm = $this->createForm(type: InternalAccountUserType::class, data: $accountUser);

        $internalAccountUserForm->add(child: 'save', type: SubmitType::class);

        $internalAccountUserForm->handleRequest(request: $request);

        return $this->render(view: 'configuration/internal_account_user/delete.html.twig', parameters: [
            'accountUserDeleteForm'   => $accountUserDeleteForm,
            'internalAccountUserForm' => $internalAccountUserForm,
            'accountUser'             => $accountUser,
            'submitResponse'          => $request->getSession()->getFlashBag()->get(type: 'submitResponse'),
            'pageTitle'               => 'Löschen',
            'activeNavGroup'          => self::ACTIVE_NAV_GROUP,
        ]);
    }

    #[Security("is_granted('ROLE_ACCOUNT_ADMIN')")]
    #[Route('/{accountUserId<\d{1,10}>}/sperren', name: 'block', methods: ['GET', 'POST'])]
    public function block(int $accountUserId, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $accountUser = $this->accountUserService->fetchAccountUserById(account: $account, withFromSubAccounts: false, id: $accountUserId);

        if ($accountUser === null) {
            throw new NotFoundHttpException();
        }

        if ($accountUser->getAccountUserType() !== AccountUserType::INTERNAL) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'edit', subject: $accountUser);

        $accountUser->setEnabled(false);

        $this->entityManager->flush();

        $this->addFlash(type: 'dataDeleted', message: 'Der Account wurde gesperrt.');

        return $this->redirectToRoute(route: 'configuration_internal_account_user_edit', parameters: ['accountUserId' => $accountUser->getId()]);
    }

    #[Security("is_granted('ROLE_ACCOUNT_ADMIN')")]
    #[Route('/{accountUserId<\d{1,10}>}/entsperren', name: 'unblock', methods: ['GET', 'POST'])]
    public function unblock(int $accountUserId, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $accountUser = $this->accountUserService->fetchAccountUserById(account: $account, withFromSubAccounts: false, id: $accountUserId);

        if ($accountUser === null) {
            throw new NotFoundHttpException();
        }

        if ($accountUser->getAccountUserType() !== AccountUserType::INTERNAL) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'edit', subject: $accountUser);

        $accountUser->setEnabled(true);

        $this->entityManager->flush();

        $this->addFlash(type: 'dataSaved', message: 'Der Account wurde entsperrt.');

        return $this->redirectToRoute(route: 'configuration_internal_account_user_edit', parameters: ['accountUserId' => $accountUser->getId()]);
    }

    #[Security("is_granted('ROLE_ACCOUNT_ADMIN')")]
    #[Route('/{accountUserId<\d{1,10}>}/passwort-zuruecksetzen', name: 'create_reset_password', methods: ['GET', 'POST'])]
    public function createResetPassword(int $accountUserId, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $accountUser = $this->accountUserService->fetchAccountUserById(account: $account, withFromSubAccounts: false, id: $accountUserId);

        if ($accountUser === null) {
            throw new NotFoundHttpException();
        }

        if ($accountUser->getAccountUserType() !== AccountUserType::INTERNAL) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'edit', subject: $accountUser);

        $this->accountUserPasswordResetService->doPasswordResetForAccountUser(accountUser: $accountUser, createdByAccountUser: $user);

        $this->addFlash(type: 'dataSaved', message: 'Eine E-Mail mit einem Link zum Zurücksetzen des Kennworts wurde versandt.');

        return $this->redirectToRoute(route: 'configuration_internal_account_user_edit', parameters: ['accountUserId' => $accountUser->getId()]);
    }

    #[Security("is_granted('ROLE_ACCOUNT_ADMIN')")]
    #[Route('/nutzerrollen-pdf-uebersicht', name: 'user_roles_overview_document', methods: ['GET'])]
    public function userRolesOverviewDocument(): Response
    {
        $file = new \SplFileInfo(filename: $this->projectDir . '/assets/files/documentation/user_roles/user_roles.pdf');
        $fileName = 'nutzerrollen.pdf';

        return new BinaryFileResponse(
            file: $file->getRealPath(),
            headers: [
                'Content-Disposition' => HeaderUtils::makeDisposition(
                    disposition: HeaderUtils::DISPOSITION_ATTACHMENT,
                    filename: $fileName,
                    filenameFallback: mb_convert_encoding(string: $fileName, to_encoding: 'ascii', from_encoding: 'utf8')
                ),
            ]
        );
    }
}
