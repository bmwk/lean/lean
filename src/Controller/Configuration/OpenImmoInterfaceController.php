<?php

declare(strict_types=1);

namespace App\Controller\Configuration;

use App\Domain\OpenImmoImporter\AccountOpenImmoFtpUserService;
use App\Form\Type\Configuration\OpenImmoInterface\FtpUserDataType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Security("is_granted('ROLE_PROPERTY_PROVIDER')")]
#[Route('/konfiguration/open-immo-schnittstelle', name: 'configuration_open_immo_interface_')]
class OpenImmoInterfaceController extends AbstractController
{
    private const ACTIVE_NAV_GROUP = 'configuration';

    public function __construct(
        private readonly string $openImmoFtpHostname,
        private readonly AccountOpenImmoFtpUserService $accountOpenImmoFtpUserService
    ) {
    }

    #[Route('/ftp-user', name: 'ftp_user', methods: ['GET', 'POST'])]
    public function ftpUser(Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $accountOpenImmoFtpUser = $this->accountOpenImmoFtpUserService->fetchAccountOpenImmoFtpUserByAccount(account: $account);

        $ftpUserDataForm = $this->createForm(type: FtpUserDataType::class);

        if ($accountOpenImmoFtpUser !== null) {
            $ftpUserDataForm->get('ftpHostname')->setData($this->openImmoFtpHostname);
            $ftpUserDataForm->get('ftpUsername')->setData($accountOpenImmoFtpUser->getFtpUsername());

            $plainPassword = null;

            $ftpUserCreateMessages = $request->getSession()->getFlashBag()->get(type: 'ftpUserCreate');
            $ftpUserPasswordResetMessages = $request->getSession()->getFlashBag()->get(type: 'ftpUserPasswordReset');

            if (empty($ftpUserCreateMessages) === false) {
                $plainPassword = $ftpUserCreateMessages[0]['plainPassword'];
            } elseif (empty($ftpUserPasswordResetMessages) === false) {
                $plainPassword = $ftpUserPasswordResetMessages[0]['plainPassword'];
            }

            if ($plainPassword !== null) {
                $ftpUserDataForm->get('ftpPassword')->setData($plainPassword);
            }

            $ftpUserDataForm->add(child: 'passwordReset', type: SubmitType::class);
        } else {
            $ftpUserDataForm->add(child: 'create', type: SubmitType::class);
        }

        $ftpUserDataForm->handleRequest(request: $request);

        if ($ftpUserDataForm->isSubmitted() && $ftpUserDataForm->isValid()) {
            if ($ftpUserDataForm->getClickedButton()?->getName() === 'create') {
                if ($accountOpenImmoFtpUser !== null) {
                    throw new BadRequestHttpException();
                }

                $plainPassword = AccountOpenImmoFtpUserService::generateRandomString(length: 12);

                $accountOpenImmoFtpUser = $this->accountOpenImmoFtpUserService->createAndSaveAccountOpenImmoFtpUser(
                    account: $account,
                    creatorAccountUser: $user,
                    plainPassword: $plainPassword
                );

                $this->addFlash(type: 'ftpUserCreate', message: ['plainPassword' => $plainPassword]);
            }

            if ($ftpUserDataForm->getClickedButton()?->getName() === 'passwordReset') {
                if ($accountOpenImmoFtpUser === null) {
                    throw new BadRequestHttpException();
                }

                $plainPassword = AccountOpenImmoFtpUserService::generateRandomString(length: 12);

                $this->accountOpenImmoFtpUserService->changeFtpUserPassword(
                    accountOpenImmoFtpUser: $accountOpenImmoFtpUser,
                    plainPassword: $plainPassword
                );

                $this->addFlash(type: 'ftpUserPasswordReset', message: ['plainPassword' => $plainPassword]);
            }

            return $this->redirectToRoute(route: 'configuration_open_immo_interface_ftp_user');
        }

        return $this->render(view: 'configuration/open_immo_interface/ftp_user.html.twig', parameters: [
            'ftpUserDataForm'        => $ftpUserDataForm,
            'accountOpenImmoFtpUser' => $accountOpenImmoFtpUser,
            'pageTitle'              => 'Konfiguration - FTP-User',
            'activeNavGroup'         => self::ACTIVE_NAV_GROUP,
        ]);
    }
}
