<?php

declare(strict_types=1);

namespace App\Controller\Configuration;

use App\Domain\PropertyVacancyReporter\PropertyVacancyReporterConfigurationService;
use App\Form\Type\Configuration\PropertyVacancyReporter\GeneralType;
use App\Form\Type\Configuration\PropertyVacancyReporter\LayoutType;
use App\Form\Type\Configuration\PropertyVacancyReporter\MandatoryFieldsType;
use App\Form\Type\Configuration\PropertyVacancyReporter\PrivacyPolicyType;
use App\Form\Type\Configuration\PropertyVacancyReporter\IndustryClassificationType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Security("is_granted('ROLE_ACCOUNT_ADMIN') or is_granted('ROLE_CONFIGURATOR')")]
#[Route('/konfiguration/leerstandsmelder', name: 'configuration_property_vacancy_reporter_')]
class PropertyVacancyReporterController extends AbstractController
{
    private const ACTIVE_NAV_GROUP = 'configuration';

    public function __construct(
        private readonly PropertyVacancyReporterConfigurationService $propertyVacancyReporterConfigurationService,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    #[Route('/allgemein', name: 'general', methods: ['GET', 'POST'])]
    public function general(Request $request, UserInterface $user): Response
    {
        $propertyVacancyReporterConfiguration = $this->propertyVacancyReporterConfigurationService
            ->fetchPropertyVacancyReporterConfigurationByAccountOrCreateAndSaveIfNotExists(account: $user->getAccount());

        $generalForm = $this->createForm(type: GeneralType::class, data: $propertyVacancyReporterConfiguration);

        $generalForm->add(child: 'save', type: SubmitType::class);

        $generalForm->handleRequest(request: $request);

        if ($generalForm->isSubmitted() && $generalForm->isValid()) {
            $this->entityManager->flush();

            $this->addFlash(type: 'dataSaved', message: 'Die Allgemeinen Einstellungen wurden gespeichert.');

            return $this->redirectToRoute(route: 'configuration_property_vacancy_reporter_general');
        }

        $errors = [];

        $formErrors = $generalForm->getErrors(deep: true);

        if ($formErrors->count() > 0) {
            foreach ($formErrors as $error) {
                $errors[$error->getOrigin()->getName()] = $error->getMessage();
            }
        }

        if (empty($errors) === false) {
            $this->addFlash(type: 'dataDeleted', message: 'Daten konnten nicht gespeichert werden');
        }

        return $this->render(view: 'configuration/property_vacancy_reporter/general.html.twig', parameters: [
            'generalForm'    => $generalForm,
            'submitResponse' => $request->getSession()->getFlashBag()->get(type: 'submitResponse'),
            'pageTitle'      => 'Konfiguration - Leerstandsmelder',
            'activeNavGroup' => self::ACTIVE_NAV_GROUP,
        ]);
    }

    #[Route('/nutzungsarten', name: 'property_usage', methods: ['GET', 'POST'])]
    public function propertyUsage(Request $request, UserInterface $user): Response
    {
        $propertyVacancyReporterConfiguration = $this->propertyVacancyReporterConfigurationService
            ->fetchPropertyVacancyReporterConfigurationByAccountOrCreateAndSaveIfNotExists(account: $user->getAccount());

        $propertyUsageForm = $this->createForm(type: IndustryClassificationType::class, data: $propertyVacancyReporterConfiguration);

        $propertyUsageForm->add(child: 'save', type: SubmitType::class);

        $propertyUsageForm->handleRequest(request: $request);

        if ($propertyUsageForm->isSubmitted() && $propertyUsageForm->isValid()) {
            $this->entityManager->flush();

            $this->addFlash(type: 'dataSaved', message: 'Die Einstellungen wurden gespeichert.');

            return $this->redirectToRoute(route: 'configuration_property_vacancy_reporter_property_usage');
        }

        return $this->render(view: 'configuration/property_vacancy_reporter/property_usage.html.twig', parameters: [
            'propertyUsageForm' => $propertyUsageForm,
            'submitResponse'    => $request->getSession()->getFlashBag()->get(type: 'submitResponse'),
            'pageTitle'         => 'Konfiguration - Leerstandsmelder',
            'activeNavGroup'    => self::ACTIVE_NAV_GROUP,
        ]);
    }

    #[Route('/pflichtfelder', name: 'mandatory_fields', methods: ['GET', 'POST'])]
    public function mandatoryFields(Request $request, UserInterface $user): Response
    {
        $propertyVacancyReporterConfiguration = $this->propertyVacancyReporterConfigurationService
            ->fetchPropertyVacancyReporterConfigurationByAccountOrCreateAndSaveIfNotExists(account: $user->getAccount());

        $mandatoryFieldsForm = $this->createForm(type: MandatoryFieldsType::class, data: $propertyVacancyReporterConfiguration);

        $mandatoryFieldsForm->add(child: 'save', type: SubmitType::class);

        $mandatoryFieldsForm->handleRequest(request: $request);

        if ($mandatoryFieldsForm->isSubmitted() && $mandatoryFieldsForm->isValid()) {
            $this->entityManager->flush();

            $this->addFlash(type: 'dataSaved', message: 'Die Einstellungen wurden gespeichert.');

            return $this->redirectToRoute(route: 'configuration_property_vacancy_reporter_mandatory_fields');
        }

        return $this->render(view: 'configuration/property_vacancy_reporter/mandatory_fields.html.twig', parameters: [
            'mandatoryFieldsForm' => $mandatoryFieldsForm,
            'submitResponse'      => $request->getSession()->getFlashBag()->get(type: 'submitResponse'),
            'pageTitle'           => 'Konfiguration - Leerstandsmelder',
            'activeNavGroup'      => self::ACTIVE_NAV_GROUP,
        ]);
    }

    #[Route('/datenschutz', name: 'privacy_policy', methods: ['GET', 'POST'])]
    public function privacyPolicy(Request $request, UserInterface $user): Response
    {
        $propertyVacancyReporterConfiguration = $this->propertyVacancyReporterConfigurationService
            ->fetchPropertyVacancyReporterConfigurationByAccountOrCreateAndSaveIfNotExists(account: $user->getAccount());

        $privacyPolicyForm = $this->createForm(type: PrivacyPolicyType::class, data: $propertyVacancyReporterConfiguration);

        $privacyPolicyForm->add(child: 'save', type: SubmitType::class);

        $privacyPolicyForm->handleRequest(request: $request);

        if ($privacyPolicyForm->isSubmitted() && $privacyPolicyForm->isValid()) {
            $this->entityManager->flush();

            $this->addFlash(type: 'dataSaved', message: 'Die Einstellungen wurden gespeichert.');

            return $this->redirectToRoute(route: 'configuration_property_vacancy_reporter_privacy_policy');
        }

        $errors = [];

        $formErrors = $privacyPolicyForm->getErrors(deep: true);

        if ($formErrors->count() > 0) {
            foreach ($formErrors as $error) {
                $errors[$error->getOrigin()->getName()] = $error->getMessage();
            }
        }

        if (empty($errors) === false) {
            $this->addFlash(type: 'dataDeleted', message: 'Die Einstellungen konnten nicht gespeichert werden.');
        }

        return $this->render(view: 'configuration/property_vacancy_reporter/privacy_policy.html.twig', parameters: [
            'privacyPolicyForm' => $privacyPolicyForm,
            'submitResponse'    => $request->getSession()->getFlashBag()->get(type: 'submitResponse'),
            'pageTitle'         => 'Konfiguration - Leerstandsmelder',
            'activeNavGroup'    => self::ACTIVE_NAV_GROUP,
        ]);
    }

    #[Route('/layout', name: 'layout', methods: ['GET', 'POST'])]
    public function layout(Request $request, UserInterface $user): Response
    {
        $propertyVacancyReporterConfiguration = $this->propertyVacancyReporterConfigurationService
            ->fetchPropertyVacancyReporterConfigurationByAccountOrCreateAndSaveIfNotExists(account: $user->getAccount());

        $layoutForm = $this->createForm(type: LayoutType ::class, data: $propertyVacancyReporterConfiguration);

        $layoutForm->add(child: 'save', type: SubmitType::class);

        $layoutForm->handleRequest(request: $request);

        if ($layoutForm->isSubmitted() && $layoutForm->isValid()) {
            $firstFontColor = $this->propertyVacancyReporterConfigurationService->calculateIdealHexFontColor(
                hexBackgroundColor: $layoutForm->getData()->getFirstColor()
            );

            $secondaryFontColor = $this->propertyVacancyReporterConfigurationService->calculateIdealHexFontColor(
                hexBackgroundColor: $layoutForm->getData()->getSecondaryColor()
            );

            $layoutForm->getData()->setFirstFontColor($firstFontColor);
            $layoutForm->getData()->setSecondaryFontColor($secondaryFontColor);

            $this->entityManager->flush();

            $this->addFlash(type: 'dataSaved', message: 'Die Einstellungen wurden gespeichert.');

            return $this->redirectToRoute(route: 'configuration_property_vacancy_reporter_layout');
        }

        return $this->render(view: 'configuration/property_vacancy_reporter/layout.html.twig', parameters: [
            'layoutForm'     => $layoutForm,
            'submitResponse' => $request->getSession()->getFlashBag()->get(type: 'submitResponse'),
            'pageTitle'      => 'Konfiguration - Leerstandsmelder',
            'activeNavGroup' => self::ACTIVE_NAV_GROUP,
        ]);
    }

    #[Route('/vorschau', name: 'preview', methods: ['GET'])]
    public function preview(UserInterface $user): Response
    {
        $account = $user->getAccount();

        $propertyVacancyReporterConfiguration = $this->propertyVacancyReporterConfigurationService
            ->fetchPropertyVacancyReporterConfigurationByAccountOrCreateAndSaveIfNotExists(account: $account);

        return $this->render(view: 'configuration/property_vacancy_reporter/preview.html.twig', parameters: [
            'accountKey'     => $propertyVacancyReporterConfiguration->getAccountKey()->toRfc4122(),
            'pageTitle'      => 'Konfiguration - Leerstandsmelder',
            'activeNavGroup' => self::ACTIVE_NAV_GROUP,
        ]);
    }

    #[Route('/einbettungscode', name: 'embed_code', methods: ['GET'])]
    public function embedCode(Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $propertyVacancyReporterConfiguration = $this->propertyVacancyReporterConfigurationService
            ->fetchPropertyVacancyReporterConfigurationByAccountOrCreateAndSaveIfNotExists(account: $account);

        return $this->render(view: 'configuration/property_vacancy_reporter/embed_code.html.twig', parameters: [
            'accountKey'     => $propertyVacancyReporterConfiguration->getAccountKey()->toRfc4122(),
            'hostname'       => 'https://' . $request->getHttpHost(),
            'pageTitle'      => 'Konfiguration - Leerstandsmelder',
            'activeNavGroup' => self::ACTIVE_NAV_GROUP,
        ]);
    }

    #[Route('/faq', name: 'faq', methods: ['GET'])]
    public function faq(): Response
    {
        return $this->render(view: 'configuration/property_vacancy_reporter/faq.html.twig', parameters: [
            'pageTitle'      => 'Konfiguration - Leerstandsmelder',
            'activeNavGroup' => self::ACTIVE_NAV_GROUP,
        ]);
    }
}
