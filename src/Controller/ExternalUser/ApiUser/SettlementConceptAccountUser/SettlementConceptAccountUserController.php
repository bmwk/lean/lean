<?php

declare(strict_types=1);

namespace App\Controller\ExternalUser\ApiUser\SettlementConceptAccountUser;

use App\Controller\AbstractLeAnController;
use App\Domain\Entity\DeletionType;
use App\Domain\Entity\PaginationParameter;
use App\Domain\Entity\SettlementConcept\SettlementConceptAccountUser;
use App\Domain\Entity\SettlementConcept\SettlementConceptAccountUserFilter;
use App\Domain\Entity\SettlementConcept\SettlementConceptAccountUserSearch;
use App\Domain\Entity\SettlementConcept\SettlementConceptAuthenticationTokenGenerationRequest;
use App\Domain\Entity\SortingOption\SortingOption;
use App\Domain\SessionStoredUrlParameter\SessionStoredUrlParameterService;
use App\Domain\SettlementConcept\SettlementConceptAccountUserDeletionService;
use App\Domain\SettlementConcept\SettlementConceptAccountUserService;
use App\Form\Type\ExternalUser\SettlementConceptAccountUser\SettlementConceptAccountUserDeleteType;
use App\Form\Type\ExternalUser\SettlementConceptAccountUser\SettlementConceptAccountUserFilterType;
use App\Form\Type\ExternalUser\SettlementConceptAccountUser\SettlementConceptAccountUserResendApiTokenType;
use App\Form\Type\ExternalUser\SettlementConceptAccountUser\SettlementConceptAccountUserSearchType;
use App\Form\Type\ExternalUser\SettlementConceptAccountUser\SettlementConceptAccountUserType;
use App\SessionStorage\SessionStorageService;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Uid\Uuid;

#[Security("is_granted('ROLE_ACCOUNT_ADMIN') or is_granted('ROLE_VIEWER')")]
#[Route('/externe-nutzer/api-nutzer/ansiedlungskonzepte', name: 'external_user_api_user_settlement_concept_account_user_')]
class SettlementConceptAccountUserController extends AbstractLeAnController
{
    private const ACTIVE_NAV_GROUP = 'external_user';
    private const ACTIVE_NAV_ITEM = self::ACTIVE_NAV_GROUP . '_api_user_settlement_concept_account_user';

    private const URL_PARAMETER_OBJECT_CLASSES = [
        PaginationParameter::class,
        SortingOption::class,
    ];

    private const OVERVIEW_ROUTE_NAME = 'external_user_api_user_settlement_concept_account_user_overview';

    private const OVERVIEW_SESSION_KEY_NAME = 'settlementConceptAccountUserOverview';

    public function __construct(
        SessionStorageService $sessionStorageService,
        SessionStoredUrlParameterService $sessionStoredUrlParameterService,
        private readonly SettlementConceptAccountUserService $settlementConceptAccountUserService,
        private readonly SettlementConceptAccountUserDeletionService $settlementConceptAccountUserDeletionService,
        private readonly EntityManagerInterface $entityManager
    ) {
        parent::__construct(
            sessionStorageService: $sessionStorageService,
            sessionStoredUrlParameterService: $sessionStoredUrlParameterService
        );
    }

    #[Route('/uebersicht', name: 'overview', methods: ['GET', 'POST'])]
    public function overview(Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $paginationParameter = PaginationParameter::createFromRequest(request: $request);

        $this->storeObjectInSession(object: $paginationParameter, sessionKeyName: self::OVERVIEW_SESSION_KEY_NAME);

        $sortingOption = null;

        if ($request->query->get(key: 'sortieren') !== null) {
            $sortingOption = SortingOption::createFromRequest(request: $request);

            $this->storeObjectInSession(object: $sortingOption, sessionKeyName: self::OVERVIEW_SESSION_KEY_NAME);
        }

        $settlementConceptAccountUserFilterForm = $this->createForm(type: SettlementConceptAccountUserFilterType::class, options: [
            'dataClassName'  => SettlementConceptAccountUserFilter::class,
            'sessionKeyName' => self::OVERVIEW_SESSION_KEY_NAME,
        ]);

        $settlementConceptAccountUserFilterForm->add(child: 'apply', type: SubmitType::class);

        $settlementConceptAccountUserFilterForm->handleRequest(request: $request);

        $settlementConceptAccountUserSearchForm = $this->createForm(type: SettlementConceptAccountUserSearchType::class, options: [
            'dataClassName'  => SettlementConceptAccountUserSearch::class,
            'sessionKeyName' => self::OVERVIEW_SESSION_KEY_NAME,
        ]);

        $settlementConceptAccountUserSearchForm->add(child: 'search', type: SubmitType::class);

        $settlementConceptAccountUserSearchForm->handleRequest(request: $request);

        $settlementConceptAccountUsers = $this->settlementConceptAccountUserService->fetchSettlementConceptAccountUsersPaginatedByAccount(
            account: $account,
            firstResult: $paginationParameter->getFirstResult(),
            maxResults: $paginationParameter->getLimit(),
            settlementConceptAccountUserFilter: $settlementConceptAccountUserFilterForm->getData(),
            settlementConceptAccountUserSearch: $settlementConceptAccountUserSearchForm->getData(),
            sortingOption: $sortingOption
        );

        return $this->render(view: 'external_user/api_user/settlement_concept_account_user/overview.html.twig', parameters: [
            'settlementConceptAccountUsers'          => $settlementConceptAccountUsers,
            'settlementConceptAccountUserFilterForm' => $settlementConceptAccountUserFilterForm,
            'settlementConceptAccountUserSearchForm' => $settlementConceptAccountUserSearchForm,
            'activeNavGroup'                         => self::ACTIVE_NAV_GROUP,
            'activeNavItem'                          => self::ACTIVE_NAV_ITEM,
            'pageTitle'                              => '',
        ]);
    }

    #[Security("is_granted('ROLE_ACCOUNT_ADMIN')")]
    #[Route('/anlegen', name: 'create', methods: ['GET', 'POST'])]
    public function create(Request $request, UserInterface $user): Response
    {
        $settlementConceptAccountUser = new SettlementConceptAccountUser();

        $settlementConceptAccountUserForm = $this->createForm(
            type: SettlementConceptAccountUserType::class,
            data: $settlementConceptAccountUser,
            options: ['canEdit' => true]
        );

        $settlementConceptAccountUserForm->add(child: 'save', type: SubmitType::class);

        $settlementConceptAccountUserForm->handleRequest(request: $request);

        if ($settlementConceptAccountUserForm->isSubmitted() && $settlementConceptAccountUserForm->isValid()) {
            $this->entityManager->beginTransaction();

            $settlementConceptAccountUser
                ->setAccount($user->getAccount())
                ->setDeleted(false)
                ->setEnabled(true)
                ->setIdentifier(Uuid::v6())
                ->setCreatedByAccountUser($user)
                ->setRoles(['ROLE_SETTLEMENT_CONCEPT_API_USER']);

            $this->entityManager->persist(entity: $settlementConceptAccountUser);

            $this->entityManager->flush();

            $settlementConceptAuthenticationTokenGenerationRequest = SettlementConceptAuthenticationTokenGenerationRequest::createFromSettlementConceptAccountUser(
                settlementConceptAccountUser:  $settlementConceptAccountUser
            );

            $validTill = new \DateTimeImmutable();

            $validTill = $validTill->add(new \DateInterval(duration: 'P7D'));

            $settlementConceptAuthenticationTokenGenerationRequest->setValidTill($validTill);

            $settlementConceptAccountUser->setIdentifier($settlementConceptAuthenticationTokenGenerationRequest->getUuid());

            $this->entityManager->persist($settlementConceptAuthenticationTokenGenerationRequest);

            $this->entityManager->flush();

            $this->entityManager->commit();

            $this->settlementConceptAccountUserService->sendEmailWithLinkToAuthenticationTokenGeneration(
                settlementConceptAuthenticationTokenGenerationRequest: $settlementConceptAuthenticationTokenGenerationRequest
            );

            $this->addFlash(type: 'dataSaved', message: $settlementConceptAccountUser->getFullName() . ' wurde als API-Nutzender angelegt und hat eine E-Mail zum Abruf des API-Token erhalten.');

            $urlParameters = $this->fetchMergedUrlParametersFromSession(
                urlParameterObjectClasses: self::URL_PARAMETER_OBJECT_CLASSES,
                sessionKeyName: self::OVERVIEW_SESSION_KEY_NAME
            );

            return $this->redirectToRoute(route: self::OVERVIEW_ROUTE_NAME, parameters: $urlParameters);
        }

        return $this->render(view: 'external_user/api_user/settlement_concept_account_user/create.html.twig', parameters: [
            'settlementConceptAccountUserForm' => $settlementConceptAccountUserForm,
            'activeNavGroup'                   => self::ACTIVE_NAV_GROUP,
            'activeNavItem'                    => self::ACTIVE_NAV_ITEM,
            'pageTitle'                        => 'API-Nutzenden anlegen',
        ]);
    }

    #[Route('/{settlementConceptAccountUserId<\d{1,10}>}', name: 'edit', methods: ['GET', 'POST'])]
    public function edit(int $settlementConceptAccountUserId, Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $settlementConceptAccountUser = $this->settlementConceptAccountUserService->fetchSettlementConceptAccountUserById(
            id: $settlementConceptAccountUserId,
            account: $account
        );

        if ($settlementConceptAccountUser === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'view', subject: $settlementConceptAccountUser);

        $settlementConceptAccountUserForm = $this->createForm(
            type: SettlementConceptAccountUserType::class,
            data: $settlementConceptAccountUser,
            options: [
                'canEdit' => $this->isGranted(attribute: 'edit', subject: $settlementConceptAccountUser),
            ]
        );

        $settlementConceptAccountUserForm->add(child: 'save', type: SubmitType::class);

        $settlementConceptAccountUserForm->handleRequest(request: $request);

        if ($settlementConceptAccountUserForm->isSubmitted() && $settlementConceptAccountUserForm->isValid()) {
            $this->denyAccessUnlessGranted(attribute: 'edit', subject: $settlementConceptAccountUser);

            $this->entityManager->flush();

            $this->addFlash(type: 'dataSaved', message: 'Die Änderungen wurden gespeichert.');

            $urlParameters = $this->fetchMergedUrlParametersFromSession(
                urlParameterObjectClasses: self::URL_PARAMETER_OBJECT_CLASSES,
                sessionKeyName: self::OVERVIEW_SESSION_KEY_NAME
            );

            return $this->redirectToRoute(route: self::OVERVIEW_ROUTE_NAME, parameters: $urlParameters);
        }

        return $this->render(view: 'external_user/api_user/settlement_concept_account_user/edit.html.twig', parameters: [
            'settlementConceptAccountUserForm' => $settlementConceptAccountUserForm,
            'settlementConceptAccountUser'     => $settlementConceptAccountUser,
            'activeNavGroup'                   => self::ACTIVE_NAV_GROUP,
            'activeNavItem'                    => self::ACTIVE_NAV_ITEM,
            'pageTitle'                        => 'API-Nutzenden bearbeiten',
        ]);
    }

    #[Security("is_granted('ROLE_ACCOUNT_ADMIN')")]
    #[Route('/{settlementConceptAccountUserId<\d{1,10}>}/loeschen', name: 'delete', methods: ['GET', 'POST'])]
    public function delete(int $settlementConceptAccountUserId, Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $settlementConceptAccountUser = $this->settlementConceptAccountUserService->fetchSettlementConceptAccountUserById(
            id: $settlementConceptAccountUserId,
            account: $account
        );

        if ($settlementConceptAccountUser === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'delete', subject: $settlementConceptAccountUser);

        $settlementConceptAccountUserDeleteForm = $this->createForm(type: SettlementConceptAccountUserDeleteType::class);

        $settlementConceptAccountUserDeleteForm->add(child: 'delete', type: SubmitType::class);

        $settlementConceptAccountUserDeleteForm->handleRequest(request: $request);

        if ($settlementConceptAccountUserDeleteForm->isSubmitted() && $settlementConceptAccountUserDeleteForm->isValid()) {
            if ($settlementConceptAccountUserDeleteForm->get('verificationCode')->getData() === 'api_nutzer_' . $settlementConceptAccountUser->getId()) {
                $this->settlementConceptAccountUserDeletionService->deleteSettlementConceptAccountUser(
                    settlementConceptAccountUser: $settlementConceptAccountUser,
                    deletionType: DeletionType::SOFT,
                    deletedByAccountUser: $user
                );

                $this->addFlash(type: 'dataDeleted', message: 'Der Account wurde gelöscht.');

                $urlParameters = $this->fetchMergedUrlParametersFromSession(
                    urlParameterObjectClasses: self::URL_PARAMETER_OBJECT_CLASSES,
                    sessionKeyName: self::OVERVIEW_SESSION_KEY_NAME
                );

                return $this->redirectToRoute(route: self::OVERVIEW_ROUTE_NAME, parameters: $urlParameters);
            } else {
                $this->addFlash(type: 'dataError', message: 'Der Verifizierungscode ist falsch.');
            }
        }

        $settlementConceptAccountUserForm = $this->createForm(
            type: SettlementConceptAccountUserType::class,
            data: $settlementConceptAccountUser,
            options: ['canEdit' => false]
        );

        return $this->render(view: 'external_user/api_user/settlement_concept_account_user/delete.html.twig', parameters: [
            'settlementConceptAccountUserDeleteForm' => $settlementConceptAccountUserDeleteForm,
            'settlementConceptAccountUserForm'       => $settlementConceptAccountUserForm,
            'settlementConceptAccountUser'           => $settlementConceptAccountUser,
            'activeNavGroup'                         => self::ACTIVE_NAV_GROUP,
            'activeNavItem'                          => self::ACTIVE_NAV_ITEM,
            'pageTitle'                              => 'API-Nutzenden löschen',
        ]);
    }

    #[Security("is_granted('ROLE_ACCOUNT_ADMIN')")]
    #[Route('/{settlementConceptAccountUserId<\d{1,10}>}/sperren', name: 'block', methods: ['GET'])]
    public function block(int $settlementConceptAccountUserId, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $settlementConceptAccountUser = $this->settlementConceptAccountUserService->fetchSettlementConceptAccountUserById(
            id: $settlementConceptAccountUserId,
            account: $account
        );

        if ($settlementConceptAccountUser === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'edit', subject: $settlementConceptAccountUser);

        $settlementConceptAccountUser->setEnabled(false);

        $this->entityManager->flush();

        $this->addFlash(type: 'dataDeleted', message: 'Der Account wurde gesperrt.');

        return $this->redirectToRoute(route: 'external_user_api_user_settlement_concept_account_user_edit', parameters: ['settlementConceptAccountUserId' => $settlementConceptAccountUserId]);
    }

    #[Security("is_granted('ROLE_ACCOUNT_ADMIN')")]
    #[Route('/{settlementConceptAccountUserId<\d{1,10}>}/entsperren', name: 'unblock', methods: ['GET'])]
    public function unblock(int $settlementConceptAccountUserId, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $settlementConceptAccountUser = $this->settlementConceptAccountUserService->fetchSettlementConceptAccountUserById(
            id: $settlementConceptAccountUserId,
            account: $account
        );

        if ($settlementConceptAccountUser === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'edit', subject: $settlementConceptAccountUser);

        $settlementConceptAccountUser->setEnabled(true);

        $this->entityManager->flush();

        $this->addFlash(type: 'dataSaved', message: 'Der Account wurde entsperrt.');

        return $this->redirectToRoute(route: 'external_user_api_user_settlement_concept_account_user_edit', parameters: ['settlementConceptAccountUserId' => $settlementConceptAccountUserId]);
    }

    #[Security("is_granted('ROLE_ACCOUNT_ADMIN')")]
    #[Route('/{settlementConceptAccountUserId<\d{1,10}>}/neuen-api-token-generieren', name: 'regenerate_api_token', methods: ['GET', 'POST'])]
    public function regenerateApiToken(int $settlementConceptAccountUserId, Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $settlementConceptAccountUser = $this->settlementConceptAccountUserService->fetchSettlementConceptAccountUserById(
            id: $settlementConceptAccountUserId,
            account: $account
        );

        if ($settlementConceptAccountUser === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'edit', subject: $settlementConceptAccountUser);

        $settlementConceptAccountUserResendApiTokenForm = $this->createForm(type: SettlementConceptAccountUserResendApiTokenType::class);

        $settlementConceptAccountUserResendApiTokenForm->add(child: 'save', type: SubmitType::class);

        $settlementConceptAccountUserResendApiTokenForm->handleRequest(request: $request);

        if ($settlementConceptAccountUserResendApiTokenForm->isSubmitted() && $settlementConceptAccountUserResendApiTokenForm->isValid()) {
           $settlementConceptAuthenticationTokenGenerationRequest = $this->settlementConceptAccountUserService->fetchSettlementConceptAuthenticationTokenGenerationRequestBySettlementConceptAccountUser(
               settlementConceptAccountUser: $settlementConceptAccountUser
           );

            $this->entityManager->beginTransaction();

            if ($settlementConceptAuthenticationTokenGenerationRequest !== null) {
                $this->entityManager->remove(entity: $settlementConceptAuthenticationTokenGenerationRequest);
                $this->entityManager->flush();
            }

            $settlementConceptAuthenticationTokenGenerationRequest = SettlementConceptAuthenticationTokenGenerationRequest::createFromSettlementConceptAccountUser(
                settlementConceptAccountUser:  $settlementConceptAccountUser
            );

            $validTill = new \DateTimeImmutable();

            $validTill = $validTill->add(new \DateInterval(duration: 'P7D'));

            $settlementConceptAuthenticationTokenGenerationRequest->setValidTill($validTill);

            $settlementConceptAccountUser->setIdentifier($settlementConceptAuthenticationTokenGenerationRequest->getUuid());

            $this->entityManager->persist($settlementConceptAuthenticationTokenGenerationRequest);

            $this->entityManager->flush();

            $this->entityManager->commit();

            $this->settlementConceptAccountUserService->sendEmailWithLinkToAuthenticationTokenGeneration(
                settlementConceptAuthenticationTokenGenerationRequest: $settlementConceptAuthenticationTokenGenerationRequest
            );

            $this->addFlash(type: 'dataSaved', message: 'Eine E-Mail zum Abruf eines neuen API-Token wurde an den Nutzenden versendet.');

            return $this->redirectToRoute(route: 'external_user_api_user_settlement_concept_account_user_edit', parameters: ['settlementConceptAccountUserId' => $settlementConceptAccountUserId]);
        }

        return $this->render(view: 'external_user/api_user/settlement_concept_account_user/resend_api_token.html.twig', parameters: [
            'settlementConceptAccountUserResendApiTokenForm' => $settlementConceptAccountUserResendApiTokenForm,
            'settlementConceptAccountUser'                   => $settlementConceptAccountUser,
            'activeNavGroup'                                 => self::ACTIVE_NAV_GROUP,
            'activeNavItem'                                  => self::ACTIVE_NAV_ITEM,
            'pageTitle'                                      => 'API-Nutzenden, Token erneut senden',
        ]);
    }
}
