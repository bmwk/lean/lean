<?php

declare(strict_types=1);

namespace App\Controller\ExternalUser\UserInvitation;

use App\Controller\AbstractLeAnController;
use App\Domain\Entity\PaginationParameter;
use App\Domain\Entity\Person\PersonType;
use App\Domain\Entity\SortingOption\SortingOption;
use App\Domain\Entity\UserRegistration\UserInvitation;
use App\Domain\Entity\UserRegistration\UserInvitationFilter;
use App\Domain\Entity\UserRegistration\UserInvitationSearch;
use App\Domain\Entity\UserRegistration\UserRegistrationType;
use App\Domain\SessionStoredUrlParameter\SessionStoredUrlParameterService;
use App\Domain\UserRegistration\UserInvitationDeletionService;
use App\Domain\UserRegistration\UserInvitationEmailService;
use App\Domain\UserRegistration\UserInvitationSecurityService;
use App\Domain\UserRegistration\UserInvitationService;
use App\Domain\UserRegistration\UserRegistrationDeletionService;
use App\Domain\UserRegistration\UserRegistrationEmailService;
use App\Form\Type\ExternalUser\UserInvitation\CompanyUserInvitationType;
use App\Form\Type\ExternalUser\UserInvitation\NaturalPersonUserInvitationType;
use App\Form\Type\ExternalUser\UserInvitation\UserInvitationDeleteType;
use App\Form\Type\ExternalUser\UserInvitation\UserInvitationFilterType;
use App\Form\Type\ExternalUser\UserInvitation\UserInvitationSearchType;
use App\SessionStorage\SessionStorageService;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Uid\Uuid;

#[Security("is_granted('ROLE_USER') or is_granted('ROLE_VIEWER')")]
#[Route('/externe-nutzer/einladungen', name: 'external_user_user_invitation_')]
class UserInvitationController extends AbstractLeAnController
{
    private const ACTIVE_NAV_GROUP = 'external_user';
    private const ACTIVE_NAV_ITEM = self::ACTIVE_NAV_GROUP . '_user_invitation';

    private const OVERVIEW_ROUTE_NAME = 'external_user_user_invitation_overview';

    private const URL_PARAMETER_OBJECT_CLASSES = [
        PaginationParameter::class,
        SortingOption::class,
    ];

    private static function fetchUserRegistrationTypeByUserRegistrationTypeName(string $userRegistrationTypeName): UserRegistrationType
    {
        return match ($userRegistrationTypeName) {
            'anbietende' => UserRegistrationType::PROVIDER,
            'suchende' => UserRegistrationType::SEEKER,
            default => throw new \RuntimeException(message: 'invalid userRegistrationTypeName')
        };
    }

    private static function fetchUserRegistrationTypeNameByUserRegistrationType(UserRegistrationType $userRegistrationType): string
    {
        return match ($userRegistrationType) {
            UserRegistrationType::PROVIDER => 'anbietende',
            UserRegistrationType::SEEKER => 'suchende',
            default => throw new \RuntimeException(message: 'invalid UserRegistrationType')
        };
    }

    private static function fetchOverviewSessionKeyNameByUserRegistrationType(UserRegistrationType $userRegistrationType): string
    {
        return match ($userRegistrationType) {
            UserRegistrationType::PROVIDER => 'propertyProviderUserInvitationOverview',
            UserRegistrationType::SEEKER => 'propertySeekerUserInvitationOverview',
            default => throw new \RuntimeException(message: 'invalid UserRegistrationType')
        };
    }

    public function __construct(
        SessionStorageService $sessionStorageService,
        SessionStoredUrlParameterService $sessionStoredUrlParameterService,
        private readonly UserInvitationService $userInvitationService,
        private readonly UserInvitationSecurityService $userInvitationSecurityService,
        private readonly UserInvitationEmailService $userInvitationEmailService,
        private readonly UserRegistrationEmailService $userRegistrationEmailService,
        private readonly UserInvitationDeletionService $userInvitationDeletionService,
        private readonly UserRegistrationDeletionService $userRegistrationDeletionService,
        private readonly EntityManagerInterface $entityManager
    ) {
        parent::__construct(
            sessionStorageService: $sessionStorageService,
            sessionStoredUrlParameterService: $sessionStoredUrlParameterService
        );
    }

    #[Route('/{userRegistrationTypeName<suchende|anbietende>}/uebersicht', name: 'overview', methods: ['GET', 'POST'])]
    public function overview(string $userRegistrationTypeName, Request $request,  UserInterface $user): Response
    {
        $grantedUserRegistrationTypes = $this->userInvitationSecurityService->getGrantedUserRegistrationTypes();

        if ($grantedUserRegistrationTypes === null) {
            throw new AccessDeniedHttpException();
        }

        $userRegistrationType = self::fetchUserRegistrationTypeByUserRegistrationTypeName(userRegistrationTypeName: $userRegistrationTypeName);
        $sessionKeyName = self::fetchOverviewSessionKeyNameByUserRegistrationType(userRegistrationType: $userRegistrationType);

        if ($userRegistrationType === UserRegistrationType::PROVIDER) {
            $pageTitle = 'Einladungen | Anbietende';
            $view = 'external_user/user_invitation/property_provider_overview.html.twig';
        } elseif ($userRegistrationType === UserRegistrationType::SEEKER) {
            $pageTitle = 'Einladungen | Suchende';
            $view = 'external_user/user_invitation/property_seeker_overview.html.twig';
        } else {
            throw new NotFoundHttpException();
        }

        if (in_array(needle: $userRegistrationType, haystack: $grantedUserRegistrationTypes) === false) {
            throw new AccessDeniedHttpException();
        }

        $paginationParameter = PaginationParameter::createFromRequest(request: $request);

        $this->storeObjectInSession(object: $paginationParameter, sessionKeyName: $sessionKeyName);

        $sortingOption = null;

        if ($request->query->get(key: 'sortieren') !== null) {
            $sortingOption = SortingOption::createFromRequest(request: $request);

            $this->storeObjectInSession(object: $sortingOption, sessionKeyName: $sessionKeyName);
        }

        $userInvitationFilterForm = $this->createForm(type: UserInvitationFilterType::class, options: [
            'dataClassName'  => UserInvitationFilter::class,
            'sessionKeyName' => $sessionKeyName,
        ]);

        $userInvitationFilterForm->add(child: 'apply', type: SubmitType::class);

        $userInvitationFilterForm->handleRequest(request: $request);

        $userInvitationSearchForm = $this->createForm(type: UserInvitationSearchType::class, options: [
            'dataClassName'  => UserInvitationSearch::class,
            'sessionKeyName' => $sessionKeyName,
        ]);

        $userInvitationSearchForm->add(child: 'search', type: SubmitType::class);

        $userInvitationSearchForm->handleRequest(request: $request);

        $userInvitations = $this->userInvitationService->fetchUserInvitationsPaginatedByAccount(
            account: $user->getAccount(),
            firstResult: $paginationParameter->getFirstResult(),
            maxResults: $paginationParameter->getLimit(),
            userRegistrationTypes: [$userRegistrationType],
            userInvitationFilter: $userInvitationFilterForm->getData(),
            userInvitationSearch: $userInvitationSearchForm->getData(),
            sortingOption: $sortingOption
        );

        return $this->render(view: $view, parameters: [
            'userInvitations'          => $userInvitations,
            'userRegistrationType'     => $userRegistrationType,
            'userInvitationFilterForm' => $userInvitationFilterForm,
            'userInvitationSearchForm' => $userInvitationSearchForm,
            'pageTitle'                => $pageTitle,
            'activeNavGroup'           => self::ACTIVE_NAV_GROUP,
            'activeNavItem'            => self::ACTIVE_NAV_ITEM,
        ]);
    }

    #[Security("is_granted('ROLE_USER')")]
    #[Route('/{userRegistrationTypeName<suchende|anbietende>}/anlegen', name: 'create', methods: ['GET', 'POST'])]
    public function create(string $userRegistrationTypeName, Request $request, UserInterface $user): Response
    {
        $grantedUserRegistrationTypes = $this->userInvitationSecurityService->getGrantedUserRegistrationTypes();

        if ($grantedUserRegistrationTypes === null) {
            throw new AccessDeniedHttpException();
        }

        $userRegistrationType = self::fetchUserRegistrationTypeByUserRegistrationTypeName(userRegistrationTypeName: $userRegistrationTypeName);
        $overviewSessionKeyName = self::fetchOverviewSessionKeyNameByUserRegistrationType(userRegistrationType: $userRegistrationType);

        $userInvitation = new UserInvitation();

        $userInvitation
            ->setUserRegistrationType($userRegistrationType)
            ->setAccount($user->getAccount())
            ->setCreatedByAccountUser($user)
            ->setUuid(Uuid::v6());

        $formOptions = [
            'userRegistrationTypes' => $grantedUserRegistrationTypes,
            'canEdit'               => true,
        ];

        $naturalPersonUserInvitationForm = $this->createForm(
            type: NaturalPersonUserInvitationType::class,
            data: $userInvitation,
            options: $formOptions
        );

        $companyUserInvitationForm = $this->createForm(
            type: CompanyUserInvitationType::class,
            data: $userInvitation,
            options: $formOptions
        );

        $companyUserInvitationForm->add(child: 'save', type: SubmitType::class);
        $naturalPersonUserInvitationForm->add(child: 'save', type: SubmitType::class);

        $companyUserInvitationForm->handleRequest(request: $request);
        $naturalPersonUserInvitationForm->handleRequest(request: $request);

        if (
            ($companyUserInvitationForm->isSubmitted() && $companyUserInvitationForm->isValid())
            || ($naturalPersonUserInvitationForm->isSubmitted() && $naturalPersonUserInvitationForm->isValid())
        ) {
            if ($companyUserInvitationForm->isSubmitted()) {
                $userInvitation->setPersonType(PersonType::COMPANY);
            }

            if ($naturalPersonUserInvitationForm->isSubmitted()) {
                $userInvitation->setPersonType(PersonType::NATURAL_PERSON);
            }

            $this->entityManager->persist(entity: $userInvitation);

            $this->entityManager->flush();

            $this->userInvitationEmailService->sendUserInvitationEmail(userInvitation: $userInvitation);

            $this->addFlash(
                type: 'dataSaved',
                message: 'Der ' . $userInvitation->getUserRegistrationType()->getName() . ' wurde angelegt und per E-Mail zur Nutzung von LeAn® eingeladen.',
            );

            $userRegistrationTypeName = match ($userInvitation->getUserRegistrationType()) {
                UserRegistrationType::SEEKER => 'suchende',
                UserRegistrationType::PROVIDER => 'anbietende'
            };

            $urlParameters = $this->fetchMergedUrlParametersFromSession(
                urlParameterObjectClasses: self::URL_PARAMETER_OBJECT_CLASSES,
                sessionKeyName: $overviewSessionKeyName
            );

            return $this->redirectToRoute(
                route: self::OVERVIEW_ROUTE_NAME,
                parameters: array_merge(['userRegistrationTypeName' => $userRegistrationTypeName], $urlParameters)
            );
        }

        return $this->render(view: 'external_user/user_invitation/create.html.twig', parameters: [
            'companyUserInvitationForm'       => $companyUserInvitationForm,
            'naturalPersonUserInvitationForm' => $naturalPersonUserInvitationForm,
            'activeNavGroup'                  => self::ACTIVE_NAV_GROUP,
            'activeNavItem'                   => self::ACTIVE_NAV_ITEM,
        ]);
    }

    #[Route('/{userInvitationId<\d{1,10}>}', name: 'edit', methods: ['GET', 'POST'])]
    public function edit(int $userInvitationId, Request $request, UserInterface $user): Response
    {
        $grantedUserRegistrationTypes = $this->userInvitationSecurityService->getGrantedUserRegistrationTypes();

        if ($grantedUserRegistrationTypes === null) {
            throw new AccessDeniedHttpException();
        }

        $userInvitation = $this->userInvitationService->fetchUserInvitationById(account: $user->getAccount(), id: $userInvitationId);

        if ($userInvitation === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'view', subject: $userInvitation);

        $formOptions = [
            'userRegistrationTypes' => $grantedUserRegistrationTypes,
            'canEdit'               => $this->isGranted(attribute: 'edit', subject: $userInvitation),
        ];

        $userInvitationForm = match ($userInvitation->getPersonType()) {
            PersonType::NATURAL_PERSON => $this->createForm(
                type: NaturalPersonUserInvitationType::class,
                data: $userInvitation,
                options: $formOptions
            ),
            PersonType::COMPANY => $this->createForm(
                type: CompanyUserInvitationType::class,
                data: $userInvitation,
                options: $formOptions
            ),
            default => throw new BadRequestException(),
        };

        $userInvitationForm->add(child: 'save', type: SubmitType::class);

        $userInvitationForm->handleRequest(request: $request);

        if ($userInvitationForm->isSubmitted() && $userInvitationForm->isValid()) {
            $this->denyAccessUnlessGranted(attribute: 'edit', subject: $userInvitation);

            $storedUserInvitation = $this->userInvitationService->fetchUserInvitationById(account: $user->getAccount(), id: $userInvitation->getId());

            if ($storedUserInvitation->getUserRegistrationType() !== $userInvitation->getUserRegistrationType()) {
                throw new BadRequestHttpException();
            }

            $this->entityManager->persist(entity: $userInvitation);

            $this->entityManager->flush();

            $this->addFlash(type: 'dataSaved', message: 'Die Daten wurden gespeichert. Eine Einladung wurde nicht erneut versandt.');

            $userRegistrationType = $userInvitation->getUserRegistrationType();

            $userRegistrationTypeName = self::fetchUserRegistrationTypeNameByUserRegistrationType(userRegistrationType: $userRegistrationType);
            $overviewSessionKeyName = self::fetchOverviewSessionKeyNameByUserRegistrationType(userRegistrationType: $userRegistrationType);

            $urlParameters = $this->fetchMergedUrlParametersFromSession(
                urlParameterObjectClasses: self::URL_PARAMETER_OBJECT_CLASSES,
                sessionKeyName: $overviewSessionKeyName
            );

            return $this->redirectToRoute(
                route: self::OVERVIEW_ROUTE_NAME,
                parameters: array_merge(['userRegistrationTypeName' => $userRegistrationTypeName], $urlParameters)
            );
        }

        return $this->render(view: 'external_user/user_invitation/edit.html.twig', parameters: [
            'userInvitationForm' => $userInvitationForm,
            'userInvitation'     => $userInvitation,
            'activeNavGroup'     => self::ACTIVE_NAV_GROUP,
            'activeNavItem'      => self::ACTIVE_NAV_ITEM,
        ]);
    }

    #[Security("is_granted('ROLE_USER')")]
    #[Route('/{userInvitationId<\d{1,10}>}/loeschen', name: 'delete', methods: ['GET', 'POST'])]
    public function delete(int $userInvitationId, Request $request, UserInterface $user): Response
    {
        $grantedUserRegistrationTypes = $this->userInvitationSecurityService->getGrantedUserRegistrationTypes();

        if ($grantedUserRegistrationTypes === null) {
            throw new AccessDeniedHttpException();
        }

        $account = $user->getAccount();

        $userInvitation = $this->userInvitationService->fetchUserInvitationById(account: $account, id: $userInvitationId);

        if ($userInvitation === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'delete', subject: $userInvitation);

        if ($user->getUserIdentifier() !== $userInvitation->getCreatedByAccountUser()->getUserIdentifier()) {
            throw new AccessDeniedHttpException();
        }

        $formOptions = [
            'userRegistrationTypes' => $grantedUserRegistrationTypes,
            'canEdit'               => false,
        ];

        $userInvitationForm = match ($userInvitation->getPersonType()) {
            PersonType::NATURAL_PERSON => $this->createForm(
                type: NaturalPersonUserInvitationType::class,
                data: $userInvitation,
                options: $formOptions
            ),
            PersonType::COMPANY => $this->createForm(
                type: CompanyUserInvitationType::class,
                data: $userInvitation,
                options: $formOptions
            ),
            default => throw new BadRequestException(),
        };

        $userInvitationDeleteForm = $this->createForm(type: UserInvitationDeleteType::class);

        $userInvitationDeleteForm->add(child: 'delete', type: SubmitType::class);

        $userInvitationDeleteForm->handleRequest(request: $request);

        if (
            $userInvitationDeleteForm->isSubmitted()
            && $userInvitationDeleteForm->isValid()
            && $userInvitationDeleteForm->get('verificationCode')->getData() === 'einladung_' . $userInvitation->getId()
        ) {
            $userRegistration = $userInvitation->getUserRegistration();

            if ($userRegistration !== null) {
                $this->userRegistrationDeletionService->deleteUserRegistration(userRegistration: $userRegistration);

                $this->userRegistrationEmailService->sendUserRegistrationDeletedEmail(userRegistration: $userRegistration);
            }

            $this->userInvitationDeletionService->deleteUserInvitation(userInvitation: $userInvitation);

            $this->userInvitationEmailService->sendUserInvitationDeletedEmail(userInvitation: $userInvitation);

            $this->addFlash(type: 'dataDeleted', message: 'Die Einladung wurde zurückgezogen und die zugehörigen Daten gelöscht.');

            $userRegistrationType = $userInvitation->getUserRegistrationType();

            $userRegistrationTypeName = self::fetchUserRegistrationTypeNameByUserRegistrationType(userRegistrationType: $userRegistrationType);
            $overviewSessionKeyName = self::fetchOverviewSessionKeyNameByUserRegistrationType(userRegistrationType: $userRegistrationType);

            $urlParameters = $this->fetchMergedUrlParametersFromSession(
                urlParameterObjectClasses: self::URL_PARAMETER_OBJECT_CLASSES,
                sessionKeyName: $overviewSessionKeyName
            );

            return $this->redirectToRoute(
                route: self::OVERVIEW_ROUTE_NAME,
                parameters: array_merge(['userRegistrationTypeName' => $userRegistrationTypeName], $urlParameters)
            );
        }

        return $this->render(view: 'external_user/user_invitation/delete.html.twig', parameters: [
            'userInvitationDeleteForm' => $userInvitationDeleteForm,
            'userInvitationForm'       => $userInvitationForm,
            'userInvitation'           => $userInvitation,
            'activeNavGroup'           => self::ACTIVE_NAV_GROUP,
            'activeNavItem'            => self::ACTIVE_NAV_ITEM,
        ]);
    }

    #[Security("is_granted('ROLE_USER')")]
    #[Route('/{userInvitationId<\d{1,10}>}/versenden', name: 'resend', methods: ['GET'])]
    public function resend(int $userInvitationId, UserInterface $user): Response
    {
        $userInvitation = $this->userInvitationService->fetchUserInvitationById(account: $user->getAccount(), id: $userInvitationId);

        if ($userInvitation === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'edit', subject:  $userInvitation);

        if ($user !== $userInvitation->getCreatedByAccountUser()) {
            throw new AccessDeniedHttpException();
        }

        if ($userInvitation->checkOverdue() === true) {
            throw new BadRequestException();
        }

        $this->userInvitationService->resendUserInvitationToUser(userInvitation: $userInvitation);

        $this->addFlash(type: 'dataSaved', message: 'Der ' . $userInvitation->getUserRegistrationType()->getName() . ' wurde erneut per E-Mail zur Nutzung von LeAn® eingeladen.');

        $userRegistrationType = $userInvitation->getUserRegistrationType();

        $userRegistrationTypeName = self::fetchUserRegistrationTypeNameByUserRegistrationType(userRegistrationType: $userRegistrationType);
        $overviewSessionKeyName = self::fetchOverviewSessionKeyNameByUserRegistrationType(userRegistrationType: $userRegistrationType);

        $urlParameters = $this->fetchMergedUrlParametersFromSession(
            urlParameterObjectClasses: self::URL_PARAMETER_OBJECT_CLASSES,
            sessionKeyName: $overviewSessionKeyName
        );

        return $this->redirectToRoute(
            route: self::OVERVIEW_ROUTE_NAME,
            parameters: array_merge(['userRegistrationTypeName' => $userRegistrationTypeName], $urlParameters)
        );
    }

    #[Security("is_granted('ROLE_USER')")]
    #[Route('/{userInvitationId<\d{1,10}>}/aktivieren', name: 'activate', methods: ['POST'])]
    public function activate(int $userInvitationId, UserInterface $user): Response
    {
        $userInvitation = $this->userInvitationService->fetchUserInvitationById(account: $user->getAccount(), id: $userInvitationId);

        if ($userInvitation === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'edit', subject: $userInvitation);

        $this->userInvitationEmailService->sendUserInvitationEmail(userInvitation: $userInvitation);

        $this->addFlash(
            type: 'dataSaved',
            message: 'Der Account wurde freigeschaltet. Eine E-Mail-Bestätigung wurde an'. $userInvitation->getFirstName() .' '.$userInvitation->getLastName().' versandt.'
        );

        $userRegistrationType = $userInvitation->getUserRegistrationType();

        $userRegistrationTypeName = self::fetchUserRegistrationTypeNameByUserRegistrationType(userRegistrationType: $userRegistrationType);
        $overviewSessionKeyName = self::fetchOverviewSessionKeyNameByUserRegistrationType(userRegistrationType: $userRegistrationType);

        $urlParameters = $this->fetchMergedUrlParametersFromSession(
            urlParameterObjectClasses: self::URL_PARAMETER_OBJECT_CLASSES,
            sessionKeyName: $overviewSessionKeyName
        );

        return $this->redirectToRoute(
            route: self::OVERVIEW_ROUTE_NAME,
            parameters: array_merge(['userRegistrationTypeName' => $userRegistrationTypeName], $urlParameters)
        );
    }
}
