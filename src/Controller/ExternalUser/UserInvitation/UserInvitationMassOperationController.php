<?php

declare(strict_types=1);

namespace App\Controller\ExternalUser\UserInvitation;

use App\Domain\UserRegistration\UserInvitationDeletionService;
use App\Domain\UserRegistration\UserInvitationEmailService;
use App\Domain\UserRegistration\UserInvitationSecurityService;
use App\Domain\UserRegistration\UserInvitationService;
use App\Domain\UserRegistration\UserRegistrationDeletionService;
use App\Domain\UserRegistration\UserRegistrationEmailService;
use App\Form\Type\ExternalUser\UserInvitation\UserInvitationMassOperationDeleteType;
use App\Form\Type\ExternalUser\UserInvitation\UserInvitationMassOperationSendInvitationType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Security("is_granted('ROLE_ACCOUNT_ADMIN') or is_granted('ROLE_USER')")]
#[Route('/externe-nutzer/einladungen/massenoperation/{userRegistrationTypeName<suchende|anbietende>}', name: 'external_user_user_invitation_mass_operation_')]
class UserInvitationMassOperationController extends AbstractController
{
    private const ACTIVE_NAV_GROUP = 'external_user';
    private const ACTIVE_NAV_ITEM = self::ACTIVE_NAV_GROUP . '_user_invitation';

    public function __construct(
        private readonly UserInvitationService $userInvitationService,
        private readonly UserInvitationSecurityService $userInvitationSecurityService,
        private readonly UserInvitationEmailService $userInvitationEmailService,
        private readonly UserInvitationDeletionService $userInvitationDeletionService,
        private readonly UserRegistrationDeletionService $userRegistrationDeletionService,
        private readonly UserRegistrationEmailService $userRegistrationEmailService
    ) {
    }

    #[Route('/loeschen', name: 'delete', methods: ['POST'])]
    public function delete(string $userRegistrationTypeName, Request $request, UserInterface $user): Response
    {
        $userRegistrationTypes = $this->userInvitationSecurityService->getGrantedUserRegistrationTypes();

        if ($userRegistrationTypes === null) {
            throw new AccessDeniedHttpException();
        }

        $account = $user->getAccount();

        $userInvitationMassOperationDeleteForm = $this->createForm(type: UserInvitationMassOperationDeleteType::class);

        $userInvitationMassOperationDeleteForm->add(child: 'delete', type: SubmitType::class);

        $userInvitationMassOperationDeleteForm->handleRequest(request: $request);

        if ($request->get('selectedRowIds') !== null) {
            $userInvitationMassOperationDeleteForm->get('selectedRowIds')->setData($request->get('selectedRowIds'));
        }

        $userInvitations = $this->userInvitationService->fetchUserInvitationsByIds(
            account: $account,
            ids: json_decode($userInvitationMassOperationDeleteForm->get('selectedRowIds')->getData()),
            userRegistrationTypes: $userRegistrationTypes
        );

        if (
            $userInvitationMassOperationDeleteForm->isSubmitted()
            && $userInvitationMassOperationDeleteForm->isValid()
            && $userInvitationMassOperationDeleteForm->get('verificationCode')->getData() === 'einladungen_' . count($userInvitations)
        ) {
            foreach ($userInvitations as $userInvitation) {
                $userRegistration = $userInvitation->getUserRegistration();

                if ($userRegistration !== null) {
                    $this->userRegistrationDeletionService->deleteUserRegistration(userRegistration: $userRegistration);

                    $this->userRegistrationEmailService->sendUserRegistrationDeletedEmail(userRegistration: $userRegistration);
                }

                $this->userInvitationDeletionService->deleteUserInvitation(userInvitation: $userInvitation);

                $this->userInvitationEmailService->sendUserInvitationDeletedEmail(userInvitation: $userInvitation);
            }

            $this->addFlash(type: 'dataDeleted', message: 'Die Einladungen wurde zurückgezogen und die zugehörigen Daten gelöscht.');

            return $this->redirectToRoute(route: 'external_user_user_invitation_overview', parameters: ['userRegistrationTypeName' => $userRegistrationTypeName]);
        }

        return $this->render(view: 'external_user/user_invitation/mass_operation/delete.html.twig', parameters: [
            'userInvitationMassOperationDeleteForm' => $userInvitationMassOperationDeleteForm,
            'userInvitations'                       => $userInvitations,
            'pageTitle'                             => 'Nutzermanagement | Einladungen löschen',
            'activeNavGroup'                        => self::ACTIVE_NAV_GROUP,
            'activeNavItem'                         => self::ACTIVE_NAV_ITEM,
        ]);
    }

    #[Route('/einladungen-erneut-senden', name: 'send_invitation', methods: ['POST'])]
    public function sendInvitation(string $userRegistrationTypeName, Request $request, UserInterface $user): Response
    {
        $userRegistrationTypes = $this->userInvitationSecurityService->getGrantedUserRegistrationTypes();

        if ($userRegistrationTypes === null) {
            throw new AccessDeniedHttpException();
        }

        $account = $user->getAccount();

        $userInvitationMassOperationSendInvitationForm = $this->createForm(type: UserInvitationMassOperationSendInvitationType::class);

        $userInvitationMassOperationSendInvitationForm->add(child: 'send_invitation', type: SubmitType::class);

        $userInvitationMassOperationSendInvitationForm->handleRequest(request: $request);

        if ($request->get('selectedRowIds') !== null) {
            $userInvitationMassOperationSendInvitationForm->get('selectedRowIds')->setData($request->get('selectedRowIds'));
        }

        $userInvitations = $this->userInvitationService->fetchUserInvitationsByIds(
            account: $account,
            ids: json_decode($userInvitationMassOperationSendInvitationForm->get('selectedRowIds')->getData()),
            userRegistrationTypes: $userRegistrationTypes
        );

        if ($userInvitationMassOperationSendInvitationForm->isSubmitted() && $userInvitationMassOperationSendInvitationForm->isValid()) {
            foreach ($userInvitations as $userInvitation) {
                if ($user === $userInvitation->getCreatedByAccountUser() && $userInvitation->checkOverdue() === true) {
                    $this->userInvitationService->resendUserInvitationToUser(userInvitation: $userInvitation);
                }
            }

            $this->addFlash(type: 'dataDeleted', message: 'Die Einladungen zur Nutzung von LeAn® wurde versendet.');

            return $this->redirectToRoute(route: 'external_user_user_invitation_overview', parameters: ['userRegistrationTypeName' => $userRegistrationTypeName]);
        }

        return $this->render(view: 'external_user/user_invitation/mass_operation/send_invitation.html.twig', parameters: [
            'userInvitationMassOperationSendInvitationForm' => $userInvitationMassOperationSendInvitationForm,
            'userInvitations'                               => $userInvitations,
            'pageTitle'                                     => 'Nutzermanagement | Einladungen erneut senden',
            'activeNavGroup'                                => self::ACTIVE_NAV_GROUP,
            'activeNavItem'                                 => self::ACTIVE_NAV_ITEM,
         ]);
    }
}
