<?php

declare(strict_types=1);

namespace App\Controller\ExternalUser\AccountUser;

use App\Controller\AbstractLeAnController;
use App\Domain\Account\AccountDeletionService;
use App\Domain\Account\AccountUserDeletionService;
use App\Domain\Account\AccountUserPasswordResetService;
use App\Domain\Account\AccountUserSecurityService;
use App\Domain\Account\AccountUserService;
use App\Domain\Entity\AccountUser\AccountUserFilter;
use App\Domain\Entity\AccountUser\AccountUserSearch;
use App\Domain\Entity\AccountUser\AccountUserType;
use App\Domain\Entity\DeletionType;
use App\Domain\Entity\PaginationParameter;
use App\Domain\Entity\SortingOption\SortingOption;
use App\Domain\SessionStoredUrlParameter\SessionStoredUrlParameterService;
use App\Form\Type\ExternalUser\AccountUser\AccountUserDeleteType;
use App\Form\Type\ExternalUser\AccountUser\AccountUserFilterType;
use App\Form\Type\ExternalUser\AccountUser\AccountUserSearchType;
use App\SessionStorage\SessionStorageService;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Security("is_granted('ROLE_ACCOUNT_ADMIN') or is_granted('ROLE_VIEWER')")]
#[Route('/externe-nutzer/benutzer', name: 'external_user_account_user_')]
class AccountUserController extends AbstractLeAnController
{
    private const ACTIVE_NAV_GROUP = 'external_user';
    private const ACTIVE_NAV_ITEM_PROVIDER = self::ACTIVE_NAV_GROUP . '_account_user_property_provider';
    private const ACTIVE_NAV_ITEM_SEEKER = self::ACTIVE_NAV_GROUP . '_account_user_property_seeker';

    private const URL_PARAMETER_OBJECT_CLASSES = [
        PaginationParameter::class,
        SortingOption::class,
    ];

    private const ACCOUNT_USER_TYPES = [AccountUserType::PROPERTY_PROVIDER, AccountUserType::PROPERTY_SEEKER];

    private const PROPERTY_PROVIDER_OVERVIEW_ROUTE_NAME = 'external_user_account_user_property_provider_overview';
    private const PROPERTY_SEEKER_OVERVIEW_ROUTE_NAME = 'external_user_account_user_property_seeker_overview';

    private const PROPERTY_PROVIDER_OVERVIEW_SESSION_KEY_NAME = 'propertyProviderAccountUserOverview';
    private const PROPERTY_SEEKER_OVERVIEW_SESSION_KEY_NAME = 'propertySeekerAccountUserOverview';

    private static function fetchOverviewRouteNameByAccountUserType(AccountUserType $accountUserType): string
    {
        return match ($accountUserType) {
            AccountUserType::PROPERTY_PROVIDER => self::PROPERTY_PROVIDER_OVERVIEW_ROUTE_NAME,
            AccountUserType::PROPERTY_SEEKER => self::PROPERTY_SEEKER_OVERVIEW_ROUTE_NAME,
            default => throw new \RuntimeException(message: 'invalid AccountUserType')
        };
    }

    private static function fetchOverviewSessionKeyNameByAccountUserType(AccountUserType $accountUserType): string
    {
        return match ($accountUserType) {
            AccountUserType::PROPERTY_PROVIDER => self::PROPERTY_PROVIDER_OVERVIEW_SESSION_KEY_NAME,
            AccountUserType::PROPERTY_SEEKER => self::PROPERTY_SEEKER_OVERVIEW_SESSION_KEY_NAME,
            default => throw new \RuntimeException(message: 'invalid AccountUserType')
        };
    }

    public function __construct(
        SessionStorageService $sessionStorageService,
        SessionStoredUrlParameterService $sessionStoredUrlParameterService,
        private readonly AccountUserService $accountUserService,
        private readonly AccountUserSecurityService $accountUserSecurityService,
        private readonly AccountUserDeletionService $accountUserDeletionService,
        private readonly AccountDeletionService $accountDeletionService,
        private readonly AccountUserPasswordResetService $accountUserPasswordResetService,
        private readonly EntityManagerInterface $entityManager
    ) {
        parent::__construct(
            sessionStorageService: $sessionStorageService,
            sessionStoredUrlParameterService: $sessionStoredUrlParameterService
        );
    }

    #[Route('/anbietende/uebersicht', name: 'property_provider_overview', methods: ['GET', 'POST'])]
    public function propertyProviderOverview(Request $request, UserInterface $user): Response
    {
        $grantedAccountUserTypes = $this->accountUserSecurityService->getGrantedAccountUserTypes();

        if ($grantedAccountUserTypes === null) {
            throw new AccessDeniedHttpException();
        }

        $accountUserType = AccountUserType::PROPERTY_PROVIDER;

        if (in_array(needle: $accountUserType, haystack: $grantedAccountUserTypes) === false) {
            throw new AccessDeniedHttpException();
        }

        $paginationParameter = PaginationParameter::createFromRequest(request: $request);

        $this->storeObjectInSession(object: $paginationParameter, sessionKeyName: self::PROPERTY_PROVIDER_OVERVIEW_SESSION_KEY_NAME);

        $sortingOption = null;

        if ($request->query->get(key: 'sortieren') !== null) {
            $sortingOption = SortingOption::createFromRequest(request: $request);

            $this->storeObjectInSession(object: $sortingOption, sessionKeyName: self::PROPERTY_PROVIDER_OVERVIEW_SESSION_KEY_NAME);
        }

        $accountUserFilterForm = $this->createForm(type: AccountUserFilterType::class, options: [
            'dataClassName'  => AccountUserFilter::class,
            'sessionKeyName' => self::PROPERTY_PROVIDER_OVERVIEW_SESSION_KEY_NAME,
        ]);

        $accountUserFilterForm->add(child: 'apply', type: SubmitType::class);

        $accountUserFilterForm->handleRequest(request: $request);

        $accountUserSearchForm = $this->createForm(type: AccountUserSearchType::class, options: [
            'dataClassName'  => AccountUserSearch::class,
            'sessionKeyName' => self::PROPERTY_PROVIDER_OVERVIEW_SESSION_KEY_NAME,
        ]);

        $accountUserSearchForm->add(child: 'search', type: SubmitType::class);

        $accountUserSearchForm->handleRequest(request: $request);

        $accountUsers = $this->accountUserService->fetchAccountUsersPaginatedByAccount(
            account: $user->getAccount(),
            withFromSubAccounts: true,
            firstResult: $paginationParameter->getFirstResult(),
            maxResults: $paginationParameter->getLimit(),
            accountUserTypes: [$accountUserType],
            accountUserFilter: $accountUserFilterForm->getData(),
            accountUserSearch: $accountUserSearchForm->getData(),
            sortingOption: $sortingOption
        );

        return $this->render(view: 'external_user/account_user/property_provider_overview.html.twig', parameters: [
            'accountUsers'          => $accountUsers,
            'accountUserType'       => $accountUserType,
            'accountUserFilterForm' => $accountUserFilterForm,
            'accountUserSearchForm' => $accountUserSearchForm,
            'activeNavGroup'        => self::ACTIVE_NAV_GROUP,
            'activeNavItem'         => self::ACTIVE_NAV_ITEM_PROVIDER,
            'pageTitle'             => 'Anbietende',
        ]);
    }

    #[Route('/suchende/uebersicht', name: 'property_seeker_overview', methods: ['GET', 'POST'])]
    public function propertySeekerOverview(Request $request, UserInterface $user): Response
    {
        $grantedAccountUserTypes = $this->accountUserSecurityService->getGrantedAccountUserTypes();

        if ($grantedAccountUserTypes === null) {
            throw new AccessDeniedHttpException();
        }

        $accountUserType = AccountUserType::PROPERTY_SEEKER;

        if (in_array(needle: $accountUserType, haystack: $grantedAccountUserTypes) === false) {
            throw new AccessDeniedHttpException();
        }

        $paginationParameter = PaginationParameter::createFromRequest(request: $request);

        $this->storeObjectInSession(object: $paginationParameter, sessionKeyName: self::PROPERTY_SEEKER_OVERVIEW_SESSION_KEY_NAME);

        $sortingOption = null;

        if ($request->query->get(key: 'sortieren') !== null) {
            $sortingOption = SortingOption::createFromRequest(request: $request);

            $this->storeObjectInSession(object: $sortingOption, sessionKeyName: self::PROPERTY_SEEKER_OVERVIEW_SESSION_KEY_NAME);
        }

        $accountUserFilterForm = $this->createForm(type: AccountUserFilterType::class, options: [
            'dataClassName'  => AccountUserFilter::class,
            'sessionKeyName' => self::PROPERTY_SEEKER_OVERVIEW_SESSION_KEY_NAME,
        ]);

        $accountUserFilterForm->add(child: 'apply', type: SubmitType::class);

        $accountUserFilterForm->handleRequest(request: $request);

        $accountUserSearchForm = $this->createForm(type: AccountUserSearchType::class, options: [
            'dataClassName'  => AccountUserSearch::class,
            'sessionKeyName' => self::PROPERTY_SEEKER_OVERVIEW_SESSION_KEY_NAME,
        ]);

        $accountUserSearchForm->add(child: 'search', type: SubmitType::class);

        $accountUserSearchForm->handleRequest(request: $request);

        $accountUsers = $this->accountUserService->fetchAccountUsersPaginatedByAccount(
            account: $user->getAccount(),
            withFromSubAccounts: false,
            firstResult: $paginationParameter->getFirstResult(),
            maxResults: $paginationParameter->getLimit(),
            accountUserTypes: [$accountUserType],
            accountUserFilter: $accountUserFilterForm->getData(),
            accountUserSearch: $accountUserSearchForm->getData(),
            sortingOption: $sortingOption
        );

        return $this->render(view: 'external_user/account_user/property_seeker_overview.html.twig', parameters: [
            'accountUsers'          => $accountUsers,
            'accountUserType'       => $accountUserType,
            'accountUserFilterForm' => $accountUserFilterForm,
            'accountUserSearchForm' => $accountUserSearchForm,
            'activeNavGroup'        => self::ACTIVE_NAV_GROUP,
            'activeNavItem'         => self::ACTIVE_NAV_ITEM_SEEKER,
            'pageTitle'             => 'Suchende',
        ]);
    }

    #[Route('/{accountUserId<\d{1,10}>}', name: 'edit', methods: ['GET', 'POST'])]
    public function edit(int $accountUserId, Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $accountUser = $this->accountUserService->fetchAccountUserById(account: $account, withFromSubAccounts: true, id: $accountUserId);

        if ($accountUser === null) {
            throw new NotFoundHttpException();
        }

        if (in_array(needle: $accountUser->getAccountUserType(), haystack: self::ACCOUNT_USER_TYPES) === false) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'view', subject: $accountUser);

        $accountUserForm = $this->createForm(type: \App\Form\Type\ExternalUser\AccountUser\AccountUserType::class, data: $accountUser, options: [
            'canEdit' => $this->isGranted(attribute: 'edit', subject: $accountUser),
        ]);

        $accountUserForm->add(child: 'save', type: SubmitType::class);

        $accountUserForm->handleRequest(request: $request);

        if ($accountUserForm->isSubmitted() && $accountUserForm->isValid()) {
            $this->denyAccessUnlessGranted(attribute: 'edit', subject: $accountUser);

            $this->entityManager->flush();

            $this->addFlash(type: 'dataSaved', message: 'Die Änderungen wurden gespeichert.');

            $sessionKeyName = self::fetchOverviewSessionKeyNameByAccountUserType(accountUserType: $accountUser->getAccountUserType());
            $route = self::fetchOverviewRouteNameByAccountUserType(accountUserType: $accountUser->getAccountUserType());

            $urlParameters = $this->fetchMergedUrlParametersFromSession(
                urlParameterObjectClasses: self::URL_PARAMETER_OBJECT_CLASSES,
                sessionKeyName: $sessionKeyName
            );

            return $this->redirectToRoute(route: $route, parameters: $urlParameters);
        }

        return $this->render(view: 'external_user/account_user/edit.html.twig', parameters: [
            'accountUserForm' => $accountUserForm,
            'accountUser'     => $accountUser,
            'submitResponse'  => $request->getSession()->getFlashBag()->get(type: 'submitResponse'),
            'pageTitle'       => '',
            'activeNavGroup'  => self::ACTIVE_NAV_GROUP,
            'activeNavItem'   => $accountUser->getAccountUserType() === AccountUserType::PROPERTY_PROVIDER ? self::ACTIVE_NAV_ITEM_PROVIDER : self::ACTIVE_NAV_ITEM_SEEKER,
        ]);
    }

    #[Security("is_granted('ROLE_ACCOUNT_ADMIN')")]
    #[Route('/{accountUserId<\d{1,10}>}/loeschen', name: 'delete', methods: ['GET', 'POST'])]
    public function delete(int $accountUserId, Request $request, UserInterface $user): Response
    {
        $accountUser = $this->accountUserService->fetchAccountUserById(account:  $user->getAccount(), withFromSubAccounts: true, id: $accountUserId);

        if ($accountUser === null) {
            throw new NotFoundHttpException();
        }

        if (in_array(needle: $accountUser->getAccountUserType(), haystack: self::ACCOUNT_USER_TYPES) === false) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'delete', subject: $accountUser);

        $accountUserDeleteForm = $this->createForm(type: AccountUserDeleteType::class);

        $accountUserDeleteForm->add(child: 'delete', type: SubmitType::class);

        $accountUserDeleteForm->handleRequest(request: $request);

        if (
            $accountUserDeleteForm->isSubmitted()
            && $accountUserDeleteForm->isValid()
            && $accountUserDeleteForm->get('verificationCode')->getData() === 'benutzer_' . $accountUser->getId()
        ) {
            if (
                $accountUser->getAccountUserType() === AccountUserType::PROPERTY_PROVIDER
                && $accountUser->getAccount()->getParentAccount() !== null
            ) {
                $this->accountUserDeletionService->deleteAccountUsersByAccount(
                    account: $accountUser->getAccount(),
                    deletionType: DeletionType::ANONYMIZATION,
                    deletedByAccountUser: $user
                );

                $this->accountDeletionService->deleteAccount(
                    account: $accountUser->getAccount(),
                    deletionType: DeletionType::SOFT,
                    deletedByAccountUser: $user
                );
            } else {
                $this->accountUserDeletionService->deleteAccountUser(
                    accountUser: $accountUser,
                    deletionType: DeletionType::ANONYMIZATION,
                    deletedByAccountUser: $user
                );
            }

            $this->addFlash(type: 'dataDeleted', message: 'Der Account wurde gelöscht.');

            $sessionKeyName = self::fetchOverviewSessionKeyNameByAccountUserType(accountUserType: $accountUser->getAccountUserType());
            $route = self::fetchOverviewRouteNameByAccountUserType(accountUserType: $accountUser->getAccountUserType());

            $urlParameters = $this->fetchMergedUrlParametersFromSession(
                urlParameterObjectClasses: self::URL_PARAMETER_OBJECT_CLASSES,
                sessionKeyName: $sessionKeyName
            );

            return $this->redirectToRoute(route: $route, parameters: $urlParameters);
        }

        $accountUserForm = $this->createForm(type: \App\Form\Type\ExternalUser\AccountUser\AccountUserType::class, data: $accountUser, options: [
            'canEdit' => false,
        ]);

        return $this->render(view: 'external_user/account_user/delete.html.twig', parameters: [
            'accountUserDeleteForm' => $accountUserDeleteForm,
            'accountUserForm'       => $accountUserForm,
            'accountUser'           => $accountUser,
            'submitResponse'        => $request->getSession()->getFlashBag()->get(type: 'submitResponse'),
            'pageTitle'             => '',
            'activeNavGroup'        => self::ACTIVE_NAV_GROUP,
            'activeNavItem'         => $accountUser->getAccountUserType() === AccountUserType::PROPERTY_PROVIDER ? self::ACTIVE_NAV_ITEM_PROVIDER : self::ACTIVE_NAV_ITEM_SEEKER,
        ]);
    }

    #[Security("is_granted('ROLE_ACCOUNT_ADMIN')")]
    #[Route('/{accountUserId<\d{1,10}>}/sperren', name: 'block', methods: ['GET', 'POST'])]
    public function block(int $accountUserId, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $accountUser = $this->accountUserService->fetchAccountUserById(account: $account, withFromSubAccounts: true, id: $accountUserId);

        if ($accountUser === null) {
            throw new NotFoundHttpException();
        }

        if (in_array(needle: $accountUser->getAccountUserType(), haystack: self::ACCOUNT_USER_TYPES) === false) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'edit', subject: $accountUser);

        $accountUser->setEnabled(false);

        $this->entityManager->flush();

        $this->addFlash(type: 'dataDeleted', message: 'Der Account wurde gesperrt.');

        return $this->redirectToRoute(route: 'external_user_account_user_edit', parameters: ['accountUserId' => $accountUser->getId()]);
    }

    #[Security("is_granted('ROLE_ACCOUNT_ADMIN')")]
    #[Route('/{accountUserId<\d{1,10}>}/entsperren', name: 'unblock', methods: ['GET', 'POST'])]
    public function unblock(int $accountUserId, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $accountUser = $this->accountUserService->fetchAccountUserById(account: $account, withFromSubAccounts: true, id: $accountUserId);

        if ($accountUser === null) {
            throw new NotFoundHttpException();
        }

        if (in_array(needle: $accountUser->getAccountUserType(), haystack: self::ACCOUNT_USER_TYPES) === false) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'edit', subject: $accountUser);

        $accountUser->setEnabled(true);

        $this->entityManager->flush();

        $this->addFlash(type: 'dataSaved', message: 'Der Account wurde entsperrt.');

        return $this->redirectToRoute(route: 'external_user_account_user_edit', parameters: ['accountUserId' => $accountUser->getId()]);
    }

    #[Security("is_granted('ROLE_ACCOUNT_ADMIN')")]
    #[Route('/{accountUserId<\d{1,10}>}/passwort-zuruecksetzen', name: 'create_reset_password', methods: ['GET', 'POST'])]
    public function createResetPassword(int $accountUserId, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $accountUser = $this->accountUserService->fetchAccountUserById(account: $account, withFromSubAccounts: true, id: $accountUserId);

        if ($accountUser === null) {
            throw new NotFoundHttpException();
        }

        if (in_array(needle: $accountUser->getAccountUserType(), haystack: self::ACCOUNT_USER_TYPES) === false) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'edit', subject: $accountUser);

        $this->accountUserPasswordResetService->doPasswordResetForAccountUser(accountUser: $accountUser, createdByAccountUser: $user);

        $this->addFlash(type: 'dataSaved', message: 'Eine E-Mail mit einem Link zum Zurücksetzen des Kennworts wurde versandt.');

        return $this->redirectToRoute(route: 'external_user_account_user_edit', parameters: ['accountUserId' => $accountUser->getId()]);
    }
}
