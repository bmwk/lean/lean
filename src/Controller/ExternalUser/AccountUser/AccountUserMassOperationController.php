<?php

declare(strict_types=1);

namespace App\Controller\ExternalUser\AccountUser;

use App\Controller\AbstractLeAnController;
use App\Domain\Account\AccountUserDeletionService;
use App\Domain\Account\AccountUserService;
use App\Domain\Entity\AccountUser\AccountUserType;
use App\Domain\Entity\DeletionType;
use App\Domain\Entity\PaginationParameter;
use App\Domain\Entity\SortingOption\SortingOption;
use App\Domain\SessionStoredUrlParameter\SessionStoredUrlParameterService;
use App\Form\Type\ExternalUser\AccountUser\AccountUserMassOperationDeleteType;
use App\SessionStorage\SessionStorageService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Security("is_granted('ROLE_ACCOUNT_ADMIN')")]
#[Route('/externe-nutzer/benutzer/massenoperation/{accountUserTypeName<suchende|anbietende>}', name: 'external_user_account_user_mass_operation_')]
class AccountUserMassOperationController extends AbstractLeAnController
{
    private const ACTIVE_NAV_GROUP = 'external_user';
    private const ACTIVE_NAV_ITEM_PROVIDER = self::ACTIVE_NAV_GROUP . '_account_user_property_provider';
    private const ACTIVE_NAV_ITEM_SEEKER = self::ACTIVE_NAV_GROUP . '_account_user_property_seeker';

    private const URL_PARAMETER_OBJECT_CLASSES = [
        PaginationParameter::class,
        SortingOption::class,
    ];

    private const PROPERTY_PROVIDER_OVERVIEW_ROUTE_NAME = 'external_user_account_user_property_provider_overview';
    private const PROPERTY_SEEKER_OVERVIEW_ROUTE_NAME = 'external_user_account_user_property_seeker_overview';

    private const PROPERTY_PROVIDER_OVERVIEW_SESSION_KEY_NAME = 'propertyProviderAccountUserOverview';
    private const PROPERTY_SEEKER_OVERVIEW_SESSION_KEY_NAME = 'propertySeekerAccountUserOverview';

    public function __construct(
        SessionStorageService $sessionStorageService,
        SessionStoredUrlParameterService $sessionStoredUrlParameterService,
        private readonly AccountUserService $accountUserService,
        private readonly AccountUserDeletionService $accountUserDeletionService
    ) {
        parent::__construct(
            sessionStorageService: $sessionStorageService,
            sessionStoredUrlParameterService: $sessionStoredUrlParameterService
        );
    }

    #[Route('/loeschen', name: 'delete', methods: ['POST'])]
    public function delete(string $accountUserTypeName, Request $request, UserInterface $user): Response
    {
        if ($accountUserTypeName === 'anbietende') {
            $accountUserType = AccountUserType::PROPERTY_PROVIDER;
            $activeNavItem =  self::ACTIVE_NAV_ITEM_PROVIDER;
            $pageTitle = 'Nutzermanagement | Anbietende löschen';
            $sessionKeyName = self::PROPERTY_PROVIDER_OVERVIEW_SESSION_KEY_NAME;
            $route = self::PROPERTY_PROVIDER_OVERVIEW_ROUTE_NAME;
        } elseif ($accountUserTypeName === 'suchende') {
            $accountUserType = AccountUserType::PROPERTY_SEEKER;
            $activeNavItem = self::ACTIVE_NAV_ITEM_SEEKER;
            $pageTitle = 'Nutzermanagement | Suchende löschen';
            $sessionKeyName = self::PROPERTY_SEEKER_OVERVIEW_SESSION_KEY_NAME;
            $route = self::PROPERTY_SEEKER_OVERVIEW_ROUTE_NAME;
        } else {
            throw new NotFoundHttpException();
        }

        $account = $user->getAccount();

        $accountUserMassOperationDeleteForm = $this->createForm(type: AccountUserMassOperationDeleteType::class);

        $accountUserMassOperationDeleteForm->add(child: 'delete', type: SubmitType::class);

        $accountUserMassOperationDeleteForm->handleRequest(request: $request);

        if ($request->get('selectedRowIds') !== null) {
            $accountUserMassOperationDeleteForm->get('selectedRowIds')->setData($request->get('selectedRowIds'));
        }

        $accountUsers = $this->accountUserService->fetchAccountUsersByIds(
            account: $account,
            withFromSubAccounts: true,
            ids: json_decode($accountUserMassOperationDeleteForm->get('selectedRowIds')->getData())
        );

        if (
            $accountUserMassOperationDeleteForm->isSubmitted()
            && $accountUserMassOperationDeleteForm->isValid()
            && $accountUserMassOperationDeleteForm->get('verificationCode')->getData() === 'benutzer_' . count($accountUsers)
        ) {
            foreach ($accountUsers as $accountUser) {
                $this->accountUserDeletionService->deleteAccountUser(
                    accountUser: $accountUser,
                    deletionType: DeletionType::ANONYMIZATION,
                    deletedByAccountUser: $user
                );
            }

            $this->addFlash(type: 'dataDeleted', message: 'Die Benutzer wurden gelöscht');

            $urlParameters = $this->fetchMergedUrlParametersFromSession(
                urlParameterObjectClasses: self::URL_PARAMETER_OBJECT_CLASSES,
                sessionKeyName: $sessionKeyName
            );

            return $this->redirectToRoute(route: $route, parameters: $urlParameters);
        }

        return $this->render(view: 'external_user/account_user/mass_operation/delete.html.twig', parameters: [
            'accountUserMassOperationDeleteForm' => $accountUserMassOperationDeleteForm,
            'accountUsers'                       => $accountUsers,
            'accountUserType'                    => $accountUserType,
            'pageTitle'                          => $pageTitle,
            'activeNavGroup'                     => self::ACTIVE_NAV_GROUP,
            'activeNavItem'                      => $activeNavItem,
        ]);
    }
}
