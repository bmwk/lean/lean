<?php

declare(strict_types=1);

namespace App\Controller\ExternalUser\UserRegistration;

use App\Controller\AbstractLeAnController;
use App\Domain\Entity\PaginationParameter;
use App\Domain\Entity\Person\PersonType;
use App\Domain\Entity\SortingOption\SortingOption;
use App\Domain\Entity\UserRegistration\UserRegistrationFilter;
use App\Domain\Entity\UserRegistration\UserRegistrationSearch;
use App\Domain\Entity\UserRegistration\UserRegistrationStatus;
use App\Domain\Entity\UserRegistration\UserRegistrationType;
use App\Domain\SessionStoredUrlParameter\SessionStoredUrlParameterService;
use App\Domain\UserRegistration\UserRegistrationDeletionService;
use App\Domain\UserRegistration\UserRegistrationEmailService;
use App\Domain\UserRegistration\UserRegistrationSecurityService;
use App\Domain\UserRegistration\UserRegistrationService;
use App\Form\Type\ExternalUser\UserRegistration\CompanyUserRegistrationType;
use App\Form\Type\ExternalUser\UserRegistration\NaturalPersonUserRegistrationType;
use App\Form\Type\ExternalUser\UserRegistration\UserRegistrationDeleteType;
use App\Form\Type\ExternalUser\UserRegistration\UserRegistrationFilterType;
use App\Form\Type\ExternalUser\UserRegistration\UserRegistrationSearchType;
use App\SessionStorage\SessionStorageService;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Security("is_granted('ROLE_USER') or is_granted('ROLE_VIEWER')")]
#[Route('/externe-nutzer/registrierungen', name: 'external_user_user_registration_')]
class UserRegistrationController extends AbstractLeAnController
{
    private const ACTIVE_NAV_GROUP = 'external_user';
    private const ACTIVE_NAV_ITEM = self::ACTIVE_NAV_GROUP . '_user_registration';

    private const OVERVIEW_ROUTE_NAME = 'external_user_user_registration_overview';

    private const URL_PARAMETER_OBJECT_CLASSES = [
        PaginationParameter::class,
        SortingOption::class,
    ];

    private static function fetchUserRegistrationTypeByUserRegistrationTypeName(string $userRegistrationTypeName): UserRegistrationType
    {
        return match ($userRegistrationTypeName) {
            'anbietende' => UserRegistrationType::PROVIDER,
            'suchende' => UserRegistrationType::SEEKER,
            default => throw new \RuntimeException(message: 'invalid userRegistrationTypeName')
        };
    }

    private static function fetchUserRegistrationTypeNameByUserRegistrationType(UserRegistrationType $userRegistrationType): string
    {
        return match ($userRegistrationType) {
            UserRegistrationType::PROVIDER => 'anbietende',
            UserRegistrationType::SEEKER => 'suchende',
            default => throw new \RuntimeException(message: 'invalid UserRegistrationType')
        };
    }

    private static function fetchOverviewSessionKeyNameByUserRegistrationType(UserRegistrationType $userRegistrationType): string
    {
        return match ($userRegistrationType) {
            UserRegistrationType::PROVIDER => 'propertyProviderUserRegistrationOverview',
            UserRegistrationType::SEEKER => 'propertySeekerUserRegistrationOverview',
            default => throw new \RuntimeException(message: 'invalid UserRegistrationType')
        };
    }

    public function __construct(
        SessionStorageService $sessionStorageService,
        SessionStoredUrlParameterService $sessionStoredUrlParameterService,
        private readonly UserRegistrationService $userRegistrationService,
        private readonly UserRegistrationSecurityService $userRegistrationSecurityService,
        private readonly UserRegistrationEmailService $userRegistrationEmailService,
        private readonly UserRegistrationDeletionService $userRegistrationDeletionService,
        private readonly EntityManagerInterface $entityManager
    ) {
        parent::__construct(
            sessionStorageService: $sessionStorageService,
            sessionStoredUrlParameterService: $sessionStoredUrlParameterService
        );
    }

    #[Route('/{userRegistrationTypeName<suchende|anbietende>}/uebersicht', name: 'overview', methods: ['GET', 'POST'])]
    public function overview(string $userRegistrationTypeName, Request $request, UserInterface $user): Response
    {
        $grantedUserRegistrationTypes = $this->userRegistrationSecurityService->getGrantedUserRegistrationTypes();

        if ($grantedUserRegistrationTypes === null) {
            throw new AccessDeniedHttpException();
        }

        $userRegistrationType = self::fetchUserRegistrationTypeByUserRegistrationTypeName(userRegistrationTypeName: $userRegistrationTypeName);
        $sessionKeyName = self::fetchOverviewSessionKeyNameByUserRegistrationType(userRegistrationType: $userRegistrationType);

        if ($userRegistrationType === UserRegistrationType::PROVIDER) {
            $pageTitle = 'Nutzermanagement | Anbietende';
            $view = 'external_user/user_registration/property_provider_overview.html.twig';
        } elseif ($userRegistrationType === UserRegistrationType::SEEKER) {
            $pageTitle = 'Nutzermanagement | Suchende';
            $view = 'external_user/user_registration/property_seeker_overview.html.twig';
        } else {
            throw new NotFoundHttpException();
        }

        if (in_array(needle: $userRegistrationType, haystack: $grantedUserRegistrationTypes) === false) {
            throw new AccessDeniedHttpException();
        }

        $paginationParameter = PaginationParameter::createFromRequest(request: $request);

        $this->storeObjectInSession(object: $paginationParameter, sessionKeyName: $sessionKeyName);

        $sortingOption = null;

        if ($request->query->get(key: 'sortieren') !== null) {
            $sortingOption = SortingOption::createFromRequest(request: $request);

            $this->storeObjectInSession(object: $sortingOption, sessionKeyName: $sessionKeyName);
        }

        $userRegistrationFilterForm = $this->createForm(type: UserRegistrationFilterType::class, options: [
            'dataClassName'  => UserRegistrationFilter::class,
            'sessionKeyName' => $sessionKeyName,
        ]);

        $userRegistrationFilterForm->add(child: 'apply', type: SubmitType::class);

        $userRegistrationFilterForm->handleRequest(request: $request);

        $userRegistrationSearchForm = $this->createForm(type: UserRegistrationSearchType::class, options: [
            'dataClassName'  => UserRegistrationSearch::class,
            'sessionKeyName' => $sessionKeyName,
        ]);

        $userRegistrationSearchForm->add(child: 'search', type: SubmitType::class);

        $userRegistrationSearchForm->handleRequest(request: $request);

        $userRegistrations = $this->userRegistrationService->fetchUserRegistrationsPaginatedByAccount(
            account: $user->getAccount(),
            withoutInvitation: false,
            firstResult: $paginationParameter->getFirstResult(),
            maxResults: $paginationParameter->getLimit(),
            userRegistrationTypes: [$userRegistrationType],
            userRegistrationFilter: $userRegistrationFilterForm->getData(),
            userRegistrationSearch: $userRegistrationSearchForm->getData(),
            sortingOption: $sortingOption
        );

        return $this->render(view: $view, parameters: [
            'userRegistrations'          => $userRegistrations,
            'userRegistrationType'       => $userRegistrationType,
            'userRegistrationFilterForm' => $userRegistrationFilterForm,
            'userRegistrationSearchForm' => $userRegistrationSearchForm,
            'pageTitle'                  => $pageTitle,
            'activeNavGroup'             => self::ACTIVE_NAV_GROUP,
            'activeNavItem'              => self::ACTIVE_NAV_ITEM,
        ]);
    }

    #[Route('/{userRegistrationId<\d{1,10}>}', name: 'edit', methods: ['GET', 'POST'])]
    public function edit(int $userRegistrationId, Request $request, UserInterface $user): Response
    {
        $userRegistration = $this->userRegistrationService->fetchUserRegistrationById(account: $user->getAccount(), id: $userRegistrationId);

        if ($userRegistration === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'view', subject: $userRegistration);

        $formOptions = [
            'canEdit' => $this->isGranted(attribute: 'edit', subject: $userRegistration),
        ];

        $userRegistrationForm = match ($userRegistration->getPersonType()) {
            PersonType::NATURAL_PERSON => $this->createForm(type: NaturalPersonUserRegistrationType::class, data: $userRegistration, options: $formOptions),
            PersonType::COMPANY => $this->createForm(type: CompanyUserRegistrationType::class, data: $userRegistration, options: $formOptions),
            default => throw new BadRequestException(),
        };

        $userRegistrationForm->add(child: 'save', type: SubmitType::class);

        $userRegistrationForm->handleRequest(request: $request);

        if ($userRegistrationForm->isSubmitted() && $userRegistrationForm->isValid()) {
            $this->denyAccessUnlessGranted(attribute: 'edit', subject: $userRegistration);

            $this->entityManager->flush();

            $this->addFlash(type: 'dataSaved', message: 'Die Daten wurden gespeichert.');

            $userRegistrationType = $userRegistration->getUserRegistrationType();

            $userRegistrationTypeName = self::fetchUserRegistrationTypeNameByUserRegistrationType(userRegistrationType: $userRegistrationType);
            $overviewSessionKeyName = self::fetchOverviewSessionKeyNameByUserRegistrationType(userRegistrationType: $userRegistrationType);

            $urlParameters = $this->fetchMergedUrlParametersFromSession(
                urlParameterObjectClasses: self::URL_PARAMETER_OBJECT_CLASSES,
                sessionKeyName: $overviewSessionKeyName
            );

            return $this->redirectToRoute(
                route: self::OVERVIEW_ROUTE_NAME,
                parameters: array_merge(['userRegistrationTypeName' => $userRegistrationTypeName], $urlParameters)
            );
        }

        return $this->render(view: 'external_user/user_registration/edit.html.twig', parameters: [
            'userRegistrationForm' => $userRegistrationForm,
            'userRegistration'     => $userRegistration,
            'activeNavGroup'       => self::ACTIVE_NAV_GROUP,
            'activeNavItem'        => self::ACTIVE_NAV_ITEM,
        ]);
    }

    #[Security("is_granted('ROLE_USER')")]
    #[Route('/{userRegistrationId<\d{1,10}>}/loeschen', name: 'delete', methods: ['GET', 'POST'])]
    public function delete(int $userRegistrationId, Request $request, UserInterface $user): Response
    {
        $userRegistration = $this->userRegistrationService->fetchUserRegistrationById(account: $user->getAccount(), id: $userRegistrationId);

        if ($userRegistration === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'delete', subject: $userRegistration);

        $userRegistrationDeleteForm = $this->createForm(type: UserRegistrationDeleteType::class);

        $userRegistrationDeleteForm->add(child: 'delete', type: SubmitType::class);

        $userRegistrationDeleteForm->handleRequest(request: $request);

        if (
            $userRegistrationDeleteForm->isSubmitted()
            && $userRegistrationDeleteForm->isValid()
            && $userRegistrationDeleteForm->get('verificationCode')->getData() === 'registrierung_' . $userRegistration->getId()
        ) {
            $this->userRegistrationDeletionService->deleteUserRegistration(userRegistration: $userRegistration);

            $this->userRegistrationEmailService->sendUserRegistrationDeletedEmail(userRegistration: $userRegistration);

            $this->addFlash(
                type: 'dataDeleted',
                message: 'Die Registrierung wurde abgelehnt und der Account gelöscht. Der ' . $userRegistration->getUserRegistrationType()->getName() . ' wurde per E-Mail informiert.'
            );

            $userRegistrationType = $userRegistration->getUserRegistrationType();

            $userRegistrationTypeName = self::fetchUserRegistrationTypeNameByUserRegistrationType(userRegistrationType: $userRegistrationType);
            $overviewSessionKeyName = self::fetchOverviewSessionKeyNameByUserRegistrationType(userRegistrationType: $userRegistrationType);

            $urlParameters = $this->fetchMergedUrlParametersFromSession(
                urlParameterObjectClasses: self::URL_PARAMETER_OBJECT_CLASSES,
                sessionKeyName: $overviewSessionKeyName
            );

            return $this->redirectToRoute(
                route: self::OVERVIEW_ROUTE_NAME,
                parameters: array_merge(['userRegistrationTypeName' => $userRegistrationTypeName], $urlParameters)
            );
        }

        $formOptions = [
            'canEdit' => false,
        ];

        $userRegistrationForm = match ($userRegistration->getPersonType()) {
            PersonType::NATURAL_PERSON => $this->createForm(type: NaturalPersonUserRegistrationType::class, data: $userRegistration, options: $formOptions),
            PersonType::COMPANY => $this->createForm(type: CompanyUserRegistrationType::class, data: $userRegistration, options: $formOptions),
            default => throw new BadRequestException(),
        };

        $userRegistrationForm->add(child: 'save', type: SubmitType::class);

        $userRegistrationForm->handleRequest(request: $request);

        return $this->render(view: 'external_user/user_registration/delete.html.twig', parameters: [
            'userRegistrationDeleteForm' => $userRegistrationDeleteForm,
            'userRegistrationForm'       => $userRegistrationForm,
            'userRegistration'           => $userRegistration,
            'activeNavGroup'             => self::ACTIVE_NAV_GROUP,
            'activeNavItem'              => self::ACTIVE_NAV_ITEM,
        ]);
    }

    #[Security("is_granted('ROLE_USER')")]
    #[Route('/{userRegistrationId<\d{1,10}>}/freischalten', name: 'activate', methods: ['GET'])]
    public function activate(int $userRegistrationId, UserInterface $user): Response
    {
        $userRegistration = $this->userRegistrationService->fetchUserRegistrationById(account: $user->getAccount(), id: $userRegistrationId);

        if ($userRegistration === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'edit', subject: $userRegistration);

        if (
            $userRegistration->getUserRegistrationStatus() !== UserRegistrationStatus::NOT_ACTIVE
            && $userRegistration->getUserRegistrationStatus() !== UserRegistrationStatus::NOT_UNLOCKED
        ) {
            throw new NotFoundHttpException();
        }

        if ($userRegistration->isEmailConfirmed() === false) {
            throw new NotFoundHttpException();
        }

        $this->userRegistrationService->activateUser(userRegistration: $userRegistration, createdByAccountUser: $user);

        $this->addFlash(
            type: 'dataSaved',
            message: 'Der Account wurde freigeschaltet. Der ' . $userRegistration->getUserRegistrationType()->getName() . ' wurde per E-Mail informiert.'
        );

        $userRegistrationType = $userRegistration->getUserRegistrationType();

        $userRegistrationTypeName = self::fetchUserRegistrationTypeNameByUserRegistrationType(userRegistrationType: $userRegistrationType);
        $overviewSessionKeyName = self::fetchOverviewSessionKeyNameByUserRegistrationType(userRegistrationType: $userRegistrationType);

        $urlParameters = $this->fetchMergedUrlParametersFromSession(
            urlParameterObjectClasses: self::URL_PARAMETER_OBJECT_CLASSES,
            sessionKeyName: $overviewSessionKeyName
        );

        return $this->redirectToRoute(
            route: self::OVERVIEW_ROUTE_NAME,
            parameters: array_merge(['userRegistrationTypeName' => $userRegistrationTypeName], $urlParameters)
        );
    }
}
