<?php

declare(strict_types=1);

namespace App\Controller\ExternalUser\UserRegistration;

use App\Domain\Entity\UserRegistration\UserRegistrationType;
use App\Domain\UserRegistration\UserRegistrationDeletionService;
use App\Domain\UserRegistration\UserRegistrationEmailService;
use App\Domain\UserRegistration\UserRegistrationService;
use App\Form\Type\ExternalUser\UserRegistration\UserRegistrationMassOperationActivateType;
use App\Form\Type\ExternalUser\UserRegistration\UserRegistrationMassOperationDeleteType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Security("is_granted('ROLE_ACCOUNT_ADMIN') or is_granted('ROLE_USER')")]
#[Route('/externe-nutzer/registrierungen/massenoperation/{userRegistrationTypeName<suchende|anbietende>}', name: 'external_user_user_registration_mass_operation_')]
class UserRegistrationMassOperationController extends AbstractController
{
    private const ACTIVE_NAV_GROUP = 'external_user';
    private const ACTIVE_NAV_ITEM = self::ACTIVE_NAV_GROUP . '_user_registration';

    public function __construct(
        private readonly UserRegistrationDeletionService $userRegistrationDeletionService,
        private readonly UserRegistrationEmailService $userRegistrationEmailService,
        private readonly UserRegistrationService $userRegistrationService
    ) {
    }

    #[Route('/loeschen', name: 'delete', methods: ['POST'])]
    public function delete(string $userRegistrationTypeName, Request $request, UserInterface $user): Response
    {
        if ($userRegistrationTypeName === 'suchende') {
            $userRegistrationType = UserRegistrationType::SEEKER;
            $pageTitle = 'Nutzermanagement | Suchende löschen';
        } elseif ($userRegistrationTypeName === 'anbietende') {
            $userRegistrationType = UserRegistrationType::PROVIDER;
            $pageTitle = 'Nutzermanagement | Anbietende löschen';
        } else {
            throw new NotFoundHttpException();
        }

        $account = $user->getAccount();

        $userRegistrationMassOperationDeleteForm = $this->createForm(type: UserRegistrationMassOperationDeleteType::class);

        $userRegistrationMassOperationDeleteForm->add(child: 'delete', type: SubmitType::class);

        $userRegistrationMassOperationDeleteForm->handleRequest(request: $request);

        if ($request->get('selectedRowIds') !== null) {
            $userRegistrationMassOperationDeleteForm->get('selectedRowIds')->setData($request->get('selectedRowIds'));
        }

        $userRegistrations = $this->userRegistrationService->fetchUserRegistrationsByIds(
            account: $account,
            ids: json_decode($userRegistrationMassOperationDeleteForm->get('selectedRowIds')->getData()),
            withoutInvitation: false,
            userRegistrationTypes: null
        );

        if (
            $userRegistrationMassOperationDeleteForm->isSubmitted()
            && $userRegistrationMassOperationDeleteForm->isValid()
            && $userRegistrationMassOperationDeleteForm->get('verificationCode')->getData() === 'registrierungen_' . count($userRegistrations)
        ) {
            foreach ($userRegistrations as $userRegistration) {
                $this->userRegistrationDeletionService->deleteUserRegistration(userRegistration: $userRegistration);

                $this->userRegistrationEmailService->sendUserRegistrationDeletedEmail(userRegistration: $userRegistration);
            }

            $this->addFlash(type: 'dataDeleted', message: 'Die Registrierungen wurden abgelehnt und die Accounts sind gelöscht');

            return $this->redirectToRoute(route: 'external_user_user_registration_overview', parameters: ['userRegistrationTypeName' => $userRegistrationTypeName]);
        }

        return $this->render(view: 'external_user/user_registration/mass_operation/delete.html.twig', parameters: [
            'userRegistrationMassOperationDeleteForm' => $userRegistrationMassOperationDeleteForm,
            'userRegistrations'                       => $userRegistrations,
            'userRegistrationType'                    => $userRegistrationType,
            'pageTitle'                               => $pageTitle,
            'activeNavGroup'                          => self::ACTIVE_NAV_GROUP,
            'activeNavItem'                           => self::ACTIVE_NAV_ITEM,
        ]);
    }

    #[Route('/freischalten', name: 'activate', methods: ['POST'])]
    public function activate(string $userRegistrationTypeName, Request $request, UserInterface $user): Response
    {
        if ($userRegistrationTypeName === 'suchende') {
            $userRegistrationType = UserRegistrationType::SEEKER;
            $pageTitle = 'Nutzermanagement | Suchende löschen';
        } elseif ($userRegistrationTypeName === 'anbietende') {
            $userRegistrationType = UserRegistrationType::PROVIDER;
            $pageTitle = 'Nutzermanagement | Anbietende löschen';
        } else {
            throw new NotFoundHttpException();
        }

        $account = $user->getAccount();

        $userRegistrationMassOperationActivateForm = $this->createForm(type: UserRegistrationMassOperationActivateType::class);

        $userRegistrationMassOperationActivateForm->add(child: 'activate', type: SubmitType::class);

        $userRegistrationMassOperationActivateForm->handleRequest(request: $request);

        if ($request->get('selectedRowIds') !== null) {
            $userRegistrationMassOperationActivateForm->get('selectedRowIds')->setData($request->get('selectedRowIds'));
        }

        $userRegistrations = $this->userRegistrationService->fetchUserRegistrationsByIds(
            account: $account,
            ids: json_decode($userRegistrationMassOperationActivateForm->get('selectedRowIds')->getData()),
            withoutInvitation: false,
            userRegistrationTypes: null
        );

        if (
            $userRegistrationMassOperationActivateForm->isSubmitted()
            && $userRegistrationMassOperationActivateForm->isValid()
            && $userRegistrationMassOperationActivateForm->get('verificationCode')->getData() === 'registrierungen_' . count($userRegistrations)
        ) {
            foreach ($userRegistrations as $userRegistration) {
                $this->userRegistrationService->activateUser(userRegistration: $userRegistration, createdByAccountUser: $user);
            }

            $this->addFlash(type: 'dataSaved', message: 'Die Accounts wurden freigeschaltet und die Nutzer:innen darüber informiert.');

            return $this->redirectToRoute(route: 'external_user_user_registration_overview', parameters: ['userRegistrationTypeName' => $userRegistrationTypeName]);
        }

        return $this->render(view: 'external_user/user_registration/mass_operation/activate.html.twig', parameters: [
            'userRegistrationMassOperationActivateForm' => $userRegistrationMassOperationActivateForm,
            'userRegistrations'                         => $userRegistrations,
            'userRegistrationType'                      => $userRegistrationType,
            'pageTitle'                                 => $pageTitle,
            'activeNavGroup'                            => self::ACTIVE_NAV_GROUP,
            'activeNavItem'                             => self::ACTIVE_NAV_ITEM,
        ]);
    }
}
