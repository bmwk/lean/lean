<?php

declare(strict_types=1);

namespace App\Controller;

use App\Domain\File\FileService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Uid\Uuid;

class FileController extends AbstractController
{
    public function __construct(
        private readonly FileService $fileService
    ) {
    }

    #[IsGranted('IS_AUTHENTICATED')]
    #[Route('/file/{fileUuid<[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}>}', name: 'file', methods: ['GET'])]
    public function fileAction(string $fileUuid, UserInterface $user): Response
    {
        $file = $this->fileService->fetchFileByUuidAndAccount(uuid: Uuid::fromString(uuid: $fileUuid), account: $user->getAccount());

        if ($file === null) {
            throw new NotFoundHttpException();
        }

        return new Response(
            content: $this->fileService->fetchFileContentFromFile($file),
            headers: ['Content-Type' => $file->getMimeType()]
        );
    }

    #[IsGranted('IS_AUTHENTICATED')]
    #[Route('/file/{fileUuid<[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}>}/download', name: 'file_download', methods: ['GET'])]
    public function fileDownload(string $fileUuid, UserInterface $user): Response
    {
        $file = $this->fileService->fetchFileByUuidAndAccount(uuid: Uuid::fromString(uuid: $fileUuid), account: $user->getAccount());

        if ($file === null) {
            throw new NotFoundHttpException();
        }

        return new Response(
            content: $this->fileService->fetchFileContentFromFile($file),
            headers: [
                'Content-Disposition' => HeaderUtils::makeDisposition(
                    disposition: HeaderUtils::DISPOSITION_ATTACHMENT,
                    filename: $file->getFileName(),
                    filenameFallback: mb_convert_encoding(string: $file->getFileName(), to_encoding: 'ascii', from_encoding: 'utf8')
                ),
            ]
        );
    }

    #[IsGranted('PUBLIC_ACCESS')]
    #[Route('/public/file/{fileUuid<[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}>}', name: 'public_file', methods: ['GET'])]
    public function publicFile(string $fileUuid): Response
    {
        $file = $this->fileService->fetchPublicFileByUuid(uuid: Uuid::fromString(uuid: $fileUuid));

        if ($file === null) {
            throw new NotFoundHttpException();
        }

        return new Response(
            content: $this->fileService->fetchFileContentFromFile($file),
            headers: ['Content-Type' => $file->getMimeType()]
        );
    }

    #[IsGranted('PUBLIC_ACCESS')]
    #[Route('/public/file/{fileUuid<[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}>}/download', name: 'public_file_download', methods: ['GET'])]
    public function publicFileDownload(string $fileUuid): Response
    {
        $file = $this->fileService->fetchPublicFileByUuid(uuid: Uuid::fromString(uuid: $fileUuid));

        if ($file === null) {
            throw new NotFoundHttpException();
        }

        return new Response(
            content: $this->fileService->fetchFileContentFromFile($file),
            headers: [
                'Content-Disposition' => HeaderUtils::makeDisposition(
                    disposition: HeaderUtils::DISPOSITION_ATTACHMENT,
                    filename: $file->getFileName(),
                    filenameFallback: mb_convert_encoding(string: $file->getFileName(), to_encoding: 'ascii', from_encoding: 'utf8')
                ),
            ]
        );
    }
}
