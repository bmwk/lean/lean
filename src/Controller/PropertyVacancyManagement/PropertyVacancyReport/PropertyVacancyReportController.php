<?php

declare(strict_types=1);

namespace App\Controller\PropertyVacancyManagement\PropertyVacancyReport;

use App\Controller\AbstractLeAnController;
use App\Domain\Entity\DeletionType;
use App\Domain\Entity\Image;
use App\Domain\Entity\Person\Person;
use App\Domain\Entity\Person\PersonType;
use App\Domain\Entity\PlaceType;
use App\Domain\Entity\Property\BuildingUnit;
use App\Domain\Entity\Property\PropertyOwner;
use App\Domain\Entity\Property\PropertyUser;
use App\Domain\Entity\PropertyVacancyReport\PropertyVacancyReportFilter;
use App\Domain\Entity\PropertyVacancyReport\PropertyVacancyReportNote;
use App\Domain\File\FileService;
use App\Domain\Location\LocationService;
use App\Domain\Property\PropertyUserService;
use App\Domain\PropertyVacancyReport\PropertyVacancyReportDeletionService;
use App\Domain\PropertyVacancyReport\PropertyVacancyReportService;
use App\Domain\SessionStoredUrlParameter\SessionStoredUrlParameterService;
use App\Form\Type\PropertyVacancyManagement\PropertyVacancyReport\PropertyVacancyReportDeleteType;
use App\Form\Type\PropertyVacancyManagement\PropertyVacancyReport\PropertyVacancyReportFilterType;
use App\Form\Type\PropertyVacancyManagement\PropertyVacancyReport\PropertyVacancyReportNoteDeleteType;
use App\Form\Type\PropertyVacancyManagement\PropertyVacancyReport\PropertyVacancyReportNoteType;
use App\Form\Type\PropertyVacancyManagement\PropertyVacancyReport\ReportToBuildingUnitType;
use App\SessionStorage\SessionStorageService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\UnitOfWork;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Security("is_granted('ROLE_USER') or is_granted('ROLE_PROPERTY_VACANCY_REPORT_MANAGER') or is_granted('ROLE_VIEWER') or is_granted('ROLE_RESTRICTED_VIEWER')")]
#[Route('/besatz-und-leerstand/leerstandsmeldungen', name: 'property_vacancy_management_property_vacancy_report_')]
class PropertyVacancyReportController extends AbstractLeAnController
{
    private const ACTIVE_NAV_GROUP = 'property_vacancy_management';
    private const ACTIVE_NAV_ITEM = self::ACTIVE_NAV_GROUP . '_property_vacancy_report';

    private const OVERVIEW_SESSION_KEY_NAME = 'propertyVacancyReportOverview';

    public function __construct(
        SessionStorageService $sessionStorageService,
        SessionStoredUrlParameterService $sessionStoredUrlParameterService,
        private readonly PropertyVacancyReportService $propertyVacancyReportService,
        private readonly PropertyVacancyReportDeletionService $propertyVacancyReportDeletionService,
        private readonly PropertyUserService $propertyUserService,
        private readonly FileService $fileService,
        private readonly LocationService $locationService,
        private readonly EntityManagerInterface $entityManager
    ) {
        parent::__construct(
            sessionStorageService: $sessionStorageService,
            sessionStoredUrlParameterService: $sessionStoredUrlParameterService
        );
    }

    #[Route('/neue-meldungen', name: 'unprocessed_overview', methods: ['GET', 'POST'])]
    public function unprocessedOverview(Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $propertyVacancyReportFilterForm = $this->createForm(type: PropertyVacancyReportFilterType::class, options: [
            'dataClassName'  => PropertyVacancyReportFilter::class,
            'sessionKeyName' => self::OVERVIEW_SESSION_KEY_NAME,
        ]);

        $propertyVacancyReportFilterForm->add(child: 'apply', type: SubmitType::class);

        $propertyVacancyReportFilterForm->handleRequest(request: $request);

        $propertyVacancyReports = $this->propertyVacancyReportService->fetchUnprocessedPropertyVacancyReportsByAccount(
            account: $account,
            emailConfirmed: true,
            propertyVacancyReportFilter: $propertyVacancyReportFilterForm->getData()
        );

        return $this->render(view: 'property_vacancy_management/property_vacancy_report/unprocessed_overview.html.twig', parameters: [
            'propertyVacancyReportFilterForm' => $propertyVacancyReportFilterForm,
            'propertyVacancyReports'          => $propertyVacancyReports,
            'pageTitle'                       => 'Neue Leerstandsmeldungen',
            'activeNavGroup'                  => self::ACTIVE_NAV_GROUP,
            'activeNavItem'                   => self::ACTIVE_NAV_ITEM,
        ]);
    }

    #[Route('/bearbeitete-meldungen', name: 'processed_overview', methods: ['GET', 'POST'])]
    public function processedOverview(Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $propertyVacancyReportFilterForm = $this->createForm(type: PropertyVacancyReportFilterType::class, options: [
            'dataClassName'  => PropertyVacancyReportFilter::class,
            'sessionKeyName' => self::OVERVIEW_SESSION_KEY_NAME,
        ]);

        $propertyVacancyReportFilterForm->add(child: 'apply', type: SubmitType::class);

        $propertyVacancyReportFilterForm->handleRequest(request: $request);

        $propertyVacancyReports = $this->propertyVacancyReportService->fetchProcessedPropertyVacancyReportsByAccount(
            account: $account,
            emailConfirmed: true,
            propertyVacancyReportFilter: $propertyVacancyReportFilterForm->getData()
        );

        return $this->render(view: 'property_vacancy_management/property_vacancy_report/processed_overview.html.twig', parameters: [
            'propertyVacancyReportFilterForm' => $propertyVacancyReportFilterForm,
            'propertyVacancyReports'          => $propertyVacancyReports,
            'pageTitle'                       => 'Bearbeitete Leerstandsmeldungen',
            'activeNavGroup'                  => self::ACTIVE_NAV_GROUP,
            'activeNavItem'                   => self::ACTIVE_NAV_ITEM,
        ]);
    }

    #[Route('/{propertyVacancyReportId<\d{1,10}>}', name: 'report', methods: ['GET'])]
    public function report(int $propertyVacancyReportId, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $propertyVacancyReport = $this->propertyVacancyReportService->fetchPropertyVacancyReportByIdAndAccount(id: $propertyVacancyReportId, account: $account);

        if ($propertyVacancyReport === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'view', subject: $propertyVacancyReport);

        return $this->render(view: 'property_vacancy_management/property_vacancy_report/report.html.twig', parameters: [
            'propertyVacancyReport' => $propertyVacancyReport,
            'pageTitle'             => 'Leerstandsmeldung bearbeiten',
            'activeNavGroup'        => self::ACTIVE_NAV_GROUP,
            'activeNavItem'         => self::ACTIVE_NAV_ITEM,
        ]);
    }

    #[Security("is_granted('ROLE_USER') or is_granted('ROLE_PROPERTY_VACANCY_REPORT_MANAGER')")]
    #[Route('/{propertyVacancyReportId<\d{1,10}>}/processed', name: 'processed', methods: ['GET'])]
    public function processed(int $propertyVacancyReportId, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $propertyVacancyReport = $this->propertyVacancyReportService->fetchPropertyVacancyReportByIdAndAccount(id: $propertyVacancyReportId, account: $account);

        if ($propertyVacancyReport === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'edit', subject: $propertyVacancyReport);

        $propertyVacancyReport
            ->setProcessed(true)
            ->setProcessedAt(new \DateTimeImmutable());

        $this->entityManager->flush();

        $this->addFlash(type: 'dataSaved', message: 'Die Meldung wurde als bearbeitet markiert.');

        return $this->redirectToRoute(route: 'property_vacancy_management_property_vacancy_report_processed_overview');
    }

    #[Security("is_granted('ROLE_USER') or is_granted('ROLE_PROPERTY_VACANCY_REPORT_MANAGER')")]
    #[Route('/{propertyVacancyReportId<\d{1,10}>}/loeschen', name: 'delete', methods: ['GET', 'POST'])]
    public function delete(int $propertyVacancyReportId, Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $propertyVacancyReport = $this->propertyVacancyReportService->fetchPropertyVacancyReportByIdAndAccount(id: $propertyVacancyReportId, account: $account);

        if ($propertyVacancyReport === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'delete', subject: $propertyVacancyReport);

        $propertyVacancyReportDeleteForm = $this->createForm(type: PropertyVacancyReportDeleteType::class);

        $propertyVacancyReportDeleteForm->add(child:'delete', type: SubmitType::class);

        $propertyVacancyReportDeleteForm->handleRequest(request: $request);

        if ($propertyVacancyReportDeleteForm->isSubmitted() && $propertyVacancyReportDeleteForm->isValid()) {
            if ($propertyVacancyReport->isProcessed()) {
                $route = 'property_vacancy_management_property_vacancy_report_processed_overview';
            } else {
                $route = 'property_vacancy_management_property_vacancy_report_unprocessed_overview';
            }

            $formData = $propertyVacancyReportDeleteForm->getData();

            if ($formData['verificationCode'] === 'meldung_' . $propertyVacancyReport->getId()) {
                $this->propertyVacancyReportDeletionService->deletePropertyVacancyReport(
                    propertyVacancyReport: $propertyVacancyReport,
                    deletionType: DeletionType::SOFT,
                    deletedByAccountUser: $user
                );

                $this->addFlash(type: 'dataDeleted', message: 'Die Meldung wurde gelöscht.');

                return $this->redirectToRoute(route: $route);
            } else {
                $this->addFlash(type: 'dataError', message: 'Der Verifizierungscode war falsch.');
            }
        }

        return $this->render(view: 'property_vacancy_management/property_vacancy_report/delete.html.twig', parameters: [
            'propertyVacancyReportDeleteForm' => $propertyVacancyReportDeleteForm,
            'propertyVacancyReport'           => $propertyVacancyReport,
            'pageTitle'                       => 'Leerstandsmeldung löschen',
            'activeNavGroup'                  => self::ACTIVE_NAV_GROUP,
            'activeNavItem'                   => self::ACTIVE_NAV_ITEM,
        ]);
    }

    #[Security("is_granted('ROLE_USER') or is_granted('ROLE_PROPERTY_VACANCY_REPORT_MANAGER')")]
    #[Route('/{propertyVacancyReportId<\d{1,10}>}/notiz/eintragen', name: 'note_create', methods: ['GET', 'POST'])]
    public function noteCreate(int $propertyVacancyReportId, Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $propertyVacancyReport = $this->propertyVacancyReportService->fetchPropertyVacancyReportByIdAndAccount(id: $propertyVacancyReportId, account: $account);

        if ($propertyVacancyReport === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'edit', subject: $propertyVacancyReport);

        $propertyVacancyReportNote = new PropertyVacancyReportNote();

        $propertyVacancyReportNoteForm = $this->createForm(type: PropertyVacancyReportNoteType::class, data: $propertyVacancyReportNote);

        $propertyVacancyReportNoteForm->add(child:'save', type: SubmitType::class);

        $propertyVacancyReportNoteForm->handleRequest(request: $request);

        if ($propertyVacancyReportNoteForm->isSubmitted() && $propertyVacancyReportNoteForm->isValid()) {
            $propertyVacancyReportNote
                ->setPropertyVacancyReport($propertyVacancyReport)
                ->setCreatedByAccountUser($user);

            $this->entityManager->persist(entity: $propertyVacancyReportNote);

            $this->entityManager->flush();

            $this->addFlash(type: 'dataSaved', message: 'Die Notiz wurde eingetragen.');

            return $this->redirectToRoute(route: 'property_vacancy_management_property_vacancy_report_report', parameters: ['propertyVacancyReportId' => $propertyVacancyReport->getId()]);
        }

        return $this->render(view: 'property_vacancy_management/property_vacancy_report/note_create.html.twig', parameters: [
            'propertyVacancyReportNoteForm' => $propertyVacancyReportNoteForm,
            'propertyVacancyReport'         => $propertyVacancyReport,
            'pageTitle'                     => 'Notiz zur Leerstandsmeldung eintragen',
            'activeNavGroup'                => self::ACTIVE_NAV_GROUP,
            'activeNavItem'                 => self::ACTIVE_NAV_ITEM,
        ]);
    }

    #[Route('/{propertyVacancyReportId<\d{1,10}>}/notizen/{propertyVacancyReportNoteId<\d{1,10}>}', name: 'note_edit', methods: ['GET', 'POST'])]
    public function noteEdit(int $propertyVacancyReportId, int $propertyVacancyReportNoteId, Request $request, UserInterface $user): Response
    {
        $propertyVacancyReport = $this->propertyVacancyReportService->fetchPropertyVacancyReportByIdAndAccount(
            id: $propertyVacancyReportId,
            account: $user->getAccount()
        );

        if ($propertyVacancyReport === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'view', subject: $propertyVacancyReport);

        $propertyVacancyReportNote = $this->propertyVacancyReportService->fetchPropertyVacancyReportNoteById(id: $propertyVacancyReportNoteId);

        if ($propertyVacancyReportNote === null) {
            throw new NotFoundHttpException();
        }

        if ($propertyVacancyReport->getPropertyVacancyReportNotes()->contains(element: $propertyVacancyReportNote) === false) {
            throw new NotFoundHttpException();
        }

        $propertyVacancyReportNoteForm = $this->createForm(type: PropertyVacancyReportNoteType::class, data: $propertyVacancyReportNote);

        $propertyVacancyReportNoteForm->add(child:'save', type: SubmitType::class);

        $propertyVacancyReportNoteForm->handleRequest(request: $request);

        if ($propertyVacancyReportNoteForm->isSubmitted() && $propertyVacancyReportNoteForm->isValid()) {
            $this->denyAccessUnlessGranted(attribute: 'edit', subject: $propertyVacancyReport);

            $this->entityManager->flush();

            $this->addFlash(type: 'dataSaved', message: 'Die Notiz wurde bearbeitet.');

            return $this->redirectToRoute(route: 'property_vacancy_management_property_vacancy_report_report', parameters: ['propertyVacancyReportId' => $propertyVacancyReport->getId()]);
        }

        return $this->render(view: 'property_vacancy_management/property_vacancy_report/note_edit.html.twig', parameters: [
            'propertyVacancyReportNoteForm' => $propertyVacancyReportNoteForm,
            'propertyVacancyReportNote'     => $propertyVacancyReportNote,
            'propertyVacancyReport'         => $propertyVacancyReport,
            'pageTitle'                     => 'Notiz zur Leerstandsmeldung bearbeiten',
            'activeNavGroup'                => self::ACTIVE_NAV_GROUP,
            'activeNavItem'                 => self::ACTIVE_NAV_ITEM,
        ]);
    }

    #[Route('/{propertyVacancyReportId<\d{1,10}>}/notizen/{propertyVacancyReportNoteId<\d{1,10}>}/loeschen', name: 'note_delete', methods: ['GET', 'POST'])]
    public function noteDelete(int $propertyVacancyReportId, int $propertyVacancyReportNoteId, Request $request, UserInterface $user): Response
    {
        $propertyVacancyReport = $this->propertyVacancyReportService->fetchPropertyVacancyReportByIdAndAccount(
            id: $propertyVacancyReportId,
            account: $user->getAccount()
        );

        if ($propertyVacancyReport === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'edit', subject: $propertyVacancyReport);

        $propertyVacancyReportNote = $this->propertyVacancyReportService->fetchPropertyVacancyReportNoteById(id: $propertyVacancyReportNoteId);

        if ($propertyVacancyReportNote === null) {
            throw new NotFoundHttpException();
        }

        if ($propertyVacancyReport->getPropertyVacancyReportNotes()->contains(element: $propertyVacancyReportNote) === false) {
            throw new NotFoundHttpException();
        }

        $propertyVacancyReportNoteDeleteForm = $this->createForm(type: PropertyVacancyReportNoteDeleteType::class);

        $propertyVacancyReportNoteDeleteForm->add(child:'delete', type: SubmitType::class);

        $propertyVacancyReportNoteDeleteForm->handleRequest(request: $request);

        if ($propertyVacancyReportNoteDeleteForm->isSubmitted() && $propertyVacancyReportNoteDeleteForm->isValid()) {
            $formData = $propertyVacancyReportNoteDeleteForm->getData();

            if ($formData['verificationCode'] === 'notiz_' . $propertyVacancyReportNote->getId()) {
                $this->propertyVacancyReportDeletionService->deletePropertyVacancyReportNote(
                    propertyVacancyReportNote: $propertyVacancyReportNote
                );

                $this->addFlash(type: 'dataDeleted', message: 'Die Notiz wurde gelöscht.');

                return $this->redirectToRoute(route: 'property_vacancy_management_property_vacancy_report_report', parameters: ['propertyVacancyReportId' => $propertyVacancyReport->getId()]);
            } else {
                $this->addFlash(type: 'dataError', message: 'Der Verifizierungscode war falsch.');
            }
        }

        $propertyVacancyReportNoteForm = $this->createForm(type: PropertyVacancyReportNoteType::class, data: $propertyVacancyReportNote);

        return $this->render(view: 'property_vacancy_management/property_vacancy_report/note_delete.html.twig', parameters: [
            'propertyVacancyReportNoteDeleteForm' => $propertyVacancyReportNoteDeleteForm,
            'propertyVacancyReportNoteForm'       => $propertyVacancyReportNoteForm,
            'propertyVacancyReportNote'           => $propertyVacancyReportNote,
            'propertyVacancyReport'               => $propertyVacancyReport,
            'pageTitle'                           => 'Notiz löschen',
            'activeNavGroup'                      => self::ACTIVE_NAV_GROUP,
            'activeNavItem'                       => self::ACTIVE_NAV_ITEM,
        ]);
    }

    #[Security("is_granted('ROLE_USER') or is_granted('ROLE_PROPERTY_VACANCY_REPORT_MANAGER')")]
    #[Route('/{propertyVacancyReportId<\d{1,10}>}/uebernehmen', name: 'report_to_building_unit', methods: ['GET', 'POST'])]
    public function reportToBuildingUnit(int $propertyVacancyReportId, Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $propertyVacancyReport = $this->propertyVacancyReportService->fetchPropertyVacancyReportByIdAndAccount(id: $propertyVacancyReportId, account: $account);

        if ($propertyVacancyReport === null) {
            throw new NotFoundHttpException();
        }

        if ($propertyVacancyReport->getBuildingUnit() !== null) {
            return $this->redirectToRoute(route: 'property_vacancy_management_property_vacancy_report_processed_overview');
        }

        $this->denyAccessUnlessGranted(attribute: 'edit', subject: $propertyVacancyReport);

        $propertyOwner = new PropertyOwner();
        $propertyUser = new PropertyUser();

        $propertyOwnerPerson = null;
        $propertyUserPerson = null;

        if ($propertyVacancyReport->isReporterAlsoPropertyOwner() === true) {
            $propertyOwnerPerson = Person::createFromPropertyVacencyReport(propertyVacancyReport: $propertyVacancyReport);
        }

        if ($propertyVacancyReport->getPropertyUserCompanyName() !== null && $propertyVacancyReport->getPropertyUserCompanyName() !== '') {
            $propertyUserPerson = new Person();

            $propertyUserPerson->setName($propertyVacancyReport->getPropertyUserCompanyName());
        }

        $buildingUnit = BuildingUnit::createFromPropertyVacancyReport(propertyVacancyReport: $propertyVacancyReport);

        $buildingUnit
            ->setAccount($account)
            ->setCreatedByAccountUser($user);

        if ($account->getAssignedPlace()->getPlaceType() === PlaceType::COUNTY) {
            $places = $account->getAssignedPlace()->getChildrenPlaces();
        } else {
            $place = $this->locationService->fetchPlaceByIdAndChildPlacesByPlaceTypes(
                placeId: $account->getAssignedPlace()->getId(),
                placeTypes: [PlaceType::CITY_QUARTER, PlaceType::POSTAL_CODE]
            );

            $buildingUnit->getAddress()->setPlace($place);
            $buildingUnit->getBuilding()->getAddress()->setPlace($place);

            $places = [$place];
        }

        $reportToBuildingUnitForm = $this->createForm(type: ReportToBuildingUnitType::class, data: $buildingUnit, options: [
            'places'  => $places,
            'account' => $account,
            'canEdit' => true,
        ]);

        $reportToBuildingUnitForm->get('propertyOwner')->setData($propertyOwner);
        $reportToBuildingUnitForm->get('propertyUser')->setData($propertyUser);

        if ($propertyOwnerPerson !== null) {
            $reportToBuildingUnitForm->get('propertyOwnerCreate')->setData('true');
            $reportToBuildingUnitForm->get('propertyOwner')->get('personSelectOrPersonCreate')->get('selectOrCreate')->setData('create');
            $reportToBuildingUnitForm->get('propertyOwner')->get('personSelectOrPersonCreate')->get('personCreate')->get('selectedPersonType')->setData($propertyOwnerPerson->getPersonType()->value);
            if ($propertyOwnerPerson->getPersonType() == PersonType::COMPANY) {
                $reportToBuildingUnitForm->get('propertyOwner')->get('personSelectOrPersonCreate')->get('personCreate')->get('company')->setData($propertyOwnerPerson);
            } else {
                $reportToBuildingUnitForm->get('propertyOwner')->get('personSelectOrPersonCreate')->get('personCreate')->get('naturalPerson')->setData($propertyOwnerPerson);
            }
        }

        if ($propertyUserPerson !== null) {
            $reportToBuildingUnitForm->get('propertyUserCreate')->setData('true');
            $reportToBuildingUnitForm->get('propertyUser')->get('personSelectOrPersonCreate')->get('selectOrCreate')->setData('create');
            $reportToBuildingUnitForm->get('propertyUser')->get('personSelectOrPersonCreate')->get('personCreate')->get('selectedPersonType')->setData(PersonType::COMPANY->value);
            $reportToBuildingUnitForm->get('propertyUser')->get('personSelectOrPersonCreate')->get('personCreate')->get('company')->setData($propertyUserPerson);
        }

        $reportToBuildingUnitForm->add(child:'save', type: SubmitType::class);

        $reportToBuildingUnitForm->handleRequest(request: $request);

        if ($reportToBuildingUnitForm->isSubmitted() && $reportToBuildingUnitForm->isValid()) {
            $this->entityManager->beginTransaction();

            $buildingUnit->getBuilding()
                ->setAccount($account)
                ->setCreatedByAccountUser($user);

            $this->entityManager->persist(entity: $buildingUnit->getBuilding()->getAddress());
            $this->entityManager->persist(entity: $buildingUnit->getBuilding()->getPropertySpacesData());
            $this->entityManager->persist(entity: $buildingUnit->getBuilding());
            $this->entityManager->persist(entity: $buildingUnit->getAddress());
            $this->entityManager->persist(entity: $buildingUnit->getPropertySpacesData());
            $this->entityManager->persist(entity: $buildingUnit->getPropertyMarketingInformation());

            if ($buildingUnit->getPropertyMarketingInformation()->getPropertyPricingInformation() !== null) {
                $this->entityManager->persist(entity: $buildingUnit->getPropertyMarketingInformation()->getPropertyPricingInformation());
            }

            foreach ($propertyVacancyReport->getPropertyVacancyReportImages() as $propertyVacancyReportImage) {
                $image = new Image();

                $imageFile = $this->fileService->copyFile(
                    file: $propertyVacancyReportImage->getFile(),
                    public: false,
                    accountUser: $user
                );

                $image->setFile($imageFile);

                if ($propertyVacancyReportImage->getThumbnailFile() !== null) {
                    $imageThumbnailFile = $this->fileService->copyFile(
                        file: $propertyVacancyReportImage->getThumbnailFile(),
                        public: false,
                        accountUser: $user
                    );

                    $image->setThumbnailFile($imageThumbnailFile);
                }

                $image
                    ->setPublic(false)
                    ->setAccount($account)
                    ->setCreatedByAccountUser($user);

                $this->entityManager->persist(entity: $image);

                $buildingUnit->getImages()->add($image);
            }

            if ($buildingUnit->getImages()->isEmpty() === false) {
                $buildingUnit->setMainImage($buildingUnit->getImages()->first());
            }

            $this->entityManager->persist(entity: $buildingUnit);

            if ($reportToBuildingUnitForm->get('propertyOwnerCreate')->getData() === 'true') {
                $propertyOwner = $reportToBuildingUnitForm->get('propertyOwner')->getData();

                $propertyOwner
                    ->setAccount($account)
                    ->setCreatedByAccountUser($user);

                $unitOfWork = $this->entityManager->getUnitOfWork();

                if ($unitOfWork->getEntityState($propertyOwner->getPerson()) === UnitOfWork::STATE_NEW) {
                    $propertyOwner->getPerson()
                        ->setAccount($account)
                        ->setCreatedByAccountUser($user)
                        ->setDeleted(false)
                        ->setAnonymized(false);

                    $this->entityManager->persist(entity: $propertyOwner->getPerson());

                    foreach ($propertyOwner->getPerson()->getContacts() as $contact) {
                        $contact
                            ->setAccount($account)
                            ->setCreatedByAccountUser($user)
                            ->setDeleted(false)
                            ->setAnonymized(false);

                        $this->entityManager->persist(entity: $contact);
                    }
                }

                $this->entityManager->persist(entity: $propertyOwner);

                $buildingUnit->getPropertyOwners()->add($propertyOwner);
            }

            if ($reportToBuildingUnitForm->get('propertyUserCreate')->getData() === 'true') {
                try {
                    $propertyUser = $this->propertyUserService->createPropertyUserFromPropertyUserForm(
                        propertyUserForm: $reportToBuildingUnitForm->get('propertyUser')
                    );
                } catch (\RuntimeException $exception) {
                    throw new BadRequestException();
                }

                $propertyUser
                    ->setAccount($account)
                    ->setCreatedByAccountUser($user);

                $unitOfWork = $this->entityManager->getUnitOfWork();

                if ($unitOfWork->getEntityState($propertyUser->getPerson()) === UnitOfWork::STATE_NEW) {
                    $propertyUser->getPerson()
                        ->setAccount($account)
                        ->setCreatedByAccountUser($user)
                        ->setDeleted(false)
                        ->setAnonymized(false);

                    $this->entityManager->persist($propertyUser->getPerson());
                }

                $this->entityManager->persist(entity: $propertyUser);

                $buildingUnit->getPropertyUsers()->add($propertyUser);
            }

            $propertyVacancyReport
                ->setBuildingUnit($buildingUnit)
                ->setProcessed(true)
                ->setProcessedAt(new \DateTimeImmutable());

            $this->entityManager->flush();
            $this->entityManager->commit();

            $this->addFlash(type: 'dataSaved', message: 'Die Meldung wurde als Objekt angelegt.');

            return $this->redirectToRoute(route: 'property_vacancy_management_property_vacancy_report_processed_overview');
        }

        return $this->render(view: 'property_vacancy_management/property_vacancy_report/report_to_building_unit.html.twig', parameters: [
            'reportToBuildingUnitForm' => $reportToBuildingUnitForm,
            'propertyVacancyReport'    => $propertyVacancyReport,
            'pageTitle'                => 'Leerstandsmeldung überführen',
            'activeNavGroup'           => self::ACTIVE_NAV_GROUP,
            'activeNavItem'            => self::ACTIVE_NAV_ITEM,
        ]);
    }
}
