<?php

declare(strict_types=1);

namespace App\Controller\PropertyVacancyManagement\PropertyExcelImport;

use App\Controller\AbstractLeAnController;
use App\Domain\Entity\PaginationParameter;
use App\Domain\Entity\SortingOption\SortingOption;
use App\Domain\Property\ExcelImport\ExcelImportColIndex;
use App\Domain\Property\ExcelImport\ExcelImportService;
use App\Domain\SessionStoredUrlParameter\SessionStoredUrlParameterService;
use App\Form\Type\PropertyVacancyManagement\PropertyExcelImportType;
use App\SessionStorage\SessionStorageService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Security("is_granted('ROLE_ACCOUNT_ADMIN') or is_granted('ROLE_PROPERTY_EXCEL_IMPORT_MANAGER')")]
#[Route('/besatz-und-leerstand/excel-import', name: 'property_vacancy_management_property_excel_import_')]
class PropertyExcelImportController extends AbstractLeAnController
{
    private const ACTIVE_NAV_GROUP = 'property_vacancy_management';
    private const ACTIVE_NAV_ITEM = self::ACTIVE_NAV_GROUP . '_property_excel_import';

    private const URL_PARAMETER_OBJECT_CLASSES = [
        PaginationParameter::class,
        SortingOption::class,
    ];

    private const OVERVIEW_ROUTE_NAME = 'property_vacancy_management_property_excel_import_overview';

    private const OVERVIEW_SESSION_KEY_NAME = 'propertyVacancyManagementPropertyExcelImportOverview';

    public function __construct(
       SessionStorageService $sessionStorageService,
       SessionStoredUrlParameterService $sessionStoredUrlParameterService,
       private readonly ExcelImportService $excelImportService,
       private readonly string $excelImportTemplateXlsxFilePath
    ) {
        parent::__construct(
            sessionStorageService: $sessionStorageService,
            sessionStoredUrlParameterService: $sessionStoredUrlParameterService
        );
    }

    #[Route('/uebersicht', name: 'overview', methods: ['GET', 'POST'])]
    public function overview(Request $request, UserInterface $user): Response
    {
        $paginationParameter = PaginationParameter::createFromRequest(request: $request);

        $this->storeObjectInSession(object: $paginationParameter, sessionKeyName: self::OVERVIEW_SESSION_KEY_NAME);

        $sortingOption = null;

        if ($request->query->get(key: 'sortieren') !== null) {
            $sortingOption = SortingOption::createFromRequest(request: $request);

            $this->storeObjectInSession(object: $sortingOption, sessionKeyName: self::OVERVIEW_SESSION_KEY_NAME);
        }

        $propertyExcelImportForm = $this->createForm(type: PropertyExcelImportType::class);

        $propertyExcelImportForm->add(child:'upload', type: SubmitType::class);

        $propertyExcelImportForm->handleRequest(request: $request);

        if ($propertyExcelImportForm->isSubmitted() && $propertyExcelImportForm->isValid()) {
            $excelFile = $propertyExcelImportForm->get('excelFile')->getData();

            $propertyExcelImport = $this->excelImportService->createAndSavePropertyExcelImportFromUploadedFile(
                uploadedFile: $excelFile,
                accountUser: $user
            );

            $this->excelImportService->createAndSaveProcessPropertyExcelImportTask(propertyExcelImport: $propertyExcelImport, accountUser: $user);

            $this->addFlash(
                type: 'dataSaved',
                message: 'Die Datei wurde hochgeladen. Die Verarbeitung erfolgt im Hintergrund, bitte rufen Sie diese Seite in einigen Minuten erneut auf, um den Fortschritt einzusehen.'
            );

            return $this->redirectToRoute(route: 'property_vacancy_management_property_excel_import_overview');
        }

        $propertyExcelImports = $this->excelImportService->fetchPropertyExcelImportsPaginatedByAccount(
            account: $user->getAccount(),
            firstResult: $paginationParameter->getFirstResult(),
            maxResults: $paginationParameter->getLimit(),
            sortingOption: $sortingOption
        );

        return $this->render(view: 'property_vacancy_management/property_excel_import/overview.html.twig', parameters: [
            'propertyExcelImportForm' => $propertyExcelImportForm,
            'propertyExcelImports'    => $propertyExcelImports,
            'excelHeaderCols'         => ExcelImportColIndex::getHeaderColumns(),
            'pageTitle'               => 'Leerstandsmanagement - Excel-Import',
            'activeNavGroup'          => self::ACTIVE_NAV_GROUP,
            'activeNavItem'           => self::ACTIVE_NAV_ITEM,
        ]);
    }

    #[Route('/vorlage/download', name: 'template_download', methods: ['GET'])]
    public function templateDownload(): BinaryFileResponse
    {
        $fileName = (new \SplFileInfo(filename: $this->excelImportTemplateXlsxFilePath))->getFilename();

        return new BinaryFileResponse(
            file: $this->excelImportTemplateXlsxFilePath,
            headers: [
                'Content-Disposition' => HeaderUtils::makeDisposition(
                    disposition: HeaderUtils::DISPOSITION_ATTACHMENT,
                    filename: $fileName,
                    filenameFallback: mb_convert_encoding(string: $fileName, to_encoding: 'ascii', from_encoding: 'utf8')
                ),
            ]
        );
    }
}
