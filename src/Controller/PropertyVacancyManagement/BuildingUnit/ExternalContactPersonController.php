<?php

declare(strict_types=1);

namespace App\Controller\PropertyVacancyManagement\BuildingUnit;

use App\Domain\Entity\Property\ExternalContactPerson;
use App\Domain\Property\BuildingUnitService;
use App\Form\Type\PropertyVacancyManagement\Property\BuildingUnitExternalContactPersonType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Security("is_granted('ROLE_USER') or is_granted('ROLE_PROPERTY_FEEDER') or is_granted('ROLE_VIEWER') or is_granted('ROLE_PROPERTY_PROVIDER')")]
#[Route('/besatz-und-leerstand/objekte/{buildingUnitId<\d{1,10}>}', name: 'property_vacancy_management_building_unit_')]
class ExternalContactPersonController extends AbstractController
{
    private const ACTIVE_NAV_GROUP = 'property_vacancy_management';
    private const ACTIVE_NAV_ITEM = self::ACTIVE_NAV_GROUP . '_building_unit';

    public function __construct(
        private readonly BuildingUnitService $buildingUnitService,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    #[Route('/ansprechperson', name: 'external_contact_person', methods: ['GET', 'POST'])]
    public function externalContactPerson(int $buildingUnitId, Request $request, UserInterface $user): Response
    {
        $buildingUnit = $this->buildingUnitService->fetchBuildingUnitById(
            account: $user->getAccount(),
            withFromSubAccounts: true,
            id: $buildingUnitId
        );

        if ($buildingUnit === null) {
            throw new NotFoundHttpException();
        }

        if ($buildingUnit->getAccount()->getParentAccount() === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'view', subject:  $buildingUnit);

        $externalContactPerson = $buildingUnit->getExternalContactPerson();

        if ($externalContactPerson === null) {
            $externalContactPerson = new ExternalContactPerson();

            $externalContactPerson->setBuildingUnit($buildingUnit);

            $this->entityManager->persist(entity: $externalContactPerson);

            $this->entityManager->flush();
        }

        $this->denyAccessUnlessGranted(attribute: 'view', subject:  $externalContactPerson);

        $buildingUnitExternalContactPersonForm = $this->createForm(
            type: BuildingUnitExternalContactPersonType::class,
            data: $externalContactPerson,
            options: [
                'canEdit' => $this->isGranted(attribute: 'edit', subject:  $buildingUnit),
            ]
        );

        $buildingUnitExternalContactPersonForm->add(child:'save', type: SubmitType::class);

        $buildingUnitExternalContactPersonForm->handleRequest(request: $request);

        if ($buildingUnitExternalContactPersonForm->isSubmitted() && $buildingUnitExternalContactPersonForm->isValid()) {
            $this->denyAccessUnlessGranted(attribute: 'edit', subject:  $buildingUnit);
            $this->denyAccessUnlessGranted(attribute: 'edit', subject:  $externalContactPerson);

            $this->entityManager->flush();

            $this->addFlash(type: 'dataSaved', message: 'Die Ansprechperson des Objektes wurde gespeichert.');

            return $this->redirectToRoute(route: 'property_vacancy_management_building_unit_external_contact_person', parameters: ['buildingUnitId' => $buildingUnit->getId()]);
        }

        return $this->render(view: 'property_vacancy_management/building_unit/external_contact_person.html.twig', parameters: [
            'buildingUnitExternalContactPersonForm' => $buildingUnitExternalContactPersonForm,
            'buildingUnit'                          => $buildingUnit,
            'pageTitle'                             => 'Leerstandsmanagement - Objektinformationen - Ansprechpersonen',
            'activeNavGroup'                        => self::ACTIVE_NAV_GROUP,
            'activeNavItem'                         => self::ACTIVE_NAV_ITEM . ($buildingUnit->isArchived() ? '_archive' : ''),
        ]);
    }
}
