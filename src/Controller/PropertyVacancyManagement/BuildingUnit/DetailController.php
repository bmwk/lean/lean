<?php

declare(strict_types=1);

namespace App\Controller\PropertyVacancyManagement\BuildingUnit;

use App\Domain\BusinessLocationArea\BusinessLocationAreaService;
use App\Domain\Entity\Property\EnergyCertificate;
use App\Domain\Entity\Property\EnergyCertificateType;
use App\Domain\Property\BuildingUnitService;
use App\Form\Type\PropertyVacancyManagement\Property\BuildingUnitDetailType;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\UnitOfWork;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Security("is_granted('ROLE_USER') or is_granted('ROLE_PROPERTY_FEEDER') or is_granted('ROLE_VIEWER') or is_granted('ROLE_RESTRICTED_VIEWER') or is_granted('ROLE_PROPERTY_PROVIDER')")]
#[Route('/besatz-und-leerstand/objekte/{buildingUnitId<\d{1,10}>}', name: 'property_vacancy_management_building_unit_')]
class DetailController extends AbstractController
{
    private const ACTIVE_NAV_GROUP = 'property_vacancy_management';
    private const ACTIVE_NAV_ITEM = self::ACTIVE_NAV_GROUP . '_building_unit';

    public function __construct(
        private readonly BuildingUnitService $buildingUnitService,
        private readonly BusinessLocationAreaService $businessLocationAreaService,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    #[Route('/details', name: 'detail', methods: ['GET', 'POST'])]
    public function detail(int $buildingUnitId, Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $buildingUnit = $this->buildingUnitService->fetchBuildingUnitById(account: $account, withFromSubAccounts: true, id: $buildingUnitId);

        if ($buildingUnit === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'view', subject:  $buildingUnit);

        $buildingUnitDetailForm = $this->createForm(type: BuildingUnitDetailType::class, data: $buildingUnit, options: [
            'canEdit' => $this->isGranted(attribute: 'edit', subject:  $buildingUnit),
        ]);

        $buildingUnitDetailForm->add(child:'save', type: SubmitType::class);

        $energyCertificate = $buildingUnit->getEnergyCertificate();

        if ($energyCertificate === null) {
            $energyCertificate = new EnergyCertificate();

            $energyCertificate->setEnergyCertificateType(EnergyCertificateType::ENERGY_REQUIREMENT_CERTIFICATE);
            $energyCertificate->setWithHotWater(false);

            $buildingUnitDetailForm->get('energyCertificate')->setData($energyCertificate);
        }

        if ($buildingUnit->getEnergyCertificate() !== null) {
            $buildingUnitDetailForm->get('energyCertificateIsAvailable')->setData('1');
        }

        $buildingUnitDetailForm->handleRequest(request: $request);

        if ($buildingUnitDetailForm->isSubmitted() && $buildingUnitDetailForm->isValid()) {
            $this->denyAccessUnlessGranted(attribute: 'edit', subject: $buildingUnit);

            $unitOfWork = $this->entityManager->getUnitOfWork();

            $this->entityManager->beginTransaction();

            $energyCertificate = $buildingUnitDetailForm->get('energyCertificate')->getData();

            if ($buildingUnitDetailForm->get('energyCertificateIsAvailable')->getData() === '1') {
                if ($unitOfWork->getEntityState($energyCertificate) === UnitOfWork::STATE_NEW) {
                    $this->entityManager->persist(entity: $energyCertificate);
                }
            } else {
                if ($unitOfWork->getEntityState($energyCertificate) !== UnitOfWork::STATE_NEW) {
                    $this->entityManager->remove(entity: $energyCertificate);
                }

                $buildingUnit->setEnergyCertificate(null);
            }

            $propertySpacesData = $buildingUnitDetailForm->get('propertySpacesData')->getData();

            if ($unitOfWork->getEntityState($propertySpacesData) === UnitOfWork::STATE_NEW) {
                $this->entityManager->persist(entity: $propertySpacesData);
            }

            $this->entityManager->flush();
            $this->entityManager->commit();

            $this->addFlash(type: 'dataSaved', message: 'Die Objektdetails wurden gespeichert.');

            return $this->redirectToRoute(route: 'property_vacancy_management_building_unit_detail', parameters: ['buildingUnitId' => $buildingUnit->getId()]);
        }

        $calculatedLocationCategory = null;

        if ($account === $buildingUnit->getAccount()) {
            if ($buildingUnit->getBuilding()->getLocationCategory() === null && $buildingUnit->getGeolocationPoint() !== null) {
                $calculatedLocationCategory = $this->businessLocationAreaService->calculateLocationCategoryByBusinessLocationArea(
                    account: $account,
                    geolocationPoint: $buildingUnit->getGeolocationPoint()
                );
            }
        }

        return $this->render(view: 'property_vacancy_management/building_unit/detail.html.twig', parameters: [
            'buildingUnitDetailForm'     => $buildingUnitDetailForm,
            'buildingUnit'               => $buildingUnit,
            'calculatedLocationCategory' => $calculatedLocationCategory,
            'pageTitle'                  => 'Leerstandsmanagement - Objektinformationen - Objektdetails',
            'activeNavGroup'             => self::ACTIVE_NAV_GROUP,
            'activeNavItem'              => self::ACTIVE_NAV_ITEM . ($buildingUnit->isArchived() ? '_archive' : ''),
        ]);
    }
}
