<?php

declare(strict_types=1);

namespace App\Controller\PropertyVacancyManagement\BuildingUnit;

use App\Domain\Account\AccountService;
use App\Domain\Account\AccountUserService;
use App\Domain\Entity\Property\BuildingUnitExpose;
use App\Domain\OneTimeToken\OneTimeTokenService;
use App\Domain\Property\BuildingUnitService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Profiler\Profiler;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Uid\Uuid;

#[IsGranted('PUBLIC_ACCESS')]
#[Route('/besatz-und-leerstand/pdf-expose/{token<[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}>}', name: 'property_vacancy_management_building_unit_pdf_expose_')]
class PdfExposeController extends AbstractController
{
    public function __construct(
        private readonly OneTimeTokenService $oneTimeTokenService,
        private readonly BuildingUnitService $buildingUnitService,
        private readonly AccountService $accountService,
        private readonly AccountUserService $accountUserService,
        private readonly SerializerInterface $serializer
    ) {
    }

    #[Route('/content', name: 'content', methods: ['GET'])]
    public function content(string $token, ?Profiler $profiler): Response
    {
        $profiler?->disable();

        $oneTimeToken = $this->oneTimeTokenService->fetchAndDeleteOneTimeToken(token: Uuid::fromString(uuid: $token));

        if ($oneTimeToken === null) {
            throw new NotFoundHttpException();
        }

        $oneTimeTokenData = $oneTimeToken->getJsonData();

        $buildingUnitId = $oneTimeTokenData['buildingUnitId'];
        $accountUserId = $oneTimeTokenData['accountUserId'];

        $buildingUnit = $this->buildingUnitService->fetchBuildingUnitByIdWithoutAccount(id: $buildingUnitId);

        if ($buildingUnit === null) {
            throw new NotFoundHttpException();
        }

        $accountUser = $this->accountUserService->fetchAccountUserByIdFromAllAccounts(id: $accountUserId);

        if ($accountUser === null) {
            throw new NotFoundHttpException();
        }

        if ($accountUser->getAccount()->getParentAccount() !== null) {
            $account = $accountUser->getAccount()->getParentAccount();
        } else {
            $account = $accountUser->getAccount();
        }

        $accountLegalTextsConfiguration = $this->accountService->fetchAccountLegalTextsConfigurationByAccount(account: $account);

        $attributes = [
            'id',
            'name',
            'currentUsageIndustryClassification' => [
                'name',
                'levelOne',
                'levelTwo',
                'levelThree',
            ],
            'pastUsageIndustryClassification' => [
                'name',
                'levelOne',
                'levelTwo',
                'levelThree',
            ],
            'futureUsageIndustryClassification' => [
                'name',
                'levelOne',
                'levelTwo',
                'levelThree',
            ],
            'objectIsEmpty',
            'objectBecomesEmpty',
            'reuseAgreed',
            'propertyMarketingInformation' => [],
            'address' => [
                'streetName',
                'houseNumber',
                'postalCode',
                'place' => [
                    'placeName',
                ],
            ],
            'geolocationPoint' => [
                'id',
                'point',
            ],
            'geolocationPolygon' => [
                'id',
                'polygon',
            ],
            'geolocationMultiPolygon' => [
                'id',
                'multiPolygon',
            ],
        ];

        $buildingUnitJsonRepresentation = $this->serializer->serialize(
            data: $buildingUnit,
            format: 'json',
            context: [AbstractNormalizer::ATTRIBUTES => $attributes]
        );

        $buildingUnitExpose = BuildingUnitExpose::createFromBuildingUnitAndAccountConfigurationAndAccountUserAndAccountLegalTextsConfiguration(
            buildingUnit: $buildingUnit,
            accountUser: $accountUser,
            accountConfiguration: $account->getAccountConfiguration(),
            accountLegalTextsConfiguration: $accountLegalTextsConfiguration
        );

        return $this->render(view: 'property_vacancy_management/building_unit/pdf_expose/pdf_expose.html.twig', parameters: [
            'buildingUnitJsonRepresentation' => $buildingUnitJsonRepresentation,
            'buildingUnitExpose'             => $buildingUnitExpose
        ]);
    }

    #[Route('/header', name: 'header', methods: ['GET'])]
    public function header(string $token, ?Profiler $profiler): Response
    {
        $profiler?->disable();

        $oneTimeToken = $this->oneTimeTokenService->fetchAndDeleteOneTimeToken(token:  Uuid::fromString(uuid: $token));

        if ($oneTimeToken === null) {
            throw new NotFoundHttpException();
        }

        $oneTimeTokenData = $oneTimeToken->getJsonData();

        $buildingUnitId = $oneTimeTokenData['buildingUnitId'];
        $accountUserId = $oneTimeTokenData['accountUserId'];

        $buildingUnit = $this->buildingUnitService->fetchBuildingUnitByIdWithoutAccount(id: $buildingUnitId);

        if ($buildingUnit === null) {
            throw new NotFoundHttpException();
        }

        $accountUser = $this->accountUserService->fetchAccountUserByIdFromAllAccounts(id: $accountUserId);

        if ($accountUser === null) {
            throw new NotFoundHttpException();
        }

        if ($accountUser->getAccount()->getParentAccount() !== null) {
            $account = $accountUser->getAccount()->getParentAccount();
        } else {
            $account = $accountUser->getAccount();
        }

        return $this->render(view: 'property_vacancy_management/building_unit/pdf_expose/header.html.twig', parameters: [
            'buildingUnit' => $buildingUnit,
            'account'      => $account,
        ]);
    }
}
