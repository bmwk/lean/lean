<?php

declare(strict_types=1);

namespace App\Controller\PropertyVacancyManagement\BuildingUnit;

use App\Domain\Entity\AccountUser\AccountUserRole;
use App\Domain\Entity\DeletionType;
use App\Domain\Entity\Property\BuildingUnit;
use App\Domain\Property\BuildingUnitDeletionService;
use App\Domain\Property\BuildingUnitService;
use App\Form\Type\PropertyVacancyManagement\Property\BuildingUnitMassOperationDeleteType;
use App\Form\Type\PropertyVacancyManagement\Property\BuildingUnitMassOperationUnarchiveType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Security("is_granted('ROLE_USER') or is_granted('ROLE_PROPERTY_FEEDER') or is_granted('ROLE_PROPERTY_PROVIDER')")]
#[Route('/besatz-und-leerstand/archiv/objekte/massenoperation', name: 'property_vacancy_management_building_unit_archive_mass_operation_')]
class ArchiveMassOperationController extends AbstractController
{
    private const ACTIVE_NAV_GROUP = 'property_vacancy_management';
    private const ACTIVE_NAV_ITEM = self::ACTIVE_NAV_GROUP . '_building_unit_archive';

    public function __construct(
        private readonly BuildingUnitDeletionService $buildingUnitDeletionService,
        private readonly BuildingUnitService $buildingUnitService,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    #[Route('/aus-dem-archiv-wiederherstellen', name: 'unarchive', methods: ['POST'])]
    public function unarchive(Request $request, UserInterface $user): Response
    {
        $buildingUnitMassOperationUnarchiveForm = $this->createForm(type: BuildingUnitMassOperationUnarchiveType::class);

        $buildingUnitMassOperationUnarchiveForm->add(child: 'unarchive', type: SubmitType::class);

        $buildingUnitMassOperationUnarchiveForm->handleRequest(request: $request);

        if ($request->get(key: 'selectedRowIds') !== null) {
            $buildingUnitMassOperationUnarchiveForm->get('selectedRowIds')->setData($request->get(key: 'selectedRowIds'));
        }

        $createdByAccountUser = $user;

        if ($this->isGranted(attribute: AccountUserRole::ROLE_USER->name) === true) {
            $createdByAccountUser = null;
        }

        $buildingUnits = $this->buildingUnitService->fetchBuildingUnitsByIds(
            account: $user->getAccount(),
            withFromSubAccounts: true,
            ids: json_decode($buildingUnitMassOperationUnarchiveForm->get('selectedRowIds')->getData()),
            archived: true,
            createdByAccountUser: $createdByAccountUser
        );

        $buildingUnits = array_filter(
            array: $buildingUnits,
            callback: function (BuildingUnit $buildingUnit): bool {
                return $this->isGranted(attribute: 'archive', subject: $buildingUnit);
            }
        );

        if ($buildingUnitMassOperationUnarchiveForm->isSubmitted() && $buildingUnitMassOperationUnarchiveForm->isValid()) {
            foreach ($buildingUnits as $buildingUnit) {
                if ($this->isGranted(attribute: 'archive', subject: $buildingUnit) === true) {
                    $buildingUnit->setArchived(false);
                }
            }

            $this->entityManager->flush();

            $this->addFlash(type: 'dataDeleted', message: 'Die Objekte wurden aus dem Archiv wiederhergestellt.');

            return $this->redirectToRoute(route: 'property_vacancy_management_building_unit_archive_overview_list');
        }

        return $this->render(view: 'property_vacancy_management/building_unit/archive/mass_operation/unarchive.html.twig', parameters: [
            'buildingUnitMassOperationUnarchiveForm' => $buildingUnitMassOperationUnarchiveForm,
            'buildingUnits'                          => $buildingUnits,
            'pageTitle'                              => 'Leerstandsmanagement - Archivierte Objekte wiederherstellen',
            'activeNavGroup'                         => self::ACTIVE_NAV_GROUP,
            'activeNavItem'                          => self::ACTIVE_NAV_ITEM,
        ]);
    }

    #[Route('/loeschen', name: 'delete', methods: ['POST'])]
    public function delete(Request $request, UserInterface $user): Response
    {
        $buildingUnitMassOperationDeleteForm = $this->createForm(type: BuildingUnitMassOperationDeleteType::class);

        $buildingUnitMassOperationDeleteForm->add(child: 'delete', type: SubmitType::class);

        $buildingUnitMassOperationDeleteForm->handleRequest(request: $request);

        if ($request->get('selectedRowIds') !== null) {
            $buildingUnitMassOperationDeleteForm->get('selectedRowIds')->setData($request->get('selectedRowIds'));
        }

        $createdByAccountUser = $user;

        if ($this->isGranted(attribute: AccountUserRole::ROLE_USER->name) === true) {
            $createdByAccountUser = null;
        }

        $buildingUnits = $this->buildingUnitService->fetchBuildingUnitsByIds(
            account: $user->getAccount(),
            withFromSubAccounts: true,
            ids: json_decode($buildingUnitMassOperationDeleteForm->get('selectedRowIds')->getData()),
            archived: true,
            createdByAccountUser: $createdByAccountUser
        );

        $buildingUnits = array_filter(
            array: $buildingUnits,
            callback: function (BuildingUnit $buildingUnit): bool {
                return $this->isGranted(attribute: 'delete', subject: $buildingUnit);
            }
        );

        if (
            $buildingUnitMassOperationDeleteForm->isSubmitted()
            && $buildingUnitMassOperationDeleteForm->isValid()
            && $buildingUnitMassOperationDeleteForm->get('verificationCode')->getData() === 'objekte_' . count($buildingUnits)
        ) {
            foreach ($buildingUnits as $buildingUnit) {
                if ($this->isGranted(attribute: 'delete', subject: $buildingUnit) === true) {
                    $this->buildingUnitDeletionService->deleteBuildingUnit(
                        buildingUnit: $buildingUnit,
                        deletionType: DeletionType::SOFT,
                        deletedByAccountUser: $user
                    );
                }
            }

            $this->addFlash(type: 'dataDeleted', message: 'Die Objekte wurden gelöscht.');

            return $this->redirectToRoute(route: 'property_vacancy_management_building_unit_archive_overview_list');
        }

        return $this->render(view: 'property_vacancy_management/building_unit/archive/mass_operation/delete.html.twig', parameters: [
            'buildingUnitMassOperationDeleteForm' => $buildingUnitMassOperationDeleteForm,
            'buildingUnits'                       => $buildingUnits,
            'pageTitle'                           => 'Leerstandsmanagement - Archivierte Objekte löschen',
            'activeNavGroup'                      => self::ACTIVE_NAV_GROUP,
            'activeNavItem'                       => self::ACTIVE_NAV_ITEM,
        ]);
    }
}
