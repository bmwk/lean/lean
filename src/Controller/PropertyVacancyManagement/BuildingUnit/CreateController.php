<?php

declare(strict_types=1);

namespace App\Controller\PropertyVacancyManagement\BuildingUnit;

use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\GeolocationPoint;
use App\Domain\Entity\PlaceType;
use App\Domain\Entity\Property\Address;
use App\Domain\Entity\Property\Building;
use App\Domain\Entity\Property\BuildingUnit;
use App\Domain\Entity\Property\PropertyMarketingInformation;
use App\Domain\Entity\Property\PropertyPricingInformation;
use App\Domain\Entity\Property\PropertySpacesData;
use App\Domain\Entity\Property\PropertyUser;
use App\Domain\Property\BuildingUnitService;
use App\Domain\Property\PropertyUserService;
use App\Form\Type\PropertyVacancyManagement\Property\BuildingUnitCreateType;
use App\GoogleMaps\Exception\GoogleMapsException;
use App\GoogleMaps\GoogleMapsService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\UnitOfWork;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Security("is_granted('ROLE_USER') or is_granted('ROLE_PROPERTY_FEEDER') or is_granted('ROLE_PROPERTY_PROVIDER')")]
#[Route('/besatz-und-leerstand/objekte', name: 'property_vacancy_management_building_unit_')]
class CreateController extends AbstractController
{
    private const ACTIVE_NAV_GROUP = 'property_vacancy_management';
    private const ACTIVE_NAV_ITEM = self::ACTIVE_NAV_GROUP . '_building_unit';

    public function __construct(
        private readonly BuildingUnitService $buildingUnitService,
        private readonly PropertyUserService $propertyUserService,
        private readonly GoogleMapsService $googleMapsService,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    #[Route('/erfassen', name: 'create', methods: ['GET', 'POST'])]
    public function create(Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $buildingUnitAddress = new Address();
        $building = new Building();
        $buildingUnit = new BuildingUnit();

        $assignedPlace = $account->getAssignedPlace();

        if (in_array($assignedPlace->getPlaceType(), [PlaceType::COUNTY, PlaceType::MUNICIPAL_ASSOCIATION])) {
            $places = $assignedPlace->getChildrenPlaces();
        } else {
            $places = [$assignedPlace];

            $buildingUnitAddress->setPlace($assignedPlace);
        }

        $buildingUnitCreateForm = $this->createForm(type: BuildingUnitCreateType::class, data: $buildingUnit, options: [
            'places'  => $places,
            'account' => $account,
            'canEdit' => true,
        ]);

        if (empty($request->get('longitude')) === false && empty($request->get('latitude')) === false) {
            $buildingUnitCreateForm->get('longitude')->setData($request->get('longitude'));
            $buildingUnitCreateForm->get('latitude')->setData($request->get('latitude'));

            try {
                $this->buildingUnitService->queryGoogleMapsByLatitudeAndLongitudeToFillAddress(
                    latitude: (float)$request->get('latitude'),
                    longitude: (float)$request->get('longitude'),
                    address: $buildingUnitAddress
                );
            } catch (GoogleMapsException $googleMapsException) {
            }
        }

        $buildingUnitCreateForm->get('address')->setData($buildingUnitAddress);

        $buildingUnitCreateForm->add(child:'save', type: SubmitType::class);

        $buildingUnitCreateForm->handleRequest(request: $request);

        if ($buildingUnitCreateForm->isSubmitted() && $buildingUnitCreateForm->isValid()) {
            $this->entityManager->beginTransaction();

            if (
                $buildingUnitCreateForm->get('longitude')->isEmpty() === false
                && $buildingUnitCreateForm->get('latitude')->isEmpty() === false
            ) {
                $geolocationPoint = GeolocationPoint::createFromLatitudeAndLongitude(
                    latitude: (float)$buildingUnitCreateForm->get('latitude')->getData(),
                    longitude: (float)$buildingUnitCreateForm->get('longitude')->getData()
                );

                $this->entityManager->persist(entity: $geolocationPoint);

                $buildingUnit->setGeolocationPoint($geolocationPoint);
            }

            if ($this->googleMapsService->isServiceAvailable() === true && $buildingUnit->getGeolocationPoint() === null) {
                $geolocationPoint = $this->createAndSaveGeolocationPointWithGoogleMapsFromAddress(address: $buildingUnitAddress);

                $buildingUnit->setGeolocationPoint($geolocationPoint);
            }

            if (count($places) === 1) {
                $buildingUnitAddress->setPlace($places[0]);
            }

            $buildingAddress = clone $buildingUnitAddress;

            $this->entityManager->persist(entity: $buildingAddress);
            $this->entityManager->persist(entity: $buildingUnitAddress);

            $buildingPropertySpacesData = new PropertySpacesData();
            $this->entityManager->persist(entity: $buildingPropertySpacesData);

            $this->entityManager->flush();

            $building
                ->setAccount($account)
                ->setCreatedByAccountUser($user)
                ->setAddress($buildingAddress)
                ->setDeleted(false)
                ->setName($buildingUnit->getName())
                ->setWheelchairAccessible(false)
                ->setRampPresent(false)
                ->setObjectForeclosed(false)
                ->setPropertySpacesData($buildingPropertySpacesData);

            $this->entityManager->persist(entity: $building);
            $this->entityManager->flush();

            $buildingUnitPropertySpacesData = new PropertySpacesData();
            $propertyMarketingInformation = new PropertyMarketingInformation();
            $propertyPricingInformation = new PropertyPricingInformation();
            $propertyMarketingInformation->setPropertyPricingInformation($propertyPricingInformation);

            $buildingUnit
                ->setAccount($account)
                ->setCreatedByAccountUser($user)
                ->setArchived(false)
                ->setDeleted(false)
                ->setBuilding($building)
                ->setAddress($buildingUnitAddress)
                ->setWheelchairAccessible(false)
                ->setRampPresent(false)
                ->setRoofingPresent(false)
                ->setObjectForeclosed(false)
                ->setPropertySpacesData($buildingUnitPropertySpacesData)
                ->setPropertyMarketingInformation($propertyMarketingInformation);

            $this->entityManager->persist(entity: $buildingUnitPropertySpacesData);
            $this->entityManager->persist(entity: $propertyMarketingInformation);
            $this->entityManager->persist(entity: $propertyPricingInformation);
            $this->entityManager->persist(entity: $buildingUnit);
            $this->entityManager->flush();

            if ($buildingUnitCreateForm->get('propertyUserCreate')->getData() === 'true') {
                $propertyUser = $this->createAndSavePropertyUserFromPropertyUserForm(
                    propertyUserForm:  $buildingUnitCreateForm->get('propertyUser'),
                    accountUser: $user
                );

                $buildingUnit->getPropertyUsers()->add($propertyUser);
            }

            $this->entityManager->flush();
            $this->entityManager->commit();

            $this->addFlash(type: 'dataSaved', message: 'Das Objekt wurde erstellt. Sie können nun weitere Informationen einpflegen.');

            return $this->redirectToRoute(route: 'property_vacancy_management_building_unit_basic_data', parameters: ['buildingUnitId' => $buildingUnit->getId()]);
        }

        return $this->render(view: 'property_vacancy_management/building_unit/create.html.twig', parameters: [
            'buildingUnitCreateForm' => $buildingUnitCreateForm,
            'pageTitle'              => 'Leerstandsmanagement - Objekt erfassen',
            'activeNavGroup'         => self::ACTIVE_NAV_GROUP,
            'activeNavItem'          => self::ACTIVE_NAV_ITEM,
        ]);
    }

    private function createAndSaveGeolocationPointWithGoogleMapsFromAddress(Address $address): ?GeolocationPoint
    {
        try {
            $googleApiResponse = $this->googleMapsService->fetchGeoDataByAddress(
                streetName: $address->getStreetName(),
                houseNumber: $address->getHouseNumber(),
                postalCode: $address->getPostalCode(),
                placeName: $address->getPlace()->getPlaceName()
            );

            if (
                count($googleApiResponse->results) === 0
                || isset($googleApiResponse->results[0]->geometry) === false
                || isset($googleApiResponse->results[0]->geometry->location) === false
            ) {
                return null;
            }

            $geolocationPoint = GeolocationPoint::createFromLatitudeAndLongitude(
                latitude: $googleApiResponse->results[0]->geometry->location->lat,
                longitude: $googleApiResponse->results[0]->geometry->location->lng
            );

            $this->entityManager->persist(entity: $geolocationPoint);
            $this->entityManager->flush();

            return $geolocationPoint;
        } catch (GoogleMapsException $googleMapsException) {
            return null;
        }
    }

    private function createAndSavePropertyUserFromPropertyUserForm(FormInterface $propertyUserForm, AccountUser $accountUser): PropertyUser
    {
        $account = $accountUser->getAccount();

        try {
            $propertyUser = $this->propertyUserService->createPropertyUserFromPropertyUserForm(propertyUserForm: $propertyUserForm);
        } catch (\RuntimeException $exception) {
            throw new BadRequestException();
        }

        $propertyUser
            ->setAccount($account)
            ->setCreatedByAccountUser($accountUser);

        $unitOfWork = $this->entityManager->getUnitOfWork();

        if ($unitOfWork->getEntityState($propertyUser->getPerson()) === UnitOfWork::STATE_NEW) {
            $propertyUser->getPerson()
                ->setAccount($account)
                ->setCreatedByAccountUser($accountUser)
                ->setDeleted(false)
                ->setAnonymized(false);

            $this->entityManager->persist(entity: $propertyUser->getPerson());
        }

        $this->entityManager->persist(entity: $propertyUser);
        $this->entityManager->flush();

        return $propertyUser;
    }
}
