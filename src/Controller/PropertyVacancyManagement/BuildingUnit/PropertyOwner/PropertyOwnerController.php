<?php

declare(strict_types=1);

namespace App\Controller\PropertyVacancyManagement\BuildingUnit\PropertyOwner;

use App\Domain\DocumentTemplate\DocumentTemplateService;
use App\Domain\Entity\FeedbackRelationship;
use App\Domain\Entity\Property\PropertyOwner;
use App\Domain\Feedback\FeedbackService;
use App\Domain\Person\PersonService;
use App\Domain\Property\BuildingUnitDeletionService;
use App\Domain\Property\BuildingUnitService;
use App\Domain\Property\PropertyOwnerService;
use App\Form\Type\PropertyVacancyManagement\Property\PropertyOwnerRemoveType;
use App\Form\Type\PropertyVacancyManagement\Property\PropertyOwnerType;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\UnitOfWork;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Security("is_granted('ROLE_USER') or is_granted('ROLE_PROPERTY_FEEDER') or is_granted('ROLE_VIEWER') or is_granted('ROLE_RESTRICTED_VIEWER')")]
#[Route('/besatz-und-leerstand/objekte/{buildingUnitId<\d{1,10}>}/eigentuemer', name: 'property_vacancy_management_building_unit_property_owner_')]
class PropertyOwnerController extends AbstractController
{
    private const ACTIVE_NAV_GROUP = 'property_vacancy_management';
    private const ACTIVE_NAV_ITEM = self::ACTIVE_NAV_GROUP . '_building_unit';

    public function __construct(
        private readonly BuildingUnitService $buildingUnitService,
        private readonly BuildingUnitDeletionService $buildingUnitDeletionService,
        private readonly PropertyOwnerService $propertyOwnerService,
        private readonly PersonService $personService,
        private readonly FeedbackService $feedbackService,
        private readonly DocumentTemplateService $documentTemplateService,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    #[Route('/uebersicht', name: 'overview', methods: ['GET'])]
    public function overview(int $buildingUnitId, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $buildingUnit = $this->buildingUnitService->fetchBuildingUnitById(account: $account, withFromSubAccounts: true, id: $buildingUnitId);

        if ($buildingUnit === null) {
            throw new NotFoundHttpException();
        }

        if ($buildingUnit->getAccount()->getParentAccount() !== null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'view', subject: $buildingUnit);

        return $this->render(view: 'property_vacancy_management/building_unit/property_owner/overview.html.twig', parameters: [
            'buildingUnit' => $buildingUnit,
            'occurrences'  => $this->personService->fetchOccurrencesByPropertyOwnersFromBuildingUnit(
                account: $account,
                withFromSubAccounts: false,
                buildingUnit: $buildingUnit
            ),
            'feedbackGroups'        => $this->feedbackService->fetchFeedbackGroupsByFeedbackRelationship(feedbackRelationship: FeedbackRelationship::PROPERTY_OWNER),
            'buildingUnitFeedbacks' => $this->feedbackService->fetchBuildingUnitFeedbacksByBuildingUnit(buildingUnit: $buildingUnit),
            'documentTemplates'     => $this->documentTemplateService->fetchDocumentTemplatesByAccount(account: $account),
            'pageTitle'             => 'Leerstandsmanagement - Objektinformationen - Eigentümer',
            'activeNavGroup'        => self::ACTIVE_NAV_GROUP,
            'activeNavItem'         => self::ACTIVE_NAV_ITEM . ($buildingUnit->isArchived() ? '_archive' : ''),
        ]);
    }

    #[Security("is_granted('ROLE_USER') or is_granted('ROLE_PROPERTY_FEEDER')")]
    #[Route('/anlegen', name: 'create', methods: ['GET', 'POST'])]
    public function create(int $buildingUnitId, Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $buildingUnit = $this->buildingUnitService->fetchBuildingUnitById(account: $account, withFromSubAccounts: true, id: $buildingUnitId);

        if ($buildingUnit === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'edit', subject: $buildingUnit);

        $propertyOwner = new PropertyOwner();

        $propertyOwnerForm = $this->createForm(type: PropertyOwnerType::class, data: $propertyOwner, options: [
            'account'        => $account,
            'selectedPerson' => null,
            'canEdit'        => $this->isGranted(attribute: 'edit', subject: $buildingUnit),
            'canViewPerson'  => true,
        ]);

        $propertyOwnerForm->add(child: 'save', type: SubmitType::class);

        $propertyOwnerForm->handleRequest(request: $request);

        if ($propertyOwnerForm->isSubmitted() && $propertyOwnerForm->isValid()) {
            $this->entityManager->beginTransaction();

            $propertyOwner
                ->setAccount($account)
                ->setCreatedByAccountUser($user);

            $unitOfWork = $this->entityManager->getUnitOfWork();

            if ($unitOfWork->getEntityState($propertyOwner->getPerson()) === UnitOfWork::STATE_NEW) {
                $propertyOwner->getPerson()
                    ->setAccount($account)
                    ->setCreatedByAccountUser($user)
                    ->setDeleted(false)
                    ->setAnonymized(false);

                $this->entityManager->persist($propertyOwner->getPerson());
            }

            $this->entityManager->persist(entity: $propertyOwner);
            $this->entityManager->flush();

            $buildingUnit->getPropertyOwners()->add($propertyOwner);

            $this->entityManager->flush();
            $this->entityManager->commit();

            $this->addFlash(type: 'dataSaved', message: 'Der Eigentümer wurde hinterlegt.');

            return $this->redirectToRoute(route: 'property_vacancy_management_building_unit_property_owner_overview', parameters: ['buildingUnitId' => $buildingUnitId]);
        }

        return $this->render(view: 'property_vacancy_management/building_unit/property_owner/create.html.twig', parameters: [
            'propertyOwnerForm' => $propertyOwnerForm,
            'buildingUnit'      => $buildingUnit,
            'pageTitle'         => 'Leerstandsmanagement - Objektinformationen - Eigentümer erfassen',
            'activeNavGroup'    => self::ACTIVE_NAV_GROUP,
            'activeNavItem'     => self::ACTIVE_NAV_ITEM . ($buildingUnit->isArchived() ? '_archive' : ''),
        ]);
    }

    #[Route('/{propertyOwnerId<\d{1,10}>}', name: 'edit', methods: ['GET', 'POST'])]
    public function edit(int $buildingUnitId, int $propertyOwnerId, Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $buildingUnit = $this->buildingUnitService->fetchBuildingUnitById(account: $account, withFromSubAccounts: true, id: $buildingUnitId);

        if ($buildingUnit === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'view', subject: $buildingUnit);

        $propertyOwner = $this->propertyOwnerService->fetchPropertyOwnerByIdAndBuildingUnit(
            account: $account,
            withFromSubAccounts: true,
            id: $propertyOwnerId,
            buildingUnit: $buildingUnit
        );

        if ($propertyOwner === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'view', subject: $propertyOwner);

        $propertyOwnerForm = $this->createForm(type: PropertyOwnerType::class, data: $propertyOwner, options: [
            'account'        => $account,
            'selectedPerson' => $propertyOwner->getPerson(),
            'canEdit'        => $this->isGranted(attribute: 'edit', subject: $buildingUnit),
            'canViewPerson'  => $this->isGranted(attribute: 'view', subject: $propertyOwner->getPerson()),
        ]);

        $propertyOwnerForm->add(child: 'save', type: SubmitType::class);

        $propertyOwnerForm->handleRequest(request: $request);

        if ($propertyOwnerForm->isSubmitted() && $propertyOwnerForm->isValid()) {
            $this->denyAccessUnlessGranted(attribute: 'edit', subject: $buildingUnit);
            $this->denyAccessUnlessGranted(attribute: 'edit', subject: $propertyOwner);

            $unitOfWork = $this->entityManager->getUnitOfWork();

            if ($unitOfWork->getEntityState($propertyOwner->getPerson()) === UnitOfWork::STATE_NEW) {
                $propertyOwner->getPerson()
                    ->setAccount($account)
                    ->setCreatedByAccountUser($user)
                    ->setDeleted(false)
                    ->setAnonymized(false);

                $this->entityManager->persist(entity: $propertyOwner->getPerson());
            }

            $this->entityManager->flush();

            $this->addFlash(type: 'dataSaved', message: 'Die Eigentümerinformationen wurden bearbeitet.');

            return $this->redirectToRoute(route: 'property_vacancy_management_building_unit_property_owner_overview', parameters: ['buildingUnitId' => $buildingUnitId]);
        }

        return $this->render(view: 'property_vacancy_management/building_unit/property_owner/edit.html.twig', parameters: [
            'propertyOwnerForm' => $propertyOwnerForm,
            'buildingUnit'      => $buildingUnit,
            'propertyOwner'     => $propertyOwner,
            'pageTitle'         => 'Leerstandsmanagement - Objektinformationen - Eigentümer bearbeiten',
            'activeNavGroup'    => self::ACTIVE_NAV_GROUP,
            'activeNavItem'     => self::ACTIVE_NAV_ITEM . ($buildingUnit->isArchived() ? '_archive' : ''),
        ]);
    }

    #[Security("is_granted('ROLE_USER') or is_granted('ROLE_PROPERTY_FEEDER')")]
    #[Route('/{propertyOwnerId<\d{1,10}>}/verknuepfung-aufheben', name: 'remove', methods: ['GET', 'POST'])]
    public function remove(int $buildingUnitId, int $propertyOwnerId, Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $buildingUnit = $this->buildingUnitService->fetchBuildingUnitById(account: $account, withFromSubAccounts: true, id: $buildingUnitId);

        if ($buildingUnit === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'edit', subject: $buildingUnit);

        $propertyOwner = $this->propertyOwnerService->fetchPropertyOwnerByIdAndBuildingUnit(
            account: $account,
            withFromSubAccounts: true,
            id: $propertyOwnerId,
            buildingUnit: $buildingUnit
        );

        if ($propertyOwner === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'delete', subject: $propertyOwner);

        $propertyOwnerRemoveForm = $this->createForm(type: PropertyOwnerRemoveType::class);

        $propertyOwnerRemoveForm->add(child:'remove', type: SubmitType::class);

        $propertyOwnerRemoveForm->handleRequest(request: $request);

        if ($propertyOwnerRemoveForm->isSubmitted() && $propertyOwnerRemoveForm->isValid()) {
            if ($propertyOwnerRemoveForm->get('verificationCode')->getData() === 'eigentuemer_' . $propertyOwner->getId()) {
                $this->buildingUnitDeletionService->deletePropertyOwner(propertyOwner: $propertyOwner, property: $buildingUnit);

                $this->addFlash(type: 'dataDeleted', message: 'Der Eigentümer wurde vom Objekt gelöscht.');

                return $this->redirectToRoute(route: 'property_vacancy_management_building_unit_property_owner_overview', parameters: ['buildingUnitId' => $buildingUnitId]);
            }
        }

        $propertyOwnerForm = $this->createForm(type: PropertyOwnerType::class, data: $propertyOwner, options: [
            'account'        => $account,
            'selectedPerson' => $propertyOwner->getPerson(),
            'canEdit'        => false,
            'canViewPerson'  => true,
        ]);

        return $this->render(view: 'property_vacancy_management/building_unit/property_owner/remove.html.twig', parameters: [
            'propertyOwnerRemoveForm' => $propertyOwnerRemoveForm,
            'propertyOwnerForm'       => $propertyOwnerForm,
            'buildingUnit'            => $buildingUnit,
            'propertyOwner'           => $propertyOwner,
            'pageTitle'               => 'Leerstandsmanagement - Objektinformationen - Eigentümer entfernen',
            'activeNavGroup'          => self::ACTIVE_NAV_GROUP,
            'activeNavItem'           => self::ACTIVE_NAV_ITEM . ($buildingUnit->isArchived() ? '_archive' : ''),
        ]);
    }
}
