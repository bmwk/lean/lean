<?php

declare(strict_types=1);

namespace App\Controller\PropertyVacancyManagement\BuildingUnit;

use App\Domain\Entity\AccountUser\AccountUserRole;
use App\Domain\Entity\DeletionType;
use App\Domain\Entity\Property\BuildingUnit;
use App\Domain\Property\BuildingUnitDeletionService;
use App\Domain\Property\BuildingUnitService;
use App\Form\Type\PropertyVacancyManagement\Property\BuildingUnitMassOperationAssignToAccountUserType;
use App\Form\Type\PropertyVacancyManagement\Property\BuildingUnitMassOperationArchiveType;
use App\Form\Type\PropertyVacancyManagement\Property\BuildingUnitMassOperationDeleteType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Security("is_granted('ROLE_USER') or is_granted('ROLE_PROPERTY_FEEDER') or is_granted('ROLE_PROPERTY_PROVIDER')")]
#[Route('/besatz-und-leerstand/objekte/massenoperation', name: 'property_vacancy_management_building_unit_mass_operation_')]
class MassOperationController extends AbstractController
{
    private const ACTIVE_NAV_GROUP = 'property_vacancy_management';
    private const ACTIVE_NAV_ITEM = self::ACTIVE_NAV_GROUP . '_building_unit';

    public function __construct(
        private readonly BuildingUnitService $buildingUnitService,
        private readonly BuildingUnitDeletionService $buildingUnitDeletionService,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    #[Route('/archivieren', name: 'archive', methods: ['POST'])]
    public function archive(Request $request, UserInterface $user): Response
    {
        $buildingUnitMassOperationArchiveForm = $this->createForm(type: BuildingUnitMassOperationArchiveType::class);

        $buildingUnitMassOperationArchiveForm->add(child: 'archive', type: SubmitType::class);

        $buildingUnitMassOperationArchiveForm->handleRequest(request: $request);

        if ($request->get('selectedRowIds') !== null) {
            $buildingUnitMassOperationArchiveForm->get('selectedRowIds')->setData($request->get('selectedRowIds'));
        }

        $createdByAccountUser = $user;

        if ($this->isGranted(attribute: AccountUserRole::ROLE_USER->name) === true) {
            $createdByAccountUser = null;
        }

        $buildingUnits = $this->buildingUnitService->fetchBuildingUnitsByIds(
            account: $user->getAccount(),
            withFromSubAccounts: true,
            ids: json_decode($buildingUnitMassOperationArchiveForm->get('selectedRowIds')->getData()),
            archived: false,
            createdByAccountUser: $createdByAccountUser
        );

        $buildingUnits = array_filter(
            array: $buildingUnits,
            callback: function (BuildingUnit $buildingUnit): bool {
                return $this->isGranted(attribute: 'archive', subject: $buildingUnit);
            }
        );

        if ($buildingUnitMassOperationArchiveForm->isSubmitted() && $buildingUnitMassOperationArchiveForm->isValid()) {
            foreach ($buildingUnits as $buildingUnit) {
                if ($this->isGranted(attribute: 'archive', subject: $buildingUnit) === false) {
                    continue;
                }

                $buildingUnit->setArchived(true);
            }

            $this->entityManager->flush();

            $this->addFlash(type: 'dataDeleted', message: 'Die Objekte wurden archiviert.');

            return $this->redirectToRoute(route: 'property_vacancy_management_building_unit_overview_list');
        }

        return $this->render(view: 'property_vacancy_management/building_unit/mass_operation/archive.html.twig', parameters: [
            'buildingUnitMassOperationArchiveForm' => $buildingUnitMassOperationArchiveForm,
            'buildingUnits'                        => $buildingUnits,
            'pageTitle'                            => 'Leerstandsmanagement - Objekte archivieren',
            'activeNavGroup'                       => self::ACTIVE_NAV_GROUP,
            'activeNavItem'                        => self::ACTIVE_NAV_ITEM,
        ]);
    }

    #[Route('/loeschen', name: 'delete', methods: ['POST'])]
    public function delete(Request $request, UserInterface $user): Response
    {
        $buildingUnitMassOperationDeleteForm = $this->createForm(type: BuildingUnitMassOperationDeleteType::class);

        $buildingUnitMassOperationDeleteForm->add(child: 'delete', type: SubmitType::class);

        $buildingUnitMassOperationDeleteForm->handleRequest(request: $request);

        if ($request->get('selectedRowIds') !== null) {
            $buildingUnitMassOperationDeleteForm->get('selectedRowIds')->setData($request->get('selectedRowIds'));
        }

        $createdByAccountUser = $user;

        if ($this->isGranted(attribute: AccountUserRole::ROLE_USER->name) === true) {
            $createdByAccountUser = null;
        }

        $buildingUnits = $this->buildingUnitService->fetchBuildingUnitsByIds(
            account: $user->getAccount(),
            withFromSubAccounts: true,
            ids: json_decode($buildingUnitMassOperationDeleteForm->get('selectedRowIds')->getData()),
            archived: false,
            createdByAccountUser: $createdByAccountUser
        );

        $buildingUnits = array_filter(
            array: $buildingUnits,
            callback: function (BuildingUnit $buildingUnit): bool {
                return $this->isGranted(attribute: 'delete', subject: $buildingUnit);
            }
        );

        if (
            $buildingUnitMassOperationDeleteForm->isSubmitted()
            && $buildingUnitMassOperationDeleteForm->isValid()
            && $buildingUnitMassOperationDeleteForm->get('verificationCode')->getData() === 'objekte_' . count($buildingUnits)
        ) {
            foreach ($buildingUnits as $buildingUnit) {
                if ($this->isGranted(attribute: 'delete', subject: $buildingUnit) === false) {
                    continue;
                }

                $this->buildingUnitDeletionService->deleteBuildingUnit(
                    buildingUnit: $buildingUnit,
                    deletionType: DeletionType::SOFT,
                    deletedByAccountUser: $user
                );
            }

            $this->addFlash(type: 'dataDeleted', message: 'Die Objekte wurden gelöscht.');

            return $this->redirectToRoute(route: 'property_vacancy_management_building_unit_overview_list');
        }

        return $this->render(view: 'property_vacancy_management/building_unit/mass_operation/delete.html.twig', parameters: [
            'buildingUnitMassOperationDeleteForm' => $buildingUnitMassOperationDeleteForm,
            'buildingUnits'                       => $buildingUnits,
            'pageTitle'                           => 'Leerstandsmanagement - Objekte löschen',
            'activeNavGroup'                      => self::ACTIVE_NAV_GROUP,
            'activeNavItem'                       => self::ACTIVE_NAV_ITEM,
        ]);
    }

    #[Route('/sachbearbeiter-zuweisen', name: 'assign_to_account_user', methods: ['POST'])]
    public function assignToAccountUser(Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $buildingUnitMassOperationAssignToAccountUserForm = $this->createForm(
            type: BuildingUnitMassOperationAssignToAccountUserType::class,
            options: ['account' => $account]
        );

        $buildingUnitMassOperationAssignToAccountUserForm->add(child: 'save', type: SubmitType::class);

        $buildingUnitMassOperationAssignToAccountUserForm->handleRequest(request: $request);

        if ($request->get('selectedRowIds') !== null) {
            $buildingUnitMassOperationAssignToAccountUserForm->get('selectedRowIds')->setData($request->get(key: 'selectedRowIds'));
        }

        $createdByAccountUser = $user;

        if ($this->isGranted(attribute: AccountUserRole::ROLE_USER->name) === true) {
            $createdByAccountUser = null;
        }

        $buildingUnits = $this->buildingUnitService->fetchBuildingUnitsByIds(
            account: $account,
            withFromSubAccounts: true,
            ids: json_decode($buildingUnitMassOperationAssignToAccountUserForm->get('selectedRowIds')->getData()),
            archived: false,
            createdByAccountUser: $createdByAccountUser
        );

        $buildingUnits = array_filter(
            array: $buildingUnits,
            callback: function (BuildingUnit $buildingUnit): bool {
                return $this->isGranted(attribute: 'assignToAccountUser', subject: $buildingUnit);
            }
        );

        if ($buildingUnitMassOperationAssignToAccountUserForm->isSubmitted() && $buildingUnitMassOperationAssignToAccountUserForm->isValid()) {
            foreach ($buildingUnits as $buildingUnit) {
                if ($this->isGranted(attribute: 'assignToAccountUser', subject: $buildingUnit) === false) {
                    continue;
                }

                $buildingUnit->setAssignedToAccountUser($buildingUnitMassOperationAssignToAccountUserForm->getData()['assignedToAccountUser']);
            }

            $this->entityManager->flush();

            if ($buildingUnitMassOperationAssignToAccountUserForm->getData()['assignedToAccountUser'] === null) {
                $this->addFlash(type: 'dataDeleted', message: 'Es wurde kein/e Sachbearbeiter:in zugewiesen.');
            } else {
                $this->addFlash(type: 'dataSaved', message: 'Sachbearbeiter:in erfolgreich zugewiesen.');
            }

            return $this->redirectToRoute(route: 'property_vacancy_management_building_unit_overview_list');
        }

        return $this->render(view: 'property_vacancy_management/building_unit/mass_operation/assign_to_account_user.html.twig', parameters: [
            'buildingUnitMassOperationAssignToAccountUserForm' => $buildingUnitMassOperationAssignToAccountUserForm,
            'buildingUnits'                                    => $buildingUnits,
            'pageTitle'                                        => 'Leerstandsmanagement - Sachbearbeiter:in zuweisen',
            'activeNavGroup'                                   => self::ACTIVE_NAV_GROUP,
            'activeNavItem'                                    => self::ACTIVE_NAV_ITEM,
        ]);
    }
}
