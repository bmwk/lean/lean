<?php

declare(strict_types=1);

namespace App\Controller\PropertyVacancyManagement\BuildingUnit\PropertyContactPerson;

use App\Domain\Entity\Property\PropertyContactPerson;
use App\Domain\Person\PersonService;
use App\Domain\Property\BuildingUnitDeletionService;
use App\Domain\Property\BuildingUnitService;
use App\Domain\Property\PropertyContactPersonService;
use App\Form\Type\PropertyVacancyManagement\Property\PropertyContactPersonDeleteType;
use App\Form\Type\PropertyVacancyManagement\Property\PropertyContactPersonType;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\UnitOfWork;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Security("is_granted('ROLE_USER') or is_granted('ROLE_PROPERTY_FEEDER') or is_granted('ROLE_VIEWER') or is_granted('ROLE_RESTRICTED_VIEWER')")]
#[Route('/besatz-und-leerstand/objekte/{buildingUnitId<\d{1,10}>}/ansprechpersonen', name: 'property_vacancy_management_building_unit_property_contact_person_')]
class PropertyContactPersonController extends AbstractController
{
    private const ACTIVE_NAV_GROUP = 'property_vacancy_management';
    private const ACTIVE_NAV_ITEM = self::ACTIVE_NAV_GROUP . '_building_unit';

    public function __construct(
        private readonly BuildingUnitService $buildingUnitService,
        private readonly BuildingUnitDeletionService $buildingUnitDeletionService,
        private readonly PropertyContactPersonService $propertyContactPersonService,
        private readonly PersonService $personService,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    #[Security("is_granted('ROLE_USER') or is_granted('ROLE_PROPERTY_FEEDER') or is_granted('ROLE_VIEWER') or is_granted('ROLE_RESTRICTED_VIEWER')")]
    #[Route('/uebersicht', name: 'overview', methods: ['GET'])]
    public function overview(int $buildingUnitId, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $buildingUnit = $this->buildingUnitService->fetchBuildingUnitById(account: $account, withFromSubAccounts: true, id: $buildingUnitId);

        if ($buildingUnit === null) {
            throw new NotFoundHttpException();
        }

        if ($buildingUnit->getAccount()->getParentAccount() !== null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'view', subject: $buildingUnit);

        return $this->render(view: 'property_vacancy_management/building_unit/property_contact_person/overview.html.twig', parameters: [
            'buildingUnit' => $buildingUnit,
            'occurrences'  => $this->personService->fetchOccurrencesByPropertyContactPersonsFromBuildingUnit(
                account: $account,
                withFromSubAccounts: false,
                buildingUnit: $buildingUnit
            ),
            'pageTitle'      => 'Leerstandsmanagement - Objektinformationen - Ansprechpersonen',
            'activeNavGroup' => self::ACTIVE_NAV_GROUP,
            'activeNavItem'  => self::ACTIVE_NAV_ITEM . ($buildingUnit->isArchived() ? '_archive' : ''),
        ]);
    }

    #[Security("is_granted('ROLE_USER') or is_granted('ROLE_PROPERTY_FEEDER')")]
    #[Route('/anlegen', name: 'create', methods: ['GET', 'POST'])]
    public function create(int $buildingUnitId, Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $buildingUnit = $this->buildingUnitService->fetchBuildingUnitById(account: $account, withFromSubAccounts: true, id: $buildingUnitId);

        if ($buildingUnit === null) {
            throw new NotFoundHttpException();
        }

        if ($buildingUnit->getAccount()->getParentAccount() !== null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'edit', subject: $buildingUnit);

        $propertyContactPerson = new PropertyContactPerson();

        $propertyContactPersonForm = $this->createForm(type: PropertyContactPersonType::class, data: $propertyContactPerson, options: [
            'account'        => $account,
            'selectedPerson' => null,
            'canEdit'        => $this->isGranted(attribute: 'edit', subject: $buildingUnit),
            'canViewPerson'  => true,
        ]);

        $propertyContactPersonForm->add(child:'save', type: SubmitType::class);

        $propertyContactPersonForm->handleRequest(request: $request);

        if ($propertyContactPersonForm->isSubmitted() && $propertyContactPersonForm->isValid()) {
            $this->entityManager->beginTransaction();

            $unitOfWork = $this->entityManager->getUnitOfWork();

            if ($unitOfWork->getEntityState($propertyContactPerson->getPerson()) === UnitOfWork::STATE_NEW) {
                $propertyContactPerson->getPerson()
                    ->setAccount($account)
                    ->setCreatedByAccountUser($user)
                    ->setDeleted(false)
                    ->setAnonymized(false);

                $this->entityManager->persist($propertyContactPerson->getPerson());
            }

            $propertyContactPerson
                ->setAccount($account)
                ->setCreatedByAccountUser($user);

            $this->entityManager->persist(entity: $propertyContactPerson);

            $buildingUnit->getPropertyContactPersons()->add($propertyContactPerson);

            $this->entityManager->flush();
            $this->entityManager->commit();

            $this->addFlash(type: 'dataSaved', message: 'Die Ansprechperson wurde gespeichert.');

            return $this->redirectToRoute(route: 'property_vacancy_management_building_unit_property_contact_person_overview', parameters: ['buildingUnitId' => $buildingUnit->getId()]);
        }

        return $this->render(view: 'property_vacancy_management/building_unit/property_contact_person/create.html.twig', parameters: [
            'propertyContactPersonForm' => $propertyContactPersonForm,
            'buildingUnit'              => $buildingUnit,
            'pageTitle'                 => 'Leerstandsmanagement - Objektinformationen - Ansprechperson erfassen',
            'activeNavGroup'            => self::ACTIVE_NAV_GROUP,
            'activeNavItem'             => self::ACTIVE_NAV_ITEM . ($buildingUnit->isArchived() ? '_archive' : ''),
        ]);
    }

    #[Route('/{id<\d{1,10}>}', name: 'edit', methods: ['GET', 'POST'])]
    public function edit(int $buildingUnitId, int $id, Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $buildingUnit = $this->buildingUnitService->fetchBuildingUnitById(account: $account, withFromSubAccounts: true, id: $buildingUnitId);

        if ($buildingUnit === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'view', subject: $buildingUnit);

        $propertyContactPerson = $this->propertyContactPersonService->fetchPropertyContactPersonByIdAndBuildingUnit(
            account: $account,
            withFromSubAccounts: true,
            id: $id,
            buildingUnit: $buildingUnit
        );

        if ($propertyContactPerson === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'view', subject: $propertyContactPerson);

        $propertyContactPersonForm = $this->createForm(type: PropertyContactPersonType::class, data: $propertyContactPerson, options: [
            'account'        => $account,
            'selectedPerson' => $propertyContactPerson->getPerson(),
            'canEdit'        => $this->isGranted(attribute: 'edit', subject: $buildingUnit),
            'canViewPerson'  => $this->isGranted(attribute: 'view', subject: $propertyContactPerson->getPerson()),
        ]);

        $propertyContactPersonForm->add(child:'save', type: SubmitType::class);

        $propertyContactPersonForm->handleRequest(request: $request);

        if ($propertyContactPersonForm->isSubmitted() && $propertyContactPersonForm->isValid()) {
            $this->denyAccessUnlessGranted(attribute: 'edit', subject: $buildingUnit);
            $this->denyAccessUnlessGranted(attribute: 'edit', subject: $propertyContactPerson);

            $unitOfWork = $this->entityManager->getUnitOfWork();

            if ($unitOfWork->getEntityState($propertyContactPerson->getPerson()) === UnitOfWork::STATE_NEW) {
                $propertyContactPerson->getPerson()
                    ->setAccount($account)
                    ->setCreatedByAccountUser($user)
                    ->setDeleted(false)
                    ->setAnonymized(false);

                $this->entityManager->persist(entity: $propertyContactPerson->getPerson());
            }

            $this->entityManager->flush();

            $this->addFlash(type: 'dataSaved', message: 'Die Ansprechperson wurde bearbeitet.');

            return $this->redirectToRoute(route: 'property_vacancy_management_building_unit_property_contact_person_overview', parameters: ['buildingUnitId' => $buildingUnitId]);
        }

        return $this->render(view: 'property_vacancy_management/building_unit/property_contact_person/edit.html.twig', parameters: [
            'propertyContactPersonForm' => $propertyContactPersonForm,
            'buildingUnit'              => $buildingUnit,
            'propertyContactPerson'     => $propertyContactPerson,
            'pageTitle'                 => 'Leerstandsmanagement - Objektinformationen - Ansprechperson bearbeiten',
            'activeNavGroup'            => self::ACTIVE_NAV_GROUP,
            'activeNavItem'             => self::ACTIVE_NAV_ITEM . ($buildingUnit->isArchived() ? '_archive' : ''),
        ]);
    }

    #[Security("is_granted('ROLE_USER') or is_granted('ROLE_PROPERTY_FEEDER') or is_granted('ROLE_PROPERTY_PROVIDER')")]
    #[Route('/{id<\d{1,10}>}/loeschen', name: 'delete', methods: ['GET', 'POST'])]
    public function delete(int $buildingUnitId, int $id, Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $buildingUnit = $this->buildingUnitService->fetchBuildingUnitById(account: $account, withFromSubAccounts: true, id: $buildingUnitId);

        if ($buildingUnit === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'edit', subject: $buildingUnit);

        $propertyContactPerson = $this->propertyContactPersonService->fetchPropertyContactPersonByIdAndBuildingUnit(
            account: $account,
            withFromSubAccounts: true,
            id: $id,
            buildingUnit: $buildingUnit
        );

        if ($propertyContactPerson === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'delete', subject: $propertyContactPerson);

        $propertyContactPersonDeleteForm = $this->createForm(type: PropertyContactPersonDeleteType::class);

        $propertyContactPersonDeleteForm->add(child:'delete', type: SubmitType::class);

        $propertyContactPersonDeleteForm->handleRequest(request: $request);

        if ($propertyContactPersonDeleteForm->isSubmitted() && $propertyContactPersonDeleteForm->isValid()) {
            if ($propertyContactPersonDeleteForm->get('verificationCode')->getData() === 'ansprechperson_' . $propertyContactPerson->getId()) {
                $this->buildingUnitDeletionService->deletePropertyContactPerson(propertyContactPerson: $propertyContactPerson, buildingUnit: $buildingUnit);

                $this->addFlash(type: 'dataDeleted', message: 'Die Ansprechperson wurde gelöscht.');

                return $this->redirectToRoute(route: 'property_vacancy_management_building_unit_property_contact_person_overview', parameters: ['buildingUnitId' => $buildingUnitId]);
            }
        }

        $propertyContactPersonForm = $this->createForm(type: PropertyContactPersonType::class, data: $propertyContactPerson, options: [
            'account'        => $account,
            'selectedPerson' => $propertyContactPerson->getPerson(),
            'canEdit'        => false,
            'canViewPerson'  => true,
        ]);

        return $this->render(view: 'property_vacancy_management/building_unit/property_contact_person/delete.html.twig', parameters: [
            'propertyContactPersonDeleteForm' => $propertyContactPersonDeleteForm,
            'propertyContactPersonForm'       => $propertyContactPersonForm,
            'buildingUnit'                    => $buildingUnit,
            'propertyContactPerson'           => $propertyContactPerson,
            'pageTitle'                       => 'Leerstandsmanagement - Objektinformationen - Ansprechperson löschen',
            'activeNavGroup'                  => self::ACTIVE_NAV_GROUP,
            'activeNavItem'                   => self::ACTIVE_NAV_ITEM . ($buildingUnit->isArchived() ? '_archive' : ''),
        ]);
    }
}
