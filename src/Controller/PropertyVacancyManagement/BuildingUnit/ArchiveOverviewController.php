<?php

declare(strict_types=1);

namespace App\Controller\PropertyVacancyManagement\BuildingUnit;

use App\Controller\AbstractLeAnController;
use App\Domain\Entity\LastVisitedPage;
use App\Domain\Entity\PaginationParameter;
use App\Domain\Entity\Property\BuildingUnitFilter;
use App\Domain\Entity\Property\BuildingUnitSearch;
use App\Domain\Entity\SortingOption\SortingOption;
use App\Domain\Property\BuildingUnitSecurityService;
use App\Domain\Property\BuildingUnitService;
use App\Domain\SessionStoredUrlParameter\SessionStoredUrlParameterService;
use App\Form\Type\PropertyVacancyManagement\Property\BuildingUnitFilterType;
use App\Form\Type\PropertyVacancyManagement\Property\BuildingUnitSearchType;
use App\SessionStorage\SessionStorageService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Security("is_granted('ROLE_USER') or is_granted('ROLE_PROPERTY_FEEDER') or is_granted('ROLE_VIEWER') or is_granted('ROLE_RESTRICTED_VIEWER') or is_granted('ROLE_PROPERTY_PROVIDER')")]
#[Route('/besatz-und-leerstand/archiv/objekte', name: 'property_vacancy_management_building_unit_archive_')]
class ArchiveOverviewController extends AbstractLeAnController
{
    private const ACTIVE_NAV_GROUP = 'property_vacancy_management';
    private const ACTIVE_NAV_ITEM = self::ACTIVE_NAV_GROUP . '_building_unit_archive';

    private const URL_PARAMETER_OBJECT_CLASSES = [
        PaginationParameter::class,
        SortingOption::class,
    ];

    public function __construct(
        SessionStorageService $sessionStorageService,
        SessionStoredUrlParameterService $sessionStoredUrlParameterService,
        private readonly BuildingUnitService $buildingUnitService,
        private readonly BuildingUnitSecurityService $buildingUnitSecurityService
    ) {
        parent::__construct(
            sessionStorageService: $sessionStorageService,
            sessionStoredUrlParameterService: $sessionStoredUrlParameterService
        );
    }

    #[Route('/karte', name: 'overview_map', methods: ['GET'])]
    public function overviewMap(Request $request, UserInterface $user): Response
    {
        $ownRouteName = $request->attributes->get(key: '_route');

        $lastVisitedPage = new LastVisitedPage(routeName: $ownRouteName);

        $this->storeObjectInSession(object: $lastVisitedPage, sessionKeyName: 'buildingUnitArchiveOverview');

        return $this->render(view: 'property_vacancy_management/building_unit/archive/overview_map.html.twig', parameters: [
            'geolocationPoint'      => $user->getAccount()->getAssignedPlace()->getGeolocationPoint(),
            'canCreateBuildingUnit' => $this->buildingUnitSecurityService->canCreateBuildingUnit(),
            'pageTitle'             => 'Leerstandsmanagement - Archiv – Kartenansicht',
            'activeNavGroup'        => self::ACTIVE_NAV_GROUP,
            'activeNavItem'         => self::ACTIVE_NAV_ITEM,
        ]);
    }

    #[Route('/galerie', name: 'overview_gallery', methods: ['GET', 'POST'])]
    public function overviewGallery(Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $sessionKeyName = 'buildingUnitArchiveOverview';
        $ownRouteName = $request->attributes->get(key: '_route');

        $paginationParameter = PaginationParameter::createFromRequest(request: $request, amountEntriesPerPage: 12);

        $this->storeObjectInSession(object: $paginationParameter, sessionKeyName: $sessionKeyName);

        $sortingOption = null;

        if ($request->query->get(key: 'sortieren') !== null) {
            $sortingOption = SortingOption::createFromRequest(request: $request);

            $this->storeObjectInSession(object: $sortingOption, sessionKeyName: $sessionKeyName);
        }

        $buildingUnitFilterForm = $this->createForm(type: BuildingUnitFilterType::class, options: [
            'account'        => $account,
            'dataClassName'  => BuildingUnitFilter::class,
            'sessionKeyName' => $sessionKeyName,
        ]);

        $buildingUnitFilterForm->add(child: 'apply', type: SubmitType::class);

        $buildingUnitFilterForm->handleRequest(request: $request);

        $buildingUnitSearchForm = $this->createForm(type: BuildingUnitSearchType::class, options: [
            'dataClassName'  => BuildingUnitSearch::class,
            'sessionKeyName' => $sessionKeyName,
        ]);

        $buildingUnitSearchForm->add(child: 'search', type: SubmitType::class);

        $buildingUnitSearchForm->handleRequest(request: $request);

        $urlParameters = $this->fetchMergedUrlParametersFromSession(
            urlParameterObjectClasses: self::URL_PARAMETER_OBJECT_CLASSES,
            sessionKeyName: $sessionKeyName
        );

        $lastVisitedPage = new LastVisitedPage(routeName: $ownRouteName, parameters: $urlParameters);

        $this->storeObjectInSession(object: $lastVisitedPage, sessionKeyName: $sessionKeyName);

        $buildingUnits = $this->buildingUnitService->fetchBuildingUnitsPaginatedByAccount(
            account: $account,
            withFromSubAccounts: true,
            archived: true,
            firstResult: $paginationParameter->getFirstResult(),
            maxResults: $paginationParameter->getLimit(),
            buildingUnitFilter: $buildingUnitFilterForm->getData(),
            buildingUnitSearch: $buildingUnitSearchForm->getData(),
            sortingOption: $sortingOption
        );

        return $this->render(view: 'property_vacancy_management/building_unit/archive/overview_gallery.html.twig', parameters: [
            'buildingUnitFilterForm' => $buildingUnitFilterForm,
            'buildingUnitSearchForm' => $buildingUnitSearchForm,
            'buildingUnits'          => $buildingUnits,
            'buildingUnitFilter'     => $buildingUnitFilterForm->getData(),
            'canCreateBuildingUnit'  => $this->buildingUnitSecurityService->canCreateBuildingUnit(),
            'pageTitle'              => 'Leerstandsmanagement - Archiv – Galerieansicht',
            'activeNavGroup'         => self::ACTIVE_NAV_GROUP,
            'activeNavItem'          => self::ACTIVE_NAV_ITEM,
        ]);
    }

    #[Route('/liste', name: 'overview_list', methods: ['GET', 'POST'])]
    public function overviewList(Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $sessionKeyName = 'buildingUnitArchiveOverview';
        $ownRouteName = $request->attributes->get(key: '_route');

        $paginationParameter = PaginationParameter::createFromRequest(request: $request);

        $this->storeObjectInSession(object: $paginationParameter, sessionKeyName: $sessionKeyName);

        $sortingOption = null;

        if ($request->query->get(key: 'sortieren') !== null) {
            $sortingOption = SortingOption::createFromRequest(request: $request);

            $this->storeObjectInSession(object: $sortingOption, sessionKeyName: $sessionKeyName);
        }

        $buildingUnitFilterForm = $this->createForm(type: BuildingUnitFilterType::class, options: [
            'account'        => $account,
            'dataClassName'  => BuildingUnitFilter::class,
            'sessionKeyName' => $sessionKeyName,
        ]);

        $buildingUnitFilterForm->add(child: 'apply', type: SubmitType::class);

        $buildingUnitFilterForm->handleRequest(request: $request);

        $buildingUnitSearchForm = $this->createForm(type: BuildingUnitSearchType::class, options: [
            'dataClassName'  => BuildingUnitSearch::class,
            'sessionKeyName' => $sessionKeyName,
        ]);

        $buildingUnitSearchForm->add(child: 'search', type: SubmitType::class);

        $buildingUnitSearchForm->handleRequest(request: $request);

        $urlParameters = $this->fetchMergedUrlParametersFromSession(
            urlParameterObjectClasses: self::URL_PARAMETER_OBJECT_CLASSES,
            sessionKeyName: $sessionKeyName
        );

        $lastVisitedPage = new LastVisitedPage(routeName: $ownRouteName, parameters: $urlParameters);

        $this->storeObjectInSession(object: $lastVisitedPage, sessionKeyName: $sessionKeyName);

        $buildingUnits = $this->buildingUnitService->fetchBuildingUnitsPaginatedByAccount(
            account: $account,
            withFromSubAccounts: true,
            archived: true,
            firstResult: $paginationParameter->getFirstResult(),
            maxResults: $paginationParameter->getLimit(),
            buildingUnitFilter: $buildingUnitFilterForm->getData(),
            buildingUnitSearch: $buildingUnitSearchForm->getData(),
            sortingOption: $sortingOption
        );

        return $this->render(view: 'property_vacancy_management/building_unit/archive/overview_list.html.twig', parameters: [
            'buildingUnitFilterForm' => $buildingUnitFilterForm,
            'buildingUnitSearchForm' => $buildingUnitSearchForm,
            'buildingUnits'          => $buildingUnits,
            'buildingUnitFilter'     => $buildingUnitFilterForm->getData(),
            'canCreateBuildingUnit'  => $this->buildingUnitSecurityService->canCreateBuildingUnit(),
            'pageTitle'              => 'Leerstandsmanagement - Archiv – Listenansicht',
            'activeNavGroup'         => self::ACTIVE_NAV_GROUP,
            'activeNavItem'          => self::ACTIVE_NAV_ITEM,
        ]);
    }
}
