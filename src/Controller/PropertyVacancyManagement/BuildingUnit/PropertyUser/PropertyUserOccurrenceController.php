<?php

declare(strict_types=1);

namespace App\Controller\PropertyVacancyManagement\BuildingUnit\PropertyUser;

use App\Controller\PropertyVacancyManagement\BuildingUnit\AbstractOccurrenceController;
use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\Person\Occurrence;
use App\Domain\Entity\Property\BuildingUnit;
use App\Domain\Entity\Property\PropertyUser;
use App\Domain\Person\OccurrenceDeletionService;
use App\Domain\Person\PersonService;
use App\Domain\Property\BuildingUnitService;
use App\Form\Type\PropertyVacancyManagement\Property\PropertyUserOccurrenceType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Security("is_granted('ROLE_USER') or is_granted('ROLE_PROPERTY_FEEDER') or is_granted('ROLE_VIEWER')")]
#[Route('/besatz-und-leerstand/objekte/{buildingUnitId<\d{1,10}>}/nutzer/vorgaenge', name: 'property_vacancy_management_building_unit_property_user_occurrence_')]
class PropertyUserOccurrenceController extends AbstractOccurrenceController
{
    public function __construct(
        BuildingUnitService $buildingUnitService,
        PersonService $personService,
        OccurrenceDeletionService $occurrenceDeletionService,
        EntityManagerInterface $entityManager
    ) {
        parent::__construct(
            buildingUnitService: $buildingUnitService,
            personService: $personService,
            occurrenceDeletionService: $occurrenceDeletionService,
            entityManager: $entityManager
        );
    }

    #[Security("is_granted('ROLE_USER') or is_granted('ROLE_PROPERTY_FEEDER')")]
    #[Route('/eintragen', name: 'create', methods: ['POST', 'GET'])]
    public function create(int $buildingUnitId, Request $request, UserInterface $user): Response
    {
        return parent::create(buildingUnitId: $buildingUnitId, request: $request, user: $user);
    }

    #[Route('/{occurrenceId<\d{1,10}>}', name: 'edit', methods: ['POST', 'GET'])]
    public function edit(int $buildingUnitId, int $occurrenceId, Request $request, UserInterface $user): Response
    {
        return parent::edit(buildingUnitId: $buildingUnitId, occurrenceId: $occurrenceId, request: $request, user: $user);
    }

    #[Security("is_granted('ROLE_USER') or is_granted('ROLE_PROPERTY_FEEDER')")]
    #[Route('/{occurrenceId<\d{1,10}>}/loeschen', name: 'delete', methods: ['GET', 'POST'])]
    public function delete(int $buildingUnitId, int $occurrenceId, Request $request, UserInterface $user): Response
    {
        return parent::delete(buildingUnitId: $buildingUnitId, occurrenceId: $occurrenceId, request: $request, user: $user);
    }

    protected function createOccurrenceForm(Occurrence $occurrence,BuildingUnit $buildingUnit, AccountUser $accountUser, bool $canEdit): FormInterface
    {
        return $this->createForm(type: PropertyUserOccurrenceType::class, data: $occurrence, options: [
            'propertyUsers' => array_map(
                callback: function (PropertyUser $propertyUser): PropertyUser {
                    return $propertyUser;
                },
                array: $buildingUnit->getPropertyUsers()->toArray()
            ),
            'accountUser' => $accountUser,
            'canEdit'     => $canEdit,
        ]);
    }

    protected function getOverviewPageRouteName(): string
    {
        return 'property_vacancy_management_building_unit_property_user_overview';
    }

    protected function getDeletePageRouteName(): string
    {
        return 'property_vacancy_management_building_unit_property_user_occurrence_delete';
    }

    protected function getOccurrenceCreatePageTitle(): string
    {
        return 'Leerstandsmanagement - Objektinformationen - Nutzer - Vorgang eintragen';
    }

    protected function getOccurrenceCreateTemplateView(): string
    {
        return 'property_vacancy_management/building_unit/property_user/occurrence/create.html.twig';
    }

    protected function getOccurrenceEditPageTitle(): string
    {
        return 'Leerstandsmanagement - Objektinformationen - Nutzer - Vorgang bearbeiten';
    }

    protected function getOccurrenceEditTemplateView(): string
    {
        return 'property_vacancy_management/building_unit/property_user/occurrence/edit.html.twig';
    }

    protected function getOccurrenceDeleteTemplateView(): string
    {
        return 'property_vacancy_management/building_unit/property_user/occurrence/delete.html.twig';
    }
}
