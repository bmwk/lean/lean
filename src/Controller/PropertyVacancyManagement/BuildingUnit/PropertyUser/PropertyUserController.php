<?php

declare(strict_types=1);

namespace App\Controller\PropertyVacancyManagement\BuildingUnit\PropertyUser;

use App\Domain\DocumentTemplate\DocumentTemplateService;
use App\Domain\Entity\FeedbackRelationship;
use App\Domain\Entity\Property\PropertyUser;
use App\Domain\Feedback\FeedbackService;
use App\Domain\Person\PersonService;
use App\Domain\Property\BuildingUnitDeletionService;
use App\Domain\Property\BuildingUnitNotificationService;
use App\Domain\Property\BuildingUnitService;
use App\Domain\Property\PropertyUserService;
use App\Form\Type\PropertyVacancyManagement\Property\PropertyUserRemoveType;
use App\Form\Type\PropertyVacancyManagement\Property\PropertyUserType;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\UnitOfWork;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Security("is_granted('ROLE_USER') or is_granted('ROLE_PROPERTY_FEEDER') or is_granted('ROLE_VIEWER') or is_granted('ROLE_RESTRICTED_VIEWER')")]
#[Route('/besatz-und-leerstand/objekte/{buildingUnitId<\d{1,10}>}/nutzer', name: 'property_vacancy_management_building_unit_property_user_')]
class PropertyUserController extends AbstractController
{
    private const ACTIVE_NAV_GROUP = 'property_vacancy_management';
    private const ACTIVE_NAV_ITEM = self:: ACTIVE_NAV_GROUP . '_building_unit';

    public function __construct(
        private readonly BuildingUnitService $buildingUnitService,
        private readonly BuildingUnitDeletionService $buildingUnitDeletionService,
        private readonly BuildingUnitNotificationService $buildingUnitNotificationService,
        private readonly PropertyUserService $propertyUserService,
        private readonly PersonService $personService,
        private readonly FeedbackService $feedbackService,
        private readonly DocumentTemplateService $documentTemplateService,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    #[Route('/uebersicht', name: 'overview', methods: ['GET'])]
    public function overview(int $buildingUnitId, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $buildingUnit = $this->buildingUnitService-> fetchBuildingUnitById(account: $account, withFromSubAccounts: true, id: $buildingUnitId);

        if ($buildingUnit === null) {
            throw new NotFoundHttpException();
        }

        if ($buildingUnit->getAccount()->getParentAccount() !== null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'view', subject: $buildingUnit);

        return $this->render(view: 'property_vacancy_management/building_unit/property_user/overview.html.twig', parameters: [
            'buildingUnit' => $buildingUnit,
            'occurrences'  => $this->personService->fetchOccurrencesByPropertyUsersFromBuildingUnit(
                account: $account,
                withFromSubAccounts: false,
                buildingUnit: $buildingUnit
            ),
            'feedbackGroups'        => $this->feedbackService->fetchFeedbackGroupsByFeedbackRelationship(feedbackRelationship: FeedbackRelationship::PROPERTY_USER),
            'buildingUnitFeedbacks' => $this->feedbackService->fetchBuildingUnitFeedbacksByBuildingUnit(buildingUnit: $buildingUnit),
            'documentTemplates'     => $this->documentTemplateService->fetchDocumentTemplatesByAccount(account: $account),
            'pageTitle'             => 'Leerstandsmanagement - Objektinformationen - Nutzer',
            'activeNavGroup'        => self::ACTIVE_NAV_GROUP,
            'activeNavItem'         => self::ACTIVE_NAV_ITEM . ($buildingUnit->isArchived() ? '_archive' : ''),
        ]);
    }

    #[Security("is_granted('ROLE_USER') or is_granted('ROLE_PROPERTY_FEEDER')")]
    #[Route('/anlegen', name: 'create', methods: ['GET', 'POST'])]
    public function create(int $buildingUnitId, Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $buildingUnit = $this->buildingUnitService->fetchBuildingUnitById(account: $account, withFromSubAccounts: true, id: $buildingUnitId);

        if ($buildingUnit === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'edit', subject: $buildingUnit);

        $propertyUser = new PropertyUser();

        $propertyUserForm = $this->createForm(type: PropertyUserType::class, data: $propertyUser, options: [
            'account'        => $account,
            'selectedPerson' => null,
            'canEdit'        => $this->isGranted(attribute: 'edit', subject: $buildingUnit),
            'canViewPerson'  => true,
        ]);

        $propertyUserForm->add(child: 'save', type: SubmitType::class);

        $propertyUserForm->handleRequest(request: $request);

        if ($propertyUserForm->isSubmitted() && $propertyUserForm->isValid()) {
            $this->entityManager->beginTransaction();

            try {
                $propertyUser = $this->propertyUserService->createPropertyUserFromPropertyUserForm(propertyUserForm: $propertyUserForm);
            } catch (\RuntimeException $exception) {
                throw new BadRequestException();
            }

            $propertyUser
                ->setAccount($account)
                ->setCreatedByAccountUser($user);

            $unitOfWork = $this->entityManager->getUnitOfWork();

            if ($unitOfWork->getEntityState($propertyUser->getPerson()) === UnitOfWork::STATE_NEW) {
                $propertyUser->getPerson()
                    ->setAccount($account)
                    ->setCreatedByAccountUser($user)
                    ->setDeleted(false)
                    ->setAnonymized(false);

                $this->entityManager->persist($propertyUser->getPerson());
            }

            $this->entityManager->persist(entity: $propertyUser);

            $buildingUnit->getPropertyUsers()->add($propertyUser);

            $this->entityManager->flush();
            $this->entityManager->commit();

            $this->addFlash(type: 'dataSaved', message: 'Der Nutzer wurde angelegt.');

            return $this->redirectToRoute(route: 'property_vacancy_management_building_unit_property_user_overview', parameters: ['buildingUnitId' => $buildingUnitId]);
        }

        return $this->render(view: 'property_vacancy_management/building_unit/property_user/create.html.twig', parameters: [
            'propertyUserForm' => $propertyUserForm,
            'buildingUnit'     => $buildingUnit,
            'pageTitle'        => 'Leerstandsmanagement - Objektinformationen - Nutzer erfassen',
            'activeNavGroup'   => self::ACTIVE_NAV_GROUP,
            'activeNavItem'    => self::ACTIVE_NAV_ITEM . ($buildingUnit->isArchived() ? '_archive' : ''),
        ]);
    }

    #[Route('/{propertyUserId<\d{1,10}>}', name: 'edit', methods: ['GET', 'POST'])]
    public function edit(int $buildingUnitId, int $propertyUserId, Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $buildingUnit = $this->buildingUnitService->fetchBuildingUnitById(account:$account, withFromSubAccounts: true, id: $buildingUnitId);

        if ($buildingUnit === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'view', subject: $buildingUnit);

        $propertyUser = $this->propertyUserService->fetchPropertyUserByIdAndBuildingUnit(
            account: $account,
            withFromSubAccounts: true,
            id: $propertyUserId,
            buildingUnit: $buildingUnit
        );

        if ($propertyUser === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'view', subject: $propertyUser);

        $propertyUserForm = $this->createForm(type: PropertyUserType::class, data: $propertyUser, options: [
            'account'        => $account,
            'selectedPerson' => $propertyUser->getPerson(),
            'canEdit'        => $this->isGranted(attribute: 'edit', subject: $buildingUnit),
            'canViewPerson'  => $this->isGranted(attribute: 'view', subject: $propertyUser->getPerson()),
        ]);

        $industryNaceClassificationForm = $propertyUserForm->get('industryNaceClassification');

        if ($propertyUser->getNaceClassification() !== null) {
            $industryNaceClassificationForm->get('naceClassificationId')->setData($propertyUser->getNaceClassification()->getId());
        }

        if ($propertyUser->getIndustryClassification() !== null) {
            $industryNaceClassificationForm->get('industryClassificationId')->setData($propertyUser->getIndustryClassification()->getId());
        }

        $propertyUserForm->add(child: 'save', type: SubmitType::class);

        $propertyUserForm->handleRequest(request: $request);

        if ($propertyUserForm->isSubmitted() && $propertyUserForm->isValid()) {
            $this->denyAccessUnlessGranted(attribute: 'edit', subject: $buildingUnit);
            $this->denyAccessUnlessGranted(attribute: 'edit', subject: $propertyUser);

            $unitOfWork = $this->entityManager->getUnitOfWork();
            $this->entityManager->beginTransaction();

            try {
                $propertyUser = $this->propertyUserService->createPropertyUserFromPropertyUserForm(propertyUserForm: $propertyUserForm);
            } catch (\RuntimeException $exception) {
                throw new BadRequestException();
            }

            if ($unitOfWork->getEntityState($propertyUser->getPerson()) === UnitOfWork::STATE_NEW) {
                $propertyUser->getPerson()
                    ->setAccount($account)
                    ->setCreatedByAccountUser($user)
                    ->setDeleted(false)
                    ->setAnonymized(false);

                $this->entityManager->persist(entity: $propertyUser->getPerson());
            }

            $unitOfWork->computeChangeSets();

            $changeSet = $unitOfWork->getEntityChangeSet($propertyUser);

            $this->entityManager->flush();
            $this->entityManager->commit();

            if (array_key_exists(key: 'industryClassification', array: $changeSet)) {
                $this->buildingUnitNotificationService->createBuildingUnitUsageChangeNotifications(buildingUnit: $buildingUnit, changedByAccountUser: $user);
            }

            $this->addFlash(type: 'dataSaved', message: 'Die Nutzerinformationen wurden bearbeitet.');

            return $this->redirectToRoute(route: 'property_vacancy_management_building_unit_property_user_overview', parameters: ['buildingUnitId' => $buildingUnitId]);
        }

        return $this->render(view: 'property_vacancy_management/building_unit/property_user/edit.html.twig', parameters: [
            'propertyUserForm' => $propertyUserForm,
            'buildingUnit'     => $buildingUnit,
            'propertyUser'     => $propertyUser,
            'pageTitle'        => 'Leerstandsmanagement - Objektinformationen - Nutzer bearbeiten',
            'activeNavGroup'   => self::ACTIVE_NAV_GROUP,
            'activeNavItem'    => self::ACTIVE_NAV_ITEM . ($buildingUnit->isArchived() ? '_archive' : ''),
        ]);
    }

    #[Security("is_granted('ROLE_USER') or is_granted('ROLE_PROPERTY_FEEDER')")]
    #[Route('/{propertyUserId<\d{1,10}>}/verknuepfung-aufheben', name: 'remove', methods: ['GET', 'POST'])]
    public function remove(int $buildingUnitId, int $propertyUserId, Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $buildingUnit = $this->buildingUnitService->fetchBuildingUnitById(account: $account, withFromSubAccounts: true, id: $buildingUnitId);

        if ($buildingUnit === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'edit', subject: $buildingUnit);

        $propertyUser = $this->propertyUserService->fetchPropertyUserByIdAndBuildingUnit(
            account: $account,
            withFromSubAccounts: true,
            id: $propertyUserId,
            buildingUnit: $buildingUnit
        );

        if ($propertyUser === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'delete', subject: $propertyUser);

        $propertyUserRemoveForm = $this->createForm(type: PropertyUserRemoveType::class);

        $propertyUserRemoveForm->add(child:'remove', type: SubmitType::class);

        $propertyUserRemoveForm->handleRequest(request: $request);

        if ($propertyUserRemoveForm->isSubmitted() && $propertyUserRemoveForm->isValid()) {
            if ($propertyUserRemoveForm->get('verificationCode')->getData() === 'nutzer_' . $propertyUser->getId()) {
                $this->buildingUnitDeletionService->deletePropertyUser(propertyUser: $propertyUser, property: $buildingUnit);

                $this->addFlash(type: 'dataDeleted', message: 'Der Nutzer wurde vom Objekt gelöscht.');

                return $this->redirectToRoute(route: 'property_vacancy_management_building_unit_property_user_overview', parameters: ['buildingUnitId' => $buildingUnitId]);
            }
        }

        $propertyUserForm = $this->createForm(type: PropertyUserType::class, data: $propertyUser, options: [
            'account'        => $account,
            'selectedPerson' => $propertyUser->getPerson(),
            'canEdit'        => false,
            'canViewPerson'  => true,
        ]);

        return $this->render(view: 'property_vacancy_management/building_unit/property_user/remove.html.twig', parameters: [
            'propertyUserRemoveForm' => $propertyUserRemoveForm,
            'propertyUserForm'       => $propertyUserForm,
            'buildingUnit'           => $buildingUnit,
            'propertyUser'           => $propertyUser,
            'pageTitle'              => 'Leerstandsmanagement - Objektinformationen - Nutzer entfernen',
            'activeNavGroup'         => self::ACTIVE_NAV_GROUP,
            'activeNavItem'          => self::ACTIVE_NAV_ITEM . ($buildingUnit->isArchived() ? '_archive' : ''),
        ]);
    }
}
