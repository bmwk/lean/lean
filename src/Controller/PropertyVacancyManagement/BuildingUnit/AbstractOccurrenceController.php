<?php

declare(strict_types=1);

namespace App\Controller\PropertyVacancyManagement\BuildingUnit;

use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\DeletionType;
use App\Domain\Entity\Person\Occurrence;
use App\Domain\Entity\Person\Person;
use App\Domain\Entity\Property\BuildingUnit;
use App\Domain\Person\OccurrenceDeletionService;
use App\Domain\Person\PersonService;
use App\Domain\Property\BuildingUnitService;
use App\Form\Type\PersonManagement\Occurrence\OccurrenceDeleteType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\User\UserInterface;

abstract class AbstractOccurrenceController extends AbstractController
{
    private const ACTIVE_NAV_GROUP = 'property_vacancy_management';
    private const ACTIVE_NAV_ITEM = self::ACTIVE_NAV_GROUP . '_building_unit';

    protected function __construct(
        private readonly BuildingUnitService $buildingUnitService,
        private readonly PersonService $personService,
        private readonly OccurrenceDeletionService $occurrenceDeletionService,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    protected function create(int $buildingUnitId, Request $request, UserInterface $user): Response
    {
        $buildingUnit = $this->buildingUnitService->fetchBuildingUnitById(
            account: $user->getAccount(),
            withFromSubAccounts: true,
            id: $buildingUnitId
        );

        if ($buildingUnit === null) {
            throw new NotFoundHttpException();
        }

        if ($buildingUnit->getAccount()->getParentAccount() !== null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'edit', subject: $buildingUnit);

        $occurrence = new Occurrence();

        $occurrence->setPerson(new Person());

        $occurrenceForm = $this->createOccurrenceForm(
            occurrence: $occurrence,
            buildingUnit: $buildingUnit,
            accountUser: $user,
            canEdit: $this->isGranted(attribute: 'edit', subject: $buildingUnit)
        );

        $occurrenceForm->add(child: 'save', type: SubmitType::class);

        $occurrenceForm->handleRequest(request: $request);

        if ($occurrenceForm->isSubmitted() && $occurrenceForm->isValid()) {
            $occurrence
                ->setAccount($user->getAccount())
                ->setCreatedByAccountUser($user)
                ->setPerformedWithAccountUser($user)
                ->setDeleted(false)
                ->setAnonymized(false);

            $this->entityManager->persist(entity: $occurrence);

            $this->entityManager->flush();

            $this->addFlash(type: 'dataSaved', message: 'Der Vorgang wurde eingetragen.');

            return $this->redirectToRoute(route:  $this->getOverviewPageRouteName(), parameters: ['buildingUnitId' => $buildingUnitId]);
        }

        return $this->render(view: $this->getOccurrenceCreateTemplateView(), parameters: [
            'occurrenceForm' => $occurrenceForm,
            'buildingUnit'   => $buildingUnit,
            'pageTitle'      => $this->getOccurrenceCreatePageTitle(),
            'activeNavGroup' => self::ACTIVE_NAV_GROUP,
            'activeNavItem'  => self::ACTIVE_NAV_ITEM,
        ]);
    }

    protected function edit(int $buildingUnitId, int $occurrenceId, Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $buildingUnit = $this->buildingUnitService->fetchBuildingUnitById(
            account: $account,
            withFromSubAccounts: true,
            id: $buildingUnitId
        );

        if ($buildingUnit === null) {
            throw new NotFoundHttpException();
        }

        if ($buildingUnit->getAccount()->getParentAccount() !== null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'view', subject: $buildingUnit);

        $occurrence = $this->personService->fetchOccurrenceById(
            account: $account,
            withFromSubAccounts: false,
            id: $occurrenceId
        );

        if ($occurrence === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'view', subject: $occurrence);

        $occurrenceForm = $this->createOccurrenceForm(
            occurrence: $occurrence,
            buildingUnit: $buildingUnit,
            accountUser: $occurrence->getPerformedWithAccountUser(),
            canEdit: $this->isGranted(attribute: 'edit', subject: $occurrence)
        );

        $occurrenceForm->add(child: 'save', type: SubmitType::class);

        $occurrenceForm->handleRequest(request: $request);

        if ($occurrenceForm->isSubmitted() && $occurrenceForm->isValid()) {
            $this->denyAccessUnlessGranted(attribute: 'edit', subject: $buildingUnit);
            $this->denyAccessUnlessGranted(attribute: 'edit', subject: $occurrence);

            $this->entityManager->flush();

            $this->addFlash(type: 'dataSaved', message: 'Der Vorgang wurde bearbeitet.');

            return $this->redirectToRoute(route: $this->getOverviewPageRouteName(), parameters: ['buildingUnitId' => $buildingUnitId]);
        }

        return $this->render(view: $this->getOccurrenceEditTemplateView(), parameters: [
            'occurrenceForm' => $occurrenceForm,
            'buildingUnit'   => $buildingUnit,
            'occurrence'     => $occurrence,
            'pageTitle'      => $this->getOccurrenceEditPageTitle(),
            'activeNavGroup' => self::ACTIVE_NAV_GROUP,
            'activeNavItem'  => self::ACTIVE_NAV_ITEM,
        ]);
    }

    protected function delete(int $buildingUnitId, int $occurrenceId, Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $buildingUnit = $this->buildingUnitService->fetchBuildingUnitById(
            account: $account,
            withFromSubAccounts: true,
            id: $buildingUnitId
        );

        if ($buildingUnit === null) {
            throw new NotFoundHttpException();
        }

        if ($buildingUnit->getAccount()->getParentAccount() !== null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'edit', subject: $buildingUnit);

        $occurrence = $this->personService->fetchOccurrenceById(
            account: $account,
            withFromSubAccounts: false,
            id: $occurrenceId
        );

        if ($occurrence === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'delete', subject: $occurrence);

        $occurrenceDeleteForm = $this->createForm(type: OccurrenceDeleteType::class);

        $occurrenceDeleteForm->add(child: 'delete', type: SubmitType::class);

        $occurrenceDeleteForm->handleRequest(request: $request);

        if ($occurrenceDeleteForm->isSubmitted() && $occurrenceDeleteForm->isValid()) {
            if ($occurrenceDeleteForm->get('verificationCode')->getData() !== 'vorgang_' . $occurrence->getId()) {
                $this->addFlash(type: 'dataError', message: 'Der Vorgang konnte nicht gelöscht werden, da der Bestätigungscode falsch eingegeben wurde.');

                return $this->redirectToRoute(route: $this->getDeletePageRouteName(), parameters: ['buildingUnitId' => $buildingUnitId, 'occurrenceId' => $occurrenceId]);
            }

            $this->occurrenceDeletionService->deleteOccurrence(occurrence: $occurrence, deletionType: DeletionType::SOFT, deletedByAccountUser: $user);

            $this->addFlash(type: 'dataDeleted', message: 'Der Vorgang wurde gelöscht.');

            return $this->redirectToRoute(route: $this->getOverviewPageRouteName(), parameters: ['buildingUnitId' => $buildingUnitId]);
        }

        $occurrenceForm = $this->createOccurrenceForm(
            occurrence: $occurrence,
            buildingUnit: $buildingUnit,
            accountUser: $occurrence->getPerformedWithAccountUser(),
            canEdit: false
        );

        return $this->render(view: $this->getOccurrenceDeleteTemplateView(), parameters: [
            'occurrenceDeleteForm' => $occurrenceDeleteForm,
            'occurrenceForm'       => $occurrenceForm,
            'buildingUnit'         => $buildingUnit,
            'occurrence'           => $occurrence,
            'pageTitle'            => $this->getOccurrenceEditPageTitle(),
            'activeNavGroup'       => self::ACTIVE_NAV_GROUP,
            'activeNavItem'        => self::ACTIVE_NAV_ITEM,
        ]);
    }

    abstract protected function createOccurrenceForm(Occurrence $occurrence, BuildingUnit $buildingUnit, AccountUser $accountUser, bool $canEdit): FormInterface;

    abstract protected function getOverviewPageRouteName(): string;

    abstract protected function getDeletePageRouteName(): string;

    abstract protected function getOccurrenceCreatePageTitle(): string;

    abstract protected function getOccurrenceCreateTemplateView(): string;

    abstract protected function getOccurrenceEditPageTitle(): string;

    abstract protected function getOccurrenceEditTemplateView(): string;

    abstract protected function getOccurrenceDeleteTemplateView(): string;
}
