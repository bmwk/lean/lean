<?php

declare(strict_types=1);

namespace App\Controller\PropertyVacancyManagement\BuildingUnit;

use App\Domain\Document\DocumentService;
use App\Domain\Property\BuildingUnitDeletionService;
use App\Domain\Property\BuildingUnitService;
use App\Form\Type\PropertyVacancyManagement\Property\DocumentDeleteType;
use App\Form\Type\PropertyVacancyManagement\Property\DocumentType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Security("is_granted('ROLE_USER') or is_granted('ROLE_PROPERTY_FEEDER') or is_granted('ROLE_VIEWER') or is_granted('ROLE_RESTRICTED_VIEWER') or is_granted('ROLE_PROPERTY_PROVIDER')")]
#[Route('/besatz-und-leerstand/objekte/{buildingUnitId<\d{1,10}>}/dokumente', name: 'property_vacancy_management_building_unit_document_')]
class DocumentController extends AbstractController
{
    private const ACTIVE_NAV_GROUP = 'property_vacancy_management';
    private const ACTIVE_NAV_ITEM = self::ACTIVE_NAV_GROUP . '_building_unit';

    public function __construct(
        private readonly BuildingUnitService $buildingUnitService,
        private readonly BuildingUnitDeletionService $buildingUnitDeletionService,
        private readonly DocumentService $documentService,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    #[Route('/uebersicht', name: 'overview', methods: ['GET'])]
    public function overview(int $buildingUnitId, UserInterface $user): Response
    {
        $buildingUnit = $this->buildingUnitService->fetchBuildingUnitWithConnectedDocumentsById(
            account: $user->getAccount(),
            withFromSubAccounts: true,
            id: $buildingUnitId
        );

        if ($buildingUnit === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'view', subject:  $buildingUnit);

        return $this->render(view: 'property_vacancy_management/building_unit/document/overview.html.twig', parameters: [
            'buildingUnit'   => $buildingUnit,
            'pageTitle'      => 'Leerstandsmanagement - Objektinformationen - Dokumente',
            'activeNavGroup' => self::ACTIVE_NAV_GROUP,
            'activeNavItem'  => self::ACTIVE_NAV_ITEM . ($buildingUnit->isArchived() ? '_archive' : ''),
        ]);
    }

    #[Security("is_granted('ROLE_USER') or is_granted('ROLE_PROPERTY_FEEDER') or is_granted('ROLE_PROPERTY_PROVIDER')")]
    #[Route('/upload', name: 'upload', methods: ['POST'])]
    public function upload(int $buildingUnitId, Request $request, UserInterface $user): JsonResponse
    {
        $buildingUnit = $this->buildingUnitService->fetchBuildingUnitById(account: $user->getAccount(), withFromSubAccounts: true, id: $buildingUnitId);

        if ($buildingUnit === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'edit', subject: $buildingUnit);

        $documentFile = $request->files->get(key: 'documentFile');

        if ($documentFile === null) {
            throw new BadRequestHttpException();
        }

        $allowedFileMimeTypes = [
            'application/pdf',
            'application/msword',
            'application/msexcel',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        ];

        if (in_array(needle: $documentFile->getMimeType(), haystack: $allowedFileMimeTypes) !== true) {
            throw new BadRequestHttpException();
        }

        $this->entityManager->beginTransaction();

        $document = $this->documentService->createAndSaveDocumentFromUploadedFile(
            uploadedFile: $documentFile,
            public: false,
            accountUser: $user,
            title: $documentFile->getClientOriginalName()
        );

        $buildingUnit->getDocuments()->add($document);

        $this->entityManager->flush();

        $this->entityManager->commit();

        $flashBag = $request->getSession()->getFlashBag();

        if ($flashBag->has(type: 'dataSaved') === false) {
            $this->addFlash(type: 'dataSaved', message: 'Dokument(e) wurde(n) hochgeladen.');
        }

        return new JsonResponse(data: ['success' => true]);
    }

    #[Route('/{documentId<\d{1,10}>}', name: 'edit', methods: ['GET', 'POST'])]
    public function edit(int $buildingUnitId, int $documentId, Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $buildingUnit = $this->buildingUnitService->fetchBuildingUnitById(account: $account, withFromSubAccounts: true, id: $buildingUnitId);

        if ($buildingUnit === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'view', subject: $buildingUnit);

        $document = $this->documentService->fetchDocumentByIdAndBuildingUnit(
            account: $account,
            withFromSubAccounts: true,
            id: $documentId,
            buildingUnit: $buildingUnit
        );

        if ($document === null) {
            throw new NotFoundHttpException();
        }

        $documentForm = $this->createForm(type: DocumentType::class, data: $document, options: [
            'canEdit' => $this->isGranted(attribute: 'edit', subject: $buildingUnit),
        ]);

        $documentForm->add(child: 'save', type: SubmitType::class);

        $documentForm->handleRequest(request: $request);

        if ($documentForm->isSubmitted() && $documentForm->isValid()) {
            $this->denyAccessUnlessGranted(attribute: 'edit', subject: $buildingUnit);

            $this->entityManager->flush();

            $this->addFlash(type: 'dataSaved', message: 'Die Dokumentinformationen wurden gespeichert.');

            return $this->redirectToRoute(route: 'property_vacancy_management_building_unit_document_overview', parameters: ['buildingUnitId' => $buildingUnit->getId()]);
        }

        return $this->render(view: 'property_vacancy_management/building_unit/document/edit.html.twig', parameters: [
            'documentForm'   => $documentForm,
            'document'       => $document,
            'buildingUnit'   => $buildingUnit,
            'pageTitle'      => 'Leerstandsmanagement - Objektinformationen - Dokumente',
            'activeNavGroup' => self::ACTIVE_NAV_GROUP,
            'activeNavItem'  => self::ACTIVE_NAV_ITEM . ($buildingUnit->isArchived() ? '_archive' : ''),
        ]);
    }

    #[Security("is_granted('ROLE_USER') or is_granted('ROLE_PROPERTY_FEEDER') or is_granted('ROLE_PROPERTY_PROVIDER')")]
    #[Route('/{documentId<\d{1,10}>}/loeschen', name: 'delete', methods: ['GET', 'POST'])]
    public function delete(int $buildingUnitId, int $documentId, Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $buildingUnit = $this->buildingUnitService->fetchBuildingUnitById(account: $account, withFromSubAccounts: false, id: $buildingUnitId);

        if ($buildingUnit === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'edit', subject: $buildingUnit);

        $document = $this->documentService->fetchDocumentByIdAndBuildingUnit(
            account: $account,
            withFromSubAccounts: false,
            id: $documentId,
            buildingUnit: $buildingUnit
        );

        if ($document === null) {
            throw new NotFoundHttpException();
        }

        $documentDeleteForm = $this->createForm(type: DocumentDeleteType::class);

        $documentDeleteForm->add(child: 'delete', type: SubmitType::class);

        $documentDeleteForm->handleRequest(request: $request);

        if (
            $documentDeleteForm->isSubmitted()
            && $documentDeleteForm->isValid()
            && $documentDeleteForm->get('verificationCode')->getData() === 'dokument_' . $document->getId()
        ) {
            $this->buildingUnitDeletionService->deleteDocument(document: $document, property: $buildingUnit);

            $this->addFlash(type: 'dataDeleted', message: 'Das Dokument wurde gelöscht.');

            return $this->redirectToRoute(route: 'property_vacancy_management_building_unit_document_overview', parameters: ['buildingUnitId' => $buildingUnit->getId()]);
        }

        $documentForm = $this->createForm(type: DocumentType::class, data: $document, options: ['canEdit' => false]);

        return $this->render(view: 'property_vacancy_management/building_unit/document/delete.html.twig', parameters: [
            'documentDeleteForm' => $documentDeleteForm,
            'documentForm'       => $documentForm,
            'document'           => $document,
            'buildingUnit'       => $buildingUnit,
            'pageTitle'          => 'Leerstandsmanagement - Dokument Löschen',
            'activeNavGroup'     => self::ACTIVE_NAV_GROUP,
            'activeNavItem'      => self::ACTIVE_NAV_ITEM . ($buildingUnit->isArchived() ? '_archive' : ''),
        ]);
    }
}
