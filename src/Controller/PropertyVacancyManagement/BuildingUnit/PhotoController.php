<?php

declare(strict_types=1);

namespace App\Controller\PropertyVacancyManagement\BuildingUnit;

use App\Domain\Image\ImageService;
use App\Domain\Property\BuildingUnitDeletionService;
use App\Domain\Property\BuildingUnitService;
use App\Form\Type\PropertyVacancyManagement\Property\PhotoDeleteType;
use App\Form\Type\PropertyVacancyManagement\Property\PhotoType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Security("is_granted('ROLE_USER') or is_granted('ROLE_PROPERTY_FEEDER') or is_granted('ROLE_VIEWER') or is_granted('ROLE_RESTRICTED_VIEWER') or is_granted('ROLE_PROPERTY_PROVIDER')")]
#[Route('/besatz-und-leerstand/objekte/{buildingUnitId<\d{1,10}>}/fotos', name: 'property_vacancy_management_building_unit_')]
class PhotoController extends AbstractController
{
    private const ACTIVE_NAV_GROUP = 'property_vacancy_management';
    private const ACTIVE_NAV_ITEM = self::ACTIVE_NAV_GROUP . '_building_unit';

    public function __construct(
        private readonly BuildingUnitService $buildingUnitService,
        private readonly BuildingUnitDeletionService $buildingUnitDeletionService,
        private readonly ImageService $imageService,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    #[Route('/uebersicht', name: 'photo_overview', methods: ['GET'])]
    public function overview(int $buildingUnitId, UserInterface $user): Response
    {
        $buildingUnit = $this->buildingUnitService->fetchBuildingUnitWithConnectedImagesById(
            account: $user->getAccount(),
            withFromSubAccounts: true,
            id: $buildingUnitId
        );

        if ($buildingUnit === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'view', subject: $buildingUnit);

        return $this->render(view: 'property_vacancy_management/building_unit/photo/overview.html.twig', parameters: [
            'buildingUnit'   => $buildingUnit,
            'pageTitle'      => 'Leerstandsmanagement - Objektinformationen - Fotos',
            'activeNavGroup' => self::ACTIVE_NAV_GROUP,
            'activeNavItem'  => self::ACTIVE_NAV_ITEM . ($buildingUnit->isArchived() ? '_archive' : ''),
        ]);
    }

    #[Security("is_granted('ROLE_USER') or is_granted('ROLE_PROPERTY_FEEDER') or is_granted('ROLE_PROPERTY_PROVIDER')")]
    #[Route('/upload', name: 'photo_upload', methods: ['POST'])]
    public function upload(int $buildingUnitId, Request $request, UserInterface $user): JsonResponse
    {
        $buildingUnit = $this->buildingUnitService->fetchBuildingUnitById(
            account: $user->getAccount(),
            withFromSubAccounts: true,
            id: $buildingUnitId
        );

        if ($buildingUnit === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'edit', subject: $buildingUnit);

        $imageFile = $request->files->get(key: 'imageFile');

        if ($imageFile === null) {
            throw new BadRequestHttpException();
        }

        $allowedFileMimeTypes = [
            'image/bmp',
            'image/cis-cod',
            'image/gif',
            'image/ief',
            'image/jpeg',
            'image/png',
            'image/pipeg',
            'image/svg+xml',
            'image/tiff',
            'image/x-cmu-raster',
            'image/x-cmx',
            'image/x-icon',
            'image/x-portable-anymap',
            'image/x-portable-bitmap',
            'image/x-portable-graymap',
            'image/x-portable-pixmap',
            'image/x-rgb',
            'image/x-xbitmap',
            'image/x-xpixmap',
            'image/x-xwindowdump',
        ];

        if (in_array(needle: $imageFile->getMimeType(), haystack: $allowedFileMimeTypes) !== true) {
            throw new BadRequestHttpException();
        }

        $this->entityManager->beginTransaction();

        $image = $this->imageService->createAndSaveImageFromFileContent(
            fileContent: $imageFile->getContent(),
            filename: $imageFile->getClientOriginalName(),
            fileExtension: $imageFile->guessExtension(),
            public: false,
            resized: true,
            withThumbnail: true,
            accountUser: $user
        );

        $images = $buildingUnit->getImages();

        if ($images->isEmpty() === true) {
            $buildingUnit->setMainImage($image);
        }

        $images->add($image);

        $this->entityManager->flush();
        $this->entityManager->commit();

        $flashBag = $request->getSession()->getFlashBag();

        if ($flashBag->has(type: 'dataSaved') === false) {
            $this->addFlash(type: 'dataSaved', message: 'Bild(er) wurde(n) hochgeladen.');
        }

        return new JsonResponse(data: ['success' => true]);
    }

    #[Route('/{imageId<\d{1,10}>}', name: 'photo', methods: ['GET', 'POST'])]
    public function edit(int $buildingUnitId, int $imageId, Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $buildingUnit = $this->buildingUnitService->fetchBuildingUnitById(account: $account, withFromSubAccounts: true, id: $buildingUnitId);

        if ($buildingUnit === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'view', subject: $buildingUnit);

        $image = $this->imageService->fetchImageByIdAndBuildingUnit(
            account: $account,
            withFromSubAccounts: true,
            id: $imageId,
            buildingUnit: $buildingUnit
        );

        if ($image === null) {
            throw new NotFoundHttpException();
        }

        $photoForm = $this->createForm(type: PhotoType::class, data: $image, options: [
            'canEdit' => $this->isGranted(attribute: 'edit', subject: $buildingUnit),
        ]);

        $photoForm->add(child: 'save', type: SubmitType::class);

        $photoForm->handleRequest(request: $request);

        if ($photoForm->isSubmitted() && $photoForm->isValid()) {
            $this->denyAccessUnlessGranted(attribute: 'edit', subject: $buildingUnit);

            $this->entityManager->flush();

            $this->addFlash(type: 'dataSaved', message: 'Die Bildinformationen wurden gespeichert.');

            return $this->redirectToRoute(route: 'property_vacancy_management_building_unit_photo_overview', parameters: ['buildingUnitId' => $buildingUnit->getId()]);
        }

        $currentIndex = $buildingUnit->getImages()->indexOf($image);

        if ($buildingUnit->getImages()->count() === 1) {
            $nextImageId = null;
            $prevImageId = null;
        } else {
            if ($currentIndex === $buildingUnit->getImages()->count() - 1) {
                $nextIndex = 0;
                $prevIndex = $currentIndex - 1;
            } elseif ($currentIndex === 0) {
                $nextIndex = 1;
                $prevIndex = $buildingUnit->getImages()->count() - 1;
            } else {
                $nextIndex = $currentIndex + 1;
                $prevIndex = $currentIndex - 1;
            }

            $nextImageId = $buildingUnit->getImages()[$nextIndex]->getId();
            $prevImageId = $buildingUnit->getImages()[$prevIndex]->getId();
        }

        return $this->render(view: 'property_vacancy_management/building_unit/photo/edit.html.twig', parameters: [
            'photoForm'      => $photoForm,
            'image'          => $image,
            'nextImageId'    => $nextImageId,
            'prevImageId'    => $prevImageId,
            'buildingUnit'   => $buildingUnit,
            'pageTitle'      => 'Leerstandsmanagement - Objektinformationen - Fotos',
            'activeNavGroup' => self::ACTIVE_NAV_GROUP,
            'activeNavItem'  => self::ACTIVE_NAV_ITEM . ($buildingUnit->isArchived() ? '_archive' : ''),
        ]);
    }

    #[Security("is_granted('ROLE_USER') or is_granted('ROLE_PROPERTY_FEEDER') or is_granted('ROLE_PROPERTY_PROVIDER')")]
    #[Route('/{imageId<\d{1,10}>}/als-hauptbild-setzen', name: 'photo_set_main_image', methods: ['GET'])]
    public function setMainImage(int $buildingUnitId, int $imageId, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $buildingUnit = $this->buildingUnitService->fetchBuildingUnitById(account: $account, withFromSubAccounts: false, id: $buildingUnitId);

        if ($buildingUnit === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'edit', subject: $buildingUnit);

        $image = $this->imageService->fetchImageByIdAndBuildingUnit(
            account: $account,
            withFromSubAccounts: false,
            id: $imageId,
            buildingUnit: $buildingUnit
        );

        $buildingUnit->setMainImage($image);

        $this->entityManager->flush();

        $this->addFlash(type: 'dataSaved', message: 'Das Hauptbild wurde geändert');

        return $this->redirectToRoute(route: 'property_vacancy_management_building_unit_photo_overview', parameters: ['buildingUnitId' => $buildingUnit->getId()]);
    }

    #[Security("is_granted('ROLE_USER') or is_granted('ROLE_PROPERTY_FEEDER') or is_granted('ROLE_PROPERTY_PROVIDER')")]
    #[Route('/{imageId<\d{1,10}>}/loeschen', name: 'photo_delete', methods: ['GET', 'POST'])]
    public function delete(int $buildingUnitId, int $imageId, Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $buildingUnit = $this->buildingUnitService->fetchBuildingUnitById(account: $account, withFromSubAccounts: false, id: $buildingUnitId);

        if ($buildingUnit === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'edit', subject: $buildingUnit);

        $image = $this->imageService->fetchImageByIdAndBuildingUnit(
            account: $account,
            withFromSubAccounts: false,
            id: $imageId,
            buildingUnit: $buildingUnit
        );

        if ($image === null) {
            throw new NotFoundHttpException();
        }

        $photoDeleteForm = $this->createForm(type: PhotoDeleteType::class);

        $photoDeleteForm->add(child: 'delete', type: SubmitType::class);

        $photoDeleteForm->handleRequest(request: $request);

        if (
            $photoDeleteForm->isSubmitted()
            && $photoDeleteForm->isValid()
            && $photoDeleteForm->get('verificationCode')->getData() === 'foto_' . $image->getId()
        ) {
            $this->buildingUnitDeletionService->deleteImage(image: $image, property: $buildingUnit);

            $this->addFlash(type: 'dataDeleted', message: 'Das Bild wurde gelöscht.');

            return $this->redirectToRoute(route: 'property_vacancy_management_building_unit_photo_overview', parameters: ['buildingUnitId' => $buildingUnit->getId()]);
        }

        $photoForm = $this->createForm(type: PhotoType::class, data: $image, options: ['canEdit' => false]);

        return $this->render(view: 'property_vacancy_management/building_unit/photo/delete.html.twig', parameters: [
            'photoDeleteForm' => $photoDeleteForm,
            'photoForm'       => $photoForm,
            'image'           => $image,
            'buildingUnit'    => $buildingUnit,
            'pageTitle'       => 'Leerstandsmanagement - Foto löschen',
            'activeNavGroup'  => self::ACTIVE_NAV_GROUP,
            'activeNavItem'   => self::ACTIVE_NAV_ITEM . ($buildingUnit->isArchived() ? '_archive' : ''),
        ]);
    }
}
