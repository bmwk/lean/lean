<?php

declare(strict_types=1);

namespace App\Controller\PropertyVacancyManagement\BuildingUnit;

use App\Domain\Entity\Property\PropertyMarketingInformation;
use App\Domain\Entity\Property\PropertyPricingInformation;
use App\Domain\Property\BuildingUnitNotificationService;
use App\Domain\Property\BuildingUnitService;
use App\Form\Type\PropertyVacancyManagement\Property\PropertyMarketingInformationType;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\UnitOfWork;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Security("is_granted('ROLE_USER') or is_granted('ROLE_PROPERTY_FEEDER') or is_granted('ROLE_VIEWER') or is_granted('ROLE_RESTRICTED_VIEWER') or is_granted('ROLE_PROPERTY_PROVIDER')")]
#[Route('/besatz-und-leerstand/objekte/{buildingUnitId<\d{1,10}>}', name: 'property_vacancy_management_building_unit_')]
class PropertyMarketingInformationController extends AbstractController
{
    private const ACTIVE_NAV_GROUP = 'property_vacancy_management';
    private const ACTIVE_NAV_ITEM = self::ACTIVE_NAV_GROUP . '_building_unit';

    public function __construct(
        private readonly BuildingUnitService $buildingUnitService,
        private readonly BuildingUnitNotificationService $buildingUnitNotificationService,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    #[Route('/vermarktung', name: 'property_marketing_information', methods: ['GET', 'POST'])]
    public function propertyMarketingInformation(int $buildingUnitId, Request $request, UserInterface $user): Response
    {
        $buildingUnit = $this->buildingUnitService->fetchBuildingUnitById(
            account: $user->getAccount(),
            withFromSubAccounts: true,
            id: $buildingUnitId
        );

        if ($buildingUnit === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'view', subject: $buildingUnit);

        $propertyMarketingInformation = $buildingUnit->getPropertyMarketingInformation();

        if ($propertyMarketingInformation === null) {
            $propertyMarketingInformation = new PropertyMarketingInformation();

            $this->entityManager->beginTransaction();

            $this->entityManager->persist(entity: $propertyMarketingInformation);

            $buildingUnit->setPropertyMarketingInformation($propertyMarketingInformation);

            $this->entityManager->flush();
            $this->entityManager->commit();
        }

        $propertyPricingInformation = $propertyMarketingInformation->getPropertyPricingInformation();

        if ($propertyPricingInformation === null) {
            $propertyPricingInformation = new PropertyPricingInformation();

            $this->entityManager->beginTransaction();

            $this->entityManager->persist(entity: $propertyPricingInformation);

            $propertyMarketingInformation->setPropertyPricingInformation($propertyPricingInformation);

            $this->entityManager->flush();
            $this->entityManager->commit();
        }

        $originalPropertyRestrictions = new ArrayCollection();

        foreach ($propertyMarketingInformation->getPropertyRestrictions() as $propertyRestriction) {
            $originalPropertyRestrictions->add($propertyRestriction);
        }

        $propertyMarketingInformationForm = $this->createForm(
            type: PropertyMarketingInformationType::class,
            data: $propertyMarketingInformation,
            options: [
                'canEdit' => $this->isGranted(attribute: 'edit', subject: $buildingUnit),
            ]
        );

        $propertyMarketingInformationForm->add(child: 'save', type: SubmitType::class);

        $propertyMarketingInformationForm->handleRequest(request: $request);

        if ($propertyMarketingInformationForm->isSubmitted() && $propertyMarketingInformationForm->isValid()) {
            $this->denyAccessUnlessGranted(attribute: 'edit', subject: $buildingUnit);

            $this->entityManager->beginTransaction();

            $unitOfWork = $this->entityManager->getUnitOfWork();

            foreach ($originalPropertyRestrictions as $propertyRestriction) {
                if ($propertyMarketingInformation->getPropertyRestrictions()->contains($propertyRestriction) === false) {
                    $this->entityManager->remove($propertyRestriction);
                }
            }

            foreach ($propertyMarketingInformation->getPropertyRestrictions() as $propertyRestriction) {
                if ($unitOfWork->getEntityState($propertyRestriction) === UnitOfWork::STATE_NEW) {
                    $this->entityManager->persist(entity: $propertyRestriction);
                }
            }

            $unitOfWork->computeChangeSets();

            $usageAdded = $propertyMarketingInformation->getPossibleUsageIndustryClassifications()->getInsertDiff();
            $usageRemoved = $propertyMarketingInformation->getPossibleUsageIndustryClassifications()->getDeleteDiff();

            $this->entityManager->flush();
            $this->entityManager->commit();

            if (empty($usageAdded) === false || empty($usageRemoved) === false) {
                $this->buildingUnitNotificationService->createBuildingUnitUsageChangeNotifications(
                    buildingUnit: $buildingUnit,
                    changedByAccountUser: $user
                );
            }

            $this->addFlash(type: 'dataSaved', message: 'Die Vermarktungsinformationen wurden gespeichert.');

            return $this->redirectToRoute(
                route: 'property_vacancy_management_building_unit_property_marketing_information',
                parameters: ['buildingUnitId' => $buildingUnit->getId()]
            );
        }

        return $this->render(view: 'property_vacancy_management/building_unit/property_marketing_information.html.twig', parameters: [
            'propertyMarketingInformationForm' => $propertyMarketingInformationForm,
            'buildingUnit'                     => $buildingUnit,
            'pageTitle'                        => 'Leerstandsmanagement - Objektinformationen - Vermarktung',
            'activeNavGroup'                   => self::ACTIVE_NAV_GROUP,
            'activeNavItem'                    => self::ACTIVE_NAV_ITEM . ($buildingUnit->isArchived() ? '_archive' : ''),
        ]);
    }
}
