<?php

declare(strict_types=1);

namespace App\Controller\PropertyVacancyManagement\BuildingUnit;

use App\Controller\AbstractLeAnController;
use App\Domain\Entity\DeletionType;
use App\Domain\Entity\LastVisitedPage;
use App\Domain\Entity\OpenImmoExporter\ExportActionType;
use App\Domain\Entity\OpenImmoExporter\ExportMode;
use App\Domain\Entity\OpenImmoExporter\ExportScope;
use App\Domain\Entity\OpenImmoExporter\ExportType;
use App\Domain\Entity\OpenImmoFtpTransfer\OpenImmoExportInitialization;
use App\Domain\Entity\PlaceType;
use App\Domain\OpenImmoExporter\OpenImmoExportService;
use App\Domain\OpenImmoFtpTransfer\OpenImmoFtpCredentialService;
use App\Domain\OpenImmoFtpTransfer\OpenImmoFtpTransferService;
use App\Domain\Property\BuildingUnitDeletionService;
use App\Domain\Property\BuildingUnitNotificationService;
use App\Domain\Property\BuildingUnitService;
use App\Domain\Property\PdfExpose\PdfExposeService;
use App\Domain\SessionStoredUrlParameter\SessionStoredUrlParameterService;
use App\Form\Type\PropertyVacancyManagement\Property\BuildingUnitArchiveType;
use App\Form\Type\PropertyVacancyManagement\Property\BuildingUnitAssignToAccountUserType;
use App\Form\Type\PropertyVacancyManagement\Property\BuildingUnitBasicDataType;
use App\Form\Type\PropertyVacancyManagement\Property\BuildingUnitDeleteType;
use App\Form\Type\PropertyVacancyManagement\Property\BuildingUnitUnarchiveType;
use App\Form\Type\PropertyVacancyManagement\Property\OpenImmoExportInitializationType;
use App\SessionStorage\SessionStorageService;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Security("is_granted('ROLE_USER') or is_granted('ROLE_PROPERTY_FEEDER') or is_granted('ROLE_VIEWER') or is_granted('ROLE_RESTRICTED_VIEWER') or is_granted('ROLE_PROPERTY_PROVIDER')")]
#[Route('/besatz-und-leerstand/objekte/{buildingUnitId<\d{1,10}>}', name: 'property_vacancy_management_building_unit_')]
class BasicDataController extends AbstractLeAnController
{
    private const ACTIVE_NAV_GROUP = 'property_vacancy_management';
    private const ACTIVE_NAV_ITEM = self::ACTIVE_NAV_GROUP . '_building_unit';

    public function __construct(
        SessionStorageService $sessionStorageService,
        SessionStoredUrlParameterService $sessionStoredUrlParameterService,
        private readonly BuildingUnitService $buildingUnitService,
        private readonly BuildingUnitDeletionService $buildingUnitDeletionService,
        private readonly BuildingUnitNotificationService $buildingUnitNotificationService,
        private readonly OpenImmoFtpCredentialService $openImmoFtpCredentialService,
        private readonly OpenImmoExportService $openImmoExportService,
        private readonly OpenImmoFtpTransferService $openImmoFtpTransferService,
        private readonly PdfExposeService $pdfExposeService,
        private readonly EntityManagerInterface $entityManager
    ) {
        parent::__construct(
            sessionStorageService: $sessionStorageService,
            sessionStoredUrlParameterService: $sessionStoredUrlParameterService
        );
    }

    #[Route('/grunddaten', name: 'basic_data', methods: ['GET', 'POST'])]
    public function basicData(int $buildingUnitId, Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $buildingUnit = $this->buildingUnitService->fetchBuildingUnitById(account: $account, withFromSubAccounts: true, id: $buildingUnitId);

        if ($buildingUnit === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'view', subject: $buildingUnit);

        $assignedPlace = $account->getAssignedPlace();

        if (in_array(needle: $account->getAssignedPlace()->getPlaceType(), haystack: [PlaceType::COUNTY, PlaceType::MUNICIPAL_ASSOCIATION])) {
            $places = $assignedPlace->getChildrenPlaces();
        } else {
            $places = [$assignedPlace];
        }

        $openImmoFtpCredentialsConfigured = $this->openImmoFtpCredentialService->checkIfOpenImmoFtpCredentialsExists(account: $account);

        $buildingUnitBasicDataForm = $this->createForm(type: BuildingUnitBasicDataType::class, data: $buildingUnit, options: [
            'places'  => $places,
            'canEdit' => $this->isGranted(attribute: 'edit', subject: $buildingUnit),
        ]);

        $buildingUnitBasicDataForm->add(child:'save', type: SubmitType::class);

        $buildingUnitBasicDataForm->handleRequest(request: $request);

        if ($buildingUnitBasicDataForm->isSubmitted() && $buildingUnitBasicDataForm->isValid()) {
            $this->denyAccessUnlessGranted(attribute: 'edit', subject: $buildingUnit);

            $unitOfWork = $this->entityManager->getUnitOfWork();

            $unitOfWork->computeChangeSets();

            $changeSet = $unitOfWork->getEntityChangeSet(entity: $buildingUnit);

            $this->entityManager->flush();

            if (array_key_exists(key: 'currentUsageIndustryClassification', array: $changeSet)) {
                $this->buildingUnitNotificationService->createBuildingUnitUsageChangeNotifications(buildingUnit: $buildingUnit, changedByAccountUser: $user);
            }

            $this->addFlash(type: 'dataSaved', message: 'Die Grunddaten des Objektes wurde gespeichert.');

            return $this->redirectToRoute(route: 'property_vacancy_management_building_unit_basic_data', parameters: ['buildingUnitId' => $buildingUnit->getId()]);
        }

        return $this->render(view: 'property_vacancy_management/building_unit/basic_data.html.twig', parameters: [
            'buildingUnitBasicDataForm'        => $buildingUnitBasicDataForm,
            'places'                           => $places,
            'buildingUnit'                     => $buildingUnit,
            'openImmoFtpCredentialsConfigured' => $openImmoFtpCredentialsConfigured,
            'pageTitle'                        => 'Leerstandsmanagement - Objektinformationen - Grunddaten',
            'activeNavGroup'                   => self::ACTIVE_NAV_GROUP,
            'activeNavItem'                    => self::ACTIVE_NAV_ITEM . ($buildingUnit->isArchived() ? '_archive' : ''),
        ]);
    }

    #[Security("is_granted('ROLE_USER') or is_granted('ROLE_PROPERTY_FEEDER') or is_granted('ROLE_PROPERTY_PROVIDER')")]
    #[Route('/archivieren', name: 'archive', methods: ['GET', 'POST'])]
    public function archive(int $buildingUnitId, Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $buildingUnit = $this->buildingUnitService->fetchBuildingUnitById(account: $account, withFromSubAccounts: true, id: $buildingUnitId);

        if ($buildingUnit === null) {
            throw new NotFoundHttpException();
        }

        if ($buildingUnit->isArchived() === true) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'archive', subject: $buildingUnit);

        $assignedPlace = $account->getAssignedPlace();

        if (in_array($assignedPlace->getPlaceType(), [PlaceType::COUNTY, PlaceType::MUNICIPAL_ASSOCIATION])) {
            $places = $assignedPlace->getChildrenPlaces();
        } else {
            $places = [$assignedPlace];
        }

        $buildingUnitBasicDataForm = $this->createForm(type: BuildingUnitBasicDataType::class, data: $buildingUnit, options: [
            'places'  => $places,
            'canEdit' => false,
        ]);

        $buildingUnitArchiveForm = $this->createForm(type: BuildingUnitArchiveType::class);

        $buildingUnitArchiveForm->add(child:'archive', type: SubmitType::class);

        $buildingUnitArchiveForm->handleRequest(request: $request);

        if ($buildingUnitArchiveForm->isSubmitted() && $buildingUnitArchiveForm->isValid()) {
            $buildingUnit->setArchived(true);

            $this->entityManager->flush();

            $this->addFlash(type: 'dataSaved', message: 'Das Objekt wurde archiviert.');

            return $this->redirectToRoute(route: 'property_vacancy_management_building_unit_basic_data', parameters: ['buildingUnitId' => $buildingUnit->getId()]);
        }

        return $this->render(view: 'property_vacancy_management/building_unit/archive.html.twig', parameters: [
            'buildingUnitArchiveForm'   => $buildingUnitArchiveForm,
            'buildingUnitBasicDataForm' => $buildingUnitBasicDataForm,
            'places'                    => $places,
            'buildingUnit'              => $buildingUnit,
            'pageTitle'                 => 'Leerstandsmanagement - Objekt archivieren',
            'activeNavGroup'            => self::ACTIVE_NAV_GROUP,
            'activeNavItem'             => self::ACTIVE_NAV_ITEM . ($buildingUnit->isArchived() ? '_archive' : ''),
        ]);
    }

    #[Security("is_granted('ROLE_USER') or is_granted('ROLE_PROPERTY_FEEDER') or is_granted('ROLE_PROPERTY_PROVIDER')")]
    #[Route('/aus-dem-archiv-wiederherstellen', name: 'unarchive', methods: ['GET', 'POST'])]
    public function unarchive(int $buildingUnitId, Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $buildingUnit = $this->buildingUnitService->fetchBuildingUnitById(account: $account, withFromSubAccounts: true, id: $buildingUnitId);

        if ($buildingUnit === null) {
            throw new NotFoundHttpException();
        }

        if ($buildingUnit->isArchived() === false) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'archive', subject: $buildingUnit);

        $assignedPlace = $account->getAssignedPlace();

        if ($assignedPlace->getPlaceType() === PlaceType::COUNTY) {
            $places = $assignedPlace->getChildrenPlaces();
        } else {
            $places = [$assignedPlace];
        }

        $buildingUnitBasicDataForm = $this->createForm(type: BuildingUnitBasicDataType::class, data: $buildingUnit, options: [
            'places'  => $places,
            'canEdit' => false,
        ]);

        $buildingUnitUnarchiveForm = $this->createForm(type: BuildingUnitUnarchiveType::class);

        $buildingUnitUnarchiveForm->add(child:'unarchive', type: SubmitType::class);

        $buildingUnitUnarchiveForm->handleRequest(request: $request);

        if ($buildingUnitUnarchiveForm->isSubmitted() && $buildingUnitUnarchiveForm->isValid()) {
            $buildingUnit->setArchived(false);

            $this->entityManager->flush();

            $this->addFlash(type: 'dataSaved', message: 'Das Objekt wurde aus dem Archiv wiederhergestellt.');

            return $this->redirectToRoute(route: 'property_vacancy_management_building_unit_basic_data', parameters: ['buildingUnitId' => $buildingUnit->getId()]);
        }

        return $this->render(view: 'property_vacancy_management/building_unit/unarchive.html.twig', parameters: [
            'buildingUnitUnarchiveForm' => $buildingUnitUnarchiveForm,
            'buildingUnitBasicDataForm' => $buildingUnitBasicDataForm,
            'places'                    => $places,
            'buildingUnit'              => $buildingUnit,
            'pageTitle'                 => 'Leerstandsmanagement - Objekt archivieren',
            'activeNavGroup'            => self::ACTIVE_NAV_GROUP,
            'activeNavItem'             => self::ACTIVE_NAV_ITEM . ($buildingUnit->isArchived() ? '_archive' : ''),
        ]);
    }

    #[Security("is_granted('ROLE_USER') or is_granted('ROLE_PROPERTY_FEEDER') or is_granted('ROLE_PROPERTY_PROVIDER')")]
    #[Route('/loeschen', name: 'delete', methods: ['GET', 'POST'])]
    public function delete(int $buildingUnitId, Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $buildingUnit = $this->buildingUnitService->fetchBuildingUnitById(account: $account, withFromSubAccounts: true, id: $buildingUnitId);

        if ($buildingUnit === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'delete', subject: $buildingUnit);

        $assignedPlace = $account->getAssignedPlace();

        if ($assignedPlace->getPlaceType() === PlaceType::COUNTY) {
            $places = $assignedPlace->getChildrenPlaces();
        } else {
            $places = [$assignedPlace];
        }

        $buildingUnitBasicDataForm = $this->createForm(type: BuildingUnitBasicDataType::class, data: $buildingUnit, options: [
            'places'  => $places,
            'canEdit' => false,
        ]);

        $buildingUnitDeleteForm = $this->createForm(type: BuildingUnitDeleteType::class);

        $buildingUnitDeleteForm->add(child:'delete', type: SubmitType::class);

        $buildingUnitDeleteForm->handleRequest(request: $request);

        if (
            $buildingUnitDeleteForm->isSubmitted()
            && $buildingUnitDeleteForm->isValid()
            && $buildingUnitDeleteForm->get('verificationCode')->getData() === 'objekt_' . $buildingUnit->getId()
        ) {
            $this->buildingUnitDeletionService->deleteBuildingUnit(buildingUnit: $buildingUnit, deletionType: DeletionType::SOFT, deletedByAccountUser: $user);

            $this->addFlash(type: 'dataDeleted', message: 'Das Objekt wurde gelöscht.');

            $lastVisitedPage = $this->fetchObjectFromSession(dataClassName: LastVisitedPage::class, sessionKeyName: 'buildingUnitOverview');

            if ($lastVisitedPage !== null) {
                return $this->redirectToRoute(route: $lastVisitedPage->getRouteName(), parameters: $lastVisitedPage->getParameters());
            } else {
                return $this->redirectToRoute(route: 'property_vacancy_management_building_unit_overview_gallery');
            }
        }

        return $this->render(view: 'property_vacancy_management/building_unit/delete.html.twig', parameters: [
            'buildingUnitDeleteForm'    => $buildingUnitDeleteForm,
            'buildingUnitBasicDataForm' => $buildingUnitBasicDataForm,
            'places'                    => $places,
            'buildingUnit'              => $buildingUnit,
            'pageTitle'                 => 'Leerstandsmanagement - Objekt löschen',
            'activeNavGroup'            => self::ACTIVE_NAV_GROUP,
            'activeNavItem'             => self::ACTIVE_NAV_ITEM . ($buildingUnit->isArchived() ? '_archive' : ''),
        ]);
    }

    #[Security("is_granted('ROLE_USER') or is_granted('ROLE_PROPERTY_FEEDER')")]
    #[Route('/sachbearbeiter-zuweisen', name: 'assign_to_account_user', methods: ['GET', 'POST'])]
    public function assignToAccountUser(int $buildingUnitId, Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $buildingUnit = $this->buildingUnitService->fetchBuildingUnitById(account: $account, withFromSubAccounts: true, id: $buildingUnitId);

        if ($buildingUnit === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'assignToAccountUser', subject: $buildingUnit);

        $assignedPlace = $account->getAssignedPlace();

        if ($assignedPlace->getPlaceType() === PlaceType::COUNTY) {
            $places = $assignedPlace->getChildrenPlaces();
        } else {
            $places = [$assignedPlace];
        }

        $buildingUnitBasicDataForm = $this->createForm(type: BuildingUnitBasicDataType::class, data: $buildingUnit, options: [
            'places'  => $places,
            'canEdit' => false,
        ]);

        $buildingUnitAssignToAccountUserForm = $this->createForm(type: BuildingUnitAssignToAccountUserType::class, data: $buildingUnit, options: ['account' => $account]);

        $buildingUnitAssignToAccountUserForm->add(child:'save', type: SubmitType::class);

        $buildingUnitAssignToAccountUserForm->handleRequest(request: $request);

        if ($buildingUnitAssignToAccountUserForm->isSubmitted() && $buildingUnitAssignToAccountUserForm->isValid()) {
            $this->entityManager->flush();

            if ($buildingUnitAssignToAccountUserForm->getData()->getAssignedToAccountUser() === null) {
                $this->addFlash(type: 'dataDeleted', message: 'Es wurde kein/e Sachbearbeiter:in zugewiesen.');
            } else {
                $this->addFlash(type: 'dataSaved', message: 'Sachbearbeiter:in zugewiesen.');
            }

            return $this->redirectToRoute(route: 'property_vacancy_management_building_unit_basic_data', parameters: ['buildingUnitId' => $buildingUnit->getId()]);
        }

        return $this->render(view: 'property_vacancy_management/building_unit/assign_to_account_user.html.twig', parameters: [
            'buildingUnitAssignToAccountUserForm' => $buildingUnitAssignToAccountUserForm,
            'buildingUnitBasicDataForm'           => $buildingUnitBasicDataForm,
            'places'                              => $places,
            'buildingUnit'                        => $buildingUnit,
            'pageTitle'                           => 'Leerstandsmanagement - Zuweisung Sachbearbeiter:in',
            'activeNavGroup'                      => self::ACTIVE_NAV_GROUP,
            'activeNavItem'                       => self::ACTIVE_NAV_ITEM . ($buildingUnit->isArchived() ? '_archive' : ''),
        ]);
    }

    #[Security("is_granted('ROLE_USER') or is_granted('ROLE_PROPERTY_FEEDER')")]
    #[Route('/pdf-expose', name: 'pdf_expose', methods: ['GET'])]
    public function pdfExpose(int $buildingUnitId, UserInterface $user): Response
    {
        $buildingUnit = $this->buildingUnitService->fetchBuildingUnitById(account: $user->getAccount(), withFromSubAccounts: true, id: $buildingUnitId);

        if ($buildingUnit === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'view', subject: $buildingUnit);

        return new Response(
            content: $this->pdfExposeService->generateBuildingUnitPdfExpose(buildingUnit: $buildingUnit, accountUser: $user),
            headers: ['Content-Type' => 'application/pdf']
        );
    }

    #[Security("is_granted('ROLE_USER') or is_granted('ROLE_PROPERTY_FEEDER')")]
    #[Route('/open-immo-export', name: 'open_immo_export', methods: ['GET', 'POST'])]
    public function openImmoExport(int $buildingUnitId, Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $buildingUnit = $this->buildingUnitService->fetchBuildingUnitById(account: $account, withFromSubAccounts: true, id: $buildingUnitId);

        if ($buildingUnit === null) {
            throw new NotFoundHttpException();
        }

        $openImmoFtpCredentials = $this->openImmoFtpCredentialService->fetchOpenImmoFtpCredentials(account: $account);

        if (empty($openImmoFtpCredentials) === true) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'openImmoExport', subject: $buildingUnit);

        $openImmoExportInitialization = new OpenImmoExportInitialization();

        $openImmoExportInitialization
            ->setOpenImmoFtpCredentials(openImmoFtpCredentials: $openImmoFtpCredentials)
            ->setExportActionType(exportActionType: ExportActionType::CHANGE);

        $openImmoExportInitializationForm = $this->createForm(type: OpenImmoExportInitializationType::class, data: $openImmoExportInitialization);

        $openImmoExportInitializationForm->add(child:'apply', type: SubmitType::class);

        $openImmoExportInitializationForm->handleRequest(request: $request);

        $openImmoTransferLogs = $this->openImmoFtpTransferService->fetchOpenImmoTransferLogByBuildingUnit(buildingUnit: $buildingUnit);

        if ($openImmoExportInitializationForm->isSubmitted() && $openImmoExportInitializationForm->isValid()) {
            foreach ($openImmoExportInitialization->getOpenImmoFtpCredentials() as $openImmoFtpCredential) {
                $openImmoExport = $this->openImmoExportService->createAndSaveOpenImmoExport(
                    buildingUnit: $buildingUnit,
                    accountUser: $user,
                    exportActionType: $openImmoExportInitialization->getExportActionType(),
                    exportMode: ExportMode::CHANGE,
                    exportScope: ExportScope::PART,
                    exportType: ExportType::ONLINE,
                    portalProviderNumber: $openImmoFtpCredential->getPortalProviderNumber()
                );

                $this->openImmoExportService->dispatchExecuteOpenImmoExportMessage(openImmoExport: $openImmoExport);

                $openImmoFtpTransferTask = $this->openImmoFtpTransferService->createAndSaveOpenImmoFtpTransferTask(
                    openImmoExport: $openImmoExport,
                    openImmoFtpCredential: $openImmoFtpCredential,
                    accountUser: $user
                );
            }

            if (empty($openImmoExportInitialization->getOpenImmoFtpCredentials()) === false) {
                if ($openImmoExportInitializationForm->getData()->getExportActionType() === ExportActionType::DELETE) {
                    $this->addFlash(type: 'dataDeleted', message: 'Die Löschung des Objekts aus den gewählten Portalen wurde initialisiert.');
                } else {
                    $this->addFlash(type: 'dataSaved', message: 'Die Übertragung des Objekts an die gewählten Portale wurde initialisiert.');
                }

                return $this->redirectToRoute(route: 'property_vacancy_management_building_unit_basic_data', parameters: ['buildingUnitId' => $buildingUnit->getId()]);
            } else {
                $this->addFlash(type: 'dataError', message: 'Bitte wählen Sie mindestens 1 Portal aus.');
            }
        }

        return $this->render(view: 'property_vacancy_management/building_unit/open_immo_export.html.twig', parameters: [
            'buildingUnit'                     => $buildingUnit,
            'openImmoExportInitializationForm' => $openImmoExportInitializationForm,
            'openImmoTransferLogs'             => $openImmoTransferLogs,
            'pageTitle'                        => 'Leerstandsmanagement - Export an Immobilienportale',
            'activeNavGroup'                   => self::ACTIVE_NAV_GROUP,
            'activeNavItem'                    => self::ACTIVE_NAV_ITEM . ($buildingUnit->isArchived() ? '_archive' : ''),
        ]);
    }
}
