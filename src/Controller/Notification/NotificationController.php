<?php

declare(strict_types=1);

namespace App\Controller\Notification;

use App\Controller\AbstractLeAnController;
use App\Domain\Entity\Notification\NotificationFilter;
use App\Domain\Entity\PaginationParameter;
use App\Domain\Entity\SortingOption\SortingOption;
use App\Domain\Notification\NotificationDeletionService;
use App\Domain\Notification\NotificationService;
use App\Domain\SessionStoredUrlParameter\SessionStoredUrlParameterService;
use App\Form\Type\Notification\NotificationDeleteType;
use App\Form\Type\Notification\NotificationFilterType;
use App\SessionStorage\SessionStorageService;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Security("is_granted('ROLE_ACCOUNT_ADMIN')
or is_granted('ROLE_USER')
or is_granted('ROLE_PROPERTY_FEEDER')
or is_granted('ROLE_LOOKING_FOR_PROPERTY_REQUEST_FEEDER')
or is_granted('ROLE_PROPERTY_VACANCY_REPORT_MANAGER')
or is_granted('ROLE_LOOKING_FOR_PROPERTY_REQUEST_REPORT_MANAGER')
or is_granted('ROLE_VIEWER') 
or is_granted('ROLE_RESTRICTED_VIEWER')
")]
#[Route('/meine-nachrichten', name: 'notification_')]
class NotificationController extends AbstractLeAnController
{
    private const ACTIVE_NAV_ITEM = 'notification';

    private const OVERVIEW_ROUTE_NAME = 'notification_overview';

    private const OVERVIEW_SESSION_KEY_NAME = 'notificationOverview';

    private const URL_PARAMETER_OBJECT_CLASSES = [
        PaginationParameter::class,
        SortingOption::class,
    ];

    public function __construct(
        SessionStorageService $sessionStorageService,
        SessionStoredUrlParameterService $sessionStoredUrlParameterService,
        private readonly NotificationService $notificationService,
        private readonly NotificationDeletionService $notificationDeletionService,
        private readonly EntityManagerInterface $entityManager
    ) {
        parent::__construct(
            sessionStorageService: $sessionStorageService,
            sessionStoredUrlParameterService: $sessionStoredUrlParameterService
        );
    }

    #[Route('/uebersicht', name: 'overview', methods: ['GET', 'POST'])]
    public function overview(Request $request, UserInterface $user): Response
    {
        $paginationParameter = PaginationParameter::createFromRequest(request: $request);

        $this->storeObjectInSession(object: $paginationParameter, sessionKeyName: self::OVERVIEW_SESSION_KEY_NAME);

        $sortingOption = null;

        if ($request->query->get(key: 'sortieren') !== null) {
            $sortingOption = SortingOption::createFromRequest(request: $request);

            $this->storeObjectInSession(object: $sortingOption, sessionKeyName: self::OVERVIEW_SESSION_KEY_NAME);
        }

        $notificationFilterForm = $this->createForm(type: NotificationFilterType::class, options: [
            'dataClassName'  => NotificationFilter::class,
            'sessionKeyName' => self::OVERVIEW_SESSION_KEY_NAME,
        ]);

        $notificationFilterForm->add(child: 'apply', type: SubmitType::class);

        $notificationFilterForm->handleRequest(request: $request);

        $notifications = $this->notificationService->fetchNotificationsPaginatedByAccountUser(
            accountUser: $user,
            firstResult: $paginationParameter->getFirstResult(),
            maxResults: $paginationParameter->getLimit(),
            notificationFilter: $notificationFilterForm->getData(),
            sortingOption: $sortingOption
        );

        return $this->render(view: 'notification/overview.html.twig', parameters: [
            'notificationFilterForm' => $notificationFilterForm,
            'notificationFilter'     => $notificationFilterForm->getData(),
            'notifications'          => $notifications,
            'pageTitle'              => 'Benachrichtigungen | Uebersicht',
            'activeNavItem'          => self::ACTIVE_NAV_ITEM,
        ]);
    }

    #[Route('/{notificationId<\d{1,10}>}/loeschen', name: 'delete', methods: ['GET', 'POST'])]
    public function delete(int $notificationId, Request $request, UserInterface $user): Response
    {
        $notification = $this->notificationService->fetchNotificationById(accountUser: $user, id: $notificationId);

        if ($notification === null) {
            throw new NotFoundHttpException();
        }

        $notificationDeleteForm = $this->createForm(type: NotificationDeleteType::class);

        $notificationDeleteForm->add(child: 'delete', type: SubmitType::class);

        $notificationDeleteForm->handleRequest(request: $request);

        if (
            $notificationDeleteForm->isSubmitted()
            && $notificationDeleteForm->isValid()
            && $notificationDeleteForm->get('verificationCode')->getData() === 'benachrichtigung_' . $notification->getId()
        ) {
            $this->notificationDeletionService->deleteNotification(notification: $notification);

            $this->addFlash(type: 'dataDeleted', message: 'Die Benachrichtigungen wurde gelöscht.');

            $urlParameters = $this->fetchMergedUrlParametersFromSession(
                urlParameterObjectClasses: self::URL_PARAMETER_OBJECT_CLASSES,
                sessionKeyName: self::OVERVIEW_SESSION_KEY_NAME
            );

            return $this->redirectToRoute(route: self::OVERVIEW_ROUTE_NAME, parameters: $urlParameters);
        }

        return $this->render(view: 'notification/delete.html.twig', parameters: [
            'notificationDeleteForm' => $notificationDeleteForm,
            'notification'           => $notification,
            'activeNavItem'          => self::ACTIVE_NAV_ITEM,
        ]);
    }

    #[Route('/{notificationId<\d{1,10}>}/gelesen', name: 'had_read', methods: ['GET'])]
    public function hadRead(int $notificationId, UserInterface $user): Response
    {
        $notification = $this->notificationService->fetchNotificationById(accountUser: $user, id: $notificationId);

        if ($notification === null) {
            throw new NotFoundHttpException();
        }

        $notification->setBeenRead(true);

        $this->entityManager->flush();

        $this->addFlash(type: 'readStatusChangedSuccess', message: ['success' => true]);

        $urlParameters = $this->fetchMergedUrlParametersFromSession(
            urlParameterObjectClasses: self::URL_PARAMETER_OBJECT_CLASSES,
            sessionKeyName: self::OVERVIEW_SESSION_KEY_NAME
        );

        return $this->redirectToRoute(route: self::OVERVIEW_ROUTE_NAME, parameters: $urlParameters);
    }

    #[Route('/{notificationId<\d{1,10}>}/ungelesen', name: 'unread', methods: ['GET'])]
    public function unread(int $notificationId, UserInterface $user): Response
    {
        $notification = $this->notificationService->fetchNotificationById(accountUser: $user, id: $notificationId);

        if ($notification === null) {
            throw new NotFoundHttpException();
        }

        $notification->setBeenRead(false);

        $this->entityManager->flush();

        $this->addFlash(type: 'readStatusChangedSuccess', message: ['success' => true]);

        $urlParameters = $this->fetchMergedUrlParametersFromSession(
            urlParameterObjectClasses: self::URL_PARAMETER_OBJECT_CLASSES,
            sessionKeyName: self::OVERVIEW_SESSION_KEY_NAME
        );

        return $this->redirectToRoute(route: self::OVERVIEW_ROUTE_NAME, parameters: $urlParameters);
    }
}
