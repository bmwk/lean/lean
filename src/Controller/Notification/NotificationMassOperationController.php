<?php

declare(strict_types=1);

namespace App\Controller\Notification;

use App\Controller\AbstractLeAnController;
use App\Domain\Entity\PaginationParameter;
use App\Domain\Entity\SortingOption\SortingOption;
use App\Domain\Notification\NotificationDeletionService;
use App\Domain\Notification\NotificationService;
use App\Domain\SessionStoredUrlParameter\SessionStoredUrlParameterService;
use App\Form\Type\Notification\NotificationMassOperationDeleteType;
use App\SessionStorage\SessionStorageService;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Security("is_granted('ROLE_ACCOUNT_ADMIN')
or is_granted('ROLE_USER')
or is_granted('ROLE_PROPERTY_FEEDER')
or is_granted('ROLE_LOOKING_FOR_PROPERTY_REQUEST_FEEDER')
or is_granted('ROLE_PROPERTY_VACANCY_REPORT_MANAGER')
or is_granted('ROLE_LOOKING_FOR_PROPERTY_REQUEST_REPORT_MANAGER')
or is_granted('ROLE_VIEWER') 
or is_granted('ROLE_RESTRICTED_VIEWER')
")]
#[Route('/meine-nachrichten', name: 'notification_mass_operation_')]
class NotificationMassOperationController extends AbstractLeAnController
{
    private const ACTIVE_NAV_ITEM = 'notification';

    private const URL_PARAMETER_OBJECT_CLASSES = [
        PaginationParameter::class,
        SortingOption::class,
    ];

    public function __construct(
        SessionStorageService $sessionStorageService,
        SessionStoredUrlParameterService $sessionStoredUrlParameterService,
        private readonly NotificationService $notificationService,
        private readonly NotificationDeletionService $notificationDeletionService,
        private readonly EntityManagerInterface $entityManager
    ) {
        parent::__construct(
            sessionStorageService: $sessionStorageService,
            sessionStoredUrlParameterService: $sessionStoredUrlParameterService
        );
    }

    #[Route('/loeschen', name: 'delete', methods: ['POST'])]
    public function delete(Request $request, UserInterface $user): Response
    {
        $notificationMassOperationDeleteForm = $this->createForm(type: NotificationMassOperationDeleteType::class);

        $notificationMassOperationDeleteForm->add(child: 'delete', type: SubmitType::class);

        $notificationMassOperationDeleteForm->handleRequest(request: $request);

        if ($request->get('selectedRowIds') !== null) {
            $notificationMassOperationDeleteForm->get('selectedRowIds')->setData($request->get('selectedRowIds'));
        }

        $notifications = $this->notificationService->fetchNotificationsByIds(
            accountUser: $user,
            ids: json_decode($notificationMassOperationDeleteForm->get('selectedRowIds')->getData()),
        );

        if (
            $notificationMassOperationDeleteForm->isSubmitted()
            && $notificationMassOperationDeleteForm->isValid()
            && $notificationMassOperationDeleteForm->get('verificationCode')->getData() === 'benachrichtigungen_' . count($notifications)
        ) {
            foreach ($notifications as $notification) {
                $this->notificationDeletionService->deleteNotification(notification: $notification);
            }

            $this->addFlash(type: 'dataDeleted', message: 'Die Benachrichtigungen wurden gelöscht.');

            $urlParameters = $this->fetchMergedUrlParametersFromSession(
                urlParameterObjectClasses: self::URL_PARAMETER_OBJECT_CLASSES,
                sessionKeyName: 'notificationOverview'
            );

            return $this->redirectToRoute(route: 'notification_overview', parameters: $urlParameters);
        }

        return $this->render(view: 'notification/mass_operation/delete.html.twig', parameters: [
            'notificationMassOperationDeleteForm' => $notificationMassOperationDeleteForm,
            'notifications'                       => $notifications,
            'pageTitle'                           => 'Benachrichtigungen',
            'activeNavItem'                       => self::ACTIVE_NAV_ITEM,
        ]);
    }

    #[Route('/gelesen', name: 'had_read', methods: ['POST'])]
    public function hadRead(Request $request, UserInterface $user): Response
    {
        $urlParameters = $this->fetchMergedUrlParametersFromSession(
            urlParameterObjectClasses: self::URL_PARAMETER_OBJECT_CLASSES,
            sessionKeyName: 'notificationOverview'
        );

        if ($request->get(key: 'selectedRowIds') === null) {
            return $this->redirectToRoute(route: 'notification_overview', parameters: $urlParameters);
        }

        $notifications = $this->notificationService->fetchNotificationsByIds(accountUser: $user, ids: json_decode($request->get('selectedRowIds')));

        foreach ($notifications as $notification) {
            $notification->setBeenRead(true);
        }

        $this->entityManager->flush();

        $this->addFlash(type: 'readStatusChangedSuccess', message: ['success' => true]);

        return $this->redirectToRoute(route: 'notification_overview', parameters: $urlParameters);
    }

    #[Route('/ungelesen', name: 'unread', methods: ['POST'])]
    public function unread(Request $request, UserInterface $user): Response
    {
        $urlParameters = $this->fetchMergedUrlParametersFromSession(
            urlParameterObjectClasses: self::URL_PARAMETER_OBJECT_CLASSES,
            sessionKeyName: 'notificationOverview'
        );

        if ($request->get('selectedRowIds') === null) {
            return $this->redirectToRoute(route: 'notification_overview', parameters: $urlParameters);
        }

        $notifications = $this->notificationService->fetchNotificationsByIds(
            accountUser: $user,
            ids: json_decode($request->get('selectedRowIds'))
        );

        foreach ($notifications as $notification) {
            $notification->setBeenRead(false);
        }

        $this->entityManager->flush();

        $this->addFlash(type: 'readStatusChangedSuccess', message: ['success' => true]);

        return $this->redirectToRoute(route: 'notification_overview', parameters: $urlParameters);
    }
}
