<?php

declare(strict_types=1);

namespace App\Controller\PublicAccess\SettlementConcept;

use App\Domain\SettlementConcept\SettlementConceptAccountUserService;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Uid\Uuid;

#[IsGranted('PUBLIC_ACCESS')]
#[Route('/ansiedlungskonzepte/api-nutzung', name: 'settlement_concept_account_user_json_web_token_')]
class SettlementConceptInviteController extends AbstractController
{
    public function __construct(
        private readonly SettlementConceptAccountUserService $settlementConceptAccountUserService,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    #[Route('/{uuid<[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}>}', name: 'landing_page')]
    public function landingPage(string $uuid): Response
    {
        $settlementConceptAuthTokenGenerationRequest = $this->settlementConceptAccountUserService->fetchSettlementConceptAuthenticationTokenGenerationRequestByUuid(
            uuid: Uuid::fromString(uuid: $uuid)
        );

        if ($settlementConceptAuthTokenGenerationRequest === null) {
            throw new BadRequestHttpException();
        }

        $settlementConceptAccountUser = $settlementConceptAuthTokenGenerationRequest->getSettlementConceptAccountUser();

        $token = $this->settlementConceptAccountUserService->generateJsonWebToken(settlementConceptAccountUser: $settlementConceptAccountUser);

        $this->entityManager->remove(entity: $settlementConceptAuthTokenGenerationRequest);

        $this->entityManager->flush();

        return $this->render(view: 'public_access/settlement_concept/authentication_token_generation_request_landing_page.html.twig', parameters: [
            'token'                        => $token,
            'settlementConceptAccountUser' => $settlementConceptAccountUser,
        ]);
    }
}
