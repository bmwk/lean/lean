<?php

declare(strict_types=1);

namespace App\Controller\PublicAccess\UserRegistration;

use App\Domain\Account\AccountService;
use App\Domain\Account\AccountUserService;
use App\Domain\Entity\AccountUser\AccountUserRole;
use App\Domain\Entity\Person\PersonType;
use App\Domain\Entity\UserRegistration\UserRegistration;
use App\Domain\Entity\UserRegistration\UserRegistrationStatus;
use App\Domain\Entity\UserRegistration\UserRegistrationType;
use App\Domain\UserRegistration\UserInvitationService;
use App\Domain\UserRegistration\UserRegistrationEmailService;
use App\Domain\UserRegistration\UserRegistrationService;
use App\Form\Type\UserRegistration\CompanyUserRegistrationType;
use App\Form\Type\UserRegistration\NaturalPersonUserRegistrationType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints\NotEqualTo;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[IsGranted('PUBLIC_ACCESS')]
#[Route('/registrieren', name: 'user_registration_')]
class UserRegistrationController extends AbstractController
{
    public function __construct(
        private readonly AccountService $accountService,
        private readonly AccountUserService $accountUserService,
        private readonly UserInvitationService $userInvitationService,
        private readonly UserRegistrationService $userRegistrationService,
        private readonly UserRegistrationEmailService $userRegistrationEmailService,
        private readonly EntityManagerInterface $entityManager,
        private readonly ValidatorInterface $validator
    ) {
    }

    #[Route('/{userRegistrationTypeName<suchende|anbietende>}', name: 'register_without_invitation', methods: ['GET', 'POST'])]
    public function registerWithoutInvitation(string $userRegistrationTypeName, Request $request): Response
    {
        $account = $this->accountService->getOperatorAccount();

        if ($account === null) {
            throw new NotFoundHttpException();
        }

        $userRegistration = new UserRegistration();

        if ($userRegistrationTypeName === 'suchende') {
            if ($account->getAccountConfiguration()->isAllowSeekerRegistration() === false) {
                return $this->redirectToRoute(route: 'information_for_seeker');
            }

            $userRegistrationType = UserRegistrationType::SEEKER;
        } elseif ($userRegistrationTypeName === 'anbietende') {
            if ($account->getAccountConfiguration()->isAllowProviderRegistration() === false) {
                return $this->redirectToRoute(route: 'information_for_provider');
            }

            $userRegistrationType = UserRegistrationType::PROVIDER;
        } else {
            throw new NotFoundHttpException();
        }

        $userRegistration
            ->setAccount($account)
            ->setUserRegistrationStatus(UserRegistrationStatus::NOT_UNLOCKED)
            ->setUserRegistrationType($userRegistrationType)
            ->setEmailConfirmed(false);

        $companyUserRegistrationForm = $this->createForm(type: CompanyUserRegistrationType::class, data: $userRegistration);
        $naturalPersonUserRegistrationForm = $this->createForm(type: NaturalPersonUserRegistrationType::class, data: $userRegistration);

        $templateParameters = [
            'userRegistrationType'              => $userRegistrationType,
            'companyUserRegistrationForm'       => $companyUserRegistrationForm,
            'naturalPersonUserRegistrationForm' => $naturalPersonUserRegistrationForm,
        ];

        $companyUserRegistrationForm->add(child: 'save', type: SubmitType::class);
        $naturalPersonUserRegistrationForm->add(child: 'save', type: SubmitType::class);

        $companyUserRegistrationForm->handleRequest($request);
        $naturalPersonUserRegistrationForm->handleRequest($request);

        if (
            ($companyUserRegistrationForm->isSubmitted() && $companyUserRegistrationForm->isValid())
            || ($naturalPersonUserRegistrationForm->isSubmitted() && $naturalPersonUserRegistrationForm->isValid())
        ) {
            if ($companyUserRegistrationForm->isSubmitted()) {
                $userRegistration->setPersonType(PersonType::COMPANY);
            }

            if ($naturalPersonUserRegistrationForm->isSubmitted()) {
                $userRegistration->setPersonType(PersonType::NATURAL_PERSON);
            }

            if ($this->accountUserService->isUsernameExistentInDatabase(userIdentifier: $userRegistration->getEmail()) === true) {
                return $this->render(view: 'user_registration/error/user_invitation_basic_error_template.html.twig', parameters: [
                    'errorHeadline' => 'Registrierung nicht möglich!',
                    'errorMessage'  => 'Leider konnte Ihre Registrierung nicht abgeschlossen werden, da die von Ihnen verwendete E-Mail-Adresse bereits in einem anderen Account genutzt wird.',
                    'pageTitle'     => 'Fehler bei der Registrierung',
                ]);
            }

            $errors = $this->validator->validate(
                value: $userRegistration->isGtcPrivacy(),
                constraints: new NotEqualTo(
                    value: false,
                    message: 'Um sich zu registrieren, müssen Sie die AGB und Datenschutzerklärung akzeptieren.'
                )
            );

            $displayErrors = [];

            if ($errors->count() > 0) {
                $displayErrors['gtcPrivacy'] = $errors->get(0);

                $templateParameters['errors'] = $displayErrors;

                return $this->render(view: 'user_registration/user_registration_without_invitation.html.twig', parameters: $templateParameters);
            }

            $userRegistration
                ->setExpirationDate((new \DateTime())->add(new \DateInterval('PT48H')))
                ->setActivationToken(
                    $this->userRegistrationService->generateActivationToken(
                        uniqueIdentifier: $userRegistration->getExpirationDate()->getTimestamp(),
                        expirationDate: $userRegistration->getExpirationDate()
                    )
                );

            $this->entityManager->persist(entity: $userRegistration);

            $this->entityManager->flush();

            $this->userRegistrationEmailService->sendUserRegistrationEmail(userRegistration: $userRegistration);

            return $this->render(view: 'user_registration/user_registration_confirmation.html.twig', parameters: [
                'userRegistration' => $userRegistration,
            ]);
        }

        return $this->render(view: 'user_registration/user_registration_without_invitation.html.twig', parameters: $templateParameters);
    }

    #[Route('/{userInvitationUuid<[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}>}', name: 'register_with_invitation', methods: ['GET', 'POST'])]
    public function registerWithInvitation(string $userInvitationUuid, Request $request): Response
    {
        $userInvitation = $this->userInvitationService->fetchUserInvitationByUuid(uuid: Uuid::fromString($userInvitationUuid));

        if ($userInvitation === null) {
            throw new NotFoundHttpException();
        }

        $operatorAccount = $this->accountService->getOperatorAccount();

        if ($operatorAccount !== null && $userInvitation->getAccount() !== $operatorAccount) {
            throw new NotFoundHttpException();
        }

        if ($userInvitation->getUserRegistration() !== null) {
            return $this->render(view: 'user_registration/error/user_invitation_basic_error_template.html.twig', parameters: [
                'errorHeadline' => 'Registrierung nicht möglich!',
                'errorMessage'  => 'Mit Ihrem Einladungscode wurde bereits eine Registrierung durchgeführt.',
                'pageTitle'     => 'Fehler bei der Registrierung',
            ]);
        }

        $userRegistration = UserRegistration::createFromUserInvitation(userInvitation: $userInvitation);

        $userRegistrationForm = match ($userRegistration->getPersonType()) {
            PersonType::COMPANY => $this->createForm(type: CompanyUserRegistrationType::class, data: $userRegistration),
            PersonType::NATURAL_PERSON => $this->createForm(type: NaturalPersonUserRegistrationType::class, data: $userRegistration),
            default => throw new \LogicException(),
        };

        $templateParameters = [
            'userRegistration'     => $userRegistration,
            'userRegistrationForm' => $userRegistrationForm,
        ];

        $userRegistrationForm->add(child: 'save', type: SubmitType::class);

        $userRegistrationForm->handleRequest(request: $request);

        if ($userRegistrationForm->isSubmitted() && $userRegistrationForm->isValid()) {
            if ($this->accountUserService->isUsernameExistentInDatabase(userIdentifier: $userRegistration->getEmail()) === true) {
                return $this->render(view: 'user_registration/error/user_invitation_basic_error_template.html.twig', parameters: [
                    'errorHeadline' => 'Registrierung nicht möglich!',
                    'errorMessage'  => 'Leider konnte Ihre Registrierung nicht abgeschlossen werden, da die von Ihnen verwendete E-Mail-Adresse bereits in einem anderen Account genutzt wird.',
                    'pageTitle'     => 'Fehler bei der Registrierung',
                ]);
            }

            $errors = $this->validator->validate(
                value: $userRegistration->isGtcPrivacy(),
                constraints: new NotEqualTo(
                    value: false,
                    message: 'Um sich zu registrieren, müssen Sie die AGB und Datenschutzerklärung akzeptieren.'
                )
            );

            $displayErrors = [];

            if ($errors->count() > 0) {
                $displayErrors['gtcPrivacy'] = $errors->get(0);

                $templateParameters['errors'] = $displayErrors;

                return $this->render(view: 'user_registration/user_registration_with_invitation.html.twig', parameters: $templateParameters);
            }

            $userRegistration
                ->setExpirationDate((new \DateTime())->add(new \DateInterval('PT48H')))
                ->setActivationToken(
                    $this->userRegistrationService->generateActivationToken(
                        uniqueIdentifier: $userRegistration->getExpirationDate()->getTimestamp(),
                        expirationDate: $userRegistration->getExpirationDate()
                    )
                );

            $this->entityManager->persist(entity: $userRegistration);

            $this->entityManager->flush();

            $this->userRegistrationEmailService->sendUserRegistrationEmail(userRegistration: $userRegistration);

            return $this->render(view:'user_registration/user_registration_confirmation.html.twig', parameters: [
                'userRegistration' => $userRegistration,
            ]);
        }

        return $this->render(view: 'user_registration/user_registration_with_invitation.html.twig', parameters: $templateParameters);
    }

    #[Route('/aktivieren/{activationToken<[a-f0-9]{64}>}', name: 'activate', methods: ['GET'])]
    public function activate(string $activationToken): Response
    {
        $userRegistration = $this->userRegistrationService->fetchUserRegistrationByValidActivationToken(activationToken: $activationToken);

        if ($userRegistration === null) {
            throw new NotFoundHttpException();
        }

        if ($userRegistration->validateActivationData() === false) {
            throw new NotFoundHttpException();
        }

        $userRegistration->setEmailConfirmed(true);

        $this->entityManager->flush();

        $userInvited = false;

        if ($userRegistration->getUserInvitation() !== null) {
            $userInvited = true;

            $this->userRegistrationService->activateUser(userRegistration: $userRegistration);
        } else {
            $adminAccountUsers = $this->accountUserService->fetchAccountUsersByAccount(
                account: $userRegistration->getAccount(),
                withFromSubAccounts: false,
                accountUserRoles: [AccountUserRole::ROLE_ACCOUNT_ADMIN]
            );

            $this->userRegistrationEmailService->sendAdminInformationEmail(userRegistration: $userRegistration, adminAccountUsers: $adminAccountUsers);
        }

        return $this->render(view:'user_registration/user_registration_activation_successfull.html.twig', parameters: [
            'userRegistration' => $userRegistration,
            'userInvited'      => $userInvited,
            'pageTitle'        => 'Aktivierung erfolgreich',
        ]);
    }
}
