<?php

declare(strict_types=1);

namespace App\Controller\PublicAccess;

use App\Domain\PropertyVacancyReport\PropertyVacancyReportService;
use App\Domain\PropertyVacancyReporter\PropertyVacancyReporterConfigurationService;
use App\Domain\PropertyVacancyReporter\PropertyVacancyReporterEmailService;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Uid\Uuid;

#[IsGranted('PUBLIC_ACCESS')]
#[Route('/leerstandsmelder', name: 'property_vacancy_reporter_')]
class PropertyVacancyReporterController extends AbstractController
{
    public function __construct(
        private readonly PropertyVacancyReporterEmailService $propertyVacancyReporterEmailService,
        private readonly PropertyVacancyReportService $propertyVacancyReportService,
        private readonly PropertyVacancyReporterConfigurationService $propertyVacancyReporterConfigurationService,
        private readonly EntityManagerInterface $entityManager,
    ) {
    }

    #[Route('/meldung/{reportUuid<[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}>}/bestaetigung/{confirmationToken<[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}>}', name: 'report_confirmation', methods: ['GET'])]
    public function reportConfirmation(string $reportUuid, string $confirmationToken): Response
    {
        $propertyVacancyReport = $this->propertyVacancyReportService->fetchPropertyVacancyReportByUuid(uuid: Uuid::fromString($reportUuid));

        if ($propertyVacancyReport === null) {
            throw new NotFoundHttpException();
        }

        if ($propertyVacancyReport->isEmailConfirmed() === false) {
            $isAlreadyConfirmed = false;

            if ($propertyVacancyReport->getConfirmationToken()->toRfc4122() !== $confirmationToken) {
                throw new NotFoundHttpException();
            }

            $propertyVacancyReporterConfiguration = $this->propertyVacancyReporterConfigurationService
                ->fetchPropertyVacancyReporterConfigurationByAccount($propertyVacancyReport->getAccount());

            $propertyVacancyReport
                ->setEmailConfirmed(true)
                ->setEmailConfirmedAt(new \DateTimeImmutable());

            $this->entityManager->flush();

            $this->propertyVacancyReporterEmailService->sendNewPropertyVacancyReportNotificationEmail(
                propertyVacancyReport: $propertyVacancyReport,
                propertyVacancyReporterConfiguration: $propertyVacancyReporterConfiguration
            );
        } else {
            $isAlreadyConfirmed = true;
        }

        $account = $propertyVacancyReport->getAccount();

        return $this->render(view: 'property_vacancy_management/property_vacancy_report/confirmation_landingpage.html.twig', parameters: [
            'pageTitle'          => 'Bestätigung Ihrer Leerstandsmeldung',
            'isAlreadyConfirmed' => $isAlreadyConfirmed,
            'account'            => $account,
        ]);
    }
}
