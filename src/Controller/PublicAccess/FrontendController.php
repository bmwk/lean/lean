<?php

declare(strict_types=1);

namespace App\Controller\PublicAccess;

use App\Domain\Account\AccountService;
use App\Domain\Account\AccountUserDeletionService;
use App\Domain\Account\AccountUserEmailService;
use App\Domain\Account\AccountUserPasswordResetService;
use App\Domain\Account\AccountUserService;
use App\Domain\LookingForPropertyRequestReporter\LookingForPropertyRequestReporterConfigurationService;
use App\Domain\PropertyVacancyReporter\PropertyVacancyReporterConfigurationService;
use App\Form\Type\Frontend\PasswordForgottenType;
use App\Form\Type\Frontend\PasswordResetType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

#[IsGranted('PUBLIC_ACCESS')]
class FrontendController extends AbstractController
{
    public function __construct(
        private readonly AccountService $accountService
    ) {
    }

    #[Route('/impressum', name: 'imprint', methods: ['GET'])]
    public function imprint(): Response
    {
        $account = $this->accountService->getOperatorAccount();

        $accountLegalTextsConfiguration = $this->accountService->fetchAccountLegalTextsConfigurationByAccount(account: $account);

        return $this->render(view: 'public_access/imprint.html.twig', parameters: [
            'pageTitle'                      => 'Impressum',
            'activeNavItem'                  => 'imprint',
            'accountLegalTextsConfiguration' => $accountLegalTextsConfiguration,
        ]);
    }

    #[Route('/informationen/anbietende', name: 'information_for_provider', methods: ['GET'])]
    public function informationForProvider(): Response
    {
        return $this->render(view: 'public_access/information_for_provider.html.twig', parameters: [
            'pageTitle' => 'Informationen für Flächenanbietende',
        ]);
    }

    #[Route('/informationen/suchende', name: 'information_for_seeker', methods: ['GET'])]
    public function informationForSeeker(): Response
    {
        return $this->render(view: 'public_access/information_for_seeker.html.twig', parameters: [
            'pageTitle' => 'Informationen für Flächensuchende',
        ]);
    }

    #[Route('/datenschutz-fuer-anbietende', name: 'privacy_policy_provider', methods: ['GET'])]
    public function privacyPolicyProvider(): Response
    {
        $account = $this->accountService->getOperatorAccount();

        $accountLegalTextsConfiguration = $this->accountService->fetchAccountLegalTextsConfigurationByAccount(account: $account);

        return $this->render(view: 'public_access/privacy_policy_provider.html.twig', parameters: [
            'pageTitle'                      => 'Datenschutzerklärung für Anbietende',
            'activeNavItem'                  => 'privacyPolicy',
            'accountLegalTextsConfiguration' => $accountLegalTextsConfiguration,
        ]);
    }

    #[Route('/datenschutz-fuer-suchende', name: 'privacy_policy_seeker', methods: ['GET'])]
    public function privacyPolicySeeker(): Response
    {
        $account = $this->accountService->getOperatorAccount();

        $accountLegalTextsConfiguration = $this->accountService->fetchAccountLegalTextsConfigurationByAccount(account: $account);

        return $this->render(view: 'public_access/privacy_policy_seeker.html.twig', parameters: [
            'pageTitle'                      => 'Datenschutzerklärung für Suchende',
            'activeNavItem'                  => 'privacyPolicy',
            'accountLegalTextsConfiguration' => $accountLegalTextsConfiguration,
        ]);
    }

    #[Route('/agb-fuer-anbietende', name: 'gtc_provider', methods: ['GET'])]
    public function gtcProvider(): Response
    {
        $account = $this->accountService->getOperatorAccount();

        $accountLegalTextsConfiguration = $this->accountService->fetchAccountLegalTextsConfigurationByAccount(account: $account);

        return $this->render(view: 'public_access/gtc_provider.html.twig', parameters: [
            'pageTitle'                      => 'Nutzungsbedingungen für Anbietende',
            'activeNavItem'                  => 'gtc',
            'accountLegalTextsConfiguration' => $accountLegalTextsConfiguration,
        ]);
    }

    #[Route('/agb-fuer-suchende', name: 'gtc_seeker', methods: ['GET'])]
    public function gtcSeeker(): Response
    {
        $account = $this->accountService->getOperatorAccount();

        $accountLegalTextsConfiguration = $this->accountService->fetchAccountLegalTextsConfigurationByAccount(account: $account);

        return $this->render(view: 'public_access/gtc_seeker.html.twig', parameters: [
            'pageTitle'                      => 'Nutzungsbedingungen für Suchende',
            'activeNavItem'                  => 'gtc',
            'accountLegalTextsConfiguration' => $accountLegalTextsConfiguration,
        ]);
    }

    #[Route('/kennwort-vergessen', name: 'password_forgotten', methods: ['GET', 'POST'])]
    public function passwordForgotten(
        Request $request,
        AccountUserService $accountUserService,
        AccountUserPasswordResetService $accountUserPasswordResetService
    ): Response {
        $passwordForgottenForm = $this->createForm(type: PasswordForgottenType::class);

        $passwordForgottenForm->add(child: 'save', type: SubmitType::class);

        $passwordForgottenForm->handleRequest(request: $request);

        if ($passwordForgottenForm->isSubmitted() && $passwordForgottenForm->isValid()) {
            $account = $this->accountService->fetchAccountByAppHostname($request->getHost());

            if ($account === null) {
                return $this->render(view: 'public_access/password_forgotten_submitted.html.twig');
            }

            $accountUser = $accountUserService->fetchAccountUserFromAllAccountsByEmail(email: $passwordForgottenForm->getData()['email']);

            if ($accountUser === null) {
                return $this->render(view: 'public_access/password_forgotten_submitted.html.twig');
            }

            $accountUserPasswordResetService->doPasswordResetForAccountUser(accountUser: $accountUser, createdByAccountUser: $accountUser);

            return $this->render(view: 'public_access/password_forgotten_submitted.html.twig');
        }

        return $this->render(view: 'public_access/password_forgotten.html.twig', parameters: [
            'passwordForgottenForm' => $passwordForgottenForm,
            'pageTitle'             => 'Kennwort vergessen',
        ]);
    }

    #[Route('/passwort-setzen/{passwordResetToken<[a-f0-9]{64}>}', name: 'reset_password', methods: ['GET', 'POST'])]
    public function resetPassword(
        string $passwordResetToken,
        Request $request,
        AccountUserPasswordResetService $accountUserPasswordResetService,
        AccountUserService $accountUserService,
        AccountUserEmailService $accountUserEmailService,
        AccountUserDeletionService $accountUserDeletionService
    ): Response {
        $passwordResetSuccess = $request->getSession()->getFlashBag()->get(type: 'passwordResetSuccess');

        if (empty($passwordResetSuccess) === false && $passwordResetSuccess[0]['success'] === true) {
            return $this->render(view: 'public_access/password_reset_success.html.twig', parameters: [
                'pageTitle' => 'Kennwort geändert',
            ]);
        }

        $accountUserPasswordReset = $accountUserPasswordResetService->fetchActiveAccountUserPasswordResetByToken(token: $passwordResetToken);

        if ($accountUserPasswordReset === null) {
            throw new NotFoundHttpException();
        }

        $passwordResetForm = $this->createForm(type: PasswordResetType::class);

        $passwordResetForm->add(child: 'save', type: SubmitType::class);

        $passwordResetForm->handleRequest(request: $request);

        if ($passwordResetForm->isSubmitted() && $passwordResetForm->isValid()) {
            $plainPassword = $passwordResetForm->getData()['password'];

            if ($accountUserService->isThePasswordSecure($plainPassword) === false) {
                return $this->render(view: 'public_access/password_reset.html.twig', parameters: [
                    'passwordResetForm' => $passwordResetForm,
                    'error'             => 'Das Kennwort muss mindestens 8 Zeichen lang sein, mindestens je einen Großbuchstaben, einen Kleinbuchstaben und eine Zahl enthalten. Idealerweise auch ein Sonderzeichen.',
                    'pageTitle'         => 'Neues Passwort setzen',
                ]);
            } else {
                $accountUserService->updatePasswordFromAccountUser(accountUser: $accountUserPasswordReset->getAccountUser(), plainPassword: $plainPassword);

                $accountUserEmailService->sendAccountUserPasswordChangeSuccessfulMail(accountUserPasswordReset: $accountUserPasswordReset);

                $accountUserDeletionService->deleteAccountUserPasswordReset(accountUserPasswordReset: $accountUserPasswordReset);

                $this->addFlash(type: 'passwordResetSuccess', message: ['success' => true]);

                return $this->redirectToRoute(route: 'reset_password', parameters: ['passwordResetToken' => $accountUserPasswordReset->getPasswordToken()]);
            }
        }

        return $this->render(view: 'public_access/password_reset.html.twig', parameters: [
            'passwordResetForm' => $passwordResetForm,
            'pageTitle'         => 'Neues Passwort setzen',
            'error'             => null,
        ]);
    }

    #[Route('/leerstand-melden', name: 'report_property_vacancy', methods: ['GET'])]
    public function reportPropertyVacancy(
        Request $request,
        PropertyVacancyReporterConfigurationService $propertyVacancyReporterConfigurationService
    ): Response {
        $account = $this->accountService->fetchAccountByAppHostname(appHostname: $request->getHost());

        if ($account === null) {
            throw new NotFoundHttpException();
        }

        $propertyVacancyReporterConfiguration = $propertyVacancyReporterConfigurationService->fetchPropertyVacancyReporterConfigurationByAccount(
            account: $account
        );

        if ($propertyVacancyReporterConfiguration === null) {
            throw new NotFoundHttpException();
        }

        return $this->render(view: 'public_access/report_property_vacancy.html.twig', parameters: [
            'accountKey' => $propertyVacancyReporterConfiguration->getAccountKey()->toRfc4122(),
            'pageTitle'  => 'Leerstand melden',
        ]);
    }

    #[Route('/gesuch-melden', name: 'report_looking_for_property_request', methods: ['GET'])]
    public function reportLookingForPropertyRequest(
        Request $request,
        LookingForPropertyRequestReporterConfigurationService $lookingForPropertyRequestReporterConfigurationService
    ): Response {
        $account = $this->accountService->fetchAccountByAppHostname(appHostname: $request->getHost());

        if ($account === null) {
            throw new NotFoundHttpException();
        }

        $lookingForPropertyRequestReporterConfiguration = $lookingForPropertyRequestReporterConfigurationService->fetchLookingForPropertyRequestReporterConfigurationByAccount(
            account: $account
        );

        if ($lookingForPropertyRequestReporterConfiguration === null) {
            throw new NotFoundHttpException();
        }

        return $this->render(view: 'public_access/report_looking_for_property_request.html.twig', parameters: [
            'accountKey' => $lookingForPropertyRequestReporterConfiguration->getAccountKey()->toRfc4122(),
            'pageTitle'  => 'Gesuch melden',
        ]);
    }
}
