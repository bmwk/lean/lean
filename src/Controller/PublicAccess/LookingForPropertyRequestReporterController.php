<?php

declare(strict_types=1);

namespace App\Controller\PublicAccess;

use App\Domain\LookingForPropertyRequestReport\LookingForPropertyRequestReportService;
use App\Domain\LookingForPropertyRequestReporter\LookingForPropertyRequestReporterConfigurationService;
use App\Domain\LookingForPropertyRequestReporter\LookingForPropertyRequestReporterEmailService;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Uid\Uuid;

#[IsGranted('PUBLIC_ACCESS')]
#[Route('/gesuchsmelder', name: 'looking_for_property_request_reporter_')]
class LookingForPropertyRequestReporterController extends AbstractController
{
    public function __construct(
        private readonly LookingForPropertyRequestReportService $lookingForPropertyRequestReportService,
        private readonly LookingForPropertyRequestReporterEmailService $lookingForPropertyRequestReporterEmailService,
        private readonly LookingForPropertyRequestReporterConfigurationService $lookingForPropertyRequestReporterConfigurationService,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    #[Route('/meldung/{reportUuid<[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}>}/bestaetigung/{confirmationToken<[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}>}', name: 'report_confirmation', methods: ['GET'])]
    public function reportConfirmation(string $reportUuid, string $confirmationToken): Response
    {
        $lookingForPropertyRequestReport = $this->lookingForPropertyRequestReportService->fetchLookingForPropertyRequestReportByUuid(uuid: Uuid::fromString($reportUuid));

        if ($lookingForPropertyRequestReport === null) {
            throw new NotFoundHttpException();
        }

        if ($lookingForPropertyRequestReport->isEmailConfirmed() === false) {
            $isAlreadyConfirmed = false;

            if ($lookingForPropertyRequestReport->getConfirmationToken()->toRfc4122() !== $confirmationToken) {
                throw new NotFoundHttpException();
            }

            $lookingForPropertyRequestReporterConfiguration = $this->lookingForPropertyRequestReporterConfigurationService
                ->fetchLookingForPropertyRequestReporterConfigurationByAccount(account: $lookingForPropertyRequestReport->getAccount());

            $lookingForPropertyRequestReport
                ->setEmailConfirmed(true)
                ->setEmailConfirmedAt(new \DateTimeImmutable());

            $this->entityManager->flush();

            $this->lookingForPropertyRequestReporterEmailService->sendNewLookingForPropertyRequestReportNotificationEmail(
                lookingForPropertyRequestReport: $lookingForPropertyRequestReport,
                lookingForPropertyRequestReporterConfiguration: $lookingForPropertyRequestReporterConfiguration
            );
        } else {
            $isAlreadyConfirmed = true;
        }

        $account = $lookingForPropertyRequestReport->getAccount();

        return $this->render(view: 'company_location_management/looking_for_property_request_report/confirmation_landingpage.html.twig', parameters: [
            'pageTitle'          => 'Bestätigung Ihrer Gesuchsmeldung',
            'isAlreadyConfirmed' => $isAlreadyConfirmed,
            'account'            => $account,
        ]);
    }
}
