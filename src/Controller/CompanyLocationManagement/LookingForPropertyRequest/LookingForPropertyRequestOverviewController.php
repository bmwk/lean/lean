<?php

declare(strict_types=1);

namespace App\Controller\CompanyLocationManagement\LookingForPropertyRequest;

use App\Controller\AbstractLeAnController;
use App\Domain\Entity\AccountUser\AccountUserType;
use App\Domain\Entity\LastVisitedPage;
use App\Domain\Entity\LookingForPropertyRequest\LookingForPropertyRequestFilter;
use App\Domain\Entity\LookingForPropertyRequest\LookingForPropertyRequestSearch;
use App\Domain\Entity\PaginationParameter;
use App\Domain\Entity\SortingOption\SortingOption;
use App\Domain\LookingForPropertyRequest\LookingForPropertyRequestSecurityService;
use App\Domain\LookingForPropertyRequest\LookingForPropertyRequestService;
use App\Domain\SessionStoredUrlParameter\SessionStoredUrlParameterService;
use App\Form\Type\CompanyLocationManagement\LookingForPropertyRequest\LookingForPropertyRequestFilterType;
use App\Form\Type\CompanyLocationManagement\LookingForPropertyRequest\LookingForPropertyRequestSearchType;
use App\SessionStorage\SessionStorageService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Security("is_granted('ROLE_USER') or is_granted('ROLE_LOOKING_FOR_PROPERTY_REQUEST_FEEDER') or is_granted('ROLE_PROPERTY_SEEKER') or is_granted('ROLE_VIEWER') or is_granted('ROLE_RESTRICTED_VIEWER')")]
#[Route('/ansiedlung/gesuche', name: 'company_location_management_looking_for_property_request_')]
class LookingForPropertyRequestOverviewController extends AbstractLeAnController
{
    private const ACTIVE_NAV_GROUP = 'company_location_management';
    private const ACTIVE_NAV_ITEM = self::ACTIVE_NAV_GROUP . '_looking_for_property_request';

    private const OVERVIEW_SESSION_KEY_NAME = 'lookingForPropertyRequestOverview';

    private const URL_PARAMETER_OBJECT_CLASSES = [
        PaginationParameter::class,
        SortingOption::class,
    ];

    public function __construct(
        SessionStorageService $sessionStorageService,
        SessionStoredUrlParameterService $sessionStoredUrlParameterService,
        private readonly LookingForPropertyRequestService $lookingForPropertyRequestService,
        private readonly LookingForPropertyRequestSecurityService $lookingForPropertyRequestSecurityService
    ) {
        parent::__construct(
            sessionStorageService: $sessionStorageService,
            sessionStoredUrlParameterService: $sessionStoredUrlParameterService
        );
    }

    #[Route('/uebersicht', name: 'overview', methods: ['GET', 'POST'])]
    #[Route('/uebersicht/inaktiv', name: 'overview_inactive', methods: ['GET', 'POST'])]
    #[Route('/uebersicht/archiv', name: 'overview_archive', methods: ['GET', 'POST'])]
    public function overview(Request $request, UserInterface $user): Response
    {
        $route = $request->attributes->get(key: '_route');

        if ($route === 'company_location_management_looking_for_property_request_overview') {
            $archived = false;
            $active = true;
            $pageTitle = 'Ansiedlungsmanagement - Aktive Gesuche';
            $view = 'company_location_management/looking_for_property_request/overview_active.html.twig';
        } elseif ($route === 'company_location_management_looking_for_property_request_overview_inactive') {
            $archived = false;
            $active = false;
            $pageTitle = 'Ansiedlungsmanagement - Inaktive Gesuche';
            $view = 'company_location_management/looking_for_property_request/overview_inactive.html.twig';
        } elseif ($route === 'company_location_management_looking_for_property_request_overview_archive') {
            $archived = true;
            $active = null;
            $pageTitle = 'Ansiedlungsmanagement - Archivierte Gesuche';
            $view = 'company_location_management/looking_for_property_request/overview_archive.html.twig';
        } else {
            throw new NotFoundHttpException();
        }

        $account = $user->getAccount();

        $paginationParameter = PaginationParameter::createFromRequest(request: $request);

        $this->storeObjectInSession(object: $paginationParameter, sessionKeyName: self::OVERVIEW_SESSION_KEY_NAME);

        $sortingOption = null;

        if ($request->query->get(key: 'sortieren') !== null) {
            $sortingOption = SortingOption::createFromRequest(request: $request);

            $this->storeObjectInSession(object: $sortingOption, sessionKeyName: self::OVERVIEW_SESSION_KEY_NAME);
        }

        $lookingForPropertyRequestFilterForm = $this->createForm(type: LookingForPropertyRequestFilterType::class, options: [
            'account'        => $account,
            'dataClassName'  => LookingForPropertyRequestFilter::class,
            'sessionKeyName' => self::OVERVIEW_SESSION_KEY_NAME,
        ]);

        $lookingForPropertyRequestFilterForm->add(child: 'apply', type: SubmitType::class);

        $lookingForPropertyRequestFilterForm->handleRequest(request: $request);

        $lookingForPropertyRequestSearchForm = $this->createForm(type: LookingForPropertyRequestSearchType::class, options: [
            'dataClassName'  => LookingForPropertyRequestSearch::class,
            'sessionKeyName' => self::OVERVIEW_SESSION_KEY_NAME,
        ]);

        $lookingForPropertyRequestSearchForm->add(child: 'search', type: SubmitType::class);

        $lookingForPropertyRequestSearchForm->handleRequest(request: $request);

        $urlParameters = $this->fetchMergedUrlParametersFromSession(
            urlParameterObjectClasses: self::URL_PARAMETER_OBJECT_CLASSES,
            sessionKeyName: self::OVERVIEW_SESSION_KEY_NAME
        );

        $lastVisitedPage = new LastVisitedPage(routeName: $route, parameters: $urlParameters);

        $this->storeObjectInSession(object: $lastVisitedPage, sessionKeyName: self::OVERVIEW_SESSION_KEY_NAME);

        if ($user->getAccountUserType() === AccountUserType::INTERNAL) {
            $lookingForPropertyRequests = $this->lookingForPropertyRequestService->fetchLookingForPropertyRequestsPaginatedByAccount(
                account: $account,
                withFromSubAccounts: false,
                firstResult: $paginationParameter->getFirstResult(),
                maxResults: $paginationParameter->getLimit(),
                archived: $archived,
                active: $active,
                lookingForPropertyRequestFilter: $lookingForPropertyRequestFilterForm->getData(),
                lookingForPropertyRequestSearch: $lookingForPropertyRequestSearchForm->getData(),
                sortingOption: $sortingOption
            );
        } else {
            $lookingForPropertyRequests = $this->lookingForPropertyRequestService->fetchLookingForPropertyRequestsPaginatedByPerson(
                account: $account,
                withFromSubAccounts: false,
                person: $user->getPerson(),
                firstResult: $paginationParameter->getFirstResult(),
                maxResults: $paginationParameter->getLimit(),
                archived: $archived,
                active: $active,
                sortingOption: $sortingOption
            );
        }

        return $this->render(view: $view, parameters: [
            'lookingForPropertyRequests'          => $lookingForPropertyRequests,
            'lookingForPropertyRequestSearchForm' => $lookingForPropertyRequestSearchForm,
            'lookingForPropertyRequestFilterForm' => $lookingForPropertyRequestFilterForm,
            'lookingForPropertyRequestFilter'     => $lookingForPropertyRequestFilterForm->getData(),
            'canCreateLookingForPropertyRequest'  => $this->lookingForPropertyRequestSecurityService->canCreateLookingForPropertyRequest(),
            'pageTitle'                           => $pageTitle,
            'activeNavGroup'                      => self::ACTIVE_NAV_GROUP,
            'activeNavItem'                       => self::ACTIVE_NAV_ITEM,
        ]);
    }
}
