<?php

declare(strict_types=1);

namespace App\Controller\CompanyLocationManagement\LookingForPropertyRequest;

use App\Domain\Entity\ForwardingResponse;
use App\Domain\LookingForPropertyRequest\LookingForPropertyRequestService;
use App\Domain\Property\PdfExpose\PdfExposeService;
use App\Form\Type\CompanyLocationManagement\LookingForPropertyRequest\ExposeForwardingRejectType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[IsGranted('ROLE_PROPERTY_SEEKER')]
#[Route('/ansiedlung/angebote', name: 'company_location_management_expose_forwarding_')]
class ExposeForwardingController extends AbstractController
{
    private const ACTIVE_NAV_GROUP = 'company_location_management';
    private const ACTIVE_NAV_ITEM = self::ACTIVE_NAV_GROUP . '_looking_for_property_request';

    public function __construct(
        private readonly LookingForPropertyRequestService $lookingForPropertyRequestService,
        private readonly PdfExposeService $pdfExposeService,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    #[Route('/{lookingForPropertyRequestId<\d{1,10}>}/uebersicht', name: 'overview', methods: ['GET', 'POST'])]
    public function overview(int $lookingForPropertyRequestId, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $lookingForPropertyRequest = $this->lookingForPropertyRequestService->fetchLookingForPropertyRequestByPersonAndId(
            account: $account,
            withFromSubAccounts: false,
            person: $user->getPerson(),
            id: $lookingForPropertyRequestId
        );

        if ($lookingForPropertyRequest === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'view', subject: $lookingForPropertyRequest);

        $lookingForPropertyRequestExposeForwardings = $this->lookingForPropertyRequestService->fetchLookingForPropertyRequestExposeForwardingByPersonAndLookingForPropertyRequest(
            account: $account,
            person: $user->getPerson(),
            lookingForPropertyRequest: $lookingForPropertyRequest
        );

        return $this->render(view: 'company_location_management/looking_for_property_request/expose_forwarding/overview.html.twig', parameters: [
            'exposeForwardings'         => $lookingForPropertyRequestExposeForwardings,
            'lookingForPropertyRequest' => $lookingForPropertyRequest,
            'activeNavGroup'            => self::ACTIVE_NAV_GROUP,
            'activeNavItem'             => self::ACTIVE_NAV_ITEM,
        ]);
    }

    #[Route('/{exposeForwardingId<\d{1,10}>}', name: 'pdf_expose', methods: ['GET'])]
    public function pdfExpose(int $exposeForwardingId, UserInterface $user): Response
    {
        $lookingForPropertyRequestExposeForwarding = $this->lookingForPropertyRequestService->fetchLookingForPropertyRequestExposeForwardingByPersonAndId(
            account: $user->getAccount(),
            person: $user->getPerson(),
            id: $exposeForwardingId
        );

        if ($lookingForPropertyRequestExposeForwarding === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'view', subject: $lookingForPropertyRequestExposeForwarding);

        $lookingForPropertyRequest = $lookingForPropertyRequestExposeForwarding->getLookingForPropertyRequest();

        if ($lookingForPropertyRequest->getAssignedToAccountUser() !== null) {
            $contactAccountUser = $lookingForPropertyRequest->getAssignedToAccountUser();
        } else {
            $contactAccountUser = $lookingForPropertyRequestExposeForwarding->getCreatedByAccountUser();
        }

        $buildingUnit = $lookingForPropertyRequestExposeForwarding->getBuildingUnit();

        $pdfContent = $this->pdfExposeService->generateBuildingUnitPdfExpose(
            buildingUnit: $buildingUnit,
            accountUser: $contactAccountUser
        );

        return new Response(content: $pdfContent, headers: ['Content-Type' => 'application/pdf']);
    }

    #[Route('/{exposeForwardingId<\d{1,10}>}/status-setzen/{forwardingResponse<\d{1,3}>}', name: 'set_forwarding_response', methods: ['GET'])]
    public function setForwardingResponse(int $exposeForwardingId, ForwardingResponse $forwardingResponse, UserInterface $user): Response
    {
        $lookingForPropertyRequestExposeForwarding = $this->lookingForPropertyRequestService->fetchLookingForPropertyRequestExposeForwardingByPersonAndId(
            account: $user->getAccount(),
            person: $user->getPerson(),
            id: $exposeForwardingId
        );

        if ($lookingForPropertyRequestExposeForwarding === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'setForwardingResponse', subject: $lookingForPropertyRequestExposeForwarding);

        $lookingForPropertyRequestExposeForwarding->setForwardingResponse($forwardingResponse);

        $this->entityManager->persist(entity: $lookingForPropertyRequestExposeForwarding);
        $this->entityManager->flush();

        $this->addFlash(type: 'dataSaved', message: 'Der Status wurde geändert.');

        return $this->redirectToRoute(route: 'company_location_management_expose_forwarding_overview', parameters: ['lookingForPropertyRequestId' => $lookingForPropertyRequestExposeForwarding->getLookingForPropertyRequest()->getId()]);
    }

    #[Route('/{exposeForwardingId<\d{1,10}>}/auf-abgelehnt-setzen', name: 'reject', methods: ['GET', 'POST'])]
    public function reject(int $exposeForwardingId, Request $request, UserInterface $user): Response
    {
        $lookingForPropertyRequestExposeForwarding= $this->lookingForPropertyRequestService->fetchLookingForPropertyRequestExposeForwardingByPersonAndId(
            account: $user->getAccount(),
            person: $user->getPerson(),
            id: $exposeForwardingId
        );

        if ($lookingForPropertyRequestExposeForwarding === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'reject', subject: $lookingForPropertyRequestExposeForwarding);

        $exposeForwardingRejectForm = $this->createForm(type: ExposeForwardingRejectType::class, data: $lookingForPropertyRequestExposeForwarding);

        $exposeForwardingRejectForm->add(child: 'save', type: SubmitType::class);

        $exposeForwardingRejectForm->handleRequest(request: $request);

        if ($exposeForwardingRejectForm->isSubmitted() && $exposeForwardingRejectForm->isValid()) {
            $lookingForPropertyRequestExposeForwarding->setForwardingResponse(forwardingResponse: ForwardingResponse::REJECTED);

            $this->entityManager->persist(entity: $lookingForPropertyRequestExposeForwarding);
            $this->entityManager->flush();

            $this->addFlash(type: 'dataSaved', message: 'Der Status wurde auf abgelehnt gesetzt.');

            return $this->redirectToRoute(route: 'company_location_management_expose_forwarding_overview', parameters: ['lookingForPropertyRequestId' => $lookingForPropertyRequestExposeForwarding->getLookingForPropertyRequest()->getId()]);
        }

        return $this->render(view: 'company_location_management/looking_for_property_request/expose_forwarding/forwarding_reject.html.twig', parameters: [
            'exposeForwarding'           => $lookingForPropertyRequestExposeForwarding,
            'exposeForwardingRejectForm' => $exposeForwardingRejectForm,
            'activeNavGroup'             => self::ACTIVE_NAV_GROUP,
            'activeNavItem'              => self::ACTIVE_NAV_ITEM,
        ]);
    }
}
