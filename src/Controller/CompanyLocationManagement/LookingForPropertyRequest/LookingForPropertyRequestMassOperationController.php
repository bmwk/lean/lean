<?php

declare(strict_types=1);

namespace App\Controller\CompanyLocationManagement\LookingForPropertyRequest;

use App\Domain\Entity\AccountUser\AccountUserType;
use App\Domain\Entity\DeletionType;
use App\Domain\Entity\LookingForPropertyRequest\LookingForPropertyRequest;
use App\Domain\LookingForPropertyRequest\LookingForPropertyRequestDeletionService;
use App\Domain\LookingForPropertyRequest\LookingForPropertyRequestService;
use App\Form\Type\CompanyLocationManagement\LookingForPropertyRequest\LookingForPropertyRequestMassOperationArchiveType;
use App\Form\Type\CompanyLocationManagement\LookingForPropertyRequest\LookingForPropertyRequestMassOperationAssignToAccountUserType;
use App\Form\Type\CompanyLocationManagement\LookingForPropertyRequest\LookingForPropertyRequestMassOperationDeleteType;
use App\Form\Type\CompanyLocationManagement\LookingForPropertyRequest\LookingForPropertyRequestMassOperationExtendDurationType;
use App\Form\Type\CompanyLocationManagement\LookingForPropertyRequest\LookingForPropertyRequestMassOperationUnarchiveType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Security("is_granted('ROLE_USER') or is_granted('ROLE_LOOKING_FOR_PROPERTY_REQUEST_FEEDER') or is_granted('ROLE_PROPERTY_SEEKER')")]
#[Route('/ansiedlung/gesuche/massenoperation', name: 'company_location_management_looking_for_property_request_mass_operation_')]
class LookingForPropertyRequestMassOperationController extends AbstractController
{
    private const ACTIVE_NAV_GROUP = 'company_location_management';
    private const ACTIVE_NAV_ITEM = self::ACTIVE_NAV_GROUP . '_looking_for_property_request';

    public function __construct(
        private readonly LookingForPropertyRequestDeletionService $lookingForPropertyRequestDeletionService,
        private readonly LookingForPropertyRequestService $lookingForPropertyRequestService,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    #[Route('/archivieren', name: 'archive', methods: ['POST'])]
    public function archive(Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $lookingForPropertyRequestMassOperationArchiveForm = $this->createForm(type: LookingForPropertyRequestMassOperationArchiveType::class);

        $lookingForPropertyRequestMassOperationArchiveForm->add(child: 'archive', type: SubmitType::class);

        $lookingForPropertyRequestMassOperationArchiveForm->handleRequest(request: $request);

        if ($request->get('selectedRowIds') !== null) {
            $lookingForPropertyRequestMassOperationArchiveForm->get('selectedRowIds')->setData($request->get('selectedRowIds'));
        }

        if ($user->getAccountUserType() === AccountUserType::INTERNAL) {
            $lookingForPropertyRequests = $this->lookingForPropertyRequestService->fetchLookingForPropertyRequestsByIds(
                account: $account,
                withFromSubAccounts: false,
                ids: json_decode($lookingForPropertyRequestMassOperationArchiveForm->get('selectedRowIds')->getData()),
                archived: false
            );
        } else {
            $lookingForPropertyRequests = $this->lookingForPropertyRequestService->fetchLookingForPropertyRequestsByPersonAndIds(
                account: $account,
                withFromSubAccounts: false,
                person: $user->getPerson(),
                ids: json_decode($lookingForPropertyRequestMassOperationArchiveForm->get('selectedRowIds')->getData()),
                archived: false
            );
        }

        $lookingForPropertyRequests = array_filter(
            array: $lookingForPropertyRequests,
            callback: function (LookingForPropertyRequest $lookingForPropertyRequest): bool {
                return $this->isGranted(attribute: 'edit', subject: $lookingForPropertyRequest);
            }
        );

        if ($lookingForPropertyRequestMassOperationArchiveForm->isSubmitted() && $lookingForPropertyRequestMassOperationArchiveForm->isValid()) {
            foreach ($lookingForPropertyRequests as $lookingForPropertyRequest) {
                if ($this->isGranted(attribute: 'edit', subject: $lookingForPropertyRequest) === false) {
                    continue;
                }

                $lookingForPropertyRequest
                    ->setArchived(true)
                    ->setArchivedAt(new \DateTimeImmutable());
            }

            $this->entityManager->flush();

            $this->addFlash(type: 'dataDeleted', message: 'Die Gesuche wurden archiviert.');

            return $this->redirectToRoute(route: 'company_location_management_looking_for_property_request_overview');
        }

        return $this->render(view: 'company_location_management/looking_for_property_request/mass_operation/archive.html.twig', parameters: [
            'lookingForPropertyRequestMassOperationArchiveForm' => $lookingForPropertyRequestMassOperationArchiveForm,
            'lookingForPropertyRequests'                        => $lookingForPropertyRequests,
            'pageTitle'                                         => 'Ansiedlungsmanagement - Gesuche archivieren',
            'activeNavGroup'                                    => self::ACTIVE_NAV_GROUP,
            'activeNavItem'                                     => self::ACTIVE_NAV_ITEM,
        ]);
    }

    #[Route('/aus-dem-archiv-wiederherstellen', name: 'unarchive', methods: ['POST'])]
    public function unarchive(Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $lookingForPropertyRequestMassOperationUnarchiveForm = $this->createForm(type: LookingForPropertyRequestMassOperationUnarchiveType::class);

        $lookingForPropertyRequestMassOperationUnarchiveForm->add(child: 'unarchive', type: SubmitType::class);

        $lookingForPropertyRequestMassOperationUnarchiveForm->handleRequest(request: $request);

        if ($request->get('selectedRowIds') !== null) {
            $lookingForPropertyRequestMassOperationUnarchiveForm->get('selectedRowIds')->setData($request->get('selectedRowIds'));
        }

        if ($user->getAccountUserType() === AccountUserType::INTERNAL) {
            $lookingForPropertyRequests = $this->lookingForPropertyRequestService->fetchLookingForPropertyRequestsByIds(
                account: $account,
                withFromSubAccounts: false,
                ids: json_decode($lookingForPropertyRequestMassOperationUnarchiveForm->get('selectedRowIds')->getData()),
                archived: true
            );
        } else {
            $lookingForPropertyRequests = $this->lookingForPropertyRequestService->fetchLookingForPropertyRequestsByPersonAndIds(
                account: $account,
                withFromSubAccounts: false,
                person: $user->getPerson(),
                ids: json_decode($lookingForPropertyRequestMassOperationUnarchiveForm->get('selectedRowIds')->getData()),
                archived: true
            );
        }

        $lookingForPropertyRequests = array_filter(
            array: $lookingForPropertyRequests,
            callback: function (LookingForPropertyRequest $lookingForPropertyRequest): bool {
                return $this->isGranted(attribute: 'edit', subject: $lookingForPropertyRequest);
            }
        );

        if ($lookingForPropertyRequestMassOperationUnarchiveForm->isSubmitted() && $lookingForPropertyRequestMassOperationUnarchiveForm->isValid()) {
            foreach ($lookingForPropertyRequests as $lookingForPropertyRequest) {
                if ($this->isGranted(attribute: 'edit', subject: $lookingForPropertyRequest) === false) {
                    continue;
                }

                $lookingForPropertyRequest
                    ->setArchived(false)
                    ->setArchivedAt(null);
            }

            $this->entityManager->flush();

            $this->addFlash(type: 'dataDeleted', message: 'Die Gesuche wurden aus dem Archiv wiederhergestellt.');

            return $this->redirectToRoute(route: 'company_location_management_looking_for_property_request_overview');
        }

        return $this->render(view: 'company_location_management/looking_for_property_request/mass_operation/unarchive.html.twig', parameters: [
            'lookingForPropertyRequestMassOperationUnarchiveForm' => $lookingForPropertyRequestMassOperationUnarchiveForm,
            'lookingForPropertyRequests'                          => $lookingForPropertyRequests,
            'pageTitle'                                           => 'Ansiedlungsmanagement - Gesuche archivieren',
            'activeNavGroup'                                      => self::ACTIVE_NAV_GROUP,
            'activeNavItem'                                       => self::ACTIVE_NAV_ITEM,
        ]);
    }

    #[Route('/loeschen', name: 'delete', methods: ['POST'])]
    public function delete(Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $lookingForPropertyRequestMassOperationDeleteForm = $this->createForm(type: LookingForPropertyRequestMassOperationDeleteType::class);

        $lookingForPropertyRequestMassOperationDeleteForm->add(child: 'delete', type: SubmitType::class);

        $lookingForPropertyRequestMassOperationDeleteForm->handleRequest(request: $request);

        if ($request->get('selectedRowIds') !== null) {
            $lookingForPropertyRequestMassOperationDeleteForm->get('selectedRowIds')->setData($request->get('selectedRowIds'));
        }

        if ($user->getAccountUserType() === AccountUserType::INTERNAL) {
            $lookingForPropertyRequests = $this->lookingForPropertyRequestService->fetchLookingForPropertyRequestsByIds(
                account: $account,
                withFromSubAccounts: false,
                ids: json_decode($lookingForPropertyRequestMassOperationDeleteForm->get('selectedRowIds')->getData())
            );
        } else {
            $lookingForPropertyRequests = $this->lookingForPropertyRequestService->fetchLookingForPropertyRequestsByPersonAndIds(
                account: $account,
                withFromSubAccounts: false,
                person: $user->getPerson(),
                ids: json_decode($lookingForPropertyRequestMassOperationDeleteForm->get('selectedRowIds')->getData())
            );
        }

        $lookingForPropertyRequests = array_filter(
            array: $lookingForPropertyRequests,
            callback: function (LookingForPropertyRequest $lookingForPropertyRequest): bool {
                return $this->isGranted(attribute: 'delete', subject: $lookingForPropertyRequest);
            }
        );

        if (
            $lookingForPropertyRequestMassOperationDeleteForm->isSubmitted()
            && $lookingForPropertyRequestMassOperationDeleteForm->isValid()
            && $lookingForPropertyRequestMassOperationDeleteForm->get('verificationCode')->getData() === 'gesuche_' . count($lookingForPropertyRequests)
        ) {
            foreach ($lookingForPropertyRequests as $lookingForPropertyRequest) {
                if ($this->isGranted(attribute: 'delete', subject: $lookingForPropertyRequest) === false) {
                    continue;
                }

                $this->lookingForPropertyRequestDeletionService->deleteLookingForPropertyRequest(
                    lookingForPropertyRequest: $lookingForPropertyRequest,
                    deletionType: DeletionType::SOFT,
                    deletedByAccountUser: $user
                );
            }

            $this->addFlash(type: 'dataDeleted', message: 'Die Gesuche wurden gelöscht.');

            return $this->redirectToRoute(route: 'company_location_management_looking_for_property_request_overview');
        }

        return $this->render(view: 'company_location_management/looking_for_property_request/mass_operation/delete.html.twig', parameters: [
            'lookingForPropertyRequestMassOperationDeleteForm' => $lookingForPropertyRequestMassOperationDeleteForm,
            'lookingForPropertyRequests'                       => $lookingForPropertyRequests,
            'pageTitle'                                        => 'Ansiedlungsmanagement - Gesuche löschen',
            'activeNavGroup'                                   => self::ACTIVE_NAV_GROUP,
            'activeNavItem'                                    => self::ACTIVE_NAV_ITEM,
        ]);
    }

    #[Route('/laufzeit-verlaengern', name: 'extend_duration', methods: ['POST'])]
    public function extendDuration(Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $lookingForPropertyRequestMassOperationExtendDurationForm = $this->createForm(type: LookingForPropertyRequestMassOperationExtendDurationType::class);

        $lookingForPropertyRequestMassOperationExtendDurationForm->add(child: 'extend', type: SubmitType::class);

        $lookingForPropertyRequestMassOperationExtendDurationForm->handleRequest(request: $request);

        if ($request->get('selectedRowIds') !== null) {
            $lookingForPropertyRequestMassOperationExtendDurationForm->get('selectedRowIds')->setData($request->get('selectedRowIds'));
        }

        if ($user->getAccountUserType() === AccountUserType::INTERNAL) {
            $lookingForPropertyRequests = $this->lookingForPropertyRequestService->fetchLookingForPropertyRequestsByIds(
                account: $account,
                withFromSubAccounts: false,
                ids: json_decode($lookingForPropertyRequestMassOperationExtendDurationForm->get('selectedRowIds')->getData())
            );
        } else {
            $lookingForPropertyRequests = $this->lookingForPropertyRequestService->fetchLookingForPropertyRequestsByPersonAndIds(
                account: $account,
                withFromSubAccounts: false,
                person: $user->getPerson(),
                ids: json_decode($lookingForPropertyRequestMassOperationExtendDurationForm->get('selectedRowIds')->getData())
            );
        }

        $lookingForPropertyRequests = array_filter(
            array: $lookingForPropertyRequests,
            callback: function (LookingForPropertyRequest $lookingForPropertyRequest): bool {
                return $this->isGranted(attribute: 'edit', subject: $lookingForPropertyRequest);
            }
        );

        if ($lookingForPropertyRequestMassOperationExtendDurationForm->isSubmitted() && $lookingForPropertyRequestMassOperationExtendDurationForm->isValid()) {
            foreach ($lookingForPropertyRequests as $lookingForPropertyRequest) {
                if ($this->isGranted(attribute: 'edit', subject: $lookingForPropertyRequest) === false) {
                    continue;
                }

                $requestAvailableTill = $lookingForPropertyRequest->getRequestAvailableTill();

                if ($requestAvailableTill !== null) {
                    $newRequestAvailableTill = clone $requestAvailableTill->modify('+1 month');
                    $lookingForPropertyRequest->setRequestAvailableTill($newRequestAvailableTill);
                }
            }

            $this->entityManager->flush();

            $this->addFlash(type: 'dataSaved', message: 'Die Laufzeit der Gesuche wurde um einen Monat verlängert.');

            return $this->redirectToRoute(route: 'company_location_management_looking_for_property_request_overview');
        }

        return $this->render(view: 'company_location_management/looking_for_property_request/mass_operation/extend_duration.html.twig', parameters: [
            'lookingForPropertyRequestMassOperationExtendDurationForm' => $lookingForPropertyRequestMassOperationExtendDurationForm,
            'lookingForPropertyRequests'                               => $lookingForPropertyRequests,
            'pageTitle'                                                => 'Ansiedlungsmanagement - Laufzeit der Gesuche verlängern',
            'activeNavGroup'                                           => self::ACTIVE_NAV_GROUP,
            'activeNavItem'                                            => self::ACTIVE_NAV_ITEM,
        ]);
    }

    #[Security("is_granted('ROLE_USER')")]
    #[Route('/sachbearbeiter-zuweisen', name: 'assign_to_account_user', methods: ['POST'])]
    public function assignToAccountUser(Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $lookingForPropertyRequestMassOperationAssignToAccountUserForm = $this->createForm(
            type: LookingForPropertyRequestMassOperationAssignToAccountUserType::class,
            options: ['account' => $account]
        );

        $lookingForPropertyRequestMassOperationAssignToAccountUserForm->add(child: 'save', type: SubmitType::class);

        $lookingForPropertyRequestMassOperationAssignToAccountUserForm->handleRequest(request: $request);

        if ($request->get('selectedRowIds') !== null) {
            $lookingForPropertyRequestMassOperationAssignToAccountUserForm->get('selectedRowIds')->setData($request->get('selectedRowIds'));
        }

        $lookingForPropertyRequests = $this->lookingForPropertyRequestService->fetchLookingForPropertyRequestsByIds(
            account: $account,
            withFromSubAccounts: false,
            ids: json_decode($lookingForPropertyRequestMassOperationAssignToAccountUserForm->get('selectedRowIds')->getData())
        );

        $lookingForPropertyRequests = array_filter(
            array: $lookingForPropertyRequests,
            callback: function (LookingForPropertyRequest $lookingForPropertyRequest): bool {
                return $this->isGranted(attribute: 'assignToAccountUser', subject: $lookingForPropertyRequest);
            }
        );

        if ($lookingForPropertyRequestMassOperationAssignToAccountUserForm->isSubmitted() && $lookingForPropertyRequestMassOperationAssignToAccountUserForm->isValid()) {
            $assignedToAccountUser = $lookingForPropertyRequestMassOperationAssignToAccountUserForm->get('assignedToAccountUser')->getData();

            foreach ($lookingForPropertyRequests as $lookingForPropertyRequest) {
                if ($this->isGranted(attribute: 'assignToAccountUser', subject: $lookingForPropertyRequest) === false) {
                    continue;
                }

                $lookingForPropertyRequest->setAssignedToAccountUser($assignedToAccountUser);
            }

            $this->entityManager->flush();

            if ($assignedToAccountUser === null) {
                $this->addFlash(type: 'dataDeleted', message: 'Es wurde kein/e Sachbearbeiter:in zugewiesen.');
            } else {
                $this->addFlash(type: 'dataSaved', message: 'Sachbearbeiter:in erfolgreich zugewiesen.');
            }

            return $this->redirectToRoute(route: 'company_location_management_looking_for_property_request_overview');
        }

        return $this->render(view: 'company_location_management/looking_for_property_request/mass_operation/assign_to_account_user.html.twig', parameters: [
            'lookingForPropertyRequestMassOperationAssignToAccountUserForm' => $lookingForPropertyRequestMassOperationAssignToAccountUserForm,
            'lookingForPropertyRequests'                                    => $lookingForPropertyRequests,
            'pageTitle'                                                     => 'Ansiedlungsmanagement - Sachbearbeiter:in zuweisen',
            'activeNavGroup'                                                => self::ACTIVE_NAV_GROUP,
            'activeNavItem'                                                 => self::ACTIVE_NAV_ITEM,
        ]);
    }
}
