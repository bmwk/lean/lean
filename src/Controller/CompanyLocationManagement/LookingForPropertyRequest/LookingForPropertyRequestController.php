<?php

declare(strict_types=1);

namespace App\Controller\CompanyLocationManagement\LookingForPropertyRequest;

use App\Controller\AbstractLeAnController;
use App\Domain\Classification\ClassificationService;
use App\Domain\Entity\AccountUser\AccountUserType;
use App\Domain\Entity\DeletionType;
use App\Domain\Entity\LastVisitedPage;
use App\Domain\Entity\LookingForPropertyRequest\LookingForPropertyRequest;
use App\Domain\Entity\LookingForPropertyRequest\PropertyRequirement;
use App\Domain\Entity\PlaceRelationType;
use App\Domain\Location\LocationService;
use App\Domain\LookingForPropertyRequest\LookingForPropertyRequestDeletionService;
use App\Domain\LookingForPropertyRequest\LookingForPropertyRequestSecurityService;
use App\Domain\LookingForPropertyRequest\LookingForPropertyRequestService;
use App\Domain\SessionStoredUrlParameter\SessionStoredUrlParameterService;
use App\Form\Type\CompanyLocationManagement\LookingForPropertyRequest\LookingForPropertyRequestAssignToAccountUserType;
use App\Form\Type\CompanyLocationManagement\LookingForPropertyRequest\LookingForPropertyRequestDeleteType;
use App\Form\Type\CompanyLocationManagement\LookingForPropertyRequest\LookingForPropertyRequestType;
use App\SessionStorage\SessionStorageService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\UnitOfWork;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Security("is_granted('ROLE_USER') or is_granted('ROLE_LOOKING_FOR_PROPERTY_REQUEST_FEEDER') or is_granted('ROLE_PROPERTY_SEEKER') or is_granted('ROLE_VIEWER') or is_granted('ROLE_RESTRICTED_VIEWER')")]
#[Route('/ansiedlung/gesuche', name: 'company_location_management_looking_for_property_request_')]
class LookingForPropertyRequestController extends AbstractLeAnController
{
    private const ACTIVE_NAV_GROUP = 'company_location_management';
    private const ACTIVE_NAV_ITEM = self::ACTIVE_NAV_GROUP . '_looking_for_property_request';

    public function __construct(
        SessionStorageService $sessionStorageService,
        SessionStoredUrlParameterService $sessionStoredUrlParameterService,
        private readonly LookingForPropertyRequestService $lookingForPropertyRequestService,
        private readonly LookingForPropertyRequestDeletionService $lookingForPropertyRequestDeletionService,
        private readonly LookingForPropertyRequestSecurityService $lookingForPropertyRequestSecurityService,
        private readonly ClassificationService $classificationService,
        private readonly LocationService $locationService,
        private readonly EntityManagerInterface $entityManager
    ) {
        parent::__construct(
            sessionStorageService: $sessionStorageService,
            sessionStoredUrlParameterService: $sessionStoredUrlParameterService
        );
    }

    #[Security("is_granted('ROLE_USER') or is_granted('ROLE_LOOKING_FOR_PROPERTY_REQUEST_FEEDER') or is_granted('ROLE_PROPERTY_SEEKER')")]
    #[Route('/erfassen', name: 'create', methods: ['GET', 'POST'])]
    public function create(Request $request, UserInterface $user): Response
    {
        if ($this->lookingForPropertyRequestSecurityService->canCreateLookingForPropertyRequest() === false) {
            throw new AccessDeniedHttpException();
        }

        $account = $user->getAccount();

        $lookingForPropertyRequest = new LookingForPropertyRequest();

        $lookingForPropertyRequest->setPropertyRequirement(new PropertyRequirement());

        $places = $this->locationService->fetchRecursivePlacesByPlacesAndPlaceRelationTypeAndPlaceType(
            places: [$account->getAssignedPlace()],
            placeTypes: $this->locationService->getCommunePlaceTypes(),
            placeRelationType: PlaceRelationType::CHILD
        );

        if ($user->getAccountUserType() === AccountUserType::PROPERTY_SEEKER) {
            $lookingForPropertyRequest->setPerson($user->getPerson());
        }

        $lookingForPropertyRequestForm = $this->createForm(type: LookingForPropertyRequestType::class, data: $lookingForPropertyRequest, options: [
            'places'          => $places,
            'accountUserType' => $user->getAccountUserType(),
            'account'         => $account,
            'selectedPerson'  => null,
            'canEdit'         => true,
            'canViewPerson'   => true,
        ]);

        $propertyRequirement = $lookingForPropertyRequest->getPropertyRequirement();

        if (count($places) === 1) {
            $propertyRequirement->setInPlaces($places);
        }

        $lookingForPropertyRequestForm->add(child: 'save', type: SubmitType::class);

        $lookingForPropertyRequestForm->handleRequest(request: $request);

        if ($lookingForPropertyRequestForm->isSubmitted() && $lookingForPropertyRequestForm->isValid()) {
            $industryClassificationForm = $lookingForPropertyRequestForm->get('industryClassification');

            $industryClassificationId = (int)$industryClassificationForm->get('industryClassificationId')->getData();

            $industryClassification = $this->classificationService->fetchIndustryClassificationById(id: $industryClassificationId);

            $unitOfWork = $this->entityManager->getUnitOfWork();

            if ($unitOfWork->getEntityState($lookingForPropertyRequest->getPerson()) === UnitOfWork::STATE_NEW) {
                $lookingForPropertyRequest->getPerson()
                    ->setAccount($account)
                    ->setCreatedByAccountUser($user)
                    ->setDeleted(false)
                    ->setAnonymized(false);

                $this->entityManager->persist(entity: $lookingForPropertyRequest->getPerson());
            }

            $lookingForPropertyRequest
                ->setAccount($account)
                ->setCreatedByAccountUser($user)
                ->setDeleted(false)
                ->getPropertyRequirement()->setIndustryClassification(industryClassification: $industryClassification);

            $this->entityManager->beginTransaction();

            $this->lookingForPropertyRequestService->persistLookingForPropertyRequest(lookingForPropertyRequest: $lookingForPropertyRequest);

            $this->entityManager->flush();
            $this->entityManager->commit();

            $this->addFlash(type: 'dataSaved', message: 'Das Gesuch wurde angelegt.');

            return $this->redirectToRoute(route: 'company_location_management_looking_for_property_request_overview');
        }

        return $this->render(view: 'company_location_management/looking_for_property_request/create.html.twig', parameters: [
            'lookingForPropertyRequestForm' => $lookingForPropertyRequestForm,
            'pageTitle'                     => 'Ansiedlungsmanagement - Ansiedlungsgesuch erfassen',
            'activeNavGroup'                => self::ACTIVE_NAV_GROUP,
            'activeNavItem'                 => self::ACTIVE_NAV_ITEM,
        ]);
    }

    #[Route('/{id<\d{1,10}>}', name: 'edit', methods: ['GET', 'POST'])]
    public function edit(int $id, Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        if ($user->getAccountUserType() === AccountUserType::INTERNAL) {
            $lookingForPropertyRequest = $this->lookingForPropertyRequestService->fetchLookingForPropertyRequestById(
                account: $account,
                withFromSubAccounts: false,
                id: $id
            );
        } else {
            if ($user->getPerson() === null) {
                throw new NotFoundHttpException();
            }

            $lookingForPropertyRequest = $this->lookingForPropertyRequestService->fetchLookingForPropertyRequestByPersonAndId(
                account: $account,
                withFromSubAccounts: false,
                person: $user->getPerson(),
                id: $id
            );
        }

        if ($lookingForPropertyRequest === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'view', subject: $lookingForPropertyRequest);

        $places = $this->locationService->fetchRecursivePlacesByPlacesAndPlaceRelationTypeAndPlaceType(
            places: [$account->getAssignedPlace()],
            placeTypes: $this->locationService->getCommunePlaceTypes(),
            placeRelationType: PlaceRelationType::CHILD
        );

        $lookingForPropertyRequestForm = $this->createForm(type: LookingForPropertyRequestType::class, data: $lookingForPropertyRequest, options: [
            'places'          => $places,
            'accountUserType' => $user->getAccountUserType(),
            'account'         => $account,
            'selectedPerson'  => $lookingForPropertyRequest->getPerson(),
            'canEdit'         => $this->isGranted(attribute: 'edit', subject: $lookingForPropertyRequest),
            'canViewPerson'   => $this->isGranted(attribute: 'view', subject: $lookingForPropertyRequest->getPerson()),
        ]);

        $industryClassificationForm = $lookingForPropertyRequestForm->get('industryClassification');

        $industryClassificationForm->get('industryClassificationId')->setData($lookingForPropertyRequest->getPropertyRequirement()->getIndustryClassification()->getId());

        $lookingForPropertyRequestForm->add(child: 'save', type: SubmitType::class);

        $lookingForPropertyRequestForm->handleRequest(request: $request);

        if ($lookingForPropertyRequestForm->isSubmitted() && $lookingForPropertyRequestForm->isValid()) {
            $this->denyAccessUnlessGranted(attribute: 'edit', subject: $lookingForPropertyRequest);

            $unitOfWork = $this->entityManager->getUnitOfWork();

            $industryClassificationId = (int) $industryClassificationForm->get('industryClassificationId')->getData();

            $industryClassification = $this->classificationService->fetchIndustryClassificationById(id: $industryClassificationId);

            $lookingForPropertyRequest->getPropertyRequirement()->setIndustryClassification(industryClassification: $industryClassification);

            if ($unitOfWork->getEntityState($lookingForPropertyRequest->getPerson()) === UnitOfWork::STATE_NEW) {
                $lookingForPropertyRequest->getPerson()
                    ->setAccount($account)
                    ->setCreatedByAccountUser($user)
                    ->setDeleted(false)
                    ->setAnonymized(false);

                $this->entityManager->persist(entity: $lookingForPropertyRequest->getPerson());
            }

            $this->entityManager->flush();

            $this->addFlash(type: 'dataSaved', message: 'Das Gesuch wurde bearbeitet.');

            return $this->redirectToRoute(route: 'company_location_management_looking_for_property_request_overview');
        }

        return $this->render(view: 'company_location_management/looking_for_property_request/edit.html.twig', parameters: [
            'lookingForPropertyRequestForm' => $lookingForPropertyRequestForm,
            'lookingForPropertyRequest'     => $lookingForPropertyRequest,
            'pageTitle'                     => 'Ansiedlungsmanagement - Gesuch bearbeiten',
            'activeNavGroup'                => self::ACTIVE_NAV_GROUP,
            'activeNavItem'                 => self::ACTIVE_NAV_ITEM,
        ]);
    }

    #[Security("is_granted('ROLE_USER') or is_granted('ROLE_LOOKING_FOR_PROPERTY_REQUEST_FEEDER') or is_granted('ROLE_PROPERTY_SEEKER')")]
    #[Route('/{id<\d{1,10}>}/aus-dem-archiv-holen', name: 'unarchive', methods: ['GET'])]
    public function unarchive(int $id, UserInterface $user): Response
    {
        $account = $user->getAccount();

        if ($user->getAccountUserType() === AccountUserType::INTERNAL) {
            $lookingForPropertyRequest = $this->lookingForPropertyRequestService->fetchLookingForPropertyRequestById(
                account: $account,
                withFromSubAccounts: false,
                id: $id
            );
        } else {
            if ($user->getPerson() === null) {
                throw new NotFoundHttpException();
            }

            $lookingForPropertyRequest = $this->lookingForPropertyRequestService->fetchLookingForPropertyRequestByPersonAndId(
                account: $account,
                withFromSubAccounts: false,
                person: $user->getPerson(),
                id: $id
            );
        }

        if ($lookingForPropertyRequest === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'edit', subject: $lookingForPropertyRequest);

        $lookingForPropertyRequest
            ->setArchived(false)
            ->setArchivedAt(null);

        $this->entityManager->flush();

        $this->addFlash(type: 'dataSaved', message: 'Das Gesuch wurde aus dem Archiv zurück geholt.');

        return $this->redirectToRoute(route: 'company_location_management_looking_for_property_request_overview_archive');
    }

    #[Security("is_granted('ROLE_USER') or is_granted('ROLE_LOOKING_FOR_PROPERTY_REQUEST_FEEDER') or is_granted('ROLE_PROPERTY_SEEKER')")]
    #[Route('/{id<\d{1,10}>}/archivieren', name: 'archive', methods: ['GET'])]
    public function archive(int $id, UserInterface $user): Response
    {
        $account = $user->getAccount();

        if ($user->getAccountUserType() === AccountUserType::INTERNAL) {
            $lookingForPropertyRequest = $this->lookingForPropertyRequestService->fetchLookingForPropertyRequestById(
                account: $account,
                withFromSubAccounts: false,
                id: $id
            );
        } else {
            if ($user->getPerson() === null) {
                throw new NotFoundHttpException();
            }

            $lookingForPropertyRequest = $this->lookingForPropertyRequestService->fetchLookingForPropertyRequestByPersonAndId(
                account: $account,
                withFromSubAccounts: false,
                person: $user->getPerson(),
                id: $id
            );
        }

        if ($lookingForPropertyRequest === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'edit', subject: $lookingForPropertyRequest);

        $lookingForPropertyRequest
            ->setArchived(true)
            ->setArchivedAt(new \DateTimeImmutable());

        $this->entityManager->flush();

        $this->addFlash(type: 'dataSaved', message: 'Das Gesuch wurde archiviert.');

        return $this->redirectToRoute(route: 'company_location_management_looking_for_property_request_overview_archive');
    }

    #[Security("is_granted('ROLE_USER') or is_granted('ROLE_LOOKING_FOR_PROPERTY_REQUEST_FEEDER') or is_granted('ROLE_PROPERTY_SEEKER')")]
    #[Route('/{id<\d{1,10}>}/loeschen', name: 'delete', methods: ['GET', 'POST'])]
    public function delete(int $id, Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        if ($user->getAccountUserType() === AccountUserType::INTERNAL) {
            $lookingForPropertyRequest = $this->lookingForPropertyRequestService->fetchLookingForPropertyRequestById(
                account: $account,
                withFromSubAccounts: false,
                id: $id
            );
        } else {
            if ($user->getPerson() === null) {
                throw new NotFoundHttpException();
            }

            $lookingForPropertyRequest = $this->lookingForPropertyRequestService->fetchLookingForPropertyRequestByPersonAndId(
                account: $account,
                withFromSubAccounts: false,
                person: $user->getPerson(),
                id: $id
            );
        }

        if ($lookingForPropertyRequest === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'delete', subject: $lookingForPropertyRequest);

        $places = $this->locationService->fetchRecursivePlacesByPlacesAndPlaceRelationTypeAndPlaceType(
            places: [$account->getAssignedPlace()],
            placeTypes: $this->locationService->getCommunePlaceTypes(),
            placeRelationType: PlaceRelationType::CHILD
        );

        $lookingForPropertyRequestDeleteForm = $this->createForm(type: LookingForPropertyRequestDeleteType::class);

        $lookingForPropertyRequestDeleteForm->add(child:'delete', type: SubmitType::class);

        $lookingForPropertyRequestDeleteForm->handleRequest(request: $request);

        if (
            $lookingForPropertyRequestDeleteForm->isSubmitted()
            && $lookingForPropertyRequestDeleteForm->isValid()
            && $lookingForPropertyRequestDeleteForm->get('verificationCode')->getData() === 'gesuch_' . $lookingForPropertyRequest->getId()
        ) {
            $this->lookingForPropertyRequestDeletionService->deleteLookingForPropertyRequest(
                lookingForPropertyRequest: $lookingForPropertyRequest,
                deletionType: DeletionType::SOFT,
                deletedByAccountUser: $user
            );

            $this->addFlash(type: 'dataDeleted', message: 'Das Gesuch wurde gelöscht.');

            $lastVisitedPage = $this->fetchObjectFromSession(dataClassName: LastVisitedPage::class, sessionKeyName: 'lookingForPropertyRequestOverview');

            if ($lastVisitedPage !== null) {
                return $this->redirectToRoute(route: $lastVisitedPage->getRouteName(), parameters: $lastVisitedPage->getParameters());
            } else {
                return $this->redirectToRoute(route: 'company_location_management_looking_for_property_request_overview');
            }
        }

        $lookingForPropertyRequestForm = $this->createForm(type: LookingForPropertyRequestType::class, data: $lookingForPropertyRequest, options: [
            'places'          => $places,
            'accountUserType' => $user->getAccountUserType(),
            'account'         => $account,
            'selectedPerson'  => $lookingForPropertyRequest->getPerson(),
            'canEdit'         => false,
            'canViewPerson'   => $this->isGranted(attribute: 'view', subject: $lookingForPropertyRequest->getPerson()),
        ]);

        $industryClassificationForm = $lookingForPropertyRequestForm->get('industryClassification');

        $industryClassificationForm->get('industryClassificationId')->setData($lookingForPropertyRequest->getPropertyRequirement()->getIndustryClassification()->getId());

        return $this->render(view: 'company_location_management/looking_for_property_request/delete.html.twig', parameters: [
            'lookingForPropertyRequestDeleteForm' => $lookingForPropertyRequestDeleteForm,
            'lookingForPropertyRequestForm'       => $lookingForPropertyRequestForm,
            'lookingForPropertyRequest'           => $lookingForPropertyRequest,
            'pageTitle'                           => 'Ansiedlungsgesuch löschen',
            'activeNavGroup'                      => self::ACTIVE_NAV_GROUP,
            'activeNavItem'                       => self::ACTIVE_NAV_ITEM,
        ]);
    }

    #[Security("is_granted('ROLE_USER') or is_granted('ROLE_LOOKING_FOR_PROPERTY_REQUEST_FEEDER')")]
    #[Route('/{id<\d{1,10}>}/sachbearbeiter-zuweisen', name: 'assign_to_account_user', methods: ['GET', 'POST'])]
    public function assignToAccountUser(int $id, Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $lookingForPropertyRequest = $this->lookingForPropertyRequestService->fetchLookingForPropertyRequestById(
            account: $account,
            withFromSubAccounts: false,
            id: $id
        );

        if ($lookingForPropertyRequest === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'assignToAccountUser', subject: $lookingForPropertyRequest);

        $places = $this->locationService->fetchRecursivePlacesByPlacesAndPlaceRelationTypeAndPlaceType(
            places: [$account->getAssignedPlace()],
            placeTypes: $this->locationService->getCommunePlaceTypes(),
            placeRelationType: PlaceRelationType::CHILD
        );

        $lookingForPropertyRequestAssignToAccountUserForm = $this->createForm(
            type: LookingForPropertyRequestAssignToAccountUserType::class,
            data: $lookingForPropertyRequest,
            options: ['account' => $account]
        );

        $lookingForPropertyRequestAssignToAccountUserForm->add(child:'save', type: SubmitType::class);

        $lookingForPropertyRequestAssignToAccountUserForm->handleRequest(request: $request);

        if ($lookingForPropertyRequestAssignToAccountUserForm->isSubmitted() && $lookingForPropertyRequestAssignToAccountUserForm->isValid()) {
            $this->entityManager->flush();

            if ($lookingForPropertyRequestAssignToAccountUserForm->getData()->getAssignedToAccountUser() === null) {
                $this->addFlash(type: 'dataDeleted', message: 'Es wurde kein/e Sachbearbeiter:in zugewiesen.');
            } else {
                $this->addFlash(type: 'dataSaved', message: 'Sachbearbeiter:in zugewiesen.');
            }

            return $this->redirectToRoute(route: 'company_location_management_looking_for_property_request_edit', parameters: ['id' => $lookingForPropertyRequest->getId()]);
        }

        $lookingForPropertyRequestForm = $this->createForm(type: LookingForPropertyRequestType::class, data: $lookingForPropertyRequest, options: [
            'places'          => $places,
            'accountUserType' => $user->getAccountUserType(),
            'account'         => $account,
            'selectedPerson'  => $lookingForPropertyRequest->getPerson(),
            'canEdit'         => false,
            'canViewPerson'   => $this->isGranted(attribute: 'view', subject: $lookingForPropertyRequest->getPerson()),
        ]);

        $industryClassificationForm = $lookingForPropertyRequestForm->get('industryClassification');

        $industryClassificationForm->get('industryClassificationId')->setData($lookingForPropertyRequest->getPropertyRequirement()->getIndustryClassification()->getId());

        return $this->render(view: 'company_location_management/looking_for_property_request/assign_to_account_user.html.twig', parameters: [
            'lookingForPropertyRequestAssignToAccountUserForm' => $lookingForPropertyRequestAssignToAccountUserForm,
            'lookingForPropertyRequestForm'                    => $lookingForPropertyRequestForm,
            'places'                                           => $places,
            'lookingForPropertyRequest'                        => $lookingForPropertyRequest,
            'pageTitle'                                        => 'Ansiedlung - Gesuche - Zuweisung Sachbearbeiter:in',
            'activeNavGroup'                                   => self::ACTIVE_NAV_GROUP,
            'activeNavItem'                                    => self::ACTIVE_NAV_ITEM,
        ]);
    }
}
