<?php

declare(strict_types=1);

namespace App\Controller\CompanyLocationManagement\Matching;

use App\Domain\SettlementConcept\SettlementConceptMatchingService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Security("is_granted('ROLE_USER') or is_granted('ROLE_VIEWER') or is_granted('ROLE_RESTRICTED_VIEWER')")]
#[Route('/ansiedlung/matching', name: 'company_location_management_matching_')]
class MatchingResultController extends AbstractController
{
    private const ACTIVE_NAV_GROUP = 'company_location_management';
    private const ACTIVE_NAV_ITEM = self::ACTIVE_NAV_GROUP . '_matching';

    public function __construct(
        private readonly SettlementConceptMatchingService $settlementConceptMatchingService,
    ) {
    }

    #[Route('/ansiedlungskonzepte/matching-ergebnisse/{matchingResultId<\d{1,10}>}', name: 'settlement_concept_matching_result', methods: ['GET'])]
    public function settlementConceptMatchingResult(int $matchingResultId, UserInterface $user): Response
    {
        $settlementConceptMatchingResult = $this->settlementConceptMatchingService->fetchSettlementConceptMatchingResultById(
            account: $user->getAccount(),
            id: $matchingResultId
        );

        if ($settlementConceptMatchingResult === null) {
            throw new NotFoundHttpException();
        }

        return $this->render(view: 'company_location_management/matching/settlement_concept_matching_result.html.twig', parameters: [
            'settlementConceptMatchingResult' => $settlementConceptMatchingResult,
            'pageTitle'                       => 'Ansiedlungsmanagement – Ansiedlungskonzept',
            'activeNavGroup'                  => self::ACTIVE_NAV_GROUP,
            'activeNavItem'                   => self::ACTIVE_NAV_ITEM,
        ]);
    }
}
