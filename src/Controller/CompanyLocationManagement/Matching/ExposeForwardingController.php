<?php

declare(strict_types=1);

namespace App\Controller\CompanyLocationManagement\Matching;

use App\Controller\AbstractLeAnController;
use App\Domain\Entity\ForwardingResponse;
use App\Domain\Entity\LastVisitedPage;
use App\Domain\Entity\LookingForPropertyRequest\LookingForPropertyRequestExposeForwarding;
use App\Domain\Entity\Person\Occurrence;
use App\Domain\Entity\Person\OccurrenceType;
use App\Domain\Entity\SettlementConcept\SettlementConceptExposeForwarding;
use App\Domain\File\FileService;
use App\Domain\LookingForPropertyRequest\LookingForPropertyRequestService;
use App\Domain\LookingForPropertyRequest\Matching\MatchingService;
use App\Domain\Property\PdfExpose\PdfExposeService;
use App\Domain\SessionStoredUrlParameter\SessionStoredUrlParameterService;
use App\Domain\SettlementConcept\SettlementConceptMatchingService;
use App\Domain\SettlementConcept\SettlementConceptService;
use App\Form\Type\CompanyLocationManagement\LookingForPropertyRequest\ExposeForwardingRejectType;
use App\Form\Type\CompanyLocationManagement\Matching\LookingForPropertyRequestExposeForwardingType;
use App\Form\Type\CompanyLocationManagement\Matching\SettlementConceptExposeForwardingRejectType;
use App\Form\Type\CompanyLocationManagement\Matching\SettlementConceptExposeForwardingType;
use App\SessionStorage\SessionStorageService;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Part\DataPart;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Security("is_granted('ROLE_USER')")]
#[Route('/ansiedlung/matching', name: 'company_location_management_matching_')]
class ExposeForwardingController extends AbstractLeAnController
{
    private const ACTIVE_NAV_GROUP = 'company_location_management';
    private const ACTIVE_NAV_ITEM = self::ACTIVE_NAV_GROUP . '_matching';

    public function __construct(
        SessionStorageService $sessionStorageService,
        SessionStoredUrlParameterService $sessionStoredUrlParameterService,
        private readonly string $noReplyEmailAddress,
        private readonly FileService $fileService,
        private readonly MatchingService $matchingService,
        private readonly SettlementConceptMatchingService $settlementConceptMatchingService,
        private readonly SettlementConceptService $settlementConceptService,
        private readonly PdfExposeService $pdfExposeService,
        private readonly LookingForPropertyRequestService $lookingForPropertyRequestService,
        private readonly EntityManagerInterface $entityManager,
        private readonly MailerInterface $mailer
    ) {
        parent::__construct(
            sessionStorageService: $sessionStorageService,
            sessionStoredUrlParameterService: $sessionStoredUrlParameterService
        );
    }

    #[Security("is_granted('ROLE_USER')")]
    #[Route('/ansiedlungsgesuche/matching-ergebnisse/{matchingResultId<\d{1,10}>}/weiterleiten', name: 'object_expose_forwarding_for_looking_for_property_request', methods: ['GET', 'POST'])]
    public function objectExposeForwardingForLookingForPropertyRequest(int $matchingResultId, Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $matchingResult = $this->matchingService->fetchMatchingResultById(account: $account, id: $matchingResultId);

        if ($matchingResult === null) {
            throw new NotFoundHttpException();
        }

        $lookingForPropertyRequest = $matchingResult->getLookingForPropertyRequest();
        $buildingUnit = $matchingResult->getBuildingUnit();

        $lookingForPropertyRequestExposeForwarding = $this->lookingForPropertyRequestService->fetchLookingForPropertyRequestExposeForwardingByLookingForPropertyRequestAndBuildingUnit(
            account:$account,
            lookingForPropertyRequest: $lookingForPropertyRequest,
            buildingUnit: $buildingUnit
        );

        if ($lookingForPropertyRequestExposeForwarding !== null) {
            return $this->redirectToRoute(route: 'company_location_management_matching_object_detail', parameters: ['id' => $buildingUnit->getId()]);
        }

        if ($lookingForPropertyRequest->getPerson() === null || $lookingForPropertyRequest->getPerson()->getAccountUser() === null) {
            throw new NotFoundHttpException();
        }

        $lookingForPropertyRequestExposeForwardingForm = $this->createForm(type: LookingForPropertyRequestExposeForwardingType::class);

        $lookingForPropertyRequestExposeForwardingForm->add(child:'submit', type: SubmitType::class);

        $lookingForPropertyRequestExposeForwardingForm->handleRequest(request: $request);

        if ($lookingForPropertyRequestExposeForwardingForm->isSubmitted() && $lookingForPropertyRequestExposeForwardingForm->isValid()) {
            $lookingForPropertyRequestExposeForwarding = new LookingForPropertyRequestExposeForwarding();

            $lookingForPropertyRequestExposeForwarding
                ->setBuildingUnit($buildingUnit)
                ->setLookingForPropertyRequest($lookingForPropertyRequest)
                ->setCreatedByAccountUser($user);

            $occurrence = new Occurrence();

            $occurrence
                ->setPerson($lookingForPropertyRequest->getPerson())
                ->setPerformedWithAccountUser($user)
                ->setAccount($user->getAccount())
                ->setCreatedByAccountUser($user)
                ->setOccurrenceType(occurrenceType: OccurrenceType::MEMO)
                ->setOccurrenceAt(new \DateTime())
                ->setNote(note:'Es wurde über LeAn® ein Kurzexposé zum Objekt ' . $buildingUnit->getId() . ', ' .$buildingUnit->getName() . ' weitergeleitet.')
                ->setFollowUp(followUp: false)
                ->setFollowUpDone(followUpDone: false)
                ->setDeleted(deleted: false)
                ->setAnonymized(anonymized: false);

            $this->entityManager->persist(entity: $lookingForPropertyRequestExposeForwarding);
            $this->entityManager->persist(entity: $occurrence);

            $this->entityManager->flush();

            $email = (new TemplatedEmail())
                ->from(addresses: $this->noReplyEmailAddress)
                ->to(
                    new Address(
                        address: $lookingForPropertyRequest->getPerson()->getAccountUser()->getEmail(),
                        name: $lookingForPropertyRequest->getPerson()->getAccountUser()->getFullName()
                    )
                )
                ->subject(subject: 'Ein neues Objektexposé wurde für Sie freigegeben')
                ->htmlTemplate(template: 'company_location_management/email/matching/expose_forwarded.html.twig')
                ->textTemplate(template: 'company_location_management/email/matching/expose_forwarded.txt.twig')
                ->context(context: [
                    'person'                    => $lookingForPropertyRequest->getPerson(),
                    'lookingForPropertyRequest' => $lookingForPropertyRequest,
                    'account'                   => $account,
                ]);

            $this->mailer->send(message: $email);

            $this->addFlash(type: 'dataSaved', message: 'Das Exposé wurde weitergeleitet.');

            $matchingDetailReturnRoute = $this->fetchObjectFromSession(dataClassName: LastVisitedPage::class , sessionKeyName: 'matchingDetailReturnRoute');

            if ($matchingDetailReturnRoute !== null) {
                return $this->redirectToRoute(route: $matchingDetailReturnRoute->getRouteName(), parameters: $matchingDetailReturnRoute->getParameters());
            } else {
                return $this->redirectToRoute(route: 'company_location_management_matching_object_detail', parameters: ['id' => $buildingUnit->getId()]);
            }
        }

        return $this->render(view: 'company_location_management/matching/looking_for_property_request_expose_forwarding.html.twig', parameters: [
            'buildingUnit'                                  => $buildingUnit,
            'lookingForPropertyRequest'                     => $lookingForPropertyRequest,
            'lookingForPropertyRequestExposeForwardingForm' => $lookingForPropertyRequestExposeForwardingForm,
            'pageTitle'                                     => 'Ansiedlung – Exposé weiterleiten',
            'activeNavGroup'                                => self::ACTIVE_NAV_GROUP,
            'activeNavItem'                                 => self::ACTIVE_NAV_ITEM,
        ]);
    }

    #[Security("is_granted('ROLE_USER')")]
    #[Route('/ansiedlungsgesuche/matching-ergebnisse/{matchingResultId<\d{1,10}>}/als-weitergeleitet-markieren', name: 'object_mark_looking_for_property_request_as_forwarded', methods: ['GET'])]
    public function objectMarkLookingForPropertyRequestAsForwarded(int $matchingResultId, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $matchingResult = $this->matchingService->fetchMatchingResultById(account: $account, id: $matchingResultId);

        if ($matchingResult === null) {
            throw new NotFoundHttpException();
        }

        $lookingForPropertyRequest = $matchingResult->getLookingForPropertyRequest();
        $buildingUnit = $matchingResult->getBuildingUnit();

        $lookingForPropertyRequestExposeForwarding = $this->lookingForPropertyRequestService->fetchLookingForPropertyRequestExposeForwardingByLookingForPropertyRequestAndBuildingUnit(
            account: $account,
            lookingForPropertyRequest: $lookingForPropertyRequest,
            buildingUnit: $buildingUnit
        );

        if ($lookingForPropertyRequestExposeForwarding !== null) {
            return $this->redirectToRoute(route: 'company_location_management_matching_object_detail', parameters: ['id' => $buildingUnit->getId()]);
        }

        $lookingForPropertyRequestExposeForwarding = new LookingForPropertyRequestExposeForwarding();

        $lookingForPropertyRequestExposeForwarding
            ->setCreatedByAccountUser($user)
            ->setBuildingUnit($buildingUnit)
            ->setLookingForPropertyRequest($lookingForPropertyRequest);

        $occurrence = new Occurrence();

        $occurrence
            ->setPerson($lookingForPropertyRequest->getPerson())
            ->setPerformedWithAccountUser($user)
            ->setAccount($user->getAccount())
            ->setCreatedByAccountUser($user)
            ->setOccurrenceType(occurrenceType: OccurrenceType::MEMO)
            ->setOccurrenceAt(new \DateTime())
            ->setNote(note:'Es wurde manuell ein Kurzexposé zum Objekt ' . $buildingUnit->getId() . ', ' .$buildingUnit->getName() . ' weitergeleitet.')
            ->setFollowUp(followUp: false)
            ->setFollowUpDone(followUpDone: false)
            ->setDeleted(deleted: false)
            ->setAnonymized(anonymized: false);

        $this->entityManager->persist(entity: $lookingForPropertyRequestExposeForwarding);
        $this->entityManager->persist(entity: $occurrence);

        $this->entityManager->flush();

        $this->addFlash(type: 'dataSaved', message: 'Es wurde eingetragen, dass das Exposé weitergeleitet wurde.');

        $matchingDetailReturnRoute = $this->fetchObjectFromSession(dataClassName: LastVisitedPage::class , sessionKeyName: 'matchingDetailReturnRoute');

        if ($matchingDetailReturnRoute !== null) {
            return $this->redirectToRoute(route: $matchingDetailReturnRoute->getRouteName(), parameters: $matchingDetailReturnRoute->getParameters());
        } else {
            return $this->redirectToRoute(route: 'company_location_management_matching_object_detail', parameters: ['id' => $buildingUnit->getId()]);
        }
    }

    #[Security("is_granted('ROLE_USER')")]
    #[Route('/ansiedlungsgesuche/matching-ergebnisse/{matchingResultId<\d{1,10}>}/status-setzen/{forwardingResponse<\d{1}>}', name: 'expose_forwarding_set_forwarding_response', methods: ['GET'])]
    public function exposeForwardingSetForwardingResponse(int $matchingResultId, ForwardingResponse $forwardingResponse, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $matchingResult = $this->matchingService->fetchMatchingResultById(account: $account, id: $matchingResultId);

        if ($matchingResult === null) {
            throw new NotFoundHttpException();
        }

        $lookingForPropertyRequest = $matchingResult->getLookingForPropertyRequest();
        $buildingUnit = $matchingResult->getBuildingUnit();

        $lookingForPropertyRequestExposeForwarding = $this->lookingForPropertyRequestService->fetchLookingForPropertyRequestExposeForwardingByLookingForPropertyRequestAndBuildingUnit(
            account: $account,
            lookingForPropertyRequest: $lookingForPropertyRequest,
            buildingUnit: $buildingUnit
        );

        if ($lookingForPropertyRequestExposeForwarding === null) {
            throw new NotFoundHttpException();
        }

        $lookingForPropertyRequestExposeForwarding->setForwardingResponse($forwardingResponse);

        $this->entityManager->persist(entity: $lookingForPropertyRequestExposeForwarding);
        $this->entityManager->flush();

        $this->addFlash(type: 'dataSaved', message: 'Der Status wurde geändert.');

        $matchingDetailReturnRoute = $this->fetchObjectFromSession(dataClassName: LastVisitedPage::class , sessionKeyName: 'matchingDetailReturnRoute');

        if ($matchingDetailReturnRoute !== null) {
            return $this->redirectToRoute(route: $matchingDetailReturnRoute->getRouteName(), parameters: $matchingDetailReturnRoute->getParameters());
        } else {
            return $this->redirectToRoute(route: 'company_location_management_matching_object_detail', parameters: ['id' => $buildingUnit->getId()]);
        }
    }

    #[Security("is_granted('ROLE_USER')")]
    #[Route('/ansiedlungsgesuche/matching-ergebnisse/{matchingResultId<\d{1,10}>}/auf-abgelehnt-setzen', name: 'expose_forwarding_reject', methods: ['GET', 'POST'])]
    public function exposeForwardingReject(int $matchingResultId, Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $matchingResult = $this->matchingService->fetchMatchingResultById(account: $account, id: $matchingResultId);

        if ($matchingResult === null) {
            throw new NotFoundHttpException();
        }

        $lookingForPropertyRequest = $matchingResult->getLookingForPropertyRequest();
        $buildingUnit = $matchingResult->getBuildingUnit();

        $lookingForPropertyRequestExposeForwarding = $this->lookingForPropertyRequestService->fetchLookingForPropertyRequestExposeForwardingByLookingForPropertyRequestAndBuildingUnit(
            account: $account,
            lookingForPropertyRequest: $lookingForPropertyRequest,
            buildingUnit: $buildingUnit
        );

        if ($lookingForPropertyRequestExposeForwarding === null) {
            throw new NotFoundHttpException();
        }

        $exposeForwardingRejectForm = $this->createForm(type: ExposeForwardingRejectType::class, data: $lookingForPropertyRequestExposeForwarding);

        $exposeForwardingRejectForm->add(child: 'save', type: SubmitType::class);

        $exposeForwardingRejectForm->handleRequest(request: $request);

        if ($exposeForwardingRejectForm->isSubmitted() && $exposeForwardingRejectForm->isValid()) {
            $lookingForPropertyRequestExposeForwarding->setForwardingResponse(forwardingResponse: ForwardingResponse::REJECTED);

            $this->entityManager->persist(entity: $lookingForPropertyRequestExposeForwarding);
            $this->entityManager->flush();

            $this->addFlash(type: 'dataSaved', message: 'Der Status wurde auf abgelehnt gesetzt.');

            $matchingDetailReturnRoute = $this->fetchObjectFromSession(dataClassName: LastVisitedPage::class , sessionKeyName: 'matchingDetailReturnRoute');

            if ($matchingDetailReturnRoute !== null) {
                return $this->redirectToRoute(route: $matchingDetailReturnRoute->getRouteName(), parameters: $matchingDetailReturnRoute->getParameters());
            } else {
                return $this->redirectToRoute(route: 'company_location_management_matching_object_detail', parameters: ['id' => $buildingUnit->getId()]);
            }
        }

        return $this->render(view: 'company_location_management/matching/forwarding_reject.html.twig', parameters: [
            'exposeForwardingRejectForm' => $exposeForwardingRejectForm,
            'lookingForPropertyRequest'  => $lookingForPropertyRequest,
            'buildingUnit'               => $buildingUnit,
            'activeNavGroup'             => self::ACTIVE_NAV_GROUP,
            'activeNavItem'              => self::ACTIVE_NAV_ITEM,
        ]);
    }

    #[Security("is_granted('ROLE_USER')")]
    #[Route('/ansiedlungskonzepte/matching-ergebnisse/{matchingResultId<\d{1,10}>}/weiterleiten', name: 'object_expose_forwarding_for_settlement_concept', methods: ['GET', 'POST'])]
    public function objectExposeForwardingForSettlementConcept(int $matchingResultId, Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $settlementConceptMatchingResult = $this->settlementConceptMatchingService->fetchSettlementConceptMatchingResultById(
            account: $account,
            id: $matchingResultId
        );

        if ($settlementConceptMatchingResult === null) {
            throw new NotFoundHttpException();
        }

        $buildingUnit = $settlementConceptMatchingResult->getBuildingUnit();
        $settlementConcept = $settlementConceptMatchingResult->getSettlementConcept();

        $settlementConceptExposeForwardingForm = $this->createForm(type: SettlementConceptExposeForwardingType::class);

        $settlementConceptExposeForwardingForm->add(child:'submit', type: SubmitType::class);

        $settlementConceptExposeForwardingForm->handleRequest(request: $request);

        if ($settlementConceptExposeForwardingForm->isSubmitted() && $settlementConceptExposeForwardingForm->isValid()) {
            $settlementConceptExposeForwarding = $this->settlementConceptService->fetchSettlementConceptExposeForwardingBySettlementConceptAndBuildingUnit(
                account: $account,
                settlementConcept: $settlementConcept,
                buildingUnit: $buildingUnit
            );

            if ($settlementConceptExposeForwarding !== null) {
                return $this->redirectToRoute(route: 'company_location_management_matching_object_detail', parameters: ['id' => $buildingUnit->getId()]);
            }

            $settlementConceptExposeForwarding = new SettlementConceptExposeForwarding();

            $settlementConceptExposeForwarding
                ->setCreatedByAccountUser($user)
                ->setSettlementConcept($settlementConcept)
                ->setBuildingUnit($buildingUnit);

            $this->entityManager->persist(entity: $settlementConceptExposeForwarding);
            $this->entityManager->flush();

            $pdfContent = $this->pdfExposeService->generateBuildingUnitPdfExpose(
                buildingUnit: $buildingUnit,
                accountUser: $settlementConceptExposeForwarding->getCreatedByAccountUser()
            );

            $email = (new TemplatedEmail())
                ->from(addresses: $this->noReplyEmailAddress)
                ->to(
                    new Address(
                        address: $settlementConcept->getSettlementConceptAccountUser()->getEmail(),
                        name: $settlementConcept->getSettlementConceptAccountUser()->getFullName()
                    )
                )
                ->subject(subject: 'Ein neues Objektexposé zur Weiterleitung an einen Konzeptanbietenden liegt vor')
                ->htmlTemplate(template: 'company_location_management/email/matching/settlement_concept_expose_forwarded.html.twig')
                ->textTemplate(template: 'company_location_management/email/matching/settlement_concept_expose_forwarded.txt.twig')
                ->addPart(new DataPart($pdfContent, filename: 'Kurzexpose.pdf', contentType: 'application/pdf'))
                ->context(context: [
                    'account'                           => $account,
                    'settlementConceptExposeForwarding' => $settlementConceptExposeForwarding
                ]);


            if ($account->getAccountConfiguration()->getCityExpose() !== null) {
                $cityExposePdfContent = $this->fileService->fetchFileContentFromFile($account->getAccountConfiguration()->getCityExpose()->getFile());

                $email->addPart(new DataPart($cityExposePdfContent, filename: 'stadt-expose.pdf', contentType: 'application/pdf'));
            }

            $this->mailer->send(message: $email);

            $this->addFlash(type: 'dataSaved', message: 'Das Expose wurde weitergeleitet.');

            return $this->redirectToRoute(route: 'company_location_management_matching_object_detail', parameters: ['id' => $buildingUnit->getId(), 'tab' => 'ansiedlungskonzepte']);
        }

        return $this->render(view: 'company_location_management/matching/settlement_concept_expose_forwarding.html.twig', parameters: [
            'buildingUnit'                          => $buildingUnit,
            'settlementConcept'                     => $settlementConcept,
            'settlementConceptExposeForwardingForm' => $settlementConceptExposeForwardingForm,
            'pageTitle'                             => 'Ansiedlung – Exposé weiterleiten',
            'activeNavGroup'                        => self::ACTIVE_NAV_GROUP,
            'activeNavItem'                         => self::ACTIVE_NAV_ITEM,
        ]);
    }

    #[Security("is_granted('ROLE_USER')")]
    #[Route('/ansiedlungskonzepte/matching-ergebnisse/{matchingResultId<\d{1,10}>}/status-setzen/{forwardingResponse<\d{1}>}', name: 'settlement_concept_expose_forwarding_set_forwarding_response', methods: ['GET'])]
    public function settlementConceptExposeForwardingSetForwardingResponse(int $matchingResultId, ForwardingResponse $forwardingResponse, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $settlementConceptMatchingResult = $this->settlementConceptMatchingService->fetchSettlementConceptMatchingResultById(account: $account, id: $matchingResultId);

        if ($settlementConceptMatchingResult === null) {
            throw new NotFoundHttpException();
        }

        $settlementConcept = $settlementConceptMatchingResult->getSettlementConcept();
        $buildingUnit = $settlementConceptMatchingResult->getBuildingUnit();

        $settlementConceptExposeForwarding = $this->settlementConceptService->fetchSettlementConceptExposeForwardingBySettlementConceptAndBuildingUnit(
            account: $account,
            settlementConcept: $settlementConcept,
            buildingUnit: $buildingUnit
        );

        if ($settlementConceptExposeForwarding === null) {
            throw new NotFoundHttpException();
        }

        $settlementConceptExposeForwarding->setForwardingResponse($forwardingResponse);

        $this->entityManager->persist(entity: $settlementConceptExposeForwarding);
        $this->entityManager->flush();

        $this->addFlash(type: 'dataSaved', message: 'Der Status wurde geändert.');

        return $this->redirectToRoute(route: 'company_location_management_matching_object_detail', parameters: ['id' => $buildingUnit->getId(), 'tab' => 'ansiedlungskonzepte']);
    }

    #[Security("is_granted('ROLE_USER')")]
    #[Route('/ansiedlungskonzepte/matching-ergebnisse/{matchingResultId<\d{1,10}>}/auf-abgelehnt-setzen', name: 'settlement_concept_expose_forwarding_reject', methods: ['GET', 'POST'])]
    public function settlementConceptExposeForwardingReject(int $matchingResultId, Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $settlementConceptMatchingResult = $this->settlementConceptMatchingService->fetchSettlementConceptMatchingResultById(account: $account, id: $matchingResultId);

        if ($settlementConceptMatchingResult === null) {
            throw new NotFoundHttpException();
        }

        $settlementConcept = $settlementConceptMatchingResult->getSettlementConcept();
        $buildingUnit = $settlementConceptMatchingResult->getBuildingUnit();

        $settlementConceptExposeForwarding = $this->settlementConceptService->fetchSettlementConceptExposeForwardingBySettlementConceptAndBuildingUnit(
            account: $account,
            settlementConcept: $settlementConcept,
            buildingUnit: $buildingUnit
        );

        if ($settlementConceptExposeForwarding === null) {
            throw new NotFoundHttpException();
        }

        $settlementConceptExposeForwardingRejectForm = $this->createForm(type: SettlementConceptExposeForwardingRejectType::class, data: $settlementConceptExposeForwarding);

        $settlementConceptExposeForwardingRejectForm->add(child: 'save', type: SubmitType::class);

        $settlementConceptExposeForwardingRejectForm->handleRequest(request: $request);

        if ($settlementConceptExposeForwardingRejectForm->isSubmitted() && $settlementConceptExposeForwardingRejectForm->isValid()) {
            $settlementConceptExposeForwarding->setForwardingResponse(forwardingResponse: ForwardingResponse::REJECTED);

            $this->entityManager->persist(entity: $settlementConceptExposeForwarding);
            $this->entityManager->flush();

            $this->addFlash(type: 'dataSaved', message: 'Der Status wurde auf abgelehnt gesetzt.');

            return $this->redirectToRoute(route: 'company_location_management_matching_object_detail', parameters: ['id' => $buildingUnit->getId(), 'tab' => 'ansiedlungskonzepte']);
        }

        return $this->render(view: 'company_location_management/matching/settlement_concept_forwarding_reject.html.twig', parameters: [
            'settlementConceptExposeForwardingRejectForm' => $settlementConceptExposeForwardingRejectForm,
            'settlementConcept'                           => $settlementConcept,
            'buildingUnit'                                => $buildingUnit,
            'activeNavGroup'                              => self::ACTIVE_NAV_GROUP,
            'activeNavItem'                               => self::ACTIVE_NAV_ITEM,
        ]);
    }
}
