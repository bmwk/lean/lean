<?php

declare(strict_types=1);

namespace App\Controller\CompanyLocationManagement\Matching;

use App\Controller\AbstractLeAnController;
use App\Domain\Entity\LastVisitedPage;
use App\Domain\Entity\LookingForPropertyRequest\LookingForPropertyRequestMatchingResponse;
use App\Domain\Entity\SettlementConcept\SettlementConceptMatchingResponse;
use App\Domain\LookingForPropertyRequest\LookingForPropertyRequestService;
use App\Domain\LookingForPropertyRequest\Matching\MatchingService;
use App\Domain\SessionStoredUrlParameter\SessionStoredUrlParameterService;
use App\Domain\SettlementConcept\SettlementConceptMatchingService;
use App\SessionStorage\SessionStorageService;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Security("is_granted('ROLE_USER')")]
#[Route('/ansiedlung/matching', name: 'company_location_management_matching_')]
class MatchingResponseController extends AbstractLeAnController
{
    public function __construct(
        SessionStorageService $sessionStorageService,
        SessionStoredUrlParameterService $sessionStoredUrlParameterService,
        private readonly MatchingService $matchingService,
        private readonly SettlementConceptMatchingService $settlementConceptMatchingService,
        private readonly LookingForPropertyRequestService $lookingForPropertyRequestService,
        private readonly EntityManagerInterface $entityManager
    ) {
        parent::__construct(
            sessionStorageService: $sessionStorageService,
            sessionStoredUrlParameterService: $sessionStoredUrlParameterService
        );
    }

    #[Security("is_granted('ROLE_USER')")]
    #[Route('/ansiedlungsgesuche/matching-ergebnisse/{matchingResultId<\d{1,10}>}/matching-status-setzen/{response<\d{1}>}', name: 'looking_for_property_request_matching_response', methods: ['GET'])]
    public function lookingForPropertyRequestMatchingResponse(int $matchingResultId, \App\Domain\Entity\Matching\Response $response, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $matchingResult = $this->matchingService->fetchMatchingResultById(account: $account, id: $matchingResultId);

        if ($matchingResult === null) {
            throw new NotFoundHttpException();
        }

        $lookingForPropertyRequest = $matchingResult->getLookingForPropertyRequest();
        $buildingUnit = $matchingResult->getBuildingUnit();

        $lookingForPropertyRequestMatchingResponse = $this->lookingForPropertyRequestService->fetchLookingForPropertyRequestMatchingResponseByLookingForPropertyRequestAndBuildingUnit(
            account: $account,
            lookingForPropertyRequest: $lookingForPropertyRequest,
            buildingUnit: $buildingUnit
        );

        if ($lookingForPropertyRequestMatchingResponse !== null) {
            $lookingForPropertyRequestMatchingResponse->setResponse($response);
        } else {
            $lookingForPropertyRequestMatchingResponse = new LookingForPropertyRequestMatchingResponse();

            $lookingForPropertyRequestMatchingResponse
                ->setCreatedByAccountUser($user)
                ->setBuildingUnit($buildingUnit)
                ->setLookingForPropertyRequest($lookingForPropertyRequest)
                ->setResponse($response);
        }

        $this->entityManager->persist(entity: $lookingForPropertyRequestMatchingResponse);
        $this->entityManager->flush();

        $this->addFlash(type: 'dataDeleted', message: 'Der Eintrag wurde als irrelevant markiert');

        $matchingDetailReturnRoute = $this->fetchObjectFromSession(dataClassName: LastVisitedPage::class , sessionKeyName: 'matchingDetailReturnRoute');

        if ($matchingDetailReturnRoute !== null) {
            return $this->redirectToRoute(route: $matchingDetailReturnRoute->getRouteName(), parameters: $matchingDetailReturnRoute->getParameters());
        } else {
            return $this->redirectToRoute(route: 'company_location_management_matching_object_detail', parameters: ['id' => $buildingUnit->getId()]);
        }
    }

    #[Security("is_granted('ROLE_USER')")]
    #[Route('/ansiedlungskonzepte/matching-ergebnisse/{matchingResultId<\d{1,10}>}/matching-status-setzen/{response<\d{1}>}', name: 'settlement_concept_matching_response', methods: ['GET'])]
    public function settlementConceptMatchingResponse(int $matchingResultId, \App\Domain\Entity\Matching\Response $response, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $settlementConceptMatchingResult = $this->settlementConceptMatchingService->fetchSettlementConceptMatchingResultById(
            account: $account,
            id: $matchingResultId
        );

        if ($settlementConceptMatchingResult === null) {
            throw new NotFoundHttpException();
        }

        $buildingUnit = $settlementConceptMatchingResult->getBuildingUnit();
        $settlementConcept = $settlementConceptMatchingResult->getSettlementConcept();

        $settlementConceptMatchingResponse = $this->settlementConceptMatchingService->fetchSettlementConceptMatchingResponseBySettlementConceptAndBuildingUnit(
            account: $account,
            settlementConcept: $settlementConcept,
            buildingUnit: $buildingUnit
        );

        if ($settlementConceptMatchingResponse !== null) {
            $settlementConceptMatchingResponse->setResponse($response);
        } else {
            $settlementConceptMatchingResponse = new SettlementConceptMatchingResponse();

            $settlementConceptMatchingResponse
                ->setCreatedByAccountUser($user)
                ->setBuildingUnit($buildingUnit)
                ->setSettlementConcept($settlementConcept)
                ->setResponse($response);
        }

        $this->entityManager->persist(entity: $settlementConceptMatchingResponse);
        $this->entityManager->flush();

        $this->addFlash(type: 'dataDeleted', message: 'Das Konzept wurde als irrelevant markiert');

        return $this->redirectToRoute(route: 'company_location_management_matching_object_detail', parameters: ['id' => $buildingUnit->getId(), 'tab' => 'ansiedlungskonzepte']);
    }
}
