<?php

declare(strict_types=1);

namespace App\Controller\CompanyLocationManagement\Matching;

use App\Controller\AbstractLeAnController;
use App\Domain\Classification\ClassificationService;
use App\Domain\Entity\LastVisitedPage;
use App\Domain\Entity\LookingForPropertyRequest\LookingForPropertyRequestFilter;
use App\Domain\Entity\LookingForPropertyRequest\LookingForPropertyRequestSearch;
use App\Domain\Entity\LookingForPropertyRequest\MatchingResult;
use App\Domain\Entity\PaginationParameter;
use App\Domain\Entity\Property\BuildingUnitFilter;
use App\Domain\Entity\PropertyOfferType;
use App\Domain\Entity\SortingOption\SortingOption;
use App\Domain\Entity\TaskStatus;
use App\Domain\LookingForPropertyRequest\LookingForPropertyRequestService;
use App\Domain\LookingForPropertyRequest\Matching\MatchingService;
use App\Domain\Property\BuildingUnitService;
use App\Domain\SessionStoredUrlParameter\SessionStoredUrlParameterService;
use App\Form\Type\CompanyLocationManagement\LookingForPropertyRequest\LookingForPropertyRequestFilterType;
use App\Form\Type\CompanyLocationManagement\LookingForPropertyRequest\LookingForPropertyRequestSearchType;
use App\SessionStorage\SessionStorageService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Security("is_granted('ROLE_USER') or is_granted('ROLE_VIEWER') or is_granted('ROLE_RESTRICTED_VIEWER')")]
#[Route('/ansiedlung/matching/ansiedlungsgesuche', name: 'company_location_management_matching_looking_for_property_request_')]
class LookingForPropertyRequestToPropertyMatchingController extends AbstractLeAnController
{
    private const ACTIVE_NAV_GROUP = 'company_location_management';
    private const ACTIVE_NAV_ITEM = self::ACTIVE_NAV_GROUP . '_matching';

    private const OVERVIEW_ROUTE_NAME = 'company_location_management_matching_looking_for_property_request_overview';

    private const OVERVIEW_SESSION_KEY_NAME = 'matchingLookingForPropertyRequestOverview';

    private const URL_PARAMETER_OBJECT_CLASSES = [
        PaginationParameter::class,
        SortingOption::class,
    ];

    public function __construct(
        SessionStorageService $sessionStorageService,
        SessionStoredUrlParameterService $sessionStoredUrlParameterService,
        private readonly BuildingUnitService $buildingUnitService,
        private readonly ClassificationService $classificationService,
        private readonly MatchingService $matchingService,
        private readonly LookingForPropertyRequestService $lookingForPropertyRequestService
    ) {
        parent::__construct(
            sessionStorageService: $sessionStorageService,
            sessionStoredUrlParameterService: $sessionStoredUrlParameterService
        );
    }

    #[Route('/uebersicht', name: 'overview', methods: ['GET', 'POST'])]
    public function overview(Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $paginationParameter = PaginationParameter::createFromRequest(request: $request);

        $this->storeObjectInSession(object: $paginationParameter, sessionKeyName: self::OVERVIEW_SESSION_KEY_NAME);

        $sortingOption = null;

        if ($request->query->get(key: 'sortieren') !== null) {
            $sortingOption = SortingOption::createFromRequest(request: $request);

            $this->storeObjectInSession(object: $sortingOption, sessionKeyName: self::OVERVIEW_SESSION_KEY_NAME);
        }

        $lookingForPropertyRequestFilterForm = $this->createForm(type: LookingForPropertyRequestFilterType::class, options: [
            'account'        => $account,
            'dataClassName'  => LookingForPropertyRequestFilter::class,
            'sessionKeyName' => self::OVERVIEW_SESSION_KEY_NAME,
        ]);

        $lookingForPropertyRequestFilterForm->add(child: 'apply', type: SubmitType::class);

        $lookingForPropertyRequestFilterForm->handleRequest(request: $request);

        $lookingForPropertyRequestSearchForm = $this->createForm(type: LookingForPropertyRequestSearchType::class, options: [
            'dataClassName'  => LookingForPropertyRequestSearch::class,
            'sessionKeyName' => self::OVERVIEW_SESSION_KEY_NAME,
        ]);

        $lookingForPropertyRequestSearchForm->add(child: 'search', type: SubmitType::class);

        $lookingForPropertyRequestSearchForm->handleRequest(request: $request);

        $lookingForPropertyRequests = $this->lookingForPropertyRequestService->fetchLookingForPropertyRequestsPaginatedByAccount(
            account: $account,
            withFromSubAccounts: true,
            firstResult: $paginationParameter->getFirstResult(),
            maxResults: $paginationParameter->getLimit(),
            archived: false,
            active: true,
            lookingForPropertyRequestFilter: $lookingForPropertyRequestFilterForm->getData(),
            lookingForPropertyRequestSearch: $lookingForPropertyRequestSearchForm->getData(),
            sortingOption: $sortingOption
        );

        return $this->render(view: 'company_location_management/matching/looking_for_property_request_overview.html.twig', parameters: [
            'lookingForPropertyRequests'          => $lookingForPropertyRequests,
            'lookingForPropertyRequestSearchForm' => $lookingForPropertyRequestSearchForm,
            'lookingForPropertyRequestFilterForm' => $lookingForPropertyRequestFilterForm,
            'lookingForPropertyRequestFilter'     => $lookingForPropertyRequestFilterForm->getData(),
            'activeMatchingTasks'                 => $this->matchingService->isMatchingTaskRunning(),
            'pageTitle'                           => 'Ansiedlungsmanagement - Ansiedlungsgesuche für Matching',
            'activeNavGroup'                      => self::ACTIVE_NAV_GROUP,
            'activeNavItem'                       => self::ACTIVE_NAV_ITEM,
        ]);
    }

    #[Route('/{id<\d{1,10}>}', name: 'matching_result', methods: ['GET'])]
    public function matchingResult(int $id, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $lastVisitedPage = new LastVisitedPage(routeName: 'company_location_management_matching_looking_for_property_request_matching_result', parameters: ['id' => $id]);

        $this->storeObjectInSession(object: $lastVisitedPage, sessionKeyName: 'matchingDetailReturnRoute');

        $lookingForPropertyRequest = $this->lookingForPropertyRequestService->fetchLookingForPropertyRequestById(account: $account, withFromSubAccounts: false, id: $id);

        if ($lookingForPropertyRequest === null) {
            throw new NotFoundHttpException();
        }

        $matchingResults = $lookingForPropertyRequest->getMatchingResults()->filter(
            p: function (MatchingResult $matchingResult): bool {
               return $matchingResult->isEliminated() === false;
            }
        );

        return $this->render(view: 'company_location_management/matching/looking_for_property_request_matching_result.html.twig', parameters: [
            'lookingForPropertyRequest' => $lookingForPropertyRequest,
            'matchingResults'           => $matchingResults,
            'numberOfMatchingResults'   => $matchingResults->count(),
            'pageTitle'                 => 'Ansiedlungsmanagement - Matching-Ergebnis',
            'activeNavGroup'            => self::ACTIVE_NAV_GROUP,
            'activeNavItem'             => self::ACTIVE_NAV_ITEM,
        ]);
    }

    #[Security("is_granted('ROLE_USER')")]
    #[Route('/matching-task-create', name: 'matching_task_create', methods: ['GET'])]
    public function matchingTaskCreate(UserInterface $user): RedirectResponse
    {
        $account = $user->getAccount();

        $propertyOfferTypes = [
            PropertyOfferType::PURCHASE,
            PropertyOfferType::RENT,
            PropertyOfferType::LEASE,
        ];

        $buildingUnitFilter = new BuildingUnitFilter();

        $buildingUnitFilter
            ->setPropertyStatus(propertyStatus: [0, 1])
            ->setAreaSizeMinimum(areaSizeMinimum: 1)
            ->setPropertyOfferTypes(propertyOfferTypes: $propertyOfferTypes)
            ->setPropertyUsageStatus(propertyUsageStatus: [0 ,1, 2])
            ->setIndustryClassifications(industryClassifications: $this->classificationService->fetchTopLevelIndustryClassifications());

        $buildingUnits = $this->buildingUnitService->fetchBuildingUnitsByAccount(
            account: $account,
            withFromSubAccounts: true,
            archived: false,
            buildingUnitFilter: $buildingUnitFilter,
        );

        $urlParameters = $this->fetchMergedUrlParametersFromSession(
            urlParameterObjectClasses: self::URL_PARAMETER_OBJECT_CLASSES,
            sessionKeyName: self::OVERVIEW_SESSION_KEY_NAME
        );

        if (empty($buildingUnits)) {
            $this->addFlash(type: 'dataDeleted', message: 'Aktuell gibt es keine Objekte, die gematcht werden können.');

            return $this->redirectToRoute(route: self::OVERVIEW_ROUTE_NAME, parameters: $urlParameters);
        }

        foreach ($buildingUnits as $buildingUnit) {
            $this->denyAccessUnlessGranted(attribute: 'view', subject: $buildingUnit);

            $matchingTask = $this->matchingService->fetchMatchingTaskByBuildingUnit(buildingUnit: $buildingUnit);

            if ($matchingTask !== null) {
                if ($matchingTask->getTaskStatus() === TaskStatus::IN_PROGRESS || $matchingTask->getTaskStatus() === TaskStatus::CREATED) {
                    return $this->redirectToRoute(route: self::OVERVIEW_ROUTE_NAME, parameters: $urlParameters);
                }

                $this->matchingService->updateTaskStatusFromMatchingTask(matchingTask: $matchingTask, taskStatus: TaskStatus::CREATED);
            } else {
                $this->matchingService->createAndSaveMatchingTask(buildingUnit: $buildingUnit, account: $account, accountUser: $user);
            }
        }

        $this->addFlash(
            type: 'dataSaved',
            message: 'Das Matching wurde gestartet. Sie können weiterarbeiten und die Seite in einigen Minuten erneut aufrufen, um die Ergebnisse einzusehen.'
        );

        return $this->redirectToRoute(route: self::OVERVIEW_ROUTE_NAME, parameters: $urlParameters);
    }
}
