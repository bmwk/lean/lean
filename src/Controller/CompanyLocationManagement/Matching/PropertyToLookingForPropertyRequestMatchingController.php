<?php

declare(strict_types=1);

namespace App\Controller\CompanyLocationManagement\Matching;

use App\Controller\AbstractLeAnController;
use App\Domain\Entity\LastVisitedPage;
use App\Domain\Entity\LookingForPropertyRequest\MatchingFilter;
use App\Domain\Entity\LookingForPropertyRequest\MatchingSearch;
use App\Domain\Entity\PaginationParameter;
use App\Domain\Entity\SortingOption\SortingOption;
use App\Domain\Entity\TaskStatus;
use App\Domain\LookingForPropertyRequest\Matching\MatchingService;
use App\Domain\Property\BuildingUnitService;
use App\Domain\Property\PdfExpose\PdfExposeService;
use App\Domain\SessionStoredUrlParameter\SessionStoredUrlParameterService;
use App\Domain\SettlementConcept\SettlementConceptMatchingService;
use App\Form\Type\CompanyLocationManagement\Matching\MatchingFilterType;
use App\Form\Type\CompanyLocationManagement\Matching\MatchingSearchType;
use App\SessionStorage\SessionStorageService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Security("is_granted('ROLE_USER') or is_granted('ROLE_VIEWER') or is_granted('ROLE_RESTRICTED_VIEWER')")]
#[Route('/ansiedlung/matching', name: 'company_location_management_matching_')]
class PropertyToLookingForPropertyRequestMatchingController extends AbstractLeAnController
{
    private const ACTIVE_NAV_GROUP = 'company_location_management';
    private const ACTIVE_NAV_ITEM = self::ACTIVE_NAV_GROUP . '_matching';

    private const OVERVIEW_ROUTE_NAME = 'company_location_management_matching_object_overview';

    private const OVERVIEW_SESSION_KEY_NAME = 'matchingOverview';

    private const URL_PARAMETER_OBJECT_CLASSES = [
        PaginationParameter::class,
        SortingOption::class,
    ];

    public function __construct(
        SessionStorageService $sessionStorageService,
        SessionStoredUrlParameterService $sessionStoredUrlParameterService,
        private readonly BuildingUnitService $buildingUnitService,
        private readonly MatchingService $matchingService,
        private readonly SettlementConceptMatchingService $settlementConceptMatchingService,
        private readonly PdfExposeService $pdfExposeService
    ) {
        parent::__construct(
            sessionStorageService: $sessionStorageService,
            sessionStoredUrlParameterService: $sessionStoredUrlParameterService
        );
    }

    #[Route('/objekte/uebersicht', name: 'object_overview', methods: ['GET', 'POST'])]
    public function objectOverview(Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $paginationParameter = PaginationParameter::createFromRequest(request: $request, amountEntriesPerPage: 12);

        $this->storeObjectInSession(object: $paginationParameter, sessionKeyName: self::OVERVIEW_SESSION_KEY_NAME);

        $matchingFilterForm = $this->createForm(type: MatchingFilterType::class, options: [
            'account'        => $account,
            'dataClassName'  => MatchingFilter::class,
            'sessionKeyName' => self::OVERVIEW_SESSION_KEY_NAME,
        ]);

        $matchingFilterForm->add(child: 'apply', type: SubmitType::class);

        $matchingFilterForm->handleRequest(request: $request);

        $matchingSearchForm = $this->createForm(type: MatchingSearchType::class, options: [
            'dataClassName'  => MatchingSearch::class,
            'sessionKeyName' => self::OVERVIEW_SESSION_KEY_NAME,
        ]);

        $matchingSearchForm->add(child: 'search', type: SubmitType::class);

        $matchingSearchForm->handleRequest(request: $request);

        $buildingUnits = $this->buildingUnitService->fetchBuildingUnitsWithMatchingResultsPaginatedByAccount(
            account: $account,
            withFromSubAccounts: true,
            archived: false,
            includeEliminatedMatchingResults: false,
            firstResult: $paginationParameter->getFirstResult(),
            maxResults: $paginationParameter->getLimit(),
            matchingFilter: $matchingFilterForm->getData(),
            matchingSearch: $matchingSearchForm->getData()
        );

        return $this->render(view: 'company_location_management/matching/object_overview.html.twig', parameters: [
            'buildingUnits'      => $buildingUnits,
            'matchingSearchForm' => $matchingSearchForm,
            'matchingFilterForm' => $matchingFilterForm,
            'matchingFilter'     => $matchingFilterForm->getData(),
            'pageTitle'          => 'Ansiedlungsmanagement - Objekte für Matching',
            'activeNavGroup'     => self::ACTIVE_NAV_GROUP,
            'activeNavItem'      => self::ACTIVE_NAV_ITEM,
        ]);
    }

    #[Route('/objekte/{id<\d{1,10}>}', name: 'object_detail', methods: ['GET'])]
    public function objectDetail(int $id, Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $lastVisitedPage = new LastVisitedPage(routeName: 'company_location_management_matching_object_detail', parameters: ['id' => $id]);

        $this->storeObjectInSession(object: $lastVisitedPage, sessionKeyName: 'matchingDetailReturnRoute');

        $buildingUnit = $this->buildingUnitService->fetchBuildingUnitWithMatchingResultsById(
            account: $account,
            withFromSubAccounts: true,
            id: $id,
            archived: false,
            includeEliminatedMatchingResults: false
        );

        if ($buildingUnit === null) {
            throw new NotFoundHttpException();
        }

        if ($buildingUnit->getMatchingTask() === null || $buildingUnit->getSettlementConceptMatchingTask() === null) {
            throw new NotFoundHttpException();
        }

        if (
            in_array(needle: $buildingUnit->getMatchingTask()->getTaskStatus(), haystack: [TaskStatus::CREATED, TaskStatus::IN_PROGRESS]) === true
            || in_array(needle: $buildingUnit->getSettlementConceptMatchingTask()->getTaskStatus(), haystack: [TaskStatus::CREATED, TaskStatus::IN_PROGRESS]) === true
        ) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'view', subject: $buildingUnit);

        if (
            $request->get(key: 'tab') !== null
            && $request->get(key: 'tab') === 'ansiedlungskonzepte' || (count($buildingUnit->getSettlementConceptMatchingResults()) > 0
            && count($buildingUnit->getMatchingResults()) === 0)
        ) {
            $activeLookingForPropertyRequest = false;
            $activeSettlementConcept = true;
        } else {
            $activeLookingForPropertyRequest = true;
            $activeSettlementConcept = false;
        }

        return $this->render(view: 'company_location_management/matching/object_detail.html.twig', parameters: [
            'buildingUnit'                    => $buildingUnit,
            'activeLookingForPropertyRequest' => $activeLookingForPropertyRequest,
            'activeSettlementConcept'         => $activeSettlementConcept,
            'pageTitle'                       => 'Ansiedlungsmanagement – Matching-Ergebnisse',
            'activeNavGroup'                  => self::ACTIVE_NAV_GROUP,
            'activeNavItem'                   => self::ACTIVE_NAV_ITEM,
        ]);
    }

    #[Security("is_granted('ROLE_USER')")]
    #[Route('/objekte/{id<\d{1,10}>}/matching-task-create', name: 'object_matching_task_create', methods: ['GET'])]
    public function objectMatchingTaskCreate(int $id, UserInterface $user): RedirectResponse
    {
        $account = $user->getAccount();

        $buildingUnit = $this->buildingUnitService->fetchBuildingUnitById(account: $account, withFromSubAccounts: true, id: $id);

        if ($buildingUnit === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'view', subject: $buildingUnit);

        $matchingTask = $this->matchingService->fetchMatchingTaskByBuildingUnit(buildingUnit: $buildingUnit);

        $settlementConceptMatchingTask = $this->settlementConceptMatchingService->fetchSettlementConceptMatchingTaskByBuildingUnit(buildingUnit: $buildingUnit);

        $urlParameters = $this->fetchMergedUrlParametersFromSession(
            urlParameterObjectClasses: self::URL_PARAMETER_OBJECT_CLASSES,
            sessionKeyName: self::OVERVIEW_SESSION_KEY_NAME
        );

        if ($matchingTask !== null) {
            if (
                $matchingTask->getTaskStatus() !== TaskStatus::IN_PROGRESS
                && $matchingTask->getTaskStatus() !== TaskStatus::CREATED
            ) {
                $this->matchingService->updateTaskStatusFromMatchingTask(matchingTask: $matchingTask, taskStatus: TaskStatus::CREATED);
            }
        } else {
            $this->matchingService->createAndSaveMatchingTask(buildingUnit: $buildingUnit, account: $account, accountUser: $user);
        }

        if ($settlementConceptMatchingTask !== null) {
            if (
                $settlementConceptMatchingTask->getTaskStatus() !== TaskStatus::IN_PROGRESS
                && $settlementConceptMatchingTask->getTaskStatus() !== TaskStatus::CREATED
            ) {
                $this->settlementConceptMatchingService->updateSettlementConceptTaskStatus(
                    settlementConceptMatchingTask: $settlementConceptMatchingTask,
                    taskStatus: TaskStatus::CREATED
                );
            }
        } else {
            $this->settlementConceptMatchingService->createAndSaveSettlementConceptMatchingTask(
                buildingUnit: $buildingUnit,
                account: $account,
                accountUser: $user
            );
        }

        $this->addFlash(
            type: 'dataSaved',
            message: 'Das Matching wurde gestartet. Sie können weiterarbeiten und die Seite in wenigen Minuten erneut aufrufen, um die Ergebnisse einzusehen.'
        );

        return $this->redirectToRoute(route: self::OVERVIEW_ROUTE_NAME, parameters: $urlParameters);
    }

    #[Route('/objekte/{id<\d{1,10}>}/pdf-expose', name: 'pdf_expose', methods: ['GET'])]
    public function pdfExpose(int $id, UserInterface $user): Response
    {
        $buildingUnit = $this->buildingUnitService->fetchBuildingUnitById(account: $user->getAccount(), withFromSubAccounts: true, id: $id);

        if ($buildingUnit === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'view', subject: $buildingUnit);

        return new Response(
            content: $this->pdfExposeService->generateBuildingUnitPdfExpose(buildingUnit: $buildingUnit, accountUser: $user),
            headers: ['Content-Type' => 'application/pdf']
        );
    }
}
