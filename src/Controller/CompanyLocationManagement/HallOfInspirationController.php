<?php

declare(strict_types=1);

namespace App\Controller\CompanyLocationManagement;

use App\Domain\InterfaceConfiguration\InterfaceConfigurationService;
use App\HallOfInspiration\HallOfInspirationService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Security("is_granted('ROLE_USER') or is_granted('ROLE_LOOKING_FOR_PROPERTY_REQUEST_FEEDER') or is_granted('ROLE_VIEWER') or is_granted('ROLE_RESTRICTED_VIEWER')")]
#[Route('/ansiedlung/hall-of-inspiration', name: 'company_location_management_hall_of_inspiration_')]
class HallOfInspirationController extends AbstractController
{
    private const ACTIVE_NAV_GROUP = 'company_location_management';
    private const ACTIVE_NAV_ITEM = self::ACTIVE_NAV_GROUP . '_hall_of_inspiration';

    public function __construct(
        private readonly InterfaceConfigurationService $interfaceConfigurationService,
        private readonly HallOfInspirationService $hallOfInspirationService
    ) {
    }

    #[Route('/uebersicht', name: 'overview', methods: ['GET'])]
    public function overview(UserInterface $user): Response
    {
        if ($this->interfaceConfigurationService->hasHallOfInspirationConfiguration(account: $user->getAccount()) === false) {
            throw new NotFoundHttpException();
        }

        $hallOfInspirationConfiguration = $this->interfaceConfigurationService->fetchHallOfInspirationConfigurationByAccount(account: $user->getAccount());

        $hallOfInspirationConcepts = $this->hallOfInspirationService->getConcepts(apiAccessToken: $hallOfInspirationConfiguration->getAccessToken());

        return $this->render(view: 'company_location_management/hall_of_inspiration/overview.html.twig', parameters: [
            'hallOfInspirationConcepts' => $hallOfInspirationConcepts,
            'pageTitle'                 => 'Hall of Inspiration - powered by Leerstandslotsen',
            'activeNavGroup'            => self::ACTIVE_NAV_GROUP,
            'activeNavItem'             => self::ACTIVE_NAV_ITEM,
        ]);
    }

    #[Route('/{id<\d{1,10}>}', name: 'detail', methods: ['GET'])]
    public function detail(int $id, UserInterface $user): Response
    {
        if ($this->interfaceConfigurationService->hasHallOfInspirationConfiguration(account: $user->getAccount()) === false) {
            throw new NotFoundHttpException();
        }

        $hallOfInspirationConfiguration = $this->interfaceConfigurationService->fetchHallOfInspirationConfigurationByAccount(account: $user->getAccount());

        $hallOfInspirationConcept = $this->hallOfInspirationService->getConcept(apiAccessToken: $hallOfInspirationConfiguration->getAccessToken(), id: $id);

        if ($hallOfInspirationConcept === null) {
            throw new NotFoundHttpException();
        }

        return $this->render(view: 'company_location_management/hall_of_inspiration/detail.html.twig', parameters: [
            'hallOfInspirationConcept' => $hallOfInspirationConcept,
            'pageTitle'                => 'Hall of Inspiration - ' . $hallOfInspirationConcept->getBrandName(),
            'activeNavGroup'           => self::ACTIVE_NAV_GROUP,
            'activeNavItem'            => self::ACTIVE_NAV_ITEM,
        ]);
    }
}
