<?php

declare(strict_types=1);

namespace App\Controller\CompanyLocationManagement\LookingForPropertyRequestReport;

use App\Controller\AbstractLeAnController;
use App\Domain\Classification\ClassificationService;
use App\Domain\Entity\LocationCategory;
use App\Domain\Entity\LocationFactor;
use App\Domain\Entity\DeletionType;
use App\Domain\Entity\LookingForPropertyRequestReport\LookingForPropertyRequestReportFilter;
use App\Domain\Entity\Person\PersonType;
use App\Domain\Entity\PlaceRelationType;
use App\Domain\Entity\PropertyOfferType;
use App\Domain\Entity\LookingForPropertyRequestReport\LookingForPropertyRequestReportNote;
use App\Domain\Location\LocationService;
use App\Domain\LookingForPropertyRequest\LookingForPropertyRequestService;
use App\Domain\LookingForPropertyRequestReport\LookingForPropertyRequestReportDeletionService;
use App\Domain\LookingForPropertyRequestReport\LookingForPropertyRequestReportService;
use App\Domain\SessionStoredUrlParameter\SessionStoredUrlParameterService;
use App\Form\Type\CompanyLocationManagement\LookingForPropertyRequestReport\LookingForPropertyRequestReportFilterType;
use App\Form\Type\CompanyLocationManagement\LookingForPropertyRequestReport\ReportDeleteType;
use App\Form\Type\CompanyLocationManagement\LookingForPropertyRequestReport\ReportNoteDeleteType;
use App\Form\Type\CompanyLocationManagement\LookingForPropertyRequestReport\ReportNoteType;
use App\Form\Type\CompanyLocationManagement\LookingForPropertyRequestReport\ReportToLookingForPropertyRequestType;
use App\SessionStorage\SessionStorageService;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Security("is_granted('ROLE_USER') or is_granted('ROLE_LOOKING_FOR_PROPERTY_REQUEST_REPORT_MANAGER') or is_granted('ROLE_VIEWER') or is_granted('ROLE_RESTRICTED_VIEWER')")]
#[Route('/ansiedlung/gesuchsmeldungen', name: 'company_location_management_looking_for_property_request_report_')]
class LookingForPropertyRequestReportController extends AbstractLeAnController
{
    private const ACTIVE_NAV_GROUP = 'company_location_management';
    private const ACTIVE_NAV_ITEM = self::ACTIVE_NAV_GROUP . '_looking_for_property_request_report';

    private const OVERVIEW_SESSION_KEY_NAME = 'lookingForPropertyRequestReportOverview';

    public function __construct(
        SessionStorageService $sessionStorageService,
        SessionStoredUrlParameterService $sessionStoredUrlParameterService,
        private readonly LookingForPropertyRequestReportService $lookingForPropertyRequestReportService,
        private readonly LookingForPropertyRequestReportDeletionService $lookingForPropertyRequestReportDeletionService,
        private readonly LookingForPropertyRequestService $lookingForPropertyRequestService,
        private readonly ClassificationService $classificationService,
        private readonly LocationService $locationService,
        private readonly EntityManagerInterface $entityManager
    ) {
        parent::__construct(
            sessionStorageService: $sessionStorageService,
            sessionStoredUrlParameterService: $sessionStoredUrlParameterService
        );
    }

    #[Route('/neue-meldungen', name: 'unprocessed_overview', methods: ['GET', 'POST'])]
    public function unprocessedOverview(Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $lookingForPropertyRequestReportFilterForm = $this->createForm(type: LookingForPropertyRequestReportFilterType::class, options: [
            'dataClassName'  => LookingForPropertyRequestReportFilter::class,
            'sessionKeyName' => self::OVERVIEW_SESSION_KEY_NAME
        ]);

        $lookingForPropertyRequestReportFilterForm->add(child:'apply', type: SubmitType::class);

        $lookingForPropertyRequestReportFilterForm->handleRequest(request: $request);

        $lookingForPropertyRequestReports = $this->lookingForPropertyRequestReportService->fetchUnprocessedLookingForPropertyRequestReportsByAccount(
            account: $account,
            emailConfirmed: true,
            lookingForPropertyRequestReportFilter: $lookingForPropertyRequestReportFilterForm->getData()
        );

        return $this->render(view: 'company_location_management/looking_for_property_request_report/unprocessed_overview.html.twig', parameters: [
            'lookingForPropertyRequestReportFilterForm' => $lookingForPropertyRequestReportFilterForm,
            'lookingForPropertyRequestReports'          => $lookingForPropertyRequestReports,
            'pageTitle'                                 => 'Neue Gesuchsmeldungen',
            'activeNavGroup'                            => self::ACTIVE_NAV_GROUP,
            'activeNavItem'                             => self::ACTIVE_NAV_ITEM,
        ]);
    }

    #[Route('/bearbeitete-meldungen', name: 'processed_overview', methods: ['GET', 'POST'])]
    public function processedOverview(Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $lookingForPropertyRequestReportFilterForm = $this->createForm(type: LookingForPropertyRequestReportFilterType::class, options: [
            'dataClassName'  => LookingForPropertyRequestReportFilter::class,
            'sessionKeyName' => self::OVERVIEW_SESSION_KEY_NAME
        ]);

        $lookingForPropertyRequestReportFilterForm->add(child:'apply', type: SubmitType::class);

        $lookingForPropertyRequestReportFilterForm->handleRequest(request: $request);

        $lookingForPropertyRequestReports = $this->lookingForPropertyRequestReportService->fetchProcessedLookingForPropertyRequestReportsByAccount(
            account: $account,
            emailConfirmed: true,
            lookingForPropertyRequestReportFilter: $lookingForPropertyRequestReportFilterForm->getData()
        );

        return $this->render(view: 'company_location_management/looking_for_property_request_report/processed_overview.html.twig', parameters: [
            'lookingForPropertyRequestReportFilterForm' => $lookingForPropertyRequestReportFilterForm,
            'lookingForPropertyRequestReports'          => $lookingForPropertyRequestReports,
            'pageTitle'                                 => 'Bearbeitete Gesuchsmeldungen',
            'activeNavGroup'                            => self::ACTIVE_NAV_GROUP,
            'activeNavItem'                             => self::ACTIVE_NAV_ITEM,
        ]);
    }

    #[Route('/meldungen/{id<\d{1,10}>}', name: 'report', methods: ['GET'])]
    public function report(int $id, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $lookingForPropertyRequestReport = $this->lookingForPropertyRequestReportService->fetchLookingForPropertyRequestReportByIdAndAccount(id: $id, account: $account);

        if ($lookingForPropertyRequestReport === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'view', subject: $lookingForPropertyRequestReport);

        return $this->render(view: 'company_location_management/looking_for_property_request_report/report.html.twig', parameters: [
            'lookingForPropertyRequestReport' => $lookingForPropertyRequestReport,
            'pageTitle'                       => 'Gesuchsmeldung',
            'locationCategories'              => LocationCategory::cases(),
            'locationFactors'                 => LocationFactor::cases(),
            'propertyOfferTypes'              => PropertyOfferType::cases(),
            'activeNavGroup'                  => self::ACTIVE_NAV_GROUP,
            'activeNavItem'                   => self::ACTIVE_NAV_ITEM,
        ]);
    }

    #[Security("is_granted('ROLE_USER') or is_granted('ROLE_LOOKING_FOR_PROPERTY_REQUEST_REPORT_MANAGER')")]
    #[Route('/meldungen/{id<\d{1,10}>}/als-bearbeitet-markieren', name: 'set_as_processed', methods: ['GET'])]
    public function setAsProcessed(int $id, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $lookingForPropertyRequestReport = $this->lookingForPropertyRequestReportService->fetchLookingForPropertyRequestReportByIdAndAccount(
            id: $id,
            account: $account
        );

        if ($lookingForPropertyRequestReport === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'edit', subject: $lookingForPropertyRequestReport);

        $lookingForPropertyRequestReport
            ->setProcessed(true)
            ->setProcessedAt(new \DateTimeImmutable());

        $this->entityManager->flush();

        $this->addFlash(type: 'dataSaved', message: 'Die Meldung wurde als bearbeitet markiert.');

        return $this->redirectToRoute(route: 'company_location_management_looking_for_property_request_report_processed_overview');
    }

    #[Security("is_granted('ROLE_USER') or is_granted('ROLE_LOOKING_FOR_PROPERTY_REQUEST_REPORT_MANAGER')")]
    #[Route('/meldungen/{id<\d{1,10}>}/loeschen', name: 'delete', methods: ['GET', 'POST'])]
    public function delete(int $id, Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $lookingForPropertyRequestReport = $this->lookingForPropertyRequestReportService->fetchLookingForPropertyRequestReportByIdAndAccount(
            id: $id,
            account: $account
        );

        if ($lookingForPropertyRequestReport === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'delete', subject: $lookingForPropertyRequestReport);

        $reportDeleteTypeForm = $this->createForm(type: ReportDeleteType::class);

        $reportDeleteTypeForm->add(child:'delete', type: SubmitType::class);

        $reportDeleteTypeForm->handleRequest(request: $request);

        if ($reportDeleteTypeForm->isSubmitted() && $reportDeleteTypeForm->isValid()) {
            if ($lookingForPropertyRequestReport->isProcessed()) {
                $route = 'company_location_management_looking_for_property_request_report_processed_overview';
            } else {
                $route = 'company_location_management_looking_for_property_request_report_unprocessed_overview';
            }

            if ($reportDeleteTypeForm->get('verificationCode')->getData() === 'meldung_' . $lookingForPropertyRequestReport->getId()) {
                $this->lookingForPropertyRequestReportDeletionService->deleteLookingForPropertyRequestReport(
                    lookingForPropertyRequestReport: $lookingForPropertyRequestReport,
                    deletionType: DeletionType::SOFT,
                    deletedByAccountUser: $user
                );

                $this->addFlash(type: 'dataDeleted', message: 'Die Meldung wurde gelöscht.');

                return $this->redirectToRoute(route: $route);
            } else {
                $this->addFlash(type: 'dataError', message: 'Der Verifizierungscode war falsch.');
            }
        }

        return $this->render(view: 'company_location_management/looking_for_property_request_report/delete.html.twig', parameters: [
            'reportDeleteForm'                => $reportDeleteTypeForm,
            'lookingForPropertyRequestReport' => $lookingForPropertyRequestReport,
            'pageTitle'                       => 'Gesuchsmeldung löschen',
            'activeNavGroup'                  => self::ACTIVE_NAV_GROUP,
            'activeNavItem'                   => self::ACTIVE_NAV_ITEM,
        ]);
    }

    #[Security("is_granted('ROLE_USER') or is_granted('ROLE_LOOKING_FOR_PROPERTY_REQUEST_REPORT_MANAGER')")]
    #[Route('/meldungen/{id<\d{1,10}>}/notiz-erstellen', name: 'note_create', methods: ['GET', 'POST'])]
    public function noteCreate(int $id, Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $lookingForPropertyRequestReport = $this->lookingForPropertyRequestReportService->fetchLookingForPropertyRequestReportByIdAndAccount(
            id: $id,
            account: $account
        );

        if ($lookingForPropertyRequestReport === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'edit', subject: $lookingForPropertyRequestReport);

        $lookingForPropertyRequestReportNote = new LookingForPropertyRequestReportNote();

        $reportNoteForm = $this->createForm(type: ReportNoteType::class, data: $lookingForPropertyRequestReportNote);

        $reportNoteForm->add(child:'save', type: SubmitType::class);

        $reportNoteForm->handleRequest(request: $request);

        if ($reportNoteForm->isSubmitted() && $reportNoteForm->isValid()) {
            $lookingForPropertyRequestReportNote
                ->setLookingForPropertyRequestReport($lookingForPropertyRequestReport)
                ->setCreatedByAccountUser($user);

            $this->entityManager->persist(entity: $lookingForPropertyRequestReportNote);

            $this->entityManager->flush();

            $this->addFlash(type: 'dataSaved', message: 'Die Notiz wurde eingetragen.');

            return $this->redirectToRoute(route: 'company_location_management_looking_for_property_request_report_report', parameters: ['id' => $lookingForPropertyRequestReport->getId()]);
        }

        return $this->render(view: 'company_location_management/looking_for_property_request_report/note_create.html.twig', parameters: [
            'reportNoteForm'                  => $reportNoteForm,
            'lookingForPropertyRequestReport' => $lookingForPropertyRequestReport,
            'pageTitle'                       => 'Notiz zur Gesuchsmeldung eintragen',
            'activeNavGroup'                  => self::ACTIVE_NAV_GROUP,
            'activeNavItem'                   => self::ACTIVE_NAV_ITEM,
        ]);
    }

    #[Route('/meldungen/{id<\d{1,10}>}/notizen/{noteId<\d{1,10}>}', name: 'note_edit', methods: ['GET', 'POST'])]
    public function noteEdit(int $id, int $noteId, Request $request, UserInterface $user): Response
    {
        $lookingForPropertyRequestReport = $this->lookingForPropertyRequestReportService->fetchLookingForPropertyRequestReportByIdAndAccount(
            id: $id,
            account: $user->getAccount()
        );

        if ($lookingForPropertyRequestReport === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'view', subject: $lookingForPropertyRequestReport);

        $lookingForPropertyRequestReportNote = $this->lookingForPropertyRequestReportService->fetchLookingForPropertyRequestReportNoteById(id: $noteId);

        if ($lookingForPropertyRequestReportNote === null) {
            throw new NotFoundHttpException();
        }

        if ($lookingForPropertyRequestReport->getLookingForPropertyRequestReportNotes()->contains(element: $lookingForPropertyRequestReportNote) === false) {
            throw new NotFoundHttpException();
        }

        $reportNoteForm = $this->createForm(type: ReportNoteType::class, data: $lookingForPropertyRequestReportNote);

        $reportNoteForm->add(child:'save', type: SubmitType::class);

        $reportNoteForm->handleRequest(request: $request);

        if ($reportNoteForm->isSubmitted() && $reportNoteForm->isValid()) {
            $this->denyAccessUnlessGranted(attribute: 'edit', subject: $lookingForPropertyRequestReport);

            $this->entityManager->flush();

            $this->addFlash(type: 'dataSaved', message: 'Die Notiz wurde bearbeitet.');

            return $this->redirectToRoute(route: 'company_location_management_looking_for_property_request_report_report', parameters: ['id' => $lookingForPropertyRequestReport->getId()]);
        }

        return $this->render(view: 'company_location_management/looking_for_property_request_report/note_edit.html.twig', parameters: [
            'reportNoteForm'                      => $reportNoteForm,
            'lookingForPropertyRequestReportNote' => $lookingForPropertyRequestReportNote,
            'lookingForPropertyRequestReport'     => $lookingForPropertyRequestReport,
            'pageTitle'                           => 'Notiz zur Gesuchsmeldung bearbeiten',
            'activeNavGroup'                      => self::ACTIVE_NAV_GROUP,
            'activeNavItem'                       => self::ACTIVE_NAV_ITEM,
        ]);
    }

    #[Route('/meldungen/{id<\d{1,10}>}/notizen/{noteId<\d{1,10}>}/delete', name: 'note_delete', methods: ['GET', 'POST'])]
    public function noteDelete(int $id, int $noteId, Request $request, UserInterface $user): Response
    {
        $lookingForPropertyRequestReport = $this->lookingForPropertyRequestReportService->fetchLookingForPropertyRequestReportByIdAndAccount(
            id: $id,
            account: $user->getAccount()
        );

        if ($lookingForPropertyRequestReport === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'edit', subject: $lookingForPropertyRequestReport);

        $lookingForPropertyRequestReportNote = $this->lookingForPropertyRequestReportService->fetchLookingForPropertyRequestReportNoteById(id: $noteId);

        if ($lookingForPropertyRequestReportNote === null) {
            throw new NotFoundHttpException();
        }

        if ($lookingForPropertyRequestReport->getLookingForPropertyRequestReportNotes()->contains(element: $lookingForPropertyRequestReportNote) === false) {
            throw new NotFoundHttpException();
        }

        $reportNoteDeleteForm = $this->createForm(type: ReportNoteDeleteType::class);

        $reportNoteDeleteForm->add(child:'delete', type: SubmitType::class);

        $reportNoteDeleteForm->handleRequest(request: $request);

        if ($reportNoteDeleteForm->isSubmitted() && $reportNoteDeleteForm->isValid()) {
            $formData = $reportNoteDeleteForm->getData();

            if ($formData['verificationCode'] === 'notiz_' . $lookingForPropertyRequestReportNote->getId()) {
                $this->lookingForPropertyRequestReportDeletionService->deleteLookingForPropertyRequestReportNote(
                    lookingForPropertyRequestReportNote: $lookingForPropertyRequestReportNote
                );

                $this->addFlash(type: 'dataDeleted', message: 'Die Notiz wurde gelöscht.');

                return $this->redirectToRoute(route: 'company_location_management_looking_for_property_request_report_report', parameters: ['id' => $lookingForPropertyRequestReport->getId()]);
            } else {
                $this->addFlash(type: 'dataError', message: 'Der Verifizierungscode war falsch.');
            }
        }

        $reportNoteForm = $this->createForm(type: ReportNoteType::class, data: $lookingForPropertyRequestReportNote);

        return $this->render(view: 'company_location_management/looking_for_property_request_report/note_delete.html.twig', parameters: [
            'reportNoteDeleteForm'                => $reportNoteDeleteForm,
            'reportNoteForm'                      => $reportNoteForm,
            'lookingForPropertyRequestReportNote' => $lookingForPropertyRequestReportNote,
            'lookingForPropertyRequestReport'     => $lookingForPropertyRequestReport,
            'pageTitle'                           => 'Notiz zur Gesuchsmeldung löschen',
            'activeNavGroup'                      => self::ACTIVE_NAV_GROUP,
            'activeNavItem'                       => self::ACTIVE_NAV_ITEM,
        ]);
    }

    #[Security("is_granted('ROLE_USER') or is_granted('ROLE_LOOKING_FOR_PROPERTY_REQUEST_REPORT_MANAGER')")]
    #[Route('/meldungen/{id<\d{1,10}>}/zu-gesuch-ueberfuehren', name: 'report_to_looking_for_property_request', methods: ['GET', 'POST'])]
    public function reportToLookingForPropertyRequest(int $id, Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $lookingForPropertyRequestReport = $this->lookingForPropertyRequestReportService->fetchLookingForPropertyRequestReportByIdAndAccount(
            id: $id,
            account: $account
        );

        if ($lookingForPropertyRequestReport === null) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(attribute: 'edit', subject: $lookingForPropertyRequestReport);

        if ($lookingForPropertyRequestReport->getLookingForPropertyRequest() !== null) {
            return $this->redirectToRoute(route: 'company_location_management_looking_for_property_request_report_processed_overview');
        }

        $lookingForPropertyRequest = $this->lookingForPropertyRequestService->createLookingForPropertyRequestFromLookingForPropertyRequestReport(
            lookingForPropertyRequestReport: $lookingForPropertyRequestReport,
            accountUser: $user
        );

        $places = $this->locationService->fetchRecursivePlacesByPlacesAndPlaceRelationTypeAndPlaceType(
            places: [$account->getAssignedPlace()],
            placeTypes: $this->locationService->getCommunePlaceTypes(),
            placeRelationType: PlaceRelationType::CHILD
        );

        $reportToLookingForPropertyRequestForm = $this->createForm(
            type: ReportToLookingForPropertyRequestType::class,
            data: $lookingForPropertyRequest,
            options: ['places' => $places]
        );

        $industryClassificationForm = $reportToLookingForPropertyRequestForm->get('industryClassification');

        $industryClassificationForm->get('industryClassificationId')->setData($lookingForPropertyRequest->getPropertyRequirement()->getIndustryClassification()->getId());

        $personCreateForm = $reportToLookingForPropertyRequestForm->get('personCreate');

        if ($lookingForPropertyRequest->getPerson()->getPersonType() === PersonType::COMPANY) {
            $personCreateForm->get('selectedPersonType')->setData(PersonType::COMPANY->value);
            $personCreateForm->get('company')->setData($lookingForPropertyRequest->getPerson());
        } elseif ($lookingForPropertyRequest->getPerson()->getPersonType() === PersonType::NATURAL_PERSON) {
            $personCreateForm->get('selectedPersonType')->setData(PersonType::NATURAL_PERSON->value);
            $personCreateForm->get('naturalPerson')->setData($lookingForPropertyRequest->getPerson());
        }

        $reportToLookingForPropertyRequestForm->add(child:'save', type: SubmitType::class);

        $reportToLookingForPropertyRequestForm->handleRequest(request: $request);

        if ($reportToLookingForPropertyRequestForm->isSubmitted() && $reportToLookingForPropertyRequestForm->isValid()) {
            $industryClassificationId = (int)$industryClassificationForm->get('industryClassificationId')->getData();

            $industryClassification = $this->classificationService->fetchIndustryClassificationById(id: $industryClassificationId);

            $lookingForPropertyRequest->getPropertyRequirement()->setIndustryClassification(industryClassification: $industryClassification);

            $this->entityManager->beginTransaction();

            $lookingForPropertyRequest->getPerson()
                ->setAccount($account)
                ->setCreatedByAccountUser($user)
                ->setDeleted(false)
                ->setAnonymized(false);

            $this->entityManager->persist($lookingForPropertyRequest->getPerson());

            foreach ($lookingForPropertyRequest->getPerson()->getContacts() as $contact) {
                $contact
                    ->setAccount($account)
                    ->setCreatedByAccountUser($user)
                    ->setDeleted(false)
                    ->setAnonymized(false);

                $this->entityManager->persist(entity: $contact);
            }

            $this->lookingForPropertyRequestService->persistLookingForPropertyRequest(lookingForPropertyRequest: $lookingForPropertyRequest);

            $lookingForPropertyRequest
                ->setAccount($account)
                ->setDeleted(false);

            $lookingForPropertyRequestReport
                ->setLookingForPropertyRequest($lookingForPropertyRequest)
                ->setProcessed(true)
                ->setProcessedAt(new \DateTimeImmutable());

            $this->entityManager->flush();
            $this->entityManager->commit();

            $this->addFlash(type: 'dataSaved', message: 'Die Meldung wurde als Gesuch angelegt.');

            return $this->redirectToRoute(route: 'company_location_management_looking_for_property_request_report_processed_overview');
        }

        return $this->render(view: 'company_location_management/looking_for_property_request_report/report_to_looking_for_property_request.html.twig', parameters: [
            'reportToLookingForPropertyRequestForm' => $reportToLookingForPropertyRequestForm,
            'lookingForPropertyRequestReport'       => $lookingForPropertyRequestReport,
            'pageTitle'                             => 'Gesuchsmeldung überführen',
            'activeNavGroup'                        => self::ACTIVE_NAV_GROUP,
            'activeNavItem'                         => self::ACTIVE_NAV_ITEM,
        ]);
    }
}
