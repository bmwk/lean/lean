<?php

declare(strict_types=1);

namespace App\Controller\SettlementConcept\Api\V1;

use App\Domain\Classification\ClassificationService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

#[IsGranted('ROLE_SETTLEMENT_CONCEPT_API_USER')]
#[Route('settlement-concept/api/v1', name: 'settlement_concept_api_')]
class IndustryClassificationController extends AbstractController
{
    private const INDUSTRY_CLASSIFICATION_ATTRIBUTES = [
        'id',
        'name',
        'levelOne',
        'levelTwo',
        'levelThree',
    ];

    public function __construct(
        private readonly ClassificationService $classificationService
    ) {
    }

    #[Route('/industry-classifications', name: 'get_industry_classifications', methods: ['GET'], format: 'json')]
    public function getIndustryClassifications(Request $request): JsonResponse
    {
        if ($request->query->has('levelOne') === false) {
            return $this->json(
                data: $this->classificationService->fetchAllIndustryClassifications(),
                context: [
                    AbstractNormalizer::ATTRIBUTES => self::INDUSTRY_CLASSIFICATION_ATTRIBUTES,
                ]
            );
        }

        $levelOne = (int) $request->query->get('levelOne');
        $levelTwo = $request->query->has('levelTwo') ? (int)$request->query->get('levelTwo') : null;
        $levelThree = $request->query->has('levelThree') ? (int)$request->query->get('levelThree') : null;

        $industryClassifications = $this->classificationService->fetchIndustryClassificationByLevels(
            levelOne: $levelOne,
            levelTwo: $levelTwo,
            levelThree: $levelThree
        );

        return $this->json(
            data: $industryClassifications,
            context: [
                AbstractNormalizer::ATTRIBUTES => self::INDUSTRY_CLASSIFICATION_ATTRIBUTES,
            ]
        );
    }

    #[Route('/industry-classifications/{industryClassificationId<\d{1,10}>}', name: 'get_industry_classification', methods: ['GET'], format: 'json')]
    public function getIndustryClassification(int $industryClassificationId): JsonResponse
    {
        $industryClassification = $this->classificationService->fetchIndustryClassificationById(id: $industryClassificationId);

        if ($industryClassification === null) {
            throw new NotFoundHttpException();
        }

        return $this->json(
            data: $industryClassification,
            context: [
                AbstractNormalizer::ATTRIBUTES => self::INDUSTRY_CLASSIFICATION_ATTRIBUTES,
            ]
        );
    }

    #[Route('/industry-classifications/{industryClassificationId<\d{1,10}>}/industry-classifications', name: 'get_industry_classifications_form_parent_industry_classification', methods: ['GET'], format: 'json')]
    public function getIndustryClassificationsFormParentIndustryClassification(int $industryClassificationId): JsonResponse
    {
        $industryClassification = $this->classificationService->fetchIndustryClassificationById(id: $industryClassificationId);

        if ($industryClassification === null) {
            throw new NotFoundHttpException();
        }

        return $this->json(
            data: $this->classificationService->fetchIndustryClassificationChildren(parentIndustryClassification: $industryClassification),
            context: [
                AbstractNormalizer::ATTRIBUTES => self::INDUSTRY_CLASSIFICATION_ATTRIBUTES,
            ]
        );
    }
}
