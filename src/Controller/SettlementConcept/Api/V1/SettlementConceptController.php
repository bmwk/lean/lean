<?php

declare(strict_types=1);

namespace App\Controller\SettlementConcept\Api\V1;

use App\Domain\Entity\SettlementConcept\SettlementConceptAccountUser;
use App\Domain\Property\PdfExpose\PdfExposeService;
use App\Domain\SettlementConcept\Api\Entity\Request\SettlementConceptRequest;
use App\Domain\SettlementConcept\SettlementConceptDeletionService;
use App\Domain\SettlementConcept\SettlementConceptService;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

#[IsGranted('ROLE_SETTLEMENT_CONCEPT_API_USER')]
#[Route('settlement-concept/api/v1', name: 'settlement_concept_api_')]
class SettlementConceptController extends AbstractController
{
    private const ATTRIBUTES = [
        'id',
        'propertyOfferTypes',
        'settlementConceptPropertyRequirement' => [
            'space' => [
                'minimumValue',
                'maximumValue'
            ],
            'secondarySpace' => [
                'minimumValue',
                'maximumValue'
            ],
            'outdoorSalesAreaRequired',
            'outdoorSalesArea' => [
                'minimumValue',
                'maximumValue'
            ],
            'storeWidth' => [
                'minimumValue',
                'maximumValue'
            ],
            'shopWindowLength' => [
                'minimumValue',
                'maximumValue'
            ],
            'groundLevelSalesAreaRequired',
            'barrierFreeAccessRequired',
            'parkingLotsRequired',
            'locationCategories',
            'locationFactors'
        ],
        'ageStructures',
        'priceSegments',
        'description',
        'externalReferenceNumber',
        'naceClassification' => [
            'id',
            'code',
            'level',
            'name',
        ],
        'industryClassification' => [
            'id',
            'levelOne',
            'levelTwo',
            'levelThree',
            'name',
        ],
    ];

    private const IGNORED_ATTRIBUTES = [
        '__initializer__',
        '__cloner__',
        '__isInitialized__',
    ];

    public function __construct(
        private readonly SettlementConceptDeletionService $settlementConceptDeletionService,
        private readonly SettlementConceptService $settlementConceptService,
        private readonly EntityManagerInterface $entityManager,
        private readonly PdfExposeService $pdfExposeService
    ) {
    }

    #[Route('/settlement-concepts', name: 'get_settlement_concepts', methods: ['GET'], format: 'json')]
    public function getSettlementConcepts(UserInterface $user): JsonResponse
    {
        /** @var SettlementConceptAccountUser $user */
        return $this->json(
            data: $this->settlementConceptService->fetchSettlementConceptsByAccountUser(settlementConceptAccountUser: $user),
            context: [
                AbstractNormalizer::ATTRIBUTES         => self::ATTRIBUTES,
                AbstractNormalizer::IGNORED_ATTRIBUTES => self::IGNORED_ATTRIBUTES,
            ]
        );
    }

    #[Route('/settlement-concepts', name: 'post_settlement_concept', methods: ['POST'], format: 'json')]
    public function postSettlementConcept(Request $request, UserInterface $user): JsonResponse
    {
        /** @var SettlementConceptAccountUser $user */
        $settlementConceptData = json_decode(json: $request->getContent(), associative: false);

        $settlementConceptPostRequest = SettlementConceptRequest::createFromJsonContent(jsonContent: $settlementConceptData);

        $settlementConcept = $this->settlementConceptService->createSettlementConceptFromPostRequest(
            settlementConceptPostRequest: $settlementConceptPostRequest,
            settlementConceptAccountUser: $user
        );

        $this->settlementConceptService->persistSettlementConcept(settlementConcept: $settlementConcept);

        $this->entityManager->flush();

        return $this->json(
            data: $settlementConcept,
            status: Response::HTTP_CREATED,
            context: [
                AbstractNormalizer::ATTRIBUTES         => self::ATTRIBUTES,
                AbstractNormalizer::IGNORED_ATTRIBUTES => self::IGNORED_ATTRIBUTES,
            ]
        );
    }

    #[Route('/settlement-concepts/{settlementConceptId<\d{1,10}>}', name: 'get_settlement_concept_by_id', methods: ['GET'], format: 'json')]
    public function getSettlementConceptById(int $settlementConceptId, UserInterface $user): JsonResponse
    {
        /** @var SettlementConceptAccountUser $user */
        $settlementConcept = $this->settlementConceptService->fetchSettlementConceptByIdAndSettlementConceptAccountUser(
            id: $settlementConceptId,
            settlementConceptAccountUser: $user
        );

        if ($settlementConcept === null) {
            throw new NotFoundHttpException();
        }

        return $this->json(
            data: $settlementConcept,
            context: [
                AbstractNormalizer::ATTRIBUTES         => self::ATTRIBUTES,
                AbstractNormalizer::IGNORED_ATTRIBUTES => self::IGNORED_ATTRIBUTES,
            ]
        );
    }

    #[Route('/settlement-concepts/{settlementConceptId<\d{1,10}>}', name: 'put_settlement_concept', methods: ['PUT'], format: 'json')]
    public function putSettlementConcept(int $settlementConceptId, Request $request, UserInterface $user): JsonResponse
    {
        /** @var SettlementConceptAccountUser $user */
        $settlementConceptData = json_decode(json: $request->getContent(), associative: false);

        $settlementConcept = $this->settlementConceptService->fetchSettlementConceptByIdAndSettlementConceptAccountUser(
            id: $settlementConceptId,
            settlementConceptAccountUser: $user
        );

        if ($settlementConcept === null) {
            throw new NotFoundHttpException();
        }

        $settlementConceptPutRequest = SettlementConceptRequest::createFromJsonContent(jsonContent: $settlementConceptData);

        $this->settlementConceptService->updateSettlementConceptWithSettlementConceptRequest(
            settlementConcept: $settlementConcept,
            settlementConceptRequest: $settlementConceptPutRequest
        );

        $this->entityManager->flush();

        return $this->json(
            data: $settlementConcept,
            context: [
                AbstractNormalizer::ATTRIBUTES         => self::ATTRIBUTES,
                AbstractNormalizer::IGNORED_ATTRIBUTES => self::IGNORED_ATTRIBUTES,
            ]
        );
    }

    #[Route('/settlement-concepts/{settlementConceptId<\d{1,10}>}', name: 'delete_settlement_concept', methods: ['DELETE'], format: 'json')]
    public function deleteSettlementConcept(int $settlementConceptId, UserInterface $user): Response
    {
        /** @var SettlementConceptAccountUser $user */
        $settlementConcept = $this->settlementConceptService->fetchSettlementConceptByIdAndSettlementConceptAccountUser(
            id: $settlementConceptId,
            settlementConceptAccountUser: $user
        );

        if ($settlementConcept === null) {
            throw new NotFoundHttpException();
        }

        $this->settlementConceptDeletionService->deleteSettlementConcept(settlementConcept: $settlementConcept);

        return new Response(content: null);
    }

    #[Route('/settlement-concepts/{settlementConceptId<\d{1,10}>}/results', name: 'get_settlement_concept_matchings_for_settlement_concept', methods: ['GET'], format: 'json')]
    public function getSettlementConceptMatchingsForSettlementConcept(int $settlementConceptId, UserInterface $user): JsonResponse
    {
        /** @var SettlementConceptAccountUser $user */
        $settlementConcept = $this->settlementConceptService->fetchSettlementConceptByIdAndSettlementConceptAccountUser(
            id: $settlementConceptId,
            settlementConceptAccountUser: $user
        );

        if ($settlementConcept === null) {
            throw new NotFoundHttpException();
        }

        $settlementConceptExposeForwardings = $this->settlementConceptService->fetchSettlementConceptExposeForwardingsBySettlementConcept(
            settlementConcept: $settlementConcept
        );

        $exposeDownloadUrls = [];

        foreach ($settlementConceptExposeForwardings as $settlementConceptExposeForwarding) {
            $exposeDownloadUrls[] = [
                'url' => $this->generateUrl(
                    route: 'settlement_concept_api_get_expose_file',
                    parameters: ['id' => $settlementConceptExposeForwarding->getId()],
                    referenceType: UrlGenerator::ABSOLUTE_URL
                ),
            ];
        }

        return $this->json(data: $exposeDownloadUrls);
    }

    #[Route('/exposes/{id<\d{1,10}>}', name: 'get_expose_file')]
    public function getExposeFile(int $id, UserInterface $user): Response
    {
        /** @var SettlementConceptAccountUser $user */
        $settlementConceptExposeForwarding = $this->settlementConceptService->fetchSettlementConceptExposeForwardingByIdAndSettlementConceptAccountUser(
            id: $id,
            settlementConceptAccountUser: $user
        );

        if ($settlementConceptExposeForwarding === null) {
            throw new NotFoundHttpException();
        }

        $buildingUnit = $settlementConceptExposeForwarding->getBuildingUnit();

        $pdfContent = $this->pdfExposeService->generateBuildingUnitPdfExpose(
            buildingUnit: $buildingUnit,
            accountUser: $settlementConceptExposeForwarding->getCreatedByAccountUser()
        );

        return new Response(content: $pdfContent, headers: ['Content-Type' => 'application/pdf']);
    }
}
