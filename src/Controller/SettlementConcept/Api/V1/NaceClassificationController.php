<?php

declare(strict_types=1);

namespace App\Controller\SettlementConcept\Api\V1;

use App\Domain\Classification\ClassificationService;
use App\Domain\Entity\Classification\NaceClassificationLevel;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

#[IsGranted('ROLE_SETTLEMENT_CONCEPT_API_USER')]
#[Route('settlement-concept/api/v1', name: 'settlement_concept_api_')]
class NaceClassificationController extends AbstractController
{
    private const NACE_CLASSIFICATION_ATTRIBUTES = [
        'id',
        'name',
        'level',
        'code',
    ];

    public function __construct(
        private readonly ClassificationService $classificationService
    ) {
    }

    #[Route('/nace-classifications', name: 'get_nace_classifications', methods: ['GET'], format: 'json')]
    public function getNaceClassifications(Request $request): JsonResponse
    {
        $naceClassificationLevel = NaceClassificationLevel::tryFrom((int)$request->query->get('level'));

        if ($naceClassificationLevel === null) {
            throw new BadRequestHttpException();
        }

        return $this->json(
            data: $this->classificationService->fetchNaceClassificationsWithLevel(level: $naceClassificationLevel),
            context: [
                AbstractNormalizer::ATTRIBUTES => self::NACE_CLASSIFICATION_ATTRIBUTES,
            ]
        );
    }

    #[Route('/nace-classifications/{naceClassificationId<\d{1,10}>}', name: 'get_nace_classification', methods: ['GET'], format: 'json')]
    public function getNaceClassification(int $naceClassificationId): JsonResponse
    {
        $naceClassification = $this->classificationService->fetchNaceClassificationById(naceClassificationId: $naceClassificationId);

        if ($naceClassification === null) {
            throw new NotFoundHttpException();
        }

        return $this->json(
            data: $naceClassification,
            context: [
                AbstractNormalizer::ATTRIBUTES => self::NACE_CLASSIFICATION_ATTRIBUTES,
            ]
        );
    }

    #[Route('/nace-classifications/{naceClassificationId<\d{1,10}>}/nace-classifications', name: 'get_nace_classifications_form_parent_nace_classification', methods: ['GET'], format: 'json')]
    public function getNaceClassificationsFromParentNaceClassification(int $naceClassificationId): JsonResponse
    {
        $naceClassification = $this->classificationService->fetchNaceClassificationById(naceClassificationId: $naceClassificationId);

        if ($naceClassification === null) {
            throw new NotFoundHttpException();
        }

        return $this->json(
            data: $this->classificationService->fetchNaceClassificationChildrens(parentNaceClassification: $naceClassification),
            context: [
                AbstractNormalizer::ATTRIBUTES => self::NACE_CLASSIFICATION_ATTRIBUTES,
            ]
        );
    }
}
