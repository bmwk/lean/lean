<?php

declare(strict_types=1);

namespace App\Controller\Report\Insights;

use App\Domain\Classification\ClassificationService;
use App\Domain\Entity\PlaceRelationType;
use App\Domain\Entity\PlaceType;
use App\Domain\Entity\Report\PropertyVacancy\PropertyVacancyDataFilter;
use App\Domain\Location\LocationService;
use App\Domain\Report\GeneralStatistics\GeneralStatisticsService;
use App\Domain\Report\PropertyVacancy\PropertyVacancyDataService;
use App\Domain\Report\SettlementInformationStatistics\SettlementInformationStatisticsService;
use App\Form\Type\Report\Report\PropertyVacancyDataFilterType;
use phpDocumentor\Reflection\Types\Integer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Security("is_granted('ROLE_USER') or is_granted('ROLE_VIEWER') or is_granted('ROLE_RESTRICTED_VIEWER')")]
#[Route('/reports-und-berichte/insights', name: 'report_insights_')]
class InsightsController extends AbstractController
{
    private const ACTIVE_NAV_GROUP = 'report';
    private const ACTIVE_NAV_ITEM = self::ACTIVE_NAV_GROUP . '_insights';

    public function __construct(
        private readonly PropertyVacancyDataService $propertyVacancyDataService,
        private readonly GeneralStatisticsService $generalStatisticsService,
        private readonly SettlementInformationStatisticsService $settlementInformationStatisticsService,
        private readonly ClassificationService $classificationService,
        private readonly LocationService $locationService
    ) {
    }

    #[Route('/leerstandsdaten', name: 'property_vacancy_data', methods: ['GET', 'POST'])]
    public function propertyVacancyData(Request $request, UserInterface $user): Response
    {
        $account = $user->getAccount();

        $generalPropertyVacancyData = $this->propertyVacancyDataService->fetchGeneralPropertyVacancyDataByAccount(
            account: $account,
            withFromSubAccounts: true
        );

        $propertyVacancyDataFilter = new PropertyVacancyDataFilter();

        $places = $this->locationService->fetchRecursivePlacesByPlacesAndPlaceRelationTypeAndPlaceType(
            places: [$account->getAssignedPlace()],
            placeTypes: $this->locationService->getCommunePlaceTypes(),
            placeRelationType: PlaceRelationType::CHILD
        );

        $propertyVacancyDataFilterForm = $this->createForm(
            type: PropertyVacancyDataFilterType::class,
            data: $propertyVacancyDataFilter,
            options: ['places' => $places]
        );

        $individualPropertyVacancyData = null;

        $propertyVacancyDataFilterForm->add(child: 'apply', type: SubmitType::class);

        $propertyVacancyDataFilterForm->handleRequest(request: $request);

        if ($propertyVacancyDataFilterForm->isSubmitted() && $propertyVacancyDataFilterForm->isValid()) {
            $industryClassificationForm = $propertyVacancyDataFilterForm->get('industryClassification');

            $industryClassificationId = (int)$industryClassificationForm->get('industryClassificationId')->getData();

            $industryClassification = $this->classificationService->fetchIndustryClassificationById(id: $industryClassificationId);

            $individualPropertyVacancyData = $this->propertyVacancyDataService->fetchIndividualPropertyDataByFilterAndAccount(
                account: $account,
                withFromSubAccounts: true,
                propertyVacancyDataFilter: $propertyVacancyDataFilterForm->getData(),
                industryClassification: $industryClassification

            );
        }

        return $this->render(view: 'report/insights/property_vacancy_data.html.twig', parameters: [
            'generalPropertyVacancyData'    => $generalPropertyVacancyData,
            'individualPropertyVacancyData' => $individualPropertyVacancyData,
            'propertyVacancyDataFilterForm' => $propertyVacancyDataFilterForm,
            'pageTitle'                     => 'Leerstandsdaten',
            'activeNavGroup'                => self::ACTIVE_NAV_GROUP,
            'activeNavItem'                 => self::ACTIVE_NAV_ITEM,
        ]);
    }

    #[Route('/ansiedlungsdaten', name: 'settlement_information', methods: ['GET'])]
    public function settlementInformation(UserInterface $user): Response
    {
        $account = $user->getAccount();

        $settlementStatistics = $this->settlementInformationStatisticsService->buildSettlementStatisticsByAccount(
            account: $account,
            withFromSubAccounts: false
        );

        return $this->render(view: 'report/insights/settlement_information.html.twig', parameters: [
            'pageTitle'            => 'Ansiedlungsdaten',
            'settlementStatistics' => $settlementStatistics,
            'activeNavGroup'       => self::ACTIVE_NAV_GROUP,
            'activeNavItem'        => self::ACTIVE_NAV_ITEM,
        ]);
    }

    #[Route('/lean-kennzahlen', name: 'general_statistics', methods: ['GET'])]
    public function generalStatistics(UserInterface $user): Response
    {
        $account = $user->getAccount();

        $userStatistics = $this->generalStatisticsService->buildUserStatisticsByAccount(
            account: $account,
            withFromSubAccounts: true
        );

        $personStatistics = $this->generalStatisticsService->buildPersonStatisticsByAccount(
            account: $account,
            withFromSubAccounts: false
        );

        return $this->render(view: 'report/insights/general_statistics.html.twig', parameters: [
            'pageTitle'        => 'Allgemeine Statistiken',
            'userStatistics'   => $userStatistics,
            'personStatistics' => $personStatistics,
            'activeNavGroup'   => self::ACTIVE_NAV_GROUP,
            'activeNavItem'    => self::ACTIVE_NAV_ITEM,
        ]);
    }

    #[Route('/impulse', name: 'impulses', methods: ['GET'])]
    public function impulses(): Response
    {
        return $this->render(view: 'report/insights/impulses.html.twig', parameters: [
            'pageTitle'      => 'Impulse',
            'activeNavGroup' => self::ACTIVE_NAV_GROUP,
            'activeNavItem'  => self::ACTIVE_NAV_ITEM,
        ]);
    }
}
