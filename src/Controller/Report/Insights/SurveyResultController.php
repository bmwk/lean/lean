<?php

declare(strict_types=1);

namespace App\Controller\Report\Insights;

use App\Domain\SurveyResult\SurveyResultService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Security("is_granted('ROLE_USER') or is_granted('ROLE_VIEWER') or is_granted('ROLE_RESTRICTED_VIEWER')")]
#[Route('/reports-und-berichte/berichte/umfrageergebnisse', name: 'report_insights_survey_result_')]
class SurveyResultController extends AbstractController
{
    private const ACTIVE_NAV_GROUP = 'report';
    private const ACTIVE_NAV_ITEM = self::ACTIVE_NAV_GROUP . '_insights';

    public function __construct(
        private readonly SurveyResultService $surveyResultService
    ) {
    }

    #[Route('/uebersicht', name: 'overview', methods: ['GET'])]
    public function overview(UserInterface $user): Response
    {
        return $this->render(view: 'report/insights/survey_result/overview.html.twig', parameters: [
            'surveyResults'  =>  $this->surveyResultService->fetchSurveyResultsByAccount(account: $user->getAccount()),
            'pageTitle'      => 'Umfrageergebnisse',
            'activeNavGroup' => self::ACTIVE_NAV_GROUP,
            'activeNavItem'  => self::ACTIVE_NAV_ITEM,
        ]);
    }

    #[Route('/{surveyResultId<\d{1,10}>}', name: 'survey_result', methods: ['GET'])]
    public function surveyResult(int $surveyResultId, UserInterface $user): Response
    {
        $surveyResult = $this->surveyResultService->fetchSurveyResultByIdAndAccount(id: $surveyResultId, account: $user->getAccount());

        if ($surveyResult === null) {
            throw new NotFoundHttpException();
        }

        return $this->render(view: 'report/insights/survey_result/survey_result.html.twig', parameters: [
            'surveyResult'   => $surveyResult,
            'pageTitle'      => 'Umfrageergebnisse',
            'activeNavGroup' => self::ACTIVE_NAV_GROUP,
            'activeNavItem'  => self::ACTIVE_NAV_ITEM,
        ]);
    }
}
