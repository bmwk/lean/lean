<?php

declare(strict_types=1);

namespace App\Controller\Report;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Security("is_granted('ROLE_ACCOUNT_ADMIN') or is_granted('ROLE_EARLY_WARNING_SYSTEM_VIEWER')")]
#[Route('/reports-und-berichte', name: 'report_')]
class EarlyWarningSystemController extends AbstractController
{
    public function __construct(
        private readonly string $projectDir
    ) {
    }

    #[Route('/fruehwarnsystem', name: 'early_warning_system', methods: ['GET'])]
    public function earlyWarningSystem(): Response
    {
        return $this->render(view: 'report/early_warning_system/early_warning_system.html.twig', parameters: [
            'pageTitle'      => 'Frühwarnsystem',
            'activeNavGroup' => 'report',
            'activeNavItem'  => 'report_early_warning_system',
        ]);
    }

    #[Route('/fruehwarnsystem-hintergrundinformationen', name: 'early_warning_system_information_document', methods: ['GET'])]
    public function earlyWarningSystemInformationDocument(): Response
    {
        $file = new \SplFileInfo(filename: $this->projectDir . '/assets/files/documentation/early_warning_system/early_warning_system_information.pdf');
        $fileName = 'hintergrundinformationen_fruehwarnsystem.pdf';

        return new BinaryFileResponse(
            file: $file->getRealPath(),
            headers: [
                'Content-Disposition' => HeaderUtils::makeDisposition(
                    disposition: HeaderUtils::DISPOSITION_ATTACHMENT,
                    filename: $fileName,
                    filenameFallback: mb_convert_encoding(string: $fileName, to_encoding: 'ascii', from_encoding: 'utf8')
                ),
            ]
        );
    }
}
