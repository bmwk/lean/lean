<?php

declare(strict_types=1);

namespace App\Controller\Report;

use App\Domain\InterfaceConfiguration\InterfaceConfigurationService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Security("is_granted('ROLE_USER') or is_granted('ROLE_VIEWER') or is_granted('ROLE_RESTRICTED_VIEWER')")]
#[Route('/reports-und-berichte', name: 'report_')]
class PedestrianFrequencyController extends AbstractController
{
    public function __construct(
        private readonly InterfaceConfigurationService $interfaceConfigurationService
    ) {
    }

    #[Route('/passantenfrequenzen', name: 'pedestrian_frequency', methods: ['GET'])]
    public function pedestrianFrequency(UserInterface $user): Response
    {
        if ($this->interfaceConfigurationService->hasHystreetConfiguration(account: $user->getAccount()) === false) {
            throw new NotFoundHttpException();
        }

        return $this->render(view: 'report/pedestrian_frequency/pedestrian_frequency.html.twig', parameters: [
            'pageTitle'      => 'Passantenfrequenzen',
            'activeNavGroup' => 'report',
            'activeNavItem'  => 'report_pedestrian_frequency',
        ]);
    }
}
