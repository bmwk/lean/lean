<?php

declare(strict_types=1);

namespace App\Controller\Report;

use App\Domain\LocationIndicators\LocationIndicatorsService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Security("is_granted('ROLE_USER') or is_granted('ROLE_VIEWER') or is_granted('ROLE_RESTRICTED_VIEWER')")]
#[Route('/reports-und-berichte', name: 'report_')]
class LocationIndicatorsController extends AbstractController
{
    public function __construct(
        private readonly LocationIndicatorsService $locationIndicatorsService
    ) {
    }

    #[Route('/standortindikatoren', name: 'location_indicators', methods: ['GET'])]
    public function locationIndicators(UserInterface $user): Response
    {
        $account = $user->getAccount();

        $wegweiserKommunePlaceMapping = $this->locationIndicatorsService->fetchWegweiserKommunePlaceMappingByPlace(
            account: $account,
            place: $account->getAssignedPlace()
        );

        if ($wegweiserKommunePlaceMapping === null) {
            throw new NotFoundHttpException();
        }

        return $this->render(view: 'report/location_indicators/location_indicators.html.twig', parameters: [
            'wegweiserKommunePlaceMapping' => $wegweiserKommunePlaceMapping,
            'pageTitle'                    => 'Standortindikatoren',
            'activeNavGroup'               => 'report',
            'activeNavItem'                => 'report_location_indicators',
        ]);
    }
}
