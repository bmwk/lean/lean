<?php

declare(strict_types=1);

namespace App\Controller\Report;

use App\Domain\InterfaceConfiguration\InterfaceConfigurationService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Security("is_granted('ROLE_USER') or is_granted('ROLE_VIEWER') or is_granted('ROLE_RESTRICTED_VIEWER')")]
#[Route('/reports-und-berichte', name: 'report_')]
class CityGoalsController extends AbstractController
{
    public function __construct(
        private readonly InterfaceConfigurationService $interfaceConfigurationService
    ) {
    }

    #[Route('/zielbild', name: 'city_goal', methods: ['GET'])]
    public function cityGoal(UserInterface $user): Response
    {
        if ($this->interfaceConfigurationService->hasZielbildcheckConfiguration(account: $user->getAccount()) === false) {
            throw new NotFoundHttpException();
        }

        return $this->render(view: 'report/city_goal/city_goal.html.twig', parameters: [
            'pageTitle'      => 'Zielbild',
            'activeNavGroup' => 'report',
            'activeNavItem'  => 'report_city_goal',
        ]);
    }
}
