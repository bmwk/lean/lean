<?php

declare(strict_types=1);

namespace App\Controller\ApiGateway;

use App\Domain\InterfaceConfiguration\InterfaceConfigurationService;
use App\Hystreet\HystreetService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\SerializerInterface;

#[Security("is_granted('ROLE_USER') or is_granted('ROLE_VIEWER') or is_granted('ROLE_RESTRICTED_VIEWER')")]
#[Route('/api-gateway/hystreet', name: 'api_gateway_hystreet_')]
class HystreetApiGatewayController extends AbstractController
{
    public function __construct(
        private readonly HystreetService $hystreetService,
        private readonly InterfaceConfigurationService $interfaceConfigurationService,
        private readonly SerializerInterface $serializer
    ) {
    }

    #[Route('/locations/{id<\d{1,10}>}', name: 'get_location', methods: ['GET'], format: 'json')]
    public function getPedestrianFrequencyMeasurement(int $id, Request $request, UserInterface $user): JsonResponse
    {
        $account = $user->getAccount();

        $hystreetConfiguration = $this->interfaceConfigurationService->fetchHystreetConfigurationByAccount(account: $account);

        if ($hystreetConfiguration === null) {
            throw new AccessDeniedHttpException();
        }

        $from = null;
        $to = null;

        if ($request->query->get('from') !== null) {
            $from = \DateTime::createFromFormat(
                format: 'Y-m-d\TH:i:s',
                datetime: $request->query->get('from'),
                timezone: new \DateTimeZone('+0200')
            );

            if ($from === false) {
                throw new BadRequestHttpException();
            }
        }

        if ($request->query->get('to') !== null) {
            $to = \DateTime::createFromFormat(
                format: 'Y-m-d\TH:i:s',
                datetime: $request->query->get('to'),
                timezone: new \DateTimeZone('+0200')
            );

            if ($to === false) {
                throw new BadRequestHttpException();
            }
        }

        return new JsonResponse(
            data: $this->serializer->serialize(
                data: $this->hystreetService->getLocation(
                    apiAccessToken: $hystreetConfiguration->getAccessToken(),
                    id: $id,
                    from: $from,
                    to: $to
                ),
                format: 'json'
            ),
            json: true
        );
    }

    #[Route('/locations', name: 'get_locations', methods: ['GET'], format: 'json')]
    public function getLocations(UserInterface $user): JsonResponse
    {
        $account = $user->getAccount();

        $hystreetConfiguration = $this->interfaceConfigurationService->fetchHystreetConfigurationByAccount(account: $account);

        if ($hystreetConfiguration === null) {
            throw new AccessDeniedHttpException();
        }

        return new JsonResponse(
            data: $this->serializer->serialize(
                data: $this->hystreetService->getLocations(apiAccessToken: $hystreetConfiguration->getAccessToken()),
                format: 'json'
            ),
            json: true
        );
    }
}
