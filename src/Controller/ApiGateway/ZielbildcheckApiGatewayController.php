<?php

declare(strict_types=1);

namespace App\Controller\ApiGateway;

use App\Domain\InterfaceConfiguration\InterfaceConfigurationService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

#[Security("is_granted('ROLE_USER') or is_granted('ROLE_VIEWER') or is_granted('ROLE_RESTRICTED_VIEWER')")]
#[Route('/api-gateway/zielbildcheck', name: 'api_gateway_zielbildcheck_')]
class ZielbildcheckApiGatewayController extends AbstractController
{
    public function __construct(
        private readonly string $zielbildcheckApiUrl,
        private readonly InterfaceConfigurationService $interfaceConfigurationService,
        private readonly HttpClientInterface $httpClient
    ) {
    }

    #[Route('/me', name: 'get_me', methods: ['GET'], format: 'json')]
    public function getMe(UserInterface $user): JsonResponse
    {
        $account = $user->getAccount();

        $zielbildcheckConfiguration = $this->interfaceConfigurationService->fetchZielbildcheckConfigurationByAccount(account: $account);

        if ($zielbildcheckConfiguration === null) {
            throw new AccessDeniedHttpException();
        }

        $response = $this->httpClient->request(method: 'GET', url: $this->zielbildcheckApiUrl . '/me', options: [
            'headers' => [
                'Content-Type: ' . 'application/json',
                'X-Api-Token: ' . $zielbildcheckConfiguration->getAccessToken(),
            ],
        ]);

        return new JsonResponse(data: $response->getContent(), json: true);
    }

    #[Route('/project/locations', name: 'get_project_locations', methods: ['GET'], format: 'json')]
    public function getProjectLocations(UserInterface $user): JsonResponse
    {
        $account = $user->getAccount();

        $zielbildcheckConfiguration = $this->interfaceConfigurationService->fetchZielbildcheckConfigurationByAccount(account: $account);

        if ($zielbildcheckConfiguration === null) {
            throw new AccessDeniedHttpException();
        }

        $response = $this->httpClient->request(method: 'GET', url: $this->zielbildcheckApiUrl . '/project/locations', options: [
            'headers' => [
                'Content-Type: ' . 'application/json',
                'X-Api-Token: ' . $zielbildcheckConfiguration->getAccessToken(),
            ],
        ]);

        return new JsonResponse(data: $response->getContent(), json: true);
    }

    #[Route('/project/surveys/finished', name: 'get_project_surveys_finished', methods: ['GET'], format: 'json')]
    public function getProjectSurveysFinished(UserInterface $user): JsonResponse
    {
        $account = $user->getAccount();

        $zielbildcheckConfiguration = $this->interfaceConfigurationService->fetchZielbildcheckConfigurationByAccount(account: $account);

        if ($zielbildcheckConfiguration === null) {
            throw new AccessDeniedHttpException();
        }

        $response = $this->httpClient->request(method: 'GET', url: $this->zielbildcheckApiUrl . '/project/surveys/finished', options: [
            'headers' => [
                'Content-Type: ' . 'application/json',
                'X-Api-Token: ' . $zielbildcheckConfiguration->getAccessToken(),
            ],
        ]);

        return new JsonResponse(data: $response->getContent(), json: true);
    }

    #[Route('/project/surveys/{surveyId<[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}>}/questions', name: 'get_project_survey_questions', methods: ['GET'], format: 'json')]
    public function getProjectSurveyQuestions(string $surveyId, UserInterface $user): JsonResponse
    {
        $account = $user->getAccount();

        $zielbildcheckConfiguration = $this->interfaceConfigurationService->fetchZielbildcheckConfigurationByAccount(account: $account);

        if ($zielbildcheckConfiguration === null) {
            throw new AccessDeniedHttpException();
        }

        $response = $this->httpClient->request(method: 'GET', url: $this->zielbildcheckApiUrl . '/project/surveys/' . $surveyId . '/questions', options: [
            'headers' => [
                'Content-Type: ' . 'application/json',
                'X-Api-Token: ' . $zielbildcheckConfiguration->getAccessToken(),
            ],
        ]);

        return new JsonResponse(data: $response->getContent(), json: true);
    }
}
