<?php

declare(strict_types=1);

namespace App\TaodEarlyWarningSystem\Entity\JsonRepresentation;

class Address
{
    private Place $place;

    private string $postalCode;

    private string $streetName;

    private string $houseNumber;

    private ?Place $boroughPlace = null;

    private ?Place $quarterPlace = null;

    private ?Place $neighbourhoodPlace = null;

    private ?string $additionalAddressInformation = null;

    public function getPlace(): Place
    {
        return $this->place;
    }

    public function setPlace(Place $place): self
    {
        $this->place = $place;

        return $this;
    }

    public function getPostalCode(): string
    {
        return $this->postalCode;
    }

    public function setPostalCode(string $postalCode): self
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    public function getStreetName(): string
    {
        return $this->streetName;
    }

    public function setStreetName(string $streetName): self
    {
        $this->streetName = $streetName;

        return $this;
    }

    public function getHouseNumber(): string
    {
        return $this->houseNumber;
    }

    public function setHouseNumber(string $houseNumber): self
    {
        $this->houseNumber = $houseNumber;

        return $this;
    }

    public function getBoroughPlace(): ?Place
    {
        return $this->boroughPlace;
    }

    public function setBoroughPlace(?Place $boroughPlace): self
    {
        $this->boroughPlace = $boroughPlace;

        return $this;
    }

    public function getQuarterPlace(): ?Place
    {
        return $this->quarterPlace;
    }

    public function setQuarterPlace(?Place $quarterPlace): self
    {
        $this->quarterPlace = $quarterPlace;

        return $this;
    }

    public function getNeighbourhoodPlace(): ?Place
    {
        return $this->neighbourhoodPlace;
    }

    public function setNeighbourhoodPlace(?Place $neighbourhoodPlace): self
    {
        $this->neighbourhoodPlace = $neighbourhoodPlace;

        return $this;
    }

    public function getAdditionalAddressInformation(): ?string
    {
        return $this->additionalAddressInformation;
    }

    public function setAdditionalAddressInformation(?string $additionalAddressInformation): self
    {
        $this->additionalAddressInformation = $additionalAddressInformation;

        return $this;
    }
}
