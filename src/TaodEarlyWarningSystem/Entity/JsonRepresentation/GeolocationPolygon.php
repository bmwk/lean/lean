<?php

declare(strict_types=1);

namespace App\TaodEarlyWarningSystem\Entity\JsonRepresentation;

use CrEOF\Spatial\PHP\Types\Geometry\Polygon;

class GeolocationPolygon
{
    private Polygon $polygon;

    public function getPolygon(): Polygon
    {
        return $this->polygon;
    }

    public function setPolygon(Polygon $polygon): self
    {
        $this->polygon = $polygon;

        return $this;
    }
}
