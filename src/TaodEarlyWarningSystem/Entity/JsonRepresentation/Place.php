<?php

declare(strict_types=1);

namespace App\TaodEarlyWarningSystem\Entity\JsonRepresentation;

use Symfony\Component\Uid\Uuid;

class Place
{
    private Uuid $uuid;

    private string $placeName;

    private ?string $placeShortName = null;

    private string $placeType;

    public function getUuid(): Uuid
    {
        return $this->uuid;
    }

    public function setUuid(Uuid $uuid): self
    {
        $this->uuid = $uuid;

        return $this;
    }

    public function getPlaceName(): string
    {
        return $this->placeName;
    }

    public function setPlaceName(string $placeName): self
    {
        $this->placeName = $placeName;

        return $this;
    }

    public function getPlaceShortName(): ?string
    {
        return $this->placeShortName;
    }

    public function setPlaceShortName(?string $placeShortName): self
    {
        $this->placeShortName = $placeShortName;

        return $this;
    }

    public function getPlaceType(): string
    {
        return $this->placeType;
    }

    public function setPlaceType(string $placeType): self
    {
        $this->placeType = $placeType;

        return $this;
    }
}
