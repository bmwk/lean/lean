<?php

declare(strict_types=1);

namespace App\TaodEarlyWarningSystem\Entity\JsonRepresentation;

class NaceClassification
{
    private string $name;

    private string $code;

    private string $level;

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getLevel(): string
    {
        return $this->level;
    }

    public function setLevel(string $level): self
    {
        $this->level = $level;

        return $this;
    }
}
