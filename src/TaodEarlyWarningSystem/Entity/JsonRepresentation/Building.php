<?php

declare(strict_types=1);

namespace App\TaodEarlyWarningSystem\Entity\JsonRepresentation;

class Building
{
    private ?string $locationCategory = null;

    private ?string $buildingStandard = null;

    private ?int $constructionYear = null;

    private ?int $numberOfFloors = null;

    private ?array $internetConnectionTypes = null;

    private ?string $buildingType = null;

    public function getLocationCategory(): ?string
    {
        return $this->locationCategory;
    }

    public function setLocationCategory(?string $locationCategory): self
    {
        $this->locationCategory = $locationCategory;

        return $this;
    }

    public function getBuildingStandard(): ?string
    {
        return $this->buildingStandard;
    }

    public function setBuildingStandard(?string $buildingStandard): self
    {
        $this->buildingStandard = $buildingStandard;

        return $this;
    }

    public function getConstructionYear(): ?int
    {
        return $this->constructionYear;
    }

    public function setConstructionYear(?int $constructionYear): self
    {
        $this->constructionYear = $constructionYear;

        return $this;
    }

    public function getNumberOfFloors(): ?int
    {
        return $this->numberOfFloors;
    }

    public function setNumberOfFloors(?int $numberOfFloors): self
    {
        $this->numberOfFloors = $numberOfFloors;

        return $this;
    }

    public function getInternetConnectionTypes(): ?array
    {
        return $this->internetConnectionTypes;
    }

    public function setInternetConnectionTypes(?array $internetConnectionTypes): self
    {
        $this->internetConnectionTypes = $internetConnectionTypes;

        return $this;
    }

    public function getBuildingType(): ?string
    {
        return $this->buildingType;
    }

    public function setBuildingType(?string $buildingType): self
    {
        $this->buildingType = $buildingType;

        return $this;
    }
}
