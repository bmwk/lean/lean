<?php

declare(strict_types=1);

namespace App\TaodEarlyWarningSystem\Entity\JsonRepresentation;

use CrEOF\Spatial\PHP\Types\Geometry\Point;

class GeolocationPoint
{
    private Point $point;

    public function getPoint(): Point
    {
        return $this->point;
    }

    public function setPoint(Point $point): self
    {
        $this->point = $point;

        return $this;
    }
}
