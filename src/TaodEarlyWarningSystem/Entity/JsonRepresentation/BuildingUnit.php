<?php

declare(strict_types=1);

namespace App\TaodEarlyWarningSystem\Entity\JsonRepresentation;

class BuildingUnit
{
    private int $id;

    private ?float $areaSize = null;

    private ?float $entranceDoorWidth = null;

    private ?string $barrierFreeAccess = null;

    private ?bool $objectForeclosed = null;

    /**
     * @var string[]
     */
    private ?array $locationFactors = null;

    private Address $address;

    private ?PropertySpacesData $propertySpacesData = null;

    private ?GeolocationPoint $geolocationPoint = null;

    private bool $objectIsEmpty;

    private ?\DateTime $objectIsEmptySince = null;

    private bool $objectBecomesEmpty;

    private ?\DateTime $objectBecomesEmptyFrom = null;

    private bool $reuseAgreed;

    private ?\DateTime $reuseFrom = null;

    private ?IndustryClassification $currentUsageIndustryClassification = null;

    private ?IndustryClassification $pastUsageIndustryClassification = null;

    private ?IndustryClassification $futureUsageIndustryClassification = null;

    private ?int $numberOfParkingLots = null;

    private ?array $inFloors = null;

    private ?bool $interimUsePossible = null;

    private ?array $interimUseTypes = null;

    private ?float $shopWindowFrontWidth = null;

    private ?bool $showroomAvailable = null;

    private ?float $shopWidth = null;

    private ?bool $groundLevelSalesArea = null;

    private ?bool $keyProperty = null;

    private Building $building;

    private int $accountId;

    /**
     * @var PropertyUser[]
     */
    private array $propertyUsers;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getAreaSize(): ?float
    {
        return $this->areaSize;
    }

    public function setAreaSize(?float $areaSize): self
    {
        $this->areaSize = $areaSize;

        return $this;
    }

    public function getEntranceDoorWidth(): ?float
    {
        return $this->entranceDoorWidth;
    }

    public function setEntranceDoorWidth(?float $entranceDoorWidth): self
    {
        $this->entranceDoorWidth = $entranceDoorWidth;

        return $this;
    }

    public function getBarrierFreeAccess(): ?string
    {
        return $this->barrierFreeAccess;
    }

    public function setBarrierFreeAccess(?string $barrierFreeAccess): self
    {
        $this->barrierFreeAccess = $barrierFreeAccess;

        return $this;
    }

    public function getObjectForeclosed(): ?bool
    {
        return $this->objectForeclosed;
    }

    public function setObjectForeclosed(?bool $objectForeclosed): self
    {
        $this->objectForeclosed = $objectForeclosed;

        return $this;
    }

    /**
     * @return string[]|null
     */
    public function getLocationFactors(): ?array
    {
        return $this->locationFactors;
    }

    /**
     * @param string[]|null $locationFactors
     */
    public function setLocationFactors(?array $locationFactors): self
    {
        $this->locationFactors = $locationFactors;

        return $this;
    }

    public function getAddress(): Address
    {
        return $this->address;
    }

    public function setAddress(Address $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getPropertySpacesData(): ?PropertySpacesData
    {
        return $this->propertySpacesData;
    }

    public function setPropertySpacesData(?PropertySpacesData $propertySpacesData): self
    {
        $this->propertySpacesData = $propertySpacesData;

        return $this;
    }

    public function getGeolocationPoint(): ?GeolocationPoint
    {
        return $this->geolocationPoint;
    }

    public function setGeolocationPoint(?GeolocationPoint $geolocationPoint): self
    {
        $this->geolocationPoint = $geolocationPoint;

        return $this;
    }

    public function isObjectIsEmpty(): bool
    {
        return $this->objectIsEmpty;
    }

    public function setObjectIsEmpty(bool $objectIsEmpty): self
    {
        $this->objectIsEmpty = $objectIsEmpty;

        return $this;
    }

    public function getObjectIsEmptySince(): ?\DateTime
    {
        return $this->objectIsEmptySince;
    }

    public function setObjectIsEmptySince(?\DateTime $objectIsEmptySince): self
    {
        $this->objectIsEmptySince = $objectIsEmptySince;

        return $this;
    }

    public function isObjectBecomesEmpty(): bool
    {
        return $this->objectBecomesEmpty;
    }

    public function setObjectBecomesEmpty(bool $objectBecomesEmpty): self
    {
        $this->objectBecomesEmpty = $objectBecomesEmpty;

        return $this;
    }

    public function getObjectBecomesEmptyFrom(): ?\DateTime
    {
        return $this->objectBecomesEmptyFrom;
    }

    public function setObjectBecomesEmptyFrom(?\DateTime $objectBecomesEmptyFrom): self
    {
        $this->objectBecomesEmptyFrom = $objectBecomesEmptyFrom;

        return $this;
    }

    public function isReuseAgreed(): bool
    {
        return $this->reuseAgreed;
    }

    public function setReuseAgreed(bool $reuseAgreed): self
    {
        $this->reuseAgreed = $reuseAgreed;

        return $this;
    }

    public function getReuseFrom(): ?\DateTime
    {
        return $this->reuseFrom;
    }

    public function setReuseFrom(?\DateTime $reuseFrom): self
    {
        $this->reuseFrom = $reuseFrom;

        return $this;
    }

    public function getCurrentUsageIndustryClassification(): ?IndustryClassification
    {
        return $this->currentUsageIndustryClassification;
    }

    public function setCurrentUsageIndustryClassification(?IndustryClassification $currentUsageIndustryClassification): self
    {
        $this->currentUsageIndustryClassification = $currentUsageIndustryClassification;

        return $this;
    }

    public function getPastUsageIndustryClassification(): ?IndustryClassification
    {
        return $this->pastUsageIndustryClassification;
    }

    public function setPastUsageIndustryClassification(?IndustryClassification $pastUsageIndustryClassification): self
    {
        $this->pastUsageIndustryClassification = $pastUsageIndustryClassification;

        return $this;
    }

    public function getFutureUsageIndustryClassification(): ?IndustryClassification
    {
        return $this->futureUsageIndustryClassification;
    }

    public function setFutureUsageIndustryClassification(?IndustryClassification $futureUsageIndustryClassification): self
    {
        $this->futureUsageIndustryClassification = $futureUsageIndustryClassification;

        return $this;
    }

    public function getNumberOfParkingLots(): ?int
    {
        return $this->numberOfParkingLots;
    }

    public function setNumberOfParkingLots(?int $numberOfParkingLots): self
    {
        $this->numberOfParkingLots = $numberOfParkingLots;

        return $this;
    }

    public function getInFloors(): ?array
    {
        return $this->inFloors;
    }

    public function setInFloors(?array $inFloors): self
    {
        $this->inFloors = $inFloors;

        return $this;
    }

    public function isInterimUsePossible(): ?bool
    {
        return $this->interimUsePossible;
    }

    public function setInterimUsePossible(?bool $interimUsePossible): self
    {
        $this->interimUsePossible = $interimUsePossible;

        return $this;
    }

    public function getInterimUseTypes(): ?array
    {
        return $this->interimUseTypes;
    }

    public function setInterimUseTypes(?array $interimUseTypes): self
    {
        $this->interimUseTypes = $interimUseTypes;

        return $this;
    }

    public function getShopWindowFrontWidth(): ?float
    {
        return $this->shopWindowFrontWidth;
    }

    public function setShopWindowFrontWidth(?float $shopWindowFrontWidth): self
    {
        $this->shopWindowFrontWidth = $shopWindowFrontWidth;

        return $this;
    }

    public function isShowroomAvailable(): ?bool
    {
        return $this->showroomAvailable;
    }

    public function setShowroomAvailable(?bool $showroomAvailable): self
    {
        $this->showroomAvailable = $showroomAvailable;

        return $this;
    }

    public function getShopWidth(): ?float
    {
        return $this->shopWidth;
    }

    public function setShopWidth(?float $shopWidth): self
    {
        $this->shopWidth = $shopWidth;

        return $this;
    }

    public function isGroundLevelSalesArea(): ?bool
    {
        return $this->groundLevelSalesArea;
    }

    public function setGroundLevelSalesArea(?bool $groundLevelSalesArea): self
    {
        $this->groundLevelSalesArea = $groundLevelSalesArea;

        return $this;
    }

    public function isKeyProperty(): ?bool
    {
        return $this->keyProperty;
    }

    public function setKeyProperty(?bool $keyProperty): self
    {
        $this->keyProperty = $keyProperty;

        return $this;
    }

    public function getBuilding(): Building
    {
        return $this->building;
    }

    public function setBuilding(Building $building): self
    {
        $this->building = $building;

        return $this;
    }

    /**
     * @return PropertyUser[]
     */
    public function getPropertyUsers(): array
    {
        return $this->propertyUsers;
    }

    /**
     * @param PropertyUser[] $propertyUsers
     */
    public function setPropertyUsers(array $propertyUsers): self
    {
        $this->propertyUsers = $propertyUsers;

        return $this;
    }

    public function getAccountId(): int
    {
        return $this->accountId;
    }

    public function setAccountId(int $accountId): self
    {
        $this->accountId = $accountId;
        return $this;
    }
}
