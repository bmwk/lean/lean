<?php

declare(strict_types=1);

namespace App\TaodEarlyWarningSystem\Entity\JsonRepresentation;

class IndustryClassification
{
    private string $name;

    private int $levelOne;

    private ?int $levelTwo = null;

    private ?int $levelThree = null;

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLevelOne(): int
    {
        return $this->levelOne;
    }

    public function setLevelOne(int $levelOne): self
    {
        $this->levelOne = $levelOne;

        return $this;
    }

    public function getLevelTwo(): ?int
    {
        return $this->levelTwo;
    }

    public function setLevelTwo(?int $levelTwo): self
    {
        $this->levelTwo = $levelTwo;

        return $this;
    }

    public function getLevelThree(): ?int
    {
        return $this->levelThree;
    }

    public function setLevelThree(?int $levelThree): self
    {
        $this->levelThree = $levelThree;

        return $this;
    }
}
