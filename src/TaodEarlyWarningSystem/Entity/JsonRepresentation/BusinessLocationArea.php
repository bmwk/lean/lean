<?php

declare(strict_types=1);

namespace App\TaodEarlyWarningSystem\Entity\JsonRepresentation;

class BusinessLocationArea
{
    private int $id;

    private string $locationCategory;

    private int $accountId;

    private GeolocationPolygon $geolocationPolygon;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getLocationCategory(): string
    {
        return $this->locationCategory;
    }

    public function setLocationCategory(string $locationCategory): self
    {
        $this->locationCategory = $locationCategory;

        return $this;
    }

    public function getGeolocationPolygon(): GeolocationPolygon
    {
        return $this->geolocationPolygon;
    }

    public function setGeolocationPolygon(GeolocationPolygon $geolocationPolygon): self
    {
        $this->geolocationPolygon = $geolocationPolygon;

        return $this;
    }

    public function getAccountId(): int
    {
        return $this->accountId;
    }

    public function setAccountId(int $accountId): self
    {
        $this->accountId = $accountId;

        return $this;
    }
}
