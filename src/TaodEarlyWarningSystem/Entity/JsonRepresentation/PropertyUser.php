<?php

declare(strict_types=1);

namespace App\TaodEarlyWarningSystem\Entity\JsonRepresentation;

class PropertyUser
{
    private Person $person;

    private string $propertyUserStatus;

    private ?\DateTime $endOfRental = null;

    private ?IndustryClassification $industryClassification = null;

    private ?NaceClassification $naceClassification = null;

    private ?string $openingHours = null;

    private ?string $rating = null;

    private ?string $website = null;

    public function getPerson(): Person
    {
        return $this->person;
    }

    public function setPerson(Person $person): self
    {
        $this->person = $person;

        return $this;
    }

    public function getPropertyUserStatus(): string
    {
        return $this->propertyUserStatus;
    }

    public function setPropertyUserStatus(string $propertyUserStatus): self
    {
        $this->propertyUserStatus = $propertyUserStatus;

        return $this;
    }

    public function getEndOfRental(): ?\DateTime
    {
        return $this->endOfRental;
    }

    public function setEndOfRental(?\DateTime $endOfRental): self
    {
        $this->endOfRental = $endOfRental;

        return $this;
    }

    public function getIndustryClassification(): ?IndustryClassification
    {
        return $this->industryClassification;
    }

    public function setIndustryClassification(?IndustryClassification $industryClassification): self
    {
        $this->industryClassification = $industryClassification;

        return $this;
    }

    public function getNaceClassification(): ?NaceClassification
    {
        return $this->naceClassification;
    }

    public function setNaceClassification(?NaceClassification $naceClassification): self
    {
        $this->naceClassification = $naceClassification;

        return $this;
    }

    public function getOpeningHours(): ?string
    {
        return $this->openingHours;
    }

    public function setOpeningHours(?string $openingHours): self
    {
        $this->openingHours = $openingHours;

        return $this;
    }

    public function getRating(): ?string
    {
        return $this->rating;
    }

    public function setRating(?string $rating): self
    {
        $this->rating = $rating;

        return $this;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    public function setWebsite(?string $website): self
    {
        $this->website = $website;

        return $this;
    }
}
