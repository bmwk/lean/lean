<?php

declare(strict_types=1);

namespace App\TaodEarlyWarningSystem\Entity\JsonRepresentation;

class PropertySpacesData
{
    private ?float $roomCount = null;

    private ?float $livingSpace = null;

    private ?float $usableSpace = null;

    private ?float $storeSpace = null;

    private ?float $storageSpace = null;

    private ?float $retailSpace = null;

    private ?float $openSpace = null;

    private ?float $officeSpace = null;

    private ?float $officePartSpace = null;

    private ?float $administrationSpace = null;

    private ?float $gastronomySpace = null;

    private ?float $productionSpace = null;

    private ?float $plotSize = null;

    private ?float $otherSpace = null;

    private ?float $balconyTurfSpace = null;

    private ?float $gardenSpace = null;

    private ?float $cellarSpace = null;

    private ?float $atticSpace = null;

    private ?float $heatableSurface = null;

    private ?float $rentableArea = null;

    private ?float $minimumSpaceForSeparation = null;

    private ?string $usableOutdoorAreaPossibility = null;

    private ?float $totalOutdoorArea = null;

    private ?float $usableOutdoorArea = null;

    private ?float $outdoorSalesArea = null;

    private ?bool $roofingPresent = null;

    public function getRoomCount(): ?float
    {
        return $this->roomCount;
    }

    public function setRoomCount(?float $roomCount): self
    {
        $this->roomCount = $roomCount;

        return $this;
    }

    public function getLivingSpace(): ?float
    {
        return $this->livingSpace;
    }

    public function setLivingSpace(?float $livingSpace): self
    {
        $this->livingSpace = $livingSpace;

        return $this;
    }

    public function getUsableSpace(): ?float
    {
        return $this->usableSpace;
    }

    public function setUsableSpace(?float $usableSpace): self
    {
        $this->usableSpace = $usableSpace;

        return $this;
    }

    public function getStoreSpace(): ?float
    {
        return $this->storeSpace;
    }

    public function setStoreSpace(?float $storeSpace): self
    {
        $this->storeSpace = $storeSpace;

        return $this;
    }

    public function getStorageSpace(): ?float
    {
        return $this->storageSpace;
    }

    public function setStorageSpace(?float $storageSpace): self
    {
        $this->storageSpace = $storageSpace;

        return $this;
    }

    public function getRetailSpace(): ?float
    {
        return $this->retailSpace;
    }

    public function setRetailSpace(?float $retailSpace): self
    {
        $this->retailSpace = $retailSpace;

        return $this;
    }

    public function getOpenSpace(): ?float
    {
        return $this->openSpace;
    }

    public function setOpenSpace(?float $openSpace): self
    {
        $this->openSpace = $openSpace;

        return $this;
    }

    public function getOfficeSpace(): ?float
    {
        return $this->officeSpace;
    }

    public function setOfficeSpace(?float $officeSpace): self
    {
        $this->officeSpace = $officeSpace;

        return $this;
    }

    public function getOfficePartSpace(): ?float
    {
        return $this->officePartSpace;
    }

    public function setOfficePartSpace(?float $officePartSpace): self
    {
        $this->officePartSpace = $officePartSpace;

        return $this;
    }

    public function getAdministrationSpace(): ?float
    {
        return $this->administrationSpace;
    }

    public function setAdministrationSpace(?float $administrationSpace): self
    {
        $this->administrationSpace = $administrationSpace;

        return $this;
    }

    public function getGastronomySpace(): ?float
    {
        return $this->gastronomySpace;
    }

    public function setGastronomySpace(?float $gastronomySpace): self
    {
        $this->gastronomySpace = $gastronomySpace;

        return $this;
    }

    public function getProductionSpace(): ?float
    {
        return $this->productionSpace;
    }

    public function setProductionSpace(?float $productionSpace): self
    {
        $this->productionSpace = $productionSpace;

        return $this;
    }

    public function getPlotSize(): ?float
    {
        return $this->plotSize;
    }

    public function setPlotSize(?float $plotSize): self
    {
        $this->plotSize = $plotSize;

        return $this;
    }

    public function getOtherSpace(): ?float
    {
        return $this->otherSpace;
    }

    public function setOtherSpace(?float $otherSpace): self
    {
        $this->otherSpace = $otherSpace;

        return $this;
    }

    public function getBalconyTurfSpace(): ?float
    {
        return $this->balconyTurfSpace;
    }

    public function setBalconyTurfSpace(?float $balconyTurfSpace): self
    {
        $this->balconyTurfSpace = $balconyTurfSpace;

        return $this;
    }

    public function getGardenSpace(): ?float
    {
        return $this->gardenSpace;
    }

    public function setGardenSpace(?float $gardenSpace): self
    {
        $this->gardenSpace = $gardenSpace;

        return $this;
    }

    public function getCellarSpace(): ?float
    {
        return $this->cellarSpace;
    }

    public function setCellarSpace(?float $cellarSpace): self
    {
        $this->cellarSpace = $cellarSpace;

        return $this;
    }

    public function getAtticSpace(): ?float
    {
        return $this->atticSpace;
    }

    public function setAtticSpace(?float $atticSpace): self
    {
        $this->atticSpace = $atticSpace;

        return $this;
    }

    public function getHeatableSurface(): ?float
    {
        return $this->heatableSurface;
    }

    public function setHeatableSurface(?float $heatableSurface): self
    {
        $this->heatableSurface = $heatableSurface;

        return $this;
    }

    public function getRentableArea(): ?float
    {
        return $this->rentableArea;
    }

    public function setRentableArea(?float $rentableArea): self
    {
        $this->rentableArea = $rentableArea;

        return $this;
    }

    public function getMinimumSpaceForSeparation(): ?float
    {
        return $this->minimumSpaceForSeparation;
    }

    public function setMinimumSpaceForSeparation(?float $minimumSpaceForSeparation): self
    {
        $this->minimumSpaceForSeparation = $minimumSpaceForSeparation;

        return $this;
    }

    public function getUsableOutdoorAreaPossibility(): ?string
    {
        return $this->usableOutdoorAreaPossibility;
    }

    public function setUsableOutdoorAreaPossibility(?string $usableOutdoorAreaPossibility): self
    {
        $this->usableOutdoorAreaPossibility = $usableOutdoorAreaPossibility;

        return $this;
    }

    public function getTotalOutdoorArea(): ?float
    {
        return $this->totalOutdoorArea;
    }

    public function setTotalOutdoorArea(?float $totalOutdoorArea): self
    {
        $this->totalOutdoorArea = $totalOutdoorArea;

        return $this;
    }

    public function getUsableOutdoorArea(): ?float
    {
        return $this->usableOutdoorArea;
    }

    public function setUsableOutdoorArea(?float $usableOutdoorArea): self
    {
        $this->usableOutdoorArea = $usableOutdoorArea;

        return $this;
    }

    public function getOutdoorSalesArea(): ?float
    {
        return $this->outdoorSalesArea;
    }

    public function setOutdoorSalesArea(?float $outdoorSalesArea): self
    {
        $this->outdoorSalesArea = $outdoorSalesArea;

        return $this;
    }

    public function isRoofingPresent(): ?bool
    {
        return $this->roofingPresent;
    }

    public function setRoofingPresent(?bool $roofingPresent): self
    {
        $this->roofingPresent = $roofingPresent;

        return $this;
    }
}
