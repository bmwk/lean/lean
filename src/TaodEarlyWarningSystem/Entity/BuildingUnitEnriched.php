<?php

declare(strict_types=1);

namespace App\TaodEarlyWarningSystem\Entity;

use App\TaodEarlyWarningSystem\Repository\BuildingUnitEnrichedRepository;
use CrEOF\Spatial\PHP\Types\Geometry\Point;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;

#[Entity(repositoryClass: BuildingUnitEnrichedRepository::class)]
#[Table(name: 'enriched_building_units', options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
class BuildingUnitEnriched
{
    #[Column(type: Types::INTEGER, nullable: true, options: ['unsigned' => true])]
    private ?int $buildingUnitId = null;

    #[Id]
    #[Column(type: Types::INTEGER, nullable: true, options: ['unsigned' => true])]
    private int $taodBuildingUnitId;

    #[Column(type: Types::INTEGER, nullable: true, options: ['unsigned' => true])]
    private ?int $businessLocationAreaId = null;

    #[Column(name: 'objectIsEmpty', type: Types::BOOLEAN, nullable: true)]
    private ?int $objectIsEmpty = null;

    #[Column(name: 'propertyUsers_industryClassification_name', type: Types::TEXT, nullable: true)]
    private ?string $propertyUsersIndustryClassificationName = null;

    #[Column(name: 'propertyUsers_industryClassification_levelOne', type: Types::INTEGER, nullable: true, options: ['unsigned' => true])]
    private ?int $propertyUsersIndustryClassificationLevelOne = null;

    #[Column(name: 'propertyUsers_industryClassification_levelTwo', type: Types::INTEGER, nullable: true, options: ['unsigned' => true])]
    private ?int $propertyUsersIndustryClassificationLevelTwo = null;

    #[Column(name: 'propertyUsers_industryClassification_levelThree', type: Types::INTEGER, nullable: true, options: ['unsigned' => true])]
    private ?int $propertyUsersIndustryClassificationLevelThree = null;

    #[Column(name: 'propertyUsers_person_name', type: Types::TEXT, nullable: true)]
    private ?string $personName = null;

    #[Column(type: Types::TEXT, nullable: true)]
    private ?string $geometry = null;

    #[Column(type: 'point', nullable: true)]
    private ?Point $geolocationPoint = null;

    public function getBuildingUnitId(): ?int
    {
        return $this->buildingUnitId;
    }

    public function setBuildingUnitId(?int $buildingUnitId): self
    {
        $this->buildingUnitId = $buildingUnitId;

        return $this;
    }

    public function getTaodBuildingUnitId(): int
    {
        return $this->taodBuildingUnitId;
    }

    public function setTaodBuildingUnitId(int $taodBuildingUnitId): self
    {
        $this->taodBuildingUnitId = $taodBuildingUnitId;

        return $this;
    }

    public function getBusinessLocationAreaId(): ?int
    {
        return $this->businessLocationAreaId;
    }

    public function setBusinessLocationAreaId(?int $businessLocationAreaId): void
    {
        $this->businessLocationAreaId = $businessLocationAreaId;
    }

    public function getObjectIsEmpty(): ?int
    {
        return $this->objectIsEmpty;
    }

    public function setObjectIsEmpty(?int $objectIsEmpty): void
    {
        $this->objectIsEmpty = $objectIsEmpty;
    }

    public function getPropertyUsersIndustryClassificationName(): ?string
    {
        return $this->propertyUsersIndustryClassificationName;
    }

    public function setPropertyUsersIndustryClassificationName(?string $propertyUsersIndustryClassificationName): self
    {
        $this->propertyUsersIndustryClassificationName = $propertyUsersIndustryClassificationName;

        return $this;
    }

    public function getPropertyUsersIndustryClassificationLevelOne(): ?int
    {
        return $this->propertyUsersIndustryClassificationLevelOne;
    }

    public function setPropertyUsersIndustryClassificationLevelOne(?int $propertyUsersIndustryClassificationLevelOne): self
    {
        $this->propertyUsersIndustryClassificationLevelOne = $propertyUsersIndustryClassificationLevelOne;

        return $this;
    }

    public function getPropertyUsersIndustryClassificationLevelTwo(): ?int
    {
        return $this->propertyUsersIndustryClassificationLevelTwo;
    }

    public function setPropertyUsersIndustryClassificationLevelTwo(?int $propertyUsersIndustryClassificationLevelTwo): self
    {
        $this->propertyUsersIndustryClassificationLevelTwo = $propertyUsersIndustryClassificationLevelTwo;

        return $this;
    }

    public function getPropertyUsersIndustryClassificationLevelThree(): ?int
    {
        return $this->propertyUsersIndustryClassificationLevelThree;
    }

    public function setPropertyUsersIndustryClassificationLevelThree(?int $propertyUsersIndustryClassificationLevelThree): self
    {
        $this->propertyUsersIndustryClassificationLevelThree = $propertyUsersIndustryClassificationLevelThree;

        return $this;
    }

    public function getPersonName(): ?string
    {
        return $this->personName;
    }

    public function setPersonName(?string $personName): self
    {
        $this->personName = $personName;

        return $this;
    }

    public function getGeometry(): ?string
    {
        return $this->geometry;
    }

    public function setGeometry(?string $geometry): self
    {
        $this->geometry = $geometry;

        return $this;
    }

    public function getGeolocationPoint(): ?Point
    {
        return $this->geolocationPoint;
    }

    public function setGeolocationPoint(?Point $geolocationPoint): self
    {
        $this->geolocationPoint = $geolocationPoint;

        return $this;
    }
}
