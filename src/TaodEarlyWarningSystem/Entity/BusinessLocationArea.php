<?php

declare(strict_types=1);

namespace App\TaodEarlyWarningSystem\Entity;

use App\TaodEarlyWarningSystem\Repository\BusinessLocationAreaRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;

#[Entity(repositoryClass: BusinessLocationAreaRepository::class)]
#[Table(options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
class BusinessLocationArea
{
    #[Id]
    #[GeneratedValue]
    #[Column(type: Types::INTEGER, nullable: false, options: ['unsigned' => true])]
    private ?int $id = null;

    #[Column(type: Types::INTEGER, unique: true, nullable: false, options: ['unsigned' => true])]
    private int $businessLocationAreaId;

    #[Column(type: Types::INTEGER, nullable: false, options: ['unsigned' => true])]
    private int $accountId;

    #[Column(type: Types::TEXT, nullable: false)]
    private string $jsonData;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getBusinessLocationAreaId(): int
    {
        return $this->businessLocationAreaId;
    }

    public function setBusinessLocationAreaId(int $businessLocationAreaId): self
    {
        $this->businessLocationAreaId = $businessLocationAreaId;

        return $this;
    }

    public function getJsonData(): string
    {
        return $this->jsonData;
    }

    public function setJsonData(string $jsonData): self
    {
        $this->jsonData = $jsonData;

        return $this;
    }

    public function getAccountId(): int
    {
        return $this->accountId;
    }

    public function setAccountId(int $accountId): self
    {
        $this->accountId = $accountId;

        return $this;
    }
}
