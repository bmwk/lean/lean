<?php

declare(strict_types=1);

namespace App\TaodEarlyWarningSystem\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;

#[Entity]
#[Table(name: 'business_location_area_scoring', options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
class BusinessLocationAreaScoring
{
    #[Id]
    #[Column(type: Types::INTEGER, nullable: true, options: ['unsigned' => true])]
    private ?int $businessLocationAreaId = null;

    #[Column(type: Types::FLOAT, nullable: true)]
    private ?float $pressureClassification = null;

    public function getBusinessLocationAreaId(): ?int
    {
        return $this->businessLocationAreaId;
    }

    public function setBusinessLocationAreaId(?int $businessLocationAreaId): self
    {
        $this->businessLocationAreaId = $businessLocationAreaId;

        return $this;
    }

    public function getPressureClassification(): ?float
    {
        return $this->pressureClassification;
    }

    public function setPressureClassification(?float $pressureClassification): self
    {
        $this->pressureClassification = $pressureClassification;

        return $this;
    }
}
