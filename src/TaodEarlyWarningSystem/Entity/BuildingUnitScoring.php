<?php

declare(strict_types=1);

namespace App\TaodEarlyWarningSystem\Entity;

use App\TaodEarlyWarningSystem\Repository\BuildingUnitScoringRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;

#[Entity(repositoryClass: BuildingUnitScoringRepository::class)]
#[Table(name: 'building_units_scoring', options: ['charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci'])]
class BuildingUnitScoring
{
    #[Column(type: Types::INTEGER, nullable: true, options: ['unsigned' => true])]
    private ?int $buildingUnitId = null;

    #[Id]
    #[Column(type: Types::INTEGER, nullable: true, options: ['unsigned' => true])]
    private int $taodBuildingUnitId;

    #[Column(type: Types::FLOAT, nullable: true)]
    private ?float $stars = null;

    #[Column(name: 'numberOfReviews', type: Types::FLOAT, nullable: true)]
    private ?float $numberOfReviews = null;

    #[Column(type: Types::FLOAT, nullable: true)]
    private ?float $localSearchPosition = null;

    #[Column(name: 'potential_reach__households', type: Types::FLOAT, nullable: true)]
    private ?float $potentialReachHouseholds = null;

    #[Column(type: Types::FLOAT, nullable: true)]
    private ?float $starsPercentile = null;

    #[Column(name: 'numberOfReviews_percentile', type: Types::FLOAT, nullable: true)]
    private ?float $numberOfReviewsPercentile = null;

    #[Column(type: Types::FLOAT, nullable: true)]
    private ?float $localSearchPositionPercentile = null;

    #[Column(name: 'potential_reach__households_percentile', type: Types::FLOAT, nullable: true)]
    private ?float $potentialReachHouseholdsPercentile = null;

    #[Column(name: 'Marktvolumen', type: Types::FLOAT, nullable: true)]
    private ?float $markedVolume = null;

    #[Column(name: 'branchenentwicklung_longterm', type: Types::FLOAT, nullable: true)]
    private ?float $industryDevelopmentLongterm = null;

    #[Column(name: 'branchenentwicklung_shortterm', type: Types::FLOAT, nullable: true)]
    private ?float $industryDevelopmentShortterm = null;

    #[Column(name: 'fachhandelsanteil_aktuell', type: Types::FLOAT, nullable: true)]
    private ?float $specialtyTradeShareCurrent = null;

    #[Column(name: 'fachhandelsanteil_entwicklung', type: Types::FLOAT, nullable: true)]
    private ?float $specialtyTradeShareDevelopment = null;

    #[Column(name: 'onlineanteil_aktuell', type: Types::FLOAT, nullable: true)]
    private ?float $onlineShareActual = null;

    #[Column(name: 'onlineanteil_entwicklung', type: Types::FLOAT, nullable: true)]
    private ?float $onlineShareDevelopment = null;

    #[Column(name: 'ratio_stationaer_online', type: Types::FLOAT, nullable: true)]
    private ?float $ratioStationaryOnline = null;

    #[Column(name: 'Marktvolumen__percentile', type: Types::FLOAT, nullable: true)]
    private ?float $marketVolumePercentile = null;

    #[Column(name: 'branchenentwicklung_longterm__percentile', type: Types::FLOAT, nullable: true)]
    private ?float $industryDevelopmentLongtermPercentile = null;

    #[Column(name: 'branchenentwicklung_shortterm__percentile', type: Types::FLOAT, nullable: true)]
    private ?float $industryDevelopmentShorttermPercentile = null;

    #[Column(name: 'fachhandelsanteil_aktuell__percentile', type: Types::FLOAT, nullable: true)]
    private ?float $specialtyTradeShareCurrentPercentile = null;

    #[Column(name: 'fachhandelsanteil_entwicklung__percentile', type: Types::FLOAT, nullable: true)]
    private ?float $specialtyTradeShareDevelopmentPercentile = null;

    #[Column(name: 'onlineanteil_aktuell__percentile', type: Types::FLOAT, nullable: true)]
    private ?float $onlineShareActualPercentile = null;

    #[Column(name: 'onlineanteil_entwicklung__percentile', type: Types::FLOAT, nullable: true)]
    private ?float $onlineShareDevelopmentPercentile = null;

    #[Column(name: 'ratio_stationaer_online__percentile', type: Types::FLOAT, nullable: true)]
    private ?float $ratioStationaryOnlinePercentile = null;

    #[Column(name: 'leerstandsquote', type: Types::TEXT, nullable: true)]
    private ?string $vacancyRate = null;

    #[Column(name: 'leerstandsquote__percentile', type: Types::FLOAT, nullable: true)]
    private ?float $vacancyRatePercentile = null;

    #[Column(type: Types::INTEGER, nullable: true, options: ['unsigned' => true])]
    private ?int $businessLocationAreaId = null;

    #[Column(name: 'Kaufkraftniveau', type: Types::FLOAT, nullable: true)]
    private ?float $purchasingPowerLevel = null;

    #[Column(name: 'Zentralitaetsniveau', type: Types::FLOAT, nullable: true)]
    private ?float $centralityLevel = null;

    #[Column(name: 'Gesamtattraktivitaet', type: Types::FLOAT, nullable: true)]
    private ?float $overallAttractiveness = null;

    #[Column(name: 'Attraktivitaetsentwicklung', type: Types::FLOAT, nullable: true)]
    private ?float $attractivenessDevelopment = null;

    #[Column(name: 'NPS', type: Types::FLOAT, nullable: true)]
    private ?float $nps = null;

    #[Column(name: 'Bevoelkerungswachstum', type: Types::FLOAT, nullable: true)]
    private ?float $populationGrowth = null;

    #[Column(name: 'Kaufkraftniveau__percentile', type: Types::FLOAT, nullable: true)]
    private ?float $purchasingPowerLevelPercentile = null;

    #[Column(name: 'Zentralitaetsniveau__percentile', type: Types::FLOAT, nullable: true)]
    private ?float $centralityLevelPercentile = null;

    #[Column(name: 'Gesamtattraktivitaet__percentile', type: Types::FLOAT, nullable: true)]
    private ?float $overallAttractivenessPercentile = null;

    #[Column(name: 'Attraktivitaetsentwicklung__percentile', type: Types::FLOAT, nullable: true)]
    private ?float $attractivenessDevelopmentPercentile = null;

    #[Column(name: 'NPS__percentile', type: Types::FLOAT, nullable: true)]
    private ?float $npsPercentile = null;

    #[Column(name: 'Bevoelkerungswachstum__percentile', type: Types::FLOAT, nullable: true)]
    private ?float $populationGrowthPercentile = null;

    #[Column(type: Types::FLOAT, nullable: true)]
    private ?float $businessLevelMean = null;

    #[Column(type: Types::FLOAT, nullable: true)]
    private ?float $businessLocationAreaLevelMean = null;

    #[Column(type: Types::FLOAT, nullable: true)]
    private ?float $cityLevelMean = null;

    #[Column(type: Types::FLOAT, nullable: true)]
    private ?float $businessLevelScoreReduction = null;

    #[Column(type: Types::INTEGER, nullable: true, options: ['unsigned' => true])]
    private ?int $businessLocationAreaLevelScoreReduction = null;

    #[Column(type: Types::INTEGER, nullable: true, options: ['unsigned' => true])]
    private ?int $cityLevelScoreReduction = null;

    #[Column(type: Types::INTEGER, nullable: true, options: ['unsigned' => true])]
    private ?int $pressureClassification = null;

    private ?BuildingUnitEnriched $buildingUnitEnriched = null;

    public function getBuildingUnitId(): ?int
    {
        return $this->buildingUnitId;
    }

    public function setBuildingUnitId(?int $buildingUnitId): self
    {
        $this->buildingUnitId = $buildingUnitId;

        return $this;
    }

    public function getTaodBuildingUnitId(): int
    {
        return $this->taodBuildingUnitId;
    }

    public function setTaodBuildingUnitId(int $taodBuildingUnitId): self
    {
        $this->taodBuildingUnitId = $taodBuildingUnitId;

        return $this;
    }

    public function getStars(): ?float
    {
        return $this->stars;
    }

    public function setStars(?float $stars): self
    {
        $this->stars = $stars;

        return $this;
    }

    public function getNumberOfReviews(): ?float
    {
        return $this->numberOfReviews;
    }

    public function setNumberOfReviews(?float $numberOfReviews): self
    {
        $this->numberOfReviews = $numberOfReviews;

        return $this;
    }

    public function getLocalSearchPosition(): ?float
    {
        return $this->localSearchPosition;
    }

    public function setLocalSearchPosition(?float $localSearchPosition): self
    {
        $this->localSearchPosition = $localSearchPosition;

        return $this;
    }

    public function getPotentialReachHouseholds(): ?float
    {
        return $this->potentialReachHouseholds;
    }

    public function setPotentialReachHouseholds(?float $potentialReachHouseholds): self
    {
        $this->potentialReachHouseholds = $potentialReachHouseholds;

        return $this;
    }

    public function getStarsPercentile(): ?float
    {
        return $this->starsPercentile;
    }

    public function setStarsPercentile(?float $starsPercentile): self
    {
        $this->starsPercentile = $starsPercentile;

        return $this;
    }

    public function getNumberOfReviewsPercentile(): ?float
    {
        return $this->numberOfReviewsPercentile;
    }

    public function setNumberOfReviewsPercentile(?float $numberOfReviewsPercentile): self
    {
        $this->numberOfReviewsPercentile = $numberOfReviewsPercentile;

        return $this;
    }

    public function getLocalSearchPositionPercentile(): ?float
    {
        return $this->localSearchPositionPercentile;
    }

    public function setLocalSearchPositionPercentile(?float $localSearchPositionPercentile): self
    {
        $this->localSearchPositionPercentile = $localSearchPositionPercentile;

        return $this;
    }

    public function getPotentialReachHouseholdsPercentile(): ?float
    {
        return $this->potentialReachHouseholdsPercentile;
    }

    public function setPotentialReachHouseholdsPercentile(?float $potentialReachHouseholdsPercentile): self
    {
        $this->potentialReachHouseholdsPercentile = $potentialReachHouseholdsPercentile;

        return $this;
    }

    public function getMarkedVolume(): ?float
    {
        return $this->markedVolume;
    }

    public function setMarkedVolume(?float $markedVolume): self
    {
        $this->markedVolume = $markedVolume;

        return $this;
    }

    public function getIndustryDevelopmentLongterm(): ?float
    {
        return $this->industryDevelopmentLongterm;
    }

    public function setIndustryDevelopmentLongterm(?float $industryDevelopmentLongterm): self
    {
        $this->industryDevelopmentLongterm = $industryDevelopmentLongterm;

        return $this;
    }

    public function getIndustryDevelopmentShortterm(): ?float
    {
        return $this->industryDevelopmentShortterm;
    }

    public function setIndustryDevelopmentShortterm(?float $industryDevelopmentShortterm): self
    {
        $this->industryDevelopmentShortterm = $industryDevelopmentShortterm;

        return $this;
    }

    public function getSpecialtyTradeShareCurrent(): ?float
    {
        return $this->specialtyTradeShareCurrent;
    }

    public function setSpecialtyTradeShareCurrent(?float $specialtyTradeShareCurrent): self
    {
        $this->specialtyTradeShareCurrent = $specialtyTradeShareCurrent;

        return $this;
    }

    public function getSpecialtyTradeShareDevelopment(): ?float
    {
        return $this->specialtyTradeShareDevelopment;
    }

    public function setSpecialtyTradeShareDevelopment(?float $specialtyTradeShareDevelopment): self
    {
        $this->specialtyTradeShareDevelopment = $specialtyTradeShareDevelopment;

        return $this;
    }

    public function getOnlineShareActual(): ?float
    {
        return $this->onlineShareActual;
    }

    public function setOnlineShareActual(?float $onlineShareActual): self
    {
        $this->onlineShareActual = $onlineShareActual;

        return $this;
    }

    public function getOnlineShareDevelopment(): ?float
    {
        return $this->onlineShareDevelopment;
    }

    public function setOnlineShareDevelopment(?float $onlineShareDevelopment): self
    {
        $this->onlineShareDevelopment = $onlineShareDevelopment;

        return $this;
    }

    public function getRatioStationaryOnline(): ?float
    {
        return $this->ratioStationaryOnline;
    }

    public function setRatioStationaryOnline(?float $ratioStationaryOnline): self
    {
        $this->ratioStationaryOnline = $ratioStationaryOnline;

        return $this;
    }

    public function getMarketVolumePercentile(): ?float
    {
        return $this->marketVolumePercentile;
    }

    public function setMarketVolumePercentile(?float $marketVolumePercentile): self
    {
        $this->marketVolumePercentile = $marketVolumePercentile;

        return $this;
    }

    public function getIndustryDevelopmentLongtermPercentile(): ?float
    {
        return $this->industryDevelopmentLongtermPercentile;
    }

    public function setIndustryDevelopmentLongtermPercentile(?float $industryDevelopmentLongtermPercentile): self
    {
        $this->industryDevelopmentLongtermPercentile = $industryDevelopmentLongtermPercentile;

        return $this;
    }

    public function getIndustryDevelopmentShorttermPercentile(): ?float
    {
        return $this->industryDevelopmentShorttermPercentile;
    }

    public function setIndustryDevelopmentShorttermPercentile(?float $industryDevelopmentShorttermPercentile): self
    {
        $this->industryDevelopmentShorttermPercentile = $industryDevelopmentShorttermPercentile;

        return $this;
    }

    public function getSpecialtyTradeShareCurrentPercentile(): ?float
    {
        return $this->specialtyTradeShareCurrentPercentile;
    }

    public function setSpecialtyTradeShareCurrentPercentile(?float $specialtyTradeShareCurrentPercentile): self
    {
        $this->specialtyTradeShareCurrentPercentile = $specialtyTradeShareCurrentPercentile;

        return $this;
    }

    public function getSpecialtyTradeShareDevelopmentPercentile(): ?float
    {
        return $this->specialtyTradeShareDevelopmentPercentile;
    }

    public function setSpecialtyTradeShareDevelopmentPercentile(?float $specialtyTradeShareDevelopmentPercentile): self
    {
        $this->specialtyTradeShareDevelopmentPercentile = $specialtyTradeShareDevelopmentPercentile;

        return $this;
    }

    public function getOnlineShareActualPercentile(): ?float
    {
        return $this->onlineShareActualPercentile;
    }

    public function setOnlineShareActualPercentile(?float $onlineShareActualPercentile): self
    {
        $this->onlineShareActualPercentile = $onlineShareActualPercentile;

        return $this;
    }

    public function getOnlineShareDevelopmentPercentile(): ?float
    {
        return $this->onlineShareDevelopmentPercentile;
    }

    public function setOnlineShareDevelopmentPercentile(?float $onlineShareDevelopmentPercentile): self
    {
        $this->onlineShareDevelopmentPercentile = $onlineShareDevelopmentPercentile;

        return $this;
    }

    public function getRatioStationaryOnlinePercentile(): ?float
    {
        return $this->ratioStationaryOnlinePercentile;
    }

    public function setRatioStationaryOnlinePercentile(?float $ratioStationaryOnlinePercentile): self
    {
        $this->ratioStationaryOnlinePercentile = $ratioStationaryOnlinePercentile;

        return $this;
    }

    public function getVacancyRate(): ?string
    {
        return $this->vacancyRate;
    }

    public function setVacancyRate(?string $vacancyRate): self
    {
        $this->vacancyRate = $vacancyRate;

        return $this;
    }

    public function getVacancyRatePercentile(): ?float
    {
        return $this->vacancyRatePercentile;
    }

    public function setVacancyRatePercentile(?float $vacancyRatePercentile): self
    {
        $this->vacancyRatePercentile = $vacancyRatePercentile;

        return $this;
    }

    public function getBusinessLocationAreaId(): ?int
    {
        return $this->businessLocationAreaId;
    }

    public function setBusinessLocationAreaId(?int $businessLocationAreaId): self
    {
        $this->businessLocationAreaId = $businessLocationAreaId;

        return $this;
    }

    public function getPurchasingPowerLevel(): ?float
    {
        return $this->purchasingPowerLevel;
    }

    public function setPurchasingPowerLevel(?float $purchasingPowerLevel): self
    {
        $this->purchasingPowerLevel = $purchasingPowerLevel;

        return $this;
    }

    public function getCentralityLevel(): ?float
    {
        return $this->centralityLevel;
    }

    public function setCentralityLevel(?float $centralityLevel): self
    {
        $this->centralityLevel = $centralityLevel;

        return $this;
    }

    public function getOverallAttractiveness(): ?float
    {
        return $this->overallAttractiveness;
    }

    public function setOverallAttractiveness(?float $overallAttractiveness): self
    {
        $this->overallAttractiveness = $overallAttractiveness;

        return $this;
    }

    public function getAttractivenessDevelopment(): ?float
    {
        return $this->attractivenessDevelopment;
    }

    public function setAttractivenessDevelopment(?float $attractivenessDevelopment): self
    {
        $this->attractivenessDevelopment = $attractivenessDevelopment;

        return $this;
    }

    public function getNps(): ?float
    {
        return $this->nps;
    }

    public function setNps(?float $nps): self
    {
        $this->nps = $nps;

        return $this;
    }

    public function getPopulationGrowth(): ?float
    {
        return $this->populationGrowth;
    }

    public function setPopulationGrowth(?float $populationGrowth): self
    {
        $this->populationGrowth = $populationGrowth;

        return $this;
    }

    public function getPurchasingPowerLevelPercentile(): ?float
    {
        return $this->purchasingPowerLevelPercentile;
    }

    public function setPurchasingPowerLevelPercentile(?float $purchasingPowerLevelPercentile): self
    {
        $this->purchasingPowerLevelPercentile = $purchasingPowerLevelPercentile;

        return $this;
    }

    public function getCentralityLevelPercentile(): ?float
    {
        return $this->centralityLevelPercentile;
    }

    public function setCentralityLevelPercentile(?float $centralityLevelPercentile): self
    {
        $this->centralityLevelPercentile = $centralityLevelPercentile;

        return $this;
    }

    public function getOverallAttractivenessPercentile(): ?float
    {
        return $this->overallAttractivenessPercentile;
    }

    public function setOverallAttractivenessPercentile(?float $overallAttractivenessPercentile): self
    {
        $this->overallAttractivenessPercentile = $overallAttractivenessPercentile;

        return $this;
    }

    public function getAttractivenessDevelopmentPercentile(): ?float
    {
        return $this->attractivenessDevelopmentPercentile;
    }

    public function setAttractivenessDevelopmentPercentile(?float $attractivenessDevelopmentPercentile): self
    {
        $this->attractivenessDevelopmentPercentile = $attractivenessDevelopmentPercentile;

        return $this;
    }

    public function getNpsPercentile(): ?float
    {
        return $this->npsPercentile;
    }

    public function setNpsPercentile(?float $npsPercentile): self
    {
        $this->npsPercentile = $npsPercentile;

        return $this;
    }

    public function getPopulationGrowthPercentile(): ?float
    {
        return $this->populationGrowthPercentile;
    }

    public function setPopulationGrowthPercentile(?float $populationGrowthPercentile): self
    {
        $this->populationGrowthPercentile = $populationGrowthPercentile;

        return $this;
    }

    public function getBusinessLevelMean(): ?float
    {
        return $this->businessLevelMean;
    }

    public function setBusinessLevelMean(?float $businessLevelMean): self
    {
        $this->businessLevelMean = $businessLevelMean;

        return $this;
    }

    public function getCityLevelMean(): ?float
    {
        return $this->cityLevelMean;
    }

    public function setCityLevelMean(?float $cityLevelMean): self
    {
        $this->cityLevelMean = $cityLevelMean;

        return $this;
    }

    public function getBusinessLevelScoreReduction(): ?float
    {
        return $this->businessLevelScoreReduction;
    }

    public function setBusinessLevelScoreReduction(?float $businessLevelScoreReduction): self
    {
        $this->businessLevelScoreReduction = $businessLevelScoreReduction;

        return $this;
    }

    public function getBusinessLocationAreaLevelScoreReduction(): ?int
    {
        return $this->businessLocationAreaLevelScoreReduction;
    }

    public function setBusinessLocationAreaLevelScoreReduction(?int $businessLocationAreaLevelScoreReduction): self
    {
        $this->businessLocationAreaLevelScoreReduction = $businessLocationAreaLevelScoreReduction;

        return $this;
    }

    public function getCityLevelScoreReduction(): ?int
    {
        return $this->cityLevelScoreReduction;
    }

    public function setCityLevelScoreReduction(?int $cityLevelScoreReduction): self
    {
        $this->cityLevelScoreReduction = $cityLevelScoreReduction;

        return $this;
    }

    public function getPressureClassification(): ?int
    {
        return $this->pressureClassification;
    }

    public function setPressureClassification(?int $pressureClassification): self
    {
        $this->pressureClassification = $pressureClassification;

        return $this;
    }

    public function getBusinessLocationAreaLevelMean(): ?float
    {
        return $this->businessLocationAreaLevelMean;
    }

    public function setBusinessLocationAreaLevelMean(?float $businessLocationAreaLevelMean): self
    {
        $this->businessLocationAreaLevelMean = $businessLocationAreaLevelMean;

        return $this;
    }

    public function getBuildingUnitEnriched(): ?BuildingUnitEnriched
    {
        return $this->buildingUnitEnriched;
    }

    public function setBuildingUnitEnriched(?BuildingUnitEnriched $buildingUnitEnriched): self
    {
        $this->buildingUnitEnriched = $buildingUnitEnriched;

        return $this;
    }
}
