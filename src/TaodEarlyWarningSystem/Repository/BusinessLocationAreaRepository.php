<?php

declare(strict_types=1);

namespace App\TaodEarlyWarningSystem\Repository;

use App\TaodEarlyWarningSystem\Entity\BusinessLocationArea;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BusinessLocationArea|null find($id, $lockMode = null, $lockVersion = null)
 * @method BusinessLocationArea|null findOneBy(array $criteria, array $orderBy = null)
 * @method BusinessLocationArea[]    findAll()
 * @method BusinessLocationArea[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BusinessLocationAreaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BusinessLocationArea::class);
    }
}
