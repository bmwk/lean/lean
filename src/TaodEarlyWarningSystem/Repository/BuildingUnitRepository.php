<?php

declare(strict_types=1);

namespace App\TaodEarlyWarningSystem\Repository;

use App\TaodEarlyWarningSystem\Entity\BuildingUnit;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BuildingUnit|null find($id, $lockMode = null, $lockVersion = null)
 * @method BuildingUnit|null findOneBy(array $criteria, array $orderBy = null)
 * @method BuildingUnit[]    findAll()
 * @method BuildingUnit[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BuildingUnitRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BuildingUnit::class);
    }
}
