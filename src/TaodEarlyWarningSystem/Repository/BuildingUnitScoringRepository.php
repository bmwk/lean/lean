<?php

declare(strict_types=1);

namespace App\TaodEarlyWarningSystem\Repository;

use App\TaodEarlyWarningSystem\Entity\BuildingUnit;
use App\TaodEarlyWarningSystem\Entity\BuildingUnitEnriched;
use App\TaodEarlyWarningSystem\Entity\BuildingUnitScoring;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BuildingUnitScoring|null find($id, $lockMode = null, $lockVersion = null)
 * @method BuildingUnitScoring|null findOneBy(array $criteria, array $orderBy = null)
 * @method BuildingUnitScoring[]    findAll()
 * @method BuildingUnitScoring[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BuildingUnitScoringRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BuildingUnitScoring::class);
    }

    /**
     * @return BuildingUnitScoring[]
     */
    public function findByAccountId(int $accountId): array
    {
        $queryBuilder = $this->createQueryBuilder(alias: 'buildingUnitScoring');

        $queryBuilder
            ->addSelect(select: 'buildingUnitEnriched')
            ->leftJoin(
                join: BuildingUnitEnriched::class,
                alias: 'buildingUnitEnriched',
                conditionType: Join::WITH,
                condition: 'buildingUnitScoring.taodBuildingUnitId = buildingUnitEnriched.taodBuildingUnitId'
            )
            ->leftJoin(
                join: BuildingUnit::class,
                alias: 'buildingUnit',
                conditionType: Join::WITH,
                condition: 'buildingUnitScoring.buildingUnitId = buildingUnit.buildingUnitId'
            )
            ->where(predicates: $queryBuilder->expr()->orX()
                ->add('buildingUnit.accountId = :accountId')
                ->add('buildingUnitScoring.buildingUnitId IS NULL'))
            ->setParameter(key: 'accountId', value: $accountId);

        $result = $queryBuilder->getQuery()->getResult();

        $buildingUnitsEnriched = array_filter(array: $result, callback: function (BuildingUnitScoring|BuildingUnitEnriched|null $object) {
            return $object instanceof BuildingUnitEnriched;
        });

        $buildingUnitsScoring = array_filter(array: $result, callback: function (BuildingUnitScoring|BuildingUnitEnriched|null $object) {
            return $object instanceof BuildingUnitScoring;
        });

        foreach ($buildingUnitsScoring as $buildingUnitScoring) {
            if ($buildingUnitScoring->getTaodBuildingUnitId() === null) {
                continue;
            }

            $filteredBuildingUnitsEnriched = array_filter(array: $buildingUnitsEnriched, callback: function (BuildingUnitEnriched $buildingUnitEnriched) use ($buildingUnitScoring) {
                return $buildingUnitEnriched->getTaodBuildingUnitId() === $buildingUnitScoring->getTaodBuildingUnitId();
            });

            if (empty($filteredBuildingUnitsEnriched) === true) {
                continue;
            }

            $buildingUnitScoring->setBuildingUnitEnriched(array_values($filteredBuildingUnitsEnriched)[0]);
        }

        return array_values($buildingUnitsScoring);
    }

    public function findOneByTaodBuildingUnitIdAndAccountId(int $taodBuildingUnitId, int $accountId): ?BuildingUnitScoring
    {
        $queryBuilder = $this->createQueryBuilder(alias: 'buildingUnitScoring');

        $queryBuilder
            ->leftJoin(
                join: BuildingUnit::class,
                alias: 'buildingUnit',
                conditionType: Join::WITH,
                condition: 'buildingUnitScoring.buildingUnitId = buildingUnit.buildingUnitId'
            )
            ->where(predicates: 'buildingUnitScoring.taodBuildingUnitId = :taodBuildingUnitId')
            ->andWhere($queryBuilder->expr()->orX()
                ->add('buildingUnit.accountId = :accountId')
                ->add('buildingUnitScoring.buildingUnitId IS NULL'))

            ->setParameter(key: 'taodBuildingUnitId', value: $taodBuildingUnitId)
            ->setParameter(key: 'accountId', value: $accountId);

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }
}
