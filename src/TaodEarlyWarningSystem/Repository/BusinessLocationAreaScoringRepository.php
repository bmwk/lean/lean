<?php

declare(strict_types=1);

namespace App\TaodEarlyWarningSystem\Repository;

use App\TaodEarlyWarningSystem\Entity\BusinessLocationArea;
use App\TaodEarlyWarningSystem\Entity\BusinessLocationAreaScoring;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BusinessLocationAreaScoring|null find($id, $lockMode = null, $lockVersion = null)
 * @method BusinessLocationAreaScoring|null findOneBy(array $criteria, array $orderBy = null)
 * @method BusinessLocationAreaScoring[]    findAll()
 * @method BusinessLocationAreaScoring[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BusinessLocationAreaScoringRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BusinessLocationAreaScoring::class);
    }

    /**
     * @return BusinessLocationAreaScoring[]
     */
    public function findByAccountId(int $accountId): array
    {
        $queryBuilder = $this->createQueryBuilder(alias: 'businessLocationAreaScoring');

        $queryBuilder
            ->leftJoin(
                join: BusinessLocationArea::class,
                alias: 'businessLocationArea',
                conditionType: Join::WITH,
                condition: 'businessLocationAreaScoring.businessLocationAreaId = businessLocationArea.businessLocationAreaId'
            )
            ->where(predicates: 'businessLocationArea.accountId = :accountId')
            ->setParameter(key: 'accountId', value: $accountId);

        return $queryBuilder->getQuery()->getResult();
    }

    public function findOneByBusinessLocationAreaIdAndAccountId(int $businessLocationAreaId, int $accountId): ?BusinessLocationAreaScoring
    {
        $queryBuilder = $this->createQueryBuilder(alias: 'businessLocationAreaScoring');

        $queryBuilder
            ->leftJoin(
                join: BusinessLocationArea::class,
                alias: 'businessLocationArea',
                conditionType: Join::WITH,
                condition: 'businessLocationAreaScoring.businessLocationAreaId = businessLocationArea.businessLocationAreaId'
            )
            ->where(predicates: 'businessLocationArea.accountId = :accountId')
            ->andWhere('businessLocationAreaScoring.index = :businessLocationAreaId')
            ->setParameter(key: 'accountId', value: $accountId)
            ->setParameter(key: 'businessLocationAreaId', value: $businessLocationAreaId);

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }
}
