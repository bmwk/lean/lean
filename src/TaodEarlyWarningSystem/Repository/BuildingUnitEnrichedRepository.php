<?php

declare(strict_types=1);

namespace App\TaodEarlyWarningSystem\Repository;

use App\TaodEarlyWarningSystem\Entity\BuildingUnit;
use App\TaodEarlyWarningSystem\Entity\BuildingUnitEnriched;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BuildingUnitEnriched|null find($id, $lockMode = null, $lockVersion = null)
 * @method BuildingUnitEnriched|null findOneBy(array $criteria, array $orderBy = null)
 * @method BuildingUnitEnriched[]    findAll()
 * @method BuildingUnitEnriched[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BuildingUnitEnrichedRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BuildingUnitEnriched::class);
    }

    public function findOneByTaodBuildingUnitIdAndAccountId(int $taodBuildingUnitId, int $accountId): ?BuildingUnitEnriched
    {
        $queryBuilder = $this->createQueryBuilder(alias: 'buildingUnitEnriched');

        $queryBuilder
            ->leftJoin(
                join: BuildingUnit::class,
                alias: 'buildingUnit',
                conditionType: Join::WITH,
                condition: 'buildingUnitEnriched.buildingUnitId = buildingUnit.buildingUnitId'
            )
            ->where(predicates: 'buildingUnitEnriched.taodBuildingUnitId = :taodBuildingUnitId')
            ->andWhere($queryBuilder->expr()->orX()
                ->add('buildingUnit.accountId = :accountId')
                ->add('buildingUnitEnriched.buildingUnitId IS NULL'))
            ->setParameter(key: 'taodBuildingUnitId', value: $taodBuildingUnitId)
            ->setParameter(key: 'accountId', value: $accountId);

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }
}
