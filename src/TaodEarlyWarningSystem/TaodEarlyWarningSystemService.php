<?php

declare(strict_types=1);

namespace App\TaodEarlyWarningSystem;

use App\TaodEarlyWarningSystem\Entity\BuildingUnit;
use App\TaodEarlyWarningSystem\Entity\BuildingUnitEnriched;
use App\TaodEarlyWarningSystem\Entity\BuildingUnitScoring;
use App\TaodEarlyWarningSystem\Entity\BusinessLocationArea;
use App\TaodEarlyWarningSystem\Entity\BusinessLocationAreaScoring;
use App\TaodEarlyWarningSystem\Entity\JsonRepresentation\BuildingUnit as BuildingUnitJsonRepresentation;
use App\TaodEarlyWarningSystem\Entity\JsonRepresentation\BusinessLocationArea as BusinessLocationAreaJsonRepresentation;
use App\TaodEarlyWarningSystem\Repository\BuildingUnitEnrichedRepository;
use App\TaodEarlyWarningSystem\Repository\BuildingUnitScoringRepository;
use App\TaodEarlyWarningSystem\Repository\BusinessLocationAreaScoringRepository;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

class TaodEarlyWarningSystemService
{
    private readonly EntityManagerInterface $entityManager;
    private readonly Connection $connection;

    public function __construct(
        private readonly BusinessLocationAreaScoringRepository $businessLocationAreaScoringRepository,
        private readonly BuildingUnitEnrichedRepository $buildingUnitEnrichedRepository,
        private readonly BuildingUnitScoringRepository $buildingUnitScoringRepository,
        private readonly SerializerInterface $serializer,
        private readonly ManagerRegistry $registry
    ) {
        $this->entityManager = $this->registry->getManager(name: 'taod_early_warning_system');
        $this->connection = $this->entityManager->getConnection();
   }

    /**
     * @return BuildingUnitScoring[]
     */
    public function fetchBuildingUnitScoringsByAccountId(int $accountId): array
    {
        return $this->buildingUnitScoringRepository->findByAccountId(accountId: $accountId);
    }

    public function fetchBuildingUnitScoringByTaodBuildingUnitIdAndAccountId(int $taodBuildingUnitId, int $accountId): ?BuildingUnitScoring
    {
        $buildingUnitScoring = $this->buildingUnitScoringRepository->findOneByTaodBuildingUnitIdAndAccountId(taodBuildingUnitId: $taodBuildingUnitId, accountId: $accountId);
        $buildingUnitEnriched = $this->fetchBuildingUnitEnrichedByTaodBuildingUnitIdAndAccountId(
            taodBuildingUnitId: $buildingUnitScoring->getTaodBuildingUnitId(),
            accountId: $accountId
        );

        $buildingUnitScoring->setBuildingUnitEnriched($buildingUnitEnriched);

        return $buildingUnitScoring;
    }

    public function fetchBuildingUnitEnrichedByTaodBuildingUnitIdAndAccountId(int $taodBuildingUnitId, int $accountId): ?BuildingUnitEnriched
    {
        return $this->buildingUnitEnrichedRepository->findOneByTaodBuildingUnitIdAndAccountId(taodBuildingUnitId: $taodBuildingUnitId, accountId: $accountId);
    }

    /**
     * @return BusinessLocationAreaScoring[]
     */
    public function fetchBusinessLocationAreaScoringsByAccountId(int $accountId): array
    {
        return $this->businessLocationAreaScoringRepository->findByAccountId(accountId: $accountId);
    }

    public function fetchBusinessLocationAreaScoringByBusinessLocationAreaIdAndAccountId(int $businessLocationAreaId, int $accountId): ?BusinessLocationAreaScoring
    {
        return $this->businessLocationAreaScoringRepository->findOneByBusinessLocationAreaIdAndAccountId(businessLocationAreaId: $businessLocationAreaId, accountId: $accountId);
    }

    /**
     * @param BuildingUnitJsonRepresentation[] $buildingUnitJsonRepresentations
     */
    public function createAndSaveBuildingUnitsFromBuildingUnitJsonRepresentations(array $buildingUnitJsonRepresentations): void
    {
        foreach ($buildingUnitJsonRepresentations as $buildingUnitJsonRepresentation) {
            $this->entityManager->persist($this->createBuildingUnitFromBuildingUnitJsonRepresentation(buildingUnitJsonRepresentation: $buildingUnitJsonRepresentation));
        }

        $this->entityManager->flush();
    }

    public function createBuildingUnitFromBuildingUnitJsonRepresentation(BuildingUnitJsonRepresentation $buildingUnitJsonRepresentation): BuildingUnit
    {
        $buildingUnit = new BuildingUnit();

        $buildingUnit
            ->setBuildingUnitId($buildingUnitJsonRepresentation->getId())
            ->setAccountId($buildingUnitJsonRepresentation->getAccountId())
            ->setJsonData($this->serializer->serialize(
                data: $buildingUnitJsonRepresentation,
                format: 'json',
                context: [AbstractNormalizer::IGNORED_ATTRIBUTES => ['accountId']]
            ));

        return $buildingUnit;
    }

    /**
     * @param BusinessLocationAreaJsonRepresentation[] $businessLocationAreaJsonRepresentations
     */
    public function createAndSaveBusinessLocationAreasFromBusinessLocationAreaJsonRepresentations(array $businessLocationAreaJsonRepresentations): void
    {
        foreach ($businessLocationAreaJsonRepresentations as $businessLocationAreaJsonRepresentation) {
            $this->entityManager->persist($this->createBusinessLocationAreaFromBusinessLocationAreaJsonRepresentation(businessLocationAreaJsonRepresentation: $businessLocationAreaJsonRepresentation));
        }

        $this->entityManager->flush();
    }

    public function createBusinessLocationAreaFromBusinessLocationAreaJsonRepresentation(BusinessLocationAreaJsonRepresentation $businessLocationAreaJsonRepresentation): BusinessLocationArea
    {
        $businessLocationArea = new BusinessLocationArea();

        $businessLocationArea
            ->setBusinessLocationAreaId($businessLocationAreaJsonRepresentation->getId())
            ->setAccountId($businessLocationAreaJsonRepresentation->getAccountId())
            ->setJsonData($this->serializer->serialize(
                data: $businessLocationAreaJsonRepresentation,
                format: 'json',
                context: [AbstractNormalizer::IGNORED_ATTRIBUTES => ['accountId']]
            ));

        return $businessLocationArea;
    }

    public function truncateBusinessLocationAreaTable(): void
    {
        $platform = $this->connection->getDatabasePlatform();

        $this->connection->executeQuery(sql: $platform->getTruncateTableSQL(tableName: 'business_location_area'));
    }

    public function truncateBuildingUnitTable(): void
    {
        $platform = $this->connection->getDatabasePlatform();

        $this->connection->executeQuery(sql: $platform->getTruncateTableSQL(tableName: 'building_unit'));
    }
}
