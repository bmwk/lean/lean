<?php

declare(strict_types=1);

namespace App\Repository\UserRegistration;

use App\Domain\Entity\Account;
use App\Domain\Entity\Person\PersonType;
use App\Domain\Entity\SortingOption\SortingOption;
use App\Domain\Entity\UserRegistration\UserInvitation;
use App\Domain\Entity\UserRegistration\UserInvitationFilter;
use App\Domain\Entity\UserRegistration\UserInvitationSearch;
use App\Domain\Entity\UserRegistration\UserRegistrationType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserInvitation|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserInvitation|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserInvitation[]    findAll()
 * @method UserInvitation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserInvitationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserInvitation::class);
    }

    /**
     * @param UserRegistrationType[]|null $userRegistrationTypes
     * @return UserInvitation[]
     */
    public function findByAccount(
        Account $account,
        ?array $userRegistrationTypes = null,
        ?UserInvitationFilter $userInvitationFilter = null,
        ?UserInvitationSearch $userInvitationSearch = null
    ): array {
        $queryBuilder = $this->createFindByAccountQueryBuilder(
            account: $account,
            userRegistrationTypes: $userRegistrationTypes,
            userInvitationFilter: $userInvitationFilter,
            userInvitationSearch: $userInvitationSearch
        );

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * @param UserRegistrationType[]|null $userRegistrationTypes
     */
    public function findPaginatedByAccount(
        Account $account,
        int $firstResult,
        int $maxResults,
        ?array $userRegistrationTypes = null,
        ?UserInvitationFilter $userInvitationFilter = null,
        ?UserInvitationSearch $userInvitationSearch = null,
        ?SortingOption $sortingOption = null
    ): Paginator {
        $queryBuilder = $this->createFindByAccountQueryBuilder(
            account: $account,
            userRegistrationTypes: $userRegistrationTypes,
            userInvitationFilter: $userInvitationFilter,
            userInvitationSearch: $userInvitationSearch,
            sortingOption: $sortingOption
        );

        $queryBuilder
            ->setFirstResult($firstResult)
            ->setMaxResults($maxResults);

        return new Paginator(query: $queryBuilder->getQuery(), fetchJoinCollection: true);
    }

    /**
     * @param int[] $ids
     * @param UserRegistrationType[]|null $userRegistrationTypes
     * @return UserInvitation[]
     */
    public function findByIds(
        Account $account,
        array $ids,
        ?array $userRegistrationTypes = null,
        ?UserInvitationFilter $userInvitationFilter = null,
        ?UserInvitationSearch $userInvitationSearch = null
    ): array {
        $queryBuilder = $this->createFindByAccountQueryBuilder(
            account: $account,
            userRegistrationTypes: $userRegistrationTypes,
            userInvitationFilter: $userInvitationFilter,
            userInvitationSearch: $userInvitationSearch
        );

        $queryBuilder
            ->andWhere('userInvitation.id IN (:ids)')
            ->setParameter(key: 'ids', value: $ids, type: Connection::PARAM_INT_ARRAY);

        return $queryBuilder->getQuery()->getResult();
    }

    public function findOneById(Account $account, int $id, ?UserRegistrationType $userRegistrationTypes): ?UserInvitation
    {
        $queryBuilder = $this->createFindByAccountQueryBuilder(account: $account, userRegistrationTypes: $userRegistrationTypes);

        $queryBuilder
            ->andWhere('userInvitation.id = :id')
            ->setParameter(key: 'id', value: $id);

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }

    /**
     * @return UserInvitation[]
     */
    public function findOverdueUserInvitations(int $daysUntilOverdue): array
    {
        $queryBuilder = $this->createQueryBuilder(alias: 'userInvitation');

        $expression = $queryBuilder->expr();

        $queryBuilder
            ->innerJoin(join: 'userInvitation.userRegistration', alias: 'userRegistration')
            ->where(
                $expression->orX(
                    $expression->andX(
                        $expression->isNull('userInvitation.resendDate'),
                        $expression->lte('userInvitation.createdAt', ':now')
                    ),
                    $expression->orX(
                        $expression->isNotNull('userInvitation.resendDate'),
                        $expression->lte('userInvitation.resendDate', ':now')
                    )
                )
            )
            ->setParameter(key: 'now', value: (new \Datetime())->sub(new \DateInterval(duration: 'P' . $daysUntilOverdue . 'D')));

        return $queryBuilder->getQuery()->getResult();
    }

    public function countByAccount(Account $account, ?array $userRegistrationTypes = null,): int
    {
        $queryBuilder = $this->createFindByAccountQueryBuilder(account: $account, userRegistrationTypes: $userRegistrationTypes);

        $queryBuilder->select(select: 'COUNT(DISTINCT userInvitation.id)');

        return $queryBuilder->getQuery()->getSingleScalarResult();
    }

    /**
     * @param UserRegistrationType[]|null $userRegistrationTypes
     */
    private function createFindByAccountQueryBuilder(
        Account $account,
        ?array $userRegistrationTypes = null,
        ?UserInvitationFilter $userInvitationFilter = null,
        ?UserInvitationSearch $userInvitationSearch = null,
        ?SortingOption $sortingOption = null
    ): QueryBuilder {
        $queryBuilder = $this->createQueryBuilder(alias: 'userInvitation');

        $queryBuilder
            ->addSelect(select: 'account')
            ->addSelect(select: 'userRegistration')
            ->innerJoin(
                join: 'userInvitation.account',
                alias: 'account',
                conditionType: Join::WITH,
                condition: 'account = :account AND account.deleted = false AND account.enabled = true'
            )
            ->leftJoin(join:'userInvitation.userRegistration', alias: 'userRegistration')
            ->where(predicates: 'userInvitation.account = account')
            ->andWhere('userRegistration IS NULL')
            ->setParameter(key: 'account', value: $account);

        if (empty($userRegistrationTypes) === false) {
            $queryBuilder
                ->andWhere('userInvitation.userRegistrationType IN (:userRegistrationTypes)')
                ->setParameter(key: 'userRegistrationTypes', value: $userRegistrationTypes);
        }

        if ($userInvitationFilter !== null && empty($userInvitationFilter->getPersonTypes()) === false) {
            $queryBuilder
                ->andWhere('userInvitation.personType IN (:personTypes)')
                ->setParameter(key: 'personTypes', value: $userInvitationFilter->getPersonTypes());
        }

        if ($userInvitationSearch !== null && empty($userInvitationSearch->getText()) === false) {
            $expression = $queryBuilder->expr()->orX();

            $expression
                ->add('userInvitation.email LIKE :searchText')
                ->add('userInvitation.lastName LIKE :searchText')
                ->add('userInvitation.firstName LIKE :searchText')
                ->add('userInvitation.personType = :personType AND userInvitation.companyName LIKE :searchText');

            $queryBuilder
                ->andWhere($expression)
                ->setParameter(key: 'searchText', value: '%' . $userInvitationSearch->getText() . '%')
                ->setParameter(key: 'personType', value: PersonType::COMPANY);
        }

        if ($sortingOption !== null) {
            self::applyUserInvitationSortingToQueryBuilder(sortingOption: $sortingOption, queryBuilder: $queryBuilder);
        }

        return $queryBuilder;
    }

    private static function applyUserInvitationSortingToQueryBuilder(SortingOption $sortingOption, QueryBuilder $queryBuilder): void
    {
        if ($sortingOption->getSortingBy() === 'vorname') {
            $queryBuilder->orderBy(sort: 'userInvitation.firstName', order: $sortingOption->getSortingDirection()->getOrderByDirection());
        }

        if ($sortingOption->getSortingBy() === 'nachname') {
            $queryBuilder->orderBy(sort: 'userInvitation.lastName', order: $sortingOption->getSortingDirection()->getOrderByDirection());
        }

        if ($sortingOption->getSortingBy() === 'firmenname') {
            $queryBuilder->orderBy(sort: 'userInvitation.companyName', order: $sortingOption->getSortingDirection()->getOrderByDirection());
        }

        if ($sortingOption->getSortingBy() === 'email') {
            $queryBuilder->orderBy(sort: 'userInvitation.email', order: $sortingOption->getSortingDirection()->getOrderByDirection());
        }

        if ($sortingOption->getSortingBy() === 'typ') {
            $orderByExpression = $queryBuilder->expr()->andX();

            $orderByExpression->add(<<<EOT
                CASE
                    WHEN userInvitation.userRegistrationType = 1 THEN 'Suchende'
                    ELSE 'Anbietende'
                END
                EOT
            );

            $queryBuilder->orderBy(sort: $orderByExpression, order: $sortingOption->getSortingDirection()->getOrderByDirection());
        }

        if ($sortingOption->getSortingBy() === 'zuletzt_eingeladen_am') {
            $orderByExpression = $queryBuilder->expr()->andX();

            $orderByExpression->add(<<<EOT
                CASE
                    WHEN userInvitation.resendDate IS NOT NULL THEN userInvitation.resendDate
                    ELSE userInvitation.createdAt
                END
                EOT
            );

            $queryBuilder->orderBy(sort: $orderByExpression, order: $sortingOption->getSortingDirection()->getOrderByDirection());
        }
    }
}
