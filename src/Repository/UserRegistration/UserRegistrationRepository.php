<?php

declare(strict_types=1);

namespace App\Repository\UserRegistration;

use App\Domain\Entity\Account;
use App\Domain\Entity\Person\PersonType;
use App\Domain\Entity\SortingOption\SortingOption;
use App\Domain\Entity\UserRegistration\UserRegistration;
use App\Domain\Entity\UserRegistration\UserRegistrationFilter;
use App\Domain\Entity\UserRegistration\UserRegistrationSearch;
use App\Domain\Entity\UserRegistration\UserRegistrationStatus;
use App\Domain\Entity\UserRegistration\UserRegistrationType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserRegistration|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserRegistration|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserRegistration[]    findAll()
 * @method UserRegistration[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRegistrationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserRegistration::class);
    }

    /**
     * @param UserRegistrationType[]|null $userRegistrationTypes
     * @return UserRegistration[]
     */
    public function findByAccount(
        Account $account,
        bool $withoutInvitation,
        ?array $userRegistrationTypes = null,
        ?UserRegistrationFilter $userRegistrationFilter = null,
        ?UserRegistrationSearch $userRegistrationSearch = null
    ): array {
        $queryBuilder = $this->createFindByAccountQueryBuilder(
            account: $account,
            withoutInvitation: $withoutInvitation,
            userRegistrationTypes: $userRegistrationTypes,
            userRegistrationFilter: $userRegistrationFilter,
            userRegistrationSearch: $userRegistrationSearch
        );

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * @param UserRegistrationType[]|null $userRegistrationTypes
     */
    public function findPaginatedByAccount(
        Account $account,
        bool $withoutInvitation,
        int $firstResult,
        int $maxResults,
        ?array $userRegistrationTypes = null,
        ?UserRegistrationFilter $userRegistrationFilter = null,
        ?UserRegistrationSearch $userRegistrationSearch = null,
        ?SortingOption $sortingOption = null
    ): Paginator {
        $queryBuilder = $this->createFindByAccountQueryBuilder(
            account: $account,
            withoutInvitation: $withoutInvitation,
            userRegistrationTypes: $userRegistrationTypes,
            userRegistrationFilter: $userRegistrationFilter,
            userRegistrationSearch: $userRegistrationSearch,
            sortingOption: $sortingOption
        );

        $queryBuilder
            ->setFirstResult($firstResult)
            ->setMaxResults($maxResults);

        return new Paginator(query: $queryBuilder->getQuery(), fetchJoinCollection: true);
    }

    /**
     * @param int[] $ids
     * @param UserRegistrationType[]|null $userRegistrationTypes
     * @return UserRegistration[]
     */
    public function findByIds(
        Account $account,
        array $ids,
        bool $withoutInvitation,
        ?array $userRegistrationTypes,
        ?UserRegistrationFilter $userRegistrationFilter = null,
        ?UserRegistrationSearch $userRegistrationSearch = null
    ): array {
        $queryBuilder = $this->createFindByAccountQueryBuilder(
            account: $account,
            withoutInvitation: $withoutInvitation,
            userRegistrationTypes: $userRegistrationTypes,
            userRegistrationFilter: $userRegistrationFilter,
            userRegistrationSearch: $userRegistrationSearch
        );

        $queryBuilder
            ->andWhere('userRegistration.id IN (:ids)')
            ->setParameter(key: 'ids', value: $ids, type: Connection::PARAM_INT_ARRAY);

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * @param UserRegistrationType[]|null $userRegistrationTypes
     */
    public function findOneById(Account $account, int $id, ?array $userRegistrationTypes = null): ?UserRegistration
    {
        $queryBuilder = $this->createFindByAccountQueryBuilder(account: $account, withoutInvitation: false, userRegistrationTypes: $userRegistrationTypes);

        $queryBuilder
            ->andWhere('userRegistration.id = :id')
            ->setParameter(key: 'id', value: $id);

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }

    public function findOneByValidActivationToken(string $activationToken): ?UserRegistration
    {
        $queryBuilder = $this->createQueryBuilder(alias: 'userRegistration');

        $queryBuilder
            ->where(predicates: 'userRegistration.activationToken = :activationToken')
            ->andWhere('userRegistration.expirationDate > :expirationDate')
            ->setParameter(key: 'activationToken', value: $activationToken)
            ->setParameter(key: 'expirationDate', value: new \DateTime());

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }

    /**
     * @return UserRegistration[]
     */
    public function findExpiredInactiveUserRegistrationsFromAllAccounts(): array
    {
        $queryBuilder = $this->createQueryBuilder(alias: 'userRegistration');

        $queryBuilder
            ->where(predicates: 'userRegistration.expirationDate < :now')
            ->andWhere('userRegistration.expirationDate IS NOT NULL')
            ->andWhere('userRegistration.activationToken IS NOT NULL')
            ->andWhere('userRegistration.userRegistrationStatus != :userRegistrationStatus')
            ->setParameter(key: 'now', value:  new \DateTime())
            ->setParameter(key: 'userRegistrationStatus' ,value: UserRegistrationStatus::ACTIVE);

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * @param UserRegistrationType[]|null $userRegistrationTypes
     * @param UserRegistrationStatus[]|null $userRegistrationStatus
     */
    public function countByAccount(
        Account $account,
        ?array $userRegistrationTypes = null,
        ?array $userRegistrationStatus = null,
    ): int {
        $queryBuilder = $this->createFindByAccountQueryBuilder(account: $account, withoutInvitation: false, userRegistrationTypes: $userRegistrationTypes);

        $queryBuilder->select(select: 'COUNT(DISTINCT userRegistration.id)');

        if (empty($userRegistrationStatus) === false) {
            $queryBuilder
                ->andWhere('userRegistration.userRegistrationStatus IN (:userRegistrationStatus)')
                ->setParameter(key: 'userRegistrationStatus', value: $userRegistrationStatus);
        }

        return $queryBuilder->getQuery()->getSingleScalarResult();
    }

    /**
     * @param UserRegistrationType[]|null $userRegistrationTypes
     */
    private function createFindByAccountQueryBuilder(
        Account $account,
        bool $withoutInvitation,
        ?array $userRegistrationTypes = null,
        ?UserRegistrationFilter $userRegistrationFilter = null,
        ?UserRegistrationSearch $userRegistrationSearch = null,
        ?SortingOption $sortingOption = null
    ): QueryBuilder {
        $queryBuilder = $this->createQueryBuilder(alias: 'userRegistration');

        $queryBuilder
            ->addSelect(select: 'account')
            ->addSelect(select: 'userInvitation')
            ->innerJoin(
                join: 'userRegistration.account',
                alias: 'account',
                conditionType: Join::WITH,
                condition: 'account = :account AND account.deleted = false AND account.enabled = true'
            )
            ->leftJoin(join: 'userRegistration.userInvitation', alias: 'userInvitation')
            ->where(predicates: 'userRegistration.account = account')
            ->setParameter(key: 'account', value: $account);

        if ($userRegistrationFilter !== null && empty($userRegistrationFilter->getPersonTypes()) === false) {
            $queryBuilder
                ->andWhere('userRegistration.personType IN (:personTypes)')
                ->setParameter(key: 'personTypes', value:  $userRegistrationFilter->getPersonTypes());
        }

        if ($withoutInvitation === true) {
            $queryBuilder->andWhere('userRegistration.userInvitation IS NULL');
        }

        if (empty($userRegistrationTypes) === false) {
            $queryBuilder
                ->andWhere('userRegistration.userRegistrationType IN (:userRegistrationTypes)')
                ->setParameter(key: 'userRegistrationTypes', value: $userRegistrationTypes);
        }

        if ($userRegistrationSearch !== null && empty($userRegistrationSearch->getText()) === false) {
            $expression = $queryBuilder->expr()->orX();

            $expression
                ->add('userRegistration.email LIKE :searchText')
                ->add('userRegistration.lastName LIKE :searchText')
                ->add('userRegistration.firstName LIKE :searchText')
                ->add('userRegistration.personType = :personType AND userRegistration.companyName LIKE :searchText');

            $queryBuilder
                ->andWhere($expression)
                ->setParameter(key: 'searchText', value: '%' . $userRegistrationSearch->getText() . '%')
                ->setParameter(key: 'personType', value: PersonType::COMPANY);
        }

        if ($sortingOption !== null) {
            self::applyUserRegistrationSortingToQueryBuilder(sortingOption: $sortingOption, queryBuilder: $queryBuilder);
        }

        return $queryBuilder;
    }

    private static function applyUserRegistrationSortingToQueryBuilder(SortingOption $sortingOption, QueryBuilder $queryBuilder): void
    {
        if ($sortingOption->getSortingBy() === 'vorname') {
            $queryBuilder->orderBy(sort: 'userRegistration.firstName', order: $sortingOption->getSortingDirection()->getOrderByDirection());
        }

        if ($sortingOption->getSortingBy() === 'nachname') {
            $queryBuilder->orderBy(sort: 'userRegistration.lastName', order: $sortingOption->getSortingDirection()->getOrderByDirection());
        }

        if ($sortingOption->getSortingBy() === 'firma') {
            $queryBuilder->orderBy(sort: 'userRegistration.companyName', order: $sortingOption->getSortingDirection()->getOrderByDirection());
        }

        if ($sortingOption->getSortingBy() === 'email') {
            $queryBuilder->orderBy(sort: 'userRegistration.email', order: $sortingOption->getSortingDirection()->getOrderByDirection());
        }

    }
}
