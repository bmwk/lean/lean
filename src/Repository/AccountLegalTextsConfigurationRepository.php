<?php

declare(strict_types=1);

namespace App\Repository;

use App\Domain\Entity\Account;
use App\Domain\Entity\AccountLegalTextsConfiguration;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AccountLegalTextsConfiguration|null find($id, $lockMode = null, $lockVersion = null)
 * @method AccountLegalTextsConfiguration|null findOneBy(array $criteria, array $orderBy = null)
 * @method AccountLegalTextsConfiguration[]    findAll()
 * @method AccountLegalTextsConfiguration[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AccountLegalTextsConfigurationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, entityClass: AccountLegalTextsConfiguration::class);
    }

    public function findByAccount(Account $account): ?AccountLegalTextsConfiguration
    {
        $queryBuilder = $this->createFindByAccountQueryBuilder(account: $account);

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }

    private function createFindByAccountQueryBuilder(Account $account): QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder(alias: 'accountLegalTextsConfiguration');

        $queryBuilder
            ->innerJoin(
                join: 'accountLegalTextsConfiguration.account',
                alias: 'account',
                conditionType: JOIN::WITH,
                condition: 'account = :account AND account.deleted = false AND account.enabled = true'
            )
            ->where(predicates: 'accountLegalTextsConfiguration.account = account')
            ->setParameter(key: 'account', value: $account);

        return $queryBuilder;
    }
}
