<?php

declare(strict_types=1);

namespace App\Repository;

use App\Domain\Entity\Account;
use App\Domain\Entity\ZielbildcheckConfiguration;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ZielbildcheckConfiguration|null find($id, $lockMode = null, $lockVersion = null)
 * @method ZielbildcheckConfiguration|null findOneBy(array $criteria, array $orderBy = null)
 * @method ZielbildcheckConfiguration[]    findAll()
 * @method ZielbildcheckConfiguration[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ZielbildcheckConfigurationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ZielbildcheckConfiguration::class);
    }

    public function findOneByAccount(Account $account): ?ZielbildcheckConfiguration
    {
        return $this->findOneBy(criteria: ['account' => $account]);
    }
}
