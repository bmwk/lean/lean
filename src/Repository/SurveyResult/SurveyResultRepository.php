<?php

declare(strict_types=1);

namespace App\Repository\SurveyResult;

use App\Domain\Entity\SurveyResult\SurveyResult;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SurveyResult|null find($id, $lockMode = null, $lockVersion = null)
 * @method SurveyResult|null findOneBy(array $criteria, array $orderBy = null)
 * @method SurveyResult[]    findAll()
 * @method SurveyResult[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SurveyResultRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SurveyResult::class);
    }
}
