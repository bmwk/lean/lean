<?php

declare(strict_types=1);

namespace App\Repository\SurveyResult;

use App\Domain\Entity\SurveyResult\SurveyResultChapter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SurveyResultChapter|null find($id, $lockMode = null, $lockVersion = null)
 * @method SurveyResultChapter|null findOneBy(array $criteria, array $orderBy = null)
 * @method SurveyResultChapter[]    findAll()
 * @method SurveyResultChapter[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SurveyResultChapterRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SurveyResultChapter::class);
    }
}
