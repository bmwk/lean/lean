<?php

declare(strict_types=1);

namespace App\Repository;

use App\Domain\Entity\LookingForPropertyRequest\ScoreCriteria;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ScoreCriteria|null find($id, $lockMode = null, $lockVersion = null)
 * @method ScoreCriteria|null findOneBy(array $criteria, array $orderBy = null)
 * @method ScoreCriteria[]    findAll()
 * @method ScoreCriteria[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ScoreCriteriaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ScoreCriteria::class);
    }
}
