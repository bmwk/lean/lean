<?php

declare(strict_types=1);

namespace App\Repository;

use App\Domain\Entity\GeolocationPoint;
use App\Domain\Entity\Place;
use App\Domain\Entity\PlaceRelationType;
use App\Domain\Entity\PlaceType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Uid\Uuid;

/**
 * @method Place|null find($id, $lockMode = null, $lockVersion = null)
 * @method Place|null findOneBy(array $criteria, array $orderBy = null)
 * @method Place[]    findAll()
 * @method Place[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlaceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Place::class);
    }

    public function findOneByPlaceName(string $placeName): ?Place
    {
        return $this->findOneBy(criteria: ['placeName' => $placeName]);
    }

    public function findOneByUuid(Uuid $uuid): ?Place
    {
        return $this->findOneBy(criteria: ['uuid' => $uuid]);
    }

    /**
     * @return Place[]
     */
    public function findByPlaceType(PlaceType $placeType): array
    {
        return $this->findBy(criteria: ['placeType' => $placeType]);
    }

    /**
     * @param PlaceType[] $placeTypes
     */
    public function findOnePlaceByIdAndChildPlacesByPlaceTypes(int $id, array $placeTypes): ?Place
    {
        $queryBuilder = $this->createQueryBuilder(alias: 'place');

        $queryBuilder
            ->addSelect(select: 'childrenPlace')
            ->addSelect(select: 'parentPlace')
            ->leftJoin(
                join: 'place.childrenPlaces',
                alias: 'childrenPlace',
                conditionType: Join::WITH,
                condition: 'childrenPlace.placeType IN (:placeTypes)'
            )
            ->leftJoin(join: 'place.parentPlaces', alias: 'parentPlace')
            ->where(predicates: 'place.id = :id')
            ->andWhere('childrenPlace.id IS NOT NULL')
            ->setParameter(key: 'placeTypes', value: $placeTypes)
            ->setParameter(key: 'id', value: $id);

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }

    /**
     * @param PlaceType[] $placeTypes
     * @return Place[]
     */
    public function findChildPlacesByPlaceTypes(Place $place, array $placeTypes): array
    {
        $queryBuilder = $this->createQueryBuilder(alias: 'place');

        $queryBuilder
            ->innerJoin(
                join: 'place.parentPlaces',
                alias: 'parentPlace',
                conditionType: Join::WITH,
                condition: 'parentPlace = :place'
            )
            ->where(predicates: 'place.placeType IN (:placeTypes)')
            ->orderBy(sort: 'place.placeName', order: 'ASC')
            ->setParameter(key: 'place', value: $place)
            ->setParameter(key: 'placeTypes', value: $placeTypes);

        return $queryBuilder->getQuery()->getResult();
    }

    public function findRecursiveByPlacesAndPlaceRelationTypeAndPlaceTypes(
        array $places,
        array $placeTypes,
        PlaceRelationType $placeRelationType = PlaceRelationType::CHILD
    ): array {
        if (empty($places) === true) {
            return [];
        }

        $resultSetMapping = new ResultSetMapping();
        $resultSetMapping->addEntityResult(class: Place::class, alias: 'childrenPlaces');
        $resultSetMapping->addFieldResult(alias: 'childrenPlaces', columnName: 'id', fieldName: 'id');
        $resultSetMapping->addFieldResult(alias: 'childrenPlaces', columnName: 'uuid', fieldName: 'uuid');
        $resultSetMapping->addFieldResult(alias: 'childrenPlaces', columnName: 'place_name', fieldName: 'placeName');
        $resultSetMapping->addFieldResult(alias: 'childrenPlaces', columnName: 'place_short_name', fieldName: 'placeShortName');
        $resultSetMapping->addFieldResult(alias: 'childrenPlaces', columnName: 'place_type', fieldName: 'placeType');
        $resultSetMapping->addFieldResult(alias: 'childrenPlaces', columnName: 'created_at', fieldName: 'createdAt');
        $resultSetMapping->addFieldResult(alias: 'childrenPlaces', columnName: 'updated_at', fieldName: 'updatedAt');
        $resultSetMapping->addJoinedEntityResult(class: GeolocationPoint::class, alias: 'geolocationPoint', parentAlias: 'childrenPlaces', relation: 'geolocationPoint');
        $resultSetMapping->addFieldResult(alias: 'geolocationPoint', columnName: 'geolocation_point_id', fieldName: 'id');
        $resultSetMapping->addFieldResult(alias: 'geolocationPoint', columnName: 'point', fieldName: 'point');
        $resultSetMapping->addFieldResult(alias: 'geolocationPoint', columnName: 'created_at', fieldName: 'createdAt');
        $resultSetMapping->addFieldResult(alias: 'geolocationPoint', columnName: 'updated_at', fieldName: 'updatedAt');

        if ($placeRelationType === PlaceRelationType::CHILD) {
            $sql = <<<SQL
                WITH RECURSIVE recursive_places AS (
                    SELECT * FROM place WHERE id IN (:ids)
                    UNION
                        SELECT place.*
                        FROM place, parent_place_children_place, recursive_places
                        WHERE place.id = parent_place_children_place.child_place_id
                        AND recursive_places.id = parent_place_children_place.parent_place_id
                )
                SQL;
        } else {
            $sql = <<<SQL
                WITH RECURSIVE recursive_places AS (
                    SELECT * FROM place WHERE id IN (:ids)
                    UNION
                        SELECT place.*
                        FROM place, parent_place_children_place, recursive_places
                        WHERE place.id = parent_place_children_place.parent_place_id 
                        AND recursive_places.id = parent_place_children_place.child_place_id
                )
                SQL;
        }

        $sql.= <<<SQL
            SELECT
                recursivePlaces.id,
                recursivePlaces.uuid,
                recursivePlaces.place_name,
                recursivePlaces.place_short_name,
                recursivePlaces.place_type,
                recursivePlaces.created_at,
                recursivePlaces.updated_at,
                geolocationPoint.id AS geolocation_point_id,
                geolocationPoint.point,
                geolocationPoint.created_at,
                geolocationPoint.updated_at
            FROM recursive_places AS recursivePlaces
            LEFT JOIN geolocation_point geolocationPoint ON recursivePlaces.geolocation_point_id = geolocationPoint.id
            WHERE place_type IN (:placeTypes)
        SQL;

        $query = $this->getEntityManager()->createNativeQuery(sql: $sql, rsm: $resultSetMapping);

        $query
            ->setParameter(
                key: 'ids',
                value: array_map(
                    callback: function (Place $place): int {
                        return $place->getId();
                    },
                    array: $places
                ),
                type: Connection::PARAM_INT_ARRAY)
            ->setParameter(
                key: 'placeTypes',
                value: array_map(
                    callback: function (PlaceType $placeType): int {
                        return $placeType->value;
                    },
                    array: $placeTypes
                ),
                type: Connection::PARAM_INT_ARRAY);

        return $query->getResult();
    }
}
