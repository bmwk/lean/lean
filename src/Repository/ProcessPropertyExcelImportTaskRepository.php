<?php

declare(strict_types=1);

namespace App\Repository;

use App\Domain\Entity\Account;
use App\Domain\Entity\ProcessPropertyExcelImportTask;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProcessPropertyExcelImportTask|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProcessPropertyExcelImportTask|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProcessPropertyExcelImportTask[]    findAll()
 * @method ProcessPropertyExcelImportTask[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProcessPropertyExcelImportTaskRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProcessPropertyExcelImportTask::class);
    }

    /**
     * @return ProcessPropertyExcelImportTask[]
     */
    public function findByAccount(Account $account): array
    {
        return $this->findBy(criteria: ['account' => $account]);
    }
}
