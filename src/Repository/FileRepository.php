<?php

declare(strict_types=1);

namespace App\Repository;

use App\Domain\Entity\Account;
use App\Domain\Entity\File;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Uid\Uuid;

/**
 * @method File|null find($id, $lockMode = null, $lockVersion = null)
 * @method File|null findOneBy(array $criteria, array $orderBy = null)
 * @method File[]    findAll()
 * @method File[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FileRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, File::class);
    }

    public function findOneByUuid(Uuid $uuid): ?File
    {
        return $this->findOneBy(criteria: ['uuid' => $uuid]);
    }

    public function findOneByUuidAndAccount(Uuid $uuid, Account $account): ?File
    {
        return $this->findOneBy(criteria: ['uuid' => $uuid, 'account' => $account]);
    }

    public function findOneByUuidAndPublic(Uuid $uuid, bool $public): ?File
    {
        return $this->findOneBy(criteria: ['uuid' => $uuid, 'public' => $public]);
    }
}
