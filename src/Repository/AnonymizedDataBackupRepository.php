<?php

namespace App\Repository;

use App\Domain\Entity\AnonymizedDataBackup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AnonymizedDataBackup|null find($id, $lockMode = null, $lockVersion = null)
 * @method AnonymizedDataBackup|null findOneBy(array $criteria, array $orderBy = null)
 * @method AnonymizedDataBackup[]    findAll()
 * @method AnonymizedDataBackup[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AnonymizedDataBackupRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AnonymizedDataBackup::class);
    }

    /**
     * @return AnonymizedDataBackup[]
     */
    public function findByCreatedAtOlderThanTimeTillDelete(int $timeTillDelete): array
    {
        $queryBuilder = $this->createQueryBuilder(alias: 'anonymizedDataBackup');

        $deleteFromDateTime = new \DateTime(strtolower(string: '-' . $timeTillDelete . ' days'));

        $queryBuilder
            ->where('anonymizedDataBackup.createdAt <= :deleteFromDateTime')
            ->setParameter(key: 'deleteFromDateTime', value: $deleteFromDateTime);

        return $queryBuilder->getQuery()->getResult();
    }
}
