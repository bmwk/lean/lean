<?php

declare(strict_types=1);

namespace App\Repository;

use App\Domain\Entity\Account;
use App\Domain\Entity\HallOfInspiration\HallOfInspirationConfiguration;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method HallOfInspirationConfiguration|null find($id, $lockMode = null, $lockVersion = null)
 * @method HallOfInspirationConfiguration|null findOneBy(array $criteria, array $orderBy = null)
 * @method HallOfInspirationConfiguration[]    findAll()
 * @method HallOfInspirationConfiguration[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HallOfInspirationConfigurationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HallOfInspirationConfiguration::class);
    }

    public function findOneByAccount(Account $account): ?HallOfInspirationConfiguration
    {
        return $this->findOneBy(criteria: ['account' => $account]);
    }
}
