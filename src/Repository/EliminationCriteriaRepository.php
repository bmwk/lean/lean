<?php

declare(strict_types=1);

namespace App\Repository;

use App\Domain\Entity\EliminationCriteria;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method EliminationCriteria|null find($id, $lockMode = null, $lockVersion = null)
 * @method EliminationCriteria|null findOneBy(array $criteria, array $orderBy = null)
 * @method EliminationCriteria[]    findAll()
 * @method EliminationCriteria[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EliminationCriteriaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EliminationCriteria::class);
    }
}
