<?php

declare(strict_types=1);

namespace App\Repository\Classification;

use App\Domain\Entity\Classification\IndustryClassification;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method IndustryClassification|null find($id, $lockMode = null, $lockVersion = null)
 * @method IndustryClassification|null findOneBy(array $criteria, array $orderBy = null)
 * @method IndustryClassification[]    findAll()
 * @method IndustryClassification[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IndustryClassificationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, IndustryClassification::class);
    }

    /**
     * @return IndustryClassification[]
     */
    public function findSecondLevelIndustryClassifications(): array
    {
        $queryBuilder = $this->createQueryBuilder(alias: 'industryClassification');

        $queryBuilder
            ->where($queryBuilder->expr()->isNotNull('industryClassification.levelOne'))
            ->andWhere($queryBuilder->expr()->isNotNull('industryClassification.levelTwo'))
            ->andWhere($queryBuilder->expr()->isNull('industryClassification.levelThree'));

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * @return IndustryClassification[]
     */
    public function findSecondLevelIndustryClassificationsByParent(IndustryClassification $industryClassificationParent): array
    {
        $queryBuilder = $this->createQueryBuilder(alias: 'industryClassification');

        $queryBuilder
            ->where('industryClassification.parent = :industryClassificationParent')
            ->andWhere($queryBuilder->expr()->isNotNull('industryClassification.levelOne'))
            ->andWhere($queryBuilder->expr()->isNotNull('industryClassification.levelTwo'))
            ->andWhere($queryBuilder->expr()->isNull('industryClassification.levelThree'))
            ->setParameter(':industryClassificationParent', $industryClassificationParent);

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * @return IndustryClassification[]
     */
    public function findThirdLevelIndustryClassifications(): array
    {
        $queryBuilder = $this->createQueryBuilder(alias: 'industryClassification');

        $queryBuilder
            ->where($queryBuilder->expr()->isNotNull('industryClassification.levelOne'))
            ->andWhere($queryBuilder->expr()->isNotNull('industryClassification.levelTwo'))
            ->andWhere($queryBuilder->expr()->isNotNull('industryClassification.levelThree'));

        return $queryBuilder->getQuery()->getResult();
    }
}
