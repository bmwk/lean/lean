<?php

declare(strict_types=1);

namespace App\Repository\Classification;

use App\Domain\Entity\Classification\NaceClassification;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method NaceClassification|null find($id, $lockMode = null, $lockVersion = null)
 * @method NaceClassification|null findOneBy(array $criteria, array $orderBy = null)
 * @method NaceClassification[]    findAll()
 * @method NaceClassification[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NaceClassificationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, NaceClassification::class);
    }
}
