<?php

declare(strict_types=1);

namespace App\Repository;

use App\Domain\Entity\PriceRequirement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PriceRequirement|null find($id, $lockMode = null, $lockVersion = null)
 * @method PriceRequirement|null findOneBy(array $criteria, array $orderBy = null)
 * @method PriceRequirement[]    findAll()
 * @method PriceRequirement[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PriceRequirementRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PriceRequirement::class);
    }
}
