<?php

declare(strict_types=1);

namespace App\Repository\OpenImmoFtpTransfer;

use App\Domain\Entity\OpenImmoFtpTransfer\OpenImmoTransferLog;
use App\Domain\Entity\Property\BuildingUnit;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OpenImmoTransferLog|null find($id, $lockMode = null, $lockVersion = null)
 * @method OpenImmoTransferLog|null findOneBy(array $criteria, array $orderBy = null)
 * @method OpenImmoTransferLog[]    findAll()
 * @method OpenImmoTransferLog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OpenImmoTransferLogRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OpenImmoTransferLog::class);
    }

    /**
     * @return OpenImmoTransferLog[]
     */
    public function findByBuildingUnit(BuildingUnit $buildingUnit): array
    {
        $queryBuilder = $this->createQueryBuilder(alias: 'openImmoTransferLog');

        $queryBuilder
            ->addSelect(select: 'openImmoFtpCredential')
            ->innerJoin(
                join: 'openImmoTransferLog.buildingUnit',
                alias: 'buildingUnit',
                conditionType: Join::WITH,
                condition: 'buildingUnit.deleted = false'
            )
            ->innerJoin(join: 'openImmoTransferLog.openImmoFtpCredential', alias: 'openImmoFtpCredential')
            ->where(predicates: 'openImmoTransferLog.buildingUnit = :buildingUnit')
            ->orderBy(sort: 'openImmoTransferLog.transferredAt', order: 'DESC')
            ->setParameter(key: 'buildingUnit', value: $buildingUnit);

        return $queryBuilder->getQuery()->getResult();
    }
}
