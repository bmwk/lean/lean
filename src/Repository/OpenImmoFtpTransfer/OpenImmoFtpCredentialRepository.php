<?php

declare(strict_types=1);

namespace App\Repository\OpenImmoFtpTransfer;

use App\Domain\Entity\OpenImmoFtpTransfer\OpenImmoFtpCredential;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OpenImmoFtpCredential|null find($id, $lockMode = null, $lockVersion = null)
 * @method OpenImmoFtpCredential|null findOneBy(array $criteria, array $orderBy = null)
 * @method OpenImmoFtpCredential[]    findAll()
 * @method OpenImmoFtpCredential[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OpenImmoFtpCredentialRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OpenImmoFtpCredential::class);
    }
}
