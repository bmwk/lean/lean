<?php

namespace App\Repository\OpenImmoFtpTransfer;

use App\Domain\Entity\Account;
use App\Domain\Entity\OpenImmoFtpTransfer\OpenImmoFtpTransferTask;
use App\Domain\Entity\TaskStatus;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OpenImmoFtpTransferTask|null find($id, $lockMode = null, $lockVersion = null)
 * @method OpenImmoFtpTransferTask|null findOneBy(array $criteria, array $orderBy = null)
 * @method OpenImmoFtpTransferTask[]    findAll()
 * @method OpenImmoFtpTransferTask[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OpenImmoFtpTransferTaskRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OpenImmoFtpTransferTask::class);
    }

    public function findOneById(int $id): ?OpenImmoFtpTransferTask
    {
        return $this->find(id: $id);
    }

    /**
     * @return OpenImmoFtpTransferTask[]
     */
    public function findByTasksStatus(TaskStatus $taskStatus, int $limit): array
    {
        return $this->findBy(criteria: ['taskStatus' => $taskStatus], limit:  $limit);
    }

    /**
     * @return OpenImmoFtpTransferTask[]
     */
    public function findByAccount(Account $account): array
    {
        return $this->findBy(criteria: ['account' => $account]);
    }
}
