<?php

declare(strict_types=1);

namespace App\Repository;

use App\Domain\Entity\EliminationCriterion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method EliminationCriterion|null find($id, $lockMode = null, $lockVersion = null)
 * @method EliminationCriterion|null findOneBy(array $criteria, array $orderBy = null)
 * @method EliminationCriterion[]    findAll()
 * @method EliminationCriterion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EliminationCriterionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EliminationCriterion::class);
    }
}
