<?php

declare(strict_types=1);

namespace App\Repository;

use App\Domain\Entity\GeolocationMultiPolygon;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method GeolocationMultiPolygon|null find($id, $lockMode = null, $lockVersion = null)
 * @method GeolocationMultiPolygon|null findOneBy(array $criteria, array $orderBy = null)
 * @method GeolocationMultiPolygon[]    findAll()
 * @method GeolocationMultiPolygon[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GeolocationMultiPolygonRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, GeolocationMultiPolygon::class);
    }
}
