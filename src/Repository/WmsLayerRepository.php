<?php

declare(strict_types=1);

namespace App\Repository;

use App\Domain\Entity\WmsLayer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method WmsLayer|null find($id, $lockMode = null, $lockVersion = null)
 * @method WmsLayer|null findOneBy(array $criteria, array $orderBy = null)
 * @method WmsLayer[]    findAll()
 * @method WmsLayer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WmsLayerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, WmsLayer::class);
    }
}
