<?php

declare(strict_types=1);

namespace App\Repository;

use App\Domain\Entity\PropertyMandatoryRequirement;
use App\Domain\Entity\ValueRequirement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PropertyMandatoryRequirement|null find($id, $lockMode = null, $lockVersion = null)
 * @method PropertyMandatoryRequirement|null findOneBy(array $criteria, array $orderBy = null)
 * @method ValueRequirement[]                findAll()
 * @method ValueRequirement[]                findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PropertyMandatoryRequirementRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PropertyMandatoryRequirement::class);
    }
}
