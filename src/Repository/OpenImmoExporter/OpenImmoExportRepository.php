<?php

namespace App\Repository\OpenImmoExporter;

use App\Domain\Entity\OpenImmoExporter\ExportStatus;
use App\Domain\Entity\OpenImmoExporter\OpenImmoExport;
use App\Domain\Entity\Property\BuildingUnit;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OpenImmoExport|null find($id, $lockMode = null, $lockVersion = null)
 * @method OpenImmoExport|null findOneBy(array $criteria, array $orderBy = null)
 * @method OpenImmoExport[]    findAll()
 * @method OpenImmoExport[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OpenImmoExportRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OpenImmoExport::class);
    }

    public function findByExportStatus(ExportStatus $exportStatus, int $limit): array
    {
        return $this->findBy(criteria: ['exportStatus' => $exportStatus], limit: $limit);
    }

    public function findOneByBuildingUnit(BuildingUnit $buildingUnit): ?OpenImmoExport
    {
        return $this->findOneBy(criteria: ['buildingUnit' => $buildingUnit]);
    }
}
