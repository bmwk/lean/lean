<?php

declare(strict_types=1);

namespace App\Repository;

use App\Domain\Entity\BuildingUnitFeedback;
use App\Domain\Entity\Feedback;
use App\Domain\Entity\Property\BuildingUnit;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BuildingUnitFeedback|null find($id, $lockMode = null, $lockVersion = null)
 * @method BuildingUnitFeedback|null findOneBy(array $criteria, array $orderBy = null)
 * @method BuildingUnitFeedback[]    findAll()
 * @method BuildingUnitFeedback[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BuildingUnitFeedbackRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BuildingUnitFeedback::class);
    }

    /**
     * @return BuildingUnitFeedback[]
     */
    public function findByBuildingUnit(BuildingUnit $buildingUnit): array
    {
        return $this->findBy(criteria: [
            'buildingUnit' => $buildingUnit,
        ]);
    }

    public function findOneByBuildingUnitAndFeedback(
        BuildingUnit $buildingUnit,
        Feedback $feedback
    ): ?BuildingUnitFeedback {
        return $this->findOneBy(criteria: [
            'buildingUnit' => $buildingUnit,
            'feedback'     => $feedback
        ]);
    }
}
