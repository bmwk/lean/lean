<?php

declare(strict_types=1);

namespace App\Repository\LookingForPropertyRequestReport;

use App\Domain\Entity\Account;
use App\Domain\Entity\LookingForPropertyRequestReport\LookingForPropertyRequestReport;
use App\Domain\Entity\LookingForPropertyRequestReport\LookingForPropertyRequestReportFilter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method LookingForPropertyRequestReport|null find($id, $lockMode = null, $lockVersion = null)
 * @method LookingForPropertyRequestReport|null findOneBy(array $criteria, array $orderBy = null)
 * @method LookingForPropertyRequestReport[]    findAll()
 * @method LookingForPropertyRequestReport[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LookingForPropertyRequestReportRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LookingForPropertyRequestReport::class);
    }

    /**
     * @return LookingForPropertyRequestReport[]
     */
    public function findByAccount(
        Account $account,
        ?bool $processed = null,
        ?bool $emailConfirmed = null,
        ?LookingForPropertyRequestReportFilter $lookingForPropertyRequestReportFilter = null
    ): array {
        $queryBuilder = $this->createFindByAccountQueryBuilder(
            account: $account,
            processed: $processed,
            emailConfirmed: $emailConfirmed,
            lookingForPropertyRequestReportFilter: $lookingForPropertyRequestReportFilter
        );

        return $queryBuilder->getQuery()->getResult();
    }

    public function countByCreatedAtRange(
        Account $account,
        \DateTime $dateTimeStart,
        \DateTime $dateTimeEnd,
        ?bool $processed = null,
        ?bool $emailConfirmed = null,
        ?LookingForPropertyRequestReportFilter $lookingForPropertyRequestReportFilter = null
    ): int {
        $queryBuilder = $this->createFindByAccountQueryBuilder(
            account: $account,
            processed: $processed,
            emailConfirmed: $emailConfirmed,
            lookingForPropertyRequestReportFilter: $lookingForPropertyRequestReportFilter
        );

        $queryBuilder
            ->select(select: 'COUNT(DISTINCT lookingForPropertyRequestReport.id)')
            ->andWhere('lookingForPropertyRequestReport.createdAt BETWEEN :dateTimeStart AND :dateTimeEnd')
            ->setParameter(key: 'dateTimeStart', value: $dateTimeStart)
            ->setParameter(key: 'dateTimeEnd', value: $dateTimeEnd);

        return $queryBuilder->getQuery()->getSingleScalarResult();
    }

    private function createFindByAccountQueryBuilder(
        Account $account,
        ?bool $processed = null,
        ?bool $emailConfirmed = null,
        ?LookingForPropertyRequestReportFilter $lookingForPropertyRequestReportFilter = null
    ): QueryBuilder {
        $queryBuilder = $this->createQueryBuilder(alias: 'lookingForPropertyRequestReport');

        $queryBuilder
            ->innerJoin(
                join: 'lookingForPropertyRequestReport.account',
                alias: 'account',
                conditionType: Join::WITH,
                condition: 'account = :account AND account.deleted = false AND account.enabled = true'
            )
            ->leftJoin(join: 'lookingForPropertyRequestReport.propertyRequirement', alias: 'propertyRequirement')
            ->leftJoin(join: 'propertyRequirement.industryClassification', alias: 'industryClassification')
            ->where(predicates: 'lookingForPropertyRequestReport.deleted = false')
            ->setParameter(key: 'account', value: $account);

        if ($processed !== null) {
            $queryBuilder
                ->andWhere('lookingForPropertyRequestReport.processed = :processed')
                ->setParameter(key: 'processed', value: $processed);
        }

        if ($emailConfirmed !== null) {
            $queryBuilder
                ->andWhere('lookingForPropertyRequestReport.emailConfirmed = :emailConfirmed')
                ->setParameter(key: 'emailConfirmed', value: $emailConfirmed);
        }

        if ($lookingForPropertyRequestReportFilter !== null) {
            if (empty($lookingForPropertyRequestReportFilter->getIndustryClassifications()) === false) {
                $queryBuilder
                    ->andWhere('industryClassification.levelOne IN (:industryClassifications)')
                    ->setParameter(key: 'industryClassifications', value: $lookingForPropertyRequestReportFilter->getIndustryClassifications());
            }
        }

        return $queryBuilder;
    }
}
