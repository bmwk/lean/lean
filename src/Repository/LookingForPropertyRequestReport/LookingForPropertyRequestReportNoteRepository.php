<?php

declare(strict_types=1);

namespace App\Repository\LookingForPropertyRequestReport;

use App\Domain\Entity\LookingForPropertyRequestReport\LookingForPropertyRequestReportNote;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method LookingForPropertyRequestReportNote|null find($id, $lockMode = null, $lockVersion = null)
 * @method LookingForPropertyRequestReportNote|null findOneBy(array $criteria, array $orderBy = null)
 * @method LookingForPropertyRequestReportNote[]    findAll()
 * @method LookingForPropertyRequestReportNote[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LookingForPropertyRequestReportNoteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LookingForPropertyRequestReportNote::class);
    }
}
