<?php

declare(strict_types=1);

namespace App\Repository;

use App\Domain\Entity\Account;
use App\Domain\Entity\PropertyVacancyReporter\PropertyVacancyReporterConfiguration;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PropertyVacancyReporterConfiguration|null find($id, $lockMode = null, $lockVersion = null)
 * @method PropertyVacancyReporterConfiguration|null findOneBy(array $criteria, array $orderBy = null)
 * @method PropertyVacancyReporterConfiguration[]    findAll()
 * @method PropertyVacancyReporterConfiguration[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PropertyVacancyReporterConfigurationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PropertyVacancyReporterConfiguration::class);
    }

    public function findOneByAccount(Account $account): ?PropertyVacancyReporterConfiguration
    {
        return $this->findOneBy(criteria: ['account' => $account]);
    }
}
