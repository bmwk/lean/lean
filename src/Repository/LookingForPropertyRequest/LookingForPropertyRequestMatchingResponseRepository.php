<?php

declare(strict_types=1);

namespace App\Repository\LookingForPropertyRequest;

use App\Domain\Entity\Account;
use App\Domain\Entity\LookingForPropertyRequest\LookingForPropertyRequest;
use App\Domain\Entity\LookingForPropertyRequest\LookingForPropertyRequestMatchingResponse;
use App\Domain\Entity\Property\BuildingUnit;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method LookingForPropertyRequestMatchingResponse|null find($id, $lockMode = null, $lockVersion = null)
 * @method LookingForPropertyRequestMatchingResponse|null findOneBy(array $criteria, array $orderBy = null)
 * @method LookingForPropertyRequestMatchingResponse[]    findAll()
 * @method LookingForPropertyRequestMatchingResponse[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LookingForPropertyRequestMatchingResponseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LookingForPropertyRequestMatchingResponse::class);
    }

    public function findOneByLookingForPropertyRequestAndBuildingUnit(
        Account $account,
        LookingForPropertyRequest $lookingForPropertyRequest,
        BuildingUnit $buildingUnit
    ): ?LookingForPropertyRequestMatchingResponse {
        $queryBuilder = $this->createQueryBuilder(alias: 'lookingForPropertyRequestMatchingResponse');

        $queryBuilder
            ->innerJoin(
                join: 'lookingForPropertyRequestMatchingResponse.lookingForPropertyRequest',
                alias: 'lookingForPropertyRequest',
                conditionType: Join::WITH,
                condition: 'lookingForPropertyRequest.deleted = false'
            )
            ->innerJoin(
                join: 'lookingForPropertyRequest.account',
                alias: 'lookingForPropertyRequestAccount',
                conditionType: Join::WITH,
                condition: '(lookingForPropertyRequestAccount.parentAccount = :account OR lookingForPropertyRequestAccount = :account) AND lookingForPropertyRequestAccount.deleted = false AND lookingForPropertyRequestAccount.enabled = true'
            )
            ->innerJoin(
                join: 'lookingForPropertyRequestMatchingResponse.buildingUnit',
                alias: 'buildingUnit',
                conditionType: Join::WITH,
                condition: 'buildingUnit.deleted = false'
            )
            ->innerJoin(
                join: 'buildingUnit.account',
                alias: 'buildingUnitAccount',
                conditionType: Join::WITH,
                condition: '(buildingUnitAccount.parentAccount = :account OR buildingUnitAccount = :account) AND buildingUnitAccount.deleted = false AND buildingUnitAccount.enabled = true'
            )
            ->where(predicates: 'lookingForPropertyRequest = :lookingForPropertyRequest')
            ->andWhere('buildingUnit = :buildingUnit')
            ->setParameter(key: 'account', value: $account)
            ->setParameter(key: 'lookingForPropertyRequest', value: $lookingForPropertyRequest)
            ->setParameter(key: 'buildingUnit', value: $buildingUnit);

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }
}
