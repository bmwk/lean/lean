<?php

declare(strict_types=1);

namespace App\Repository\LookingForPropertyRequest;

use App\Domain\Entity\LookingForPropertyRequest\MatchingTask;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MatchingTask|null find($id, $lockMode = null, $lockVersion = null)
 * @method MatchingTask|null findOneBy(array $criteria, array $orderBy = null)
 * @method MatchingTask[]    findAll()
 * @method MatchingTask[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MatchingTaskRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MatchingTask::class);
    }
}
