<?php

declare(strict_types=1);

namespace App\Repository\LookingForPropertyRequest;

use App\Domain\Entity\Account;
use App\Domain\Entity\LookingForPropertyRequest\MatchingResult;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MatchingResult|null find($id, $lockMode = null, $lockVersion = null)
 * @method MatchingResult|null findOneBy(array $criteria, array $orderBy = null)
 * @method MatchingResult[]    findAll()
 * @method MatchingResult[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MatchingResultRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MatchingResult::class);
    }

    public function countGroupedByBuildingUnitAndEliminated(Account $account): array
    {
        $queryBuilder = $this->createQueryBuilder(alias: 'matchingResult');

        $queryBuilder
            ->select(select: 'buildingUnit.id AS buildingUnitId')
            ->addSelect(select: 'matchingResult.eliminated')
            ->addSelect(select: 'COUNT(DISTINCT matchingResult.id) AS matches')
            ->innerJoin(join: 'matchingResult.buildingUnit', alias: 'buildingUnit')
            ->andWhere('matchingResult.account = :account')
            ->groupBy(groupBy: 'matchingResult.buildingUnit, matchingResult.eliminated')
            ->setParameter(key: 'account', value: $account);

        return $queryBuilder->getQuery()->getResult();
    }
}
