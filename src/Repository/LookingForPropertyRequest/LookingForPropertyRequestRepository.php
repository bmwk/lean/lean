<?php

declare(strict_types=1);

namespace App\Repository\LookingForPropertyRequest;

use App\Domain\Entity\Account;
use App\Domain\Entity\LookingForPropertyRequest\LookingForPropertyRequest;
use App\Domain\Entity\LookingForPropertyRequest\LookingForPropertyRequestFilter;
use App\Domain\Entity\LookingForPropertyRequest\LookingForPropertyRequestSearch;
use App\Domain\Entity\Person\Person;
use App\Domain\Entity\SortingOption\SortingOption;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method LookingForPropertyRequest|null find($id, $lockMode = null, $lockVersion = null)
 * @method LookingForPropertyRequest|null findOneBy(array $criteria, array $orderBy = null)
 * @method LookingForPropertyRequest[]    findAll()
 * @method LookingForPropertyRequest[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LookingForPropertyRequestRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LookingForPropertyRequest::class);
    }

    /**
     * @return LookingForPropertyRequest[]
     */
    public function findByAccount(Account $account, bool $withFromSubAccounts, ?bool $archived = null, ?bool $active = null): array
    {
        $queryBuilder = $this->createFindByAccountQueryBuilder(account: $account, withFromSubAccounts: $withFromSubAccounts, archived: $archived, active: $active);

        $queryBuilder->orderBy(sort: 'lookingForPropertyRequest.createdAt', order: 'DESC');

        return $queryBuilder->getQuery()->getResult();
    }

    public function findPaginatedByAccount(
        Account $account,
        bool $withFromSubAccounts,
        int $firstResult,
        int $maxResults,
        ?bool $archived = null,
        ?bool $active = null,
        ?LookingForPropertyRequestFilter $lookingForPropertyRequestFilter = null,
        ?LookingForPropertyRequestSearch $lookingForPropertyRequestSearch = null,
        ?SortingOption $sortingOption = null
    ): Paginator {
        $queryBuilder = $this->createFindByAccountQueryBuilder(
            account: $account,
            withFromSubAccounts: $withFromSubAccounts,
            archived: $archived,
            active: $active,
            lookingForPropertyRequestFilter: $lookingForPropertyRequestFilter,
            lookingForPropertyRequestSearch: $lookingForPropertyRequestSearch,
            sortingOption: $sortingOption
        );

        $queryBuilder
            ->setFirstResult($firstResult)
            ->setMaxResults($maxResults)
            ->addOrderBy(sort: 'lookingForPropertyRequest.createdAt', order:'DESC');

        return new Paginator(query: $queryBuilder->getQuery(), fetchJoinCollection: true);
    }

    /**
     * @param int[] $ids
     * @return LookingForPropertyRequest[]
     */
    public function findByIds(Account $account, bool $withFromSubAccounts, array $ids, ?bool $archived = null, ?bool $active = null): array
    {
        $queryBuilder = $this->createFindByAccountQueryBuilder(account: $account, withFromSubAccounts: $withFromSubAccounts, archived: $archived, active: $active);

        $queryBuilder
            ->andWhere('lookingForPropertyRequest.id IN (:ids)')
            ->orderBy(sort: 'lookingForPropertyRequest.createdAt', order: 'DESC')
            ->setParameter(key: 'ids', value: $ids, type: Connection::PARAM_INT_ARRAY);

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * @param int[] $ids
     * @return LookingForPropertyRequest[]
     */
    public function findByPersonAndIds(Account $account, bool $withFromSubAccounts, Person $person, array $ids, ?bool $archived = null, ?bool $active = null): array
    {
        $queryBuilder = $this->createFindByAccountQueryBuilder(account: $account, withFromSubAccounts: $withFromSubAccounts, archived: $archived, active: $active);

        $queryBuilder
            ->andWhere('lookingForPropertyRequest.person = :person')
            ->andWhere('lookingForPropertyRequest.id IN (:ids)')
            ->orderBy(sort: 'lookingForPropertyRequest.createdAt', order: 'DESC')
            ->setParameter(key: 'person', value: $person)
            ->setParameter(key: 'ids', value: $ids, type: Connection::PARAM_INT_ARRAY);

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * @return LookingForPropertyRequest[]
     */
    public function findByPerson(Account $account, bool $withFromSubAccounts, Person $person, ?bool $archived = null, ?bool $active = null): array
    {
        $queryBuilder = $this->createFindByAccountQueryBuilder(
            account: $account,
            withFromSubAccounts: $withFromSubAccounts,
            archived: $archived,
            active: $active
        );

        $queryBuilder
            ->andWhere('lookingForPropertyRequest.person = :person')
            ->orderBy(sort: 'lookingForPropertyRequest.createdAt', order: 'DESC')
            ->setParameter(key: 'person', value: $person);

        return $queryBuilder->getQuery()->getResult();
    }

    public function findPaginatedByPerson(
        Account $account,
        bool $withFromSubAccounts,
        Person $person,
        int $firstResult,
        int $maxResults,
        ?bool $archived = null,
        ?bool $active = null,
        ?SortingOption $sortingOption = null
    ): Paginator {
        $queryBuilder = $this->createFindByAccountQueryBuilder(account: $account, withFromSubAccounts: $withFromSubAccounts, archived: $archived, active: $active, sortingOption: $sortingOption);

        $queryBuilder
            ->andWhere('lookingForPropertyRequest.person = :person')
            ->setParameter(key: 'person', value: $person)
            ->addOrderBy(sort: 'lookingForPropertyRequest.createdAt', order:'DESC')
            ->setFirstResult($firstResult)
            ->setMaxResults($maxResults);

        return new Paginator(query: $queryBuilder->getQuery(), fetchJoinCollection: true);
    }

    public function findOneById(Account $account, bool $withFromSubAccounts, int $id): ?LookingForPropertyRequest
    {
        $queryBuilder = $this->createFindByAccountQueryBuilder(account: $account, withFromSubAccounts: $withFromSubAccounts);

        $queryBuilder
            ->andWhere('lookingForPropertyRequest.id IN (:id)')
            ->setParameter(key: 'id', value: $id);

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }

    public function findOneByPersonAndId(Account $account, bool $withFromSubAccounts, Person $person, int $id): ?LookingForPropertyRequest
    {
        $queryBuilder = $this->createFindByAccountQueryBuilder(account: $account, withFromSubAccounts: $withFromSubAccounts);

        $queryBuilder
            ->andWhere('lookingForPropertyRequest.person = :person')
            ->andWhere('lookingForPropertyRequest.id IN (:id)')
            ->setParameter(key: 'person', value: $person)
            ->setParameter(key: 'id', value: $id);

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }

    /**
     * @return LookingForPropertyRequest[]
     */
    public function findWithoutMatchFound(Account $account, bool $withFromSubAccounts, int $noMatchFoundInDays, int $intervalLength): array
    {
        $queryBuilder = $this->createFindByAccountQueryBuilder(account: $account, withFromSubAccounts: $withFromSubAccounts, archived: false);

        $expressionNoResult = $queryBuilder->expr()->andX();
        $expressionWithResult = $queryBuilder->expr()->andX();

        $queryBuilder
            ->addSelect(select: 'matchingResult')
            ->leftJoin(join: 'lookingForPropertyRequest.matchingResults', alias: 'matchingResult');

        $expressionNoResult
            ->add('MOD(DATE_DIFF(lookingForPropertyRequest.createdAt, CURRENT_DATE()), :noMatchFoundInDays) = 0')
            ->add('matchingResult.id IS NULL');

        $expressionWithResult
            ->add('matchingResult.id IS NOT NULL')
            ->add('MOD(DATE_DIFF(MIN(matchingResult.createdAt), CURRENT_DATE()), :intervalLength) = 0');

        $queryBuilder
            ->groupBy(groupBy: 'lookingForPropertyRequest.id')
            ->andHaving(having: $expressionNoResult)
            ->orHaving(having: $expressionWithResult)
            ->setParameter(key: ':noMatchFoundInDays', value: $noMatchFoundInDays)
            ->setParameter(key: ':intervalLength', value: $intervalLength);

        return $queryBuilder->getQuery()->getResult();
    }

    public function countByPerson(Account $account, bool $withFromSubAccounts, Person $person, ?bool $archived = null, ?bool $active = null): int
    {
        $queryBuilder = $this->createFindByAccountQueryBuilder(account: $account, withFromSubAccounts: $withFromSubAccounts, archived: $archived, active: $active);

        $queryBuilder
            ->select(select: 'count(lookingForPropertyRequest.id)')
            ->andWhere('lookingForPropertyRequest.person = :person')
            ->setParameter(key: 'person', value: $person);

        return $queryBuilder->getQuery()->getSingleScalarResult();
    }

    public function countByAccount(Account $account, bool $withFromSubAccounts, ?bool $archived = null, ?bool $active = null): int
    {
        $queryBuilder = $this->createFindByAccountQueryBuilder(account: $account, withFromSubAccounts: $withFromSubAccounts, archived: $archived, active: $active);

        $queryBuilder->select(select: 'count(lookingForPropertyRequest.id)');

        return $queryBuilder->getQuery()->getSingleScalarResult();
    }

    private function createFindByAccountQueryBuilder(
        Account $account,
        bool $withFromSubAccounts,
        ?bool $archived = null,
        ?bool $active = null,
        ?LookingForPropertyRequestFilter $lookingForPropertyRequestFilter = null,
        ?LookingForPropertyRequestSearch $lookingForPropertyRequestSearch = null,
        ?SortingOption $sortingOption = null
    ): QueryBuilder {
        $queryBuilder = $this->createQueryBuilder(alias: 'lookingForPropertyRequest');

        $queryBuilder
            ->addSelect(select: 'person')
            ->addSelect(select: 'lookingForPropertyRequestExposeForwarding')
            ->addSelect(select: 'propertyRequirement')
            ->addSelect(select: 'propertyValueRangeRequirement')
            ->addSelect(select: 'totalSpace')
            ->addSelect(select: 'industryClassification')
            ->addSelect(select: 'assignedToAccountUser');

        if ($withFromSubAccounts === true) {
            $queryBuilder->innerJoin(
                join: 'lookingForPropertyRequest.account',
                alias: 'account',
                conditionType: Join::WITH,
                condition: '(account.parentAccount = :account OR account = :account) AND account.deleted = false AND account.enabled = true'
            );
        } else {
            $queryBuilder->innerJoin(
                join: 'lookingForPropertyRequest.account',
                alias: 'account',
                conditionType: Join::WITH,
                condition: 'account = :account AND account.deleted = false AND account.enabled = true'
            );
        }

        $queryBuilder
            ->leftJoin(join: 'lookingForPropertyRequest.person', alias: 'person')
            ->leftJoin(join: 'lookingForPropertyRequest.lookingForPropertyRequestExposeForwardings', alias: 'lookingForPropertyRequestExposeForwarding')
            ->leftJoin(join: 'lookingForPropertyRequest.propertyRequirement', alias: 'propertyRequirement')
            ->leftJoin(join: 'propertyRequirement.propertyValueRangeRequirement', alias: 'propertyValueRangeRequirement')
            ->leftJoin(join: 'propertyValueRangeRequirement.totalSpace', alias: 'totalSpace')
            ->leftJoin(join: 'propertyRequirement.industryClassification', alias: 'industryClassification')
            ->leftJoin(join: 'lookingForPropertyRequest.assignedToAccountUser', alias: 'assignedToAccountUser');

        $queryBuilder
            ->where(predicates: 'lookingForPropertyRequest.account = account')
            ->andWhere('lookingForPropertyRequest.deleted = false')
            ->setParameter(key: 'account', value: $account);

        if ($archived !== null) {
            $queryBuilder
                ->andWhere('lookingForPropertyRequest.archived = :archived')
                ->setParameter(key: 'archived', value: $archived);
        }

        if ($active !== null) {
            if ($active === true) {
                $activeFindExpression = $queryBuilder->expr()->orX();

                $activeFindExpression
                    ->add('lookingForPropertyRequest.requestAvailableTill >= :now')
                    ->add('lookingForPropertyRequest.requestAvailableTill IS NULL');

                $queryBuilder->andWhere($activeFindExpression);
            } else {
                $queryBuilder->andWhere('lookingForPropertyRequest.requestAvailableTill < :now');
            }

            $queryBuilder->setParameter(key: 'now', value:  new \DateTime());
        }

        if ($lookingForPropertyRequestSearch !== null && empty($lookingForPropertyRequestSearch->getText()) === false) {
            $expression = $queryBuilder->expr()->orX();

            $expression
                ->add('lookingForPropertyRequest.id LIKE :searchTextForId')
                ->add('lookingForPropertyRequest.title LIKE :searchText')
                ->add('lookingForPropertyRequest.conceptDescription LIKE :searchText')
                ->add('lookingForPropertyRequest.requestReason LIKE :searchText')
                ->add('person.name LIKE :searchText')
                ->add('person.placeName LIKE :searchText');

            $queryBuilder
                ->andWhere($expression)
                ->setParameter(key: 'searchTextForId', value: $lookingForPropertyRequestSearch->getText())
                ->setParameter(key: 'searchText', value: '%' . $lookingForPropertyRequestSearch->getText() . '%');
        }

        if ($lookingForPropertyRequestFilter !== null) {
            if (empty($lookingForPropertyRequestFilter->getRequestReasons()) === false) {
                $queryBuilder
                    ->andWhere('lookingForPropertyRequest.requestReason IN (:requestReasons)')
                    ->setParameter(key: 'requestReasons', value: $lookingForPropertyRequestFilter->getRequestReasons());
            }

            if (empty($lookingForPropertyRequestFilter->getIndustryClassifications()) === false) {
                $queryBuilder
                    ->andWhere('propertyRequirement.industryClassification IN (:industryClassifications)')
                    ->setParameter(key: 'industryClassifications', value: $lookingForPropertyRequestFilter->getIndustryClassifications());
            }

            if ($lookingForPropertyRequestFilter->getAreaSizeMinimum() !== null) {
                $queryBuilder
                    ->andWhere('totalSpace.minimumValue >= :areaSizeMinimum')
                    ->setParameter(key: 'areaSizeMinimum', value: $lookingForPropertyRequestFilter->getAreaSizeMinimum());
            }

            if ($lookingForPropertyRequestFilter->getAreaSizeMaximum() !== null) {
                $queryBuilder
                    ->andWhere('totalSpace.maximumValue <= :areaSizeMaximum')
                    ->setParameter(key: 'areaSizeMaximum', value: $lookingForPropertyRequestFilter->getAreaSizeMaximum());
            }

            if ($lookingForPropertyRequestFilter->getAssignedToAccountUser() !== null) {
                $queryBuilder
                    ->andWhere('lookingForPropertyRequest.assignedToAccountUser = :assignedToAccountUser')
                    ->setParameter(key: 'assignedToAccountUser', value: $lookingForPropertyRequestFilter->getAssignedToAccountUser());
            }

            if ($lookingForPropertyRequestFilter->getLastUpdatedFrom() !== null) {
                $queryBuilder
                    ->andWhere('lookingForPropertyRequest.updatedAt >= :lastUpdatedFrom')
                    ->setParameter(key: 'lastUpdatedFrom', value: $lookingForPropertyRequestFilter->getLastUpdatedFrom());
            }

            if ($lookingForPropertyRequestFilter->getLastUpdatedTill() !== null) {
                $queryBuilder
                    ->andWhere('lookingForPropertyRequest.updatedAt <= :lastUpdatedTill')
                    ->setParameter(key: 'lastUpdatedTill', value: $lookingForPropertyRequestFilter->getLastUpdatedTill());
            }
        }

        if ($sortingOption !== null) {
            self::applyLookingForPropertyRequestSortingToQueryBuilder(sortingOption: $sortingOption, queryBuilder: $queryBuilder);
        }

        return $queryBuilder;
    }

    private static function applyLookingForPropertyRequestSortingToQueryBuilder(SortingOption $sortingOption, QueryBuilder $queryBuilder): void
    {
        if ($sortingOption->getSortingBy() === 'nummer') {
            $queryBuilder->orderBy(sort: 'lookingForPropertyRequest.id', order: $sortingOption->getSortingDirection()->getOrderByDirection());
        }

        if ($sortingOption->getSortingBy() === 'titel') {
            $queryBuilder->orderBy(sort: 'lookingForPropertyRequest.title', order: $sortingOption->getSortingDirection()->getOrderByDirection());
        }

        if ($sortingOption->getSortingBy() === 'suchender') {
            $queryBuilder->orderBy(sort: 'person.name', order: $sortingOption->getSortingDirection()->getOrderByDirection());
        }

        if ($sortingOption->getSortingBy() === 'nutzung') {
            $queryBuilder->orderBy(sort: 'industryClassification.name', order: $sortingOption->getSortingDirection()->getOrderByDirection());
        }

        if ($sortingOption->getSortingBy() === 'grund') {
            $orderByExpression = $queryBuilder->expr()->andX();

            $orderByExpression->add(<<<EOT
                CASE
                    WHEN lookingForPropertyRequest.requestReason = 0 THEN 'Erweiterung'
                    WHEN lookingForPropertyRequest.requestReason = 1 THEN 'Zweigstelle'
                    WHEN lookingForPropertyRequest.requestReason = 2 THEN 'Zuzug'
                    WHEN lookingForPropertyRequest.requestReason = 3 THEN 'Umzug'
                    WHEN lookingForPropertyRequest.requestReason = 4 THEN 'Existenzgründung'
                    WHEN lookingForPropertyRequest.requestReason = 5 THEN 'Sonstiges'
                    ELSE 'Sonstiges'
                END
                EOT
            );

            $queryBuilder->orderBy(sort: $orderByExpression, order: $sortingOption->getSortingDirection()->getOrderByDirection());
        }

        if ($sortingOption->getSortingBy() === 'status') {
            $orderByExpression = $queryBuilder->expr()->andX();

            $orderByExpression->add(<<<EOT
                CASE
                    WHEN lookingForPropertyRequest.archived IS NOT NULL AND lookingForPropertyRequest.archived = 1 THEN 'archiviert'
                    WHEN lookingForPropertyRequest.requestAvailableTill IS NOT NULL AND lookingForPropertyRequest.requestAvailableTill < CURRENT_TIMESTAMP() THEN 'abgelaufen'
                    ELSE 'aktiv'
                END
                EOT
            );

            $queryBuilder->orderBy(sort: $orderByExpression, order: $sortingOption->getSortingDirection()->getOrderByDirection());
        }

        if ($sortingOption->getSortingBy() === 'aktiv_bis') {
            $queryBuilder->orderBy(sort: 'lookingForPropertyRequest.requestAvailableTill', order: $sortingOption->getSortingDirection()->getOrderByDirection());
        }

        if ($sortingOption->getSortingBy() === 'flaechenbedarf') {
            $queryBuilder->orderBy(sort: 'totalSpace.maximumValue', order: $sortingOption->getSortingDirection()->getOrderByDirection());
        }

        if ($sortingOption->getSortingBy() === 'sachbearbeiter') {
            $queryBuilder->orderBy(sort: 'assignedToAccountUser.fullName', order: $sortingOption->getSortingDirection()->getOrderByDirection());
        }

        if ($sortingOption->getSortingBy() === 'weiterleitungen' || $sortingOption->getSortingBy() === 'angebote') {
            $queryBuilder
                ->addSelect(select: 'COUNT(lookingForPropertyRequestExposeForwarding.id) AS HIDDEN amountOfForwardings')
                ->orderBy(sort: 'amountOfForwardings', order: $sortingOption->getSortingDirection()->getOrderByDirection())
                ->groupBy(groupBy: 'lookingForPropertyRequest');
        }
    }
}
