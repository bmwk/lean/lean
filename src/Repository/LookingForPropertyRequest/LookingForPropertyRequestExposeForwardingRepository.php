<?php

declare(strict_types=1);

namespace App\Repository\LookingForPropertyRequest;

use App\Domain\Entity\Account;
use App\Domain\Entity\ForwardingResponse;
use App\Domain\Entity\LookingForPropertyRequest\LookingForPropertyRequest;
use App\Domain\Entity\LookingForPropertyRequest\LookingForPropertyRequestExposeForwarding;
use App\Domain\Entity\Person\Person;
use App\Domain\Entity\Property\BuildingUnit;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method LookingForPropertyRequestExposeForwarding|null find($id, $lockMode = null, $lockVersion = null)
 * @method LookingForPropertyRequestExposeForwarding|null findOneBy(array $criteria, array $orderBy = null)
 * @method LookingForPropertyRequestExposeForwarding[]    findAll()
 * @method LookingForPropertyRequestExposeForwarding[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LookingForPropertyRequestExposeForwardingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LookingForPropertyRequestExposeForwarding::class);
    }

    public function findOneByLookingForPropertyRequestAndBuildingUnit(
        Account $account,
        LookingForPropertyRequest $lookingForPropertyRequest,
        BuildingUnit $buildingUnit
    ): ?LookingForPropertyRequestExposeForwarding {
        $queryBuilder = $this->createFindByAccountQueryBuilder(account: $account);

        $queryBuilder
            ->where(predicates: 'lookingForPropertyRequest = :lookingForPropertyRequest')
            ->andWhere('buildingUnit = :buildingUnit')
            ->setParameter(key: 'lookingForPropertyRequest', value: $lookingForPropertyRequest)
            ->setParameter(key: 'buildingUnit', value: $buildingUnit);

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }

    /**
     * @return LookingForPropertyRequestExposeForwarding[]
     */
    public function findByPersonAndLookingForPropertyRequest(
        Account $account,
        Person $person,
        LookingForPropertyRequest $lookingForPropertyRequest
    ): array {
        $queryBuilder = $this->createFindByAccountQueryBuilder(account: $account);

        $queryBuilder
            ->where(predicates: 'person = :person')
            ->andWhere('lookingForPropertyRequest = :lookingForPropertyRequest')
            ->setParameter(key: 'person', value: $person)
            ->setParameter(key: 'lookingForPropertyRequest', value: $lookingForPropertyRequest);

        return $queryBuilder->getQuery()->getResult();
    }

    public function findOneByPersonAndId(Account $account, Person $person, int $id): ?LookingForPropertyRequestExposeForwarding
    {
        $queryBuilder = $this->createFindByAccountQueryBuilder(account: $account);

        $queryBuilder
            ->where(predicates: 'person = :person')
            ->andWhere('lookingForPropertyRequestExposeForwarding.id = :id')
            ->setParameter(key: 'person', value: $person)
            ->setParameter(key: 'id', value: $id);

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }

    public function countByAccount(Account $account): int
    {
        $queryBuilder = $this->createFindByAccountQueryBuilder(account: $account);

        $queryBuilder->select(select: 'COUNT(DISTINCT lookingForPropertyRequestExposeForwarding.id)');

        return $queryBuilder->getQuery()->getSingleScalarResult();
    }

    public function countByAccountAndForwardingResponse(Account $account, ForwardingResponse $forwardingResponse): int
    {
        $queryBuilder = $this->createFindByAccountQueryBuilder(account: $account);

        $queryBuilder
            ->select(select: 'COUNT(DISTINCT lookingForPropertyRequestExposeForwarding.id)')
            ->andWhere('lookingForPropertyRequestExposeForwarding.forwardingResponse = :forwardingResponse')
            ->setParameter(key: 'forwardingResponse', value: $forwardingResponse);

        return $queryBuilder->getQuery()->getSingleScalarResult();
    }

    private function createFindByAccountQueryBuilder(Account $account): QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder(alias: 'lookingForPropertyRequestExposeForwarding');

        $queryBuilder
            ->innerJoin(
                join: 'lookingForPropertyRequestExposeForwarding.lookingForPropertyRequest',
                alias: 'lookingForPropertyRequest',
                conditionType: Join::WITH,
                condition: 'lookingForPropertyRequest.deleted = false'
            )
            ->innerJoin(
                join: 'lookingForPropertyRequest.account',
                alias: 'lookingForPropertyRequestAccount',
                conditionType: Join::WITH,
                condition: '(lookingForPropertyRequestAccount.parentAccount = :account OR lookingForPropertyRequestAccount = :account) AND lookingForPropertyRequestAccount.deleted = false AND lookingForPropertyRequestAccount.enabled = true'
            )
            ->innerJoin(
                join: 'lookingForPropertyRequestExposeForwarding.buildingUnit',
                alias: 'buildingUnit',
                conditionType: Join::WITH,
                condition: 'buildingUnit.deleted = false'
            )
            ->innerJoin(
                join: 'buildingUnit.account',
                alias: 'buildingUnitAccount',
                conditionType: Join::WITH,
                condition: '(buildingUnitAccount.parentAccount = :account OR buildingUnitAccount = :account) AND buildingUnitAccount.deleted = false AND buildingUnitAccount.enabled = true'
            )
            ->innerJoin(join: 'lookingForPropertyRequest.person', alias: 'person')
            ->setParameter(key: 'account', value: $account);

        return $queryBuilder;
    }
}
