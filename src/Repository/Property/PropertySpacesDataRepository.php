<?php

declare(strict_types=1);

namespace App\Repository\Property;

use App\Domain\Entity\Property\PropertySpacesData;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PropertySpacesData|null find($id, $lockMode = null, $lockVersion = null)
 * @method PropertySpacesData|null findOneBy(array $criteria, array $orderBy = null)
 * @method PropertySpacesData[]    findAll()
 * @method PropertySpacesData[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PropertySpacesDataRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PropertySpacesData::class);
    }
}
