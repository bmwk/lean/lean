<?php

declare(strict_types=1);

namespace App\Repository\Property;

use App\Domain\Entity\Account;
use App\Domain\Entity\Person\Person;
use App\Domain\Entity\Property\BuildingUnit;
use App\Domain\Entity\Property\PropertyUser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PropertyUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method PropertyUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method PropertyUser[]    findAll()
 * @method PropertyUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PropertyUserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PropertyUser::class);
    }

    public function findOneByIdAndBuildingUnit(
        Account $account,
        bool $withFromSubAccounts,
        int $id,
        BuildingUnit $buildingUnit
    ): ?PropertyUser {
        $queryBuilder = $this->createFindByAccountQueryBuilder(account: $account, withFromSubAccounts: $withFromSubAccounts);

        $queryBuilder
            ->addSelect(select: 'buildingUnit')
            ->innerJoin(
                join: 'propertyUser.buildingUnits',
                alias: 'buildingUnit',
                conditionType: Join::WITH,
                condition: 'buildingUnit = :buildingUnit AND buildingUnit.deleted = false'
            )
            ->andWhere('propertyUser.id = :id')
            ->setParameter(key: 'id', value: $id)
            ->setParameter(key: 'buildingUnit', value: $buildingUnit);

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }

    public function findOneByBuildingUnitAndPerson(
        Account $account,
        bool $withFromSubAccounts,
        BuildingUnit $buildingUnit,
        Person $person
    ): ?PropertyUser {
        $queryBuilder = $this->createFindByAccountQueryBuilder(account: $account, withFromSubAccounts: $withFromSubAccounts);

        $queryBuilder
            ->addSelect(select: 'buildingUnit')
            ->innerJoin(
                join: 'propertyUser.buildingUnits',
                alias: 'buildingUnit',
                conditionType: Join::WITH,
                condition: 'buildingUnit = :buildingUnit AND buildingUnit.deleted = false'
            )
            ->andWhere('propertyUser.person = :person')
            ->setParameter(key: 'buildingUnit', value: $buildingUnit)
            ->setParameter(key: 'person', value: $person);

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }

    private function createFindByAccountQueryBuilder(Account $account, bool $withFromSubAccounts): QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder(alias: 'propertyUser');

        $queryBuilder->addSelect(select: 'account');

        if ($withFromSubAccounts === true ) {
            $queryBuilder->innerJoin(
                join: 'propertyUser.account',
                alias: 'account',
                conditionType: Join::WITH,
                condition: '(account.parentAccount = :account OR account = :account) AND account.deleted = false AND account.enabled = true'
            );
        } else {
            $queryBuilder->innerJoin(
                join: 'propertyUser.account',
                alias: 'account',
                conditionType: Join::WITH,
                condition: 'account = :account AND account.deleted = false AND account.enabled = true'
            );
        }

        $queryBuilder
            ->where(predicates: 'propertyUser.account = account')
            ->setParameter(key: 'account', value: $account);

        return $queryBuilder;
    }
}
