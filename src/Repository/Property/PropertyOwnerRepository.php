<?php

declare(strict_types=1);

namespace App\Repository\Property;

use App\Domain\Entity\Account;
use App\Domain\Entity\Person\Person;
use App\Domain\Entity\Property\BuildingUnit;
use App\Domain\Entity\Property\PropertyOwner;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PropertyOwner|null find($id, $lockMode = null, $lockVersion = null)
 * @method PropertyOwner|null findOneBy(array $criteria, array $orderBy = null)
 * @method PropertyOwner[]    findAll()
 * @method PropertyOwner[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PropertyOwnerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PropertyOwner::class);
    }

    public function findOneByIdAndBuildingUnit(
        Account $account,
        bool $withFromSubAccounts,
        int $id,
        BuildingUnit $buildingUnit
    ): ?PropertyOwner {
        $queryBuilder = $this->createFindByAccountQueryBuilder(account: $account, withFromSubAccounts: $withFromSubAccounts);

        $queryBuilder
            ->addSelect(select: 'buildingUnit')
            ->innerJoin(
                join: 'propertyOwner.buildingUnits',
                alias: 'buildingUnit',
                conditionType: Join::WITH,
                condition: 'buildingUnit = :buildingUnit AND buildingUnit.deleted = false'
            )
            ->andWhere('propertyOwner.id = :id')
            ->setParameter(key: 'id', value: $id)
            ->setParameter(key: 'buildingUnit', value: $buildingUnit);

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }

    public function findOneByBuildingUnitAndPerson(
        Account $account,
        bool $withFromSubAccounts,
        BuildingUnit $buildingUnit,
        Person $person
    ): ?PropertyOwner {
        $queryBuilder = $this->createFindByAccountQueryBuilder(account: $account, withFromSubAccounts: $withFromSubAccounts);

        $queryBuilder
            ->addSelect(select: 'buildingUnit')
            ->innerJoin(
                join: 'propertyOwner.buildingUnits',
                alias: 'buildingUnit',
                conditionType: Join::WITH,
                condition: 'buildingUnit = :buildingUnit AND buildingUnit.deleted = false'
            )
            ->andWhere('propertyOwner.person = :person')
            ->setParameter(key: 'buildingUnit', value: $buildingUnit)
            ->setParameter(key: 'person', value: $person);

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }

    private function createFindByAccountQueryBuilder(Account $account, bool $withFromSubAccounts): QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder(alias: 'propertyOwner');

        $queryBuilder->addSelect(select: 'account');

        if ($withFromSubAccounts === true ) {
            $queryBuilder->innerJoin(
                join: 'propertyOwner.account',
                alias: 'account',
                conditionType: Join::WITH,
                condition: '(account.parentAccount = :account OR account = :account) AND account.deleted = false AND account.enabled = true'
            );
        } else {
            $queryBuilder->innerJoin(
                join: 'propertyOwner.account',
                alias: 'account',
                conditionType: Join::WITH,
                condition: 'account = :account AND account.deleted = false AND account.enabled = true'
            );
        }

        $queryBuilder
            ->where(predicates: 'propertyOwner.account = account')
            ->setParameter(key: 'account', value: $account);

        return $queryBuilder;
    }
}
