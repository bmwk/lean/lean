<?php

declare(strict_types=1);

namespace App\Repository\Property;

use App\Domain\Entity\Property\ExternalContactPerson;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ExternalContactPerson|null find($id, $lockMode = null, $lockVersion = null)
 * @method ExternalContactPerson|null findOneBy(array $criteria, array $orderBy = null)
 * @method ExternalContactPerson[]    findAll()
 * @method ExternalContactPerson[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExternalContactPersonRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ExternalContactPerson::class);
    }
}
