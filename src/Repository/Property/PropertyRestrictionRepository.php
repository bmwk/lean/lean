<?php

declare(strict_types=1);

namespace App\Repository\Property;

use App\Domain\Entity\Property\PropertyRestriction;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PropertyRestriction|null find($id, $lockMode = null, $lockVersion = null)
 * @method PropertyRestriction|null findOneBy(array $criteria, array $orderBy = null)
 * @method PropertyRestriction[]    findAll()
 * @method PropertyRestriction[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PropertyRestrictionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PropertyRestriction::class);
    }
}
