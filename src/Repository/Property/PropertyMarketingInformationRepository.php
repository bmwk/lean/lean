<?php

declare(strict_types=1);

namespace App\Repository\Property;

use App\Domain\Entity\Property\PropertyMarketingInformation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PropertyMarketingInformation|null find($id, $lockMode = null, $lockVersion = null)
 * @method PropertyMarketingInformation|null findOneBy(array $criteria, array $orderBy = null)
 * @method PropertyMarketingInformation[]    findAll()
 * @method PropertyMarketingInformation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PropertyMarketingInformationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PropertyMarketingInformation::class);
    }
}
