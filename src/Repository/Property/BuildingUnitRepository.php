<?php

declare(strict_types=1);

namespace App\Repository\Property;

use App\Domain\Entity\Account;
use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\Classification\IndustryClassification;
use App\Domain\Entity\LocationCategory;
use App\Domain\Entity\LookingForPropertyRequest\MatchingFilter;
use App\Domain\Entity\LookingForPropertyRequest\MatchingSearch;
use App\Domain\Entity\Person\Person;
use App\Domain\Entity\Property\AbstractBuildingUnitFilter;
use App\Domain\Entity\Property\BuildingUnit;
use App\Domain\Entity\Property\BuildingUnitFilter;
use App\Domain\Entity\Property\BuildingUnitSearch;
use App\Domain\Entity\Property\PropertyUserStatus;
use App\Domain\Entity\PropertyOfferType;
use App\Domain\Entity\Report\PropertyVacancy\PropertyVacancyDataFilter;
use App\Domain\Entity\SortingOption\SortingOption;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\Query\Expr\Orx;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BuildingUnit|null find($id, $lockMode = null, $lockVersion = null)
 * @method BuildingUnit|null findOneBy(array $criteria, array $orderBy = null)
 * @method BuildingUnit[]    findAll()
 * @method BuildingUnit[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BuildingUnitRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BuildingUnit::class);
    }

    /**
     * @return BuildingUnit[]
     */
    public function findByAccount(
        Account $account,
        bool $withFromSubAccounts,
        bool $archived,
        ?BuildingUnitFilter $buildingUnitFilter = null,
        ?BuildingUnitSearch $buildingUnitSearch = null,
        ?SortingOption $sortingOption = null
    ): array {
        $queryBuilder = $this->createFindByAccountQueryBuilder(
            account: $account,
            withFromSubAccounts: $withFromSubAccounts,
            archived: $archived,
            buildingUnitFilter: $buildingUnitFilter,
            buildingUnitSearch: $buildingUnitSearch,
            sortingOption: $sortingOption
        );

        return $queryBuilder->getQuery()->getResult();
    }

    public function findPaginatedByAccount(
        Account $account,
        bool $withFromSubAccounts,
        bool $archived,
        int $firstResult,
        int $maxResults,
        ?BuildingUnitFilter $buildingUnitFilter = null,
        ?BuildingUnitSearch $buildingUnitSearch = null,
        ?SortingOption $sortingOption = null
    ): Paginator {
        $queryBuilder = $this->createFindByAccountQueryBuilder(
            account: $account,
            withFromSubAccounts: $withFromSubAccounts,
            archived: $archived,
            buildingUnitFilter: $buildingUnitFilter,
            buildingUnitSearch: $buildingUnitSearch,
            sortingOption: $sortingOption
        );

        $queryBuilder
            ->setFirstResult($firstResult)
            ->setMaxResults($maxResults);

        return new Paginator(query: $queryBuilder->getQuery(), fetchJoinCollection: true);
    }

    /**
     * @param int[] $ids
     * @return BuildingUnit[]
     */
    public function findByIds(
        Account $account,
        bool $withFromSubAccounts,
        array $ids,
        bool $archived,
        ?AccountUser $createdByAccountUser = null,
        ?BuildingUnitFilter $buildingUnitFilter = null,
        ?BuildingUnitSearch $buildingUnitSearch = null,
        ?SortingOption $sortingOption = null
    ): array {
        $queryBuilder = $this->createFindByAccountQueryBuilder(
            account: $account,
            withFromSubAccounts: $withFromSubAccounts,
            archived: $archived,
            buildingUnitFilter: $buildingUnitFilter,
            buildingUnitSearch: $buildingUnitSearch,
            sortingOption: $sortingOption
        );

        $queryBuilder
            ->andWhere('buildingUnit.id IN (:ids)')
            ->setParameter(key: 'ids', value: $ids, type: Connection::PARAM_INT_ARRAY);

        if ($createdByAccountUser !== null) {
            $queryBuilder
                ->andWhere('buildingUnit.createdByAccountUser = :createdByAccountUser')
                ->setParameter(key: 'createdByAccountUser', value: $createdByAccountUser);
        }

        return $queryBuilder->getQuery()->getResult();
    }

    public function findOneById(Account $account, bool $withFromSubAccounts, int $id, ?bool $archived = null): ?BuildingUnit
    {
        $queryBuilder = $this->createFindByAccountQueryBuilder(account: $account, withFromSubAccounts: $withFromSubAccounts, archived: $archived);

        $queryBuilder
            ->andWhere('buildingUnit.id = :id')
            ->setParameter(key: 'id', value: $id);

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }

    public function findOneWithConnectedImagesById(Account $account, bool $withFromSubAccounts, int $id): ?BuildingUnit
    {
        $queryBuilder = $this->createFindByAccountQueryBuilder(account: $account, withFromSubAccounts: $withFromSubAccounts);

        $queryBuilder
            ->addSelect(select: 'image')
            ->leftJoin(join: 'buildingUnit.images', alias: 'image')
            ->andWhere('buildingUnit.id = :id')
            ->orderBy(sort: 'image.createdAt', order: 'DESC')
            ->setParameter(key: 'id', value: $id);

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }

    public function findOneWithConnectedDocumentsById(Account $account, bool $withFromSubAccounts, int $id): ?BuildingUnit
    {
        $queryBuilder = $this->createFindByAccountQueryBuilder(account: $account, withFromSubAccounts: $withFromSubAccounts);

        $queryBuilder
            ->addSelect(select: 'document')
            ->leftJoin(join: 'buildingUnit.documents', alias: 'document')
            ->andWhere('buildingUnit.id = :id')
            ->setParameter(key: 'id', value: $id);

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }

    /**
     * @return BuildingUnit[]
     */
    public function findByConnectedPerson(Account $account, bool $withFromSubAccounts, Person $person, bool $archived): array
    {
        $queryBuilder = $this->createFindByAccountQueryBuilder(account: $account, withFromSubAccounts: $withFromSubAccounts, archived: $archived);

        $expression = $queryBuilder->expr()->orX();

        $expression
            ->add('propertyUser.person = :person')
            ->add('propertyOwner.person = :person')
            ->add('propertyContactPerson.person = :person');

        $queryBuilder
            ->andWhere($expression)
            ->setParameter(key: 'person', value: $person);

        return $queryBuilder->getQuery()->getResult();
    }

    public function findPaginatedWithMatchingResults(
        Account $account,
        bool $withFromSubAccounts,
        bool $archived,
        bool $includeEliminatedMatchingResults,
        int $firstResult,
        int $maxResults,
        ?MatchingFilter $matchingFilter = null,
        ?MatchingSearch $matchingSearch = null
    ): Paginator {
        $queryBuilder = $this->createFindWithMatchingResultsByAccountQueryBuilder(
            account: $account,
            withFromSubAccounts: $withFromSubAccounts,
            archived: $archived,
            includeEliminatedMatchingResults: $includeEliminatedMatchingResults,
            matchingFilter: $matchingFilter,
            matchingSearch: $matchingSearch
        );

        $queryBuilder
            ->setFirstResult($firstResult)
            ->setMaxResults($maxResults);

        return new Paginator(query: $queryBuilder->getQuery(), fetchJoinCollection: true);
    }

    public function findOneWithMatchingResultsById(
        Account $account,
        bool $withFromSubAccounts,
        int $id,
        bool $archived,
        bool $includeEliminatedMatchingResults
    ): ?BuildingUnit {
        $queryBuilder = $this->createFindWithMatchingResultsByAccountQueryBuilder(
            account: $account,
            withFromSubAccounts: $withFromSubAccounts,
            archived: $archived,
            includeEliminatedMatchingResults: $includeEliminatedMatchingResults
        );

        $queryBuilder
            ->andWhere('buildingUnit.id = :id')
            ->setParameter(key: 'id', value: $id)
            ->orderBy(sort: 'settlementConceptMatchingResult.score / settlementConceptMatchingResult.possibleMaxScore', order: 'DESC')
            ->addOrderBy(sort: 'matchingResult.score / matchingResult.possibleMaxScore', order: 'DESC');

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }

    public function findWithCurrentUsageInBoundingBoxByIndustryClassification(
        Account $account,
        bool $withFromSubAccounts,
        bool $archived,
        BuildingUnit $buildingUnit,
        IndustryClassification $industryClassification,
        float $searchRadius = 1.5
    ): array {
        if ($buildingUnit->getGeolocationPoint() === null) {
            return [];
        }

        $earthRadius = 6371.0;
        $point = $buildingUnit->getGeolocationPoint()->getPoint();
        $lat = $point->getLatitude();
        $long = $point->getLongitude();

        $maxLat = $lat + rad2deg($searchRadius / $earthRadius);
        $minLat = $lat - rad2deg($searchRadius / $earthRadius);
        $maxLong = $long + rad2deg($searchRadius / $earthRadius / cos(deg2rad($lat)));
        $minLong = $long - rad2deg($searchRadius / $earthRadius / cos(deg2rad($lat)));

        $lowerCorner = $minLong . ' ' . $minLat;
        $upperCorner = $maxLong . ' ' . $maxLat;

        $queryBuilder = $this->createFindByAccountQueryBuilder(account: $account, withFromSubAccounts: $withFromSubAccounts, archived: $archived);

        $queryBuilder
            ->andWhere('buildingUnit != :buildingUnit')
            ->andWhere('buildingUnit.currentUsageIndustryClassification = :industryClassification')
            ->andWhere('MBRContains(st_envelope(st_geomfromtext(:linestring)), geolocationPoint.point) = true')
            ->setParameter(key: 'buildingUnit', value: $buildingUnit)
            ->setParameter(key: 'industryClassification', value: $industryClassification)
            ->setParameter(key: 'linestring', value: 'linestring(' . $lowerCorner . ', ' . $upperCorner . ')');

        return $queryBuilder->getQuery()->execute();
    }

    /**
     * @return BuildingUnit[]
     */
    public function findWithoutMatchingExecuted(Account $account, bool $withFromSubAccounts, int $notMatchingExecutedInDays): array
    {
        $queryBuilder = $this->createFindByAccountQueryBuilder(account: $account, withFromSubAccounts: $withFromSubAccounts, archived: false);

        $queryBuilder
            ->leftJoin(join: 'buildingUnit.matchingTask', alias: 'matchingTask')
            ->addSelect(select: <<<EOT
                CASE
                    WHEN matchingTask.id IS NOT NULL THEN matchingTask.updatedAt
                    WHEN buildingUnit.objectIsEmpty = true AND buildingUnit.objectIsEmptySince IS NOT NULL THEN buildingUnit.objectIsEmptySince
                    WHEN buildingUnit.objectIsEmpty = true AND buildingUnit.objectIsEmptySince IS NULL THEN buildingUnit.createdAt
                    WHEN buildingUnit.objectBecomesEmpty = true AND buildingUnit.objectBecomesEmptyFrom IS NOT NULL THEN buildingUnit.objectBecomesEmptyFrom
                    WHEN buildingUnit.objectBecomesEmpty = true AND buildingUnit.objectBecomesEmptyFrom IS NULL THEN buildingUnit.createdAt
                    ELSE :null
                END AS referenceDate
                EOT
            )
            ->groupBy(groupBy: 'buildingUnit.id')
            ->having(having: 'MOD(DATE_DIFF(MAX(referenceDate), CURRENT_DATE()), :divideBy ) = 0 AND DATE_DIFF(CURRENT_DATE(), MAX(referenceDate)) - 1 >= 0')
            ->setParameter(key: ':null', value: null)
            ->setParameter(key: ':divideBy', value: $notMatchingExecutedInDays);

        return array_reduce(
            array: $queryBuilder->getQuery()->getResult(),
            callback: function (array $resultSet, array $resultRow): array {
                return array_merge($resultSet, [$resultRow[0]]);
            },
            initial: []
        );
    }

    public function findOneByLongestTimeEmpty(Account $account, bool $withFromSubAccounts): ?BuildingUnit
    {
        $queryBuilder = $this->createFindByAccountQueryBuilder(account: $account, withFromSubAccounts: $withFromSubAccounts, archived: false);

        $queryBuilder
            ->andWhere('buildingUnit.objectIsEmpty = true or buildingUnit.reuseAgreed = true')
            ->andWhere('buildingUnit.objectIsEmptySince IS NOT NULL')
            ->orderBy(sort: 'buildingUnit.objectIsEmptySince', order: 'ASC')
            ->setMaxResults(maxResults: 1);

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }

    public function findOneByShortestTimeEmpty(Account $account, bool $withFromSubAccounts): ?BuildingUnit
    {
        $queryBuilder = $this->createFindByAccountQueryBuilder(account: $account, withFromSubAccounts: $withFromSubAccounts, archived: false);

        $queryBuilder
            ->andWhere('buildingUnit.objectIsEmpty = true or buildingUnit.reuseAgreed = true')
            ->andWhere('buildingUnit.objectIsEmptySince IS NOT NULL')
            ->orderBy(sort: 'buildingUnit.objectIsEmptySince', order: 'DESC')
            ->setMaxResults(maxResults: 1);

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }

    public function fetchAverageTimeEmpty(Account $account, bool $withFromSubAccounts): ?int
    {
        $queryBuilder = $this->createFindByAccountQueryBuilder(account: $account, withFromSubAccounts: $withFromSubAccounts, archived: false);

        $queryBuilder->select(select: 'AVG(DATE_DIFF(CURRENT_DATE(), buildingUnit.objectIsEmptySince))');

        $queryBuilder
            ->andWhere('buildingUnit.objectIsEmpty = true or buildingUnit.reuseAgreed = true')
            ->andWhere('buildingUnit.objectIsEmptySince IS NOT NULL');

        return (int) $queryBuilder->getQuery()->getSingleScalarResult();
    }

    /**
     * @return BuildingUnit[]
     */
    public function findByBecameEmptyToday(Account $account, bool $withFromSubAccounts): array
    {
        $queryBuilder = $this->createFindByAccountQueryBuilder(account: $account, withFromSubAccounts: $withFromSubAccounts, archived: false);

        $queryBuilder->andWhere('buildingUnit.objectBecomesEmptyFrom = CURRENT_DATE()');

        return $queryBuilder->getQuery()->execute();
    }

    /**
     * @return BuildingUnit[]
     */
    public function findByRentalAgreementExpiredToday(Account $account, bool $withFromSubAccounts): array
    {
        $queryBuilder = $this->createFindByAccountQueryBuilder(account: $account, withFromSubAccounts: $withFromSubAccounts, archived: false);

        $queryBuilder->andWhere('propertyUser.endOfRental = CURRENT_DATE()');

        return $queryBuilder->getQuery()->execute();
    }

    /**
     * @return BuildingUnit[]
     */
    public function findByNotUpdatedInDays(Account $account, bool $withFromSubAccounts, int $notUpdatedInDays): array
    {
        $queryBuilder = $this->createFindByAccountQueryBuilder(account: $account, withFromSubAccounts: $withFromSubAccounts, archived: false);

        $queryBuilder
            ->andWhere('MOD(DATE_DIFF(CURRENT_DATE(), buildingUnit.updatedAt), :notUpdatedInDays) = 0')
            ->andWhere('DATE_DIFF(CURRENT_DATE(), buildingUnit.updatedAt) >= :notUpdatedInDays')
            ->andWhere('buildingUnit.objectIsEmpty = true')
            ->setParameter(key: ':notUpdatedInDays', value: $notUpdatedInDays);

        return $queryBuilder->getQuery()->execute();
    }

    /**
     * @return BuildingUnit[]
     */
    public function findWithoutFoundMatching(Account $account, bool $withFromSubAccounts, int $noMatchFoundInDays, int $intervalLength): array
    {
        $queryBuilder = $this->createFindByAccountQueryBuilder(account: $account, withFromSubAccounts: $withFromSubAccounts, archived: false);

        $queryBuilder
            ->innerJoin(
                join: 'buildingUnit.matchingTask',
                alias: 'matchingTask',
                conditionType: Join::WITH,
                condition: 'matchingTask.createdAt IS NOT NULL AND matchingTask.updatedAt IS NOT NULL'
            )
            ->innerJoin(join: 'buildingUnit.matchingResults', alias: 'matchingResult')
            ->andWhere('buildingUnit.objectIsEmpty = true')
            ->orWhere('buildingUnit.objectBecomesEmpty = true')
            ->groupBy(groupBy: 'buildingUnit.id')
            ->having(having: 'SUM(matchingResult.score) = 0')
            ->andHaving(having: 'MOD(DATE_DIFF(MIN(matchingTask.createdAt), CURRENT_DATE()), :intervalLength) = 0')
            ->andHaving(having: 'DATE_DIFF(CURRENT_DATE(), MIN(matchingTask.createdAt)) - :noMatchFoundInDays >= 0')
            ->setParameter(key: ':noMatchFoundInDays', value: $noMatchFoundInDays)
            ->setParameter(key: ':intervalLength', value: $intervalLength);

        return $queryBuilder->getQuery()->getResult();
    }

    public function countByAccount(Account $account, bool $withFromSubAccounts, bool $archived, ?bool $objectIsEmpty = null): int
    {
        $queryBuilder = $this->createFindByAccountQueryBuilder(account: $account, withFromSubAccounts: $withFromSubAccounts, archived: $archived);

        $queryBuilder->select(select: 'COUNT(DISTINCT buildingUnit.id)');

        if ($objectIsEmpty !== null) {
            $queryBuilder
                ->andWhere('buildingUnit.objectIsEmpty = :objectIsEmpty')
                ->setParameter(key:'objectIsEmpty', value: $objectIsEmpty);
        }

        return $queryBuilder->getQuery()->getSingleScalarResult();
    }

    public function countByAtFloorGround(Account $account, bool $withFromSubAccounts, bool $archived, ?bool $objectIsEmpty = null): int
    {
        $queryBuilder = $this->createFindByAccountQueryBuilder(account: $account, withFromSubAccounts: $withFromSubAccounts, archived: $archived);

        $queryBuilder->select(select: 'COUNT(DISTINCT buildingUnit.id)');

        $queryBuilder
            ->andWhere('buildingUnit.inFloors LIKE :floor')
            ->setParameter(key: 'floor', value: '%EG%');

        if ($objectIsEmpty !== null) {
            $queryBuilder
                ->andWhere('buildingUnit.objectIsEmpty = :objectIsEmpty')
                ->setParameter(key:'objectIsEmpty', value: $objectIsEmpty);
        }

        return $queryBuilder->getQuery()->getSingleScalarResult();
    }

    /**
     * @param LocationCategory[] $locationCategories
     */
    public function countByLocationCategories(
        Account $account,
        bool $withFromSubAccounts,
        array $locationCategories,
        bool $archived,
        ?bool $objectIsEmpty = null
    ): int {
        $queryBuilder = $this->createFindByAccountQueryBuilder(account: $account, withFromSubAccounts: $withFromSubAccounts, archived: $archived);

        $queryBuilder
            ->select(select: 'COUNT(DISTINCT buildingUnit.id)')
            ->leftJoin(join: 'buildingUnit.building', alias: 'building')
            ->andWhere('building.locationCategory IN (:locationCategories)')
            ->setParameter(key: 'locationCategories', value: $locationCategories);

        if ($objectIsEmpty !== null) {
            $queryBuilder
                ->andWhere('buildingUnit.objectIsEmpty = :objectIsEmpty')
                ->setParameter(key:'objectIsEmpty', value: $objectIsEmpty);
        }

        return $queryBuilder->getQuery()->getSingleScalarResult();
    }

    public function countByPropertyVacancyDataFilter(
        Account $account,
        bool $withFromSubAccounts,
        bool $archived,
        ?bool $objectIsEmpty = null,
        ?PropertyVacancyDataFilter $propertyVacancyDataFilter = null,
    ): int {
        $queryBuilder = $this->createFindByAccountQueryBuilder(account: $account, withFromSubAccounts: $withFromSubAccounts, archived: $archived);

        $queryBuilder->select(select: 'COUNT(DISTINCT buildingUnit.id)');

        if ($propertyVacancyDataFilter !== null) {
            if ($propertyVacancyDataFilter->getOnlyGroundFloor() === true) {
                $queryBuilder
                    ->andWhere('buildingUnit.inFloors LIKE :floor')
                    ->setParameter(key: 'floor', value: '%EG%');
            }

            if ($propertyVacancyDataFilter->getQuarterPlace() !== null) {
                $queryBuilder
                    ->andWhere('address.quarterPlace = :quarterPlace')
                    ->setParameter(key: 'quarterPlace', value: $propertyVacancyDataFilter->getQuarterPlace());
            }

            if (empty($propertyVacancyDataFilter->getLocationCategories()) === false) {
                $queryBuilder
                    ->innerJoin(join: 'buildingUnit.building', alias: 'building')
                    ->andWhere('building.locationCategory IN (:locationCategories)')
                    ->setParameter(key: 'locationCategories', value: $propertyVacancyDataFilter->getLocationCategories());
            }

            if ($objectIsEmpty === null) {
                if ($propertyVacancyDataFilter->getIndustryClassification() !== null) {
                    $industryClassification = $propertyVacancyDataFilter->getIndustryClassification();

                    $expression = $queryBuilder->expr()->orX();

                    $expression
                        ->add('buildingUnit.objectIsEmpty = false AND buildingUnit.reuseAgreed = false AND buildingUnit.currentUsageIndustryClassification = :industryClassification')
                        ->add('buildingUnit.objectIsEmpty = true AND buildingUnit.pastUsageIndustryClassification = :industryClassification')
                        ->add('buildingUnit.reuseAgreed = true AND buildingUnit.futureUsageIndustryClassification = :industryClassification')
                        ->add('propertyUser.propertyUserStatus = :currentPropertyUserStatus AND propertyUser.industryClassification = :industryClassification')
                        ->add('propertyUser.propertyUserStatus = :formerPropertyUserStatus AND propertyUser.industryClassification = :industryClassification');

                    $queryBuilder
                        ->andWhere($expression)
                        ->setParameter(key: 'industryClassification', value: $industryClassification)
                        ->setParameter(key: 'currentPropertyUserStatus', value: PropertyUserStatus::CURRENT_PROPERTY_USER)
                        ->setParameter(key: 'formerPropertyUserStatus', value: PropertyUserStatus::FORMER_PROPERTY_USER);
                }
            }
        }

        if ($objectIsEmpty !== null) {
            if ($propertyVacancyDataFilter !== null) {
                if (empty($propertyVacancyDataFilter->getVacancyReasons()) === false) {
                    $queryBuilder
                        ->andWhere('buildingUnit.vacancyReason IN (:vacancyReasons)')
                        ->setParameter(key: 'vacancyReasons', value: $propertyVacancyDataFilter->getVacancyReasons());
                }

                if ($propertyVacancyDataFilter->getIndustryClassification() !== null) {
                    $industryClassification = $propertyVacancyDataFilter->getIndustryClassification();

                    $expression = $queryBuilder->expr()->orX();

                    $expression
                        ->add('buildingUnit.pastUsageIndustryClassification = :industryClassification')
                        ->add('propertyUser.propertyUserStatus = :formerPropertyUserStatus and propertyUser.industryClassification = :industryClassification');

                    $queryBuilder
                        ->andWhere($expression)
                        ->setParameter(key: 'industryClassification', value: $industryClassification)
                        ->setParameter(key: 'formerPropertyUserStatus', value: PropertyUserStatus::FORMER_PROPERTY_USER);
                }
            }

            $queryBuilder
                ->andWhere('buildingUnit.objectIsEmpty = :objectIsEmpty')
                ->setParameter(key:'objectIsEmpty', value: $objectIsEmpty);
        }

        return $queryBuilder->getQuery()->getSingleScalarResult();
    }

    public function countByCurrentUsage(Account $account, bool $withFromSubAccounts, ?IndustryClassification $industryClassification = null): int
    {
        $queryBuilder = $this->createFindByAccountQueryBuilder(account: $account, withFromSubAccounts: $withFromSubAccounts, archived: false);

        $queryBuilder
            ->select(select: 'COUNT(DISTINCT buildingUnit.id)')
            ->andWhere('buildingUnit.objectIsEmpty = false')
            ->andWhere('buildingUnit.reuseAgreed = false');

        if ($industryClassification === null) {
            $queryBuilder->andWhere('buildingUnit.currentUsageIndustryClassification IS NULL');
        } else {
            $queryBuilder
                ->andWhere('buildingUnit.currentUsageIndustryClassification = :industryClassification')
                ->setParameter(key: 'industryClassification', value: $industryClassification);
        }

        return $queryBuilder->getQuery()->getSingleScalarResult();
    }

    /**
     * @return BuildingUnit[]
     */
    public function findToBeDeletedFromAllAccounts(?int $timeTillHardDelete = null): array
    {
        $queryBuilder = $this->createQueryBuilder(alias: 'buildingUnit');

        $queryBuilder->where(predicates: 'buildingUnit.deleted = true');

        if ($timeTillHardDelete !== null) {
            $deleteFromDateTime = new \DateTime(strtolower(string: '-' . $timeTillHardDelete . ' days'));

            $queryBuilder
                ->andWhere('buildingUnit.deletedAt <= :deleteFromDateTime')
                ->setParameter(key: 'deleteFromDateTime', value: $deleteFromDateTime);
        }

        return $queryBuilder->getQuery()->getResult();
    }

    private function createFindByAccountQueryBuilder(
        Account $account,
        bool $withFromSubAccounts,
        ?bool $archived = null,
        ?BuildingUnitFilter $buildingUnitFilter = null,
        ?BuildingUnitSearch $buildingUnitSearch = null,
        ?SortingOption $sortingOption = null
    ): QueryBuilder {
        $queryBuilder = $this->createQueryBuilder(alias: 'buildingUnit');

        $queryBuilder
            ->addSelect(select: 'address')
            ->addSelect(select: 'place')
            ->addSelect(select: 'propertyMarketingInformation')
            ->addSelect(select: 'possibleUsageIndustryClassification')
            ->addSelect(select: 'propertyPricingInformation')
            ->addSelect(select: 'currentUsageIndustryClassification')
            ->addSelect(select: 'pastUsageIndustryClassification')
            ->addSelect(select: 'futureUsageIndustryClassification')
            ->addSelect(select: 'propertyUser')
            ->addSelect(select: 'propertyOwner')
            ->addSelect(select: 'propertyContactPerson')
            ->addSelect(select: 'createdByAccountUser')
            ->addSelect(select: 'externalContactPerson')
            ->addSelect(select: 'geolocationPoint')
            ->addSelect(select: 'geolocationPolygon')
            ->addSelect(select: 'openImmoAdditionalInformation')
            ->addSelect(select: 'assignedToAccountUser');

        if ($withFromSubAccounts === true) {
            $queryBuilder->innerJoin(
                join: 'buildingUnit.account',
                alias: 'account',
                conditionType: Join::WITH,
                condition: '(account.parentAccount = :account OR account = :account) AND account.deleted = false AND account.enabled = true'
            );
        } else {
            $queryBuilder->innerJoin(
                join: 'buildingUnit.account',
                alias: 'account',
                conditionType: Join::WITH,
                condition: 'account = :account AND account.deleted = false AND account.enabled = true'
            );
        }

        $queryBuilder
            ->leftJoin(join: 'buildingUnit.address', alias: 'address')
            ->leftJoin(join: 'address.place', alias: 'place')
            ->leftJoin(join: 'address.quarterPlace', alias: 'quarterPlace')
            ->leftJoin(join: 'buildingUnit.propertyMarketingInformation', alias: 'propertyMarketingInformation')
            ->leftJoin(join: 'propertyMarketingInformation.possibleUsageIndustryClassifications', alias: 'possibleUsageIndustryClassification')
            ->leftJoin(join: 'propertyMarketingInformation.propertyPricingInformation', alias: 'propertyPricingInformation')
            ->leftJoin(join: 'buildingUnit.currentUsageIndustryClassification', alias: 'currentUsageIndustryClassification')
            ->leftJoin(join: 'buildingUnit.pastUsageIndustryClassification', alias: 'pastUsageIndustryClassification')
            ->leftJoin(join: 'buildingUnit.futureUsageIndustryClassification', alias: 'futureUsageIndustryClassification')
            ->leftJoin(join: 'buildingUnit.propertyUsers', alias: 'propertyUser')
            ->leftJoin(join: 'buildingUnit.propertyOwners', alias: 'propertyOwner')
            ->leftJoin(join: 'buildingUnit.propertyContactPersons', alias: 'propertyContactPerson')
            ->leftJoin(join: 'propertyUser.industryClassification', alias: 'propertyUserIndustryClassification')
            ->leftJoin(join: 'buildingUnit.createdByAccountUser', alias: 'createdByAccountUser')
            ->leftJoin(join: 'buildingUnit.externalContactPerson', alias: 'externalContactPerson')
            ->leftJoin(join: 'buildingUnit.geolocationPoint', alias: 'geolocationPoint')
            ->leftJoin(join: 'buildingUnit.geolocationPolygon', alias: 'geolocationPolygon')
            ->leftJoin(join: 'buildingUnit.openImmoAdditionalInformation', alias: 'openImmoAdditionalInformation')
            ->leftJoin(join: 'buildingUnit.assignedToAccountUser', alias: 'assignedToAccountUser');

        $queryBuilder
            ->where(predicates: 'buildingUnit.account = account')
            ->andWhere('buildingUnit.deleted = false')
            ->setParameter(key: 'account', value: $account);

        if ($archived !== null) {
            $queryBuilder
                ->andWhere('buildingUnit.archived = :archived')
                ->setParameter(key: 'archived', value: $archived);
        }

        if ($buildingUnitSearch !== null && empty($buildingUnitSearch->getText()) === false) {
            $expression = $queryBuilder->expr()->orX();

            $expression
                ->add('buildingUnit.id LIKE :searchTextForId')
                ->add('buildingUnit.name LIKE :searchText')
                ->add('buildingUnit.description LIKE :searchText')
                ->add('buildingUnit.internalNumber LIKE :searchText')
                ->add('buildingUnit.placeDescription LIKE :searchText')
                ->add('buildingUnit.currentUsageDescription LIKE :searchText')
                ->add('buildingUnit.pastUsageDescription LIKE :searchText')
                ->add('buildingUnit.internalNote LIKE :searchText')
                ->add('address.postalCode LIKE :searchText')
                ->add('address.streetName LIKE :searchText')
                ->add('address.houseNumber LIKE :searchText')
                ->add('address.additionalAddressInformation LIKE :searchText')
                ->add('quarterPlace.placeName LIKE :searchText');

            $queryBuilder
                ->andWhere($expression)
                ->setParameter(key: 'searchTextForId', value: $buildingUnitSearch->getText())
                ->setParameter(key: 'searchText', value: '%' . $buildingUnitSearch->getText() . '%');
        }

        if ($buildingUnitFilter !== null) {
            $this->applyBuildingUnitFilterToQueryBuilder(buildingUnitFilter: $buildingUnitFilter, queryBuilder: $queryBuilder);
        }

        if ($sortingOption !== null) {
            self::applyBuildingUnitSortingToQueryBuilder(sortingOption: $sortingOption, queryBuilder: $queryBuilder);
        }

        return $queryBuilder;
    }

    private function createFindWithMatchingResultsByAccountQueryBuilder(
        Account $account,
        bool $withFromSubAccounts,
        bool $archived,
        bool $includeEliminatedMatchingResults,
        ?MatchingFilter $matchingFilter = null,
        ?MatchingSearch $matchingSearch = null
    ): QueryBuilder {
        $queryBuilder = $this->createQueryBuilder(alias: 'buildingUnit');

        $queryBuilder
            ->addSelect(select: 'address')
            ->addSelect(select: 'propertyMarketingInformation')
            ->addSelect(select: 'propertyPricingInformation')
            ->addSelect(select: 'possibleUsageIndustryClassification')
            ->addSelect(select: 'matchingTask')
            ->addSelect(select: 'settlementConceptMatchingTask')
            ->addSelect(select: 'matchingResult')
            ->addSelect(select: 'settlementConceptMatchingResult')
            ->addSelect(select: 'lookingForPropertyRequest')
            ->addSelect(select: 'settlementConcept')
            ->addSelect(select: 'eliminationCriteria')
            ->addSelect(select: 'scoreCriteria')
            ->addSelect(select: 'settlementConceptEliminationCriteria')
            ->addSelect(select: 'settlementConceptScoreCriteria')
            ->addSelect(select: 'propertyVacancyReport')
            ->addSelect(select: 'openImmoAdditionalInformation')
            ->addSelect(select: 'externalContactPerson')
            ->addSelect(select: 'mainImage')
            ->addSelect(select: 'mainImageThumbnailFile');

        if ($withFromSubAccounts === true) {
            $queryBuilder->innerJoin(
                join: 'buildingUnit.account',
                alias: 'account',
                conditionType: Join::WITH,
                condition: '(account.parentAccount = :account OR account = :account) AND account.deleted = false AND account.enabled = true'
            );
        } else {
            $queryBuilder->innerJoin(
                join: 'buildingUnit.account',
                alias: 'account',
                conditionType: Join::WITH,
                condition: 'account = :account AND account.deleted = false AND account.enabled = true'
            );
        }

        $queryBuilder
            ->leftJoin(join: 'buildingUnit.address', alias: 'address')
            ->leftJoin(join: 'address.quarterPlace', alias: 'quarterPlace')
            ->leftJoin(join: 'buildingUnit.propertyMarketingInformation', alias: 'propertyMarketingInformation')
            ->leftJoin(join: 'propertyMarketingInformation.propertyPricingInformation', alias: 'propertyPricingInformation')
            ->leftJoin(join: 'propertyMarketingInformation.possibleUsageIndustryClassifications', alias: 'possibleUsageIndustryClassification')
            ->leftJoin(join: 'buildingUnit.matchingTask', alias: 'matchingTask')
            ->leftJoin(join: 'buildingUnit.settlementConceptMatchingTask', alias: 'settlementConceptMatchingTask')
            ->leftJoin(
                join: 'buildingUnit.matchingResults',
                alias: 'matchingResult',
                conditionType: Join::WITH,
                condition: 'matchingResult.eliminated = :includeEliminatedMatchingResults'
            )
            ->leftJoin(
                join: 'buildingUnit.settlementConceptMatchingResults',
                alias: 'settlementConceptMatchingResult',
                conditionType: Join::WITH,
                condition: 'settlementConceptMatchingResult.eliminated = :includeEliminatedMatchingResults'
            )
            ->leftJoin(join: 'matchingResult.lookingForPropertyRequest', alias: 'lookingForPropertyRequest')
            ->leftJoin(join: 'settlementConceptMatchingResult.settlementConcept', alias: 'settlementConcept')
            ->leftJoin(join: 'matchingResult.eliminationCriteria', alias: 'eliminationCriteria')
            ->leftJoin(join: 'matchingResult.scoreCriteria', alias: 'scoreCriteria')
            ->leftJoin(join: 'settlementConceptMatchingResult.settlementConceptEliminationCriteria', alias: 'settlementConceptEliminationCriteria')
            ->leftJoin(join: 'settlementConceptMatchingResult.settlementConceptScoreCriteria', alias: 'settlementConceptScoreCriteria')
            ->leftJoin(join: 'buildingUnit.propertyVacancyReport', alias: 'propertyVacancyReport')
            ->leftJoin(join: 'buildingUnit.openImmoAdditionalInformation', alias: 'openImmoAdditionalInformation')
            ->leftJoin(join: 'buildingUnit.externalContactPerson', alias: 'externalContactPerson')
            ->leftJoin(join: 'buildingUnit.mainImage', alias: 'mainImage')
            ->leftJoin(join: 'mainImage.thumbnailFile', alias: 'mainImageThumbnailFile');

        $queryBuilder
            ->where(predicates: 'buildingUnit.account = account')
            ->andWhere('buildingUnit.deleted = false')
            ->andWhere('buildingUnit.archived = :archived')
            ->setParameter(key: 'account', value: $account)
            ->setParameter(key: 'archived', value: $archived)
            ->setParameter(key: 'includeEliminatedMatchingResults', value: $includeEliminatedMatchingResults);

        if ($matchingSearch !== null && empty($matchingSearch->getText()) === false) {
            $expression = $queryBuilder->expr()->orX();

            $expression
                ->add('buildingUnit.id LIKE :searchTextForId')
                ->add('buildingUnit.name LIKE :searchText')
                ->add('buildingUnit.description LIKE :searchText')
                ->add('buildingUnit.internalNumber LIKE :searchText')
                ->add('buildingUnit.placeDescription LIKE :searchText')
                ->add('buildingUnit.currentUsageDescription LIKE :searchText')
                ->add('buildingUnit.pastUsageDescription LIKE :searchText')
                ->add('buildingUnit.internalNote LIKE :searchText')
                ->add('address.postalCode LIKE :searchText')
                ->add('address.streetName LIKE :searchText')
                ->add('address.houseNumber LIKE :searchText')
                ->add('address.additionalAddressInformation LIKE :searchText')
                ->add('quarterPlace.placeName LIKE :searchText');

            $queryBuilder
                ->andWhere($expression)
                ->setParameter(key: 'searchTextForId', value: $matchingSearch->getText())
                ->setParameter(key: 'searchText', value: '%' . $matchingSearch->getText() . '%');
        }

        if ($matchingFilter !== null) {
            $this->applyMatchingFilterToQueryBuilder(matchingFilter: $matchingFilter, queryBuilder: $queryBuilder);
        }

        if ($matchingFilter === null || empty($matchingFilter->getPropertyStatus()) === true) {
            $queryBuilder->andWhere('buildingUnit.objectIsEmpty = true OR buildingUnit.objectBecomesEmpty = true');
        }

        return $queryBuilder;
    }

    private static function applyBuildingUnitFilterToQueryBuilder(BuildingUnitFilter $buildingUnitFilter, QueryBuilder $queryBuilder): void
    {
        $propertyStatus = $buildingUnitFilter->getPropertyStatus();
        $propertyUsageStatus = $buildingUnitFilter->getPropertyUsageStatus();
        $industryClassifications = $buildingUnitFilter->getIndustryClassifications();
        $industryClassificationsLevelTwo = $buildingUnitFilter->getIndustryClassificationsLevelTwo();
        $propertyOfferTypes = $buildingUnitFilter->getPropertyOfferTypes();
        $areaSizeMinimum = $buildingUnitFilter->getAreaSizeMinimum();
        $areaSizeMaximum = $buildingUnitFilter->getAreaSizeMaximum();
        $shopWindowFrontWidthMinimum = $buildingUnitFilter->getShopWindowFrontWidthMinimum();
        $shopWindowFrontWidthMaximum = $buildingUnitFilter->getShopWindowFrontWidthMaximum();
        $lastUpdatedFrom = $buildingUnitFilter->getLastUpdatedFrom();
        $lastUpdatedTill = $buildingUnitFilter->getLastUpdatedTill();
        $barrierFreeAccess = $buildingUnitFilter->getBarrierFreeAccess();
        $assignedToAccountUser = $buildingUnitFilter->getAssignedToAccountUser();

        $industryClassificationlevelTwoIds = [];

        if ($industryClassificationsLevelTwo !== null && count($industryClassificationsLevelTwo) > 0) {
            foreach ($industryClassificationsLevelTwo as $industryClassificationLevelTwo) {
                array_push($industryClassificationlevelTwoIds, $industryClassificationLevelTwo->getId());

                $parentIndustryClassifications = $industryClassificationLevelTwo->getParent();

                foreach ($industryClassifications as $index => $industryClassification) {
                    if ($industryClassification->getId() == $parentIndustryClassifications->getId()) {
                        unset($industryClassifications[$index]);
                    }
                }
            }
        }

        $industryClassificationLevelOneIds = [];

        if (empty($industryClassifications) === false) {
            foreach ($industryClassifications as $industryClassification) {
                array_push($industryClassificationLevelOneIds, $industryClassification->getId());
            }
        }

        if (empty($propertyUsageStatus) === true && empty($industryClassifications) === false) {
            self::propertyUsageStatusFilter(
                propertyUsageStatus: $propertyUsageStatus,
                industryClassificationLevelOneIds: $industryClassificationLevelOneIds,
                industryClassificationLevelTwoIds: $industryClassificationlevelTwoIds,
                queryBuilder: $queryBuilder,
                searchInAllPropertyUsageStatus: true
            );
        } elseif (empty($propertyUsageStatus) === false) {
            self::propertyUsageStatusFilter(
                propertyUsageStatus: $propertyUsageStatus,
                industryClassificationLevelOneIds: $industryClassificationLevelOneIds,
                industryClassificationLevelTwoIds: $industryClassificationlevelTwoIds,
                queryBuilder: $queryBuilder
            );
        }

        if (empty($propertyStatus) === false) {
            $propertyStatusOrxExpression = $queryBuilder->expr()->orX();

            if (in_array(needle: 0, haystack: $propertyStatus) === true) {
                $propertyStatusOrxExpression->add('buildingUnit.objectIsEmpty = true OR buildingUnit.reuseAgreed = true');
            }

            if (in_array(needle: 1, haystack: $propertyStatus) === true) {
                $propertyStatusOrxExpression->add('buildingUnit.objectBecomesEmpty = true');
            }

            if (in_array(needle: 2, haystack: $propertyStatus) === true) {
                $propertyStatusOrxExpression->add(
                    $queryBuilder->expr()->andX('buildingUnit.objectIsEmpty = false', 'buildingUnit.objectBecomesEmpty = false', 'buildingUnit.reuseAgreed = false')
                );
            }

            if (in_array(needle: 3, haystack: $propertyStatus) === true) {
                $propertyStatusOrxExpression->add('buildingUnit.keyProperty = true');
            }

            $queryBuilder->andWhere($propertyStatusOrxExpression);
        }

        if (empty($propertyOfferTypes) === false) {
            $queryBuilder
                ->andWhere('propertyMarketingInformation.propertyOfferType IN (:propertyOfferTypes)')
                ->setParameter(key: ':propertyOfferTypes', value: $propertyOfferTypes);
        }

        if ($areaSizeMinimum !== null) {
            $queryBuilder
                ->andWhere('buildingUnit.areaSize >= :areaSizeMinimum')
                ->setParameter(key: 'areaSizeMinimum', value: $areaSizeMinimum);
        }

        if ($areaSizeMaximum !== null) {
            $queryBuilder
                ->andWhere('buildingUnit.areaSize <= :areaSizeMaximum')
                ->setParameter(key: 'areaSizeMaximum', value: $areaSizeMaximum);
        }

        if ($shopWindowFrontWidthMinimum !== null) {
            $queryBuilder
                ->andWhere('buildingUnit.shopWindowFrontWidth >= :shopWindowFrontWidthMinimum')
                ->setParameter(key: 'shopWindowFrontWidthMinimum', value: $shopWindowFrontWidthMinimum);
        }

        if ($shopWindowFrontWidthMaximum !== null) {
            $queryBuilder
                ->andWhere('buildingUnit.shopWindowFrontWidth <= :shopWindowFrontWidthMaximum')
                ->setParameter(key: 'shopWindowFrontWidthMaximum', value: $shopWindowFrontWidthMaximum);
        }

        if ($lastUpdatedFrom !== null) {
            $queryBuilder
                ->andWhere('buildingUnit.updatedAt >= :lastUpdatedFrom')
                ->setParameter(key: 'lastUpdatedFrom', value: $lastUpdatedFrom);
        }

        if ($lastUpdatedTill !== null) {
            $queryBuilder
                ->andWhere('buildingUnit.updatedAt <= :lastUpdatedTill')
                ->setParameter(key: 'lastUpdatedTill', value: $lastUpdatedTill->setTime(hour: 23, minute: 59, second: 59));
        }

        if ($barrierFreeAccess !== null) {
            $queryBuilder
                ->andWhere('buildingUnit.barrierFreeAccess = :barrierFreeAccess')
                ->setParameter(key: 'barrierFreeAccess', value: $barrierFreeAccess);
        }

        $propertyOfferFilterExpression = self::buildPropertyOfferFilterExpression(buildingUnitFilter: $buildingUnitFilter, queryBuilder: $queryBuilder);

        $queryBuilder->andWhere($propertyOfferFilterExpression);

        if ($assignedToAccountUser !== null) {
            $queryBuilder
                ->andWhere('buildingUnit.assignedToAccountUser = :assignedToAccountUser')
                ->setParameter(key: 'assignedToAccountUser', value: $assignedToAccountUser);
        }
    }

    private function applyMatchingFilterToQueryBuilder(MatchingFilter $matchingFilter, QueryBuilder $queryBuilder): void
    {
        $propertyStatus = $matchingFilter->getPropertyStatus();
        $industryClassifications = $matchingFilter->getIndustryClassifications();
        $propertyOfferTypes = $matchingFilter->getPropertyOfferTypes();
        $areaSizeMinimum = $matchingFilter->getAreaSizeMinimum();
        $areaSizeMaximum = $matchingFilter->getAreaSizeMaximum();
        $shopWindowFrontWidthMinimum = $matchingFilter->getShopWindowFrontWidthMinimum();
        $shopWindowFrontWidthMaximum = $matchingFilter->getShopWindowFrontWidthMaximum();
        $lastMatchingFrom = $matchingFilter->getLastMatchingFrom();
        $lastMatchingTill = $matchingFilter->getLastMatchingTill();
        $matchingStatus = $matchingFilter->getMatchingStatus();
        $barrierFreeAccess = $matchingFilter->getBarrierFreeAccess();
        $assignedToAccountUser = $matchingFilter->getAssignedToAccountUser();

        if (empty($industryClassifications) === false) {
            $queryBuilder->leftJoin(join: 'propertyMarketingInformation.possibleUsageIndustryClassifications', alias: 'possibleUsageIndustryClassifications');

            $industryClassificationsOrxExpression = $queryBuilder->expr()->orX();

            $industryClassificationsOrxExpression
                ->add('buildingUnit.currentUsageIndustryClassification IN (:industryClassifications)')
                ->add('buildingUnit.pastUsageIndustryClassification IN (:industryClassifications)')
                ->add('possibleUsageIndustryClassifications IN (:industryClassifications)');

            $queryBuilder
                ->andWhere($industryClassificationsOrxExpression)
                ->setParameter(key: 'industryClassifications', value: $industryClassifications);
        }

        if (empty($propertyStatus) === false) {
            $propertyStatusOrxExpression = $queryBuilder->expr()->orX();

            if (in_array(needle: 0, haystack: $propertyStatus) === true) {
                $propertyStatusOrxExpression->add('buildingUnit.objectIsEmpty = true');
            }

            if (in_array(needle: 1, haystack: $propertyStatus) === true) {
                $propertyStatusOrxExpression->add('buildingUnit.objectBecomesEmpty = true');
            }

            if (in_array(needle: 2, haystack: $propertyStatus)) {
                $propertyStatusOrxExpression->add('buildingUnit.keyProperty = true');
            }

            $queryBuilder->andWhere($propertyStatusOrxExpression);
        }

        if (empty($propertyOfferTypes) === false) {
            $queryBuilder
                ->andWhere('propertyMarketingInformation.propertyOfferType IN (:propertyOfferTypes)')
                ->setParameter(key: ':propertyOfferTypes', value: $propertyOfferTypes);
        }

        if ($areaSizeMinimum !== null) {
            $queryBuilder
                ->andWhere('buildingUnit.areaSize >= :areaSizeMinimum')
                ->setParameter(key: 'areaSizeMinimum', value: $areaSizeMinimum);
        }

        if ($areaSizeMaximum !== null) {
            $queryBuilder
                ->andWhere('buildingUnit.areaSize <= :areaSizeMaximum')
                ->setParameter(key: 'areaSizeMaximum', value: $areaSizeMaximum);
        }

        if ($shopWindowFrontWidthMinimum !== null) {
            $queryBuilder
                ->andWhere('buildingUnit.shopWindowFrontWidth >= :shopWindowFrontWidthMinimum')
                ->setParameter(key: 'shopWindowFrontWidthMinimum', value: $shopWindowFrontWidthMinimum);
        }

        if ($shopWindowFrontWidthMaximum !== null) {
            $queryBuilder
                ->andWhere('buildingUnit.shopWindowFrontWidth <= :shopWindowFrontWidthMaximum')
                ->setParameter(key: 'shopWindowFrontWidthMaximum', value: $shopWindowFrontWidthMaximum);
        }

        if ($lastMatchingFrom !== null) {
            $queryBuilder
                ->andWhere('matchingTask.updatedAt >= :lastUpdatedFrom')
                ->setParameter(key: 'lastUpdatedFrom', value: $lastMatchingFrom);
        }

        if ($lastMatchingTill !== null) {
            $queryBuilder
                ->andWhere('matchingTask.updatedAt <= :lastUpdatedTill')
                ->setParameter(key: 'lastUpdatedTill', value: $lastMatchingTill);
        }

        if (empty($matchingStatus) === false) {
            $matchingStatusOrxExpression = $queryBuilder->expr()->orX();

            if (in_array(needle: 0, haystack: $matchingStatus) === true) {
                $matchingStatusOrxExpression->add('matchingResult IS NOT NULL');
            }

            if (in_array(needle: 1, haystack: $matchingStatus) === true) {
                $matchingStatusOrxExpression->add('matchingResult IS NULL AND matchingTask IS NOT NULL');
            }

            if (in_array(needle: 2, haystack: $matchingStatus) === true) {
                $matchingStatusOrxExpression->add('matchingTask IS NULL');
            }

            $queryBuilder->andWhere($matchingStatusOrxExpression);
        }

        if ($barrierFreeAccess !== null) {
            $queryBuilder
                ->andWhere('buildingUnit.barrierFreeAccess = :barrierFreeAccess')
                ->setParameter(key: 'barrierFreeAccess', value: $barrierFreeAccess);
        }

        $propertyOfferFilterExpression = self::buildPropertyOfferFilterExpression(buildingUnitFilter: $matchingFilter, queryBuilder: $queryBuilder);

        $queryBuilder->andWhere($propertyOfferFilterExpression);

        if ($assignedToAccountUser !== null) {
            $queryBuilder
                ->andWhere('buildingUnit.assignedToAccountUser = :assignedToAccountUser')
                ->setParameter(key: 'assignedToAccountUser', value: $assignedToAccountUser);
        }
    }

    private static function buildPropertyOfferFilterExpression(AbstractBuildingUnitFilter $buildingUnitFilter, QueryBuilder $queryBuilder): Orx
    {
        $propertyOfferTypes = $buildingUnitFilter->getPropertyOfferTypes();
        $purchasePriceGross = $buildingUnitFilter->getPurchasePriceGross();
        $pricePerSquareMeter = $buildingUnitFilter->getPurchasePricePerSquareMeter();
        $coldRent = $buildingUnitFilter->getColdRent();
        $rentalPricePerSquareMeter = $buildingUnitFilter->getRentalPricePerSquareMeter();
        $lease = $buildingUnitFilter->getLease();

        $propertyOfferFilterExpression = $queryBuilder->expr()->orX();

        if ($propertyOfferTypes !== null && in_array(needle: PropertyOfferType::PURCHASE, haystack: $propertyOfferTypes) === true) {
            if ($purchasePriceGross !== null) {
                $buyExpression = $queryBuilder->expr()->andX();

                if ($purchasePriceGross->getMinimumValue() !== null) {
                    $buyExpressionMin = $queryBuilder->expr()->orX();

                    $buyExpressionMin->add(<<<EOT
                        CASE
                            WHEN propertyPricingInformation.purchasePriceGross IS NOT NULL THEN propertyPricingInformation.purchasePriceGross 
                            ELSE propertyPricingInformation.purchasePriceNet 
                        END >= :purchasePriceMinimum
                        EOT
                    );

                    $queryBuilder->setParameter(key: ':purchasePriceMinimum', value: $purchasePriceGross->getMinimumValue());

                    $buyExpression->add($buyExpressionMin);
                }

                if ($purchasePriceGross->getMaximumValue() !== null) {
                    $buyExpressionMax = $queryBuilder->expr()->orX();

                    $buyExpressionMax->add(<<<EOT
                        CASE
                            WHEN propertyPricingInformation.purchasePriceGross IS NOT NULL THEN propertyPricingInformation.purchasePriceGross 
                            ELSE propertyPricingInformation.purchasePriceNet 
                        END <= :purchasePriceMaximum
                        EOT
                    );

                    $queryBuilder->setParameter(key: ':purchasePriceMaximum', value: $purchasePriceGross->getMaximumValue());

                    $buyExpression->add($buyExpressionMax);
                }

                $propertyOfferFilterExpression->add($buyExpression);
            }

            if ($pricePerSquareMeter !== null) {
                $buyPerSquareExpression = $queryBuilder->expr()->andX();

                if ($pricePerSquareMeter->getMinimumValue() !== null) {
                    $buyPerSquareExpression->add('propertyPricingInformation.purchasePricePerSquareMeter >= :purchasePricePerSquareMeterMinimum');

                    $queryBuilder->setParameter(key: ':purchasePricePerSquareMeterMinimum', value: $pricePerSquareMeter->getMinimumValue());
                }

                if ($pricePerSquareMeter->getMaximumValue() !== null) {
                    $buyPerSquareExpression->add('propertyPricingInformation.purchasePricePerSquareMeter <= :purchasePricePerSquareMeterMaximum');

                    $queryBuilder->setParameter(key: ':purchasePricePerSquareMeterMaximum', value: $pricePerSquareMeter->getMaximumValue());
                }

                $propertyOfferFilterExpression->add($buyPerSquareExpression);
            }
        }

        if ($propertyOfferTypes !== null && in_array(needle: PropertyOfferType::RENT, haystack: $propertyOfferTypes) === true) {
            if ($coldRent !== null) {
                $rentExpression = $queryBuilder->expr()->andX();

                if ($coldRent->getMinimumValue() !== null) {
                    $rentExpression->add('propertyPricingInformation.coldRent >= :coldRentMinimum');

                    $queryBuilder->setParameter(key: ':coldRentMinimum', value: $coldRent->getMinimumValue());
                }

                if ($coldRent->getMaximumValue() !== null) {
                    $rentExpression->add('propertyPricingInformation.coldRent <= :coldRentMaximum');

                    $queryBuilder->setParameter(key: ':coldRentMaximum', value: $coldRent->getMaximumValue());
                }

                $propertyOfferFilterExpression->add($rentExpression);
            }

            if ($rentalPricePerSquareMeter !== null) {
                $rentPerSquareExpression = $queryBuilder->expr()->andX();

                if ($rentalPricePerSquareMeter->getMinimumValue() !== null) {
                    $rentPerSquareExpression->add('propertyPricingInformation.rentalPricePerSquareMeter >= :rentalPricePerSquareMeterMinimum');

                    $queryBuilder->setParameter(key: 'rentalPricePerSquareMeterMinimum', value: $rentalPricePerSquareMeter->getMinimumValue());
                }

                if ($rentalPricePerSquareMeter->getMaximumValue() !== null) {
                    $rentPerSquareExpression->add('propertyPricingInformation.rentalPricePerSquareMeter <= :rentalPricePerSquareMeterMaximum');

                    $queryBuilder->setParameter(key: ':rentalPricePerSquareMeterMaximum', value: $rentalPricePerSquareMeter->getMaximumValue());
                }

                $propertyOfferFilterExpression->add($rentPerSquareExpression);
            }
        }

        if ($propertyOfferTypes !== null && in_array(needle: PropertyOfferType::LEASE, haystack: $propertyOfferTypes) === true) {
            if ($lease !== null) {
                $leaseExpression = $queryBuilder->expr()->andX();

                if ($lease->getMinimumValue() !== null) {
                    $leaseExpression->add('propertyPricingInformation.lease >= :leaseMinimum');

                    $queryBuilder->setParameter(key: ':leaseMinimum', value: $lease->getMinimumValue());
                }

                if ($lease->getMaximumValue() !== null) {
                    $leaseExpression->add('propertyPricingInformation.lease <= :leaseMaximum');

                    $queryBuilder->setParameter(key: ':leaseMaximum', value: $lease->getMaximumValue());
                }

                $propertyOfferFilterExpression->add($leaseExpression);
            }
        }

        return $propertyOfferFilterExpression;
    }

    /**
     * @param int[] $propertyUsageStatus
     * @param int[] $industryClassificationLevelOneIds
     * @param int[] $industryClassificationLevelTwoIds
     */
    private static function propertyUsageStatusFilter(
        array $propertyUsageStatus,
        array $industryClassificationLevelOneIds,
        array $industryClassificationLevelTwoIds,
        QueryBuilder $queryBuilder,
        bool $searchInAllPropertyUsageStatus = false
    ): void {
        $addIndustryClassificationLevelTwoParameter = false;
        $propertyUsageExpression = $queryBuilder->expr()->orX();

        if (in_array(needle: 0, haystack: $propertyUsageStatus) || $searchInAllPropertyUsageStatus) {
            $currentPropertyUsageExpression = $queryBuilder->expr()->orX();
            $currentPropertyUsageExpression->add('buildingUnit.currentUsageIndustryClassification IN (:industryClassifications)');

            if (count($industryClassificationLevelTwoIds) > 0) {
                $addIndustryClassificationLevelTwoParameter = true;

                $currentIndustryClassificationsExpression = $queryBuilder->expr()->andX();
                $currentIndustryClassificationsExpression->add('propertyUser.propertyUserStatus = 0');

                $industryClassificationLevelTwoExpression = $queryBuilder->expr()->orX();

                $industryClassificationLevelTwoExpression->add('propertyUserIndustryClassification IN (:industryClassificationsLevelTwo)');
                $industryClassificationLevelTwoExpression->add('propertyUserIndustryClassification.parent IN (:industryClassificationsLevelTwo)');
                $currentIndustryClassificationsExpression->add($industryClassificationLevelTwoExpression);

                $currentPropertyUsageExpression->add($currentIndustryClassificationsExpression);
            }

            $propertyUsageExpression->add($currentPropertyUsageExpression);
        }

        if (in_array(needle: 1, haystack: $propertyUsageStatus) || $searchInAllPropertyUsageStatus) {
            $pastPropertyUsageExpression = $queryBuilder->expr()->orX();
            $pastPropertyUsageExpression->add('buildingUnit.pastUsageIndustryClassification IN (:industryClassifications)');

            if (count($industryClassificationLevelTwoIds) > 0) {
                $addIndustryClassificationLevelTwoParameter = true;

                $pastIndustryClassificationsExpression = $queryBuilder->expr()->andX();
                $pastIndustryClassificationsExpression->add('propertyUser.propertyUserStatus = 1');

                $industryClassificationLevelTwoExpression = $queryBuilder->expr()->orX();
                $industryClassificationLevelTwoExpression->add('propertyUserIndustryClassification IN (:industryClassificationsLevelTwo)');
                $industryClassificationLevelTwoExpression->add('propertyUserIndustryClassification.parent IN (:industryClassificationsLevelTwo)');

                $pastIndustryClassificationsExpression->add($industryClassificationLevelTwoExpression);

                $pastPropertyUsageExpression->add($pastIndustryClassificationsExpression);
            }

            $propertyUsageExpression->add($pastPropertyUsageExpression);
        }

        if (in_array(needle: 2, haystack: $propertyUsageStatus) || $searchInAllPropertyUsageStatus) {
            $propertyUsageExpression->add('buildingUnit.futureUsageIndustryClassification IN (:industryClassifications)');
        }

        $queryBuilder->andWhere($propertyUsageExpression);
        $queryBuilder->setParameter(key: ':industryClassifications', value: $industryClassificationLevelOneIds);

        if ($addIndustryClassificationLevelTwoParameter) {
            $queryBuilder->setParameter(key: ':industryClassificationsLevelTwo', value: $industryClassificationLevelTwoIds);
        }
    }

    private static function applyBuildingUnitSortingToQueryBuilder(SortingOption $sortingOption, QueryBuilder $queryBuilder): void
    {
        if ($sortingOption->getSortingBy() === 'objektnummer') {
            $queryBuilder->orderBy(sort: 'buildingUnit.id', order: $sortingOption->getSortingDirection()->getOrderByDirection());
        }

        if ($sortingOption->getSortingBy() === 'bezeichnung') {
            $queryBuilder->orderBy(sort: 'buildingUnit.name', order: $sortingOption->getSortingDirection()->getOrderByDirection());
        }

        if ($sortingOption->getSortingBy() === 'strasse') {
            $queryBuilder
                ->orderBy(sort: 'address.streetName', order: $sortingOption->getSortingDirection()->getOrderByDirection())
                ->addOrderBy(sort: 'ABS(address.houseNumber)', order: $sortingOption->getSortingDirection()->getOrderByDirection());
        }

        if ($sortingOption->getSortingBy() === 'plz') {
            $queryBuilder
                ->orderBy(sort: 'ABS(address.postalCode)', order: $sortingOption->getSortingDirection()->getOrderByDirection())
                ->addOrderBy(sort: 'place.placeName', order: $sortingOption->getSortingDirection()->getOrderByDirection());
        }

        if ($sortingOption->getSortingBy() === 'nutzung') {
            $queryBuilder->orderBy(sort: 'currentUsageIndustryClassification.name', order: $sortingOption->getSortingDirection()->getOrderByDirection());
        }

        if ($sortingOption->getSortingBy() === 'flaeche') {
            $queryBuilder->orderBy(sort: 'buildingUnit.areaSize', order: $sortingOption->getSortingDirection()->getOrderByDirection());
        }

        if ($sortingOption->getSortingBy() === 'sachbearbeiter') {
            $queryBuilder->orderBy(sort: 'assignedToAccountUser.fullName', order: $sortingOption->getSortingDirection()->getOrderByDirection());
        }

        if ($sortingOption->getSortingBy() === 'letzte aenderung') {
            $queryBuilder->orderBy(sort: 'buildingUnit.updatedAt', order: $sortingOption->getSortingDirection()->getOrderByDirection());
        }

        if ($sortingOption->getSortingBy() === 'leerstand') {
            $orderByExpression = $queryBuilder->expr()->andX();

            $orderByExpression->add(<<<EOT
                CASE 
                    WHEN buildingUnit.objectIsEmptySince IS NOT NULL THEN buildingUnit.objectIsEmptySince 
                    WHEN buildingUnit.objectBecomesEmptyFrom IS NOT NULL THEN buildingUnit.objectBecomesEmptyFrom
                    ELSE buildingUnit.objectIsEmptySince
                END
                EOT
            );

            $queryBuilder->orderBy(sort: $orderByExpression, order: $sortingOption->getSortingDirection()->getOrderByDirection());
        }
    }
}
