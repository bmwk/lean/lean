<?php

declare(strict_types=1);

namespace App\Repository\Property;

use App\Domain\Entity\Property\PropertyPricingInformation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PropertyPricingInformation|null find($id, $lockMode = null, $lockVersion = null)
 * @method PropertyPricingInformation|null findOneBy(array $criteria, array $orderBy = null)
 * @method PropertyPricingInformation[]    findAll()
 * @method PropertyPricingInformation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PropertyPricingInformationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PropertyPricingInformation::class);
    }
}
