<?php

declare(strict_types=1);

namespace App\Repository\Property;

use App\Domain\Entity\Property\OpenImmoAdditionalInformation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OpenImmoAdditionalInformation|null find($id, $lockMode = null, $lockVersion = null)
 * @method OpenImmoAdditionalInformation|null findOneBy(array $criteria, array $orderBy = null)
 * @method OpenImmoAdditionalInformation[]    findAll()
 * @method OpenImmoAdditionalInformation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OpenImmoAdditionalInformationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OpenImmoAdditionalInformation::class);
    }
}
