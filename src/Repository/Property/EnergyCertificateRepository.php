<?php

declare(strict_types=1);

namespace App\Repository\Property;

use App\Domain\Entity\Property\EnergyCertificate;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method EnergyCertificate|null find($id, $lockMode = null, $lockVersion = null)
 * @method EnergyCertificate|null findOneBy(array $criteria, array $orderBy = null)
 * @method EnergyCertificate[]    findAll()
 * @method EnergyCertificate[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EnergyCertificateRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EnergyCertificate::class);
    }
}
