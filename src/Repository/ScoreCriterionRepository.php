<?php

declare(strict_types=1);

namespace App\Repository;

use App\Domain\Entity\ScoreCriterion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ScoreCriterion|null find($id, $lockMode = null, $lockVersion = null)
 * @method ScoreCriterion|null findOneBy(array $criteria, array $orderBy = null)
 * @method ScoreCriterion[]    findAll()
 * @method ScoreCriterion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ScoreCriterionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ScoreCriterion::class);
    }
}
