<?php

declare(strict_types=1);

namespace App\Repository;

use App\Domain\Entity\Account;
use App\Domain\Entity\PropertyExcelImport;
use App\Domain\Entity\SortingOption\SortingOption;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PropertyExcelImport|null find($id, $lockMode = null, $lockVersion = null)
 * @method PropertyExcelImport|null findOneBy(array $criteria, array $orderBy = null)
 * @method PropertyExcelImport[]    findAll()
 * @method PropertyExcelImport[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PropertyExcelImportRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PropertyExcelImport::class);
    }

    /**
     * @return PropertyExcelImport[]
     */
    public function findByAccount(Account $account, ?SortingOption $sortingOption = null): array
    {
        $queryBuilder = $this->createFindByAccountQueryBuilder(account: $account, sortingOption: $sortingOption);

        return $queryBuilder->getQuery()->getResult();
    }

    public function findPaginatedByAccount(
        Account $account,
        int $firstResult,
        int $maxResults,
        ?SortingOption $sortingOption = null
    ): Paginator {
        $queryBuilder = $this->createFindByAccountQueryBuilder(account: $account, sortingOption: $sortingOption);

        $queryBuilder
            ->setFirstResult($firstResult)
            ->setMaxResults($maxResults);

        $queryBuilder->addOrderBy(sort: 'propertyExcelImport.createdAt', order: 'DESC');

        return new Paginator(query: $queryBuilder->getQuery(), fetchJoinCollection: true);
    }

    private function createFindByAccountQueryBuilder(Account $account, ?SortingOption $sortingOption = null): QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder(alias: 'propertyExcelImport');

        $queryBuilder
            ->addSelect(select: 'account')
            ->addSelect(select: 'propertyExcelImport')
            ->innerJoin(
                join: 'propertyExcelImport.account',
                alias: 'account',
                conditionType: Join::WITH,
                condition: 'account = :account AND account.deleted = false AND account.enabled = true'
            )
            ->leftJoin(join: 'propertyExcelImport.file', alias: 'file')
            ->where(predicates: 'propertyExcelImport.account = account')
            ->setParameter(key: 'account', value: $account);

        if ($sortingOption !== null) {
            self::applyPropertyExcelImportSortingToQueryBuilder(sortingOption: $sortingOption, queryBuilder: $queryBuilder);
        }

        return $queryBuilder;
    }

    private static function applyPropertyExcelImportSortingToQueryBuilder(SortingOption $sortingOption, QueryBuilder $queryBuilder): void
    {
        if ($sortingOption->getSortingBy() === 'dateiname') {
            $queryBuilder->orderBy(sort: 'file.fileName', order: $sortingOption->getSortingDirection()->getOrderByDirection());
        }

        if ($sortingOption->getSortingBy() === 'erstellt_am') {
            $queryBuilder->orderBy(sort: 'propertyExcelImport.createdAt', order: $sortingOption->getSortingDirection()->getOrderByDirection());
        }
    }
}
