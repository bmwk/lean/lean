<?php

declare(strict_types=1);

namespace App\Repository;

use App\Domain\Entity\Account;
use App\Domain\Entity\HystreetConfiguration;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method HystreetConfiguration|null find($id, $lockMode = null, $lockVersion = null)
 * @method HystreetConfiguration|null findOneBy(array $criteria, array $orderBy = null)
 * @method HystreetConfiguration[]    findAll()
 * @method HystreetConfiguration[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HystreetConfigurationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HystreetConfiguration::class);
    }

    public function findOneByAccount(Account $account): ?HystreetConfiguration
    {
        return $this->findOneBy(criteria: ['account' => $account]);
    }
}
