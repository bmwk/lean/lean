<?php

declare(strict_types=1);

namespace App\Repository;

use App\Domain\Entity\GeolocationPoint;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method GeolocationPoint|null find($id, $lockMode = null, $lockVersion = null)
 * @method GeolocationPoint|null findOneBy(array $criteria, array $orderBy = null)
 * @method GeolocationPoint[]    findAll()
 * @method GeolocationPoint[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GeolocationPointRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, GeolocationPoint::class);
    }
}
