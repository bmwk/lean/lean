<?php

declare(strict_types=1);

namespace App\Repository;

use App\Domain\Entity\FeedbackGroup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FeedbackGroup|null find($id, $lockMode = null, $lockVersion = null)
 * @method FeedbackGroup|null findOneBy(array $criteria, array $orderBy = null)
 * @method FeedbackGroup[]    findAll()
 * @method FeedbackGroup[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FeedbackGroupRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FeedbackGroup::class);
    }
}
