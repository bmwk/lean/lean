<?php

declare(strict_types=1);

namespace App\Repository;

use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\LookingForPropertyRequest\LookingForPropertyRequest;
use App\Domain\Entity\Notification\Notification;
use App\Domain\Entity\Notification\NotificationFilter;
use App\Domain\Entity\Notification\NotificationType;
use App\Domain\Entity\Person\Occurrence;
use App\Domain\Entity\Property\BuildingUnit;
use App\Domain\Entity\SortingOption\SortingOption;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Notification|null find($id, $lockMode = null, $lockVersion = null)
 * @method Notification|null findOneBy(array $criteria, array $orderBy = null)
 * @method Notification[]    findAll()
 * @method Notification[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NotificationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Notification::class);
    }

    /**
     * @return Notification[]
     */
    public function findByAccountUser(AccountUser $accountUser, ?NotificationFilter $notificationFilter = null, ?int $limit = null): array
    {
        $queryBuilder = $this->createFindByAccountUserQueryBuilder(accountUser: $accountUser, notificationFilter: $notificationFilter);

        if ($limit !== null) {
            $queryBuilder->setMaxResults($limit);
        }

        $queryBuilder->addOrderBy(sort: 'notification.createdAt', order:'DESC');

        return $queryBuilder->getQuery()->getResult();
    }

    public function findPaginatedByAccountUser(
        AccountUser $accountUser,
        int $firstResult,
        int $maxResults,
        ?NotificationFilter $notificationFilter = null,
        ?SortingOption $sortingOption = null
    ): Paginator {
        $queryBuilder = $this->createFindByAccountUserQueryBuilder(
            accountUser: $accountUser,
            notificationFilter: $notificationFilter,
            sortingOption: $sortingOption
        );

        $queryBuilder
            ->setFirstResult($firstResult)
            ->setMaxResults($maxResults);

        $queryBuilder->addOrderBy(sort: 'notification.createdAt', order:'DESC');

        return new Paginator(query: $queryBuilder->getQuery(), fetchJoinCollection: true);
    }

    /**
     * @param int[] $ids
     * @return Notification[]
     */
    public function findByIds(AccountUser $accountUser, array $ids, ?NotificationFilter $notificationFilter = null): array
    {
        $queryBuilder = $this->createFindByAccountUserQueryBuilder(accountUser: $accountUser, notificationFilter: $notificationFilter);

        $queryBuilder
            ->andWhere('notification.id IN (:ids)')
            ->setParameter(key: 'ids', value: $ids, type: Connection::PARAM_INT_ARRAY);

        $queryBuilder->addOrderBy(sort: 'notification.createdAt', order:'DESC');

        return $queryBuilder->getQuery()->getResult();
    }

    public function findOneById(AccountUser $accountUser, int $id, ?NotificationFilter $notificationFilter = null): ?Notification
    {
        $queryBuilder = $this->createFindByAccountUserQueryBuilder(accountUser: $accountUser, notificationFilter: $notificationFilter);

        $queryBuilder
            ->andWhere('notification.id = :id')
            ->setParameter(key: 'id', value: $id);

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }


    public function findOneByNotificationTypeAndObject(
        AccountUser $accountUser,
        NotificationType $notificationType,
        BuildingUnit|Occurrence|LookingForPropertyRequest $object
    ): ?Notification {
        $queryBuilder = $this->createFindByAccountUserQueryBuilder(accountUser: $accountUser);

        $queryBuilder
            ->andWhere('notification.notificationType = :notificationType')
            ->setParameter(key: 'notificationType', value: $notificationType);

        if ($object instanceof BuildingUnit) {
            $queryBuilder
                ->andWhere('notification.buildingUnit = :buildingUnit')
                ->setParameter(key: 'buildingUnit', value: $object);
        }

        if ($object instanceof Occurrence) {
            $queryBuilder
                ->andWhere('notification.occurrence = :occurrence')
                ->setParameter(key: 'occurrence', value: $object);
        }

        if ($object instanceof LookingForPropertyRequest) {
            $queryBuilder
                ->andWhere('notification.lookingForPropertyRequest = :lookingForPropertyRequest')
                ->setParameter(key: 'lookingForPropertyRequest', value: $object);
        }

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }

    public function countByAccountUser(AccountUser $accountUser, ?NotificationFilter $notificationFilter = null): int
    {
        $queryBuilder = $this->createFindByAccountUserQueryBuilder(accountUser: $accountUser, notificationFilter: $notificationFilter);

        $queryBuilder->select(select: 'COUNT(DISTINCT notification.id)');

        return $queryBuilder->getQuery()->getSingleScalarResult();
    }

    private function createFindByAccountUserQueryBuilder(
        AccountUser $accountUser,
        ?NotificationFilter $notificationFilter = null,
        ?SortingOption $sortingOption = null
    ): QueryBuilder {
        $queryBuilder = $this->createQueryBuilder(alias: 'notification');

        $queryBuilder
            ->addSelect(select: 'buildingUnit')
            ->addSelect(select: 'occurrence')
            ->addSelect(select: 'lookingForPropertyRequest')
            ->leftJoin(
                join: 'notification.buildingUnit',
                alias: 'buildingUnit',
                conditionType: Join::WITH,
                condition: 'notification.notificationType IN (:buildingUnitNotificationTypes) AND buildingUnit.deleted = false'
            )
            ->leftJoin(
                join: 'notification.occurrence',
                alias: 'occurrence',
                conditionType: Join::WITH,
                condition: 'notification.notificationType IN (:occurrenceNotificationTypes) AND occurrence.deleted = false'
            )
            ->leftJoin(
                join: 'notification.lookingForPropertyRequest',
                alias: 'lookingForPropertyRequest',
                conditionType: Join::WITH,
                condition: 'notification.notificationType IN (:lookingForPropertyRequestNotificationTypes) AND lookingForPropertyRequest.deleted = false'
            );

        $buildingUnitNotificationTypes = [
            NotificationType::PROPERTY_BECAME_EMPTY,
            NotificationType::PROPERTY_USAGE_CHANGE,
            NotificationType::PROPERTY_NO_MATCHING_EXECUTED,
            NotificationType::PROPERTY_RENTAL_AGREEMENT_EXPIRED ,
            NotificationType::PROPERTY_NO_MATCH_FOUND,
            NotificationType::PROPERTY_NOT_UPDATED,
        ];

        $occurrenceNotificationTypes = [
            NotificationType::OCCURRENCE_RESUBMISSION_DUE,
        ];

        $lookingForPropertyRequestNotificationTypes = [
            NotificationType::NO_MATCH_FOUND_FOR_LOOKING_FOR_PROPERTY_REQUEST,
        ];

        $queryBuilder
            ->where(predicates: 'notification.accountUser = :accountUser')
            ->andWhere(
                $queryBuilder->expr()->orX()
                    ->add('(notification.notificationType IN (:buildingUnitNotificationTypes) AND buildingUnit IS NOT NULL)')
                    ->add('(notification.notificationType IN (:occurrenceNotificationTypes) AND occurrence IS NOT NULL)')
                    ->add('(notification.notificationType IN (:lookingForPropertyRequestNotificationTypes) AND lookingForPropertyRequest IS NOT NULL)')
            )
            ->setParameter(key: 'accountUser', value: $accountUser)
            ->setParameter(key: 'buildingUnitNotificationTypes', value: $buildingUnitNotificationTypes)
            ->setParameter(key: 'occurrenceNotificationTypes', value: $occurrenceNotificationTypes)
            ->setParameter(key: 'lookingForPropertyRequestNotificationTypes', value: $lookingForPropertyRequestNotificationTypes);

        if ($notificationFilter !== null) {
            if (empty($notificationFilter->getNotificationTypes()) === false) {
                $queryBuilder
                    ->andWhere('notification.notificationType IN (:notificationTypes)')
                    ->setParameter(key: 'notificationTypes', value: $notificationFilter->getNotificationTypes());
            }

            if (empty($notificationFilter->getReadStatus()) === false) {
                $queryBuilder
                    ->andWhere('notification.beenRead = :beenRead')
                    ->setParameter(key: 'beenRead', value: $notificationFilter->getReadStatus());
            }
        }

        if ($sortingOption !== null) {
            self::applyNotificationSortingToQueryBuilder(sortingOption: $sortingOption, queryBuilder: $queryBuilder);
        }

        return $queryBuilder;
    }

    private static function applyNotificationSortingToQueryBuilder(SortingOption $sortingOption, QueryBuilder $queryBuilder): void
    {
        if ($sortingOption->getSortingBy() === 'datum') {
            $queryBuilder->orderBy(sort: 'notification.createdAt', order: $sortingOption->getSortingDirection()->getOrderByDirection());
        }

        if ($sortingOption->getSortingBy() === 'bezug') {
            $orderByExpression = $queryBuilder->expr()->andX();

            $orderByExpression->add(<<<EOT
                CASE
                    WHEN notification.notificationType = 3 THEN 'Kontakt'
                    WHEN notification.notificationType = 2 OR notification.notificationType = 5  THEN 'Matching'
                    WHEN notification.notificationType = 5 THEN 'Gesuch'
                    ELSE ''
                END
                EOT
            );

            $queryBuilder->orderBy(sort: $orderByExpression, order: $sortingOption->getSortingDirection()->getOrderByDirection());
        }
    }
}
