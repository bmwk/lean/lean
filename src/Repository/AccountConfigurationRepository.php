<?php

declare(strict_types=1);

namespace App\Repository;

use App\Domain\Entity\Account;
use App\Domain\Entity\AccountConfiguration;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AccountConfiguration|null find($id, $lockMode = null, $lockVersion = null)
 * @method AccountConfiguration|null findOneBy(array $criteria, array $orderBy = null)
 * @method AccountConfiguration[]    findAll()
 * @method AccountConfiguration[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AccountConfigurationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, entityClass: AccountConfiguration::class);
    }

    public function findByAccount(Account $account): ?AccountConfiguration
    {
        $queryBuilder = $this->createFindByAccountQueryBuilder(account: $account);

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }

    private function createFindByAccountQueryBuilder(Account $account): QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder(alias: 'accountConfiguration');

        $queryBuilder
            ->innerJoin(
                join: 'accountConfiguration.account',
                alias: 'account',
                conditionType: JOIN::WITH,
                condition: 'account = :account AND account.deleted = false AND account.enabled = true'
            )
            ->where(predicates: 'accountConfiguration.account = account')
            ->setParameter(key: 'account', value: $account);

        return $queryBuilder;
    }
}
