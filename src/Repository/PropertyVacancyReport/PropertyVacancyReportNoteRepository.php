<?php

declare(strict_types=1);

namespace App\Repository\PropertyVacancyReport;

use App\Domain\Entity\PropertyVacancyReport\PropertyVacancyReportNote;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PropertyVacancyReportNote|null find($id, $lockMode = null, $lockVersion = null)
 * @method PropertyVacancyReportNote|null findOneBy(array $criteria, array $orderBy = null)
 * @method PropertyVacancyReportNote[]    findAll()
 * @method PropertyVacancyReportNote[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PropertyVacancyReportNoteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PropertyVacancyReportNote::class);
    }
}
