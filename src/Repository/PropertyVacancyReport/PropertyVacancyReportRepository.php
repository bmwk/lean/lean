<?php

declare(strict_types=1);

namespace App\Repository\PropertyVacancyReport;

use App\Domain\Entity\Account;
use App\Domain\Entity\PropertyVacancyReport\PropertyVacancyReport;
use App\Domain\Entity\PropertyVacancyReport\PropertyVacancyReportFilter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PropertyVacancyReport|null find($id, $lockMode = null, $lockVersion = null)
 * @method PropertyVacancyReport|null findOneBy(array $criteria, array $orderBy = null)
 * @method PropertyVacancyReport[]    findAll()
 * @method PropertyVacancyReport[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PropertyVacancyReportRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PropertyVacancyReport::class);
    }

    /**
     * @return PropertyVacancyReport[]
     */
    public function findByAccount(
        Account $account,
        ?bool $processed = null,
        ?bool $emailConfirmed = null,
        ?PropertyVacancyReportFilter $propertyVacancyReportFilter = null
    ): array {
        $queryBuilder = $this->createFindByAccountQueryBuilder(
            account: $account,
            processed: $processed,
            emailConfirmed: $emailConfirmed,
            propertyVacancyReportFilter: $propertyVacancyReportFilter
        );

        return $queryBuilder->getQuery()->getResult();
    }

    public function countByCreatedAtRange(
        Account $account,
        \DateTime $dateTimeStart,
        \DateTime $dateTimeEnd,
        ?bool $processed = null,
        ?bool $emailConfirmed = null,
        ?PropertyVacancyReportFilter $propertyVacancyReportFilter = null
    ): int {
        $queryBuilder = $this->createFindByAccountQueryBuilder(
            account: $account,
            processed: $processed,
            emailConfirmed: $emailConfirmed,
            propertyVacancyReportFilter: $propertyVacancyReportFilter
        );

        $queryBuilder
            ->select(select: 'COUNT(DISTINCT propertyVacancyReport.id)')
            ->andWhere('propertyVacancyReport.createdAt BETWEEN :dateTimeStart AND :dateTimeEnd')
            ->setParameter(key: 'dateTimeStart', value: $dateTimeStart)
            ->setParameter(key: 'dateTimeEnd', value: $dateTimeEnd);

        return $queryBuilder->getQuery()->getSingleScalarResult();
    }

    private function createFindByAccountQueryBuilder(
        Account $account,
        ?bool $processed = null,
        ?bool $emailConfirmed = null,
        ?PropertyVacancyReportFilter $propertyVacancyReportFilter = null
    ): QueryBuilder {
        $queryBuilder = $this->createQueryBuilder(alias: 'propertyVacancyReport');

        $queryBuilder
            ->innerJoin(
                join: 'propertyVacancyReport.account',
                alias: 'account',
                conditionType: Join::WITH,
                condition: 'account = :account AND account.deleted = false AND account.enabled = true'
            )
            ->where(predicates: 'propertyVacancyReport.account = :account')
            ->andWhere('propertyVacancyReport.deleted = false')
            ->setParameter(key: 'account', value: $account);

        if ($processed !== null) {
            $queryBuilder
                ->andWhere('propertyVacancyReport.processed = :processed')
                ->setParameter(key: 'processed', value: $processed);
        }

        if ($emailConfirmed !== null) {
            $queryBuilder
                ->andWhere('propertyVacancyReport.emailConfirmed = :emailConfirmed')
                ->setParameter(key: 'emailConfirmed', value: $emailConfirmed);
        }

        if ($propertyVacancyReportFilter !== null) {
            if (empty($propertyVacancyReportFilter->getIndustryClassifications()) === false) {
                $queryBuilder
                    ->andWhere('propertyVacancyReport.industryClassification IN (:industryClassifications)')
                    ->setParameter(key: 'industryClassifications', value: $propertyVacancyReportFilter->getIndustryClassifications());
            }
        }

        return $queryBuilder;
    }
}
