<?php

declare(strict_types=1);

namespace App\Repository;

use App\Domain\Entity\Account;
use App\Domain\Entity\DocumentTemplate;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DocumentTemplate|null find($id, $lockMode = null, $lockVersion = null)
 * @method DocumentTemplate|null findOneBy(array $criteria, array $orderBy = null)
 * @method DocumentTemplate[]    findAll()
 * @method DocumentTemplate[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DocumentTemplateRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DocumentTemplate::class);
    }

    public function findOneById(Account $account, int $id): ?DocumentTemplate
    {
        $queryBuilder = $this->createFindByAccountQueryBuilder(account: $account);

        $queryBuilder
            ->andWhere('documentTemplate.id = :id')
            ->setParameter(key: 'id', value: $id);

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }

    /**
     * @return DocumentTemplate[]
     */
    public function findByAccount(Account $account): array
    {
        $queryBuilder = $this->createFindByAccountQueryBuilder(account: $account);

        return $queryBuilder->getQuery()->getResult();
    }

    private function createFindByAccountQueryBuilder(Account $account): QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder(alias: 'documentTemplate');

        $queryBuilder
            ->innerJoin(
                join: 'documentTemplate.account',
                alias: 'account',
                conditionType: Join::WITH,
                condition: 'account = :account AND account.deleted = false AND account.enabled = true'
            )
            ->where(predicates: 'documentTemplate.account = account')
            ->setParameter(key: 'account', value: $account);

        return $queryBuilder;
    }
}
