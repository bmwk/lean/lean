<?php

declare(strict_types=1);

namespace App\Repository;

use App\Domain\Entity\GeolocationPolygon;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method GeolocationPolygon|null find($id, $lockMode = null, $lockVersion = null)
 * @method GeolocationPolygon|null findOneBy(array $criteria, array $orderBy = null)
 * @method GeolocationPolygon[]    findAll()
 * @method GeolocationPolygon[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GeolocationPolygonRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, GeolocationPolygon::class);
    }
}
