<?php

declare(strict_types=1);

namespace App\Repository\SettlementConcept;

use App\Domain\Entity\Account;
use App\Domain\Entity\SettlementConcept\SettlementConceptMatchingResult;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SettlementConceptMatchingResult|null find($id, $lockMode = null, $lockVersion = null)
 * @method SettlementConceptMatchingResult|null findOneBy(array $criteria, array $orderBy = null)
 * @method SettlementConceptMatchingResult[]    findAll()
 * @method SettlementConceptMatchingResult[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SettlementConceptMatchingResultRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SettlementConceptMatchingResult::class);
    }

    public function countGroupedByBuildingUnitAndEliminated(Account $account): array
    {
        $queryBuilder = $this->createQueryBuilder(alias: 'settlementConceptMatchingResult');

        $queryBuilder
            ->select(select: 'buildingUnit.id AS buildingUnitId')
            ->addSelect(select: 'settlementConceptMatchingResult.eliminated')
            ->addSelect(select: 'COUNT(DISTINCT settlementConceptMatchingResult.id) AS matches')
            ->innerJoin(join: 'settlementConceptMatchingResult.buildingUnit', alias: 'buildingUnit')
            ->andWhere('settlementConceptMatchingResult.account = :account')
            ->setParameter(key: 'account', value: $account)
            ->groupBy(groupBy: 'buildingUnit, settlementConceptMatchingResult.eliminated');

        return $queryBuilder->getQuery()->getResult();
    }
}
