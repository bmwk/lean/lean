<?php

declare(strict_types=1);

namespace App\Repository\SettlementConcept;

use App\Domain\Entity\SettlementConcept\SettlementConceptEliminationCriteria;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SettlementConceptEliminationCriteria|null find($id, $lockMode = null, $lockVersion = null)
 * @method SettlementConceptEliminationCriteria|null findOneBy(array $criteria, array $orderBy = null)
 * @method SettlementConceptEliminationCriteria[]    findAll()
 * @method SettlementConceptEliminationCriteria[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SettlementConceptEliminationCriteriaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SettlementConceptEliminationCriteria::class);
    }
}
