<?php

declare(strict_types=1);

namespace App\Repository\SettlementConcept;

use App\Domain\Entity\Account;
use App\Domain\Entity\SettlementConcept\SettlementConceptAccountUser;
use App\Domain\Entity\SettlementConcept\SettlementConceptAccountUserFilter;
use App\Domain\Entity\SettlementConcept\SettlementConceptAccountUserSearch;
use App\Domain\Entity\SortingOption\SortingOption;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SettlementConceptAccountUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method SettlementConceptAccountUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method SettlementConceptAccountUser[] findAll()
 * @method SettlementConceptAccountUser[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SettlementConceptAccountUserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SettlementConceptAccountUser::class);
    }

    public function findPaginatedByAccount(
        Account $account,
        int $firstResult,
        int $maxResults,
        ?SettlementConceptAccountUserFilter $settlementConceptAccountUserFilter = null,
        ?SettlementConceptAccountUserSearch $settlementConceptAccountUserSearch = null,
        ?SortingOption $sortingOption = null
    ): Paginator {
        $queryBuilder = $this->createFindByAccountQueryBuilder(
            account: $account,
            settlementConceptAccountUserFilter: $settlementConceptAccountUserFilter,
            settlementConceptAccountUserSearch: $settlementConceptAccountUserSearch,
            sortingOption: $sortingOption
        );

        $queryBuilder
            ->setFirstResult($firstResult)
            ->setMaxResults($maxResults);

        return new Paginator(query: $queryBuilder->getQuery(), fetchJoinCollection: true);
    }

    /**
     * @return SettlementConceptAccountUser[]
     */
    public function findToBeDeletedSettlementConceptAccountUserFromAllAccounts(?int $timeTillHardDelete): array
    {
        $queryBuilder = $this->createQueryBuilder(alias: 'settlementConceptAccountUser');

        $queryBuilder->where(predicates: 'settlementConceptAccountUser.deleted = true');

        if (empty($timeTillHardDelete) === false) {
            $deleteFromDateTime = new \DateTime(strtolower(string: '-' . $timeTillHardDelete . ' days'));

            $queryBuilder
                ->andWhere('settlementConceptAccountUser.deletedAt <= :deleteFromDateTime')
                ->setParameter(key: 'deleteFromDateTime', value: $deleteFromDateTime);
        }

        return $queryBuilder->getQuery()->getResult();
    }

    private function createFindByAccountQueryBuilder(
        Account $account,
        ?SettlementConceptAccountUserFilter $settlementConceptAccountUserFilter = null,
        ?SettlementConceptAccountUserSearch $settlementConceptAccountUserSearch = null,
        ?SortingOption $sortingOption = null
    ): QueryBuilder {
        $queryBuilder = $this->createQueryBuilder(alias: 'settlementConceptAccountUser');

        $queryBuilder
            ->innerJoin(
                join: 'settlementConceptAccountUser.account',
                alias: 'account',
                conditionType: Join::WITH,
                condition: 'account = :account AND account.deleted = false AND account.enabled = true'
            )
            ->where(predicates: 'settlementConceptAccountUser.account = account')
            ->andWhere('settlementConceptAccountUser.deleted = false')
            ->setParameter(key: 'account', value: $account);

        if ($settlementConceptAccountUserFilter !== null && empty($settlementConceptAccountUserFilter->getEnabledStates()) === false) {
            $queryBuilder
                ->andWhere('settlementConceptAccountUser.enabled IN (:enabledStates)')
                ->setParameter(key: 'enabledStates', value:  $settlementConceptAccountUserFilter->getEnabledStates());
        }

        if ($settlementConceptAccountUserSearch !== null && empty($settlementConceptAccountUserSearch->getText()) === false) {
            $expression = $queryBuilder->expr()->orX();

            $expression
                ->add('settlementConceptAccountUser.email LIKE :searchText')
                ->add('settlementConceptAccountUser.fullName LIKE :searchText');

            $queryBuilder
                ->andWhere($expression)
                ->setParameter(key: 'searchText', value: '%' . $settlementConceptAccountUserSearch->getText() . '%');
        }

        if ($sortingOption !== null) {
            self::applySettlementConceptAccountUserSortingToQueryBuilder(sortingOption: $sortingOption, queryBuilder: $queryBuilder);
        }

        return $queryBuilder;
    }

    private static function applySettlementConceptAccountUserSortingToQueryBuilder(SortingOption $sortingOption, QueryBuilder $queryBuilder): void
    {
        if ($sortingOption->getSortingBy() === 'name') {
            $queryBuilder->orderBy(sort: 'settlementConceptAccountUser.fullName', order: $sortingOption->getSortingDirection()->getOrderByDirection());
        }

        if ($sortingOption->getSortingBy() === 'email') {
            $queryBuilder->orderBy(sort: 'settlementConceptAccountUser.email', order: $sortingOption->getSortingDirection()->getOrderByDirection());
        }

    }
}
