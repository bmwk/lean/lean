<?php

declare(strict_types=1);

namespace App\Repository\SettlementConcept;

use App\Domain\Entity\SettlementConcept\SettlementConceptAuthenticationTokenGenerationRequest;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SettlementConceptAuthenticationTokenGenerationRequest|null find($id, $lockMode = null, $lockVersion = null)
 * @method SettlementConceptAuthenticationTokenGenerationRequest|null findOneBy(array $criteria, array $orderBy = null)
 * @method SettlementConceptAuthenticationTokenGenerationRequest[] findAll()
 * @method SettlementConceptAuthenticationTokenGenerationRequest[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SettlementConceptAuthenticationTokenGenerationRequestRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SettlementConceptAuthenticationTokenGenerationRequest::class);
    }
}
