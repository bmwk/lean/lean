<?php

declare(strict_types=1);

namespace App\Repository\SettlementConcept;

use App\Domain\Entity\Account;
use App\Domain\Entity\SettlementConcept\SettlementConcept;
use App\Domain\Entity\SettlementConcept\SettlementConceptAccountUser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SettlementConcept|null find($id, $lockMode = null, $lockVersion = null)
 * @method SettlementConcept|null findOneBy(array $criteria, array $orderBy = null)
 * @method SettlementConcept[]    findAll()
 * @method SettlementConcept[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SettlementConceptRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SettlementConcept::class);
    }

    /**
     * @return SettlementConcept[]
     */
    public function findByAccount(
        Account $account,
        bool $fromDeletedSettlementConceptAccountUser,
        bool $fromDisabledSettlementConceptAccountUser,
        ?SettlementConceptAccountUser $settlementConceptAccountUser = null
    ): array {
        $queryBuilder = $this->createFindByAccountQueryBuilder(
            account: $account,
            fromDeletedSettlementConceptAccountUser: $fromDeletedSettlementConceptAccountUser,
            fromDisabledSettlementConceptAccountUser: $fromDisabledSettlementConceptAccountUser,
            settlementConceptAccountUser: $settlementConceptAccountUser
        );

        return $queryBuilder->getQuery()->getResult();
    }

    public function findOneById(
        Account $account,
        int $id,
        bool $fromDeletedSettlementConceptAccountUser,
        bool $fromDisabledSettlementConceptAccountUser,
        ?SettlementConceptAccountUser $settlementConceptAccountUser = null
    ): ?SettlementConcept {
        $queryBuilder = $this->createFindByAccountQueryBuilder(
            account: $account,
            fromDeletedSettlementConceptAccountUser: $fromDeletedSettlementConceptAccountUser,
            fromDisabledSettlementConceptAccountUser: $fromDisabledSettlementConceptAccountUser,
            settlementConceptAccountUser: $settlementConceptAccountUser
        );

        $queryBuilder
            ->andWhere('settlementConcept.id = :id')
            ->setParameter(key: 'id', value: $id);

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }

    private function createFindByAccountQueryBuilder(
        Account $account,
        bool $fromDeletedSettlementConceptAccountUser,
        bool $fromDisabledSettlementConceptAccountUser,
        ?SettlementConceptAccountUser $settlementConceptAccountUser = null
    ): QueryBuilder {
        $queryBuilder = $this->createQueryBuilder(alias: 'settlementConcept');

        $queryBuilder
            ->innerJoin(
                join: 'settlementConcept.account',
                alias: 'account',
                conditionType: Join::WITH,
                condition: 'account = :account AND account.deleted = false AND account.enabled = true'
            )
            ->innerJoin(join: 'settlementConcept.settlementConceptAccountUser', alias: 'settlementConceptAccountUser')
            ->where(predicates: 'settlementConcept.account = account')
            ->setParameter(key: 'account', value: $account);

            if ($fromDeletedSettlementConceptAccountUser === false) {
                $queryBuilder->andWhere('settlementConceptAccountUser.deleted = false');
            }

            if ($fromDisabledSettlementConceptAccountUser === false) {
                $queryBuilder->andWhere('settlementConceptAccountUser.enabled = true');
            }

            if ($settlementConceptAccountUser !== null) {
                $queryBuilder
                    ->andWhere('settlementConceptAccountUser = :settlementConceptAccountUser')
                    ->setParameter(key: 'settlementConceptAccountUser', value: $settlementConceptAccountUser);
            }

        return $queryBuilder;
    }
}
