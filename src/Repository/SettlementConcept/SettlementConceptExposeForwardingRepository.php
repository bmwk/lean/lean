<?php

declare(strict_types=1);

namespace App\Repository\SettlementConcept;

use App\Domain\Entity\Account;
use App\Domain\Entity\ForwardingResponse;
use App\Domain\Entity\Property\BuildingUnit;
use App\Domain\Entity\SettlementConcept\SettlementConcept;
use App\Domain\Entity\SettlementConcept\SettlementConceptAccountUser;
use App\Domain\Entity\SettlementConcept\SettlementConceptExposeForwarding;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SettlementConceptExposeForwarding|null find($id, $lockMode = null, $lockVersion = null)
 * @method SettlementConceptExposeForwarding|null findOneBy(array $criteria, array $orderBy = null)
 * @method SettlementConceptExposeForwarding[]    findAll()
 * @method SettlementConceptExposeForwarding[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SettlementConceptExposeForwardingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SettlementConceptExposeForwarding::class);
    }

    public function findOneByIdAndSettlementConceptAccountUser(
        int $id,
        SettlementConceptAccountUser $settlementConceptAccountUser
    ): ?SettlementConceptExposeForwarding {
        $queryBuilder = $this->createQueryBuilder(alias: 'settlementConceptExposeForwarding');

        $queryBuilder
            ->innerJoin(join: 'settlementConceptExposeForwarding.settlementConcept', alias: 'settlementConcept')
            ->innerJoin(
                join: 'settlementConceptExposeForwarding.buildingUnit',
                alias: 'buildingUnit',
                conditionType: Join::WITH,
                condition: 'buildingUnit.deleted = false'
            )
            ->innerJoin(join: 'settlementConcept.settlementConceptAccountUser', alias: 'settlementConceptAccountUser')
            ->where(predicates: 'settlementConceptExposeForwarding.id = :id')
            ->andWhere('settlementConceptAccountUser = :settlementConceptAccountUser')
            ->setParameter(key: 'id', value: $id)
            ->setParameter(key: 'settlementConceptAccountUser', value: $settlementConceptAccountUser);

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }

    public function findBySettlementConceptAndBuildingUnit(
        Account $account,
        SettlementConcept $settlementConcept,
        BuildingUnit $buildingUnit
    ): ?SettlementConceptExposeForwarding {
        $queryBuilder = $this->createFindByAccountQueryBuilder(account: $account);

        $queryBuilder
            ->where(predicates: 'settlementConcept = :settlementConcept')
            ->andWhere('buildingUnit = :buildingUnit')
            ->setParameter(key: 'settlementConcept', value: $settlementConcept)
            ->setParameter(key: 'buildingUnit', value: $buildingUnit);

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }

    public function countByAccount(Account $account): int
    {
        $queryBuilder = $this->createFindByAccountQueryBuilder(account: $account);

        $queryBuilder->select(select: 'COUNT(DISTINCT settlementConceptExposeForwarding.id)');

        return $queryBuilder->getQuery()->getSingleScalarResult();
    }

    public function countByAccountAndForwardingResponse(Account $account, ForwardingResponse $forwardingResponse): int
    {
        $queryBuilder = $this->createFindByAccountQueryBuilder(account: $account);

        $queryBuilder
            ->select(select: 'COUNT(DISTINCT settlementConceptExposeForwarding.id)')
            ->andWhere('settlementConceptExposeForwarding.forwardingResponse = :forwardingResponse')
            ->setParameter(key: 'forwardingResponse', value: $forwardingResponse);

        return $queryBuilder->getQuery()->getSingleScalarResult();
    }

    private function createFindByAccountQueryBuilder(Account $account): QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder(alias: 'settlementConceptExposeForwarding');

        $queryBuilder
            ->innerJoin(join: 'settlementConceptExposeForwarding.settlementConcept', alias: 'settlementConcept')
            ->innerJoin(
                join: 'settlementConcept.account',
                alias: 'settlementConceptAccount',
                conditionType: Join::WITH,
                condition: '(settlementConceptAccount.parentAccount = :account OR settlementConceptAccount = :account) AND settlementConceptAccount.deleted = false AND settlementConceptAccount.enabled = true'
            )
            ->innerJoin(
                join: 'settlementConceptExposeForwarding.buildingUnit',
                alias: 'buildingUnit',
                conditionType: Join::WITH,
                condition: 'buildingUnit.deleted = false'
            )
            ->innerJoin(
                join: 'buildingUnit.account',
                alias: 'buildingUnitAccount',
                conditionType: Join::WITH,
                condition: '(buildingUnitAccount.parentAccount = :account OR buildingUnitAccount = :account) AND buildingUnitAccount.deleted = false AND buildingUnitAccount.enabled = true'
            )
            ->setParameter(key: 'account', value: $account);

        return $queryBuilder;
    }
}
