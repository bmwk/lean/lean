<?php

declare(strict_types=1);

namespace App\Repository\SettlementConcept;

use App\Domain\Entity\SettlementConcept\SettlementConceptMatchingTask;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SettlementConceptMatchingTask|null find($id, $lockMode = null, $lockVersion = null)
 * @method SettlementConceptMatchingTask|null findOneBy(array $criteria, array $orderBy = null)
 * @method SettlementConceptMatchingTask[]    findAll()
 * @method SettlementConceptMatchingTask[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SettlementConceptMatchingTaskRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SettlementConceptMatchingTask::class);
    }
}
