<?php

declare(strict_types=1);

namespace App\Repository\SettlementConcept;

use App\Domain\Entity\Account;
use App\Domain\Entity\Property\BuildingUnit;
use App\Domain\Entity\SettlementConcept\SettlementConcept;
use App\Domain\Entity\SettlementConcept\SettlementConceptMatchingResponse;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SettlementConceptMatchingResponse|null find($id, $lockMode = null, $lockVersion = null)
 * @method SettlementConceptMatchingResponse|null findOneBy(array $criteria, array $orderBy = null)
 * @method SettlementConceptMatchingResponse[]    findAll()
 * @method SettlementConceptMatchingResponse[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SettlementConceptMatchingResponseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SettlementConceptMatchingResponse::class);
    }

    public function findOneBySettlementConceptAndBuildingUnit(
        Account $account,
        SettlementConcept $settlementConcept,
        BuildingUnit $buildingUnit
    ): ?SettlementConceptMatchingResponse {
        $queryBuilder = $this->createQueryBuilder(alias: 'settlementConceptMatchingResponse');

        $queryBuilder
            ->innerJoin(join: 'settlementConceptMatchingResponse.settlementConcept', alias: 'settlementConcept')
            ->innerJoin(
                join: 'settlementConcept.account',
                alias: 'settlementConceptAccount',
                conditionType: Join::WITH,
                condition: '(settlementConceptAccount.parentAccount = :account OR settlementConceptAccount = :account) AND settlementConceptAccount.deleted = false AND settlementConceptAccount.enabled = true'
            )
            ->innerJoin(
                join: 'settlementConceptMatchingResponse.buildingUnit',
                alias: 'buildingUnit',
                conditionType: Join::WITH,
                condition: 'buildingUnit.deleted = false'
            )
            ->innerJoin(
                join: 'buildingUnit.account',
                alias: 'buildingUnitAccount',
                conditionType: Join::WITH,
                condition: '(buildingUnitAccount.parentAccount = :account OR buildingUnitAccount = :account) AND buildingUnitAccount.deleted = false AND buildingUnitAccount.enabled = true'
            )
            ->where(predicates: 'settlementConcept = :settlementConcept')
            ->andWhere('buildingUnit = :buildingUnit')
            ->setParameter(key: 'account', value: $account)
            ->setParameter(key: 'settlementConcept', value: $settlementConcept)
            ->setParameter(key: 'buildingUnit', value: $buildingUnit);

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }
}
