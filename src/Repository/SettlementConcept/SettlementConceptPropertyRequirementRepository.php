<?php

declare(strict_types=1);

namespace App\Repository\SettlementConcept;

use App\Domain\Entity\SettlementConcept\SettlementConceptPropertyRequirement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SettlementConceptPropertyRequirement|null find($id, $lockMode = null, $lockVersion = null)
 * @method SettlementConceptPropertyRequirement|null findOneBy(array $criteria, array $orderBy = null)
 * @method SettlementConceptPropertyRequirement[]    findAll()
 * @method SettlementConceptPropertyRequirement[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SettlementConceptPropertyRequirementRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SettlementConceptPropertyRequirement::class);
    }
}
