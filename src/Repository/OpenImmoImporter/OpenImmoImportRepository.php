<?php

declare(strict_types=1);

namespace App\Repository\OpenImmoImporter;

use App\Domain\Entity\Account;
use App\Domain\Entity\OpenImmoImporter\ImportStatus;
use App\Domain\Entity\OpenImmoImporter\OpenImmoImport;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OpenImmoImport|null find($id, $lockMode = null, $lockVersion = null)
 * @method OpenImmoImport|null findOneBy(array $criteria, array $orderBy = null)
 * @method OpenImmoImport[]    findAll()
 * @method OpenImmoImport[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OpenImmoImportRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OpenImmoImport::class);
    }

    public function countByAccountAndImportStatus(Account $account, ImportStatus $importStatus): int
    {
        return $this->count(criteria: ['account' => $account, 'importStatus' => $importStatus]);
    }

    /**
     * @return OpenImmoImport[]
     */
    public function findByImportStatus(ImportStatus $importStatus, int $limit): array
    {
        return $this->findBy(criteria: ['importStatus' => $importStatus], limit: $limit);
    }
}
