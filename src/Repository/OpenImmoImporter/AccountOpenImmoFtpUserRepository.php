<?php

declare(strict_types=1);

namespace App\Repository\OpenImmoImporter;

use App\Domain\Entity\Account;
use App\Domain\Entity\OpenImmoImporter\AccountOpenImmoFtpUser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AccountOpenImmoFtpUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method AccountOpenImmoFtpUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method AccountOpenImmoFtpUser[]    findAll()
 * @method AccountOpenImmoFtpUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AccountOpenImmoFtpUserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AccountOpenImmoFtpUser::class);
    }

    public function findOneByAccount(Account $account): ?AccountOpenImmoFtpUser
    {
        return $this->findOneBy(criteria: ['account' => $account]);
    }
}
