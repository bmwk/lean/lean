<?php

declare(strict_types=1);

namespace App\Repository\OpenImmoImporter;

use App\Domain\Entity\OpenImmoImporter\OpenImmoUpload;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OpenImmoUpload|null find($id, $lockMode = null, $lockVersion = null)
 * @method OpenImmoUpload|null findOneBy(array $criteria, array $orderBy = null)
 * @method OpenImmoUpload[]    findAll()
 * @method OpenImmoUpload[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OpenImmoUploadRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OpenImmoUpload::class);
    }
}
