<?php

declare(strict_types=1);

namespace App\Repository;

use App\Domain\Entity\MinMaxScoreCriterion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MinMaxScoreCriterion|null find($id, $lockMode = null, $lockVersion = null)
 * @method MinMaxScoreCriterion|null findOneBy(array $criteria, array $orderBy = null)
 * @method MinMaxScoreCriterion[]    findAll()
 * @method MinMaxScoreCriterion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MinMaxScoreCriterionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MinMaxScoreCriterion::class);
    }
}
