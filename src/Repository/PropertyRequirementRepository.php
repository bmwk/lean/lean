<?php

declare(strict_types=1);

namespace App\Repository;

use App\Domain\Entity\LookingForPropertyRequest\PropertyRequirement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PropertyRequirement|null find($id, $lockMode = null, $lockVersion = null)
 * @method PropertyRequirement|null findOneBy(array $criteria, array $orderBy = null)
 * @method PropertyRequirement[]    findAll()
 * @method PropertyRequirement[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PropertyRequirementRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PropertyRequirement::class);
    }
}
