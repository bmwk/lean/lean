<?php

declare(strict_types=1);

namespace App\Repository;

use App\Domain\Entity\Account;
use App\Domain\Entity\Document;
use App\Domain\Entity\Property\BuildingUnit;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Document|null find($id, $lockMode = null, $lockVersion = null)
 * @method Document|null findOneBy(array $criteria, array $orderBy = null)
 * @method Document[]    findAll()
 * @method Document[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DocumentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Document::class);
    }

    public function findOneByIdAndBuildingUnit(Account $account, bool $withFromSubAccounts, int $id, BuildingUnit $buildingUnit): ?Document
    {
        $queryBuilder = $this->createFindByAccountQueryBuilder(account: $account, withFromSubAccounts: $withFromSubAccounts);

        $queryBuilder
            ->addSelect(select: 'buildingUnit')
            ->innerJoin(
                join: 'document.buildingUnits',
                alias: 'buildingUnit',
                conditionType: Join::WITH,
                condition: 'buildingUnit = :buildingUnit'
            )
            ->andWhere('document.id = :id')
            ->setParameter(key: 'buildingUnit', value: $buildingUnit)
            ->setParameter(key: 'id', value: $id);

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }

    private function createFindByAccountQueryBuilder(Account $account, bool $withFromSubAccounts): QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder(alias: 'document');

        if ($withFromSubAccounts === true) {
            $queryBuilder->innerJoin(
                join: 'document.account',
                alias: 'account',
                conditionType: Join::WITH,
                condition: '(account.parentAccount = :account OR account = :account) AND account.deleted = false AND account.enabled = true'
            );
        } else {
            $queryBuilder->innerJoin(
                join: 'document.account',
                alias: 'account',
                conditionType: Join::WITH,
                condition: 'account = :account AND account.deleted = false AND account.enabled = true'
            );
        }

        $queryBuilder
            ->where(predicates: 'document.account = account')
            ->setParameter(key: 'account', value: $account);

        return $queryBuilder;
    }
}
