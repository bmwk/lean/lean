<?php

declare(strict_types=1);

namespace App\Repository\Person;

use App\Domain\Entity\Account;
use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\Person\Person;
use App\Domain\Entity\Person\PersonFilter;
use App\Domain\Entity\Person\PersonSearch;
use App\Domain\Entity\Property\PropertyContactResponsibility;
use App\Domain\Entity\SortingOption\SortingOption;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Person|null find($id, $lockMode = null, $lockVersion = null)
 * @method Person|null findOneBy(array $criteria, array $orderBy = null)
 * @method Person[]    findAll()
 * @method Person[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PersonRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Person::class);
    }

    /**
     * @return Person[]
     */
    public function findByAccount(
        Account $account,
        ?bool $anonymized = null,
        ?AccountUser $createdByAccountUser = null,
        ?PersonFilter $personFilter = null,
        ?PersonSearch $personSearch = null,
        ?SortingOption $sortingOption = null
    ): array {
        $queryBuilder = $this->createFindByAccountQueryBuilder(
            account: $account,
            anonymized: $anonymized,
            createdByAccountUser: $createdByAccountUser,
            personFilter: $personFilter,
            personSearch: $personSearch,
            sortingOption: $sortingOption
        );

        return $queryBuilder->getQuery()->getResult();
    }

    public function findPaginatedByAccount(
        Account $account,
        int $firstResult,
        int $maxResults,
        ?bool $anonymized = null,
        ?AccountUser $createdByAccountUser = null,
        ?PersonFilter $personFilter = null,
        ?PersonSearch $personSearch = null,
        ?SortingOption $sortingOption = null
    ): Paginator {
        $queryBuilder = $this->createFindByAccountQueryBuilder(
            account: $account,
            anonymized: $anonymized,
            createdByAccountUser: $createdByAccountUser,
            personFilter: $personFilter,
            personSearch: $personSearch,
            sortingOption: $sortingOption
        );

        $queryBuilder
            ->setFirstResult($firstResult)
            ->setMaxResults($maxResults);

        return new Paginator(query: $queryBuilder->getQuery(), fetchJoinCollection: true);
    }

    /**
     * @param int[] $ids
     * @return Person[]
     */
    public function findByIds(
        Account $account,
        array $ids,
        ?bool $anonymized = null,
        ?PersonFilter $personFilter = null,
        ?PersonSearch $personSearch = null,
        ?SortingOption $sortingOption = null
    ): array {
        $queryBuilder = $this->createFindByAccountQueryBuilder(
            account: $account,
            anonymized: $anonymized,
            personFilter: $personFilter,
            personSearch: $personSearch,
            sortingOption: $sortingOption
        );

        $queryBuilder
            ->andWhere('person.id IN (:ids)')
            ->setParameter(key: 'ids', value: $ids, type: Connection::PARAM_INT_ARRAY);

        return $queryBuilder->getQuery()->getResult();
    }

    public function findOneById(Account $account, int $id): ?Person
    {
        $queryBuilder = $this->createFindByAccountQueryBuilder(account: $account);

        $queryBuilder
            ->andWhere('person.id = :id')
            ->setParameter(key: 'id', value: $id);

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }

    public function countByAccount(Account $account, bool $anonymized, ?PersonFilter $personFilter = null): int
    {
        $queryBuilder = $this->createFindByAccountQueryBuilder(account: $account, anonymized: $anonymized, personFilter: $personFilter);

        $queryBuilder->select(select: 'COUNT(DISTINCT person.id)');

        return $queryBuilder->getQuery()->getSingleScalarResult();
    }

    private function createFindByAccountQueryBuilder(
        Account $account,
        ?bool $anonymized = null,
        ?AccountUser $createdByAccountUser = null,
        ?PersonFilter $personFilter = null,
        ?PersonSearch $personSearch = null,
        ?SortingOption $sortingOption = null
    ): QueryBuilder {
        $queryBuilder = $this->createQueryBuilder(alias: 'person');

        $queryBuilder
            ->innerJoin(
                join: 'person.account',
                alias: 'account',
                conditionType: Join::WITH,
                condition: 'account = :account AND account.deleted = false AND account.enabled = true'
            )
            ->leftJoin(join: 'person.propertyOwners', alias: 'propertyOwner')
            ->leftJoin(join: 'person.propertyUsers', alias: 'propertyUser')
            ->leftJoin(join: 'person.propertyContactPersons', alias: 'propertyContactPerson')
            ->leftJoin(join: 'person.lookingForPropertyRequests', alias: 'lookingForPropertyRequest')
            ->where(predicates: 'person.account = account')
            ->andWhere('person.deleted = false')
            ->setParameter(key: 'account', value: $account);

        if ($anonymized !== null) {
            $queryBuilder
                ->andWhere('person.anonymized = :anonymized')
                ->setParameter(key: 'anonymized', value: $anonymized);
        }

        if ($createdByAccountUser !== null) {
            $queryBuilder
                ->andWhere('person.createdByAccountUser = :createdByAccountUser')
                ->setParameter(key: 'createdByAccountUser', value: $createdByAccountUser);
        }

        if ($personFilter !== null) {
            if (empty($personFilter->getPersonTypes()) === false) {
                $queryBuilder
                    ->andWhere('person.personType IN (:personTypes)')
                    ->setParameter(key: 'personTypes', value: $personFilter->getPersonTypes());
            }

            if (empty($personFilter->getUsageTypes()) === false) {
                if (in_array(needle: 0, haystack: $personFilter->getUsageTypes())) {
                    $queryBuilder->andWhere('propertyOwner IS NOT NULL');
                }

                if (in_array(needle: 1, haystack: $personFilter->getUsageTypes())) {
                    $queryBuilder->andWhere('propertyUser IS NOT NULL');
                }

                if (in_array(needle: 2, haystack: $personFilter->getUsageTypes())) {
                    $queryBuilder
                        ->andWhere('propertyContactPerson IS NOT NULL')
                        ->andWhere('propertyContactPerson.propertyContactResponsibility = :propertyContactResponsibility')
                        ->setParameter(key: 'propertyContactResponsibility', value: PropertyContactResponsibility::PROPERTY);
                }

                if (in_array(needle: 3, haystack: $personFilter->getUsageTypes())) {
                    $queryBuilder
                        ->andWhere('propertyContactPerson IS NOT NULL')
                        ->andWhere('propertyContactPerson.propertyContactResponsibility = :propertyContactResponsibility')
                        ->setParameter(key: 'propertyContactResponsibility', value: PropertyContactResponsibility::MARKETING);
                }

                if (in_array(needle: 4, haystack: $personFilter->getUsageTypes())) {
                    $queryBuilder->andWhere('lookingForPropertyRequest IS NOT NULL');
                }
            }
        }

        if ($personSearch !== null && empty($personSearch->getText()) === false) {
            $expression = $queryBuilder->expr()->orX();

            $expression
                ->add('person.name LIKE :searchText')
                ->add('person.firstName LIKE :searchText')
                ->add('person.birthName LIKE :searchText')
                ->add('person.postalCode LIKE :searchText')
                ->add('person.placeName LIKE :searchText')
                ->add('person.streetName LIKE :searchText')
                ->add('person.email LIKE :searchText')
                ->add('person.phoneNumber LIKE :searchText')
                ->add('person.mobilePhoneNumber LIKE :searchText')
                ->add('person.faxNumber LIKE :searchText')
                ->add('person.website LIKE :searchText');

            $queryBuilder
                ->andWhere($expression)
                ->setParameter(key: 'searchText', value: '%' . $personSearch->getText() . '%');
        }

        if ($sortingOption !== null) {
            self::applyPersonSortingToQueryBuilder(sortingOption: $sortingOption, queryBuilder: $queryBuilder);
        }

        return $queryBuilder;
    }

    private static function applyPersonSortingToQueryBuilder(SortingOption $sortingOption, QueryBuilder $queryBuilder): void
    {
        if ($sortingOption->getSortingBy() === 'name') {
            $orderByExpression = $queryBuilder->expr()->andX();

            $orderByExpression->add(<<<EOT
                CASE
                    WHEN person.deleted = 1 THEN 'Gelöschter Kontakt'
                    ELSE person.name
                END
                EOT
            );

            $queryBuilder->orderBy(sort: $orderByExpression, order: $sortingOption->getSortingDirection()->getOrderByDirection());
        }

        if ($sortingOption->getSortingBy() === 'straße') {
            $queryBuilder->orderBy(sort: 'person.streetName', order: $sortingOption->getSortingDirection()->getOrderByDirection());
        }

        if ($sortingOption->getSortingBy() === 'plz') {
            $queryBuilder->orderBy(sort: 'person.postalCode', order: $sortingOption->getSortingDirection()->getOrderByDirection());
        }

        if ($sortingOption->getSortingBy() === 'ort') {
            $queryBuilder->orderBy(sort: 'person.placeName', order: $sortingOption->getSortingDirection()->getOrderByDirection());
        }

        if ($sortingOption->getSortingBy() === 'telefon') {
            $queryBuilder->orderBy(sort: 'person.phoneNumber', order: $sortingOption->getSortingDirection()->getOrderByDirection());
        }
    }
}
