<?php

declare(strict_types=1);

namespace App\Repository\Person;

use App\Domain\Entity\Account;
use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\Person\Occurrence;
use App\Domain\Entity\Person\Person;
use App\Domain\Entity\Property\BuildingUnit;
use App\Domain\Entity\SortingOption\SortingOption;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Occurrence|null find($id, $lockMode = null, $lockVersion = null)
 * @method Occurrence|null findOneBy(array $criteria, array $orderBy = null)
 * @method Occurrence[]    findAll()
 * @method Occurrence[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OccurrenceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Occurrence::class);
    }

    /**
     * @return Occurrence[]
     */
    public function findByAccount(
        Account $account,
        bool $withFromSubAccounts,
        ?AccountUser $performedWithAccountUser = null,
        ?bool $followUp = null,
        ?bool $followUpDone = null,
        ?\DateTime $followUpAt = null,
        ?SortingOption $sortingOption = null
    ): array {
        $queryBuilder = $this->createFindByAccountQueryBuilder(
            account: $account,
            withFromSubAccounts: $withFromSubAccounts,
            performedWithAccountUser: $performedWithAccountUser,
            followUp: $followUp,
            followUpDone: $followUpDone,
            followUpAt: $followUpAt,
            sortingOption: $sortingOption
        );

        $queryBuilder->orderBy(sort: 'occurrence.followUpAt', order: 'ASC');

        return $queryBuilder->getQuery()->getResult();
    }

    public function findPaginatedByAccount(
        Account $account,
        bool $withFromSubAccounts,
        int $firstResult,
        int $maxResults,
        ?AccountUser $performedWithAccountUser = null,
        ?bool $followUp = null,
        ?bool $followUpDone = null,
        ?\DateTime $followUpAt = null,
        ?SortingOption $sortingOption = null
    ): Paginator {
        $queryBuilder = $this->createFindByAccountQueryBuilder(
            account: $account,
            withFromSubAccounts: $withFromSubAccounts,
            performedWithAccountUser: $performedWithAccountUser,
            followUp: $followUp,
            followUpDone: $followUpDone,
            followUpAt: $followUpAt,
            sortingOption: $sortingOption
        );

        $queryBuilder
            ->setFirstResult($firstResult)
            ->setMaxResults($maxResults);

        $queryBuilder->addOrderBy(sort: 'occurrence.followUpAt', order: 'ASC');

        return new Paginator(query: $queryBuilder->getQuery(), fetchJoinCollection: true);
    }

    public function findPaginatedByPerson(
        Account $account,
        bool $withFromSubAccounts,
        Person $person,
        int $firstResult,
        int $maxResults,
        ?AccountUser $performedWithAccountUser = null,
        ?bool $followUp = null,
        ?bool $followUpDone = null,
        ?\DateTime $followUpAt = null,
        ?SortingOption $sortingOption = null
    ): Paginator {
        $queryBuilder = $this->createFindByAccountQueryBuilder(
            account: $account,
            withFromSubAccounts: $withFromSubAccounts,
            performedWithAccountUser: $performedWithAccountUser,
            followUp: $followUp,
            followUpDone: $followUpDone,
            followUpAt: $followUpAt,
            sortingOption: $sortingOption
        );

        $queryBuilder
            ->andWhere('person = :person')
            ->addOrderBy(sort: 'occurrence.occurrenceAt', order: 'DESC')
            ->setParameter(key: 'person', value: $person)
            ->setFirstResult($firstResult)
            ->setMaxResults($maxResults);

        return new Paginator(query: $queryBuilder->getQuery(), fetchJoinCollection: true);
    }

    /**
     * @return Occurrence[]
     */
    public function findByPropertyOwnersFromBuildingUnit(
        Account $account,
        bool $withFromSubAccounts,
        BuildingUnit $buildingUnit,
        ?AccountUser $performedWithAccountUser = null
    ): array {
        $queryBuilder = $this->createFindByAccountQueryBuilder(
            account: $account,
            withFromSubAccounts: $withFromSubAccounts,
            performedWithAccountUser: $performedWithAccountUser
        );

        $queryBuilder
            ->addSelect(select: 'buildingUnit')
            ->addSelect(select: 'propertyOwner')
            ->addSelect(select: 'person')
            ->innerJoin(join: 'occurrence.propertyOwners', alias: 'propertyOwner')
            ->innerJoin(join: 'propertyOwner.buildingUnits', alias: 'buildingUnit')
            ->andWhere('buildingUnit = :buildingUnit')
            ->andWhere('buildingUnit.deleted = false')
            ->setParameter(key: 'buildingUnit', value: $buildingUnit)
            ->orderBy(sort: 'occurrence.occurrenceAt', order: 'DESC');

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * @return Occurrence[]
     */
    public function findByPropertyUsersFromBuildingUnit(
        Account $account,
        bool $withFromSubAccounts,
        BuildingUnit $buildingUnit,
        ?AccountUser $performedWithAccountUser = null
    ): array {
        $queryBuilder = $this->createFindByAccountQueryBuilder(
            account: $account,
            withFromSubAccounts: $withFromSubAccounts,
            performedWithAccountUser: $performedWithAccountUser
        );

        $queryBuilder
            ->addSelect(select: 'buildingUnit')
            ->addSelect(select: 'propertyUser')
            ->addSelect(select: 'person')
            ->innerJoin(join: 'occurrence.propertyUsers', alias: 'propertyUser')
            ->innerJoin(join: 'propertyUser.buildingUnits', alias: 'buildingUnit')
            ->andWhere('buildingUnit = :buildingUnit')
            ->andWhere('buildingUnit.deleted = false')
            ->setParameter(key: 'buildingUnit', value: $buildingUnit)
            ->orderBy(sort: 'occurrence.occurrenceAt', order: 'DESC');

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * @return Occurrence[]
     */
    public function findByPropertyContactPersonsFromBuildingUnit(
        Account $account,
        bool $withFromSubAccounts,
        BuildingUnit $buildingUnit,
        ?AccountUser $performedWithAccountUser = null
    ): array {
        $queryBuilder = $this->createFindByAccountQueryBuilder(
            account: $account,
            withFromSubAccounts: $withFromSubAccounts,
            performedWithAccountUser: $performedWithAccountUser
        );

        $queryBuilder
            ->addSelect(select: 'buildingUnit')
            ->addSelect(select: 'propertyContactPerson')
            ->addSelect(select: 'person')
            ->innerJoin(join: 'occurrence.propertyContactPersons', alias: 'propertyContactPerson')
            ->innerJoin(join: 'propertyContactPerson.buildingUnits', alias: 'buildingUnit')
            ->andWhere('buildingUnit = :buildingUnit')
            ->andWhere('buildingUnit.deleted = false')
            ->setParameter(key: 'buildingUnit', value: $buildingUnit)
            ->orderBy(sort: 'occurrence.occurrenceAt', order: 'DESC');

        return $queryBuilder->getQuery()->getResult();
    }

    public function findOneById(Account $account, bool $withFromSubAccounts, int $id, ?AccountUser $performedWithAccountUser = null): ?Occurrence
    {
        $queryBuilder = $this->createFindByAccountQueryBuilder(
            account: $account,
            withFromSubAccounts: $withFromSubAccounts,
            performedWithAccountUser: $performedWithAccountUser
        );

        $queryBuilder
            ->andWhere('occurrence.id = :id')
            ->setParameter(key: 'id', value: $id);

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }

    public function findOneByIdAndPerson(
        Account $account, bool $withFromSubAccounts,
        int $id,
        Person $person,
        ?AccountUser $performedWithAccountUser = null
    ): ?Occurrence {
        $queryBuilder = $this->createFindByAccountQueryBuilder(
            account: $account,
            withFromSubAccounts: $withFromSubAccounts,
            performedWithAccountUser: $performedWithAccountUser
        );

        $queryBuilder
            ->andWhere('occurrence.id = :id')
            ->andWhere('person = :person')
            ->setParameter(key: 'id', value: $id)
            ->setParameter(key: 'person', value: $person);

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }

    private function createFindByAccountQueryBuilder(
        Account $account,
        bool $withFromSubAccounts,
        ?AccountUser $performedWithAccountUser = null,
        ?bool $followUp = null,
        ?bool $followUpDone = null,
        ?\DateTime $followUpAt = null,
        ?SortingOption $sortingOption = null
    ): QueryBuilder {
        $queryBuilder = $this->createQueryBuilder(alias: 'occurrence');

        if ($withFromSubAccounts === true ) {
            $queryBuilder->innerJoin(
                join: 'occurrence.account',
                alias: 'account',
                conditionType: Join::WITH,
                condition: '(account.parentAccount = :account OR account = :account) AND account.deleted = false AND account.enabled = true'
            );
        } else {
            $queryBuilder->innerJoin(
                join: 'occurrence.account',
                alias: 'account',
                conditionType: Join::WITH,
                condition: 'account = :account AND account.deleted = false AND account.enabled = true'
            );
        }

        $queryBuilder
            ->addSelect(select: 'person')
            ->addSelect(select: 'performedWithAccountUser')
            ->addSelect(select: 'occurrenceAttachment')
            ->innerJoin(
                join: 'occurrence.person',
                alias: 'person',
                conditionType: Join::WITH,
                condition: 'person.deleted = false'
            )
            ->leftJoin(join: 'occurrence.performedWithAccountUser', alias: 'performedWithAccountUser')
            ->leftJoin(join: 'occurrence.occurrenceAttachments', alias: 'occurrenceAttachment')
            ->where(predicates: 'occurrence.account = account')
            ->andWhere('occurrence.deleted = false')
            ->setParameter(key: 'account', value: $account);

        if ($performedWithAccountUser !== null) {
            $queryBuilder
                ->andWhere('occurrence.performedWithAccountUser = :performedWithAccountUser')
                ->setParameter(key: 'performedWithAccountUser', value: $performedWithAccountUser);
        }

        if ($followUp !== null) {
            $queryBuilder
                ->andWhere('occurrence.followUp = :followUp')
                ->setParameter(key: 'followUp', value: $followUp);
        }

        if ($followUpDone !== null) {
            $queryBuilder
                ->andWhere('occurrence.followUpDone = :followUpDone')
                ->setParameter(key: 'followUpDone', value: $followUpDone);
        }

        if ($followUpAt !== null) {
            $queryBuilder
                ->andWhere('occurrence.followUpAt <= :followUpAt')
                ->setParameter(key: 'followUpAt', value: $followUpAt);
        }

        if ($sortingOption !== null) {
            self::applyOccurrenceSortingToQueryBuilder(sortingOption: $sortingOption, queryBuilder: $queryBuilder);
        }

        return $queryBuilder;
    }

    private static function applyOccurrenceSortingToQueryBuilder(SortingOption $sortingOption, QueryBuilder $queryBuilder): void
    {
        if ($sortingOption->getSortingBy() === 'faellig_am') {
            $queryBuilder->orderBy(sort: 'occurrence.followUpAt', order: $sortingOption->getSortingDirection()->getOrderByDirection());
        }

        if ($sortingOption->getSortingBy() === 'person') {
            $orderByExpression = $queryBuilder->expr()->andX();

            $orderByExpression->add(<<<EOT
                CASE
                    WHEN person.deleted = 1 THEN 'Gelöschter Kontakt'
                    ELSE person.name
                END
                EOT
            );

            $queryBuilder->orderBy(sort: $orderByExpression, order: $sortingOption->getSortingDirection()->getOrderByDirection());
        }

        if ($sortingOption->getSortingBy() === 'vorgang') {
            $orderByExpression = $queryBuilder->expr()->andX();

            $orderByExpression->add(<<<EOT
                CASE
                    WHEN occurrence.occurrenceType = 0 THEN 'Feedback erhalten'
                    WHEN occurrence.occurrenceType = 1 THEN 'Anschreiben versendet'
                    WHEN occurrence.occurrenceType = 2 THEN 'Telefonat geführt'
                    WHEN occurrence.occurrenceType = 3 THEN 'Fragebogen versendet'
                    WHEN occurrence.occurrenceType = 4 THEN 'Aktennotiz'
                    WHEN occurrence.occurrenceType = 6 THEN 'E-Mail-Korrespondenz'
                    ELSE 'Sonstiges'
                END
                EOT
            );

            $queryBuilder->orderBy(sort: $orderByExpression, order: $sortingOption->getSortingDirection()->getOrderByDirection());
        }

        if ($sortingOption->getSortingBy() === 'notiz') {
            $queryBuilder->orderBy(sort: 'occurrence.note', order: $sortingOption->getSortingDirection()->getOrderByDirection());
        }

        if ($sortingOption->getSortingBy() === 'datum') {
            $queryBuilder->orderBy(sort: 'occurrence.occurrenceAt', order: $sortingOption->getSortingDirection()->getOrderByDirection());
        }

        if ($sortingOption->getSortingBy() === 'anhang') {
            $queryBuilder
                ->addSelect(select: 'COUNT(occurrenceAttachment.id) AS HIDDEN amountOfOccurrenceAttachment')
                ->orderBy(sort: 'amountOfOccurrenceAttachment', order: $sortingOption->getSortingDirection()->getOrderByDirection())
                ->groupBy(groupBy: 'occurrence');
        }

        if ($sortingOption->getSortingBy() === 'sachbearbeiter') {
            $orderByExpression = $queryBuilder->expr()->andX();

            $orderByExpression->add(<<<EOT
                CASE
                    WHEN performedWithAccountUser.deleted = 1 THEN 'gelöscht'
                    ELSE performedWithAccountUser.fullName
                END
                EOT
            );

            $queryBuilder->orderBy(sort: $orderByExpression, order: $sortingOption->getSortingDirection()->getOrderByDirection());
        }
    }
}
