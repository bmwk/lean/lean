<?php

declare(strict_types=1);

namespace App\Repository\Person;

use App\Domain\Entity\Person\Contact;
use App\Domain\Entity\Person\Person;
use App\Domain\Entity\SortingOption\SortingOption;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Contact|null find($id, $lockMode = null, $lockVersion = null)
 * @method Contact|null findOneBy(array $criteria, array $orderBy = null)
 * @method Contact[]    findAll()
 * @method Contact[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ContactRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Contact::class);
    }

    public function findPaginatedByPerson(
        Person $person,
        int $firstResult,
        int $maxResults,
        ?SortingOption $sortingOption = null
    ): Paginator {
        $queryBuilder = $this->createFindByPersonQueryBuilder(person: $person, sortingOption: $sortingOption);

        $queryBuilder
            ->setFirstResult($firstResult)
            ->setMaxResults($maxResults);

        return new Paginator(query: $queryBuilder->getQuery(), fetchJoinCollection: true);
    }

    private function createFindByPersonQueryBuilder(
        Person $person,
        ?SortingOption $sortingOption = null
    ): QueryBuilder {
        $queryBuilder = $this->createQueryBuilder(alias: 'contact');

        $queryBuilder
            ->innerJoin(
                join: 'contact.person',
                alias: 'person',
                conditionType: Join::WITH,
                condition: 'person = :person AND person.deleted = false'
            )
            ->andWhere('contact.deleted = false')
            ->setParameter(key: 'person', value: $person);

        if ($sortingOption !== null) {
            self::applyContactSortingToQueryBuilder(sortingOption: $sortingOption, queryBuilder: $queryBuilder);
        }

        return $queryBuilder;
    }

    private static function applyContactSortingToQueryBuilder(SortingOption $sortingOption, QueryBuilder $queryBuilder): void
    {
        if ($sortingOption->getSortingBy() === 'vorname') {
            $queryBuilder->orderBy(sort: 'contact.firstName', order: $sortingOption->getSortingDirection()->getOrderByDirection());
        }

        if ($sortingOption->getSortingBy() === 'name') {
            $queryBuilder->orderBy(sort: 'contact.name', order: $sortingOption->getSortingDirection()->getOrderByDirection());
        }

        if ($sortingOption->getSortingBy() === 'position') {
            $queryBuilder->orderBy(sort: 'contact.position', order: $sortingOption->getSortingDirection()->getOrderByDirection());
        }

        if ($sortingOption->getSortingBy() === 'telefon') {
            $queryBuilder->orderBy(sort: 'contact.phoneNumber', order: $sortingOption->getSortingDirection()->getOrderByDirection());
        }

        if ($sortingOption->getSortingBy() === 'mobil') {
            $queryBuilder->orderBy(sort: 'contact.mobilePhoneNumber', order: $sortingOption->getSortingDirection()->getOrderByDirection());
        }

        if ($sortingOption->getSortingBy() === 'email') {
            $queryBuilder->orderBy(sort: 'contact.email', order: $sortingOption->getSortingDirection()->getOrderByDirection());
        }
    }
}
