<?php

declare(strict_types=1);

namespace App\Repository\Person;

use App\Domain\Entity\Person\OccurrenceAttachment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OccurrenceAttachment|null find($id, $lockMode = null, $lockVersion = null)
 * @method OccurrenceAttachment|null findOneBy(array $criteria, array $orderBy = null)
 * @method OccurrenceAttachment[]    findAll()
 * @method OccurrenceAttachment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OccurrenceAttachmentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OccurrenceAttachment::class);
    }
}
