<?php

declare(strict_types=1);

namespace App\Repository\AccountUser;

use App\Domain\Entity\Account;
use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\AccountUser\AccountUserRole;
use App\Domain\Entity\AccountUser\AccountUserType;
use App\Domain\Entity\AccountUser\AccountUserFilter;
use App\Domain\Entity\AccountUser\AccountUserSearch;
use App\Domain\Entity\SortingOption\SortingOption;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AccountUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method AccountUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method AccountUser[]    findAll()
 * @method AccountUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AccountUserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AccountUser::class);
    }

    /**
     * @param AccountUserType[]|null $accountUserTypes
     * @param AccountUserRole[]|null $accountUserRoles
     * @return AccountUser[]
     */
    public function findByAccount(
        Account $account,
        bool $withFromSubAccounts,
        ?array $accountUserTypes = null,
        ?array $accountUserRoles = null,
        ?AccountUserFilter $accountUserFilter = null,
        ?AccountUserSearch $accountUserSearch = null
    ): array {
        $queryBuilder = $this->createFindByAccountQueryBuilder(
            account: $account,
            withFromSubAccounts: $withFromSubAccounts,
            accountUserTypes: $accountUserTypes,
            accountUserRoles: $accountUserRoles,
            accountUserFilter: $accountUserFilter,
            accountUserSearch: $accountUserSearch
        );

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * @param AccountUserType[]|null  $accountUserTypes
     * @param AccountUserRole[]|null $accountUserRoles
     */
    public function findPaginatedByAccount(
        Account $account,
        bool $withFromSubAccounts,
        int $firstResult,
        int $maxResults,
        ?array $accountUserTypes = null,
        ?array $accountUserRoles = null,
        ?AccountUserFilter $accountUserFilter = null,
        ?AccountUserSearch $accountUserSearch = null,
        ?SortingOption $sortingOption = null
    ): Paginator {
        $queryBuilder = $this->createFindByAccountQueryBuilder(
            account: $account,
            withFromSubAccounts: $withFromSubAccounts,
            accountUserTypes: $accountUserTypes,
            accountUserRoles: $accountUserRoles,
            accountUserFilter: $accountUserFilter,
            accountUserSearch: $accountUserSearch,
            sortingOption: $sortingOption
        );

        $queryBuilder
            ->setFirstResult($firstResult)
            ->setMaxResults($maxResults);

        return new Paginator(query: $queryBuilder->getQuery(), fetchJoinCollection: true);
    }

    public function findByIds(Account $account, bool $withFromSubAccounts, array $ids): array
    {
        $queryBuilder = $this->createFindByAccountQueryBuilder(account: $account, withFromSubAccounts: $withFromSubAccounts);

        $queryBuilder
            ->andWhere('accountUser.id IN (:ids)')
            ->setParameter(key: 'ids', value: $ids, type: Connection::PARAM_INT_ARRAY);

        return $queryBuilder->getQuery()->getResult();
    }

    public function findOneById(Account $account, bool $withFromSubAccounts, int $id): ?AccountUser
    {
        $queryBuilder = $this->createFindByAccountQueryBuilder(account: $account, withFromSubAccounts: $withFromSubAccounts);

        $queryBuilder
            ->andWhere('accountUser.id = :id')
            ->setParameter(key: 'id', value: $id);

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }

    public function findOneByIdFromAllAccounts(int $id): ?AccountUser
    {
        $queryBuilder = $this->createQueryBuilder(alias: 'accountUser');

        $queryBuilder
            ->innerJoin(
                join: 'accountUser.account',
                alias: 'account',
                conditionType: Join::WITH,
                condition: 'account.deleted = false AND account.enabled = true'
            )
            ->where(predicates: 'accountUser.id = :id')
            ->andWhere('accountUser.deleted = false')
            ->andWhere('accountUser.enabled = true')
            ->setParameter(key: 'id', value: $id);

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }

    /**
     * @param AccountUserType[] $accountUserTypes
     * @return AccountUser[]
     */
    public function findToBeDeletedByAccountUserTypesFromAllAccounts(array $accountUserTypes, ?int $timeTillHardDelete = null): array
    {
        $queryBuilder = $this->createQueryBuilder(alias: 'accountUser');

        $queryBuilder
            ->where(predicates: 'accountUser.accountUserType IN (:accountUserTypes)')
            ->andWhere('accountUser.deleted = true')
            ->setParameter(key: 'accountUserTypes', value: $accountUserTypes);

        if (empty($timeTillHardDelete) === false) {
            $deleteFromDateTime = new \DateTime(strtolower(string: '-' . $timeTillHardDelete . ' days'));

            $queryBuilder
                ->andWhere('accountUser.deletedAt <= :deleteFromTimeStamp')
                ->setParameter(key: 'deleteFromTimeStamp', value: $deleteFromDateTime);
        }

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * @param AccountUserType[]|null $accountUserTypes
     */
    public function countByAccount(
        Account $account,
        bool $withFromSubAccounts,
        ?bool $enabled = null,
        ?array $accountUserTypes = null
    ): int {
        $queryBuilder = $this->createFindByAccountQueryBuilder(account: $account, withFromSubAccounts: $withFromSubAccounts, accountUserTypes: $accountUserTypes);

        $queryBuilder->select(select: 'COUNT(DISTINCT accountUser.id)');

        if ($enabled !== null) {
            $queryBuilder
                ->andWhere('accountUser.enabled = :enabled')
                ->setParameter(key: 'enabled', value: $enabled);
        }

        return $queryBuilder->getQuery()->getSingleScalarResult();
    }

    /**
     * @param AccountUserType[]|null $accountUserTypes
     * @param AccountUserRole[]|null $accountUserRoles
     */
    private function createFindByAccountQueryBuilder(
        Account $account,
        bool $withFromSubAccounts,
        ?array $accountUserTypes = null,
        ?array $accountUserRoles = null,
        ?AccountUserFilter $accountUserFilter = null,
        ?AccountUserSearch $accountUserSearch = null,
        ?SortingOption $sortingOption = null
    ): QueryBuilder {
        $queryBuilder = $this->createQueryBuilder(alias: 'accountUser');

        if ($withFromSubAccounts === true ) {
            $queryBuilder->innerJoin(
                join: 'accountUser.account',
                alias: 'account',
                conditionType: Join::WITH,
                condition: '(account.parentAccount = :account OR account = :account) AND account.deleted = false AND account.enabled = true'
            );
        } else {
            $queryBuilder->innerJoin(
                join: 'accountUser.account',
                alias: 'account',
                conditionType: Join::WITH,
                condition: 'account = :account AND account.deleted = false AND account.enabled = true'
            );
        }

        $queryBuilder
            ->where(predicates: 'accountUser.account = account')
            ->andWhere('accountUser.deleted = false')
            ->setParameter(key: 'account', value: $account);

        if (empty($accountUserTypes) === false) {
            $queryBuilder
                ->andWhere('accountUser.accountUserType IN (:accountUserTypes)')
                ->setParameter(key: 'accountUserTypes', value: $accountUserTypes);
        }

        if (empty($accountUserRoles) === false) {
            $roleExpression = $queryBuilder->expr()->orX();

            foreach (array_keys($accountUserRoles) as $key) {
                $roleExpression->add('JSON_CONTAINS(accountUser.roles, ?' . $key . ', \'$\') > 0');
                $queryBuilder->setParameter(key: $key, value: '"' . $accountUserRoles[$key]->value . '"');
            }

            $queryBuilder->andWhere($roleExpression);
        }

        if ($accountUserSearch !== null) {
            $expression = $queryBuilder->expr()->orX();

            $expression
                ->add('accountUser.email LIKE :searchText')
                ->add('accountUser.fullName LIKE :searchText')
                ->add('accountUser.username LIKE :searchText');

            $queryBuilder
                ->andWhere($expression)
                ->setParameter(key: 'searchText', value: '%' . $accountUserSearch->getText() . '%');
        }

        if ($sortingOption !== null) {
            self::applyAccountUserSortingToQueryBuilder(sortingOption: $sortingOption, queryBuilder: $queryBuilder);
        }

        return $queryBuilder;
    }

    private static function applyAccountUserSortingToQueryBuilder(SortingOption $sortingOption, QueryBuilder $queryBuilder): void
    {
        if ($sortingOption->getSortingBy() === 'name') {
            $queryBuilder->orderBy(sort: 'accountUser.fullName', order: $sortingOption->getSortingDirection()->getOrderByDirection());
        }

        if ($sortingOption->getSortingBy() === 'nutzername') {
            $queryBuilder->orderBy(sort: 'accountUser.username', order: $sortingOption->getSortingDirection()->getOrderByDirection());
        }

        if ($sortingOption->getSortingBy() === 'email') {
            $queryBuilder->orderBy(sort: 'accountUser.email', order: $sortingOption->getSortingDirection()->getOrderByDirection());
        }
    }
}
