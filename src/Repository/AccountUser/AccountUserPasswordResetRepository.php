<?php

declare(strict_types=1);

namespace App\Repository\AccountUser;

use App\Domain\Entity\AccountUser\AccountUser;
use App\Domain\Entity\AccountUser\AccountUserPasswordReset;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AccountUserPasswordReset|null find($id, $lockMode = null, $lockVersion = null)
 * @method AccountUserPasswordReset|null findOneBy(array $criteria, array $orderBy = null)
 * @method AccountUserPasswordReset[]    findAll()
 * @method AccountUserPasswordReset[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AccountUserPasswordResetRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AccountUserPasswordReset::class);
    }

    public function findOneByToken(string $token, bool $active): ?AccountUserPasswordReset
    {
        $queryBuilder = $this->createQueryBuilder(alias: 'accountUserPasswordReset');

        $queryBuilder
            ->where(predicates: 'accountUserPasswordReset.passwordToken = :passwordToken')
            ->setParameter(key: 'passwordToken', value: $token);

        if ($active === true) {
            $queryBuilder
                ->andWhere('accountUserPasswordReset.expirationDate > :now')
                ->setParameter(key: 'now', value: new \DateTime());
        }

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }

    public function findOneByAccountUser(AccountUser $accountUser, ?bool $active = null): ?AccountUserPasswordReset
    {
        $queryBuilder = $this->createQueryBuilder(alias: 'accountUserPasswordReset');

        $queryBuilder
            ->where(predicates: 'accountUserPasswordReset.accountUser = :accountUser')
            ->setParameter(key: 'accountUser', value: $accountUser);

        if ($active !== null) {
            if ($active === true) {
                $queryBuilder
                    ->andWhere('accountUserPasswordReset.expirationDate > :now')
                    ->setParameter(key: 'now', value: new \DateTime());
            }

            if ($active === false) {
                $queryBuilder
                    ->andWhere('accountUserPasswordReset.expirationDate < :now')
                    ->setParameter(key: 'now', value: new \DateTime());
            }
        }

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }
}
