<?php

declare(strict_types=1);

namespace App\Repository;

use App\Domain\Entity\Account;
use App\Domain\Entity\BusinessLocationArea;
use App\Domain\Entity\GeolocationPoint;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BusinessLocationArea|null find($id, $lockMode = null, $lockVersion = null)
 * @method BusinessLocationArea|null findOneBy(array $criteria, array $orderBy = null)
 * @method BusinessLocationArea[]    findAll()
 * @method BusinessLocationArea[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BusinessLocationAreaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BusinessLocationArea::class);
    }

    public function findOneByPolygonContainingPoint(Account $account, GeolocationPoint $geolocationPoint): ?BusinessLocationArea
    {
        $point = $geolocationPoint->getPoint();

        $queryBuilder = $this->createQueryBuilder(alias: 'businessLocationArea');

        $queryBuilder
            ->innerJoin(
                join: 'businessLocationArea.account',
                alias: 'account',
                conditionType: Join::WITH,
                condition: 'account = :account AND account.deleted = false AND account.enabled = true'
            )
            ->leftJoin(join: 'businessLocationArea.geolocationPolygon', alias: 'geolocationPolygon')
            ->where(predicates: 'businessLocationArea.account = account')
            ->andWhere('st_contains(geolocationPolygon.polygon, point(:longitude, :latitude)) = true')
            ->setParameter(key: 'account', value: $account)
            ->setParameter(key: 'longitude', value: $point->getLongitude())
            ->setParameter(key: 'latitude', value: $point->getLatitude())
            ->orderBy(sort: 'businessLocationArea.locationCategory')
            ->setMaxResults(maxResults: 1);

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }
}
