<?php

declare(strict_types=1);

namespace App\Repository;

use App\Domain\Entity\Account;
use App\Domain\Entity\Place;
use App\Domain\Entity\WegweiserKommunePlaceMapping;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method WegweiserKommunePlaceMapping|null find($id, $lockMode = null, $lockVersion = null)
 * @method WegweiserKommunePlaceMapping|null findOneBy(array $criteria, array $orderBy = null)
 * @method WegweiserKommunePlaceMapping[]    findAll()
 * @method WegweiserKommunePlaceMapping[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WegweiserKommunePlaceMappingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, WegweiserKommunePlaceMapping::class);
    }

    /**
     * @return WegweiserKommunePlaceMapping[]
     */
    public function findByAccount(Account $account): array
    {
        return $this->findBy(criteria: ['account' => $account]);
    }

    public function findOneByPlace(Account $account, Place $place): ?WegweiserKommunePlaceMapping
    {
        return $this->findOneBy(criteria: [
            'account' => $account,
            'place'   => $place,
        ]);
    }
}
