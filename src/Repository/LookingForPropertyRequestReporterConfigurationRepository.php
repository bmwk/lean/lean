<?php

declare(strict_types=1);

namespace App\Repository;

use App\Domain\Entity\Account;
use App\Domain\Entity\LookingForPropertyRequestReporter\LookingForPropertyRequestReporterConfiguration;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method LookingForPropertyRequestReporterConfiguration|null find($id, $lockMode = null, $lockVersion = null)
 * @method LookingForPropertyRequestReporterConfiguration|null findOneBy(array $criteria, array $orderBy = null)
 * @method LookingForPropertyRequestReporterConfiguration[]    findAll()
 * @method LookingForPropertyRequestReporterConfiguration[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LookingForPropertyRequestReporterConfigurationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LookingForPropertyRequestReporterConfiguration::class);
    }

    public function findOneByAccount(Account $account): ?LookingForPropertyRequestReporterConfiguration
    {
        return $this->findOneBy(criteria: ['account' => $account]);
    }
}
