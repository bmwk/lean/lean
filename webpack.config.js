const Encore = require('@symfony/webpack-encore');

// Manually configure the runtime environment if not already configured yet by the "encore" command.
// It's useful when you use tools that rely on webpack.config.js file.
if (!Encore.isRuntimeEnvironmentConfigured()) {
    Encore.configureRuntimeEnvironment(process.env.NODE_ENV || 'dev');
}

Encore
    .setOutputPath('public/build/')
    .setPublicPath('/build')

    .addEntry('app', './assets/lean/js/app.ts')
    .addEntries({
        'account_user/base_data': './assets/lean/js/account_user/base_data.ts',
        'account_user/delete': './assets/lean/js/account_user/delete.ts'
    })
    .addEntries({
        'company_location_management/hall_of_inspiration/detail': './assets/lean/js/company_location_management/hall_of_inspiration/detail.ts',
        'company_location_management/hall_of_inspiration/overview': './assets/lean/js/company_location_management/hall_of_inspiration/overview.ts'
    })
    .addEntries({
        'company_location_management/looking_for_property_request/expose_forwarding/forwarding_reject': './assets/lean/js/company_location_management/looking_for_property_request/expose_forwarding/forwarding_reject.ts',
        'company_location_management/looking_for_property_request/expose_forwarding/overview': './assets/lean/js/company_location_management/looking_for_property_request/expose_forwarding/overview.ts'
    })
    .addEntries({
        'company_location_management/looking_for_property_request/mass_operation/archive': './assets/lean/js/company_location_management/looking_for_property_request/mass_operation/archive.ts',
        'company_location_management/looking_for_property_request/mass_operation/assign_to_account_user': './assets/lean/js/company_location_management/looking_for_property_request/mass_operation/assign_to_account_user.ts',
        'company_location_management/looking_for_property_request/mass_operation/delete': './assets/lean/js/company_location_management/looking_for_property_request/mass_operation/delete.ts',
        'company_location_management/looking_for_property_request/mass_operation/extend_duration': './assets/lean/js/company_location_management/looking_for_property_request/mass_operation/extend_duration.ts',
        'company_location_management/looking_for_property_request/mass_operation/unarchive': './assets/lean/js/company_location_management/looking_for_property_request/mass_operation/unarchive.ts'
    })
    .addEntries({
        'company_location_management/looking_for_property_request/assign_to_account_user': './assets/lean/js/company_location_management/looking_for_property_request/assign_to_account_user.ts',
        'company_location_management/looking_for_property_request/create': './assets/lean/js/company_location_management/looking_for_property_request/create.ts',
        'company_location_management/looking_for_property_request/edit': './assets/lean/js/company_location_management/looking_for_property_request/edit.ts',
        'company_location_management/looking_for_property_request/delete': './assets/lean/js/company_location_management/looking_for_property_request/delete.ts',
        'company_location_management/looking_for_property_request/overview_active': './assets/lean/js/company_location_management/looking_for_property_request/overview_active.ts',
        'company_location_management/looking_for_property_request/overview_archive': './assets/lean/js/company_location_management/looking_for_property_request/overview_archive.ts',
        'company_location_management/looking_for_property_request/overview_inactive': './assets/lean/js/company_location_management/looking_for_property_request/overview_inactive.ts'
    })
    .addEntries({
        'company_location_management/looking_for_property_request_report/delete': './assets/lean/js/company_location_management/looking_for_property_request_report/delete.ts',
        'company_location_management/looking_for_property_request_report/note_delete': './assets/lean/js/company_location_management/looking_for_property_request_report/note_delete.ts',
        'company_location_management/looking_for_property_request_report/note_edit': './assets/lean/js/company_location_management/looking_for_property_request_report/note_edit.ts',
        'company_location_management/looking_for_property_request_report/processed_overview': './assets/lean/js/company_location_management/looking_for_property_request_report/processed_overview.ts',
        'company_location_management/looking_for_property_request_report/report': './assets/lean/js/company_location_management/looking_for_property_request_report/report.ts',
        'company_location_management/looking_for_property_request_report/report_to_looking_for_property_request': './assets/lean/js/company_location_management/looking_for_property_request_report/report_to_looking_for_property_request.ts',
        'company_location_management/looking_for_property_request_report/unprocessed_overview': './assets/lean/js/company_location_management/looking_for_property_request_report/unprocessed_overview.ts'
    })
    .addEntries({
        'company_location_management/matching/forwarding_reject': './assets/lean/js/company_location_management/matching/forwarding_reject.ts',
        'company_location_management/matching/looking_for_property_request_expose_forwarding': './assets/lean/js/company_location_management/matching/looking_for_property_request_expose_forwarding.ts',
        'company_location_management/matching/looking_for_property_request_matching_result': './assets/lean/js/company_location_management/matching/looking_for_property_request_matching_result.ts',
        'company_location_management/matching/looking_for_property_request_overview': './assets/lean/js/company_location_management/matching/looking_for_property_request_overview.ts',
        'company_location_management/matching/object_detail': './assets/lean/js/company_location_management/matching/object_detail.ts',
        'company_location_management/matching/object_overview': './assets/lean/js/company_location_management/matching/object_overview.ts',
        'company_location_management/matching/settlement_concept_expose_forwarding': './assets/lean/js/company_location_management/matching/settlement_concept_expose_forwarding.ts',
        'company_location_management/matching/settlement_concept_forwarding_reject': './assets/lean/js/company_location_management/matching/settlement_concept_forwarding_reject.ts',
        'company_location_management/matching/settlement_concept_matching_result': './assets/lean/js/company_location_management/matching/settlement_concept_matching_result.ts'
    })
    .addEntries({
        'configuration/basic_settings/city_expose/delete': './assets/lean/js/configuration/basic_settings/city_expose/delete.ts',
        'configuration/basic_settings/city_expose/overview': './assets/lean/js/configuration/basic_settings/city_expose/overview.ts'
    })
    .addEntries({
        'configuration/basic_settings/wms/choose_layers': './assets/lean/js/configuration/basic_settings/wms/choose_layers.ts',
        'configuration/basic_settings/wms/delete': './assets/lean/js/configuration/basic_settings/wms/delete.ts',
        'configuration/basic_settings/wms/edit': './assets/lean/js/configuration/basic_settings/wms/edit.ts',
        'configuration/basic_settings/wms/overview': './assets/lean/js/configuration/basic_settings/wms/overview.ts'
    })
    .addEntries({
        'configuration/basic_settings/account_data': './assets/lean/js/configuration/basic_settings/account_data.ts',
        'configuration/basic_settings/gtc': './assets/lean/js/configuration/basic_settings/gtc.ts',
        'configuration/basic_settings/imprint': './assets/lean/js/configuration/basic_settings/imprint.ts',
        'configuration/basic_settings/interfaces': './assets/lean/js/configuration/basic_settings/interfaces.ts',
        'configuration/basic_settings/layout': './assets/lean/js/configuration/basic_settings/layout.ts',
        'configuration/basic_settings/logo_delete': './assets/lean/js/configuration/basic_settings/logo_delete.ts',
        'configuration/basic_settings/privacy_policy': './assets/lean/js/configuration/basic_settings/privacy_policy.ts',
        'configuration/basic_settings/processes': './assets/lean/js/configuration/basic_settings/processes.ts',
    })
    .addEntries({
        'configuration/business_location_area/map': './assets/lean/js/configuration/business_location_area/map.ts'
    })
    .addEntries({
        'configuration/document_template/delete': './assets/lean/js/configuration/document_template/delete.ts',
        'configuration/document_template/edit': './assets/lean/js/configuration/document_template/edit.ts',
        'configuration/document_template/overview': './assets/lean/js/configuration/document_template/overview.ts'
    })
    .addEntries({
        'configuration/internal_account_user/mass_operation/delete': './assets/lean/js/configuration/internal_account_user/mass_operation/delete.ts',
        'configuration/internal_account_user/mass_operation/lock': './assets/lean/js/configuration/internal_account_user/mass_operation/lock.ts',
        'configuration/internal_account_user/create': './assets/lean/js/configuration/internal_account_user/create.ts',
        'configuration/internal_account_user/delete': './assets/lean/js/configuration/internal_account_user/delete.ts',
        'configuration/internal_account_user/edit': './assets/lean/js/configuration/internal_account_user/edit.ts',
        'configuration/internal_account_user/overview': './assets/lean/js/configuration/internal_account_user/overview.ts'
    })
    .addEntries({
        'configuration/looking_for_property_request_reporter/embed_code': './assets/lean/js/configuration/looking_for_property_request_reporter/embed_code.ts',
        'configuration/looking_for_property_request_reporter/faq': './assets/lean/js/configuration/looking_for_property_request_reporter/faq.ts',
        'configuration/looking_for_property_request_reporter/general': './assets/lean/js/configuration/looking_for_property_request_reporter/general.ts',
        'configuration/looking_for_property_request_reporter/layout': './assets/lean/js/configuration/looking_for_property_request_reporter/layout.ts',
        'configuration/looking_for_property_request_reporter/mandatory_fields': './assets/lean/js/configuration/looking_for_property_request_reporter/mandatory_fields.ts',
        'configuration/looking_for_property_request_reporter/preview': './assets/lean/js/configuration/looking_for_property_request_reporter/preview.ts',
        'configuration/looking_for_property_request_reporter/privacy_policy': './assets/lean/js/configuration/looking_for_property_request_reporter/privacy_policy.ts',
        'configuration/looking_for_property_request_reporter/property_usage': './assets/lean/js/configuration/looking_for_property_request_reporter/property_usage.ts'
    })
    .addEntries({
        'configuration/open_immo_ftp_credential/create': './assets/lean/js/configuration/open_immo_ftp_credential/create.ts',
        'configuration/open_immo_ftp_credential/delete': './assets/lean/js/configuration/open_immo_ftp_credential/delete.ts',
        'configuration/open_immo_ftp_credential/edit': './assets/lean/js/configuration/open_immo_ftp_credential/edit.ts',
        'configuration/open_immo_ftp_credential/overview': './assets/lean/js/configuration/open_immo_ftp_credential/overview.ts'
    })
    .addEntries({
        'configuration/open_immo_interface/ftp_user': './assets/lean/js/configuration/open_immo_interface/ftp_user.ts'
    })
    .addEntries({
        'configuration/property_vacancy_reporter/embed_code': './assets/lean/js/configuration/property_vacancy_reporter/embed_code.ts',
        'configuration/property_vacancy_reporter/faq': './assets/lean/js/configuration/property_vacancy_reporter/faq.ts',
        'configuration/property_vacancy_reporter/general': './assets/lean/js/configuration/property_vacancy_reporter/general.ts',
        'configuration/property_vacancy_reporter/layout': './assets/lean/js/configuration/property_vacancy_reporter/layout.ts',
        'configuration/property_vacancy_reporter/mandatory_fields': './assets/lean/js/configuration/property_vacancy_reporter/mandatory_fields.ts',
        'configuration/property_vacancy_reporter/preview': './assets/lean/js/configuration/property_vacancy_reporter/preview.ts',
        'configuration/property_vacancy_reporter/privacy_policy': './assets/lean/js/configuration/property_vacancy_reporter/privacy_policy.ts',
        'configuration/property_vacancy_reporter/property_usage': './assets/lean/js/configuration/property_vacancy_reporter/property_usage.ts'
    })
    .addEntries({
        'configuration/survey_result/survey_result_chapter/create': './assets/lean/js/configuration/survey_result/survey_result_chapter/create.ts',
        'configuration/survey_result/survey_result_chapter/delete': './assets/lean/js/configuration/survey_result/survey_result_chapter/delete.ts',
        'configuration/survey_result/survey_result_chapter/edit': './assets/lean/js/configuration/survey_result/survey_result_chapter/edit.ts'
    })
    .addEntries({
        'configuration/survey_result/create': './assets/lean/js/configuration/survey_result/create.ts',
        'configuration/survey_result/delete': './assets/lean/js/configuration/survey_result/delete.ts',
        'configuration/survey_result/edit': './assets/lean/js/configuration/survey_result/edit.ts',
        'configuration/survey_result/overview': './assets/lean/js/configuration/survey_result/overview.ts'
    })
    .addEntry('dashboard/dashboard', './assets/lean/js/dashboard/dashboard.ts')
    .addEntry('documentation/api/settlement_concept', './assets/lean/js/documentation/api/settlement_concept.js')
    .addEntries({
        'external_user/account_user/mass_operation/delete': './assets/lean/js/external_user/account_user/mass_operation/delete.ts',
        'external_user/account_user/delete': './assets/lean/js/external_user/account_user/delete.ts',
        'external_user/account_user/edit': './assets/lean/js/external_user/account_user/edit.ts',
        'external_user/account_user/overview': './assets/lean/js/external_user/account_user/overview.ts'
    })
    .addEntries({
        'external_user/api_user/settlement_concept_account_user/create': './assets/lean/js/external_user/api_user/settlement_concept_account_user/create.ts',
        'external_user/api_user/settlement_concept_account_user/delete': './assets/lean/js/external_user/api_user/settlement_concept_account_user/delete.ts',
        'external_user/api_user/settlement_concept_account_user/edit': './assets/lean/js/external_user/api_user/settlement_concept_account_user/edit.ts',
        'external_user/api_user/settlement_concept_account_user/overview': './assets/lean/js/external_user/api_user/settlement_concept_account_user/overview.ts',
        'external_user/api_user/settlement_concept_account_user/resend_api_token': './assets/lean/js/external_user/api_user/settlement_concept_account_user/resend_api_token.ts'
    })
    .addEntries({
        'external_user/user_invitation/mass_operation/delete': './assets/lean/js/external_user/user_invitation/mass_operation/delete.ts',
        'external_user/user_invitation/mass_operation/send_invitation': './assets/lean/js/external_user/user_invitation/mass_operation/send_invitation.ts',
        'external_user/user_invitation/create': './assets/lean/js/external_user/user_invitation/create.ts',
        'external_user/user_invitation/delete': './assets/lean/js/external_user/user_invitation/delete.ts',
        'external_user/user_invitation/edit': './assets/lean/js/external_user/user_invitation/edit.ts',
        'external_user/user_invitation/property_provider_overview': './assets/lean/js/external_user/user_invitation/property_provider_overview.ts',
        'external_user/user_invitation/property_seeker_overview': './assets/lean/js/external_user/user_invitation/property_seeker_overview.ts'
    })
    .addEntries({
        'external_user/user_registration/mass_operation/activate': './assets/lean/js/external_user/user_registration/mass_operation/activate.ts',
        'external_user/user_registration/mass_operation/delete': './assets/lean/js/external_user/user_registration/mass_operation/delete.ts',
        'external_user/user_registration/delete': './assets/lean/js/external_user/user_registration/delete.ts',
        'external_user/user_registration/edit': './assets/lean/js/external_user/user_registration/edit.ts',
        'external_user/user_registration/property_provider_overview': './assets/lean/js/external_user/user_registration/property_provider_overview.ts',
        'external_user/user_registration/property_seeker_overview': './assets/lean/js/external_user/user_registration/property_seeker_overview.ts'
    })
    .addEntry('help/customer_care/support_request', './assets/lean/js/help/customer_care/support_request.ts')
    .addEntries({
        'notification/mass_operation/delete': './assets/lean/js/notification/mass_operation/delete.ts',
        'notification/delete': './assets/lean/js/notification/delete.ts',
        'notification/overview': './assets/lean/js/notification/overview.ts'
    })
    .addEntries({
        'person_management/follow_up/overview': './assets/lean/js/person_management/follow_up/overview.ts',
    })
    .addEntries({
        'person_management/person/contact/create': './assets/lean/js/person_management/person/contact/create.ts',
        'person_management/person/contact/delete': './assets/lean/js/person_management/person/contact/delete.ts',
        'person_management/person/contact/edit': './assets/lean/js/person_management/person/contact/edit.ts',
        'person_management/person/contact/overview': './assets/lean/js/person_management/person/contact/overview.ts'
    })
    .addEntries({
        'person_management/person/looking_for_property_request/overview': './assets/lean/js/person_management/person/looking_for_property_request/overview.ts'
    })
    .addEntries({
        'person_management/person/mass_operation/delete': './assets/lean/js/person_management/person/mass_operation/delete.ts'
    })
    .addEntries({
        'person_management/person/occurrence/create': './assets/lean/js/person_management/person/occurrence/create.ts',
        'person_management/person/occurrence/delete': './assets/lean/js/person_management/person/occurrence/delete.ts',
        'person_management/person/occurrence/edit': './assets/lean/js/person_management/person/occurrence/edit.ts',
        'person_management/person/occurrence/overview': './assets/lean/js/person_management/person/occurrence/overview.ts'
    })
    .addEntries({
        'person_management/person/property/overview': './assets/lean/js/person_management/person/property/overview.ts'
    })
    .addEntries({
        'person_management/person/base_data': './assets/lean/js/person_management/person/base_data.ts',
        'person_management/person/create': './assets/lean/js/person_management/person/create.ts',
        'person_management/person/delete': './assets/lean/js/person_management/person/delete.ts',
        'person_management/person/overview': './assets/lean/js/person_management/person/overview.ts',
    })
    .addEntries({
        'property_vacancy_management/building_unit/archive/mass_operation/delete': './assets/lean/js/property_vacancy_management/building_unit/archive/mass_operation/delete.ts',
        'property_vacancy_management/building_unit/archive/mass_operation/unarchive': './assets/lean/js/property_vacancy_management/building_unit/archive/mass_operation/unarchive.ts',
        'property_vacancy_management/building_unit/archive/overview_gallery': './assets/lean/js/property_vacancy_management/building_unit/archive/overview_gallery.ts',
        'property_vacancy_management/building_unit/archive/overview_list': './assets/lean/js/property_vacancy_management/building_unit/archive/overview_list.ts',
        'property_vacancy_management/building_unit/archive/overview_map': './assets/lean/js/property_vacancy_management/building_unit/archive/overview_map.ts'
    })
    .addEntries({
        'property_vacancy_management/building_unit/document/delete': './assets/lean/js/property_vacancy_management/building_unit/document/delete.ts',
        'property_vacancy_management/building_unit/document/edit': './assets/lean/js/property_vacancy_management/building_unit/document/edit.ts',
        'property_vacancy_management/building_unit/document/overview': './assets/lean/js/property_vacancy_management/building_unit/document/overview.ts'
    })
    .addEntries({
        'property_vacancy_management/building_unit/mass_operation/assign_to_account_user': './assets/lean/js/property_vacancy_management/building_unit/mass_operation/assign_to_account_user.ts',
        'property_vacancy_management/building_unit/mass_operation/delete': './assets/lean/js/property_vacancy_management/building_unit/mass_operation/delete.ts',
        'property_vacancy_management/building_unit/mass_operation/archive': './assets/lean/js/property_vacancy_management/building_unit/mass_operation/archive.ts'
    })
    .addEntries({
        'property_vacancy_management/building_unit/photo/delete': './assets/lean/js/property_vacancy_management/building_unit/photo/delete.ts',
        'property_vacancy_management/building_unit/photo/edit': './assets/lean/js/property_vacancy_management/building_unit/photo/edit.ts',
        'property_vacancy_management/building_unit/photo/overview': './assets/lean/js/property_vacancy_management/building_unit/photo/overview.ts'
    })
    .addEntries({
        'property_vacancy_management/building_unit/property_contact_person/occurrence/create': './assets/lean/js/property_vacancy_management/building_unit/property_contact_person/occurrence/create.ts',
        'property_vacancy_management/building_unit/property_contact_person/occurrence/delete': './assets/lean/js/property_vacancy_management/building_unit/property_contact_person/occurrence/delete.ts',
        'property_vacancy_management/building_unit/property_contact_person/occurrence/edit': './assets/lean/js/property_vacancy_management/building_unit/property_contact_person/occurrence/edit.ts'
    })
    .addEntries({
        'property_vacancy_management/building_unit/property_contact_person/create': './assets/lean/js/property_vacancy_management/building_unit/property_contact_person/create.ts',
        'property_vacancy_management/building_unit/property_contact_person/delete': './assets/lean/js/property_vacancy_management/building_unit/property_contact_person/delete.ts',
        'property_vacancy_management/building_unit/property_contact_person/edit': './assets/lean/js/property_vacancy_management/building_unit/property_contact_person/edit.ts',
        'property_vacancy_management/building_unit/property_contact_person/overview': './assets/lean/js/property_vacancy_management/building_unit/property_contact_person/overview.ts'
    })
    .addEntries({
        'property_vacancy_management/building_unit/property_owner/occurrence/create': './assets/lean/js/property_vacancy_management/building_unit/property_owner/occurrence/create.ts',
        'property_vacancy_management/building_unit/property_owner/occurrence/delete': './assets/lean/js/property_vacancy_management/building_unit/property_owner/occurrence/delete.ts',
        'property_vacancy_management/building_unit/property_owner/occurrence/edit': './assets/lean/js/property_vacancy_management/building_unit/property_owner/occurrence/edit.ts'
    })
    .addEntries({
        'property_vacancy_management/building_unit/property_owner/create': './assets/lean/js/property_vacancy_management/building_unit/property_owner/create.ts',
        'property_vacancy_management/building_unit/property_owner/edit': './assets/lean/js/property_vacancy_management/building_unit/property_owner/edit.ts',
        'property_vacancy_management/building_unit/property_owner/overview': './assets/lean/js/property_vacancy_management/building_unit/property_owner/overview.ts',
        'property_vacancy_management/building_unit/property_owner/remove': './assets/lean/js/property_vacancy_management/building_unit/property_owner/remove.ts'
    })
    .addEntries({
        'property_vacancy_management/building_unit/property_user/occurrence/create': './assets/lean/js/property_vacancy_management/building_unit/property_user/occurrence/create.ts',
        'property_vacancy_management/building_unit/property_user/occurrence/delete': './assets/lean/js/property_vacancy_management/building_unit/property_user/occurrence/delete.ts',
        'property_vacancy_management/building_unit/property_user/occurrence/edit': './assets/lean/js/property_vacancy_management/building_unit/property_user/occurrence/edit.ts'
    })
    .addEntries({
        'property_vacancy_management/building_unit/property_user/create': './assets/lean/js/property_vacancy_management/building_unit/property_user/create.ts',
        'property_vacancy_management/building_unit/property_user/edit': './assets/lean/js/property_vacancy_management/building_unit/property_user/edit.ts',
        'property_vacancy_management/building_unit/property_user/overview': './assets/lean/js/property_vacancy_management/building_unit/property_user/overview.ts',
        'property_vacancy_management/building_unit/property_user/remove': './assets/lean/js/property_vacancy_management/building_unit/property_user/remove.ts'
    })
    .addEntries({
        'property_vacancy_management/building_unit/archive': './assets/lean/js/property_vacancy_management/building_unit/archive.ts',
        'property_vacancy_management/building_unit/assign_to_account_user': './assets/lean/js/property_vacancy_management/building_unit/assign_to_account_user.ts',
        'property_vacancy_management/building_unit/basic_data': './assets/lean/js/property_vacancy_management/building_unit/basic_data.ts',
        'property_vacancy_management/building_unit/create': './assets/lean/js/property_vacancy_management/building_unit/create.ts',
        'property_vacancy_management/building_unit/delete': './assets/lean/js/property_vacancy_management/building_unit/delete.ts',
        'property_vacancy_management/building_unit/detail': './assets/lean/js/property_vacancy_management/building_unit/detail.ts',
        'property_vacancy_management/building_unit/external_contact_person': './assets/lean/js/property_vacancy_management/building_unit/external_contact_person',
        'property_vacancy_management/building_unit/location_map': './assets/lean/js/property_vacancy_management/building_unit/location_map.ts',
        'property_vacancy_management/building_unit/open_immo_export': './assets/lean/js/property_vacancy_management/building_unit/open_immo_export.ts',
        'property_vacancy_management/building_unit/overview_gallery': './assets/lean/js/property_vacancy_management/building_unit/overview_gallery.ts',
        'property_vacancy_management/building_unit/overview_list': './assets/lean/js/property_vacancy_management/building_unit/overview_list.ts',
        'property_vacancy_management/building_unit/overview_map': './assets/lean/js/property_vacancy_management/building_unit/overview_map.ts',
        'property_vacancy_management/building_unit/pdf_expose': './assets/lean/js/property_vacancy_management/building_unit/pdf_expose.ts',
        'property_vacancy_management/building_unit/property_marketing_information': './assets/lean/js/property_vacancy_management/building_unit/property_marketing_information.ts',
        'property_vacancy_management/building_unit/unarchive': './assets/lean/js/property_vacancy_management/building_unit/unarchive.ts'
    })
    .addEntries({
        'property_vacancy_management/property_excel_import/overview': './assets/lean/js/property_vacancy_management/property_excel_import/overview.ts'
    })
    .addEntries({
        'property_vacancy_management/property_vacancy_report/delete': './assets/lean/js/property_vacancy_management/property_vacancy_report/delete.ts',
        'property_vacancy_management/property_vacancy_report/note_delete': './assets/lean/js/property_vacancy_management/property_vacancy_report/note_delete.ts',
        'property_vacancy_management/property_vacancy_report/note_edit': './assets/lean/js/property_vacancy_management/property_vacancy_report/note_edit.ts',
        'property_vacancy_management/property_vacancy_report/processed_overview': './assets/lean/js/property_vacancy_management/property_vacancy_report/processed_overview.ts',
        'property_vacancy_management/property_vacancy_report/report': './assets/lean/js/property_vacancy_management/property_vacancy_report/report.ts',
        'property_vacancy_management/property_vacancy_report/report_to_building_unit': './assets/lean/js/property_vacancy_management/property_vacancy_report/report_to_building_unit.ts',
        'property_vacancy_management/property_vacancy_report/unprocessed_overview': './assets/lean/js/property_vacancy_management/property_vacancy_report/unprocessed_overview.ts'
    })
    .addEntries({
        'public_access/base': './assets/lean/js/public_access/base.ts',
        'public_access/login': './assets/lean/js/public_access/login.ts',
        'public_access/authentification_token_generation': './assets/lean/js/public_access/authentification_token_generation.ts',
        'public_access/password_forgotten': './assets/lean/js/public_access/password_forgotten.ts',
        'public_access/password_reset': './assets/lean/js/public_access/password_reset.ts',
        'public_access/privacy_policy_provider': './assets/lean/js/public_access/privacy_policy_provider.ts',
        'public_access/privacy_policy_seeker': './assets/lean/js/public_access/privacy_policy_seeker.ts',
        'public_access/gtc_provider': './assets/lean/js/public_access/gtc_provider.ts',
        'public_access/gtc_seeker': './assets/lean/js/public_access/gtc_seeker.ts'
    })
    .addEntries({
        'report/city_goal/city_goal': './assets/lean/js/report/city_goal/city_goal.ts',
        'report/early_warning_system/early_warning_system': './assets/lean/js/report/early_warning_system/early_warning_system.ts',
        'report/location_indicators/location_indicators': './assets/lean/js/report/location_indicators/location_indicators.ts',
        'report/pedestrian_frequency/pedestrian_frequency': './assets/lean/js/report/pedestrian_frequency/pedestrian_frequency.ts'
    })
    .addEntries({
        'report/insights/survey_result/overview': './assets/lean/js/report/insights/survey_result/overview.ts',
        'report/insights/survey_result/survey_result': './assets/lean/js/report/insights/survey_result/survey_result.ts'
    })
    .addEntries({
        'report/insights/general_statistics': './assets/lean/js/report/insights/general_statistics.ts',
        'report/insights/impulses': './assets/lean/js/report/insights/impulses.ts',
        'report/insights/property_vacancy_data': './assets/lean/js/report/insights/property_vacancy_data.ts',
        'report/insights/settlement_information': './assets/lean/js/report/insights/settlement_information.ts'
    })
    .addEntries({
        'user_registration/user_registration_with_invitation': './assets/lean/js/user_registration/user_registration_with_invitation.ts',
        'user_registration/user_registration_without_invitation': './assets/lean/js/user_registration/user_registration_without_invitation.ts'
    })

    .copyFiles({
        from: './assets/lean/images',
        to: 'images/[path][name].[ext]',
        pattern: /\.(svg|gif|png|jpg)$/
    })
    .splitEntryChunks()
    .enableSingleRuntimeChunk()
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    .enableVersioning(Encore.isProduction())
    .configureBabel((config) => {
        config.plugins.push('@babel/plugin-proposal-class-properties');
    })

    .configureBabelPresetEnv((config) => {
        config.useBuiltIns = 'usage';
        config.corejs = 3;
    })
    .enableSassLoader()
    .enableTypeScriptLoader()
    .enableReactPreset()
;

const leanConfig = Encore.getWebpackConfig();
leanConfig.name = 'leanConfig';

Encore.reset();

Encore
    .setOutputPath('public/property_vacancy_reporter/build/')
    .setPublicPath('/property_vacancy_reporter/build')

    .addEntry('api', './assets/property_vacancy_reporter/src/index.tsx')

    .disableSingleRuntimeChunk()
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    .configureBabel((config) => {
        config.plugins.push('@babel/plugin-proposal-class-properties');
    })
    .configureBabelPresetEnv((config) => {
        config.useBuiltIns = 'usage';
        config.corejs = 3;
    })
    .configureCssLoader(options => {
        options.modules = true
    })
    .enableSassLoader()
    .enableTypeScriptLoader()
    .enableReactPreset()
;

const propertyVacancyReporterConfig = Encore.getWebpackConfig();
propertyVacancyReporterConfig.name = 'propertyVacancyReporterConfig';

Encore.reset();

Encore
   .setOutputPath('public/looking_for_property_request_reporter/build/')
   .setPublicPath('/looking_for_property_request_reporter/build')

   .addEntry('api', './assets/looking_for_property_request_reporter/src/index.tsx')

   .disableSingleRuntimeChunk()
   .cleanupOutputBeforeBuild()
   .enableBuildNotifications()
   .enableSourceMaps(!Encore.isProduction())
   .configureBabel((config) => {
       config.plugins.push('@babel/plugin-proposal-class-properties');
   })
   .configureBabelPresetEnv((config) => {
       config.useBuiltIns = 'usage';
       config.corejs = 3;
   })
   .configureCssLoader(options => {
       options.modules = true
   })
   .enableSassLoader()
   .enableTypeScriptLoader()
   .enableReactPreset()
;

const lookingForPropertyRequestReportConfig = Encore.getWebpackConfig();
lookingForPropertyRequestReportConfig.name = 'lookingForPropertyRequestReportConfig';

Encore.reset();

module.exports = [leanConfig, propertyVacancyReporterConfig, lookingForPropertyRequestReportConfig];
