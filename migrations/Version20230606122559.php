<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230606122559 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE property_vacancy_report CHANGE property_description property_description LONGTEXT DEFAULT NULL, CHANGE place_description place_description LONGTEXT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE property_vacancy_report CHANGE property_description property_description TINYTEXT DEFAULT NULL, CHANGE place_description place_description TINYTEXT DEFAULT NULL');
    }
}
